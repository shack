dnl Support functions for configure script
dnl Copyright(c) 2002 Justin David Smith
dnl


dnl Include the libmojave-specific macros.
sinclude(libmojave.m4)


dnl Include the mcc macros.
sinclude(mcc.m4)
