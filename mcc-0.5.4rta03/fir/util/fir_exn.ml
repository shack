(*
 * Normal FIR exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir

(*
 * Exception values.
 *)
type fir_error =
   UnboundVar of var
 | UnboundType of ty_var
 | AtomNotAVar of atom
 | TypeError of ty * ty
 | TypeAtomError of ty * ty * atom
 | TypeError4 of ty * ty * ty * ty
 | TypeErrorMutable4 of ty * ty * ty * bool * ty * bool
 | InvalidTyDelayed of ty
 | ArityMismatch of int * int
 | NotAFunction of ty
 | NotASubscript of ty
 | NotAnAggregate of ty
 | NotAUnion of ty
 | NotAUnionDef of tydef
 | NotAnIntSet of set
 | NotARawIntSet of set
 | NotAFloatSet of set
 | TyDefError of tydef
 | SetError of set * set
 | FieldOutOfBounds of ty * int
 | IncompleteMatch of set * set
 | IndexOutOfBounds of ty * int
 | IllegalAlloc of ty
 | SubscriptOutOfBounds of int
 | SubscriptError of ty * ty
 | InternalError of string
 | NotImplemented of string
 | StringError of string
 | StringIntError of string * int
 | StringVarError of string * var
 | StringAtomError of string * atom
 | StringTypeError of string * ty
 | StringTydefError of string * tydef
 | StringIntTypeError of string * int * ty
 | StringFormatError of string * (Format.formatter -> unit)
 | MarshalError

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
