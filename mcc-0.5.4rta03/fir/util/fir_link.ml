(*
 * FIR linking.  the main thing that we have to do here is
 * check that each import/export pair have the same type.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mc_string_util
open Printf
open Symbol

open Fir
open Fir_mprog
open Fir_standardize

(*
 * Default file name for output.
 * BUG: better figure out how to do linking of different file classes.
 *)
let file_info =
   { file_dir = ".";
     file_name = "link-obj";
     file_class = FileNaml
   }

(*
 * Build the export table.
 * It is illegal to have more than one definition
 * for a symbol.
 *)
let insert_export export v { export_name = s; export_type = ty } =
   StringTable.filter_add export s (function
      Some _ ->
         if s = "init" then
            { export_name = v; export_type = ty }
         else
            raise (Invalid_argument ("Fir_link: insert_export: duplicate definition of symbol " ^ s))
    | None ->
         { export_name = v; export_type = ty })

let build_export progs =
   List.fold_left (fun export prog ->
         SymbolTable.fold insert_export export prog.prog_export) (**)
      StringTable.empty progs

(*
 * Invert the export string table.
 *)
let invert_export export =
   StringTable.fold (fun export s { export_name = v; export_type = ty } ->
         SymbolTable.add export v { export_name = s; export_type = ty }) SymbolTable.empty export

(*
 * Resolve imports from the export table.
 * If the export is defined, use the new name.
 * For each prog, call the standardizer to do the
 * linking.
 *
 * venv is the table of variable renaming.
 * import is the resulting import list.
 *)
let link_import_prog export import venv prog =
   SymbolTable.fold (fun (venv, import) v info ->
         let { import_name = s } = info in
            try
               let { export_name = v'; export_type = ty' } = StringTable.find export s in
               let venv = SymbolTable.add venv v v' in
                  venv, import
            with
               Not_found ->
                  let import = SymbolTable.add import v info in
                     venv, import) (venv, import) prog.prog_import

(*
 * Look through all the imports.
 *)
let build_import export progs =
   List.fold_left (fun (venv, import) prog ->
         link_import_prog export import venv prog) (**)
      (SymbolTable.empty, SymbolTable.empty) progs

(*
 * SymbolTable concatenation.
 *)
let concat_tables tables =
   List.fold_left (fun union table ->
         SymbolTable.fold (fun union v x ->
               SymbolTable.add union v x) union table) SymbolTable.empty tables

let collect_types progs =
   let tables = List.map (fun prog -> prog.prog_types) progs in
      concat_tables tables

let collect_globals progs =
   let tables = List.map (fun prog -> prog.prog_globals) progs in
      concat_tables tables

let collect_funs progs =
   let tables = List.map (fun prog -> prog.prog_funs) progs in
      concat_tables tables

let collect_names progs =
   let tables = List.map (fun prog -> prog.prog_names) progs in
      concat_tables tables

let collect_frames progs =
   let frames = List.map (fun prog -> prog.prog_frames) progs in
      concat_tables frames

(*
 * For linking, collect all the exports.
 * Then link all the corresponding imports.
 * Then concat all the types, globals, and funs.
 *
 * We assume that all the programs have been
 * standardized--they don't share symbols.
 *)
let link_prog progs =
   (* Build the new export, import list *)
   let export = build_export progs in
   let venv, import = build_import export progs in
   let export = invert_export export in

   (* Concat all the other tables *)
   let types = collect_types progs in
   let globals = collect_globals progs in
   let funs = collect_funs progs in
   let names = collect_names progs in
   let frames = collect_frames progs in

   (* Build the linked program *)
   let prog =
      { prog_file = file_info;
        prog_import = import;
        prog_export = export;
        prog_types = types;
        prog_frames = frames;
        prog_globals = globals;
        prog_funs = funs;
        prog_names = names
      }
   in

   (* Now rename all the imports *)
   let _, prog = standardize_prog_venv venv prog in
      prog

(*
 * Link all the fir, then extra the globals ordering.
 *)
let link_mprog mprogs =
   eprintf "warning:  Fir_link.link_mprog, as written, is DANGEROUS to system migration  --JDS, 2002.06.06@.";
   let progs = List.map (fun mprog -> mprog.marshal_fir) mprogs in
   let prog = link_prog progs in
      mprog_of_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
