(*
 * Define a type substitution.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_print

module Pos = MakePos (struct let name = "Fir_subst" end)
open Pos

(************************************************************************
 * FREE VARIABLES
 ************************************************************************)

(*
 * Calculate the free type vars.
 *)
let rec free_vars_type vars ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyDelayed
    | TyRawData
    | TyProject _
    | TyPointer _
    | TyDTuple (_, None) ->
         vars
    | TyFun (ty_vars, ty_res) ->
         let vars = free_vars_type_list vars ty_vars in
            free_vars_type vars ty_res
    | TyUnion (tv, tl, i) ->
         free_vars_type_list vars tl
    | TyTuple (_, fields)
    | TyDTuple (_, Some fields)
    | TyTag (_, fields) ->
         free_vars_mutable_type_list vars fields
    | TyArray ty ->
         free_vars_type vars ty
    | TyCase ty ->
         free_vars_type vars ty
    | TyVar v ->
         SymbolSet.add vars v
    | TyApply (_, tyl)
    | TyFrame (_, tyl) ->
         free_vars_type_list vars tyl
    | TyAll (vars', ty)
    | TyExists (vars', ty) ->
         let vars = free_vars_type vars ty in
            SymbolSet.subtract_list vars vars'
    | TyObject (v, ty) ->
         let vars = free_vars_type vars ty in
            SymbolSet.remove vars v

and free_vars_type_list vars tyl =
   List.fold_left free_vars_type vars tyl

and free_vars_mutable_type_list vars fields =
   List.fold_left (fun vars (ty, _) ->
         free_vars_type vars ty) vars fields

let free_vars_list ty =
   SymbolSet.to_list (free_vars_type SymbolSet.empty ty)


(*
 * Calcuate the free types vars of an atom.
 *)
let rec free_vars_atom_type vars a =
   match a with
      AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomLabel _
    | AtomSizeof _
    | AtomVar _
    | AtomFun _
    | AtomTyUnpack _ ->
         vars
    | AtomNil ty
    | AtomConst (ty, _, _) ->
         free_vars_type vars ty
    | AtomTyApply (a, ty, tyl) ->
         free_vars_atom_type (free_vars_type (free_vars_type_list vars tyl) ty) a
    | AtomTyPack (_, ty, tyl) ->
         free_vars_type (free_vars_type_list vars tyl) ty
    | AtomUnop (_, a) ->
         free_vars_atom_type vars a
    | AtomBinop (op, a1, a2) ->
         let vars =
            match op with
               EqEqOp ty
             | NeqEqOp ty ->
                  free_vars_type vars ty
             | _ ->
                  vars
         in
            free_vars_atom_type (free_vars_atom_type vars a1) a2

and free_vars_atom_type_list vars args =
   List.fold_left free_vars_atom_type vars args

let free_vars_atom_type_opt_list vars args =
   List.fold_left (fun vars a ->
         match a with
            Some a ->
               free_vars_atom_type vars a
          | None ->
               vars) vars args

(*
 * For allocations, modify AllocFrame to include the new type args.
 *)
let free_vars_alloc_op_type vars op =
   match op with
      AllocTuple (_, ty_vars, ty, args)
    | AllocUnion (ty_vars, ty, _, _, args) ->
         SymbolSet.subtract_list (free_vars_type (free_vars_atom_type_list vars args) ty) ty_vars
    | AllocDTuple (ty, _, _, args) ->
         free_vars_type (free_vars_atom_type_list vars args) ty
    | AllocArray (ty, args) ->
         free_vars_type (free_vars_atom_type_list vars args) ty
    | AllocVArray (ty, _, a1, a2) ->
         free_vars_type (free_vars_atom_type (free_vars_atom_type vars a2) a1) ty
    | AllocMalloc (ty, a) ->
         free_vars_type (free_vars_atom_type vars a) ty
    | AllocFrame (_, tyl) ->
         free_vars_type_list vars tyl

(*
 * For the rest of the conversion, nothing special happens.
 * In fact, we need to define a generic module to perform
 * grunge code like this.  All we do in the rest is map
 * the closure calls over all the types, atoms, and expressions
 * in the program.
 *)
let free_vars_tailop_type vars op =
   match op with
      TailSysMigrate (_, a1, a2, a3, args) ->
         free_vars_atom_type (free_vars_atom_type (free_vars_atom_type (free_vars_atom_type_list vars args) a3) a2) a1
    | TailAtomic (a1, a2, args)
    | TailAtomicCommit (a1, a2, args) ->
         free_vars_atom_type (free_vars_atom_type (free_vars_atom_type_list vars args) a2) a1
    | TailAtomicRollback (a1, a2) ->
         free_vars_atom_type (free_vars_atom_type vars a2) a1

let free_vars_pred_type vars pred =
   match pred with
      IsMutable a ->
         free_vars_atom_type vars a
    | Reserve (a1, a2) ->
         free_vars_atom_type (free_vars_atom_type vars a1) a2
    | BoundsCheck (_, a1, a2, a3) ->
         free_vars_atom_type (free_vars_atom_type (free_vars_atom_type vars a3) a2) a1
    | ElementCheck (ty, _, a1, a2) ->
         free_vars_type (free_vars_atom_type (free_vars_atom_type vars a2) a1) ty

let free_vars_init_type vars init =
   match init with
      InitAtom a ->
         free_vars_atom_type vars a
    | InitAlloc op ->
         free_vars_alloc_op_type vars op
    | InitTag (_, fields) ->
         free_vars_mutable_type_list vars fields
    | InitRawData _
    | InitNames _ ->
         vars

let rec free_vars_exp_type vars e =
   match dest_exp_core e with
      LetAtom (_, ty, a, e) ->
         free_vars_type (free_vars_atom_type (free_vars_exp_type vars e) a) ty
    | LetExt (_, ty1, _, _, ty2, ty_args, args, e) ->
         free_vars_type_list (free_vars_type (free_vars_type (free_vars_atom_type_list (free_vars_exp_type vars e) args) ty2) ty1) ty_args
    | TailCall (_, f, args) ->
         free_vars_atom_type (free_vars_atom_type_list vars args) f
    | SpecialCall (_, op) ->
         free_vars_tailop_type vars op
    | Match (a, cases) ->
         let vars =
            List.fold_left (fun vars (_, _, e) ->
                  free_vars_exp_type vars e) vars cases
         in
            free_vars_atom_type vars a
    | MatchDTuple (a, cases) ->
         let vars =
            List.fold_left (fun vars (_, _, e) ->
                  free_vars_exp_type vars e) vars cases
         in
            free_vars_atom_type vars a
    | TypeCase (a1, a2, _, _, e1, e2) ->
         free_vars_atom_type (free_vars_atom_type (free_vars_exp_type (free_vars_exp_type vars e2) e1) a2) a1
    | LetAlloc (_, op, e) ->
         free_vars_alloc_op_type (free_vars_exp_type vars e) op
    | LetSubscript (_, _, ty, a1, a2, e) ->
         free_vars_type (free_vars_atom_type (free_vars_atom_type (free_vars_exp_type vars e) a2) a1) ty
    | SetSubscript (_, _, a1, a2, ty, a3, e) ->
         free_vars_atom_type (free_vars_atom_type (free_vars_type (free_vars_atom_type (free_vars_exp_type vars e) a3) ty) a2) a1
    | LetGlobal (_, _, ty, _, e) ->
         free_vars_type (free_vars_exp_type vars e) ty
    | SetGlobal (_, _, _, ty, a, e) ->
         free_vars_type (free_vars_atom_type (free_vars_exp_type vars e) a) ty
    | Memcpy (_, _, a1, a2, a3, a4, a5, e) ->
         free_vars_atom_type (free_vars_atom_type (free_vars_atom_type (free_vars_atom_type (free_vars_atom_type (free_vars_exp_type vars e) a5) a4) a3) a2) a1
    | Call (_, f, args, e) ->
         free_vars_atom_type (free_vars_atom_type_opt_list (free_vars_exp_type vars e) args) f
    | Assert (_, pred, e) ->
         free_vars_pred_type (free_vars_exp_type vars e) pred
    | Debug (_, e) ->
         free_vars_exp_type vars e

(************************************************************************
 * SUBSTITUTION
 ************************************************************************)

(*
 * We need two substitutions, one for type variables,
 * and one for mutable variables.
 *)
type subst =
   { subst_types : ty SymbolTable.t;
     subst_mutable : mutable_flag SymbolTable.t
   }

let subst_empty =
   { subst_types = SymbolTable.empty;
     subst_mutable = SymbolTable.empty
   }

(*
 * Check additions to the substitution to make sure
 * we don't try to add polymorphic variables.
 *)
let subst_add_type_var subst v ty =
   { subst with subst_types = SymbolTable.add subst.subst_types v ty }

let subst_add_mutable_var subst v b =
   { subst with subst_mutable = SymbolTable.add subst.subst_mutable v b }

(*
 * Lookup.
 *)
let subst_lookup_type_var subst v =
   SymbolTable.find subst.subst_types v

let subst_lookup_mutable_var subst v =
   SymbolTable.find subst.subst_mutable v

(*
 * Removal.
 *)
let subst_remove_type_var subst v =
   { subst with subst_types = SymbolTable.remove subst.subst_types v }

let subst_remove_type_vars subst vars =
   { subst with subst_types = List.fold_left SymbolTable.remove subst.subst_types vars }

let subst_remove_mutable_var subst v =
   { subst with subst_mutable = SymbolTable.remove subst.subst_mutable v }

(*
 * Printing.
 *)
let pp_print_menv buf menv =
   fprintf buf "@[<hv 0>@[<hv 3>mutable {";
   SymbolTable.iter (fun v b ->
         fprintf buf "@ %a = %a;" (**)
            pp_print_symbol v
            pp_print_mutable_flag b) menv;
   fprintf buf "@]@ }@]"

let pp_print_tenv buf subst =
   Format.fprintf buf "@[<hv 0>@[<hv 3>types {";
   SymbolTable.iter (fun v ty ->
         Format.fprintf buf "@ @[<hv 3>'%a =@ %a@]" (**)
            pp_print_symbol v
            pp_print_type ty) subst;
   Format.fprintf buf "@]@ }@]"

let pp_print_subst buf subst =
   let { subst_types = tenv;
         subst_mutable = menv
       } = subst
   in
      fprintf buf "@[<hv 0>@[<hv 3>subst {@ %a@ %a@]@ }@]" (**)
         pp_print_tenv tenv
         pp_print_menv menv

(*
 * Type substitution.
 *)
let rec subst_type subst ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyDelayed
    | TyRawData
    | TyProject _
    | TyPointer _ ->
         ty
    | TyFrame (v, tyl) ->
         let tyl = subst_types subst tyl in
            TyFrame (v, tyl)
    | TyFun (ty_vars, ty_res) ->
         let ty_vars = subst_types subst ty_vars in
         let ty_res = subst_type subst ty_res in
            TyFun (ty_vars, ty_res)
    | TyUnion (tv, tl, i) ->
         TyUnion (tv, subst_types subst tl, i)
    | TyTuple (tclass, fields) ->
         TyTuple (tclass, subst_mutable_types subst fields)
    | TyDTuple (ty_var, fields) ->
         let fields =
            match fields with
               Some fields -> Some (subst_mutable_types subst fields)
             | None -> None
         in
            TyDTuple (ty_var, fields)
    | TyTag (ty_var, fields) ->
         TyTag (ty_var, subst_mutable_types subst fields)
    | TyArray ty ->
         TyArray (subst_type subst ty)
    | TyCase ty ->
         TyCase (subst_type subst ty)
    | TyVar v ->
         (try subst_type (subst_remove_type_var subst v) (subst_lookup_type_var subst v) with
             Not_found ->
                ty)
    | TyApply (v, tyl) ->
         TyApply (v, subst_types subst tyl)
    | TyAll (vars, ty) ->
         let subst = subst_remove_type_vars subst vars in
            TyAll (vars, subst_type subst ty)
    | TyExists (vars, ty) ->
         let subst = subst_remove_type_vars subst vars in
            TyExists (vars, subst_type subst ty)
    | TyObject (v, ty) ->
         let subst = subst_remove_type_var subst v in
            TyObject (v, subst_type subst ty)

and subst_types subst tyl =
   List.map (subst_type subst) tyl

and subst_mutable subst b =
   match b with
      Mutable
    | Immutable
    | MutableDelayed ->
         b
    | MutableVar v ->
         try subst_mutable (subst_remove_mutable_var subst v) (subst_lookup_mutable_var subst v) with
            Not_found ->
               b

and subst_mutable_type subst (ty, b) =
   subst_type subst ty, subst_mutable subst b

and subst_mutable_types subst tyl =
   List.map (subst_mutable_type subst) tyl

(*
 * Simulataneous substitution.
 *)
let subst_type_simul pos ty vars tyl =
   let pos = string_pos "subst_type_simul" pos in
   let len1 = List.length vars in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let subst = List.fold_left2 subst_add_type_var subst_empty vars tyl in
      subst_type subst ty

(*
 * Map a function over all the types in the unop.
 *)
let subst_type_unop subst op =
   match op with
      NotEnumOp _

      (* Negation (arithmetic and bitwise) for NAML ints *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Negation (arithmetic and bitwise) for native ints *)
    | UMinusRawIntOp _
    | NotRawIntOp _

      (*
       * RawBitFieldOp (pre, signed, offset, length):
       *    Extracts the bitfield in an integer value, which starts at bit
       *    <offset> (counting from LSB) and containing <length> bits. The
       *    bit shift and length are constant values.  To access a variable
       *    bitfield you must do a manual shift/mask; this optimization is
       *    only good for constant values.  (pre, signed) refer to the
       *    rawint precision of the result; the input should be int32.
       *)
    | RawBitFieldOp     _

      (* Unary floating-point operations *)
    | UMinusFloatOp     _
    | AbsFloatOp        _
    | SinFloatOp        _
    | CosFloatOp        _
    | TanFloatOp        _
    | ASinFloatOp       _
    | ACosFloatOp       _
    | ATanFloatOp       _
    | SinHFloatOp       _
    | CosHFloatOp       _
    | TanHFloatOp       _
    | ExpFloatOp        _
    | LogFloatOp        _
    | Log10FloatOp      _
    | SqrtFloatOp       _
    | CeilFloatOp       _
    | FloorFloatOp      _

      (*
       * Coercions:
       *    Int refers to an ML-style integer
       *    Float refers to floating-point value
       *    RawInt refers to native int, any precision
       *
       * In operators where both terms have precision qualifiers, the
       * destination precision is always specified before the source
       * precision.
       *)

      (* Coercions to int *)
    | IntOfFloatOp      _
    | IntOfRawIntOp     _

      (* Coerce to float *)
    | FloatOfIntOp      _
    | FloatOfFloatOp    _
    | FloatOfRawIntOp   _

      (* Coerce to rawint *)
    | RawIntOfIntOp     _
    | RawIntOfEnumOp    _
    | RawIntOfFloatOp   _

      (*
       * Coerce a rawint _
       * First pair is destination precision, second pair is source.
       *)
    | RawIntOfRawIntOp  _

      (*
       * Integer<->pointer coercions (only for C, not inherently safe)
       * These operators are specifically related to infix pointers...
       *)
    | RawIntOfPointerOp _
    | PointerOfRawIntOp _

      (*
       * Create an infix pointer from a block pointer.  The infix pointer
       * so that both the base and _
       * to the beginning _
       *)
    | PointerOfBlockOp _ ->
         op

      (*
       * Get the block length.
       *)
    | LengthOfBlockOp (subop, ty) ->
         LengthOfBlockOp (subop, subst_type subst ty)

      (*
       * Type coercions.
       *)
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         DTupleOfDTupleOp (ty_var, subst_mutable_types subst ty_fields)
    | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
         UnionOfUnionOp (ty_var, subst_types subst tyl, s1, s2)
    | RawDataOfFrameOp (ty_var, tyl) ->
         RawDataOfFrameOp (ty_var, subst_types subst tyl)

(*
 * Map a function over all the types in the binop.
 *)
let subst_type_binop subst op =
   match op with
      (* Bitwise operations on enumerations. *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

      (* Standard binary operations on NAML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on NAML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (*
       * Standard binary operations on native ints.  The precision is
       * the result precision; the inputs should match this precision.
       *)
    | PlusRawIntOp   _
    | MinusRawIntOp  _
    | MulRawIntOp    _
    | DivRawIntOp    _
    | RemRawIntOp    _
    | SlRawIntOp     _
    | SrRawIntOp     _
    | AndRawIntOp    _
    | OrRawIntOp     _
    | XorRawIntOp    _
    | MaxRawIntOp    _
    | MinRawIntOp    _

      (*
       * RawSetBitFieldOp (pre, signed, _
       *    See comments for RawBitFieldOp. This modifies the bitfield starting
       *    at bit <_
       *       First atom is the integer containing the field.
       *       Second atom is the value to be set in the field.
       *    The resulting integer contains the revised field, with type
       *    ACRawInt (pre, signed)
       *)
    | RawSetBitFieldOp _

      (* Comparisons on native ints *)
    | EqRawIntOp     _
    | NeqRawIntOp    _
    | LtRawIntOp     _
    | LeRawIntOp     _
    | GtRawIntOp     _
    | GeRawIntOp     _
    | CmpRawIntOp    _

      (* Standard binary operations on floats *)
    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

      (* Comparisons on floats *)
    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

      (*
       * Arctangent.  This computes arctan(y/x), where y is the first atom
       * and x is the second atom given.  Handles case when x = 0 correctly.
       *)
    | ATan2FloatOp _

      (*
       * Power.  This computes x^y.
       *)
    | PowerFloatOp _

      (*
       * Float hacking.
       * This sets the exponent field _
       *)
    | LdExpFloatIntOp _

      (*
       * Pointer arithmetic. The pointer in the first argument, and the
       * returned pointer should be infix pointers (which keep the base
       * pointer as well as a pointer to anywhere within the block).
       *)
    | PlusPointerOp _ ->
         op


      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty ->
         EqEqOp (subst_type subst ty)
    | NeqEqOp ty ->
         NeqEqOp (subst_type subst ty)

(*
 * Map a function over all types in the atom.
 *)
let rec subst_type_atom subst a =
   match a with
      AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomVar _
    | AtomFun _
    | AtomLabel _
    | AtomSizeof _
    | AtomTyUnpack _ ->
         a
    | AtomNil ty ->
         AtomNil (subst_type subst ty)
    | AtomConst (ty, ty_v, i) ->
         AtomConst (subst_type subst ty, ty_v, i)
    | AtomTyApply (v, ty, tyl) ->
         AtomTyApply (v, subst_type subst ty, List.map (subst_type subst) tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, subst_type subst ty, List.map (subst_type subst) tyl)
    | AtomUnop (op, a) ->
         AtomUnop (subst_type_unop subst op, subst_type_atom subst a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (subst_type_binop subst op, subst_type_atom subst a1, subst_type_atom subst a2)

let subst_type_atom_opt subst a =
   match a with
      Some a ->
         Some (subst_type_atom subst a)
    | None ->
         None

let subst_type_atoms subst args =
   List.map (subst_type_atom subst) args

let subst_type_atom_opt_list subst args =
   List.map (subst_type_atom_opt subst) args

(*
 * Map a type over all the types in the alloc_op.
 *)
let subst_type_alloc_op subst op =
   match op with
      AllocTuple (tclass, ty_vars, ty, args) ->
         let subst = subst_remove_type_vars subst ty_vars in
            AllocTuple (tclass, ty_vars, subst_type subst ty, subst_type_atoms subst args)
    | AllocDTuple (ty, ty_var, a, args) ->
         AllocDTuple (subst_type subst ty, ty_var, subst_type_atom subst a, subst_type_atoms subst args)
    | AllocUnion (ty_vars, ty, ty_v, i, args) ->
         let subst = subst_remove_type_vars subst ty_vars in
            AllocUnion (ty_vars, subst_type subst ty, ty_v, i, subst_type_atoms subst args)
    | AllocArray (ty, args) ->
         AllocArray (subst_type subst ty, subst_type_atoms subst args)
    | AllocVArray (ty, sub_index, a1, a2) ->
         AllocVArray (subst_type subst ty, sub_index, subst_type_atom subst a1, subst_type_atom subst a2)
    | AllocMalloc (ty, a) ->
         AllocMalloc (subst_type subst ty, subst_type_atom subst a)
    | AllocFrame (v, tyl) ->
         AllocFrame (v, List.map (subst_type subst) tyl)

(*
 * Map a type over the types in the debug_info.
 *)
let subst_type_debug_var subst (v1, ty, v2) =
   v1, subst_type subst ty, v2

let subst_type_debug_vars subst vars =
   List.map (subst_type_debug_var subst) vars

let subst_type_debug_info subst info =
   match info with
      DebugString _ ->
         info
    | DebugContext (line, vars) ->
         DebugContext (line, subst_type_debug_vars subst vars)

(*
 * Predicate substitition.
 *)
let subst_type_pred subst pred =
   match pred with
      IsMutable a ->
         IsMutable (subst_type_atom subst a)
    | Reserve (a1, a2) ->
         Reserve (subst_type_atom subst a1, subst_type_atom subst a2)
    | BoundsCheck (op, a1, a2, a3) ->
         BoundsCheck (op, subst_type_atom subst a1, subst_type_atom subst a2, subst_type_atom subst a3)
    | ElementCheck (ty, op, a1, a2) ->
         ElementCheck (subst_type subst ty, op, subst_type_atom subst a1, subst_type_atom subst a2)

(*
 * Map a function over all types.
 *)
let rec subst_type_exp subst e =
   let loc = loc_of_exp e in
   let e = subst_type_exp_core subst (dest_exp_core e) in
      make_exp loc e

and subst_type_exp_core subst e =
   match e with
      LetAtom (v, ty, a, e) ->
         let ty = subst_type subst ty in
         let a = subst_type_atom subst a in
         let e = subst_type_exp subst e in
            LetAtom (v, ty, a, e)
    | LetExt (v, ty, s, b, ty2, ty_args, al, e) ->
         let ty = subst_type subst ty in
         let ty2 = subst_type subst ty2 in
         let ty_args = List.map (subst_type subst) ty_args in
         let al = subst_type_atoms subst al in
         let e = subst_type_exp subst e in
            LetExt (v, ty, s, b, ty2, ty_args, al, e)
    | TailCall (label, v, args) ->
         let v = subst_type_atom subst v in
         let args = subst_type_atoms subst args in
            TailCall (label, v, args)
    | SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, f, args)) ->
         let loc_ptr = subst_type_atom subst loc_ptr in
         let loc_off = subst_type_atom subst loc_off in
         let f = subst_type_atom subst f in
         let args = subst_type_atoms subst args in
            SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, f, args))
    | SpecialCall (label, TailAtomic (f, c, args)) ->
         let c = subst_type_atom subst c in
         let f = subst_type_atom subst f in
         let args = subst_type_atoms subst args in
            SpecialCall (label, TailAtomic (f, c, args))
    | SpecialCall (label, TailAtomicRollback (level, c)) ->
         let level = subst_type_atom subst level in
         let c = subst_type_atom subst c in
            SpecialCall (label, TailAtomicRollback (level, c))
    | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
         let level = subst_type_atom subst level in
         let f = subst_type_atom subst f in
         let args = subst_type_atoms subst args in
            SpecialCall (label, TailAtomicCommit (level, f, args))
    | Match (a, cases) ->
         let a = subst_type_atom subst a in
         let cases =
            List.map (fun (label, set, e) -> label, set, subst_type_exp subst e) cases
         in
            Match (a, cases)
    | MatchDTuple (a, cases) ->
         let a = subst_type_atom subst a in
         let cases =
            List.map (fun (label, set, e) -> label, set, subst_type_exp subst e) cases
         in
            MatchDTuple (a, cases)
    | TypeCase (a1, a2, name, v, e1, e2) ->
         let a1 = subst_type_atom subst a1 in
         let a2 = subst_type_atom subst a2 in
         let e1 = subst_type_exp subst e1 in
         let e2 = subst_type_exp subst e2 in
            TypeCase (a1, a2, name, v, e1, e2)
    | LetAlloc (v, op, e) ->
         let op = subst_type_alloc_op subst op in
         let e = subst_type_exp subst e in
            LetAlloc (v, op, e)
    | LetSubscript (op, v1, ty, a2, a3, e) ->
         let ty = subst_type subst ty in
         let a2 = subst_type_atom subst a2 in
         let a3 = subst_type_atom subst a3 in
         let e = subst_type_exp subst e in
            LetSubscript (op, v1, ty, a2, a3, e)
    | SetSubscript (op, label, a1, a2, ty, a3, e) ->
         let ty = subst_type subst ty in
         let a1 = subst_type_atom subst a1 in
         let a2 = subst_type_atom subst a2 in
         let a3 = subst_type_atom subst a3 in
         let e = subst_type_exp subst e in
            SetSubscript (op, label, a1, a2, ty, a3, e)
    | LetGlobal (op, v, ty, l, e) ->
         let ty = subst_type subst ty in
         let e = subst_type_exp subst e in
            LetGlobal (op, v, ty, l, e)
    | SetGlobal (op, label, v, ty, a, e) ->
         let ty = subst_type subst ty in
         let a = subst_type_atom subst a in
         let e = subst_type_exp subst e in
            SetGlobal (op, label, v, ty, a, e)
    | Memcpy (op, label, a1, a2, a3, a4, a5, e) ->
         let a1 = subst_type_atom subst a1 in
         let a2 = subst_type_atom subst a2 in
         let a3 = subst_type_atom subst a3 in
         let a4 = subst_type_atom subst a4 in
         let a5 = subst_type_atom subst a5 in
         let e = subst_type_exp subst e in
            Memcpy (op, label, a1, a2, a3, a4, a5, e)
    | Assert (label, pred, e) ->
         Assert (label, subst_type_pred subst pred, subst_type_exp subst e)
    | Call (label, v, args, e) ->
         Call (label, subst_type_atom subst v, subst_type_atom_opt_list subst args, subst_type_exp subst e)
    | Debug (info, e) ->
         let info = subst_type_debug_info subst info in
         let e = subst_type_exp subst e in
            Debug (info, e)

(*
 * Map a function over a type definition.
 *)
let subst_type_tydef subst tydef =
   match tydef with
      TyDefUnion (ty_vars, fields) ->
         let subst = subst_remove_type_vars subst ty_vars in
         let fields =
            List.map (List.map (fun (ty, b) -> subst_type subst ty, b)) fields
         in
            TyDefUnion (ty_vars, fields)
    | TyDefLambda (ty_vars, ty) ->
         let subst = subst_remove_type_vars subst ty_vars in
            TyDefLambda (ty_vars, subst_type subst ty)
    | TyDefDTuple _ ->
         tydef

(*
 * Map a function over a global initializer.
 *)
let subst_type_init subst init =
   match init with
      InitAtom a ->
         InitAtom (subst_type_atom subst a)
    | InitAlloc op ->
         InitAlloc (subst_type_alloc_op subst op)
    | InitTag (ty_var, fields) ->
         InitTag (ty_var, List.map (fun (ty, b) -> subst_type subst ty, b) fields)
    | InitRawData _
    | InitNames _ ->
         init

let subst_type_globals subst globals =
   SymbolTable.map (fun (ty, init) ->
         subst_type subst ty, subst_type_init subst init) globals

let subst_type_import subst import =
   SymbolTable.map (fun import ->
         { import with import_type = subst_type subst import.import_type }) import

let subst_type_export subst export =
   SymbolTable.map (fun export ->
         { export with export_type = subst_type subst export.export_type }) export

(*
 * Map a function over a function definition.
 *)
let subst_type_fundef subst (line, ty_vars, ty, vars, e) =
   line, ty_vars, subst_type subst ty, vars, subst_type_exp subst e

let subst_type_funs subst funs =
   SymbolTable.map (subst_type_fundef subst) funs

(*
 * Map a function over the type definitions.
 *)
let subst_type_types subst types =
   SymbolTable.map (subst_type subst) types

let subst_type_tydefs subst types =
   SymbolTable.map (subst_type_tydef subst) types

(*
 * Map a function over the type definitions in the frame.
 *)
let subst_type_frames subst frames =
   SymbolTable.map (fun (vars, frame) ->
         let subst = subst_remove_type_vars subst vars in
         let frame =
            SymbolTable.map (fun vars ->
                  List.map (fun (v, ty, i) -> v, subst_type subst ty, i) vars) frame
         in
            vars, frame) frames

(*
 * Map a function over all types in the program.
 *)
let subst_type_prog subst
    { prog_file = file;
      prog_import = import;
      prog_export = export;
      prog_types = types;
      prog_frames = frames;
      prog_names = names;
      prog_globals = globals;
      prog_funs = funs
    } =
   { prog_file = file;
     prog_import = subst_type_import subst import;
     prog_export = subst_type_export subst export;
     prog_types = subst_type_tydefs subst types;
     prog_frames = subst_type_frames subst frames;
     prog_names = subst_type_types subst names;
     prog_globals = subst_type_globals subst globals;
     prog_funs = subst_type_funs subst funs
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
