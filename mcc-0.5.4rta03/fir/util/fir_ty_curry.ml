(*
 * Type currying: eliminate existentials in function parameters
 * in favor of polymorphic functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_util

(*
 * For type currying, do not eliminate existentials in positive positions.
 *)
let rec curry_type ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyRawData
    | TyPointer _
    | TyVar _
    | TyProject _
    | TyDelayed ->
         ty

    | TyFun (ty_vars, ty_res) ->
         let vars, ty_vars = curry_exists_type_list [] ty_vars in
         let ty_res = curry_type ty_res in
            TyAll (vars, TyFun (ty_vars, ty_res))

    | TyUnion (tv, tyl, i) ->
         let tyl = curry_type_list tyl in
            TyUnion (tv, tyl, i)

    | TyTuple (tc, tyl) ->
         let tyl = curry_mutable_type_list tyl in
            TyTuple (tc, tyl)

    | TyDTuple (ty_var, tyl) ->
         let tyl = curry_mutable_type_list_opt tyl in
            TyDTuple (ty_var, tyl)

    | TyTag (ty_var, tyl) ->
         let tyl = curry_mutable_type_list tyl in
            TyTag (ty_var, tyl)

    | TyArray ty ->
         let ty = curry_type ty in
            TyArray ty

    | TyFrame (v, tyl) ->
         let tyl = curry_type_list tyl in
            TyFrame (v, tyl)

    | TyApply (v, tyl) ->
         let tyl = curry_type_list tyl in
            TyApply (v, tyl)

    | TyExists (vars, ty) ->
         let ty = curry_type ty in
            TyExists (vars, ty)

    | TyAll (vars, ty) ->
         (match curry_type ty with
             TyAll (vars', ty) ->
                TyAll (vars @ vars', ty)
           | ty ->
                TyAll (vars, ty))

    | TyCase ty ->
         let ty = curry_type ty in
            TyCase ty

    | TyObject (v, ty) ->
         let ty = curry_type ty in
            TyObject (v, ty)

and curry_type_list tyl =
   List.map curry_type tyl

and curry_mutable_type (ty, b) =
   curry_type ty, b

and curry_mutable_type_list tyl =
   List.map curry_mutable_type tyl

and curry_mutable_type_list_opt tyl =
   match tyl with
      Some tyl -> Some (curry_mutable_type_list tyl)
    | None -> None

(*
 * Eliminate outermost existentials.
 *)
and curry_exists_type vars ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyRawData
    | TyPointer _
    | TyVar _
    | TyProject _
    | TyDelayed ->
         vars, ty

    | TyFun (ty_vars, ty_res) ->
         let vars', ty_vars = curry_exists_type_list [] ty_vars in
         let ty_res = curry_type ty_res in
         let ty = TyAll (vars', TyFun (ty_vars, ty_res)) in
            vars, ty

    | TyUnion (tv, tyl, i) ->
         let vars, tyl = curry_exists_type_list vars tyl in
         let ty = TyUnion (tv, tyl, i) in
            vars, ty

    | TyTuple (tc, tyl) ->
         let vars, tyl = curry_exists_mutable_type_list vars tyl in
         let ty = TyTuple (tc, tyl) in
            vars, ty

    | TyDTuple (ty_var, tyl) ->
         let vars, tyl = curry_exists_mutable_type_list_opt vars tyl in
         let ty = TyDTuple (ty_var, tyl) in
            vars, ty

    | TyTag (ty_var, tyl) ->
         let vars, tyl = curry_exists_mutable_type_list vars tyl in
         let ty = TyTag (ty_var, tyl) in
            vars, ty

    | TyArray ty ->
         let vars, ty = curry_exists_type vars ty in
         let ty = TyArray ty in
            vars, ty

    | TyFrame (v, tyl) ->
         let vars, tyl = curry_exists_type_list vars tyl in
         let ty = TyFrame (v, tyl) in
            vars, ty

    | TyApply (v, tyl) ->
         let vars, tyl = curry_exists_type_list vars tyl in
         let ty = TyApply (v, tyl) in
            vars, ty

    | TyExists (vars', ty) ->
         curry_exists_type (vars' @ vars) ty

    | TyAll (vars', ty) ->
         let vars, ty = curry_exists_type vars ty in
         let ty = TyAll (vars', ty) in
            vars, ty

    | TyCase ty ->
         let vars, ty = curry_exists_type vars ty in
         let ty = TyCase ty in
            vars, ty

    | TyObject (v, ty) ->
         let vars, ty = curry_exists_type vars ty in
         let ty = TyObject (v, ty) in
            vars, ty

and curry_exists_type_list vars tyl =
   let vars, tyl =
      List.fold_left (fun (vars, tyl) ty ->
            let vars, ty = curry_exists_type vars ty in
               vars, ty :: tyl) (vars, []) tyl
   in
      vars, List.rev tyl

and curry_exists_mutable_type_list vars fields =
   let vars, fields =
      List.fold_left (fun (vars, fields) (ty, b) ->
            let vars, ty = curry_exists_type vars ty in
               vars, (ty, b) :: fields) (vars, []) fields
   in
      vars, List.rev fields

and curry_exists_mutable_type_list_opt vars fields_opt =
   match fields_opt with
      Some fields ->
         let vars, fields = curry_exists_mutable_type_list vars fields in
            vars, Some fields
    | None ->
         vars, None

(*
 * Curry all the types in the program.
 *)
let ty_curry_prog prog =
   map_type_prog curry_type prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
