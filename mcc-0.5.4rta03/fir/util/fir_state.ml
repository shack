(*
 * Globals state variables.
 * Currently, this includes only debugging flags.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Flags
open Location

(*
 * Directories in the search path.
 * This is kept in reverse order by default.
 *)
let rev_include = ref ["."]


(***  Optimization Flags  ***)


(*
 * Set and check the default optimization level.
 *)
let opt_set_level level = std_flags_set_int "opt.core.level" level
let opt_get_level ()    = std_flags_get_int "opt.core.level"


(*
 * Set and check if assembly optimizations are enabled.
 *)
let opt_set_asm value   = std_flags_set_bool "opt.core.asm" value
let opt_get_asm ()      = std_flags_get_bool "opt.core.asm"


(*
 * Set and check if MIR optimizations are enabled.
 *)
let opt_set_mir value   = std_flags_set_bool "opt.core.mir" value
let opt_get_mir ()      = std_flags_get_bool "opt.core.mir"


(*
 * Set and check if FIR optimizations are enabled.
 *)
let opt_set_fir value   = std_flags_set_bool "opt.core.fir" value
let opt_get_fir ()      = std_flags_get_bool "opt.core.fir"


(***  Higher Level Optimize Interface  ***)


let optimize flag                   = std_flags_get_bool flag
let optimize_level enable level     = std_flags_get_bool enable && opt_get_level () >= level
let optimize_fir_level enable level = opt_get_fir () && optimize_level enable level
let optimize_mir_level enable level = opt_get_mir () && optimize_level enable level
let optimize_asm_level enable level = opt_get_asm () && optimize_level enable level


(*
 * Register the standard names
 *)
let () = std_flags_help_section_text "state"
   "Flags in the standard compiler state."

let () = std_flags_register_list_help "state"
  ["opt.core.level", FlagInt 1,
                     "Set the default optimization level for all stages of the compiler";
   "opt.core.fir",   FlagBool true,
                     "Enable or disable all FIR optimizations";
   "opt.core.mir",   FlagBool true,
                     "Enable or disable all MIR optimizations";
   "opt.core.asm",   FlagBool true,
                     "Enable or disable all assembly (backend) optimizations"]


(***  Yet More Flags!  ***)


(*
 * Default grammar and options.
 *)
let grammar_filename = ref ""
let compiled_grammar_filename = ref ""
let debug_grammar = ref false
let save_grammar = ref false
let use_fc_ast = ref false
let apply_no_rewrites = ref false
let phobos_paths = ref [""]

(*
 * Default tabstop.
 *)
let tabstop = 3


(*
 * Phobos debugging.
 *)
let debug_phobos =
   create_debug (**)
      { debug_name = "phobos";
        debug_description = "print debugging information for Phobos";
        debug_value = false
      }


(*
 * Debug FIR loop optimizations.
 *)
let debug_loop =
   create_debug (**)
      { debug_name = "loop";
        debug_description = "display loop-nest information (very verbose)";
        debug_value = false
      }

let debug_alias =
   create_debug (**)
      { debug_name = "alias";
        debug_description = "display alias analysis debugging information";
        debug_value = false
      }

let debug_dead =
   create_debug (**)
      { debug_name = "dead";
        debug_description = "display dead-code elimination debugging information";
        debug_value = false
      }

(*
 * Debug FIR partial redundancy elimination.
 *)
let debug_pre =
   create_debug (**)
      { debug_name = "pre";
        debug_description = "display partial-redundancy-elimination debugging information";
        debug_value = false
      }


(*
 * Debug FIR closure conversion.
 *)
let debug_fir_ty_closure = ref false
let debug_fir_closure = ref false


(*
 * Print position information on exceptions.
 *)
let debug_pos = Position.debug_pos
let debug_trace_pos = Position.trace_pos


(*
 * Turn on debugging info.
 * This will ask the front-end to generate debugging info,
 * and the back-end should not reorder assembly blocks.
 *)
let debug_stab = ref false


(*
 * Enable profiling of compile
 *)
let debug_profile = ref false
let profile msg f arg =
   if !debug_profile then
      Profile.profile msg f arg
   else
      f arg


(*
 * FIR type inference.
 * Debug unification code.
 *)
let debug_unify = ref false


(*
 * Debug type inference.
 *)
let debug_infer = ref false


(*
 * Append bootstrap code to Pasqual
 *)
let pasqual_add_boot = ref false


(*
 * Print intermediate code.
 *)
let debug_print_parse   =
   create_debug (**)
      { debug_name = "print_part";
        debug_description = "print the program's parsed abstract syntax tree";
        debug_value = false
      }

let debug_print_ast     =
   create_debug (**)
      { debug_name = "print_ast";
        debug_description = "print the program's abstract syntax tree";
        debug_value = false
      }

let debug_print_air    =
   create_debug (**)
      { debug_name = "print_air";
        debug_description = "print the program's abstract intermediate representation";
        debug_value = false
      }

let debug_print_tast    =
   create_debug (**)
      { debug_name = "print_tast";
        debug_description = "print the program's typed abstract syntax tree";
        debug_value = false
      }

let debug_print_ir    =
   create_debug (**)
      { debug_name = "print_ir";
        debug_description = "print the program's intermediate representation";
        debug_value = false
      }

let debug_print_fir     = ref 0

let debug_print_asm   =
   create_debug (**)
      { debug_name = "print_asm";
        debug_description = "print the program's initial assembly";
        debug_value = false
      }

let debug_print_no_poly = ref false


(*
 * Typecheck the intermediate code (for front-ends
 * that support it).
 *)
let debug_check_ir = ref false
let debug_check_fir = ref false


(*
 * Evaluate during stages.
 *)
let debug_eval_ir = ref false


(*
 * Parametres for controlling safety checking:
 *
 *    enable_mir_safety:  Enable generation of the old-school
 *       safety checks in the MIR.  Bounds checks are generated,
 *       and index<->pointer operations from unsafe data structs
 *       will get safety checks.
 *
 *    enable_fir_safety:  Enable generation and use of the new
 *       FIR safety checks, using the Assert statement.  The
 *       MIR will accept Assert and translate it to appropriate
 *       statements.
 *
 * At least one of these values must be true to ensure safe
 * code generation!  In addition, the following safety flags
 * are defined:
 *
 *    strict_poly:  enforce explicit coercions for polymorphic
 *       values, even when the coercion occurs in a function
 *       pointer (which requires a new stub function).  Must be
 *       true for safe code.
 *
 *    safe_functions:  Use arity tags to ensure function pointers
 *       are used safely.  Must be true for safe code.
 *)
let fir_safety_name = "fir.safety"
let mir_safety_name = "mir.safety"
let mir_strict_poly_name = "mir.strict_poly"
let mir_safe_functions_name = "mir.safe_functions"

let () = std_flags_register_list_help "state"
  [fir_safety_name,           FlagBool false,
                              "Generate runtime safety checks in the FIR." ^
                              " For safe execution, this or " ^ mir_safety_name ^ " must be enabled.";
   mir_safety_name,           FlagBool true,
                              "Generate runtime safety checks in the MIR." ^
                              " For safe execution, this or " ^ fir_safety_name ^ " must be enabled.";
   mir_strict_poly_name,      FlagBool true,
                              "Enforce explicit coercions for polymorphic values, even when the" ^
                              " coercion occurs in a function pointer (requires stub function generation)." ^
                              " This must be true for safe execution.";
   mir_safe_functions_name,   FlagBool true,
                              "Use arity tags in the backend to ensure function pointers are used" ^
                              " safely.  This must be true for safe execution."]

let fir_safety_enabled () =
   std_flags_get_bool fir_safety_name

let mir_safety_enabled () =
   std_flags_get_bool mir_safety_name

let safety_enabled () =
   fir_safety_enabled () || mir_safety_enabled ()

let strict_poly () =
   std_flags_get_bool mir_strict_poly_name

let safe_functions () =
   std_flags_get_bool mir_safe_functions_name


(*
 * Hint for exception printing; how much of the expression should we
 * print before we bail and print ellipses, in exception output?
 *)
let exn_expr_size_name = "exn.expr_size"

let () = std_flags_register_list_help "state"
  [exn_expr_size_name,        FlagInt 5,
                              "Set the number of expression lines that should be printed" ^
                              " by the exception handler when an exception occurs.  This" ^
                              " flag is processed by FIR and MIR exception printers."]

let exn_expr_size () =
   std_flags_get_int exn_expr_size_name

(*
 * Print a warning.
 *)
let debug_warning_is_error =
   create_debug (**)
      { debug_name = "warn-error";
        debug_description = "terminate on warnings";
        debug_value = false
      }

let string_warning loc s =
   eprintf "@[<hv 3>%a@ Warning: %s@]@." pp_print_location loc s;
   if !debug_warning_is_error then
      exit 3