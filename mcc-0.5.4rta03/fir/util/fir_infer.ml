(*
 * Type inference.  This pass eliminates all TyDelayed.
 * This algorithm is not guaranteed to catch errors if the
 * program is ill-formed.  For that, use Fir_check.check_prog
 * _after_ you run type inference.
 *
 * Here is how it works.
 *
 *   1. Replace all TyDelayed with (TyVar v) where v is new.
 *
 *   2. Perform type unification.
 *
 *   3. It is not possible to figure out the types
 *      for subscript objects during unification (a LetSubscript
 *      might be applied to a Aggr, Tuple, Union, or Array).
 *      In the third pass, unify against the exact type.
 *
 *   4. The first three phases ignore type quantification,
 *      so introduce polymorphic coercions in this phase.
 *
 *   4. Type inference is now complete, so perform the
 *      substitution for all the vars introduced in the
 *      first step.
 *
 * Note: the unify_types function is _not_ symmetric.  It allows
 * the first type to be a "subtype" of the second.  A TyExists (vars, ty1)
 * is a subtype of a ty2 if ty1 can be unified with ty2.  A ty1 is
 * a subtype of a TyAll (vars, ty2) if ty1 can be unified with ty2.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Interval_set

open Fir
open Fir_ds
open Fir_set
open Fir_exn
open Fir_env
open Fir_pos
open Fir_util
open Fir_type
open Fir_subst
open Fir_state
open Fir_print
open Fir_ty_closure
open Fir_standardize

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_infer" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Generalized constant subscripts.
 *)
type subscript =
   IntSubscript of int
 | LabelSubscript of frame_label * Rawint.rawint
 | NoSubscript

(************************************************************************
 * BOUND VARS
 ************************************************************************)

type bvars = SymbolSet.t

let bvars_empty = SymbolSet.empty

let bvars_mem = SymbolSet.mem

let bvars_add_var = SymbolSet.add

let bvars_add_vars bvars vars =
   List.fold_left bvars_add_var bvars vars

let bvars_to_set bvars =
   bvars

(*
 * Identifier equality.
 *)
let rec eq_mem eq v1 v2 =
   match eq with
      (v1', v2') :: eq ->
         Symbol.eq v1 v1' && Symbol.eq v2 v2'
         || Symbol.eq v1 v2' && Symbol.eq v2 v1'
         || eq_mem eq v1 v2
    | [] ->
         false

let eq_add eq v1 v2 =
   (v1, v2) :: eq

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Convert to a word index.
 *)
let word_index index i =
   match index with
      ByteIndex ->
         i / sizeof_pointer
    | WordIndex ->
         i

(*
 * Union with an IntSet opt.
 *)
let int_set_opt_union pos s1 s2 =
   match s1 with
      Some (IntSet s1) ->
         Some (IntSet (IntSet.union s1 s2))
    | Some (RawIntSet s1) ->
         let pre = RawIntSet.precision s1 in
         let signed = RawIntSet.signed s1 in
            raise (FirException (pos, TypeError (TyInt, TyRawInt (pre, signed))))
    | None ->
         Some (IntSet s2)

let rawint_set_opt_union pos s1 s2 =
   match s1 with
      Some (RawIntSet s1) ->
         Some (RawIntSet (RawIntSet.union s1 s2))
    | Some (IntSet s1) ->
         let pre = RawIntSet.precision s2 in
         let signed = RawIntSet.signed s2 in
            raise (FirException (pos, TypeError (TyRawInt (pre, signed), TyInt)))
    | None ->
         Some (RawIntSet s2)

(*
 * Check additions to the substitution to make sure
 * we don't try to add polymorphic variables.
 *)
let subst_add_type_var bvars subst pos v ty =
   if bvars_mem bvars v then
      raise (FirException (string_pos "subst_add_type_var" pos, StringVarError ("trying to unify a fixed type variable", v)));
   subst_add_type_var subst v ty

(*
 * Expand the substitution just enough to expose the outer type.
 *)
let rec subst_expand_vars vars genv (subst : subst) pos ty =
   let pos = string_pos "subst_expand_vars" pos in
   let ty = expand_type genv pos ty in
      match ty with
         TyVar v ->
            if List.mem v vars then
               raise (FirException (pos, StringVarError ("recursive substitution", v)));
            (try
                subst_expand_vars (v :: vars) genv subst pos (subst_lookup_type_var subst v)
             with
                Not_found ->
                   ty)
       | _ ->
            ty

let subst_expand genv (subst : subst) pos ty =
   subst_expand_vars [] genv subst pos ty

(*
 * Get the union fields.
 * Make sure the rename the type variables.
 *)
let dest_union_fields genv pos ty_v i =
   let pos = string_pos "dest_union_fields" pos in
   let tydef = genv_lookup_type genv pos ty_v in
   let tydef = standardize_tydef tydef in
      match tydef with
         TyDefUnion (ty_vars, fields) ->
            let len = List.length fields in
               if i < 0 || i >= len then
                  raise (FirException (pos, ArityMismatch (len, i)));
               ty_vars, List.nth fields i
       | TyDefLambda _
       | TyDefDTuple _ ->
            raise (FirException (pos, NotAUnionDef tydef))

(*
 * Expand the union.
 *)
let apply_union_fields genv pos ty_v ty_args i =
   let pos = string_pos "apply_union_fields" pos in
   let fields = apply_union genv pos ty_v ty_args in
   let len = List.length fields in
      if i < 0 || i >= len then
         raise (FirException (pos, ArityMismatch (len, i)));
      List.nth fields i

(*
 * Get the number of fields in the union.
 *)
let dest_union_tydef_count genv pos ty_v =
   let pos = string_pos "dest_union_tydef_count" pos in
      match genv_lookup_type genv pos ty_v with
         TyDefUnion (_, fields) ->
            Some (List.length fields)
       | TyDefLambda _
       | TyDefDTuple _ ->
            None

let rec dest_union_ty_count genv subst pos ty =
   let pos = string_pos "dest_union_ty_count" pos in
      match expand_type genv pos ty with
         TyUnion (ty_v, _, _) ->
            dest_union_tydef_count genv pos ty_v
       | TyVar v ->
            (try dest_union_ty_count genv subst pos (subst_lookup_type_var subst v) with
                Not_found ->
                   None)
       | TyAll (_, ty)
       | TyExists (_, ty) ->
            dest_union_ty_count genv subst pos ty
       | _ ->
            None

(*
 * Specialize the counter.
 *)
let specialize_union_count genv pos ty_v set =
   let pos = string_pos "specialize_union_count" pos in
      match genv_lookup_type genv pos ty_v with
         TyDefUnion (_, [_]) ->
            0
       | tydef ->
            if IntSet.is_singleton set then
               IntSet.dest_singleton set
            else if IntSet.is_empty set then
               raise (FirException (pos, StringTydefError ("union has no cases", tydef)))
            else
               raise (FirException (pos, StringTydefError ("union has multiple cases: " ^ (string_of_int_set set), tydef)))

(*
 * Check if a type is a RawData type.
 *)
let is_rawdata_type genv subst pos ty =
   let pos = string_pos "is_rawdata_type" pos in
      match subst_expand genv subst pos ty with
         TyRawData ->
            true
       | _ ->
            false

(*
 * Some destructors.
 *)
let dest_tuple_type genv subst pos ty =
   let pos = string_pos "dest_tuple_type" pos in
      dest_tuple_type genv pos (subst_expand genv subst pos ty)

let dest_dtuple_type genv subst pos ty =
   let pos = string_pos "dest_dtuple_type" pos in
      dest_dtuple_type genv pos (subst_expand genv subst pos ty)

let dest_tag_type genv subst pos ty =
   let pos = string_pos "dest_tag_type" pos in
      dest_tag_type genv pos (subst_expand genv subst pos ty)

let dest_union_type genv subst pos ty =
   let pos = string_pos "dest_union_type" pos in
      dest_union_type genv pos (subst_expand genv subst pos ty)

let dest_array_type genv subst pos ty =
   let pos = string_pos "dest_array_type" pos in
      dest_array_type genv pos (subst_expand genv subst pos ty)

let dest_all_type_vars genv subst pos ty ty_vars =
   let pos = string_pos "dest_all_type_vars" pos in
   let ty_vars', ty = dest_all_type genv pos (subst_expand genv subst pos ty) in
   let len1 = List.length ty_vars' in
   let len2 = List.length ty_vars in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let subst' =
      List.fold_left2 (fun subst v1 v2 ->
            Fir_subst.subst_add_type_var subst v1 (TyVar v2)) subst_empty ty_vars' ty_vars
   in
      subst_type subst' ty

(************************************************************************
 * TYPE UTILITIES
 ************************************************************************)

(*
 * Rename the type vars in the type to new vars.
 *)
let rename_type_vars_any ty vars =
   let subst =
      List.fold_left (fun subst v ->
            Fir_subst.subst_add_type_var subst v (TyVar (new_symbol v))) subst_empty vars
   in
      subst_type subst ty

(*
 * Eliminate existential type at the outermost
 * level.
 *)
let exists_elim_aux1 genv pos v vars ty =
   let subst, _ =
      List.fold_left (fun (subst, i) v' ->
            let subst = Fir_subst.subst_add_type_var subst v' (TyProject (v, i)) in
               subst, succ i) (subst_empty, 0) vars
   in
      subst_type subst ty

let exists_elim_aux2 genv subst pos v ty =
   let pos = string_pos "exists_elim_aux2" pos in
      match subst_expand genv subst pos ty with
         TyExists (vars, ty) ->
            exists_elim_aux1 genv pos v vars ty
       | _ ->
            ty

let rec exists_elim_var genv subst pos v ty =
   let pos = string_pos "exists_elim_var" pos in
      match subst_expand genv subst pos ty with
         TyExists (vars, ty) ->
            exists_elim_aux1 genv pos v vars ty
       | _ ->
            ty

let exists_elim genv subst pos a ty =
   match a with
      AtomVar v ->
         exists_elim_var genv subst pos v ty
    | _ ->
         ty

(*
 * Strip the outermost TyExists, introducing a random var.
 *)
let exists_elim_any genv bvars subst pos ty =
   exists_elim_var genv subst pos (new_symbol_string "exists_elim_any") ty

(************************************************************************
 * TyDelayed ELIMINATION
 ************************************************************************)

(*
 * Eliminate MutableDelayed
 *)
let new_mutable_flag b =
   match b with
      Mutable
    | Immutable
    | MutableVar _ ->
         b
    | MutableDelayed ->
         MutableVar (new_symbol_string "mutablep")

(*
 * Eliminate all TyDelayed.
 *)
let rec new_type ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyVar _
    | TyProject _
    | TyRawData
    | TyPointer _ ->
         ty

    | TyFrame (v, tyl) ->
         let tyl = List.map new_type tyl in
            TyFrame (v, tyl)

    | TyFun (ty_vars, ty_res) ->
         let ty_vars = List.map new_type ty_vars in
         let ty_res = new_type ty_res in
            TyFun (ty_vars, ty_res)

    | TyUnion (v, tyl, i) ->
         let tyl = List.map new_type tyl in
            TyUnion (v, tyl, i)

    | TyTuple (tclass, tyl) ->
         let tyl = List.map new_mutable_type tyl in
            TyTuple (tclass, tyl)

    | TyDTuple (ty_var, tyl_opt) ->
         let tyl_opt =
            match tyl_opt with
               Some tyl -> Some (List.map new_mutable_type tyl)
             | None -> None
         in
            TyDTuple (ty_var, tyl_opt)

    | TyTag (ty_var, tyl) ->
         let tyl = List.map new_mutable_type tyl in
            TyTag (ty_var, tyl)

    | TyArray ty ->
         let ty = new_type ty in
            TyArray ty

    | TyCase ty ->
         let ty = new_type ty in
            TyCase ty

    | TyExists (vars, ty) ->
         let ty = new_type ty in
            TyExists (vars, ty)

    | TyAll (vars, ty) ->
         let ty = new_type ty in
            TyAll (vars, ty)

    | TyApply (v, tyl) ->
         let tyl = List.map new_type tyl in
            TyApply (v, tyl)

    | TyObject (v, ty) ->
         TyObject (v, new_type ty)

    | TyDelayed ->
         TyVar (new_symbol_string "delayed")

and new_mutable_type (ty, b) =
   let ty = new_type ty in
   let b = new_mutable_flag b in
      ty, b

let new_tydef tydef =
   match tydef with
      TyDefUnion (vars, fields) ->
         let fields =
            List.map (List.map new_mutable_type) fields
         in
            TyDefUnion (vars, fields)
    | TyDefLambda (vars, ty) ->
         let ty = new_type ty in
            TyDefLambda (vars, ty)
    | TyDefDTuple _ ->
         tydef

(************************************************************************
 * UNIFICATION/COERCION
 ************************************************************************)

(*
 * Unify two types.
 * Note: unification is asymmetric: the first type should
 * be a _subtype_ of the second.
 *    1. A (TyExists (vars, ty1)) is a subtype of ty2 if
 *       ty1 is a subtype of ty2
 *)
let unify_types strictp ty1' ty2' genv bvars subst pos bvars eq_ids ty1 ty2 =
   let pos = string_pos "unify_types" pos in

   (* Generic error handler *)
   let type_error pos ty1' ty2' =
      raise (FirException (pos, TypeError4 (subst_type subst ty1,
                                            subst_type subst ty2,
                                            subst_type subst ty1',
                                            subst_type subst ty2')))
   in

   (*
    * Rename the vars from one list into another.
    *)
   let rename_type_vars_same ty vars1 vars2 =
      let subst =
         List.fold_left2 (fun subst v1 v2 ->
               Fir_subst.subst_add_type_var subst v1 (TyVar v2)) subst_empty vars1 vars2
      in
         subst_type subst ty
   in

   (*
    * With error checking.
    *)
   let rename_type_vars_same_check ty1 ty2 pos ty vars1 vars2 =
      let pos = string_pos "rename_type_vars_check" pos in
      let len1 = List.length vars1 in
      let len2 = List.length vars2 in
      let _ =
         if len1 <> len2 then
            type_error pos ty1 ty2
      in
         rename_type_vars_same ty vars1 vars2
   in

   (* Unification *)
   let rec unify_types strictp subst pos bvars eq_ids ty1 ty2 =
      let pos =
         if !debug_pos then
            del_exp_pos (fun buf ->
                  fprintf buf "@[<hv 3>Fir_infer.unify_types:@ @[<hv 3>Type1:@ %a@]@ @[<hv 3>Type2:@ %a@]@]" (**)
                     pp_print_type ty1
                     pp_print_type ty2) pos
         else
            string_pos "unify_types" pos
      in
         match ty1, ty2 with
            (* Variables *)
            TyVar v1, TyVar v2 ->
               let pos = string_pos "var_var" pos in
                  if debug debug_unify then
                     Format.eprintf "unify vars: %a/%a@." (**)
                        pp_print_symbol v1
                        pp_print_symbol v2;
                  (if v1 = v2 then
                      subst
                   else
                      try
                         let ty1 = subst_lookup_type_var subst v1 in
                            unify_types strictp subst pos bvars eq_ids ty1 ty2
                      with
                         Not_found ->
                            try
                               let ty2 = subst_lookup_type_var subst v2 in
                                  unify_types strictp subst pos bvars eq_ids ty1 ty2
                            with
                               Not_found ->
                                  if bvars_mem bvars v1 then
                                     if bvars_mem bvars v2 then
                                        type_error pos ty1 ty2
                                     else
                                        subst_add_type_var bvars subst pos v2 ty1
                                  else
                                     subst_add_type_var bvars subst pos v1 ty2)
          | TyVar v1, _ ->
               let pos = string_pos "var_left" pos in
                  if debug debug_unify then
                     Format.printf "unify var1: %a@." pp_print_symbol v1;
                  if bvars_mem bvars v1 then
                     type_error pos ty1 ty2;
                  (try
                      let ty1 = subst_lookup_type_var subst v1 in
                         unify_types strictp subst pos bvars eq_ids ty1 ty2
                   with
                      Not_found ->
                         subst_add_type_var bvars subst pos v1 ty2)
          | _, TyVar v2 ->
               let pos = string_pos "var_right" pos in
                  if debug debug_unify then
                     Format.printf "unify var2: %a@." pp_print_symbol v2;
                  if bvars_mem bvars v2 then
                     type_error pos ty1 ty2;
                  (try
                      let ty2 = subst_lookup_type_var subst v2 in
                         unify_types strictp subst pos bvars eq_ids ty1 ty2
                   with
                      Not_found ->
                         subst_add_type_var bvars subst pos v2 ty1)

            (* Base types *)
          | TyInt, TyInt ->
               subst
          | TyFloat pre1, TyFloat pre2 ->
               let pos = string_pos "unify_float" pos in
                  if pre1 <> pre2 then
                     type_error pos ty1 ty2;
                  subst
          | TyEnum n1, TyEnum n2 ->
               let pos = string_pos "unify_enum" pos in
                  if n1 <> n2 then
                     type_error pos ty1 ty2;
                  subst

          | TyRawInt (pre1, signed1), TyRawInt (pre2, signed2) ->
               let pos = string_pos "unify_rawint" pos in
                  if pre1 <> pre2 || signed1 <> signed2 then
                     type_error pos ty1 ty2;
                  subst

            (* Functions: swap the vars to get subtyping right *)
          | TyFun (ty_vars1, ty_res1), TyFun (ty_vars2, ty_res2) ->
               let pos = string_pos "unify_fun" pos in
               let subst = unify_type_lists strictp ty1 ty2 subst pos bvars eq_ids ty_vars2 ty_vars1 in
                  unify_types strictp subst pos bvars eq_ids ty_res1 ty_res2

            (* Aggregate types *)
          | TyRawData, TyRawData ->
               subst
          | TyPointer op1, TyPointer op2 ->
               if op1 <> op2 then
                  type_error pos ty1 ty2;
               subst

          | TyUnion (v1, tyl1, i1), TyUnion (v2, tyl2, i2) ->
               let pos = string_pos "unify_union" pos in
                  if Symbol.eq v1 v2 || eq_mem eq_ids v1 v2 then
                     unify_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2
                  else
                     let eq_ids = eq_add eq_ids v1 v2 in
                        unify_mutable_type_lists_lists strictp ty1 ty2 subst pos bvars eq_ids (**)
                           (apply_union genv pos v1 tyl1)
                           (apply_union genv pos v2 tyl2)

          | TyTuple (tclass1, tyl1), TyTuple (tclass2, tyl2) ->
               let pos = string_pos "unify_tuple" pos in
                  if tclass1 <> tclass2 then
                     type_error pos ty1 ty2;
                  unify_short_mutable_type_lists ty1 ty2 subst pos bvars eq_ids tyl1 tyl2

          | TyDTuple (ty_var1, tyl_opt1), TyDTuple (ty_var2, tyl_opt2) ->
               let pos = string_pos "unify_dtuple" pos in
                  if not (Symbol.eq ty_var1 ty_var2) then
                     type_error pos ty1 ty2;
                  (match tyl_opt1, tyl_opt2 with
                      None, _ ->
                         subst
                    | Some tyl1, Some tyl2 ->
                         unify_mutable_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2
                    | Some _, None ->
                         type_error pos ty1 ty2)

          | TyTag (ty_var1, tyl1), TyTag (ty_var2, tyl2) ->
               let pos = string_pos "unify_tag" pos in
                  if not (Symbol.eq ty_var1 ty_var2) then
                     type_error pos ty1 ty2;
                  unify_mutable_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2

          | TyArray ty1, TyArray ty2 ->
               let pos = string_pos "unify_array" pos in
                  unify_types strictp subst pos bvars eq_ids ty1 ty2

            (* Polymorphic types *)
          | TyAll ([], ty1), ty2
          | TyExists ([], ty1), ty2
          | ty1, TyAll ([], ty2)
          | ty1, TyExists ([], ty2) ->
               let pos = string_pos "unify_trivial_poly" pos in
                  unify_types strictp subst pos bvars eq_ids ty1 ty2

            (*
             * We allow partial application on universal types.
             *)
          | TyAll (vars1, ty1''), TyAll (vars2, ty2'') ->
               let len1 = List.length vars1 in
               let len2 = List.length vars2 in
                  if len1 = len2 then
                     (* Rename vars in the second type so they match the first *)
                     let pos = string_pos "unify_all_same" pos in
                     let ty2'' = rename_type_vars_same ty2'' vars2 vars1 in
                     let bvars = bvars_add_vars bvars vars1 in
                        unify_types strictp subst pos bvars eq_ids ty1'' ty2''
                  else if not strictp && len2 > len1 then
                     (* Give new names to the extra type variables *)
                     let pos = string_pos "unify_all_too_many" pos in
                     let vars2, vars2' = Mc_list_util.split (len2 - len1) vars2 in
                     let ty2 = TyAll (vars2', rename_type_vars_any ty2'' vars2) in
                        unify_types strictp subst pos bvars eq_ids ty1 ty2
                  else
                     let pos = string_pos "unify_all_too_few" pos in
                        type_error pos ty1 ty2

            (* This is the universal subtyping case *)
          | _, TyAll (vars2, ty2) when not strictp ->
               let pos = string_pos "unify_all_subtype2" pos in
               let ty2 = rename_type_vars_any ty2 vars2 in
                  unify_types strictp subst pos bvars eq_ids ty1 ty2

          | TyExists (vars1, ty1'), TyExists (vars2, ty2') ->
               let pos = string_pos "unify_poly" pos in
               let ty2' = rename_type_vars_same_check ty1 ty2 pos ty2' vars2 vars1 in
                  unify_types strictp subst pos bvars eq_ids ty1' ty2'

            (* This is the existential subtyping case *)
          | TyExists (vars1, ty1), _ when not strictp ->
               let pos = string_pos "unify_exists_subtype" pos in
               let ty1 = rename_type_vars_any ty1 vars1 in
                  unify_types strictp subst pos bvars eq_ids ty1 ty2

            (* Projections have to use equal vars (as defined in nenv) *)
          | TyProject (v1, i1), TyProject (v2, i2) ->
               let pos = string_pos "unify_project" pos in
                  if not (genv_equal_norm genv v1 v2) || i1 <> i2 then
                     type_error pos ty1 ty2;
                  subst

            (* Object types *)
          | TyObject (v1, ty1), TyObject (v2, ty2) ->
               let ty2 = rename_type_vars_same ty2 [v2] [v1] in
               let bvars = bvars_add_var bvars v1 in
                  unify_types strictp subst pos bvars eq_ids ty1 ty2

          | TyObject (v1, ty1), _ ->
               let subst = subst_add_type_var bvars subst pos v1 ty2 in
                  unify_types strictp subst pos bvars eq_ids ty1 ty2

          | TyCase ty1, TyCase ty2 ->
               unify_types strictp subst pos bvars eq_ids ty1 ty2

            (* Pointer types *)
          | TyFrame (label1, tyl1), TyFrame (label2, tyl2) ->
               if not (Symbol.eq label1 label2) then
                  type_error pos ty1 ty2;
               unify_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2
          | TyRawData, TyFrame _ when not strictp ->
               subst

            (* Defined types *)
          | TyApply (v1, args1), TyApply (v2, args2) ->
               let pos = string_pos "unify_apply_apply" pos in
                  if Symbol.eq v1 v2 || eq_mem eq_ids v1 v2 then
                     unify_type_lists strictp ty1 ty2 subst pos bvars eq_ids args1 args2
                  else if List.length args1 = List.length args2 then
                     let eq_ids = eq_add eq_ids v1 v2 in
                        unify_types strictp subst pos bvars eq_ids (**)
                           (apply_type genv pos v1 args1)
                           (apply_type genv pos v2 args2)
                  else
                     unify_types strictp subst pos bvars eq_ids (**)
                        (apply_type genv pos v1 args1) ty2
          | TyApply (v1, args1), _ ->
               let pos = string_pos "unify_apply_any" pos in
                  unify_types strictp subst pos bvars eq_ids (**)
                     (apply_type genv pos v1 args1) ty2
          | _, TyApply (v2, args2) ->
               let pos = string_pos "unify_any_apply" pos in
                  unify_types strictp subst pos bvars eq_ids (**)
                     ty1 (apply_type genv pos v2 args2)

            (* All others fail *)
          | _ ->
               type_error pos ty1 ty2

   (*
    * Mutable unification.
    *)
   and unify_mutable t1 t2 subst pos b1 b2 =
      let pos = string_pos "unify_mutable" pos in
         match b1, b2 with
            MutableVar v, b
          | b, MutableVar v ->
               (try unify_mutable t1 t2 subst pos (subst_lookup_mutable_var subst v) b with
                   Not_found ->
                      subst_add_mutable_var subst v b)
          | _ ->
               if b1 <> b2 then
                  type_error pos t1 t2;
               subst

   (*
    * List unification.
    *)
   and unify_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2 =
      let pos = string_pos "unify_type_lists" pos in
         match tyl1, tyl2 with
            ty1 :: tyl1, ty2 :: tyl2 ->
               let subst = unify_types strictp subst pos bvars eq_ids ty1 ty2 in
                  unify_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2
          | [], [] ->
               subst
          | _ ->
               type_error pos ty1 ty2

   and unify_mutable_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2 =
      let pos = string_pos "unify_mutable_type_lists" pos in
         match tyl1, tyl2 with
            (ty1, b1) :: tyl1, (ty2, b2) :: tyl2 ->
               let subst = unify_types strictp subst pos bvars eq_ids ty1 ty2 in
               let subst = unify_mutable ty1 ty2 subst pos b1 b2 in
                  unify_mutable_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2
          | [], [] ->
               subst
          | _ ->
               type_error pos ty1 ty2

   and unify_mutable_type_lists_lists strictp ty1 ty2 subst pos bvars eq_ids tyll1 tyll2 =
      let pos = string_pos "unify_mutable_type_lists_lists" pos in
         match tyll1, tyll2 with
            tyl1 :: tyll1, tyl2 :: tyll2 ->
               let subst = unify_mutable_type_lists strictp ty1 ty2 subst pos bvars eq_ids tyl1 tyl2 in
                  unify_mutable_type_lists_lists strictp ty1 ty2 subst pos bvars eq_ids tyll1 tyll2
          | [], [] ->
               subst
          | _ ->
               type_error pos ty1 ty2

   and unify_short_mutable_type_lists ty1 ty2 subst pos bvars eq_ids tyl1 tyl2 =
      let pos = string_pos "unify_short_mutable_type_lists" pos in
         match tyl1, tyl2 with
            (ty1, b1) :: tyl1, (ty2, b2) :: tyl2 ->
               let subst = unify_types false subst pos bvars eq_ids ty1 ty2 in
               let subst = unify_mutable ty1 ty2 subst pos b1 b2 in
                  unify_short_mutable_type_lists ty1 ty2 subst pos bvars eq_ids tyl1 tyl2
          | [], _ ->
               subst
          | _ ->
               type_error pos ty1 ty2
   in
      unify_types strictp subst pos bvars eq_ids ty1' ty2'

(*
 * Wrap toplevel calls.
 *)
let unify_types genv bvars subst pos ty1 ty2 =
   unify_types false ty1 ty2 genv bvars subst pos bvars [] ty1 ty2

and unify_types_strict genv bvars subst pos ty1 ty2 =
   unify_types true ty1 ty2 genv bvars subst pos bvars [] ty1 ty2

let unify_types_opt genv bvars subst pos ty1 ty2 =
   match ty1 with
      Some ty1 -> unify_types genv bvars subst pos ty1 ty2
    | None -> subst

(************************************************************************
 * OPERATION INFERENCE
 ************************************************************************)

(*
 * Infer the type of an atom.
 *)
let rec infer_atom genv bvars subst pos a =
   let pos = string_pos "infer_atom" pos in
      match a with
         AtomNil ty ->
            ty, subst
       | AtomInt _ ->
            TyInt, subst
       | AtomEnum (n, _) ->
            TyEnum n, subst
       | AtomFloat x ->
            TyFloat (Rawfloat.precision x), subst
       | AtomRawInt i ->
            TyRawInt (Rawint.precision i, Rawint.signed i), subst
       | AtomVar v
       | AtomFun v ->
            let ty =
               if genv_mem_fun genv v then
                  genv_lookup_fun genv pos v
               else
                  genv_lookup_var genv pos v
            in
               ty, subst
       | AtomLabel _
       | AtomSizeof _ ->
            TyRawInt (precision_subscript, signed_subscript), subst
       | AtomConst (ty, ty_v, i) ->
            (match genv_lookup_type genv pos ty_v with
               TyDefUnion (vars, _) ->
                  let vars' = List.map (fun v -> TyVar (new_symbol_pre "ia" v)) vars in
                  let ty' = TyUnion (ty_v, vars', IntSet.of_point i) in
                  let subst = unify_types genv bvars subst pos ty ty' in
                     ty, subst
              | TyDefLambda _
              | TyDefDTuple _ ->
                  raise (FirException (pos, StringTypeError ("not a union type", ty))))
       | AtomUnop (op, a) ->
            let ty_dst, ty_src = type_of_unop pos op in
            let ty_atom, subst = infer_atom genv bvars subst pos a in
            let subst = unify_types genv bvars subst pos ty_src ty_atom in
               ty_dst, subst
       | AtomBinop (op, a1, a2) ->
            let ty_atom1, subst = infer_atom genv bvars subst pos a1 in
            let ty_atom2, subst = infer_atom genv bvars subst pos a2 in
            let ty_dst, ty_src1, ty_src2 = type_of_binop pos op in
            let subst = unify_types genv bvars subst pos ty_src1 ty_atom1 in
            let subst = unify_types genv bvars subst pos ty_src2 ty_atom2 in
               ty_dst, subst

         (*
          * Ignore all polymorphic coercions in this pass.
          * We'll reintroduce them on output.
          *)
       | AtomTyApply (a, _, _) ->
            infer_atom genv bvars subst pos a
       | AtomTyPack (v, _, _)
       | AtomTyUnpack v ->
            genv_lookup_var genv pos v, subst

let infer_atoms genv bvars subst pos args =
   let tyl, subst =
      List.fold_left (fun (tyl, subst) a ->
            let ty, subst = infer_atom genv bvars subst pos a in
               ty :: tyl, subst) ([], subst) args
   in
      List.rev tyl, subst

(*
 * Helper functions.
 *)
let unify_atom genv bvars subst pos a ty1 =
   let pos = string_pos "unify_atom" pos in
   let ty2, subst = infer_atom genv bvars subst pos a in
      unify_types genv bvars subst pos ty2 ty1

let unify_atom_rawindex genv bvars subst pos a =
   let pos = string_pos "unify_atom_rawindex" pos in
      unify_atom genv bvars subst pos a (TyRawInt (precision_subscript, signed_subscript))

(*
 * Function application.
 *)
let unify_atom_apply genv bvars subst pos ty_f args =
   let pos = string_pos "unify_atom_apply" pos in
   let ty_args, subst =
      List.fold_left (fun (ty_args, subst) a ->
            let ty, subst = infer_atom genv bvars subst pos a in
            let ty = exists_elim genv subst pos a ty in
               ty :: ty_args, subst) ([], subst) args
   in
   let ty_args = List.rev ty_args in
   let ty_res = TyVar (new_symbol_string "resuaa") in
   let ty_f' = TyFun (ty_args, ty_res) in
   let subst = unify_types genv bvars subst pos ty_f' ty_f in
      ty_res, subst

(*
 * Function application, but some of the arguments
 * are undefined.
 *)
let unify_atom_opt_apply genv bvars subst pos ty_f args =
   let pos = string_pos "unify_atom_partial_apply" pos in
   let ty_args, subst =
      List.fold_left (fun (ty_args, subst) a ->
            match a with
               Some a ->
                  let ty, subst = infer_atom genv bvars subst pos a in
                  let ty = exists_elim genv subst pos a ty in
                     ty :: ty_args, subst
             | None ->
                  let v = new_symbol_string "none" in
                  let ty = TyVar v in
                     ty :: ty_args, subst) ([], subst) args
   in
   let ty_args = List.rev ty_args in
   let ty_res = TyVar (new_symbol_string "resuaoa") in
   let ty_f' = TyFun (ty_args, ty_res) in
   let subst = unify_types genv bvars subst pos ty_f' ty_f in
      ty_res, subst

(*
 * External calls.
 *)
let apply_all_type genv pos ty ty_args =
   let pos = string_pos "apply_all_type" pos in

   (* The type had better be a forall type, generate new vars *)
   let ty_vars, ty = dest_all_type genv pos ty in
   let len1 = List.length ty_vars in
   let len2 = List.length ty_args in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let subst = List.fold_left2 Fir_subst.subst_add_type_var subst_empty ty_vars ty_args in
      subst_type subst ty

let infer_ext genv bvars subst pos s ty ty_args args =
   let pos = string_pos "infer_ext" pos in
   let ty = apply_all_type genv pos ty ty_args in
      unify_atom_apply genv bvars subst pos ty args

(*
 * Allocation.
 *)
let infer_alloc_op genv bvars subst pos op =
   match op with
      AllocTuple (tclass, ty_vars, ty, args) ->
         let pos = string_pos "infer_alloc_tuple" pos in
         let bvars = bvars_add_vars bvars ty_vars in
         let tyl, subst = infer_atoms genv bvars subst pos args in
         let tyl = List.map (fun ty -> ty, MutableVar (new_symbol_string "tuple")) tyl in
         let ty = TyAll (ty_vars, ty) in
         let ty' = TyAll (ty_vars, TyTuple (tclass, tyl)) in
         let subst = unify_types genv bvars subst pos ty ty' in
            ty, subst
    | AllocUnion (ty_vars, ty, ty_v, i, args) ->
         let pos = string_pos "infer_alloc_union" pos in
         let bvars = bvars_add_vars bvars ty_vars in
         let tyl', subst = infer_atoms genv bvars subst pos args in
         let tyl' = List.map (fun ty -> ty, MutableVar (new_symbol_string "union")) tyl' in
         let ty_vars', tyl = dest_union_fields genv pos ty_v i in
         let subst = unify_types genv bvars subst pos (TyTuple (NormalTuple, tyl)) (TyTuple (NormalTuple, tyl')) in
         let ty_args = List.map (fun v -> TyVar v) ty_vars' in
         let ty' = TyAll (ty_vars, TyUnion (ty_v, ty_args, IntSet.of_point i)) in
         let ty = TyAll (ty_vars, ty) in
         let subst = unify_types genv bvars subst pos ty ty' in
            ty, subst
    | AllocDTuple (ty1, ty_var, a, args) ->
         let pos = string_pos "infer_alloc_dtuple" pos in
         let tyl, subst = infer_atoms genv bvars subst pos args in
         let ty_fields = List.map (fun ty -> ty, MutableVar (new_symbol_string "dtuple")) tyl in

         (* Unify the tag types *)
         let ty_tag1, subst = infer_atom genv bvars subst pos a in
         let ty_tag2 = TyTag (ty_var, ty_fields) in
         let subst = unify_types genv bvars subst pos ty_tag1 ty_tag2 in

         (* The type should be a TyDTuple *)
         let ty2 = TyDTuple (ty_var, Some ty_fields) in
         let subst = unify_types genv bvars subst pos ty1 ty2 in
            ty1, subst

    | AllocArray (ty, []) ->
         let pos = string_pos "infer_alloc_array_nil" pos in
         let v = new_symbol_string "alloc_array" in
         let subst = unify_types genv bvars subst pos ty (TyArray (TyVar v)) in
            ty, subst
    | AllocArray (ty, a :: args) ->
         let pos = string_pos "infer_alloc_array" pos in
         let ty_a, subst = infer_atom genv bvars subst pos a in
         let subst =
            List.fold_left (fun subst a ->
                  let ty', subst = infer_atom genv bvars subst pos a in
                     unify_types genv bvars subst pos ty_a ty') subst args
         in
         let ty' = TyArray ty_a in
         let subst = unify_types genv bvars subst pos ty ty' in
            ty, subst
    | AllocVArray (ty, sub_index, a1, a2) ->
         let pos = string_pos "infer_alloc_varray" pos in
         let ty_a, subst = infer_atom genv bvars subst pos a2 in
         let ty' = TyArray ty_a in
         let subst = unify_types genv bvars subst pos ty ty' in
            ty, subst
    | AllocMalloc (ty, a) ->
         let pos = string_pos "infer_alloc_malloc" pos in
         let subst = unify_types genv bvars subst pos ty TyRawData in
            ty, subst
    | AllocFrame (v, tyl) ->
         TyFrame (v, tyl), subst

(*
 * Type inference for a name record.
 * The type of the record is not allowed to be Delayed.
 *)
let infer_names genv bvars subst pos ty v names =
   let pos = string_pos "infer_names" pos in
   let ty_case = dest_case_type genv pos ty in
   let subst =
      List.fold_left (fun subst (v, v_opt) ->
            let ty' = genv_lookup_var genv pos v in
            let subst = unify_types genv bvars subst pos ty' ty_case in
               match v_opt with
                  Some v ->
                     raise (FirException (pos, StringError "interfaces not implemented"))
                | None ->
                     subst) subst names
   in
      ty, subst

(*
 * Type inference for initializations.
 *)
let infer_init genv bvars subst pos ty init =
   let pos = string_pos "infer_init" pos in
   let ty', subst =
      match init with
         InitAtom a ->
            infer_atom genv bvars subst pos a
       | InitAlloc op ->
            infer_alloc_op genv bvars subst pos op
       | InitTag (ty_var, fields) ->
            TyTag (ty_var, fields), subst
       | InitNames (v, names) ->
            infer_names genv bvars subst pos ty v names
       | InitRawData _ ->
            TyRawData, subst
   in
      match expand_type genv pos ty with
         TyRawData ->
            (* No need to check the type *)
            subst
       | ty ->
            unify_types genv bvars subst pos ty' ty

(*
 * Predicate inference.
 * The subop should always specify a rawint index.
 *)
let infer_pred genv bvars subst pos pred =
   let pos = string_pos "infer_pred" pos in
   let check_subop op =
      match op with
         { sub_index = ByteIndex; sub_script = RawIntIndex (pre, signed) }
         when pre = precision_subscript && signed = signed_subscript ->
               ()
       | _ ->
            raise (FirException (pos, StringError "subscript operator should specify a int32 byte index"))
   in
      match pred with
         IsMutable _ ->
            subst
       | ElementCheck (_, op, _, a) ->
            check_subop op;
            unify_atom_rawindex genv bvars subst pos a
       | Reserve (a1, a2) ->
            let subst = unify_atom_rawindex genv bvars subst pos a1 in
            let subst = unify_atom_rawindex genv bvars subst pos a2 in
               subst
       | BoundsCheck (op, _, a1, a2) ->
            check_subop op;
            let subst = unify_atom_rawindex genv bvars subst pos a1 in
            let subst = unify_atom_rawindex genv bvars subst pos a2 in
               subst

(************************************************************************
 * TYPE INFERENCE
 ************************************************************************)

(*
 * Infer the type for an expression.
 *)
let rec infer_exp genv bvars subst e =
   let pos = string_pos "infer_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty,  a, e) ->
            infer_atom_exp genv bvars subst pos v ty a e
       | LetExt (v, ty, s, _, ty2, ty_args, args, e) ->
            infer_ext_exp genv bvars subst pos v ty s ty2 ty_args args e
       | TailCall (_, f, args) ->
            infer_tailcall_exp genv bvars subst pos f args
       | SpecialCall (_, TailSysMigrate (i, loc_ptr, loc_off, f, args)) ->
            infer_tail_sysmigrate_exp genv bvars subst pos i loc_ptr loc_off f args
       | SpecialCall (_, TailAtomic (f, c, args)) ->
            infer_tail_atomic_exp genv bvars subst pos f c args
       | SpecialCall (_, TailAtomicRollback (level, c)) ->
            infer_tail_atomic_rollback_exp genv bvars subst pos level c
       | SpecialCall (_, TailAtomicCommit (level, f, args)) ->
            infer_tail_atomic_commit_exp genv bvars subst pos level f args
       | Match (a, cases) ->
            infer_match_exp genv bvars subst pos a cases
       | MatchDTuple (a, cases) ->
            infer_match_dtuple_exp genv bvars subst pos a cases
       | TypeCase (a1, a2, name, v, e1, e2) ->
            infer_typecase_exp genv bvars subst pos a1 a2 name v e1 e2
       | LetAlloc (v, op, e) ->
            infer_alloc_exp genv bvars subst pos v op e
       | LetSubscript (op, v, ty, v2, a, e) ->
            infer_subscript_exp genv bvars subst pos op v ty v2 a e
       | SetSubscript (op, _, v, a1, ty, a2, e) ->
            infer_set_subscript_exp genv bvars subst pos op v a1 ty a2 e
       | LetGlobal (op, v, ty, l, e) ->
            infer_global_exp genv bvars subst pos op v ty l e
       | SetGlobal (op, _, v, ty, a, e) ->
            infer_set_global_exp genv bvars subst pos op v ty a e
       | Memcpy (op, _, v1, a1, v2, a2, a3, e) ->
            infer_memcpy_exp genv bvars subst pos op v1 a1 v2 a2 a3 e
       | Assert (_, pred, e) ->
            infer_assert_exp genv bvars subst pos pred e
       | Call (_, f, args, e) ->
            infer_call_exp genv bvars subst pos f args e
       | Debug (info, e) ->
            infer_debug_exp genv bvars subst pos info e

(*
 * Operators.
 *)
and infer_atom_exp genv bvars subst pos v ty a e =
   let pos = string_pos "infer_atom_exp" pos in
   let genv =
      match a with
         AtomVar v' ->
            genv_add_norm genv v v'
       | _ ->
            genv
   in
   let ty'', subst = infer_atom genv bvars subst pos a in
   let ty' = exists_elim genv subst pos a ty in
   let ty'' = exists_elim genv subst pos a ty'' in
   let subst = unify_types genv bvars subst pos ty' ty'' in
   let genv = genv_add_var genv v ty in
      infer_exp genv bvars subst e

and infer_ext_exp genv bvars subst pos v ty s ty2 ty_args args e =
   let pos = string_pos "infer_ext_exp" pos in
   let ty_res, subst = infer_ext genv bvars subst pos s ty2 ty_args args in
   let subst = unify_types genv bvars subst pos ty ty_res in
   let genv = genv_add_var genv v ty in
     infer_exp genv bvars subst e

and infer_global_exp genv bvars subst pos subop v ty l e =
   let pos = string_pos "infer_global_exp" pos in
   let ty' = genv_lookup_global genv pos l in
   let subst = unify_types genv bvars subst pos ty ty' in
   let genv = genv_add_var genv v ty in
      infer_exp genv bvars subst e

(*
 * Tailcall.
 *)
and infer_tailcall_exp genv bvars subst pos f args =
   let pos = string_pos "infer_tailcall_exp" pos in
   let ty_f, subst = infer_atom genv bvars subst pos f in
   let ty_res, subst = unify_atom_apply genv bvars subst pos ty_f args in
   let subst = unify_types genv bvars subst pos ty_void ty_res in
      ty_void, subst

and infer_call_exp genv bvars subst pos f args e =
   let pos = string_pos "infer_call_exp" pos in
   let ty_f, subst = infer_atom genv bvars subst pos f in
      unify_atom_opt_apply genv bvars subst pos ty_f args

(*
 * Conditional.
 *)
and infer_match_exp genv bvars subst pos a cases =
   let pos = string_pos "infer_match_exp" pos in
   let ty_a, subst = infer_atom genv bvars subst (int_pos 1 pos) a in

   (*
    * Inside a match case, union types are specialized.
    *)
   let adjust_genv, full_set =
      let ty = subst_type subst (exists_elim genv subst pos a ty_a) in
      let ty = expand_type genv pos ty in       (* In case we get TyApply. *)
      match a, ty with
         AtomVar v, TyUnion (ty_v, tyl, set) ->
            let adjust genv set' = genv_add_var genv v (TyUnion (ty_v, tyl, IntSet.isect set set')) in
            let set = Some set in
               adjust, set
       | _, TyEnum n ->
            let adjust genv set = genv in
            let set = Some (IntSet.of_interval (Closed 0) (Open n)) in
               adjust, set
       | _ ->
            let adjust genv set = genv in
            let set = None in
               adjust, set
   in

   (*
    * Get the type of the union.
    *)
   let set, ty, subst =
      List.fold_left (fun (set, ty, subst) (_, set', e) ->
            let genv, set =
               match set' with
                  IntSet s ->
                     let genv = adjust_genv genv s in
                     let s = int_set_opt_union (int_pos 2 pos) set s in
                        genv, s
                | RawIntSet s ->
                     genv, rawint_set_opt_union (int_pos 3 pos) set s
            in
            let ty', subst = infer_exp genv bvars subst e in
            let subst = unify_types_opt genv bvars subst (int_pos 4 pos) ty ty' in
               set, Some ty', subst) (None, None, subst) cases
   in
      (*
       * Check for incomplete matches.
       *)
      match full_set, set, ty with
         Some full_set, Some (IntSet set), Some ty ->
            (* This is a union *)
            let _ =
               if not (IntSet.subset full_set set) then
                  raise (FirException (int_pos 5 pos, IncompleteMatch (IntSet full_set, IntSet set)))
            in
               ty, subst

       | None, Some set, Some ty ->
            let ty_set =
               match set with
                  IntSet s ->
                     if IntSet.is_total s then
                        TyInt
                     else if IntSet.is_enum s 0 then
                        let n = IntSet.to_enum s in
                           TyEnum n
                     else
                        raise (FirException (int_pos 6 pos, IncompleteMatch (IntSet IntSet.max_set, set)))
                | RawIntSet s ->
                     let pre = RawIntSet.precision s in
                     let signed = RawIntSet.signed s in
                        if RawIntSet.is_total s then
                           TyRawInt (pre, signed)
                        else
                           raise (FirException (int_pos 7 pos, IncompleteMatch (RawIntSet (RawIntSet.max_set pre signed), set)))
            in
            let subst = unify_types genv bvars subst (int_pos 8 pos) ty_set ty_a in
               ty, subst

       | _, None, None ->
            let subst = unify_types genv bvars subst (int_pos 9 pos) ty_void ty_a in
               ty_void, subst

       | _ ->
            raise (FirException (int_pos 10 pos, InternalError "match impossible"))

(*
 * DTuple matching.
 * The constraint is that the atom has to be a TyDTuple (ty_var, _),
 * and each of the tags have to have type TyTag (ty_var, tyl).  To make things
 * easy, we assume that the tag types are known.
 *)
and infer_match_dtuple_exp genv bvars subst pos a cases =
   let pos = string_pos "infer_match_dtuple_exp" pos in
   let ty_match = TyVar (new_symbol_string "infer_match_dtuple") in
   let v = var_of_atom (int_pos 1 pos) a in

   (* Match all the cases--assuming that the tag types are known *)
   let ty_var_opt, subst =
      List.fold_left (fun (ty_var_opt, subst) (_, a_opt, e) ->
            match a_opt with
               Some a ->
                  let ty_tag, subst = infer_atom genv bvars subst (int_pos 2 pos) a in
                  let ty_var, ty_fields = dest_tag_type genv subst (int_pos 3 pos) ty_tag in
                  let genv = genv_add_var genv v (TyDTuple (ty_var, Some ty_fields)) in
                  let ty, subst = infer_exp genv bvars subst e in
                  let subst = unify_types genv bvars subst (int_pos 4 pos) ty_match ty in
                     Some ty_var, subst
             | None ->
                  let ty, subst = infer_exp genv bvars subst e in
                  let subst = unify_types genv bvars subst (int_pos 5 pos) ty_match ty in
                     ty_var_opt, subst) (None, subst) cases
   in

   (* Get the dtuple name *)
   let ty_var =
      match ty_var_opt with
         Some ty_var ->
            ty_var
       | None ->
            raise (FirException (int_pos 6 pos, StringError "match_dtuple has no cases"))
   in

   (* Now unify with the argument type *)
   let ty_arg1 = TyDTuple (ty_var, None) in
   let ty_arg2, subst = infer_atom genv bvars subst (int_pos 7 pos) a in
   let subst = unify_types genv bvars subst (int_pos 8 pos) ty_arg1 ty_arg2 in
      (* Finally, we're done *)
      ty_match, subst

(*
 * Typecase.
 *)
and infer_typecase_exp genv bvars subst pos a1 a2 name v e1 e2 =
   let pos = string_pos "infer_typecase_exp" pos in
   let ty1, subst = infer_atom genv bvars subst pos a1 in
   let ty2, subst = infer_atom genv bvars subst pos a2 in
   let ty_name = genv_lookup_var genv pos name in
   let subst = unify_types genv bvars subst pos ty_name ty1 in
   let subst = unify_types genv bvars subst pos (TyCase ty1) ty2 in
   let ty2, subst = infer_exp genv bvars subst e2 in
   let genv = genv_add_var genv v ty_name in
   let ty1, subst = infer_exp genv bvars subst e1 in
   let subst = unify_types genv bvars subst pos ty1 ty2 in
      ty1, subst

(*
 * Allocation.
 *
 * Be careful to infer the rest of the program first, to
 * give priority to how the data is used.
 *)
and infer_alloc_exp genv bvars subst pos v op e =
   let pos = string_pos "infer_alloc_exp" pos in
   let ty_op1 = type_of_alloc_op op in
   let genv_op = genv_add_var genv v ty_op1 in
   let ty_exp, subst = infer_exp genv_op bvars subst e in
   let ty_op2, subst = infer_alloc_op genv bvars subst pos op in
   let subst = unify_types genv bvars subst pos ty_op1 ty_op2 in
      ty_exp, subst

(*
 * Subscripting.  We figure out as much as we can in this phase,
 * but if that is not possible, put it off until the next stage.
 *)
and infer_subscript_exp genv bvars subst pos op v1 ty v2 a3 e =
   let pos = string_pos "infer_subscript_exp" pos in
   let ty_array, subst = infer_atom genv bvars subst pos v2 in
   let subst = infer_subscript genv bvars subst pos op false ty ty_array a3 in
   let genv = genv_add_var genv v1 ty in
      infer_exp genv bvars subst e

and infer_set_subscript_exp genv bvars subst pos op v1 a2 ty a3 e =
   let pos = string_pos "infer_set_subscript_exp" pos in
   let ty_array, subst = infer_atom genv bvars subst (int_pos 1 pos) v1 in
   let subst = infer_subscript genv bvars subst pos op true ty ty_array a2 in
   let ty3, subst = infer_atom genv bvars subst (int_pos 3 pos) a3 in
   let subst = unify_types genv bvars subst pos ty ty3 in
      infer_exp genv bvars subst e

and infer_subscript genv bvars subst pos op swap ty_elem ty_array a_index =
   let pos = string_pos "infer_subscript" pos in
   let ty_index, subst = infer_atom genv bvars subst (int_pos 0 pos) a_index in
   let { sub_index = sub_index;
         sub_value = sub_value;
         sub_script = sub_script
       } = op
   in

   (* Swap element types *)
   let unify_types_swap =
      if swap then
         (fun pos ty1 ty2 -> unify_types genv bvars subst pos ty2 ty1)
      else
         (unify_types genv bvars subst)
   in

   (* Make sure the index has the right type *)
   let ty_index' =
      match sub_script with
         IntIndex ->
            TyInt
       | RawIntIndex (pre, signed) ->
            TyRawInt (Rawint.Int32, true)
   in
   let subst = unify_types genv bvars subst (int_pos 3 pos) ty_index' ty_index in

   (* Make sure the value has the right type *)
   let subst =
      match sub_value with
         EnumSub n ->
            unify_types_swap (int_pos 19 pos) ty_elem (TyEnum n)
       | IntSub ->
            unify_types_swap (int_pos 20 pos) ty_elem TyInt
       | TagSub (ty_var, tyl) ->
            unify_types_swap (int_pos 25 pos) ty_elem (TyTag (ty_var, tyl))
       | RawIntSub (pre, signed) ->
            unify_types_swap (int_pos 21 pos) ty_elem (TyRawInt (pre, signed))
       | RawFloatSub pre ->
            unify_types_swap (int_pos 22 pos) ty_elem (TyFloat pre)
       | PolySub
       | BlockPointerSub
       | RawPointerSub
       | PointerInfixSub
       | FunctionSub ->
            subst
   in

   (* Get constant index *)
   let i =
      match a_index with
         AtomInt i
       | AtomEnum (_, i) ->
            IntSubscript (word_index sub_index i)
       | AtomRawInt i ->
            IntSubscript (word_index sub_index (Rawint.to_int i))
       | AtomLabel (label, i) ->
            LabelSubscript (label, i)
       | _ ->
            NoSubscript
   in

   (* See if we can figure out the element type *)
   let rec unify ty_array =
      let pos = error_pos (StringTypeError ("infer_subscript", ty_array)) pos in
      let ty_array = subst_expand genv subst (int_pos 6 pos) ty_array in
         match ty_array, i with
            TyArray ty_elem', _ ->
               unify_types_swap (int_pos 23 pos) ty_elem ty_elem'
          | TyTuple (_, tyl), IntSubscript i ->
               let len = List.length tyl in
               let i = word_index sub_index i in
                  if i < 0 || i >= len then
                     raise (FirException (int_pos 7 pos, ArityMismatch (len, i)));
                  let ty_elem', _ = List.nth tyl i in
                     unify_types_swap (int_pos 24 pos) ty_elem ty_elem'
          | TyUnion (ty_v, ty_args, j), IntSubscript i ->
               let j = specialize_union_count genv (int_pos 8 pos) ty_v j in
               let tyl = apply_union_fields genv (int_pos 9 pos) ty_v ty_args j in
               let len = List.length tyl in
                  if i < 0 || i >= len then
                     raise (FirException (int_pos 10 pos, ArityMismatch (len, j)));
                  let ty_elem', _ = List.nth tyl i in
                     unify_types_swap (int_pos 25 pos) ty_elem ty_elem'
          | TyFrame (ty_v, ty_args), LabelSubscript (label, i) ->
               let ty_elem', i' = apply_frame genv (int_pos 11 pos) ty_v ty_args label in
               let subst =
                  if Rawint.is_zero i then
                     subst
                  else
                     unify_types_swap (int_pos 26 pos) TyRawData ty_elem'
               in
                  if is_rawdata_type genv subst (int_pos 12 pos) ty_elem' then
                     subst
                  else
                     unify_types_swap (int_pos 27 pos) ty_elem ty_elem'
          | TyExists (vars1, ty_array), _ ->
               let subst' =
                  List.fold_left (fun subst v ->
                        Fir_subst.subst_add_type_var subst v (TyVar (new_symbol_pre "is" v))) subst_empty vars1
               in
               let ty_array = subst_type subst' ty_array in
                  unify ty_array
          | _ ->
               (* If we can't figure anything else, postpone until the second stage *)
               subst
   in
      unify ty_array

(*
 * Store a value in memory.
 *)
and infer_set_global_exp genv bvars subst pos op v1 ty a2 e =
   let pos = string_pos "infer_set_global_exp" pos in
   let ty1 = genv_lookup_var genv (int_pos 1 pos) v1 in
   let ty2, subst = infer_atom genv bvars subst (int_pos 2 pos) a2 in
   let subst =
      match op with
         EnumSub n ->
            unify_types genv bvars subst pos (TyEnum n) ty
       | IntSub ->
            unify_types genv bvars subst pos TyInt ty
       | TagSub (ty_var, fields) ->
            unify_types genv bvars subst pos (TyTag (ty_var, fields)) ty
       | RawIntSub (pre, signed) ->
            unify_types genv bvars subst pos (TyRawInt (pre, signed)) ty
       | RawFloatSub pre ->
            unify_types genv bvars subst pos (TyFloat pre) ty
       | PolySub
       | BlockPointerSub
       | RawPointerSub
       | PointerInfixSub
       | FunctionSub ->
            subst
   in
   let subst = unify_types genv bvars subst (int_pos 4 pos) ty ty1 in
   let subst = unify_types genv bvars subst (int_pos 16 pos) ty ty2 in
      infer_exp genv bvars subst e

(*
 * Copy data in memory.
 *)
and infer_memcpy_exp genv bvars subst pos op v1 a1 v2 a2 a3 e =
   let pos = string_pos "infer_memcpy_exp" pos in
   let ty_a1, subst = infer_atom genv bvars subst pos a1 in
   let ty_a2, subst = infer_atom genv bvars subst pos a2 in
   let ty_a3, subst = infer_atom genv bvars subst pos a3 in
   let ty_index =
      match op.sub_script with
         IntIndex ->
            TyInt
       | RawIntIndex (pre, signed) ->
            TyRawInt (pre, signed)
   in
   let subst = unify_types genv bvars subst pos ty_index ty_a1 in
   let subst = unify_types genv bvars subst pos ty_index ty_a2 in
   let subst = unify_types genv bvars subst pos ty_index ty_a3 in
      infer_exp genv bvars subst e

(*
 * Assertion.
 *)
and infer_assert_exp genv bvars subst pos pred e =
   let pos = string_pos "infer_assert_exp" pos in
   let subst = infer_pred genv bvars subst pos pred in
      infer_exp genv bvars subst e

(*
 * Debugging.
 *)
and infer_debug_exp genv bvars subst pos info e =
   let pos = string_pos "infer_debug_exp" pos in
   let ty_subst = infer_exp genv bvars subst e in
      match info with
         DebugString _ ->
            ty_subst
       | DebugContext (_, vars) ->
            let ty, subst = ty_subst in
            let subst =
               List.fold_left (fun subst (_, ty, v) ->
                     let pos = var_pos v pos in
                     let ty' = genv_lookup_var genv pos v in
                     let ty = exists_elim_var genv subst pos v ty in
                     let ty' = exists_elim_var genv subst pos v ty' in
                        unify_types genv bvars subst pos ty ty') subst vars
            in
               ty, subst

(*
 * Migration.
 *)
and infer_tail_sysmigrate_exp genv bvars subst pos i loc_ptr loc_off f args =
   let pos = string_pos "infer_tail_sysmigrate_exp" pos in

   (* Verify the types of the destination pointer *)
   let ty_loc_ptr, subst = infer_atom genv bvars subst pos loc_ptr in
   let ty_loc_off, subst = infer_atom genv bvars subst pos loc_off in
   let subst = unify_types genv bvars subst pos TyRawData ty_loc_ptr in
   let subst = unify_types genv bvars subst pos ty_int32 ty_loc_off in

   (* Verify the types of the function arguments *)
   let ty_f, subst = infer_atom genv bvars subst pos f in
      unify_atom_apply genv bvars subst pos ty_f args

(*
 * Rollback.
 *)
and infer_tail_atomic_exp genv bvars subst pos f c args =
   let pos = string_pos "infer_tail_atomic_exp" pos in

   (* Verify the types of the special options *)
   let ty_c, subst = infer_atom genv bvars subst pos c in
   let subst = unify_types genv bvars subst pos ty_int32 ty_c in

   (* Verify the types of the function arguments *)
   let ty_f, subst = infer_atom genv bvars subst pos f in
      unify_atom_apply genv bvars subst pos ty_f (c :: args)

and infer_tail_atomic_commit_exp genv bvars subst pos level f args =
   let pos = string_pos "infer_tail_atomic_commit_exp" pos in

   (* Verify the types of the special options *)
   let ty_level, subst = infer_atom genv bvars subst pos level in
   let subst = unify_types genv bvars subst pos ty_int32 ty_level in

   (* Verify the types of the function arguments *)
   let ty_f, subst = infer_atom genv bvars subst pos f in
      unify_atom_apply genv bvars subst pos ty_f args

and infer_tail_atomic_rollback_exp genv bvars subst pos level c =
   let pos = string_pos "infer_tail_atomic_rollback_exp" pos in

   (* Verify the types of the special options *)
   let ty_level, subst = infer_atom genv bvars subst pos level in
   let subst = unify_types genv bvars subst pos ty_int32 ty_level in
   let ty_c, subst = infer_atom genv bvars subst pos c in
   let subst = unify_types genv bvars subst pos ty_int32 ty_c in

   (* No explicit apply is done on rollback *)
   let ty_res = TyVar (new_symbol_string "res") in
      ty_res, subst

(*
 * Type inference for a global declaration.
 *)
let infer_global genv bvars subst v ty init =
   let pos = string_pos "infer_global" (var_exp_pos v) in
   let subst = infer_init genv bvars subst pos ty init in
   let genv = genv_add_var genv v ty in
      genv, subst

let infer_globals genv bvars subst globals =
   SymbolTable.fold (fun (genv, subst) v (ty, init) ->
         infer_global genv bvars subst v ty init) (genv, subst) globals

(*
 * Type inference for a function declaration.
 *)
let infer_fundef genv bvars subst f def =
   let pos = string_pos "infer_fundef" (fundef_pos f def) in
   let _, ty_vars, ty, vars, e = def in
   let ty_args, ty_res = dest_fun_type genv (int_pos 1 pos) ty in
   let len1 = List.length ty_args in
   let len2 = List.length vars in
   let _ =
      if len1 <> len2 then
         raise (FirException (int_pos 2 pos, ArityMismatch (len1, len2)))
   in
   let genv =
      List.fold_left2 (fun genv v ty ->
            let ty = exists_elim_var genv subst pos v ty in
               genv_add_var genv v ty) genv vars ty_args
   in
   let ty_res', subst = infer_exp genv bvars subst e in
      unify_types genv bvars subst (int_pos 3 pos) ty_res ty_res'

(*
 * Type inference for the program.
 *)
let infer_prog prog =
   (* Add all the type parameters as bound *)
   let bvars =
      SymbolTable.fold (fun bvars _ (_, ty_vars, _, _, _) ->
            bvars_add_vars bvars ty_vars) bvars_empty prog.prog_funs
   in

   (* Infer globals *)
   let genv = genv_of_prog prog in
   let genv, subst = infer_globals genv bvars subst_empty prog.prog_globals in
      (* Infer all the functions *)
      SymbolTable.fold (fun subst f def ->
            infer_fundef genv bvars subst f def) subst prog.prog_funs

(************************************************************************
 * SUBSCRIPT INFERENCE
 ************************************************************************)

(*
 * Infer the type for an expression.
 *)
let rec subscript_exp genv bvars subst e =
   let pos = string_pos "subscript_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, _, e)
       | LetExt (v, ty, _, _, _, _, _, e)
       | LetGlobal (_, v, ty, _, e) ->
            subscript_op_exp genv bvars subst pos v ty e
       | TailCall _ ->
            subst
       | SpecialCall (_, TailSysMigrate (i, loc_ptr, loc_off, f, args)) ->
            subscript_tail_sysmigrate_exp genv bvars subst pos i loc_ptr loc_off f args
       | SpecialCall (_, TailAtomic (f, c, args)) ->
            subscript_tail_atomic_exp genv bvars subst pos f c args
       | SpecialCall (_, TailAtomicRollback (level, c)) ->
            subscript_tail_atomic_rollback_exp genv bvars subst pos level c
       | SpecialCall (_, TailAtomicCommit (level, f, args)) ->
            subscript_tail_atomic_commit_exp genv bvars subst pos level f args
       | Match (a, cases) ->
            subscript_match_exp genv bvars subst pos a cases
       | MatchDTuple (a, cases) ->
            subscript_match_dtuple_exp genv bvars subst pos a cases
       | TypeCase (_, _, name, v, e1, e2) ->
            subscript_typecase_exp genv bvars subst pos name v e1 e2
       | LetAlloc (v, op, e) ->
            subscript_alloc_exp genv bvars subst pos v op e
       | LetSubscript (op, v, ty, v2, a, e) ->
            subscript_subscript_exp genv bvars subst pos op v ty v2 a e
       | SetSubscript (op, _, v, a1, ty, a2, e) ->
            subscript_set_subscript_exp genv bvars subst pos op v a1 ty a2 e
       | SetGlobal (op, _, v, ty, a, e) ->
            subscript_set_global_exp genv bvars subst pos op v ty a e
       | Assert (_, op, e) ->
            subscript_pred_exp genv bvars subst pos op e
       | Call (_, _, _, e)
       | Debug (_, e)
       | Memcpy (_, _, _, _, _, _, _, e) ->
            subscript_exp genv bvars subst e

(*
 * Operators.
 *)
and subscript_op_exp genv bvars subst pos v ty e =
   let pos = string_pos "subscript_op_exp" pos in
   let genv = genv_add_var genv v ty in
      subscript_exp genv bvars subst e

(*
 * Conditional.
 *)
and subscript_match_exp genv bvars subst pos a cases =
   let pos = string_pos "subscript_match_exp" pos in

   (*
    * Inside a match case, union types are specialized.
    *)
   let ty_a = type_of_atom genv pos a in
   let adjust_genv =
      let ty = subst_type subst (exists_elim genv subst pos a ty_a) in
      let ty = expand_type genv pos ty in       (* In case we get TyApply. *)
         match a, ty with
            AtomVar v, TyUnion (ty_v, tyl, set) ->
               (fun genv set' -> genv_add_var genv v (TyUnion (ty_v, tyl, IntSet.isect set set')))
          | _ ->
               (fun genv set -> genv)
   in
      (* Now check all subscripts in the cases *)
      List.fold_left (fun subst (_, set, e) ->
            let genv =
               match set with
                  IntSet s ->
                     adjust_genv genv s
                | RawIntSet _ ->
                     genv
            in
               subscript_exp genv bvars subst e) subst cases

(*
 * Tuple matching.
 *)
and subscript_match_dtuple_exp genv bvars subst pos a cases =
   let pos = string_pos "subscript_match_dtuple_exp" pos in

   (*
    * Inside a match case, union types are specialized.
    *)
   let ty_a = type_of_atom genv pos a in
   let adjust_genv =
      let ty = subst_type subst (exists_elim genv subst pos a ty_a) in
      let ty = expand_type genv pos ty in       (* In case we get TyApply. *)
         match a, ty with
            AtomVar v, TyDTuple (ty_v, _) ->
               (fun genv tyl -> genv_add_var genv v (TyDTuple (ty_v, Some tyl)))
          | _ ->
               (fun genv tyl -> genv)
   in
      (* Now check all subscripts in the cases *)
      List.fold_left (fun subst (_, a_opt, e) ->
            let genv =
               match a_opt with
                  Some a ->
                     let ty_tag = type_of_global_atom genv pos a in
                     let _, tyl = dest_tag_type genv subst pos ty_tag in
                        adjust_genv genv tyl
                | None ->
                     genv
            in
               subscript_exp genv bvars subst e) subst cases

(*
 * Typecase.
 *)
and subscript_typecase_exp genv bvars subst pos name v e1 e2 =
   let pos = string_pos "subscript_typecase_exp" pos in
   let subst = subscript_exp genv bvars subst e2 in
   let ty_name = genv_lookup_var genv pos name in
   let genv = genv_add_var genv v ty_name in
      subscript_exp genv bvars subst e1

(*
 * Allocation.
 *)
and subscript_alloc_exp genv bvars subst pos v op e =
   let pos = string_pos "subscript_alloc_exp" pos in
   let ty = type_of_alloc_op op in
   let genv = genv_add_var genv v ty in
      subscript_exp genv bvars subst e

(*
 * Subscripting.
 * Now that we have finished the first pass of type
 * unification, we should know what kind of subscript we
 * are performing.  If will _still_ can't figure out what
 * the type of the base tuple/union/array is, we assume it
 * is an error.
 *)
and subscript_subscript_exp genv bvars subst pos op v1 ty v2 a3 e =
   let pos = string_pos "subscript_subscript_exp" pos in
   let ty_array, subst = infer_atom genv bvars subst pos v2 in
   let subst = subscript_subscript genv bvars subst pos op false ty ty_array a3 in
   let genv = genv_add_var genv v1 ty in
      subscript_exp genv bvars subst e

and subscript_set_subscript_exp genv bvars subst pos op v1 a2 ty a3 e =
   let pos = string_pos "subscript_set_subscript_exp" pos in
   let ty_array, subst = infer_atom genv bvars subst pos v1 in
   let subst = subscript_subscript genv bvars subst pos op true ty ty_array a2 in
      subscript_exp genv bvars subst e

and subscript_subscript genv bvars subst pos subop swap ty_elem ty_array a_index =
   let pos = string_pos "subscript_subscript" pos in
   let ty_index, subst = infer_atom genv bvars subst pos a_index in
   let { sub_block = sub_block;
         sub_value = sub_value;
         sub_index = sub_index;
         sub_script = sub_script
       } = subop
   in

   (* Swap element types *)
   let unify_types_swap =
      if swap then
         (fun ty1 ty2 -> unify_types genv bvars subst pos ty2 ty1)
      else
         (unify_types genv bvars subst pos)
   in

   (* Make sure the index has the right type *)
   let ty_index' =
      match sub_script with
         IntIndex ->
            TyInt
       | RawIntIndex (pre, signed) ->
            TyRawInt (Rawint.Int32, true)
   in
   let subst = unify_types genv bvars subst pos ty_index' ty_index in

   (* Make sure the value has the right type *)
   let subst =
      match sub_value with
         EnumSub n ->
            unify_types_swap ty_elem (TyEnum n)
       | IntSub ->
            unify_types_swap ty_elem TyInt
       | TagSub (ty_var, tyl) ->
            unify_types_swap ty_elem (TyTag (ty_var, tyl))
       | RawIntSub (pre, signed) ->
            unify_types_swap ty_elem (TyRawInt (pre, signed))
       | RawFloatSub pre ->
            unify_types_swap ty_elem (TyFloat pre)
       | PolySub
       | BlockPointerSub
       | RawPointerSub
       | PointerInfixSub
       | FunctionSub ->
            subst
   in

   (* Get constant index *)
   let i =
      match a_index with
         AtomInt i
       | AtomEnum (_, i) ->
            IntSubscript (word_index sub_index i)
       | AtomRawInt i ->
            IntSubscript (word_index sub_index (Rawint.to_int i))
       | AtomLabel (label, i) ->
            LabelSubscript (label, i)
       | _ ->
            NoSubscript
   in

   (* Unify element types *)
   let rec unify ty_array =
      let ty_array = subst_expand genv subst (int_pos 4 pos) ty_array in
         match ty_array, i with
            TyArray ty_elem', _ ->
               unify_types_swap ty_elem ty_elem'
          | TyTuple (_, tyl), IntSubscript i
          | TyDTuple (_, Some tyl), IntSubscript i ->
               let len = List.length tyl in
               let i = word_index sub_index i in
                  if i < 0 || i >= len then
                     raise (FirException (int_pos 6 pos, ArityMismatch (len, i)));
                  let ty_elem', _ = List.nth tyl i in
                     unify_types_swap ty_elem ty_elem'
          | TyUnion (ty_v, ty_args, j), IntSubscript i ->
               let j = specialize_union_count genv (int_pos 8 pos) ty_v j in
               let tyl = apply_union_fields genv (int_pos 9 pos) ty_v ty_args j in
               let len = List.length tyl in
                  if i < 0 || i >= len then
                     raise (FirException (int_pos 10 pos, ArityMismatch (len, j)));
                  let ty_elem', _ = List.nth tyl i in
                     unify_types_swap ty_elem ty_elem'
          | TyFrame (ty_v, ty_args), LabelSubscript (label, i) ->
               let ty_elem', i' = apply_frame genv pos ty_v ty_args label in
               let subst =
                  if Rawint.is_zero i then
                     subst
                  else
                     unify_types_swap TyRawData ty_elem'
               in
                  if is_rawdata_type genv subst pos ty_elem' then
                     subst
                  else
                     unify_types_swap ty_elem ty_elem'

          | TyExists (vars1, ty_array), _ ->
               let subst' =
                  List.fold_left (fun subst v ->
                        Fir_subst.subst_add_type_var subst v (TyVar (new_symbol_pre "ss" v))) subst_empty vars1
               in
               let ty_array = subst_type subst' ty_array in
                  unify ty_array
          | TyObject (v, ty_array), _ ->
               let subst' = subst_add_type_var bvars subst_empty pos v ty_array in
               let ty_array = subst_type subst' ty_array in
                  unify ty_array
          | TyPointer _, _
          | TyRawData, _
          | TyFrame _, _ ->
               subst
          | _ ->
               raise (FirException (int_pos 12 pos, SubscriptError (ty_array, ty_elem)))
   in
      unify ty_array

(*
 * Store a value in memory.
 *)
and subscript_set_global_exp genv bvars subst pos op v ty a e =
   let pos = string_pos "subscript_set_global_exp" pos in
      subscript_exp genv bvars subst e

(*
 * Check a subscript dimension.
 *)
and subscript_sub_subscript_exp genv bvars subst pos ty_v v1 v2 ty v3 a e =
   let pos = string_pos "subscript_sub_subscript_exp" pos in
   let genv = genv_add_var genv v1 (TyVar ty_v) in
   let genv = genv_add_var genv v2 ty in
      subscript_exp genv bvars subst e

(*
 * Predicates.
 *)
and subscript_pred_exp genv bvars subst pos op e =
   let pos = string_pos "subscript_pred_exp" pos in

   (* Make sure the array is really an array *)
   let rec unify ty_array subst =
      let ty_array = subst_expand genv subst (int_pos 1 pos) ty_array in
         match ty_array with
            TyArray _
          | TyTuple _
          | TyUnion _
          | TyRawData
          | TyFrame _ ->
               subst
          | TyExists (vars1, ty_array) ->
               let subst' =
                  List.fold_left (fun subst v ->
                        Fir_subst.subst_add_type_var subst v (TyVar (new_symbol_pre "spe" v))) subst_empty vars1
               in
               let ty_array = subst_type subst' ty_array in
                  unify ty_array subst
          | TyObject (v, ty_array) ->
               let subst' = Fir_subst.subst_add_type_var subst_empty v ty_array in
               let ty_array = subst_type subst' ty_array in
                  unify ty_array subst
          | _ ->
               raise (FirException (int_pos 2 pos, StringTypeError ("assertion needs a pointer", ty_array)))
   in
      match op with
         Reserve _ ->
            subst
       | IsMutable v
       | BoundsCheck (_, v, _, _) ->
            (* Check that these refer to an array *)
            let ty_array, subst = infer_atom genv bvars subst (int_pos 1 pos) v in
               unify ty_array subst
       | ElementCheck (ty, op, v, a) ->
            (* This is just like an array subscript *)
            let ty_array, subst = infer_atom genv bvars subst pos v in
            let subst = subscript_subscript genv bvars subst pos op false ty ty_array a in
               subscript_exp genv bvars subst e

(*
 * Debugging.
 *)
and subscript_debug_exp genv bvars subst pos info e =
   let pos = string_pos "subscript_debug_exp" pos in
      subscript_exp genv bvars subst e

(*
 * Migration.
 *)
and subscript_tail_sysmigrate_exp genv bvars subst pos i loc_ptr loc_off f args =
   let pos = string_pos "subscript_tail_sysmigrate_exp" pos in
      subst

(*
 * Rollback.
 *)
and subscript_tail_atomic_exp genv bvars subst pos f c args =
   let pos = string_pos "subscript_tail_atomic_exp" pos in
      subst

and subscript_tail_atomic_rollback_exp genv bvars subst pos level c =
   let pos = string_pos "subscript_tail_atomic_rollback_exp" pos in
      subst

and subscript_tail_atomic_commit_exp genv bvars subst pos level f args =
   let pos = string_pos "subscript_tail_atomic_commit_exp" pos in
      subst

(*
 * Type inference for a function declaration.
 *)
let subscript_fundef genv bvars subst f def =
   let pos = string_pos "subscript_fundef" (fundef_pos f def) in
   let _, ty_vars, ty, vars, e = def in
   let ty_args, ty_res = dest_fun_type genv pos ty in
   let genv =
      List.fold_left2 (fun genv v ty ->
            let ty = exists_elim_var genv subst pos v ty in
               genv_add_var genv v ty) genv vars ty_args
   in
      subscript_exp genv bvars subst e

(*
 * Type inference for the program.
 *)
let subscript_prog subst prog =
   let { prog_funs = funs } = prog in

   (* Add all the type arguments as polymorphic variables *)
   let bvars =
      SymbolTable.fold (fun bvars _ (_, ty_vars, _, _, _) ->
            bvars_add_vars bvars ty_vars) bvars_empty prog.prog_funs
   in
   let genv = genv_of_prog prog in
      (* Infer all the functions *)
      SymbolTable.fold (fun subst f def ->
            subscript_fundef genv bvars subst f def) subst funs

(************************************************************************
 * PROGRAM REWRITE
 ************************************************************************)

(*
 * This is the coercion to apply.
 *)
type coercion =
   CoerceNone
 | CoerceTyPack of ty * ty list
 | CoerceTyUnpack of ty
 | CoerceTyApply of ty * ty list
 | CoerceDTupleOfDTuple of ty_var * mutable_ty list
 | CoerceUnionOfUnion of ty_var * ty list * int_set * int_set
 | CoerceRawDataOfFrame of ty_var * ty list

(*
 * Coerce an atom to the desired type.
 *)
let coerce_type_aux genv bvars subst pos ty1 ty2 =
   let pos = string_pos "coerce_type_aux" pos in
   let ty1 = subst_expand genv subst pos ty1 in
   let ty2 = subst_expand genv subst pos ty2 in
      match ty1, ty2 with
         TyExists _, TyExists _ ->
            (* No coercion necessary *)
            let subst = unify_types_strict genv bvars subst pos ty1 ty2 in
               CoerceNone, subst

         (* Pack *)
       | TyExists (vars, ty1'), _ ->
            let ty_vars = List.map (fun v -> TyVar (new_symbol_pre "ctae" v)) vars in
            let ty1' = subst_type_simul pos ty1' vars ty_vars in
            let subst = unify_types_strict genv bvars subst pos ty1' ty2 in
               CoerceTyPack (ty1, ty_vars), subst

         (* Unpack *)
       | _, TyExists _ ->
            CoerceTyUnpack ty2, subst

         (* Apply *)
       | TyAll (vars1, ty1'), TyAll (vars2, ty2') ->
            let len1 = List.length vars1 in
            let len2 = List.length vars2 in
               if len2 > len1 then
                  let vars2, vars2' = Mc_list_util.split (len2 - len1) vars2 in
                  let ty_vars = List.map (fun v -> TyVar (new_symbol_pre "ctaa" v)) vars2 in
                  let ty2' = TyAll (vars2', subst_type_simul pos ty2' vars2 ty_vars) in
                  let subst = unify_types_strict genv bvars subst pos ty1 ty2' in
                     if debug debug_infer then
                        begin
                           Format.eprintf "@[<hv 3>coerce_type_aux: all case";
                           Format.eprintf "@ @[<hv 3>ty2' =@ %a@]" pp_print_type ty2';
                           Format.eprintf "@ @[<hv 3>ty1 =@ %a@]" pp_print_type ty1;
                           Format.eprintf "@ @[<hv 3>%a@]" pp_print_subst subst;
                           Format.eprintf "@]@."
                        end;
                     CoerceTyApply (ty1, ty_vars), subst
               else
                  let subst = unify_types_strict genv bvars subst pos ty1 ty2 in
                     CoerceNone, subst

         (* Apply *)
       | _, TyAll (vars, ty2') ->
            let ty_vars = List.map (fun v -> TyVar (new_symbol_pre "ctaa" v)) vars in
            let ty2' = subst_type_simul pos ty2' vars ty_vars in
            let subst = unify_types_strict genv bvars subst pos ty1 ty2' in
               if debug debug_infer then
                  begin
                     Format.eprintf "@[<hv 3>coerce_type_aux: all case";
                     Format.eprintf "@ @[<hv 3>ty2' =@ %a@]" pp_print_type ty2';
                     Format.eprintf "@ @[<hv 3>ty1 =@ %a@]" pp_print_type ty1;
                     Format.eprintf "@ @[<hv 3>%a@]" pp_print_subst subst;
                     Format.eprintf "@]@."
                  end;
               CoerceTyApply (ty1, ty_vars), subst

         (* DTuples *)
       | TyDTuple (_, None), TyDTuple (ty_var, Some ty_fields) ->
            CoerceDTupleOfDTuple (ty_var, ty_fields), subst

         (* Unions have to be coerced if the set changes *)
       | TyUnion (ty_var, tyl, s1), TyUnion (_, _, s2) when not (IntSet.equal s1 s2) ->
            CoerceUnionOfUnion (ty_var, tyl, s1, s2), subst

         (* Frames to rawdata *)
       | TyRawData, TyFrame (ty_var, tyl) ->
            CoerceRawDataOfFrame (ty_var, tyl), subst

         (* No coercions for the rest *)
       | _ ->
            let subst = unify_types_strict genv bvars subst pos ty1 ty2 in
               CoerceNone, subst

let coerce_atom_aux genv pos op a =
   let pos = string_pos "coerce_atom_aux" pos in
      match op with
         CoerceNone ->
            a
       | CoerceTyPack (ty, tyl) ->
            let v = var_of_fun pos a in
               AtomTyPack (v, ty, tyl)
       | CoerceTyUnpack _ ->
            let v = var_of_fun pos a in
               AtomTyUnpack v
       | CoerceTyApply (ty, tyl) ->
            let v = var_of_fun pos a in
            let a =
               if genv_mem_fun genv v then
                  AtomFun v
               else
                  AtomVar v
            in
               AtomTyApply (a, ty, tyl)
       | CoerceDTupleOfDTuple (ty_var, ty_fields) ->
            AtomUnop (DTupleOfDTupleOp (ty_var, ty_fields), a)
       | CoerceUnionOfUnion (ty_var, tyl, s1, s2) ->
            AtomUnop (UnionOfUnionOp (ty_var, tyl, s1, s2), a)
       | CoerceRawDataOfFrame (ty_var, tyl) ->
            AtomUnop (RawDataOfFrameOp (ty_var, tyl), a)

let coerce_var_aux genv pos loc op v subst cont =
   let pos = string_pos "coerce_var_aux" pos in
      match op with
         CoerceNone ->
            cont v subst
       | CoerceTyPack (ty, tyl) ->
            let e, subst = cont v subst in
            let e = LetAtom (v, ty, AtomTyPack (v, ty, tyl), e) in
               make_exp loc e, subst
       | CoerceTyUnpack ty ->
            let e, subst = cont v subst in
            let ty = exists_elim_var genv  subst pos v ty in
            let e = LetAtom (v, ty, AtomTyUnpack v, e) in
               make_exp loc e, subst
       | CoerceTyApply (ty, tyl) ->
            let e, subst = cont v subst in
            let a =
               if genv_mem_fun genv v then
                  AtomFun v
               else
                  AtomVar v
            in
            let e = LetAtom (v, ty, AtomTyApply (a, ty, tyl), e) in
               make_exp loc e, subst
       | CoerceDTupleOfDTuple (ty_var, ty_fields) ->
            let e, subst = cont v subst in
            let e = LetAtom (v, TyDTuple (ty_var, None), AtomUnop (DTupleOfDTupleOp (ty_var, ty_fields), AtomVar v), e) in
               make_exp loc e, subst
       | CoerceUnionOfUnion (ty_var, tyl, s1, s2) ->
            let e, subst = cont v subst in
            let e = LetAtom (v, TyUnion (ty_var, tyl, s1), AtomUnop (UnionOfUnionOp (ty_var, tyl, s1, s2), AtomVar v), e) in
               make_exp loc e, subst
       | CoerceRawDataOfFrame (ty_var, tyl) ->
            let e, subst = cont v subst in
            let e = LetAtom (v, TyRawData, AtomUnop (RawDataOfFrameOp (ty_var, tyl), AtomVar v), e) in
               make_exp loc e, subst

(*
 * Coerce a variable to the given type.
 *)
let coerce_var genv bvars subst pos loc ty1 ty2 v cont =
   let pos = string_pos "coerce_var" pos in
   let ty1 = subst_expand genv subst pos ty1 in
   let ty2 = subst_expand genv subst pos ty2 in
   let op, subst = coerce_type_aux genv bvars subst pos ty1 ty2 in
      coerce_var_aux genv pos loc op v subst cont

(*
 * Coerce an atom to the given type.
 *)
let coerce_type genv bvars subst pos loc ty1 ty2 a =
   let pos = string_pos "coerce_type" pos in
   let ty1 = subst_expand genv subst pos ty1 in
   let ty2 = subst_expand genv subst pos ty2 in
   let op, subst = coerce_type_aux genv bvars subst pos ty1 ty2 in
   let a' = coerce_atom_aux genv pos op a in
      if debug debug_infer then
         begin
            Format.eprintf "@[<v 3>coerce_type:";
            Format.eprintf "@ @[<hv 3>ty_dst =@ %a;@]" pp_print_type ty1;
            Format.eprintf "@ @[<hv 3>ty_src =@ %a;@]" pp_print_type ty2;
            Format.eprintf "@ @[<hv 3>atom_dst =@ %a;@]" pp_print_atom a';
            Format.eprintf "@ @[<hv 3>atom_src =@ %a;@]" pp_print_atom a;
            Format.eprintf "@]@."
         end;
      a', subst

let coerce_type_list2 genv bvars subst pos loc ty1 tyl2 args =
   let pos = string_pos "coerce_type_list2" pos in
   let len2 = List.length tyl2 in
   let len3 = List.length args in
   let _ =
      if len2 <> len3 then
         raise (FirException (pos, ArityMismatch (len2, len3)))
   in
   let rec coerce args' subst tyl2 args =
      match tyl2, args with
         ty2 :: tyl2, a :: args ->
            let a, subst = coerce_type genv bvars subst pos loc ty1 ty2 a in
               coerce (a :: args') subst tyl2 args
       | [], [] ->
            List.rev args', subst
       | _ ->
            raise (Invalid_argument "coerce_type_list2")
   in
      coerce [] subst tyl2 args

let coerce_type_list3 genv bvars subst pos loc tyl1 tyl2 args =
   let pos = string_pos "coerce_type_list3" pos in
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
   let len3 = List.length args in
   let _ =
      if len1 <> len3 then
         raise (FirException (pos, ArityMismatch (len1, len3)));
      if len2 <> len3 then
         raise (FirException (pos, ArityMismatch (len2, len3)))
   in
   let rec coerce args' subst tyl1 tyl2 args =
      match tyl1, tyl2, args with
         ty1 :: tyl1, ty2 :: tyl2, a :: args ->
            let a, subst = coerce_type genv bvars subst pos loc ty1 ty2 a in
               coerce (a :: args') subst tyl1 tyl2 args
       | [], [], [] ->
            List.rev args', subst
       | _ ->
            raise (Invalid_argument "coerce_type_list3")
   in
      coerce [] subst tyl1 tyl2 args

(*
 * Coerce the type of an atom.
 *)
let rec coerce_atom genv bvars subst pos loc a cont =
   let pos = string_pos "coerce_atom" pos in
      match a with
         AtomNil ty ->
            cont ty a subst
       | AtomInt _ ->
            cont TyInt a subst
       | AtomEnum (n, _) ->
            cont (TyEnum n) a subst
       | AtomFloat x ->
            cont (TyFloat (Rawfloat.precision x)) a subst
       | AtomRawInt i ->
            cont (TyRawInt (Rawint.precision i, Rawint.signed i)) a subst
       | AtomLabel _
       | AtomSizeof _ ->
            cont ty_subscript a subst
       | AtomConst (ty, _, _) ->
            cont ty a subst
       | AtomUnop (op, a) ->
            let ty_dst, ty_src = type_of_unop pos op in
               coerce_atom genv bvars subst pos loc a (fun ty_atom a subst ->
                     let a, subst = coerce_type genv bvars subst pos loc ty_src ty_atom a in
                        cont ty_dst (AtomUnop (op, a)) subst)
       | AtomBinop (op, a1, a2) ->
            coerce_atom genv bvars subst pos loc a1 (fun ty_atom1 a1 subst ->
            coerce_atom genv bvars subst pos loc a2 (fun ty_atom2 a2 subst ->
                  let ty_dst, ty_src1, ty_src2 = type_of_binop pos op in
                  let a1, subst = coerce_type genv bvars subst pos loc ty_src1 ty_atom1 a1 in
                  let a2, subst = coerce_type genv bvars subst pos loc ty_src2 ty_atom2 a2 in
                     cont ty_dst (AtomBinop (op, a1, a2)) subst))

         (*
          * Ignore polymorphic coercions.  They will get re-added
          * by coerce_type.
          *)
       | AtomTyApply (a, _, _) ->
            coerce_atom genv bvars subst pos loc a cont
       | AtomVar v
       | AtomFun v
       | AtomTyPack (v, _, _)
       | AtomTyUnpack v ->
            if genv_mem_global genv v then
               let ty = genv_lookup_global genv pos v in
               let e, subst = cont ty (AtomVar v) subst in
               let subop = sub_value_of_type genv pos ty in
               let e = LetGlobal (subop, v, ty, v, e) in
                  make_exp loc e, subst
            else if genv_mem_fun genv v then
               let ty = genv_lookup_fun genv pos v in
                  cont ty (AtomFun v) subst
            else
               let ty = genv_lookup_var genv pos v in
                  cont ty (AtomVar v) subst

let coerce_atom_opt genv bvars subst pos loc a cont =
   let pos = string_pos "coerce_atom_opt" pos in
      match a with
         Some a ->
            coerce_atom genv bvars subst pos loc a (fun ty a subst ->
                  cont (Some (ty, a)) subst)
       | None ->
            cont None subst

(*
 * Argument list.
 *)
let coerce_atom_list genv bvars subst pos loc args cont =
   let pos = string_pos "coerce_atom_list" pos in
   let rec collect ty_args args subst args' =
      match args' with
         a :: args' ->
            coerce_atom genv bvars subst pos loc a (fun ty a subst ->
                  collect (ty :: ty_args) (a :: args) subst args')
       | [] ->
            cont (List.rev ty_args) (List.rev args) subst
   in
      collect [] [] subst args

let coerce_atom_opt_list genv bvars subst pos loc args cont =
   let pos = string_pos "coerce_atom_opt_list" pos in
   let rec collect args subst args' =
      match args' with
         a :: args' ->
            coerce_atom_opt genv bvars subst pos loc a (fun a subst ->
                  collect (a :: args) subst args')
       | [] ->
            cont (List.rev args) subst
   in
      collect [] subst args

(*
 * Application.
 *)
let coerce_apply genv bvars subst pos loc ty_f args cont =
   let pos = string_pos "coerce_apply" pos in
      coerce_atom_list genv bvars subst pos loc args (fun ty_args args subst ->
            (* Coerce the args to their correct type *)
            let ty_f = subst_expand genv subst pos ty_f in
            let ty_args', ty_res' = poly_fun_type genv pos ty_f in
            let args, subst =
               Mc_list_util.fold_left3 (fun (args, subst) a ty_arg' ty_arg ->
                     let a, subst = coerce_type genv bvars subst pos loc ty_arg' ty_arg a in
                     let args = a :: args in
                        args, subst) ([], subst) args ty_args' ty_args
            in
            let args = List.rev args in
               (* Coerce the function to the right type *)
               cont (TyFun (ty_args', ty_res')) args ty_res' subst)

let coerce_apply_opt genv bvars subst pos loc ty_f args cont =
   let pos = string_pos "coerce_apply_opt" pos in
      coerce_atom_opt_list genv bvars subst pos loc args (fun args subst ->
            (* Coerce the args to their correct type *)
            let ty_f = subst_expand genv subst pos ty_f in
            let ty_args', ty_res' = poly_fun_type genv pos ty_f in
            let args, subst =
               List.fold_left2 (fun (args, subst) ty_arg' a ->
                     match a with
                        Some (ty_arg, a) ->
                           let a, subst = coerce_type genv bvars subst pos loc ty_arg' ty_arg a in
                           let args = Some a :: args in
                              if debug debug_infer then
                                 begin
                                    Format.eprintf "@[<hv 3>coerce_apply_opt:";
                                    Format.eprintf "@ @[<hv 3>ty_arg' =@ %a@]" pp_print_type ty_arg';
                                    Format.eprintf "@ @[<hv 3>ty_arg =@ %a@]" pp_print_type ty_arg;
                                    Format.eprintf "@ @[<hv 3>a =@ %a@]" pp_print_atom a;
                                    Format.eprintf "@ %a" pp_print_subst subst;
                                    Format.eprintf "@]@."
                                 end;
                              args, subst
                      | None ->
                           let args = None :: args in
                              args, subst) ([], subst) ty_args' args
            in
            let args = List.rev args in
               (* Coerce the function to the right type *)
               cont (TyFun (ty_args', ty_res')) args ty_res' subst)

(*
 * Figure out the type of the allocation.
 *)
let coerce_alloc_tuple genv bvars subst pos loc tc ty_vars ty_tuple args cont =
   let pos = string_pos "coerce_alloc_tuple" pos in
   let bvars = bvars_add_vars bvars ty_vars in
   let ty_args = dest_tuple_type genv subst pos ty_tuple in
   let ty_args = List.map fst ty_args in
      coerce_atom_list genv bvars subst pos loc args (fun ty_args' args subst ->
            let args, subst = coerce_type_list3 genv bvars subst pos loc ty_args ty_args' args in
               cont (TyAll (ty_vars, ty_tuple)) (AllocTuple (tc, ty_vars, ty_tuple, args)) subst)

let coerce_alloc_dtuple genv bvars subst pos loc ty_dtuple ty_var a args cont =
   let pos = string_pos "coerce_alloc_dtuple" pos in
   let _, ty_args = dest_dtuple_type genv subst pos ty_dtuple in
      coerce_atom_list genv bvars subst pos loc args (fun ty_args' args subst ->
            let args, subst =
               match ty_args with
                  Some ty_args ->
                     coerce_type_list3 genv bvars subst pos loc (List.map fst ty_args) ty_args' args
                | None ->
                     args, subst
            in
               cont ty_dtuple (AllocDTuple (ty_dtuple, ty_var, a, args)) subst)

let coerce_alloc_union genv bvars subst pos loc ty_vars ty_union ty_v i args cont =
   let pos = string_pos "coerce_alloc_union" pos in
   let bvars = bvars_add_vars bvars ty_vars in
   let _, ty_args, _ = dest_union_type genv subst pos ty_union in
   let ty_args = apply_union_fields genv pos ty_v ty_args i in
   let ty_args = List.map fst ty_args in
      coerce_atom_list genv bvars subst pos loc args (fun ty_args' args subst ->
            let args, subst = coerce_type_list3 genv bvars subst pos loc ty_args ty_args' args in
               cont (TyAll (ty_vars, ty_union)) (AllocUnion (ty_vars, ty_union, ty_v, i, args)) subst)

let coerce_alloc_array genv bvars subst pos loc ty args cont =
   let pos = string_pos "coerce_alloc_array" pos in
   let ty' = dest_array_type genv subst pos ty in
      coerce_atom_list genv bvars subst pos loc args (fun ty_args' args subst ->
            let args, subst = coerce_type_list2 genv bvars subst pos loc ty' ty_args' args in
               cont ty (AllocArray (ty, args)) subst)

let coerce_alloc_varray genv bvars subst pos loc ty subop a_len a_init cont =
   let pos = string_pos "coerce_alloc_varray" pos in
   let ty' = dest_array_type genv subst pos ty in
      coerce_atom genv bvars subst pos loc a_len (fun ty_len a_len subst ->
      coerce_atom genv bvars subst pos loc a_init (fun ty_init a_init subst ->
            let a_init, subst = coerce_type genv bvars subst pos loc ty' ty_init a_init in
               cont ty (AllocVArray (ty, subop, a_len, a_init)) subst))

let coerce_alloc_malloc genv bvars subst pos loc ty a cont =
   let pos = string_pos "coerce_alloc_malloc" pos in
      coerce_atom genv bvars subst pos loc a (fun ty_a a subst ->
            let a, subst = coerce_type genv bvars subst pos loc ty_subscript ty_a a in
               cont ty (AllocMalloc (ty, a)) subst)

let coerce_alloc_frame genv bvars subst pos loc v tyl cont =
   cont (TyFrame (v, tyl)) (AllocFrame (v, tyl)) subst

let coerce_alloc_op genv bvars subst pos loc op cont =
   let pos = string_pos "coerce_alloc_op" pos in
      match op with
         AllocTuple (tc, ty, ty_vars, args) ->
            coerce_alloc_tuple genv bvars subst pos loc tc ty ty_vars args cont
       | AllocDTuple (ty, ty_var, a, args) ->
            coerce_alloc_dtuple genv bvars subst pos loc ty ty_var a args cont
       | AllocUnion (ty, ty_vars, tv, i, args) ->
            coerce_alloc_union genv bvars subst pos loc ty ty_vars tv i args cont
       | AllocArray (ty, args) ->
            coerce_alloc_array genv bvars subst pos loc ty args cont
       | AllocVArray (ty, subop, a_len, a_init) ->
            coerce_alloc_varray genv bvars subst pos loc ty subop a_len a_init cont
       | AllocMalloc (ty, a) ->
            coerce_alloc_malloc genv bvars subst pos loc ty a cont
       | AllocFrame (v, tyl) ->
            coerce_alloc_frame genv bvars subst pos loc v tyl cont

(*
 * Figure out the type of the subscript.
 *)
let coerce_subscript genv bvars subst pos loc op ty_elem a_array a_index cont =
   let pos = string_pos "coerce_subscript" pos in
      coerce_atom genv bvars subst pos loc a_array (fun ty_array a_array subst ->
      coerce_atom genv bvars subst pos loc a_index (fun ty_index a_index subst ->
            let { sub_index = sub_index;
                  sub_value = sub_value;
                  sub_script = sub_script
                } = op
            in

            (* Make sure the index has the right type *)
            let ty_index' =
               match sub_script with
                  IntIndex ->
                     TyInt
                | RawIntIndex (pre, signed) ->
                     TyRawInt (Rawint.Int32, true)
            in
            let subst = unify_types genv bvars subst pos ty_index' ty_index in

            (* Get constant index *)
            let i =
               match a_index with
                  AtomInt i
                | AtomEnum (_, i) ->
                     IntSubscript (word_index sub_index i)
                | AtomRawInt i ->
                     IntSubscript (word_index sub_index (Rawint.to_int i))
                | AtomLabel (label, i) ->
                     LabelSubscript (label, i)
                | _ ->
                     NoSubscript
            in

            (* Figure out the element type *)
            let rec coerce ty_array a_array =
               let pos = error_pos (StringTypeError ("coerce_subscript", ty_array)) pos in
               let ty_array = subst_expand genv subst pos ty_array in
                  match ty_array, i with
                     TyArray ty_elem', _ ->
                        cont ty_elem a_array a_index subst
                   | TyTuple (_, tyl), IntSubscript i
                   | TyDTuple (_, Some tyl), IntSubscript i ->
                        let len = List.length tyl in
                        let i = word_index sub_index i in
                        let _ =
                           if i < 0 || i >= len then
                              raise (FirException (pos, ArityMismatch (len, i)))
                        in
                        let ty_elem', _ = List.nth tyl i in
                           cont ty_elem' a_array a_index subst
                   | TyUnion (ty_v, ty_args, j), IntSubscript i ->
                        let j = specialize_union_count genv pos ty_v j in
                        let tyl = apply_union_fields genv pos ty_v ty_args j in
                        let len = List.length tyl in
                        let _ =
                           if i < 0 || i >= len then
                              raise (FirException (pos, ArityMismatch (len, j)))
                        in
                        let ty_elem', _ = List.nth tyl i in
                           cont ty_elem' a_array a_index subst
                   | TyFrame (ty_v, ty_args), LabelSubscript (label, i) ->
                        let ty_elem', i' = apply_frame genv pos ty_v ty_args label in
                        let ty_elem' =
                           if Rawint.is_zero i then
                              ty_elem'
                           else
                              TyRawData
                        in
                        let ty_elem' =
                           if is_rawdata_type genv subst pos ty_elem' then
                              ty_elem
                           else
                              ty_elem'
                        in
                           cont ty_elem' a_array a_index subst
                   | TyExists (vars1, ty_array), _ ->
                        let subst' =
                           List.fold_left (fun subst v ->
                                 Fir_subst.subst_add_type_var subst v (TyVar (new_symbol_pre "is" v))) subst_empty vars1
                        in
                        let ty_array = subst_type subst' ty_array in
                        let v_array = var_of_atom pos a_array in
                           coerce ty_array (AtomTyUnpack v_array)
                   | TyObject (v, ty_array), _ ->
                        let subst' = subst_add_type_var bvars subst_empty pos v ty_array in
                        let ty_array = subst_type subst' ty_array in
                           coerce ty_array a_array
                   | TyPointer _, _
                   | TyRawData, _ ->
                        cont ty_elem a_array a_index subst
                   | TyFrame (ty_var, tyl), _ ->
                        cont ty_elem (AtomUnop (RawDataOfFrameOp (ty_var, tyl), a_array)) a_index subst
                   | _ ->
                        raise (FirException (int_pos 12 pos, SubscriptError (ty_array, ty_index)))
            in
               coerce ty_array a_array))

(*
 * Next, rewrite all the types, and include any explicit coercions.
 *)
let rec coerce_exp genv bvars subst e =
   let pos = string_pos "coerce_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            coerce_atom_exp genv bvars subst pos loc v ty a e
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            coerce_ext_exp genv bvars subst pos loc v ty1 s b ty2 ty_args args e
       | TailCall (label, f, args) ->
            coerce_tailcall_exp genv bvars subst pos loc label f args
       | SpecialCall (label, op) ->
            coerce_specialcall_exp genv bvars subst pos loc label op
       | Match (a, cases) ->
            coerce_match_exp genv bvars subst pos loc a cases
       | MatchDTuple (a, cases) ->
            coerce_match_dtuple_exp genv bvars subst pos loc a cases
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            coerce_typecase_exp genv bvars subst pos loc a1 a2 v1 v2 e1 e2
       | LetAlloc (v, op, e) ->
            coerce_alloc_exp genv bvars subst pos loc v op e
       | LetSubscript (op, v1, ty, v2, a3, e) ->
            coerce_subscript_exp genv bvars subst pos loc op v1 ty v2 a3 e
       | SetSubscript (op, label, v1, a2, ty, a3, e) ->
            coerce_set_subscript_exp genv bvars subst pos loc op label v1 a2 ty a3 e
       | LetGlobal (op, v, ty, l, e) ->
            coerce_global_exp genv bvars subst pos loc op v ty l e
       | SetGlobal (op, label, v, ty, a, e) ->
            coerce_set_global_exp genv bvars subst pos loc op label v ty a e
       | Memcpy (op, label, v1, a2, v3, a4, a5, e) ->
            coerce_memcpy_exp genv bvars subst pos loc op label v1 a2 v3 a4 a5 e
       | Call (label, f, args, e) ->
            coerce_call_exp genv bvars subst pos loc label f args e
       | Assert (label, pred, e) ->
            coerce_assert_exp genv bvars subst pos loc label pred e
       | Debug (info, e) ->
            coerce_debug_exp genv bvars subst pos loc info e

(*
 * Coerce an atom.
 *)
and coerce_atom_exp genv bvars subst pos loc v ty a e =
   let pos = string_pos "coerce_atom_exp" pos in
      coerce_atom genv bvars subst pos loc a (fun ty_atom a subst ->
            let a, subst = coerce_type genv bvars subst pos loc ty ty_atom a in
            let genv = genv_add_var genv v ty in
            let e, subst = coerce_exp genv bvars subst e in
            let e = LetAtom (v, ty, a, e) in
               make_exp loc e, subst)

(*
 * Function calls.
 *)
and coerce_ext_exp genv bvars subst pos loc v ty1 s b ty2 ty_args args e =
   let pos = string_pos "coerce_ext_exp" pos in
   let ty2' = apply_all_type genv pos ty2 ty_args in
      coerce_apply genv bvars subst pos loc ty2' args (fun ty2' args ty_res subst ->
            let e, subst =
               coerce_var genv bvars subst pos loc ty1 ty_res v (fun v subst ->
                     let genv = genv_add_var genv v ty1 in
                        coerce_exp genv bvars subst e)
            in
            let e = LetExt (v, ty_res, s, b, ty2, ty_args, args, e) in
               make_exp loc e, subst)

and coerce_tailcall_exp genv bvars subst pos loc label f args =
   let pos = string_pos "coerce_tailcall_exp" pos in
      coerce_atom genv bvars subst pos loc f (fun ty_fun f subst ->
      coerce_apply genv bvars subst pos loc ty_fun args (fun ty_fun' args _ subst ->
            let f, subst = coerce_type genv bvars subst pos loc ty_fun' ty_fun f in
            let e = TailCall (label, f, args) in
               make_exp loc e, subst))

and coerce_tail_sysmigrate_exp genv bvars subst pos loc label id dst_bas dst_off f args =
   let pos = string_pos "coerce_tail_sysmigrate_exp" pos in
      coerce_atom genv bvars subst pos loc dst_bas (fun ty_dst_bas dst_bas subst ->
      coerce_atom genv bvars subst pos loc dst_off (fun ty_dst_off dst_off subst ->
      coerce_atom genv bvars subst pos loc f (fun ty_fun f subst ->
      coerce_apply genv bvars subst pos loc ty_fun args (fun ty_fun' args _ subst ->
            let subst = unify_types genv bvars subst pos TyRawData ty_dst_bas in
            let subst = unify_types genv bvars subst pos ty_int32 ty_dst_off in
            let f, subst = coerce_type genv bvars subst pos loc ty_fun' ty_fun f in
            let e = SpecialCall (label, TailSysMigrate (id, dst_bas, dst_off, f, args)) in
               make_exp loc e, subst))))

and coerce_tail_atomic_exp genv bvars subst pos loc label f c args =
   let pos = string_pos "coerce_tail_atomic_exp" pos in
      coerce_atom genv bvars subst pos loc f (fun ty_fun f subst ->
      coerce_apply genv bvars subst pos loc ty_fun (c :: args) (fun ty_fun' args _ subst ->
            let c, args =
               match args with
                  c :: args ->
                     c, args
                | [] ->
                     raise (FirException (pos, InternalError "Lost the c argument to the function somehow"))
            in
            let f, subst = coerce_type genv bvars subst pos loc ty_fun' ty_fun f in
            let e = SpecialCall (label, TailAtomic (f, c, args)) in
               make_exp loc e, subst))

and coerce_tail_atomicrollback_exp genv bvars subst pos loc label level c =
   let pos = string_pos "coerce_tail_atomicrollback_exp" pos in
      coerce_atom genv bvars subst pos loc level (fun ty_level level subst ->
      coerce_atom genv bvars subst pos loc c (fun ty_c c subst ->
            let subst = unify_types genv bvars subst pos ty_int32 ty_level in
            let subst = unify_types genv bvars subst pos ty_int32 ty_c in
            let e = SpecialCall (label, TailAtomicRollback (level, c)) in
               make_exp loc e, subst))

and coerce_tail_atomiccommit_exp genv bvars subst pos loc label level f args =
   let pos = string_pos "coerce_tail_atomiccommit_exp" pos in
      coerce_atom genv bvars subst pos loc level (fun ty_level level subst ->
      coerce_atom genv bvars subst pos loc f (fun ty_fun f subst ->
      coerce_apply genv bvars subst pos loc ty_fun args (fun ty_fun' args _ subst ->
            let subst = unify_types genv bvars subst pos ty_int32 ty_level in
            let f, subst = coerce_type genv bvars subst pos loc ty_fun' ty_fun f in
            let e = SpecialCall (label, TailAtomicCommit (level, f, args)) in
               make_exp loc e, subst)))

and coerce_specialcall_exp genv bvars subst pos loc label op =
   let pos = string_pos "coerce_specialcall_exp" pos in
      match op with
         TailSysMigrate (id, dst_bas, dst_off, f, args) ->
            coerce_tail_sysmigrate_exp genv bvars subst pos loc label id dst_bas dst_off f args
       | TailAtomic (f, c, args) ->
            coerce_tail_atomic_exp genv bvars subst pos loc label f c args
       | TailAtomicRollback (level, c) ->
            coerce_tail_atomicrollback_exp genv bvars subst pos loc label level c
       | TailAtomicCommit (level, f, args) ->
            coerce_tail_atomiccommit_exp genv bvars subst pos loc label level f args

and coerce_call_exp genv bvars subst pos loc label f args e =
   let pos = string_pos "coerce_call_exp" pos in
      coerce_atom genv bvars subst pos loc f (fun ty_fun f subst ->
      coerce_apply_opt genv bvars subst pos loc ty_fun args (fun ty_fun' args _ subst ->
            let f, subst = coerce_type genv bvars subst pos loc ty_fun' ty_fun f in
            let e, subst = coerce_exp genv bvars subst e in
            let e = Call (label, f, args, e) in
               make_exp loc e, subst))

(*
 * Pattern match.
 *)
and coerce_match_exp genv bvars subst pos loc a cases =
   let pos = string_pos "coerce_match_exp" pos in
      coerce_atom genv bvars subst pos loc a (fun _ a subst ->
            let ty = subst_type subst (type_of_atom genv pos a) in
            let ty = expand_type genv pos ty in
            let cases, subst =
               List.fold_left (fun (cases, subst) (label, s, e) ->
                     let s2 =
                        match s with
                           IntSet s ->
                              s
                         | RawIntSet s ->
                              int_set_of_raw_int_set s
                     in
                     let genv =
                        match a, ty with
                           AtomVar v, TyUnion (ty_var, tyl, s1) ->
                              genv_add_var genv v (TyUnion (ty_var, tyl, IntSet.isect s1 s2))
                         | _ ->
                              genv
                     in
                     let e, subst = coerce_exp genv bvars subst e in
                     let cases = (label, s, e) :: cases in
                        cases, subst) ([], subst) cases
            in
            let e = Match (a, List.rev cases) in
               make_exp loc e, subst)

and coerce_match_dtuple_exp genv bvars subst pos loc a cases =
   let pos = string_pos "coerce_match_dtuple_exp" pos in
      coerce_atom genv bvars subst pos loc a (fun _ a subst ->
            let cases, subst =
               List.fold_left (fun (cases, subst) (label, a_opt, e) ->
                     let e, subst = coerce_exp genv bvars subst e in
                     let cases = (label, a_opt, e) :: cases in
                        cases, subst) ([], subst) cases
            in
            let e = MatchDTuple (a, List.rev cases) in
               make_exp loc e, subst)

(*
 * Typecase.
 *)
and coerce_typecase_exp genv bvars subst pos loc a1 a2 v1 v2 e1 e2 =
   let pos = string_pos "coerce_typecase_exp" pos in
      coerce_atom genv bvars subst pos loc a1 (fun _ a1 subst ->
      coerce_atom genv bvars subst pos loc a2 (fun _ a2 subst ->
            let e1, subst = coerce_exp genv bvars subst e1 in
            let e2, subst = coerce_exp genv bvars subst e2 in
            let e = TypeCase (a1, a2, v1, v2, e1, e2) in
               make_exp loc e, subst))

(*
 * Allocation.
 *)
and coerce_alloc_exp genv bvars subst pos loc v op e =
   let pos = string_pos "coerce_alloc_exp" pos in
      coerce_alloc_op genv bvars subst pos loc op (fun ty op subst ->
            let genv = genv_add_var genv v ty in
            let e, subst = coerce_exp genv bvars subst e in
            let e = LetAlloc (v, op, e) in
               make_exp loc e, subst)

(*
 * Subscripting.
 *)
and coerce_subscript_exp genv bvars subst pos loc op v1 ty a2 a3 e =
   let pos = string_pos "coerce_subscript_exp" pos in
      coerce_subscript genv bvars subst pos loc op ty a2 a3 (fun ty_elem a2 a3 subst ->
            let e, subst =
               coerce_var genv bvars subst pos loc ty ty_elem v1 (fun v1 subst ->
                     let genv = genv_add_var genv v1 ty in
                        coerce_exp genv bvars subst e)
            in
            let e = LetSubscript (op, v1, ty_elem, a2, a3, e) in
               make_exp loc e, subst)

and coerce_set_subscript_exp genv bvars subst pos loc op label a1 a2 ty a3 e =
   let pos = string_pos "coerce_set_subscript_exp" pos in
      coerce_subscript genv bvars subst pos loc op ty a1 a2 (fun ty_elem a1 a2 subst ->
      coerce_atom genv bvars subst pos loc a3 (fun ty_elem' a3 subst ->
            let a3, subst = coerce_type genv bvars subst pos loc ty ty_elem' a3 in
            let a3, subst = coerce_type genv bvars subst pos loc ty_elem ty a3 in
            let e, subst = coerce_exp genv bvars subst e in
            let e = SetSubscript (op, label, a1, a2, ty_elem, a3, e) in
               make_exp loc e, subst))

and coerce_memcpy_exp genv bvars subst pos loc op label a1 a2 a3 a4 a5 e =
   let pos = string_pos "coerce_memcpy_exp" pos in
      coerce_subscript genv bvars subst pos loc op ty_int8 a1 a2 (fun ty_elem1 a1 a2 subst ->
      coerce_subscript genv bvars subst pos loc op ty_int8 a3 a4 (fun ty_elem2 a3 a4 subst ->
      coerce_atom genv bvars subst pos loc a5 (fun ty_len a5 subst ->
            let a5, subst = coerce_type genv bvars subst pos loc ty_int32 ty_len a5 in
            let e, subst = coerce_exp genv bvars subst e in
            let e = Memcpy (op, label, a1, a2, a3, a4, a5, e) in
               make_exp loc e, subst)))

and coerce_global_exp genv bvars subst pos loc op v ty l e =
   let pos = string_pos "coerce_global_exp" pos in
   let genv = genv_add_var genv v ty in
   let e, subst = coerce_exp genv bvars subst e in
   let e = LetGlobal (op, v, ty, l, e) in
      make_exp loc e, subst

and coerce_set_global_exp genv bvars subst pos loc op label v ty a e =
   let pos = string_pos "coerce_set_global_exp" pos in
   let e, subst = coerce_exp genv bvars subst e in
   let e = SetGlobal (op, label, v, ty, a, e) in
      make_exp loc e, subst

(*
 * Assertions.
 *)
and coerce_assert_exp genv bvars subst pos loc label pred e =
   let pos = string_pos "coerce_assert_exp" pos in
   let e, subst = coerce_exp genv bvars subst e in
   let e = Assert (label, pred, e) in
      make_exp loc e, subst

(*
 * Debugging.
 *)
and coerce_debug_exp genv bvars subst pos loc info e =
   let pos = string_pos "coerce_debug_exp" pos in
   let e, subst = coerce_exp genv bvars subst e in
   let e = Debug (info, e) in
      make_exp loc e, subst

(*
 * Coerce the program.
 *)
let coerce_prog subst prog =
   let { prog_funs = funs } = prog in

   (* Add all the type parameters as polymorphic *)
   let genv = genv_of_prog prog in
   let bvars =
      SymbolTable.fold (fun bvars _ (_, ty_vars, _, _, _) ->
            bvars_add_vars bvars ty_vars) bvars_empty prog.prog_funs
   in

   (* Add coercions to all the expressions *)
   let subst, funs =
      SymbolTable.fold_map (fun subst f def ->
            let pos = string_pos "coerce_prog" (fundef_pos f def) in
            let info, ty_vars, ty, vars, e = def in
            let ty_args, ty_res = dest_fun_type genv pos ty in
            let genv = List.fold_left2 genv_add_var genv vars ty_args in
            let e, subst = coerce_exp genv bvars subst e in
            let def = info, ty_vars, ty, vars, e in
               subst, def) subst funs
   in
   let prog = { prog with prog_funs = funs } in
      prog, subst

(************************************************************************
 * TYPE CLOSURE
 ************************************************************************)

(*
 * Close the program by instantiating all free type variables with
 * ty_void.
 *)
let close_prog prog =
   let { prog_funs = funs } = prog in
   let funs =
      SymbolTable.map (fun (info, ty_vars, ty, vars, e) ->
            let fvars = SymbolSet.empty in
            let fvars = free_vars_type fvars ty in
            let fvars = free_vars_exp_type fvars e in
            let fvars = List.fold_left SymbolSet.remove fvars ty_vars in
            let ty, e =
               if SymbolSet.is_empty fvars then
                  ty, e
               else
                  let subst =
                     SymbolSet.fold (fun subst v ->
                           Fir_subst.subst_add_type_var subst v ty_void) subst_empty fvars
                  in
                     subst_type subst ty, subst_type_exp subst e
            in
               info, ty_vars, ty, vars, e) funs
   in
      { prog with prog_funs = funs }

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Type inference.
 *    1. Replace all TyDelayed with TyVar v (new v)
 *    2. Apply first pass of type inference.
 *       This will infer all types except for subscripts.
 *    3. Infer types for subscripts.
 *    4. Apply type substitution
 *)
let infer_prog prog =
   let _ =
      if debug Fir_state.debug_infer then
         Fir_print.debug_prog "*** Fir_infer.infer_prog: begin" prog
   in
   let prog = map_type_prog new_type prog in
   let _ =
      if debug Fir_state.debug_infer then
         Fir_print.debug_prog "*** Fir_infer.infer_prog: new_types" prog
   in
   let subst = infer_prog prog in
   let _ =
      if debug Fir_state.debug_infer then
         begin
            Fir_print.debug_prog "*** Fir_infer.infer_prog: infer_prog" prog;
            Format.eprintf "%a@." pp_print_subst subst
         end
   in
   let subst = subscript_prog subst prog in
   let _ =
      if debug Fir_state.debug_infer then
         begin
            Fir_print.debug_prog "*** Fir_infer.infer_prog: subscript_prog" prog;
            Format.eprintf "%a@." pp_print_subst subst
         end
   in
   let prog, subst = coerce_prog subst prog in
   let _ =
      if debug Fir_state.debug_infer then
         begin
            Fir_print.debug_prog "*** Fir_infer.infer_prog: coerce_prog" prog;
            Format.eprintf "%a@." pp_print_subst subst
         end
   in
   let prog = subst_type_prog subst prog in
   let _ =
      if debug Fir_state.debug_infer then
         Fir_print.debug_prog "*** Fir_infer.infer_prog: subst_prog" prog
   in
   let prog = standardize_prog prog in
      close_prog prog

let infer_prog = Fir_state.profile "Fir_infer.infer_prog" infer_prog

let infer_mprog mprog =
   let { marshal_fir = fir } = mprog in
   let fir = infer_prog fir in
      { mprog with marshal_fir = fir }

let infer_mprog = Fir_state.profile "Fir_infer.infer_mprog" infer_mprog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
