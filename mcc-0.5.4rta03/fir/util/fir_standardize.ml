(*
 * Rename all the type vars and vars in the program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
vim:sts=3:sw=3
 *)
open Symbol

open Fir
open Fir_ds
open Fir_mprog

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = var SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

let rec env_add_vars env vars vars' =
   match vars, vars' with
      v :: vars, v' :: vars' ->
         env_add_vars (env_add env v v') vars vars'
    | [], [] ->
         env
    | _ ->
         raise (Invalid_argument "Fc_ir_standardize.env_add_vars")

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

let env_lookup_opt env v_opt =
   match v_opt with
      Some v ->
         Some (env_lookup env v)
    | None ->
         None

(*
 * Check is a variable is defined.
 *)
let env_mem env v =
   SymbolTable.mem v env

(*
 * Free type-variable environment.
 * Whenever we encounter a new free type variable,
 * rename it, and add the renaming to the free-variable
 * enironment.
 *)
type fenv = var SymbolTable.t option

let fenv_empty = Some SymbolTable.empty

(*
 * Type environment has two parts, one for
 * ids, and one for vars.
 *)
type tenv =
   { tenv_types : env;
     tenv_vars : env
   }

(*
 * Type operations.
 *)
let tenv_empty =
   { tenv_types = env_empty;
     tenv_vars = env_empty
   }

let tenv_add_type tenv v v' =
   { tenv with tenv_types = env_add tenv.tenv_types v v' }

let tenv_add_var tenv v v' =
   { tenv with tenv_vars = env_add tenv.tenv_vars v v' }

let tenv_add_vars tenv vars vars' =
   { tenv with tenv_vars = env_add_vars tenv.tenv_vars vars vars' }

let tenv_lookup_var tenv (fenv : fenv) v =
   try SymbolTable.find tenv.tenv_vars v, fenv with
      Not_found ->
         match fenv with
            None ->
               v, fenv
          | Some fenv' ->
               try SymbolTable.find fenv' v, fenv with
                  Not_found ->
                     let v' = new_symbol v in
                     let fenv' = SymbolTable.add fenv' v v' in
                        v', Some fenv'

let tenv_lookup_type tenv v =
   env_lookup tenv.tenv_types v

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

let standardize_var = env_lookup

let standardize_var_option env = function
    None -> None
  | Some v -> Some (standardize_var env v)

(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv (fenv : fenv) venv ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyDelayed
    | TyRawData
    | TyPointer _ ->
         ty, fenv
    | TyFrame (v, tyl) ->
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let ty = TyFrame (v, tyl) in
            ty, fenv
    | TyFun (ty_vars, ty_res) ->
         let ty_vars, fenv = standardize_type_list tenv fenv venv ty_vars in
         let ty_res, fenv = standardize_type tenv fenv venv ty_res in
         let ty = TyFun (ty_vars, ty_res) in
            ty, fenv
    | TyUnion (ty_v, tyl, i) ->
         let ty_v = tenv_lookup_type tenv ty_v in
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let ty = TyUnion (ty_v, tyl, i) in
            ty, fenv
    | TyTuple (tclass, fields) ->
         let fields, fenv = standardize_mutable_type_list tenv fenv venv fields in
         let ty = TyTuple (tclass, fields) in
            ty, fenv
    | TyDTuple (ty_var, fields) ->
         let fields, fenv =
            match fields with
               Some fields ->
                  let fields, fenv = standardize_mutable_type_list tenv fenv venv fields in
                     Some fields, fenv
             | None ->
                  None, fenv
         in
         let ty_var = tenv_lookup_type tenv ty_var in
         let ty = TyDTuple (ty_var, fields) in
            ty, fenv
    | TyTag (ty_var, fields) ->
         let ty_var = tenv_lookup_type tenv ty_var in
         let fields, fenv = standardize_mutable_type_list tenv fenv venv fields in
         let ty = TyTag (ty_var, fields) in
            ty, fenv
    | TyArray ty ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let ty = TyArray ty in
            ty, fenv
    | TyVar v ->
         let v, fenv = tenv_lookup_var tenv fenv v in
         let ty = TyVar v in
            ty, fenv
    | TyApply (v, tyl) ->
         let v = tenv_lookup_type tenv v in
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let ty = TyApply (v, tyl) in
            ty, fenv
    | TyExists (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let ty = TyExists (vars', ty) in
            ty, fenv
    | TyAll (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let ty = TyAll (vars', ty) in
            ty, fenv
    | TyProject (v, i) ->
         let v = standardize_var venv v in
         let ty = TyProject (v, i) in
            ty, fenv
    | TyCase ty ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let ty = TyCase ty in
            ty, fenv
    | TyObject (v, ty) ->
         let v' = new_symbol v in
         let tenv = tenv_add_var tenv v v' in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let ty = TyObject (v', ty) in
            ty, fenv

and standardize_type_list tenv fenv venv tyl =
   let tyl, fenv =
      List.fold_left (fun (tyl, fenv) ty ->
            let ty, fenv = standardize_type tenv fenv venv ty in
               ty :: tyl, fenv) ([], fenv) tyl
   in
      List.rev tyl, fenv

and standardize_mutable_type_list tenv fenv venv tyl =
   let tyl, fenv =
      List.fold_left (fun (tyl, fenv) (ty, b) ->
            let ty, fenv = standardize_type tenv fenv venv ty in
               (ty, b) :: tyl, fenv) ([], fenv) tyl
   in
      List.rev tyl, fenv

(*
 * Rename the vars in a type definition.
 *)
let standardize_tydef tenv fenv venv tyd =
   match tyd with
      TyDefLambda (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let tydef = TyDefLambda (vars', ty) in
            tydef, fenv
    | TyDefUnion (vars, fields) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let fields, fenv =
            List.fold_left (fun (fields, fenv) subfields ->
                  let subfields, fenv =
                     List.fold_left (fun (subfields, fenv) (ty, b) ->
                           let ty, fenv = standardize_type tenv fenv venv ty in
                           let subfields = (ty, b) :: subfields in
                              subfields, fenv) ([], fenv) subfields
                  in
                  let fields = List.rev subfields :: fields in
                     fields, fenv) ([], fenv) fields
         in
         let fields = List.rev fields in
         let tydef = TyDefUnion (vars', fields) in
            tydef, fenv
    | TyDefDTuple ty_var ->
         TyDefDTuple (tenv_lookup_type tenv ty_var), fenv

(*
 * Map a function over all the types in the unop.
 *)
let standardize_unop tenv fenv venv op =
   match op with
      NotEnumOp _

      (* Negation (arithmetic and bitwise) for NAML ints *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Negation (arithmetic and bitwise) for native ints *)
    | UMinusRawIntOp _
    | NotRawIntOp _

      (*
       * RawBitFieldOp (pre, signed, offset, length):
       *    Extracts the bitfield in an integer value, which starts at bit
       *    <offset> (counting from LSB) and containing <length> bits. The
       *    bit shift and length are constant values.  To access a variable
       *    bitfield you must do a manual shift/mask; this optimization is
       *    only good for constant values.  (pre, signed) refer to the
       *    rawint precision of the result; the input should be int32.
       *)
    | RawBitFieldOp     _

      (* Unary floating-point operations *)
    | UMinusFloatOp     _
    | AbsFloatOp        _
    | SinFloatOp        _
    | CosFloatOp        _
    | TanFloatOp        _
    | ASinFloatOp       _
    | ACosFloatOp       _
    | ATanFloatOp       _
    | SinHFloatOp       _
    | CosHFloatOp       _
    | TanHFloatOp       _
    | ExpFloatOp        _
    | LogFloatOp        _
    | Log10FloatOp      _
    | SqrtFloatOp       _
    | CeilFloatOp       _
    | FloorFloatOp      _

      (*
       * Coercions:
       *    Int refers to an ML-style integer
       *    Float refers to floating-point value
       *    RawInt refers to native int, any precision
       *
       * In operators where both terms have precision qualifiers, the
       * destination precision is always specified before the source
       * precision.
       *)

      (* Coercions to int *)
    | IntOfFloatOp      _
    | IntOfRawIntOp     _

      (* Coerce to float *)
    | FloatOfIntOp      _
    | FloatOfFloatOp    _
    | FloatOfRawIntOp   _

      (* Coerce to rawint *)
    | RawIntOfIntOp     _
    | RawIntOfEnumOp    _
    | RawIntOfFloatOp   _

      (*
       * Coerce a rawint _
       * First pair is destination precision, second pair is source.
       *)
    | RawIntOfRawIntOp  _

      (*
       * Integer<->pointer coercions (only for C, not inherently safe)
       * These operators are specifically related to infix pointers...
       *)
    | RawIntOfPointerOp _
    | PointerOfRawIntOp _

      (*
       * Create an infix pointer from a block pointer.  The infix pointer
       * so that both the base and _
       * to the beginning _
       *)
    | PointerOfBlockOp _ ->
         op, fenv

      (*
       * Get the block length.
       *)
    | LengthOfBlockOp (subop, ty) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let op = LengthOfBlockOp (subop, ty) in
            op, fenv

      (*
       * Type coercions.
       *)
    | DTupleOfDTupleOp (ty_var, fields) ->
         let ty_var = tenv_lookup_type tenv ty_var in
         let fields, fenv = standardize_mutable_type_list tenv fenv venv fields in
         let ty = DTupleOfDTupleOp (ty_var, fields) in
            ty, fenv
    | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
         let ty_var = tenv_lookup_type tenv ty_var in
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let ty = UnionOfUnionOp (ty_var, tyl, s1, s2) in
            ty, fenv
    | RawDataOfFrameOp (ty_var, tyl) ->
         let ty_var = tenv_lookup_type tenv ty_var in
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let ty = RawDataOfFrameOp (ty_var, tyl) in
            ty, fenv

(*
 * Map a function over all the types in the binop.
 *)
let standardize_binop tenv fenv venv op =
   match op with
   (* Bitwise operations on enumerations *)
   AndEnumOp _
 | OrEnumOp _
 | XorEnumOp _

   (* Standard binary operations on NAML ints *)
 | PlusIntOp
 | MinusIntOp
 | MulIntOp
 | DivIntOp
 | RemIntOp
 | LslIntOp
 | LsrIntOp
 | AsrIntOp
 | AndIntOp
 | OrIntOp
 | XorIntOp
 | MaxIntOp
 | MinIntOp

   (* Comparisons on NAML ints *)
 | EqIntOp
 | NeqIntOp
 | LtIntOp
 | LeIntOp
 | GtIntOp
 | GeIntOp
 | CmpIntOp                (* Similar to ML's ``compare'' function. *)

   (*
    * Standard binary operations on native ints.  The precision is
    * the result precision; the inputs should match this precision.
    *)
 | PlusRawIntOp   _
 | MinusRawIntOp  _
 | MulRawIntOp    _
 | DivRawIntOp    _
 | RemRawIntOp    _
 | SlRawIntOp     _
 | SrRawIntOp     _
 | AndRawIntOp    _
 | OrRawIntOp     _
 | XorRawIntOp    _
 | MaxRawIntOp    _
 | MinRawIntOp    _

   (*
    * RawSetBitFieldOp (pre, signed, _
    *    See comments for RawBitFieldOp. This modifies the bitfield starting
    *    at bit <_
    *       First atom is the integer containing the field.
    *       Second atom is the value to be set in the field.
    *    The resulting integer contains the revised field, with type
    *    ACRawInt (pre, signed)
    *)
 | RawSetBitFieldOp _

   (* Comparisons on native ints *)
 | EqRawIntOp     _
 | NeqRawIntOp    _
 | LtRawIntOp     _
 | LeRawIntOp     _
 | GtRawIntOp     _
 | GeRawIntOp     _
 | CmpRawIntOp    _

   (* Standard binary operations on floats *)
 | PlusFloatOp  _
 | MinusFloatOp _
 | MulFloatOp   _
 | DivFloatOp   _
 | RemFloatOp   _
 | MaxFloatOp   _
 | MinFloatOp   _

   (* Comparisons on floats *)
 | EqFloatOp    _
 | NeqFloatOp   _
 | LtFloatOp    _
 | LeFloatOp    _
 | GtFloatOp    _
 | GeFloatOp    _
 | CmpFloatOp   _

   (*
    * Arctangent.  This computes arctan(y/x), where y is the first atom
    * and x is the second atom given.  Handles case when x = 0 correctly.
    *)
 | ATan2FloatOp _

   (*
    * Power.  This computes x^y.
    *)
 | PowerFloatOp _

   (*
    * Float hacking.
    * This sets the exponent field _
    *)
 | LdExpFloatIntOp _

   (*
    * Pointer arithmetic. The pointer in the first argument, and the
    * returned pointer should be infix pointers (which keep the base
    * pointer as well as a pointer to anywhere within the block).
    *)
 | PlusPointerOp _ ->
      op, fenv


   (* Pointer (in)equality.  Arguments must have the given type *)
 | EqEqOp ty ->
      let ty, fenv = standardize_type tenv fenv venv ty in
      let op = EqEqOp ty in
         op, fenv
 | NeqEqOp ty ->
      let ty, fenv = standardize_type tenv fenv venv ty in
      let op = NeqEqOp ty in
         op, fenv

(*
 * Rename the vars in an atom.
 *)
let rec standardize_atom tenv fenv venv atom =
   match atom with
      AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomLabel _
    | AtomSizeof _ ->
         atom, fenv
    | AtomNil ty ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let a = AtomNil ty in
            a, fenv
    | AtomConst (ty, tv, i) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let tv = tenv_lookup_type tenv tv in
         let a = AtomConst (ty, tv, i) in
            a, fenv
    | AtomVar v ->
         let a = AtomVar (env_lookup venv v) in
            a, fenv
    | AtomFun v ->
         let a = AtomFun (env_lookup venv v) in
            a, fenv
    | AtomTyApply (a, ty, tyl) ->
         let a, fenv = standardize_atom tenv fenv venv a in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let a = AtomTyApply (a, ty, tyl) in
            a, fenv
    | AtomTyPack (v, ty, tyl) ->
         let v = env_lookup venv v in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let a = AtomTyPack (v, ty, tyl) in
            a, fenv
    | AtomTyUnpack v ->
         let a = AtomTyUnpack (env_lookup venv v) in
            a, fenv
    | AtomUnop (op, a) ->
         let op, fenv = standardize_unop tenv fenv venv op in
         let a, fenv = standardize_atom tenv fenv venv a in
         let a = AtomUnop (op, a) in
            a, fenv
    | AtomBinop (op, a1, a2) ->
         let op, fenv = standardize_binop tenv fenv venv op in
         let a1, fenv = standardize_atom tenv fenv venv a1 in
         let a2, fenv = standardize_atom tenv fenv venv a2 in
         let a = AtomBinop (op, a1, a2) in
            a, fenv

let standardize_atom_opt tenv fenv venv a =
   match a with
      Some a ->
         let a, fenv = standardize_atom tenv fenv venv a in
            Some a, fenv
    | None ->
         None, fenv

let standardize_atoms tenv fenv venv atoms =
   let atoms, fenv =
      List.fold_left (fun (atoms, fenv) a ->
            let a, fenv = standardize_atom tenv fenv venv a in
               a :: atoms, fenv) ([], fenv) atoms
   in
      List.rev atoms, fenv

let standardize_atom_opt_list tenv fenv venv atoms =
   let atoms, fenv =
      List.fold_left (fun (atoms, fenv) a ->
            let a, fenv = standardize_atom_opt tenv fenv venv a in
               a :: atoms, fenv) ([], fenv) atoms
   in
      List.rev atoms, fenv

(*
 * Standardize a predicate.
 *)
let standardize_pred tenv fenv venv pred =
   match pred with
      IsMutable a ->
         let a, fenv = standardize_atom tenv fenv venv a in
         let pred = IsMutable a in
            pred, fenv
    | Reserve (a1, a2) ->
         let a1, fenv = standardize_atom tenv fenv venv a1 in
         let a2, fenv = standardize_atom tenv fenv venv a2 in
         let pred = Reserve (a1, a2) in
            pred, fenv
    | BoundsCheck (op, a1, a2, a3) ->
         let a1, fenv = standardize_atom tenv fenv venv a1 in
         let a2, fenv = standardize_atom tenv fenv venv a2 in
         let a3, fenv = standardize_atom tenv fenv venv a3 in
         let pred = BoundsCheck (op, a1, a2, a3) in
            pred, fenv
    | ElementCheck (ty, op, a1, a2) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let a1, fenv = standardize_atom tenv fenv venv a1 in
         let a2, fenv = standardize_atom tenv fenv venv a2 in
         let pred = ElementCheck (ty, op, a1, a2) in
            pred, fenv

(*
 * Rename all the vars in the expr.
 *)
let rec standardize_expr tenv fenv venv e =
   let loc = loc_of_exp e in
   let e = standardize_expr_core tenv fenv venv (dest_exp_core e) in
      make_exp loc e

and standardize_expr_core tenv fenv venv e =
   match e with
      LetAtom (v, ty, a, e) ->
         standardize_atom_expr tenv fenv venv v ty a e
    | LetExt (v, ty, s, b, ty2, ty_args, al, e) ->
         standardize_ext_expr tenv fenv venv v ty s b ty2 ty_args al e
    | TailCall (label, f, args) ->
         standardize_tailcall_expr tenv fenv venv label f args
    | SpecialCall (label, op) ->
         standardize_specialcall_expr tenv fenv venv label op
    | Match (a, cases) ->
         standardize_match_expr tenv fenv venv a cases
    | MatchDTuple (a, cases) ->
         standardize_match_dtuple_expr tenv fenv venv a cases
    | TypeCase (a1, a2, name, v, e1, e2) ->
         standardize_typecase_expr tenv fenv venv a1 a2 name v e1 e2
    | LetAlloc (v, op, e) ->
         standardize_alloc_expr tenv fenv venv v op e
    | LetSubscript (op, v1, ty, v2, a, e) ->
         standardize_subscript_expr tenv fenv venv op v1 ty v2 a e
    | SetSubscript (op, label, v, a1, ty, a2, e) ->
         standardize_set_subscript_expr tenv fenv venv op label v a1 ty a2 e
    | LetGlobal (op, v, ty, l, e) ->
         standardize_global_expr tenv fenv venv op v ty l e
    | SetGlobal (op, label, v, ty, a, e) ->
         standardize_set_global_expr tenv fenv venv op label v ty a e
    | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
         standardize_memcpy_expr tenv fenv venv op label v1 a1 v2 a2 a3 e
    | Call (label, f, args, e) ->
         standardize_call_expr tenv fenv venv label f args e
    | Assert (label, pred, e) ->
         standardize_assert_expr tenv fenv venv label pred e
    | Debug (info, e) ->
         standardize_debug_expr tenv fenv venv info e

(*
 * Debug.
 *)
and standardize_debug_expr tenv fenv venv info e =
   match info with
      DebugString _ ->
         let e = standardize_expr tenv fenv venv e in
            Debug (info, e)
       | DebugContext (line, info) ->
            let info, fenv =
               List.fold_left (fun (info, fenv) (v1, ty, v2) ->
                     let ty, fenv = standardize_type tenv fenv venv ty in
                     let v2 = standardize_var venv v2 in
                     let info = (v1, ty, v2) :: info in
                        info, fenv) ([], fenv) info
            in
            let e = standardize_expr tenv fenv venv e in
               Debug (DebugContext (line, info), e)

(*
 * Operators.
 *)
and standardize_atom_expr tenv fenv venv v ty a e =
  let v' = new_symbol v in
  let ty, fenv = standardize_type tenv fenv venv ty in
  let a, fenv = standardize_atom tenv fenv venv a in
  let venv = env_add venv v v' in
  let e = standardize_expr tenv fenv venv e in
     LetAtom (v', ty, a, e)

and standardize_ext_expr tenv fenv venv v ty s b ty2 ty_args al e =
  let v' = new_symbol v in
  let ty, fenv = standardize_type tenv fenv venv ty in
  let ty2, fenv = standardize_type tenv fenv venv ty2 in
  let ty_args, fenv = standardize_type_list tenv fenv venv ty_args in
  let al, fenv = standardize_atoms tenv fenv venv al in
  let venv = env_add venv v v' in
  let e = standardize_expr tenv fenv venv e in
     LetExt (v', ty, s, b, ty2, ty_args, al, e)

(*
 * Function call.
 *)
and standardize_tailcall_expr tenv fenv venv label f args =
   let label = new_symbol label in
   let f, fenv = standardize_atom tenv fenv venv f in
   let args, fenv = standardize_atoms tenv fenv venv args in
      TailCall (label, f, args)

and standardize_call_expr tenv fenv venv label f args e =
   let label = new_symbol label in
   let f, fenv = standardize_atom tenv fenv venv f in
   let args, fenv = standardize_atom_opt_list tenv fenv venv args in
   let e = standardize_expr tenv fenv venv e in
      Call (label, f, args, e)

and standardize_specialcall_expr tenv fenv venv label op =
   let op =
      match op with
         TailSysMigrate (id, loc_ptr, loc_off, f, args) ->
            let loc_ptr, fenv = standardize_atom tenv fenv venv loc_ptr in
            let loc_off, fenv = standardize_atom tenv fenv venv loc_off in
            let f, fenv = standardize_atom tenv fenv venv f in
            let args, fenv = standardize_atoms tenv fenv venv args in
               TailSysMigrate (id, loc_ptr, loc_off, f, args)
       | TailAtomic (f, c, args) ->
            let f, fenv = standardize_atom tenv fenv venv f in
            let c, fenv = standardize_atom tenv fenv venv c in
            let args, fenv = standardize_atoms tenv fenv venv args in
               TailAtomic (f, c, args)
       | TailAtomicRollback (level, c) ->
            let level, fenv = standardize_atom tenv fenv venv level in
            let c, fenv = standardize_atom tenv fenv venv c in
               TailAtomicRollback (level, c)
       | TailAtomicCommit (level, f, args) ->
            let level, fenv = standardize_atom tenv fenv venv level in
            let f, fenv = standardize_atom tenv fenv venv f in
            let args, fenv = standardize_atoms tenv fenv venv args in
               TailAtomicCommit (level, f, args)
   in
   let label = new_symbol label in
      SpecialCall (label, op)

(*
 * Conditional.
 *)
and standardize_match_expr tenv fenv venv a cases =
   let a, fenv = standardize_atom tenv fenv venv a in
   let cases = List.map (fun (label, set, e) -> new_symbol label, set, standardize_expr tenv fenv venv e) cases in
      Match (a, cases)

and standardize_match_dtuple_expr tenv fenv venv a cases =
   let a, fenv = standardize_atom tenv fenv venv a in
   let cases =
      List.map (fun (label, a_opt, e) ->
            let label = new_symbol label in
            let a_opt, fenv = standardize_atom_opt tenv fenv venv a_opt in
            let e = standardize_expr tenv fenv venv e in
               label, a_opt, e) cases
   in
      MatchDTuple (a, cases)

(*
 * Typecase.
 *)
and standardize_typecase_expr tenv fenv venv a1 a2 name v e1 e2 =
   let a1, fenv = standardize_atom tenv fenv venv a1 in
   let a2, fenv = standardize_atom tenv fenv venv a2 in
   let name = env_lookup venv name in
   let e2 = standardize_expr tenv fenv venv e2 in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e2 = standardize_expr tenv fenv venv e1 in
      TypeCase (a1, a2, name, v, e1, e2)

(*
 * Allocation.
 *)
and standardize_alloc_op tenv fenv venv (op : alloc_op) =
   match op with
      AllocUnion (ty_vars, ty, ty_v, i, args) ->
         let ty_v = tenv_lookup_type tenv ty_v in
         let ty_vars' = List.map new_symbol ty_vars in
         let tenv = tenv_add_vars tenv ty_vars ty_vars' in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let args, fenv = standardize_atoms tenv fenv venv args in
         let op = AllocUnion (ty_vars', ty, ty_v, i, args) in
            op, fenv
    | AllocTuple (tclass, ty_vars, ty, args) ->
         let ty_vars' = List.map new_symbol ty_vars in
         let tenv = tenv_add_vars tenv ty_vars ty_vars' in
         let ty, fenv = standardize_type tenv fenv venv ty in
         let args, fenv = standardize_atoms tenv fenv venv args in
         let op = AllocTuple (tclass, ty_vars', ty, args) in
            op, fenv
    | AllocDTuple (ty, ty_var, a, args) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let ty_var = tenv_lookup_type tenv ty_var in
         let a, fenv = standardize_atom tenv fenv venv a in
         let args, fenv = standardize_atoms tenv fenv venv args in
         let op = AllocDTuple (ty, ty_var, a, args) in
            op, fenv
    | AllocArray (ty, args) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let args, fenv = standardize_atoms tenv fenv venv args in
         let op = AllocArray (ty, args) in
            op, fenv
    | AllocVArray (ty, sub_index, a1, a2) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let a1, fenv = standardize_atom tenv fenv venv a1 in
         let a2, fenv = standardize_atom tenv fenv venv a2 in
         let op = AllocVArray (ty, sub_index, a1, a2) in
            op, fenv
    | AllocMalloc (ty, a) ->
         let ty, fenv = standardize_type tenv fenv venv ty in
         let a, fenv = standardize_atom tenv fenv venv a in
         let op = AllocMalloc (ty, a) in
            op, fenv
    | AllocFrame (v, tyl) ->
         let tyl, fenv = standardize_type_list tenv fenv venv tyl in
         let op = AllocFrame (v, tyl) in
            op, fenv

and standardize_alloc_expr tenv fenv venv v op e =
   let v' = new_symbol v in
   let op, fenv = standardize_alloc_op tenv fenv venv op in
   let venv = env_add venv v v' in
   let e = standardize_expr tenv fenv venv e in
      LetAlloc (v', op, e)

(*
 * Subscripting.
 *)
and standardize_subscript_expr tenv fenv venv op v1 ty a1 a2 e =
   let v' = new_symbol v1 in
   let ty, fenv = standardize_type tenv fenv venv ty in
   let a1, fenv = standardize_atom tenv fenv venv a1 in
   let a2, fenv = standardize_atom tenv fenv venv a2 in
   let venv = env_add venv v1 v' in
   let e = standardize_expr tenv fenv venv e in
      LetSubscript (op, v', ty, a1, a2, e)

and standardize_set_subscript_expr tenv fenv venv op label a1 a2 ty a3 e =
   let a1, fenv = standardize_atom tenv fenv venv a1 in
   let a2, fenv = standardize_atom tenv fenv venv a2 in
   let ty, fenv = standardize_type tenv fenv venv ty in
   let a3, fenv = standardize_atom tenv fenv venv a3 in
   let e = standardize_expr tenv fenv venv e in
   let label = new_symbol label in
      SetSubscript (op, label, a1, a2, ty, a3, e)

and standardize_global_expr tenv fenv venv op v ty l e =
   let v = env_lookup venv v in
   let ty, fenv = standardize_type tenv fenv venv ty in
   let l = env_lookup venv l in
   let e = standardize_expr tenv fenv venv e in
      LetGlobal (op, v, ty, l, e)

and standardize_set_global_expr tenv fenv venv op label v ty a e =
   let v = env_lookup venv v in
   let ty, fenv = standardize_type tenv fenv venv ty in
   let a, fenv = standardize_atom tenv fenv venv a in
   let e = standardize_expr tenv fenv venv e in
   let label = new_symbol label in
      SetGlobal (op, label, v, ty, a, e)

and standardize_memcpy_expr tenv fenv venv op label v1 a1 v2 a2 a3 e =
   let v1, fenv = standardize_atom tenv fenv venv v1 in
   let a1, fenv = standardize_atom tenv fenv venv a1 in
   let v2, fenv = standardize_atom tenv fenv venv v2 in
   let a2, fenv = standardize_atom tenv fenv venv a2 in
   let a3, fenv = standardize_atom tenv fenv venv a3 in
   let e = standardize_expr tenv fenv venv e in
   let label = new_symbol label in
      Memcpy (op, label, v1, a1, v2, a2, a3, e)

and standardize_assert_expr tenv fenv venv label pred e =
   let pred, fenv = standardize_pred tenv fenv venv pred in
   let e = standardize_expr tenv fenv venv e in
   let label = new_symbol label in
      Assert (label, pred, e)

(*
 * Rename the vars in the name record.
 *)
let standardize_names venv names =
   List.map (fun (v, v_opt) ->
         env_lookup venv v, env_lookup_opt venv v_opt) names

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

let standardize_prog_venv venv
    { prog_file = file;
      prog_import = import;
      prog_export = export;
      prog_types = types;
      prog_frames = frames;
      prog_names = names;
      prog_globals = globals;
      prog_funs = funs
    } =

   (* Rename all the types first *)
   let tenv =
      SymbolTable.fold (fun tenv v _ ->
            let v' = new_symbol v in
               tenv_add_type tenv v v') tenv_empty types
   in
   let types =
      SymbolTable.fold (fun types v tyd ->
            let v = tenv_lookup_type tenv v in
            let tyd, _ = standardize_tydef tenv fenv_empty env_empty tyd in
               SymbolTable.add types v tyd) SymbolTable.empty types
   in

   (* Rename all of the type names *)
   let venv =
      SymbolTable.fold (fun venv v _ ->
            let v' = new_symbol v in
               env_add venv v v') venv names
   in

   (* Rename all the globals *)
   let venv =
      SymbolTable.fold (fun venv v _ ->
            let v' = new_symbol v in
               env_add venv v v') venv globals
   in

   (* Now rename all the functions *)
   let venv =
      SymbolTable.fold (fun venv f _ ->
            let f' = new_symbol f in
               env_add venv f f') venv funs
   in

   (* Now rename their contents *)
   let names =
      SymbolTable.fold (fun names v ty ->
            let v' = env_lookup venv v in
            let ty, _ = standardize_type tenv fenv_empty venv ty in
               SymbolTable.add names v' ty) SymbolTable.empty names
   in

   (* Rename the fields in the frames *)
   let frames =
      SymbolTable.map (fun (vars, fields) ->
            let vars' = List.map new_symbol vars in
            let tenv = tenv_add_vars tenv vars vars' in
            let fields, fenv =
               SymbolTable.fold (fun (fields, fenv) v subfields ->
                     let subfields, fenv =
                        List.fold_left (fun (subfields, fenv) (v, ty, i) ->
                           let ty, fenv = standardize_type tenv fenv venv ty in
                           let subfields = (v, ty, i) :: subfields in
                              subfields, fenv) ([], fenv) subfields
                     in
                     let fields = SymbolTable.add fields v (List.rev subfields) in
                        fields, fenv) (SymbolTable.empty, fenv_empty) fields
            in
               vars', fields) frames
   in

   let globals =
      SymbolTable.fold (fun globals v (ty, init) ->
            let init =
               match init with
                  InitAtom a ->
                     let a, _ = standardize_atom tenv fenv_empty venv a in
                        InitAtom a
                | InitAlloc op ->
                     let op, _ = standardize_alloc_op tenv fenv_empty venv op in
                        InitAlloc op
                | InitNames (v, names) ->
                     let v, _ = tenv_lookup_var tenv fenv_empty v in
                        InitNames (v, standardize_names venv names)
                | InitRawData _ ->
                     init
                | InitTag (ty_var, fields) ->
                     let ty_var = tenv_lookup_type tenv ty_var in
                     let fields, _ = standardize_mutable_type_list tenv fenv_empty venv fields in
                        InitTag (ty_var, fields)
            in
            let v' = env_lookup venv v in
            let ty, _ = standardize_type tenv fenv_empty venv ty in
               env_add globals v' (ty, init)) env_empty globals
   in

   let import =
      SymbolTable.fold (fun import v info ->
            let v' = env_lookup venv v in
            let ty, _ = standardize_type tenv fenv_empty venv info.import_type in
            let info = { info with import_type = ty } in
               env_add import v' info) env_empty import
   in

   let export =
      SymbolTable.fold (fun export v info ->
            let v' = env_lookup venv v in
            let ty, _ = standardize_type tenv fenv_empty venv info.export_type in
            let info = { info with export_type = ty } in
               env_add export v' info) env_empty export
   in

   let funs =
      SymbolTable.fold (fun funs f (line, ty_vars, ty, vars, e) ->
            let ty_vars' = List.map new_symbol ty_vars in
            let tenv = tenv_add_vars tenv ty_vars ty_vars' in
            let ty, _ = standardize_type tenv fenv_empty venv ty in
            let vars' = List.map new_symbol vars in
            let venv = env_add_vars venv vars vars' in
            let e = standardize_expr tenv fenv_empty venv e in
            let f' = env_lookup venv f in
            let fund = line, ty_vars', ty, vars', e in
               SymbolTable.add funs f' fund) SymbolTable.empty funs
   in

   (* Find out the new name of the init symbol *)
   let prog =
      { prog_file = file;
        prog_import = import;
        prog_export = export;
        prog_types = types;
        prog_frames = frames;
        prog_names = names;
        prog_globals = globals;
        prog_funs = funs
      }
   in
      venv, prog

let standardize_prog prog =
   let _, prog = standardize_prog_venv env_empty prog in
      prog

let standardize_mprog mprog =
   let { marshal_fir           = fir;
         marshal_globals       = globals;
         marshal_funs          = funs;
         marshal_rttd          = rttd
       } = mprog
   in
   let venv, fir = standardize_prog_venv env_empty fir in
   let standardize_symbol_list = List.map (SymbolTable.find venv) in
   let globals = standardize_symbol_list globals in
   let funs = standardize_symbol_list funs in
   let rttd = Rttd.map (fun _ v -> SymbolTable.find venv v) rttd in
      { marshal_fir           = fir;
        marshal_globals       = globals;
        marshal_funs          = funs;
        marshal_rttd          = rttd
      }

(*
 * Other wrappers.
 *)
let standardize_type ty =
   let ty, _ = standardize_type tenv_empty None SymbolTable.empty ty in
      ty

let standardize_tydef tydef =
   let tydef, _ = standardize_tydef tenv_empty None SymbolTable.empty tydef in
      tydef

let standardize_expr e =
   standardize_expr tenv_empty None SymbolTable.empty e

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
