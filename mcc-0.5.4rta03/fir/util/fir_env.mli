(*
 * Standard FIR environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_pos

(*
 * Environments are abstract.
 *)
type tenv = tydef SymbolTable.t
type venv = ty SymbolTable.t

(*
 * Type environment operations.
 *)
val tenv_empty : tenv
val tenv_add : tenv -> var -> tydef -> tenv
val tenv_lookup : tenv -> pos -> var -> tydef
val tenv_fold : ('a -> var -> tydef -> 'a) -> 'a -> tenv -> 'a

(*
 * Variable environment operations.
 *)
val venv_empty : venv
val venv_add : venv -> var -> ty -> venv
val venv_mem : venv -> var -> bool
val venv_lookup : venv -> pos -> var -> ty
val venv_lookup_opt : venv -> var -> ty option

(*
 * The "global" environment contains a spec for
 * all the different kinds of objects.
 *)
type genv

val tenv_of_genv : genv -> tenv
val venv_of_genv : genv -> venv

val genv_empty : genv
val genv_of_prog : prog -> genv

val genv_add_type : genv -> var -> tydef -> genv
val genv_add_var : genv -> var -> ty -> genv
val genv_add_fun : genv -> var -> ty -> genv
val genv_add_global : genv -> var -> ty -> genv
val genv_add_name : genv -> var -> ty -> genv
val genv_add_frame : genv -> var -> frame -> genv
val genv_add_poly : genv -> var list -> genv

val genv_mem_type : genv -> var -> bool
val genv_mem_var : genv -> var -> bool
val genv_mem_fun : genv -> var -> bool
val genv_mem_global : genv -> var -> bool
val genv_mem_frame : genv -> var -> bool
val genv_mem_poly : genv -> var -> bool

val genv_lookup_type : genv -> pos -> var -> tydef
val genv_lookup_var : genv -> pos -> var -> ty
val genv_lookup_fun : genv -> pos -> var -> ty
val genv_lookup_global : genv -> pos -> var -> ty
val genv_lookup_name : genv -> pos -> var -> ty
val genv_lookup_frame : genv -> pos -> var -> frame
val genv_lookup_var_or_global : genv -> pos -> var -> ty

val genv_add_vars : genv -> pos -> var list -> ty list -> genv

(*
 * This is for variable "normalization".
 * The first variable is defined to be the same
 * as the second.
 *)
val genv_add_norm : genv -> var -> var -> genv
val genv_equal_norm : genv -> var -> var -> bool

(*
 * Variable alpha equivalence.
 *)
val genv_add_alpha : genv -> pos -> var list -> var list -> genv
val genv_equal_alpha : genv -> var -> var -> bool

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
