(*
   Read and write binary-FIR code
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Debug
open Symbol
open Flags
open Mc_string_util

open Fmarshal
open Location
open Interval_set

open Fir
open Fir_ds
open Fir_set
open Fir_exn
open Fir_pos
open Fir_mprog

module Pos = MakePos (struct let name = "Fir_marshal" end)
open Pos

(************************************************************************
 * MARSHALLER MODULE SIGNATURE
 ************************************************************************)

(*
 * Generic marshaler.
 *)
module type MarshalSig =
sig
   type in_channel
   type out_channel

   val marshal_prog : out_channel -> mprog -> unit
   val unmarshal_prog : in_channel -> mprog
end

(************************************************************************
 * MAGIC NUMBERS
 ************************************************************************)

let version_number = Hashtbl.hash "$Id: fir_marshal.ml,v 1.50 2002/12/23 00:13:15 jyh Exp $"

type magic =
   NoneMagic
 | SomeMagic
 | SymbolMagic
 | IntSetMagic
 | RawIntSetMagic
 | RawInt8Magic
 | RawInt16Magic
 | RawInt32Magic
 | RawInt64Magic
 | SingleMagic
 | DoubleMagic
 | LongDoubleMagic
 | TyIntMagic
 | TyEnumMagic
 | TyRawIntMagic
 | TyFloatMagic
 | TyFunMagic
 | TyRawDataMagic
 | TyUnionMagic
 | TyTupleMagic
 | TyDTupleMagic
 | TyTagMagic
 | TyArrayMagic
 | TyVarMagic
 | TyApplyMagic
 | TyExistsMagic
 | TyAllMagic
 | TyProjectMagic
 | TyPointerMagic
 | TyFrameMagic
 | TyCaseMagic
 | TyObjectMagic
 | TyDelayedMagic
 | TydefUnionMagic
 | TydefLambdaMagic
 | TydefDTupleMagic
 | NotEnumOpMagic
 | UMinusIntOpMagic
 | AbsIntOpMagic
 | NotIntOpMagic
 | AndEnumOpMagic
 | OrEnumOpMagic
 | XorEnumOpMagic
 | PlusIntOpMagic
 | MinusIntOpMagic
 | MulIntOpMagic
 | DivIntOpMagic
 | RemIntOpMagic
 | LslIntOpMagic
 | LsrIntOpMagic
 | AsrIntOpMagic
 | AndIntOpMagic
 | OrIntOpMagic
 | XorIntOpMagic
 | MinIntOpMagic
 | MaxIntOpMagic
 | EqIntOpMagic
 | NeqIntOpMagic
 | LtIntOpMagic
 | LeIntOpMagic
 | GtIntOpMagic
 | GeIntOpMagic
 | CmpIntOpMagic
 | RawBitFieldOpMagic
 | RawSetBitFieldOpMagic
 | UMinusRawIntOpMagic
 | NotRawIntOpMagic
 | PlusRawIntOpMagic
 | MinusRawIntOpMagic
 | MulRawIntOpMagic
 | DivRawIntOpMagic
 | RemRawIntOpMagic
 | SlRawIntOpMagic
 | SrRawIntOpMagic
 | AndRawIntOpMagic
 | OrRawIntOpMagic
 | XorRawIntOpMagic
 | MinRawIntOpMagic
 | MaxRawIntOpMagic
 | EqRawIntOpMagic
 | NeqRawIntOpMagic
 | LtRawIntOpMagic
 | LeRawIntOpMagic
 | GtRawIntOpMagic
 | GeRawIntOpMagic
 | CmpRawIntOpMagic
 | UMinusFloatOpMagic
 | AbsFloatOpMagic
 | SinFloatOpMagic
 | CosFloatOpMagic
 | TanFloatOpMagic
 | ASinFloatOpMagic
 | ACosFloatOpMagic
 | ATanFloatOpMagic
 | SinHFloatOpMagic
 | CosHFloatOpMagic
 | TanHFloatOpMagic
 | ExpFloatOpMagic
 | LogFloatOpMagic
 | Log10FloatOpMagic
 | SqrtFloatOpMagic
 | PlusFloatOpMagic
 | MinusFloatOpMagic
 | MulFloatOpMagic
 | DivFloatOpMagic
 | RemFloatOpMagic
 | MinFloatOpMagic
 | MaxFloatOpMagic
 | EqFloatOpMagic
 | NeqFloatOpMagic
 | LtFloatOpMagic
 | LeFloatOpMagic
 | GtFloatOpMagic
 | GeFloatOpMagic
 | CmpFloatOpMagic
 | ATan2FloatOpMagic
 | PowerFloatOpMagic
 | LdExpFloatIntOpMagic
 | EqEqOpMagic
 | NeqEqOpMagic
 | PlusPointerOpMagic
 | IntOfFloatOpMagic
 | FloorFloatOpMagic
 | CeilFloatOpMagic
 | FloatOfIntOpMagic
 | FloatOfFloatOpMagic
 | RawIntOfIntOpMagic
 | IntOfRawIntOpMagic
 | RawIntOfEnumOpMagic
 | RawIntOfFloatOpMagic
 | FloatOfRawIntOpMagic
 | RawIntOfRawIntOpMagic
 | RawIntOfPointerOpMagic
 | PointerOfRawIntOpMagic
 | PointerOfBlockOpMagic
 | LengthOfBlockOpMagic
 | DTupleOfDTupleOpMagic
 | UnionOfUnionOpMagic
 | RawDataOfFrameOpMagic
 | ExternalOpMagic
 | AtomNilMagic
 | AtomIntMagic
 | AtomEnumMagic
 | AtomRawIntMagic
 | AtomFloatMagic
 | AtomConstMagic
 | AtomVarMagic
 | AtomFunMagic
 | AtomLabelMagic
 | AtomSizeofMagic
 | AtomTyApplyMagic
 | AtomTyPackMagic
 | AtomTyUnpackMagic
 | AtomUnopMagic
 | AtomBinopMagic
 | AtomListMagic
 | AtomOptListMagic
 | AllocTupleMagic
 | AllocDTupleMagic
 | AllocUnionMagic
 | AllocArrayMagic
 | AllocVArrayMagic
 | AllocMallocMagic
 | AllocFrameMagic
 | DebugStringMagic
 | DebugContextMagic
 | LocationMagic
 | ExpMagic
 | LetAtomMagic
 | LetExtMagic
 | TailCallMagic
 | TailSysMigrateMagic
 | TailAtomicMagic
 | TailAtomicRollbackMagic
 | TailAtomicCommitMagic
 | MatchMagic
 | MatchDTupleMagic
 | TypeCaseMagic
 | LetAllocMagic
 | LetSubscriptMagic
 | SetSubscriptMagic
 | LetGlobalMagic
 | SetGlobalMagic
 | MemcpyMagic
 | DebugMagic
 | MatchCaseMagic
 | MatchDTupleCaseMagic
 | TypedefMagic
 | TypesMagic
 | SimpleTypeMagic
 | SimpleTypesMagic
 | InitAtomMagic
 | InitAllocMagic
 | InitRawDataMagic
 | InitNamesMagic
 | InitTagMagic
 | GlobalMagic
 | GlobalsMagic
 | ImportMagic
 | ImportsMagic
 | ExportMagic
 | ExportsMagic
 | FundefMagic
 | FunsMagic
 | FileInfoMagic
 | ProgMagic
 | SymbolTableMagic
 | FlagMagic
 | FlagBoolMagic
 | FlagIntMagic
 | ArgFunctionMagic
 | ArgPointerMagic
 | ArgRawIntMagic
 | ArgRawFloatMagic
 | ImportGlobalMagic
 | ImportVarFunMagic
 | ImportFunMagic
 | SubOpMagic
 | BlockSubMagic
 | RawDataSubMagic
 | RawTupleSubMagic
 | TupleSubMagic
 | PolySubMagic
 | EnumSubMagic
 | IntSubMagic
 | TagSubMagic
 | RawIntSubMagic
 | RawFloatSubMagic
 | BlockPointerSubMagic
 | RawPointerSubMagic
 | PointerInfixSubMagic
 | FunctionSubMagic
 | ByteIndexMagic
 | WordIndexMagic
 | IntIndexMagic
 | RawIntIndexMagic
 | NormalTupleMagic
 | RawTupleMagic
 | BoxTupleMagic
 | MutableMagic
 | ImmutableMagic
 | MutableDelayedMagic
 | MutableVarMagic
 | FileFCMagic
 | FileJavaMagic
 | FileNamlMagic
 | FileAmlMagic
 | RttdEntryMagic
 | RttdMagic
 | IsMutableMagic
 | ReserveMagic
 | BoundsCheckMagic
 | ElementCheckMagic
 | AssertMagic
 | CallMagic
 | SubfieldMagic
 | SubfieldsMagic
 | FieldMagic
 | FieldsMagic
 | FrameMagic
 | FramesMagic
 | MaxMagic

(*
 * Magic numbers.
 * We cheat a little here.
 *)
let int_of_magic magic =
   (Obj.magic magic : int)

let max_magic = int_of_magic MaxMagic

let magic_of_int i =
   if i < 0 || i >= max_magic then
      raise (Failure "magic_of_int");
   (Obj.magic i : magic)


(************************************************************************
 * MARSHALLED EXPRESSION PRINTER
 ************************************************************************)

let string_of_magic = function
   NoneMagic -> "None"
 | SomeMagic -> "Some"
 | SymbolMagic -> "Symbol"
 | IntSetMagic -> "IntSet"
 | RawIntSetMagic -> "RawIntSet"
 | RawInt8Magic -> "RawInt8"
 | RawInt16Magic -> "RawInt16"
 | RawInt32Magic -> "RawInt32"
 | RawInt64Magic -> "RawInt64"
 | SingleMagic -> "Single"
 | DoubleMagic -> "Double"
 | LongDoubleMagic -> "LongDouble"
 | TyIntMagic -> "TyInt"
 | TyEnumMagic -> "TyEnum"
 | TyRawIntMagic -> "TyRawInt"
 | TyFloatMagic -> "TyFloat"
 | TyFunMagic -> "TyFun"
 | TyRawDataMagic -> "TyRawData"
 | TyUnionMagic -> "TyUnion"
 | TyTupleMagic -> "TyTuple"
 | TyDTupleMagic -> "TyDTuple"
 | TyTagMagic -> "TyTag"
 | TyArrayMagic -> "TyArray"
 | TyVarMagic -> "TyVar"
 | TyApplyMagic -> "TyApply"
 | TyExistsMagic -> "TyExists"
 | TyAllMagic -> "TyAll"
 | TyProjectMagic -> "TyProject"
 | TyPointerMagic -> "TyPointer"
 | TyFrameMagic -> "TyFrame"
 | TyCaseMagic -> "TyCase"
 | TyObjectMagic -> "TyObject"
 | TyDelayedMagic -> "TyDelayed"
 | TydefUnionMagic -> "TydefUnion"
 | TydefLambdaMagic -> "TydefLambda"
 | TydefDTupleMagic -> "TydefDTuple"
 | NotEnumOpMagic -> "NotEnumOp"
 | UMinusIntOpMagic -> "UMinusIntOp"
 | AbsIntOpMagic -> "AbsIntOp"
 | NotIntOpMagic -> "NotIntOp"
 | AndEnumOpMagic -> "AndEnumOp"
 | OrEnumOpMagic -> "OrEnumOp"
 | XorEnumOpMagic -> "XorEnumOp"
 | PlusIntOpMagic -> "PlusIntOp"
 | MinusIntOpMagic -> "MinusIntOp"
 | MulIntOpMagic -> "MulIntOp"
 | DivIntOpMagic -> "DivIntOp"
 | RemIntOpMagic -> "RemIntOp"
 | LslIntOpMagic -> "LslIntOp"
 | LsrIntOpMagic -> "LsrIntOp"
 | AsrIntOpMagic -> "AsrIntOp"
 | AndIntOpMagic -> "AndIntOp"
 | OrIntOpMagic -> "OrIntOp"
 | XorIntOpMagic -> "XorIntOp"
 | MinIntOpMagic -> "MinIntOp"
 | MaxIntOpMagic -> "MaxIntOp"
 | EqIntOpMagic -> "EqIntOp"
 | NeqIntOpMagic -> "NeqIntOp"
 | LtIntOpMagic -> "LtIntOp"
 | LeIntOpMagic -> "LeIntOp"
 | GtIntOpMagic -> "GtIntOp"
 | GeIntOpMagic -> "GeIntOp"
 | CmpIntOpMagic -> "CmpIntOp"
 | RawBitFieldOpMagic -> "RawBitFieldOp"
 | RawSetBitFieldOpMagic -> "RawSetBitFieldOp"
 | UMinusRawIntOpMagic -> "UMinusRawIntOp"
 | NotRawIntOpMagic -> "NotRawIntOp"
 | PlusRawIntOpMagic -> "PlusRawIntOp"
 | MinusRawIntOpMagic -> "MinusRawIntOp"
 | MulRawIntOpMagic -> "MulRawIntOp"
 | DivRawIntOpMagic -> "DivRawIntOp"
 | RemRawIntOpMagic -> "RemRawIntOp"
 | SlRawIntOpMagic -> "SlRawIntOp"
 | SrRawIntOpMagic -> "SrRawIntOp"
 | AndRawIntOpMagic -> "AndRawIntOp"
 | OrRawIntOpMagic -> "OrRawIntOp"
 | XorRawIntOpMagic -> "XorRawIntOp"
 | MinRawIntOpMagic -> "MinRawIntOp"
 | MaxRawIntOpMagic -> "MaxRawIntOp"
 | EqRawIntOpMagic -> "EqRawIntOp"
 | NeqRawIntOpMagic -> "NeqRawIntOp"
 | LtRawIntOpMagic -> "LtRawIntOp"
 | LeRawIntOpMagic -> "LeRawIntOp"
 | GtRawIntOpMagic -> "GtRawIntOp"
 | GeRawIntOpMagic -> "GeRawIntOp"
 | CmpRawIntOpMagic -> "CmpRawIntOp"
 | UMinusFloatOpMagic -> "UMinusFloatOp"
 | AbsFloatOpMagic -> "AbsFloatOp"
 | SinFloatOpMagic -> "SinFloatOp"
 | CosFloatOpMagic -> "CosFloatOp"
 | TanFloatOpMagic -> "TanFloatOp"
 | ASinFloatOpMagic -> "ASinFloatOp"
 | ACosFloatOpMagic -> "ACosFloatOp"
 | ATanFloatOpMagic -> "ATanFloatOp"
 | SinHFloatOpMagic -> "SinHFloatOp"
 | CosHFloatOpMagic -> "CosHFloatOp"
 | TanHFloatOpMagic -> "TanHFloatOp"
 | ExpFloatOpMagic -> "ExpFloatOp"
 | LogFloatOpMagic -> "LogFloatOp"
 | Log10FloatOpMagic -> "Log10FloatOp"
 | SqrtFloatOpMagic -> "SqrtFloatOp"
 | PlusFloatOpMagic -> "PlusFloatOp"
 | MinusFloatOpMagic -> "MinusFloatOp"
 | MulFloatOpMagic -> "MulFloatOp"
 | DivFloatOpMagic -> "DivFloatOp"
 | RemFloatOpMagic -> "RemFloatOp"
 | MinFloatOpMagic -> "MinFloatOp"
 | MaxFloatOpMagic -> "MaxFloatOp"
 | EqFloatOpMagic -> "EqFloatOp"
 | NeqFloatOpMagic -> "NeqFloatOp"
 | LtFloatOpMagic -> "LtFloatOp"
 | LeFloatOpMagic -> "LeFloatOp"
 | GtFloatOpMagic -> "GtFloatOp"
 | GeFloatOpMagic -> "GeFloatOp"
 | CmpFloatOpMagic -> "CmpFloatOp"
 | ATan2FloatOpMagic -> "ATan2FloatOp"
 | PowerFloatOpMagic -> "PowerFloatOp"
 | LdExpFloatIntOpMagic -> "LdExpFloatIntOp"
 | EqEqOpMagic -> "EqEqOp"
 | NeqEqOpMagic -> "NeqEqOp"
 | PlusPointerOpMagic -> "PlusPointerOp"
 | IntOfFloatOpMagic -> "IntOfFloatOp"
 | FloorFloatOpMagic -> "FloorFloatOp"
 | CeilFloatOpMagic -> "CeilFloatOp"
 | FloatOfIntOpMagic -> "FloatOfIntOp"
 | FloatOfFloatOpMagic -> "FloatOfFloatOp"
 | RawIntOfIntOpMagic -> "RawIntOfIntOp"
 | IntOfRawIntOpMagic -> "IntOfRawIntOp"
 | RawIntOfEnumOpMagic -> "RawIntOfEnumOp"
 | RawIntOfFloatOpMagic -> "RawIntOfFloatOp"
 | FloatOfRawIntOpMagic -> "FloatOfRawIntOp"
 | RawIntOfRawIntOpMagic -> "RawIntOfRawIntOp"
 | RawIntOfPointerOpMagic -> "RawIntOfPointerOp"
 | PointerOfRawIntOpMagic -> "PointerOfRawIntOp"
 | PointerOfBlockOpMagic -> "PointerOfBlockOp"
 | LengthOfBlockOpMagic -> "LengthOfBlockOp"
 | DTupleOfDTupleOpMagic -> "DTupleOfDTupleOp"
 | UnionOfUnionOpMagic -> "UnionOfUnionOp"
 | RawDataOfFrameOpMagic -> "RawDataOfFrameOp"
 | ExternalOpMagic -> "ExternalOp"
 | AtomNilMagic -> "AtomNil"
 | AtomIntMagic -> "AtomInt"
 | AtomEnumMagic -> "AtomEnum"
 | AtomRawIntMagic -> "AtomRawInt"
 | AtomFloatMagic -> "AtomFloat"
 | AtomConstMagic -> "AtomConst"
 | AtomVarMagic -> "AtomVar"
 | AtomFunMagic -> "AtomFun"
 | AtomLabelMagic -> "AtomLabel"
 | AtomSizeofMagic -> "AtomSizeof"
 | AtomTyApplyMagic -> "AtomTyApply"
 | AtomTyPackMagic -> "AtomTyPack"
 | AtomTyUnpackMagic -> "AtomTyUnpack"
 | AtomUnopMagic -> "AtomUnop"
 | AtomBinopMagic -> "AtomBinop"
 | AtomListMagic -> "AtomList"
 | AtomOptListMagic -> "AtomOptList"
 | AllocTupleMagic -> "AllocTuple"
 | AllocDTupleMagic -> "AllocDTuple"
 | AllocUnionMagic -> "AllocUnion"
 | AllocArrayMagic -> "AllocArray"
 | AllocVArrayMagic -> "AllocVArray"
 | AllocMallocMagic -> "AllocMalloc"
 | AllocFrameMagic -> "AllocFrame"
 | DebugStringMagic -> "DebugString"
 | DebugContextMagic -> "DebugContext"
 | LocationMagic -> "Location"
 | ExpMagic -> "Exp"
 | LetAtomMagic -> "LetAtom"
 | LetExtMagic -> "LetExt"
 | TailCallMagic -> "TailCall"
 | TailSysMigrateMagic -> "TailSysMigrate"
 | TailAtomicMagic -> "TailAtomic"
 | TailAtomicRollbackMagic -> "TailAtomicRollback"
 | TailAtomicCommitMagic -> "TailAtomicCommit"
 | MatchMagic -> "Match"
 | MatchDTupleMagic -> "MatchDTuple"
 | TypeCaseMagic -> "TypeCase"
 | LetAllocMagic -> "LetAlloc"
 | LetSubscriptMagic -> "LetSubscript"
 | SetSubscriptMagic -> "SetSubscript"
 | LetGlobalMagic -> "LetGlobal"
 | SetGlobalMagic -> "SetGlobal"
 | MemcpyMagic -> "Memcpy"
 | DebugMagic -> "Debug"
 | MatchCaseMagic -> "MatchCase"
 | MatchDTupleCaseMagic -> "MatchDTupleCase"
 | TypedefMagic -> "Typedef"
 | TypesMagic -> "Types"
 | SimpleTypeMagic -> "SimpleType"
 | SimpleTypesMagic -> "SimpleTypes"
 | InitAtomMagic -> "InitAtom"
 | InitAllocMagic -> "InitAlloc"
 | InitRawDataMagic -> "InitRawData"
 | InitNamesMagic -> "InitNames"
 | InitTagMagic -> "InitTag"
 | GlobalMagic -> "Global"
 | GlobalsMagic -> "Globals"
 | ImportMagic -> "Import"
 | ImportsMagic -> "Imports"
 | ExportMagic -> "Export"
 | ExportsMagic -> "Exports"
 | FundefMagic -> "Fundef"
 | FunsMagic -> "Funs"
 | FileInfoMagic -> "FileInfo"
 | ProgMagic -> "Prog"
 | SymbolTableMagic -> "SymbolTable"
 | FlagMagic -> "Flag"
 | FlagBoolMagic -> "FlagBool"
 | FlagIntMagic -> "FlagInt"
 | ArgFunctionMagic -> "ArgFunction"
 | ArgPointerMagic -> "ArgPointer"
 | ArgRawIntMagic -> "ArgRawInt"
 | ArgRawFloatMagic -> "ArgRawFloat"
 | ImportGlobalMagic -> "ImportGlobal"
 | ImportVarFunMagic -> "ImportVarFun"
 | ImportFunMagic -> "ImportFun"
 | SubOpMagic -> "SubOp"
 | BlockSubMagic -> "BlockSub"
 | RawDataSubMagic -> "RawDataSub"
 | RawTupleSubMagic -> "RawTupleSub"
 | TupleSubMagic -> "TupleSub"
 | PolySubMagic -> "PolySub"
 | EnumSubMagic -> "EnumSub"
 | IntSubMagic -> "IntSub"
 | TagSubMagic -> "TagSub"
 | RawIntSubMagic -> "RawIntSub"
 | RawFloatSubMagic -> "RawFloatSub"
 | BlockPointerSubMagic -> "BlockPointerSub"
 | RawPointerSubMagic -> "RawPointerSub"
 | PointerInfixSubMagic -> "PointerInfixSub"
 | FunctionSubMagic -> "FunctionSub"
 | ByteIndexMagic -> "ByteIndex"
 | WordIndexMagic -> "WordIndex"
 | IntIndexMagic -> "IntIndex"
 | RawIntIndexMagic -> "RawIntIndex"
 | NormalTupleMagic -> "NormalTuple"
 | RawTupleMagic -> "RawTuple"
 | BoxTupleMagic -> "BoxTuple"
 | MutableMagic -> "MutableMagic"
 | ImmutableMagic -> "ImmutableMagic"
 | MutableDelayedMagic -> "MutableDelayed"
 | MutableVarMagic -> "MutableVar"
 | FileFCMagic -> "FileFC"
 | FileJavaMagic -> "FileJava"
 | FileNamlMagic -> "FileNaml"
 | FileAmlMagic -> "FileAml"
 | RttdEntryMagic -> "RttdEntry"
 | RttdMagic -> "Rttd"
 | IsMutableMagic -> "IsMutable"
 | ReserveMagic -> "Reserve"
 | BoundsCheckMagic -> "BoundsCheck"
 | ElementCheckMagic -> "ElementCheck"
 | AssertMagic -> "Assert"
 | CallMagic -> "Call"
 | SubfieldMagic -> "Subfield"
 | SubfieldsMagic -> "Subfields"
 | FieldMagic -> "Field"
 | FieldsMagic -> "Fields"
 | FrameMagic -> "Frame"
 | FramesMagic -> "Frames"
 | MaxMagic -> "Max"

let rec print_marshal_exp e =
   match e with
      Bool b ->
         printf "Bool[%b]" b
    | Int i ->
         printf "Int[%d]" i
    | Rawint i ->
         printf "Rawint[%s]" (Rawint.to_string i)
    | Float f ->
         printf "Float[%g]" f
    | Rawfloat f ->
         printf "Float[%s]" (Rawfloat.to_string f)
    | String s ->
         printf "String[\"%s\"]" s
    | Magic m ->
         printf "Magic[%s]" (string_of_magic m)
    | List ls ->
         printf "List[@[<hv 0>";
         print_marshal_space_delim ls;
         printf "]@]"

and print_marshal_space_delim ls =
   let _ = List.fold_left (fun first e ->
      if not first then printf ",@ ";
      print_marshal_exp e;
      false) true ls
   in
      ()

let print_marshal_exp e =
   printf "@[<hv 3>";
   print_marshal_exp e;
   printf "@]@."

(************************************************************************
 * MARSHALING FUNCTIONS
 ************************************************************************)

(*
 * Symbols are special.
 *)
let marshal_symbol senv v =
   let senv, i =
      try
         let i = SymbolTable.find senv v in
            senv, i
      with
         Not_found ->
            let i = SymbolTable.cardinal senv in
            let senv = SymbolTable.add senv v i in
               senv, i
   in
      senv, List [Magic SymbolMagic; Int i]

let marshal_symbol_option senv = function
    None ->
      senv, Magic NoneMagic
  | Some v ->
      let senv, v = marshal_symbol senv v in
        senv, List [Magic SomeMagic; v]


let marshal_symbol_list senv vars =
   let senv, vars =
      List.fold_left (fun (senv, vars) v ->
            let senv, v = marshal_symbol senv v in
               senv, v :: vars) (senv, []) vars
   in
      senv, List (List.rev vars)

let unmarshal_symbol senv pos l =
   let pos = string_pos "unmarshal_symbol" pos in
      match l with
         List [Magic SymbolMagic; Int i] ->
            if i < 0 || i >= Array.length senv then
               raise (FirException (pos, MarshalError));
            senv.(i)
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_symbol_option senv pos l =
  let pos = string_pos "unmarshal_symbol_option" pos in
    match l with
        Magic NoneMagic -> None
      | List [Magic SomeMagic; l] ->
          let v = unmarshal_symbol senv pos l in
            Some v
      | _ ->
          raise (FirException (pos, MarshalError))

let unmarshal_symbol_list senv pos l =
   let pos = string_pos "unmarshal_symbol_list" pos in
      match l with
         List l ->
            List.map (unmarshal_symbol senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Convert a FIR term to a marshal item.
 *)
let marshal_int_precision = function
   Rawint.Int8  -> Magic RawInt8Magic
 | Rawint.Int16 -> Magic RawInt16Magic
 | Rawint.Int32 -> Magic RawInt32Magic
 | Rawint.Int64 -> Magic RawInt64Magic

let marshal_float_precision = function
   Rawfloat.Single     -> Magic SingleMagic
 | Rawfloat.Double     -> Magic DoubleMagic
 | Rawfloat.LongDouble -> Magic LongDoubleMagic

let marshal_signed b =
   Bool b

let unmarshal_int_precision pos l =
   let pos = string_pos "unmarshal_precision" pos in
      match l with
         Magic RawInt8Magic -> Rawint.Int8
       | Magic RawInt16Magic -> Rawint.Int16
       | Magic RawInt32Magic -> Rawint.Int32
       | Magic RawInt64Magic -> Rawint.Int64
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_float_precision pos l =
   let pos = string_pos "unmarshal_precision" pos in
      match l with
         Magic SingleMagic -> Rawfloat.Single
       | Magic DoubleMagic -> Rawfloat.Double
       | Magic LongDoubleMagic -> Rawfloat.LongDouble
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_signed pos l =
   let pos = string_pos "unmarshal_signed" pos in
      match l with
         Bool b -> b
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Integer set marshaling.
 *)
let marshal_int_bound = function
   Infinity ->
      List []
 | Open i ->
      List [Bool false; Int i]
 | Closed i ->
      List [Bool true; Int i]

let unmarshal_int_bound pos l =
   let pos = string_pos "unmarshal_int_bound" pos in
      match l with
         List [] -> Infinity
       | List [Bool false; Int i] -> Open i
       | List [Bool true; Int i] -> Closed i
       | _ -> raise (FirException (pos, MarshalError))

let marshal_int_interval left right =
   let left = marshal_int_bound left in
   let right = marshal_int_bound right in
      List [left; right]

let unmarshal_int_interval pos l =
   let pos = string_pos "unmarshal_int_interval" pos in
      match l with
         List [left; right] ->
            unmarshal_int_bound pos left, unmarshal_int_bound pos right
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_int_set s =
   let l =
      IntSet.fold (fun l left right ->
            let interval = marshal_int_interval left right in
               interval :: l) [] s
   in
      List [Magic IntSetMagic; List l]

let unmarshal_int_set pos l =
   let pos = string_pos "unmarshal_int_set" pos in
      List.fold_left (fun set interval ->
            let left, right = unmarshal_int_interval pos interval in
            let set' = IntSet.of_interval left right in
               IntSet.union set set') IntSet.empty l

(*
 * Integer set marshaling.
 *)
let marshal_rawint_bound = function
   Infinity ->
      List []
 | Open i ->
      List [Bool false; Rawint i]
 | Closed i ->
      List [Bool true; Rawint i]

let unmarshal_rawint_bound pos l =
   let pos = string_pos "unmarshal_rawint_bound" pos in
      match l with
         List [] -> Infinity
       | List [Bool false; Rawint i] -> Open i
       | List [Bool true; Rawint i] -> Closed i
       | _ -> raise (FirException (pos, MarshalError))

let marshal_rawint_interval left right =
   let left = marshal_rawint_bound left in
   let right = marshal_rawint_bound right in
      List [left; right]

let unmarshal_rawint_interval pos l =
   let pos = string_pos "unmarshal_rawint_rawinterval" pos in
      match l with
         List [left; right] ->
            unmarshal_rawint_bound pos left, unmarshal_rawint_bound pos right
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_rawint_set s =
   let pre = RawIntSet.precision s in
   let signed = RawIntSet.signed s in
   let l =
      RawIntSet.fold (fun l left right ->
            let interval = marshal_rawint_interval left right in
               interval :: l) [] s
   in
      List [Magic RawIntSetMagic;
            marshal_int_precision pre;
            marshal_signed signed;
            List l]

let unmarshal_rawint_set pos pre signed l =
   let pos = string_pos "unmarshal_rawint_set" pos in
   let pre = unmarshal_int_precision pos pre in
   let signed = unmarshal_signed pos signed in
      List.fold_left (fun set interval ->
            let left, right = unmarshal_rawint_interval pos interval in
            let set' = RawIntSet.of_interval pre signed left right in
               RawIntSet.union set set') (RawIntSet.empty pre signed) l

(*
 * Generic set marshaling.
 *)
let marshal_set = function
   IntSet s ->
      marshal_int_set s
 | RawIntSet s ->
      marshal_rawint_set s

let unmarshal_set pos l =
   let pos = string_pos "unmarshal_set" pos in
      match l with
         List [Magic RawIntSetMagic; pre; signed; List l] ->
            RawIntSet (unmarshal_rawint_set pos pre signed l)
       | List [Magic IntSetMagic; List l] ->
            IntSet (unmarshal_int_set pos l)
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Tuple classes.
 *)
let marshal_tuple_class = function
   NormalTuple -> Magic NormalTupleMagic
 | RawTuple -> Magic RawTupleMagic
 | BoxTuple -> Magic BoxTupleMagic

let unmarshal_tuple_class pos l =
   let pos = string_pos "unmarshal_tuple_class" pos in
      match l with
         Magic NormalTupleMagic ->
            NormalTuple
       | Magic RawTupleMagic ->
            RawTuple
       | Magic BoxTupleMagic ->
            BoxTuple
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Mutability.
 *)
let marshal_mutable_flag senv b =
   match b with
      Mutable ->
         senv, Magic MutableMagic
    | Immutable ->
         senv, Magic ImmutableMagic
    | MutableDelayed ->
         senv, Magic MutableDelayedMagic
    | MutableVar v ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic MutableVarMagic; v]

let unmarshal_mutable_flag senv pos l =
   let pos = string_pos "unmarshal_mutable_flag" pos in
      match l with
         Magic MutableMagic ->
            Mutable
       | Magic ImmutableMagic ->
            Immutable
       | Magic MutableDelayedMagic ->
            MutableDelayed
       | List [Magic MutableVarMagic; v] ->
            MutableVar (unmarshal_symbol senv pos v)
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Subscript operators.
 *)
let marshal_sub_block block_op =
   match block_op with
      BlockSub -> Magic BlockSubMagic
    | RawDataSub -> Magic RawDataSubMagic
    | RawTupleSub -> Magic RawTupleSubMagic
    | TupleSub -> Magic TupleSubMagic

let marshal_sub_index sub_index =
   match sub_index with
      ByteIndex -> Magic ByteIndexMagic
    | WordIndex -> Magic WordIndexMagic

let marshal_sub_script sub =
   match sub with
      IntIndex ->
         Magic IntIndexMagic
    | RawIntIndex (pre, signed) ->
         List [Magic RawIntIndexMagic;
               marshal_int_precision pre;
               marshal_signed signed]

(*
 * Locations.
 *)
let marshal_loc senv loc =
   let name, start_line, start_char, end_line, end_char = dest_loc loc in
   let senv, name = marshal_symbol senv name in
      senv, List [Magic LocationMagic; name; Int start_line; Int start_char; Int end_line; Int end_char]

let unmarshal_loc senv pos l =
   let pos = string_pos "unmarshal_loc" pos in
      match l with
         List [Magic LocationMagic; name; Int start_line; Int start_char; Int end_line; Int end_char] ->
            let name = unmarshal_symbol senv pos name in
               create_loc name start_line start_char end_line end_char
       | _ ->
            raise (FirException (pos, MarshalError))

let rec marshal_sub_value senv value_op =
   match value_op with
      PolySub ->
         senv, Magic PolySubMagic
    | EnumSub n ->
         senv, List [Magic EnumSubMagic; Int n]
    | IntSub ->
         senv, Magic IntSubMagic
    | TagSub (ty_var, tyl) ->
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, tyl = marshal_mutable_type_fields senv tyl in
            senv, List [Magic TagSubMagic; ty_var; tyl]
    | RawIntSub (pre, signed) ->
         senv, List [Magic RawIntSubMagic;
                     marshal_int_precision pre;
                     marshal_signed signed]
    | RawFloatSub pre ->
         senv, List [Magic RawFloatSubMagic;
                     marshal_float_precision pre]
    | BlockPointerSub ->
         senv, Magic BlockPointerSubMagic
    | RawPointerSub ->
         senv, Magic RawPointerSubMagic
    | PointerInfixSub ->
         senv, Magic PointerInfixSubMagic
    | FunctionSub ->
         senv, Magic FunctionSubMagic

and marshal_subscript_op senv subop =
   let { sub_block = sub_block;
         sub_value = sub_value;
         sub_index = sub_index;
         sub_script = sub_script
       } = subop
   in
   let senv, sub_value = marshal_sub_value senv sub_value in
   let l =
      List [Magic SubOpMagic;
            marshal_sub_block sub_block;
            sub_value;
            marshal_sub_index sub_index;
            marshal_sub_script sub_script]
   in
      senv, l

(*
 * Type marshaling.
 *)
and marshal_type senv ty =
   match ty with
      TyInt ->
         senv, Magic TyIntMagic
    | TyEnum n ->
         senv, List [Magic TyEnumMagic; Int n]
    | TyRawInt (p, s) ->
         senv, List [Magic TyRawIntMagic; marshal_int_precision p; marshal_signed s]
    | TyFloat p ->
         senv, List [Magic TyFloatMagic; marshal_float_precision p]
    | TyFun (tyl, ty) ->
         let senv, tyl = marshal_type_list senv tyl in
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TyFunMagic; tyl; ty]
    | TyRawData ->
         senv, Magic TyRawDataMagic
    | TyUnion (v, tyl, set) ->
         let senv, v = marshal_symbol senv v in
         let senv, tyl = marshal_type_list senv tyl in
         let set = marshal_int_set set in
         let l = [Magic TyUnionMagic; v; tyl; set] in
            senv, List l
    | TyTuple (tclass, tyl) ->
         let tclass = marshal_tuple_class tclass in
         let senv, tyl = marshal_mutable_type_fields senv tyl in
            senv, List [Magic TyTupleMagic; tclass; tyl]
    | TyDTuple (ty_var, tyl_opt) ->
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, tyl_opt = marshal_mutable_type_fields_opt senv tyl_opt in
            senv, List [Magic TyDTupleMagic; ty_var; tyl_opt]
    | TyTag (ty_var, tyl) ->
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, tyl = marshal_mutable_type_fields senv tyl in
            senv, List [Magic TyTagMagic; ty_var; tyl]
    | TyArray ty ->
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TyArrayMagic; ty]
    | TyVar v ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic TyVarMagic; v]
    | TyApply (v, tyl) ->
         let senv, v = marshal_symbol senv v in
         let senv, tyl = marshal_type_list senv tyl in
            senv, List [Magic TyApplyMagic; v; tyl]
    | TyExists (vars, ty) ->
         let senv, vars = marshal_symbol_list senv vars in
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TyExistsMagic; vars; ty]
    | TyAll (vars, ty) ->
         let senv, vars = marshal_symbol_list senv vars in
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TyAllMagic; vars; ty]
    | TyProject (v, i) ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic TyProjectMagic; v; Int i]
    | TyCase ty ->
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TyCaseMagic; ty]
    | TyObject (v, ty) ->
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TyObjectMagic; v; ty]
    | TyPointer op ->
         let op = marshal_sub_block op in
            senv, List [Magic TyPointerMagic; op]
    | TyFrame (v, tyl) ->
         let senv, v = marshal_symbol senv v in
         let senv, tyl = marshal_type_list senv tyl in
            senv, List [Magic TyFrameMagic; v; tyl]
    | TyDelayed ->
         senv, Magic TyDelayedMagic

and marshal_type_list senv tyl =
   let senv, tyl =
      List.fold_left (fun (senv, tyl) ty ->
            let senv, ty = marshal_type senv ty in
               senv, ty :: tyl) (senv, []) tyl
   in
      senv, List (List.rev tyl)

and marshal_mutable_type_field senv (ty, b) =
   let senv, ty = marshal_type senv ty in
   let senv, b = marshal_mutable_flag senv b in
      senv, List [ty; b]

and marshal_mutable_type_fields senv fields =
   let senv, fields =
      List.fold_left (fun (senv, fields) field ->
            let senv, field = marshal_mutable_type_field senv field in
               senv, field :: fields) (senv, []) fields
   in
      senv, List (List.rev fields)

and marshal_mutable_type_fields_opt senv fields_opt =
   match fields_opt with
      Some fields ->
         let senv, fields = marshal_mutable_type_fields senv fields in
            senv, List [Magic SomeMagic; fields]
    | None ->
         senv, Magic NoneMagic

 (*
  * Subscript values.
  *)
let unmarshal_sub_block pos l =
   match l with
      Magic BlockSubMagic -> BlockSub
    | Magic RawDataSubMagic -> RawDataSub
    | Magic RawTupleSubMagic -> RawTupleSub
    | Magic TupleSubMagic -> TupleSub
    | _ -> raise (FirException (pos, MarshalError))

let unmarshal_sub_index pos l =
   match l with
      Magic ByteIndexMagic -> ByteIndex
    | Magic WordIndexMagic -> WordIndex
    | _ -> raise (FirException (pos, MarshalError))

let unmarshal_sub_script pos l =
   match l with
      Magic IntIndexMagic ->
         IntIndex
    | List [Magic RawIntIndexMagic; pre; signed] ->
         RawIntIndex (unmarshal_int_precision pos pre, unmarshal_signed pos signed)
    | _ ->
         raise (FirException (pos, MarshalError))

(*
 * Subscript values.
 *)
let rec unmarshal_sub_value senv pos l =
   match l with
      Magic PolySubMagic ->
         PolySub
    | List [Magic EnumSubMagic; Int n] ->
         EnumSub n
    | Magic IntSubMagic ->
         IntSub
    | List [Magic TagSubMagic; ty_var; tyl] ->
         let ty_var = unmarshal_symbol senv pos ty_var in
         let tyl = unmarshal_mutable_type_fields senv pos tyl in
            TagSub (ty_var, tyl)
    | List [Magic RawIntSubMagic; pre; signed] ->
         RawIntSub (unmarshal_int_precision pos pre, unmarshal_signed pos signed)
    | List [Magic RawFloatSubMagic; pre] ->
         RawFloatSub (unmarshal_float_precision pos pre)
    | Magic BlockPointerSubMagic ->
         BlockPointerSub
    | Magic RawPointerSubMagic ->
         RawPointerSub
    | Magic PointerInfixSubMagic ->
         PointerInfixSub
    | Magic FunctionSubMagic ->
         FunctionSub
    | _ ->
         raise (FirException (pos, MarshalError))

and unmarshal_subscript_op senv pos l =
   let pos = string_pos "unmarshal_subscript_op" pos in
      match l with
         List [Magic SubOpMagic; block_op; value_op; index_op; script_op] ->
            { sub_block = unmarshal_sub_block pos block_op;
              sub_value = unmarshal_sub_value senv pos value_op;
              sub_index = unmarshal_sub_index pos index_op;
              sub_script = unmarshal_sub_script pos script_op
            }
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Unmarshal types.
 *)
and unmarshal_type senv pos l =
   let pos = string_pos "unmarshal_type" pos in
      match l with
         Magic TyIntMagic ->
            TyInt
       | List [Magic TyEnumMagic; Int n] ->
            TyEnum n
       | List [Magic TyRawIntMagic; pre; signed] ->
            let pre = unmarshal_int_precision pos pre in
            let signed = unmarshal_signed pos signed in
               TyRawInt (pre, signed)
       | List [Magic TyFloatMagic; pre] ->
            let pre = unmarshal_float_precision pos pre in
               TyFloat pre
       | List [Magic TyFunMagic; tyl; ty] ->
            let tyl = unmarshal_type_list senv pos tyl in
            let ty = unmarshal_type senv pos ty in
               TyFun (tyl, ty)
       | Magic TyRawDataMagic ->
            TyRawData
       | List [Magic TyUnionMagic; v; tyl; List set] ->
            let v = unmarshal_symbol senv pos v in
            let tyl = unmarshal_type_list senv pos tyl in
            let set = unmarshal_int_set pos set in
               TyUnion (v, tyl, set)
       | List [Magic TyTupleMagic; tclass; tyl] ->
            let tclass = unmarshal_tuple_class pos tclass in
            let tyl = unmarshal_mutable_type_fields senv pos tyl in
               TyTuple (tclass, tyl)
       | List [Magic TyDTupleMagic; ty_var; ty_fields] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
            let ty_fields = unmarshal_mutable_type_fields_opt senv pos ty_fields in
               TyDTuple (ty_var, ty_fields)
       | List [Magic TyTagMagic; ty_var; ty_fields] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
            let ty_fields = unmarshal_mutable_type_fields senv pos ty_fields in
               TyTag (ty_var, ty_fields)
       | List [Magic TyArrayMagic; ty] ->
            let ty = unmarshal_type senv pos ty in
               TyArray ty
       | List [Magic TyVarMagic; v] ->
            let v = unmarshal_symbol senv pos v in
               TyVar v
       | List [Magic TyApplyMagic; v; tyl] ->
            let v = unmarshal_symbol senv pos v in
            let tyl = unmarshal_type_list senv pos tyl in
               TyApply (v, tyl)
       | List [Magic TyExistsMagic; vars; ty] ->
            let vars = unmarshal_symbol_list senv pos vars in
            let ty = unmarshal_type senv pos ty in
               TyExists (vars, ty)
       | List [Magic TyAllMagic; vars; ty] ->
            let vars = unmarshal_symbol_list senv pos vars in
            let ty = unmarshal_type senv pos ty in
               TyAll (vars, ty)
       | List [Magic TyProjectMagic; v; Int i] ->
            let v = unmarshal_symbol senv pos v in
               TyProject (v, i)
       | List [Magic TyCaseMagic; ty] ->
            let ty = unmarshal_type senv pos ty in
               TyCase ty
       | List [Magic TyObjectMagic; v; ty] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
               TyObject (v, ty)
       | List [Magic TyPointerMagic; op] ->
            let op = unmarshal_sub_block pos op in
               TyPointer op
       | List [Magic TyFrameMagic; v; tyl] ->
            let v = unmarshal_symbol senv pos v in
            let tyl = unmarshal_type_list senv pos tyl in
               TyFrame (v, tyl)
       | Magic TyDelayedMagic ->
            TyDelayed
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_type_list senv pos l : ty list =
   let pos = string_pos "unmarshal_type_list" pos in
      match l with
         List l ->
            List.map (unmarshal_type senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_mutable_type_field senv pos l =
   let pos = string_pos "unmarshal_mutable_type_field" pos in
      match l with
         List [ty; b] ->
            let ty = unmarshal_type senv pos ty in
            let b = unmarshal_mutable_flag senv pos b in
               ty, b
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_mutable_type_fields senv pos l =
   let pos = string_pos "unmarshal_mutable_type_fields" pos in
      match l with
         List l ->
            List.map (unmarshal_mutable_type_field senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_mutable_type_fields_opt senv pos l =
   let pos = string_pos "unmarshal_mutable_type_fields_opt" pos in
      match l with
         List [Magic SomeMagic; ty_fields] ->
            Some (unmarshal_mutable_type_fields senv pos ty_fields)
       | Magic NoneMagic ->
            None
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Mutable types.
 *)
let marshal_union_fields_list senv fields =
   let senv, fields =
      List.fold_left (fun (senv, fields) field ->
            let senv, field = marshal_mutable_type_fields senv field in
               senv, field :: fields) (senv, []) fields
   in
      senv, List (List.rev fields)

(*
 * Unions.
 *)
let unmarshal_union_fields_list senv pos l =
   let pos = string_pos "unmarshal_union_fields_list" pos in
      match l with
         List l ->
            List.map (unmarshal_mutable_type_fields senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Type defs.
 *)
let marshal_tydef senv tydef =
   match tydef with
      TyDefUnion (vars, fields) ->
         let senv, vars = marshal_symbol_list senv vars in
         let senv, fields = marshal_union_fields_list senv fields in
            senv, List [Magic TydefUnionMagic; vars; fields]
    | TyDefLambda (vars, ty) ->
         let senv, vars = marshal_symbol_list senv vars in
         let senv, ty = marshal_type senv ty in
            senv, List [Magic TydefLambdaMagic; vars; ty]
    | TyDefDTuple v ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic TydefDTupleMagic; v]

let unmarshal_tydef senv pos l =
   let pos = string_pos "unmarshal_tydef" pos in
      match l with
         List [Magic TydefUnionMagic; vars; fields] ->
            let vars = unmarshal_symbol_list senv pos vars in
            let fields = unmarshal_union_fields_list senv pos fields in
               TyDefUnion (vars, fields)
       | List [Magic TydefLambdaMagic; vars; ty] ->
            let vars = unmarshal_symbol_list senv pos vars in
            let ty = unmarshal_type senv pos ty in
               TyDefLambda (vars, ty)
       | List [Magic TydefDTupleMagic; ty_var] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
               TyDefDTuple ty_var
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Rawint operators with precision and signed flag.
 *)
let marshal_enum_op op n =
   List [Magic op; Int n]

let marshal_rawint_op op p s =
   List [Magic op; marshal_int_precision p; marshal_signed s]

let unmarshal_rawint_op pos op p s =
   let p = unmarshal_int_precision pos p in
   let s = unmarshal_signed pos s in
      op p s

let marshal_rawfloat_op op p =
   List [Magic op; marshal_float_precision p]

let unmarshal_rawfloat_op pos op p =
   let p = unmarshal_float_precision pos p in
      op p

(*
 * Operators.
 *)
let marshal_unop senv op =
   match op with
      NotEnumOp n ->
         senv, List [Magic NotEnumOpMagic; Int n]
    | UMinusIntOp ->
         senv, Magic UMinusIntOpMagic
    | NotIntOp ->
         senv, Magic NotIntOpMagic
    | AbsIntOp ->
         senv, Magic AbsIntOpMagic
    | UMinusRawIntOp (p, s) ->
         senv, marshal_rawint_op UMinusRawIntOpMagic p s
    | NotRawIntOp (p, s) ->
         senv, marshal_rawint_op NotRawIntOpMagic p s
    | RawIntOfPointerOp (p, s) ->
         senv, marshal_rawint_op RawIntOfPointerOpMagic p s
    | PointerOfRawIntOp (p, s) ->
         senv, marshal_rawint_op PointerOfRawIntOpMagic p s
    | UMinusFloatOp p ->
         senv, marshal_rawfloat_op UMinusFloatOpMagic p
    | AbsFloatOp p ->
         senv, marshal_rawfloat_op AbsFloatOpMagic p
    | SinFloatOp p ->
	 senv, marshal_rawfloat_op SinFloatOpMagic p
    | CosFloatOp p ->
	 senv, marshal_rawfloat_op CosFloatOpMagic p
    | TanFloatOp p ->
	 senv, marshal_rawfloat_op TanFloatOpMagic p
    | ASinFloatOp p ->
	 senv, marshal_rawfloat_op ASinFloatOpMagic p
    | ACosFloatOp p ->
	 senv, marshal_rawfloat_op ACosFloatOpMagic p
    | ATanFloatOp p ->
	 senv, marshal_rawfloat_op ATanFloatOpMagic p
    | SinHFloatOp p ->
	 senv, marshal_rawfloat_op SinHFloatOpMagic p
    | CosHFloatOp p ->
	 senv, marshal_rawfloat_op CosHFloatOpMagic p
    | TanHFloatOp p ->
	 senv, marshal_rawfloat_op TanHFloatOpMagic p
    | ExpFloatOp p ->
	 senv, marshal_rawfloat_op ExpFloatOpMagic p
    | LogFloatOp p ->
	 senv, marshal_rawfloat_op LogFloatOpMagic p
    | Log10FloatOp p ->
	 senv, marshal_rawfloat_op Log10FloatOpMagic p
    | SqrtFloatOp p ->
	 senv, marshal_rawfloat_op SqrtFloatOpMagic p
    | CeilFloatOp p ->
	 senv, marshal_rawfloat_op CeilFloatOpMagic p
    | FloorFloatOp p ->
	 senv, marshal_rawfloat_op FloorFloatOpMagic p
    | IntOfFloatOp p ->
         senv, List [Magic IntOfFloatOpMagic;
                     marshal_float_precision p]
    | FloatOfIntOp p ->
         senv, marshal_rawfloat_op FloatOfIntOpMagic p
    | FloatOfFloatOp (p1, p2) ->
         senv, List [Magic FloatOfFloatOpMagic;
                     marshal_float_precision p1;
                     marshal_float_precision p2]
    | IntOfRawIntOp (p, s) ->
         senv, List [Magic IntOfRawIntOpMagic;
                     marshal_int_precision p;
                     marshal_signed s]
    | RawIntOfIntOp (p, s) ->
         senv, List [Magic RawIntOfIntOpMagic;
                     marshal_int_precision p;
                     marshal_signed s]
    | RawIntOfEnumOp (p, s, n) ->
         senv, List [Magic RawIntOfEnumOpMagic;
                     marshal_int_precision p;
                     marshal_signed s;
                     Int n]
    | RawIntOfFloatOp (p1, s, p2) ->
         senv, List [Magic RawIntOfFloatOpMagic;
                     marshal_int_precision p1;
                     marshal_signed s;
                     marshal_float_precision p2]
    | FloatOfRawIntOp (p1, p2, s) ->
         senv, List [Magic FloatOfRawIntOpMagic;
                     marshal_float_precision p1;
                     marshal_int_precision p2;
                     marshal_signed s]
    | RawIntOfRawIntOp (p1, s1, p2, s2) ->
         senv, List [Magic RawIntOfRawIntOpMagic;
                     marshal_int_precision p1;
                     marshal_signed s1;
                     marshal_int_precision p2;
                     marshal_signed s2]
    | RawBitFieldOp (p, s, off, len) ->
         senv, List [Magic RawBitFieldOpMagic;
                     marshal_int_precision p;
                     marshal_signed s;
                     Int off;
                     Int len]
    | PointerOfBlockOp op ->
         senv, List [Magic PointerOfBlockOpMagic;
                     marshal_sub_block op]
    | LengthOfBlockOp (subop, ty) ->
         let senv, ty = marshal_type senv ty in
         let senv, subop = marshal_subscript_op senv subop in
            senv, List [Magic LengthOfBlockOpMagic; subop; ty]
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, ty_fields = marshal_mutable_type_fields senv ty_fields in
            senv, List [Magic DTupleOfDTupleOpMagic; ty_var; ty_fields]
    | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, tyl = marshal_type_list senv tyl in
         let s1 = marshal_int_set s1 in
         let s2 = marshal_int_set s2 in
            senv, List [Magic UnionOfUnionOpMagic; ty_var; tyl; s1; s2]
    | RawDataOfFrameOp (ty_var, ty_fields) ->
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, ty_fields = marshal_type_list senv ty_fields in
            senv, List [Magic RawDataOfFrameOpMagic; ty_var; ty_fields]


let marshal_binop senv op =
   match op with
      AndEnumOp n ->
         senv, marshal_enum_op AndEnumOpMagic n
    | OrEnumOp n ->
         senv, marshal_enum_op OrEnumOpMagic n
    | XorEnumOp n ->
         senv, marshal_enum_op XorEnumOpMagic n
    | PlusIntOp ->
         senv, Magic PlusIntOpMagic
    | MinusIntOp ->
         senv, Magic MinusIntOpMagic
    | MulIntOp ->
         senv, Magic MulIntOpMagic
    | DivIntOp ->
         senv, Magic DivIntOpMagic
    | RemIntOp ->
         senv, Magic RemIntOpMagic
    | LslIntOp ->
         senv, Magic LslIntOpMagic
    | LsrIntOp ->
         senv, Magic LsrIntOpMagic
    | AsrIntOp ->
         senv, Magic AsrIntOpMagic
    | AndIntOp ->
         senv, Magic AndIntOpMagic
    | OrIntOp ->
         senv, Magic OrIntOpMagic
    | XorIntOp ->
         senv, Magic XorIntOpMagic
    | MinIntOp ->
         senv, Magic MinIntOpMagic
    | MaxIntOp ->
         senv, Magic MaxIntOpMagic
    | EqIntOp ->
         senv, Magic EqIntOpMagic
    | NeqIntOp ->
         senv, Magic NeqIntOpMagic
    | LtIntOp ->
         senv, Magic LtIntOpMagic
    | LeIntOp ->
         senv, Magic LeIntOpMagic
    | GtIntOp ->
         senv, Magic GtIntOpMagic
    | GeIntOp ->
         senv, Magic GeIntOpMagic
    | CmpIntOp ->
         senv, Magic CmpIntOpMagic

    | PlusRawIntOp (p, s) ->
         senv, marshal_rawint_op PlusRawIntOpMagic p s
    | MinusRawIntOp (p, s) ->
         senv, marshal_rawint_op MinusRawIntOpMagic p s
    | MulRawIntOp (p, s) ->
         senv, marshal_rawint_op MulRawIntOpMagic p s
    | DivRawIntOp (p, s) ->
         senv, marshal_rawint_op DivRawIntOpMagic p s
    | RemRawIntOp (p, s) ->
         senv, marshal_rawint_op RemRawIntOpMagic p s
    | SlRawIntOp (p, s) ->
         senv, marshal_rawint_op SlRawIntOpMagic p s
    | SrRawIntOp (p, s) ->
         senv, marshal_rawint_op SrRawIntOpMagic p s
    | AndRawIntOp (p, s) ->
         senv, marshal_rawint_op AndRawIntOpMagic p s
    | OrRawIntOp  (p, s) ->
         senv, marshal_rawint_op OrRawIntOpMagic p s
    | XorRawIntOp (p, s) ->
         senv, marshal_rawint_op XorRawIntOpMagic p s
    | MinRawIntOp (p, s) ->
         senv, marshal_rawint_op MinRawIntOpMagic p s
    | MaxRawIntOp (p, s) ->
         senv, marshal_rawint_op MaxRawIntOpMagic p s
    | EqRawIntOp (p, s) ->
         senv, marshal_rawint_op EqRawIntOpMagic p s
    | NeqRawIntOp (p, s) ->
         senv, marshal_rawint_op NeqRawIntOpMagic p s
    | LtRawIntOp (p, s) ->
         senv, marshal_rawint_op LtRawIntOpMagic p s
    | LeRawIntOp (p, s) ->
         senv, marshal_rawint_op LeRawIntOpMagic p s
    | GtRawIntOp (p, s) ->
         senv, marshal_rawint_op GtRawIntOpMagic p s
    | GeRawIntOp (p, s) ->
         senv, marshal_rawint_op GeRawIntOpMagic p s
    | CmpRawIntOp (p, s) ->
         senv, marshal_rawint_op CmpRawIntOpMagic p s

    | RawSetBitFieldOp (p, s, off, len) ->
         senv, List [Magic RawSetBitFieldOpMagic;
                     marshal_int_precision p;
                     marshal_signed s;
                     Int off;
                     Int len]

      (* Floats *)
    | PlusFloatOp p ->
         senv, marshal_rawfloat_op PlusFloatOpMagic p
    | MinusFloatOp p ->
         senv, marshal_rawfloat_op MinusFloatOpMagic p
    | MulFloatOp p ->
         senv, marshal_rawfloat_op MulFloatOpMagic p
    | DivFloatOp p ->
         senv, marshal_rawfloat_op DivFloatOpMagic p
    | RemFloatOp p ->
         senv, marshal_rawfloat_op RemFloatOpMagic p
    | MinFloatOp p ->
         senv, marshal_rawfloat_op MinFloatOpMagic p
    | MaxFloatOp p ->
         senv, marshal_rawfloat_op MaxFloatOpMagic p

    | EqFloatOp p ->
         senv, marshal_rawfloat_op EqFloatOpMagic p
    | NeqFloatOp p ->
         senv, marshal_rawfloat_op NeqFloatOpMagic p
    | LtFloatOp p ->
         senv, marshal_rawfloat_op LtFloatOpMagic p
    | LeFloatOp p ->
         senv, marshal_rawfloat_op LeFloatOpMagic p
    | GtFloatOp p ->
         senv, marshal_rawfloat_op GtFloatOpMagic p
    | GeFloatOp p ->
         senv, marshal_rawfloat_op GeFloatOpMagic p
    | CmpFloatOp p ->
         senv, marshal_rawfloat_op CmpFloatOpMagic p
    | ATan2FloatOp p ->
	 senv, marshal_rawfloat_op ATan2FloatOpMagic p
    | PowerFloatOp p ->
	 senv, marshal_rawfloat_op PowerFloatOpMagic p
    | LdExpFloatIntOp p ->
         senv, marshal_rawfloat_op LdExpFloatIntOpMagic p

      (* Pointer equality *)
    | EqEqOp ty ->
         let senv, ty = marshal_type senv ty in
            senv, List [Magic EqEqOpMagic; ty]
    | NeqEqOp ty ->
         let senv, ty = marshal_type senv ty in
            senv, List [Magic NeqEqOpMagic; ty]
    | PlusPointerOp (op, p, s) ->
         let op = marshal_sub_block op in
         let p = marshal_int_precision p in
         let s = marshal_signed s in
            senv, List [Magic PlusPointerOpMagic; op; p; s]

let unmarshal_unop senv pos l =
   let pos = string_pos "unmarshal_unop" pos in
      match l with
         List [Magic NotEnumOpMagic; Int n] -> NotEnumOp n
       | Magic UMinusIntOpMagic -> UMinusIntOp
       | Magic NotIntOpMagic   -> NotIntOp
       | List [Magic UMinusRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> UMinusRawIntOp (p, s)) pre signed
       | List [Magic NotRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> NotRawIntOp (p, s)) pre signed
       | List [Magic RawIntOfPointerOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> RawIntOfPointerOp (p, s)) pre signed
       | List [Magic UMinusFloatOpMagic; pre] -> UMinusFloatOp (unmarshal_float_precision pos pre)
       | List [Magic AbsFloatOpMagic; pre] -> AbsFloatOp (unmarshal_float_precision pos pre)
       | List [Magic SinFloatOpMagic; pre] -> SinFloatOp (unmarshal_float_precision pos pre)
       | List [Magic CosFloatOpMagic; pre] -> CosFloatOp (unmarshal_float_precision pos pre)
       | List [Magic TanFloatOpMagic; pre] -> TanFloatOp (unmarshal_float_precision pos pre)
       | List [Magic ASinFloatOpMagic; pre] -> ASinFloatOp (unmarshal_float_precision pos pre)
       | List [Magic ACosFloatOpMagic; pre] -> ACosFloatOp (unmarshal_float_precision pos pre)
       | List [Magic ATanFloatOpMagic; pre] -> ATanFloatOp (unmarshal_float_precision pos pre)
       | List [Magic SinHFloatOpMagic; pre] -> SinHFloatOp (unmarshal_float_precision pos pre)
       | List [Magic CosHFloatOpMagic; pre] -> CosHFloatOp (unmarshal_float_precision pos pre)
       | List [Magic TanHFloatOpMagic; pre] -> TanHFloatOp (unmarshal_float_precision pos pre)
       | List [Magic ExpFloatOpMagic; pre] -> ExpFloatOp (unmarshal_float_precision pos pre)
       | List [Magic LogFloatOpMagic; pre] -> LogFloatOp (unmarshal_float_precision pos pre)
       | List [Magic Log10FloatOpMagic; pre] -> Log10FloatOp (unmarshal_float_precision pos pre)
       | List [Magic SqrtFloatOpMagic; pre] -> SqrtFloatOp (unmarshal_float_precision pos pre)
       | List [Magic CeilFloatOpMagic; pre] -> CeilFloatOp (unmarshal_float_precision pos pre)
       | List [Magic FloorFloatOpMagic; pre] -> FloorFloatOp (unmarshal_float_precision pos pre)
       | List [Magic IntOfFloatOpMagic; pre] -> IntOfFloatOp (unmarshal_float_precision pos pre)
       | List [Magic FloatOfIntOpMagic; pre] -> FloatOfIntOp (unmarshal_float_precision pos pre)
       | List [Magic FloatOfFloatOpMagic; pre1; pre2] ->
            let p1 = unmarshal_float_precision pos pre1 in
            let p2 = unmarshal_float_precision pos pre2 in
               FloatOfFloatOp (p1, p2)
       | List [Magic RawIntOfFloatOpMagic; pre1; signed; pre2] ->
            let p1 = unmarshal_int_precision pos pre1 in
            let s = unmarshal_signed pos signed in
            let p2 = unmarshal_float_precision pos pre2 in
               RawIntOfFloatOp (p1, s, p2)
       | List [Magic FloatOfRawIntOpMagic; pre1; pre2; signed] ->
            let p1 = unmarshal_float_precision pos pre1 in
            let s = unmarshal_signed pos signed in
            let p2 = unmarshal_int_precision pos pre2 in
               FloatOfRawIntOp (p1, p2, s)
       | List [Magic RawIntOfRawIntOpMagic; p1; s1; p2; s2] ->
            let p1 = unmarshal_int_precision pos p1 in
            let s1 = unmarshal_signed pos s1 in
            let p2 = unmarshal_int_precision pos p2 in
            let s2 = unmarshal_signed pos s2 in
               RawIntOfRawIntOp (p1, s1, p2, s2)
       | List [Magic IntOfRawIntOpMagic; p; s] ->
            let p = unmarshal_int_precision pos p in
            let s = unmarshal_signed pos s in
               IntOfRawIntOp (p, s)
       | List [Magic RawIntOfIntOpMagic; p; s] ->
            let p = unmarshal_int_precision pos p in
            let s = unmarshal_signed pos s in
               RawIntOfIntOp (p, s)
       | List [Magic RawIntOfEnumOpMagic; p; s; Int n] ->
            let p = unmarshal_int_precision pos p in
            let s = unmarshal_signed pos s in
               RawIntOfEnumOp (p, s, n)
       | List [Magic RawBitFieldOpMagic; p; s; Int off; Int len] ->
            let p = unmarshal_int_precision pos p in
            let s = unmarshal_signed pos s in
               RawBitFieldOp (p, s, off, len)
       | List [Magic PointerOfBlockOpMagic; op] ->
            let op = unmarshal_sub_block pos op in
               PointerOfBlockOp op
       | List [Magic LengthOfBlockOpMagic; subop; ty] ->
            let subop = unmarshal_subscript_op senv pos subop in
            let ty = unmarshal_type senv pos ty in
               LengthOfBlockOp (subop, ty)
       | List [Magic DTupleOfDTupleOpMagic; ty_var; ty_fields] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
            let ty_fields = unmarshal_mutable_type_fields senv pos ty_fields in
               DTupleOfDTupleOp (ty_var, ty_fields)
       | List [Magic UnionOfUnionOpMagic; ty_var; tyl; List s1; List s2] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
            let tyl = unmarshal_type_list senv pos tyl in
            let s1 = unmarshal_int_set pos s1 in
            let s2 = unmarshal_int_set pos s2 in
               UnionOfUnionOp (ty_var, tyl, s1, s2)
       | List [Magic RawDataOfFrameOpMagic; ty_var; ty_fields] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
            let ty_fields = unmarshal_type_list senv pos ty_fields in
               RawDataOfFrameOp (ty_var, ty_fields)
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_binop senv pos l =
   let pos = string_pos "unmarshal_binop" pos in
      match l with
         List [Magic AndEnumOpMagic; Int n] ->
            AndEnumOp n
       | List [Magic OrEnumOpMagic; Int n] ->
            OrEnumOp n
       | List [Magic XorEnumOpMagic; Int n] ->
            XorEnumOp n

       | Magic PlusIntOpMagic  -> PlusIntOp
       | Magic MinusIntOpMagic -> MinusIntOp
       | Magic MulIntOpMagic   -> MulIntOp
       | Magic DivIntOpMagic   -> DivIntOp
       | Magic RemIntOpMagic   -> RemIntOp
       | Magic LslIntOpMagic   -> LslIntOp
       | Magic LsrIntOpMagic   -> LsrIntOp
       | Magic AsrIntOpMagic   -> AsrIntOp
       | Magic AndIntOpMagic   -> AndIntOp
       | Magic OrIntOpMagic    -> OrIntOp
       | Magic XorIntOpMagic   -> XorIntOp
       | Magic MinIntOpMagic   -> MinIntOp
       | Magic MaxIntOpMagic   -> MaxIntOp
       | Magic EqIntOpMagic    -> EqIntOp
       | Magic NeqIntOpMagic   -> NeqIntOp
       | Magic LtIntOpMagic    -> LtIntOp
       | Magic LeIntOpMagic    -> LeIntOp
       | Magic GtIntOpMagic    -> GtIntOp
       | Magic GeIntOpMagic    -> GeIntOp
       | Magic CmpIntOpMagic   -> CmpIntOp

       | List [Magic PlusRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> PlusRawIntOp (p, s)) pre signed
       | List [Magic MinusRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> MinusRawIntOp (p, s)) pre signed
       | List [Magic MulRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> MulRawIntOp (p, s)) pre signed
       | List [Magic DivRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> DivRawIntOp (p, s)) pre signed
       | List [Magic RemRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> RemRawIntOp (p, s)) pre signed
       | List [Magic MinRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> MinRawIntOp (p, s)) pre signed
       | List [Magic MaxRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> MaxRawIntOp (p, s)) pre signed
       | List [Magic SlRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> SlRawIntOp (p, s)) pre signed
       | List [Magic SrRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> SrRawIntOp (p, s)) pre signed
       | List [Magic AndRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> AndRawIntOp (p, s)) pre signed
       | List [Magic OrRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> OrRawIntOp (p, s)) pre signed
       | List [Magic XorRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> XorRawIntOp (p, s)) pre signed
       | List [Magic EqRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> EqRawIntOp (p, s)) pre signed
       | List [Magic NeqRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> NeqRawIntOp (p, s)) pre signed
       | List [Magic LtRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> LtRawIntOp (p, s)) pre signed
       | List [Magic LeRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> LeRawIntOp (p, s)) pre signed
       | List [Magic GtRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> GtRawIntOp (p, s)) pre signed
       | List [Magic GeRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> GeRawIntOp (p, s)) pre signed
       | List [Magic CmpRawIntOpMagic; pre; signed] ->
            unmarshal_rawint_op pos (fun p s -> CmpRawIntOp (p, s)) pre signed
       | List [Magic RawSetBitFieldOpMagic; pre; signed; Int off; Int len] ->
            let pre = unmarshal_int_precision pos pre in
            let signed = unmarshal_signed pos signed in
               RawSetBitFieldOp (pre, signed, off, len)

         (* Floats *)
       | List [Magic PlusFloatOpMagic; pre] -> PlusFloatOp (unmarshal_float_precision pos pre)
       | List [Magic MinusFloatOpMagic; pre] -> MinusFloatOp (unmarshal_float_precision pos pre)
       | List [Magic MulFloatOpMagic; pre] -> MulFloatOp (unmarshal_float_precision pos pre)
       | List [Magic DivFloatOpMagic; pre] -> DivFloatOp (unmarshal_float_precision pos pre)
       | List [Magic RemFloatOpMagic; pre] -> RemFloatOp (unmarshal_float_precision pos pre)
       | List [Magic MinFloatOpMagic; pre] -> MinFloatOp (unmarshal_float_precision pos pre)
       | List [Magic MaxFloatOpMagic; pre] -> MaxFloatOp (unmarshal_float_precision pos pre)

       | List [Magic EqFloatOpMagic; pre] -> EqFloatOp (unmarshal_float_precision pos pre)
       | List [Magic NeqFloatOpMagic; pre] -> NeqFloatOp (unmarshal_float_precision pos pre)
       | List [Magic LtFloatOpMagic; pre] -> LtFloatOp (unmarshal_float_precision pos pre)
       | List [Magic LeFloatOpMagic; pre] -> LeFloatOp (unmarshal_float_precision pos pre)
       | List [Magic GtFloatOpMagic; pre] -> GtFloatOp (unmarshal_float_precision pos pre)
       | List [Magic GeFloatOpMagic; pre] -> GeFloatOp (unmarshal_float_precision pos pre)
       | List [Magic CmpFloatOpMagic; pre] -> CmpFloatOp (unmarshal_float_precision pos pre)
       | List [Magic ATan2FloatOpMagic; pre] -> ATan2FloatOp (unmarshal_float_precision pos pre)
       | List [Magic PowerFloatOpMagic; pre] -> PowerFloatOp (unmarshal_float_precision pos pre)
       | List [Magic LdExpFloatIntOpMagic; pre] -> LdExpFloatIntOp (unmarshal_float_precision pos pre)

         (* Pointer equality *)
       | List [Magic EqEqOpMagic; ty] ->
            let ty = unmarshal_type senv pos ty in
               EqEqOp ty
       | List [Magic NeqEqOpMagic; ty] ->
            let ty = unmarshal_type senv pos ty in
               NeqEqOp ty
       | List [Magic PlusPointerOpMagic; op; pre; signed] ->
            let op = unmarshal_sub_block pos op in
               unmarshal_rawint_op pos (fun p s -> PlusPointerOp (op, p, s)) pre signed

       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Atoms.
 *)
let rec marshal_atom senv a =
   match a with
      AtomNil ty ->
         let senv, ty = marshal_type senv ty in
            senv, List [Magic AtomNilMagic; ty]
    | AtomInt i ->
         senv, List [Magic AtomIntMagic; Int i]
    | AtomEnum (i, n) ->
         senv, List [Magic AtomEnumMagic; Int i; Int n]
    | AtomRawInt i ->
         senv, List [Magic AtomRawIntMagic; Rawint i]
    | AtomFloat x ->
         senv, List [Magic AtomFloatMagic; Rawfloat x]
    | AtomConst (ty, ty_v, i) ->
         let senv, ty = marshal_type senv ty in
         let senv, ty_v = marshal_symbol senv ty_v in
            senv, List [Magic AtomConstMagic; ty; ty_v; Int i]
    | AtomVar v ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic AtomVarMagic; v]
    | AtomFun v ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic AtomFunMagic; v]
    | AtomLabel ((frame, field, subfield), off) ->
         let senv, frame = marshal_symbol senv frame in
         let senv, field = marshal_symbol senv field in
         let senv, subfield = marshal_symbol senv subfield in
            senv, List [Magic AtomLabelMagic; frame; field; subfield; Rawint off]
    | AtomSizeof (vars, off) ->
         let senv, vars = marshal_symbol_list senv vars in
            senv, List [Magic AtomSizeofMagic; vars; Rawint off]
    | AtomTyApply (a, ty, tyl) ->
         let senv, a = marshal_atom senv a in
         let senv, ty = marshal_type senv ty in
         let senv, tyl = marshal_type_list senv tyl in
            senv, List [Magic AtomTyApplyMagic; a; ty; tyl]
    | AtomTyPack (v, ty, tyl) ->
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
         let senv, tyl = marshal_type_list senv tyl in
            senv, List [Magic AtomTyPackMagic; v; ty; tyl]
    | AtomTyUnpack v ->
         let senv, v = marshal_symbol senv v in
            senv, List [Magic AtomTyUnpackMagic; v]
    | AtomUnop (op, a) ->
         let senv, op = marshal_unop senv op in
         let senv, a = marshal_atom senv a in
            senv, List [Magic AtomUnopMagic; op; a]
    | AtomBinop (op, a1, a2) ->
         let senv, op = marshal_binop senv op in
         let senv, a1 = marshal_atom senv a1 in
         let senv, a2 = marshal_atom senv a2 in
            senv, List [Magic AtomBinopMagic; op; a1; a2]

let marshal_atom_opt senv a =
   match a with
      Some a ->
         let senv, a = marshal_atom senv a in
            senv, List [Magic SomeMagic; a]
    | None ->
         senv, Magic NoneMagic

let marshal_atom_list senv args =
   let senv, args =
      List.fold_left (fun (senv, args) a ->
            let senv, a = marshal_atom senv a in
               senv, a :: args) (senv, []) args
   in
      senv, List [Magic AtomListMagic; List (List.rev args)]

let marshal_atom_opt_list senv args =
   let senv, args =
      List.fold_left (fun (senv, args) a ->
            let senv, a = marshal_atom_opt senv a in
               senv, a :: args) (senv, []) args
   in
      senv, List [Magic AtomOptListMagic; List (List.rev args)]

let rec unmarshal_atom senv pos l =
   let pos = string_pos "unmarshal_atom" pos in
      match l with
         List [Magic AtomNilMagic; ty] ->
            let ty = unmarshal_type senv pos ty in
               AtomNil ty
       | List [Magic AtomIntMagic; Int i] ->
            AtomInt i
       | List [Magic AtomEnumMagic; Int i; Int n] ->
            AtomEnum (i, n)
       | List [Magic AtomRawIntMagic; Rawint i] ->
            AtomRawInt i
       | List [Magic AtomFloatMagic; Rawfloat x] ->
            AtomFloat x
       | List [Magic AtomConstMagic; ty; ty_v; Int i] ->
            let ty = unmarshal_type senv pos ty in
            let ty_v = unmarshal_symbol senv pos ty_v in
               AtomConst (ty, ty_v, i)
       | List [Magic AtomVarMagic; v] ->
            let v = unmarshal_symbol senv pos v in
               AtomVar v
       | List [Magic AtomFunMagic; v] ->
            let v = unmarshal_symbol senv pos v in
               AtomFun v
       | List [Magic AtomLabelMagic; frame; field; subfield; Rawint off] ->
            let frame = unmarshal_symbol senv pos frame in
            let field = unmarshal_symbol senv pos field in
            let subfield = unmarshal_symbol senv pos subfield in
               AtomLabel ((frame, field, subfield), off)
       | List [Magic AtomSizeofMagic; vars; Rawint off] ->
            let vars = unmarshal_symbol_list senv pos vars in
               AtomSizeof (vars, off)
       | List [Magic AtomTyApplyMagic; a; ty; tyl] ->
            let a = unmarshal_atom senv pos a in
            let ty = unmarshal_type senv pos ty in
            let tyl = unmarshal_type_list senv pos tyl in
               AtomTyApply (a, ty, tyl)
       | List [Magic AtomTyPackMagic; v; ty; tyl] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let tyl = unmarshal_type_list senv pos tyl in
               AtomTyPack (v, ty, tyl)
       | List [Magic AtomTyUnpackMagic; v] ->
            let v = unmarshal_symbol senv pos v in
               AtomTyUnpack v
       | List [Magic AtomUnopMagic; op; a] ->
            let op = unmarshal_unop senv pos op in
            let a = unmarshal_atom senv pos a in
               AtomUnop (op, a)
       | List [Magic AtomBinopMagic; op; a1; a2] ->
            let op = unmarshal_binop senv pos op in
            let a1 = unmarshal_atom senv pos a1 in
            let a2 = unmarshal_atom senv pos a2 in
               AtomBinop (op, a1, a2)
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_atom_opt senv pos l =
   let pos = string_pos "unmarshal_atom_opt" pos in
      match l with
         Magic NoneMagic ->
            None
       | List [Magic SomeMagic; a] ->
            let a = unmarshal_atom senv pos a in
               Some a
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_atom_list senv pos l =
   let pos = string_pos "unmarshal_atom_list" pos in
      match l with
         List [Magic AtomListMagic; List l] ->
            List.map (unmarshal_atom senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_atom_opt_list senv pos l =
   let pos = string_pos "unmarshal_atom_opt_list" pos in
      match l with
         List [Magic AtomOptListMagic; List l] ->
            List.map (unmarshal_atom_opt senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Allocation.
 *)
let marshal_alloc_op senv op =
   match op with
      AllocTuple (tclass, ty_vars, ty, args) ->
         let tclass = marshal_tuple_class tclass in
         let senv, ty_vars = marshal_symbol_list senv ty_vars in
         let senv, ty = marshal_type senv ty in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic AllocTupleMagic; tclass; ty_vars; ty; args]
    | AllocDTuple (ty, ty_var, a, args) ->
         let senv, ty = marshal_type senv ty in
         let senv, ty_var = marshal_symbol senv ty_var in
         let senv, a = marshal_atom senv a in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic AllocDTupleMagic; ty; ty_var; a; args]
    | AllocUnion (ty_vars, ty, ty_v, i, args) ->
         let senv, ty_vars = marshal_symbol_list senv ty_vars in
         let senv, ty = marshal_type senv ty in
         let senv, ty_v = marshal_symbol senv ty_v in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic AllocUnionMagic; ty_vars; ty; ty_v; Int i; args]
    | AllocArray (ty, args) ->
         let senv, ty = marshal_type senv ty in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic AllocArrayMagic; ty; args]
    | AllocVArray (ty, sub_index, a1, a2) ->
         let senv, ty = marshal_type senv ty in
         let sub_index = marshal_sub_index sub_index in
         let senv, a1 = marshal_atom senv a1 in
         let senv, a2 = marshal_atom senv a2 in
            senv, List [Magic AllocVArrayMagic; ty; sub_index; a1; a2]
    | AllocMalloc (ty, a) ->
         let senv, ty = marshal_type senv ty in
         let senv, a = marshal_atom senv a in
            senv, List [Magic AllocMallocMagic; ty; a]
    | AllocFrame (v, tyl) ->
         let senv, v = marshal_symbol senv v in
         let senv, tyl = marshal_type_list senv tyl in
            senv, List [Magic AllocFrameMagic; v; tyl]

let unmarshal_alloc_op senv pos l =
   let pos = string_pos "unmarshal_alloc_op" pos in
      match l with
         List [Magic AllocTupleMagic; tclass; ty_vars; ty; args] ->
            let tclass = unmarshal_tuple_class pos tclass in
            let ty_vars = unmarshal_symbol_list senv pos ty_vars in
            let ty = unmarshal_type senv pos ty in
            let args = unmarshal_atom_list senv pos args in
               AllocTuple (tclass, ty_vars, ty, args)
       | List [Magic AllocDTupleMagic; ty; ty_var; a; args] ->
            let ty = unmarshal_type senv pos ty in
            let ty_var = unmarshal_symbol senv pos ty_var in
            let a = unmarshal_atom senv pos a in
            let args = unmarshal_atom_list senv pos args in
               AllocDTuple (ty, ty_var, a, args)
       | List [Magic AllocUnionMagic; ty_vars; ty; ty_v; Int i; args] ->
            let ty_vars = unmarshal_symbol_list senv pos ty_vars in
            let ty = unmarshal_type senv pos ty in
            let ty_v = unmarshal_symbol senv pos ty_v in
            let args = unmarshal_atom_list senv pos args in
               AllocUnion (ty_vars, ty, ty_v, i, args)
       | List [Magic AllocArrayMagic; ty; args] ->
            let ty = unmarshal_type senv pos ty in
            let args = unmarshal_atom_list senv pos args in
               AllocArray (ty, args)
       | List [Magic AllocVArrayMagic; ty; sub_index; a1; a2] ->
            let ty = unmarshal_type senv pos ty in
            let sub_index = unmarshal_sub_index pos sub_index in
            let a1 = unmarshal_atom senv pos a1 in
            let a2 = unmarshal_atom senv pos a2 in
               AllocVArray (ty, sub_index, a1, a2)
       | List [Magic AllocMallocMagic; ty; a] ->
            let ty = unmarshal_type senv pos ty in
            let a = unmarshal_atom senv pos a in
               AllocMalloc (ty, a)
       | List [Magic AllocFrameMagic; v; tyl] ->
            let v = unmarshal_symbol senv pos v in
            let tyl = unmarshal_type_list senv pos tyl in
               AllocFrame (v, tyl)
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Debug.
 *)
let marshal_debug_var senv (v1, ty, v2) =
   let senv, v1 = marshal_symbol senv v1 in
   let senv, ty = marshal_type senv ty in
   let senv, v2 = marshal_symbol senv v2 in
      senv, List [v1; ty; v2]

let unmarshal_debug_var senv pos l =
   let pos = string_pos "unmarshal_debug_var" pos in
      match l with
         List [v1; ty; v2] ->
            let v1 = unmarshal_symbol senv pos v1 in
            let ty = unmarshal_type senv pos ty in
            let v2 = unmarshal_symbol senv pos v2 in
               v1, ty, v2
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_debug_vars senv vars =
   let senv, vars =
      List.fold_left (fun (senv, vars) v ->
            let senv, v = marshal_debug_var senv v in
               senv, v :: vars) (senv, []) vars
   in
      senv, List (List.rev vars)

let unmarshal_debug_vars senv pos l =
   let pos = string_pos "unmarshal_debug_vars" pos in
      match l with
         List l ->
            List.map (unmarshal_debug_var senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_debug_var_option senv = function
   Some info ->
      let senv, info = marshal_debug_var senv info in
         senv, List [Magic SomeMagic; info]
 | None ->
      senv, List [Magic NoneMagic]

let unmarshal_debug_var_option senv pos l =
   let pos = string_pos "unmarshal_debug_var_option" pos in
      match l with
         List [Magic NoneMagic] ->
            None
       | List [Magic SomeMagic; l] ->
            Some (unmarshal_debug_var senv pos l)
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_debug senv info =
   match info with
      DebugString s ->
         senv, List [Magic DebugStringMagic; String s]
    | DebugContext (line, vars) ->
         let senv, line = marshal_loc senv line in
         let senv, vars = marshal_debug_vars senv vars in
            senv, List [Magic DebugContextMagic; line; vars]

let unmarshal_debug senv pos l =
   let pos = string_pos "unmarshal_debug" pos in
      match l with
         List [Magic DebugStringMagic; String s] ->
            DebugString s
       | List [Magic DebugContextMagic; line; vars] ->
            let vars = unmarshal_debug_vars senv pos vars in
            let line = unmarshal_loc senv pos line in
               DebugContext (line, vars)
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Predicates.
 *)
let marshal_pred senv pred =
   match pred with
      IsMutable v ->
         let senv, v = marshal_atom senv v in
            senv, List [Magic IsMutableMagic; v]
    | Reserve (s1, a2) ->
         let senv, a1 = marshal_atom senv s1 in
         let senv, a2 = marshal_atom senv a2 in
            senv, List [Magic ReserveMagic; a1; a2]
    | BoundsCheck (op, v, a1, a2) ->
         let senv, op = marshal_subscript_op senv op in
         let senv, v = marshal_atom senv v in
         let senv, a1 = marshal_atom senv a1 in
         let senv, a2 = marshal_atom senv a2 in
            senv, List [Magic BoundsCheckMagic; op; v; a1; a2]
    | ElementCheck (ty, op, v, a) ->
         let senv, ty = marshal_type senv ty in
         let senv, op = marshal_subscript_op senv op in
         let senv, v = marshal_atom senv v in
         let senv, a = marshal_atom senv a in
            senv, List [Magic ElementCheckMagic; ty; op; v; a]

let unmarshal_pred senv pos l =
   let pos = string_pos "unmarshal_pred" pos in
      match l with
         List [Magic IsMutableMagic; v] ->
            let v = unmarshal_atom senv pos v in
               IsMutable v
       | List [Magic ReserveMagic; a1; a2] ->
            let a1 = unmarshal_atom senv pos a1 in
            let a2 = unmarshal_atom senv pos a2 in
               Reserve (a1, a2)
       | List [Magic BoundsCheckMagic; op; v; a1; a2] ->
            let op = unmarshal_subscript_op senv pos op in
            let v = unmarshal_atom senv pos v in
            let a1 = unmarshal_atom senv pos a1 in
            let a2 = unmarshal_atom senv pos a2 in
               BoundsCheck (op, v, a1, a2)
       | List [Magic ElementCheckMagic; ty; op; v; a] ->
            let ty = unmarshal_type senv pos ty in
            let op = unmarshal_subscript_op senv pos op in
            let v = unmarshal_atom senv pos v in
            let a = unmarshal_atom senv pos a in
               ElementCheck (ty, op, v, a)
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Expressions.
 *)
let rec marshal_exp senv e =
   let loc = loc_of_exp e in
   let e = dest_exp_core e in
   let senv, loc = marshal_loc senv loc in
   let senv, e = marshal_exp_core senv e in
      senv, List [Magic ExpMagic; loc; e]

and marshal_exp_core senv e =
   match e with
      LetAtom (v, ty, a, e) ->
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
         let senv, a = marshal_atom senv a in
         let senv, e = marshal_exp senv e in
            senv, List [Magic LetAtomMagic; v; ty; a; e]
    | LetExt (v, ty, s, b, ty2, ty_args, args, e) ->
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
         let senv, ty2 = marshal_type senv ty2 in
         let senv, ty_args = marshal_type_list senv ty_args in
         let senv, args = marshal_atom_list senv args in
         let senv, e = marshal_exp senv e in
            senv, List [Magic LetExtMagic; v; ty; String s; Bool b; ty2; ty_args; args; e]
    | TailCall (label, f, args) ->
         let senv, label = marshal_symbol senv label in
         let senv, f = marshal_atom senv f in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic TailCallMagic; label; f; args]
    | SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, f, args)) ->
         let senv, label = marshal_symbol senv label in
         let senv, loc_ptr = marshal_atom senv loc_ptr in
         let senv, loc_off = marshal_atom senv loc_off in
         let senv, f = marshal_atom senv f in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic TailSysMigrateMagic; label; Int id; loc_ptr; loc_off; f; args]
    | SpecialCall (label, TailAtomic (f, c, args)) ->
         let senv, label = marshal_symbol senv label in
         let senv, f = marshal_atom senv f in
         let senv, c = marshal_atom senv c in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic TailAtomicMagic; label; f; c; args]
    | SpecialCall (label, TailAtomicRollback (level, c)) ->
         let senv, label = marshal_symbol senv label in
         let senv, level = marshal_atom senv level in
         let senv, c = marshal_atom senv c in
            senv, List [Magic TailAtomicRollbackMagic; label; level; c]
    | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
         let senv, label = marshal_symbol senv label in
         let senv, level = marshal_atom senv level in
         let senv, f = marshal_atom senv f in
         let senv, args = marshal_atom_list senv args in
            senv, List [Magic TailAtomicCommitMagic; label; level; f; args]
    | Match (a, cases) ->
         let senv, a = marshal_atom senv a in
         let senv, cases = marshal_match_cases senv cases in
            senv, List [Magic MatchMagic; a; cases]
    | MatchDTuple (a, cases) ->
         let senv, a = marshal_atom senv a in
         let senv, cases = marshal_match_dtuple_cases senv cases in
            senv, List [Magic MatchDTupleMagic; a; cases]
    | TypeCase (a1, a2, name, v, e1, e2) ->
         let senv, a1 = marshal_atom senv a1 in
         let senv, a2 = marshal_atom senv a2 in
         let senv, name = marshal_symbol senv name in
         let senv, v = marshal_symbol senv v in
         let senv, e1 = marshal_exp senv e1 in
         let senv, e2 = marshal_exp senv e2 in
            senv, List [Magic TypeCaseMagic; a1; a2; name; v; e1; e2]
    | LetAlloc (v, op, e) ->
         let senv, v = marshal_symbol senv v in
         let senv, op = marshal_alloc_op senv op in
         let senv, e = marshal_exp senv e in
            senv, List [Magic LetAllocMagic; v; op; e]
    | LetSubscript (op, v, ty, v2, a, e) ->
         let senv, op = marshal_subscript_op senv op in
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
         let senv, v2 = marshal_atom senv v2 in
         let senv, a = marshal_atom senv a in
         let senv, e = marshal_exp senv e in
            senv, List [Magic LetSubscriptMagic; op; v; ty; v2; a; e]
    | SetSubscript (op, label, v, a1, ty, a2, e) ->
         let senv, op = marshal_subscript_op senv op in
         let senv, label = marshal_symbol senv label in
         let senv, v = marshal_atom senv v in
         let senv, a1 = marshal_atom senv a1 in
         let senv, ty = marshal_type senv ty in
         let senv, a2 = marshal_atom senv a2 in
         let senv, e = marshal_exp senv e in
            senv, List [Magic SetSubscriptMagic; op; label; v; a1; ty; a2; e]
    | LetGlobal (op, v, ty, l, e) ->
         let senv, op = marshal_sub_value senv op in
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
         let senv, l = marshal_symbol senv l in
         let senv, e = marshal_exp senv e in
            senv, List [Magic LetGlobalMagic; op; v; ty; l; e]
    | SetGlobal (op, label, v, ty, a, e) ->
         let senv, op = marshal_sub_value senv op in
         let senv, label = marshal_symbol senv label in
         let senv, v = marshal_symbol senv v in
         let senv, ty = marshal_type senv ty in
         let senv, a = marshal_atom senv a in
         let senv, e = marshal_exp senv e in
            senv, List [Magic SetGlobalMagic; op; label; v; ty; a; e]
    | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
         let senv, op = marshal_subscript_op senv op in
         let senv, label = marshal_symbol senv label in
         let senv, v1 = marshal_atom senv v1 in
         let senv, a1 = marshal_atom senv a1 in
         let senv, v2 = marshal_atom senv v2 in
         let senv, a2 = marshal_atom senv a2 in
         let senv, a3 = marshal_atom senv a3 in
         let senv, e = marshal_exp senv e in
            senv, List [Magic MemcpyMagic; op; label; v1; a1; v2; a2; a3; e]
    | Call (label, f, args, e) ->
         let senv, label = marshal_symbol senv label in
         let senv, f = marshal_atom senv f in
         let senv, args = marshal_atom_opt_list senv args in
         let senv, e = marshal_exp senv e in
            senv, List [Magic CallMagic; label; f; args; e]
    | Assert (label, pred, e) ->
         let senv, label = marshal_symbol senv label in
         let senv, pred = marshal_pred senv pred in
         let senv, e = marshal_exp senv e in
            senv, List [Magic AssertMagic; label; pred; e]
    | Debug (info, e) ->
         let senv, info = marshal_debug senv info in
         let senv, e = marshal_exp senv e in
            senv, List [Magic DebugMagic; info; e]

and marshal_match_case senv (l, s, e) =
   let senv, l = marshal_symbol senv l in
   let s = marshal_set s in
   let senv, e = marshal_exp senv e in
      senv, List [Magic MatchCaseMagic; l; s; e]

and marshal_match_cases senv cases =
   let senv, cases =
      List.fold_left (fun (senv, cases) info ->
            let senv, info = marshal_match_case senv info in
               senv, info :: cases) (senv, []) cases
   in
      senv, List (List.rev cases)

and marshal_match_dtuple_case senv (l, a_opt, e) =
   let senv, l = marshal_symbol senv l in
   let senv, a_opt = marshal_atom_opt senv a_opt in
   let senv, e = marshal_exp senv e in
      senv, List [Magic MatchDTupleCaseMagic; l; a_opt; e]

and marshal_match_dtuple_cases senv cases =
   let senv, cases =
      List.fold_left (fun (senv, cases) info ->
            let senv, info = marshal_match_dtuple_case senv info in
               senv, info :: cases) (senv, []) cases
   in
      senv, List (List.rev cases)

(*
 * Unmarshaling an expression.
 *)
let rec unmarshal_exp senv pos l =
   let pos = string_pos "unmarshal_exp" pos in
      match l with
         List [Magic ExpMagic; loc; e] ->
            let loc = unmarshal_loc senv pos loc in
            let e = unmarshal_exp_core senv pos e in
               make_exp loc e
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_exp_core senv pos l =
   let pos = string_pos "unmarshal_exp_core" pos in
      match l with
         List [Magic LetAtomMagic; v; ty; a; e] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let a = unmarshal_atom senv pos a in
            let e = unmarshal_exp senv pos e in
               LetAtom (v, ty, a, e)
       | List [Magic LetExtMagic; v; ty; String s; Bool b; ty2; ty_args; al; e] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let ty2 = unmarshal_type senv pos ty2 in
            let ty_args = unmarshal_type_list senv pos ty_args in
            let al = unmarshal_atom_list senv pos al in
            let e = unmarshal_exp senv pos e in
               LetExt (v, ty, s, b, ty2, ty_args, al, e)
       | List [Magic TailCallMagic; label; f; args] ->
            let label = unmarshal_symbol senv pos label in
            let f = unmarshal_atom senv pos f in
            let args = unmarshal_atom_list senv pos args in
               TailCall (label, f, args)
       | List [Magic TailSysMigrateMagic; label; Int id; loc_ptr; loc_off; f; args] ->
            let label = unmarshal_symbol senv pos label in
            let loc_ptr = unmarshal_atom senv pos loc_ptr in
            let loc_off = unmarshal_atom senv pos loc_off in
            let f = unmarshal_atom senv pos f in
            let args = unmarshal_atom_list senv pos args in
               SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, f, args))
       | List [Magic TailAtomicMagic; label; f; c; args] ->
            let label = unmarshal_symbol senv pos label in
            let f = unmarshal_atom senv pos f in
            let c = unmarshal_atom senv pos c in
            let args = unmarshal_atom_list senv pos args in
               SpecialCall (label, TailAtomic (f, c, args))
       | List [Magic TailAtomicRollbackMagic; label; level; c] ->
            let label = unmarshal_symbol senv pos label in
            let level = unmarshal_atom senv pos level in
            let c = unmarshal_atom senv pos c in
               SpecialCall (label, TailAtomicRollback (level, c))
       | List [Magic TailAtomicCommitMagic; label; level; f; args] ->
            let label = unmarshal_symbol senv pos label in
            let level = unmarshal_atom senv pos level in
            let f = unmarshal_atom senv pos f in
            let args = unmarshal_atom_list senv pos args in
               SpecialCall (label, TailAtomicCommit (level, f, args))
       | List [Magic MatchMagic; a; cases] ->
            let a = unmarshal_atom senv pos a in
            let cases = unmarshal_match_cases senv pos cases in
               Match (a, cases)
       | List [Magic TypeCaseMagic; a1; a2; name; v; e1; e2] ->
            let a1 = unmarshal_atom senv pos a1 in
            let a2 = unmarshal_atom senv pos a2 in
            let name = unmarshal_symbol senv pos name in
            let v = unmarshal_symbol senv pos v in
            let e1 = unmarshal_exp senv pos e1 in
            let e2 = unmarshal_exp senv pos e2 in
               TypeCase (a1, a2, name, v, e1, e2)
       | List [Magic LetAllocMagic; v; op; e] ->
            let v = unmarshal_symbol senv pos v in
            let op = unmarshal_alloc_op senv pos op in
            let e = unmarshal_exp senv pos e in
               LetAlloc (v, op, e)
       | List [Magic LetSubscriptMagic; op; v; ty; v2; a; e] ->
            let op = unmarshal_subscript_op senv pos op in
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let v2 = unmarshal_atom senv pos v2 in
            let a = unmarshal_atom senv pos a in
            let e = unmarshal_exp senv pos e in
               LetSubscript (op, v, ty, v2, a, e)
       | List [Magic SetSubscriptMagic; op; label; v; a1; ty; a2; e] ->
            let op = unmarshal_subscript_op senv pos op in
            let label = unmarshal_symbol senv pos label in
            let v = unmarshal_atom senv pos v in
            let a1 = unmarshal_atom senv pos a1 in
            let ty = unmarshal_type senv pos ty in
            let a2 = unmarshal_atom senv pos a2 in
            let e = unmarshal_exp senv pos e in
               SetSubscript (op, label, v, a1, ty, a2, e)
       | List [Magic LetGlobalMagic; op; v; ty; l; e] ->
            let op = unmarshal_sub_value senv pos op in
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let l = unmarshal_symbol senv pos l in
            let e = unmarshal_exp senv pos e in
               LetGlobal (op, v, ty, l, e)
       | List [Magic SetGlobalMagic; op; label; v; ty; a; e] ->
            let op = unmarshal_sub_value senv pos op in
            let label = unmarshal_symbol senv pos label in
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let a = unmarshal_atom senv pos a in
            let e = unmarshal_exp senv pos e in
               SetGlobal (op, label, v, ty, a, e)
       | List [Magic MemcpyMagic; op; label; v1; a1; v2; a2; a3; e] ->
            let op = unmarshal_subscript_op senv pos op in
            let label = unmarshal_symbol senv pos label in
            let v1 = unmarshal_atom senv pos v1 in
            let a1 = unmarshal_atom senv pos a1 in
            let v2 = unmarshal_atom senv pos v2 in
            let a2 = unmarshal_atom senv pos a2 in
            let a3 = unmarshal_atom senv pos a3 in
            let e = unmarshal_exp senv pos e in
               Memcpy (op, label, v1, a1, v2, a2, a3, e)
       | List [Magic CallMagic; label; f; args; e] ->
            let label = unmarshal_symbol senv pos label in
            let f = unmarshal_atom senv pos f in
            let args = unmarshal_atom_opt_list senv pos args in
            let e = unmarshal_exp senv pos e in
               Call (label, f, args, e)
       | List [Magic AssertMagic; label; pred; e] ->
            let label = unmarshal_symbol senv pos label in
            let pred = unmarshal_pred senv pos pred in
            let e = unmarshal_exp senv pos e in
               Assert (label, pred, e)
       | List [Magic DebugMagic; info; e] ->
            let info = unmarshal_debug senv pos info in
            let e = unmarshal_exp senv pos e in
               Debug (info, e)
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_match_case senv pos l =
   let pos = string_pos "unmarshal_match_case" pos in
      match l with
         List [Magic MatchCaseMagic; l; s; e] ->
            let l = unmarshal_symbol senv pos l in
            let s = unmarshal_set pos s in
            let e = unmarshal_exp senv pos e in
               l, s, e
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_match_cases senv pos l =
   let pos = string_pos "unmarshal_match_cases" pos in
      match l with
         List l ->
            List.map (unmarshal_match_case senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_dtuple_match_case senv pos l =
   let pos = string_pos "unmarshal_dtuple_match_case" pos in
      match l with
         List [Magic MatchDTupleCaseMagic; l; a_opt; e] ->
            let l = unmarshal_symbol senv pos l in
            let a_opt = unmarshal_atom_opt senv pos a_opt in
            let e = unmarshal_exp senv pos e in
               l, a_opt, e
       | _ ->
            raise (FirException (pos, MarshalError))

and unmarshal_dtuple_match_cases senv pos l =
   let pos = string_pos "unmarshal_dtuple_match_cases" pos in
      match l with
         List l ->
            List.map (unmarshal_dtuple_match_case senv pos) l
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Type definitions.
 *)
let marshal_typedef senv v tydef =
   let senv, v = marshal_symbol senv v in
   let senv, tydef = marshal_tydef senv tydef in
      senv, List [Magic TypedefMagic; v; tydef]

let unmarshal_typedef senv pos l =
   let pos = string_pos "unmarshal_typedef" pos in
      match l with
         List [Magic TypedefMagic; v; tydef] ->
            let v = unmarshal_symbol senv pos v in
            let tydef = unmarshal_tydef senv pos tydef in
               v, tydef
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_typedefs senv types =
   let senv, types =
      SymbolTable.fold (fun (senv, types) v tydef ->
            let senv, l = marshal_typedef senv v tydef in
               senv, l :: types) (senv, []) types
   in
      senv, List [Magic TypesMagic; List types]

let unmarshal_typedefs senv pos l =
   let pos = string_pos "unmarshal_types" pos in
      match l with
         List [Magic TypesMagic; List l] ->
            let types = List.map (unmarshal_typedef senv pos) l in
               List.fold_left (fun types (v, tydef) ->
                     SymbolTable.add types v tydef) SymbolTable.empty types
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_simple_types senv types =
   let senv, types =
      SymbolTable.fold (fun (senv, types) v ty ->
            let senv, v = marshal_symbol senv v in
            let senv, ty = marshal_type senv ty in
            let types = List [Magic SimpleTypeMagic; v; ty] :: types in
               senv, types) (senv, []) types
   in
      senv, List [Magic SimpleTypesMagic; List types]

let unmarshal_simple_type senv pos l =
   let pos = string_pos "unmarshal_simple_type" pos in
      match l with
         List [Magic SimpleTypeMagic; v; ty] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
               v, ty
       | _ ->
            raise (FirException (pos, MarshalError))

let unmarshal_simple_types senv pos types =
   let pos = string_pos "unmarshal_simple_types" pos in
      match types with
         List [Magic SimpleTypesMagic; List types] ->
            let types = List.map (unmarshal_simple_type senv pos) types in
               List.fold_left (fun types (v, ty) ->
                     SymbolTable.add types v ty) SymbolTable.empty types
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Names.
 *)
let marshal_names senv names =
   let senv, names =
      List.fold_left (fun (senv, names) (v, v_opt) ->
            let senv, v = marshal_symbol senv v in
            let senv, v_opt = marshal_symbol_option senv v_opt in
            let names = List [v; v_opt] :: names in
               senv, names) (senv, []) names
   in
      senv, List names

let unmarshal_names senv pos names =
   match names with
      List names ->
         List.fold_left (fun names name ->
               match name with
                  List [v; v_opt] ->
                     let v = unmarshal_symbol senv pos v in
                     let v_opt = unmarshal_symbol_option senv pos v_opt in
                        (v, v_opt) :: names
                | _ ->
                     raise (FirException (pos, MarshalError))) [] names
    | _ ->
         raise (FirException (pos, MarshalError))

(*
 * Initializers.
 *)
let marshal_init senv = function
   InitAtom a ->
      let senv, a = marshal_atom senv a in
         senv, List [Magic InitAtomMagic; a]
 | InitAlloc op ->
      let senv, op = marshal_alloc_op senv op in
         senv, List [Magic InitAllocMagic; op]
 | InitRawData (pre, s) ->
      let pre = marshal_int_precision pre in
      let s = Array.to_list s in
      let s = List.map (fun i -> Int i) s in
         senv, List [Magic InitRawDataMagic; pre; List s]
 | InitNames (v, names) ->
      let senv, v = marshal_symbol senv v in
      let senv, names = marshal_names senv names in
         senv, List [Magic InitNamesMagic; v; names]
 | InitTag (ty_var, tyl) ->
      let senv, ty_var = marshal_symbol senv ty_var in
      let senv, tyl = marshal_mutable_type_fields senv tyl in
         senv, List [Magic InitTagMagic; ty_var; tyl]

let unmarshal_init senv pos l =
   let pos = string_pos "unmarshal_init" pos in
      match l with
         List [Magic InitAtomMagic; a] ->
            let a = unmarshal_atom senv pos a in
               InitAtom a
       | List [Magic InitAllocMagic; op] ->
            let op = unmarshal_alloc_op senv pos op in
               InitAlloc op
       | List [Magic InitRawDataMagic; pre; List l] ->
            let pre = unmarshal_int_precision pos pre in
            let l =
               List.map (fun x ->
                     match x with
                        Int i -> i
                      | _ -> raise (FirException (pos, MarshalError))) l
            in
               InitRawData (pre, Array.of_list l)
       | List [Magic InitNamesMagic; v; names] ->
            let v = unmarshal_symbol senv pos v in
            let names = unmarshal_names senv pos names in
               InitNames (v, names)
       | List [Magic InitTagMagic; ty_var; tyl] ->
            let ty_var = unmarshal_symbol senv pos ty_var in
            let tyl = unmarshal_mutable_type_fields senv pos tyl in
               InitTag (ty_var, tyl)
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_global senv v ty init =
   let senv, v = marshal_symbol senv v in
   let senv, ty = marshal_type senv ty in
   let senv, init = marshal_init senv init in
      senv, List [Magic GlobalMagic; v; ty; init]

let unmarshal_global senv pos l =
   let pos = string_pos "unmarshal_global" pos in
      match l with
         List [Magic GlobalMagic; v; ty; init] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let init = unmarshal_init senv pos init in
               v, (ty, init)
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_globals senv globals =
   let senv, globals =
      SymbolTable.fold (fun (senv, globals) v (ty, init) ->
            let senv, l = marshal_global senv v ty init in
               senv, l :: globals) (senv, []) globals
   in
      senv, List [Magic GlobalsMagic; List (List.rev globals)]

let unmarshal_globals senv pos l =
   let pos = string_pos "unmarshal_globals" pos in
   let globals = SymbolTable.empty in
      match l with
         List [Magic GlobalsMagic; List l] ->
            List.fold_left (fun globals l ->
               let v, (ty, init) = unmarshal_global senv pos l in
                  SymbolTable.add globals v (ty, init)) globals l
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_import_arg = function
   ArgFunction ->
      Magic ArgFunctionMagic
 | ArgPointer ->
      Magic ArgPointerMagic
 | ArgRawInt (pre, signed) ->
      List [Magic ArgRawIntMagic;
            marshal_int_precision pre;
            marshal_signed signed]
 | ArgRawFloat pre ->
      List [Magic ArgRawFloatMagic;
            marshal_float_precision pre]

let unmarshal_import_arg pos l =
   let pos = string_pos "unmarshal_import_arg" pos in
      match l with
         Magic ArgFunctionMagic ->
            ArgFunction
       | Magic ArgPointerMagic ->
            ArgPointer
       | List [Magic ArgRawIntMagic; pre; signed] ->
            ArgRawInt (unmarshal_int_precision pos pre,
                       unmarshal_signed pos signed)
       | List [Magic ArgRawFloatMagic; pre] ->
            ArgRawFloat (unmarshal_float_precision pos pre)
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_import_info info =
   match info with
      ImportGlobal ->
         Magic ImportGlobalMagic
    | ImportFun (b, args) ->
         let args = List.map marshal_import_arg args in
            List [Magic ImportFunMagic; Bool b; List args]

let unmarshal_import_info pos info =
   let pos = string_pos "unmarshal_import_info" pos in
      match info with
         Magic ImportGlobalMagic ->
            ImportGlobal
       | List [Magic ImportFunMagic; Bool b; List args] ->
            ImportFun (b, List.map (unmarshal_import_arg pos) args)
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_import_entry senv v { import_name = s;
                                  import_type = ty;
                                  import_info = info
    } =
   let senv, v = marshal_symbol senv v in
   let senv, ty = marshal_type senv ty in
   let info = marshal_import_info info in
      senv, List [Magic ImportMagic; v; String s; ty; info]

let unmarshal_import_entry senv pos l =
   let pos = string_pos "unmarshal_import" pos in
      match l with
         List [Magic ImportMagic; v; String s; ty; info] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let info = unmarshal_import_info pos info in
            let info =
               { import_name = s;
                 import_type = ty;
                 import_info = info
               }
            in
               v, info
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_import senv import =
   let senv, imports =
      SymbolTable.fold (fun (senv, import) v info ->
            let senv, l = marshal_import_entry senv v info in
               senv, l :: import) (senv, []) import
   in
      senv, List [Magic ImportsMagic; List (List.rev imports)]

let unmarshal_import senv pos l =
   let pos = string_pos "unmarshal_import" pos in
   let import = SymbolTable.empty in
      match l with
         List [Magic ImportsMagic; List l] ->
            List.fold_left (fun import l ->
                  let v, info = unmarshal_import_entry senv pos l in
                     SymbolTable.add import v info) import l
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Exports.
 *)
let marshal_export_entry senv v { export_name = s;
                                  export_type = ty
    } =
   let senv, v = marshal_symbol senv v in
   let senv, ty = marshal_type senv ty in
      senv, List [Magic ExportMagic; v; String s; ty]

let unmarshal_export_entry senv pos l =
   let pos = string_pos "unmarshal_export_entry" pos in
      match l with
         List [Magic ExportMagic; v; String s; ty] ->
            let v = unmarshal_symbol senv pos v in
            let ty = unmarshal_type senv pos ty in
            let info =
               { export_name = s;
                 export_type = ty
               }
            in
               v, info
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_export senv export =
   let senv, exports =
      SymbolTable.fold (fun (senv, export) v info ->
            let senv, l = marshal_export_entry senv v info in
               senv, l :: export) (senv, []) export
   in
      senv, List [Magic ExportsMagic; List (List.rev exports)]

let unmarshal_export senv pos l =
   let pos = string_pos "unmarshal_export" pos in
   let export = SymbolTable.empty in
      match l with
         List [Magic ExportsMagic; List l] ->
            List.fold_left (fun export l ->
                  let v, info = unmarshal_export_entry senv pos l in
                     SymbolTable.add export v info) export l
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Functions.
 *)
let marshal_fundef senv f (debug, ty_vars, ty, vars, e) =
   let senv, f = marshal_symbol senv f in
   let senv, line = marshal_loc senv debug in
   let senv, ty_vars = marshal_symbol_list senv ty_vars in
   let senv, ty = marshal_type senv ty in
   let senv, vars = marshal_symbol_list senv vars in
   let senv, e = marshal_exp senv e in
      senv, List [Magic FundefMagic; f; line; ty_vars; ty; vars; e]

let unmarshal_fundef senv pos l =
   let pos = string_pos "unmarshal_fundef" pos in
      match l with
         List [Magic FundefMagic; f; debug; ty_vars; ty; vars; e] ->
            let f = unmarshal_symbol senv pos f in
            let debug = unmarshal_loc senv pos debug in
            let ty_vars = unmarshal_symbol_list senv pos ty_vars in
            let ty = unmarshal_type senv pos ty in
            let vars = unmarshal_symbol_list senv pos vars in
            let e = unmarshal_exp senv pos e in
               f, (debug, ty_vars, ty, vars, e)
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_funs senv funs =
   let senv, funs =
      SymbolTable.fold (fun (senv, funs) f info ->
            let senv, l = marshal_fundef senv f info in
               senv, l :: funs) (senv, []) funs
   in
      senv, List [Magic FunsMagic; List funs]

let unmarshal_funs senv pos l =
   let pos = string_pos "unmarshal_funs" pos in
      match l with
         List [Magic FunsMagic; List l] ->
            let funs = List.map (unmarshal_fundef senv pos) l in
               List.fold_left (fun funs (f, info) ->
                     SymbolTable.add funs f info) SymbolTable.empty funs
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Frames.
 *)
let marshal_subfields senv vars =
   let senv, vars =
      List.fold_left (fun (senv, vars) (v, ty, i) ->
            let senv, v = marshal_symbol senv v in
            let senv, ty = marshal_type senv ty in
            let field = List [Magic SubfieldMagic; v; ty; Int i] in
               senv, field :: vars) (senv, []) vars
   in
      senv, List [Magic SubfieldsMagic; List vars]

let marshal_field senv field vars =
   let senv, field = marshal_symbol senv field in
   let senv, subfields = marshal_subfields senv vars in
      senv, List [Magic FieldMagic; field; subfields]

let marshal_fields senv fields =
   let senv, fields =
      SymbolTable.fold (fun (senv, fields) field vars ->
            let senv, vars = marshal_field senv field vars in
               senv, vars :: fields) (senv, []) fields
   in
      senv, List [Magic FieldsMagic; List fields]

let marshal_frames senv frames =
   let senv, frames =
      SymbolTable.fold (fun (senv, frames) frame (vars, fields) ->
            let senv, frame = marshal_symbol senv frame in
            let senv, vars = marshal_symbol_list senv vars in
            let senv, fields = marshal_fields senv fields in
            let frame = List [Magic FrameMagic; frame; vars; fields] in
            let frames = frame :: frames in
               senv, frames) (senv, []) frames
   in
      senv, List [Magic FramesMagic; List frames]

let unmarshal_subfield senv pos subfield =
   match subfield with
      List [Magic SubfieldMagic; v; ty; Int i] ->
         let v = unmarshal_symbol senv pos v in
         let ty = unmarshal_type senv pos ty in
            v, ty, i
    | _ ->
         raise (FirException (pos, MarshalError))

let unmarshal_subfields senv pos subfields =
   match subfields with
      List [Magic SubfieldsMagic; List vars] ->
         List.rev_map (unmarshal_subfield senv pos) vars
    | _ ->
         raise (FirException (pos, MarshalError))

let unmarshal_field senv pos fields field =
   match field with
      List [Magic FieldMagic; field; subfields] ->
         let field = unmarshal_symbol senv pos field in
         let subfield = unmarshal_subfields senv pos subfields in
            SymbolTable.add fields field subfield
    | _ ->
         raise (FirException (pos, MarshalError))

let unmarshal_fields senv pos fields =
   match fields with
      List [Magic FieldsMagic; List fields] ->
         List.fold_left (fun fields field ->
               unmarshal_field senv pos fields field) SymbolTable.empty fields
    | _ ->
         raise (FirException (pos, MarshalError))

let unmarshal_frame senv pos frames frame =
   match frame with
      List [Magic FrameMagic; frame; vars; fields] ->
         let frame = unmarshal_symbol senv pos frame in
         let vars = unmarshal_symbol_list senv pos vars in
         let fields = unmarshal_fields senv pos fields in
            SymbolTable.add frames frame (vars, fields)
    | _ ->
         raise (FirException (pos, MarshalError))

let unmarshal_frames senv pos frames =
   match frames with
      List [Magic FramesMagic; List frames] ->
         List.fold_left (fun frames frame ->
               unmarshal_frame senv pos frames frame) SymbolTable.empty frames
    | _ ->
         raise (FirException (pos, MarshalError))

(*
 * String table listing.
 *)
let marshal_senv senv =
   let strings = Array.create (SymbolTable.cardinal senv) (false, "") in
   let _ =
      SymbolTable.iter (fun v i ->
            let b = Symbol.is_interned v in
            let s = Symbol.to_string v in
               strings.(i) <- (b, s)) senv
   in
   let l =
      List.map (fun (b, s) ->
            List [String s; Bool b]) (Array.to_list strings)
   in
      List (Magic SymbolTableMagic :: l)

let unmarshal_senv pos l =
   let pos = string_pos "unmarshal_senv" pos in
      match l with
         List (Magic SymbolTableMagic :: l) ->
            let len = List.length l in
            let senv = Array.create len (new_symbol_string "$unknown") in
            let rec collect i = function
               List [String s; Bool b] :: tl ->
                  let v =
                     if b then
                        Symbol.add s
                     else
                        new_symbol_string s
                  in
                     senv.(i) <- v;
                     collect (succ i) tl
             | [] ->
                  ()
             | _ ->
                  raise (FirException (pos, MarshalError))
            in
               collect 0 l;
               senv
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Programs.
 *)
let marshal_file_class fclass =
   let fclass =
      match fclass with
         FileFC ->
            FileFCMagic
       | FileJava ->
            FileJavaMagic
       | FileNaml ->
            FileNamlMagic
       | FileAml ->
            FileAmlMagic
   in
      Magic fclass

let unmarshal_file_class pos fclass =
   match fclass with
      Magic FileFCMagic ->
         FileFC
    | Magic FileJavaMagic ->
         FileJava
    | Magic FileNamlMagic ->
         FileNaml
    | Magic FileAmlMagic ->
         FileAml
    | _ ->
         raise (FirException (pos, MarshalError))

let marshal_file_info { file_dir = dir; file_name = name; file_class = fclass } =
   let fclass = marshal_file_class fclass in
      List [Magic FileInfoMagic; String dir; String name; fclass]

let unmarshal_file_info pos l =
   let pos = string_pos "unmarshal_file_info" pos in
      match l with
         List [Magic FileInfoMagic; String dir; String name; fclass] ->
            let fclass = unmarshal_file_class pos fclass in
               { file_dir = dir; file_name = name; file_class = fclass }
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_flags flags =
   let flags = StringTable.fold (fun list flag value ->
      let value =
         match value with
            FlagBool value ->
               List [Magic FlagBoolMagic; String flag; Bool value]
          | FlagInt value ->
               List [Magic FlagIntMagic; String flag; Int value]
      in
         value :: list) [] flags
   in
      List (Magic FlagMagic :: flags)

let unmarshal_flags pos l =
   let pos = string_pos "unmarshal_flags" pos in
   let flags_init = { flags_empty with flag_defaults = !std_flags.flag_defaults } in
      match l with
         List (Magic FlagMagic :: l) ->
            let flags = List.fold_left (fun flags l ->
               match l with
                  List [Magic FlagBoolMagic; String flag; Bool value] ->
                     (try
                        flags_set_bool flags flag value
                     with
                        Failure _ ->
                           (* Ignore undefined flags *)
                           flags)
                | List [Magic FlagIntMagic; String flag; Int value] ->
                     (try
                        flags_set_int flags flag value
                     with
                        Failure _ ->
                           (* Ignore undefined flags *)
                           flags)
                | _ ->
                     raise (FirException (pos, MarshalError))) flags_init l
            in
               flags
       | _ ->
            raise (FirException (pos, MarshalError))

let marshal_fir_prog senv { prog_import = import;
                            prog_export = export;
                            prog_file = file;
                            prog_types = types;
                            prog_frames = frames;
                            prog_globals = globals;
                            prog_funs = funs;
                            prog_names = names
                          } =
   let senv = SymbolTable.empty in
   let file = marshal_file_info file in
   let senv, import = marshal_import senv import in
   let senv, export = marshal_export senv export in
   let senv, types = marshal_typedefs senv types in
   let senv, frames = marshal_frames senv frames in
   let senv, globals = marshal_globals senv globals in
   let senv, funs = marshal_funs senv funs in
   let senv, names = marshal_simple_types senv names in
      senv, List [file; import; export; types; frames; globals; funs; names]

let unmarshal_fir_prog senv pos l =
   let pos = string_pos "unmarshal_fir_prog" pos in
      match l with
         List [file; import; export; types; frames; globals; funs; names] ->
            let file = unmarshal_file_info pos file in
            let import = unmarshal_import senv pos import in
            let export = unmarshal_export senv pos export in
            let types = unmarshal_typedefs senv pos types in
            let frames = unmarshal_frames senv pos frames in
            let globals = unmarshal_globals senv pos globals in
            let funs = unmarshal_funs senv pos funs in
            let names = unmarshal_simple_types senv pos names in
               { prog_import = import;
                 prog_export = export;
                 prog_file = file;
                 prog_types = types;
                 prog_frames = frames;
                 prog_globals = globals;
                 prog_funs = funs;
                 prog_names = names
               }
       | _ ->
            raise (FirException (pos, MarshalError))

(*
 * Marshal a rttd.
 *)
let marshal_rttd senv rttd =
   let rttd = Rttd.to_list rttd in
   let senv, rttd =
      List.fold_left (fun (senv, rttd) (v, il) ->
            let il = List.map (fun i -> Int i) il in
            let senv, v = marshal_symbol senv v in
            let rttd = List [Magic RttdEntryMagic; v; List il] :: rttd in
               senv, rttd) (senv, []) rttd
   in
      senv, List [Magic RttdMagic; List rttd]

let unmarshal_rttd senv pos rttd =
   match rttd with
      List [Magic RttdMagic; List rttd] ->
         List.fold_left (fun rttd l ->
               match l with
                  List [Magic RttdEntryMagic; v; List il] ->
                     let il =
                        List.map (function
                           Int i -> i
                         | _ -> raise (FirException (pos, MarshalError))) il
                     in
                     let v = unmarshal_symbol senv pos v in
                        Rttd.add rttd v il
                | _ ->
                     raise (FirException (pos, MarshalError))) Rttd.empty rttd
    | _ ->
         raise (FirException (pos, MarshalError))

(*
 * Program marshaling.
 *)
let marshal_prog { marshal_fir           = fir;
                   marshal_globals       = globals;
                   marshal_funs          = funs;
                   marshal_rttd          = rttd
                 } =
   let senv = SymbolTable.empty in
   let senv, fir = marshal_fir_prog senv fir in
   let senv, globals = marshal_symbol_list senv globals in
   let senv, funs = marshal_symbol_list senv funs in
   let senv, rttd = marshal_rttd senv rttd in
   let senv = marshal_senv senv in
   let flags = marshal_flags !std_flags.flag_values in
      List [Magic ProgMagic; Int version_number; fir; globals; funs; rttd; senv; flags]

let unmarshal_prog pos l =
   let pos = string_pos "unmarshal_prog" pos in
      (* TEMP: Uncomment this code to get the marshal expression debug
               (I need to turn this into a real option at some point).
      printf "begin marshal prog@.";
      print_marshal_exp l;
      printf "end marshal prog@.";
       *)
      match l with
         List [Magic ProgMagic; Int version; fir; globals; funs; rttd; senv; flags] ->
            if version <> version_number then
               raise (FirException (pos, StringIntError ("bad version number in binary", version)));
            let flags = unmarshal_flags pos flags in
            let _ = std_flags := flags in
            let senv = unmarshal_senv pos senv in
            let fir = unmarshal_fir_prog senv pos fir in
            let globals = unmarshal_symbol_list senv pos globals in
            let funs = unmarshal_symbol_list senv pos funs in
            let rttd = unmarshal_rttd senv pos rttd in
               { marshal_fir           = fir;
                 marshal_globals       = globals;
                 marshal_funs          = funs;
                 marshal_rttd          = rttd
               }
       | String _ ->    raise (FirException (int_pos 99 pos, MarshalError))
       | Rawfloat _ ->  raise (FirException (int_pos 2 pos, MarshalError))
       | Float _ ->     raise (FirException (int_pos 3 pos, MarshalError))
       | Rawint _ ->    raise (FirException (int_pos 4 pos, MarshalError))
       | Magic _ ->     raise (FirException (int_pos 5 pos, MarshalError))
       | Int _ ->       raise (FirException (int_pos 6 pos, MarshalError))
       | Bool _ ->      raise (FirException (int_pos 7 pos, MarshalError))
       | List _ ->      raise (FirException (int_pos 8 pos, MarshalError))


(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * IO modules.
 *)
module ChannelIO =
struct
   type t = magic
   type in_channel = Pervasives.in_channel
   type out_channel = Pervasives.out_channel

   let magic_of_int = magic_of_int
   let int_of_magic = int_of_magic

   let input_byte = Pervasives.input_byte
   let input_buffer = Pervasives.really_input
   let output_byte = Pervasives.output_byte
   let output_buffer = Pervasives.output
end

module BufferIO =
struct
   type t = magic
   type in_channel = Pervasives.in_channel
   type out_channel = Buffer.t

   let magic_of_int = magic_of_int
   let int_of_magic = int_of_magic

   let input_byte = Pervasives.input_byte
   let input_buffer = Pervasives.really_input

   let output_byte buf i =
      Buffer.add_char buf (Char.chr i)

   let output_buffer buf s off len =
      Buffer.add_substring buf s off len
end

(*
 * Marshaller.
 *)
module Make (IO : MarshalIOSig with type t = magic) =
struct
   type in_channel = IO.in_channel
   type out_channel = IO.out_channel

   module Marshal = Fmarshal.Make (IO)

   let marshal_prog out e =
      Marshal.marshal out (marshal_prog e)

   let unmarshal_prog inc =
      unmarshal_prog (var_exp_pos (Symbol.add "Fir_marshal")) (Marshal.unmarshal inc)
end

module ChannelMarshal = Make (ChannelIO)
module BufferMarshal = Make (BufferIO)

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
