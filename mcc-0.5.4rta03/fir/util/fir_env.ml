(*
 * Standard environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_exn
open Fir_pos

module Pos = MakePos (struct let name = "Fir_env" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Type and variable environments.
 *)
type tenv = tydef SymbolTable.t
type venv = ty SymbolTable.t

(************************************************************************
 * ENVIRONMENTS
 ************************************************************************)

(*
 * New environments.
 *)
let tenv_empty = SymbolTable.empty
let venv_empty = SymbolTable.empty

(*
 * Add a type definition.
 *)
let tenv_add = SymbolTable.add

(*
 * Lookup a type.
 *)
let tenv_lookup tenv pos v =
   try SymbolTable.find tenv v with
      Not_found ->
         raise (FirException (pos, UnboundType v))

(*
 * Fold over the whole table.
 *)
let tenv_fold = SymbolTable.fold

(*
 * Add a variable to the table.
 *)
let venv_add = SymbolTable.add

(*
 * Lookup a var.
 *)
let venv_mem = SymbolTable.mem

let venv_lookup venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let venv_lookup_opt venv v =
   try Some (SymbolTable.find venv v) with
      Not_found ->
         None

(*
 * The "global" environment contains a spec for
 * all the different kinds of objects.
 *)
type genv =
   { genv_types : tenv;
     genv_vars : venv;
     genv_funs : venv;
     genv_globals : venv;
     genv_names : venv;
     genv_frames : frame SymbolTable.t;
     genv_norm : var SymbolTable.t;
     genv_poly : SymbolSet.t;
     genv_alpha : var SymbolTable.t
   }

let tenv_of_genv { genv_types = tenv } =
   tenv

let venv_of_genv { genv_vars = venv } =
   venv

let genv_empty =
   { genv_types = tenv_empty;
     genv_vars = venv_empty;
     genv_funs = venv_empty;
     genv_globals = venv_empty;
     genv_names = venv_empty;
     genv_frames = SymbolTable.empty;
     genv_norm = SymbolTable.empty;
     genv_poly = SymbolSet.empty;
     genv_alpha = SymbolTable.empty
   }

let genv_of_prog prog =
   let { prog_types = types;
         prog_import = import;
         prog_funs = funs;
         prog_globals = globals;
         prog_frames = frames;
         prog_names = names
       } = prog
   in

   (* Functions *)
   let fenv =
      SymbolTable.fold (fun fenv f (_, ty_vars, ty, _, _) ->
            venv_add fenv f (TyAll (ty_vars, ty))) venv_empty funs
   in

   (* Globals *)
   let venv =
      SymbolTable.fold (fun venv v (ty, _) ->
            venv_add venv v ty) venv_empty globals
   in

   (* Import values *)
   let venv, fenv =
      SymbolTable.fold (fun (venv, fenv) v import ->
            let { import_type = ty;
                  import_info = info
                } = import
            in
               match info with
                  ImportGlobal ->
                     let venv = venv_add venv v ty in
                        venv, fenv
                | ImportFun _ ->
                     let fenv = venv_add fenv v ty in
                        venv, fenv) (venv, fenv) import
   in

   (* Names *)
   let nenv =
      SymbolTable.fold (fun nenv v ty ->
            venv_add nenv v ty) venv_empty names
   in
      { genv_types = types;
        genv_vars = venv_empty;
        genv_funs = fenv;
        genv_globals = venv;
        genv_names = names;
        genv_frames = frames;
        genv_norm = SymbolTable.empty;
        genv_poly = SymbolSet.empty;
        genv_alpha = SymbolTable.empty
      }

(*
 * Adding to the environment.
 *)
let genv_add_type genv v ty =
   { genv with genv_types = SymbolTable.add genv.genv_types v ty }

let genv_add_var genv v ty =
   { genv with genv_vars = SymbolTable.add genv.genv_vars v ty }

let genv_add_fun genv v ty =
   { genv with genv_funs = SymbolTable.add genv.genv_funs v ty }

let genv_add_global genv v ty =
   { genv with genv_globals = SymbolTable.add genv.genv_globals v ty }

let genv_add_name genv v ty =
   { genv with genv_names = SymbolTable.add genv.genv_names v ty }

let genv_add_frame genv v frame =
   { genv with genv_frames = SymbolTable.add genv.genv_frames v frame }

let genv_add_poly genv vars =
   let penv = List.fold_left SymbolSet.add genv.genv_poly vars in
      { genv with genv_poly = penv }

let genv_add_vars genv pos vars tyl =
   let pos = string_pos "genv_add_vars" pos in
   let len1 = List.length vars in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let venv = List.fold_left2 SymbolTable.add genv.genv_vars vars tyl in
      { genv with genv_vars = venv }

(*
 * Membership.
 *)
let genv_mem_type genv v =
   SymbolTable.mem genv.genv_types v

let genv_mem_var genv v =
   SymbolTable.mem genv.genv_vars v

let genv_mem_fun genv v =
   SymbolTable.mem genv.genv_funs v

let genv_mem_global genv v =
   SymbolTable.mem genv.genv_globals v

let genv_mem_frame genv v =
   SymbolTable.mem genv.genv_frames v

let genv_mem_poly genv v =
   SymbolSet.mem genv.genv_poly v

(*
 * Lookup.
 *)
let genv_lookup_type genv pos v =
   try SymbolTable.find genv.genv_types v with
      Not_found ->
         raise (FirException (string_pos "genv_lookup_type" pos, UnboundType v))

let genv_lookup_var genv pos v =
   try SymbolTable.find genv.genv_vars v with
      Not_found ->
         raise (FirException (string_pos "genv_lookup_var" pos, UnboundVar v))

let genv_lookup_global genv pos v =
   try SymbolTable.find genv.genv_globals v with
      Not_found ->
         raise (FirException (string_pos "genv_lookup_global" pos, UnboundVar v))

let genv_lookup_var_or_global genv pos v =
   try SymbolTable.find genv.genv_vars v with
      Not_found ->
         try SymbolTable.find genv.genv_globals v with
            Not_found ->
               raise (FirException (string_pos "genv_lookup_global" pos, UnboundVar v))

let genv_lookup_fun genv pos v =
   try SymbolTable.find genv.genv_funs v with
      Not_found ->
         raise (FirException (string_pos "genv_lookup_fun" pos, UnboundVar v))

let genv_lookup_name genv pos v =
   try SymbolTable.find genv.genv_names v with
      Not_found ->
         raise (FirException (string_pos "genv_lookup_name" pos, UnboundVar v))

let genv_lookup_frame genv pos v =
   try SymbolTable.find genv.genv_frames v with
      Not_found ->
         raise (FirException (string_pos "genv_lookup_frame" pos, UnboundVar v))

(*
 * Variable normalization.
 *)
let genv_add_norm genv v1 v2 =
   { genv with genv_norm = SymbolTable.add genv.genv_norm v1 v2 }

let rec norm nenv v =
   try norm nenv (SymbolTable.find nenv v) with
      Not_found ->
         v

let genv_equal_norm genv v1 v2 =
   let nenv = genv.genv_norm in
      Symbol.eq (norm nenv v1) (norm nenv v2)

(*
 * Alpha-equivalence of vars.
 *)
let genv_add_alpha genv pos vars1 vars2 =
   let pos = string_pos "genv_add_alpha" pos in
   let len1 = List.length vars1 in
   let len2 = List.length vars2 in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let aenv = List.fold_left2 SymbolTable.add genv.genv_alpha vars1 vars2 in
      { genv with genv_alpha = aenv }

let genv_equal_alpha genv v1 v2 =
   try Symbol.eq (SymbolTable.find genv.genv_alpha v1) v2 with
      Not_found ->
         false

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
