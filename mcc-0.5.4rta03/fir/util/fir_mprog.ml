(*
 * Construct the marshal-friendly version of the FIR.  To make the FIR safe
 * to marshal during migration, we have to ensure that various symbol tables
 * do not get re-ordered.  See notes for the mprog type below for more
 * information.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_ds
open Fir_env
open Fir_exn
open Fir_pos
open Fir_type
open Fir_rttd_map

module Pos = MakePos (struct let name = "Fir_mprog" end)
open Pos


(************************************************************************
 * RTTD SCANNING
 ************************************************************************)


module Rttd =
struct
   type t = var RttdTable.t

   let empty = RttdTable.empty

   let add rttd v il =
      RttdTable.add rttd il v

   (*
    * Build a list of ints that define the rttd.
    * The ints are 1 for entries that are pointers.
    *)
   let build_mask tenv pos tyl =
      let mask =
         List.map (fun ty ->
               if is_pointer_type_dir tenv pos ty then
                  1
               else
                  0) tyl
      in
         0 :: mask

   (*
    * Find the global for a rttd.
    *)
   let find rttd tenv pos tyl =
      let pos = string_pos "rttd_find" pos in
      let mask = build_mask tenv pos tyl in
         try RttdTable.find rttd mask with
            Not_found ->
               raise (FirException (pos, StringError "rttd has not been defined"))

   (*
    * Return a list.
    *)
   let to_list rttd =
      RttdTable.fold (fun rttd il v ->
            (v, il) :: rttd) [] rttd

   (*
    * Map over the table.
    *)
   let map = RttdTable.mapi
end


(*
 * A Rttd is an array of integers that refer
 * to the indexes of pointers in the tuple.
 *)
let ty_rttd = TyArray (TyRawInt (Rawint.Int32, true))


(*
 * Build the mask for a RawTuple.
 * the mask includes the indexes of every field that is
 * a pointer.  The indexes start from 1.
 *)
let build_raw_tuple (tenv : tenv) rttd globals pos tyl =
   let pos = string_pos "build_raw_tuple" pos in
   let mask = Rttd.build_mask tenv pos tyl in
      if RttdTable.mem rttd mask then
         rttd, globals
      else
         let v = new_symbol_string "rttd" in

         (* Allocation op *)
         let args = List.map (fun i -> AtomRawInt (Rawint.of_int Rawint.Int32 true i)) mask in
         let op = AllocArray (ty_rttd, args) in

         (* Add the record to the globals *)
         let globals = SymbolTable.add globals v (ty_rttd, InitAlloc op) in

         (* Add to rttd table *)
         let rttd = RttdTable.add rttd mask v in
            rttd, globals


(*
 * Scan a type for occurrences of RawTuples.
 *)
let rec build_type (tenv : tenv) rttd globals pos ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyRawData
    | TyVar _
    | TyProject _
    | TyPointer _
    | TyFrame _
    | TyDTuple (_, None) ->
         rttd, globals

    | TyFun (ty_vars, ty_res) ->
         build_type_list tenv rttd globals pos (ty_res :: ty_vars)
    | TyUnion (_, tyl, _)
    | TyApply (_, tyl) ->
         build_type_list tenv rttd globals pos tyl
    | TyTuple (NormalTuple, fields)
    | TyTuple (BoxTuple, fields)
    | TyDTuple (_, Some fields)
    | TyTag (_, fields) ->
         build_mutable_type_list tenv rttd globals pos fields
    | TyArray ty
    | TyCase ty
    | TyExists (_, ty)
    | TyAll (_, ty)
    | TyObject (_, ty) ->
         build_type tenv rttd globals pos ty
    | TyTuple (RawTuple, fields) ->
         let tyl = List.map fst fields in
         let rttd, globals = build_type_list tenv rttd globals pos tyl in
            build_raw_tuple tenv rttd globals pos tyl
    | TyDelayed ->
         raise (FirException (pos, StringError "illegal TyDelayed"))

and build_type_list tenv rttd globals pos tyl =
   List.fold_left (fun (rttd, globals) ty ->
         build_type tenv rttd globals pos ty) (rttd, globals) tyl

and build_mutable_type_list tenv rttd globals pos fields =
   List.fold_left (fun (rttd, globals) (ty, _) ->
         build_type tenv rttd globals pos ty) (rttd, globals) fields

(*
 * Scan a type definition for a RawTuple.
 *)
let build_tydef tenv rttd globals v tydef =
   let pos = string_pos "build_tydef" (var_exp_pos v) in
      match tydef with
         TyDefLambda (_, ty) ->
            build_type tenv rttd globals pos ty
       | TyDefUnion (_, fields) ->
            List.fold_left (fun (rttd, globals) fields ->
                  List.fold_left (fun (rttd, globals) (ty, _) ->
                        build_type tenv rttd globals pos ty) (rttd, globals) fields) (rttd, globals) fields
       | TyDefDTuple _ ->
            rttd, globals


(*
 * Scan all the type definitions.
 * Whenever we find a RawTuple, add the rttd.
 *)
let rttd_prog prog =
   let { prog_types = tenv;
         prog_globals = globals
       } = prog
   in
   let rttd, globals =
      SymbolTable.fold (fun (rttd, globals) v ty ->
            build_tydef tenv rttd globals v ty) (RttdTable.empty, globals) tenv
   in
   let prog = { prog with prog_globals = globals } in
      prog, rttd


(************************************************************************
 * ESCAPING FUNCTIONS
 ************************************************************************)


(*
 * Add an escaping atom.
 *)
let esc_var esc funs v =
   if SymbolTable.mem funs v then
      SymbolSet.add esc v
   else
      esc


(*
 * Look for escaping functions.
 *)
let rec esc_atom esc funs a =
   match a with
      AtomNil _
    | AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomConst _
    | AtomLabel _
    | AtomSizeof _ ->
         esc
    | AtomVar v
    | AtomFun v
    | AtomTyPack (v, _, _)
    | AtomTyUnpack v ->
         esc_var esc funs v
    | AtomTyApply (a, _, _)
    | AtomUnop (_, a) ->
         esc_atom esc funs a
    | AtomBinop (_, a1, a2) ->
         esc_atom (esc_atom esc funs a1) funs a2

let esc_atoms esc funs args =
   List.fold_left (fun esc a -> esc_atom esc funs a) esc args

(*
 * Escaping functions in allocation.
 *)
let esc_alloc_op esc funs op =
   match op with
      AllocTuple (_, _, _, args)
    | AllocDTuple (_, _, _, args)
    | AllocUnion (_, _, _, _, args) ->
         esc_atoms esc funs args
    | AllocArray (_, args) ->
         esc_atoms esc funs args
    | AllocVArray (_, _, a1, a2) ->
         esc_atom (esc_atom esc funs a1) funs a2
    | AllocMalloc (_, a) ->
         esc_atom esc funs a
    | AllocFrame _ ->
         esc

(*
 * Tailcalls.
 *)
let esc_tailop esc funs op =
   match op with
      TailSysMigrate (_, a1, a2, _, args) ->
         esc_atoms esc funs (a1 :: a2 :: args)
    | TailAtomic (v, a1, args) ->
         esc_atom (esc_atoms esc funs (a1 :: args)) funs v
    | TailAtomicRollback (a1, a2) ->
         esc_atoms esc funs [a1; a2]
    | TailAtomicCommit (a, _, args) ->
         esc_atoms esc funs (a :: args)


(*
 * Initializers.
 *)
let esc_init esc funs init =
   match init with
      InitAtom a ->
         esc_atom esc funs a
    | InitAlloc op ->
         esc_alloc_op esc funs op
    | InitTag _
    | InitRawData _
    | InitNames _ ->
         esc


(*
 * Expressions.
 *)
let rec esc_exp esc funs e =
   match dest_exp_core e with
      LetAtom (_, _, a, e)
    | LetSubscript (_, _, _, _, a, e)
    | SetSubscript (_, _, _, _, _, a, e)
    | SetGlobal (_, _, _, _, a, e) ->
         esc_exp (esc_atom esc funs a) funs e
    | LetGlobal (_, _, _, l, e) ->
         esc_exp (esc_var esc funs l) funs e
    | LetExt (_, _, _, _, _, _, args, e) ->
         esc_exp (esc_atoms esc funs args) funs e
    | TailCall (_, _, args) ->
         esc_atoms esc funs args
    | SpecialCall (_, op) ->
         esc_tailop esc funs op
    | Match (a, cases) ->
         List.fold_left (fun esc (_, _, e) ->
               esc_exp esc funs e) (esc_atom esc funs a) cases
    | MatchDTuple (a, cases) ->
         List.fold_left (fun esc (_, _, e) ->
               esc_exp esc funs e) (esc_atom esc funs a) cases
    | TypeCase (a1, a2, _, _, e1, e2) ->
         esc_atom (esc_atom (esc_exp (esc_exp esc funs e1) funs e2) funs a2) funs a1
    | LetAlloc (_, op, e) ->
         esc_exp (esc_alloc_op esc funs op) funs e
    | Memcpy (_, _, _, _, _, _, _, e)
    | Call (_, _, _, e)
    | Assert (_, _, e)
    | Debug (_, e) ->
         esc_exp esc funs e


(*
 * Escpaing functions in the program.
 *)
let esc_prog prog =
   let { prog_funs = funs;
         prog_export = export;
         prog_globals = globals
       } = prog
   in

   (* Add exported functions to the list *)
   let esc =
      SymbolTable.fold (fun esc v _ ->
            esc_var esc funs v) SymbolSet.empty export
   in
   let esc =
      SymbolTable.fold (fun esc v (_, init) ->
            esc_init esc funs init) esc globals
   in
   let esc =
      SymbolTable.fold (fun esc _ (_, _, _, _, e) ->
            esc_exp esc funs e) esc funs
   in
      SymbolSet.to_list esc


(************************************************************************
 * GLOBAL
 ************************************************************************)


(*
 * Marshal the program.
 *)
let mprog_of_prog prog =
   (* Build the RTTD data structures *)
   let prog, rttd = rttd_prog prog in

   (* Get the escaping functions *)
   let esc = esc_prog prog in

   (* Order the globals *)
   let globals =
      SymbolTable.fold (fun globals v _ ->
            v :: globals) [] prog.prog_globals
   in
      { marshal_fir = prog;
        marshal_globals = globals;
        marshal_funs = esc;
        marshal_rttd = rttd
      }


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
