(*
 * Some common unitilites.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_ds

(*
 * Field tables.
 *)
(*
 * Frame label tables.
 *)
module FrameLabelCompare =
struct
   type t = frame_label

   let compare (x1, x2, x3) (y1, y2, y3) =
      let cmp1 = Symbol.compare x1 y1 in
         if cmp1 = 0 then
            let cmp2 = Symbol.compare x2 y2 in
               if cmp2 = 0 then
                  Symbol.compare x3 y3
               else
                  cmp2
         else
            cmp1
end

module FrameLabelTable = Mc_map.McMake (FrameLabelCompare)

(*
 * Map a function over all the types in the unop.
 *)
let map_type_unop f op =
   match op with
      NotEnumOp _

      (* Negation (arithmetic and bitwise) for NAML ints *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Negation (arithmetic and bitwise) for native ints *)
    | UMinusRawIntOp _
    | NotRawIntOp _

      (*
       * RawBitFieldOp (pre, signed, offset, length):
       *    Extracts the bitfield in an integer value, which starts at bit
       *    <offset> (counting from LSB) and containing <length> bits. The
       *    bit shift and length are constant values.  To access a variable
       *    bitfield you must do a manual shift/mask; this optimization is
       *    only good for constant values.  (pre, signed) refer to the
       *    rawint precision of the result; the input should be int32.
       *)
    | RawBitFieldOp     _

      (* Unary floating-point operations *)
    | UMinusFloatOp     _
    | AbsFloatOp        _
    | SinFloatOp        _
    | CosFloatOp        _
    | TanFloatOp        _
    | ASinFloatOp       _
    | ACosFloatOp       _
    | ATanFloatOp       _
    | SinHFloatOp       _
    | CosHFloatOp       _
    | TanHFloatOp       _
    | ExpFloatOp        _
    | LogFloatOp        _
    | Log10FloatOp      _
    | SqrtFloatOp       _
    | CeilFloatOp       _
    | FloorFloatOp      _

      (*
       * Coercions:
       *    Int refers to an ML-style integer
       *    Float refers to floating-point value
       *    RawInt refers to native int, any precision
       *
       * In operators where both terms have precision qualifiers, the
       * destination precision is always specified before the source
       * precision.
       *)

      (* Coercions to int *)
    | IntOfFloatOp      _
    | IntOfRawIntOp     _

      (* Coerce to float *)
    | FloatOfIntOp      _
    | FloatOfFloatOp    _
    | FloatOfRawIntOp   _

      (* Coerce to rawint *)
    | RawIntOfIntOp     _
    | RawIntOfEnumOp    _
    | RawIntOfFloatOp   _

      (*
       * Coerce a rawint _
       * First pair is destination precision, second pair is source.
       *)
    | RawIntOfRawIntOp  _

      (*
       * Integer<->pointer coercions (only for C, not inherently safe)
       * These operators are specifically related to infix pointers...
       *)
    | RawIntOfPointerOp _
    | PointerOfRawIntOp _

      (*
       * Create an infix pointer from a block pointer.  The infix pointer
       * so that both the base and _
       * to the beginning _
       *)
    | PointerOfBlockOp _ ->
         op

      (*
       * Get the block length.
       *)
    | LengthOfBlockOp (subop, ty) ->
         LengthOfBlockOp (subop, f ty)

      (*
       * Type coercions.
       *)
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         DTupleOfDTupleOp (ty_var, List.map (fun (ty, b) -> f ty, b) ty_fields)
    | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
         UnionOfUnionOp (ty_var, List.map f tyl, s1, s2)
    | RawDataOfFrameOp (ty_var, tyl) ->
         RawDataOfFrameOp (ty_var, List.map f tyl)

(*
 * Map a function over all the types in the binop.
 *)
let map_type_binop f op =
   match op with
      (* Bitwise operations on enumerations. *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

      (* Standard binary operations on NAML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on NAML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (*
       * Standard binary operations on native ints.  The precision is
       * the result precision; the inputs should match this precision.
       *)
    | PlusRawIntOp   _
    | MinusRawIntOp  _
    | MulRawIntOp    _
    | DivRawIntOp    _
    | RemRawIntOp    _
    | SlRawIntOp     _
    | SrRawIntOp     _
    | AndRawIntOp    _
    | OrRawIntOp     _
    | XorRawIntOp    _
    | MaxRawIntOp    _
    | MinRawIntOp    _

      (*
       * RawSetBitFieldOp (pre, signed, _
       *    See comments for RawBitFieldOp. This modifies the bitfield starting
       *    at bit <_
       *       First atom is the integer containing the field.
       *       Second atom is the value to be set in the field.
       *    The resulting integer contains the revised field, with type
       *    ACRawInt (pre, signed)
       *)
    | RawSetBitFieldOp _

      (* Comparisons on native ints *)
    | EqRawIntOp     _
    | NeqRawIntOp    _
    | LtRawIntOp     _
    | LeRawIntOp     _
    | GtRawIntOp     _
    | GeRawIntOp     _
    | CmpRawIntOp    _

      (* Standard binary operations on floats *)
    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

      (* Comparisons on floats *)
    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

      (*
       * Arctangent.  This computes arctan(y/x), where y is the first atom
       * and x is the second atom given.  Handles case when x = 0 correctly.
       *)
    | ATan2FloatOp _

      (*
       * Power.  This computes x^y.
       *)
    | PowerFloatOp _

      (*
       * Float hacking.
       * This sets the exponent field _
       *)
    | LdExpFloatIntOp _

      (*
       * Pointer arithmetic. The pointer in the first argument, and the
       * returned pointer should be infix pointers (which keep the base
       * pointer as well as a pointer to anywhere within the block).
       *)
    | PlusPointerOp _ ->
         op


      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty ->
         EqEqOp (f ty)
    | NeqEqOp ty ->
         NeqEqOp (f ty)

(*
 * Map a function over all types in the atom.
 *)
let rec map_type_atom f a =
   match a with
      AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomVar _
    | AtomFun _
    | AtomLabel _
    | AtomSizeof _
    | AtomTyUnpack _ ->
         a
    | AtomNil ty ->
         AtomNil (f ty)
    | AtomConst (ty, ty_v, i) ->
         AtomConst (f ty, ty_v, i)
    | AtomTyApply (v, ty, tyl) ->
         AtomTyApply (v, f ty, List.map f tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, f ty, List.map f tyl)
    | AtomUnop (op, a) ->
         AtomUnop (map_type_unop f op, map_type_atom f a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (map_type_binop f op, map_type_atom f a1, map_type_atom f a2)

let map_type_atom_opt f a =
   match a with
      Some a ->
         Some (map_type_atom f a)
    | None ->
         None

let map_type_atoms f args =
   List.map (map_type_atom f) args

let map_type_atom_opt_list f args =
   List.map (map_type_atom_opt f) args

(*
 * Map a type over all the types in the alloc_op.
 *)
let map_type_alloc_op f op =
   match op with
      AllocTuple (tclass, ty_vars, ty, args) ->
         AllocTuple (tclass, ty_vars, f ty, map_type_atoms f args)
    | AllocUnion (ty_vars, ty, ty_v, i, args) ->
         AllocUnion (ty_vars, f ty, ty_v, i, map_type_atoms f args)
    | AllocArray (ty, args) ->
         AllocArray (f ty, map_type_atoms f args)
    | AllocVArray (ty, sub_index, a1, a2) ->
         AllocVArray (f ty, sub_index, map_type_atom f a1, map_type_atom f a2)
    | AllocMalloc (ty, a) ->
         AllocMalloc (f ty, map_type_atom f a)
    | AllocFrame (v, tyl) ->
         AllocFrame (v, List.map f tyl)
    | AllocDTuple (ty, ty_var, a, args) ->
         AllocDTuple (f ty, ty_var, map_type_atom f a, map_type_atoms f args)

(*
 * Map a type over the types in the debug_info.
 *)
let map_type_debug_var f (v1, ty, v2) =
   v1, f ty, v2

let map_type_debug_vars f vars =
   List.map (map_type_debug_var f) vars

let map_type_debug_info f info =
   match info with
      DebugString _ ->
         info
    | DebugContext (line, vars) ->
         DebugContext (line, map_type_debug_vars f vars)

(*
 * Map a function over the types in the pred.
 *)
let map_type_pred f pred =
   match pred with
      IsMutable a ->
         IsMutable (map_type_atom f a)
    | Reserve (a1, a2) ->
         Reserve (map_type_atom f a1, map_type_atom f a2)
    | BoundsCheck (op, a1, a2, a3) ->
         BoundsCheck (op, map_type_atom f a1, map_type_atom f a2, map_type_atom f a3)
    | ElementCheck (ty, op, a1, a2) ->
         ElementCheck (f ty, op, map_type_atom f a1, map_type_atom f a2)

(*
 * Map a function over all types.
 *)
let rec map_type_exp f e =
   let loc = loc_of_exp e in
   let e = map_type_exp_core f (dest_exp_core e) in
      make_exp loc e

and map_type_exp_core f e =
   match e with
      LetAtom (v, ty, a, e) ->
         let ty = f ty in
         let a = map_type_atom f a in
         let e = map_type_exp f e in
            LetAtom (v, ty, a, e)
    | LetExt (v, ty, s, b, ty2, ty_args, al, e) ->
         let ty = f ty in
         let ty2 = f ty2 in
         let ty_args = List.map f ty_args in
         let al = map_type_atoms f al in
         let e = map_type_exp f e in
            LetExt (v, ty, s, b, ty2, ty_args, al, e)
    | TailCall (label, v, args) ->
         let v = map_type_atom f v in
         let args = map_type_atoms f args in
            TailCall (label, v, args)
    | SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, fn, args)) ->
         let loc_ptr = map_type_atom f loc_ptr in
         let loc_off = map_type_atom f loc_off in
         let fn = map_type_atom f fn in
         let args = map_type_atoms f args in
            SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, fn, args))
    | SpecialCall (label, TailAtomic (fn, c, args)) ->
         let c = map_type_atom f c in
         let fn = map_type_atom f fn in
         let args = map_type_atoms f args in
            SpecialCall (label, TailAtomic (fn, c, args))
    | SpecialCall (label, TailAtomicRollback (level, c)) ->
         let level = map_type_atom f level in
         let c = map_type_atom f c in
            SpecialCall (label, TailAtomicRollback (level, c))
    | SpecialCall (label, TailAtomicCommit (level, fn, args)) ->
         let level = map_type_atom f level in
         let fn = map_type_atom f fn in
         let args = map_type_atoms f args in
            SpecialCall (label, TailAtomicCommit (level, fn, args))
    | Match (a, cases) ->
         let a = map_type_atom f a in
         let cases =
            List.map (fun (label, set, e) -> label, set, map_type_exp f e) cases
         in
            Match (a, cases)
    | MatchDTuple (a, cases) ->
         let a = map_type_atom f a in
         let cases =
            List.map (fun (label, a_opt, e) ->
                  label, map_type_atom_opt f a_opt, map_type_exp f e) cases
         in
            MatchDTuple (a, cases)
    | TypeCase (a1, a2, name, v, e1, e2) ->
         let a1 = map_type_atom f a1 in
         let a2 = map_type_atom f a2 in
         let e1 = map_type_exp f e1 in
         let e2 = map_type_exp f e2 in
            TypeCase (a1, a2, name, v, e1, e2)
    | LetAlloc (v, op, e) ->
         let op = map_type_alloc_op f op in
         let e = map_type_exp f e in
            LetAlloc (v, op, e)
    | LetSubscript (op, v1, ty, a2, a3, e) ->
         let ty = f ty in
         let a2 = map_type_atom f a2 in
         let a3 = map_type_atom f a3 in
         let e = map_type_exp f e in
            LetSubscript (op, v1, ty, a2, a3, e)
    | SetSubscript (op, label, a1, a2, ty, a3, e) ->
         let ty = f ty in
         let a1 = map_type_atom f a1 in
         let a2 = map_type_atom f a2 in
         let a3 = map_type_atom f a3 in
         let e = map_type_exp f e in
            SetSubscript (op, label, a1, a2, ty, a3, e)
    | LetGlobal (op, v, ty, l, e) ->
         let ty = f ty in
         let e = map_type_exp f e in
            LetGlobal (op, v, ty, l, e)
    | SetGlobal (op, label, v, ty, a, e) ->
         let ty = f ty in
         let a = map_type_atom f a in
         let e = map_type_exp f e in
            SetGlobal (op, label, v, ty, a, e)
    | Memcpy (op, label, a1, a2, a3, a4, a5, e) ->
         let a1 = map_type_atom f a1 in
         let a2 = map_type_atom f a2 in
         let a3 = map_type_atom f a3 in
         let a4 = map_type_atom f a4 in
         let a5 = map_type_atom f a5 in
         let e = map_type_exp f e in
            Memcpy (op, label, a1, a2, a3, a4, a5, e)
    | Assert (label, pred, e) ->
         Assert (label, map_type_pred f pred, map_type_exp f e)
    | Call (label, v, args, e) ->
         Call (label, v, map_type_atom_opt_list f args, map_type_exp f e)
    | Debug (info, e) ->
         let info = map_type_debug_info f info in
         let e = map_type_exp f e in
            Debug (info, e)

(*
 * Map a function over a type definition.
 *)
let map_type_tydef f tydef =
   match tydef with
      TyDefUnion (ty_vars, fields) ->
         let fields =
            List.map (List.map (fun (ty, b) -> f ty, b)) fields
         in
            TyDefUnion (ty_vars, fields)
    | TyDefLambda (ty_vars, ty) ->
         TyDefLambda (ty_vars, f ty)
    | TyDefDTuple _ ->
         tydef

(*
 * Map a function over a global initializer.
 *)
let map_type_init f init =
   match init with
      InitAtom a ->
         InitAtom (map_type_atom f a)
    | InitAlloc op ->
         InitAlloc (map_type_alloc_op f op)
    | InitTag (ty_var, tyl) ->
         InitTag (ty_var, List.map (fun (ty, b) -> f ty, b) tyl)
    | InitRawData _
    | InitNames _ ->
         init

let map_type_globals f globals =
   SymbolTable.map (fun (ty, init) ->
         f ty, map_type_init f init) globals

let map_type_import f import =
   SymbolTable.map (fun import ->
         { import with import_type = f import.import_type }) import

let map_type_export f export =
   SymbolTable.map (fun export ->
         { export with export_type = f export.export_type }) export

(*
 * Map a function over a function definition.
 *)
let map_type_fundef f (line, ty_vars, ty, vars, e) =
   line, ty_vars, f ty, vars, map_type_exp f e

let map_type_funs f funs =
   SymbolTable.map (map_type_fundef f) funs

(*
 * Map a function over the type definitions.
 *)
let map_type_types f types =
   SymbolTable.map f types

let map_type_tydefs f types =
   SymbolTable.map (map_type_tydef f) types

(*
 * Map a function over the type definitions in the frame.
 *)
let map_type_frames f frames =
   SymbolTable.map (fun (vars, frame) ->
         let frame =
            SymbolTable.map (fun vars ->
                  List.map (fun (v, ty, i) -> v, f ty, i) vars) frame
         in
            vars, frame) frames

(*
 * Map a function over all types in the program.
 *)
let map_type_prog f
    { prog_file = file;
      prog_import = import;
      prog_export = export;
      prog_types = types;
      prog_frames = frames;
      prog_names = names;
      prog_globals = globals;
      prog_funs = funs
    } =
   { prog_file = file;
     prog_import = map_type_import f import;
     prog_export = map_type_export f export;
     prog_types = map_type_tydefs f types;
     prog_frames = map_type_frames f frames;
     prog_names = map_type_types f names;
     prog_globals = map_type_globals f globals;
     prog_funs = map_type_funs f funs
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
