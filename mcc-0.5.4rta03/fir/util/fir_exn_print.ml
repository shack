(*
 * Normal FIR exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fir
open Fir_exn
open Fir_pos
open Fir_print
open Fir_state

module Pos = MakePos (struct let name = "Fir_exn_print" end)
open Pos

(*
 * Exception printer.
 *)
let pp_print_exn buf e =
   match e with
      FirException (pos, exn) ->
         Format.fprintf buf "@[<v 0>%a@ @[<hv 3>*** FIR Error:@ %a@]@]@." (**)
            pp_print_pos pos
            pp_print_error exn
    | exn ->
         let s = Printexc.to_string exn in
            Format.fprintf buf "FIR uncaught exception:@ %s@." s;
            raise exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      FirException _ as exn ->
         pp_print_exn err_formatter exn;
         exit 2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
