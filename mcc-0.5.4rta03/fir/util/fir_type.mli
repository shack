(*
 * Type checker for FIR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir
open Fir_env
open Fir_pos
open Fir_set

(*
 * Standard types.
 *)
val ty_int8 : ty
val ty_int16 : ty
val ty_int32 : ty
val ty_int64 : ty
val ty_uint64 : ty

val ty_float32 : ty
val ty_float64 : ty
val ty_float80 : ty

val ty_char : ty
val ty_bool : ty
val ty_string : ty
val ty_unit : ty
val ty_void : ty
val ty_subscript : ty

val ty_ml_string : ty

(* Type of the FC main function. *)
val ty_fc_main : ty

val int_set_zero : int_set
val int_set_one : int_set

val true_set : int_set
val false_set : int_set

(*
 * Atom should be a variable.
 *)
val var_of_atom : pos -> atom -> var
val var_of_fun : pos -> atom -> var

(*
 * Type operations using just the type environment.
 *)
val apply_type_dir : tenv -> pos -> ty_var -> ty list -> ty
val apply_union_dir : tenv -> pos -> ty_var -> ty list -> mutable_ty list list

val tenv_expand_dir : tenv -> pos -> ty -> ty
val union_size_dir : tenv -> pos -> ty_var -> int

val is_dtuple_tydef_dir : tenv -> pos -> ty_var -> bool

val is_fun_type_dir : tenv -> pos -> ty -> bool
val is_pointer_type_dir : tenv -> pos -> ty -> bool
val is_frame_type_dir : tenv -> pos -> ty -> bool
val is_rawdata_type_dir : tenv -> pos -> ty -> bool

val dest_exists_type_dir : tenv -> pos -> ty -> var list * ty
val dest_all_type_dir : tenv -> pos -> ty -> var list * ty
val dest_all_type_rename_dir : tenv -> pos -> ty -> ty_var list -> ty
val dest_all_fun_type_dir : tenv -> pos -> ty -> var list * ty list * ty
val dest_fun_type_dir : tenv -> pos -> ty -> ty list * ty
val dest_union_type_dir : tenv -> pos -> ty -> ty_var * ty list * int_set
val dest_tuple_type_dir : tenv -> pos -> ty -> mutable_ty list
val dest_dtuple_type_dir : tenv -> pos -> ty -> ty_var * mutable_ty list option
val dest_tag_type_dir : tenv -> pos -> ty -> ty_var * mutable_ty list
val dest_array_type_dir : tenv -> pos -> ty -> ty
val dest_case_type_dir : tenv -> pos -> ty -> ty
val dest_object_type_dir : tenv -> pos -> ty -> ty
val dest_frame_type_dir : tenv -> pos -> ty -> label * ty list
val poly_fun_type_dir : tenv -> pos -> ty -> ty list * ty

val unfold_object_type_dir : tenv -> pos -> ty -> ty

val sub_value_of_type_dir : tenv -> pos -> ty -> sub_value

(*
 * Type operations using the global environment.
 *
 * type_of_unop returns the (dst, src) types
 * type_of_binop returns the (dst, src1, src2) types
 *)
val type_of_unop : pos -> unop -> ty * ty
val type_of_binop : pos -> binop -> ty * ty * ty
val type_of_alloc_op : 'a poly_alloc_op -> ty
val type_of_atom : genv -> pos -> atom -> ty
val type_of_global_atom : genv -> pos -> atom -> ty

val apply_type : genv -> pos -> ty_var -> ty list -> ty
val apply_union : genv -> pos -> ty_var -> ty list -> mutable_ty list list
val apply_frame : genv -> pos -> ty_var -> ty list -> frame_label -> ty * int

val expand_type : genv -> pos -> ty -> ty
val union_size : genv -> pos -> ty_var -> int

val is_dtuple_tydef : genv -> pos -> ty_var -> bool

val is_fun_type : genv -> pos -> ty -> bool
val is_pointer_type : genv -> pos -> ty -> bool
val is_frame_type : genv -> pos -> ty -> bool
val is_rawdata_type : genv -> pos -> ty -> bool

val dest_union_tydef : pos -> tydef -> var list * mutable_ty list list

val dest_exists_type : genv -> pos -> ty -> var list * ty
val dest_all_type : genv -> pos -> ty -> var list * ty
val dest_all_type_rename : genv -> pos -> ty -> ty_var list -> ty
val dest_all_fun_type : genv -> pos -> ty -> var list * ty list * ty
val dest_fun_type : genv -> pos -> ty -> ty list * ty
val dest_union_type : genv -> pos -> ty -> ty_var * ty list * int_set
val dest_tuple_type : genv -> pos -> ty -> mutable_ty list
val dest_dtuple_type : genv -> pos -> ty -> ty_var * mutable_ty list option
val dest_tag_type : genv -> pos -> ty -> ty_var * mutable_ty list
val dest_array_type : genv -> pos -> ty -> ty
val dest_case_type : genv -> pos -> ty -> ty
val dest_object_type : genv -> pos -> ty -> ty
val dest_frame_type : genv -> pos -> ty -> label * ty list
val poly_fun_type : genv -> pos -> ty -> ty list * ty

val unfold_object_type : genv -> pos -> ty -> ty

val sub_value_of_type : genv -> pos -> ty -> sub_value

val prog_genv : prog -> genv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
