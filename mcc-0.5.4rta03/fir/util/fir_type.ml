(*
 * FIR type checker.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol
open Interval_set

open Fir
open Fir_ds
open Fir_set
open Fir_exn
open Fir_pos
open Fir_env
open Fir_subst
open Fir_state
open Fir_print

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_type" end)
open Pos


(************************************************************************
 * WELL-KNOWN TYPES
 ************************************************************************)

let ty_int8    = TyRawInt (Rawint.Int8,  false)
let ty_int16   = TyRawInt (Rawint.Int16, true)
let ty_int32   = TyRawInt (Rawint.Int32, true)
let ty_int64   = TyRawInt (Rawint.Int64, true)
let ty_uint64  = TyRawInt (Rawint.Int64, false)

let ty_float32 = TyFloat Rawfloat.Single
let ty_float64 = TyFloat Rawfloat.Double
let ty_float80 = TyFloat Rawfloat.LongDouble

let ty_char       = TyRawInt (Rawint.Int8,  true)
let ty_subscript  = TyRawInt (precision_subscript, signed_subscript)

let ty_string     = TyArray ty_char
let ty_ml_string  = TyArray (TyEnum 256)

let int_set_zero  = IntSet.of_point 0
let false_set     = int_set_zero
let int_set_one   = IntSet.of_point 1
let true_set      = int_set_one

let ty_bool = TyEnum 2
let ty_unit = TyEnum 1
let ty_void = TyEnum 0

(* Type of the FC main function. *)
(*
 * BUG: Assumes the 2 argument form of main.
 *)
let ty_fc_sym1 = new_symbol_string "ty_fc_main"
let ty_fc_sym2 = new_symbol_string "ty_fc_main"
let ty_fc_main =
   TyAll ( [ ty_fc_sym1; ty_fc_sym2 ],
      TyFun (  [  ty_int32;
                  TyRawData;
                  ty_int32;
                  TyFun ( [ TyVar ty_fc_sym1; ty_int32 ], ty_void );
                  TyVar ty_fc_sym1;
                  TyFun ( [ TyVar ty_fc_sym2; TyRawData; ty_int32 ], ty_void );
                  TyVar ty_fc_sym2
               ],
               ty_void ) )

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Var should be an atom.
 *)
let var_of_atom pos a =
   match a with
      AtomVar v ->
         v
    | _ ->
         let pos = string_pos "var_of_atom" pos in
            raise (FirException (pos, StringAtomError ("not a var", a)))

let rec var_of_fun pos a =
   match a with
      AtomTyApply (a, _, _) ->
         var_of_fun pos a
    | AtomFun v
    | AtomVar v ->
         v
    | _ ->
         let pos = string_pos "var_of_fun" pos in
            raise (FirException (pos, StringAtomError ("not a var", a)))

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Apply the type.
 *)
let apply_type_dir tenv pos v tyl =
   let pos = string_pos "apply_type_dir" pos in
   match tenv_lookup tenv pos v with
      TyDefLambda (vars, ty) ->
         subst_type_simul pos ty vars tyl
    | TyDefUnion (vars, fields) ->
         let len1 = List.length vars in
         let len2 = List.length tyl in
         let _ =
            if len1 <> len2 then
               raise (FirException (pos, ArityMismatch (len1, len2)))
         in
         let tags_num = List.length fields in
         let tag_set = IntSet.of_interval (Closed 0) (Open tags_num) in
            TyUnion (v, tyl, tag_set)
    | TyDefDTuple v ->
         if tyl = [] then
            TyDTuple (v, None)
         else
            raise (FirException (pos, StringError "dependent tuples are not polymorphic"))

(*
 * Expand the union fields.
 *)
let apply_union_dir tenv pos v tyl =
   match tenv_lookup tenv pos v with
      TyDefUnion (vars, fields) ->
         if vars = [] then
            fields
         else
            let tvenv =
               let len1 = List.length vars in
               let len2 = List.length tyl in
                  if len1 <> len2 then
                     raise (FirException (pos, ArityMismatch (len1, len2)));
                  List.fold_left2 subst_add_type_var subst_empty vars tyl
            in
               List.map (List.map (fun (ty, b) -> subst_type tvenv ty, b)) fields
    | tyd ->
         raise (FirException (pos, TyDefError tyd))

let union_size_dir tenv pos tv =
   match tenv_lookup tenv pos tv with
      TyDefUnion (_, l) ->
         List.length l
    | tyd ->
         raise (FirException (string_pos "union_size" pos, TyDefError tyd))

(*
 * Expand a type one level.
 *)
let rec tenv_expand_dir tenv pos ty =
   match ty with
      TyApply (v, tyl) ->
         tenv_expand_dir tenv pos (apply_type_dir tenv pos v tyl)
    | TyAll ([], ty)
    | TyExists ([], ty) ->
         tenv_expand_dir tenv pos ty
    | _ ->
         ty

(*
 * Destruct a union type.
 *)
let dest_union_tydef pos ty =
   let pos = string_pos "dest_union_tydef" pos in
      match ty with
         TyDefUnion (vars, fields) ->
            vars, fields
       | TyDefLambda _
       | TyDefDTuple _ ->
            raise (FirException (pos, StringTydefError ("not a union type", ty)))

(*
 * Check that the type var is a dependent tuple type.
 *)
let is_dtuple_tydef_dir tenv pos ty_var =
   let pos = string_pos "dest_dtuple_tydef" pos in
      match tenv_lookup tenv pos ty_var with
         TyDefDTuple _ ->
            true
       | TyDefUnion _
       | TyDefLambda _ ->
            false

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Tests.
 *)
let rec is_fun_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyFun _ ->
         true
    | TyAll (_, ty)
    | TyExists (_, ty) ->
         is_fun_type_dir tenv pos ty
    | _ ->
         false

let rec is_pointer_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyUnion _
    | TyTuple _
    | TyArray _
    | TyRawData
    | TyVar _
    | TyProject _
    | TyObject _
    | TyCase _
    | TyPointer _
    | TyFrame _
    | TyDTuple _ ->
         true
    | TyAll (_, ty)
    | TyExists (_, ty) ->
         is_pointer_type_dir tenv pos ty
    | TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyFun _
    | TyTag _ ->
         false
    | TyApply _
    | TyDelayed as ty ->
         raise (FirException (pos, StringTypeError ("invalid pointer test", ty)))

(*
 * Destructors.
 *)
let rec dest_exists_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyExists (vars, ty) ->
         vars, ty
    | ty ->
         [], ty

let rec dest_all_type_dir tenv pos ty =
   let pos = string_pos "dest_all_type_dir" pos in
      match tenv_expand_dir tenv pos ty with
         TyAll (vars1, ty) ->
            let vars2, ty = dest_all_type_dir tenv pos ty in
               vars1 @ vars2, ty
       | ty ->
            [], ty

let dest_all_type_rename_dir tenv pos ty ty_vars =
   let pos = string_pos "dest_all_type_rename_dir" pos in
   let ty_vars', ty = dest_all_type_dir tenv pos ty in
   let len1 = List.length ty_vars' in
   let len2 = List.length ty_vars in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let subst =
      List.fold_left2 (fun subst v1 v2 ->
            subst_add_type_var subst v1 (TyVar v2)) subst_empty ty_vars' ty_vars
   in
      subst_type subst ty

(*
 * Break apart the function type.
 * Also destruct the universal quantifier.
 *)
let rec dest_all_fun_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyFun (ty_vars, ty_res) ->
         [], ty_vars, ty_res
    | TyAll (vars, ty) ->
         let vars', ty_vars, ty_res = dest_all_fun_type_dir tenv pos ty in
            vars @ vars', ty_vars, ty_res
    | ty ->
         raise (FirException (pos, NotAFunction ty))

let dest_fun_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyFun (ty_vars, ty_res) ->
         ty_vars, ty_res
    | ty ->
         raise (FirException (pos, NotAFunction ty))

(*
 * Get some random new type for the function
 * by using new vars for the all quantifiers.
 *)
let poly_fun_type_dir tenv pos ty =
   let pos = string_pos "poly_fun_type" pos in
   let ty_vars, ty_args, ty_res = dest_all_fun_type_dir tenv pos ty in
   let ty_vars' = List.map (fun v -> TyVar (new_symbol_pre "poly" v)) ty_vars in
   let subst = List.fold_left2 subst_add_type_var subst_empty ty_vars ty_vars' in
   let ty_args = List.map (subst_type subst) ty_args in
   let ty_res = subst_type subst ty_res in
      ty_args, ty_res

let rec dest_union_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyUnion (tv, tl, i) ->
         tv, tl, i
    | _ ->
         raise (FirException (pos, NotAUnion ty))

let dest_tuple_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyTuple (_, tl) ->
         tl
    | ty ->
         raise (FirException (pos, StringTypeError ("not a tuple type", ty)))

let dest_dtuple_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyDTuple (ty_var, fields) ->
         ty_var, fields
    | ty ->
         raise (FirException (pos, StringTypeError ("not a dependent tuple type", ty)))

let dest_tag_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyTag (ty_var, fields) ->
         ty_var, fields
    | ty ->
         raise (FirException (pos, StringTypeError ("not a tag type", ty)))

let rec dest_array_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyArray ty ->
         ty
    | ty ->
         raise (FirException (pos, StringTypeError ("not an array type", ty)))

let dest_case_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyCase ty ->
         ty
    | ty ->
         raise (FirException (pos, StringTypeError ("not a case type", ty)))

let dest_object_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyObject (v, ty') ->
         let subst = subst_add_type_var subst_empty v ty in
            subst_type subst ty'
    | ty ->
         raise (FirException (pos, StringTypeError ("not an object type", ty)))

let unfold_object_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyObject (v, ty') ->
         let subst = subst_add_type_var subst_empty v ty in
            subst_type subst ty'
    | ty ->
         ty

let is_frame_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyFrame _ ->
         true
    | _ ->
         false

let dest_frame_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyFrame (label, tyl) ->
         label, tyl
    | ty ->
         raise (FirException (pos, StringTypeError ("not a frame type", ty)))

let is_rawdata_type_dir tenv pos ty =
   match tenv_expand_dir tenv pos ty with
      TyRawData ->
         true
    | _ ->
         false

(*
 * Existential unpacking.
 *)
let unpack_type_dir tenv pos v ty =
   let pos = string_pos "unpack_type" pos in
   let vars, ty = dest_exists_type_dir tenv pos ty in
   let subst, _ =
      List.fold_left (fun (subst, i) v' ->
            let subst = subst_add_type_var subst v' (TyProject (v, i)) in
               subst, succ i) (subst_empty, 0) vars
   in
      subst_type subst ty

(*
 * Get the subscript value from the type.
 *)
let rec sub_value_of_type_dir tenv pos ty =
   let pos = string_pos "sub_value_of_type" pos in
      match tenv_expand_dir tenv pos ty with
         TyInt ->
            IntSub
       | TyTag (ty_var, tyl) ->
            TagSub (ty_var, tyl)
       | TyEnum n ->
            EnumSub n
       | TyRawInt (pre, signed) ->
            RawIntSub (pre, signed)
       | TyFloat pre ->
            RawFloatSub pre
       | TyFun _ ->
            FunctionSub
       | TyUnion _
       | TyTuple _
       | TyDTuple _
       | TyArray _
       | TyCase _
       | TyObject _ ->
            BlockPointerSub
       | TyRawData
       | TyFrame _ ->
            RawPointerSub
       | TyPointer _ ->
            PointerInfixSub
       | TyVar _
       | TyProject _
       | TyDelayed ->
            PolySub
       | TyExists (_, ty)
       | TyAll (_, ty) ->
            sub_value_of_type_dir tenv pos ty
       | TyApply _ ->
            raise (Invalid_argument "sub_value_of_type")

(************************************************************************
 * GLOBAL VERSIONS
 ************************************************************************)

(*
 * Figure out the type of a field in a frame.
 *)
let apply_frame genv pos v tyl (_, v_field, v_subfield) =
   let pos = string_pos "apply_frame_dir" pos in
   let vars, fields = genv_lookup_frame genv pos v in
   let subfields =
      try SymbolTable.find fields v_field with
         Not_found ->
            raise (FirException (pos, StringVarError ("unbound field", v_field)))
   in
   let ty, i =
      let rec search = function
         (v_subfield', ty, i) :: subfields ->
            if Symbol.eq v_subfield' v_subfield then
               ty, i
            else
               search subfields
       | [] ->
            raise (FirException (pos, StringVarError ("unbound subfield", v_subfield)))
      in
         search subfields
   in
   let ty = subst_type_simul pos ty vars tyl in
      ty, i

(*
 * All the rest are straightforward.
 *)
let apply_type genv = apply_type_dir (tenv_of_genv genv)
let apply_union genv = apply_union_dir (tenv_of_genv genv)

let expand_type genv = tenv_expand_dir (tenv_of_genv genv)
let union_size genv = union_size_dir (tenv_of_genv genv)

let is_dtuple_tydef genv = is_dtuple_tydef_dir (tenv_of_genv genv)

let is_fun_type genv = is_fun_type_dir (tenv_of_genv genv)
let is_pointer_type genv = is_pointer_type_dir (tenv_of_genv genv)
let is_frame_type genv = is_frame_type_dir (tenv_of_genv genv)
let is_rawdata_type genv = is_rawdata_type_dir (tenv_of_genv genv)

let dest_exists_type genv = dest_exists_type_dir (tenv_of_genv genv)
let dest_all_type genv = dest_all_type_dir (tenv_of_genv genv)
let dest_all_type_rename genv = dest_all_type_rename_dir (tenv_of_genv genv)
let dest_all_fun_type genv = dest_all_fun_type_dir (tenv_of_genv genv)
let dest_fun_type genv = dest_fun_type_dir (tenv_of_genv genv)
let poly_fun_type genv = poly_fun_type_dir (tenv_of_genv genv)
let dest_union_type genv = dest_union_type_dir (tenv_of_genv genv)
let dest_tuple_type genv = dest_tuple_type_dir (tenv_of_genv genv)
let dest_dtuple_type genv = dest_dtuple_type_dir (tenv_of_genv genv)
let dest_tag_type genv = dest_tag_type_dir (tenv_of_genv genv)
let dest_array_type genv = dest_array_type_dir (tenv_of_genv genv)
let dest_case_type genv = dest_case_type_dir (tenv_of_genv genv)
let dest_object_type genv = dest_object_type_dir (tenv_of_genv genv)
let dest_frame_type genv = dest_frame_type_dir (tenv_of_genv genv)

let unfold_object_type genv = unfold_object_type_dir (tenv_of_genv genv)

let sub_value_of_type genv = sub_value_of_type_dir (tenv_of_genv genv)

let unpack_type genv = unpack_type_dir (tenv_of_genv genv)

(************************************************************************
 * EXPRESSION CHECKING
 ************************************************************************)

(*
 * Type of an allocation.
 *)
let type_of_alloc_op op =
   match op with
      AllocTuple (_, ty_vars, ty, _)
    | AllocUnion (ty_vars, ty, _, _, _) ->
         TyAll (ty_vars, ty)
    | AllocDTuple (ty, _, _, _)
    | AllocArray (ty, _)
    | AllocVArray (ty, _, _, _)
    | AllocMalloc (ty, _) ->
         ty
    | AllocFrame (v, tyl) ->
         TyFrame (v, tyl)


(*
 * Get argument and result types of unary operators.
 *)
let type_of_unop pos op =
   let pos = string_pos "type_of_unop" pos in
      match op with
         NotEnumOp n ->
            let ty = TyEnum n in
               ty, ty

       | UMinusIntOp
       | AbsIntOp
       | NotIntOp ->
            TyInt, TyInt
       | UMinusRawIntOp (pre, signed)
       | NotRawIntOp (pre, signed) ->
            let ty = TyRawInt (pre, signed) in
               ty, ty

       | RawBitFieldOp (pre, signed, _, _) ->
            if pre = Rawint.Int64 then
               raise (FirException (pos, StringError "RawBitFieldOp cannot return Int64"));
            TyRawInt (pre, signed), TyRawInt (Rawint.Int32, signed)

       | UMinusFloatOp pre
       | AbsFloatOp pre
       | SinFloatOp pre
       | CosFloatOp pre
       | TanFloatOp pre
       | ASinFloatOp pre
       | ACosFloatOp pre
       | ATanFloatOp pre
       | SinHFloatOp pre
       | CosHFloatOp pre
       | TanHFloatOp pre
       | ExpFloatOp pre
       | LogFloatOp pre
       | Log10FloatOp pre
       | SqrtFloatOp pre
       | CeilFloatOp pre
       | FloorFloatOp pre ->
            let ty = TyFloat pre in
               ty, ty

       | IntOfFloatOp pre ->
            TyInt, TyFloat pre

       | FloatOfIntOp pre ->
            TyFloat pre, TyInt
       | FloatOfFloatOp (pre1, pre2) ->
            TyFloat pre1, TyFloat pre2
       | FloatOfRawIntOp (fpre, rpre, rsigned) ->
            TyFloat fpre, TyRawInt (rpre, rsigned)
       | IntOfRawIntOp (pre, signed) ->
            TyInt, TyRawInt (pre, signed)
       | RawIntOfIntOp (pre, signed) ->
            TyRawInt (pre, signed), TyInt
       | RawIntOfEnumOp (pre, signed, n) ->
            TyRawInt (pre, signed), TyEnum n
       | RawIntOfRawIntOp (pre1, signed1, pre2, signed2) ->
            TyRawInt (pre1, signed1), TyRawInt (pre2, signed2)
       | RawIntOfFloatOp (pre, signed, fpre) ->
            TyRawInt (pre, signed), TyFloat fpre

       | LengthOfBlockOp (subop, ty) ->
            let ty_res =
               match subop.sub_script with
                  IntIndex -> TyInt
                | RawIntIndex (p, s) -> TyRawInt (p, s)
            in
               ty_res, ty

       | DTupleOfDTupleOp (ty_var, ty_fields) ->
            TyDTuple (ty_var, None), TyDTuple (ty_var, Some ty_fields)
       | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
            TyUnion (ty_var, tyl, s1), TyUnion (ty_var, tyl, s2)
       | RawDataOfFrameOp (ty_var, tyl) ->
            TyRawData, TyFrame (ty_var, tyl)

         (* BUG: wait until we understand these *)
       | RawIntOfPointerOp _
       | PointerOfRawIntOp _
       | PointerOfBlockOp _ ->
            raise (FirException (pos, NotImplemented "type checking for rawint/pointer conversions"))

(*
 * Get the argument and result type for a binary operation.
 *)
let type_of_binop pos op =
   let pos = string_pos "type_of_binop" pos in
      match op with
         (* Bitwise operations on enumerations. *)
         AndEnumOp n
       | OrEnumOp n
       | XorEnumOp n ->
            let ty = TyEnum n in
               ty, ty, ty

         (* Standard binary operations on NAML ints *)
       | PlusIntOp
       | MinusIntOp
       | MulIntOp
       | DivIntOp
       | RemIntOp
       | LslIntOp
       | LsrIntOp
       | AsrIntOp
       | AndIntOp
       | OrIntOp
       | XorIntOp
       | MaxIntOp
       | MinIntOp ->
            TyInt, TyInt, TyInt

         (* Comparisons on NAML ints *)
       | EqIntOp
       | NeqIntOp
       | LtIntOp
       | LeIntOp
       | GtIntOp
       | GeIntOp ->
            ty_bool, TyInt, TyInt

         (* Similar to ML's ``compare'' function. *)
       | CmpIntOp ->
            TyInt, TyInt, TyInt

         (*
          * Standard binary operations on native ints.  The precision is
          * the result precision; the inputs should match this precision.
          *)
       | PlusRawIntOp   (pre, signed)
       | MinusRawIntOp  (pre, signed)
       | MulRawIntOp    (pre, signed)
       | DivRawIntOp    (pre, signed)
       | RemRawIntOp    (pre, signed)
       | SlRawIntOp     (pre, signed)
       | SrRawIntOp     (pre, signed)
       | AndRawIntOp    (pre, signed)
       | OrRawIntOp     (pre, signed)
       | XorRawIntOp    (pre, signed)
       | MaxRawIntOp    (pre, signed)
       | MinRawIntOp    (pre, signed) ->
            let ty = TyRawInt (pre, signed) in
               ty, ty, ty

         (*
          * BUG (?): Someone may want to check this one again. --emre
          *)
       | RawSetBitFieldOp (pre, signed, _, _) ->
            let ty = TyRawInt (pre, signed) in
               ty, TyRawInt (Rawint.Int32, signed), ty

         (* Comparisons on native ints *)
       | EqRawIntOp     (pre, signed)
       | NeqRawIntOp    (pre, signed)
       | LtRawIntOp     (pre, signed)
       | LeRawIntOp     (pre, signed)
       | GtRawIntOp     (pre, signed)
       | GeRawIntOp     (pre, signed) ->
            let ty = TyRawInt (pre, signed) in
               ty_bool, ty, ty

       | CmpRawIntOp    (pre, signed) ->
            let ty = TyRawInt (pre, signed) in
               TyInt, ty, ty

         (* Standard binary operations on floats *)
       | PlusFloatOp pre
       | MinusFloatOp pre
       | MulFloatOp  pre
       | DivFloatOp  pre
       | RemFloatOp  pre
       | MaxFloatOp  pre
       | MinFloatOp  pre
       | ATan2FloatOp pre
       | PowerFloatOp pre ->
            let ty = TyFloat pre in
               ty, ty, ty

       | LdExpFloatIntOp pre ->
            let ty = TyFloat pre in
               ty, ty, TyInt

         (* Comparisons on floats *)
       | EqFloatOp   pre
       | NeqFloatOp  pre
       | LtFloatOp   pre
       | LeFloatOp   pre
       | GtFloatOp   pre
       | GeFloatOp   pre ->
            let ty = TyFloat pre in
               ty_bool, ty, ty
       | CmpFloatOp  pre ->
            let ty = TyFloat pre in
               TyInt, ty, ty

         (*
          * Pointer (in)equality.  Arguments must be pointers.
          * BUG: this is actually polymorphic.
          *)
       | EqEqOp ty
       | NeqEqOp ty ->
            ty_bool, ty, ty

         (*
          * Pointer arithmetic. The pointer in the first argument, and the
          * returned pointer should be infix pointers (which keep the base
          * pointer as well as a pointer to anywhere within the block).
          *)
       | PlusPointerOp (sub, pre, signed) ->
            TyPointer sub, TyPointer sub, TyRawInt (pre, signed)

(*
 * Get the type of an atom.
 *)
let rec type_of_atom_aux genv_lookup_var genv pos a =
   let pos = string_pos "type_of_atom" pos in
      match a with
         AtomNil ty ->
            ty
       | AtomInt _ ->
            TyInt
       | AtomEnum (n, _) ->
            TyEnum n
       | AtomFloat x ->
            TyFloat (Rawfloat.precision x)
       | AtomRawInt i ->
            TyRawInt (Rawint.precision i, Rawint.signed i)
       | AtomVar v ->
            genv_lookup_var genv pos v
       | AtomFun v ->
            genv_lookup_fun genv pos v
       | AtomLabel _
       | AtomSizeof _ ->
            TyRawInt (precision_subscript, signed_subscript)
       | AtomConst (ty, _, i) ->
            let tv, tl, _ = dest_union_type genv pos ty in
               TyUnion (tv, tl, IntSet.of_point i)
       | AtomTyApply (_, ty, _)
       | AtomTyPack (_, ty, _) ->
            ty
       | AtomTyUnpack v ->
            let ty = genv_lookup_var genv pos v in
               unpack_type genv pos v ty
       | AtomUnop (op, a) ->
            let ty, _ = type_of_unop pos op in
               ty
       | AtomBinop (op, _, _) ->
            let ty, _, _ = type_of_binop pos op in
               ty

let type_of_atom = type_of_atom_aux genv_lookup_var
let type_of_global_atom = type_of_atom_aux genv_lookup_var_or_global

(************************************************************************
 * PROGRAM TYPING
 ************************************************************************)

(*
 * Assume all binding occurrences have different names.
 * Get the type of every variable in the program.
 *)
let rec type_exp genv e =
   let pos = string_pos "type_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, _, e)
       | LetExt (v, ty, _, _, _, _, _, e)
       | LetSubscript (_, v, ty, _, _, e)
       | LetGlobal (_, v, ty, _, e) ->
            let genv = genv_add_var genv v ty in
               type_exp genv e
       | LetAlloc (v, op, e) ->
            let genv = genv_add_var genv v (type_of_alloc_op op) in
               type_exp genv e
       | TailCall _
       | SpecialCall _ ->
            genv
       | TypeCase (_, _, name, v, e1, e2) ->
            let genv = genv_add_var genv v (genv_lookup_name genv pos name) in
               type_exp genv e
       | Call (_, _, _, e)
       | Assert (_, _, e)
       | SetSubscript (_, _, _, _, _, _, e)
       | SetGlobal (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, _, e)
       | Debug (_, e) ->
            type_exp genv e
       | Match (_, cases) ->
            List.fold_left (fun genv (_, _, e) ->
                  type_exp genv e) genv cases
       | MatchDTuple (_, cases) ->
            List.fold_left (fun genv (_, _, e) ->
                  type_exp genv e) genv cases

(*
 * Type for a function.
 *)
let type_fun genv f (info, _, ty, vars, e) =
   let pos = string_pos "type_fun" (var_exp_pos f) in
   let ty_args, _ = dest_fun_type genv pos ty in
   let genv = List.fold_left2 genv_add_var genv vars ty_args in
      type_exp genv e

(*
 * Get all the var types in the program.
 *)
let prog_genv prog =
   let genv = genv_of_prog prog in
      SymbolTable.fold type_fun genv prog.prog_funs

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
