(*
 * Print the FIR program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Flags
open Debug
open Symbol
open Location
open Interval_set

open Fir
open Fir_ds
open Fir_set
open Fir_state

(*
 * Options to the FIR printer.
 *)
let prefix_print = "fir.print"
let name_omit_types = prefix_print ^ ".omit-types"

let () = std_flags_help_section_text prefix_print
   "Flags for controlling the FIR printer"

let () = std_flags_register_list_help prefix_print
   [name_omit_types, FlagBool false, "omit FIR types when FIR is printed"]

let omit_types () =
   std_flags_get_bool name_omit_types


(*
 * Default tabstop.
 *)
let tabstop = 3

let pp_print_option out print x =
   match x with
      None ->
         ()
    | Some x ->
         print out x

(*
 * We want to print symbols in a parsable format.
 *)
let pp_print_symbol = pp_print_ext_symbol

(*
 * Separated list of fields.
 *)
let pp_print_sep_list sep printer buf l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "%s@ " sep;
                 fprintf buf "@[<hv 3>%a@]" printer x;
                 false) true l);
   pp_close_box buf ()

 let pp_print_symbol_list sep buf l =
    pp_print_sep_list sep pp_print_symbol buf l

let pp_print_symbol_set sep buf l =
   pp_open_hvbox buf 0;
   ignore (SymbolSet.fold (fun first x ->
                 if not first then
                    fprintf buf "%s@ " sep;
                 fprintf buf "@[<hv 3>%a@]" pp_print_symbol x;
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_tab_sep_list tabstop sep printer buf l =
   pp_open_hvbox buf tabstop;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "%s@ " sep;
                 fprintf buf "@[<hv 3>%a@]" printer x;
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_pre_sep_list sep printer buf l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "@ %s" sep;
                 fprintf buf "@[<hv 3>%a@]" printer x;
                 false) true l);
   pp_close_box buf ()

let pp_print_int_nice buf i =
  if i == min_int then
    pp_print_string buf "$min"
  else if i == max_int then
    pp_print_string buf "$max"
  else
    pp_print_int buf i

(*
 * Print a string.
 *)
let n_code = Char.code '\n'
let t_code = Char.code '\t'
let v_code = Char.code '\011'
let b_code = Char.code '\b'
let r_code = Char.code '\r'
let f_code = Char.code '\012'
let a_code = Char.code '\007'
let slash_code = Char.code '\\'
let quote_code = Char.code '\''
let dquote_code = Char.code '\022'

let zero_code = Char.code '0'
let space_code = Char.code ' '
let del_code = 126

let octal_string c =
   let s = String.create 4 in
      s.[0] <- '\\';
      s.[1] <- Char.chr (((c lsr 6) land 7) + zero_code);
      s.[2] <- Char.chr (((c lsr 3) land 7) + zero_code);
      s.[3] <- Char.chr ((c land 7) + zero_code);
      s

let hex_char c =
   if c < 10 then
      Char.chr (c + zero_code)
   else
      Char.chr (c + a_code)

let hex_string c =
   let s = String.create 5 in
      s.[0] <- '\\';
      s.[1] <- 'x';
      s.[2] <- hex_char ((c lsr 8) land 15);
      s.[3] <- hex_char ((c lsr 4) land 15);
      s.[5] <- hex_char (c land 15);
      s

let pp_print_raw_char buf c =
   let s =
      if c = n_code then
         "\\n"
      else if c = t_code then
         "\\t"
      else if c = v_code then
         "\\v"
      else if c = b_code then
         "\\b"
      else if c = r_code then
         "\\r"
      else if c = f_code then
         "\\f"
      else if c = a_code then
         "\\a"
      else if c = slash_code then
         "\\"
      else if c = quote_code then
         "'"
      else if c = dquote_code then
         "\022"
      else if c < space_code then
         octal_string c
      else if c > del_code then
         hex_string c
      else
         String.make 1 (Char.chr c)
   in
      pp_print_string buf s

let pp_print_raw_string buf s =
   let len = Array.length s in
      pp_print_char buf '"';
      for i = 0 to pred len do
         pp_print_raw_char buf s.(i)
      done;
      pp_print_char buf '"'

(*
 * Print rawfloats/rawints.
 *)
let pp_print_int_precision buf p =
   let s =
      match p with
         Rawint.Int8  -> "int8"
       | Rawint.Int16 -> "int16"
       | Rawint.Int32 -> "int32"
       | Rawint.Int64 -> "int64"
   in
      pp_print_string buf s

let pp_print_int_signed buf s =
   if not s then
      pp_print_string buf "unsigned "

let string_of_rip p s =
   let s1 =
      if not s then
         "u"
      else
         ""
   in
   let s2 =
      match p with
         Rawint.Int8  -> "int8"
       | Rawint.Int16 -> "int16"
       | Rawint.Int32 -> "int32"
       | Rawint.Int64 -> "int64"
   in
      s1 ^ s2

let string_of_rfp p =
   match p with
      Rawfloat.Single -> "float32"
    | Rawfloat.Double -> "float64"
    | Rawfloat.LongDouble -> "float80"

let string_of_rfp_bits p =
   match p with
      Rawfloat.Single -> "32"
    | Rawfloat.Double -> "64"
    | Rawfloat.LongDouble -> "80"

let pp_print_rip p s buf =
   pp_print_string buf (string_of_rip p s)

let pp_print_rfp buf p =
   pp_print_string buf (string_of_rfp p)

(*
 * Print the sets.
 *)
let string_of_int_set set =
   if IntSet.is_singleton set then
      "{" ^ (string_of_int (IntSet.dest_singleton set)) ^ "}"
   else
      IntSet.fold (fun set left right ->
         let set =
            match left with
               Infinity ->
                  set ^ "($inf"
             | Open i ->
                  set ^ "(" ^ (string_of_int i)
             | Closed i ->
                  set ^ "[" ^ (string_of_int i)
         in
         let set = set ^ "," in
         let set =
            match right with
               Infinity ->
                  set ^ "$inf)"
             | Open i ->
                  set ^ (string_of_int i) ^ ")"
             | Closed i ->
                  set ^ (string_of_int i) ^ "]"
         in
            set) "" set

let pp_print_int_set buf set =
   pp_print_string buf (string_of_int_set set)

(*
 * Print the sets.
 *)
let string_of_rawint i =
   Printf.sprintf "%s(%s)"
      (string_of_rip (Rawint.precision i) (Rawint.signed i))
      (Rawint.to_string i)

let pp_print_raw_int buf i =
   pp_print_string buf (string_of_rawint i)

(* TEMP[phobos]: needs fixing *)
let pp_print_raw_int_set buf set =
(*   print_string (RawIntSet.name set);
   if not (RawIntSet.is_total set) then
      begin*)
         pp_print_string buf "|{";
         RawIntSet.iter (fun left right ->
               (match left with
                   Infinity ->
                      pp_print_string buf "($inf, "
                 | Open i ->
                      fprintf buf "(%a" pp_print_raw_int i
                 | Closed i ->
                      fprintf buf "[%a" pp_print_raw_int i);
               pp_print_string buf ",";
               (match right with
                   Infinity ->
                      pp_print_string buf "$inf)"
                 | Open i ->
                      fprintf buf "%a)" pp_print_raw_int i
                 | Closed i ->
                      fprintf buf "%a]" pp_print_raw_int i)) set;
         pp_print_string buf "}"
(*      end*)

(*
 * Print an arbitrary set.
 *)
let pp_print_set buf s =
   match s with
      IntSet set -> pp_print_int_set buf set
    | RawIntSet set -> pp_print_raw_int_set buf set

(*
 * Tuple classes.
 *)
let string_of_tuple_class = function
   NormalTuple -> "$normal"
 | RawTuple -> "$raw"
 | BoxTuple -> "$box"

(*
 * Use precedences to reduce the
 * amount of parentheses when printing types.
 *)
let prec_none   = 0
let prec_fun    = 1
let prec_union  = 2
let prec_prod   = 3
let prec_apply  = 4

(*
 * Block classes.
 *)
let string_of_sub_block = function
   BlockSub    -> "$block"
 | RawDataSub  -> "$rawdata"
 | TupleSub    -> "$tuple"
 | RawTupleSub -> "$rawtuple"

let pp_print_sub_block buf op =
   pp_print_string buf (string_of_sub_block op)

let pp_print_sub_index buf index_op =
   match index_op with
      ByteIndex ->
         pp_print_string buf "$byte_index"
    | WordIndex ->
         pp_print_string buf "$word_index"

let rec pp_print_sub_value buf value_op =
   match value_op with
      EnumSub n ->
         fprintf buf "$enum %d" n
    | PolySub ->
         pp_print_string buf "$poly"
    | IntSub ->
         pp_print_string buf "$int"
    | TagSub (ty_var, fields) ->
         fprintf buf "@[<hv 0>@[<hv 3>$tag[%a] {@ %a@]@ }@]"
            pp_print_symbol ty_var
            (pp_print_mutable_types "," prec_none) fields
    | RawIntSub (pre, signed) ->
         fprintf buf "$rawint %t" (pp_print_rip pre signed)
    | RawFloatSub pre ->
         fprintf buf "$rawfloat %a" pp_print_rfp pre
    | PointerInfixSub ->
         pp_print_string buf "$pointer_infix"
    | BlockPointerSub ->
         pp_print_string buf "$block_pointer"
    | RawPointerSub ->
         pp_print_string buf "$raw_pointer"
    | FunctionSub ->
         pp_print_string buf "$function"

and pp_print_subop buf subop =
   if omit_types () then
      ()
   else begin
      let { sub_block = block_op;
            sub_value = value_op;
            sub_index = index_op;
            sub_script = script
          } = subop
      in
         fprintf buf "{%s, %a, %a, " (string_of_sub_block block_op)
            pp_print_sub_value value_op
            pp_print_sub_index index_op;
         (match script with
             IntIndex ->
                pp_print_string buf "$int_index"
           | RawIntIndex (pre, signed) ->
                fprintf buf "$rawint_index %t" (pp_print_rip pre signed));
         pp_print_string buf "}"
   end

(*
 * Rename the vars in a type.
 *)
and pp_print_type pre buf ty =
   match ty with
      TyInt ->
         pp_print_string buf "int"
    | TyEnum 0 ->
         pp_print_string buf "void"
    | TyEnum 1 ->
         pp_print_string buf "unit"
    | TyEnum 2 ->
         pp_print_string buf "bool"
    | TyEnum 256 ->
         pp_print_string buf "enum_char"
    | TyEnum n ->
         fprintf buf "$enum %d" n
    | TyRawInt (p, s) ->
         pp_print_rip p s buf
    | TyFloat p ->
         pp_print_rfp buf p
    | TyFun (ty_vars, ty_res) ->
         if omit_types () then
            fprintf buf "$fun"
         else begin
            pp_open_hvbox buf tabstop;
            if pre > prec_fun then
               pp_print_string buf "(";
            (match ty_vars with
                [] ->
                  pp_print_string buf "$fun () ->"
              | [t] ->
                   fprintf buf "$fun (%a) ->" (pp_print_type prec_prod) t
              | tl ->
                   fprintf buf "$fun (@[<hv 0>%a@]) ->"
                      (pp_print_sep_list "," (pp_print_type prec_none)) ty_vars);
            pp_print_space buf ();
            pp_print_type prec_fun buf ty_res;
            if pre > prec_fun then
               pp_print_string buf ")";
            pp_close_box buf ()
         end
    | TyRawData ->
         pp_print_string buf "rawdata"
    | TyPointer op ->
         fprintf buf "$ty_pointer{%s}" (string_of_sub_block op)
    | TyFrame (v, tyl) ->
         if omit_types () then
            fprintf buf "$ty_frame[%a]" pp_print_symbol v
         else
            fprintf buf "@[<hv 3>$ty_frame{%a}[%a]@]"
               pp_print_symbol v
               (pp_print_sep_list "," (pp_print_type prec_none)) tyl
    | TyUnion (v, tyl, i) ->
         if omit_types () then
            fprintf buf "$ty_union"
         else
            fprintf buf "$ty_union{@[<hv 0>%a[%a], {%a}@]}"
               pp_print_symbol v
               (pp_print_sep_list "," (pp_print_type prec_apply)) tyl
               pp_print_int_set i
    | TyTuple (tclass, fields) ->
         if omit_types () then
            fprintf buf "$ty_tuple"
         else
            fprintf buf "@[<hv 0>@[<hv 3>$ty_tuple [%s] {@ %a@]@ }@]"
               (string_of_tuple_class tclass)
               (pp_print_mutable_types "," prec_none) fields
    | TyDTuple (ty_var, fields_opt) ->
         if omit_types () then
            fprintf buf "$ty_dtuple"
         else begin
            fprintf buf "@[<hv 0>@[<hv 3>$ty_dtuple[%a]" pp_print_symbol ty_var;
            (match fields_opt with
                Some fields ->
                   fprintf buf "{@ %a@]@ }@]" (pp_print_mutable_types "," prec_none) fields
              | None ->
                   fprintf buf "@]@]")
         end
    | TyTag (ty_var, fields) ->
         fprintf buf "@[<hv 0>@[<hv 3>$ty_tag[%a] {@ %a@]@ }@]"
            pp_print_symbol ty_var
            (pp_print_mutable_types "," prec_none) fields
    | TyArray ty ->
         if pre >= prec_apply then
            pp_print_string buf "(";
         fprintf buf "@[<hv 3>$ty_array{%a}@]" (pp_print_type prec_apply) ty;
         if pre >= prec_apply then
            print_string ")"
    | TyVar v ->
         fprintf buf "'%a" pp_print_symbol v
    | TyProject (v, i) ->
         fprintf buf "%a.%d" pp_print_symbol v i
    | TyApply (v, tyl) ->
         if omit_types () then
            fprintf buf "$ty_apply"
         else
            fprintf buf "@[<hv 3>$ty_apply{%a, [%a]}@]"
               pp_print_symbol v
               (pp_print_sep_list "," (pp_print_type prec_apply)) tyl
    | TyAll (ty_vars, ty) ->
         if omit_types () then
            fprintf buf "@[<hv 3>$all@ %a@]" (pp_print_type prec_none) ty
         else
            fprintf buf "@[<hv 3>($all [%a].@ %a)@]"
               (pp_print_sep_list "," (fun buf v ->
                      fprintf buf "'%a" pp_print_symbol v)) ty_vars
               (pp_print_type prec_none) ty
    | TyExists (ty_vars, ty) ->
         if omit_types () then
            fprintf buf "@[<hv 3>$exists@ %a@]" (pp_print_type prec_none) ty
         else
            fprintf buf "@[<hv 3>($exists [%a].@ %a)@]"
               (pp_print_sep_list "," (fun buf v ->
                      fprintf buf "'%a" pp_print_symbol v)) ty_vars
               (pp_print_type prec_none) ty
    | TyDelayed ->
         pp_print_string buf "$ty_delayed"
    | TyCase ty ->
         fprintf buf "$ty_case(%a)" (pp_print_type prec_none) ty
    | TyObject (v, ty) ->
         if omit_types () then
            fprintf buf "$ty_object"
         else
            fprintf buf "@[<hv 3>$ty_object(%a.@ %a)@]"
               pp_print_symbol v
               (pp_print_type prec_none) ty

and pp_print_mutable_flag buf b =
   match b with
      Mutable -> pp_print_string buf "$mutable"
    | Immutable -> pp_print_string buf "$immutable"
    | MutableDelayed -> pp_print_string buf "$mutable_delayed"
    | MutableVar v -> fprintf buf "$mutable_var[%a]" pp_print_symbol v

and pp_print_mutable_type pre buf (ty, b) =
   fprintf buf "%a %a"
      pp_print_mutable_flag b
      (pp_print_type pre) ty

and pp_print_mutable_types sep pre buf fields =
   pp_print_sep_list sep (pp_print_mutable_type pre) buf fields

let pp_print_type = pp_print_type prec_none

let pp_print_types buf tyl =
   pp_print_sep_list "," pp_print_type buf tyl

let pp_print_type_spec buf ty =
   fprintf buf " : %a" pp_print_type ty

let pp_print_break_type_spec buf ty =
   fprintf buf " :@ %a" pp_print_type ty

let pp_print_type_spec buf ty =
   if omit_types () then
      ()
   else
      pp_print_type_spec buf ty

let pp_print_break_type_spec buf ty =
   if omit_types () then
      ()
   else
      pp_print_break_type_spec buf ty


let pp_print_tydef buf tyd =
   let pp_print_lambda buf tv =
      fprintf buf "[%a]@ "
         (pp_print_sep_list "," (fun buf v ->
                fprintf buf "'%a" pp_print_symbol v)) tv
   in
      match tyd with
         TyDefUnion (tvl, []) ->
            fprintf buf "$void_union %a" pp_print_lambda tvl
       | TyDefUnion (tvl, tyll) ->
            fprintf buf "@[<hv 3>$union %a.(%a)@]"
               pp_print_lambda tvl
               (pp_print_sep_list " +" (fun buf l ->
                      match l with
                         [] -> pp_print_string buf "()"
                       | tyl -> pp_print_tab_sep_list tabstop " *" (pp_print_mutable_type prec_union) buf tyl)) tyll
       | TyDefLambda (tvl, ty) ->
            fprintf buf "@[<hv 3>$lambda %a.@ %a@]"
               pp_print_lambda tvl
               pp_print_type ty
       | TyDefDTuple v ->
            fprintf buf "$dtuple[%a]" pp_print_symbol v

(*
 * Print a label.
 *)
let pp_print_label buf (frame, field, subfield) =
   fprintf buf "$label[%a,%a,%a]"
      pp_print_symbol frame
      pp_print_symbol field
      pp_print_symbol subfield

(*
 * Global flag.
 *)
let string_of_unop op =
   match op with
      NotEnumOp n -> Printf.sprintf "$not[%d]" n

    | UMinusIntOp -> "-"
    | UMinusRawIntOp (p, s) ->
         Printf.sprintf "-|{%s}" (string_of_rip p s)
    | UMinusFloatOp p ->
         Printf.sprintf "-.{%s}" (string_of_rfp p)
    | AbsIntOp ->
         "$abs"
    | AbsFloatOp p ->
         Printf.sprintf "$abs%s" (string_of_rfp_bits p)
    | SinFloatOp p ->
         Printf.sprintf "$sin%s" (string_of_rfp_bits p)
    | CosFloatOp p ->
         Printf.sprintf "$cos%s" (string_of_rfp_bits p)
    | TanFloatOp p ->
         Printf.sprintf "$tan%s" (string_of_rfp_bits p)
    | ASinFloatOp p ->
         Printf.sprintf "$asin%s" (string_of_rfp_bits p)
    | ACosFloatOp p ->
         Printf.sprintf "$acos%s" (string_of_rfp_bits p)
    | ATanFloatOp p ->
         Printf.sprintf "$atan%s" (string_of_rfp_bits p)
    | SinHFloatOp p ->
         Printf.sprintf "$sinh%s" (string_of_rfp_bits p)
    | CosHFloatOp p ->
         Printf.sprintf "$cosh%s" (string_of_rfp_bits p)
    | TanHFloatOp p ->
         Printf.sprintf "$tanh%s" (string_of_rfp_bits p)
    | SqrtFloatOp p ->
         Printf.sprintf "$sqrt%s" (string_of_rfp_bits p)
    | ExpFloatOp p ->
         Printf.sprintf "$exp%s" (string_of_rfp_bits p)
    | LogFloatOp p ->
         Printf.sprintf "$log%s" (string_of_rfp_bits p)
    | Log10FloatOp p ->
         Printf.sprintf "$log10%s" (string_of_rfp_bits p)
    | CeilFloatOp p ->
         Printf.sprintf "$ceil%s" (string_of_rfp_bits p)
    | FloorFloatOp p ->
         Printf.sprintf "$floor%s" (string_of_rfp_bits p)
    | NotIntOp -> "~"
    | NotRawIntOp (p, s) ->
         Printf.sprintf "~|{%s}" (string_of_rip p s)
    | IntOfFloatOp p ->
         Printf.sprintf "$coerce{$int, %s}" (string_of_rfp p)
    | RawIntOfIntOp (p, s) ->
         Printf.sprintf "$coerce{%s, $int}" (string_of_rip p s)
    | IntOfRawIntOp (p, s) ->
         Printf.sprintf "$coerce{$int, %s}" (string_of_rip p s)
    | RawIntOfEnumOp (p, s, i) ->
         Printf.sprintf "$coerce{%s, %d}" (string_of_rip p s) i
    | RawIntOfFloatOp (p, s, fp) ->
         Printf.sprintf "$coerce{%s, %s}" (string_of_rip p s) (string_of_rfp fp)
    | RawIntOfPointerOp (p, s) ->
         Printf.sprintf "$coerce{%s, $pointer}" (string_of_rip p s)
    | FloatOfIntOp p ->
         Printf.sprintf "$coerce{%s, $int}" (string_of_rfp p)
    | FloatOfRawIntOp (fp, p, s) ->
         Printf.sprintf "$coerce{%s, %s}" (string_of_rfp fp) (string_of_rip p s)
    | FloatOfFloatOp (p1, p2) ->
         Printf.sprintf "$coerce{%s, %s}" (string_of_rfp p1) (string_of_rfp p2)
    | PointerOfRawIntOp (p, s) ->
         Printf.sprintf "$coerce{$pointer, %s}" (string_of_rip p s)
    | PointerOfBlockOp sb ->
         Printf.sprintf "$pointer{%s}" (string_of_sub_block sb)
    | RawBitFieldOp (p, s, off, len) ->
         sprintf "$field{%s}(off:%d; len:%d)" (string_of_rip p s) off len
    | RawIntOfRawIntOp (p1, s1, p2, s2) ->
         Printf.sprintf "$coerce{%s, %s}" (string_of_rip p1 s1) (string_of_rip p2 s2)
    | LengthOfBlockOp (subop, ty) ->
         let _ =
            fprintf str_formatter "$lengthofblockop{%a, %a}"
               pp_print_subop subop
               pp_print_type ty
         in
            flush_str_formatter ()
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         fprintf str_formatter "@[<hv 0>@[<hv 3>$dtuple_of_dtuple[%a] {@ %a@]@ }@]"
            pp_print_symbol ty_var
            (pp_print_mutable_types "," prec_none) ty_fields;
         flush_str_formatter ()
    | UnionOfUnionOp (ty_var, tyl, set1, set2) ->
         fprintf str_formatter "$union_of_union{@[<hv 0>%a[%a], {%a}<-{%a}@]}"
            pp_print_symbol ty_var
            (pp_print_sep_list "," pp_print_type) tyl
            pp_print_int_set set1
            pp_print_int_set set2;
         flush_str_formatter ()
    | RawDataOfFrameOp (ty_var, tyl) ->
         if omit_types () then
            fprintf str_formatter "$raw_data_of_frame[%a]" pp_print_symbol ty_var
         else
            fprintf str_formatter "@[<hv 0>@[<hv 3>$raw_data_of_frame[%a] {@ %a@]@ }@]"
               pp_print_symbol ty_var
               pp_print_types tyl;
         flush_str_formatter ()

let string_of_eqeq_op s ty =
   fprintf str_formatter "%s[%a]" s pp_print_type ty;
   flush_str_formatter ()

let string_of_binop op =
   match op with
      PlusIntOp -> "+"
    | PlusRawIntOp (p, s) ->
         Printf.sprintf "+|{%s}" (string_of_rip p s)
    | PlusFloatOp p ->
         Printf.sprintf "+.{%s}" (string_of_rfp p)
    | PlusPointerOp (sub_block, p, s) ->
         Printf.sprintf "+*{%s,%s}" (string_of_sub_block sub_block) (string_of_rip p s)
    | MinusIntOp -> "-"
    | MinusRawIntOp (p, s) ->
         Printf.sprintf "-|{%s}" (string_of_rip p s)
    | MinusFloatOp p ->
         Printf.sprintf "-.{%s}" (string_of_rfp p)
    | MulIntOp -> "*"
    | MulRawIntOp (p, s) ->
         Printf.sprintf "*|{%s}" (string_of_rip p s)
    | MulFloatOp p ->
         Printf.sprintf "*.{%s}" (string_of_rfp p)
    | DivIntOp -> "/"
    | DivRawIntOp (p, s) ->
         Printf.sprintf "/|{%s}" (string_of_rip p s)
    | DivFloatOp p ->
         Printf.sprintf "/.{%s}" (string_of_rfp p)
    | RemIntOp -> "%"
    | RemRawIntOp (p, s) ->
         Printf.sprintf "%%|{%s}" (string_of_rip p s)
    | RemFloatOp p ->
         Printf.sprintf "%%.{%s}" (string_of_rfp p)
    | LslIntOp -> "<<"
    | SlRawIntOp (p, s) ->
         Printf.sprintf "<<|{%s}" (string_of_rip p s)
    | LsrIntOp -> ">>"
    | AsrIntOp -> ">>>"
    | SrRawIntOp (p, s) ->
         Printf.sprintf ">>|{%s}" (string_of_rip p s)
    | AndEnumOp i ->
         Printf.sprintf "&#{%d}" i
    | AndIntOp -> "&"
    | AndRawIntOp (p, s) ->
         Printf.sprintf "&|{%s}" (string_of_rip p s)
    | OrEnumOp i ->
         Printf.sprintf "|#{%d}" i
    | OrIntOp -> "|"
    | OrRawIntOp (p, s) ->
         Printf.sprintf "||{%s}" (string_of_rip p s)
    | XorIntOp -> "^"
    | XorRawIntOp (p, s) ->
         Printf.sprintf "^|{%s}" (string_of_rip p s)
    | XorEnumOp i ->
         Printf.sprintf "^#{%d}" i
    | EqIntOp -> "="
    | EqRawIntOp (p, s) ->
         Printf.sprintf "=|{%s}" (string_of_rip p s)
    | EqFloatOp p ->
         Printf.sprintf "=.{%s}" (string_of_rfp p)
    | NeqIntOp -> "!="
    | NeqRawIntOp (p, s) ->
         Printf.sprintf "!=|{%s}" (string_of_rip p s)
    | NeqFloatOp p ->
         Printf.sprintf "!=.{%s}" (string_of_rfp p)
    | NeqEqOp ty -> string_of_eqeq_op "!=*" ty
    | EqEqOp ty -> string_of_eqeq_op "==*" ty
    | LtIntOp -> "<"
    | LtRawIntOp (p, s) ->
         Printf.sprintf "<|{%s}" (string_of_rip p s)
    | LtFloatOp p ->
         Printf.sprintf "<.{%s}" (string_of_rfp p)
    | LeIntOp -> "<="
    | LeRawIntOp (p, s) ->
         Printf.sprintf "<=|{%s}" (string_of_rip p s)
    | LeFloatOp p ->
         Printf.sprintf "<=.{%s}" (string_of_rfp p)
    | GtIntOp -> ">"
    | GtRawIntOp (p, s) ->
         Printf.sprintf ">|{%s}" (string_of_rip p s)
    | GtFloatOp p ->
         Printf.sprintf ">.{%s}" (string_of_rfp p)
    | GeIntOp -> ">="
    | GeFloatOp p ->
         Printf.sprintf ">=.{%s}" (string_of_rfp p)
    | GeRawIntOp (p, s) ->
         Printf.sprintf ">=|{%s}" (string_of_rip p s)
    | CmpIntOp -> "<=>"
    | CmpRawIntOp (p, s) ->
         Printf.sprintf "<=>|{%s}" (string_of_rip p s)
    | CmpFloatOp p ->
         Printf.sprintf "<=>.{%s}" (string_of_rfp p)
    | ATan2FloatOp p ->
         Printf.sprintf "$atan2_%s" (string_of_rfp_bits p)
    | PowerFloatOp p ->
         Printf.sprintf "$power%s" (string_of_rfp_bits p)
    | LdExpFloatIntOp p ->
         Printf.sprintf "$ldexp%s" (string_of_rfp_bits p)
    | MinIntOp -> "$min"
    | MinRawIntOp (p, s) ->
         Printf.sprintf "$min|{%s}" (string_of_rip p s)
    | MinFloatOp p ->
         Printf.sprintf "$min%s" (string_of_rfp_bits p)
    | MaxIntOp -> "$max"
    | MaxRawIntOp (p, s) ->
         Printf.sprintf "$max|{%s}" (string_of_rip p s)
    | MaxFloatOp p ->
         Printf.sprintf "$max%s" (string_of_rfp_bits p)
    | RawSetBitFieldOp (p, s, off, len) ->
         sprintf "$field{%s}(off:%d; len:%d)" (string_of_rip p s) off len

(*
 * Print the atom.
 *)
let rec pp_print_atom buf a =
   match a with
      AtomNil ty ->
         fprintf buf "$nil{%a}" pp_print_type ty
    | AtomInt i ->
         pp_print_int buf i
    | AtomRawInt i ->
         pp_print_raw_int buf i
    | AtomFloat x ->
         fprintf buf "%s(%s)"
            (string_of_rfp (Rawfloat.precision x))
            (Rawfloat.to_string x)
    | AtomVar v ->
         pp_print_symbol buf v
    | AtomFun v ->
         fprintf buf "($funvar %a)" pp_print_symbol v
    | AtomEnum (1, 0) ->
         pp_print_string buf "$unit"
    | AtomEnum (2, 0) ->
         pp_print_string buf "$false"
    | AtomEnum (2, 1) ->
         pp_print_string buf "$true"
    | AtomEnum (256, i) ->
         let s = " " in
           s.[0] <- char_of_int i;
           pp_print_string buf (String.escaped ("'" ^ s ^ "'"))
    | AtomEnum (n, i) ->
         fprintf buf "$enum[%d < %d]" i n
    | AtomConst (ty, tv, i) ->
         fprintf buf "$const[%d]{%a, %a}" i
            pp_print_type ty
            pp_print_symbol tv
    | AtomLabel (label, off) ->
         pp_print_label buf label;
         if not (Rawint.is_zero off) then
            fprintf buf "++%s" (string_of_rawint off)
    | AtomSizeof (vars, i) ->
         fprintf buf "@[<hv 3>$sizeof(";
         List.iter (fun v ->
               fprintf buf "%a,@ " pp_print_symbol v) vars;
         fprintf buf "; %s)@]" (string_of_rawint i)

    | AtomTyApply (a, ty, tyl) ->
         if debug debug_print_no_poly then
            pp_print_atom buf a
         else
            fprintf buf "@[<hv 3>$apply(%a,@ %a,@ [%a])@]"
               pp_print_atom a
               pp_print_type ty
               (pp_print_sep_list "," pp_print_type) tyl
    | AtomTyPack (v, t, tyl) ->
         if debug debug_print_no_poly then
            pp_print_symbol buf v
         else
            fprintf buf "@[<hv 3>$pack(%a :@ %a[%a])@]"
               pp_print_symbol v
               pp_print_type t
               (pp_print_sep_list "," pp_print_type) tyl
    | AtomTyUnpack v ->
         if debug debug_print_no_poly then
            pp_print_symbol buf v
         else
            fprintf buf "$unpack(%a)" pp_print_symbol v
    | AtomUnop (op, a) ->
         pp_print_unop buf op a
    | AtomBinop (op, a1, a2) ->
         pp_print_binop buf op a1 a2

and pp_print_unop buf op a =
   fprintf buf "(%s %a)" (string_of_unop op) pp_print_atom a

and pp_print_binop buf op a1 a2 =
   fprintf buf "(%a %s %a)"
      pp_print_atom a1
      (string_of_binop op)
      pp_print_atom a2

let pp_print_atom_opt buf a =
   match a with
      Some a ->
         pp_print_atom buf a
    | None ->
         pp_print_string buf "<none>"

let pp_print_tag buf a =
   match a with
      Some a ->
         pp_print_atom buf a
    | None ->
         pp_print_string buf "_"

let pp_print_atoms buf atoms =
   pp_print_sep_list "," pp_print_atom buf atoms

let pp_print_atom_brack left a right buf =
   match a with
      Some a ->
         fprintf buf "%s%a%s" left pp_print_atom a right
    | None ->
         ()

(*
 * Print operators.
 *)
let pp_print_ext buf s ty ty_args args =
   fprintf buf "($ext[`\"%s\" : %a][%a]{ %a })"
      (String.escaped s)
      pp_print_type_spec ty
      (pp_print_sep_list "," pp_print_type) ty_args
      (pp_print_sep_list "," pp_print_atom) args

let string_of_atom_list al =
  let s = String.make (List.length al) '\000' in
  let rec fill n al =
    match al with
        [] -> ()
      | AtomEnum (256, c) :: al ->
          s.[n] <- char_of_int c;
          fill (n+1) al
      | _ -> raise Exit in
  fill 0 al;
  s

(*
 * Debugging.
 *)
let pp_print_debug_line = pp_print_location

let pp_print_debug_vars buf vars =
   fprintf buf " Debug vars: %a"
      (pp_print_sep_list "," (fun buf (v1, ty, v2) ->
             fprintf buf "%a=%a : %a"
                pp_print_symbol v1
                pp_print_symbol v2
                pp_print_type ty)) vars

let pp_print_debug buf info =
   match info with
      DebugString s ->
         fprintf buf "# Comment: \"%s\"" s
    | DebugContext (line, vars) ->
         fprintf buf "%a@ %a"
            pp_print_debug_line line
            pp_print_debug_vars vars

(*
 * Subscript operations
 *)
let pp_print_subscript_op subop v a buf =
   fprintf buf "%a%t%a"
      pp_print_atom v
      (pp_print_atom_brack ".(" a ")")
      pp_print_subop subop

let pp_print_subscript op v a buf =
   pp_print_subscript_op op v (Some a) buf

(*
 * Allocation.
 *)
let pp_print_poly_alloc_op pp_print_atom buf op =
   match op with
      AllocTuple (tclass, ty_vars, ty, al) ->
         fprintf buf "@[<hv 3>$alloc_tuple[%s][%a](%a)@ : %a@]"
            (string_of_tuple_class tclass)
            (pp_print_sep_list "," (fun buf v -> fprintf buf "'%a" pp_print_symbol v)) ty_vars
            (pp_print_sep_list "," pp_print_atom) al
            pp_print_type ty
    | AllocDTuple (ty, ty_var, a, al) ->
         fprintf buf "@[<hv 3>$alloc_dtuple[%a.%a](%a)@ : %a@]"
            pp_print_symbol ty_var
            pp_print_atom a
            (pp_print_sep_list "," pp_print_atom) al
            pp_print_type ty
    | AllocUnion (ty_vars, ty, tv, i, args) ->
         fprintf buf "@[<hv 3>$alloc_union[%a](tu:%a=%a,@ tag:%d,@ args={%a})@]"
            (pp_print_sep_list "," (fun buf v -> fprintf buf "'%a" pp_print_symbol v)) ty_vars
            pp_print_type ty
            pp_print_symbol tv
            i
            (pp_print_sep_list "," pp_print_atom) args
    | AllocArray (ty, al) ->
         fprintf buf "$alloc_array(%a : %a)"
            (pp_print_sep_list "," pp_print_atom) al
            pp_print_type ty
    | AllocVArray (ty, sub_index, a1, a2) ->
         fprintf buf "$alloc_varray(%a : %a, %a : %a)"
            pp_print_atom a1
            pp_print_sub_index sub_index
            pp_print_atom a2
            pp_print_type ty
    | AllocMalloc (ty, a) ->
         fprintf buf "$malloc(%a: %a)"
            pp_print_type ty
            pp_print_atom a
    | AllocFrame (v, tyl) ->
         fprintf buf "@[<hv 3>$alloc_frame(%a)[%a]@]"
            pp_print_symbol v
            (pp_print_sep_list "," pp_print_type) tyl

let pp_print_alloc_op = pp_print_poly_alloc_op pp_print_atom

(*
 * Print a predicate.
 *)
let pp_print_poly_pred pp_print_atom buf pred =
   match pred with
      IsMutable v ->
         fprintf buf "is_mutable(%a)" pp_print_atom v
    | Reserve (bytes, pointers) ->
         fprintf buf "reserve(bytes=%a, pointers=%a)"
            pp_print_atom bytes
            pp_print_atom pointers
    | BoundsCheck (subop, v, a1, a2) ->
         fprintf buf "@[<hv 0>@[<hv 3>BoundsCheck {@ subop = %a;@ array = %a;@ lower = %a;@ upper = %a;@]@ }@]"
            pp_print_subop subop
            pp_print_atom v
            pp_print_atom a1
            pp_print_atom a2
    | ElementCheck (ty, subop, v, a) ->
         fprintf buf "@[<hv 0>@[<hv 3>ElementCheck{@ @[<hv 3>type =@ %a@];@ subop = %a;@ array = %a;@ subscript = %a;@]@ }@]"
            pp_print_type ty
            pp_print_subop subop
            pp_print_atom v
            pp_print_atom a

let pp_print_pred = pp_print_poly_pred pp_print_atom

(*
 * "Let" opener.
 *)
let pp_print_let_open buf v ty =
   fprintf buf "@[<hv 3>let %a%a =@ "
      pp_print_symbol v
      pp_print_type_spec ty

let pp_print_let_var_open buf v =
   fprintf buf "@[<hv 3>let %a =@ "
      pp_print_symbol v

let rec pp_print_let_close limit buf e =
   fprintf buf " in@]@ ";
   pp_print_expr limit buf e

(*
 * Rename all the vars in the expr.
 *)
and pp_print_expr_now limit buf e =
   match dest_exp_core e with
      LetAtom (v, ty, a, e) ->
         pp_print_let_open buf v ty;
         pp_print_atom buf a;
         pp_print_let_close limit buf e

    | LetExt (v, ty, s, b, ty2, ty_args, args, e) ->
         pp_print_let_open buf v ty;
         pp_print_ext buf s ty2 ty_args args;
         pp_print_let_close limit buf e

    | Call (label, f, args, e) ->
         fprintf buf "$call[%a, %a]{%a} in@ "
            pp_print_symbol label
            pp_print_atom f
            (pp_print_sep_list "," pp_print_atom_opt) args;
         pp_print_expr limit buf e

    | TailCall (label, f, args) ->
         fprintf buf "%a: %a(%a)"
            pp_print_symbol label
            pp_print_atom f
            (pp_print_sep_list "," pp_print_atom) args

    | SpecialCall (label, TailSysMigrate (id, loc_ptr, loc_off, f, args)) ->
         fprintf buf "%a: $sysmigrate(%d, %a, %a, %a)"
            pp_print_symbol label
            id
            (pp_print_sep_list "," pp_print_atom) [loc_ptr; loc_off]
            pp_print_atom f
            (pp_print_sep_list "," pp_print_atom) args

    | SpecialCall (label, TailAtomic (f, c, args)) ->
         fprintf buf "%a: $atomic(%a, %a)"
            pp_print_symbol label
            pp_print_atom f
            (pp_print_sep_list "," pp_print_atom) (c :: args)

    | SpecialCall (label, TailAtomicRollback (level, c)) ->
         fprintf buf "%a: $atomicrollback(%a)"
            pp_print_symbol label
            (pp_print_sep_list "," pp_print_atom) [level; c]

    | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
         fprintf buf "%a: $atomiccommit(%a, %a, %a)"
            pp_print_symbol label
            pp_print_atom level
            pp_print_atom f
            (pp_print_sep_list "," pp_print_atom) args

    | Match (a, cases) ->
         fprintf buf "@[<hv 3>($match %a $with" pp_print_atom a;
         List.iter (fun (label, set, e) ->
               fprintf buf "@ @[<hv 3>| %a: %a ->@ %a@]"
                  pp_print_symbol label
                  pp_print_set set
                  (pp_print_expr limit) e) cases;
         fprintf buf ")@]"

    | MatchDTuple (a, cases) ->
         fprintf buf "@[<hv 3>($match_dtuple %a $with" pp_print_atom a;
         List.iter (fun (label, a_opt, e) ->
               fprintf buf "@ @[<hv 3>| %a: %a ->@ %a@]"
                  pp_print_symbol label
                  pp_print_tag a_opt
                  (pp_print_expr limit) e) cases;
         fprintf buf ")@]"

    | TypeCase (a1, a2, name, v, e1, e2) ->
         fprintf buf "@[<hv 3>($typecase(%a, %a)@ @[<hv 3>%a %a ->@ %a@]@ @[<hv 3>| _ ->@ %a)@]@]"
            pp_print_atom a1
            pp_print_atom a2
            pp_print_symbol name
            pp_print_symbol v
            (pp_print_expr limit) e1
            (pp_print_expr limit) e2

    | LetAlloc (v, op, e) ->
         pp_print_let_var_open buf v;
         fprintf buf "@[<hv 3>%a@]" pp_print_alloc_op op;
         pp_print_let_close limit buf e

    | LetSubscript (op, v1, ty, v2, a, e) ->
         pp_print_let_open buf v1 ty;
         pp_print_subscript op v2 a buf;
         pp_print_let_close limit buf e

    | SetSubscript (op, label, v, a1, ty, a2, e) ->
         fprintf buf "@[<hv 3>%a:@ %t%a@ <- %a in @]@ "
            pp_print_symbol label
            (pp_print_subscript op v a1)
            pp_print_break_type_spec ty
            pp_print_atom a2;
         pp_print_expr limit buf e

    | LetGlobal (op, v, ty, l, e) ->
         pp_print_let_open buf v ty;
         fprintf buf "$global[%a] %a" pp_print_sub_value op pp_print_symbol l;
         pp_print_let_close limit buf e

    | SetGlobal (op, label, v, ty, a, e) ->
         fprintf buf "@[<hv 3>%a: %a{%a} : %a <-@ %a;@]@ "
            pp_print_symbol label
            pp_print_symbol v
            pp_print_sub_value op
            pp_print_type ty
            pp_print_atom a;
         pp_print_expr limit buf e

    | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
         fprintf buf "@[<hv 3>%a:@ $memcpy(@[<hov 0>%t,@ %t,@ %a@]) in@]@ "
            pp_print_symbol label
            (pp_print_subscript op v1 a1)
            (pp_print_subscript op v2 a2)
            pp_print_atom a3;
         pp_print_expr limit buf e

    | Assert (label, pred, e) ->
         fprintf buf "@[<hv 3>$assert %a: (%a) in@]@ "
            pp_print_symbol label
            pp_print_pred pred;
         pp_print_expr limit buf e

    | Debug (info, e) ->
         fprintf buf "%a@ " pp_print_debug info;
         pp_print_expr limit buf e

and pp_print_expr limit buf e =
   match limit with
      None ->
         pp_print_expr_now None buf e
    | Some i ->
         if i <= 0 then
            fprintf buf "...@ "
         else
            pp_print_expr_now (Some (i - 1)) buf e

and pp_print_function buf f (line, ty_vars, f_ty, vars, body) =
   fprintf buf "%a@ @[<hv 3>(%a%a)@ [%a]@ @[<hv 1>(%a)@] =@ %a@]"
      pp_print_debug_line line
      pp_print_symbol f
      pp_print_type_spec f_ty
      (pp_print_sep_list "," (fun buf v ->
             fprintf buf "'%a" pp_print_symbol v)) ty_vars
      (pp_print_sep_list "," pp_print_symbol) vars
      (pp_print_expr None) body

let pp_print_expr_size i = pp_print_expr (Some i)
let pp_print_expr = pp_print_expr None

(*
 * Print an arg list.
 *)
let pp_print_import_arg buf arg =
   match arg with
      ArgFunction ->
         pp_print_string buf "function"
    | ArgPointer ->
         pp_print_string buf "pointer"
    | ArgRawInt (pre, signed) ->
         pp_print_rip pre signed buf
    | ArgRawFloat pre ->
         pp_print_rfp buf pre

let pp_print_import_args buf args =
   fprintf buf "{%a}"
      (pp_print_sep_list "," pp_print_import_arg) args

let pp_print_import_info buf info =
   match info with
      ImportGlobal ->
         pp_print_string buf "<global>"
    | ImportFun (b, args) ->
         fprintf buf "%s: %a"
            (if b then "stdargs" else "args")
            pp_print_import_args args

(*
 * Print a name record.
 *)
let pp_print_names buf names =
   fprintf buf "@[<hv 3>names {";
   List.iter (fun (v, v_opt) ->
         fprintf buf "@ %a" pp_print_symbol v;
         (match v_opt with
             Some v ->
                fprintf buf " = %a" pp_print_symbol v
           | None ->
                ());
         pp_print_string buf ";") names;
   fprintf buf " }@]"

(*
 * Print file class.
 *)
let pp_print_file_class buf info =
   let s =
      match info with
         FileFC ->
            "FileFC"
       | FileNaml ->
            "FileNaml"
       | FileJava ->
            "FileJava"
       | FileAml ->
            "FileAml"
   in
      pp_print_string buf s

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

let pp_print_prog buf
    { prog_file = { file_dir = dir; file_name = file; file_class = fclass };
      prog_import = import;
      prog_export = export;
      prog_types = types;
      prog_names = names;
      prog_globals = globals;
      prog_frames = frames;
      prog_funs = funs
    } =

   fprintf buf "@[<v 0>@[<v 3>$Program info:@ \"%s\", \"%s\", %a@]"
      dir file pp_print_file_class fclass;

   fprintf buf "@ @[<v 3>$Program imports:";
   SymbolTable.iter (fun v { import_name = s;
                             import_type = ty;
                             import_info = info
                         } ->
         fprintf buf "@ @[<hv 3>%a = \"%s\" :@ %a@ %a@]"
            pp_print_symbol v
            (String.escaped s)
            pp_print_type ty
            pp_print_import_info info) import;
   pp_close_box buf ();

   fprintf buf "@ @[<v 3>$Program exports:";
   SymbolTable.iter (fun v { export_name = s;
                             export_type = ty
                         } ->
         fprintf buf "@ @[<hv 3>%a = \"%s\" : %a@]"
            pp_print_symbol v
            (String.escaped s)
            pp_print_type ty) export;
   pp_close_box buf ();

   fprintf buf "@ @[<v 3>$Program types:";
   SymbolTable.iter (fun v tyd ->
         fprintf buf "@ @[<hv 3>%a =@ %a@]"
            pp_print_symbol v
            pp_print_tydef tyd) types;
   pp_close_box buf ();

   fprintf buf "@ @[<hv 3>$Program frames:";
   SymbolTable.iter (fun frame (vars, fields) ->
         fprintf buf "@ @[<hv 0>@[<hv 3>%a [%a] {"
            pp_print_symbol frame
            (pp_print_sep_list "," (fun buf v ->
                   fprintf buf "'%a" pp_print_symbol v)) vars;
         SymbolTable.iter (fun field subfields ->
               fprintf buf "@ @[<hv 0>@[<hv 3>%a {" pp_print_symbol field;
               List.iter (fun (v, ty, i) ->
                     fprintf buf "@ @[<hv 3>%a[%d] :@ %a;@]"
                        pp_print_symbol v
                        i
                        pp_print_type ty) subfields;
               fprintf buf "@]@ }@]") fields;
         fprintf buf "@]@ }@]") frames;
   fprintf buf "@]";

   fprintf buf "@ @[<v 3>$Program names:";
   SymbolTable.iter (fun v ty ->
         fprintf buf "@ @[<hv 3>%a =@ %a@]"
            pp_print_symbol v
            pp_print_type ty) names;
   pp_close_box buf ();

   fprintf buf "@ @[<v 3>$Program globals:";
   SymbolTable.iter (fun v (ty, init) ->
         fprintf buf "@ @[<hv 3>%a%a =@ "
            pp_print_symbol v
            pp_print_type_spec ty;
         (match init with
             InitAtom a ->
                fprintf buf "$atom(%a)" pp_print_atom a
          | InitAlloc op ->
               pp_print_alloc_op buf op
          | InitRawData (pre, s) ->
               fprintf buf "{%a} %a"
                  pp_print_int_precision pre
                  pp_print_raw_string s
          | InitNames (v, names) ->
               fprintf buf "%a, %a"
                  pp_print_symbol v
                  pp_print_names names
          | InitTag (ty_var, fields) ->
               fprintf buf "@[<hv 0>@[<hv 3>$tag[%a] {@ %a@]@ }@]"
                  pp_print_symbol ty_var
                  (pp_print_mutable_types "," prec_none) fields);
         pp_close_box buf ()) globals;
   pp_close_box buf ();

   (* Sort the functions by line number *)
   let funs = SymbolTable.fold (fun funs f fund -> (f, fund) :: funs) [] funs in
   let funs =
      List.sort (fun (_, (loc1, _, _, _, _)) (_, (loc2, _, _, _, _)) ->
            Pervasives.compare loc1 loc2) funs
   in
      fprintf buf "@ @[<v 3>$Program funs:";
      List.iter (fun (f, fund) ->
            fprintf buf "@ @[<hv 3>%a@]" (fun buf -> pp_print_function buf f) fund) funs;
      fprintf buf "@]@]"

let pp_print_mprog buf
    { marshal_fir       = mfir;
      marshal_globals   = mglobals;
      marshal_funs      = mfuns;
      marshal_rttd      = mrttd;
    } =
   fprintf buf "@[<v 0>@[<v 3>$Marshal globals:";
   List.iter (fun v ->
         fprintf buf "@ %a" pp_print_symbol v) mglobals;

   fprintf buf "@]@ @[<v 3>$Marshal funs:";
   List.iter (fun v ->
         fprintf buf "@ %a" pp_print_symbol v) mfuns;

   fprintf buf "@]@ @[<v 3>$Marshal RTTD:@ /* unable to print this information */@]";

   fprintf buf "@ @[<v 3>$Marshal FIR:@ %a@]@]" pp_print_prog mfir

let debug_prog debug prog =
   eprintf "@[<v 0>*** FIR: %s@ %a@]@." debug pp_print_prog prog

let debug_mprog debug mprog =
   eprintf "@[<v 0>*** FIR: %s@ %a@]@." debug pp_print_mprog mprog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
