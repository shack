(*
 * Print FIR code.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open Fir
open Fir_set

val tabstop : int

(*
 * Print helpers.
 *)
val pp_print_sep_list : string -> (formatter -> 'a -> unit) -> formatter -> 'a list -> unit

val pp_print_symbol_list : string -> formatter -> var list -> unit
val pp_print_symbol_set : string -> formatter -> SymbolSet.t -> unit

(*
 * Set printing.
 *)
val pp_print_set           : formatter -> set -> unit
val pp_print_int_set       : formatter -> IntSet.t -> unit
val pp_print_raw_int_set   : formatter -> RawIntSet.t -> unit
val pp_print_int_precision : formatter -> int_precision -> unit
val pp_print_int_signed    : formatter -> int_signed -> unit

(*
 * Expr and type printing.
 *)
val string_of_unop : unop -> string
val string_of_binop : binop -> string
val string_of_int_set : IntSet.t -> string

val pp_print_mutable_flag  : formatter -> mutable_flag -> unit
val pp_print_type          : formatter -> ty -> unit
val pp_print_tydef         : formatter -> tydef -> unit
val pp_print_unop          : formatter -> unop -> atom -> unit
val pp_print_binop         : formatter -> binop -> atom -> atom -> unit
val pp_print_subop         : formatter -> subop -> unit
val pp_print_sub_block     : formatter -> sub_block -> unit
val pp_print_ext           : formatter -> string -> ty -> ty list -> atom list -> unit
val pp_print_alloc_op      : formatter -> alloc_op -> unit
val pp_print_subscript     : subop -> atom -> atom -> formatter -> unit
val pp_print_expr          : formatter -> exp -> unit
val pp_print_expr_size     : int -> formatter -> exp -> unit
val pp_print_atom          : formatter -> atom -> unit
val pp_print_prog          : formatter -> prog -> unit
val pp_print_raw_string    : formatter -> int array -> unit
val pp_print_poly_alloc_op : (formatter -> 'a -> unit) -> formatter -> 'a poly_alloc_op -> unit
val pp_print_alloc_op      : formatter -> alloc_op -> unit
val pp_print_poly_pred     : (formatter -> 'a -> unit) -> formatter -> 'a poly_pred -> unit
val pp_print_pred          : formatter -> pred -> unit
val pp_print_label         : formatter -> frame_label -> unit
val pp_print_function      : formatter -> var -> fundef -> unit

val pp_print_import_args   : formatter -> import_arg list -> unit
val pp_print_import_info   : formatter -> import_info -> unit

val debug_prog : string -> prog -> unit
val debug_mprog : string -> mprog -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
