(*
 * Add assertions around all array and tuple accesses.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_frame
open Fir_standardize

open Sizeof_const
open Sizeof_type

module Pos = MakePos (struct let name = "Fir_assert" end)
open Pos

(*
 * Type for RawData subscripts.
 *)
let pre_subscript = precision_subscript
let ty_subscript = TyRawInt (pre_subscript, signed_subscript)
let plus_subscript = PlusRawIntOp (pre_subscript, signed_subscript)
let atom_word = AtomRawInt (Rawint.of_int pre_subscript signed_subscript sizeof_pointer)

let plus_pointer_op op = PlusPointerOp (op, pre_subscript, signed_subscript)
let sub_script = RawIntIndex (pre_subscript, signed_subscript)

let subscript_of_int i =
   AtomRawInt (Rawint.of_int pre_subscript signed_subscript i)

let zero32 = Rawint.of_int pre_subscript signed_subscript 0
let zero_subscript = subscript_of_int 0
let one_subscript = subscript_of_int 1

(*
 * Adjust subscript operation to use a int32 index.
 *)
let assert_rawint_index genv pos loc op a cont =
   let pos = string_pos "assert_rawint_index" pos in
      match op.sub_script with
         IntIndex ->
            let v_index = new_symbol_string "rawint_index" in
            let genv = genv_add_var genv v_index ty_subscript in
            let op = { op with sub_script = RawIntIndex (pre_subscript, signed_subscript) } in
               make_exp loc (LetAtom (v_index, ty_subscript, AtomUnop (RawIntOfIntOp (pre_subscript, signed_subscript), a),
                                      cont genv op (AtomVar v_index)))
       | RawIntIndex (pre, signed) ->
            let v_index = new_symbol_string "rawint_index" in
            let genv = genv_add_var genv v_index ty_subscript in
            let op = { op with sub_script = RawIntIndex (pre_subscript, signed_subscript) } in
               make_exp loc (LetAtom (v_index, ty_subscript, AtomUnop (RawIntOfRawIntOp (pre_subscript, signed_subscript, pre, signed), a),
                                      cont genv op (AtomVar v_index)))

(*
 * Adjust subscript operation to use a byte index.
 *)
let assert_byte_index genv pos loc sub_index a cont =
   let pos = string_pos "assert_byte_index" pos in
      match sub_index with
         WordIndex ->
            let v_index = new_symbol_string "byte_index" in
            let genv = genv_add_var genv v_index ty_subscript in
               make_exp loc (LetAtom (v_index, ty_subscript, AtomBinop (MulRawIntOp (pre_subscript, signed_subscript), a, atom_word),
                                      cont genv (AtomVar v_index)))
       | ByteIndex ->
            cont genv a

let assert_byte_index_op genv pos loc op a cont =
   let pos = string_pos "assert_byte_index_op" pos in
      assert_byte_index genv pos loc op.sub_index a (fun genv a ->
            cont genv { op with sub_index = ByteIndex } a)

(*
 * Convert the length.
 *)
let assert_length genv pos loc op a cont =
   let pos = string_pos "assert_length" pos in
      match a with
         AtomVar _ ->
            assert_rawint_index genv pos loc op a (fun genv op a ->
                  assert_byte_index_op genv pos loc op a cont)
       | AtomInt i
       | AtomEnum (_, i) ->
            let i = Rawint.of_int pre_subscript signed_subscript (i * sizeof_pointer) in
            let op = { op with sub_index = ByteIndex; sub_script = sub_script } in
               cont genv op (AtomRawInt i)
       | AtomRawInt i ->
            cont genv op (AtomRawInt (Rawint.of_rawint pre_subscript signed_subscript i))
       | AtomLabel _
       | AtomSizeof _ ->
            cont genv op a
       | AtomNil _
       | AtomFloat _
       | AtomConst _
       | AtomTyApply _
       | AtomTyPack _
       | AtomTyUnpack _
       | AtomFun _ ->
            raise (FirException (pos, StringAtomError ("not an integer", a)))
       | AtomUnop _
       | AtomBinop _ ->
            raise (FirException (pos, StringAtomError ("not implemented", a)))

(*
 * Adjust subscript to use TyPointer.
 *)
let assert_pointer_index genv pos loc op v a cont =
   let pos = string_pos "assert_pointer_index" pos in
   let s = "ptr" in
   let v1 = new_symbol_string (s ^ "_base") in
   let v2 = new_symbol_string (s ^ "_ptr") in
   let block_op = op.sub_block in
   let ty = TyPointer block_op in
   let e = cont genv op a (AtomVar v2) zero_subscript in
   let e = make_exp loc (LetAtom (v2, ty, AtomBinop (plus_pointer_op block_op, AtomVar v1, a), e)) in
      make_exp loc (LetAtom (v1, ty, AtomUnop (PointerOfBlockOp block_op, v), e))

(*
 * Adjust the subscript operation.
 *)
let assert_subscript genv pos loc op v a cont =
   let pos = string_pos "assert_subscript" pos in
      assert_length genv pos loc op a (fun genv op a ->
            match a with
               AtomVar _ ->
                  assert_pointer_index genv pos loc op v a cont
             | AtomRawInt _
             | AtomLabel _ ->
                  cont genv op a v a
             | _ ->
                  raise (FirException (pos, StringAtomError ("not an integer", a))))

(*
 * Expressions.
 *)
let rec assert_exp fenv genv e =
   let pos = string_pos "assert_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            make_exp loc (LetAtom (v, ty, a, assert_exp fenv genv e))
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            make_exp loc (LetExt (v, ty1, s, b, ty2, ty_args, args, assert_exp fenv genv e))
       | TailCall _
       | SpecialCall _ ->
            e
       | Match (a, cases) ->
            make_exp loc (Match (a, List.map (fun (l, s, e) -> l, s, assert_exp fenv genv e) cases))
       | MatchDTuple (a, cases) ->
            make_exp loc (MatchDTuple (a, List.map (fun (l, a_opt, e) -> l, a_opt, assert_exp fenv genv e) cases))
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            make_exp loc (TypeCase (a1, a2, v1, v2, assert_exp fenv genv e1, assert_exp fenv genv e2))
       | LetAlloc (v, op, e) ->
            assert_alloc_exp fenv genv pos loc v op e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            assert_subscript_exp fenv genv pos loc op v1 ty v2 a e
       | SetSubscript (op, label, v, a1, ty, a2, e) ->
            assert_set_subscript_exp fenv genv pos loc op label v a1 ty a2 e
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            assert_memcpy_exp fenv genv pos loc op label v1 a1 v2 a2 a3 e
       | LetGlobal (op, v, ty, l, e) ->
            make_exp loc (LetGlobal (op, v, ty, l, assert_exp fenv genv e))
       | SetGlobal (op, label, v, ty, a, e) ->
            make_exp loc (SetGlobal (op, label, v, ty, a, assert_exp fenv genv e))
       | Assert (_, _, e) ->
            (* Ignore existing assertions *)
            assert_exp fenv genv e
       | Call (label, f, args, e) ->
            make_exp loc (Call (label, f, args, assert_exp fenv genv e))
       | Debug (info, e) ->
            make_exp loc (Debug (info, assert_exp fenv genv e))

(*
 * Add an allocation.
 * Make sure to reserve space.
 *)
and assert_alloc_exp fenv genv pos loc v op e =
   let pos = string_pos "assert_alloc_exp" pos in
   let alloc size op =
      let label = new_symbol_string "reserve" in
      let e = make_exp loc (LetAlloc (v, op, assert_exp fenv genv e)) in
         make_exp loc (Assert (label, Reserve (size, one_subscript), e))
   in
      match op with
         AllocTuple (_, _, _, args)
       | AllocUnion (_, _, _, _, args)
       | AllocArray (_, args)
       | AllocDTuple (_, _, _, args) ->
            let size = List.length args * sizeof_pointer in
               alloc (subscript_of_int size) op
       | AllocVArray (ty, sub_index, size, init) ->
            assert_byte_index genv pos loc sub_index size (fun genv size ->
                  alloc size (AllocVArray (ty, ByteIndex, size, init)))
       | AllocMalloc (_, size) ->
            alloc size op
       | AllocFrame (v, _) ->
            alloc (AtomSizeof ([v], zero32)) op

(*
 * Add an assertion before a subscript.
 *    1. Check the array bounds
 *    2. Check the element type for TyRawData
 *)
and assert_subscript_exp fenv genv pos loc op v1 ty v2 a2 e =
   let pos = string_pos "assert_subscript_exp" pos in
      assert_subscript genv pos loc op v2 a2 (fun genv op a2 v2' a2' ->
            (* Add a type check *)
            let e = make_exp loc (LetSubscript (op, v1, ty, v2', a2', assert_exp fenv genv e)) in
            let e =
               match op.sub_value with
                  PolySub
                | BlockPointerSub
                | RawPointerSub
                | FunctionSub ->
                     let label = new_symbol_string "check_element" in
                        make_exp loc (Assert (label, ElementCheck (ty, op, v2, a2), e))
                | _ ->
                     e
            in

            (* Add a bounds check *)
            let size = subscript_of_int (sizeof_type genv pos 0 ty) in
            let v_limit = new_symbol_string "limit" in
            let label = new_symbol_string "check_bounds" in
            let e = make_exp loc (Assert (label, BoundsCheck (op, v2, a2, AtomVar v_limit), e)) in
               make_exp loc (LetAtom (v_limit, ty_subscript, AtomBinop (plus_subscript, a2, size), e)))

(*
 * Add an bounds-check assertion before an assignment.
 *)
and assert_set_subscript_exp fenv genv pos loc op label v1 a1 ty a2 e =
   let pos = string_pos "assert_set_subscript_exp" pos in
      assert_subscript genv pos loc op v1 a1 (fun genv op a1 v1' a1' ->
            (* Add a bounds check *)
            let size = subscript_of_int (sizeof_type genv pos 0 ty) in
            let v_limit = new_symbol_string "limit" in
            let label' = new_symbol_string "check_bounds" in
            let e = make_exp loc (SetSubscript (op, label, v1', a1', ty, a2, assert_exp fenv genv e)) in
            let e = make_exp loc (Assert (label', BoundsCheck (op, v1, a1, AtomVar v_limit), e)) in
               make_exp loc (LetAtom (v_limit, ty_subscript, AtomBinop (plus_subscript, a1, size), e)))

(*
 * Add bounds-check assertions before a memcpy.
 *)
and assert_memcpy_exp fenv genv pos loc op label v1 a1 v2 a2 a3 e =
   let pos = string_pos "assert_memcpy_exp" pos in
      assert_subscript genv pos loc op v1 a1 (fun genv _ a1 v1' a1' ->
      assert_subscript genv pos loc op v2 a2 (fun genv _ a2 v2' a2' ->
      assert_length genv pos loc op a3 (fun genv op a3 ->
            (* Add bounds checks *)
            let v_to_limit = new_symbol_string "to_limit" in
            let v_from_limit = new_symbol_string "from_limit" in
            let label_to = new_symbol_string "check_bounds_to" in
            let label_from = new_symbol_string "check_bounds_from" in
            let e = make_exp loc (Memcpy (op, label, v1', a1', v2', a2', a3, assert_exp fenv genv e)) in
            let e = make_exp loc (Assert (label_from, BoundsCheck (op, v2, a2, AtomVar v_from_limit), e)) in
            let e = make_exp loc (LetAtom  (v_from_limit, ty_subscript, AtomBinop (plus_subscript, a2, a3), e)) in
            let e = make_exp loc (Assert (label_to, BoundsCheck (op, v1, a1, AtomVar v_to_limit), e)) in
               make_exp loc (LetAtom  (v_to_limit, ty_subscript, AtomBinop (plus_subscript, a1, a3), e)))))

(*
 * Add assertions to the program.
 *)
let assert_prog prog =
   let { prog_funs = funs;
         prog_types = tenv;
         prog_globals = globals;
         prog_names = names
       } = prog
   in
   let genv = prog_genv prog in
   let fenv = frame_prog prog in

   (* Add assertions to all the function bodies *)
   let funs =
      SymbolTable.map (fun (info, ty, ty_vars, vars, e) ->
            info, ty, ty_vars, vars, assert_exp fenv genv e) funs
   in

   (* Put the functions pack *)
   let prog = { prog with prog_funs = funs } in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
