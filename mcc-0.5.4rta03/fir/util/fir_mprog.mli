(*
 * Marshal program construction
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir
open Fir_env
open Fir_pos
open Fir_rttd_map

(*
 * An rttd table.
 *)
module Rttd :
sig
   type t = var RttdTable.t

   val empty : t
   val add : t -> var -> int list -> t
   val find : t -> tenv -> pos -> ty list -> var
   val map : (int list -> var -> var) -> t -> t
   val to_list : t -> (var * int list) list
end

(*
 * Build an initial mprog for the FIR.
 *)
val mprog_of_prog : prog -> mprog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
