(*
 * (Partial) type closure conversion for types.
 *
 * These are the parts that are implemented:
 *
 *   1. Close the frame types.  This is a normal-style closure
 *      conversion where we compute free type variables of each
 *      frame as a fixpoint.
 *
 *   2. Closure conversion for functions.  We calculate
 *      free type variables of function bodies.
 *
 * This closure conversion deals with universal types exclusively,
 * namely for frames and functions.  It will never add any type
 * arguments to any of the type definitions.
 *
 * CAUTION:
 *
 * Running ty_closure may introduce TyDelayed, so you
 * should run type inference (in Fir_infer) after this.
 *
 * We do not modify any AtomTyApply, because Fir_infer will
 * ignore this information anyway.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_print
open Fir_state
open Fir_standardize

open Sizeof_type

module Pos = MakePos (struct let name = "Fir_ty_closure" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * This operation is supposed to adjust frame arities.
 * This is essentially a closure conversion.  We compute defs/uses
 * for each frame to form the dataflow equations, then we solve them,
 * then we add extra type arguments to the frames.
 *
 *     dflow_params: the frame's type parameters
 *     dflow_uses: type variables that are used
 *     dflow_calls: frames that are called, and the parameters
 *        that were supplied
 *)
type denv =
   { denv_vars : var list;
     denv_params : SymbolSet.t;
     denv_uses : SymbolSet.t;
     denv_calls : (var * SymbolSet.t) list
   }

type fenv = denv SymbolTable.t

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * A utility to place the first n element of the list into a set.
 *)
let firstn_set n l =
   let rec collect s n l =
      if n = 0 then
         s
      else
         match l with
            h :: l ->
               collect (SymbolSet.add s h) (pred n) l
          | [] ->
               s
   in
      collect SymbolSet.empty n l

(*
 * We should have one ty for each var.
 * Once the types run out, use TyDelayed for the rest.
 *)
let rec append_ty_delayed vars tyl =
   match vars, tyl with
      _ :: vars, ty :: tyl ->
         ty :: append_ty_delayed vars tyl
    | _ :: vars, [] ->
         TyDelayed :: append_ty_delayed vars tyl
    | [], _ ->
         []

(*
 * Print the denv.
 *)
let pp_print_call buf (v, defs) =
   Format.fprintf buf "@ @[<b 3>%a(%a)@]" (**)
      pp_print_symbol v
      (pp_print_symbol_set ",") defs

let pp_print_calls buf calls =
   List.iter (pp_print_call buf) calls

let pp_print_denv buf denv =
   let { denv_vars = vars;
         denv_params = params;
         denv_uses = uses;
         denv_calls = calls
       } = denv
   in
      Format.fprintf buf "@[<hv 0>@[<hv 3>denv {";
      Format.fprintf buf "@ @[<b 3>vars =@ %a@]" (pp_print_symbol_list ",") vars;
      Format.fprintf buf "@ @[<b 3>params =@ %a@]" (pp_print_symbol_set ",") params;
      Format.fprintf buf "@ @[<b 3>uses =@ %a@]" (pp_print_symbol_set ",") uses;
      Format.fprintf buf "@ @[<b 3>calls =%a@]" pp_print_calls calls;
      Format.fprintf buf "@]@ }@]"

(*
 * Print all the entries.
 *)
let pp_print_fenv buf fenv =
   Format.fprintf buf "@[<hv 0>@[<hv 3>fenv {";
   SymbolTable.iter (fun v denv ->
         Format.fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
            pp_print_symbol v
            pp_print_denv denv) fenv;
   Format.fprintf buf "@]@ }@]"

(************************************************************************
 * DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Empty info based on original binding vars.
 *)
let denv_empty vars =
   { denv_vars = [];
     denv_params = List.fold_left SymbolSet.add SymbolSet.empty vars;
     denv_uses = SymbolSet.empty;
     denv_calls = []
   }

(*
 * Add to denv.
 *)
let denv_add_use denv v =
   { denv with denv_uses = SymbolSet.add denv.denv_uses v }

let denv_subtract_use denv v =
   { denv with denv_uses = SymbolSet.remove denv.denv_uses v }

let denv_subtract_uses denv vars =
   let uses = List.fold_left SymbolSet.remove denv.denv_uses vars in
      { denv with denv_uses = uses }

let denv_add_call denv v vars =
   { denv with denv_calls = (v, vars) :: denv.denv_calls }

(*
 * Construct the dataflow equations.
 *)
let rec dflow_type genv pos denv ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyDelayed
    | TyRawData
    | TyProject _
    | TyPointer _
    | TyDTuple (_, None) ->
         denv
    | TyFun (ty_vars, ty_res) ->
         let denv = dflow_type_list genv pos denv ty_vars in
            dflow_type genv pos denv ty_res
    | TyUnion (_, tyl, _)
    | TyApply (_, tyl) ->
         dflow_type_list genv pos denv tyl
    | TyTuple (_, fields)
    | TyDTuple (_, Some fields)
    | TyTag (_, fields) ->
         dflow_mutable_type_list genv pos denv fields
    | TyArray ty
    | TyCase ty ->
         dflow_type genv pos denv ty
    | TyAll (vars, ty)
    | TyExists (vars, ty) ->
         let denv = dflow_type genv pos denv ty in
            denv_subtract_uses denv vars
    | TyObject (v, ty) ->
         let denv = dflow_type genv pos denv ty in
            denv_subtract_use denv v
    | TyVar v ->
         denv_add_use denv v
    | TyFrame (v, tyl) ->
         let denv = dflow_type_list genv pos denv tyl in
         let vars, _ = genv_lookup_frame genv pos v in
         let len = List.length tyl in
         let defs = firstn_set len vars in
            denv_add_call denv v defs

and dflow_type_list genv pos denv tyl =
   List.fold_left (dflow_type genv pos) denv tyl

and dflow_mutable_type_list genv pos denv fields =
   List.fold_left (fun denv (ty, _) ->
         dflow_type genv pos denv ty) denv fields
(*
 * Liveness looks at all types in the frame.
 *)
let dflow_frame genv pos denv fields =
   SymbolTable.fold (fun denv _ subfields ->
         List.fold_left (fun denv (_, ty, _) ->
               dflow_type genv pos denv ty) denv subfields) denv fields

(*
 * Dataflow for a function.
 *)
let dflow_fundef genv pos vars ty =
   let pos = string_pos "dflow_fundef" pos in
      dflow_type genv pos (denv_empty vars) ty

(*
 * Compute dataflow equations for each of the frames
 * and each of the functions.
 *)
let dflow_prog genv prog =
   let { prog_frames = frames;
         prog_funs = funs
       } = prog
   in
   let fenv = SymbolTable.empty in

   (* Dataflow for the frames *)
   let fenv =
      SymbolTable.fold (fun fenv v (vars, frame) ->
            let pos = string_pos "dflow_prog" (var_exp_pos v) in
            let denv = dflow_frame genv pos (denv_empty vars) frame in
               SymbolTable.add fenv v denv) fenv frames
   in

   (* Dataflow for the functions *)
   let fenv =
      SymbolTable.fold (fun fenv f (_, ty_vars, ty, _, _) ->
            let pos = string_pos "dflow_prog" (var_exp_pos f) in
            let denv = dflow_fundef genv pos ty_vars ty in
               SymbolTable.add fenv f denv) fenv funs
   in
      fenv

(************************************************************************
 * DATAFLOW SOLVING
 ************************************************************************)

(*
 * A single step in solving the dataflow equations.
 *)
let dflow_step fenv =
   SymbolTable.fold (fun (fenv, changed) v denv ->
         let { denv_params = params;
               denv_uses = uses;
               denv_calls = calls
             } = denv
         in
         let uses' =
            List.fold_left (fun uses (v', defs) ->
                  let denv' = SymbolTable.find fenv v' in
                  let { denv_params = params';
                        denv_uses = uses'
                      } = denv'
                  in
                  let uses' = SymbolSet.diff (SymbolSet.diff uses' params') defs in
                     SymbolSet.union uses uses') uses calls
         in
            if SymbolSet.equal uses' uses then
               fenv, changed
            else
               let denv = { denv with denv_uses = uses' } in
               let fenv = SymbolTable.add fenv v denv in
                  fenv, true) (fenv, false) fenv

(*
 * Solve for the fixpoint.
 *)
let rec dflow_fixpoint fenv =
   let fenv, changed = dflow_step fenv in
      if changed then
         dflow_fixpoint fenv
      else
         fenv

(*
 * Now compute the vars.
 *)
let dflow_solve fenv =
   let fenv = dflow_fixpoint fenv in
   let fenv =
      SymbolTable.map (fun denv ->
            let { denv_params = params;
                  denv_uses = uses
                } = denv
            in
            let uses = SymbolSet.diff uses params in
            let uses = SymbolSet.to_list uses in
               { denv with denv_vars = uses }) fenv
   in
      if debug debug_fir_ty_closure then
         Format.eprintf "@[<hv 3>*** fir_ty_closure: solution:@ %a@]@." pp_print_fenv fenv;
      fenv

(************************************************************************
 * CLOSURE CONVERSION
 ************************************************************************)

(*
 * Now that we have the fixpoint, compute the new parameters,
 * and modify all the calls.
 *)
let rec close_type fenv ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyDelayed
    | TyRawData
    | TyProject _
    | TyPointer _
    | TyVar _
    | TyDTuple (_, None) ->
         ty
    | TyFun (ty_vars, ty_res) ->
         let ty_vars = close_type_list fenv ty_vars in
         let ty_res = close_type fenv ty_res in
            TyFun (ty_vars, ty_res)
    | TyUnion (v, tyl, i) ->
         TyUnion (v, close_type_list fenv tyl, i)
    | TyTuple (tc, fields) ->
         TyTuple (tc, close_mutable_type_list fenv fields)
    | TyDTuple (ty_var, Some fields) ->
         TyDTuple (ty_var, Some (close_mutable_type_list fenv fields))
    | TyTag (ty_var, fields) ->
         TyTag (ty_var, close_mutable_type_list fenv fields)
    | TyApply (v, tyl) ->
         TyApply (v, close_type_list fenv tyl)
    | TyArray ty ->
         TyArray (close_type fenv ty)
    | TyCase ty ->
         TyCase (close_type fenv ty)
    | TyAll (vars, ty) ->
         TyAll (vars, close_type fenv ty)
    | TyExists (vars, ty) ->
         TyExists (vars, close_type fenv ty)
    | TyObject (v, ty) ->
         TyObject (v, close_type fenv ty)
    | TyFrame (v, tyl) ->
         let tyl = close_type_list fenv tyl in
         let { denv_vars = vars } = SymbolTable.find fenv v in
         let len1 = List.length tyl in
         let vars = Mc_list_util.nth_tl len1 vars in
         let ty_vars = List.map (fun v -> TyVar v) vars in
         let tyl = tyl @ ty_vars in
            TyFrame (v, tyl)

and close_type_list fenv tyl =
   List.map (close_type fenv) tyl

and close_mutable_type_list fenv fields =
   List.map (fun (ty, b) -> close_type fenv ty, b) fields

(*
 * Close a frame.
 * Just add all the new vars.
 *)
let close_frame fenv v (_, frame) =
   let fields =
      SymbolTable.map (fun subfields ->
            List.map (fun (v_subfield, ty, i) ->
                  v_subfield, close_type fenv ty, i) subfields) frame
   in
   let { denv_vars = vars } = SymbolTable.find fenv v in
      vars, fields

(*
 * Map a function over all the types in the unop.
 *)
let close_unop fenv op =
   match op with
      NotEnumOp _

      (* Negation (arithmetic and bitwise) for NAML ints *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Negation (arithmetic and bitwise) for native ints *)
    | UMinusRawIntOp _
    | NotRawIntOp _

      (*
       * RawBitFieldOp (pre, signed, offset, length):
       *    Extracts the bitfield in an integer value, which starts at bit
       *    <offset> (counting from LSB) and containing <length> bits. The
       *    bit shift and length are constant values.  To access a variable
       *    bitfield you must do a manual shift/mask; this optimization is
       *    only good for constant values.  (pre, signed) refer to the
       *    rawint precision of the result; the input should be int32.
       *)
    | RawBitFieldOp     _

      (* Unary floating-point operations *)
    | UMinusFloatOp     _
    | AbsFloatOp        _
    | SinFloatOp        _
    | CosFloatOp        _
    | TanFloatOp        _
    | ASinFloatOp       _
    | ACosFloatOp       _
    | ATanFloatOp       _
    | SinHFloatOp       _
    | CosHFloatOp       _
    | TanHFloatOp       _
    | ExpFloatOp        _
    | LogFloatOp        _
    | Log10FloatOp      _
    | SqrtFloatOp       _
    | CeilFloatOp       _
    | FloorFloatOp      _

      (*
       * Coercions:
       *    Int refers to an ML-style integer
       *    Float refers to floating-point value
       *    RawInt refers to native int, any precision
       *
       * In operators where both terms have precision qualifiers, the
       * destination precision is always specified before the source
       * precision.
       *)

      (* Coercions to int *)
    | IntOfFloatOp      _
    | IntOfRawIntOp     _

      (* Coerce to float *)
    | FloatOfIntOp      _
    | FloatOfFloatOp    _
    | FloatOfRawIntOp   _

      (* Coerce to rawint *)
    | RawIntOfIntOp     _
    | RawIntOfEnumOp    _
    | RawIntOfFloatOp   _

      (*
       * Coerce a rawint _
       * First pair is destination precision, second pair is source.
       *)
    | RawIntOfRawIntOp  _

      (*
       * Integer<->pointer coercions (only for C, not inherently safe)
       * These operators are specifically related to infix pointers...
       *)
    | RawIntOfPointerOp _
    | PointerOfRawIntOp _

      (*
       * Create an infix pointer from a block pointer.  The infix pointer
       * so that both the base and _
       * to the beginning _
       *)
    | PointerOfBlockOp _ ->
         op

      (*
       * Get the block length.
       *)
    | LengthOfBlockOp (subop, ty) ->
         LengthOfBlockOp (subop, close_type fenv ty)

      (*
       * Type coercions.
       *)
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         DTupleOfDTupleOp (ty_var, close_mutable_type_list fenv ty_fields)
    | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
         UnionOfUnionOp (ty_var, close_type_list fenv tyl, s1, s2)
    | RawDataOfFrameOp (ty_var, tyl) ->
         RawDataOfFrameOp (ty_var, close_type_list fenv tyl)

(*
 * Map a function over all the types in the binop.
 *)
let close_binop fenv op =
   match op with
      (* Bitwise operations on enumerations. *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

      (* Standard binary operations on NAML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on NAML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (*
       * Standard binary operations on native ints.  The precision is
       * the result precision; the inputs should match this precision.
       *)
    | PlusRawIntOp   _
    | MinusRawIntOp  _
    | MulRawIntOp    _
    | DivRawIntOp    _
    | RemRawIntOp    _
    | SlRawIntOp     _
    | SrRawIntOp     _
    | AndRawIntOp    _
    | OrRawIntOp     _
    | XorRawIntOp    _
    | MaxRawIntOp    _
    | MinRawIntOp    _

      (*
       * RawSetBitFieldOp (pre, signed, _
       *    See comments for RawBitFieldOp. This modifies the bitfield starting
       *    at bit <_
       *       First atom is the integer containing the field.
       *       Second atom is the value to be set in the field.
       *    The resulting integer contains the revised field, with type
       *    ACRawInt (pre, signed)
       *)
    | RawSetBitFieldOp _

      (* Comparisons on native ints *)
    | EqRawIntOp     _
    | NeqRawIntOp    _
    | LtRawIntOp     _
    | LeRawIntOp     _
    | GtRawIntOp     _
    | GeRawIntOp     _
    | CmpRawIntOp    _

      (* Standard binary operations on floats *)
    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

      (* Comparisons on floats *)
    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

      (*
       * Arctangent.  This computes arctan(y/x), where y is the first atom
       * and x is the second atom given.  Handles case when x = 0 correctly.
       *)
    | ATan2FloatOp _

      (*
       * Power.  This computes x^y.
       *)
    | PowerFloatOp _

      (*
       * Float hacking.
       * This sets the exponent field _
       *)
    | LdExpFloatIntOp _

      (*
       * Pointer arithmetic. The pointer in the first argument, and the
       * returned pointer should be infix pointers (which keep the base
       * pointer as well as a pointer to anywhere within the block).
       *)
    | PlusPointerOp _ ->
         op


      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty ->
         EqEqOp (close_type fenv ty)
    | NeqEqOp ty ->
         NeqEqOp (close_type fenv ty)

(*
 * Close an expression.
 * This is more work than it seems.  All we really have to do is look
 * for occurrences of AllocFrame and add the extra arguments as TyDelayed.
 * Do the same for all the types.
 *)
let rec close_atom fenv a =
   match a with
      AtomNil ty ->
         AtomNil (close_type fenv ty)
    | AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomLabel _
    | AtomSizeof _
    | AtomVar _
    | AtomFun _
    | AtomTyUnpack _ ->
         a
    | AtomConst (ty, tv, i) ->
         AtomConst (close_type fenv ty, tv, i)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (close_atom fenv a, close_type fenv ty, close_type_list fenv tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, close_type fenv ty, close_type_list fenv tyl)
    | AtomUnop (op, a) ->
         AtomUnop (close_unop fenv op, close_atom fenv a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (close_binop fenv op, close_atom fenv a1, close_atom fenv a2)

and close_atom_opt fenv a_opt =
   match a_opt with
      Some a -> Some (close_atom fenv a)
    | None -> None

and close_atom_list fenv args =
   List.map (close_atom fenv) args

let close_atom_opt_list fenv args =
   List.map (close_atom_opt fenv) args

(*
 * For allocations, modify AllocFrame to include the new type args.
 *)
let close_alloc_op fenv op =
   match op with
      AllocTuple (tc, ty_vars, ty, args) ->
         AllocTuple (tc, ty_vars, close_type fenv ty, close_atom_list fenv args)
    | AllocDTuple (ty, ty_var, a, args) ->
         AllocDTuple (close_type fenv ty, ty_var, close_atom fenv a, close_atom_list fenv args)
    | AllocUnion (ty_vars, ty, tv, i, args) ->
         AllocUnion (ty_vars, close_type fenv ty, tv, i, close_atom_list fenv args)
    | AllocArray (ty, args) ->
         AllocArray (close_type fenv ty, close_atom_list fenv args)
    | AllocVArray (ty, op, a1, a2) ->
         AllocVArray (close_type fenv ty, op, close_atom fenv a1, close_atom fenv a2)
    | AllocMalloc (ty, a) ->
         AllocMalloc (close_type fenv ty, close_atom fenv a)
    | AllocFrame (v, tyl) ->
         let { denv_vars = vars } = SymbolTable.find fenv v in
            AllocFrame (v, append_ty_delayed vars tyl)

(*
 * For the rest of the conversion, nothing special happens.
 * In fat, we need to define a generic module to perform
 * grunge code like this.  All we do in the rest is map
 * the closure calls over all the types, atoms, and expressions
 * in the program.
 *)
let close_tailop fenv op =
   match op with
      TailSysMigrate (i, a1, a2, a3, args) ->
         TailSysMigrate (i, close_atom fenv a1, close_atom fenv a2, close_atom fenv a3, close_atom_list fenv args)
    | TailAtomic (a1, a2, args) ->
         TailAtomic (close_atom fenv a1, close_atom fenv a2, close_atom_list fenv args)
    | TailAtomicRollback (a1, a2) ->
         TailAtomicRollback (close_atom fenv a1, close_atom fenv a2)
    | TailAtomicCommit (a1, a2, args) ->
         TailAtomicCommit (close_atom fenv a1, close_atom fenv a2, close_atom_list fenv args)


let close_pred fenv pred =
   match pred with
      IsMutable a ->
         IsMutable (close_atom fenv a)
    | Reserve (a1, a2) ->
         Reserve (close_atom fenv a1, close_atom fenv a2)
    | BoundsCheck (op, a1, a2, a3) ->
         BoundsCheck (op, close_atom fenv a1, close_atom fenv a2, close_atom fenv a3)
    | ElementCheck (ty, op, a1, a2) ->
         ElementCheck (close_type fenv ty, op, close_atom fenv a1, close_atom fenv a2)

let close_init fenv init =
   match init with
      InitAtom a ->
         InitAtom (close_atom fenv a)
    | InitAlloc op ->
         InitAlloc (close_alloc_op fenv op)
    | InitTag (ty_var, fields) ->
         InitTag (ty_var, close_mutable_type_list fenv fields)
    | InitRawData _
    | InitNames _ ->
         init

let rec close_exp fenv e =
   let loc = loc_of_exp e in
   let e = close_exp_core fenv (dest_exp_core e) in
      make_exp loc e

and close_exp_core fenv e =
   match e with
      LetAtom (v, ty, a, e) ->
         LetAtom (v, close_type fenv ty, close_atom fenv a, close_exp fenv e)
    | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
         LetExt (v, close_type fenv ty1, s, b, close_type fenv ty2, close_type_list fenv ty_args, close_atom_list fenv args, close_exp fenv e)
    | TailCall (label, f, args) ->
         TailCall (label, close_atom fenv f, close_atom_list fenv args)
    | SpecialCall (label, op) ->
         SpecialCall (label, close_tailop fenv op)
    | Match (a, cases) ->
         Match (close_atom fenv a, List.map (fun (l, s, e) -> l, s, close_exp fenv e) cases)
    | MatchDTuple (a, cases) ->
         MatchDTuple (close_atom fenv a, List.map (fun (l, a_opt, e) -> l, close_atom_opt fenv a_opt, close_exp fenv e) cases)
    | TypeCase (a1, a2, v1, v2, e1, e2) ->
         TypeCase (close_atom fenv a1, close_atom fenv a2, v1, v2, close_exp fenv e1, close_exp fenv e2)
    | LetAlloc (v, op, e) ->
         LetAlloc (v, close_alloc_op fenv op, close_exp fenv e)
    | LetSubscript (op, v, ty, a1, a2, e) ->
         LetSubscript (op, v, close_type fenv ty, close_atom fenv a1, close_atom fenv a2, close_exp fenv e)
    | SetSubscript (op, label, a1, a2, ty, a3, e) ->
         SetSubscript (op, label, close_atom fenv a1, close_atom fenv a2, close_type fenv ty, close_atom fenv a3, close_exp fenv e)
    | LetGlobal (op, v, ty, l, e) ->
         LetGlobal (op, v, close_type fenv ty, l, close_exp fenv e)
    | SetGlobal (op, label, l, ty, a, e) ->
         SetGlobal (op, label, l, close_type fenv ty, close_atom fenv a, close_exp fenv e)
    | Memcpy (op, label, a1, a2, a3, a4, a5, e) ->
         Memcpy (op, label, close_atom fenv a1, close_atom fenv a2, close_atom fenv a3, close_atom fenv a4, close_atom fenv a5, close_exp fenv e)
    | Call (label, f, args, e) ->
         Call (label, close_atom fenv f, close_atom_opt_list fenv args, close_exp fenv e)
    | Assert (label, pred, e) ->
         Assert (label, close_pred fenv pred, close_exp fenv e)
    | Debug (info, e) ->
         Debug (info, close_exp fenv e)

(*
 * Type definitions close each of the types in the
 * definition.  Note that we do not need to add any quantifications
 * here, the closure conversion only deals with universal quantification.
 *)
let close_tydef fenv tydef =
   match tydef with
      TyDefUnion (vars, fields) ->
         let fields = List.map (List.map (fun (ty, b) -> close_type fenv ty, b)) fields in
            TyDefUnion (vars, fields)
    | TyDefLambda (vars, ty) ->
         TyDefLambda (vars, close_type fenv ty)
    | TyDefDTuple _ ->
         tydef

(*
 * Function definitions are another special case.
 * We add the new variable arguments to the type and the
 * var list.
 *)
let close_fundef genv fenv f (info, ty_vars, ty, vars, e) =
   let pos = string_pos "close_fundef" (var_exp_pos f) in
   let { denv_vars = pvars } = SymbolTable.find fenv f in
   let ty_vars = ty_vars @ pvars in
   let ty = close_type fenv ty in
   let e = close_exp fenv e in
      info, ty_vars, ty, vars, e

(*
 * Other top-level conversions.
 *)
let close_import fenv import =
   { import with import_type = close_type fenv import.import_type }

let close_export fenv export =
   { export with export_type = close_type fenv export.export_type }

let close_global fenv (ty, init) =
   close_type fenv ty, close_init fenv init

(*
 * Compute the dataflow information, then update all the types
 * in the program.
 *)
let ty_close_prog prog =
   let _ =
      if debug debug_fir_ty_closure then
         debug_prog "fir_ty_closure: before" prog
   in
   let { prog_file = file;
         prog_types = tenv;
         prog_import = import;
         prog_export = export;
         prog_frames = frames;
         prog_names = names;
         prog_globals = globals;
         prog_funs = funs
       } = prog
   in
   let genv = genv_of_prog prog in

   (* Compute dataflow information *)
   let fenv = dflow_prog genv prog in
   let fenv = dflow_solve fenv in

   (* Modify all the parts *)
   let tenv = SymbolTable.map (close_tydef fenv) tenv in
   let import = SymbolTable.map (close_import fenv) import in
   let export = SymbolTable.map (close_export fenv) export in
   let frames = SymbolTable.mapi (close_frame fenv) frames in
   let names = SymbolTable.map (close_type fenv) names in
   let globals = SymbolTable.map (close_global fenv) globals in
   let funs = SymbolTable.mapi (close_fundef genv fenv) funs in

   let prog =
      (* Rebuild the program *)
      { prog_file = file;
        prog_types = tenv;
        prog_import = import;
        prog_export = export;
        prog_frames = frames;
        prog_names = names;
        prog_globals = globals;
        prog_funs = funs
      }
   in
      if debug debug_fir_ty_closure || !debug_print_fir > 1 then
         debug_prog "fir_ty_closure: after" prog;
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
