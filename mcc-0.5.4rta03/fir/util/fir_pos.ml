(*
 * FIR position information.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fir
open Fir_ds
open Fir_exn
open Fir_print
open Fir_state

(*
 * Location of exception.
 *)
type item =
   Exp of exp
 | String of string
 | Symbol of symbol
 | Type of ty
 | Atom of atom
 | Error of fir_error
 | Fundef of var * fundef
 | TypeLoc of loc * ty
 | AtomLoc of loc * atom

type pos = item Position.pos

(*
 * FIR exception.
 *)
exception FirException of pos * fir_error

(*
 * Location of a value.
 *)
let bloc = bogus_loc "Fir_pos"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_exp e
    | Fundef (_, (loc, _, _, _, _)) ->
         loc
    | String _
    | Symbol _
    | Type _
    | Atom _
    | Error _ ->
         bloc
    | TypeLoc (loc, _)
    | AtomLoc (loc, _) ->
         loc

(*
 * Print a location.
 *)
let rec pp_print_value buf e =
   match e with
      Exp e ->
         pp_print_expr_size (exn_expr_size ()) buf e
    | Fundef (f, def) ->
         pp_print_function buf f def
    | Atom a
    | AtomLoc (_, a) ->
         pp_print_atom buf a
    | Type ty
    | TypeLoc (_, ty) ->
         pp_print_type buf ty
    | String s ->
         pp_print_string buf s
    | Symbol v ->
         pp_print_symbol buf v
    | Error err ->
         pp_print_error buf err

(*
 * Exception printer.
 *)
and pp_print_error buf err =
   match err with
      UnboundVar v ->
         fprintf buf "unbound variable: %a" pp_print_symbol v
    | UnboundType v ->
         fprintf buf "unbound type: %a" pp_print_symbol v
    | AtomNotAVar a ->
         fprintf buf "atom not a var@ %a" pp_print_atom a
    | TypeError (ty1, ty2) ->
         fprintf buf "type error:@ @[<hv 3>this expression has type@ %a@]@ @[<hv 3>but it used with type@ %a@]" (**)
            pp_print_type ty2
            pp_print_type ty1
    | TypeAtomError (ty1, ty2, a) ->
         fprintf buf "type error:@ @[<hv 3>atom %a@]@ @[<hv 3>has type@ %a@]@ @[<hv 3>but it used with type@ %a@]" (**)
            pp_print_atom a
            pp_print_type ty2
            pp_print_type ty1
    | SubscriptError (ty1, ty2) ->
         fprintf buf "subscript error:@ @[<hv 3>array has type@ %a;@]@ @[<hv 3>index has type@ %a@]" (**)
            pp_print_type ty1
            pp_print_type ty2
    | TypeError4 (ty1a, ty2a, ty1b, ty2b) ->
         fprintf buf "@[<v 0>type error:@ @[<hv 3>this expression has type:@ %a@]@ @[<hv 3>but is used with type: @ %a@]@ @[<hv 3>this type:@ %a@]@ @[<hv 3>should have type:@ %a@]@]" (**)
            pp_print_type ty2a
            pp_print_type ty1a
            pp_print_type ty2b
            pp_print_type ty1b
    | TypeErrorMutable4 (ty1a, ty2a, ty1b, b1, ty2b, b2) ->
         fprintf buf "@[<v 0>type error (mutability mismatch):@ @[<hv 3>this expression has type:@ %a@]@ @[<hv 3>but is used with type:@ %a@]@ @[<hv 3>this subtype:@ %s %a@]@ @[<hv 3>should have type:@ %s %a@]@]" (**)
            pp_print_type ty2a
            pp_print_type ty1a
            (if b1 then "mutable" else "immutable") pp_print_type ty2b
            (if b2 then "mutable" else "immutable") pp_print_type ty1b
    | InvalidTyDelayed t ->
         fprintf buf "incompletely specified type: %a" pp_print_type t
    | ArityMismatch (i, j) ->
         fprintf buf "arity mismatch: wanted %d, got %d" i j
    | NotAFunction ty ->
         fprintf buf "not a function:@ %a" pp_print_type ty
    | NotASubscript ty ->
         fprintf buf "not a subscript:@ %a" pp_print_type ty
    | NotAnAggregate ty ->
         fprintf buf "not an aggregate:@ %a" pp_print_type ty
    | NotAUnion ty ->
         fprintf buf "not a union:@ %a" pp_print_type ty
    | NotAUnionDef tydef ->
         fprintf buf "not a union:@ %a" pp_print_tydef tydef
    | NotAnIntSet s ->
         fprintf buf "not an int set:@ %a" pp_print_set s
    | NotARawIntSet s ->
         fprintf buf "not a raw int set:@ %a" pp_print_set s
    | NotAFloatSet s ->
         fprintf buf "not a float set:@ %a" pp_print_set s
    | TyDefError tyd ->
         fprintf buf "incorrect type of type definition:@ %a" pp_print_tydef tyd
    | SetError (a, b) ->
         fprintf buf "the type of the set@ %a does not match that of@ %a" (**)
            pp_print_set a
            pp_print_set b
    | FieldOutOfBounds (ty, i) ->
         fprintf buf "field index out of bounds: #%d@ %a" i pp_print_type ty
    | IncompleteMatch (s1, s2) ->
         fprintf buf "incomplete match: wanted@ %a got@ %a" (**)
            pp_print_set s1
            pp_print_set s2
    | IndexOutOfBounds (ty, i) ->
         fprintf buf "index out of bounds: %d:@ %a" i pp_print_type ty
    | IllegalAlloc ty ->
         fprintf buf "illegal allocation type:@ %a" pp_print_type ty
    | SubscriptOutOfBounds i ->
         fprintf buf "subscript out of bounds:@ %d" i
    | InternalError s ->
         fprintf buf "internal error: %s" s
    | NotImplemented s ->
         fprintf buf "not implemented: %s" s
    | StringError s ->
         pp_print_string buf s
    | StringIntError (s, i) ->
         fprintf buf "%s: %d" s i
    | StringVarError (s, v) ->
         fprintf buf "%s: %a" s pp_print_symbol v
    | StringAtomError (s, a) ->
         fprintf buf "%s: %a" s pp_print_atom a
    | StringTypeError (s, ty) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_type ty
    | StringTydefError (s, ty) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_tydef ty
    | StringIntTypeError (s, i, ty) ->
         fprintf buf "@[<hv 3>%s%d:@ %a@]" s i pp_print_type ty
    | StringFormatError (s, f) ->
         fprintf buf "@[<hv 3>%s:@ %t@]" s f
    | MarshalError ->
         pp_print_string buf "marshaling error"

(*
 * Exception handlers.
 *)
module type PosSig =
sig
   val loc_pos        : loc -> pos

   val exp_pos        : exp -> pos
   val var_exp_pos    : var -> pos
   val string_exp_pos : string -> pos
   val type_exp_pos   : loc -> ty -> pos
   val atom_exp_pos   : loc -> atom -> pos

   val pos_pos    : pos -> pos -> pos
   val int_pos    : int -> pos -> pos
   val var_pos    : var -> pos -> pos
   val type_pos   : ty -> pos -> pos
   val atom_pos   : atom -> pos -> pos
   val error_pos  : fir_error -> pos -> pos
   val string_pos : string -> pos -> pos
   val fundef_pos : var -> fundef -> pos

   val del_pos     : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let exp_pos e           = base_pos (Exp e)
   let var_exp_pos v       = base_pos (Symbol v)
   let string_exp_pos s    = base_pos (String s)
   let type_exp_pos loc ty = base_pos (TypeLoc (loc, ty))
   let atom_exp_pos loc a  = base_pos (AtomLoc (loc, a))
   let fundef_pos v def    = base_pos (Fundef (v, def))

   let var_pos             = symbol_pos
   let type_pos ty pos     = cons_pos (Type ty) pos
   let atom_pos a pos      = cons_pos (Atom a) pos
   let error_pos e pos     = cons_pos (Error e) pos
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
