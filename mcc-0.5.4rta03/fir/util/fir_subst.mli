(*
 * Type substitutions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_pos

type subst

(*
 * Printing.
 *)
val pp_print_subst : Format.formatter -> subst -> unit

(*
 * Calculate free type vars.
 *)
val free_vars_type : SymbolSet.t -> ty -> SymbolSet.t
val free_vars_list : ty -> var list
val free_vars_exp_type : SymbolSet.t -> exp -> SymbolSet.t

(*
 * Substitution operations.
 *)
val subst_empty : subst

val subst_add_type_var : subst -> ty_var -> ty -> subst
val subst_add_mutable_var : subst -> var -> mutable_flag -> subst

val subst_lookup_type_var : subst -> ty_var -> ty
val subst_lookup_mutable_var : subst -> var -> mutable_flag

val subst_remove_type_var : subst -> ty_var -> subst
val subst_remove_mutable_var : subst -> var -> subst

val subst_type : subst -> ty -> ty
val subst_type_exp : subst -> exp -> exp
val subst_type_prog : subst -> prog -> prog

(*
 * Generic simultaneous substitution.
 *)
val subst_type_simul : pos -> ty -> var list -> ty list -> ty

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
