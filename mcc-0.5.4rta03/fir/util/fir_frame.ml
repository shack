(*
 * Frame computations.  We save the offset of each label,
 * as well as the total size of each frame.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_exn
open Fir_pos

open Sizeof_type

module Pos = MakePos (struct let name = "Fir_frame" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Frame info.
 *)
type t =
   { frame_size : int SymbolTable.t;
     frame_labels : int SymbolListTable.t
   }

(************************************************************************
 * ACCESS
 ************************************************************************)

(*
 * Get the size of a frame.
 *)
let sizeof_frame frames pos v =
   try SymbolTable.find frames.frame_size v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Get the offset of a label.
 *)
let offsetof_label frames pos (frame, field, subfield) =
   try SymbolListTable.find frames.frame_labels [frame; field; subfield] with
      Not_found ->
         raise (FirException (pos, UnboundVar subfield))

(************************************************************************
 * BUILDING
 ************************************************************************)

(*
 * Scan a single frame.
 *)
let frame_frame tenv sizes labels frame fields =
   let pos = string_pos "frame_frame" (var_exp_pos frame) in
   let labels, size =
      SymbolTable.fold (fun (labels, off) field subfields ->
            List.fold_left (fun (labels, off) (subfield, ty, size) ->
                  let off = align_type_dir tenv pos ty off in
                  let labels = SymbolListTable.add labels [frame; field; subfield] off in
                     labels, off + size) (labels, off) subfields) (labels, 0) fields
   in
   let sizes = SymbolTable.add sizes frame size in
      sizes, labels

(*
 * Scan the frames.
 *)
let frame_frames tenv sizes labels frames =
   SymbolTable.fold (fun (sizes, labels) v (_, frame) ->
         frame_frame tenv sizes labels v frame) (sizes, labels) frames

(*
 * Compute frame sizes and offsets for each frame.
 *)
let frame_prog prog =
   let { prog_types = tenv;
         prog_frames = frames
       } = prog
   in
   let sizes, labels = frame_frames tenv SymbolTable.empty SymbolListTable.empty frames in
      { frame_size = sizes;
        frame_labels = labels
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
