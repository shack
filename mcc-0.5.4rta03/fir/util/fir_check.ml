(*
 * FIR type checker.
 *
 * TODO:
 *    o Implement subop
 *    o Check mutability in SetSubscript for unions
 *    o All the object-related functions
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Interval_set

open Sizeof_const

open Fir
open Fir_ds
open Fir_pos
open Fir_env
open Fir_exn
open Fir_set
open Fir_type
open Fir_print
open Fir_state
open Fir_subst
open Fir_standardize

module Pos = MakePos (struct let name = "Fir_check" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Check that an integer set is bounded 0 <= s < n
 *)
let int_set_bounded s n =
   IntSet.fold (fun bounded left right ->
         let lflag =
            match left with
               Infinity ->
                  false
             | Closed i ->
                  i >= 0
             | Open i ->
                  i >= -1
         in
         let rflag =
            match right with
               Infinity ->
                  false
             | Closed i ->
                  i < n
             | Open i ->
                  i <= n
         in
            bounded && lflag && rflag) true s

(*
 * Integer form of subscript.
 * This should construct a word address.
 *)
let index_of_subscript pos a =
   let pos = string_pos "index_of_subscript" pos in
      match a with
         AtomInt i ->
            i
       | AtomRawInt i ->
            let i = Rawint.to_int i in
               if i mod sizeof_subscript <> 0 then
                  raise (FirException (pos, StringIntError ("unaligned subscript", i)));
               i / sizeof_subscript
       | _ ->
            raise (FirException (pos, StringAtomError ("illegal subscript", a)))

(*
 * Number of parameters in the tydef.
 *)
let arity_of_tydef tydef =
   let vars =
      match tydef with
         TyDefLambda (vars, _)
       | TyDefUnion (vars, _) ->
            vars
       | TyDefDTuple _ ->
            []
   in
      List.length vars

(************************************************************************
 * TYPE WELL-FORMEDNESS
 ************************************************************************)

(*
 * A type is "small" if it is not a rawint, float, or pointer.
 * Essentially, for a type to be small, it has to fit unambiguously
 * into a machine word.
 *)
type small_type =
   SmallType
 | AnyType

(*
 * Check that a type is well-formed.
 * This just makes sure that all the variables are defined.
 *
 * smallp is SmallType iff the type check is for a "small" type.
 * All types are small except TyRawInt, TyFloat, and TyPointer.
 *)
let rec check_type genv smallp pos ty =
   match ty with
      TyInt
    | TyRawData ->
         ()
    | TyEnum i ->
         if i < 0 || i > max_enum then
            raise (FirException (pos, StringIntError ("enumeration index out of range", i)))
    | TyRawInt _
    | TyFloat _
    | TyPointer _ ->
         if smallp <> AnyType then
            raise (FirException (pos, StringTypeError ("not a small type", ty)))

      (* At this stage we force the functions to return void *)
    | TyFun (tyl, TyEnum 0) ->
         check_type_list genv AnyType pos tyl
    | TyFun _ ->
         raise (FirException (pos, StringTypeError ("function does not return void", ty)))

      (* Tuples and arrays *)
    | TyTuple (NormalTuple, tyl)
    | TyTuple (RawTuple, tyl) ->
         check_mutable_type_list genv SmallType pos tyl
    | TyTuple (BoxTuple, [ty, _]) ->
         check_type genv AnyType pos ty
    | TyTuple (BoxTuple, _) ->
         raise (FirException (pos, StringTypeError ("box tuple should have exactly one element", ty)))

      (* Dependent tuples *)
    | TyDTuple (ty_var, Some tyl)
    | TyTag (ty_var, tyl) ->
         (* Check that ty_var is really a dependent tuple type *)
         if not (is_dtuple_tydef genv pos ty_var) then
            raise (FirException (pos, StringTypeError ("type is not a dependent tuple type", ty)));

         (* Elements must be small types *)
         check_mutable_type_list genv SmallType pos tyl

    | TyDTuple (ty_var, None) ->
         (* Check that ty_var is really a dependent tuple type *)
         if not (is_dtuple_tydef genv pos ty_var) then
            raise (FirException (pos, StringTypeError ("type is not a dependent tuple type", ty)))

      (* INCONSISTENCY: The technical report has AnyType, not SmallType.. *)
    | TyArray ty ->
         check_type genv SmallType pos ty

      (* Unions must be defined with the right number of arguments *)
    | TyUnion (v, tyl, s) ->
         check_union_type genv pos v tyl s

      (* Frames must be defined *)
    | TyFrame (v, tyl) ->
         if genv_mem_frame genv v then
            check_type_list genv SmallType pos tyl
         else
            raise (FirException (pos, StringVarError ("frame variable is not bound", v)))

      (* Polymorphism *)
    | TyVar v ->
         if not (genv_mem_poly genv v) then
            raise (FirException (pos, StringVarError ("unbound polymorphic variable", v)))
    | TyAll (vars, ty)
    | TyExists (vars, ty) ->
         let genv = genv_add_poly genv vars in
            check_type genv smallp pos ty
    | TyApply (v, tyl) ->
         let tydef = genv_lookup_type genv pos v in
         let len1 = arity_of_tydef tydef in
         let len2 = List.length tyl in
         let _ =
            if len1 <> len2 then
               raise (FirException (pos, ArityMismatch (len1, len2)))
         in
            check_type_list genv SmallType pos tyl

      (*
       * For TyProject (v, i), v must have an existential type,
       * and i must refer to one of the possible polymorphic vars.
       *)
    | TyProject (v, i) ->
         let ty = genv_lookup_var genv pos v in
         let vars, _ = dest_exists_type genv pos ty in
         let len = List.length vars in
            if i < 0 || i >= len then
               raise (FirException (pos, StringIntError ("index is out of range", i)))

      (*
       * BUG: not sure about these yet.
       *)
    | TyCase _
    | TyObject _ ->
         raise (FirException (pos, StringTypeError ("object types are not implemented", ty)))

      (*
       * We don't allow TyDelayed anywhere.
       *)
    | TyDelayed ->
         raise (FirException (pos, StringError "illegal TyDelayed"))

and check_union_type genv pos v tyl s =
   let pos = string_pos "check_union_type" pos in
   let tydef = genv_lookup_type genv pos v in
   let vars, ty_cases = dest_union_tydef pos tydef in
   let len0 = List.length ty_cases in
   let len1 = List.length vars in
   let len2 = List.length tyl in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
(*
 * BUG: this will fail for naml exceptions.
 * The real answer is to implement ExnUnion, but in the
 * meantime we allow the set to include values outside
 * the number of cases in the union.
      else if not (int_set_bounded s len0) then
         raise (FirException (pos, StringIntError ("union set is not bounded", len0)))
 *)
      else
         check_type_list genv SmallType pos tyl

and check_type_list genv smallp pos tyl =
   List.iter (check_type genv smallp pos) tyl

and check_mutable_type_list genv smallp pos tyl =
   List.iter (fun (ty, _) -> check_type genv smallp pos ty) tyl

(*
 * Type definitions.
 *)
let check_tydef genv pos ty_var tydef =
   match tydef with
      TyDefUnion (vars, fields) ->
         let genv = genv_add_poly genv vars in
            List.iter (fun subfields ->
                  List.iter (fun (ty, _) ->
                        check_type genv AnyType pos ty) subfields) fields
    | TyDefLambda (vars, ty) ->
         let genv = genv_add_poly genv vars in
            check_type genv AnyType pos ty
    | TyDefDTuple v ->
         (* Make sure the variable is the same as the name *)
         if not (Symbol.eq v ty_var) then
            raise (FirException (pos, StringVarError ("TyDefTuple var should be the same as the name", v)))

(*
 * Wrappers set the position.
 *)
let check_type genv smallp pos ty =
   let pos = type_pos ty (string_pos "check_type" pos) in
      check_type genv smallp pos ty

let check_type_list genv smallp pos tyl =
   let pos = string_pos "check_type_list" pos in
      List.iter (check_type genv smallp pos) tyl

let check_union_type genv pos v tyl s =
   let pos = string_pos "check_union_type" pos in
      check_union_type genv pos v tyl s

let check_tydef genv pos ty_var tydef =
   let pos = string_pos "check_tydef" pos in
      check_tydef genv pos ty_var tydef

(************************************************************************
 * TYPE EQUALITY
 ************************************************************************)

(*
 * Compare two types for equality.
 * Note that this _does not_ check that the types are
 * well-formed.
 *)
let check_types genv pos ty1 ty2 =
   let pos = string_pos "check_types" pos in
   let type_error ty1' ty2' =
      raise (FirException (pos, TypeError4 (ty1, ty2, ty1', ty2')))
   in
   let rec check_types genv ty1 ty2 =
      match ty1, ty2 with
         TyInt, TyInt
       | TyRawData, TyRawData ->
            ()
       | TyEnum n1, TyEnum n2 ->
            if n1 <> n2 then
               type_error ty1 ty2
       | TyRawInt (pre1, signed1), TyRawInt (pre2, signed2) ->
            if pre1 <> pre2 || signed1 <> signed2 then
               type_error ty1 ty2
       | TyFloat pre1, TyFloat pre2 ->
            if pre1 <> pre2 then
               type_error ty1 ty2
       | TyFun (ty_args1, ty_res1), TyFun (ty_args2, ty_res2) ->
            check_type_lists genv ty1 ty2 ty_args1 ty_args2;
            check_types genv ty_res1 ty_res2
       | TyUnion (v1, tyl1, s1), TyUnion (v2, tyl2, s2) ->
            if not (IntSet.equal s1 s2) then
               type_error ty1 ty2
            else
               (*
                * BUG: this method for checking recursive types is
                * probably too weak.  This code assumes that the
                * type definitions for v1 and v2 have to be exactly
                * the same, and they take arguments in the same order,
                * which is not really necessary.
                *)
               let () = check_type_lists genv ty1 ty2 tyl1 tyl2 in
                  if not (Symbol.eq v1 v2 || genv_equal_norm genv v1 v2) then
                     let genv = genv_add_norm genv v1 v2 in
                        check_mutable_type_lists_lists genv ty1 ty2 (**)
                           (apply_union genv pos v1 tyl1)
                           (apply_union genv pos v2 tyl2)
       | TyTuple (tc1, tyl1), TyTuple (tc2, tyl2) ->
            if tc1 <> tc2 then
               type_error ty1 ty2;
            check_mutable_type_lists genv ty1 ty2 tyl1 tyl2
       | TyDTuple (ty_var1, None), TyDTuple (ty_var2, None) ->
            if not (Symbol.eq ty_var1 ty_var2) then
               type_error ty1 ty2
       | TyDTuple (ty_var1, Some tyl1), TyDTuple (ty_var2, Some tyl2)
       | TyTag (ty_var1, tyl1), TyTag (ty_var2, tyl2) ->
            if not (Symbol.eq ty_var1 ty_var2) then
               type_error ty1 ty2;
            check_mutable_type_lists genv ty1 ty2 tyl1 tyl2
       | TyArray ty1, TyArray ty2 ->
            check_types genv ty1 ty2
       | TyPointer sub1, TyPointer sub2 ->
            if sub1 <> sub2 then
               type_error ty1 ty2
       | TyFrame (v1, tyl1), TyFrame (v2, tyl2) ->
            if not (Symbol.eq v1 v2) then
               type_error ty1 ty2;
            check_type_lists genv ty1 ty2 tyl1 tyl2

         (* Variables and type application *)
       | TyVar v1, TyVar v2 ->
            (*
             * BUG: are we _really_ sure it is always safe to
             * allow exact equality on vars?  The reasoning is that
             * two different programs will never share variables, so
             * the exact equality check will always fail.
             *)
            if not (Symbol.eq v1 v2 || genv_equal_alpha genv v1 v2) then
               type_error ty1 ty2
       | TyApply (v1, tyl1), TyApply (v2, tyl2) ->
            if Symbol.eq v1 v2 then
               check_type_lists genv ty1 ty2 tyl1 tyl2
            else
               check_types genv (apply_type genv pos v1 tyl1) ty2
       | TyApply (v1, tyl1), _ ->
            check_types genv (apply_type genv pos v1 tyl1) ty2
       | _, TyApply (v2, tyl2) ->
            check_types genv ty1 (apply_type genv pos v2 tyl2)

         (* We can always strip trivial quantifiers *)
       | TyAll ([], ty1), ty2
       | TyExists ([], ty1), ty2
       | ty1, TyAll ([], ty2)
       | ty1, TyExists ([], ty2) ->
            check_types genv ty1 ty2

         (* Polymorphism *)
       | TyExists (vars1, ty1), TyExists (vars2, ty2)
       | TyAll (vars1, ty1), TyAll (vars2, ty2) ->
            let len1 = List.length vars1 in
            let len2 = List.length vars2 in
               if len1 = len2 then
                  let genv = genv_add_alpha genv pos vars1 vars2 in
                     check_types genv ty1 ty2
               else
                  type_error ty1 ty2
       | TyProject (v1, i1), TyProject (v2, i2) ->
            (*
             * BUG: we want to handle equal variables.
             * For example, if we have "let v1 = v2 in e" then
             * v1.0 and v2.0 refer to the same type.
             *)
            if not (Symbol.eq v1 v2 && i1 = i2) then
               type_error ty1 ty2

         (* Objects *)
       | TyCase _, _
       | TyObject _, TyObject _ ->
            raise (FirException (pos, NotImplemented "type checking for objects"))

       | _ ->
            type_error ty1 ty2

    and check_type_lists genv ty1 ty2 tyl1 tyl2 =
       let len1 = List.length tyl1 in
       let len2 = List.length tyl2 in
          if len1 <> len2 then
             type_error ty1 ty2;
          List.iter2 (check_types genv) tyl1 tyl2

    and check_mutable_type_lists genv ty1 ty2 fields1 fields2 =
       let len1 = List.length fields1 in
       let len2 = List.length fields2 in
          if len1 <> len2 then
             type_error ty1 ty2;
          List.iter2 (fun (ty1', b1) (ty2', b2) ->
                if b1 <> b2 then
                   type_error ty1 ty2;
                check_types genv ty1' ty2') fields1 fields2

    and check_mutable_type_lists_lists genv ty1 ty2 fields1 fields2 =
       let len1 = List.length fields1 in
       let len2 = List.length fields2 in
          if len1 <> len2 then
             type_error ty1 ty2;
          List.iter2 (fun fields1' fields2' ->
             check_mutable_type_lists genv ty1 ty2 fields1' fields2') fields1 fields2
    in
       check_types genv ty1 ty2

(*
 * Check a list of types.
 *)
let check_type_lists genv pos tyl1 tyl2 args =
   let pos = string_pos "check_type_lists" pos in
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
   let len3 = List.length args in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      Mc_list_util.iter3 (fun ty1 ty2 a ->
            let pos = atom_pos a pos in
               check_types genv pos ty1 ty2) tyl1 tyl2 args

(*
 * These type have mutable flags.
 *)
let check_mutable_type_lists genv pos tyl1 tyl2 args =
   let pos = string_pos "check_mutable_type_lists" pos in
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
   let len3 = List.length args in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      Mc_list_util.iter3 (fun (ty1, b1) (ty2, b2) a ->
            let pos = atom_pos a pos in
               if b1 <> b2 then
                  raise (FirException (pos, StringError "mutable flags do not match"));
               check_types genv pos ty1 ty2) tyl1 tyl2 args

(*
 * The first field list is an option.
 *)
let check_dtuple_type_lists genv pos fields1 fields2 =
   let pos = string_pos "check_dtuple_type_lists" pos in
      match fields1 with
         None ->
            ()
       | Some fields1 ->
            let len1 = List.length fields1 in
            let len2 = List.length fields2 in
               if len1 <> len2 then
                  raise (FirException (pos, ArityMismatch (len1, len2)));
               List.iter2 (fun (ty1, b1) (ty2, b2) ->
                     if b1 <> b2 then
                        raise (FirException (pos, StringError "mutable flags do not match"));
                     check_types genv pos ty1 ty2) fields1 fields2

(*
 * The second type list is a list of options.
 *)
let check_type_opt_lists genv pos tyl1 tyl2 =
   let pos = string_pos "check_type_lists" pos in
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      List.iter2 (fun ty1 ty2 ->
            match ty2 with
               Some ty2 ->
                  check_types genv pos ty1 ty2
             | None ->
                  ()) tyl1 tyl2

(*
 * Some special cases.
 *)
let check_subscript_type genv pos ty =
   let pos = string_pos "check_subscript_type" pos in
      check_types genv pos ty_subscript ty

let check_rawdata_type genv pos ty =
   let pos = string_pos "check_rawdata_type" pos in
      check_types genv pos TyRawData ty

let check_pointer_type genv pos ty =
   let pos = string_pos "check_pointer_type" pos in
      if not (is_pointer_type genv pos ty) then
         raise (FirException (pos, StringTypeError ("not a pointer type", ty)))

(************************************************************************
 * ATOM TYPING
 ************************************************************************)

(*
 * Get the type of an atom.
 *)
let rec check_atom genv pos a =
   let pos = string_pos "check_atom" pos in
      match a with
         AtomNil ty ->
            ty
       | AtomInt _ ->
            TyInt
       | AtomEnum (n, _) ->
            TyEnum n
       | AtomFloat x ->
            TyFloat (Rawfloat.precision x)
       | AtomRawInt i ->
            TyRawInt (Rawint.precision i, Rawint.signed i)
       | AtomVar v ->
            genv_lookup_var genv pos v
       | AtomFun v ->
            genv_lookup_fun genv pos v
       | AtomLabel ((frame, field, subfield), off) ->
            check_label genv pos frame field subfield off
       | AtomSizeof (frames, _) ->
            check_sizeof genv pos frames
       | AtomConst (ty, tv, i) ->
            check_const genv pos ty tv i
       | AtomTyApply (a, ty, tyl) ->
            check_ty_apply genv pos a ty tyl
       | AtomTyPack (v, ty, tyl) ->
            check_ty_pack genv pos v ty tyl
       | AtomTyUnpack v ->
            check_ty_unpack genv pos v
       | AtomUnop (op, a) ->
            check_unop genv pos op a
       | AtomBinop (op, a1, a2) ->
            check_binop genv pos op a1 a2

(*
 * A frame has to subscripted by a label.
 *)
and check_label genv pos v_frame v_field v_subfield off =
   let pos = string_pos "check_label" pos in
   let _, frame = genv_lookup_frame genv pos v_frame in

   (* Check the field *)
   let subfields =
      try SymbolTable.find frame v_field with
         Not_found ->
            raise (FirException (pos, StringVarError ("frame field is not defined", v_field)))
   in

   (* Check the subfield *)
   let size =
      let rec search = function
         (v_subfield', _, size) :: subfields ->
            if Symbol.eq v_subfield' v_subfield then
               size
            else
               search subfields
       | [] ->
            raise (FirException (pos, StringVarError ("subfield is not defined", v_subfield)))
      in
         search subfields
   in

   (* Check the offset *)
   let off = Rawint.to_int off in
      if off < 0 || off >= size then
         raise (FirException (pos, StringIntError ("subfield offset is out of range", off)));
      ty_subscript

(*
 * Sizeof vars have to be frames.
 *)
and check_sizeof genv pos frames =
   let pos = string_pos "check_sizeof" pos in
      List.iter (fun v ->
            if not (genv_mem_frame genv v) then
               raise (FirException (pos, StringVarError ("not a frame", v)))) frames;
      ty_subscript

(*
 * Union constructor has to match union definition.
 *)
and check_const genv pos ty tv i =
   let pos = string_pos "check_const" pos in
   let tv', tl, _ = dest_union_type genv pos ty in
   let _ =
      if not (Symbol.eq tv tv') then
         raise (FirException (pos, StringError "malformed atom"))
   in
   let ty' = TyUnion (tv, tl, IntSet.of_point i) in
      check_type genv SmallType pos ty;
      check_types genv pos ty ty';
      ty

(*
 * Type application, the variable should be instantiated at the
 * given type.  We allow partial applications.
 *)
and check_ty_apply genv pos a ty tyl =
   let pos = string_pos "check_ty_apply" pos in
   let ty' = check_atom genv pos a in
   let ty' = check_all_apply genv pos ty' tyl in
      check_type genv SmallType pos ty;
      check_type_list genv SmallType pos tyl;
      check_types genv pos ty ty';
      ty

and check_all_apply genv pos ty tyl =
   let pos = string_pos "check_all_apply" pos in
(*
 * I'd put the following in a guard, but I don't know the appropriate
 * flag, so I'll just comment it out and make JDS happy. --emre
   let _ = eprintf "@[<hv 3>check_all_apply:@ %a@]@." pp_print_type ty in
*)
   let ty_vars, ty = dest_all_type genv pos ty in
   let len1 = List.length ty_vars in
   let len2 = List.length tyl in
   let _ =
      if len2 > len1 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
   in
   let ty_vars1, ty_vars2 = Mc_list_util.split len2 ty_vars in
   let ty = subst_type_simul pos ty ty_vars1 tyl in
      TyAll (ty_vars2, ty)

(*
 * Type packing.
 *)
and check_ty_pack genv pos v ty tyl =
   let pos = string_pos "check_ty_pack" pos in

   (* Get the unpacked type *)
   let ty_vars, ty' = dest_exists_type genv pos ty in
   let ty' = subst_type_simul pos ty' ty_vars tyl in

   (* Check that it matches the type of the variable *)
   let ty'' = genv_lookup_var genv pos v in
      check_type genv SmallType pos ty;
      check_type_list genv SmallType pos tyl;
      check_types genv pos ty' ty'';
      ty

(*
 * Type unpacking.
 *)
and check_ty_unpack genv pos v =
   let pos = string_pos "check_ty_unpack" pos in
   let ty = genv_lookup_var genv pos v in
   let ty_vars, ty = dest_exists_type genv pos ty in
   let subst, i =
      List.fold_left (fun (subst, i) v' ->
            let subst = subst_add_type_var subst v' (TyProject (v, i)) in
               subst, succ i) (subst_empty, 0) ty_vars
   in
      subst_type subst ty

(*
 * Arithmetic.
 *)
and check_unop genv pos op a =
   let pos = string_pos "check_unop" pos in
   let ty_arg = check_atom genv pos a in
   let ty_dst, ty_src = type_of_unop pos op in
      check_types genv pos ty_src ty_arg;
      ty_dst

and check_binop genv pos op a1 a2 =
   let pos = string_pos "check_binop" pos in
   let ty_arg1 = check_atom genv pos a1 in
   let ty_arg2 = check_atom genv pos a2 in
   let ty_dst, ty_src1, ty_src2 = type_of_binop pos op in
      check_types genv pos ty_src1 ty_arg1;
      check_types genv pos ty_src2 ty_arg2;
      ty_dst

(************************************************************************
 * EXPRESSION WELL-FORMEDNESS
 ************************************************************************)

(*
 * These are all the external functions we know about.
 * BUG: we probably want to read the types from the runtime,
 * rather than hardcoding them here.
 *)
let ty_aml_float = TyFloat Rawfloat.Double
let ty_box_float = TyTuple (BoxTuple, [ty_aml_float, Immutable])

let type_of_ext pos s =
   let pos = string_pos "type_of_ext" pos in
      match s with
         "fc_print_string" ->
            false, TyFun ([TyRawData; ty_subscript], ty_unit)
       | "fc_print_string_int" ->
            false, TyFun ([TyRawData; ty_subscript; ty_int32], ty_unit)
       | "fc_print_string_hex" ->
            false, TyFun ([TyRawData; ty_subscript; ty_int32], ty_unit)
       | "fc_print_string_long_hex" ->
            false, TyFun ([TyRawData; ty_subscript; ty_int64], ty_unit)
       | "fc_flush" ->
            false, TyFun ([], ty_unit)
       | "fc_atoi" ->
            false, TyFun ([TyRawData; ty_subscript], ty_int32)
       | "fc_atof" ->
            false, TyFun ([TyRawData; ty_subscript], ty_float64)
       | "fc_print_char" ->
            false, TyFun ([ty_int8], ty_unit)
       | "fc_print_int"
       | "fc_print_hex"
       | "fc_exit" ->
            false, TyFun ([ty_int32], ty_unit)
       | "fc_print_long"
       | "fc_print_long_hex" ->
            false, TyFun ([ty_int64], ty_unit)
       | "fc_print_float" ->
            false, TyFun ([ty_float32], ty_unit)
       | "fc_print_double" ->
            false, TyFun ([ty_float64], ty_unit)
       | "fc_print_long_double" ->
            false, TyFun ([ty_float80], ty_unit)
       | "fc_atomic_levels" ->
            false, TyFun ([], ty_int32)
       | "fc_utime" ->
            false, TyFun ([], ty_uint64)
       | "fc_fpitest_msg" ->
            false, TyFun ([TyRawData; ty_subscript], ty_unit)
       | "fc_fpitest_data" ->
            false, TyFun ([TyRawData; ty_subscript], ty_unit)
       | "fc_fpitest_check_true" ->
            false, TyFun ([TyRawData; ty_subscript; ty_int32], ty_unit)
       | "fc_fpitest_check_false" ->
            false, TyFun ([TyRawData; ty_subscript; ty_int32], ty_unit)
       | "fc_fptest_check_tol_float" ->
            false, TyFun ([TyRawData; ty_subscript; ty_float32; ty_float32], ty_unit)
       | "fc_fptest_check_tol_double" ->
            false, TyFun ([TyRawData; ty_subscript; ty_float64; ty_float64], ty_unit)
       | "fc_fptest_check_tol_long_double" ->
            false, TyFun ([TyRawData; ty_subscript; ty_float80; ty_float80], ty_unit)
       | "fc_gc" ->
            true, TyFun ([ty_int32], ty_int32)
       | "ml_cmp" ->
            false, TyFun ([TyInt; TyInt], TyInt)
       | "ml_print_int"
       | "aml_print_int" ->
            false, TyFun ([TyInt], ty_unit)
       | "ml_print_string" ->
            false, TyFun ([ty_ml_string], ty_unit)
       | "aml_print_string" ->
            false, TyFun ([TyRawData], ty_unit)
       | "aml_exit" ->
            let v = new_symbol_string "a" in
               false, TyAll ([v], TyFun ([TyInt], TyVar v))
       | "aml_string_equal" ->
            false, TyFun ([TyRawData; TyRawData], ty_bool)
       | "aml_strcat" ->
            false, TyFun ([TyRawData; TyRawData], TyRawData)
       | "aml_equal"
       | "aml_nequal"
       | "aml_lt"
       | "aml_le"
       | "aml_gt"
       | "aml_ge" ->
            let v = new_symbol_string "a" in
            let ty_v = TyVar v in
               false, TyAll ([v], TyFun ([ty_v; ty_v], ty_bool))
       | "aml_compare" ->
            let v = new_symbol_string "a" in
            let ty_v = TyVar v in
               false, TyAll ([v], TyFun ([ty_v; ty_v], TyInt))
       | "aml_string_of_int" ->
            false, TyFun ([TyInt], TyRawData)
       | "aml_int_of_string" ->
            false, TyFun ([TyRawData], TyInt)
       | "aml_string_of_float" ->
            false, TyFun ([ty_aml_float], TyRawData)
       | "aml_float_of_string" ->
            false, TyFun ([TyRawData], ty_aml_float)
       | "aml_frexp" ->
            false, TyFun ([ty_aml_float], TyTuple (NormalTuple, [ty_box_float, Immutable; TyInt, Immutable]))
       | "aml_fmodf" ->
            false, TyFun ([ty_aml_float], TyTuple (NormalTuple, [ty_box_float, Immutable; ty_box_float, Immutable]))
       | _ ->
            raise (FirException (pos, StringError ("unknown external function:" ^ s)))

(*
 * Type checking for expressions.
 *)
let rec check_exp genv e =
   let pos = string_pos "check_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            check_atom_exp genv pos v ty a e
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            check_ext_exp genv pos v ty1 s b ty2 ty_args args e
       | TailCall (_, f, args) ->
            check_tailcall_exp genv pos f args
       | SpecialCall (_, op) ->
            check_special_call_exp genv pos op
       | Match (a, cases) ->
            check_match_exp genv pos a cases
       | MatchDTuple (a, cases) ->
            check_match_dtuple_exp genv pos a cases
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            check_typecase_exp genv pos a1 a2 v1 v2 e1 e2
       | LetAlloc (v, op, e) ->
            check_alloc_exp genv pos v op e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            check_subscript_exp genv pos op v1 ty v2 a e
       | SetSubscript (op, _, v1, a1, ty, a2, e) ->
            check_set_subscript_exp genv pos op v1 a1 ty a2 e
       | LetGlobal (op, v, ty, l, e) ->
            check_global_exp genv pos op v ty l e
       | SetGlobal (op, _, v, ty, a, e) ->
            check_set_global_exp genv pos op v ty a e
       | Memcpy (op, _, v1, a1, v2, a2, a3, e) ->
            check_memcpy_exp genv pos op v1 a1 v2 a2 a3 e
       | Assert (_, pred, e) ->
            check_assert_exp genv pos pred e
       | Call (_, f, args, e) ->
            check_call_exp genv pos f args e
       | Debug (_, e) ->
            check_exp genv e

(*
 * Check an atom.
 * We have to check the argument and return type.
 *)
and check_atom_exp genv pos v ty a e =
   let pos = string_pos "check_unop_exp" pos in
   let ty_arg = check_atom genv pos a in
      check_type genv AnyType pos ty;
      check_types genv pos ty ty_arg;
      check_exp (genv_add_var genv v ty) e

(*
 * Global lookup.
 *)
and check_global_exp genv pos op v ty l e =
   let pos = string_pos "check_global_exp" pos in
   let ty_glob = genv_lookup_global genv pos l in
      check_type genv AnyType pos ty;
      check_types genv pos ty ty_glob;
      check_exp (genv_add_var genv v ty) e

(*
 * External operation.
 *)
and check_ext_exp genv pos v ty1 s b ty2 tyl args e =
   let pos = string_pos "check_ext_exp" pos in
   let b_ext, ty_ext = type_of_ext pos s in
   let ty_args1 = List.map (check_atom genv pos) args in
   let ty_fun = check_all_apply genv pos ty_ext tyl in
   let ty_params, ty_args2, ty_res = dest_all_fun_type genv pos ty_fun in
      if b <> b_ext then
         raise (FirException (pos, StringError "GC flags do not match"));
      (* Subst type arguments *)
      check_type genv AnyType pos ty1;
      check_types genv pos ty_ext ty2;
      check_type_lists genv pos ty_args2 ty_args1 args;
      check_types genv pos ty1 ty_res;
      check_exp (genv_add_var genv v ty1) e

(*
 * Tailcall.
 *)
and check_tailcall_exp genv pos f args =
   let pos = string_pos "check_tailcall_exp" pos in
   let ty_fun = check_atom genv pos f in
   let ty_vars1, ty_args1, ty_res1 = dest_all_fun_type genv pos ty_fun in
   let ty_args2 =
      List.map (fun a ->
            let pos = atom_pos a pos in
               check_atom genv pos a) args
   in
      if ty_vars1 <> [] then
         raise (FirException (pos, StringTypeError ("polymorphic function", ty_fun)));
      check_type_lists genv pos ty_args1 ty_args2 args;
      check_types genv pos ty_void ty_res1

and check_call_exp genv pos f args e =
   let pos = string_pos "check_call_exp" pos in
   let ty_fun = check_atom genv pos f in
   let ty_vars1, ty_args1, ty_res1 = dest_all_fun_type genv pos ty_fun in
   let ty_args2 =
      List.map (function
         Some a -> Some (check_atom genv pos a)
       | None -> None) args
   in
      if ty_vars1 <> [] then
         raise (FirException (pos, StringTypeError ("polymorphic function", ty_fun)));
      check_type_opt_lists genv pos ty_args1 ty_args2;
      check_types genv pos ty_void ty_res1;
      check_exp genv e

(*
 * Special call.
 *)
and check_tail_sysmigrate_exp genv pos id dst_bas dst_off f args =
   let pos = string_pos "check_tail_sysmigrate_exp" pos in
   let ty_dst_bas = check_atom genv pos dst_bas in
   let ty_dst_off = check_atom genv pos dst_off in
   let ty_fun = check_atom genv pos f in
   let ty_vars1, ty_args1, ty_res1 = dest_all_fun_type genv pos ty_fun in
   let ty_args2 = List.map (check_atom genv pos) args in
      if ty_vars1 <> [] then
         raise (FirException (pos, StringTypeError ("polymorphic function", ty_fun)));
      check_types genv pos TyRawData ty_dst_bas;
      check_types genv pos ty_int32 ty_dst_off;
      check_type_lists genv pos ty_args1 ty_args2 args;
      check_types genv pos ty_void ty_res1

and check_tail_atomic_exp genv pos f c args =
   let pos = string_pos "check_tail_atomic_exp" pos in
   let ty_c = check_atom genv pos c in
   let ty_fun = check_atom genv pos f in
   let ty_vars1, ty_args1, ty_res1 = dest_all_fun_type genv pos ty_fun in
   let ty_args2 = List.map (check_atom genv pos) (c :: args) in
      if ty_vars1 <> [] then
         raise (FirException (pos, StringTypeError ("polymorphic function", ty_fun)));
      check_types genv pos ty_int32 ty_c;
      check_type_lists genv pos ty_args1 ty_args2 (c :: args);
      check_types genv pos ty_void ty_res1

and check_tail_atomicrollback_exp genv pos level c =
   let pos = string_pos "check_tail_atomicrollback_exp" pos in
   let ty_level = check_atom genv pos level in
   let ty_c = check_atom genv pos c in
      check_types genv pos ty_int32 ty_level;
      check_types genv pos ty_int32 ty_c

and check_tail_atomiccommit_exp genv pos level f args =
   let pos = string_pos "check_tail_atomiccommit_exp" pos in
   let ty_level = check_atom genv pos level in
   let ty_fun = check_atom genv pos f in
   let ty_vars1, ty_args1, ty_res1 = dest_all_fun_type genv pos ty_fun in
   let ty_args2 = List.map (check_atom genv pos) args in
      if ty_vars1 <> [] then
         raise (FirException (pos, StringTypeError ("polymorphic function", ty_fun)));
      check_types genv pos ty_int32 ty_level;
      check_type_lists genv pos ty_args1 ty_args2 args;
      check_types genv pos ty_void ty_res1

and check_special_call_exp genv pos op =
   let pos = string_pos "check_special_call_exp" pos in
      match op with
         TailSysMigrate (id, dst_bas, dst_off, f, args) ->
            check_tail_sysmigrate_exp genv pos id dst_bas dst_off f args
       | TailAtomic (f, c, args) ->
            check_tail_atomic_exp genv pos f c args
       | TailAtomicRollback (level, c) ->
            check_tail_atomicrollback_exp genv pos level c
       | TailAtomicCommit (level, f, args) ->
            check_tail_atomiccommit_exp genv pos level f args

(*
 * Match statement.  The only difficult case is for unions,
 * where we have to instantiate the union type with some arguments,
 * and the variable _changes type_ for each case.
 *)
and check_match_exp genv pos a cases =
   let pos = string_pos "check_match_exp" pos in
   let ty_arg = check_atom genv pos a in
      match expand_type genv pos ty_arg, a with
         TyInt, _ ->
            check_match_int genv pos cases
       | TyEnum n, _ ->
            check_match_enum genv pos n cases
       | TyRawInt (pre, signed), _ ->
            check_match_rawint genv pos pre signed cases
       | TyUnion (v, tyl, s), AtomVar v' ->
            check_match_union genv pos v' v tyl s cases
       | _ ->
            raise (FirException (pos, StringTypeError ("unrecognized match", ty_arg)))

(*
 * Dependent tuple matching.
 * We have to check that all the atoms are tags.
 * The body should be well-formed with the forced element types.
 *)
and check_match_dtuple_exp genv pos a cases =
   let pos = string_pos "check_match_dtuple_exp" pos in
   let ty_arg = check_atom genv pos a in
   let ty_var, ty_fields = dest_dtuple_type genv pos ty_arg in
   let defaultp =
      List.fold_left (fun defaultp (_, a_opt, e) ->
            (* The atom (if it exists) should be of a valid TyTag type *)
            let defaultp, ty_fields =
               match a_opt with
                  Some a ->
                     let ty_arg = check_atom genv pos a in
                     let ty_var', ty_fields' = dest_tag_type genv pos ty_arg in
                        if not (Symbol.eq ty_var' ty_var) then
                           raise (FirException (pos, StringVarError ("match case has the wrong type", ty_var')));
                        check_dtuple_type_lists genv pos ty_fields ty_fields';
                        true, Some ty_fields'
                | None ->
                     defaultp, ty_fields
            in

            (* Adjust the atom type *)
            let v = var_of_atom pos a in
            let genv = genv_add_var genv v (TyDTuple (ty_var, ty_fields)) in
               (* Check the body with the new type *)
               check_exp genv e;
               defaultp) false cases
   in
      (* There must be a default case *)
      if not defaultp then
         raise (FirException (pos, StringError "dependent tuple match does not have a default case"))

(*
 * BUG: None of the four functions below checks that the union of the sets
 * in the cases covers all possible values of the type in question.  In the
 * case of unions, the union cannot be a void type, and this is also not
 * enforced.
 *
 * --emre
 *)

and check_match_int genv pos cases =
   let pos = string_pos "check_match_int" pos in
      List.iter (fun (_, s, e) ->
            match s with
               IntSet _ ->
                  check_exp genv e
             | RawIntSet _ ->
                  raise (FirException (pos, StringError "RawInt case inside a Int match"))) cases

and check_match_enum genv pos n cases =
   let pos = string_pos "check_match_enum" pos in
      List.iter (fun (_, s, e) ->
            match s with
               IntSet s ->
(* NOTE: we allow the set to match more values than the enum can have
                  if not (int_set_bounded s n) then
                     raise (FirException (pos, StringError "case is out of range"));
*)
                  check_exp genv e
             | RawIntSet _ ->
                  raise (FirException (pos, StringError "RawInt case inside an Enum match"))) cases

and check_match_rawint genv pos pre signed cases =
   let pos = string_pos "check_match_rawint" pos in
      List.iter (fun (_, s, e) ->
            match s with
               IntSet _ ->
                  raise (FirException (pos, StringError "Int case inside a RawInt match"))
             | RawIntSet s ->
                  let pre' = RawIntSet.precision s in
                  let signed' = RawIntSet.signed s in
                     if pre' <> pre || signed' <> signed then
                        raise (FirException (pos, StringError "RawInt precision does not match"));
                     check_exp genv e) cases

and check_match_union genv pos v ty_v ty_args s cases =
   let pos = string_pos "check_match_union" pos in
      check_type_list genv SmallType pos ty_args;
      check_union_type genv pos ty_v ty_args s;
      List.iter (fun (_, s', e) ->
            let s =
               match s' with
                  IntSet s' ->
                     IntSet.isect s s'
                | RawIntSet _ ->
                     raise (FirException (pos, StringError "RawInt case inside a union match"))
            in
            let genv = genv_add_var genv v (TyUnion (ty_v, ty_args, s)) in
               check_exp genv e) cases

(*
 * BUG: type case is not checked.
 *)
and check_typecase_exp genv pos a1 a2 v1 v2 e1 e2 =
   let pos = string_pos "check_typecase_exp" pos in
      raise (FirException (pos, NotImplemented "object types"))

(*
 * Allocation.  Perform a case analysis on the allocation
 * type, and also check the types for well-formedness to catch
 * allocations of bogus types.
 *)
and check_alloc_exp genv pos v op e =
   let pos = string_pos "check_alloc_exp" pos in
   let ty = type_of_alloc_op genv pos op in
      check_type genv AnyType pos ty;
      check_exp (genv_add_var genv v ty) e

(*
 * Get the type of the allocation operator.
 *)
and type_of_alloc_op genv pos op =
   let pos = string_pos "type_of_alloc_op" pos in
      match op with
         AllocTuple (tc, ty_vars, ty, args) ->
            type_of_alloc_tuple genv pos ty_vars ty tc args
       | AllocDTuple (ty, ty_var, tag, args) ->
            type_of_alloc_dtuple genv pos ty ty_var tag args
       | AllocUnion (ty_vars, ty, tv, i, args) ->
            type_of_alloc_union genv pos ty_vars ty tv i args
       | AllocArray (ty, args) ->
            type_of_alloc_array genv pos ty args
       | AllocVArray (ty, sub, a1, a2) ->
            type_of_alloc_varray genv pos ty sub a1 a2
       | AllocMalloc (ty, a) ->
            type_of_alloc_malloc genv pos ty a
       | AllocFrame (v, tyl) ->
            TyFrame (v, tyl)

(*
 * Tuple allocation checks that the tuple arguments
 * are defined in the tuple type.  Note, type wf is
 * checked in check_alloc_exp.
 *)
and type_of_alloc_tuple genv pos ty_vars ty tc args =
   let pos = string_pos "type_of_alloc_tuple" pos in
   let genv = genv_add_poly genv ty_vars in
   let ty_args = List.map (check_atom genv pos) args in
   let fields = dest_tuple_type genv pos ty in
   let ty_args' = List.map fst fields in
      check_type_lists genv pos ty_args' ty_args args;
      TyAll (ty_vars, ty)

(*
 * Union allocation.  We have to make sure the union is defined with
 * the right type arguments.  The result type specifies the union type
 * arguments, so we break it apart and instantiate it.  Note, type wf is
 * checked in check_alloc_exp.
 *)
and type_of_alloc_union genv pos ty_params ty tv i args =
   let pos = string_pos "type_of_alloc_union" pos in
   let genv = genv_add_poly genv ty_params in
   let ty_args = List.map (check_atom genv pos) args in

   (* Figure out what is produced *)
   let tv', ty_vars, s = dest_union_type genv pos ty in
   let fields = apply_union genv pos tv' ty_vars in
   let len = List.length fields in
      if not (Symbol.eq tv tv' && i >= 0 && i < len && IntSet.mem_point i s) then
         raise (FirException (pos, StringTypeError ("union construction type error", ty)));
      check_type_lists genv pos (List.map fst (List.nth fields i)) ty_args args;
      TyAll (ty_params, ty)

(*
 * Dependent tuple allocation.
 * Checks that the tuple arguments are defined in the tuple type,
 * and check that the tag has the right type.  Note, type wf is
 * checked in check_alloc_exp.
 *)
and type_of_alloc_dtuple genv pos ty ty_var tag args =
   let pos = string_pos "type_of_alloc_dtuple" pos in

   (* Get the result type *)
   let ty_var1, ty_fields1_opt = dest_dtuple_type genv pos ty in

   (* Get the tag type *)
   let ty_tag = check_atom genv pos tag in
   let ty_var2, ty_fields2 = dest_tag_type genv pos ty_tag in

   (* The type variables must match, and they should be a dependent tuple *)
   let () =
      if not (Symbol.eq ty_var1 ty_var2) then
         raise (FirException (pos, StringVarError ("tag has the wrong type", ty_var2)));
      if not (is_dtuple_tydef genv pos ty_var1) then
         raise (FirException (pos, StringTypeError ("type is not a dependent tuple type", ty)))
   in

   (* If the result has some fields, they must match *)
   let () =
      match ty_fields1_opt with
         Some ty_fields1 ->
            check_mutable_type_lists genv pos ty_fields1 ty_fields2 args
       | None ->
            ()
   in

   (* Check the argument types *)
   let ty_args1 = List.map fst ty_fields2 in
   let ty_args2 = List.map (check_atom genv pos) args in
      check_type_lists genv pos ty_args1 ty_args2 args;
      ty

(*
 * Array allocation allocates a tuple of elements of the same type.
 * Note, type wf is checked in check_alloc_exp.
 *)
and type_of_alloc_array genv pos ty args =
   let pos = string_pos "type_of_alloc_array" pos in
   let ty_elem = dest_array_type genv pos ty in
      List.iter (fun a -> check_types genv pos ty_elem (check_atom genv pos a)) args;
      ty

(*
 * Variable-sized array.  All elements are initialized with the same value.
 * Note, type wf is checked in check_alloc_exp.
 *)
and type_of_alloc_varray genv pos ty sub a1 a2 =
   let pos = string_pos "type_of_alloc_varray" pos in
      check_types genv pos ty_subscript (check_atom genv pos a1);
      check_types genv pos ty (TyArray (check_atom genv pos a2));
      ty

(*
 * Malloc always returns RawData.
 * Note, type wf is checked in check_alloc_exp.
 *)
and type_of_alloc_malloc genv pos ty a =
   let pos = string_pos "type_of_alloc_malloc" pos in
      check_types genv pos ty TyRawData;
      check_types genv pos ty_subscript (check_atom genv pos a);
      ty

(*
 * Subscripting.
 *)
and check_subscript_exp genv pos op v1 ty a2 a3 e =
   let pos = string_pos "check_subscript_exp" pos in
   let ty_block = check_atom genv pos a2 in
   let ty_elem = check_subscript genv pos false ty_block ty a3 in
      check_type genv AnyType pos ty;
      check_types genv pos ty ty_elem;
      check_exp (genv_add_var genv v1 ty) e

and check_set_subscript_exp genv pos op a1 a2 ty a3 e =
   let pos = string_pos "check_set_subscript_exp" pos in
   let ty_block = check_atom genv pos a1 in
   let ty_elem = check_subscript genv pos true ty_block ty a2 in
   let ty_value = check_atom genv pos a3 in
      check_type genv AnyType pos ty;
      check_types genv pos ty_elem ty;
      check_types genv pos ty ty_value;
      check_exp genv e

(*
 * This is the generic subscripting checker.
 * It returns the type of the element.
 *)
and check_subscript genv pos mutatep ty_block ty_elem a =
   let pos = string_pos "check_subscript" pos in
      match expand_type genv pos ty_block with
         TyTuple (_, tyl)
       | TyDTuple (_, Some tyl) ->
            check_tuple_subscript genv pos mutatep tyl a
       | TyUnion (tv, ty_args, s) ->
            check_union_subscript genv pos mutatep tv ty_args s a
       | TyArray ty ->
            check_array_subscript genv pos ty a
       | TyFrame (v, tyl) ->
            check_frame_subscript genv pos v tyl ty_elem a
       | TyRawData ->
            check_rawdata_subscript genv pos ty_elem a
       | ty ->
            raise (FirException (pos, StringTypeError ("value can't be subscripted", ty)))

(*
 * For tuple subscripts, the index must be a constant.
 * If the index is raw, then it is a byte offset.  If the
 * index is int, then it is a word subscript.
 *)
and check_tuple_subscript genv pos mutatep fields a =
   let pos = string_pos "check_tuple_subscript" pos in
   let i = index_of_subscript pos a in
   let _ =
      if i < 0 || i >= List.length fields then
         raise (FirException (pos, StringIntError ("subscript is out of bounds", i)))
   in
   let ty, mutablep = List.nth fields i in
   let error =
      (* The flag is true iff there is a mutability error *)
      match mutablep with
         Mutable -> false
       | Immutable -> mutatep
       | MutableDelayed
       | MutableVar _ -> true
   in
   let _ =
      if error then
         raise (FirException (pos, StringIntError ("field is not mutable", i)))
   in
      ty

(*
 * Check a union subscript.
 * The union has to be specialized to a single case, so that
 * we know which case it is.  The index must be a constant.
 *)
and check_union_subscript genv pos mutatep tv ty_args s a =
   let pos = string_pos "check_union_subscript" pos in
   let i = index_of_subscript pos a in

   (* Get the union tag *)
   let j =
      if not (IntSet.is_singleton s) then
         raise (FirException (pos, StringError "ambiguous union subscript"));
      IntSet.dest_singleton s
   in

   (* Get the union fields for the tag *)
   let ty_fields = apply_union genv pos tv ty_args in
   let ty_fields =
      if j < 0 || j >= List.length ty_fields then
         raise (FirException (pos, StringError "union case is out of range"));
      List.nth ty_fields j
   in
   let _ =
      if i < 0 || i >= List.length ty_fields then
         raise (FirException (pos, StringIntError ("subscript is out of bounds", i)))
   in
   let ty, mutablep = List.nth ty_fields i in
   let error =
      (* The flag is true iff there is a mutability error *)
      match mutablep with
         Mutable -> false
       | Immutable -> mutatep
       | MutableDelayed
       | MutableVar _ -> true
   in
   let _ =
      if error then
         raise (FirException (pos, StringIntError ("field is not mutable", i)))
   in
      ty

(*
 * Arrays can be subscripted with variable inidices.
 * It is not a _type error_ to have an index that is out-of-bounds.
 *)
and check_array_subscript genv pos ty a =
   let pos = string_pos "check_array_subscript" pos in
   let ty_index = check_atom genv pos a in
      match expand_type genv pos ty_index with
         TyInt
       | TyEnum _
       | TyRawInt _ ->
            ty
       | ty ->
            raise (FirException (pos, StringTypeError ("subscript is not an integer", ty)))

(*
 * A frame has to be subscripted by a label.
 *)
and check_frame_subscript genv pos v tyl ty_elem a =
   let pos = string_pos "check_frame_subscript" pos in
   let ty_vars, frame = genv_lookup_frame genv pos v in

   (* Index should be a label *)
   let (v_frame, v_field, v_subfield), off =
      match a with
         AtomLabel (field, off) ->
            field, off
       | _ ->
            raise (FirException (pos, StringAtomError ("subscript is not a label", a)))
   in

   (* Check the frame *)
   let _ =
      if not (Symbol.eq v v_frame) then
         raise (FirException (pos, StringVarError ("frame label does not match", v_frame)))
   in

   (* Check the field *)
   let subfields =
      try SymbolTable.find frame v_field with
         Not_found ->
            raise (FirException (pos, StringVarError ("frame field is not defined", v_field)))
   in

   (* Get the subfield *)
   let ty, size =
      let rec search = function
         (v_subfield', ty', i) :: subfields ->
            if Symbol.eq v_subfield' v_subfield then
               subst_type_simul pos ty' ty_vars tyl, i
            else
               search subfields
       | [] ->
            raise (FirException (pos, StringVarError ("subfield is not defined", v_subfield)))
      in
         search subfields
   in

   (* Check the offset *)
   let off = Rawint.to_int off in
      if off < 0 || off >= size then
         raise (FirException (pos, StringIntError ("subfield offset is out of range", off)));
      if off = 0 then
         (* BUG: what types are allowed in RawData? *)
         if is_rawdata_type genv pos ty then
            ty_elem
         else
            ty
      else
         (* BUG: fix this eventually to allow RawData subscripting *)
         raise (FirException (pos, NotImplemented "non-zero frame offsets"))

(*
 * Rawdata subscripting allows us to fetch _any_ kind of value from
 * the Rawdata block.
 *
 * BUG: what types are allowed in RawData?
 *)
and check_rawdata_subscript genv pos ty_elem a =
   let pos = string_pos "check_rawdata_subscript" pos in
   let ty_index = check_atom genv pos a in
      check_types genv pos ty_subscript ty_index;
      ty_elem

(*
 * Memcpy.
 * For now, we only implement RawData copying.
 *)
and check_memcpy_exp genv pos subop v1 a1 v2 a2 a3 e =
   let pos = string_pos "check_memcpy_exp" pos in
   let ty_block1 = check_atom genv pos v1 in
   let ty_block2 = check_atom genv pos v2 in
   let ty_off1 = check_atom genv pos a1 in
   let ty_off2 = check_atom genv pos a2 in
   let ty_size = check_atom genv pos a3 in
      check_types genv pos TyRawData ty_block1;
      check_types genv pos TyRawData ty_block2;
      check_types genv pos ty_subscript ty_off1;
      check_types genv pos ty_subscript ty_off2;
      check_types genv pos ty_subscript ty_size;
      check_exp genv e

(*
 * Global operations.
 *)
and check_set_global_exp genv pos subop v ty a e =
   let pos = string_pos "check_set_global_exp" pos in
   let ty_var = genv_lookup_var genv pos v in
   let ty_atom = check_atom genv pos a in
      check_types genv pos ty_var ty;
      check_types genv pos ty_var ty_atom;
      check_exp genv e

(*
 * Assertions.
 *)
and check_assert_exp genv pos pred e =
   let pos = string_pos "check_assert_exp" pos in
   let _ =
      match pred with
         IsMutable v ->
            check_is_mutable genv pos v
       | Reserve (a1, a2) ->
            check_reserve genv pos a1 a2
       | BoundsCheck (subop, v, a1, a2) ->
            check_bounds genv pos subop v a1 a2
       | ElementCheck (ty, subop, v, a) ->
            check_element genv pos ty subop v a
   in
      check_exp genv e

and check_is_mutable genv pos v =
   let pos = string_pos "check_is_mutable" pos in
   let ty_block = check_atom genv pos v in
      check_pointer_type genv pos ty_block

and check_reserve genv pos a1 a2 =
   let pos = string_pos "check_reserve" pos in
   let ty_heap = check_atom genv pos a1 in
   let ty_ptrs = check_atom genv pos a2 in
      check_subscript_type genv pos ty_heap;
      check_subscript_type genv pos ty_ptrs

and check_bounds genv pos subop v a1 a2 =
   let pos = string_pos "check_bounds" pos in
   let ty_block = check_atom genv pos v in
   let ty_lower = check_atom genv pos a1 in
   let ty_upper = check_atom genv pos a2 in
      check_pointer_type genv pos ty_block;
      check_subscript_type genv pos ty_lower;
      check_subscript_type genv pos ty_upper

and check_element genv pos ty subop v a =
   let pos = string_pos "check_element" pos in
   let ty_block = check_atom genv pos v in
   let ty_index = check_atom genv pos a in
      check_type genv SmallType pos ty;
      check_rawdata_type genv pos ty_block;
      check_subscript_type genv pos ty_index

(*
 * Get the type of an initializer, and show it is well-formed.
 *)
let type_of_init genv pos init =
   match init with
      InitAtom a ->
         check_atom genv pos a
    | InitAlloc op ->
         type_of_alloc_op genv pos op
    | InitTag (ty_var, tyl) ->
         TyTag (ty_var, tyl)
    | InitRawData _ ->
         TyRawData
    | InitNames _ ->
         raise (FirException (pos, NotImplemented "InitNames"))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Check a program.
 * The program _must_ be standardized for the type checker to work,
 * so to be extra safe, we do it here.
 *)
let check_prog_no_standardize prog =
   let _ =
      if !debug_print_fir > 0 then
         debug_prog "Check prog: before checking" prog
   in
   let genv = genv_of_prog prog in
   let { prog_file = { file_class = file_class };
         prog_types = tenv;
         prog_import = import;
         prog_export = export;
         prog_frames = frames;
         prog_names = names;
         prog_globals = globals;
         prog_funs = funs
       } = prog
   in

   (* Check all the types in the type environment *)
   let _ =
      SymbolTable.iter (fun v ty ->
            let pos = string_pos "check_tydef" (var_exp_pos v) in
               check_tydef genv pos v ty) tenv
   in

   (* Check that all the globals have well-formed types *)
   let genv =
      SymbolTable.fold (fun genv v (ty, init) ->
            let pos = string_pos "check_global" (var_exp_pos v) in
            let ty' = type_of_init genv pos init in
               check_type genv AnyType pos ty;
               check_types genv pos ty ty';
               genv_add_var genv v ty) genv globals
   in

   (* Check that all of the imports are well-defined *)
   let genv =
      SymbolTable.fold (fun genv v import ->
            let pos = string_pos "check_import" (var_exp_pos v) in
            let { import_type = ty;
                  import_info = info
                } = import
            in
               (* BUG: have to check import info *)
               check_type genv AnyType pos ty;
               genv_add_var genv v ty) genv import
   in

   (* Check that all the types in the frames are well-formed *)
   let _ =
      SymbolTable.iter (fun v (vars, fields) ->
            let pos = string_pos "check_frame" (var_exp_pos v) in
            let genv = genv_add_poly genv vars in
               SymbolTable.iter (fun _ subfields ->
                     List.iter (fun (_, ty, i) ->
                           check_type genv AnyType pos ty;
                           if i < 0 then
                              raise (FirException (pos, StringIntError ("frame field has negative size", i)))) subfields) fields) frames
   in

   (* Check that all the name records are well-formed *)
   let _ =
      SymbolTable.iter (fun v ty ->
            let pos = string_pos "check_name" (var_exp_pos v) in
               check_type genv AnyType pos ty;
               raise (FirException (pos, NotImplemented "name records")))
   in

   (* Add all the functions to the environment *)
   let genv =
      SymbolTable.fold (fun genv f def ->
            let pos = string_pos "add_fun" (fundef_pos f def) in
            let _, ty_vars, ty, _, _ = def in
            let ty = TyAll (ty_vars, ty) in
               check_type genv AnyType pos ty;
               genv_add_var genv f ty) genv funs
   in

   (*
    * Now check all the functions for real.
    * We know the type is well-formed.
    *)
   let _ =
      SymbolTable.iter (fun f def ->
            let pos = string_pos "check_fun" (fundef_pos f def) in
            let _, ty_vars, ty, vars, e = def in
            let genv = genv_add_poly genv ty_vars in
            let ty_args, ty_res = dest_fun_type genv pos ty in
            let genv = genv_add_vars genv pos vars ty_args in
               check_types genv pos ty_void ty_res;
               check_exp genv e) funs
   in

   (*
    * Check that exports have the right types.  For FC programs,
    * if a ``main'' function is exported, we check that it has the
    * right type.
    *)
   (* BUG:  We really should check ``main'' for other file classes. *)
   let _ =
      SymbolTable.iter (fun v { export_name = name; export_type = ty } ->
            let pos = string_pos "check_export" (var_exp_pos v) in
            let ty' = genv_lookup_var genv pos v in
            let _ = check_types genv pos ty ty' in
               match file_class with
                  FileFC when name = "main" ->
                     let pos = string_pos "check_fc_main" (var_exp_pos v) in
                        check_types genv pos ty ty_fc_main
                | _ ->
                     ()
         ) export
   in
      prog

(*
 * Check the marshal-prog.
 *)
let check_mprog_no_standardize mprog =
   let _ =
      if !debug_print_fir > 0 then
         debug_mprog "Check mprog: before checking" mprog
   in
   let { marshal_fir = prog;
         marshal_globals = order_globals;
         marshal_funs = order_funs;
         marshal_rttd = rttd
       } = mprog
   in
   let prog = check_prog_no_standardize prog in
   let { prog_globals = globals;
         prog_funs = funs
       } = prog
   in

   (* Check that the globals are bound *)
   let _ =
      List.iter (fun v ->
            let pos = string_pos "check_mglobal" (var_exp_pos v) in
               if not (SymbolTable.mem globals v) then
                  raise (FirException (pos, UnboundVar v))) order_globals
   in

   (* Check the the functions are bound *)
   let _ =
      List.iter (fun f ->
            let pos = string_pos "check_mfun" (var_exp_pos f) in
               if not (SymbolTable.mem funs f) then
                  raise (FirException (pos, UnboundVar f))) order_funs
   in
      (* BUG: check the RTTD *)
      { mprog with marshal_fir = prog }

(*
 * The global versions _must_ standardize:
 * the checking assumes a standardized form.
 *)
let check_prog prog =
   let prog = standardize_prog prog in
      check_prog_no_standardize prog

let check_mprog mprog =
   let mprog = standardize_mprog mprog in
      check_mprog_no_standardize mprog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
