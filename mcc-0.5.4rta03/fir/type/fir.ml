(*
 * Functional Intermediate Representation.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Interval_set
open Attribute
open Location

open Fir_set
open Fir_rttd_map

(*
 * This is used to annotate terms with location information.
 *)
type 'core simple_subst = (loc, 'core) simple_term


(*
 * FIR version string
 *)
let fir_version = "$Id: fir.ml,v 1.90 2002/12/23 00:13:15 jyh Exp $"


(*
 * Inherit some types.
 *)
type var     = symbol
type ty_var  = symbol
type label   = symbol


(*
 * Sets of values.
 *)
type int_set = IntSet.t
type rawint_set = RawIntSet.t

type float_precision = Rawfloat.float_precision
type int_precision = Rawint.int_precision
type int_signed = Rawint.int_signed


(*
 * General sets.
 *)
type set =
   IntSet of int_set
 | RawIntSet of rawint_set


(*
 * Types of tuples:
 *
 *    NormalTuple
 *       Each element in the tuple must be a ``smalltype'' -- is, elements
 *       can be polymorphic values or other word- size data, but cannot be
 *       raw data (RawInt or Float).
 *
 *    RawTuple
 *       Can contain pointers and other rawdata.  Requires a slightly
 *       different runtime representation, where the first field in the
 *       tuple contains run-time type information.  Used by Java, where we
 *       have type safety but also raw data within tuples.
 *
 *    BoxTuple
 *       Must be a 1-tuple, but the element can be a value of any type.
 *       Used to box raw data (RawInt and Floats)
 *)
type tuple_class =
   NormalTuple
 | RawTuple
 | BoxTuple


(*
 * The fields in tuples, unions, and dtuples are allowed to
 * be mutable (array fields are always mutable).  The following
 * flags determine mutablility:
 *
 *    Mutable: the field is mutable
 *    Immutable: the field is not mutable
 *
 *    MutableDelayed:
 *       The mutability is not known.  This is eliminated
 *       by type inference, and it will not pass the type
 *       checker.
 *
 *    MutableVar v:
 *       This is used only during type inference, where
 *       the mutability is associated with a variable
 *       in a substitution table.
 *)
type mutable_flag =
   Mutable
 | Immutable
 | MutableDelayed
 | MutableVar of var


(*
 * Subscript operations.  The subop is a hint to the backend on how to
 * interpret the data being read/written, and how to interpret the index
 * value used in subscripting.  A subscript operation has four parts:
 *
 *    1. the kind of block being subscripted:
 *          BlockSub:    a ML block: TyUnion, TyTuple, TyArray
 *          RawDataSub:  a TyRawData block
 *          TupleSub:    a ML block containing only pointers
 *          RawTupleSub: a mixed rawdata/pointer block
 *
 *    2. the kind of element being read/written:
 *          PolySub:          a TyInt or a ML block
 *          RawIntSub:        a TyRawInt with indicated precision/signed
 *          RawFloatSub:      a TyRawFloat with indicated precision
 *          PointerInfixSub:  a infix pointer (pair of base and offset ptr)
 *          BlockPointerSub:  a data pointer
 *          RawPointerSub:    a data pointer
 *          FunctionSub:      a function pointer
 *
 *       (note that the actual memory format for these values is not
 *        specified until the MIR)
 *
 *    3. the element width (used to determine how to read the index):
 *          ByteSub: byte width
 *          WordSub: word width
 *
 *    4. the kind of subscript index:
 *          IntIndex: ML-style int subscript
 *          RawIntIndex (pre, signed): rawint subscript
 *)
type sub_block =
   BlockSub
 | RawDataSub
 | TupleSub
 | RawTupleSub

type sub_index =
   ByteIndex
 | WordIndex

type sub_script =
   IntIndex
 | RawIntIndex of int_precision * int_signed

type sub_value =
   EnumSub of int
 | IntSub
 | TagSub of ty_var * mutable_ty list
 | PolySub
 | RawIntSub of int_precision * int_signed
 | RawFloatSub of float_precision
 | PointerInfixSub
 | BlockPointerSub
 | RawPointerSub
 | FunctionSub

and subop =
   { sub_block  : sub_block;
     sub_value  : sub_value;
     sub_index  : sub_index;
     sub_script : sub_script
   }

(*
 * Types.  A few types are marked as ``not a smalltype''; the types marked
 * this way cannot be used in certain types of tuples or unions, and cannot
 * be used as a type argument in a TyApply or AtomTyApply.
 *
 * Unfortunately, types are polymorphic with respect to a 'mutable_flag,
 * which describes whether tuple and union fields are mutable.  The only
 * time when we use something other than the type mutable_flag here
 * is during type inference.
 *
 *    TyInt
 *       Normal 31-bit ML-style integer
 *
 *    TyEnum size
 *       Enumeration in the interval [0, size) (unsigned).  The
 *       size must be less than max_enum (defined in sizeof_const.ml).
 *
 *    TyRawInt (pre, signed)
 *       Raw integer, with indicated precision and signedness
 *       (not a smalltype).
 *
 *    TyFloat fpre
 *       Raw floating-point value, with indicated precision
 *       (not a smalltype).
 *
 *    TyFun (args, return)
 *       Function with indicated argument and return types.
 *
 *    TyUnion (union_name, type_args, int_set)
 *       Value of union_name case (union_name is bound in the type defs).
 *       The type_args are a list of types used to instantiate the union;
 *       they are NOT the types of the elements!  THe type_args must be
 *       smalltypes.
 *
 *       If the union has N cases, then the int_set is a subset of [0..N),
 *       and indicates which cases this union may be.  If int_set is a
 *       singleton set, then we know exactly which union case we are in,
 *       and can dereference the union pointer.
 *
 *       Union elements must be smalltypes.
 *
 *    TyTuple (tuple_class, flags_types)
 *       Tuple with indicated element types.  The flags indicate mutability
 *       of the field.  See tuple_class comments above for more information.
 *
 *    TyDTuple (ty_var, tyl_opt)
 *       A dependent-tuple type.  This represents the following type:
 *          Sigma (t1, ..., tn) : Type. t1 * ... * tn
 *       An element of this type is tagged with a TyTag (described below)
 *       that defines the element types.  The ty_var must refer to a
 *       TyDefDTuple.  If tyl_opt is (Some tyl), then tyl are the types
 *       of the elements; otherwise the element types are not known.
 *
 *    TyTag (ty_var, tyl)
 *       A tag used for constructing TyDTuple values (see AllocDTuple
 *       below).  The tyl are the types of the elements in the tuple.
 *
 *    TyArray t
 *       Array of elements, all of type t.
 *
 *    TyRawData
 *       Byte array of arbitrary data.  Used for most C data structures,
 *       where we don't know the types of the elements within.  Requires
 *       safety checks.
 *
 *    TyPointer sub_block
 *       Infix pointers; a pointer into rawdata that contains an offset
 *       from the base of a rawdata block.
 *
 *    TyFrame (frame_name, type_args)
 *       Pointer to frame data (frame_name is bound in the frame defs).
 *       The type_args are a list of types used to instantiate the frame;
 *       they must be smalltypes.
 *
 *       Frames are intended to be used for frames generated during closure
 *       conversion.  The assumption is that frames are not directly access-
 *       able to the programmer, so they do not escape.  As a result, we are
 *       free to store extra fields in the frame before a CPS call, and are
 *       guaranteed that the values will be correct when the continuation
 *       is called.  If this assumption is violated, the program will still
 *       be type-safe, but the Fir_closure module will change the semantics
 *       of the program.
 *
 *    TyVar ty_var
 *       Polymorphic type variable.
 *
 *    TyApply (ty_var, type_args)
 *       Apply polymorphic type_args to the type function indicated by
 *       ty_var (which is bound in the type defs).  type_args must all be
 *       smalltypes.
 *
 *    TyExists (poly_vars, t)
 *       Existential type, binding the named poly_vars.
 *
 *    TyAll (poly_vars, t)
 *       Universal type, binding the named poly_vars.
 *
 *    TyProject (v, i)
 *       If v is a variable with type TyExists (alpha_1, ..., alpha_n . t),
 *       then the projection v.i refers to alpha_i.
 *
 *    TyCase t
 *       Used for object system.
 *
 *    TyObject (ty_var, t)
 *       Used for object system.
 *
 *    TyDelayed
 *       Used by frontends and some optimizations to request that the
 *       FIR type inference infer this type automatically.  No TyDelayed's
 *       should exist in the program after type inference phase.
 *)
and ty =
   (* Base types *)
   TyInt
 | TyEnum of int

   (* Native types *)
 | TyRawInt of int_precision * int_signed
 | TyFloat of float_precision

   (* Functions *)
 | TyFun of ty list * ty

   (* Tuples *)
 | TyUnion of ty_var * ty list * int_set
 | TyTuple of tuple_class * mutable_ty list
 | TyDTuple of ty_var * mutable_ty list option
 | TyTag of ty_var * mutable_ty list

   (* Other aggregates *)
 | TyArray of ty
 | TyRawData
 | TyPointer of sub_block
 | TyFrame of ty_var * ty list

   (* Polymorphism *)
 | TyVar of ty_var
 | TyApply of ty_var * ty list
 | TyExists of ty_var list * ty
 | TyAll of ty_var list * ty
 | TyProject of var * int

   (* Object-oriented *)
 | TyCase of ty
 | TyObject of ty_var * ty

   (* Type should be inferred *)
 | TyDelayed

and mutable_ty = ty * mutable_flag


(*
 * A frame has a list of fields, each of which has a list of subfields.
 * The var list is a universal quantifier.
 *
 * The outer symbol table is indexed on the field names.  The list is
 * a list of subfields for that field --- certain fields (e.g. pointers)
 * must be broken into multiple subfields, such as pointers which become
 * a base/offset pair. The subfield tuple is (sf_name, sf_type, sf_size)
 *)
type frame = var list * ((var * ty * int) list SymbolTable.t)


(*
 * Type definitions are the toplevel constructions.
 *)
type tydef =
   TyDefUnion of ty_var list * mutable_ty list list
 | TyDefLambda of ty_var list * ty
 | TyDefDTuple of ty_var


(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)


(*
 * Inlined operators
 *)
type unop =
   NotEnumOp         of int

   (* Negation (arithmetic and bitwise) for NAML ints *)
 | UMinusIntOp
 | NotIntOp
 | AbsIntOp

   (* Negation (arithmetic and bitwise) for native ints *)
 | UMinusRawIntOp    of int_precision * int_signed
 | NotRawIntOp       of int_precision * int_signed

   (*
    * RawBitFieldOp (pre, signed, offset, length):
    *    Extracts the bitfield in an integer value, which starts at bit
    *    <offset> (counting from LSB) and containing <length> bits. The
    *    bit shift and length are constant values.  To access a variable
    *    bitfield you must do a manual shift/mask; this optimization is
    *    only good for constant values.  (pre, signed) refer to the
    *    rawint precision of the result; the input should be int32.
    *)
 | RawBitFieldOp     of int_precision * int_signed * int * int

   (* Unary floating-point operations *)
 | UMinusFloatOp     of float_precision   (* Arithmetic negation *)
 | AbsFloatOp        of float_precision   (* Absolute value *)
 | SinFloatOp        of float_precision   (* Sine *)
 | CosFloatOp        of float_precision   (* Cosine *)
 | TanFloatOp        of float_precision   (* Tangent *)
 | ASinFloatOp       of float_precision   (* ArcSine *)
 | ACosFloatOp       of float_precision   (* ArcCosine *)
 | ATanFloatOp       of float_precision   (* ArcTangent *)
 | SinHFloatOp       of float_precision   (* SineHyp *)
 | CosHFloatOp       of float_precision   (* CosineHyp *)
 | TanHFloatOp       of float_precision   (* TangentHyp *)
 | ExpFloatOp        of float_precision   (* Exponential *)
 | LogFloatOp        of float_precision   (* Logarithm *)
 | Log10FloatOp      of float_precision   (* Log, base 10 *)
 | SqrtFloatOp       of float_precision   (* Square root *)
 | CeilFloatOp       of float_precision   (* Ceiling *)
 | FloorFloatOp      of float_precision   (* Truncate *)

   (*
    * Coercions:
    *    Int refers to an ML-style integer
    *    Float refers to floating-point value
    *    RawInt refers to native int, any precision
    *
    * In operators where both terms have precision qualifiers, the
    * destination precision is always specified before the source
    * precision.
    *)

   (* Coercions to int *)
 | IntOfFloatOp      of float_precision
 | IntOfRawIntOp     of int_precision * int_signed

   (* Coerce to float *)
 | FloatOfIntOp      of float_precision
 | FloatOfFloatOp    of float_precision * float_precision
 | FloatOfRawIntOp   of float_precision * int_precision * int_signed

   (* Coerce to rawint *)
 | RawIntOfIntOp     of int_precision * int_signed
 | RawIntOfEnumOp    of int_precision * int_signed * int
 | RawIntOfFloatOp   of int_precision * int_signed * float_precision

   (*
    * Coerce a rawint of one type to another:
    * First pair is destination precision, second pair is source.
    *)
 | RawIntOfRawIntOp  of int_precision * int_signed * int_precision * int_signed

   (*
    * Integer<->pointer coercions (only for C, not inherently safe)
    * These operators are specifically related to infix pointers...
    *)
 | RawIntOfPointerOp of int_precision * int_signed
 | PointerOfRawIntOp of int_precision * int_signed

   (*
    * Create an infix pointer from a block pointer.  The infix pointer
    * so that both the base and offset pointers contained within it point
    * to the beginning of the block.  The result is always an infix ptr.
    *)
 | PointerOfBlockOp of sub_block

   (*
    * Get the block length.
    *)
 | LengthOfBlockOp of subop * ty

   (*
    * Type coercions.
    *)
 | DTupleOfDTupleOp of ty_var * mutable_ty list
 | UnionOfUnionOp of ty_var * ty list * int_set * int_set
 | RawDataOfFrameOp of ty_var * ty list


type binop =
   (* Bitwise operations on enumerations. *)
   AndEnumOp of int
 | OrEnumOp of int
 | XorEnumOp of int

   (* Standard binary operations on NAML ints *)
 | PlusIntOp
 | MinusIntOp
 | MulIntOp
 | DivIntOp
 | RemIntOp
 | LslIntOp
 | LsrIntOp
 | AsrIntOp
 | AndIntOp
 | OrIntOp
 | XorIntOp
 | MaxIntOp
 | MinIntOp

   (* Comparisons on NAML ints *)
 | EqIntOp
 | NeqIntOp
 | LtIntOp
 | LeIntOp
 | GtIntOp
 | GeIntOp
 | CmpIntOp                (* Similar to ML's ``compare'' function. *)

   (*
    * Standard binary operations on native ints.  The precision is
    * the result precision; the inputs should match this precision.
    *)
 | PlusRawIntOp   of int_precision * int_signed
 | MinusRawIntOp  of int_precision * int_signed
 | MulRawIntOp    of int_precision * int_signed
 | DivRawIntOp    of int_precision * int_signed
 | RemRawIntOp    of int_precision * int_signed
 | SlRawIntOp     of int_precision * int_signed
 | SrRawIntOp     of int_precision * int_signed
 | AndRawIntOp    of int_precision * int_signed
 | OrRawIntOp     of int_precision * int_signed
 | XorRawIntOp    of int_precision * int_signed
 | MaxRawIntOp    of int_precision * int_signed
 | MinRawIntOp    of int_precision * int_signed

   (*
    * RawSetBitFieldOp (pre, signed, offset, length)
    *    See comments for RawBitFieldOp. This modifies the bitfield starting
    *    at bit <offset> and extending <length> bits.  There are two atoms:
    *       First atom is the integer containing the field.
    *       Second atom is the value to be set in the field.
    *    The resulting integer contains the revised field, with type
    *    ACRawInt (pre, signed)
    *)
 | RawSetBitFieldOp of int_precision * int_signed * int * int

   (* Comparisons on native ints *)
 | EqRawIntOp     of int_precision * int_signed
 | NeqRawIntOp    of int_precision * int_signed
 | LtRawIntOp     of int_precision * int_signed
 | LeRawIntOp     of int_precision * int_signed
 | GtRawIntOp     of int_precision * int_signed
 | GeRawIntOp     of int_precision * int_signed
 | CmpRawIntOp    of int_precision * int_signed

   (* Standard binary operations on floats *)
 | PlusFloatOp  of float_precision
 | MinusFloatOp of float_precision
 | MulFloatOp   of float_precision
 | DivFloatOp   of float_precision
 | RemFloatOp   of float_precision
 | MaxFloatOp   of float_precision
 | MinFloatOp   of float_precision

   (* Comparisons on floats *)
 | EqFloatOp    of float_precision
 | NeqFloatOp   of float_precision
 | LtFloatOp    of float_precision
 | LeFloatOp    of float_precision
 | GtFloatOp    of float_precision
 | GeFloatOp    of float_precision
 | CmpFloatOp   of float_precision

   (*
    * Arctangent.  This computes arctan(y/x), where y is the first atom
    * and x is the second atom given.  Handles case when x = 0 correctly.
    *)
 | ATan2FloatOp of float_precision

   (*
    * Power.  This computes x^y.
    *)
 | PowerFloatOp of float_precision

   (*
    * Float hacking.
    * This sets the exponent field of the float.
    *)
 | LdExpFloatIntOp of float_precision

   (* Pointer (in)equality.  Arguments must have the given type *)
 | EqEqOp of ty
 | NeqEqOp of ty

   (*
    * Pointer arithmetic. The pointer in the first argument, and the
    * returned pointer should be infix pointers (which keep the base
    * pointer as well as a pointer to anywhere within the block).
    *)
 | PlusPointerOp of sub_block * int_precision * int_signed


(*
 * A field is identified by a triple (frame, field, subfield).
 *)
type frame_label = label * label * label


(*
 * Normal values:
 *    AtomNil ty
 *       A nil pointer of indicated type.  This will be a zero-size block,
 *       therefore when dereferencing the pointer you can use a standard
 *       BoundsCheck to catch.
 *
 *    AtomInt i
 *       ML-style integer i (31 bit, LSB is 1)
 *
 *    AtomEnum (bound, value)
 *       Enumeration value.  Should satisfy 0 <= value < bound.
 *
 *    AtomRawInt i
 *       Native integer i (precision and signed are encoded in i)
 *
 *    AtomFloat f
 *       Floating-point value f (precision is encoded in f)
 *
 *    AtomLabel (label, off)
 *       Offset off to a value in a TyFrame (label) block.
 *
 *    AtomSizeof (frames, constant)
 *       Total size of the frames in the list, plus an arbitrary constant.
 *
 *    AtomConst (ty, ty_var, i)
 *       Constant constructor (used for constructor cases in unions which
 *       have no arguments).  Non-constant constructors are created with
 *       AllocUnion, below, which has a similar form.
 *
 *    AtomVar v
 *       Reference to a variable v.
 *
 *    AtomFun v
 *       Global function v.
 *
 *    AtomTyApply (v, t, [t_1; ...; t_n]):
 *       v should have type (TyAll x_1, ..., x_n). t
 *       The quantifier is instantiated to get an atom of type
 *          t[t_1, ..., t_n/x_1, ..., x_n]
 *
 *    AtomTyPack (v, s, [t_1, ..., t_n])
 *       Existential introduction; here's the informal rule.
 *
 *       v : s[t_1, ..., t_n/x_1, ..., x_n]
 *       s = TyExists (x_1, ..., x_n. s')
 *       ----------------------------------
 *       AtomTyPack (v, s, [t_1, ..., t_n]) : s
 *
 *    AtomTyUnpack v
 *       If v has type TyExists (x_0, ..., x_{n - 1}. t)
 *       then (AtomTyUnpack v) : t[v.0, ..., v_{n - 1}/x_0, ..., x_{n - 1}]
 *
 *    AtomRawDataOfFrame v:
 *       Convert v from some frame type to a TyRawData
 *
 *    AtomUnop (op, a):
 *       Unary arithmetic
 *
 *    AtomBinop (op, a1, a2):
 *       Binary arithmetic
 *
 * AtomLabel and AtomSizeof are used for frames; since the FIR may rearrange
 * and add new elements to the frames, we cannot use constant offsets/sizes
 * for these objects during the FIR.
 *)
type atom =
   (* Base atoms *)
   AtomNil of ty
 | AtomInt of int
 | AtomEnum of int * int
 | AtomRawInt of Rawint.rawint
 | AtomFloat of Rawfloat.rawfloat
 | AtomLabel of frame_label * Rawint.rawint
 | AtomSizeof of ty_var list * Rawint.rawint
 | AtomConst of ty * ty_var * int
 | AtomVar of var
 | AtomFun of var

   (* Polymorphism *)
 | AtomTyApply of atom * ty * ty list
 | AtomTyPack of var * ty * ty list
 | AtomTyUnpack of var

   (* Arithmetic *)
 | AtomUnop of unop * atom
 | AtomBinop of binop * atom * atom


(*
 * Allocation operators:
 *    AllocTuple (class, ty_vars, ty, atoms)
 *       Allocate a tuple of indicated type ty, and using the indicated
 *       atoms as initializer values.  If ty_vars is not empty, then the
 *       tuple is embedded in a universal qualifier that binds the named
 *       symbols; the ty_vars may be used by the initializer atoms.
 *
 *    AllocDTuple (ty, ty_var, tag, atoms)
 *       Allocate a dependent tuple with the given tag and the indicated
 *       elements as initializer values.
 *
 *    AllocUnion (ty_vars, ty, ty_var, tag, atoms)
 *       Allocate a union with the integer tag given, and using the atoms
 *       as initialisers for that union case.  The name of the union is
 *       given in ty_var.  The type arguments to instantiate the union
 *       are in the result type, ty.  If ty_vars is not empty, then the
 *       tuple is embedded in a universal qualifier that binds the named
 *       symbols; the ty_vars may be used by the initializer atoms.
 *
 *    AllocArray (ty, atoms)
 *       Allocate an array with indicated atoms for initialization.  The
 *       type given is the type of the array, so if ty is (TyArray ty'),
 *       then the type of the atoms should be ty'.
 *
 *    AllocVArray (ty, indexty, size, init)
 *       Allocate a VArray with indicated size, and using the initialiser
 *       atom to initialise all entries.  The indexty is used to indicate
 *       whether size is in bytes or words.  The type given is the type
 *       of the array.
 *
 *    AllocMalloc (ty, size)
 *       Standard C allocation.  Allocate a TyRawData block with size bytes.
 *
 *    AllocFrame (v, type_args)
 *       Allocate a new frame with the name v.  The frame is instantiated
 *       with the type_args indicated (which must be smalltypes).
 *)
type 'atom poly_alloc_op =
   AllocTuple   of tuple_class * ty_var list * ty * 'atom list
 | AllocUnion   of ty_var list * ty * ty_var * int * 'atom list
 | AllocDTuple  of ty * ty_var * 'atom * 'atom list
 | AllocArray   of ty * 'atom list
 | AllocVArray  of ty * sub_index * 'atom * 'atom
 | AllocMalloc  of ty * 'atom
 | AllocFrame   of ty_var * ty list

type alloc_op = atom poly_alloc_op


(*
 * Tail call operators for the SpecialCall construct.  The SpecialCall
 * is used by constructs which will eventually call some other local
 * function (say, f), and as such they resemble normal tailcalls. How-
 * ever, these constructs require some special processing first, such
 * as process migration or atomicity.  This construct identifies which
 * case is occurring.
 *
 * The special ops are briefly described below; check the MIR or runtime
 * docs for a more thorough description however.
 *
 *    TailSysMigrate (id, loc_ptr, loc_off, f, args)
 *       Perform a migration between systems.  id is a unique identifier for
 *       the call.  The destination is described by a null-terminated string
 *       at (loc_ptr, loc_off).  See migration documentation for details on
 *       the string's format.  After migration, the function f is called
 *       with args given; if args = (v1, v2, ..., vn) then we will call
 *       f(v1, v2, ..., vn).
 *
 * Note for atomic transactions:  the atomic levels are organized such that
 * level *0* is the outermost level, where no atomic entries have occurred.
 * Each entry creates a new level.  Suppose there are N levels; rollback and
 * commit can operate on the i'th level, where 0 < i <= N.  Commit/rollback
 * on the Nth level will take action on the most recently entry, and commit/
 * rollback on the 1st level will take action on the oldest entry.  Also, if
 * i = 0, then the backend will take i = N automatically.
 *
 *    TailAtomic (f, c, args)
 *       Enter a new atomic block, then call f.  c is (currently) an
 *       integer constant that is passed to f; args are the variables
 *       which are normally passed to f; note that on rollback, f will
 *       be called again; it will receive the same args but the value
 *       of c will change.  If args = (v1, v2, ..., vn) then we will
 *       call f(c, v1, v2, ..., vn).
 *
 *    TailAtomicRollback (level, c)
 *       Rollback the atomic level indicated, and call the function f
 *       again (where f was the function used by that atomic level).
 *       ALL levels more recent than <level> must be rolled back.  We
 *       will be in level <level> after this is called; in effect, we
 *       REENTER the transaction (this is not an abort).
 *
 *    TailAtomicCommit (level, g, args)
 *       Commits (exits) the atomic level indicated, then calls function
 *       g with the args given.  Note that g is not same as function f
 *       above; this can be an arbitrary function, and it receives only
 *       the args given here.  A commit on an older level will simply
 *       merge that level's data with the previous level --- it will NOT
 *       commit more recent levels.
 *)
type tailop =
   TailSysMigrate of int * atom * atom * atom * atom list
 | TailAtomic of atom * atom * atom list
 | TailAtomicRollback of atom * atom
 | TailAtomicCommit of atom * atom * atom list


(*
 * Predicates and assertions.
 * Currently, these are used mainly for runtime safety checks.
 * However, it is likely that this set will grow to include
 * higher-level proofs, whenever we figure out what those are.
 *
 * For now we have these simple forms:
 *    IsMutable v
 *       Required between any Reserve and SetSubscript.  The argument is
 *       a block, and the assertion tells the runtime to make the block
 *       mutable so it can be modified.
 *
 *    Reserve (bytes, pointers)
 *       Reserve at least "a" bytes of storage from the runtime.  This
 *       number includes the size of the maximum allocation along any
 *       execution path, terminating at the next reserve.  The size
 *       includes the size of runtime block headers, which are defined
 *       in arch/conf/sizeof.ml.
 *
 *    BoundsCheck (subop, v, a1, a2)
 *       An array bounds check. Succeeds if a1 >= 0 && a2 <= array length.
 *       In general, a1 is the lower bound and a2 is the upper bound.
 *
 *    ElementCheck (subop, v, a)
 *       Succeeds if v[a] has the element specified by the subop.
 *)
type 'atom poly_pred =
   IsMutable of 'atom
 | Reserve of 'atom * 'atom
 | BoundsCheck of subop * 'atom * 'atom * 'atom
 | ElementCheck of ty * subop * 'atom * 'atom

type pred = atom poly_pred


(*
 * Debugging info.
 *)
type debug_line = loc

type debug_vars = (var * ty * var) list

type debug_info =
   DebugString of string
 | DebugContext of debug_line * debug_vars


(*
 * Expressions.
 *)
type exp = exp_core simple_subst

and exp_core =
   (* Primitive operations *)
   LetAtom of var * ty * atom * exp

   (* Function application *)
   (*
    * LetExt (v, ty_v, gc_flag, f, ty_f, ty_args, args, e)
    *    Call an external function f, with function type ty_f, and give
    *    it the arguments listed in ty_args and args. The return value of f is stored
    *    in v, which should have type ty_v.
    *
    * TailCall (label, f, args)
    *    Call the function f with indicated arguments.  f may either be
    *    the name of a function directly, or it may be a variable holding
    *    a function pointer.  The label is only used for debugging, to
    *    identify this TailCall.
    *
    * SpecialCall (label, tailop)
    *    Execute a special call.  SpecialCalls resemble TailCalls in that
    *    control is transferred to another function; however, some action
    *    is usually performed before the new function is invoked (e.g.
    *    a system migration).  See comments for tailop (above) for details.
    *    The label is only used for debugging to identify this SpecialCall.
    *)
 | LetExt of var * ty * string * bool * ty * ty list * atom list * exp
 | TailCall of label * atom * atom list
 | SpecialCall of label * tailop

   (*
    * Control.
    *
    * Match (a, [l1, s1, e1; ...; ln, sn, en])
    *    Choose the first set si where (a in si); the value is ei.
    *
    * MatchDTuple (a, [l1, a1, e1; ...; ln, an, en])
    *    The atom a should be a dependent tuple (of type TyDTuple).
    *    Choose the first case ei where a has tag ai.
    *    A tag of None matches any tag.
    *
    * TypeCase (a1, a2, v1, v2, e1, e2):
    *    This is like MatchDTuple, but jyh has completely
    *    forgotten what all the different fields are.
    *)
 | Match of atom * (label * set * exp) list
 | MatchDTuple of atom * (label * atom option * exp) list
 | TypeCase of atom * atom * var * var * exp * exp

   (* Allocation *)
 | LetAlloc of var * alloc_op * exp

   (* Memory operations. *)
   (*
    * Subscripting operations use various types of offsets, depending
    * on the type of the pointer being subscripted.  Requirements on the
    * offsets are listed below:
    *    TyTuple, TyUnion:
    *       Offset must be constant; AtomInt is interpreted as a word
    *       offset, and AtomRawInt is interpreted as a byte offset.
    *       For BoxTuple tuples, the constant must be zero.
    *    TyArray, TyRawData, TyPointer:
    *       Offset can be constant or variable.  For TyPointer, the
    *       offset is relative to the offset embedded in the infix ptr,
    *       and can be negative.
    *    TyFrame:
    *       Offset must be an AtomLabel, which indicates the frame,
    *       field, subfield, and offset into the subfield being accessed.
    *
    * LetSubscript (subop, v, ty, ptr, off, e)
    *    Read ptr[off] and store the result in v:ty, using subop to
    *    interpret the value in memory and the type of offset used.
    *
    * SetSubscript (subop, label, ptr, off, ty, a, e)
    *    Store a:ty into memory at ptr[off], using subop to interpret
    *    the value in a and the type of offset used.  The label is only
    *    used for debugging to identify this SetSubscript.
    *
    * LetGlobal (subvalue, v, ty, global, e)
    *    Read a global variable global:ty, using subvalue to interpret
    *    the value read out.
    *
    * SetGlobal (subvalue, label, global, ty, a, e)
    *    Store a into the global variable global:ty, using subvalue to
    *    interpret the value in a.  The label is used for debugging.
    *
    * Memcpy (subop, label, dstp, dsto, srcp, srco, size, e)
    *    Copies a block of memory from one block into another.  The
    *    data is read from srcp[srco], and is written to dstp[dsto].
    *    size bytes/words are copied (depending on subop). The subop
    *    indicates what types of blocks srcp and dstp are, and also
    *    how to interpret the offsets srco, dsto.  Currently both
    *    blocks must have the same subop type.
    *
    *    WARNING:  This implements a forward-copy; if dstp and srcp
    *    are the same block, and srco < dsto < srco + size, then the
    *    copy will overwrite data that it is supposed to copy, BEFORE
    *    it has copied the data. As a result, Memcpy is not recommended
    *    when srcp and dstp refer to the same block.
    *
    *    The label is used for debugging only, to identify this Memcpy.
    *)
 | LetSubscript of subop * var * ty * atom * atom * exp
 | SetSubscript of subop * label * atom * atom * ty * atom * exp
 | LetGlobal of sub_value * var * ty * label * exp
 | SetGlobal of sub_value * label * label * ty * atom * exp
 | Memcpy of subop * label * atom * atom * atom * atom * atom * exp

   (* Assertions: label is a unique label identifying this assertion *)
 | Call of label * atom * atom option list * exp
 | Assert of label * pred * exp

   (* Debugging *)
 | Debug of debug_info * exp


(*
 * Initializers.
 *)
type init_exp =
   InitAtom of atom
 | InitAlloc of alloc_op
 | InitRawData of int_precision * int array
 | InitNames of ty_var * (var * var option) list
 | InitTag of ty_var * mutable_ty list


(*
 * Function definition.
 *)
type fundef = debug_line * ty_var list * ty * var list * exp


(*
 * Program.
 *)
type global = ty * init_exp


(*
 * General debugging info.
 *)
type file_class =
   FileFC
 | FileNaml
 | FileJava
 | FileAml

type file_info =
   { file_dir : string;
     file_name : string;
     file_class : file_class
   }


(*
 * For imports, we save information about the _original_
 * argument types so we can recover the original calling
 * convention.
 *)
type import_arg =
   ArgFunction
 | ArgPointer
 | ArgRawInt of int_precision * int_signed
 | ArgRawFloat of float_precision

(*
 * On ImportFun, the bool is true iff this function
 * uses the stdargs convention.
 *)
type import_info =
   ImportGlobal
 | ImportFun of bool * import_arg list

type 'a poly_import =
   { import_name : 'a;
     import_type : ty;
     import_info : import_info
   }

type import = string poly_import


(*
 * For exports, we just keep the name and type.
 *)
type 'a poly_export =
   { export_name : 'a;
     export_type : ty
   }

type export = string poly_export


(*
 * This is all the info for a program.
 *)
type prog =
   { prog_file    : file_info;                    (* Source path/filename *)
     prog_import  : import SymbolTable.t;         (* Imports are external definitions *)
     prog_export  : export SymbolTable.t;         (* Exports are global values *)
     prog_types   : tydef  SymbolTable.t;         (* List of type definitions *)
     prog_frames  : frame  SymbolTable.t;         (* List of frame definitions *)
     prog_names   : ty     SymbolTable.t;         (* Names for some of the types *)
     prog_globals : global SymbolTable.t;         (* List of program globals *)
     prog_funs    : fundef SymbolTable.t;         (* List of functions *)
   }


(*
 * The "marshal" program is used for programs that may migrate.
 * It contains all the usual FIR information, but also includes
 * additional information to ensure symbol tables are not re-
 * ordered during migration:
 *
 *    marshal_fir:      Original FIR data.
 *    marshal_globals:  Ordered list of all program globals.
 *                      The globals are emitted in this order.
 *    marshal_funs:     Ordered list of all escaping functions.
 *                      The function table is emitted in this
 *                      order.
 *    marshal_rttd:     Ordered list of type descriptors.
 *
 * This structure is built by Fir_mprog:mprog_of_prog immediately
 * before calling the backend.  The FIR should not be significantly
 * altered once this structure is built (e.g. do not change the FIR
 * in ways that would affect the globals or funs lists).
 *
 * WARNING: The marshal_globals/marshal_funs MUST agree with the
 *          marshal_fir here.  DO NOT MODIFY any of these fields
 *          once this structure is generated.  See the warnings in
 *          the MIR program structure (near global_order/fun_order
 *          fields) for more information.
 *
 *          Any modification of this structure, once generated,
 *          may be fatal for system migration.
 *)
type mprog =
   { marshal_fir           : prog;              (* FIR data *)
     marshal_globals       : var list;          (* Ordered list of globals *)
     marshal_funs          : var list;          (* Ordered list of escaping functions *)
     marshal_rttd          : var RttdTable.t;   (* Ordered list of type descriptors *)
   }


(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
