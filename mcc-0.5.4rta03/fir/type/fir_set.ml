(*
 * Interval sets.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Interval_set

(************************************************************************
 * NORMAL VALUES
 ************************************************************************)

(*
 * Normal integers.
 *)
module IntElement =
struct
   type t = int
   let compare = Pervasives.compare
   let pred = Pervasives.pred
   let succ = Pervasives.succ
   let min = Pervasives.min_int
   let max = Pervasives.max_int
end

module IntSet = CountableIntervalSet (IntElement)

(************************************************************************
 * RAW INT
 ************************************************************************)

(*
 * Various forms of raw integers.
 *)
module Int8Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int8 true
   let max = Rawint.max_int Rawint.Int8 true
end

module Int8IntervalSet = CountableIntervalSet (Int8Element)

module Int16Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int16 true
   let max = Rawint.max_int Rawint.Int16 true
end

module Int16IntervalSet = CountableIntervalSet (Int16Element)

module Int32Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int32 true
   let max = Rawint.max_int Rawint.Int32 true
end

module Int32IntervalSet = CountableIntervalSet (Int32Element)

module Int64Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int64 true
   let max = Rawint.max_int Rawint.Int64 true
end

module Int64IntervalSet = CountableIntervalSet (Int64Element)

module UInt8Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int8 false
   let max = Rawint.max_int Rawint.Int8 false
end

module UInt8IntervalSet = CountableIntervalSet (UInt8Element)

module UInt16Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int16 false
   let max = Rawint.max_int Rawint.Int16 false
end

module UInt16IntervalSet = CountableIntervalSet (UInt16Element)

module UInt32Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int32 false
   let max = Rawint.max_int Rawint.Int32 false
end

module UInt32IntervalSet = CountableIntervalSet (UInt32Element)

module UInt64Element =
struct
   type t = Rawint.rawint

   let compare = Rawint.compare
   let pred = Rawint.pred
   let succ = Rawint.succ
   let min = Rawint.min_int Rawint.Int64 false
   let max = Rawint.max_int Rawint.Int64 false
end

module UInt64IntervalSet = CountableIntervalSet (UInt64Element)

(*
 * Combine them all.
 *)
module RawIntSet =
struct
   type elt = Rawint.rawint
   type t =
      Int8Set  of Int8IntervalSet.t
    | Int16Set of Int16IntervalSet.t
    | Int32Set of Int32IntervalSet.t
    | Int64Set of Int64IntervalSet.t
    | UInt8Set  of UInt8IntervalSet.t
    | UInt16Set of UInt16IntervalSet.t
    | UInt32Set of UInt32IntervalSet.t
    | UInt64Set of UInt64IntervalSet.t


   (*
    * Print the set.
    *)
   let name = function
      Int8Set _   -> "int8"
    | UInt8Set _  -> "uint8"
    | Int16Set _  -> "int16"
    | UInt16Set _ -> "uint16"
    | Int32Set _  -> "int32"
    | UInt32Set _ -> "uint32"
    | Int64Set _  -> "int64"
    | UInt64Set _ -> "uint64"

   (*
    * Precision of the set.
    *)
   let precision = function
      Int8Set _
    | UInt8Set _  -> Rawint.Int8
    | Int16Set _
    | UInt16Set _ -> Rawint.Int16
    | Int32Set _
    | UInt32Set _ -> Rawint.Int32
    | Int64Set _
    | UInt64Set _ -> Rawint.Int64

   (*
    * Signedness of the set.
    *)
   let signed = function
      Int8Set _
    | Int16Set _
    | Int32Set _
    | Int64Set _  -> true
    | UInt8Set _
    | UInt16Set _
    | UInt32Set _
    | UInt64Set _ -> false

   (*
    * Empty set.
    *)
   let empty pre signed =
      match pre, signed with
         Rawint.Int8, true -> Int8Set Int8IntervalSet.empty
       | Rawint.Int8, false -> UInt8Set UInt8IntervalSet.empty
       | Rawint.Int16, true -> Int16Set Int16IntervalSet.empty
       | Rawint.Int16, false -> UInt16Set UInt16IntervalSet.empty
       | Rawint.Int32, true -> Int32Set Int32IntervalSet.empty
       | Rawint.Int32, false -> UInt32Set UInt32IntervalSet.empty
       | Rawint.Int64, true -> Int64Set Int64IntervalSet.empty
       | Rawint.Int64, false -> UInt64Set UInt64IntervalSet.empty

   (*
    * Largest set.
    *)
   let max_set pre signed =
      match pre, signed with
         Rawint.Int8, true -> Int8Set Int8IntervalSet.max_set
       | Rawint.Int8, false -> UInt8Set UInt8IntervalSet.max_set
       | Rawint.Int16, true -> Int16Set Int16IntervalSet.max_set
       | Rawint.Int16, false -> UInt16Set UInt16IntervalSet.max_set
       | Rawint.Int32, true -> Int32Set Int32IntervalSet.max_set
       | Rawint.Int32, false -> UInt32Set UInt32IntervalSet.max_set
       | Rawint.Int64, true -> Int64Set Int64IntervalSet.max_set
       | Rawint.Int64, false -> UInt64Set UInt64IntervalSet.max_set

   let is_total set =
      match set with
         Int8Set set   -> Int8IntervalSet.is_total set
       | UInt8Set set  -> UInt8IntervalSet.is_total set
       | Int16Set set  -> Int16IntervalSet.is_total set
       | UInt16Set set -> UInt16IntervalSet.is_total set
       | Int32Set set  -> Int32IntervalSet.is_total set
       | UInt32Set set -> UInt32IntervalSet.is_total set
       | Int64Set set  -> Int64IntervalSet.is_total set
       | UInt64Set set -> UInt64IntervalSet.is_total set

   (*
    * Make from an interval.
    *)
   let of_interval pre signed left right =
      match pre, signed with
         Rawint.Int8, true -> Int8Set (Int8IntervalSet.of_interval left right)
       | Rawint.Int8, false -> UInt8Set (UInt8IntervalSet.of_interval left right)
       | Rawint.Int16, true -> Int16Set (Int16IntervalSet.of_interval left right)
       | Rawint.Int16, false -> UInt16Set (UInt16IntervalSet.of_interval left right)
       | Rawint.Int32, true -> Int32Set (Int32IntervalSet.of_interval left right)
       | Rawint.Int32, false -> UInt32Set (UInt32IntervalSet.of_interval left right)
       | Rawint.Int64, true -> Int64Set (Int64IntervalSet.of_interval left right)
       | Rawint.Int64, false -> UInt64Set (UInt64IntervalSet.of_interval left right)

   (*
    * Check if a set is empty.
    *)
   let is_empty = function
      Int8Set set -> Int8IntervalSet.is_empty set
    | UInt8Set set -> UInt8IntervalSet.is_empty set
    | Int16Set set -> Int16IntervalSet.is_empty set
    | UInt16Set set -> UInt16IntervalSet.is_empty set
    | Int32Set set -> Int32IntervalSet.is_empty set
    | UInt32Set set -> UInt32IntervalSet.is_empty set
    | Int64Set set -> Int64IntervalSet.is_empty set
    | UInt64Set set -> UInt64IntervalSet.is_empty set

   (*
    * Test two raw_int sets.
    *)
   let subset set1 set2 =
      match set1, set2 with
         Int8Set set1, Int8Set set2     -> Int8IntervalSet.subset set1 set2
       | UInt8Set set1, UInt8Set set2   -> UInt8IntervalSet.subset set1 set2
       | Int16Set set1, Int16Set set2   -> Int16IntervalSet.subset set1 set2
       | UInt16Set set1, UInt16Set set2 -> UInt16IntervalSet.subset set1 set2
       | Int32Set set1, Int32Set set2   -> Int32IntervalSet.subset set1 set2
       | UInt32Set set1, UInt32Set set2 -> UInt32IntervalSet.subset set1 set2
       | Int64Set set1, Int64Set set2   -> Int64IntervalSet.subset set1 set2
       | UInt64Set set1, UInt64Set set2 -> UInt64IntervalSet.subset set1 set2
       | _ -> false

   let equal set1 set2 =
      match set1, set2 with
         Int8Set set1, Int8Set set2     -> Int8IntervalSet.equal set1 set2
       | UInt8Set set1, UInt8Set set2   -> UInt8IntervalSet.equal set1 set2
       | Int16Set set1, Int16Set set2   -> Int16IntervalSet.equal set1 set2
       | UInt16Set set1, UInt16Set set2 -> UInt16IntervalSet.equal set1 set2
       | Int32Set set1, Int32Set set2   -> Int32IntervalSet.equal set1 set2
       | UInt32Set set1, UInt32Set set2 -> UInt32IntervalSet.equal set1 set2
       | Int64Set set1, Int64Set set2   -> Int64IntervalSet.equal set1 set2
       | UInt64Set set1, UInt64Set set2 -> UInt64IntervalSet.equal set1 set2
       | _ -> false

   (*
    * Negate the set.
    *)
   let negate set =
      match set with
         Int8Set set    -> Int8Set (Int8IntervalSet.negate set)
       | UInt8Set set   -> UInt8Set (UInt8IntervalSet.negate set)
       | Int16Set set   -> Int16Set (Int16IntervalSet.negate set)
       | UInt16Set set  -> UInt16Set (UInt16IntervalSet.negate set)
       | Int32Set set   -> Int32Set (Int32IntervalSet.negate set)
       | UInt32Set set  -> UInt32Set (UInt32IntervalSet.negate set)
       | Int64Set set   -> Int64Set (Int64IntervalSet.negate set)
       | UInt64Set set  -> UInt64Set (UInt64IntervalSet.negate set)

   (*
    * Subtract the two sets.
    *)
   let subtract set1 set2 =
      match set1, set2 with
         Int8Set set1, Int8Set set2     -> Int8Set (Int8IntervalSet.subtract set1 set2)
       | UInt8Set set1, UInt8Set set2   -> UInt8Set (UInt8IntervalSet.subtract set1 set2)
       | Int16Set set1, Int16Set set2   -> Int16Set (Int16IntervalSet.subtract set1 set2)
       | UInt16Set set1, UInt16Set set2 -> UInt16Set (UInt16IntervalSet.subtract set1 set2)
       | Int32Set set1, Int32Set set2   -> Int32Set (Int32IntervalSet.subtract set1 set2)
       | UInt32Set set1, UInt32Set set2 -> UInt32Set (UInt32IntervalSet.subtract set1 set2)
       | Int64Set set1, Int64Set set2   -> Int64Set (Int64IntervalSet.subtract set1 set2)
       | UInt64Set set1, UInt64Set set2 -> UInt64Set (UInt64IntervalSet.subtract set1 set2)
       | _ -> raise (Invalid_argument "RawIntSet.subtract")

   (*
    * Union of the sets.
    *)
   let union set1 set2 =
      match set1, set2 with
         Int8Set set1, Int8Set set2     -> Int8Set (Int8IntervalSet.union set1 set2)
       | UInt8Set set1, UInt8Set set2   -> UInt8Set (UInt8IntervalSet.union set1 set2)
       | Int16Set set1, Int16Set set2   -> Int16Set (Int16IntervalSet.union set1 set2)
       | UInt16Set set1, UInt16Set set2 -> UInt16Set (UInt16IntervalSet.union set1 set2)
       | Int32Set set1, Int32Set set2   -> Int32Set (Int32IntervalSet.union set1 set2)
       | UInt32Set set1, UInt32Set set2 -> UInt32Set (UInt32IntervalSet.union set1 set2)
       | Int64Set set1, Int64Set set2   -> Int64Set (Int64IntervalSet.union set1 set2)
       | UInt64Set set1, UInt64Set set2 -> UInt64Set (UInt64IntervalSet.union set1 set2)
       | _ -> raise (Invalid_argument "RawIntSet.union")

   (*
    * Intersection of the sets.
    *)
   let isect set1 set2 =
      match set1, set2 with
         Int8Set set1, Int8Set set2     -> Int8Set (Int8IntervalSet.isect set1 set2)
       | UInt8Set set1, UInt8Set set2   -> UInt8Set (UInt8IntervalSet.isect set1 set2)
       | Int16Set set1, Int16Set set2   -> Int16Set (Int16IntervalSet.isect set1 set2)
       | UInt16Set set1, UInt16Set set2 -> UInt16Set (UInt16IntervalSet.isect set1 set2)
       | Int32Set set1, Int32Set set2   -> Int32Set (Int32IntervalSet.isect set1 set2)
       | UInt32Set set1, UInt32Set set2 -> UInt32Set (UInt32IntervalSet.isect set1 set2)
       | Int64Set set1, Int64Set set2   -> Int64Set (Int64IntervalSet.isect set1 set2)
       | UInt64Set set1, UInt64Set set2 -> UInt64Set (UInt64IntervalSet.isect set1 set2)
       | _ -> raise (Invalid_argument "RawIntSet.isect")

   (*
    * Singletons.
    *)
   let of_point i =
      match Rawint.precision i, Rawint.signed i with
         Rawint.Int8, true -> Int8Set     (Int8IntervalSet.of_point i)
       | Rawint.Int8, false -> UInt8Set   (UInt8IntervalSet.of_point i)
       | Rawint.Int16, true -> Int16Set   (Int16IntervalSet.of_point i)
       | Rawint.Int16, false -> UInt16Set (UInt16IntervalSet.of_point i)
       | Rawint.Int32, true -> Int32Set   (Int32IntervalSet.of_point i)
       | Rawint.Int32, false -> UInt32Set (UInt32IntervalSet.of_point i)
       | Rawint.Int64, true -> Int64Set   (Int64IntervalSet.of_point i)
       | Rawint.Int64, false -> UInt64Set (UInt64IntervalSet.of_point i)

   (*
    * Point membership.
    *)
   let mem_point i set =
      match set with
         Int8Set set   -> Int8IntervalSet.mem_point i set
       | UInt8Set set  -> UInt8IntervalSet.mem_point i set
       | Int16Set set  -> Int16IntervalSet.mem_point i set
       | UInt16Set set -> UInt16IntervalSet.mem_point i set
       | Int32Set set  -> Int32IntervalSet.mem_point i set
       | UInt32Set set -> UInt32IntervalSet.mem_point i set
       | Int64Set set  -> Int64IntervalSet.mem_point i set
       | UInt64Set set -> UInt64IntervalSet.mem_point i set

   (*
    * Add a point.
    *)
   let add_point set i =
      match set with
         Int8Set set   -> Int8Set   (Int8IntervalSet.add_point set i)
       | UInt8Set set  -> UInt8Set  (UInt8IntervalSet.add_point set i)
       | Int16Set set  -> Int16Set  (Int16IntervalSet.add_point set i)
       | UInt16Set set -> UInt16Set (UInt16IntervalSet.add_point set i)
       | Int32Set set  -> Int32Set  (Int32IntervalSet.add_point set i)
       | UInt32Set set -> UInt32Set (UInt32IntervalSet.add_point set i)
       | Int64Set set  -> Int64Set  (Int64IntervalSet.add_point set i)
       | UInt64Set set -> UInt64Set (UInt64IntervalSet.add_point set i)

   (*
    * Subtract a point.
    *)
   let subtract_point set i =
      match set with
         Int8Set set   -> Int8Set   (Int8IntervalSet.subtract_point set i)
       | UInt8Set set  -> UInt8Set  (UInt8IntervalSet.subtract_point set i)
       | Int16Set set  -> Int16Set  (Int16IntervalSet.subtract_point set i)
       | UInt16Set set -> UInt16Set (UInt16IntervalSet.subtract_point set i)
       | Int32Set set  -> Int32Set  (Int32IntervalSet.subtract_point set i)
       | UInt32Set set -> UInt32Set (UInt32IntervalSet.subtract_point set i)
       | Int64Set set  -> Int64Set  (Int64IntervalSet.subtract_point set i)
       | UInt64Set set -> UInt64Set (UInt64IntervalSet.subtract_point set i)

   (*
    * Check for a singleton set.
    *)
   let is_singleton set =
      match set with
         Int8Set set   -> Int8IntervalSet.is_singleton set
       | UInt8Set set  -> UInt8IntervalSet.is_singleton set
       | Int16Set set  -> Int16IntervalSet.is_singleton set
       | UInt16Set set -> UInt16IntervalSet.is_singleton set
       | Int32Set set  -> Int32IntervalSet.is_singleton set
       | UInt32Set set -> UInt32IntervalSet.is_singleton set
       | Int64Set set  -> Int64IntervalSet.is_singleton set
       | UInt64Set set -> UInt64IntervalSet.is_singleton set

   (*
    * Check for a singleton set.
    *)
   let dest_singleton set =
      match set with
         Int8Set set   -> Int8IntervalSet.dest_singleton set
       | UInt8Set set  -> UInt8IntervalSet.dest_singleton set
       | Int16Set set  -> Int16IntervalSet.dest_singleton set
       | UInt16Set set -> UInt16IntervalSet.dest_singleton set
       | Int32Set set  -> Int32IntervalSet.dest_singleton set
       | UInt32Set set -> UInt32IntervalSet.dest_singleton set
       | Int64Set set  -> Int64IntervalSet.dest_singleton set
       | UInt64Set set -> UInt64IntervalSet.dest_singleton set

   (*
    * Iteration.
    *)
   let iter f set =
      match set with
         Int8Set set   -> Int8IntervalSet.iter f set
       | UInt8Set set  -> UInt8IntervalSet.iter f set
       | Int16Set set  -> Int16IntervalSet.iter f set
       | UInt16Set set -> UInt16IntervalSet.iter f set
       | Int32Set set  -> Int32IntervalSet.iter f set
       | UInt32Set set -> UInt32IntervalSet.iter f set
       | Int64Set set  -> Int64IntervalSet.iter f set
       | UInt64Set set -> UInt64IntervalSet.iter f set

   let fold f x set =
      match set with
         Int8Set set   -> Int8IntervalSet.fold f x set
       | UInt8Set set  -> UInt8IntervalSet.fold f x set
       | Int16Set set  -> Int16IntervalSet.fold f x set
       | UInt16Set set -> UInt16IntervalSet.fold f x set
       | Int32Set set  -> Int32IntervalSet.fold f x set
       | UInt32Set set -> UInt32IntervalSet.fold f x set
       | Int64Set set  -> Int64IntervalSet.fold f x set
       | UInt64Set set -> UInt64IntervalSet.fold f x set
end (* module *)

let int_set_of_raw_int_set s =
   RawIntSet.fold (fun s left right ->
         let left =
            match left with
               Infinity ->
                  Infinity
             | Open i ->
                  Open (Rawint.to_int i)
             | Closed i ->
                  Closed (Rawint.to_int i)
         in
         let right =
            match right with
               Infinity ->
                  Infinity
             | Open i ->
                  Open (Rawint.to_int i)
             | Closed i ->
                  Closed (Rawint.to_int i)
         in
         let s' = IntSet.of_interval left right in
            IntSet.union s s') IntSet.empty s

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
