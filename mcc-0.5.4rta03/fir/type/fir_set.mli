(*
 * Sets of scalars.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Interval_set

module IntSet : IntervalSetSig with type elt = int

(*
 * Rawint sets.
 *)
module Int8IntervalSet    : IntervalSetSig with type elt = Rawint.rawint
module Int16IntervalSet   : IntervalSetSig with type elt = Rawint.rawint
module Int32IntervalSet   : IntervalSetSig with type elt = Rawint.rawint
module Int64IntervalSet   : IntervalSetSig with type elt = Rawint.rawint

module UInt8IntervalSet   : IntervalSetSig with type elt = Rawint.rawint
module UInt16IntervalSet  : IntervalSetSig with type elt = Rawint.rawint
module UInt32IntervalSet  : IntervalSetSig with type elt = Rawint.rawint
module UInt64IntervalSet  : IntervalSetSig with type elt = Rawint.rawint

(*
 * This is the parallel form to IntervalSet.
 *)
module RawIntSet :
sig
   type t
   type elt = Rawint.rawint

   val name : t -> string
   val precision : t -> Rawint.int_precision
   val signed : t -> Rawint.int_signed

   val empty : Rawint.int_precision -> Rawint.int_signed -> t
   val max_set : Rawint.int_precision -> Rawint.int_signed -> t
   val of_interval : Rawint.int_precision -> Rawint.int_signed -> elt bound -> elt bound -> t
   val is_empty : t -> bool
   val is_total : t -> bool

   val subset : t -> t -> bool
   val equal : t -> t -> bool
   val subtract : t -> t -> t
   val negate : t -> t
   val union : t -> t -> t
   val isect : t -> t -> t

   val of_point : elt -> t
   val mem_point : elt -> t -> bool
   val add_point : t -> elt -> t
   val is_singleton : t -> bool
   val dest_singleton : t -> elt
   val subtract_point : t -> elt -> t

   val iter : (elt bound -> elt bound -> unit) -> t -> unit
   val fold : ('a -> elt bound -> elt bound -> 'a) -> 'a -> t -> 'a
end

(*
 * Set conversion.  Of course, you need to worry about overflow.
 *)
val int_set_of_raw_int_set : RawIntSet.t -> IntSet.t

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
