(*
 * The "heap" expressions are much like Fir_algebra, but they
 * are specifically designed to deal with memory operations.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Attribute

open Fir
open Fir_exn
open Fir_pos
open Fir_type
open Fir_print
open Fir_algebra

module Pos = MakePos (struct let name = "Fir_heap" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * The heap is a set of previous values.
 *)
type heap = SymbolSet.t

(*
 * These are the heap expressions.
 *)
type htree_core =
   HTETree of etree_core
 | HTAlloc of heap * etree_core poly_alloc_op
 | HTSubscript of heap * subop * ty * etree_core * etree_core
 | HTSetSubscript of heap * subop * ty * etree_core * etree_core * etree_core
 | HTMemcpy of heap * subop * etree_core * etree_core * etree_core * etree_core * etree_core
 | HTExt of heap * ty * string * bool * ty * ty list * etree_core list
 | HTAssert of heap * etree_core poly_pred
 | HTDepends of heap

type htree = htree_core simple_subst

(*
 * Term operations.
 *)
let loc_of_htree = label_of_simple_term
let make_htree = wrap_simple_core
let dest_htree_core = dest_simple_core

(*
 * HTree tables.
 *)
module HTreeCompare =
struct
   type t = htree

   let etree_compare = ETreeCompare.compare

   let rec etree_list_compare el1 el2 =
      match el1, el2 with
         e1 :: el1, e2 :: el2 ->
            let cmp = etree_compare e1 e2 in
               if cmp = 0 then
                  etree_list_compare el1 el2
               else
                  cmp
       | _ :: _, [] ->
            1
       | [], _ :: _ ->
            -1
       | [], [] ->
            0

   let heap_compare s1 s2 =
      SymbolSet.compare s1 s2

   let alloc_op_compare op1 op2 =
      match op1, op2 with
         AllocTuple (t1, _, _, e1),
         AllocTuple (t2, _, _, e2) ->
            let cmp =
               match t1, t2 with
                  NormalTuple, NormalTuple
                | RawTuple, RawTuple
                | BoxTuple, BoxTuple ->
                     0
                | NormalTuple, RawTuple
                | NormalTuple, BoxTuple
                | RawTuple, BoxTuple ->
                     -1
                | RawTuple, NormalTuple
                | BoxTuple, NormalTuple
                | BoxTuple, RawTuple ->
                     1
            in
               if cmp = 0 then
                  etree_list_compare e1 e2
               else
                  cmp
       | AllocDTuple (_, v1, a1, args1),
         AllocDTuple (_, v2, a2, args2) ->
            let cmp = Symbol.compare v1 v2 in
               if cmp = 0 then
                  let cmp = etree_compare a1 a2 in
                     if cmp = 0 then
                        etree_list_compare args1 args2
                     else
                        cmp
               else
                  cmp
       | AllocUnion (_, _, v1, i1, el1),
         AllocUnion (_, _, v2, i2, el2) ->
            let cmp = Symbol.compare v1 v2 in
               if cmp = 0 then
                  let cmp = i1 - i2 in
                     if cmp = 0 then
                        etree_list_compare el1 el2
                     else
                        cmp
               else
                  cmp
       | AllocArray (_, el1),
         AllocArray (_, el2) ->
            etree_list_compare el1 el2
       | AllocVArray (_, _, e11, e12),
         AllocVArray (_, _, e21, e22) ->
            let cmp = etree_compare e11 e21 in
               if cmp = 0 then
                  etree_compare e12 e22
               else
                  cmp
       | AllocMalloc (_, e1),
         AllocMalloc (_, e2) ->
            etree_compare e1 e2
       | AllocFrame (v1, _),
         AllocFrame (v2, _) ->
            Symbol.compare v1 v2

       | AllocTuple _, AllocDTuple _
       | AllocTuple _, AllocUnion _
       | AllocTuple _, AllocArray _
       | AllocTuple _, AllocVArray _
       | AllocTuple _, AllocMalloc _
       | AllocTuple _, AllocFrame _
       | AllocDTuple _, AllocUnion _
       | AllocDTuple _, AllocArray _
       | AllocDTuple _, AllocVArray _
       | AllocDTuple _, AllocMalloc _
       | AllocDTuple _, AllocFrame _
       | AllocUnion _, AllocArray _
       | AllocUnion _, AllocVArray _
       | AllocUnion _, AllocMalloc _
       | AllocUnion _, AllocFrame _
       | AllocArray _, AllocVArray _
       | AllocArray _, AllocMalloc _
       | AllocArray _, AllocFrame _
       | AllocVArray _, AllocMalloc _
       | AllocVArray _, AllocFrame _
       | AllocMalloc _, AllocFrame _ ->
            -1
       | AllocDTuple _, AllocTuple _
       | AllocUnion _,  AllocTuple _
       | AllocArray _,  AllocTuple _
       | AllocVArray _, AllocTuple _
       | AllocMalloc _, AllocTuple _
       | AllocFrame _,  AllocTuple _
       | AllocUnion _,  AllocDTuple _
       | AllocArray _,  AllocDTuple _
       | AllocVArray _, AllocDTuple _
       | AllocMalloc _, AllocDTuple _
       | AllocFrame _,  AllocDTuple _
       | AllocArray _,  AllocUnion _
       | AllocVArray _, AllocUnion _
       | AllocMalloc _, AllocUnion _
       | AllocFrame _,  AllocUnion _
       | AllocVArray _, AllocArray _
       | AllocMalloc _, AllocArray _
       | AllocFrame _,  AllocArray _
       | AllocMalloc _, AllocVArray _
       | AllocFrame _,  AllocVArray _
       | AllocFrame _,  AllocMalloc _ ->
            1

   let pred_compare pred1 pred2 =
      match pred1, pred2 with
         IsMutable e1,
         IsMutable e2 ->
            etree_compare e1 e2

       | Reserve (e11, e12),
         Reserve (e21, e22) ->
            let cmp = etree_compare e11 e21 in
               if cmp = 0 then
                  etree_compare e12 e22
               else
                  cmp

       | BoundsCheck (op1, e11, e12, e13),
         BoundsCheck (op2, e21, e22, e23) ->
            let cmp = Pervasives.compare op1 op2 in
               if cmp = 0 then
                  let cmp = etree_compare e11 e21 in
                     if cmp = 0 then
                        let cmp = etree_compare e12 e22 in
                           if cmp = 0 then
                              etree_compare e13 e23
                           else
                              cmp
                     else
                        cmp
               else
                  cmp

       | ElementCheck (ty1, op1, e11, e12),
         ElementCheck (ty2, op2, e21, e22) ->
            let cmp = Pervasives.compare op1 op2 in
               if cmp = 0 then
                  let cmp = etree_compare e11 e21 in
                     if cmp = 0 then
                        etree_compare e12 e22
                     else
                        cmp
               else
                  cmp

       | IsMutable _,    Reserve _
       | IsMutable _,    BoundsCheck _
       | IsMutable _,    ElementCheck _
       | Reserve _,      BoundsCheck _
       | Reserve _,      ElementCheck _
       | BoundsCheck _,  ElementCheck _ ->
            -1
       | Reserve _,      IsMutable _
       | BoundsCheck _,  IsMutable _
       | ElementCheck _, IsMutable _
       | BoundsCheck _,  Reserve _
       | ElementCheck _, Reserve _
       | ElementCheck _, BoundsCheck _ ->
            1

   let compare h1 h2 =
      match dest_htree_core h1, dest_htree_core h2 with
         HTETree e1, HTETree e2 ->
            ETreeCompare.compare e1 e2
       | HTAlloc (heap1, op1),
         HTAlloc (heap2, op2) ->
            let cmp = heap_compare heap1 heap2 in
               if cmp = 0 then
                  alloc_op_compare op1 op2
               else
                  cmp
       | HTSubscript (heap1, op1, ty1, e11, e12),
         HTSubscript (heap2, op2, ty2, e21, e22) ->
            let cmp = heap_compare heap1 heap2 in
               if cmp = 0 then
                  let cmp = Pervasives.compare op1 op2 in
                     if cmp = 0 then
                        let cmp = etree_compare e11 e21 in
                           if cmp = 0 then
                              etree_compare e12 e22
                           else
                              cmp
                     else
                        cmp
               else
                  cmp
       | HTSetSubscript (heap1, op1, ty1, e11, e12, e13),
         HTSetSubscript (heap2, op2, ty2, e21, e22, e23) ->
            let cmp = heap_compare heap1 heap2 in
               if cmp = 0 then
                  let cmp = Pervasives.compare op1 op2 in
                     if cmp = 0 then
                        let cmp = etree_compare e11 e21 in
                           if cmp = 0 then
                              let cmp = etree_compare e12 e22 in
                                 if cmp = 0 then
                                    let cmp = etree_compare e13 e23 in
                                       cmp
                                 else
                                    cmp
                           else
                              cmp
                     else
                        cmp
               else
                  cmp
       | HTMemcpy (heap1, op1, e11, e12, e13, e14, e15),
         HTMemcpy (heap2, op2, e21, e22, e23, e24, e25) ->
            let cmp = heap_compare heap1 heap2 in
               if cmp = 0 then
                  let cmp = Pervasives.compare op1 op2 in
                     if cmp = 0 then
                        let cmp = etree_compare e11 e21 in
                           if cmp = 0 then
                              let cmp = etree_compare e12 e22 in
                                 if cmp = 0 then
                                    let cmp = etree_compare e13 e23 in
                                       if cmp = 0 then
                                          let cmp = etree_compare e14 e24 in
                                             if cmp = 0 then
                                                etree_compare e15 e25
                                             else
                                                cmp
                                       else
                                          cmp
                                 else
                                    cmp
                           else
                              cmp
                     else
                        cmp
               else
                  cmp
       | HTExt (heap1, _, s1, _, _, _, el1),
         HTExt (heap2, _, s2, _, _, _, el2) ->
            let cmp = Pervasives.compare s1 s2 in
               if cmp = 0 then
                  let cmp = heap_compare heap1 heap2 in
                     if cmp = 0 then
                        etree_list_compare el1 el2
                     else
                        cmp
               else
                  cmp

       | HTAssert (heap1, pred1),
         HTAssert (heap2, pred2) ->
            let cmp = heap_compare heap1 heap2 in
               if cmp = 0 then
                  pred_compare pred1 pred2
               else
                  cmp

       | HTDepends s1,
         HTDepends s2 ->
            heap_compare s1 s2

       | HTETree _,        HTAlloc _
       | HTETree _,        HTSubscript _
       | HTETree _,        HTSetSubscript _
       | HTETree _,        HTMemcpy _
       | HTETree _,        HTExt _
       | HTETree _,        HTAssert _
       | HTETree _,        HTDepends _
       | HTAlloc _,        HTSubscript _
       | HTAlloc _,        HTSetSubscript _
       | HTAlloc _,        HTMemcpy _
       | HTAlloc _,        HTExt _
       | HTAlloc _,        HTAssert _
       | HTAlloc _,        HTDepends _
       | HTSubscript _,    HTSetSubscript _
       | HTSubscript _,    HTMemcpy _
       | HTSubscript _,    HTExt _
       | HTSubscript _,    HTAssert _
       | HTSubscript _,    HTDepends _
       | HTSetSubscript _, HTMemcpy _
       | HTSetSubscript _, HTExt _
       | HTSetSubscript _, HTAssert _
       | HTSetSubscript _, HTDepends _
       | HTMemcpy _,       HTExt _
       | HTMemcpy _,       HTAssert _
       | HTMemcpy _,       HTDepends _
       | HTExt _,          HTAssert _
       | HTExt _,          HTDepends _
       | HTAssert _,       HTDepends _ ->
            -1
       | HTAlloc _,        HTETree _
       | HTSubscript _,    HTETree _
       | HTSetSubscript _, HTETree _
       | HTMemcpy _,       HTETree _
       | HTExt _,          HTETree _
       | HTAssert _,       HTETree _
       | HTDepends _,      HTETree _
       | HTSubscript _,    HTAlloc _
       | HTSetSubscript _, HTAlloc _
       | HTMemcpy _,       HTAlloc _
       | HTExt _,          HTAlloc _
       | HTAssert _,       HTAlloc _
       | HTDepends _,      HTAlloc _
       | HTSetSubscript _, HTSubscript _
       | HTMemcpy _,       HTSubscript _
       | HTExt _,          HTSubscript _
       | HTAssert _,       HTSubscript _
       | HTDepends _,      HTSubscript _
       | HTMemcpy _,       HTSetSubscript _
       | HTExt _,          HTSetSubscript _
       | HTAssert _,       HTSetSubscript _
       | HTDepends _,      HTSetSubscript _
       | HTExt _,          HTMemcpy _
       | HTAssert _,       HTMemcpy _
       | HTDepends _,      HTMemcpy _
       | HTAssert _,       HTExt _
       | HTDepends _,      HTExt _
       | HTDepends _,      HTAssert _ ->
            1

end

module HTreeTable = Mc_map.McMake (HTreeCompare)

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print the heap set.
 *)
let pp_print_heap buf h =
   SymbolSet.iter (fun v ->
         Format.fprintf buf "@ %a;" pp_print_symbol v) h

(*
 * Print the tree.
 *)
let rec pp_print_htree buf h =
   pp_print_htree_core buf (dest_htree_core h)

and pp_print_htree_core buf e =
   match e with
      HTETree e ->
         pp_print_etree_core buf e
    | HTAlloc (h1, op) ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>alloc {@ @[<hv 0>@[<hv 3>heap = {%a@]@ }@]@ @[<hv 3>op =@ %a@]@]@ }@]" (**)
            pp_print_heap h1
            (pp_print_poly_alloc_op pp_print_etree_core) op
    | HTSubscript (h1, op, ty, e2, e3) ->
         Format.printf "@[<hv 0>@[<hv 3>subscript {@ subop = ";
         pp_print_subop buf op;
         Format.fprintf buf ";@ @[<hv 3>type =@ ";
         pp_print_type buf ty;
         Format.fprintf buf ";@]@ @[<hv 0>@[<hv 3>heap = {";
         pp_print_heap buf h1;
         Format.fprintf buf "@]@ };@]@ @[<hv 3>value =@ ";
         pp_print_etree_core buf e2;
         Format.fprintf buf "[";
         pp_print_etree_core buf e3;
         Format.fprintf buf "]@]@]@ }@]"
    | HTSetSubscript (h1, op, ty, e2, e3, e4) ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>set-subscript {@ subop = ";
         pp_print_subop buf op;
         Format.fprintf buf ";@ @[<hv 3>type =@ ";
         pp_print_type buf ty;
         Format.fprintf buf ";@]@ @[<hv 0>@[<hv 3>heap = {";
         pp_print_heap buf h1;
         Format.fprintf buf "@]@ };@]@ @[<hv 3>value =@ ";
         pp_print_etree_core buf e2;
         Format.fprintf buf "[";
         pp_print_etree_core buf e3;
         Format.fprintf buf "] <-@ ";
         pp_print_etree_core buf e4;
         Format.fprintf buf "@]@]@ }@]"
    | HTMemcpy (h1, op, e2, e3, e4, e5, e6) ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>memcpy {@ subop = ";
         pp_print_subop buf op;
         Format.fprintf buf ";@ @[<hv 3>heap = {";
         pp_print_heap buf h1;
         Format.fprintf buf "@]@ };@]@ @[<hv 3>dst =@ ";
         pp_print_etree_core buf e2;
         Format.fprintf buf "[";
         pp_print_etree_core buf e3;
         Format.fprintf buf "];@]@ @[<hv 3>src =@ ";
         pp_print_etree_core buf e4;
         Format.fprintf buf "[";
         pp_print_etree_core buf e5;
         Format.fprintf buf "];@]@ @[<hv 3>length =@ ";
         pp_print_etree_core buf e6;
         Format.fprintf buf "@]@]@ }@]"
    | HTExt (h, ty1, s, b, ty2, ty_args, el) ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>extcall {@ @[<hv 3>type1 =@ ";
         pp_print_type buf ty1;
         Format.fprintf buf ";@]@ name = \"%s\", %b;@ @[<hv 3>type2 =@ " (String.escaped s) b;
         pp_print_type buf ty2;
         Format.fprintf buf ";@]@ @[<hv 0>@[<hv 3>type args = {";
         List.iter (fun ty ->
               pp_print_space buf ();
               pp_print_type buf ty) ty_args;
         Format.fprintf buf ";@]@ @[<hv 0>@[<hv 3>args = {";
         List.iter (fun e ->
               pp_print_space buf ();
               pp_print_etree_core buf e) el;
         Format.fprintf buf "@]@ }@]@]@ }@]"
    | HTAssert (h, pred) ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>assert {@ @[<hv 0>@[<hv 3>heap = {";
         pp_print_heap buf h;
         Format.fprintf buf "@]@ };@]@ @[<hv 3>pred =@ ";
         pp_print_poly_pred pp_print_etree_core buf pred;
         Format.fprintf buf "@]@]@ }@]"
    | HTDepends h ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>depends {";
         pp_print_heap buf h;
         Format.fprintf buf "@]@ }@]"

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Give a type to the dependency.
 *)
let ty_depends = TyEnum 1

(*
 * Type of the tree.
 *)
let type_of_htree_core genv pos h =
   let pos = string_pos "type_of_htree" pos in
      match h with
         HTETree e ->
            type_of_etree_core genv pos e
       | HTAlloc (_, op) ->
            type_of_alloc_op op
       | HTSubscript (_, _, ty, _, _)
       | HTExt (_, ty, _, _, _, _, _) ->
            ty
       | HTSetSubscript _
       | HTMemcpy _
       | HTAssert _
       | HTDepends _ ->
            ty_depends

let type_of_htree genv pos h =
   type_of_htree_core genv pos (dest_htree_core h)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
