(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

open Fir_alias_pre_ind
open Fir_alias_pre_call
open Fir_alias_pre_class
open Fir_alias_pre_norm
open Fir_alias_pre_cse
open Fir_alias_pre_loop
open Fir_alias_pre_triv
open Fir_alias_pre_useless

module Pos = MakePos (struct let name = "Fir_alias_prog" end)
open Pos

let alias_prog prog =
   let prog, cgraph, trace, venv = build_prog prog in
   let ienv = ind_prog venv in
   let venv, fenv, cenv = call_prog venv in
   let aenv = alias_prog venv ienv in
   let aenv = normalize_aenv aenv in
   let gvenv, cenv = cse_prog aenv cenv in
   let gvenv, lenv = loop_prog gvenv aenv in
   let gvenv, lenv = useless_prog gvenv lenv in
   (* let gvenv, lenv = triv_prog gvenv lenv in *)
   let info =
      { alias_aenv = gvenv.gvenv_values;
        alias_cenv = cenv;
        alias_fenv = fenv;
        alias_lenv = lenv
      }
   in
      if debug debug_alias then
         Format.printf "@[<v 3>*** FIR: alias:@ %a@]@." pp_print_alias_info info;
      prog, cgraph, trace, info

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
