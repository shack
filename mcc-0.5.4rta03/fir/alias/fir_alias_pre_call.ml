(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

module Pos = MakePos (struct let name = "Fir_alias_pre_call" end)
open Pos

(*
 * Collect a table of all the function calls in the program.
 *
 * In this step, we adjust all the function arguments so that they
 * are always variables.
 *)

(*
 * Abstract an argument.  If it is not already a var,
 * create one and add the definition to the venv.
 *)
let call_abstract_arg venv vc =
   match dest_vclass_core vc with
      VarExp (_, ETVar v) ->
         venv, v
    | _ ->
         let v = new_symbol_string "arg" in
         let loc = loc_of_vclass vc in
         let depth, e = etree_of_vclass_simple_core vc in
         let venv = venv_add_subst venv v depth (make_htree loc (HTETree e)) in
            venv, v

let call_abstract_args venv args =
   let venv, args =
      List.fold_left (fun (venv, args) arg ->
            let venv, arg = call_abstract_arg venv arg in
               venv, arg :: args) (venv, []) args
   in
      venv, List.rev args

(*
 * Heap arguments are also abstracted.
 *)
let call_abstract_heap venv ps_args =
   SymbolTable.fold_map (fun venv _ (depth, arg) ->
         match dest_htree_core arg with
            HTETree (ETVar v) ->
               venv, v
          | _ ->
               let v = new_symbol_string "heap_arg" in
               let venv = venv_add_subst venv v depth arg in
                  venv, v) venv ps_args

(*
 * Add the call entry.
 *)
let call_entry venv cenv pos ps_args label entry =
   let pos = string_pos "call_entry" pos in
   let venv, args = call_abstract_args venv entry.call_args in
   let ps_args =
      try SymbolTable.find ps_args label with
         Not_found ->
            SymbolTable.empty
   in
   let venv, ps_args = call_abstract_heap venv ps_args in
   let cenv = SymbolTable.add cenv label (args, ps_args) in
      venv, cenv

let call_entries venv cenv pos ps_args entries =
   let pos = string_pos "call_entries" pos in
      SymbolTable.fold (fun (venv, cenv) label entry ->
            call_entry venv cenv pos ps_args label entry) (venv, cenv) entries

(*
 * Add an abstracted call to the call environment.
 *)
let call_call venv fenv cenv f call =
   let { call_outer = outer;
         call_inner = inner;
         call_ps_args = ps_args;
         call_ps_vars = ps_vars
       } = call
   in
   let pos = string_pos "call_call" (var_exp_pos f) in
   let venv, cenv = call_entries venv cenv pos ps_args outer in
   let fenv = SymbolTable.add fenv f ps_vars in
   let venv, cenv = call_entries venv cenv pos ps_args inner in
      venv, fenv, cenv

let call_prog venv =
   SymbolTable.fold (fun (venv, fenv, cenv) f call ->
         call_call venv fenv cenv f call) (venv, SymbolTable.empty, SymbolTable.empty) venv.venv_calls


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
