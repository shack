(*
 * Basic integer logic.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Interval_set

open Fir
open Fir_exn
open Fir_pos
open Fir_set
open Fir_print
open Fir_algebra

module type FirLogicSig =
sig
   type pred

   (* Printing *)
   val pp_print_pred : Format.formatter -> pred -> unit

   (* Add an assignment *)
   val pred_assign : pred -> var -> etree -> pred

   (* Assert a relation *)
   val pred_assert : pred -> pos -> binop -> etree -> etree -> pred

   (* Add a match *)
   val pred_match : loc -> pred -> atom -> set -> pred

   (* Operators *)
   val pred_true : pred
   val pred_or : pred -> pred -> pred
end

module FirLogic : FirLogicSig =
struct
   (*
    * Relations on numbers.
    *)
   type relop =
      LtOp
    | LeOp
    | GtOp
    | GeOp
    | EqOp
    | NeqOp

   (*
    * Arithmetic on numbers.
    *)
   type unop =
      UMinusOp

   type binop =
      PlusOp
    | MulOp

   (*
    * A simplified form of etree.
    *)
   type ltree =
      LTAtom of atom
    | LTUnop of unop * atom
    | LTBinop of binop * atom * atom

   (*
    * Comparisons.
    *)
   module CompareLTree =
   struct
      type t = ltree
      let compare = Pervasives.compare
   end

   module LTreeTable = Mc_map.McMake (CompareLTree)

   (*
    * A conjunct.
    *)
   type state =
      { state_name : var;
        state_parents : SymbolSet.t;
        state_table : var LTreeTable.t;
        state_const : (relop * atom) list SymbolTable.t
      }

   (*
    * A predicate is represented as a DNF.
    *)
   type pred =
      { pred_states : state list }

   (*
    * Internal exceptions.
    *)
   exception HashError
   exception Unsatisfiable

   (************************************************************************
    * PRINTING
    ************************************************************************)

   (*
    * Print a unary operation.
    *)
   let pp_print_unop buf op a =
      let s =
         match op with
            UMinusOp -> "-"
      in
         Format.fprintf buf "%s %a" s pp_print_atom a

   let pp_print_binop buf op a1 a2 =
      let s =
         match op with
            PlusOp -> "+"
          | MulOp -> "*"
      in
         Format.fprintf buf "%a %s %a" pp_print_atom a1 s pp_print_atom a2

   (*
    * Print a ltree.
    *)
   let pp_print_ltree buf e =
      match e with
         LTAtom a ->
            pp_print_atom buf a
       | LTUnop (op, a) ->
            pp_print_unop buf op a
       | LTBinop (op, a1, a2) ->
            pp_print_binop buf op a1 a2

   (*
    * Print the state table.
    *)
   let pp_print_state_table buf table =
      LTreeTable.iter (fun e v ->
            Format.fprintf buf "@ @[<hv 3>%a =@ %a@]" pp_print_symbol v pp_print_ltree e) table

   (*
    * Print a relation.
    *)
   let pp_print_rel buf (op, a) =
      let s =
         match op with
            LtOp -> "<"
          | LeOp -> "<="
          | GtOp -> ">"
          | GeOp -> ">="
          | EqOp -> "="
          | NeqOp -> "<>"
      in
         Format.fprintf buf "(%s %a)" s pp_print_atom a

   (*
    * Print the constraint table.
    *)
   let pp_print_state_const buf table =
      SymbolTable.iter (fun v rels ->
            Format.fprintf buf "@ @[<hv 3> %a:" pp_print_symbol v;
            List.iter (fun rel ->
                  Format.fprintf buf "@ %a" pp_print_rel rel) rels;
            Format.fprintf buf "@]") table

   (*
    * Print a state.
    *)
   let pp_print_state buf state =
      let { state_table = table;
            state_const = const
          } = state
      in
         Format.fprintf buf "@[<v 3>State:@ @[<v 3>Table:";
         pp_print_state_table buf table;
         Format.fprintf buf "@]@ @[<v 3>Constraints:";
         pp_print_state_const buf const;
         Format.fprintf buf "@]@]"

   (*
    * Print a predicate.
    *)
   let pp_print_pred buf pred =
      let { pred_states = states } = pred in
         Format.fprintf buf "@[<v 3>FirLogic states:";
         List.iter (fun state ->
               Format.pp_print_space buf ();
               pp_print_state buf state) states;
         Format.fprintf buf "@]"

   (************************************************************************
    * UTILITIES
    ************************************************************************)

   (*
    * Invert a relop.
    *)
   let invert_op = function
      LtOp -> GeOp
    | LeOp -> GtOp
    | GtOp -> LeOp
    | GeOp -> LtOp
    | EqOp -> NeqOp
    | NeqOp -> EqOp

   (*
    * Check an integer relation.
    *)
   let check_int_relop op i1 i2 =
      let b =
         match op with
            LtOp -> i1 < i2
          | LeOp -> i1 <= i2
          | GtOp -> i1 > i2
          | GeOp -> i1 >= i2
          | EqOp -> i1 = i2
          | NeqOp -> i1 <> i2
      in
         if not b then
            raise Unsatisfiable

   (*
    * Check a rawint relation.
    *)
   let check_rawint_relop op i1 i2 =
      let cmp = Rawint.compare i1 i2 in
      let b =
         match op with
            LtOp -> cmp < 0
          | LeOp -> cmp <= 0
          | GtOp -> cmp > 0
          | GeOp -> cmp >= 0
          | EqOp -> cmp = 0
          | NeqOp -> cmp <> 0
      in
         if not b then
            raise Unsatisfiable

   (************************************************************************
    * CONSTRUCTORS
    ************************************************************************)

   (*
    * Basic state operations.
    *)
   let true_name = new_symbol_string "true"

   let state_true =
      { state_name = true_name;
        state_parents = SymbolSet.singleton true_name;
        state_table = LTreeTable.empty;
        state_const = SymbolTable.empty
      }

   let new_state_table state table =
      let { state_name = name;
            state_parents = parents;
            state_const = const
          } = state
      in
      let name' = new_symbol name in
         { state_name = name';
           state_parents = SymbolSet.add parents name';
           state_table = table;
           state_const = const
         }

   let new_state_const state const =
      let { state_name = name;
            state_parents = parents;
            state_table = table
          } = state
      in
      let name' = new_symbol name in
         { state_name = name';
           state_parents = SymbolSet.add parents name';
           state_table = table;
           state_const = const
         }

   (*
    * Predicates.
    *)
   let pred_true =
      { pred_states = [state_true] }

   (*
    * Hash-consing.
    * Make sure identical expressions
    * use the same representatives.
    *)
   let hash_ltree state e =
      let table = state.state_table in
      let state, v =
         try
            let v = LTreeTable.find table e in
               state, v
         with
            Not_found ->
               let v = new_symbol_string "tmp" in
               let table = LTreeTable.add table e v in
               let state = new_state_table state table in
                  state, v
      in
         state, AtomVar v

   let rec hash_etree state e =
      match e with
         ETConst a ->
            state, a
       | ETVar v ->
            state, AtomVar v
       | ETUnop (UMinusIntOp, e)
       | ETUnop (UMinusRawIntOp _, e) ->
            let state, a = hash_etree state e in
            let e = LTUnop (UMinusOp, a) in
               hash_ltree state e
       | ETBinop (PlusIntOp, e1, e2)
       | ETBinop (PlusRawIntOp _, e1, e2) ->
            let state, a1 = hash_etree state e1 in
            let state, a2 = hash_etree state e2 in
            let e = LTBinop (PlusOp, a1, a2) in
               hash_ltree state e
       | ETBinop (MulIntOp, ETConst a1, e2)
       | ETBinop (MulRawIntOp _, ETConst a1, e2) ->
            let state, a2 = hash_etree state e2 in
            let e = LTBinop (MulOp, a1, a2) in
               hash_ltree state e
       | _ ->
            raise HashError

   let hash_etree state e =
      hash_etree state (dest_etree_core e)

   (*
    * Add a constant integer constraint.
    *)
   let int_set_of_constraint op i =
      match op with
         EqOp  -> IntSet.of_point i
       | NeqOp -> IntSet.negate (IntSet.of_point i)
       | LtOp  -> IntSet.of_interval Infinity (Open i)
       | LeOp  -> IntSet.of_interval Infinity (Closed i)
       | GtOp  -> IntSet.of_interval (Open i) Infinity
       | GeOp  -> IntSet.of_interval (Closed i) Infinity

   let constrain_int op1 i rels =
      let set1 = int_set_of_constraint op1 i in
      let rec collect rels =
         match rels with
            (op2, AtomInt j) as rel :: rels' ->
               let set2 = int_set_of_constraint op2 j in
                  if IntSet.equal set1 set2 then
                     rels
                  else
                     let set3 = IntSet.isect set1 set2 in
                        if IntSet.is_empty set3 then
                           raise Unsatisfiable
                        else
                           rel :: collect rels'
          | rel :: rels ->
               rel :: collect rels
          | [] ->
               [op1, AtomInt i]
      in
         collect rels

   (*
    * Add a constant rawint constraint.
    *)
   let rawint_set_of_constraint op i =
      let pre = Rawint.precision i in
      let signed = Rawint.signed i in
         match op with
            EqOp  -> RawIntSet.of_point i
          | NeqOp -> RawIntSet.negate (RawIntSet.of_point i)
          | LtOp  -> RawIntSet.of_interval pre signed Infinity (Open i)
          | LeOp  -> RawIntSet.of_interval pre signed Infinity (Closed i)
          | GtOp  -> RawIntSet.of_interval pre signed (Open i) Infinity
          | GeOp  -> RawIntSet.of_interval pre signed (Closed i) Infinity

   let constrain_rawint op1 i rels =
      let set1 = rawint_set_of_constraint op1 i in
      let rec collect rels =
         match rels with
            (op2, AtomRawInt j) as rel :: rels' ->
               let set2 = rawint_set_of_constraint op2 j in
                  if RawIntSet.equal set1 set2 then
                     rels
                  else
                     let set3 = RawIntSet.isect set1 set2 in
                        if RawIntSet.is_empty set3 then
                           raise Unsatisfiable
                        else
                           rel :: collect rels'
          | rel :: rels ->
               rel :: collect rels
          | [] ->
               [op1, AtomRawInt i]
      in
         collect rels

   (*
    * For variables, just check if the contsraint is unsatisfiable.
    *)
   let constrain_var op1 v rels =
      let rec collect rels =
         match rels with
            (op2, AtomVar v') as rel :: rels' when Symbol.eq v' v ->
               let op =
                  if op1 = op2 then
                     op1
                  else
                     match op1, op2 with
                        EqOp, LeOp
                      | EqOp, GeOp ->
                           EqOp
                      | NeqOp, LtOp
                      | NeqOp, LeOp
                      | LtOp, NeqOp
                      | LeOp, NeqOp
                      | LtOp, LeOp
                      | LeOp, LtOp ->
                           LtOp
                      | NeqOp, GtOp
                      | NeqOp, GeOp
                      | GtOp, NeqOp
                      | GeOp, NeqOp
                      | GtOp, GeOp
                      | GeOp, GtOp ->
                           GtOp
                      | _ ->
                           raise Unsatisfiable
               in
                  (op, AtomVar v) :: rels'
          | rel :: rels ->
               rel :: collect rels
          | [] ->
               [op1, AtomVar v]
      in
         collect rels

   let constrain op a rels =
      match a with
         AtomInt i ->
            constrain_int op i rels
       | AtomRawInt i ->
            constrain_rawint op i rels
       | AtomVar v ->
            constrain_var op v rels
       | _ ->
            raise HashError

   (*
    * Add a constraint to the state.
    *)
   let state_constraint state v op a =
      let const = state.state_const in
      let rels =
         try SymbolTable.find const v with
            Not_found ->
               []
      in
      let rels = constrain op a rels in
      let const = SymbolTable.add const v rels in
         new_state_const state const

   (*
    * Add an assignment to the state.
    *)
   let state_assign state v op e =
      let state, a = hash_etree state e in
         state_constraint state v op a

   (*
    * Add a relation to the state.
    *)
   let state_assert state op e1 e2 =
      let state, a1 = hash_etree state e1 in
      let state, a2 = hash_etree state e2 in
         match a1, a2 with
            AtomVar v, a ->
               state_constraint state v op a
          | a, AtomVar v ->
               state_constraint state v (invert_op op) a
          | AtomInt i1, AtomInt i2 ->
               check_int_relop op i1 i2;
               state
          | AtomRawInt i1, AtomRawInt i2 ->
               check_rawint_relop op i1 i2;
               state
          | _ ->
               raise HashError

   (*
    * Assert a relation in the state.
    * This will split the states into two cases:
    *    Case 1: v = 1 && op e1 e2
    *    Case 2: v <> 1 && not (op e1 e2)
    *)
   let bloc = bogus_loc "<Fir_logic>"
   let one = make_etree bloc (ETConst (AtomInt 1))

   let pred_relop loc pred v op e1 e2 =
      (* State augmentation *)
      let e1 = make_etree loc e1 in
      let e2 = make_etree loc e2 in
      let rec assign_states op1 op2 states' states =
         match states with
            state :: states ->
               let states' =
                  try
                     let state = state_assign state v op1 one in
                     let state = state_assert state op2 e1 e2 in
                        state :: states'
                  with
                     Unsatisfiable ->
                        states'
               in
                  assign_states op1 op2 states' states
          | [] ->
               states'
      in

      (* Case 1: the predicate is true *)
      let states = pred.pred_states in
      let states1 = assign_states EqOp op [] states in

      (* Case 2: the predicate is not true *)
      let states2 = assign_states NeqOp (invert_op op) states1 states in
         { pred with pred_states = states2 }

   (*
    * A relation.
    *)
   let pred_assert pred op e1 e2 =
      let rec assert_states states' states =
         match states with
            state :: states ->
               let states' =
                  try state_assert state op e1 e2 :: states' with
                     Unsatisfiable ->
                        states'
               in
                  assert_states states' states
          | [] ->
               states'
      in
         { pred with pred_states = assert_states [] pred.pred_states }

   (*
    * A normal assignment.
    *)
   let pred_assign pred v e =
      let rec collect states' states =
         match states with
            state :: states ->
               let states' =
                  try state_assign state v EqOp e :: states' with
                     Unsatisfiable ->
                        states'
               in
                  collect states' states
          | [] ->
               states'
      in
         { pred with pred_states = collect [] pred.pred_states }

   (*
    * Add an assignment.
    * Relations are special.
    *)
   let pred_assign pred v e =
      let loc = loc_of_etree e in
         match dest_etree_core e with
            ETBinop (EqIntOp, e1, e2)
          | ETBinop (EqRawIntOp _, e1, e2) ->
               pred_relop loc pred v EqOp e1 e2
          | ETBinop (LtIntOp, e1, e2)
          | ETBinop (LtRawIntOp _, e1, e2) ->
               pred_relop loc pred v LtOp e1 e2
          | ETBinop (LeIntOp, e1, e2)
          | ETBinop (LeRawIntOp _, e1, e2) ->
               pred_relop loc pred v LeOp e1 e2
          | ETBinop (GtIntOp, e1, e2)
          | ETBinop (GtRawIntOp _, e1, e2) ->
               pred_relop loc pred v GtOp e1 e2
          | ETBinop (GeIntOp, e1, e2)
          | ETBinop (GeRawIntOp _, e1, e2) ->
               pred_relop loc pred v GeOp e1 e2
          | ETBinop (NeqIntOp, e1, e2)
          | ETBinop (NeqRawIntOp _, e1, e2) ->
               pred_relop loc pred v NeqOp e1 e2
          | _ ->
               pred_assign pred v e

   (*
    * Add an assignment, but trap the HashError.
    *)
   let pred_assign pred v e =
      try pred_assign pred v e with
         HashError ->
            pred

   (*
    * Pattern match.
    *)
   let pred_match loc pred a s =
      match a, s with
         AtomVar v, IntSet s ->
            if IntSet.is_singleton s then
               let j = IntSet.dest_singleton s in
                  pred_assert pred EqOp (make_etree loc (ETVar v)) (make_etree loc (ETConst (AtomInt j)))
            else
               pred
       | AtomVar v, RawIntSet s ->
            if RawIntSet.is_singleton s then
               let j = RawIntSet.dest_singleton s in
                  pred_assert pred EqOp (make_etree loc (ETVar v)) (make_etree loc (ETConst (AtomRawInt j)))
            else
               pred
       | _ ->
            pred

   (*
    * General assertion.
    *)
   let pred_assert pred pos op e1 e2 =
      let op =
         match op with
            EqIntOp
          | EqRawIntOp _
          | EqEqOp _ ->
               EqOp
          | NeqIntOp
          | NeqRawIntOp _
          | NeqEqOp _ ->
               NeqOp
          | LtIntOp
          | LtRawIntOp _ ->
               LtOp
          | LeIntOp
          | LeRawIntOp _ ->
               LeOp
          | GtIntOp
          | GtRawIntOp _ ->
               GtOp
          | GeIntOp
          | GeRawIntOp _ ->
               GeOp
          | _ ->
               raise (FirException (pos, StringError "illegal relation"))
      in
         try pred_assert pred op e1 e2 with
            HashError ->
               pred

   (*
    * Check if one of the states in the list is a parent.
    *)
   let parent_state state states =
      let parents = state.state_parents in
         List.exists (fun state -> SymbolSet.mem parents state.state_name) states

   (*
    * Disjunction.
    *)
   let pred_or p1 p2 =
      let rec collect states1 states2 =
         match states2 with
            state :: states2 ->
               let states1 =
                  if parent_state state states1 then
                     states1
                  else
                     state :: states1
               in
                  collect states1 states2
          | [] ->
               states1
      in
      let { pred_states = states1 } = p1 in
      let { pred_states = states2 } = p2 in
         { pred_states = collect states1 states2 }
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
