(* Fir constant folding, algebraic simplification, and reassociation
 * Geoffrey Irving
 * $Id: fir_algebra.mli,v 1.4 2002/08/11 19:11:07 jyh Exp $ *)

open Format

open Location

open Fir
open Fir_env
open Fir_pos

type etree_core =
   ETConst of atom
 | ETVar of var
 | ETUnop of unop * etree_core
 | ETBinop of binop * etree_core * etree_core

type etree

val loc_of_etree : etree -> loc
val make_etree : loc -> etree_core -> etree
val dest_etree_core : etree -> etree_core

module ETreeCompare : Mc_map.OrderedType with type t = etree_core
module ETreeTable : Mc_map.McMap with type key = etree_core

val equal_etree : etree -> etree -> bool
val equal_etree_core : etree_core -> etree_core -> bool

(*
 * Tree transformers.
 *)
val canonicalize_etree : pos -> etree -> etree
val canonicalize_etree_core : pos -> etree_core -> etree_core
val optimize_etree : pos -> etree -> etree
val optimize_etree_core : pos -> etree_core -> etree_core

(*
 * Printers
 *)
val pp_print_etree_core : formatter -> etree_core -> unit
val pp_print_etree : formatter -> etree -> unit

(*
 * Check if an etree is a var plus some other stuff.
 *)
val etree_is_sum_of_var_core : etree_core -> var -> (binop * etree_core) option
val etree_is_sum_of_var : etree -> var -> (binop * etree) option

(*
 * Apply a predicate to all vars in the etree.
 *)
val etree_forall_vars_core : (var -> bool) -> etree_core -> bool
val etree_forall_vars : (var -> bool) -> etree -> bool

(*
 * Get the etree type.
 *)
val type_of_etree_core : genv -> pos -> etree_core -> ty
val type_of_etree : genv -> pos -> etree -> ty
