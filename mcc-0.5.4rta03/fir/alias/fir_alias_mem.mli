(*
 * Memory operations in alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Fir
open Fir_env
open Fir_pos
open Fir_heap
open Fir_algebra

open Fir_logic.FirLogic

open Fir_depth
open Fir_alias_type

val root_alias : var
val heap_empty : heap
val default_empty : default
val block_new_root : loc -> block

val alias_new_unknown : genv -> venv -> alias_env -> depth -> pos -> loc -> var -> ty -> (unit -> default) -> venv * alias_env * default option * iclass

(*
 * Memory operations.
 *)
val alias_overwrite_args : alias_env -> pos -> var -> vclass list -> alias_env * SymbolSet.t
val alias_subscript : genv -> venv -> alias_env -> pos -> loc -> depth -> subop -> var -> ty -> etree_core -> var -> etree_core -> etree_core -> venv * alias_env * vclass
val alias_set_subscript : venv -> alias_env -> var -> pos -> loc -> subop -> var -> etree_core -> etree_core -> ty -> etree_core -> var -> SymbolSet.t -> SymbolSet.t -> etree_core -> int -> vclass -> venv * alias_env
val alias_memcpy : venv -> alias_env -> depth -> pos -> loc -> subop -> var -> etree_core -> etree_core -> etree_core -> etree_core -> etree_core -> SymbolSet.t -> etree_core -> SymbolSet.t -> SymbolSet.t -> etree_core -> venv * alias_env

(*
 * Predicates.
 *)
val alias_is_mutable : venv -> alias_env -> depth -> pos -> loc -> var -> etree_core -> var -> venv * alias_env
val alias_reserve : venv -> alias_env -> depth -> pos -> loc -> var -> etree_core -> etree_core -> venv * alias_env
val alias_bounds_check : venv -> alias_env -> depth -> pos -> loc -> pred -> var -> subop -> etree_core -> etree_core -> etree_core -> etree_core -> var -> venv * alias_env * pred
val alias_element_check : genv -> venv -> alias_env -> pos -> loc -> depth -> label -> subop -> ty -> etree_core -> var -> etree_core -> etree_core -> venv * alias_env

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
