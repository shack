(*
 * Dominators and loop nests.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Trace

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_state

module Pos = MakePos (struct let name = "Fir_loop" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

type fun_node =
   { fnode_name : var;
     fnode_in_calls : var SymbolTable.t;
     fnode_out_calls : var SymbolTable.t
   }

type calltable = fun_node SymbolTable.t

type callgraph =
   { call_root : fun_node;
     call_nodes : calltable
   }

module type FirLoopSig =
sig
   val flow_prog : prog -> prog
   val build_callgraph : prog -> callgraph
   val build_incalls : calltable -> calltable
   val pp_print_calltable : formatter -> calltable -> unit
   val pp_print_callgraph : formatter -> callgraph -> unit
   val build_loop : callgraph -> fun_node trace
   val trace_loops : var trace -> SymbolSet.t SymbolTable.t
end

module FirLoop : FirLoopSig =
struct
   (************************************************************************
    * PRINTING
    ************************************************************************)

   (*
    * Print a callgraph node.
    *)
   let pp_print_fnode buf info =
      let { fnode_name = name;
            fnode_in_calls = icalls;
            fnode_out_calls = ocalls
          } = info
      in
         fprintf buf "@[<v 3>Function: %a" pp_print_symbol name;
         fprintf buf "@ @[<hv 3>in-calls:";
         SymbolTable.iter (fun label v ->
               fprintf buf "@ %a: %a" pp_print_symbol label pp_print_symbol v) icalls;
         fprintf buf "@]@ @[<hv 3>out-calls:";
         SymbolTable.iter (fun label v ->
               fprintf buf "@ %a: %a" pp_print_symbol label pp_print_symbol v) ocalls;
         fprintf buf "@]@]"

   (*
    * Print the call graph.
    *)
   let pp_print_calltable buf nodes =
      fprintf buf "@[<v 3>Call table:";
      SymbolTable.iter (fun _ node ->
            pp_print_space buf ();
            pp_print_fnode buf node) nodes;
      fprintf buf "@]"

   let pp_print_callgraph buf { call_root = root; call_nodes = nodes } =
      fprintf buf "@[<v 3>Callgraph:";
      fprintf buf "@ @[<v 3>Root node:@ ";
      pp_print_fnode buf root;
      fprintf buf "@]@ @[<v 3>Call nodes:";
      SymbolTable.iter (fun _ node ->
            pp_print_space buf ();
            pp_print_fnode buf node) nodes;
      fprintf buf "@]@]"

   (*
    * Print the loop-nest tree.
    *)
   let pp_print_loop buf loop =
      fprintf buf "@[<v 3>Loop:";
      Trace.pp_print buf pp_print_fnode loop;
      fprintf buf "@]"

   (************************************************************************
    * CPS NODES
    ************************************************************************)

   (*
    * Collect the args that are environments.
    *)
   let flow_args genv aenv pos ty_vars =
      let pos = string_pos "flow_args" pos in
      let args =
         List.fold_left (fun args ty_var ->
               match expand_type genv pos ty_var with
                  TyVar v ->
                     let arg =
                        try Some (SymbolTable.find aenv v) with
                           Not_found ->
                              None
                     in
                        arg :: args
                | _ ->
                     None :: args) [] ty_vars
      in
         List.rev args

   (*
    * Examine a function and identify the vars that
    * correspond to CPS calls.  Collect a set of all
    * the arguments that look like CPS calls.  All of
    * these will have an argument that is a var bound
    * by a existential quantifier.
    *)
   let flow_fun genv fenv f ty =
      let pos = string_pos "flow_fun" (var_exp_pos f) in
      let evars, ty_vars, _ = dest_all_fun_type genv pos ty in

      (* Find all the arguments that have the environment type *)
      let aenv, _ =
         List.fold_left (fun (aenv, index) ty_var ->
               let aenv =
                  match expand_type genv pos ty_var with
                     TyVar v ->
                        if List.mem v evars then
                           SymbolTable.add aenv v index
                        else
                           aenv
                   | _ ->
                        aenv
               in
                  aenv, succ index) (SymbolTable.empty, 0) ty_vars
      in

      (* Find all the arguments that are higher-order functions *)
      let funs, _ =
         List.fold_left (fun (funs, index) ty_var ->
               let funs =
                  match expand_type genv pos ty_var with
                     TyFun (ty_vars, _) ->
                        let args = flow_args genv aenv pos ty_vars in
                           (index, args) :: funs
                   | _ ->
                        funs
               in
                  funs, succ index) ([], 0) ty_vars
      in
         SymbolTable.add fenv f funs

   (*
    * Collect all the CPS info for the funs in the program.
    *)
   let flow_funs prog =
      let { prog_funs = funs;
            prog_import = import
          } = prog
      in
      let genv = genv_of_prog prog in

      (* Build function table for each function *)
      let fenv = SymbolTable.empty in
      let fenv =
         SymbolTable.fold (fun fenv f (_, evars, ty, _, _) ->
               flow_fun genv fenv f (TyAll (evars, ty))) fenv funs
      in
      let fenv =
         SymbolTable.fold (fun fenv f { import_type = ty } ->
               let pos = string_pos "flow_funs" (var_exp_pos f) in
                  if is_fun_type genv pos ty then
                     flow_fun genv fenv f ty
                  else
                     fenv) fenv import
      in
         fenv

   (*
    * Insert comment calls before the tailcall.
    *)
   let rec nth_arg pos i args =
      match i, args with
         0, a :: _ -> a
       | _, _ :: args -> nth_arg pos (pred i) args
       | _, [] -> raise (FirException (pos, StringIntError ("index out of bounds", i)))

   let nth_args pos g_args args =
      List.map (function
         Some i -> Some (nth_arg pos i args)
       | None -> None) g_args

   let nth_var pos i args =
      var_of_atom pos (nth_arg pos i args)

   let flow_tailcall fenv pos loc f args e =
      try
         let f = var_of_fun pos f in
         let fargs = SymbolTable.find fenv f in
         let pos = string_pos "flow_tailcall" (var_exp_pos f) in
            List.fold_left (fun e (i, g_args) ->
                  let label = new_symbol_string "call" in
                  let g = nth_var pos i args in
                  let args = nth_args pos g_args args in
                     if SymbolTable.mem fenv g then
                        make_exp loc (Call (label, AtomVar g, args, e))
                     else
                        e) e fargs
      with
         Not_found ->
            e

   (*
    * Add extra calls before all the tailcalls.
    *)
   let rec flow_exp fenv e =
      let pos = string_pos "flow_exp" (exp_pos e) in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetAtom (v, ty, a, e) ->
               make_exp loc (LetAtom (v, ty, a, flow_exp fenv e))
          | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
               make_exp loc (LetExt (v, ty1, s, b, ty2, ty_args, args, flow_exp fenv e))
          | LetAlloc (v, op, e) ->
               make_exp loc (LetAlloc (v, op, flow_exp fenv e))
          | Debug (info, e) ->
               make_exp loc (Debug (info, flow_exp fenv e))
          | LetSubscript (op, v, ty, v2, a, e) ->
               make_exp loc (LetSubscript (op, v, ty, v2, a, flow_exp fenv e))
          | SetSubscript (op, label, v, a1, ty, a2, e) ->
               make_exp loc (SetSubscript (op, label, v, a1, ty, a2, flow_exp fenv e))
          | LetGlobal (op, v, ty, a, e) ->
               make_exp loc (LetGlobal (op, v, ty, a, flow_exp fenv e))
          | SetGlobal (op, label, v, ty, a, e) ->
               make_exp loc (SetGlobal (op, label, v, ty, a, flow_exp fenv e))
          | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
               make_exp loc (Memcpy (op, label, v1, a1, v2, a2, a3, flow_exp fenv e))
          | Assert (label, pred, e) ->
               make_exp loc (Assert (label, pred, flow_exp fenv e))
          | Call (_, _, _, e) ->
               flow_exp fenv e
          | Match (a, cases) ->
               let cases = List.map (fun (l, s, e) -> l, s, flow_exp fenv e) cases in
                  make_exp loc (Match (a, cases))
          | MatchDTuple (a, cases) ->
               let cases = List.map (fun (l, a_opt, e) -> l, a_opt, flow_exp fenv e) cases in
                  make_exp loc (MatchDTuple (a, cases))
          | TypeCase (a1, a2, v1, v2, e1, e2) ->
               make_exp loc (TypeCase (a1, a2, v1, v2, flow_exp fenv e1, flow_exp fenv e2))
          | TailCall (_, f, args)
          | SpecialCall (_, TailSysMigrate (_, _, _, f, args))
          | SpecialCall (_, TailAtomicCommit (_, f, args)) ->
               flow_tailcall fenv pos loc f args e
          | SpecialCall (_, TailAtomic (f, c, args)) ->
               flow_tailcall fenv pos loc f (c :: args) e
          | SpecialCall (_, TailAtomicRollback _) ->
               e

   (*
    * Convert all the function bodies.
    *)
   let flow_prog prog =
      let { prog_funs = funs } = prog in
      let fenv = flow_funs prog in
      let funs =
         SymbolTable.map (fun (info, ty, ty_vars, vars, e) ->
               info, ty, ty_vars, vars, flow_exp fenv e) funs
      in
         { prog with prog_funs = funs }

   (************************************************************************
    * CONTROL-FLOW GRAPH.
    ************************************************************************)

   (*
    * Collect all the calls in the function.
    *)
   let rec collect_calls funs calls e =
      let pos = string_pos "collect_calls" (exp_pos e) in
         match dest_exp_core e with
            LetAtom (_, _, _, e)
          | LetExt (_, _, _, _, _, _, _, e)
          | LetAlloc (_, _, e)
          | Debug (_, e)
          | LetSubscript (_, _, _, _, _, e)
          | SetSubscript (_, _, _, _, _, _, e)
          | LetGlobal (_, _, _, _, e)
          | SetGlobal (_, _, _, _, _, e)
          | Memcpy (_, _, _, _, _, _, _, e)
          | Assert (_, _, e) ->
               collect_calls funs calls e
          | Match (_, cases) ->
               List.fold_left (fun calls (_, _, e) ->
                     collect_calls funs calls e) calls cases
          | MatchDTuple (_, cases) ->
               List.fold_left (fun calls (_, _, e) ->
                     collect_calls funs calls e) calls cases
          | TypeCase (_, _, _, _, e1, e2) ->
               collect_calls funs (collect_calls funs calls e1) e2
          | TailCall (label, f, _)
          | SpecialCall (label, TailSysMigrate (_, _, _, f, _))
          | SpecialCall (label, TailAtomicCommit (_, f, _))
          | SpecialCall (label, TailAtomic (f, _, _)) ->
               let f = var_of_fun pos f in
                  if SymbolTable.mem funs f then
                     SymbolTable.add calls label f
                  else
                     calls
          | SpecialCall (_, TailAtomicRollback _) ->
               calls
          | Call (label, f, _, e) ->
               let f = var_of_fun pos f in
               let calls =
                  if SymbolTable.mem funs f then
                     SymbolTable.add calls label f
                  else
                     calls
               in
                  collect_calls funs calls e

   (*
    * Build the out-calls for each of the functions.
    *)
   let collect_funs funs =
      SymbolTable.mapi (fun f (_, _, _, _, e) ->
            let calls = collect_calls funs SymbolTable.empty e in
               { fnode_name = f;
                 fnode_in_calls = SymbolTable.empty;
                 fnode_out_calls = calls
               }) funs

   (*
    * Once all the calls are defined, add the in-edges.
    *)
   let build_incalls calls =
      SymbolTable.fold (fun funs f ginfo ->
            let { fnode_out_calls = calls } = ginfo in
               SymbolTable.fold (fun funs label g ->
                     SymbolTable.filter_add funs g (fun ginfo ->
                           match ginfo with
                              Some ginfo ->
                                 let in_calls = SymbolTable.add ginfo.fnode_in_calls label f in
                                    { ginfo with fnode_in_calls = in_calls }
                            | None ->
                                 raise (Invalid_argument "collect_incalls: illegal call"))) funs calls) (**)
         calls calls

   (*
    * If a function has no in-calls, connect it to
    * the root.
    *)
   let collect_roots v_root root calls =
      let inset = SymbolTable.add SymbolTable.empty v_root v_root in
      let ocalls, calls =
         SymbolTable.fold (fun (ocalls, calls) f info ->
               if SymbolTable.is_empty info.fnode_in_calls then
                  let info = { info with fnode_in_calls = inset } in
                  let ocalls = SymbolTable.add ocalls v_root f in
                  let calls = SymbolTable.add calls f info in
                     ocalls, calls
               else
                  ocalls, calls) (root.fnode_out_calls, calls) calls
      in
      let ocalls = SymbolTable.remove ocalls v_root in
      let root = { root with fnode_out_calls = ocalls } in
      let calls = SymbolTable.add calls v_root root in
         calls

   (*
    * Build the call graph.
    *)
   let build_callgraph prog =
      let { prog_types = tenv;
            prog_funs = funs;
            prog_export = export
          } = prog
      in

      (* Collect out-calls for each of the funs *)
      let calls = collect_funs funs in

      (* Build a fake root node that calls all exported funs *)
      let roots =
         SymbolTable.fold (fun roots f _ ->
               if SymbolTable.mem funs f then
                  SymbolTable.add roots f f
               else
                  roots) SymbolTable.empty export
      in
      let v_root = new_symbol_string "root" in
      let root =
         { fnode_name = v_root;
           fnode_in_calls = SymbolTable.empty;
           fnode_out_calls = roots
         }
      in
      let calls = SymbolTable.add calls v_root root in

      (* Construct the in-edges *)
      let calls = build_incalls calls in

      (* Add other root edges *)
      let calls = collect_roots v_root root calls in

      (* Remove the root node *)
      let root = SymbolTable.find calls v_root in
      let calls = SymbolTable.remove calls v_root in
      let callgraph =
         { call_root = root;
           call_nodes = calls
         }
      in
         if debug debug_loop then
            begin
               eprintf "@[<v 3>*** FIR: callgraph@ ";
               pp_print_callgraph err_formatter callgraph;
               eprintf "@]@."
            end;
         callgraph

   (************************************************************************
    * LOOP-NEST TREE
    ************************************************************************)

   (*
    * Build the call graph.
    *)
   let build_loop { call_root = root; call_nodes = calls } =
      (* Flatten into a list *)
      let nodes =
         SymbolTable.fold (fun nodes _ info ->
               info :: nodes) [] calls
      in
      let node_name { fnode_name = name } =
         name
      in
      let node_calls { fnode_out_calls = calls } =
         let calls =
            SymbolTable.fold (fun calls _ f ->
                  SymbolSet.add calls f) SymbolSet.empty calls
         in
            SymbolSet.to_list calls
      in

      (* Build the loop-nest tree *)
      let loop = Loop.create "Fir_loop" node_name node_calls root nodes in
      let loop = Loop.loop_nest loop node_name in
         if debug debug_loop then
            begin
               eprintf "@[<v 3>*** FIR: loop-nest tree@ ";
               pp_print_loop err_formatter loop;
               eprintf "@]@."
            end;
         loop

   (*
    * Build the loop set from the trace.
    *)
   let rec trace_loops loops s trace =
      match trace with
         Elem f :: trace ->
            trace_loops loops (SymbolSet.add s f) trace
       | Trace (f, trace1) :: trace2 ->
            let loops, s' = trace_loops loops (SymbolSet.singleton f) trace1 in
            let loops = SymbolTable.add loops f s' in
            let s = SymbolSet.union s s' in
               trace_loops loops s trace2
       | [] ->
            loops, s

   let trace_loops trace =
      let loops, _ = trace_loops SymbolTable.empty SymbolSet.empty trace in
         loops
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
