(*
 * Remove useless and almost-useless induction variables.
 * A variable is useless if it is never used.  An almost-useless
 * variable is used only in comparisons against loop-invariant
 * values.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_print
open Fir_state
open Fir_depth
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_type
open Fir_alias_util
open Fir_alias_print

open Fir_alias_pre_cse

module Pos = MakePos (struct let name = "Fir_alias_pre_useless" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Test if a binop is a comparison.
 *)
let is_compare_binop op =
   match op with
      (* Non-trivial operators *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

    | PlusRawIntOp   _
    | MinusRawIntOp  _
    | MulRawIntOp    _
    | DivRawIntOp    _
    | RemRawIntOp    _
    | SlRawIntOp     _
    | SrRawIntOp     _
    | AndRawIntOp    _
    | OrRawIntOp     _
    | XorRawIntOp    _
    | MaxRawIntOp    _
    | MinRawIntOp    _

    | RawSetBitFieldOp _

    | PlusPointerOp _

    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

    | ATan2FloatOp _
    | PowerFloatOp _
    | LdExpFloatIntOp _ ->
         false

      (* Trivial operators *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp

    | EqRawIntOp     _
    | NeqRawIntOp    _
    | LtRawIntOp     _
    | LeRawIntOp     _
    | GtRawIntOp     _
    | GeRawIntOp     _
    | CmpRawIntOp    _

    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

    | EqEqOp _
    | NeqEqOp _ ->
         true

(*
 * A multiplier is "trivial" if it is 1.
 *)
let emul_atom_is_trivial a =
   match a with
      AtomInt i
    | AtomEnum (_, i) ->
         i = 1
    | AtomRawInt i ->
         Rawint.is_one i
    | _ ->
         false

let emul_is_trivial e =
   match e with
      ETConst a ->
         emul_atom_is_trivial a
    | _ ->
         false

let eadd_is_trivial e =
   match e with
      ETConst (AtomInt 0)
    | ETConst (AtomEnum (_, 0)) ->
         true
    | ETConst (AtomRawInt i) when Rawint.is_zero i ->
         true
    | _ ->
         false

(*
 * Get the depth of an etree.
 * It should be a constant or variable.
 *)
let rec depth_of_etree gvenv pos e =
   let pos = string_pos "depth_of_etree" pos in
      match e with
         ETConst _ ->
            pred_depth zero_depth
       | ETVar v ->
            let ac, _ = gvenv_lookup_value gvenv pos v in
               depth_of_aclass ac
       | ETUnop _
       | ETBinop _ ->
            raise (FirException (pos, StringFormatError ("not a variable", (fun buf -> pp_print_etree_core buf e))))

(************************************************************************
 * STEP 1: COLLECT NON-USELESS VARS
 ************************************************************************)

(*
 * Add the vars as being non-useless.
 *)
let rec set_etree vars e =
   match e with
      ETConst _ ->
         vars
    | ETVar v ->
         SymbolSet.add vars v
    | ETUnop (_, e) ->
         set_etree vars e
    | ETBinop (_, e1, e2) ->
         let vars = set_etree vars e1 in
         let vars = set_etree vars e2 in
            vars

let set_etrees vars args =
   List.fold_left set_etree vars args

(*
 * Add an allocation operator.
 *)
let set_alloc_op vars op =
   match op with
      AllocTuple (_, _, _, args)
    | AllocUnion (_, _, _, _, args)
    | AllocArray (_, args) ->
         set_etrees vars args
    | AllocDTuple (_, _, a, args) ->
         set_etree (set_etrees vars args) a
    | AllocVArray (_, _, e1, e2) ->
         set_etree (set_etree vars e1) e2
    | AllocMalloc (_, e) ->
         set_etree vars e
    | AllocFrame _ ->
         vars

(*
 * Add the predicate.
 *)
let set_pred vars pred =
   match pred with
      IsMutable e ->
         set_etree vars e
    | Reserve (e1, e2)
    | ElementCheck (_, _, e1, e2) ->
         set_etree (set_etree vars e1) e2
    | BoundsCheck (_, e1, e2, e3) ->
         set_etree (set_etree (set_etree vars e1) e2) e3

(*
 * Find vars used in non-trivial expressions.
 *)
let rec collect_etree vars e =
   match e with
      ETConst _
    | ETVar _ ->
         vars
    | ETUnop (op, e) ->
         collect_unop vars op e
    | ETBinop (op, e1, e2) ->
         collect_binop vars op e1 e2

and collect_unop vars op e =
   set_etree vars e

and collect_binop vars op e1 e2 =
   if is_compare_binop op then
      let vars = collect_etree vars e1 in
      let vars = collect_etree vars e2 in
         vars
   else
      let vars = set_etree vars e1 in
      let vars = set_etree vars e2 in
         vars

(*
 * HTree collection.
 *)
let collect_htree vars h =
   match h with
      HTETree e ->
         collect_etree vars e
    | HTAlloc (_, op) ->
         set_alloc_op vars op
    | HTSubscript (_, _, _, e1, e2) ->
         let vars = set_etree vars e1 in
         let vars = set_etree vars e2 in
            vars
    | HTSetSubscript (_, _, _, e1, e2, e3) ->
         let vars = set_etree vars e1 in
         let vars = set_etree vars e2 in
         let vars = set_etree vars e3 in
            vars
    | HTMemcpy (_, _, e1, e2, e3, e4, e5) ->
         let vars = set_etree vars e1 in
         let vars = set_etree vars e2 in
         let vars = set_etree vars e3 in
         let vars = set_etree vars e4 in
         let vars = set_etree vars e5 in
            vars
    | HTExt (_, _, _, _, _, _, args) ->
         set_etrees vars args
    | HTAssert (_, pred) ->
         set_pred vars pred
    | HTDepends _ ->
         vars

(*
 * Collect the set of vars that are used in non-trivial expressions.
 *)
let collect_prog gvenv =
   let vars =
      SymbolTable.fold (fun vars _ (ac, _) ->
            match dest_aclass_core ac with
               AliasInduction (_, _, _, _, _, _, _, _, base) ->
                  set_etree vars base
             | AliasAlias (_, e, _, _, _, _) ->
                  set_etree vars e
             | AliasExp (_, h) ->
                  collect_htree vars h) SymbolSet.empty gvenv.gvenv_values
   in
      if debug debug_alias then
         begin
            Format.printf "@[<b 3>*** FIR: pre useless: non-useless vars:";
            SymbolSet.iter (fun v ->
                  Format.printf "@ %a" pp_print_symbol v) vars;
            Format.printf "@]@."
         end;
      vars

(************************************************************************
 * STEP 2: COLLECT ALL INDUCTION VARS
 ************************************************************************)

(*
 * Get a table of all induction vars for each loop.
 *)
let collect_loops gvenv =
   SymbolTable.fold (fun loops _ (ac, _) ->
         match dest_aclass_core ac with
            AliasInduction (_, v, g, _, _, _, _, _, _) ->
               SymbolTable.filter_add loops g (fun table ->
                     let table =
                        match table with
                           Some table ->
                              table
                         | None ->
                              SymbolTable.empty
                     in
                        SymbolTable.add table v ac)
          | AliasAlias _
          | AliasExp _ ->
               loops) SymbolTable.empty gvenv.gvenv_values

(************************************************************************
 * STEP 3: CONVERT COMPARISONS
 ************************************************************************)

(*
 * Test is a variable is useless.
 *)
let is_useless_var vars v =
   not (SymbolSet.mem vars v)

(*
 * Find a related induction var that is not useless, and
 * whose multiplier are related integers.
 *)
let find_related_induction_var lenv vars loops pos g v1 mul1 add1 =
   let table =
      try SymbolTable.find loops g with
         Not_found ->
            SymbolTable.empty
   in
      SymbolTable.fold (fun entry v2 ac ->
            match entry with
               Some _ ->
                  entry
             | None ->
                  match dest_aclass_core ac with
                     AliasInduction (depth, v2, _, op, v3, mul2, add2, _, _) ->
                        (match op with
                            IntClass
                          | RawIntClass _ ->
                               if is_useless_var vars v2 || not (Symbol.eq v3 v1) || not (SymbolTable.mem lenv v2) then
                                  None
                               else if emul_is_trivial mul1 then
                                  let add1 = negate_etree op add1 in
                                  let add2 = add_etree op add2 add1 in
                                     Some (v2, mul2, add2)
                               else if mul1 = mul2 then
                                  let mul2 = etree_of_int op 1 in
                                  let add1 = negate_etree op add1 in
                                  let add2 = add_etree op add2 add1 in
                                     Some (v2, mul2, add2)
                               else
                                  None
                          | _ ->
                               None)
                   | _ ->
                        raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac))))) None table

(*
 * Try to rewrite a comparison to use a non-useless induction var.
 *)
let useless_compare_op gvenv aenv lenv vars loops pos e1 e2 =
   let pos = string_pos "useless_compare_op" pos in
      match e1 with
         ETVar v1 when is_useless_var vars v1 ->
            let ac, h = gvenv_lookup_value gvenv pos v1 in
               (match dest_aclass_core ac with
                   AliasInduction (depth, _, g, op, v2, mul, add, _, _) ->
                      (match find_related_induction_var lenv vars loops pos g v2 mul add with
                          Some (v3, mul, add) ->
                             (* We found a related var, so rewrite the other expression *)
                             let e2 = mul_etree op e2 mul in
                             let e2 = add_etree op e2 add in
                             let gvenv, _, e2 = cse_normalize_etree gvenv aenv pos depth e2 in
                             let lenv = SymbolTable.remove lenv v1 in
                                gvenv, lenv, vars, ETVar v3, e2
                        | None ->
                             (* No related induction var, so make this one non-useless *)
                             let vars = SymbolSet.add vars v1 in
                                gvenv, lenv, vars, e1, e2)
                 | _ ->
                      (* Not an induction var, so make it non-useless *)
                      let vars = SymbolSet.add vars v1 in
                         gvenv, lenv, vars, e1, e2)
       | _ ->
            gvenv, lenv, vars, e1, e2

(*
 * Try to rewrite a comparison to use a non-useless induction var.
 *)
let useless_compare gvenv aenv lenv vars loops pos loc v depth op e1 e2 =
   let pos = string_pos "useless_compare" pos in
   let depth1 = depth_of_etree gvenv pos e1 in
   let depth2 = depth_of_etree gvenv pos e2 in
   let _ =
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>Useless compare:@ %a" pp_print_symbol v;
            Format.printf "[%a](%a, %a) = %a" (**)
               pp_print_depth depth
               pp_print_depth depth1
               pp_print_depth depth2
               pp_print_etree_core e1;
            Format.printf " %s@ %a" (**)
               (string_of_binop op)
               pp_print_etree_core e2;
            Format.printf "@]@."
         end
   in
   let gvenv, lenv, vars, e1, e2 =
      if lt_depth depth2 depth1 then
         useless_compare_op gvenv aenv lenv vars loops pos e1 e2
      else if lt_depth depth1 depth2 then
         let gvenv, lenv, vars, e2, e1 = useless_compare_op gvenv aenv lenv vars loops pos e2 e1 in
            gvenv, lenv, vars, e1, e2
      else
         gvenv, lenv, vars, e1, e2
   in
   let h = HTETree (ETBinop (op, e1, e2)) in
   let ac = make_aclass loc (AliasExp (depth, h)) in
   let aenv = SymbolTable.add aenv v ac in
   let gvenv = gvenv_add_value gvenv v ac (Some (make_htree loc h)) in
      gvenv, aenv, lenv, vars

(*
 * Look for binops.
 *)
let useless_aclass gvenv aenv lenv vars loops v ac h =
   let pos = string_pos "useless_aclass" (var_exp_pos v) in
   let loc = loc_of_aclass ac in
      match dest_aclass_core ac with
         AliasExp (depth, HTETree (ETBinop (op, e1, e2)))
         when is_compare_binop op ->
            useless_compare gvenv aenv lenv vars loops pos loc v depth op e1 e2
       | _ ->
            gvenv, aenv, lenv, vars

(*
 * Scan the table for comparisons.
 *)
let useless_gvenv gvenv aenv lenv vars loops =
   let gvenv, _, lenv, vars =
      SymbolTable.fold (fun (gvenv, aenv, lenv, vars) v (ac, h) ->
            useless_aclass gvenv aenv lenv vars loops v ac h) (gvenv, aenv, lenv, vars) gvenv.gvenv_values
   in
   let lenv =
      SymbolTable.fold (fun lenv v _ ->
            if is_useless_var vars v then
               SymbolTable.remove lenv v
            else
               lenv) lenv gvenv.gvenv_values
   in
      gvenv, lenv, vars

(************************************************************************
 * GLOBAL FUNCTION
 ************************************************************************)

(*
 * Print the loop table.
 *)
let pp_print_loop_table lenv vars buf =
   SymbolTable.iter (fun v (step, base) ->
         Format.fprintf buf "@ @[<v 3>";
         pp_print_symbol buf v;
         if is_useless_var vars v then
            Format.fprintf buf " (useless)";
         Format.fprintf buf "@ @[<hv 3>base =@ %a" pp_print_etree base;
         Format.fprintf buf "@]@ @[<hv 3>step =@ %a" pp_print_etree step;
         Format.fprintf buf "@]@]") lenv

let useless_prog gvenv lenv =
   let aenv = SymbolTable.map fst gvenv.gvenv_values in
   let vars = collect_prog gvenv in
   let loops = collect_loops gvenv in
   let gvenv, lenv, vars = useless_gvenv gvenv aenv lenv vars loops in
      if debug debug_alias then
         begin
            Format.printf "@[<v 3>*** FIR: alias after useless induction removal:@ @[<v 3>Aliases:%a" (**)
               pp_print_alias_htree_table gvenv.gvenv_values;
            Format.printf "@]@ @[<v 3>Loops:%t" (**)
               (pp_print_loop_table lenv vars);
            Format.printf "@]@]@."
         end;
      gvenv, lenv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
