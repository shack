(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_algebra

open Fir_depth
open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

module Pos = MakePos (struct let name = "Fir_alias_pre_ind" end)
open Pos

(*
 * At this point, we have a solved venv.  That means:
 *    1. For each function, we have a call record with
 *       arguments, heap arguments, inner calls, and outer
 *       calls.
 *    2. The venv_subst contains a heap definition for every
 *       veriable mentioned in the program.
 *
 * In this step, we classify the induction variables.
 * To compute inducation variables, we scan each loop header.
 * If a loop parameter is a variable, and the inner value is
 * a sum of that variable and a loop invariant value, then
 * we have an induction variable.
 *)

(*
 * Create a new induction var.
 * We make up a new name for the var.
 *)
let ind_var venv ienv loc depth g v op step base =
   let v2 = new_symbol v in
   let ic = int_class_of_binop op in
   let xc = loc, depth, v2, g, ic, step, base in
   let mul = etree_of_int ic 1 in
   let add = etree_of_int ic 0 in
   let ac = make_aclass loc (AliasInduction (depth, v2, g, ic, v2, mul, add, step, base)) in
   let ienv = ienv_add_var ienv v2 ac in
      ienv_add_ind_var ienv v xc

(*
 * Find all the induction vars in the loop arguments.
 *)
let ind_arg venv ienv pos loc depth g arg oarg iarg =
   let pos = string_pos "ind_arg" pos in
   let depth = pred_depth depth in
   let _ =
      if debug debug_alias then
         begin
            let arg' = expand_var_opt venv arg in
            let arg = expand_vclass venv pos arg in
            let oarg = expand_vclass venv pos oarg in
            let iarg = expand_vclass venv pos iarg in
               Format.printf "@[<hv 3>Induction variable analysis[%a]:@ @[<hv 3>arg =@ %a" (**)
                  pp_print_depth depth
                  pp_print_vclass arg;
               (match arg' with
                   Some (depth, v) ->
                      Format.printf "@ <var>[%a] = %a" pp_print_depth depth pp_print_symbol v
                 | None ->
                      ());
               Format.printf "@]@ @[<hv 3>oarg =@ %a" pp_print_vclass oarg;
               Format.printf "@]@ @[<hv 3>iarg =@ %a" pp_print_vclass iarg;
               Format.printf "@]@]@."
         end
   in
      match expand_var_opt venv arg with
         Some (depth1, v1)
         when eq_depth depth1 depth ->
            let depth2 = depth_of_vclass iarg in
               if eq_depth depth2 depth then
                  let _, e2 = etree_of_vclass_core venv pos iarg in
                     (match etree_is_sum_of_var_core e2 v1 with
                         Some (op, e2) ->
                            let base =
                               match dest_vclass_core oarg with
                                  VarExp (_, e)
                                | VarAlias (_, e, _, _, _, _) ->
                                     e
                            in
                               ind_var venv ienv loc depth g v1 op e2 base
                       | None ->
                            ienv)
               else
                  ienv
       | _ ->
            ienv

let ind_args venv ienv pos loc depth g args oargs iargs =
   let pos = string_pos "ind_args" pos in
      Mc_list_util.fold_left3 (fun ienv arg oarg iarg ->
            ind_arg venv ienv pos loc depth g arg oarg iarg) ienv args oargs iargs

(*
 * Find induction vars in array arguments.
 *)
let ind_iclass venv ienv loc depth g ic oic iic =
   let pos = string_pos "ind_iclass" (var_exp_pos g) in
   let arg = vclass_of_iclass ic in
   let oarg = vclass_of_iclass oic in
   let iarg = vclass_of_iclass iic in
      ind_arg venv ienv pos loc depth g arg oarg iarg

(*
 * Scan the alias environment for induction vars.
 *)
let rec ind_int_fields venv ienv loc depth g fields ofields ifields =
   match fields, ofields, ifields with
      (i, ic) :: fields', (oi, oic) :: ofields', (ii, iic) :: ifields' ->
         let mi = min (min i oi) ii in
            if i = oi && i = ii then
               let ienv = ind_int_fields venv ienv loc depth g fields' ofields' ifields' in
                  ind_iclass venv ienv loc depth g ic oic iic
            else if i = mi then
               ind_int_fields venv ienv loc depth g fields' ofields ifields
            else if oi = mi then
               ind_int_fields venv ienv loc depth g fields ofields' ifields
            else
               ind_int_fields venv ienv loc depth g fields ofields ifields'
    | _ ->
         ienv

let ind_lab_subfields venv ienv loc depth g subfields osubfields isubfields =
   SymbolTable.fold (fun ienv subfield ic ->
         try
            let oic = SymbolTable.find osubfields subfield in
            let iic = SymbolTable.find isubfields subfield in
               ind_iclass venv ienv loc depth g ic oic iic
         with
            Not_found ->
               ienv) ienv subfields

let ind_lab_fields venv ienv loc depth g fields ofields ifields =
   SymbolTable.fold (fun ienv field (subfields, _) ->
         try
            let osubfields, _ = SymbolTable.find ofields field in
            let isubfields, _ = SymbolTable.find ifields field in
               ind_lab_subfields venv ienv loc depth g subfields osubfields isubfields
         with
            Not_found ->
               ienv) ienv fields

(*
 * Find induction vars in the block.
 * BUG: we could try to examine var fields for induction vars,
 * but we have to figure out whether the index is loop-invariant.
 *)
let ind_block venv ienv loc depth g block oblock iblock =
   let { block_int_fields = int_fields;
         block_lab_fields = lab_fields
       } = block
   in
   let { block_int_fields = oint_fields;
         block_lab_fields = olab_fields
       } = oblock
   in
   let { block_int_fields = iint_fields;
         block_lab_fields = ilab_fields
       } = iblock
   in
   let ienv = ind_int_fields venv ienv loc depth g int_fields oint_fields iint_fields in
   let ienv = ind_lab_fields venv ienv loc depth g lab_fields olab_fields ilab_fields in
      ienv

(*
 * Find all induction vars in the alias environment.
 *)
let ind_aenv venv ienv depth g aenv oaenv iaenv =
   let { aenv_blocks = blocks  } = aenv in
   let { aenv_blocks = oblocks } = oaenv in
   let { aenv_blocks = iblocks } = iaenv in
      SymbolTable.fold (fun ienv v block ->
            try
               let loc = block.block_loc in
               let oblock = SymbolTable.find oblocks v in
               let iblock = SymbolTable.find iblocks v in
                  ind_block venv ienv loc depth g block oblock iblock
            with
               Not_found ->
                  ienv) ienv blocks

(*
 * Find all the induction vars in the call.
 * If the call is not a loop, ignore it.
 *)
let ind_call venv ienv pos g call =
   let pos = string_pos "ind_call" pos in
   let { fun_vars = vars;
         fun_depth = depth
       } = venv_lookup_fun venv pos g
   in
   let { call_loc = loc;
         call_info = info;
         call_loop_outer = outer_loop;
         call_loop_inner = inner_loop
       } = call
   in
      (* Get the info *)
      match info, outer_loop, inner_loop with
         Some (_, aenv, args, _), Some (_, oaenv, oargs, _), Some (_, iaenv, iargs, _) ->
            let ienv = ind_args venv ienv pos loc depth g args oargs iargs in
            let ienv = ind_aenv venv ienv depth g aenv oaenv iaenv in
               ienv
       | _ ->
            ienv

(*
 * Find all the induction vars in the venv.
 *)
let ind_prog venv =
   let ienv =
      SymbolTable.fold (fun ienv g call ->
            let pos = string_pos "ind_prog" (var_exp_pos g) in
               ind_call venv ienv pos g call) ienv_empty venv.venv_calls
   in
      if debug debug_alias then
         begin
            Format.printf "@[<v 0>@[<v 3>*** FIR: after induction variable analysis:@ %a" pp_print_ienv ienv;
            Format.printf "@]@ @[<v 3>Variable environment:@ %a" pp_print_venv venv;
            Format.printf "@]@]@."
         end;
      ienv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
