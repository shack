(*
 * Alias printing functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_heap
open Fir_algebra

open Fir_alias
open Fir_alias_type

val pp_print_vclass : Format.formatter -> vclass -> unit
val pp_print_iclass : Format.formatter -> iclass -> unit
val pp_print_aclass : Format.formatter -> aclass -> unit

val pp_print_venv : Format.formatter -> venv -> unit
val pp_print_ienv : Format.formatter -> ienv -> unit
val pp_print_aenv : Format.formatter -> alias_env -> unit

val pp_print_block : Format.formatter -> block -> unit

val pp_print_loop_table : Format.formatter -> lenv -> unit
val pp_print_call_entry : Format.formatter -> label -> call_entry -> unit
val pp_print_call_table : Format.formatter -> (etree list * etree SymbolTable.t) SymbolTable.t -> unit
val pp_print_alias_table : Format.formatter -> aclass SymbolTable.t -> unit
val pp_print_alias_etree_table : Format.formatter -> (aclass * etree) SymbolTable.t -> unit
val pp_print_alias_htree_table : Format.formatter -> (aclass * htree option) SymbolTable.t -> unit
val pp_print_alias_info : Format.formatter -> Fir_alias.t -> unit

(*!
 * @docoff
 *)
