(*
 * Alias argument and environment unification.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir
open Fir_pos
open Fir_logic.FirLogic

open Fir_depth
open Fir_alias_type

exception UnifyError

(*
 * Call unification.
 *)
val unify_call : venv -> pos -> var -> depth -> (var * ty) list -> call -> call * pred * venv * alias_env * vclass list

(*
 * These operations are used in Fir_alias_mem.
 *)
val unify_block_vfield : venv -> alias_env -> pos -> vclass -> iclass -> venv * alias_env * iclass
val unify_block_ifield : venv -> alias_env -> pos -> iclass -> iclass -> venv * alias_env * iclass

val unify_block_int_fields : venv -> alias_env -> pos -> (int * iclass) list -> (int * iclass) list -> venv * alias_env * (int * iclass) list

val unify_induction_field : venv -> pos -> vclass -> iclass -> iclass

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
