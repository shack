(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol
open Location

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_algebra
open Fir_logic.FirLogic

open Fir_depth
open Fir_alias_env
open Fir_alias_type
open Fir_alias_util
open Fir_alias_subst
open Fir_alias_print

module Pos = MakePos (struct let name = "Fir_alias_unify" end)
open Pos

(************************************************************************
 * EQUALITY
 ************************************************************************)

(*
 * Compare two values.
 *)
let equal_vclass vc1 vc2 =
   match dest_vclass_core vc1, dest_vclass_core vc2 with
      VarExp (d1, e1), VarExp (d2, e2) ->
         eq_depth d1 d2 && e1 = e2
    | VarExp _, _
    | _, VarExp _ ->
         false
    | VarAlias (d1, e1, o1, v1, s1, l1), VarAlias (d2, e2, o2, v2, s2, l2) ->
         eq_depth d1 d2 && Symbol.eq v1 v2 && SymbolSet.equal s1 s2 && SymbolSet.equal l1 l2 && e1 = e2 && o1 = o2

let equal_iclass ic1 ic2 =
   match ic1, ic2 with
      IVarUnknown (vc1, v1), IVarUnknown (vc2, v2) ->
         Symbol.eq v1 v2 && equal_vclass vc1 vc2
    | IVarVar (vc1, v1), IVarVar (vc2, v2) ->
         Symbol.eq v1 v2 && equal_vclass vc1 vc2
    | IVarInd (vc1, op1, v1, e1), IVarInd (vc2, op2, v2, e2) ->
         op1 = op2 && Symbol.eq v1 v2 && e1 = e2 && equal_vclass vc1 vc2
    | IVarExp vc1, IVarExp vc2 ->
         equal_vclass vc1 vc2
    | _ ->
         false

let rec equal_int_fields fields1 fields2 =
   match fields1, fields2 with
      (i1, ic1) :: fields1, (i2, ic2) :: fields2 ->
         i1 = i2 && equal_iclass ic1 ic2 && equal_int_fields fields1 fields2
    | [], [] ->
         true
    | _ ->
         false

let equal_default default1 default2 =
   let { default_alias = alias1;
         default_heap = heap1
       } = default1
   in
   let { default_alias = alias2;
         default_heap = heap2
       } = default2
   in
      SymbolSet.equal alias1 alias2 && SymbolSet.equal heap1 heap2

let equal_lab_subfields subfields1 subfields2 =
   SymbolTable.forall2 equal_iclass subfields1 subfields2

let equal_lab_fields fields1 fields2 =
   SymbolTable.forall2 (fun (subfields1, default1) (subfields2, default2) ->
         equal_lab_subfields subfields1 subfields2 && equal_default default1 default2) fields1 fields2

let equal_var_fields fields1 fields2 =
   ETreeTable.forall2 equal_iclass fields1 fields2

let equal_blocks block1 block2 =
   let { block_depth      = depth1;
         block_mutable    = mutable1;
         block_length     = length1;
         block_default    = default1;
         block_int_fields = int_fields1;
         block_lab_fields = lab_fields1;
         block_var_fields = var_fields1
       } = block1
   in
   let { block_depth      = depth2;
         block_mutable    = mutable2;
         block_length     = length2;
         block_default    = default2;
         block_int_fields = int_fields2;
         block_lab_fields = lab_fields2;
         block_var_fields = var_fields2
       } = block2
   in
      (eq_depth depth1 depth2)
      && (mutable1 = mutable2)
      && (length1 = length2)
      && equal_default default1 default2
      && equal_int_fields int_fields1 int_fields2
      && equal_lab_fields lab_fields1 lab_fields2
      && equal_var_fields var_fields1 var_fields2

let equal_aenv aenv1 aenv2 =
   let { aenv_blocks = blocks1;
         aenv_subst = subst1
       } = aenv1
   in
   let { aenv_blocks = blocks2;
         aenv_subst = subst2
       } = aenv2
   in
      SymbolTable.forall2 equal_blocks blocks1 blocks2
      && SymbolTable.forall2 SymbolSet.equal subst1 subst2

let rec equal_args args1 args2 =
   match args1, args2 with
      arg1 :: args1, arg2 :: args2 ->
         equal_vclass arg1 arg2 && equal_args args1 args2
    | [], [] ->
         true
    | _ ->
         false

let equal_ps_args ps_args1 ps_args2 =
   SymbolTable.forall2 (fun (d1, h1) (d2, h2) ->
         d1 = d2 && HTreeCompare.compare h1 h2 = 0) ps_args1 ps_args2

let equal_call_info info1 info2 =
   match info1, info2 with
      Some (_, aenv1, args1, ps_args1), Some (_, aenv2, args2, ps_args2) ->
         equal_args args1 args2 && equal_aenv aenv1 aenv2 && equal_ps_args ps_args1 ps_args2
    | None, None ->
         true
    | _ ->
         false

(************************************************************************
 * UNIFICATION
 ************************************************************************)

(*
 * For handling unification errors.
 * This is an internal exception only: it should
 * not propagate beyond this file.
 *)
exception UnifyError

(*
 * "Flush" the heap in a default.  This means to make the heap dependent on
 * a single symbol.  Add the existing symbols as a dependency.
 * Get the symbol for the arg, then add the depndency.
 *)
let v_mutable = new_symbol_string "mutable"
let v_length = new_symbol_string "length"
let v_default = new_symbol_string "default"
let v_reserve = new_symbol_string "reserve"
let v_alloc = new_symbol_string "alloc"
let v_field = new_symbol_string "field"

let unify_flush_heap venv cenv loc frame depth names f heap =
   let venv, v = venv_new_name venv names in
   let cenv = cenv_add_param cenv v in
   let _ =
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>unify_flush_heap:@ frame = %a" pp_print_symbol frame;
            Format.printf ";@ label = %a" pp_print_symbol f;
            Format.printf ";@ from function = %a" pp_print_symbol cenv.cenv_fun;
            Format.printf ";@ @[<hv 3>name = %a" NameCompare.pp_print names;
            Format.printf ";@]@ ps_var = %a" pp_print_symbol v;
            Format.printf ";@ @[<hv 3>depends =";
            SymbolSet.iter (fun v ->
                  Format.printf "@ %a" pp_print_symbol v) heap;
            Format.printf "@]@]@."
         end
   in
   let cenv = cenv_add_arg cenv f v depth (make_htree loc (HTDepends heap)) in
      venv, cenv, SymbolSet.singleton v

let unify_flush_default venv cenv loc frame depth names f default =
   let { default_heap = heap } = default in
   let names = NameVar v_default :: names in
   let venv, cenv, heap = unify_flush_heap venv cenv loc frame depth names f heap in
   let default = { default with default_heap = heap } in
      venv, cenv, default

let unify_flush_lab_fields venv cenv loc frame depth names f fields =
   let (venv, cenv), fields =
      SymbolTable.fold_map (fun (venv, cenv) field (subfields, default) ->
            let names = NameVar v_field :: NameVar field :: names in
            let venv, cenv, default = unify_flush_default venv cenv loc frame depth names f default in
               (venv, cenv), (subfields, default)) (venv, cenv) fields
   in
      venv, cenv, fields

let unify_flush_block venv cenv names f frame block =
   let { block_depth = depth;
         block_default = default;
         block_mutable_heap = mutable_heap;
         block_length_heap = length_heap;
         block_lab_fields = lab_fields;
         block_loc = loc
       } = block
   in
   let names = NameVar frame :: names in
   let mutable_names = NameVar v_mutable :: names in
   let length_names = NameVar v_length :: names in
   let venv, cenv, mutable_heap = unify_flush_heap venv cenv loc frame depth mutable_names f mutable_heap in
   let venv, cenv, length_heap = unify_flush_heap venv cenv loc frame depth length_names f length_heap in
   let venv, cenv, default = unify_flush_default venv cenv loc frame depth names f default in
   let venv, cenv, lab_fields = unify_flush_lab_fields venv cenv loc frame depth names f lab_fields in
   let block =
      { block with block_default = default;
                   block_lab_fields = lab_fields;
                   block_mutable_heap = mutable_heap;
                   block_length_heap = length_heap
      }
   in
      venv, cenv, block

let unify_flush_aenv venv cenv f aenv =
   let { aenv_loc = loc;
         aenv_reserve_heap = reserve_heap;
         aenv_alloc_heap = alloc_heap;
         aenv_blocks = blocks
       } = aenv
   in
   let names = [NameVar cenv.cenv_fun] in
   let reserve_heap_names = NameVar v_reserve :: names in
   let venv, cenv, reserve_heap = unify_flush_heap venv cenv loc v_reserve zero_depth reserve_heap_names f reserve_heap in
   let alloc_heap_names = NameVar v_alloc :: names in
   let venv, cenv, alloc_heap = unify_flush_heap venv cenv loc v_alloc zero_depth alloc_heap_names f alloc_heap in
   let (venv, cenv), blocks =
      SymbolTable.fold_map (fun (venv, cenv) frame block ->
            let venv, cenv, block = unify_flush_block venv cenv names f frame block in
               (venv, cenv), block) (venv, cenv) blocks
   in
   let aenv =
      { aenv with aenv_reserve_heap = reserve_heap;
                  aenv_alloc_heap = alloc_heap;
                  aenv_blocks = blocks
      }
   in
      venv, cenv, aenv

(*
 * Unify the default sets.  The aliases just get combined.
 * The heaps are a special case.
 *)
let unify_default default1 default2 =
   let { default_alias = alias1 } = default1 in
   let { default_alias = alias2 } = default2 in
      { default1 with default_alias = SymbolSet.union alias1 alias2 }

(*
 * If two alias expressions are different,
 * then the new expression is the new var,
 * with offset 0.
 *)
let unify_alias_expr venv pos v e1 e2 off1 off2 =
   let pos = string_pos "unify_alias_expr" pos in
   let e1 = expand_etree_core venv pos e1 in
   let e2 = expand_etree_core venv pos e2 in
      if equal_etree_core e1 e2 then
         e1, off1
      else
         ETVar v, et_zero_subscript

(*
 * Unify two variable classes.
 *)
let unify_field field_of_vfield unknown_field venv cenv pos v vc1 vc2 =
   let pos = string_pos "unify_vfield" pos in
   let loc = loc_of_vclass vc1 in
      match dest_vclass_core vc1, dest_vclass_core vc2 with
         VarExp (d1, e1), VarExp (d2, e2)
         when e1 = e2 ->
            (* Identical values: don't do anything *)
            let ic = field_of_vfield vc1 in
               venv, cenv, ic

       | VarExp _, _
       | _, VarExp _ ->
            (* Non-identical values: become the var itself *)
            unknown_field venv cenv v

         (* Alias classes *)
       | VarAlias (depth1, e1, off1, v1, s1, l1), VarAlias (depth2, e2, off2, v2, s2, l2) ->
            let e, off = unify_alias_expr venv pos v e1 e2 off1 off2 in
            let d = max_depth depth1 depth2 in
            let s = SymbolSet.union s1 s2 in
            let l = SymbolSet.union l1 l2 in
               if Symbol.eq v1 v2 then
                  (* The two must-alias classes are the same, so just combine the may-alias sets *)
                  let vc = VarAlias (d, e, off, v1, s, l) in
                  let ic = field_of_vfield (make_vclass loc vc) in
                     venv, cenv, ic
               else
                  (*
                   * The two must-alias classes are different, so it must be that
                   * the arrays have been swapped or newly allocated.  Try to be smart
                   * here.  We can unify the two must-alias sets, and assign to the alias
                   * class for v.
                   *)
                  let cenv = cenv_add_alias cenv v v1 v2 in
                  let s = SymbolSet.add s v1 in
                  let s = SymbolSet.add s v2 in
                  let s = SymbolSet.remove s v in
                  let vc = VarAlias (d, e, off, v, s, l) in
                  let ic = field_of_vfield (make_vclass loc vc) in
                     venv, cenv, ic

let unify_vfield venv cenv pos v vc1 vc2 =
   let loc = loc_of_vclass vc1 in
   let field_of_vfield vc =
      vc
   in
   let unknown_field venv cenv v =
      let vc = VarExp (cenv.cenv_depth, ETVar v) in
         venv, cenv, make_vclass loc vc
   in
   let venv, cenv, vc = unify_field field_of_vfield unknown_field venv cenv pos v vc1 vc2 in
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>unify_vfield:@ @[<hv 3>vc1 =@ ";
            pp_print_vclass Format.std_formatter vc1;
            Format.printf "@]@ @[<hv 3>vc2 =@ ";
            pp_print_vclass Format.std_formatter vc2;
            Format.printf "@]@ @[<hv 3>vc =@ ";
            pp_print_vclass Format.std_formatter vc;
            Format.printf "@]@]@."
         end;
      venv, cenv, vc

(*
 * Unify two memory fields.  If the two fields are
 * both vars, we pick one of them.
 *)
let unify_ifield venv cenv pos v ic1 ic2 =
   let loc = loc_of_iclass ic1 in
   let field_of_vfield vc =
      IVarExp vc
   in
   let unknown_field venv cenv v =
      let depth = cenv.cenv_depth in
      let vc = make_vclass loc (VarExp (depth, ETVar v)) in
      let ic = IVarUnknown (vc, v) in
      let venv = venv_add_var venv v vc in
         venv, cenv, ic
   in
   let pos = string_pos "unify_ifield" pos in
      match ic1, ic2 with
         IVarVar (vc1, v1), IVarVar (vc2, v2) ->
            (* Pick the smaller one *)
            if Symbol.compare v1 v2 < 0 then
               venv, cenv, ic1
            else
               venv, cenv, ic2
       | IVarInd (vc1, op1, v1, e1), IVarInd (vc2, op2, v2, e2)
         when expand_etree_core venv pos e1 = expand_etree_core venv pos e2 ->
            if Symbol.compare v1 v2 < 0 then
               venv, cenv, ic1
            else
               venv, cenv, ic2
       | _ ->
            let vc1 = vclass_of_iclass ic1 in
            let vc2 = vclass_of_iclass ic2 in
               unify_field field_of_vfield unknown_field venv cenv pos v vc1 vc2

let unify_ifield venv cenv pos v ic1 ic2 =
   let venv, cenv, ic = unify_ifield venv cenv pos v ic1 ic2 in
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>unify_ifield:@ @[<hv 3>ic1 =@ ";
            pp_print_iclass Format.std_formatter ic1;
            Format.printf "@]@ @[<hv 3>ic2 =@ ";
            pp_print_iclass Format.std_formatter ic2;
            Format.printf "@]@ @[<hv 3>ic =@ ";
            pp_print_iclass Format.std_formatter ic;
            Format.printf "@]@]@."
         end;
      venv, cenv, ic

(*
 * Unify an "inner" field.  This means the field is
 *   1. Not present in the outer set
 *   2. Present in the inner set
 *
 * We can keep the field if it is a plain var or an induction var.
 *)
let unify_inner_field ic =
   match ic with
      IVarVar _ ->
         ic
    | IVarInd (vc, _, v, _) ->
         let depth = depth_of_vclass vc in
         let loc = loc_of_vclass vc in
         let vc = make_vclass loc (VarExp (depth, ETVar v)) in
            IVarVar (vc, v)
    | IVarUnknown _
    | IVarExp _ ->
         raise UnifyError

(*
 * Merge two field lists.
 * We include as many entries as possible.
 *)
let rec unify_int_fields venv cenv pos names inner fields1 fields2 =
   match fields1, fields2 with
      field1 :: fields1', field2 :: fields2' ->
         let i1, ic1 = field1 in
         let i2, ic2 = field2 in
            if i1 = i2 then
               (* We found a matching field: try to unify it *)
               let venv, cenv, fields = unify_int_fields venv cenv pos names inner fields1' fields2' in
                  try
                     let venv, v = venv_new_name venv (NameInt i1 :: names) in
                     let venv, cenv, ic = unify_ifield venv cenv pos v ic1 ic2 in
                     let fields = (i1, ic) :: fields in
                        venv, cenv, fields
                  with
                     UnifyError ->
                        venv, cenv, fields
            else if i1 < i2 then
               (* The original field was lost: drop it *)
               unify_int_fields venv cenv pos names inner fields1' fields2
            else
               (* There is a new field: try and keep it *)
               let venv, cenv, fields = unify_int_fields venv cenv pos names inner fields1 fields2' in
                  if inner then
                     try
                        let ic = unify_inner_field ic2 in
                        let fields = (i2, ic) :: fields in
                           venv, cenv, fields
                     with
                        UnifyError ->
                           venv, cenv, fields
                  else
                     venv, cenv, fields
    | _ ->
         venv, cenv, []

(*
 * Unify the subfields in a field of a frame.
 * If the entry exists in both fields, try to unify it.
 * If it only exists in the second set, try to keep it.
 *)
let unify_sub_fields venv cenv pos names inner subfields1 subfields2 =
   let pos = string_pos "unify_sub_fields" pos in
   let venv, cenv, subfields1, subfields2 =
      SymbolTable.fold (fun (venv, cenv, subfields1, subfields2) subfield ic1 ->
            try
               (* The field exists in both sets; so try to unify it *)
               let ic2 = SymbolTable.find subfields2 subfield in
               let venv, v = venv_new_name venv (NameVar subfield :: names) in
               let venv, cenv, ic = unify_ifield venv cenv pos v ic1 ic2 in
               let subfields1 = SymbolTable.add subfields1 subfield ic in
               let subfields2 = SymbolTable.remove subfields2 subfield in
                  venv, cenv, subfields1, subfields2
            with
               Not_found
             | UnifyError ->
                  (* If it was a unification error, ignore the entry *)
                  venv, cenv, subfields1, subfields2) (venv, cenv, SymbolTable.empty, subfields2) subfields1
   in

   (* Try to keep fields from subfields2 *)
   let subfields =
      if inner then
         SymbolTable.fold (fun subfields subfield ic ->
               try
                  let ic = unify_inner_field ic in
                     SymbolTable.add subfields subfield ic
               with
                  UnifyError ->
                     subfields) subfields1 subfields2
      else
         subfields1
   in
      venv, cenv, subfields

(*
 * Unify the fields in a frame.
 * If the field entry exists in both sets, try to unify the subfields.
 * If it only exists in the second set, try to keep it.
 *)
let unify_lab_fields venv cenv pos depth names inner fields1 fields2 =
   let pos = string_pos "unify_lab_fields" pos in
   let venv, cenv, fields1, fields2 =
      SymbolTable.fold (fun (venv, cenv, fields1, fields2) field (subfields1, default1) ->
            try
               let subfields2, default2 = SymbolTable.find fields2 field in
               let names = NameVar field :: names in
               let venv, cenv, subfields = unify_sub_fields venv cenv pos names inner subfields1 subfields2 in
               let default = unify_default default1 default2 in
               let fields1 = SymbolTable.add fields1 field (subfields, default) in
               let fields2 = SymbolTable.remove fields2 field in
                  venv, cenv, fields1, fields2
            with
               Not_found ->
                  (* The field was not found in fields2, so ignore it *)
                  venv, cenv, fields1, fields2) (venv, cenv, SymbolTable.empty, fields2) fields1
   in

   (* Try to keep the fields from fields2 *)
   let fields =
      if inner then
         SymbolTable.fold (fun fields field (subfields, default) ->
               let subfields =
                  SymbolTable.fold (fun subfields subfield ic ->
                        try
                           let ic = unify_inner_field ic in
                              SymbolTable.add subfields subfield ic
                        with
                           UnifyError ->
                              subfields) SymbolTable.empty subfields
               in
                  SymbolTable.add fields field (subfields, default)) fields1 fields2
      else
         fields1
   in
      venv, cenv, fields

(*
 * Unify the variable fields.
 * If the field entry exists in both sets, try to unify it.
 * BUG: for now, we drop all inner fields.  We can keep them if
 * we can show that the index is loop-invariant (hard).
 *)
let unify_var_fields venv cenv pos names inner fields1 fields2 =
   let pos = string_pos "unify_var_fields" pos in
      ETreeTable.fold (fun (venv, cenv, fields) e ic1 ->
            try
               let ic2 = ETreeTable.find fields2 e in
               let venv, v = venv_new_name venv (NameETree e :: names) in
               let venv, cenv, ic = unify_ifield venv cenv pos v ic1 ic2 in
               let fields = ETreeTable.add fields e ic in
                  venv, cenv, fields
            with
               Not_found
             | UnifyError ->
                  (* The field was not found: drop it *)
                  venv, cenv, fields) (venv, cenv, ETreeTable.empty) fields1

(*
 * Unify two blocks.
 *)
let unify_block venv cenv pos names inner block1 block2 =
   let pos = string_pos "unify_block" pos in
   let { block_loc          = loc1;
         block_depth        = depth1;
         block_mutable      = mutable1;
         block_mutable_heap = mutable_heap1;
         block_length       = length1;
         block_length_heap  = length_heap1;
         block_default      = default1;
         block_int_fields   = int_fields1;
         block_lab_fields   = lab_fields1;
         block_var_fields   = var_fields1
       } = block1
   in
   let { block_depth        = depth2;
         block_mutable      = mutable2;
         block_mutable_heap = mutable_heap2;
         block_length       = length2;
         block_length_heap  = length_heap2;
         block_default      = default2;
         block_int_fields   = int_fields2;
         block_lab_fields   = lab_fields2;
         block_var_fields   = var_fields2
       } = block2
   in
   let depth = max_depth depth1 depth2 in
   let default = unify_default default1 default2 in
   let venv, cenv, int_fields = unify_int_fields venv cenv pos names inner int_fields1 int_fields2 in
   let venv, cenv, lab_fields = unify_lab_fields venv cenv pos depth names inner lab_fields1 lab_fields2 in
   let venv, cenv, var_fields = unify_var_fields venv cenv pos names inner var_fields1 var_fields2 in
   let block =
      { block_loc          = loc1;
        block_depth        = depth;
        block_mutable      = mutable1 && mutable2;
        block_mutable_heap = mutable_heap1;
        block_length       = canonicalize_etree_core pos (ETBinop (min_subscript_op, length1, length2));
        block_length_heap  = length_heap1;
        block_default      = default;
        block_int_fields   = int_fields;
        block_lab_fields   = lab_fields;
        block_var_fields   = var_fields
      }
   in
      venv, cenv, block

(*
 * Unify two alias environments.
 * For those entries that are in both tables, unify the entries.
 * For all the other entries, just add them as-is.
 *)
let unify_aenv venv cenv pos names inner aenv1 aenv2 =
   let pos = string_pos "unify_aenv" pos in
   let { aenv_loc = loc1;
         aenv_reserve_heap = reserve_heap1;
         aenv_alloc_heap = alloc_heap1;
         aenv_blocks = blocks1;
         aenv_subst = subst1
       } = aenv1
   in
   let { aenv_blocks = blocks2;
         aenv_subst = subst2
       } = aenv2
   in
   let venv, cenv, blocks1, blocks2 =
      SymbolTable.fold (fun (venv, cenv, blocks1, blocks2) v block1 ->
            try
               let block2 = SymbolTable.find blocks2 v in
               let names = NameVar v :: names in
               let venv, cenv, block = unify_block venv cenv pos names inner block1 block2 in
               let blocks1 = SymbolTable.add blocks1 v block in
               let blocks2 = SymbolTable.remove blocks2 v in
                  venv, cenv, blocks1, blocks2
            with
               Not_found ->
                  venv, cenv, blocks1, blocks2) (venv, cenv, blocks1, blocks2) blocks1
   in
   let blocks = SymbolTable.fold SymbolTable.add blocks1 blocks2 in
   let subst1, subst2 =
      SymbolTable.fold (fun (subst1, subst2) v s1 ->
            try
               let s2 = SymbolTable.find subst2 v in
               let s = SymbolSet.union s1 s2 in
               let subst1 = SymbolTable.add subst1 v s in
               let subst2 = SymbolTable.remove subst2 v in
                  subst1, subst2
            with
               Not_found ->
                  subst1, subst2) (subst1, subst2) subst1
   in
   let subst = SymbolTable.fold SymbolTable.add subst1 subst2 in
   let aenv =
      { aenv_loc = loc1;
        aenv_reserve_heap = reserve_heap1;
        aenv_alloc_heap = alloc_heap1;
        aenv_blocks = blocks;
        aenv_subst = subst
      }
   in
      venv, cenv, aenv

(*
 * Unify two argument lists.
 *)
let rec unify_args venv cenv pos vars args1 args2 =
   match vars, args1, args2 with
      (v, _) :: vars, arg1 :: args1, arg2 :: args2 ->
         let venv, cenv, args = unify_args venv cenv pos vars args1 args2 in
         let venv, cenv, arg = unify_vfield venv cenv pos v arg1 arg2 in
            venv, cenv, arg :: args
    | _ ->
         venv, cenv, args2

(*
 * Unify the heap arguments.
 *)
let bloc = bogus_loc "<Fir_alias_unify>"
let depends_empty = make_htree bloc (HTDepends SymbolSet.empty)

let unify_ps_args venv ps_vars ps_args1 ps_args2 =
   SymbolSet.fold (fun (venv, ps_args) v ->
         let depth1, ps_arg1 =
            try SymbolTable.find ps_args1 v with
               Not_found ->
                  zero_depth, depends_empty
         in
         let depth2, ps_arg2 =
            try SymbolTable.find ps_args2 v with
               Not_found ->
                  zero_depth, depends_empty
         in
         let cmp = HTreeCompare.compare ps_arg1 ps_arg2 in
         let loc = loc_of_htree ps_arg1 in
         let venv, ps_arg =
            if cmp = 0 then
               venv, ps_arg1
            else
               let venv = venv_remove_subst venv v in
                  venv, make_htree loc (HTDepends (SymbolSet.singleton v))
         in
         let ps_args = SymbolTable.add ps_args v (max_depth depth1 depth2, ps_arg) in
            venv, ps_args) (venv, SymbolTable.empty) ps_vars

(*
 * Unify all the calls.
 *)
let unify_call_entries venv cenv pos vars entries =
   let pos = string_pos "unify_call_entries" pos in
      SymbolTable.fold (fun (venv, cenv, info) g entry ->
            let { call_pred = pred2;
                  call_aenv = aenv2;
                  call_args = args2
                } = entry
            in
            let venv, cenv, aenv2 = unify_flush_aenv venv cenv g aenv2 in
            let ps_args2 = SymbolTable.find cenv.cenv_ps_args g in
               match info with
                  Some (pred1, aenv1, args1, ps_args1) ->
                     let pred = pred_or pred1 pred2 in
                     let venv, cenv, aenv = unify_aenv venv cenv pos [] false aenv1 aenv2 in
                     let venv, cenv, args = unify_args venv cenv pos vars args1 args2 in
                     let venv, ps_args = unify_ps_args venv cenv.cenv_ps_vars ps_args1 ps_args2 in
                     let info = Some (pred, aenv, args, ps_args) in
                        venv, cenv, info
                | None ->
                     let info = Some (pred2, aenv2, args2, ps_args2) in
                        venv, cenv, info) (venv, cenv, None) entries

(*
 * Fold together the alias classes.
 *)
let unify_call_blocks venv cenv aenv pos =
   let pos = string_pos "unify_call_blocks" pos in
   let unify_step venv cenv aenv =
      SymbolTable.fold (fun (venv, cenv, aenv, changed) v (v1, v2) ->
            if aenv_mem_block aenv v then
               venv, cenv, aenv, changed
            else
               let names = [NameVar v] in
               let block1 = aenv_lookup_block aenv pos v1 in
               let block2 = aenv_lookup_block aenv pos v2 in
               let venv, cenv, block = unify_block venv cenv pos names false block1 block2 in
               let aenv = aenv_add_block aenv v block in
               let aenv = aenv_add_alias aenv v1 v in
               let aenv = aenv_add_alias aenv v2 v in
                  venv, cenv, aenv, true) (venv, cenv, aenv, false) cenv.cenv_alias
   in
   let rec unify_fixpoint venv cenv aenv =
      let venv, cenv, aenv, changed = unify_step venv cenv aenv in
         if changed then
            unify_fixpoint venv cenv aenv
         else
            venv, cenv, aenv
   in
      unify_fixpoint venv cenv aenv

(*
 * Unify all the parts of the call.
 *)
let unify_call venv pos f fdepth vars call =
   let pos = string_pos "unify_call" pos in
   let { call_outer = outer;
         call_inner = inner;
         call_loop_inner = iinfo;
         call_loop_outer = oinfo;
         call_info  = info;
         call_loop  = loop
       } = call
   in
   let fdepth =
      if loop then
         pred_depth fdepth
      else
         fdepth
   in

   (* Initial call environment *)
   let cenv =
      { cenv_fun = f;
        cenv_depth = fdepth;
        cenv_ps_vars = SymbolSet.empty;
        cenv_ps_args = SymbolTable.empty;
        cenv_alias = SymbolTable.empty
      }
   in

   (* Unify the call collections *)
   let venv, cenv, info1 = unify_call_entries venv cenv pos vars outer in
   let changed = not (equal_call_info info1 oinfo) in
   let venv, cenv, pred, aenv, args, ps_args, info1, info2 =
      if changed then
         match info1 with
            Some (pred, aenv, args, ps_args) ->
               venv, cenv, pred, aenv, args, ps_args, info1, None
          | None ->
               raise (FirException (pos, StringError "no call arguments"))
      else
         (* Outer arguments are now stable *)
         let venv, cenv, info2 = unify_call_entries venv cenv pos vars inner in
         let venv, cenv, pred, aenv, args, ps_args =
            match info1, info2 with
               Some (pred1, aenv1, args1, ps_args1), Some (pred2, aenv2, args2, ps_args2) ->
                  let pred = pred_or pred1 pred2 in
                  let venv, cenv, aenv = unify_aenv venv cenv pos [] true aenv1 aenv2 in
                  let venv, cenv, args = unify_args venv cenv pos vars args1 args2 in
                  let venv, ps_args = unify_ps_args venv cenv.cenv_ps_vars ps_args1 ps_args2 in
                     venv, cenv, pred, aenv, args, ps_args
             | Some (pred, aenv, args, ps_args), None ->
                  venv, cenv, pred, aenv, args, ps_args
             | _ ->
                  raise (FirException (pos, StringError "no call arguments"))
         in
            venv, cenv, pred, aenv, args, ps_args, info1, info2
   in

   (* Add the folded alias classes *)
   let venv, cenv, aenv = unify_call_blocks venv cenv aenv pos in

   (* Add the definitions for the ps_vars *)
   let venv =
      SymbolSet.fold (fun venv v ->
            let depth, ps_arg =
               try SymbolTable.find ps_args v with
                  Not_found ->
                     zero_depth, depends_empty
            in
               venv_add_subst venv v depth ps_arg) venv cenv.cenv_ps_vars
   in

   (* Compare the info *)
   let info' = Some (pred, aenv, args, ps_args) in
   let changed = changed || not (equal_call_info info info') in
   let { cenv_ps_vars = ps_vars;
         cenv_ps_args = ps_args
       } = cenv
   in
   let call =
      { call with call_changed = changed;
                  call_ps_vars = ps_vars;
                  call_ps_args = ps_args;
                  call_loop_outer = info1;
                  call_loop_inner = info2;
                  call_info = info'
      }
   in
      call, pred, venv, aenv, args

(************************************************************************
 * UNIFICATION FOR ALIASES
 ************************************************************************)

(*
 * Unify a vclass with an iclass.
 * BUG: we're lazy about this.  If the expressions don't unify exactly,
 * we reject them.
 *
 * The alternative is to  do a smart unification using unify_vclass.
 * However, that will require updating the aenv, which is expensive.
 *)
let unify_block_vfield venv aenv pos vc ic =
   let pos = string_pos "unify_block_vfield" pos in
   let e1 = etree_of_vclass venv pos vc in
   let e2 = etree_of_iclass venv pos ic in
      if e1 = e2 then
         venv, aenv, ic
      else
         raise UnifyError

(*
 * Unify two iclasses.
 *)
let unify_block_ifield venv aenv pos ic1 ic2 =
   let pos = string_pos "unify_block_ifield" pos in
   let e1 = etree_of_iclass venv pos ic1 in
   let e2 = etree_of_iclass venv pos ic2 in
      if e1 = e2 then
         venv, aenv, ic1
      else
         raise UnifyError

let rec unify_block_int_fields venv aenv pos fields1 fields2 =
   match fields1, fields2 with
      (i1, ic1) :: fields1', (i2, ic2) :: fields2' ->
         if i1 = i2 then
            let venv, aenv, fields = unify_block_int_fields venv aenv pos fields1' fields2' in
               try
                  let venv, aenv, ic = unify_block_ifield venv aenv pos ic1 ic2 in
                  let fields = (i1, ic) :: fields in
                     venv, aenv, fields
               with
                  UnifyError ->
                     venv, aenv, fields
         else if i1 < i2 then
            unify_block_int_fields venv aenv pos fields1' fields2
         else
            unify_block_int_fields venv aenv pos fields1 fields2'
    | _ ->
         venv, aenv, []

(*
 * Unify two variables.  This may be an induction,
 * check for addition operations.
 *)
let unify_induction_field venv pos vc ic1 =
   let pos = string_pos "unify_induction_field" pos in
   let ic2 =
      match ic1 with
         IVarVar (_, v) ->
            let _, e = etree_of_vclass venv pos vc in
               (match etree_is_sum_of_var e v with
                   Some (op, e) ->
                      IVarInd (vc, op, v, dest_etree_core e)
                 | None ->
                      IVarExp vc)
       | IVarInd (_, op1, v1, _) ->
            let _, e = etree_of_vclass venv pos vc in
               (match etree_is_sum_of_var e v1 with
                   Some (op2, e) when op2 = op1 ->
                      IVarInd (vc, op2, v1, dest_etree_core e)
                 | _ ->
                      IVarExp vc)
       | IVarUnknown _
       | IVarExp _ ->
            IVarExp vc
   in
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>unify_induction_field:@ @[<hv 3>vclass =@ ";
            pp_print_vclass Format.std_formatter vc;
            Format.printf "@]@ @[<hv 3>iclass1 =@ ";
            pp_print_iclass Format.std_formatter ic1;
            Format.printf "@]@ @[<hv 3>iclass2 =@ ";
            pp_print_iclass Format.std_formatter ic2;
            Format.printf "@]@]@."
         end;
      ic2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
