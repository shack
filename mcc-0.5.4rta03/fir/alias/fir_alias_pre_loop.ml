(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_depth
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

open Fir_alias_pre_cse

module Pos = MakePos (struct let name = "Fir_alias_pre_loop" end)
open Pos

(*
 * Add loop arguments.
 *)
let loop_arg gvenv aenv lenv loc v op step base =
   let pos = string_pos "ps_loop_arg" (var_exp_pos v) in
   let gvenv, _, step = cse_normalize_etree gvenv aenv pos zero_depth (add_etree op (ETVar v) step) in
   let lenv = SymbolTable.add lenv v (make_etree loc step, make_etree loc base) in
      gvenv, lenv

(*
 * In the final step, add the base and step expressions as pseudo-arguments
 * to the loop.
 *)
let loop_prog gvenv aenv =
   let gvenv, lenv =
      SymbolTable.fold (fun (gvenv, lenv) _ (ac, _) ->
            let loc = loc_of_aclass ac in
               match dest_aclass_core ac with
                  AliasInduction (_, v, _, op, _, _, _, step, base) ->
                     loop_arg gvenv aenv lenv loc v op step base
                | AliasExp _
                | AliasAlias _ ->
                     gvenv, lenv) (gvenv, SymbolTable.empty) gvenv.gvenv_values
   in
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>*** FIR: alias: loop table:";
            SymbolTable.iter (fun v (base, step) ->
                  Format.printf "@ @[<hv 0>@[<hv 3> %a" pp_print_symbol v;
                  Format.printf " {@ @[<hv 3>base =@ %a" pp_print_etree base;
                  Format.printf "@];@ @[<hv 3>step =@ %a" pp_print_etree step;
                  Format.printf "@]@]@ }@]") lenv;
            Format.printf "@]@."
         end;
      gvenv, lenv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
