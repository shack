(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_type
open Fir_state
open Fir_algebra

open Fir_logic.FirLogic

open Fir_depth
open Fir_alias_env
open Fir_alias_type
open Fir_alias_util
open Fir_alias_print
open Fir_alias_subst
open Fir_alias_unify

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_alias_mem" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Empty defaults.
 *)
let heap_empty = SymbolSet.empty

let default_empty =
   { default_alias = SymbolSet.empty;
     default_heap = heap_empty
   }

(*
 * Add the heaps.
 *)
let default_heap default heap =
   SymbolSet.union heap default.default_heap

(*
 * Union of two default sets.
 *)
let default_union default1 default2 =
   let { default_alias = alias1;
         default_heap = heap1
       } = default1
   in
   let { default_alias = alias2;
         default_heap = heap2
       } = default2
   in
      { default_alias = SymbolSet.union alias1 alias2;
        default_heap = SymbolSet.union heap1 heap2
      }

let default_union_heap default1 heap1 default2 =
   let { default_alias = alias1 } = default1 in
   let { default_alias = alias2;
         default_heap = heap2
       } = default2
   in
      { default_alias = SymbolSet.union alias1 alias2;
        default_heap = SymbolSet.union heap1 heap2
      }

(*
 * Build a default record for the value being stored.
 *)
let default_of_vclass label ic =
   let depth, alias =
      match dest_vclass_core ic with
         VarExp (depth, _) ->
            depth, SymbolSet.empty
       | VarAlias (depth, _, _, v1, s1, _) ->
            depth, SymbolSet.add s1 v1
   in
   let default =
      { default_alias = alias;
        default_heap = SymbolSet.singleton label
      }
   in
      depth, default

(*
 * Default block.
 *)
let block_new_default loc depth length default =
   { block_loc          = loc;
     block_depth        = depth;
     block_mutable      = false;
     block_mutable_heap = heap_empty;
     block_length       = ETVar length;
     block_length_heap  = heap_empty;
     block_default      = default;
     block_int_fields   = [];
     block_lab_fields   = SymbolTable.empty;
     block_var_fields   = ETreeTable.empty
   }

let root_alias = new_symbol_string "root_alias"

let root_default =
   { default_empty with default_alias = SymbolSet.singleton root_alias }

let block_new_root loc =
   let v_length = new_symbol_string "rlength" in
      block_new_default loc zero_depth v_length root_default

(*
 * Update all the blocks in an alias class.
 *)
let alias_map aenv pos update s =
   let pos = string_pos "alias_update" pos in
   let blocks = aenv.aenv_blocks in
   let blocks =
      SymbolSet.fold (fun blocks v ->
            SymbolTable.filter_add blocks v (function
               Some block ->
                  update block
             | None ->
                  raise (FirException (pos, UnboundVar v)))) blocks s
   in
      { aenv with aenv_blocks = blocks }

let alias_fold_map aenv x pos update s =
   let pos = string_pos "alias_fold_map" pos in
   let blocks = aenv.aenv_blocks in
   let blocks, x =
      SymbolSet.fold (fun (blocks, x) v ->
            try
               let block = SymbolTable.find blocks v in
               let x, block = update x block in
               let blocks = SymbolTable.add blocks v block in
                  blocks, x
            with
               Not_found ->
                  raise (FirException (pos, UnboundVar v))) (blocks, x) s
   in
      { aenv with aenv_blocks = blocks }, x

(*
 * Fold a value over the blocks.
 * The blocks are unchanged.
 *)
let alias_fold aenv pos update x s =
   let pos = string_pos "alias_fold" pos in
   let blocks = aenv.aenv_blocks in
      SymbolSet.fold (fun x v ->
            let block =
               try SymbolTable.find blocks v with
                  Not_found ->
                     raise (FirException (pos, UnboundVar v))
            in
               update x v block) x s

(*
 * Overwrite the fields mentioned in the set.
 * default1 is the default alias set for the whole block,
 * default2 is the new defaults to be added.
 *)
let alias_overwrite_labels fields labels default1 default2 =
   SymbolSet.fold (fun fields v ->
         SymbolTable.filter_add fields v (fun entry ->
               let default1 =
                  match entry with
                     Some (_, default1) ->
                        default1
                   | None ->
                        default1
               in
               let default = default_union default1 default2 in
                  SymbolTable.empty, default)) fields labels

(*
 * Overwrite the entire block.
 * Only have to overwrite those fields that
 * are possible aliases.
 *)
let alias_overwrite_block aenv pos heap s labels default2 =
   let pos = string_pos "alias_overwrite_block" pos in
   let update heap block =
      let { block_default = default1;
            block_lab_fields = lab_fields
          } = block
      in
      let heap = default_heap default1 heap in
      let block =
         { block with block_int_fields = [];
                      block_lab_fields = alias_overwrite_labels lab_fields labels default1 default2;
                      block_var_fields = ETreeTable.empty;
                      block_default = default_union default1 default2
         }
      in
         heap, block
   in
      alias_fold_map aenv heap pos update s

(*
 * Overwrite the arguments.
 *)
let alias_overwrite_args aenv pos label ics =
   let pos = string_pos "alias_overwrite_args" pos in
   let default2 =
      { default_alias = SymbolSet.empty;
        default_heap = SymbolSet.singleton label
      }
   in
      List.fold_left (fun (aenv, heap) ic ->
            match dest_vclass_core ic with
               VarAlias (_, _, _, v, s, labels) ->
                  alias_overwrite_block aenv pos heap (SymbolSet.add s v) labels default2
             | VarExp _ ->
                  aenv, heap) (aenv, heap_empty) ics

(************************************************************************
 * LET_SUBSCRIPT
 ************************************************************************)

(*
 * Make up a new unknown.
 *)
let alias_new_unknown genv venv aenv depth pos loc v ty default =
   let pos = string_pos "alias_new_unknown" pos in
   let venv = venv_add_unknown venv v depth in
      if is_pointer_type genv pos ty then
         (* This is a pointer type, so make a new alias class *)
         let _ =
            if debug debug_alias then
               Format.printf "Generating a new unknown type@."
         in
         let venv, length = venv_new_name venv [NameVar v] in
         let default = default () in
         let block = block_new_default loc depth length default in
         let aenv = aenv_add_block aenv v block in
         let alias = default.default_alias in
         let vc = make_vclass loc (VarAlias (depth, ETVar v, et_zero_subscript, v, alias, SymbolSet.empty)) in
         let ic = IVarVar (vc, v) in
         let default = { default with default_alias = SymbolSet.add alias v } in
            venv, aenv, Some default, ic
      else
         let vc = make_vclass loc (VarExp (depth, ETVar v)) in
            venv, aenv, None, IVarVar (vc, v)

(*
 * Make a new field for one that doesn't exist.
 * The subscript is added to the venv.  Also,
 * the value depends on the type.
 *)
let alias_new_field genv venv aenv depth pos loc op v1 ty default length_heap e1 e2 =
   let pos = string_pos "alias_new_field" pos in

   (* Add the expression to the venv *)
   let heap = SymbolSet.union default.default_heap length_heap in
   let e = make_htree loc (HTSubscript (heap, op, ty, e1, e2)) in
   let venv = venv_add_subst venv v1 depth e in
   let venv, aenv, default', ic = alias_new_unknown genv venv aenv depth pos loc v1 ty (fun () -> default) in
   let default =
      match default' with
         Some default ->
            default
       | None ->
            default
   in
      venv, aenv, default, ic

(*
 * Fetch a field with an integer index.
 *    v1 is the variable we are fetching into.
 *    v2 is the array alias
 *)
let alias_subscript_int genv venv aenv pos loc depth op v1 ty e1 e2 v2 i =
   let pos = string_pos "alias_subscript_int" pos in
   let i =
      match op.sub_index with
         ByteIndex -> i
       | WordIndex -> i * sizeof_int
   in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v2 in
   let { block_depth = depth';
         block_int_fields = fields;
         block_default = default;
         block_length_heap = length_heap
       } = block
   in
   let depth' = max_depth depth depth' in

   (* Make a new field *)
   let new_int_field fields =
      let venv, aenv, default, ic = alias_new_field genv venv aenv depth' pos loc op v1 ty default length_heap e1 e2 in
         venv, aenv, default, ic, (i, ic) :: fields
   in

   (* If the field is unknown, update it *)
   let make_int_field ic fields =
      match ic with
         IVarUnknown (vc, v) ->
            let depth = depth_of_vclass vc in
            let venv, aenv, default, ic = alias_new_field genv venv aenv depth pos loc op v1 ty default length_heap e1 e2 in
            let venv = venv_add_subst venv v depth (make_htree loc (HTETree (ETVar v1))) in
               venv, aenv, default, ic, (i, ic) :: fields
       | _ ->
            venv, aenv, default, ic, (i, ic) :: fields
   in

   (* Look through the field list *)
   let rec search_int_fields fields =
      match fields with
         (i', ic') :: fields' ->
            if i' < i then
               let venv, aenv, default, ic, fields = search_int_fields fields' in
                  venv, aenv, default, ic, (i', ic') :: fields'
            else if i' = i then
               make_int_field ic' fields'
            else
               new_int_field fields
       | [] ->
            new_int_field []
   in
   let venv, aenv, default, ic, fields = search_int_fields fields in
   let block =
      { block with block_default = default;
                   block_int_fields = fields
      }
   in
   let aenv = aenv_add_block aenv v2 block in
      venv, aenv, ic

(*
 * Fetch the field with a label index.
 *   v1 is the variable we are fetching info.
 *   v2 is the array alias name.
 *)
let alias_subscript_label genv venv aenv pos loc depth op v1 ty e1 e2 v2 label =
   let pos = string_pos "alias_subscript_label" pos in
   let _, field, subfield = label in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v2 in
   let { block_depth = depth';
         block_lab_fields = fields;
         block_default = default1;
         block_length_heap = length_heap
       } = block
   in
   let depth' = max_depth depth depth' in

   (* Add a new subfield *)
   let new_label_field depth subfields default2 =
      let venv, aenv, default2, ic = alias_new_field genv venv aenv depth pos loc op v1 ty default2 length_heap e1 e2 in
      let subfields = SymbolTable.add subfields subfield ic in
      let fields = SymbolTable.add fields field (subfields, default2) in
      let block =
         { block with block_lab_fields = fields;
                      block_default = default_union default1 default2
         }
      in
      let aenv = aenv_add_block aenv v2 block in
         venv, aenv, ic
   in

   (* Resolve unknown fields *)
   let make_label_field ic subfields default2 =
      match ic with
         IVarUnknown (vc, v) ->
            let depth = depth_of_vclass vc in
            let venv, aenv, ic = new_label_field depth subfields default2 in
            let venv = venv_add_subst venv v depth (make_htree loc (HTETree (ETVar v1))) in
               venv, aenv, ic
       | _ ->
            venv, aenv, ic
   in
      try
         let subfields, default2 = SymbolTable.find fields field in
            try
               let ic = SymbolTable.find subfields subfield in
                  make_label_field ic subfields default2
            with
               Not_found ->
                  new_label_field depth' subfields default2
      with
         Not_found ->
            new_label_field depth' SymbolTable.empty default1

(*
 * Fetch a field with a variable index from a block.
 *)
let alias_subscript_var genv venv aenv pos loc depth op v1 ty e1 e2 v2 e =
   let pos = string_pos "alias_subscript_bvar" pos in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v2 in
   let { block_depth = depth';
         block_var_fields = fields;
         block_default = default;
         block_length_heap = length_heap
       } = block
   in
   let depth' = max_depth depth depth' in

   (* New field *)
   let new_var_field depth fields =
      let venv, aenv, default, ic = alias_new_field genv venv aenv depth pos loc op v1 ty default length_heap e1 e2 in
      let fields = ETreeTable.add fields e ic in
      let block =
         { block with block_var_fields = fields;
                      block_default = default
         }
      in
      let aenv = aenv_add_block aenv v2 block in
         venv, aenv, ic
   in

   (* Catch unknown fields *)
   let make_var_field ic fields =
      match ic with
         IVarUnknown (vc, v) ->
            let depth = depth_of_vclass vc in
            let venv, aenv, ic = new_var_field depth fields in
            let venv = venv_add_subst venv v depth (make_htree loc (HTETree (ETVar v1))) in
               venv, aenv, ic
       | _ ->
            venv, aenv, ic
   in
      try
         let ic = ETreeTable.find fields e in
            make_var_field ic fields
      with
         Not_found ->
            new_var_field depth' fields

(*
 * Look up a field from a block.
 *   v1: the variable being assigned
 *   ty: the type of v1
 *   e2: the array being subscripted
 *   v2: the array must-alias for e2
 *   off2: the offset into the array alias
 *   e2: the subscript
 *)
let alias_subscript genv venv aenv pos loc depth op v1 ty e1 v2 off2 e2 =
   let pos = string_pos "alias_subscript" pos in
   let e = expand_subscript_sum venv pos op off2 e2 in
   let venv, aenv, ic =
      match e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_subscript_int genv venv aenv pos loc depth op v1 ty e1 e2 v2 i
       | ETConst (AtomRawInt i) ->
            alias_subscript_int genv venv aenv pos loc depth op v1 ty e1 e2 v2 (Rawint.to_int i)
       | ETConst (AtomLabel (label, off))
         when Rawint.is_zero off ->
            alias_subscript_label genv venv aenv pos loc depth op v1 ty e1 e2 v2 label
       | _ ->
            alias_subscript_var genv venv aenv pos loc depth op v1 ty e1 e2 v2 e
   in
      venv, aenv, vclass_of_iclass ic

(************************************************************************
 * SET_SUBSCRIPT
 ************************************************************************)

(*
 * Store a field with an integer index.
 * Note that this deletes all the var fields.
 * BUG: we can be smarter about this if we know
 * variable ranges.
 *)
let alias_may_set_subscript_int venv aenv heap default2 depth2 pos s labels i1 size1 vc1 =
   let pos = string_pos "alias_may_set_subscript_int" pos in

   (* Store in an integer field *)
   let rec store_int_field venv aenv fields =
      match fields with
         (i2, ic2) as field :: fields' ->
            if i2 = i1 then
               try
                  let venv, aenv, ic = unify_block_vfield venv aenv pos vc1 ic2 in
                  let fields = (i1, ic) :: fields' in
                     venv, aenv, fields
               with
                  UnifyError ->
                     venv, aenv, fields'
            else if i2 > i1 then
               venv, aenv, fields'
            else
               let venv, aenv, fields' = store_int_field venv aenv fields' in
               let fields = field :: fields' in
                  venv, aenv, fields
       | [] ->
            venv, aenv, []
   in
   let store_block (venv, aenv, heap) v block =
      let { block_int_fields = int_fields;
            block_lab_fields = lab_fields;
            block_length_heap = length_heap;
            block_mutable_heap = mutable_heap;
            block_default = default1;
            block_depth = depth1
          } = block
      in
      let venv, aenv, int_fields = store_int_field venv aenv int_fields in
      let heap = default_heap default1 heap in
      let heap = SymbolSet.union heap length_heap in
      let heap = SymbolSet.union heap mutable_heap in
      let block =
         { block with block_int_fields = int_fields;
                      block_lab_fields = alias_overwrite_labels lab_fields labels default1 default2;
                      block_var_fields = ETreeTable.empty;
                      block_default = default_union_heap default1 heap default2;
                      block_depth = max_depth depth1 depth2
         }
      in
      let aenv = aenv_add_block aenv v block in
         venv, aenv, heap
   in
      alias_fold aenv pos store_block (venv, aenv, heap) s

(*
 * Store a field with a label index.
 * Note that this deletes all other fields.
 *)
let alias_may_set_subscript_label venv aenv heap default2 depth2 g pos s label size vc1 =
   let pos = string_pos "alias_set_subscript_label" pos in
   let _, field, subfield = label in

   (* Store in a label field *)
   let store_sub_field venv aenv heap default1 fields =
      try
         let subfields, default1 = SymbolTable.find fields subfield in
         let venv, aenv, subfields =
            try
               let ic2 = SymbolTable.find subfields subfield in
               let venv, aenv, ic = unify_block_vfield venv aenv pos vc1 ic2 in
               let subfields = SymbolTable.add subfields subfield ic2 in
                  venv, aenv, subfields
            with
               Not_found ->
                  venv, aenv, subfields
             | UnifyError ->
                  venv, aenv, SymbolTable.remove subfields subfield
         in
         let heap = default_heap default1 heap in
         let default = default_union default1 default2 in
         let fields = SymbolTable.add fields field (subfields, default) in
            venv, aenv, heap, fields
      with
         Not_found ->
            let heap = default_heap default1 heap in
               venv, aenv, heap, fields
   in

   (* Store into the block *)
   let store_block (venv, aenv, heap) v block =
      let { block_mutable_heap = mutable_heap;
            block_length_heap = length_heap;
            block_lab_fields = fields;
            block_default = default1;
            block_depth = depth1
          } = block
      in
      let venv, aenv, heap, fields = store_sub_field venv aenv heap default1 fields in
      let heap = SymbolSet.union heap mutable_heap in
      let heap = SymbolSet.union heap length_heap in
      let block =
         { block with block_int_fields = [];
                      block_lab_fields = fields;
                      block_var_fields = ETreeTable.empty;
                      block_default = default_union_heap default1 heap default2;
                      block_depth = max_depth depth1 depth2
         }
      in
      let aenv = aenv_add_block aenv v block in
         venv, aenv, heap
   in
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>alias_may_set_subscript:@ from function: %a" pp_print_symbol g;
            Format.printf "@ ";
            pp_print_aenv Format.std_formatter aenv;
            Format.printf "@]@."
         end;
      alias_fold aenv pos store_block (venv, aenv, heap) s

(*
 * Store a field with a variable index.
 * Note that this deletes all but the indexed field.
 * BUG: we can be smarter about this if we know
 * variable ranges.
 *)
let alias_may_set_subscript_var venv aenv heap default2 depth2 pos s labels e size vc1 =
   let pos = string_pos "alias_set_subscript_var" pos in

   (* Store in an variable field *)
   let store_var_field venv aenv fields =
      try
         let ic2 = ETreeTable.find fields e in
         let venv, aenv, ic = unify_block_vfield venv aenv pos vc1 ic2 in
         let fields = ETreeTable.add fields e ic in
            venv, aenv, fields
      with
         Not_found ->
            venv, aenv, fields
       | UnifyError ->
            venv, aenv, ETreeTable.remove fields e
   in
   let store_block (venv, aenv, heap) v block =
      let { block_mutable_heap = mutable_heap;
            block_length_heap = length_heap;
            block_var_fields = var_fields;
            block_lab_fields = lab_fields;
            block_default = default1;
            block_depth = depth1
          } = block
      in
      let venv, aenv, var_fields = store_var_field venv aenv var_fields in
      let heap = default_heap default1 heap in
      let heap = SymbolSet.union heap mutable_heap in
      let heap = SymbolSet.union heap length_heap in
      let block =
         { block with block_int_fields = [];
                      block_lab_fields = alias_overwrite_labels lab_fields labels default1 default2;
                      block_var_fields = var_fields;
                      block_default = default_union_heap default1 heap default2;
                      block_depth = max_depth depth1 depth2
         }
      in
      let aenv = aenv_add_block aenv v block in
         venv, aenv, heap
   in
      alias_fold aenv pos store_block (venv, aenv, heap) s

(*
 * Store a value in an array.
 * This will destroy parts of the array.
 *)
let alias_may_set_subscript venv aenv heap default2 depth2 g pos s labels e size ic =
   let pos = string_pos "alias_may_set_subscript" pos in
      match dest_etree_core e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_may_set_subscript_int venv aenv heap default2 depth2 pos s labels i size ic
       | ETConst (AtomRawInt i) ->
            alias_may_set_subscript_int venv aenv heap default2 depth2 pos s labels (Rawint.to_int i) size ic
       | ETConst (AtomLabel (label, off))
         when Rawint.is_zero off ->
            alias_may_set_subscript_label venv aenv heap default2 depth2 g pos s label size ic
       | e ->
            alias_may_set_subscript_var venv aenv heap default2 depth2 pos s labels e size ic

(*
 * Store a field with an integer index.
 * BUG: we wipe out all variable fields.
 * We could be smarter.
 *)
let alias_must_set_subscript_int venv aenv heap default2 depth2 set_label pos v labels i1 size vc1 =
   let pos = string_pos "alias_must_set_subscript_int" pos in

   (* Store in an integer field *)
   let rec store_int_field fields =
      match fields with
         (i2, ic2) as field :: fields' ->
            if i2 = i1 then
               (i1, unify_induction_field venv pos vc1 ic2) :: fields'
            else if i2 > i1 then
               (i1, IVarExp vc1) :: fields
            else
               field :: store_int_field fields'
       | [] ->
            [i1, IVarExp vc1]
   in
   let block = aenv_lookup_block aenv pos v in
   let { block_mutable_heap = mutable_heap;
         block_length_heap = length_heap;
         block_int_fields = int_fields;
         block_lab_fields = lab_fields;
         block_default = default1;
         block_depth = depth1
       } = block
   in
   let heap = default_heap default1 heap in
   let heap = SymbolSet.union heap mutable_heap in
   let heap = SymbolSet.union heap length_heap in
   let block =
      { block with block_int_fields = store_int_field int_fields;
                   block_lab_fields = alias_overwrite_labels lab_fields labels default1 default2;
                   block_var_fields = ETreeTable.empty;
                   block_default = default_union_heap default1 heap default2;
                   block_depth = max_depth depth1 depth2
      }
   in
   let aenv = aenv_add_block aenv v block in
      aenv, heap

(*
 * Store a field with a label index.
 * Wipe out all fields of other types.
 *)
let alias_must_set_subscript_label venv aenv heap default2 depth2 set_label pos v label size vc1 =
   let pos = string_pos "alias_must_set_subscript_label" pos in
   let _, field, subfield = label in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v in
   let { block_mutable_heap = mutable_heap;
         block_length_heap = length_heap;
         block_lab_fields = fields;
         block_default = default0;
         block_depth = depth0
       } = block
   in

   (* If there is no field entry, make one *)
   let subfields, default1 =
      try SymbolTable.find fields field with
         Not_found ->
            SymbolTable.empty, default0
   in
   let heap = default_heap default1 heap in
   let heap = SymbolSet.union heap mutable_heap in
   let heap = SymbolSet.union heap length_heap in

   (* Add the subfield *)
   let subfields =
      SymbolTable.filter_add subfields subfield (function
         Some ic2 ->
            unify_induction_field venv pos vc1 ic2
       | None ->
            IVarExp vc1)
   in
   let fields = SymbolTable.add fields field (subfields, default_union_heap default1 heap default2) in
   let heap = default_heap default0 heap in
   let block' =
      { block with block_int_fields = [];
                   block_lab_fields = fields;
                   block_var_fields = ETreeTable.empty;
                   block_default = default_union_heap default0 heap default2;
                   block_depth = max_depth depth0 depth2
      }
   in
   let _ =
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>alias_must_set_subscript_label:@ aclass = %a" pp_print_symbol v;
            Format.printf ";@ label = %a" pp_print_symbol set_label;
            Format.printf ";@ @[<hv 3>vclass =@ ";
            pp_print_vclass Format.std_formatter vc1;
            Format.printf "@];@ @[<hv 3>block1 =@ ";
            pp_print_block Format.std_formatter block;
            Format.printf "@];@ @[<hv 3>block2 =@ ";
            pp_print_block Format.std_formatter block';
            Format.printf "@]@]@."
         end
   in
   let aenv = aenv_add_block aenv v block' in
      aenv, heap

(*
 * Store a field with a variable index.
 * For now, we assume no aliasing.
 * BUG: we wipe out all other variable fields,
 * as well as all int fields.  We could be smarter.
 *)
let alias_must_set_subscript_var venv aenv heap default2 depth2 set_label pos v labels e size vc1 =
   let pos = string_pos "alias_must_set_subscript_bvar" pos in

   (* Add the fields *)
   let block = aenv_lookup_block aenv pos v in
   let { block_mutable_heap = mutable_heap;
         block_length_heap = length_heap;
         block_var_fields = var_fields;
         block_lab_fields = lab_fields;
         block_default = default1;
         block_depth = depth1
       } = block
   in

   (* Wipe out old entries *)
   let ic1 =
      try
         let ic2 = ETreeTable.find var_fields e in
            unify_induction_field venv pos vc1 ic2
      with
         Not_found ->
            IVarExp vc1
   in
   let var_fields = ETreeTable.add ETreeTable.empty e ic1 in

   let heap = default_heap default1 heap in
   let heap = SymbolSet.union heap mutable_heap in
   let heap = SymbolSet.union heap length_heap in
   let block =
      { block with block_int_fields = [];
                   block_lab_fields = alias_overwrite_labels lab_fields labels default1 default2;
                   block_var_fields = var_fields;
                   block_default = default_union_heap default1 heap default2;
                   block_depth = max_depth depth1 depth2
      }
   in
   let _ =
      if debug debug_alias then
         begin
            Format.printf "@[<hv 3>alias_must_set_subscript_var:@ aclass = %a" pp_print_symbol v;
            Format.printf ";@ label = %a" pp_print_symbol set_label;
            Format.printf ";@ @[<hv 3>labels =";
            SymbolSet.iter (fun v ->
                  Format.printf "@ %a" pp_print_symbol v) labels;
            Format.printf "@];@ @[<hv 3>etree =@ %a" pp_print_etree_core e;
            Format.printf "@];@ @[<hv 3>vclass =@ %a" pp_print_vclass vc1;
            Format.printf "@];@ @[<hv 3>block =@ %a" pp_print_block block;
            Format.printf "@]@]@."
         end
   in
   let aenv = aenv_add_block aenv v block in
      aenv, heap

(*
 * Store a value in an array.
 * If we can't figure out what the index is, just destroy the entire array.
 *)
let alias_must_set_subscript venv aenv heap default2 depth2 set_label pos v labels e size ic =
   let pos = string_pos "alias_must_set_subscript" pos in
      match dest_etree_core e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_must_set_subscript_int venv aenv heap default2 depth2 set_label pos v labels i size ic
       | ETConst (AtomRawInt i) ->
            alias_must_set_subscript_int venv aenv heap default2 depth2 set_label pos v labels (Rawint.to_int i) size ic
       | ETConst (AtomLabel (label, off))
         when Rawint.is_zero off ->
            alias_must_set_subscript_label venv aenv heap default2 depth2 set_label pos v label size ic
       | e ->
            alias_must_set_subscript_var venv aenv heap default2 depth2 set_label pos v labels e size ic

(*
 * General set_subscript routine.
 *)
let alias_set_subscript venv aenv g pos loc op label e1 e2 ty e3 v s labels off2 size ic =
   let pos = string_pos "alias_set_subscript" pos in
   let e = expand_subscript_sum venv pos op off2 e2 in
   let e = make_etree loc e in
   let depth2, default2 = default_of_vclass label ic in
   let heap = heap_empty in
   let s = aenv_expand aenv v s in
   let venv, aenv, heap = alias_may_set_subscript venv aenv heap default2 depth2 g pos s labels e size ic in
   let aenv, heap = alias_must_set_subscript venv aenv heap default2 depth2 label pos v labels e size ic in
   let venv = venv_add_subst venv label depth2 (make_htree loc (HTSetSubscript (heap, op, ty, e1, e2, e3))) in
      venv, aenv

(************************************************************************
 * MEMCPY
 ************************************************************************)

(*
 * Get the fields and defaults from all the source arrays.
 *)
let alias_memcpy_fields venv aenv pos s2 i2 i3 =
   let pos = string_pos "alias_memcpy_fields" pos in

   (* Get the fields from the blocks *)
   let limit2 = i2 + i3 in
   let collect_block (venv, aenv, default2, depth2, fields2) v block =
      let { block_int_fields = fields1;
            block_default = default1;
            block_depth = depth1
          } = block
      in

      (* Get the fields in range *)
      let fields1 =
         List.fold_left (fun fields2 (i, ic) ->
               if i >= i2 && i < limit2 then
                  (i, ic) :: fields1
               else
                  fields1) [] fields1
      in
      let fields1 = List.rev fields1 in

      (* Merge two field lists *)
      let venv, aenv, fields = unify_block_int_fields venv aenv pos fields2 fields1 in
      let default = default_union default1 default2 in
      let depth = max_depth depth1 depth2 in
         venv, aenv, default, depth, fields
   in
      (* Put together the fields in the sources *)
      alias_fold aenv pos collect_block (venv, aenv, default_empty, zero_depth, []) s2

(*
 * Copy data from one array to another.
 * Indices and bounds are constants.
 *)
let alias_memcpy_int venv aenv pos s1 labels i1 s2 i2 i3 =
   let pos = string_pos "alias_memcpy_int" pos in

   (* Get the fields *)
   let venv, aenv, default2, depth2, fields2 = alias_memcpy_fields venv aenv pos s2 i2 i3 in

   (* Unify the fields in range *)
   let base1 = i1 in
   let limit1 = i1 + i3 in
   let rec unify_int_fields venv aenv pos fields1 fields2 =
      match fields1 with
         field1 :: fields1' ->
            let i1, ic1 = field1 in
               if i1 < base1 || i1 >= limit1 then
                  let venv, aenv, fields = unify_int_fields venv aenv pos fields1' fields2 in
                  let fields = field1 :: fields in
                     venv, aenv, fields
               else
                  (match fields2 with
                      field2 :: fields2' ->
                         let i2, ic2 = field2 in
                            if i1 = i2 then
                               let venv, aenv, fields = unify_int_fields venv aenv pos fields1' fields2' in
                                  try
                                     let venv, aenv, ic = unify_block_ifield venv aenv pos ic1 ic2 in
                                     let fields = (i1, ic) :: fields in
                                        venv, aenv, fields
                                  with
                                     UnifyError ->
                                        venv, aenv, fields
                            else if i1 < i2 then
                               unify_int_fields venv aenv pos fields1' fields2
                            else
                               unify_int_fields venv aenv pos fields1 fields2'
                    | [] ->
                         unify_int_fields venv aenv pos fields1' [])
       | [] ->
            venv, aenv, []
   in

   (* Store the fields *)
   let store_block (venv, aenv, heap) v block =
      let { block_mutable_heap = mutable_heap;
            block_length_heap = length_heap;
            block_int_fields = int_fields;
            block_lab_fields = lab_fields;
            block_default = default1;
            block_depth = depth1
          } = block
      in
      let heap = default_heap default1 heap in
      let heap = SymbolSet.union heap mutable_heap in
      let heap = SymbolSet.union heap length_heap in
      let venv, aenv, int_fields = unify_int_fields venv aenv pos int_fields fields2 in
      let lab_fields = alias_overwrite_labels lab_fields labels default1 default2 in
      let block =
         { block with block_int_fields = int_fields;
                      block_lab_fields = lab_fields;
                      block_var_fields = ETreeTable.empty;
                      block_default = default_union default1 default2;
                      block_depth = max_depth depth1 depth2
         }
      in
      let aenv = aenv_add_block aenv v block in
         venv, aenv, heap
   in
      alias_fold aenv pos store_block (venv, aenv, heap_empty) s1

(*
 * Copy data from one array to another.
 *)
let alias_memcpy venv aenv depth pos loc op label e1 e2 e3 e4 e5 s1 off1 labels s3 off3 =
   let pos = string_pos "alias_memcpy" pos in
   let off2 = expand_subscript_sum venv pos op e2 off1 in
   let off4 = expand_subscript_sum venv pos op e4 off3 in
   let off5 = expand_subscript_width venv op e5 in
   let i2 = int_of_etree_core off1 0 in
   let i4 = int_of_etree_core off4 0 in
   let i5 = int_of_etree_core off5 max_int in
   let venv, aenv, heap = alias_memcpy_int venv aenv pos s1 labels i2 s3 i4 i5 in
   let venv = venv_add_subst venv label depth (make_htree loc (HTMemcpy (heap, op, e1, e2, e3, e4, e5))) in
      venv, aenv

(************************************************************************
 * PREDICATES
 ************************************************************************)

(*
 * Assert that a block must be mutable.
 *    e: the block being mutated
 *    v: the must-alias class
 * We don't say anything about the may-alias classes.
 *)
let alias_is_mutable venv aenv depth pos loc label e v =
   let pos = string_pos "alias_is_mutable" pos in
   let block = aenv_lookup_block aenv pos v in
   let { block_mutable = mutablep;
         block_mutable_heap = mutable_heap
       } = block
   in
      if block.block_mutable then
         venv, aenv
      else
         let venv = venv_add_subst venv label depth (make_htree loc (HTAssert (mutable_heap, IsMutable e))) in
         let block =
            { block with block_mutable = true;
                         block_mutable_heap = SymbolSet.add mutable_heap label
            }
         in
         let aenv = aenv_add_block aenv v block in
            venv, aenv

(*
 * The reserve makes all blocks immutable.
 *)
let alias_reserve venv aenv depth pos loc label e1 e2 =
   let pos = string_pos "alias_reserve" pos in
   let { aenv_reserve_heap = heap;
         aenv_blocks = blocks
       } = aenv
   in
   let blocks =
      SymbolTable.map (fun block ->
            let { block_mutable_heap = mutable_heap } = block in
            let mutable_heap = SymbolSet.add mutable_heap label in
               { block with block_mutable_heap = mutable_heap;
                            block_mutable = false
               }) blocks
   in
   let aenv =
      { aenv with aenv_reserve_heap = SymbolSet.add heap label;
                  aenv_blocks = blocks
      }
   in
   let venv = venv_add_subst venv label depth (make_htree loc (HTAssert (heap, Reserve (e1, e2)))) in
      venv, aenv

(*
 * Check the bounds on the must-alias class.
 *)
let ge_subscript = GeRawIntOp (precision_subscript, signed_subscript)
let zero_subscript = ETConst zero_subscript

let alias_bounds_check venv aenv depth pos loc pred label op e1 off1 e2 e3 v =
   let pos = string_pos "alias_bounds_check" pos in
   let block = aenv_lookup_block aenv pos v in
   let { block_loc = block_loc;
         block_length = length;
         block_length_heap = length_heap
       } = block
   in
   let length = make_etree loc length in
   let zero_subscript = make_etree loc zero_subscript in
   let e2' = make_etree loc e2 in
   let e3' = make_etree loc e3 in
   let pred = pred_assert pred pos ge_subscript length e3' in
   let pred = pred_assert pred pos ge_subscript e2' zero_subscript in
   let e1 = expand_subscript_sum venv pos op e1 off1 in
   let venv = venv_add_subst venv label depth (make_htree loc (HTAssert (length_heap, BoundsCheck (op, e1, e2, e3)))) in
   let block = { block with block_length_heap = SymbolSet.add length_heap label } in
   let aenv = aenv_add_block aenv v block in
      venv, aenv, pred

(************************************************************************
 * ELEMENT CHECK
 ************************************************************************)

(*
 * Check if a vclass has the right type.
 *)
let vclass_has_type genv venv pos vc ty op =
   let pos = string_pos "vclass_has_type" pos in
      match op.sub_value, dest_vclass_core vc with
         PolySub, _
       | RawIntSub _, _
       | RawFloatSub _, _
       | FunctionSub, VarAlias _
       | BlockPointerSub, VarAlias _
       | RawPointerSub, VarAlias _ ->
            true
       | _ ->
            false

(*
 * Fetch a field with an integer index.
 * Pretend that we are fetching into a variable.
 *    v1 is the variable we are fetching into.
 *    v2 is the array alias
 *)
let alias_element_int genv venv aenv pos loc depth op v1 ty e1 e2 v2 i =
   let pos = string_pos "alias_element_int" pos in
   let i =
      match op.sub_index with
         ByteIndex -> i
       | WordIndex -> i * sizeof_int
   in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v2 in
   let { block_depth = depth';
         block_int_fields = fields;
         block_default = default;
         block_length_heap = length_heap
       } = block
   in
   let depth' = max_depth depth depth' in

   (* Make a new field *)
   let new_int_field fields =
      let venv, aenv, default, ic = alias_new_field genv venv aenv depth' pos loc op v1 ty default length_heap e1 e2 in
         venv, aenv, default, (i, ic) :: fields
   in

   (* If the field is unknown, update it *)
   let make_int_field ic fields =
      match ic with
         IVarUnknown (vc, v) ->
            let depth = depth_of_vclass vc in
            let venv, aenv, default, ic = alias_new_field genv venv aenv depth pos loc op v1 ty default length_heap e1 e2 in
            let venv = venv_add_subst venv v depth (make_htree loc (HTETree (ETVar v1))) in
               venv, aenv, default, (i, ic) :: fields
       | IVarVar (vc, _)
       | IVarExp vc
       | IVarInd (vc, _, _, _) ->
            if vclass_has_type genv venv pos vc ty op then
               venv, aenv, default, (i, ic) :: fields
            else
               new_int_field fields
   in

   (* Look through the field list *)
   let rec search_int_fields fields =
      match fields with
         (i', ic') :: fields' ->
            if i' < i then
               let venv, aenv, default, fields = search_int_fields fields' in
                  venv, aenv, default, (i', ic') :: fields'
            else if i' = i then
               make_int_field ic' fields'
            else
               new_int_field fields
       | [] ->
            new_int_field []
   in
   let venv, aenv, default, fields = search_int_fields fields in
   let block =
      { block with block_default = default;
                   block_int_fields = fields
      }
   in
   let aenv = aenv_add_block aenv v2 block in
      venv, aenv

(*
 * Fetch the field with a label index.
 *   v1 is the variable we are fetching info.
 *   v2 is the array alias name.
 *)
let alias_element_label genv venv aenv pos loc depth op v1 ty e1 e2 v2 label =
   let pos = string_pos "alias_element_label" pos in
   let _, field, subfield = label in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v2 in
   let { block_depth = depth';
         block_lab_fields = fields;
         block_default = default1;
         block_length_heap = length_heap
       } = block
   in
   let depth' = max_depth depth depth' in

   (* Add a new subfield *)
   let new_label_field depth subfields default2 =
      let venv, aenv, default2, ic = alias_new_field genv venv aenv depth pos loc op v1 ty default2 length_heap e1 e2 in
      let subfields = SymbolTable.add subfields subfield ic in
      let fields = SymbolTable.add fields field (subfields, default2) in
      let block =
         { block with block_lab_fields = fields;
                      block_default = default_union default1 default2
         }
      in
      let aenv = aenv_add_block aenv v2 block in
         venv, aenv
   in

   (* Resolve unknown fields *)
   let make_label_field ic subfields default2 =
      match ic with
         IVarUnknown (vc, v) ->
            let depth = depth_of_vclass vc in
            let venv, aenv = new_label_field depth subfields default2 in
            let venv = venv_add_subst venv v depth (make_htree loc (HTETree (ETVar v1))) in
               venv, aenv
       | IVarVar (vc, _)
       | IVarExp vc
       | IVarInd (vc, _, _, _) ->
            if vclass_has_type genv venv pos vc ty op then
               venv, aenv
            else
               new_label_field depth subfields default2
   in
      try
         let subfields, default2 = SymbolTable.find fields field in
            try
               let ic = SymbolTable.find subfields subfield in
                  make_label_field ic subfields default2
            with
               Not_found ->
                  new_label_field depth' subfields default2
      with
         Not_found ->
            new_label_field depth' SymbolTable.empty default1

(*
 * Fetch a field with a variable index from a block.
 *)
let alias_element_var genv venv aenv pos loc depth op v1 ty e1 e2 v2 e =
   let pos = string_pos "alias_element_var" pos in

   (* Get the block *)
   let block = aenv_lookup_block aenv pos v2 in
   let { block_depth = depth';
         block_var_fields = fields;
         block_default = default;
         block_length_heap = length_heap
       } = block
   in
   let depth' = max_depth depth depth' in

   (* New field *)
   let new_var_field depth fields =
      let venv, aenv, default, ic = alias_new_field genv venv aenv depth pos loc op v1 ty default length_heap e1 e2 in
      let fields = ETreeTable.add fields e ic in
      let block =
         { block with block_var_fields = fields;
                      block_default = default
         }
      in
      let aenv = aenv_add_block aenv v2 block in
         venv, aenv
   in

   (* Catch unknown fields *)
   let make_var_field ic fields =
      match ic with
         IVarUnknown (vc, v) ->
            let depth = depth_of_vclass vc in
            let venv, aenv = new_var_field depth fields in
            let venv = venv_add_subst venv v depth (make_htree loc (HTETree (ETVar v1))) in
               venv, aenv
       | IVarVar (vc, _)
       | IVarExp vc
       | IVarInd (vc, _, _, _) ->
            if vclass_has_type genv venv pos vc ty op then
               venv, aenv
            else
               new_var_field depth fields
   in
      try
         let ic = ETreeTable.find fields e in
            make_var_field ic fields
      with
         Not_found ->
            new_var_field depth' fields

(*
 * Look up a field from a block.
 *   v1: the variable being assigned
 *   ty: the type of v1
 *   e2: the array being subscripted
 *   v2: the array must-alias for e2
 *   off2: the offset into the array alias
 *   e2: the subscript
 *)
let alias_element_check genv venv aenv pos loc depth label op ty e1 v2 off2 e2 =
   let pos = string_pos "alias_element_check" pos in
   let v1 = new_symbol label in
   let e = expand_subscript_sum venv pos op off2 e2 in
   let venv, aenv =
      match e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_element_int genv venv aenv pos loc depth op v1 ty e1 e2 v2 i
       | ETConst (AtomRawInt i) ->
            alias_element_int genv venv aenv pos loc depth op v1 ty e1 e2 v2 (Rawint.to_int i)
       | ETConst (AtomLabel (label, off))
         when Rawint.is_zero off ->
            alias_element_label genv venv aenv pos loc depth op v1 ty e1 e2 v2 label
       | _ ->
            alias_element_var genv venv aenv pos loc depth op v1 ty e1 e2 v2 e
   in
      venv, aenv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
