(*
 * Define an abstract partial order.
 * Define it so that native comparisons
 * do not work.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

(*
 * This is a total hack.
 * the problem is that OCaml allows native comparisons
 * on all OCaml values, but we want to force the code
 * to use our comparison.  So we add an abstract element,
 * that we initialize from C.
 *)
type abs

type depth =
   MinDepth
 | SomeDepth of abs * symbol * int
 | MaxDepth

external new_abstract : unit -> abs = "new_abstract"

let zero_depth = MinDepth

let create_depth v i =
   SomeDepth (new_abstract (), v, i)

let is_zero_depth = function
   MinDepth ->
      true
 | SomeDepth _
 | MaxDepth ->
      false

let succ_depth depth =
   match depth with
      MinDepth ->
         raise (Invalid_argument "succ_depth")
    | MaxDepth ->
         depth
    | SomeDepth (a, v, i) ->
         SomeDepth (a, v, succ i)

let pred_depth depth =
   match depth with
      MinDepth
    | MaxDepth ->
         depth
    | SomeDepth (a, v, i) ->
         SomeDepth (a, v, pred i)

let max_depth depth1 depth2 =
   match depth1, depth2 with
      MaxDepth, _
    | _, MaxDepth ->
         MaxDepth
    | MinDepth, MinDepth ->
         MinDepth
    | SomeDepth (a1, v1, i1), SomeDepth (a2, v2, i2) ->
         if Symbol.eq v1 v2 then
            SomeDepth (a1, v1, max i1 i2)
         else
            MaxDepth
    | SomeDepth _, MinDepth ->
         depth1
    | MinDepth, SomeDepth _ ->
         depth2

let pp_print_depth buf depth =
   match depth with
      MinDepth ->
         pp_print_string buf "<min-depth>"
    | MaxDepth ->
         pp_print_string buf "<max-depth>"
    | SomeDepth (_, v, i) ->
         fprintf buf "%s.%d" (string_of_symbol v) i

(*
 * Relations.
 *)
let compare_depth depth1 depth2 =
   match depth1, depth2 with
      MaxDepth, MaxDepth
    | MinDepth, MinDepth ->
         0
    | SomeDepth (_, v1, i1), SomeDepth (_, v2, i2) ->
         let cmp = Symbol.compare v1 v2 in
            if cmp = 0 then
               i1 - i2
            else
               cmp
    | MinDepth, SomeDepth _
    | MinDepth, MaxDepth
    | SomeDepth _, MaxDepth ->
         -1
    | SomeDepth _, MinDepth
    | MaxDepth, MinDepth
    | MaxDepth, SomeDepth _ ->
         1

(*
 * Equality.
 *)
let eq_depth depth1 depth2 =
   compare_depth depth1 depth2 = 0

let le_depth depth1 depth2 =
   match depth1, depth2 with
      SomeDepth (_, v1, i1), SomeDepth (_, v2, i2) ->
         if Symbol.eq v1 v2 then
            i1 <= i2
         else
            false
    | _ ->
         compare_depth depth1 depth2 <= 0

let lt_depth depth1 depth2 =
   match depth1, depth2 with
      SomeDepth (_, v1, i1), SomeDepth (_, v2, i2) ->
         if Symbol.eq v1 v2 then
            i1 < i2
         else
            false
    | _ ->
         compare_depth depth1 depth2 < 0

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
