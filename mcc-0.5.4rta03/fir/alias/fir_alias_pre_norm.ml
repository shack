(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

module Pos = MakePos (struct let name = "Fir_alias_pre_norm" end)
open Pos

(*
 * At this point, we have a table of aliases for all the variables in the program.
 * Expand all definitions in all the etrees, and expand all heap sets
 * in all htrees.
 *)

(*
 * Expand an expression tree.
 *)
let rec expand_etree_core aenv (e : etree_core) =
   if false && debug debug_alias then
      Format.printf "@[<hv 3>expand_etree_core:@ %a@]@." pp_print_etree_core e;
   match e with
      ETConst _ ->
         e
    | ETVar v ->
         (try
             match dest_aclass_core (SymbolTable.find aenv v) with
                AliasInduction (_, v', _, op, v, mul, add, _, _) ->
                   ETVar v'
              | AliasExp (_, HTETree (ETVar v'))
                when Symbol.eq v' v ->
                   ETVar v
              | AliasExp (_, HTETree e) ->
                   expand_etree_core aenv e
              | _ ->
                   e
          with
             Not_found ->
                e)
    | ETUnop (op, e) ->
         ETUnop (op, expand_etree_core aenv e)
    | ETBinop (op, e1, e2) ->
         ETBinop (op, expand_etree_core aenv e1, expand_etree_core aenv e2)

(*
 * Normalize the expression, and optimize it.
 *)
let normalize_etree_core aenv pos e =
   let pos = string_pos "normalize_etree_core" pos in
   let e = expand_etree_core aenv e in
      if false && debug debug_alias then
         Format.printf "@[<hv 3>normalize_etree_core:@ %a@]@." pp_print_etree_core e;
      optimize_etree_core pos (canonicalize_etree_core pos e)

let normalize_etree_core_list aenv pos el =
   List.map (normalize_etree_core aenv pos) el

(*
 * Normalize a heap.
 * Expand all dependencies.
 *)
let normalize_heap aenv s =
   let rec expand_heap aenv s1 s2 =
      SymbolSet.fold (fun s1 v ->
            try
               match dest_aclass_core (SymbolTable.find aenv v) with
                  AliasExp (_, HTDepends s2) ->
                     let aenv = SymbolTable.remove aenv v in
                        expand_heap aenv s1 s2
                | _ ->
                     SymbolSet.add s1 v
            with
               Not_found ->
                  SymbolSet.add s1 v) s1 s2
   in
      expand_heap aenv SymbolSet.empty s

(*
 * Expand an allocation.
 *)
let normalize_alloc_op aenv pos op =
   let pos = string_pos "normalize_alloc_op" pos in
      match op with
         AllocTuple (tclass, ty, ty_vars, el) ->
            AllocTuple (tclass, ty, ty_vars, normalize_etree_core_list aenv pos el)
       | AllocDTuple (ty, ty_var, e, el) ->
            AllocDTuple (ty, ty_var, normalize_etree_core aenv pos e, normalize_etree_core_list aenv pos el)
       | AllocUnion (ty, ty_vars, v, i, el) ->
            AllocUnion (ty, ty_vars, v, i, normalize_etree_core_list aenv pos el)
       | AllocArray (ty, el) ->
            AllocArray (ty, normalize_etree_core_list aenv pos el)
       | AllocVArray (ty, op, e1, e2) ->
            AllocVArray (ty, op, normalize_etree_core aenv pos e1, normalize_etree_core aenv pos e2)
       | AllocMalloc (ty, e) ->
            AllocMalloc (ty, normalize_etree_core aenv pos e)
       | AllocFrame _ ->
            op

(*
 * Expand a predicate.
 *)
let normalize_pred aenv pos pred =
   let pos = string_pos "normalize_pred" pos in
      match pred with
         IsMutable e ->
            IsMutable (normalize_etree_core aenv pos e)
       | Reserve (e1, e2) ->
            Reserve (normalize_etree_core aenv pos e1,
                     normalize_etree_core aenv pos e2)
       | ElementCheck (ty, op, e1, e2) ->
            ElementCheck (ty, op,
                          normalize_etree_core aenv pos e1,
                          normalize_etree_core aenv pos e2)
       | BoundsCheck (op, e1, e2, e3) ->
            BoundsCheck (op,
                         normalize_etree_core aenv pos e1,
                         normalize_etree_core aenv pos e2,
                         normalize_etree_core aenv pos e3)

(*
 * Expand an htree.
 * Normalize the heap set, as well as the expressions.
 *)
let normalize_htree_core aenv pos h =
   let pos = string_pos "normalize_htree" pos in
      match h with
         HTETree e ->
            HTETree (normalize_etree_core aenv pos e)
       | HTAlloc (heap, op) ->
            HTAlloc (normalize_heap aenv heap,
                     normalize_alloc_op aenv pos op)
       | HTSubscript (heap, op, ty, e1, e2) ->
            HTSubscript (normalize_heap aenv heap, op, ty,
                         normalize_etree_core aenv pos e1,
                         normalize_etree_core aenv pos e2)
       | HTSetSubscript (heap, op, ty, e1, e2, e3) ->
            HTSetSubscript (normalize_heap aenv heap, op, ty,
                            normalize_etree_core aenv pos e1,
                            normalize_etree_core aenv pos e2,
                            normalize_etree_core aenv pos e3)
       | HTMemcpy (heap, op, e1, e2, e3, e4, e5) ->
            HTMemcpy (normalize_heap aenv heap, op,
                      normalize_etree_core aenv pos e1,
                      normalize_etree_core aenv pos e2,
                      normalize_etree_core aenv pos e3,
                      normalize_etree_core aenv pos e4,
                      normalize_etree_core aenv pos e5)
       | HTExt (heap, ty1, s, b, ty2, ty_args, args) ->
            HTExt (heap, ty1, s, b, ty2, ty_args, List.map (normalize_etree_core aenv pos) args)
       | HTAssert (heap, pred) ->
            HTAssert (normalize_heap aenv heap,
                      normalize_pred aenv pos pred)
       | HTDepends heap ->
            HTDepends (normalize_heap aenv heap)

let normalize_htree aenv pos h =
   let loc = loc_of_htree h in
   let h = normalize_htree_core aenv pos (dest_htree_core h) in
      make_htree loc h

(*
 * Normalize an alias.
 *)
let normalize_alias_core aenv pos ac =
   let pos = string_pos "normalize_alias" pos in
      match ac with
         AliasInduction (depth, v1, g, op, v2, mul, add, step, base) ->
            AliasInduction (depth, v1, g, op, v2,
                            normalize_etree_core aenv pos mul, normalize_etree_core aenv pos add,
                            normalize_etree_core aenv pos step, normalize_etree_core aenv pos base)
       | AliasExp (depth, h) ->
            AliasExp (depth, normalize_htree_core aenv pos h)
       | AliasAlias (depth, e, off, v, s, l) ->
            AliasAlias (depth,
                        normalize_etree_core aenv pos e,
                        normalize_etree_core aenv pos off,
                        v, s, l)

let normalize_alias aenv pos ac =
   let loc = loc_of_aclass ac in
   let ac = normalize_alias_core aenv pos (dest_aclass_core ac) in
      make_aclass loc ac

(*
 * Normalize all the expressions in the substitution environment.
 *)
let normalize_aenv aenv =
   SymbolTable.mapi (fun v ac ->
         let pos = string_pos "normalize_aenv" (var_exp_pos v) in
            normalize_alias aenv pos ac) aenv


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
