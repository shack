(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol
open Location
open Attribute

open Fir
open Fir_exn
open Fir_pos
open Fir_algebra

open Fir_alias
open Fir_alias_type

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_alias_util" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Subscript arithmetic.
 *)
let zero_subscript = AtomRawInt (Rawint.of_int precision_subscript signed_subscript 0)
let et_zero_subscript = ETConst zero_subscript
let ty_subscript = TyRawInt (precision_subscript, signed_subscript)

let plus_subscript_op = PlusRawIntOp (precision_subscript, signed_subscript)
let min_subscript_op = MinRawIntOp (precision_subscript, signed_subscript)

(*
 * Term operations.
 *)
let loc_of_vclass = label_of_simple_term
let make_vclass = wrap_simple_core
let dest_vclass_core = dest_simple_core

let loc_of_aclass = label_of_simple_term
let make_aclass = wrap_simple_core
let dest_aclass_core = dest_simple_core

(*
 * Get an integer from an atom.
 *)
let int_of_etree_core e default =
   match e with
      ETConst (AtomInt i)
    | ETConst (AtomEnum (_, i)) ->
         i
    | ETConst (AtomRawInt i) ->
         Rawint.to_int i
    | _ ->
         default

let int_of_etree e default =
   int_of_etree_core (dest_etree_core e) default

(*
 * Get the etree from the vclass.
 *)
let vclass_of_iclass = function
   IVarUnknown (e, _)
 | IVarVar (e, _)
 | IVarExp e
 | IVarInd (e, _, _, _) ->
      e

let loc_of_iclass ic =
   loc_of_vclass (vclass_of_iclass ic)

(*
 * Get the depth.
 *)
let depth_of_vclass vc =
   match dest_vclass_core vc with
      VarExp (depth, _)
    | VarAlias (depth, _, _, _, _, _) ->
         depth

let depth_of_aclass ac =
   match dest_aclass_core ac with
      AliasInduction (depth, _, _, _, _, _, _, _, _)
    | AliasAlias (depth, _, _, _, _, _)
    | AliasExp (depth, _) ->
         depth

(*
 * Get the operator class.
 *)
let int_class_of_binop op =
   match op with
      (* enums *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

      (* naml ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp ->
         IntClass

      (* Pointer arithmetic *)
    | PlusPointerOp (op, pre, signed) ->
         PointerClass (op, pre, signed)

      (* Rawints *)
    | PlusRawIntOp   (pre, signed)
    | MinusRawIntOp  (pre, signed)
    | MulRawIntOp    (pre, signed)
    | DivRawIntOp    (pre, signed)
    | RemRawIntOp    (pre, signed)
    | SlRawIntOp     (pre, signed)
    | SrRawIntOp     (pre, signed)
    | AndRawIntOp    (pre, signed)
    | OrRawIntOp     (pre, signed)
    | XorRawIntOp    (pre, signed)
    | MaxRawIntOp    (pre, signed)
    | MinRawIntOp    (pre, signed)

    | EqRawIntOp     (pre, signed)
    | NeqRawIntOp    (pre, signed)
    | LtRawIntOp     (pre, signed)
    | LeRawIntOp     (pre, signed)
    | GtRawIntOp     (pre, signed)
    | GeRawIntOp     (pre, signed)
    | CmpRawIntOp    (pre, signed) ->
         RawIntClass (pre, signed)

      (* Rawints not handled *)
    | RawSetBitFieldOp _

      (* Floats *)
    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

    | ATan2FloatOp _
    | PowerFloatOp _
    | LdExpFloatIntOp _

      (* Pointer equality *)
    | EqEqOp _
    | NeqEqOp _ ->
         raise (Invalid_argument "int_class_of_op")

(*
 * Integers from operators.
 *)
let etree_of_int op i =
   match op with
      IntClass ->
         ETConst (AtomInt i)
    | RawIntClass (pre, signed)
    | PointerClass (_, pre, signed) ->
         ETConst (AtomRawInt (Rawint.of_int pre signed i))

let negate_etree op e =
   let op =
      match op with
         IntClass ->
            UMinusIntOp
       | RawIntClass (pre, signed)
       | PointerClass (_, pre, signed) ->
            UMinusRawIntOp (pre, signed)
   in
      ETUnop (op, e)

let add_etree op e1 e2 =
   let op =
      match op with
         IntClass ->
            PlusIntOp
       | RawIntClass (pre, signed) ->
            PlusRawIntOp (pre, signed)
       | PointerClass (op, pre, signed) ->
            PlusPointerOp (op, pre, signed)
   in
      ETBinop (op, e1, e2)

let mul_etree op e1 e2 =
   let op =
      match op with
         IntClass ->
            MulIntOp
       | RawIntClass (pre, signed)
       | PointerClass (_, pre, signed) ->
            MulRawIntOp (pre, signed)
   in
      ETBinop (op, e1, e2)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
