(*
 * Alias utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Fir
open Fir_exn
open Fir_algebra

open Fir_depth
open Fir_alias
open Fir_alias_type

(*
 * Constants.
 *)
val ty_subscript : ty
val zero_subscript : atom
val et_zero_subscript : etree_core
val etree_of_int : int_class -> int -> etree_core
val int_of_etree : etree -> int -> int
val int_of_etree_core : etree_core -> int -> int

val min_subscript_op : binop

val int_class_of_binop : binop -> int_class

(*
 * Term operations.
 *)
val loc_of_vclass : vclass -> loc
val make_vclass : loc -> vclass_core -> vclass
val dest_vclass_core : vclass -> vclass_core

val loc_of_iclass : iclass -> loc

val loc_of_aclass : aclass -> loc
val make_aclass : loc -> aclass_core -> aclass
val dest_aclass_core : aclass -> aclass_core

(*
 * ETree operations.
 *)
val negate_etree : int_class -> etree_core -> etree_core
val add_etree    : int_class -> etree_core -> etree_core -> etree_core
val mul_etree    : int_class -> etree_core -> etree_core -> etree_core

(*
 * Conversions.
 *)
val vclass_of_iclass : iclass -> vclass

(*
 * Other utilities.
 *)
val depth_of_vclass : vclass -> depth
val depth_of_aclass : aclass -> depth

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
