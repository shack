(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_pos
open Fir_exn
open Fir_heap
open Fir_algebra

open Fir_depth
open Fir_alias_type
open Fir_alias_util
open Fir_alias_print

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_alias_env" end)
open Pos

let minus_subscript_op = MinusRawIntOp (precision_subscript, signed_subscript)

(*
 * Empty alias environment.
 *)
let aenv_empty loc =
   { aenv_loc = loc;
     aenv_reserve_heap = SymbolSet.empty;
     aenv_alloc_heap = SymbolSet.empty;
     aenv_blocks = SymbolTable.empty;
     aenv_subst = SymbolTable.empty
   }

(*
 * Add an alias entry.
 *)
let aenv_mem_block aenv v =
   SymbolTable.mem aenv.aenv_blocks v

let aenv_lookup_block_exn aenv v =
   SymbolTable.find aenv.aenv_blocks v

let aenv_lookup_block aenv pos v =
   try aenv_lookup_block_exn aenv v with
      Not_found ->
         let pos = string_pos "aenv_lookup_block" pos in
            raise (FirException (pos, UnboundVar v))

let aenv_add_block aenv v block =
   { aenv with aenv_blocks = SymbolTable.add aenv.aenv_blocks v block }

(*
 * Alias class expansions.
 *)
let aenv_expand aenv v s =
   let subst = aenv.aenv_subst in
   let rec expand s1 s2 =
      SymbolSet.fold (fun s1 v ->
            let s1 = SymbolSet.add s1 v in
            let s2 =
               try Some (SymbolTable.find subst v) with
                  Not_found ->
                     None
            in
               match s2 with
                  Some s2 ->
                     expand s1 s2
                | None ->
                     s1) s1 s2
   in
   let s = expand SymbolSet.empty (SymbolSet.add s v) in
      SymbolSet.remove s v

(*
 * Add an alias subst.
 *)
let aenv_add_alias aenv v1 v2 =
   let subst =
      SymbolTable.filter_add aenv.aenv_subst v1 (fun s ->
            let s =
               match s with
                  Some s ->
                     s
                | None ->
                     SymbolSet.empty
            in
               SymbolSet.add s v2)
   in
      { aenv with aenv_subst = subst }

(*
 * Empty environment.
 *)
let venv_empty =
   { venv_info    = SymbolTable.empty;
     venv_subst   = SymbolTable.empty;
     venv_funs    = SymbolTable.empty;
     venv_vars    = SymbolTable.empty;
     venv_names   = NameTable.empty;
     venv_unknown = SymbolTable.empty;
     venv_calls   = SymbolTable.empty;
     venv_loops   = SymbolTable.empty
   }

(*
 * Loop info.
 *)
let venv_add_loop venv v s =
   { venv with venv_loops = SymbolTable.add venv.venv_loops v s }

let venv_is_loop venv v =
   SymbolTable.mem venv.venv_loops v

let venv_lookup_loop venv pos v =
   try SymbolTable.find venv.venv_loops v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Add an entry to the variable environment.
 *)
let venv_add_info venv v ic aenv_before pred_before aenv_after pred_after =
   let info =
      { ainfo_class = ic;
        ainfo_env_before = aenv_before;
        ainfo_pred_before = pred_before;
        ainfo_env_after = aenv_after;
        ainfo_pred_after = pred_after
      }
   in
      { venv with venv_info = SymbolTable.add venv.venv_info v info }

let venv_lookup_info venv v =
   SymbolTable.find venv.venv_info v

(*
 * Substitutions.
 *)
let venv_add_subst venv v depth ac =
   { venv with venv_subst = SymbolTable.add venv.venv_subst v (depth, ac) }

let venv_remove_subst venv v =
   { venv with venv_subst = SymbolTable.remove venv.venv_subst v }

let venv_subst venv v =
   SymbolTable.find venv.venv_subst v

(*
 * Function parameters.
 *)
let venv_add_fun venv f info =
   { venv with venv_funs = SymbolTable.add venv.venv_funs f info }

let venv_lookup_fun_exn venv f =
   SymbolTable.find venv.venv_funs f

let venv_lookup_fun venv pos f =
   try venv_lookup_fun_exn venv f with
      Not_found ->
         raise (FirException (pos, UnboundVar f))

(*
 * Loop environment.
 *)
let venv_add_var venv v e =
   { venv with venv_vars = SymbolTable.add venv.venv_vars v e }

let venv_lookup_var_exn venv v =
   SymbolTable.find venv.venv_vars v

let venv_lookup_var venv pos v =
   let pos = string_pos "venv_lookup_var" pos in
      try venv_lookup_var_exn venv v with
         Not_found ->
            raise (FirException (pos, UnboundVar v))

let venv_lookup_atom venv pos loc a =
   let pos = string_pos "venv_lookup_atom" pos in
      match a with
         AtomVar v ->
            venv_lookup_var venv pos v
       | _ ->
            make_vclass loc (VarExp (zero_depth, ETConst a))

(*
 * Make up a new symbol name unless it already exists.
 *)
let venv_new_name venv l =
   try venv, NameTable.find venv.venv_names l with
      Not_found ->
         let v = new_symbol_string "name" in
         let venv = { venv with venv_names = NameTable.add venv.venv_names l v } in
            venv, v

(*
 * Add a loop call.
 *)
let venv_add_call venv v call =
   { venv with venv_calls = SymbolTable.add venv.venv_calls v call }

let venv_lookup_call venv pos v =
   try SymbolTable.find venv.venv_calls v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Unknnowns.
 *)
let venv_add_unknown venv v depth =
   { venv with venv_unknown = SymbolTable.add venv.venv_unknown v depth }

let venv_lookup_unknown venv v =
   SymbolTable.find venv.venv_unknown v

(*
 * Call environment.
 *)
let cenv_add_param cenv v =
   { cenv with cenv_ps_vars = SymbolSet.add cenv.cenv_ps_vars v }

let cenv_add_arg cenv f v depth e =
   let table =
      SymbolTable.filter_add cenv.cenv_ps_args f (fun table ->
            let table =
               match table with
                  Some table ->
                     table
                | None ->
                     SymbolTable.empty
            in
               SymbolTable.add table v (depth, e))
   in
      { cenv with cenv_ps_args = table }

(*
 * Add a may-alias relation.
 *)
let cenv_add_alias cenv v v1 v2 =
   { cenv with cenv_alias = SymbolTable.add cenv.cenv_alias v (v1, v2) }

(*
 * Induction environment.
 *)
let ienv_empty =
   { ienv_ind_vars = SymbolTable.empty;
     ienv_vars = SymbolTable.empty
   }

let ienv_add_ind_var ienv v ac =
   { ienv with ienv_ind_vars = SymbolTable.add ienv.ienv_ind_vars v ac }

let ienv_lookup_ind_var ienv v =
   SymbolTable.find ienv.ienv_ind_vars v

let ienv_add_var ienv v ac =
   { ienv with ienv_vars = SymbolTable.add ienv.ienv_vars v ac }

let ienv_lookup_var ienv v =
   SymbolTable.find ienv.ienv_vars v

(*
 * CSE environment.
 *)
let gvenv_empty =
   { gvenv_values = SymbolTable.empty;
     gvenv_var_table = SymbolTable.empty;
     gvenv_induction_table = NameTable.empty;
     gvenv_htree_table = HTreeTable.empty;
     gvenv_etree_table = ETreeTable.empty
   }

(*
 * Var hashing.
 *)
let gvenv_add_value gvenv v ac e =
   { gvenv with gvenv_values = SymbolTable.add gvenv.gvenv_values v (ac, e) }

let gvenv_lookup_value_exn gvenv v =
   SymbolTable.find gvenv.gvenv_values v

let gvenv_lookup_value gvenv pos v =
   try gvenv_lookup_value_exn gvenv v with
      Not_found ->
         let pos = string_pos "gvenv_lookup_value" pos in
            raise (FirException (pos, UnboundVar v))

(*
 * Var definition.
 *)
let gvenv_add_var gvenv v ac e =
   { gvenv with gvenv_var_table = SymbolTable.add gvenv.gvenv_var_table v (ac, e) }

let gvenv_lookup_var gvenv v =
   SymbolTable.find gvenv.gvenv_var_table v

(*
 * Induction var hashing.
 *)
let gvenv_add_induction_key gvenv key ac e =
   { gvenv with gvenv_induction_table = NameTable.add gvenv.gvenv_induction_table key (ac, e) }

let gvenv_lookup_induction_key gvenv key =
   NameTable.find gvenv.gvenv_induction_table key

(*
 * Tree hashing.
 *)
let gvenv_add_htree gvenv h ac e =
   { gvenv with gvenv_htree_table = HTreeTable.add gvenv.gvenv_htree_table h (ac, e) }

let gvenv_lookup_htree gvenv h =
   HTreeTable.find gvenv.gvenv_htree_table h

let gvenv_add_etree gvenv e ac e' =
   { gvenv with gvenv_etree_table = ETreeTable.add gvenv.gvenv_etree_table e (ac, e') }

let gvenv_lookup_etree gvenv e =
   ETreeTable.find gvenv.gvenv_etree_table e

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
