(*
 * Common-subexpression elimination.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_pos
open Fir_depth
open Fir_algebra

open Fir_alias
open Fir_alias_type

val cse_etree : gvenv -> aclass SymbolTable.t -> pos -> depth -> etree_core -> gvenv * aclass * etree_core
val cse_normalize_etree : gvenv -> aclass SymbolTable.t -> pos -> depth -> etree_core -> gvenv * aclass * etree_core

val cse_prog : aclass SymbolTable.t -> (var list * var SymbolTable.t) SymbolTable.t -> gvenv * (etree list * etree SymbolTable.t) SymbolTable.t

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
