(* Fir constant folding, algebraic simplification, and reassociation
 * Geoffrey Irving
 * $Id: fir_algebra.ml,v 1.9 2002/09/02 21:10:33 jyh Exp $ *)

open Symbol
open Location
open Attribute

open Fir
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_print

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_algebra" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

type etree_core =
   ETConst of atom
 | ETVar of var
 | ETUnop of unop * etree_core
 | ETBinop of binop * etree_core * etree_core

type etree = etree_core simple_subst

(*
 * Operator classes.
 * This is a lot more conventient than checking
 * each operator individually.
 *)
type op_class =
   PlusClass
 | MinusClass
 | MulClass
 | DivClass
 | RemClass
 | AndClass
 | OrClass
 | XorClass
 | OtherClass

(*
 * Exception if constants can't be evaled.
 * This exception does not escape this module.
 *)
exception EvalError

(************************************************************************
 * TERM OPERATIONS
 ************************************************************************)

let loc_of_etree = label_of_simple_term
let make_etree = wrap_simple_core
let dest_etree_core = dest_simple_core

(************************************************************************
 * PRINTING
 ************************************************************************)

let rec pp_print_etree_core buf t =
   match t with
      ETConst a ->
         Format.fprintf buf "const(%a)" pp_print_atom a
    | ETVar v ->
         pp_print_symbol buf v
    | ETUnop (op, t) ->
         Format.fprintf buf "@[<hv 3>(%s%a)@] " (string_of_unop op) pp_print_etree_core t
    | ETBinop (op, t1, t2) ->
         Format.fprintf buf "@[<hv 3>(%a@ %s %a)@]" (**)
            pp_print_etree_core t1
            (string_of_binop op)
            pp_print_etree_core t2

let pp_print_etree buf t =
   pp_print_etree_core buf (dest_etree_core t)

(************************************************************************
 * UTILITIES
 ************************************************************************)

let atom_false = AtomEnum (2, 0)
let atom_true = AtomEnum (2, 1)

let et_int_zero = ETConst (AtomInt 0)
let et_rawint_zero p s = ETConst (AtomRawInt (Rawint.of_int p s 0))
let et_subscript_zero = et_rawint_zero precision_subscript signed_subscript
let et_1div0 = ETBinop (DivIntOp, ETConst (AtomInt 1), ETConst (AtomInt 0))

let ty_label = TyRawInt (precision_subscript, signed_subscript)

let test b =
   if b then
      atom_true
   else
      atom_false

let rec etree_of_atom = function
   AtomVar v -> ETVar v
 | AtomUnop (op, a) -> ETUnop (op, etree_of_atom a)
 | AtomBinop (op, a1, a2) -> ETBinop (op, etree_of_atom a1, etree_of_atom a2)
 | a -> ETConst a

(*
 * Classes of operators.
 *)
let op_class op =
   match op with
      PlusIntOp
    | PlusRawIntOp _
    | PlusFloatOp _ ->
         PlusClass
    | MinusIntOp
    | MinusRawIntOp _
    | MinusFloatOp _ ->
         MinusClass
    | MulIntOp
    | MulRawIntOp _ ->
         MulClass
    | DivIntOp
    | DivRawIntOp _ ->
         DivClass
    | RemIntOp
    | RemRawIntOp _ ->
         RemClass
    | AndIntOp
    | AndEnumOp _
    | AndRawIntOp _ ->
         AndClass
    | OrIntOp
    | OrEnumOp _
    | OrRawIntOp _ ->
         OrClass
    | XorIntOp
    | XorEnumOp _
    | XorRawIntOp _ ->
         XorClass
    | _ ->
         OtherClass

(*
 * In some cases, we squash some classes further.
 * See associate3 below for details.
 *)
let op_class_squash cl =
   match cl with
      PlusClass
    | MinusClass ->
         PlusClass
    | _ ->
         cl

(*
 * Negate an addition operator.
 *)
let negate_op op =
   match op with
      MinusIntOp ->
         PlusIntOp
    | MinusRawIntOp (p, s) ->
         PlusRawIntOp (p, s)
    | MinusFloatOp p ->
         PlusFloatOp p
    | _ ->
         raise (Invalid_argument "negate_op")

(*
 * Get the variable.
 *)
let var_of_etree pos e =
   match dest_etree_core e with
      ETVar v ->
         v
    | _ ->
         raise (FirException (pos, StringFormatError ("not a variable", (fun buf -> pp_print_etree buf e))))

(************************************************************************
 * TABLES
 ************************************************************************)

module ETreeCompare =
struct
   type t = etree_core

   let compare = Pervasives.compare
end

module ETreeTable = Mc_map.McMake (ETreeCompare)

let equal_etree_core e1 e2 =
   e1 = e2

let equal_etree e1 e2 =
   equal_etree_core (dest_etree_core e1) (dest_etree_core e2)

(************************************************************************
 * CONSTANT FOLDING
 ************************************************************************)

(*
 * Constant evaluation for unops.
 *)
let compute_unop op c =
   let c =
      match op, c with
         UMinusIntOp,                    AtomInt i       -> AtomInt (~-i)
       | NotIntOp,                       AtomInt i       -> AtomInt (lnot i)
       | UMinusRawIntOp _,               AtomRawInt i    -> AtomRawInt (Rawint.neg i)
       | NotRawIntOp _,                  AtomRawInt i    -> AtomRawInt (Rawint.lognot i)
       | UMinusFloatOp _,                AtomFloat x     -> AtomFloat (Rawfloat.neg x)
       | AbsFloatOp _,                   AtomFloat x     -> AtomFloat (Rawfloat.abs x)
       | SinFloatOp _,                   AtomFloat x     -> AtomFloat (Rawfloat.sin x)
       | CosFloatOp _,                   AtomFloat x     -> AtomFloat (Rawfloat.cos x)
(* BUG: we should add the other trig functions *)
       | SqrtFloatOp _,                  AtomFloat x     -> AtomFloat (Rawfloat.sqrt x)
       | IntOfFloatOp _,                 AtomFloat x     -> AtomInt (Rawfloat.to_int x)
       | FloatOfIntOp p,                 AtomInt i       -> AtomFloat (Rawfloat.of_int p i)
       | FloatOfFloatOp (p, _),          AtomFloat x     -> AtomFloat (Rawfloat.of_rawfloat p x)
       | RawIntOfEnumOp (p, s, _),       AtomEnum (_, i) -> AtomRawInt (Rawint.of_int p s i)
       | RawIntOfFloatOp (p, s, _),      AtomFloat x     -> AtomRawInt (Rawfloat.to_rawint p s x)
       | FloatOfRawIntOp (p, _, _),      AtomRawInt i    -> AtomFloat (Rawfloat.of_rawint p i)
       | RawIntOfRawIntOp (p, s, _, _),  AtomRawInt i    -> AtomRawInt (Rawint.of_rawint p s i)
       | RawBitFieldOp (p, s, off, len), AtomRawInt i    -> AtomRawInt (Rawint.field i off len)
       | _ -> raise EvalError
   in
      ETConst c

let compute_unop op c =
   try compute_unop op c with
      EvalError ->
         let t = ETConst c in
            match op with
               UMinusIntOp ->
                  ETBinop (MinusIntOp, et_int_zero, t)
             | UMinusRawIntOp (p, s) ->
                  ETBinop (MinusRawIntOp (p, s), et_rawint_zero p s, t)
             | _ ->
                  ETUnop (op, t)

(*
 * Constant evaluation for binops.
 * Don't worry about division by zero,
 * we'll catch it below as an exception.
 *)
let compute_binop_unprotected op c1 c2 =
   let c =
      match c1, c2 with
         AtomInt i, AtomInt j ->
            (match op with
                PlusIntOp  -> AtomInt (i + j)
              | MinusIntOp -> AtomInt (i - j)
              | MulIntOp   -> AtomInt (i * j)
              | DivIntOp   -> AtomInt (i / j)
              | RemIntOp   -> AtomInt (i mod j)
              | LslIntOp   -> AtomInt (i lsl j)
              | LsrIntOp   -> AtomInt (i lsr j)
              | AsrIntOp   -> AtomInt (i asr j)
              | AndIntOp   -> AtomInt (i land j)
              | OrIntOp    -> AtomInt (i lor j)
              | XorIntOp   -> AtomInt (i lxor j)
              | MinIntOp   -> AtomInt (min i j)
              | MaxIntOp   -> AtomInt (max i j)
              | EqIntOp  -> test (i == j)
              | NeqIntOp -> test (i != j)
              | LtIntOp  -> test (i <  j)
              | LeIntOp  -> test (i <= j)
              | GtIntOp  -> test (i >  j)
              | GeIntOp  -> test (i >= j)
              | CmpIntOp -> AtomInt (Pervasives.compare i j)
              | EqEqOp _ -> test (i == j)
              | NeqEqOp _ -> test (i != j)
              | _ -> raise EvalError)
       | AtomRawInt i, AtomRawInt j ->
            (match op with
                PlusRawIntOp _  -> AtomRawInt (Rawint.add i j)
              | MinusRawIntOp _ -> AtomRawInt (Rawint.sub i j)
              | MulRawIntOp _   -> AtomRawInt (Rawint.mul i j)
              | DivRawIntOp _   -> AtomRawInt (Rawint.div i j)
              | RemRawIntOp _   -> AtomRawInt (Rawint.rem i j)
              | SlRawIntOp _    -> AtomRawInt (Rawint.shift_left i j)
              | SrRawIntOp _    -> AtomRawInt (Rawint.shift_right i j)
              | AndRawIntOp _   -> AtomRawInt (Rawint.logand i j)
              | OrRawIntOp _    -> AtomRawInt (Rawint.logor i j)
              | XorRawIntOp _   -> AtomRawInt (Rawint.logxor i j)
              | MinRawIntOp _   -> AtomRawInt (Rawint.min i j)
              | MaxRawIntOp _   -> AtomRawInt (Rawint.max i j)
              | EqRawIntOp _  -> test (Rawint.compare i j == 0)
              | NeqRawIntOp _ -> test (Rawint.compare i j != 0)
              | LtRawIntOp _  -> test (Rawint.compare i j <  0)
              | LeRawIntOp _  -> test (Rawint.compare i j <= 0)
              | GtRawIntOp _  -> test (Rawint.compare i j >  0)
              | GeRawIntOp _  -> test (Rawint.compare i j >= 0)
              | CmpRawIntOp _ -> AtomRawInt (Rawint.of_int Rawint.Int32 true (Rawint.compare i j))
              | RawSetBitFieldOp (_, _, off, len) -> AtomRawInt (Rawint.set_field i off len j)
              | _ -> raise EvalError)
       | AtomFloat x, AtomFloat y ->
            (match op with
                PlusFloatOp  _ -> AtomFloat (Rawfloat.add x y)
              | MinusFloatOp _ -> AtomFloat (Rawfloat.sub x y)
              | MulFloatOp   _ -> AtomFloat (Rawfloat.mul x y)
              | DivFloatOp   _ -> AtomFloat (Rawfloat.div x y)
              | MinFloatOp   _ -> AtomFloat (Rawfloat.min x y)
              | MaxFloatOp   _ -> AtomFloat (Rawfloat.max x y)
              | ATan2FloatOp _ -> AtomFloat (Rawfloat.atan2 x y)
              | EqFloatOp  _ -> test (Rawfloat.compare x y = 0)
              | NeqFloatOp _ -> test (Rawfloat.compare x y <> 0)
              | LtFloatOp  _ -> test (Rawfloat.compare x y < 0)
              | LeFloatOp  _ -> test (Rawfloat.compare x y <= 0)
              | GtFloatOp  _ -> test (Rawfloat.compare x y > 0)
              | GeFloatOp  _ -> test (Rawfloat.compare x y >= 0)
              | CmpFloatOp _ -> AtomInt (Rawfloat.compare x y)
              | _ -> raise EvalError)
       | AtomEnum (_, i), AtomEnum (_, j) ->
            (match op with
                EqEqOp _ -> test (i == j)
              | NeqEqOp _ -> test (i != j)
              | AndEnumOp n -> AtomEnum (n, i land j)
              | OrEnumOp n -> AtomEnum (n, i lor j)
              | _ -> raise EvalError)
       | AtomConst (_, _, i), AtomConst (_, _, j) ->
            (match op with
                EqEqOp _ -> test (i == j)
              | NeqEqOp _ -> test (i != j)
              | _ -> raise EvalError)
       | AtomLabel (l, i), AtomRawInt j ->
            let off =
               match op with
                PlusRawIntOp _  -> Rawint.add i j
              | MinusRawIntOp _ -> Rawint.sub i j
              | _ -> raise EvalError
            in
               AtomLabel (l, off)
       | AtomRawInt i, AtomLabel (l, j) ->
            let off =
               match op with
                PlusRawIntOp _  -> Rawint.add i j
              | _ -> raise EvalError
            in
               AtomLabel (l, off)
       | AtomLabel (l1, i), AtomLabel (l2, j)
         when l1 = l2 ->
            let off =
               match op with
                  MinusRawIntOp _ -> Rawint.sub i j
                | _ -> raise EvalError
            in
               AtomRawInt off
       | AtomSizeof (l, i), AtomRawInt j ->
            let off =
               match op with
                PlusRawIntOp _  -> Rawint.add i j
              | MinusRawIntOp _ -> Rawint.sub i j
              | _ -> raise EvalError
            in
               AtomSizeof (l, off)
       | AtomRawInt i, AtomSizeof (l, j) ->
            let off =
               match op with
                PlusRawIntOp _  -> Rawint.add i j
              | _ -> raise EvalError
            in
               AtomSizeof (l, off)
       | AtomSizeof (l1, i), AtomSizeof (l2, j) ->
            (match op with
                PlusRawIntOp _ ->
                   let l = List.sort Symbol.compare (l1 @ l2) in
                      AtomSizeof (l, Rawint.add i j)
              | _ ->
                   raise EvalError)
       | _ ->
            raise EvalError
   in
      ETConst c

let compute_binop op c1 c2 =
   try compute_binop_unprotected op c1 c2 with
      EvalError ->
         ETBinop (op, ETConst c1, ETConst c2)
    | Division_by_zero ->
         et_1div0

(************************************************************************
 * OPERATOR SIMPLIFICATION
 ************************************************************************)

(*
 * Symmetric operators.
 *)
let is_symmetric op =
   match op with
      PlusIntOp
    | MulIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MinIntOp
    | MaxIntOp

    | PlusRawIntOp _
    | MulRawIntOp _
    | AndRawIntOp _
    | OrRawIntOp _
    | XorRawIntOp _
    | MinRawIntOp _
    | MaxRawIntOp _

    | AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _ ->
         true

    | _ ->
         false

(*
 * Simplify an etree as much as possible.
 *)
let rec simple pos t =
   let pos = string_pos "simple" pos in
      match t with
         ETConst _
       | ETVar _ ->
            t
       | ETUnop (op, t) ->
            simple_unop pos op t
       | ETBinop (op, t1, t2) ->
            simple_binop pos op t1 t2

(*
 * For unops, rewrite binary negation to its binary form.
 * That way we can collapse with more binary operations.
 *)
and simple_unop pos op t =
   let pos = string_pos "simple_unop" pos in
   let t = simple pos t in
      match t with
         ETConst a ->
            compute_unop op a
       | t ->
            match op with
               RawIntOfRawIntOp (p1, s1, p2, s2)
               when p1 = p2 && s1 = s2 ->
                  t
             | UMinusIntOp ->
                  ETBinop (MinusIntOp, et_int_zero, t)
             | UMinusRawIntOp (p, s) ->
                  ETBinop (MinusRawIntOp (p, s), et_rawint_zero p s, t)
             | _ ->
                  ETUnop (op, t)

(*
 * For binary operators,
 * swap the operands to canonical form.
 *)
and simple_binop pos op t1 t2 =
   let pos = string_pos "simple_binop" pos in
   let t1 = simple pos t1 in
   let t2 = simple pos t2 in
      match t1, t2 with
         ETConst c1, ETConst c2 ->
            (* fully constant operations *)
            compute_binop op c1 c2

       | ETConst c, _ ->
            (* left identity and annihilation *)
            (match op, c with
                (* These have no effect *)
                PlusIntOp,  AtomInt 0
              | OrIntOp,    AtomInt 0
              | XorIntOp,   AtomInt 0
              | OrEnumOp _, AtomEnum (_, 0) ->
                   t2

              | PlusRawIntOp _,  AtomRawInt i
              | OrRawIntOp _,    AtomRawInt i
              | XorRawIntOp _,   AtomRawInt i
              | PlusPointerOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   t2

                (* These all compute to zero *)
              | MulIntOp, AtomInt 0
              | AndIntOp, AtomInt 0
              | LslIntOp, AtomInt 0
              | LsrIntOp, AtomInt 0
              | AsrIntOp, AtomInt 0 ->
                   t1
              | AndEnumOp _, AtomEnum (_, 0) ->
                   t1
              | MulRawIntOp _, AtomRawInt i
              | AndRawIntOp _, AtomRawInt i
              | SlRawIntOp _, AtomRawInt i
              | SrRawIntOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   t1

                (* These are more identities *)
              | MulIntOp, AtomInt 1 ->
                   t2
              | MulRawIntOp _, AtomRawInt i
                when Rawint.is_one i ->
                   t2

                (* For comparisons, we put the constants on the right *)
              | EqIntOp, _
              | NeqIntOp, _
              | EqRawIntOp _, _
              | NeqRawIntOp _, _
              | EqFloatOp _, _
              | NeqFloatOp _, _ ->
                   simple_binop pos op t2 t1

              | LtIntOp, _ -> simple_binop pos GtIntOp t2 t1
              | LeIntOp, _ -> simple_binop pos GeIntOp t2 t1
              | GtIntOp, _ -> simple_binop pos LtIntOp t2 t1
              | GeIntOp, _ -> simple_binop pos LeIntOp t2 t1
              | LtRawIntOp (p, s), _ -> simple_binop pos (GtRawIntOp (p, s)) t2 t1
              | LeRawIntOp (p, s), _ -> simple_binop pos (GeRawIntOp (p, s)) t2 t1
              | GtRawIntOp (p, s), _ -> simple_binop pos (LtRawIntOp (p, s)) t2 t1
              | GeRawIntOp (p, s), _ -> simple_binop pos (LeRawIntOp (p, s)) t2 t1
              | LtFloatOp p, _ -> simple_binop pos (GtFloatOp p) t2 t1
              | LeFloatOp p, _ -> simple_binop pos (GeFloatOp p) t2 t1
              | GtFloatOp p, _ -> simple_binop pos (LtFloatOp p) t2 t1
              | GeFloatOp p, _ -> simple_binop pos (LeFloatOp p) t2 t1

                (* Finally, if nothing else works, leave as-is *)
              | _ ->
                   ETBinop (op, t1, t2))

         (* commutativity and right identity and annihilation *)
       | _, ETConst c ->
            (match op, c with
                (* For symmetric operators, swap operands *)
                PlusIntOp, _
              | MulIntOp, _
              | AndIntOp, _
              | OrIntOp, _
              | XorIntOp, _
              | MaxIntOp, _
              | MinIntOp, _
              | PlusRawIntOp _, _
              | MulRawIntOp _, _
              | AndRawIntOp _, _
              | OrRawIntOp _, _
              | XorRawIntOp _, _
              | MaxRawIntOp _, _
              | MinRawIntOp _, _
              | AndEnumOp _, _
              | OrEnumOp _, _ ->
                   simple_binop pos op t2 t1

              | PlusPointerOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   t1

                (* For subtraction, convert to addition *)
              | MinusIntOp, AtomInt i ->
                   simple_binop pos PlusIntOp (ETConst (AtomInt (~-i))) t1
              | MinusRawIntOp (pre, signed), AtomRawInt i ->
                   simple_binop pos (PlusRawIntOp (pre, signed)) (ETConst (AtomRawInt (Rawint.neg i))) t1

                (* Convert shifts to products and division *)
              | LslIntOp, AtomInt i ->
                   simple_binop pos MulIntOp (ETConst (AtomInt (1 lsl i))) t1
           (* | AsrIntOp, AtomInt i ->
                   simple_binop DivIntOp (ETConst (AtomInt (1 lsl i))) t1 *)
              | SlRawIntOp (p, s), AtomRawInt i ->
                   simple_binop pos (MulRawIntOp (p, s)) (ETConst (AtomRawInt (Rawint.shift_left (Rawint.of_int p s 1) i))) t1

           (* | SrRawIntOp  ----   FIXME *)

              | LsrIntOp, AtomInt 0 ->
                   t1
              | SrRawIntOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   t1

              | DivIntOp, AtomInt 1 ->
                   t1
              | DivRawIntOp _, AtomRawInt i when Rawint.is_one i ->
                   t1

              | RemIntOp, AtomInt 1 ->
                   et_int_zero
              | RemRawIntOp (p, s), AtomRawInt
                i when Rawint.is_one i ->
                   et_rawint_zero p s

              | _ ->
                   ETBinop (op, t1, t2))

       | _ ->
            (* Annihilation *)
            if t1 = t2 then
               match op with
                  MinusIntOp ->
                     et_int_zero
                | MinusRawIntOp (p, s) ->
                     et_rawint_zero p s
                | MinIntOp
                | MinRawIntOp _
                | MinFloatOp _
                | MaxIntOp
                | MaxRawIntOp _
                | MaxFloatOp _ ->
                     t1
                | _ ->
                     ETBinop (op, t1, t2)
            else
               ETBinop (op, t1, t2)

(************************************************************************
 * OPTIMIZATION
 ************************************************************************)

(*
 * Try to convert comparison operators to built-in operators.
 *)
let rec comparison t =
   match t with
      ETConst _
    | ETVar _ ->
         t
    | ETUnop (op, t) ->
         ETUnop (op, comparison t)
    | ETBinop (op, (ETBinop (op2, t1, t2) as t1'), (ETConst (AtomInt 0) as t2')) ->
         (match op2, op with
             CmpIntOp, EqIntOp
           | CmpIntOp, NeqIntOp
           | CmpIntOp, LtIntOp
           | CmpIntOp, LeIntOp
           | CmpIntOp, GtIntOp ->
                ETBinop (op, t1, t2)
           | CmpRawIntOp (p, s), EqIntOp ->
                ETBinop (EqRawIntOp (p, s),  comparison t1, comparison t2)
           | CmpRawIntOp (p, s), NeqIntOp ->
                ETBinop (NeqRawIntOp (p, s), comparison t1, comparison t2)
           | CmpRawIntOp (p, s), LtIntOp ->
                ETBinop (LtRawIntOp (p, s),  comparison t1, comparison t2)
           | CmpRawIntOp (p, s), LeIntOp ->
                ETBinop (LeRawIntOp (p, s),  comparison t1, comparison t2)
           | CmpRawIntOp (p, s), GtIntOp ->
                ETBinop (GtRawIntOp (p, s),  comparison t1, comparison t2)
           | CmpRawIntOp (p, s), GeIntOp ->
                ETBinop (GeRawIntOp (p, s),  comparison t1, comparison t2)
           | CmpFloatOp p, EqIntOp ->
                ETBinop (EqFloatOp p,  comparison t1, comparison t2)
           | CmpFloatOp p, NeqIntOp ->
                ETBinop (NeqFloatOp p, comparison t1, comparison t2)
           | CmpFloatOp p, LtIntOp ->
                ETBinop (LtFloatOp p,  comparison t1, comparison t2)
           | CmpFloatOp p, LeIntOp ->
                ETBinop (LeFloatOp p,  comparison t1, comparison t2)
           | CmpFloatOp p, GtIntOp ->
                ETBinop (GtFloatOp p,  comparison t1, comparison t2)
           | CmpFloatOp p, GeIntOp ->
                ETBinop (GeFloatOp p,  comparison t1, comparison t2)
           | _ ->
                ETBinop (op, comparison t1', comparison t2'))

    | ETBinop (NeqRawIntOp _,
               ETUnop (RawIntOfEnumOp (_, _, 2), t1),
               ETConst (AtomRawInt i))
      when Rawint.is_zero i ->
         comparison t1

    | ETBinop (op, t1, t2) ->
         ETBinop (op, comparison t1, comparison t2)

(************************************************************************
 * REASSOCIATION
 ************************************************************************)

(*
 * Use algebraic transformations to reassociate the
 * code.
 *)
let is_associative op =
   match op with
      PlusClass
    | MinusClass
    | MulClass
    | AndClass
    | OrClass
    | XorClass ->
         true
    | DivClass
    | RemClass
    | OtherClass ->
         false

let is_associative2 op1 op2 =
   let c1 = op_class_squash (op_class op1) in
   let c2 = op_class_squash (op_class op2) in
      c1 = c2 && is_associative c1

let is_associative3 op1 op2 op3 =
   let c1 = op_class_squash (op_class op1) in
   let c2 = op_class_squash (op_class op2) in
   let c3 = op_class_squash (op_class op3) in
      c1 = c2 && c1 = c3 && is_associative c1

(*
 * Rewrite an expression of the form  (t1 op1 (t2 op2 t3))
 *)
let associate2 op1 op2 e1 e2 e3 =
   let c1 = op_class op1 in
   let c2 = op_class op2 in
      match c1, c2 with
         MinusClass, MinusClass ->
            ETBinop (negate_op op1, ETBinop (op1, e1, e2), e3)
       | _ ->
            ETBinop (op2, ETBinop (op1, e1, e2), e3)

(*
 * In this function, we want to rewrite an expression ((a op2 b) op1 (c op3 d))
 * to the expression ((a op2' c) op1' (b op3' d)).  This function decides the
 * operators.
 *)
let associate3 op1 op2 op3 e1 e2 e3 e4 =
   let c1 = op_class op1 in
   let c2 = op_class op2 in
   let c3 = op_class op3 in
      match c1, c2, c3 with
         PlusClass, MinusClass, PlusClass ->
            ETBinop (op1, ETBinop (op3, e1, e3), ETBinop (op2, e4, e2))
       | PlusClass, MinusClass, MinusClass ->
            ETBinop (op2, ETBinop (op1, e1, e3), ETBinop (op1, e2, e4))
       | MinusClass, PlusClass, PlusClass ->
            ETBinop (op2, ETBinop (op1, e1, e3), ETBinop (op3, e2, e4))
       | MinusClass, PlusClass, MinusClass ->
            ETBinop (op2, ETBinop (op1, e1, e3), ETBinop (op2, e2, e4))
       | MinusClass, MinusClass, MinusClass ->
            ETBinop (op1, ETBinop (op2, e1, e3), ETBinop (negate_op op3, e2, e4))
       | _ ->
            ETBinop (op1, ETBinop (op2, e1, e3), ETBinop (op3, e2, e4))

(*
 * Apply associativiy rules.
 *)
let rec associate pos t =
   let pos = string_pos "associate" pos in
      match t with
         ETConst _
       | ETVar _ ->
            t
       | ETUnop (op, t') ->
            let t'' = associate pos t' in
               if t'' != t' then
                  ETUnop (op, t'')
               else
                  t

       | ETBinop (op1, ETBinop (op2, ETConst c1, t2), ETConst c2)
         when is_associative2 op1 op2 ->
            ETBinop (op2, compute_binop op1 c1 c2, associate pos t2)

       | ETBinop (op1, t1, ETBinop (op2, t2, t3))
         when is_associative2 op1 op2 ->
            associate2 op1 op2 t1 t2 t3

(*
       | ETBinop (op1, ETBinop (op2, (ETConst _ as e1), e2), ETBinop (op3, (ETConst _ as e3), e4))
         when is_associative3 op1 op2 op3 ->
            associate3 op1 op2 op3 e1 e2 e3 e4
*)

       | ETBinop (op, t1, t2) ->
            let t1' = associate pos t1 in
            let t2' = associate pos t2 in
               if t1 != t1' || t2 != t2' then
                  ETBinop (op, t1', t2')
               else
                  t

(************************************** distributivity *)

let is_ring op1 op2 =
   match op1, op2 with
      MulIntOp, PlusIntOp
    | MulIntOp, MinusIntOp
    | MulRawIntOp _, PlusRawIntOp _
    | MulRawIntOp _, MinusRawIntOp _
    | AndIntOp, OrIntOp
    | AndIntOp, XorIntOp
    | AndEnumOp _, OrEnumOp _
    | AndRawIntOp _, OrRawIntOp _
    | AndRawIntOp _, XorRawIntOp _
    | MaxIntOp, MinIntOp
    | MaxRawIntOp _, MinRawIntOp _ ->
         true
    | _ ->
         false

let rec distribute pos t =
   let pos = string_pos "distribute" pos in
      match t with
         ETConst _
       | ETVar _ ->
            t
       | ETUnop (op, t') ->
            let t'' = distribute pos t' in
               if t'' != t' then
                  ETUnop (op, t'')
               else
                  t
       | ETBinop (op1, ETBinop (op2, t1, t2), t3)
         when is_ring op1 op2 ->
            let t3 = distribute pos t3 in
               ETBinop (op2, ETBinop (op1, distribute pos t1, t3), ETBinop (op1, distribute pos t2, t3))
       | ETBinop (op1, ETConst c1, ETBinop (op2, ETConst c2, t))
         when is_ring op1 op2 ->
            ETBinop (op2, compute_binop op1 c1 c2, distribute pos t)
       | ETBinop (op1, t1, ETBinop (op2, t2, t3))
         when is_ring op1 op2 ->
            let t1 = distribute pos t1 in
               ETBinop (op2, ETBinop (op1, t1, distribute pos t2), ETBinop (op1, t1, distribute pos t3))
       | ETBinop (op, t1, t2) ->
            let t1' = distribute pos t1 in
            let t2' = distribute pos t2 in
               if t1 != t1' || t2 != t2' then
                  ETBinop (op, t1', t2')
               else
                  t

(************************************************************************
 * OPTIMIZATION
 ************************************************************************)

(*
 * Inverse-distributivity.
 *)
let rec factor t =
   match t with
      ETConst _
    | ETVar _ ->
         t
    | ETUnop (op, t) ->
         ETUnop (op, factor t)
    | ETBinop (op, t1, t2) ->
         let t1 = factor t1 in
         let t2 = factor t2 in
            match t1, t2 with
               ETBinop (op1, t1, t2), ETBinop (op2, t3, t4)
               when op1 = op2 && is_ring op1 op ->
                  if t1 = t3 then
                     ETBinop (op1, ETBinop (op, t2, t4), t1)
                  else if t1 = t4 then
                     ETBinop (op1, ETBinop (op, t2, t3), t1)
                  else if t2 = t3 then
                     ETBinop (op1, ETBinop (op, t1, t4), t2)
                  else if t2 = t4 then
                     ETBinop (op1, ETBinop (op, t1, t3), t2)
                  else
                     ETBinop (op, ETBinop (op1, t1, t2), ETBinop (op2, t3, t4))
             | _ ->
                  ETBinop (op, t1, t2)

(************************************** strength reduction *)

(*
 * Strength reduction is not implemented yet.
 *)
let rec strength t =
   t

(************************************************************************
 * TOPLEVEL
 ************************************************************************)

(*
 * Produce the expression in canonical form.
 *)
let rec canonicalize_etree_core pos t =
   let pos = string_pos "canonicalize_etree" pos in
   let t = simple pos t in
   let t = comparison t in
   let t' = associate pos t in
   let t' = distribute pos t' in
      if t != t' then
         canonicalize_etree_core pos t'
      else
         t'

(*
 * Invert distributivity to get faster computation.
 *)
let optimize_etree_core pos t =
   let pos = string_pos "optimize_etree" pos in
      strength (simple pos (factor t))

(*
 * Check if an etree is the sum of a variable and some
 * other stuff.  If it is, then return the other stuff,
 * otherwise return None.
 *)
let rec etree_is_sum_of_var_core e v1 =
   match e with
      ETBinop (PlusIntOp as op, ETVar v2, e2)
    | ETBinop (PlusRawIntOp _ as op, ETVar v2, e2)
    | ETBinop (PlusPointerOp _ as op, ETVar v2, e2)

    | ETBinop (PlusIntOp as op, e2, ETVar v2)
    | ETBinop (PlusRawIntOp _ as op, e2, ETVar v2)
    | ETBinop (PlusPointerOp _ as op, e2, ETVar v2) ->
         if Symbol.eq v1 v2 then
            Some (op, e2)
         else
            (match etree_is_sum_of_var_core e2 v1 with
                Some (op, e3) ->
                   Some (op, ETBinop (op, e3, e2))
              | None ->
                   None)
    | _ ->
         None

(*
 * Check that every variable in the etree belongs to a
 * specified set.
 *)
let rec etree_forall_vars_core f e =
   match e with
      ETConst _ ->
         true
    | ETVar v ->
         f v
    | ETUnop (_, e) ->
         etree_forall_vars_core f e
    | ETBinop (_, e1, e2) ->
         etree_forall_vars_core f e1 && etree_forall_vars_core f e2

(************************************************************************
 * TYPING
 ************************************************************************)

(*
 * Get the types for the operators.
 *)
let rec type_of_unop genv pos op e1 =
   let pos = string_pos "type_of_unop" pos in
      match op with
         NotEnumOp n ->
            TyEnum n

         (* naml ints *)
       | UMinusIntOp
       | AbsIntOp
       | NotIntOp ->
            TyInt

         (* Native ints *)
       | RawBitFieldOp  (pre, signed, _, _)
       | UMinusRawIntOp (pre, signed)
       | NotRawIntOp    (pre, signed) ->
            TyRawInt (pre, signed)

         (* Floats *)
       | UMinusFloatOp  pre
       | AbsFloatOp     pre
       | SinFloatOp	pre
       | CosFloatOp     pre
       | TanFloatOp     pre
       | ASinFloatOp	pre
       | ACosFloatOp    pre
       | ATanFloatOp    pre
       | SinHFloatOp	pre
       | CosHFloatOp    pre
       | TanHFloatOp    pre
       | ExpFloatOp	pre
       | LogFloatOp	pre
       | Log10FloatOp	pre
       | SqrtFloatOp	pre
       | CeilFloatOp    pre
       | FloorFloatOp   pre ->
            TyFloat pre

         (* Coerce to int *)
       | IntOfFloatOp  _
       | IntOfRawIntOp _
       | LengthOfBlockOp ({ sub_script = IntIndex }, _) ->
            TyInt

         (* Coerce to float *)
       | FloatOfIntOp      pre
       | FloatOfFloatOp    (pre, _)
       | FloatOfRawIntOp   (pre, _, _) ->
            TyFloat pre

         (* Coerce to rawint *)
       | RawIntOfIntOp     (pre, signed)
       | RawIntOfEnumOp    (pre, signed, _)
       | RawIntOfFloatOp   (pre, signed, _)
       | LengthOfBlockOp ({ sub_script = RawIntIndex (pre, signed) }, _) ->
            TyRawInt (pre, signed)

         (* Type coercions *)
       | DTupleOfDTupleOp (ty_var, _) ->
            TyDTuple (ty_var, None)
       | UnionOfUnionOp (ty_var, tyl, s1, _) ->
            TyUnion (ty_var, tyl, s1)
       | RawDataOfFrameOp _ ->
            TyRawData

         (*
          * Coerce a rawint of one type to another:
          * First pair is destination type, second pair is source.
          *)
       | RawIntOfRawIntOp  (pre, signed, _, _) ->
            TyRawInt (pre, signed)

         (* Integer/pointer coercions (only for C) *)
       | RawIntOfPointerOp (pre, signed) ->
            TyRawInt (pre, signed)
       | PointerOfRawIntOp _ ->
            TyRawData
       | PointerOfBlockOp op ->
            TyPointer op

and type_of_binop genv pos op e1 e2 =
   let pos = string_pos "type_of_binop" pos in
      match op with
         (* enums *)
         AndEnumOp n
       | OrEnumOp n
       | XorEnumOp n ->
            TyEnum n

         (* naml ints *)
       | PlusIntOp
       | MinusIntOp
       | MulIntOp
       | DivIntOp
       | RemIntOp
       | LslIntOp
       | LsrIntOp
       | AsrIntOp
       | AndIntOp
       | OrIntOp
       | XorIntOp
       | MaxIntOp
       | MinIntOp ->
            TyInt

       | EqIntOp
       | NeqIntOp
       | LtIntOp
       | LeIntOp
       | GtIntOp
       | GeIntOp
       | CmpIntOp ->
            TyEnum 2

         (* Rawints *)
       | PlusRawIntOp   (pre, signed)
       | MinusRawIntOp  (pre, signed)
       | MulRawIntOp    (pre, signed)
       | DivRawIntOp    (pre, signed)
       | RemRawIntOp    (pre, signed)
       | SlRawIntOp     (pre, signed)
       | SrRawIntOp     (pre, signed)
       | AndRawIntOp    (pre, signed)
       | OrRawIntOp     (pre, signed)
       | XorRawIntOp    (pre, signed)
       | MaxRawIntOp    (pre, signed)
       | MinRawIntOp    (pre, signed)
       | RawSetBitFieldOp (pre, signed, _, _) ->
            TyRawInt (pre, signed)

       | EqRawIntOp     _
       | NeqRawIntOp    _
       | LtRawIntOp     _
       | LeRawIntOp     _
       | GtRawIntOp     _
       | GeRawIntOp     _
       | CmpRawIntOp    _ ->
            TyEnum 2


         (* Floats *)
       | PlusFloatOp  pre
       | MinusFloatOp pre
       | MulFloatOp   pre
       | DivFloatOp   pre
       | RemFloatOp   pre
       | MaxFloatOp   pre
       | MinFloatOp   pre
       | ATan2FloatOp pre
       | PowerFloatOp pre
       | LdExpFloatIntOp pre ->
            TyFloat pre

       | EqFloatOp    _
       | NeqFloatOp   _
       | LtFloatOp    _
       | LeFloatOp    _
       | GtFloatOp    _
       | GeFloatOp    _
       | CmpFloatOp   _ ->
            TyEnum 2

         (* Pointer operations *)
       | EqEqOp _
       | NeqEqOp _ ->
            TyEnum 2

       | PlusPointerOp (op, _, _) ->
            TyPointer op

(*
 * Get the type of an etree.
 *)
and type_of_etree_core genv pos e =
   let pos = string_pos "type_of_etree" pos in
      match e with
         ETConst a ->
            type_of_atom genv pos a
       | ETVar v ->
            genv_lookup_var genv pos v
       | ETUnop (op, e) ->
            type_of_unop genv pos op e
       | ETBinop (op, e1, e2) ->
            type_of_binop genv pos op e1 e2

(************************************************************************)

(*
 * Global versions.
 *)
let canonicalize_etree pos e =
   let loc = loc_of_etree e in
   let e = canonicalize_etree_core pos (dest_etree_core e) in
      make_etree loc e

let optimize_etree pos e =
   let loc = loc_of_etree e in
   let e = optimize_etree_core pos (dest_etree_core e) in
      make_etree loc e

let etree_is_sum_of_var t v =
   let loc = loc_of_etree t in
      match etree_is_sum_of_var_core (dest_etree_core t) v with
         Some (op, e) ->
            Some (op, make_etree loc e)
       | None ->
            None

let etree_forall_vars f t =
   etree_forall_vars_core f (dest_etree_core t)

let type_of_etree genv pos t =
   type_of_etree_core genv pos (dest_etree_core t)

(*
 *)