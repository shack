(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol
open Location

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_depth
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

open Fir_alias_pre_norm

module Pos = MakePos (struct let name = "Fir_alias_pre_cse" end)
open Pos

type result = gvenv * aclass * etree_core

let const_loc = bogus_loc "<Fir_alias_pre_cse>"

(*
 * Tree hashing.
 *)
let cse_hash_htree gvenv depth h : result =
   try
      let ac, e = gvenv_lookup_htree gvenv h in
         gvenv, ac, dest_etree_core e
   with
      Not_found ->
         let v = new_symbol_string "htree" in
         let loc = loc_of_htree h in
         let e' = ETVar v in
         let e = make_etree loc e' in
         let ac = make_aclass loc (AliasExp (depth, HTETree e')) in
         let gvenv = gvenv_add_htree gvenv h ac e in
         let gvenv = gvenv_add_value gvenv v (make_aclass loc (AliasExp (depth, dest_htree_core h))) (Some h) in
         let gvenv = gvenv_add_var gvenv v ac e in
            gvenv, ac, e'

(*
 * In this nearly final step, we have an alias environment that
 * classifies all the variables in the program, and all the trees
 * are fully expanded and optimized.  Perform a CSE, and fold-together
 * common definitions.  In addition, introduce hierarchy to
 * establish the invariant that every subexpression of an etree is
 * either a constant or a var.
 *)
type cse =
   CseValue of aclass * etree_core
 | CseAlias of aclass

let rec cse_var gvenv aenv pos v : result =
   let pos = string_pos "cse_var" pos in
   let e =
      try
         let ac, e = gvenv_lookup_var gvenv v in
            CseValue (ac, dest_etree_core e)
      with
         Not_found ->
            try CseAlias (SymbolTable.find aenv v) with
               Not_found ->
                  raise (FirException (pos, UnboundVar v))
   in
      match e with
         CseValue (ac, e) ->
            gvenv, ac, e
       | CseAlias ac ->
            let loc = loc_of_aclass ac in
            let gvenv, ac, e = cse_alias_var gvenv aenv pos v ac in
            let gvenv = gvenv_add_value gvenv v ac (Some (make_htree loc (HTETree e))) in
            let gvenv = gvenv_add_var gvenv v ac (make_etree loc e) in
               gvenv, ac, e

(*
 * Convert an alias class.
 *)
and cse_alias_var gvenv aenv pos v ac : result =
   let pos = string_pos "cse_alias_var" pos in
   let loc = loc_of_aclass ac in
      match dest_aclass_core ac with
         AliasExp (_, HTETree (ETVar v'))
         when Symbol.eq v' v ->
            let gvenv = gvenv_add_value gvenv v ac None in
               gvenv, ac, ETVar v
       | AliasInduction (depth, v1, g, op, v2, mul, add, step, base) ->
            cse_induction gvenv aenv pos loc depth v1 g op v2 mul add step base
       | AliasExp (depth, h) ->
            cse_htree gvenv aenv pos loc depth h
       | AliasAlias _ ->
            raise (FirException (pos, StringError "illegal alias expression"))

(*
 * Be careful about induction vars, because the var is
 * used in the induction step itself.  We see if we can fold
 * the var together with previously defined induction vars.
 *)
and cse_induction gvenv aenv pos loc depth v1 g op v2 mul add step base : result =
   let pos = string_pos "cse_induction" pos in
   let gvenv, _, base = cse_etree gvenv aenv pos depth base in
   let gvenv, _, step = cse_etree gvenv aenv pos depth step in
   let key1 = [NameVar g; NameETree base; NameETree step] in
   let key2 = [NameVar g; NameETree step] in
      try
         (* If we find another induction var with the same def, use it *)
         let ac, e = gvenv_lookup_induction_key gvenv key1 in
            gvenv, ac, dest_etree_core e
      with
         Not_found ->
            try
               (* If we find another induction var with the same step, derive this one *)
               let ac, e = gvenv_lookup_induction_key gvenv key2 in
               let loc = loc_of_aclass ac in
                  match dest_aclass_core ac with
                     AliasInduction (depth, v2, g, op, v3, mul3, add3, _, base2) ->
                        let e2 = negate_etree op base in
                        let e2 = add_etree op base2 base in
                        let gvenv, _, add3 = cse_normalize_etree gvenv aenv pos depth (add_etree op add3 e2) in
                        let ac = make_aclass loc (AliasInduction (depth, v1, g, op, v3, mul3, add3, step, base)) in
                        let e' = make_etree loc (ETVar v1) in
                        let e = dest_etree_core e in
                        let gvenv, _, e = cse_normalize_etree gvenv aenv pos depth (add_etree op e e2) in
                        let gvenv = gvenv_add_etree gvenv e ac e' in
                        let gvenv = gvenv_add_value gvenv v1 ac (Some (make_htree loc (HTETree e))) in
                        let gvenv = gvenv_add_var gvenv v1 ac e' in
                           gvenv, ac, e
                   | _ ->
                        raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac))))
            with
               Not_found ->
                  (* Establish this as the canonical induction var *)
                  let ac = make_aclass loc (AliasInduction (depth, v1, g, op, v2, mul, add, step, base)) in
                  let gvenv = gvenv_add_value gvenv v1 ac None in
                  let e = ETVar v1 in
                  let e' = make_etree loc (ETVar v1) in
                  let gvenv = gvenv_add_induction_key gvenv key1 ac e' in
                  let gvenv = gvenv_add_induction_key gvenv key2 ac e' in
                     gvenv, ac, e

(*
 * Heap trees.
 *)
and cse_htree gvenv aenv pos loc depth h : result =
   let pos = string_pos "cse_htree" pos in
      match h with
         HTETree e ->
            cse_etree gvenv aenv pos depth e
       | HTAlloc (heap, op) ->
            let gvenv, op = cse_alloc_op gvenv aenv pos depth op in
            let h = make_htree loc (HTAlloc (heap, op)) in
               cse_hash_htree gvenv depth h
       | HTSubscript (heap, op, ty, e1, e2) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
            let h = make_htree loc (HTSubscript (heap, op, ty, e1, e2)) in
               cse_hash_htree gvenv depth h
       | HTSetSubscript (heap, op, ty, e1, e2, e3) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
            let gvenv, _, e3 = cse_etree gvenv aenv pos depth e3 in
            let h = make_htree loc (HTSetSubscript (heap, op, ty, e1, e2, e3)) in
               cse_hash_htree gvenv depth h
       | HTMemcpy (heap, op, e1, e2, e3, e4, e5) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
            let gvenv, _, e3 = cse_etree gvenv aenv pos depth e3 in
            let gvenv, _, e4 = cse_etree gvenv aenv pos depth e4 in
            let gvenv, _, e5 = cse_etree gvenv aenv pos depth e5 in
            let h = make_htree loc (HTMemcpy (heap, op, e1, e2, e3, e4, e5)) in
               cse_hash_htree gvenv depth h
       | HTExt (heap, ty1, s, b, ty2, ty_args, args) ->
            let gvenv, args =
               List.fold_left (fun (gvenv, args) e ->
                     let gvenv, _, e = cse_etree gvenv aenv pos depth e in
                        gvenv, e :: args) (gvenv, []) args
            in
            let h = make_htree loc (HTExt (heap, ty1, s, b, ty2, ty_args, List.rev args)) in
               cse_hash_htree gvenv depth h
       | HTAssert (heap, pred) ->
            let gvenv, pred = cse_pred gvenv aenv pos depth pred in
            let h = make_htree loc (HTAssert (heap, pred)) in
               cse_hash_htree gvenv depth h
       | HTDepends _ ->
            cse_hash_htree gvenv depth (make_htree loc h)

(*
 * Allocation.
 *)
and cse_alloc_op gvenv aenv pos depth op =
   let pos = string_pos "cse_alloc_op" pos in
      match op with
         AllocTuple (tclass, ty, ty_vars, el) ->
            let gvenv, el = cse_etree_list gvenv aenv pos depth el in
               gvenv, AllocTuple (tclass, ty, ty_vars, el)
       | AllocDTuple (ty, ty_var, e, el) ->
            let gvenv, _, e = cse_etree gvenv aenv pos depth e in
            let gvenv, el = cse_etree_list gvenv aenv pos depth el in
               gvenv, AllocDTuple (ty, ty_var, e, el)
       | AllocUnion (ty, ty_vars, v, i, el) ->
            let gvenv, el = cse_etree_list gvenv aenv pos depth el in
               gvenv, AllocUnion (ty, ty_vars, v, i, el)
       | AllocArray (ty, el) ->
            let gvenv, el = cse_etree_list gvenv aenv pos depth el in
               gvenv, AllocArray (ty, el)
       | AllocVArray (ty, op, e1, e2) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
               gvenv, AllocVArray (ty, op, e1, e2)
       | AllocMalloc (ty, e) ->
            let gvenv, _, e = cse_etree gvenv aenv pos depth e in
               gvenv, AllocMalloc (ty, e)
       | AllocFrame _ ->
            gvenv, op

(*
 * Assertions.
 *)
and cse_pred gvenv aenv pos depth pred =
   let pos = string_pos "cse_pred" pos in
      match pred with
         IsMutable e ->
            let gvenv, _, e = cse_etree gvenv aenv pos depth e in
               gvenv, IsMutable e
       | Reserve (e1, e2) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
               gvenv, Reserve (e1, e2)
       | ElementCheck (ty, op, e1, e2) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
               gvenv, ElementCheck (ty, op, e1, e2)
       | BoundsCheck (op, e1, e2, e3) ->
            let gvenv, _, e1 = cse_etree gvenv aenv pos depth e1 in
            let gvenv, _, e2 = cse_etree gvenv aenv pos depth e2 in
            let gvenv, _, e3 = cse_etree gvenv aenv pos depth e3 in
               gvenv, BoundsCheck (op, e1, e2, e3)

(*
 * Expressions.
 *)
and cse_normalize_etree gvenv aenv pos depth e =
   let pos = string_pos "cse_normalize_etree" pos in
      cse_etree gvenv aenv pos depth (normalize_etree_core aenv pos e)

and cse_etree gvenv aenv pos depth e : result =
   let pos = string_pos "cse_etree" pos in
      match e with
         ETConst _ ->
            gvenv, make_aclass const_loc (AliasExp (zero_depth, HTETree e)), e
       | ETVar v ->
            cse_var gvenv aenv pos v
       | ETUnop (op, e1) ->
            cse_unop gvenv aenv pos depth op e1
       | ETBinop (op, e1, e2) ->
            cse_binop gvenv aenv pos depth op e1 e2

and cse_etree_list gvenv aenv pos depth el =
   let pos = string_pos "cse_etree_list" pos in
   let rec collect gvenv el' el =
      match el with
         e :: el ->
            let gvenv, _, e = cse_etree gvenv aenv pos depth e in
               collect gvenv (e :: el') el
       | [] ->
            gvenv, List.rev el'
   in
      collect gvenv [] el

(*
 * Unary expressions.
 *)
and cse_unop gvenv aenv pos depth op e1 : result =
   let pos = string_pos "cse_unop" pos in
   let gvenv, ac, e1 = cse_etree gvenv aenv pos depth e1 in
   let loc = loc_of_aclass ac in
   let e = ETUnop (op, e1) in
      try
         let ac, e = gvenv_lookup_etree gvenv e in
            gvenv, ac, dest_etree_core e
      with
         Not_found ->
            (* Look for induction variables *)
            let v = new_symbol_string "cse_unop" in
            let gvenv, ac =
               match op, dest_aclass_core ac with
                  UMinusIntOp, AliasInduction (depth, v2, g, op, v3, mul3, add3, step, base)
                | UMinusRawIntOp _, AliasInduction (depth, v2, g, op, v3, mul3, add3, step, base) ->
                     let gvenv, _, mul3 = cse_normalize_etree gvenv aenv pos depth (negate_etree op mul3) in
                     let gvenv, _, add3 = cse_normalize_etree gvenv aenv pos depth (negate_etree op add3) in
                     let gvenv, _, step = cse_normalize_etree gvenv aenv pos depth (negate_etree op step) in
                     let gvenv, _, base = cse_normalize_etree gvenv aenv pos depth (negate_etree op base) in
                        gvenv, AliasInduction (depth, v, g, op, v3, mul3, add3, step, base)

                | _ ->
                     gvenv, AliasExp (depth, HTETree e)
            in

            (* Add the new alias class *)
            let e' = ETVar v in
            let e'' = make_etree loc e' in
            let ac = make_aclass loc ac in
            let gvenv = gvenv_add_etree gvenv e ac e'' in
            let gvenv = gvenv_add_value gvenv v ac (Some (make_htree loc (HTETree e))) in
            let gvenv = gvenv_add_var gvenv v ac e'' in
               gvenv, ac, e'

(*
 * Binary expressions.
 *)
and cse_binop gvenv aenv pos depth op e1 e2 : result =
   let pos = string_pos "cse_binop" pos in
   let gvenv, ac1, e1 = cse_etree gvenv aenv pos depth e1 in
   let gvenv, ac2, e2 = cse_etree gvenv aenv pos depth e2 in
   let depth1 = depth_of_aclass ac1 in
   let depth2 = depth_of_aclass ac2 in
   let loc = loc_of_aclass ac1 in

   (* Swap the arguments to make the pattern match easier *)
   let depth1, ac1, e1, depth2, ac2, e2 =
      if le_depth depth1 depth2 then
         match op, dest_aclass_core ac2 with
            PlusIntOp,       AliasInduction _
          | PlusRawIntOp _,  AliasInduction _
          | PlusPointerOp _, AliasInduction _
          | MulIntOp,        AliasInduction _
          | MulRawIntOp _,   AliasInduction _ ->
               depth2, ac2, e2, depth1, ac1, e1
          | _ ->
               depth1, ac1, e1, depth2, ac2, e2
      else
         depth1, ac1, e1, depth2, ac2, e2
   in

   (* Look for a previous match *)
   let e = ETBinop (op, e1, e2) in
      try
         let ac, e = gvenv_lookup_etree gvenv e in
            gvenv, ac, dest_etree_core e
      with
         Not_found ->
            (* Try to find induction variables *)
            let v = new_symbol_string "cse_binop" in
            let gvenv, ac =
               if lt_depth depth2 depth1 then
                  cse_binop_ind_exp gvenv aenv pos v op depth1 e ac1 e2
               else if eq_depth depth2 depth1 then
                  cse_binop_ind gvenv aenv pos v op depth1 e ac1 e2 ac2
               else
                  gvenv, AliasExp (max_depth depth1 depth2, HTETree e)
            in

            (* Add the new alias class *)
            let e' = ETVar v in
            let e'' = make_etree loc e' in
            let ac = make_aclass loc ac in
            let gvenv = gvenv_add_etree gvenv e ac e'' in
            let gvenv = gvenv_add_value gvenv v ac (Some (make_htree loc (HTETree e))) in
            let gvenv = gvenv_add_var gvenv v ac e'' in
               gvenv, ac, e'

(*
 * Look for the induction cases.
 *)
and cse_binop_ind gvenv aenv pos v1 op1 depth e ac1 e2 ac2 =
   let pos = string_pos "cse_binop_ind" pos in
      match op1, dest_aclass_core ac1, dest_aclass_core ac2 with
         PlusIntOp,
         AliasInduction (_, _, g1, op2, v3, mul1, add1, step1, base1),
         AliasInduction (_, _, g2, _,  v4, mul2, add2, step2, base2)
       | PlusRawIntOp _,
         AliasInduction (_, _, g1, op2, v3, mul1, add1, step1, base1),
         AliasInduction (_, _, g2, _,  v4, mul2, add2, step2, base2)
         when Symbol.eq g1 g2 && Symbol.eq v3 v4 ->
            let gvenv, _, mul  = cse_normalize_etree gvenv aenv pos depth (add_etree op2 mul1 mul2) in
            let gvenv, _, add  = cse_normalize_etree gvenv aenv pos depth (add_etree op2 add1 add2) in
            let gvenv, _, step = cse_normalize_etree gvenv aenv pos depth (add_etree op2 step1 step2) in
            let gvenv, _, base = cse_normalize_etree gvenv aenv pos depth (add_etree op2 base1 base2) in
               gvenv, AliasInduction (depth, v1, g1, op2, v3, mul, add, step, base)

       | PlusPointerOp (op, pre, signed),
         AliasInduction (_, _, g1, _, v3, mul1, add1, step1, base1),
         AliasInduction (_, _, g2, _,  v4, mul2, add2, step2, base2)
         when Symbol.eq g1 g2 && Symbol.eq v3 v4 ->
            let op1 = PointerClass (op, pre, signed) in
            let op2 = RawIntClass (pre, signed) in
            let gvenv, _, mul  = cse_normalize_etree gvenv aenv pos depth (add_etree op2 mul1 mul2) in
            let gvenv, _, add  = cse_normalize_etree gvenv aenv pos depth (add_etree op2 add1 add2) in
            let gvenv, _, step = cse_normalize_etree gvenv aenv pos depth (add_etree op2 step1 step2) in
            let gvenv, _, base = cse_normalize_etree gvenv aenv pos depth (add_etree op1 base1 base2) in
               gvenv, AliasInduction (depth, v1, g1, op1, v3, mul, add, step, base)

       | _, _, AliasExp _ ->
            cse_binop_ind_exp gvenv aenv pos v1 op1 depth e ac1 e2

       | _ ->
            gvenv, AliasExp (depth, HTETree e)

and cse_binop_ind_exp gvenv aenv pos v1 op1 depth e ac1 e2 =
   let pos = string_pos "cse_binop_ind_exp" pos in
      match op1, dest_aclass_core ac1 with
         PlusIntOp,       AliasInduction (depth, v2, g, op2, v3, mul3, add3, step, base)
       | PlusRawIntOp _,  AliasInduction (depth, v2, g, op2, v3, mul3, add3, step, base) ->
            let gvenv, _, add3 = cse_normalize_etree gvenv aenv pos depth (add_etree op2 add3 e2) in
            let gvenv, _, base = cse_normalize_etree gvenv aenv pos depth (add_etree op2 base e2) in
               gvenv, AliasInduction (depth, v1, g, op2, v3, mul3, add3, step, base)

       | PlusPointerOp (op, pre, signed), AliasInduction (depth, v2, g, _, v3, mul3, add3, step, base) ->
            let op1 = PointerClass (op, pre, signed) in
            let op2 = RawIntClass (pre, signed) in
            let gvenv, _, add3 = cse_normalize_etree gvenv aenv pos depth (add_etree op2 e2 add3) in
            let gvenv, _, base = cse_normalize_etree gvenv aenv pos depth (add_etree op1 e2 base) in
               gvenv, AliasInduction (depth, v1, g, op1, v3, mul3, add3, step, base)

       | MulIntOp,       AliasInduction (depth, v2, g, op2, v3, mul3, add3, step, base)
       | MulRawIntOp _,  AliasInduction (depth, v2, g, op2, v3, mul3, add3, step, base) ->
            let gvenv, _, mul3 = cse_normalize_etree gvenv aenv pos depth (mul_etree op2 mul3 e2) in
            let gvenv, _, add3 = cse_normalize_etree gvenv aenv pos depth (mul_etree op2 add3 e2) in
            let gvenv, _, base = cse_normalize_etree gvenv aenv pos depth (mul_etree op2 base e2) in
            let gvenv, _, step = cse_normalize_etree gvenv aenv pos depth (mul_etree op2 step e2) in
               gvenv, AliasInduction (depth, v1, g, op2, v3, mul3, add3, step, base)

       | _ ->
            gvenv, AliasExp (depth, HTETree e)

(*
 * Build the program by constructing each variable in the program.
 *)
let cse_prog aenv cenv =
   let gvenv =
      SymbolTable.fold (fun gvenv v _ ->
            let pos = string_pos "cse_prog" (var_exp_pos v) in
            let gvenv, _, _ = cse_var gvenv aenv pos v in
               gvenv) gvenv_empty aenv
   in
   let gvenv, cenv =
      SymbolTable.fold_map (fun gvenv label (args, ps_args) ->
            let gvenv, args =
               List.fold_left (fun (gvenv, args) v ->
                     let pos = string_pos "cse_prog" (var_exp_pos v) in
                     let gvenv, ac, arg = cse_var gvenv aenv pos v in
                     let loc = loc_of_aclass ac in
                     let arg = make_etree loc arg in
                        gvenv, arg :: args) (gvenv, []) args
            in
            let gvenv, ps_args =
               SymbolTable.fold_map (fun gvenv v v' ->
                     let pos = string_pos "cse_prog" (var_exp_pos v) in
                     let gvenv, ac, arg = cse_var gvenv aenv pos v' in
                     let loc = loc_of_aclass ac in
                     let arg = make_etree loc arg in
                        gvenv, arg) gvenv ps_args
            in
               gvenv, (List.rev args, ps_args)) gvenv cenv
   in
      if debug debug_alias then
         begin
            Format.printf "@[<v 3>*** FIR: alias after CSE:@ @[<v 3>Aliases:%a" pp_print_alias_htree_table gvenv.gvenv_values;
            Format.printf "@]@ @[<v 3>Values:%a" pp_print_alias_etree_table gvenv.gvenv_var_table;
            Format.printf "@]@ @[<v 3>Calls:%a" pp_print_call_table cenv;
            Format.printf "@]@]@."
         end;
      gvenv, cenv


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
