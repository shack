(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Fir
open Fir_heap
open Fir_depth
open Fir_alias
open Fir_algebra
open Fir_logic.FirLogic

(*
 * A name is a list of names.
 *)
type name =
   NameInt of int
 | NameVar of var
 | NameETree of etree_core

module NameCompare =
struct
   type t = name list

   let compare_name n1 n2 =
      match n1, n2 with
         NameInt i1, NameInt i2 ->
            i1 - i2
       | NameVar v1, NameVar v2 ->
            Symbol.compare v1 v2
       | NameETree e1, NameETree e2 ->
            Pervasives.compare e1 e2
       | NameInt _, NameVar _
       | NameInt _, NameETree _
       | NameVar _, NameETree _ ->
            -1
       | NameVar _, NameInt _
       | NameETree _, NameInt _
       | NameETree _, NameVar _ ->
            1

   let rec compare l1 l2 =
      match l1, l2 with
         n1 :: l1, n2 :: l2 ->
            let cmp = compare_name n1 n2 in
               if cmp = 0 then
                  compare l1 l2
               else
                  cmp
       | [], _ :: _ ->
            -1
       | _ :: _, [] ->
            1
       | [], [] ->
            0

   let rec pp_print_list buf l =
      match l with
         h :: t ->
            Format.fprintf buf "%a/@," pp_print_name h;
            pp_print_list buf t
       | [] ->
            ()

   and pp_print_name buf name =
      match name with
         NameInt i ->
            Format.pp_print_int buf i
       | NameVar v ->
            pp_print_symbol buf v
       | NameETree e ->
            pp_print_etree_core buf e

   let pp_print buf l =
      Format.fprintf buf "@[<hv 3>%a@]" pp_print_list l
end

module NameTable = Mc_map.McMake (NameCompare)

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * This is the inner class we use for classifying
 * values during loop analysis.
 *
 * VarExprExpr (depth, e): the value is an expression
 *
 * VarInduction (depth, op, v, e): the value is
 *    a the sum (v op e)
 *
 * VarAlias (depth, e, off, v, s, l): the value points to a block
 *    off: the offset into the block (always zero for normal arrays)
 *    e: the expression
 *    v: the must-alias class
 *    s: the may-alias classes
 *    l: the label aliases
 *)
type vclass_core =
   VarExp of depth * etree_core
 | VarAlias of depth * etree_core * etree_core * var * SymbolSet.t * SymbolSet.t

type vclass = vclass_core simple_subst

(*
 * These values are used for classifying values in blocks.
 * We just want to keep track of induction analysis.
 *)
type iclass =
   IVarUnknown of vclass * var
 | IVarVar of vclass * var
 | IVarExp of vclass
 | IVarInd of vclass * binop * var * etree_core

(*
 * For each separate area in an alias block, we keep track of
 * the set of all assignments to that block, as well as all
 * possible alias classes of pointers stored in the block.
 *    default_alias: alias classes for pointers in the block
 *    default_heap: the labels of all the assignments to this block
 *)
type default =
   { default_alias : SymbolSet.t;
     default_heap : heap
   }

(*
 * A block alias.
 *    block_heap: the heap the block is defined in
 *    block_depth: the definition loop-nesting depth of the block
 *    block_mutable: block is mutable
 *    block_length: length of the array
 *    block_default: all the possible pointers stored in this array
 *    block_int_fields: fields of the array with int indices
 *    block_lab_fields: fields of the array with field indices
 *    block_var_fields: fields with variable indices
 *)
type block =
   { block_loc : loc;
     block_depth : depth;
     block_mutable : bool;
     block_mutable_heap : heap;
     block_length : etree_core;
     block_length_heap : heap;
     block_default : default;
     block_int_fields : (int * iclass) list;
     block_lab_fields : (iclass SymbolTable.t * default) SymbolTable.t;
     block_var_fields : iclass ETreeTable.t
   }

(*
 * The alias environment keeps track of block info.
 *    aenv_reserve_heap: the reservation dependencies
 *    aenv_blocks: the heap info
 *    aenv_subst: alias class expansions
 *)
type alias_env =
   { aenv_loc : loc;
     aenv_reserve_heap : heap;
     aenv_alloc_heap : heap;
     aenv_blocks : block SymbolTable.t;
     aenv_subst : SymbolSet.t SymbolTable.t
   }

(*
 * A function call has arguments
 * and a predicate.
 *    call_args: function arguments
 *    call_aenv: alias environment at point of call
 *    call_pred: predicate at point of call
 *    call_ps_args: pseudo arguments passed through the heap
 *
 *    call_changed: this call has changed since it was last analyzed
 *    call_outer: calls from functions outside the loop
 *    call_inner: calls from functions inside the loop (only used for loop headers)
 *    call_heap_vars: implicit parameters passed through the heap
 *)
type call_entry =
   { call_pred      : pred;
     call_aenv      : alias_env;
     call_args      : vclass list
   }

type psenv = (depth * htree) SymbolTable.t

type call =
   { call_loc        : loc;
     call_changed    : bool;
     call_loop       : bool;
     call_outer      : call_entry SymbolTable.t;
     call_inner      : call_entry SymbolTable.t;
     call_ps_args    : psenv SymbolTable.t;
     call_ps_vars    : SymbolSet.t;
     call_loop_outer : (pred * alias_env * vclass list * psenv) option;
     call_loop_inner : (pred * alias_env * vclass list * psenv) option;
     call_info       : (pred * alias_env * vclass list * psenv) option
   }

(*
 * A call environment is used to collect info
 * for a function call.
 *)
type call_env =
   { cenv_fun       : var;
     cenv_depth     : depth;
     cenv_ps_vars   : SymbolSet.t;
     cenv_ps_args   : (depth * htree) SymbolTable.t SymbolTable.t;
     cenv_alias     : (var * var) SymbolTable.t
   }

(*
 * This is the info we collect for a single function.
 * It is basically a repackaging of the function.
 *
 *   fun_name: the name of the function
 *   fun_vars: the parameters of the function
 *   fun_depth: the loop-nesting depth of the function
 *      Note: for loop headers, this is one less than the loop-nesting depth
 *   fun_body: the body of the function
 *)
type finfo =
   { fun_name  : var;
     fun_vars  : (var * ty) list;
     fun_depth : depth;
     fun_body  : exp
   }

(*
 * A variable environment has:
 *    alias_class: the type of the variable
 *    alias_env: the current alias environment
 *    alias_pred: the current predicate
 *)
type ainfo =
   { ainfo_class       : vclass;
     ainfo_env_before  : alias_env;
     ainfo_pred_before : pred;
     ainfo_env_after   : alias_env;
     ainfo_pred_after  : pred
   }

(*
 * Variable environment remembers info about all
 * the vars.
 *
 *    venv_info: the state at each program point
 *    venv_subst: a substitution, augmented during loop analysis
 *    venv_vars: variable classes of variables we know about
 *    venv_funs: function depth and parameter names
 *    venv_names: environment for making up new names
 *    venv_calls: function calls in the program
 *    venv_unknown: variables whose value is not known
 *)
type venv =
   { venv_info    : ainfo SymbolTable.t;
     venv_subst   : (depth * htree) SymbolTable.t;
     venv_vars    : vclass SymbolTable.t;
     venv_loops   : SymbolSet.t SymbolTable.t;
     venv_funs    : finfo SymbolTable.t;
     venv_names   : var NameTable.t;
     venv_unknown : depth SymbolTable.t;
     venv_calls   : call SymbolTable.t
   }

(*
 * A "induction" environment contains entries for induction
 * vars.
 *
 *   ienv_ind_vars: these are the induction variable definitions
 *      the format is exactly the same as AliasInduction
 *)
type ienv =
   { ienv_ind_vars : (loc * depth * var * var * int_class * etree_core * etree_core) SymbolTable.t;
     ienv_vars : aclass SymbolTable.t
   }

(*
 * The CSE environment contains entries for all vars.
 *)
type gvenv =
   { gvenv_values          : (aclass * htree option) SymbolTable.t;
     gvenv_var_table       : (aclass * etree) SymbolTable.t;
     gvenv_induction_table : (aclass * etree) NameTable.t;
     gvenv_htree_table     : (aclass * etree) HTreeTable.t;
     gvenv_etree_table     : (aclass * etree) ETreeTable.t
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
