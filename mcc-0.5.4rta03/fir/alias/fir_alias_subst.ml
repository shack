(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_algebra
open Fir_logic.FirLogic

open Fir_alias_env
open Fir_alias_type
open Fir_alias_util

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_alias_subst" end)
open Pos

(*
 * Expand a variable.
 *)
let rec expand_var venv depth v =
   try
      let depth, htree = venv_subst venv v in
         match dest_htree_core htree with
            HTETree (ETVar v) ->
               expand_var venv depth v
          | _ ->
               depth, v
   with
      Not_found ->
         depth, v

let expand_var_opt venv vc =
   match dest_vclass_core vc with
      VarExp (depth, ETVar v)
    | VarAlias (depth, ETVar v, _, _, _, _) ->
         let depth, v = expand_var venv depth v in
            Some (depth, v)
    | _ ->
         None

(*
 * Expand a heap.
 * Expand all dependencies.
 *)
let expand_heap venv s =
   let rec expand_heap s1 s2 =
      SymbolSet.fold (fun s1 v ->
            try
               let depth, htree = venv_subst venv v in
                  match dest_htree_core htree with
                     HTDepends s2 ->
                        expand_heap s1 s2
                   | _ ->
                        SymbolSet.add s1 v
            with
               Not_found ->
                  SymbolSet.add s1 v) s1 s2
   in
      expand_heap SymbolSet.empty s

(*
 * Expand an expression.
 * We expand all occurrences of variables that are etrees.
 *)
let rec expand_loop venv e =
   match e with
      ETConst _ ->
         e
    | ETVar v ->
         (try
             let depth, htree = venv_subst venv v in
                match dest_htree_core htree with
                   HTETree e ->
                      expand_loop venv e
                 | _ ->
                      e
          with
             Not_found ->
                e)
    | ETUnop (op, e) ->
         ETUnop (op, expand_loop venv e)
    | ETBinop (op, e1, e2) ->
         ETBinop (op, expand_loop venv e1, expand_loop venv e2)

let expand_etree_core venv pos e =
   let pos = string_pos "expand_etree" pos in
      canonicalize_etree_core pos (expand_loop venv e)

let expand_etree venv pos e =
   let loc = loc_of_etree e in
   let e = expand_etree_core venv pos (dest_etree_core e) in
      make_etree loc e

(*
 * Subscripts should get expanded to byte offsets.
 *)
let expand_subscript_width venv op e =
   let { sub_index = index;
         sub_script = script
       } = op
   in
      match index with
         ByteIndex ->
            e
       | WordIndex ->
            match script with
               IntIndex ->
                  ETBinop (MulIntOp, ETConst (AtomInt sizeof_pointer), e)
             | RawIntIndex (pre, signed) ->
                  ETBinop (MulRawIntOp (pre, signed), ETConst (AtomRawInt (Rawint.of_int pre signed sizeof_pointer)), e)

let expand_subscript_sum venv pos op e1 e2 =
   let pos = string_pos "expand_subscript_sum" pos in
   let e1 = expand_subscript_width venv op e1 in
   let e2 = expand_subscript_width venv op e2 in
   let e =
      match op.sub_script with
         IntIndex ->
            ETBinop (PlusIntOp, e1, e2)
       | RawIntIndex (pre, signed) ->
            ETBinop (PlusRawIntOp (pre, signed), e1, e2)
   in
      expand_etree_core venv pos e

let expand_vclass venv pos vc =
   let pos = string_pos "expand_vclass" pos in
   let loc = loc_of_vclass vc in
   let vc =
      match dest_vclass_core vc with
         VarExp (depth, e) ->
            VarExp (depth, expand_etree_core venv pos e)
       | VarAlias (depth, e, off, v, s, l) ->
            VarAlias (depth, expand_etree_core venv pos e, expand_etree_core venv pos off, v, s, l)
   in
      make_vclass loc vc

(*
 * Etree expansion.
 *)
let etree_of_vclass_simple vc =
   let loc = loc_of_vclass vc in
      match dest_vclass_core vc with
         VarExp (depth, e)
       | VarAlias (depth, e, _, _, _, _) ->
            depth, make_etree loc e

let etree_of_vclass_simple_core vc =
   match dest_vclass_core vc with
      VarExp (depth, e)
    | VarAlias (depth, e, _, _, _, _) ->
         depth, e

let etree_of_vclass venv pos vc =
   let pos = string_pos "etree_of_vclass" pos in
   let depth, e = etree_of_vclass_simple vc in
   let e = expand_etree venv pos e in
      depth, e

let etree_of_vclass_core venv pos vc =
   let pos = string_pos "etree_of_vclass_core" pos in
   let depth, e = etree_of_vclass_simple_core vc in
   let e = expand_etree_core venv pos e in
      depth, e

let etree_of_iclass venv pos ic =
   let pos = string_pos "etree_of_iclass" pos in
      etree_of_vclass venv pos (vclass_of_iclass ic)

let etree_of_iclass_core venv pos ic =
   let pos = string_pos "etree_of_iclass" pos in
      etree_of_vclass_core venv pos (vclass_of_iclass ic)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
