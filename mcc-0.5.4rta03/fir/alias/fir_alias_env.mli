(*
 * Environments for alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Fir
open Fir_pos
open Fir_heap
open Fir_algebra
open Fir_logic.FirLogic

open Fir_depth
open Fir_alias
open Fir_alias_type

(*
 * Variable environment.
 *)
val venv_empty : venv

val venv_add_subst : venv -> var -> depth -> htree -> venv
val venv_new_name : venv -> name list -> venv * var
val venv_add_var : venv -> var -> vclass -> venv
val venv_add_fun : venv -> var -> finfo -> venv
val venv_add_unknown : venv -> var -> depth -> venv
val venv_add_info : venv -> var -> vclass -> alias_env -> pred -> alias_env -> pred -> venv
val venv_add_call : venv -> var -> call -> venv
val venv_add_loop : venv -> var -> SymbolSet.t -> venv

val venv_remove_subst : venv -> var -> venv

val venv_subst : venv -> var -> depth * htree
val venv_lookup_fun_exn : venv -> var -> finfo
val venv_lookup_var_exn : venv -> var -> vclass
val venv_lookup_var : venv -> pos -> var -> vclass
val venv_lookup_fun : venv -> pos -> var -> finfo
val venv_lookup_atom : venv -> pos -> loc -> atom -> vclass
val venv_lookup_unknown : venv -> var -> depth
val venv_is_loop : venv -> var -> bool
val venv_lookup_loop : venv -> pos -> var -> SymbolSet.t

(*
 * Call environment.
 *)
val cenv_add_param : call_env -> var -> call_env
val cenv_add_arg : call_env -> var -> var -> depth -> htree -> call_env
val cenv_add_alias : call_env -> var -> var -> var -> call_env

(*
 * Alias environment.
 *)
val aenv_empty : loc -> alias_env

val aenv_add_alias : alias_env -> var -> var -> alias_env
val aenv_expand : alias_env -> var -> SymbolSet.t -> SymbolSet.t

val aenv_add_block : alias_env -> var -> block -> alias_env
val aenv_mem_block : alias_env -> var -> bool
val aenv_lookup_block_exn : alias_env -> var -> block
val aenv_lookup_block : alias_env -> pos -> var -> block

(*
 * Induction environment.
 *)
val ienv_empty : ienv

val ienv_add_var : ienv -> var -> aclass -> ienv
val ienv_add_ind_var : ienv -> var -> loc * depth * var * var * int_class * etree_core * etree_core -> ienv

val ienv_lookup_var : ienv -> var -> aclass
val ienv_lookup_ind_var : ienv -> var -> loc * depth * var * var * int_class * etree_core * etree_core

(*
 * CSE environment.
 *)
val gvenv_empty : gvenv

val gvenv_add_htree : gvenv -> htree -> aclass -> etree -> gvenv
val gvenv_add_value : gvenv -> var -> aclass -> htree option -> gvenv
val gvenv_add_var : gvenv -> var -> aclass -> etree -> gvenv
val gvenv_add_induction_key : gvenv -> name list -> aclass -> etree -> gvenv
val gvenv_add_etree : gvenv -> etree_core -> aclass -> etree -> gvenv

val gvenv_lookup_var : gvenv -> var -> aclass * etree
val gvenv_lookup_htree : gvenv -> htree -> aclass * etree
val gvenv_lookup_induction_key : gvenv -> name list -> aclass * etree
val gvenv_lookup_etree : gvenv -> etree_core -> aclass * etree
val gvenv_lookup_value_exn : gvenv -> var -> aclass * htree option
val gvenv_lookup_value : gvenv -> pos -> var -> aclass * htree option

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
