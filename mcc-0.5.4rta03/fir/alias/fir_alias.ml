(*
 * Alias and loop analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace

open Fir
open Fir_loop
open Fir_heap
open Fir_depth
open Fir_algebra

(*
 * Alias information for a variable.
 *
 * AliasInduction (depth, v1, g, op, v2, mul, add, step, base)
 *    The variable v1 is an induction variable for loop g,
 *    offset by the loop-invariant expression step
 *    each time around the loop.  The base is the
 *    starting value.  The "depth" is the loop-nesting depth.
 *    The value is derived from induction variable v2 (v2 may
 *    be the same as v1), and the value is v1 = v2 * mul + add.
 *
 * AliasExp (depth, e):
 *    The value is loop-invariant at loop-nesting depth
 *
 * AliasAlias (depth, e, off, v, s):
 *    The value is expr e, and it must alias v at offset
 *    off, and it may-alias s at the same offset.
 *)
type int_class =
   IntClass
 | RawIntClass of int_precision * int_signed
 | PointerClass of sub_block * int_precision * int_signed

type aclass_core =
   AliasInduction of depth * var * var * int_class * var * etree_core * etree_core * etree_core * etree_core
 | AliasExp       of depth * htree_core
 | AliasAlias     of depth * etree_core * etree_core * var * SymbolSet.t * SymbolSet.t

type aclass = aclass_core simple_subst

(*
 * An alias environment is a table with
 * an alias class for each variable.
 *)
type aenv = (aclass * htree option) SymbolTable.t

(*
 * A call environment gives arguments for
 * each function call.
 *)
type cenv = (etree list * etree SymbolTable.t) SymbolTable.t

(*
 * Function parameters.
 *)
type fenv = SymbolSet.t SymbolTable.t

(*
 * The loop environment gives base/step for
 * each induction variable.
 *)
type lenv = (etree * etree) SymbolTable.t

(*
 * Result of alias analysis.
 * This is incomplete--the analyzer can provide alias info too.
 * However, this is enough for partial-redundancy relimination.
 *)
type t =
   { alias_aenv : aenv;
     alias_cenv : cenv;
     alias_fenv : fenv;
     alias_lenv : lenv
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
