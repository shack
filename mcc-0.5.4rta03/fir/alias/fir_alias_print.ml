(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Trace
open Symbol
open Location

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_print
open Fir_logic
open Fir_algebra

open Fir_logic.FirLogic

open Fir_depth
open Fir_alias
open Fir_alias_type
open Fir_alias_util

module Pos = MakePos (struct let name = "Fir_alias_print" end)
open Pos

(*
 * Function parameters.
 *)
let pp_print_vars buf vars =
   fprintf buf "(@[<hv 0>";
   ignore (List.fold_left (fun commap (v, ty) ->
                 if commap then
                    fprintf buf ",@ ";
                 fprintf buf "@[<hv 3>%a :@ %a@]" pp_print_symbol v pp_print_type ty;
                 true) false vars);
   fprintf buf "@])"

(*
 * Print a loop variable.
 *)
let pp_print_int_class buf op =
   match op with
      IntClass ->
         pp_print_string buf "<int>"
    | RawIntClass (pre, signed) ->
         fprintf buf "<rawint>{%a}" pp_print_int_precision pre
    | PointerClass (op, pre, signed) ->
         fprintf buf "<pointer>{%a, %a}" pp_print_sub_block op pp_print_int_precision pre

let pp_print_aclass buf ac =
   match dest_aclass_core ac with
      AliasInduction (depth, v1, g, op, v2, mul, add, step, base) ->
         fprintf buf "@[<hv 0>@[<hv 3><ind>{@ depth = %a;" pp_print_depth depth;
         fprintf buf "@ op = ";
         pp_print_int_class buf op;
         fprintf buf ";@ var = ";
         pp_print_symbol buf v1;
         fprintf buf ";@ loop = ";
         pp_print_symbol buf g;
         fprintf buf ";@ ind_var = ";
         pp_print_symbol buf v2;
         fprintf buf ";@ @[<hv 3>mul =@ ";
         pp_print_etree_core buf mul;
         fprintf buf ";@]@ @[<hv 3>add =@ ";
         pp_print_etree_core buf add;
         fprintf buf ";@]@ @[<hv 3>step =@ ";
         pp_print_etree_core buf step;
         fprintf buf ";@]@ @[<hv 3>base =@ ";
         pp_print_etree_core buf base;
         fprintf buf ";@]@]@ }@]"
    | AliasExp (depth, e) ->
         fprintf buf "@[<hv 3><expr>(%a, " pp_print_depth depth;
         pp_print_htree_core buf e;
         fprintf buf ")@]"
    | AliasAlias (depth, e, off, v, s, l) ->
         fprintf buf "@[<hv 0>@[<hv 3><alias>{@ depth = %a;@ @[<hv 3>expr =@ " pp_print_depth depth;
         pp_print_etree_core buf e;
         fprintf buf "@];@ @[<hv 3>offset =@ ";
         pp_print_etree_core buf off;
         fprintf buf "@];@ must-alias = ";
         pp_print_symbol buf v;
         fprintf buf ";@ @[<hv 0>@[<hv 3>may-alias = {";
         SymbolSet.iter (fun v ->
               pp_print_space buf ();
               pp_print_symbol buf v;
               pp_print_string buf ";") s;
         fprintf buf "@]@ }@];@ @[<hv 0>@[<hv 3>labels = {";
         SymbolSet.iter (fun v ->
               pp_print_space buf ();
               pp_print_symbol buf v;
               pp_print_string buf ";") l;
         fprintf buf "@]@ }@]@]@ }@]"

(*
 * Print an inner class.
 *)
let rec pp_print_vclass buf vc =
   match dest_vclass_core vc with
      VarExp (depth, e) ->
         fprintf buf "@[<hv 3><expr>(%a, " pp_print_depth depth;
         pp_print_etree_core buf e;
         fprintf buf ")@]"
    | VarAlias (depth, e, off, v, s, l) ->
         fprintf buf "@[<hv 0>@[<hv 3><alias>{@ depth = %a;@ @[<hv 3>expr =@ " pp_print_depth depth;
         pp_print_etree_core buf e;
         fprintf buf "@];@ @[<hv 3>offset =@ ";
         pp_print_etree_core buf off;
         fprintf buf "@]@ must-alias = ";
         pp_print_symbol buf v;
         fprintf buf "@ @[<hv 0>@[<hv 3>may-alias = {";
         SymbolSet.iter (fun v ->
               pp_print_space buf ();
               pp_print_symbol buf v) s;
         fprintf buf "@]@ }@]@ @[<hv 0>@[<hv 3>label-alias = {";
         SymbolSet.iter (fun v ->
               pp_print_space buf ();
               pp_print_symbol buf v) l;
         fprintf buf "@]@ }@]@]@ }@]"

(*
 * Print an array class.
 *)
let pp_print_iclass buf ic =
   match ic with
      IVarUnknown (vc, v) ->
         fprintf buf "@[<hv 3><ivar_unknown>(";
         pp_print_symbol buf v;
         fprintf buf ",@ ";
         pp_print_vclass buf vc;
         fprintf buf ")@]"
    | IVarVar (vc, v) ->
         fprintf buf "@[<hv 3><ivar_var>(";
         pp_print_symbol buf v;
         fprintf buf ",@ ";
         pp_print_vclass buf vc;
         fprintf buf ")@]"
    | IVarExp vc ->
         fprintf buf "@[<hv 3><ivar_expr>(";
         pp_print_vclass buf vc;
         fprintf buf ")@]"
    | IVarInd (vc, op, v, e) ->
         fprintf buf "@[<hv 3><ivar_ind>(";
         pp_print_symbol buf v;
         fprintf buf " %s@ " (string_of_binop op);
         pp_print_etree_core buf e;
         fprintf buf ",@ ";
         pp_print_vclass buf vc;
         fprintf buf ")@]"

(*
 * Print the alias environment.
 *)
let pp_print_default buf default =
   let { default_alias = s;
         default_heap = h
       } = default
   in
      fprintf buf "@[<hv 0>@[<hv 3>default = {";
      SymbolSet.iter (fun v ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf ";") s;
      fprintf buf "@]@ }@]@ @[<hv 0>@[<hv 3>heap = {";
      SymbolSet.iter (fun v ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf ";") h;
      fprintf buf "@]@ }@]"

let pp_print_block buf block =
   let { block_depth = depth;
         block_default = default;
         block_mutable = mutablep;
         block_length = length;
         block_int_fields = int_fields;
         block_lab_fields = lab_fields;
         block_var_fields = var_fields
       } = block
   in
      fprintf buf "@[<hv 0>@[<hv 3>block {@ mutable = %b;@ length = " mutablep;
      pp_print_etree_core buf length;
      fprintf buf ";@ ";
      pp_print_default buf default;
      fprintf buf "@ @[<hv 0>@[<hv 3>int_fields = {";
      List.iter (fun (i, ic) ->
            fprintf buf "@ @[<hv 3> %d =@ " i;
            pp_print_iclass buf ic;
            fprintf buf "@];") int_fields;
      fprintf buf "@]@ }@];@ @[<hv 0>@[<hv 3>lab_fields = {";
      SymbolTable.iter (fun v (subfields, default) ->
            fprintf buf "@ @[<hv 0>@[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf "@ ";
            pp_print_default buf default;
            fprintf buf ";@ {";
            SymbolTable.iter (fun v ic ->
                  fprintf buf "@ ";
                  pp_print_symbol buf v;
                  fprintf buf " =@ ";
                  pp_print_iclass buf ic;
                  fprintf buf ";") subfields;
            fprintf buf "@]@ }@]") lab_fields;
      fprintf buf "@]@ }@];@ @[<hv 0>@[<hv 3>var_fields = {";
      ETreeTable.iter (fun e ic ->
            fprintf buf "@ @[<hv 3>";
            pp_print_etree_core buf e;
            fprintf buf " =@ ";
            pp_print_iclass buf ic;
            fprintf buf "@];") var_fields;
      fprintf buf "@]@ }@]@]@ }@]"

let pp_print_aenv buf aenv =
   let { aenv_reserve_heap = reserve_heap;
         aenv_alloc_heap = alloc_heap;
         aenv_blocks = blocks;
         aenv_subst = subst
       } = aenv
   in
      fprintf buf "@[<hv 3>Alias environment:@ @[<hv 0>@[<b 3>Reserve Heap: {";
      SymbolSet.iter (fun v ->
            pp_print_space buf ();
            pp_print_symbol buf v) reserve_heap;
      fprintf buf "@]@ }@]@ @[<hv 0>@[<b 3>Alloc Heap: {";
      SymbolSet.iter (fun v ->
            pp_print_space buf ();
            pp_print_symbol buf v) alloc_heap;
      fprintf buf "@]@ }@]@ @[<hv 3>Subst:";
      SymbolTable.iter (fun v s ->
            fprintf buf "@ @[<hv 0>@[<b 3>";
            pp_print_symbol buf v;
            fprintf buf " {";
            SymbolSet.iter (fun v ->
                  pp_print_space buf ();
                  pp_print_symbol buf v) s;
            fprintf buf "@]@ }@]") subst;
      fprintf buf "@]@ @[<hv 3>Blocks:";
      SymbolTable.iter (fun v block ->
            fprintf buf "@ @[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " =@ ";
            pp_print_block buf block;
            fprintf buf "@]") blocks;
      fprintf buf "@]@]"

(*
 * Print a calling environment.
 *)
let pp_print_call_entry buf label entry =
   let { call_pred = pred;
         call_aenv = aenv;
         call_args = args
       } = entry
   in
      fprintf buf "@ @[<hv 3>";
      pp_print_symbol buf label;
      fprintf buf ":@ @[<hv 3>Args:";
      List.iter (fun arg ->
            pp_print_space buf ();
            pp_print_vclass buf arg;
            pp_print_string buf ";") args;
      fprintf buf "@]@ @[<hv 3>";
      pp_print_aenv buf aenv;
      fprintf buf "@]@ @[<hv 3>Pred:@ ";
      pp_print_pred buf pred;
      fprintf buf "@]@]"

let pp_print_call_entries buf table =
   SymbolTable.iter (pp_print_call_entry buf) table

let pp_print_call_info buf s info =
   fprintf buf "@ @[<hv 3>%s:" s;
   match info with
      Some (pred, aenv, args, ps_args) ->
         List.iter (fun arg ->
               pp_print_space buf ();
               pp_print_vclass buf arg) args;
         fprintf buf "@]@ @[<hv 3>PS Args:";
         SymbolTable.iter (fun v (depth, h) ->
               fprintf buf "@ @[<hv 3>";
               pp_print_symbol buf v;
               fprintf buf "[%a] =@ " pp_print_depth depth;
               pp_print_htree buf h;
               fprintf buf "@]") ps_args;
         fprintf buf "@]@ @[<hv 3>";
         pp_print_aenv buf aenv;
         fprintf buf "@]@ @[<hv 3>Pred:@ ";
         pp_print_pred buf pred;
         fprintf buf "@]"
    | None ->
         fprintf buf " <none>@]"

let pp_print_call buf call =
   let { call_info = info;
         call_outer = outer;
         call_inner = inner;
         call_loop_outer = loop_outer;
         call_loop_inner = loop_inner;
         call_ps_vars = ps_vars;
         call_ps_args = ps_args
       } = call
   in
      fprintf buf "@[<hv 3>Call:";
      pp_print_call_info buf "Loop Args" info;
      pp_print_call_info buf "Loop Inner Args" loop_inner;
      pp_print_call_info buf "Loop Outer Args" loop_outer;
      fprintf buf "@ @[<hv 3>Outer:";
      pp_print_call_entries buf outer;
      fprintf buf "@]@ @[<hv 3>Inner:";
      pp_print_call_entries buf inner;
      fprintf buf "@]@ @[<hv 0>@[<hv 3>Heap vars: {";
      SymbolSet.iter (fun v ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf ";") ps_vars;
      fprintf buf "@]@ }@]@ @[<hv 3>Heap args:";
      SymbolTable.iter (fun v args ->
            fprintf buf "@ @[<hv 0>@[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " = {";
            SymbolTable.iter (fun v (depth, arg) ->
                  fprintf buf "@ @[<hv 3>";
                  pp_print_symbol buf v;
                  fprintf buf "[%a] =@ " pp_print_depth depth;
                  pp_print_htree buf arg;
                  fprintf buf ";@]") args;
            fprintf buf "@]@ }@]") ps_args;
      fprintf buf "@]@]"

let pp_print_calls buf calls =
   SymbolTable.iter (fun f call ->
         fprintf buf "@ @[<v 3>";
         pp_print_symbol buf f;
         pp_print_call buf call;
         fprintf buf "@]") calls

(*
 * Print a function description.
 *)
let pp_print_finfo buf info =
   let { fun_name = f;
         fun_vars = vars
       } = info
   in
      fprintf buf "@[<v 3>Fun: ";
      pp_print_symbol buf f;
      pp_print_vars buf vars;
      fprintf buf "@]"

(*
 * Alias info for a var.
 *)
let pp_print_ainfo buf info =
   let { ainfo_class = ic } = info in
      pp_print_vclass buf ic

(*
 * Print the variable environment.
 *)
let pp_print_venv buf venv =
   let { venv_vars = vars;
         venv_info = info;
         venv_subst = subst;
         venv_calls = calls
       } = venv
   in
      fprintf buf "@[<v 3>Venv:";
      fprintf buf "@ @[<v 3>Vars:";
      SymbolTable.iter (fun v ic ->
            fprintf buf "@ @[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " =@ ";
            pp_print_vclass buf ic;
            fprintf buf "@]") vars;
(*
      fprintf buf "@]@ @[<v 3>Info:";
      SymbolTable.iter (fun v ic ->
            fprintf buf "@ @[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " =@ ";
            pp_print_ainfo buf ic;
            fprintf buf "@]") info;
*)
      fprintf buf "@]@ @[<v 3>Subst:";
      SymbolTable.iter (fun v (depth, e) ->
            fprintf buf "@ @[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf "[%a] =@ " pp_print_depth depth;
            pp_print_htree buf e;
            fprintf buf "@]") subst;
      fprintf buf "@]@ @[<v 3>Calls:";
      SymbolTable.iter (fun v call ->
            fprintf buf "@ @[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " =@ ";
            pp_print_call buf call;
            fprintf buf "@]") calls;
      fprintf buf "@]@]"

(*
 * Alias result.
 *)
let pp_print_alias_table buf table =
   SymbolTable.iter (fun v ac ->
         fprintf buf "@ @[<hv 3>";
         pp_print_symbol buf v;
         fprintf buf " =@ ";
         pp_print_aclass buf ac;
         fprintf buf "@]") table

let pp_print_alias_htree_table buf table =
   SymbolTable.iter (fun v (ac, h) ->
         fprintf buf "@ @[<hv 0>@[<hv 3>";
         pp_print_symbol buf v;
         fprintf buf " = {@ @[<hv 3>aclass =@ ";
         pp_print_aclass buf ac;
         fprintf buf "@]@ @[<hv 3>htree =@ ";
         (match h with
             Some h ->
                pp_print_htree buf h
           | None ->
                pp_print_string buf "<none>");
         fprintf buf "@]@]@ }@]") table

let pp_print_alias_etree_table buf table =
   SymbolTable.iter (fun v (ac, e) ->
         fprintf buf "@ @[<hv 3>";
         pp_print_symbol buf v;
         fprintf buf " =@ @[<hv 3>Alias class:@ ";
         pp_print_aclass buf ac;
         fprintf buf "@]@ @[<hv 3>Expr:@ ";
         pp_print_etree buf e;
         fprintf buf "@]@]") table

let pp_print_call_table buf table =
   SymbolTable.iter (fun label (args, ps_args) ->
         fprintf buf "@ @[<hv 3>";
         pp_print_symbol buf label;
         fprintf buf " =@ @[<hv 0>@[<hv 3>Args: {";
         List.iter (fun ac ->
               fprintf buf "@ ";
               pp_print_etree buf ac;
               fprintf buf ";") args;
         fprintf buf "@]@ }@]@ @[<hv 3>Heap:";
         SymbolTable.iter (fun v ac ->
               fprintf buf "@ @[<hv 3>";
               pp_print_symbol buf v;
               fprintf buf " =@ ";
               pp_print_etree buf ac;
               fprintf buf "@]") ps_args;
         fprintf buf "@]@]") table

let pp_print_fun_table buf table =
   SymbolTable.iter (fun f vars ->
         fprintf buf "@ @[<hv 0>@[<hv 3>";
         pp_print_symbol buf f;
         fprintf buf " {";
         SymbolSet.iter (fun v ->
               pp_print_space buf ();
               pp_print_symbol buf v;
               pp_print_string buf ";") vars;
         fprintf buf "@]@ }@]") table

(*
 * Print the induction table.
 *)
let pp_print_ienv buf ienv =
   let { ienv_ind_vars = ind_vars;
         ienv_vars = vars
       } = ienv
   in
      fprintf buf "@[<hv 3>Induction vars:";
      SymbolTable.iter (fun v (loc, depth, v, g, op, step, base) ->
            let mul = etree_of_int op 1 in
            let add = etree_of_int op 0 in
            let ac = make_aclass loc (AliasInduction (depth, v, g, op, v, mul, add, step, base)) in
               fprintf buf "@ @[<hv 3>";
               pp_print_symbol buf v;
               fprintf buf " =@ ";
               pp_print_aclass buf ac;
               fprintf buf "@]") ind_vars;
      fprintf buf "@]@ @[<hv 3>Vars:";
      SymbolTable.iter (fun v ac ->
            fprintf buf "@ @[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " =@ ";
            pp_print_aclass buf ac;
            fprintf buf "@]") vars;
      fprintf buf "@]"

(*
 * Print the loop table.
 *)
let pp_print_loop_table buf lenv =
   SymbolTable.iter (fun v (step, base) ->
         fprintf buf "@ @[<v 3>";
         pp_print_symbol buf v;
         fprintf buf "@ @[<hv 3>base =@ ";
         pp_print_etree buf base;
         fprintf buf "@]@ @[<hv 3>step =@ ";
         pp_print_etree buf step;
         fprintf buf "@]@]") lenv

(*
 * Print the info.
 *)
let pp_print_alias_info buf info =
   let { alias_aenv = aenv;
         alias_cenv = cenv;
         alias_fenv = fenv;
         alias_lenv = lenv
       } = info
   in
      fprintf buf "@[<v 3>Aliases:";
      pp_print_alias_htree_table buf aenv;
      fprintf buf "@]@ @[<v 3>Calls:";
      pp_print_call_table buf cenv;
      fprintf buf "@]@ @[<v 3>Funs:";
      pp_print_fun_table buf fenv;
      fprintf buf "@]@ @[<v 3>Loops:";
      pp_print_loop_table buf lenv;
      fprintf buf "@]"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
