(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol
open Location

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_depth
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_prog
open Fir_alias_subst
open Fir_alias_print

module Pos = MakePos (struct let name = "Fir_alias_pre_class" end)
open Pos

(*
 * BUG: we need this for variables...
 *)
let var_loc = bogus_loc "<Fir_alias_pre_class>"

(*
 * At this point, we have:
 *    1. A table of all the induction vars
 *    2. A table venv.venv_subst of all the variables and expressions in the program
 *    3. A table of all the function calls in the program
 *
 * We want to expand expressions as much as possible, in
 * their optimized form.  The only hard part here is for
 * induction variables.
 *)
type loop =
   LoopValue of aclass
 | LoopInduction of (loc * depth * var * var * int_class * etree_core * etree_core)
 | LoopSubst of (depth * htree)
 | LoopVClass of vclass
 | LoopNone of depth

(*
 * For variables, there are a sequence of cases.
 *    1. If the var already exists, use the existing value
 *    2. If the var is an induction var, it is AliasInduction
 *    3. If the var is an expression, compute recursively
 *    4. Otherwise, the var expands to itself
 *)
let rec alias_var venv ienv pos v =
   let pos = string_pos "alias_var" pos in
   let e =
      try LoopValue (ienv_lookup_var ienv v) with
         Not_found ->
            try LoopInduction (ienv_lookup_ind_var ienv v) with
               Not_found ->
                  try LoopSubst (venv_subst venv v) with
                     Not_found ->
                        try LoopVClass (venv_lookup_var_exn venv v) with
                           Not_found ->
                              try LoopNone (venv_lookup_unknown venv v) with
                                 Not_found ->
                                    raise (FirException (pos, UnboundVar v))
   in
      match e with
         LoopValue e ->
            ienv, e
       | LoopInduction (loc, d, v2, g, op, step, base) ->
            alias_ind_var venv ienv v loc d v2 g op step base
       | LoopSubst (depth, h) ->
            alias_htree_var venv ienv pos v depth h
       | LoopVClass vc ->
            alias_vclass_var venv ienv pos v vc
       | LoopNone depth ->
            alias_none_var venv ienv v depth

(*
 * When gathering an induction var, add it to the environment.
 *)
and alias_ind_var venv ienv v1 loc d v2 g op step base =
   let mul = etree_of_int op 1 in
   let add = etree_of_int op 0 in
   let ac = make_aclass loc (AliasInduction (d, v2, g, op, v2, mul, add, step, base)) in
   let ienv = ienv_add_var ienv v1 ac in
      ienv, ac

(*
 * Expand an htree.
 *)
and alias_htree_var venv ienv pos v depth h =
   let loc = loc_of_htree h in
      match dest_htree_core h with
         HTETree e ->
            alias_etree_var venv ienv pos v depth e
       | h ->
            let ac = make_aclass loc (AliasExp (depth, h)) in
            let ienv = ienv_add_var ienv v ac in
               ienv, ac

(*
 * Expand a variable class.
 *)
and alias_vclass_var venv ienv pos v vc =
   match dest_vclass_core vc with
      VarExp (depth, e)
    | VarAlias (depth, e, _, _, _, _) ->
         alias_etree_var venv ienv pos v depth e

(*
 * Expand a variable with no binding.
 *)
and alias_none_var venv ienv v depth =
   let ac = make_aclass var_loc (AliasExp (depth, HTETree (ETVar v))) in
   let ienv = ienv_add_var ienv v ac in
      ienv, ac

(*
 * Expand an etree.
 *)
and alias_etree_var venv ienv pos v depth e =
   let pos = string_pos "alias_etree_var" pos in
      match e with
         ETVar v'
         when Symbol.eq v v' ->
            (* Self-references are unknown *)
            alias_none_var venv ienv v depth
       | _ ->
            let ienv, ac = alias_etree venv ienv pos var_loc e in
            let ienv = ienv_add_var ienv v ac in
               ienv, ac

(*
 * Expand an etree.
 *)
and alias_etree venv ienv pos loc e =
   let pos = string_pos "alias_etree" pos in
      match e with
         ETConst _ ->
            ienv, make_aclass loc (AliasExp (zero_depth, HTETree e))
       | ETVar v ->
            alias_var venv ienv pos v
       | ETUnop (op, e1) ->
            let ienv, depth, e = alias_unop_exp venv ienv pos op e1 in
               ienv, make_aclass loc (AliasExp (depth, HTETree e))
       | ETBinop (op, e1, e2) ->
            let ienv, depth, e = alias_binop_exp venv ienv pos op e1 e2 in
               ienv, make_aclass loc (AliasExp (depth, HTETree e))

(*
 * Expand the etree, but force the value to be an etree.
 *)
and alias_etree_exp venv ienv pos e =
   let pos = string_pos "alias_etree_exp" pos in
      match e with
         ETConst _ ->
            ienv, zero_depth, e
       | ETVar v ->
            let ienv, ac = alias_var venv ienv pos v in
               (match dest_aclass_core ac with
                   AliasExp (depth, HTETree e) ->
                      ienv, depth, e
                 | AliasExp (depth, _) ->
                      ienv, depth, ETVar v
                 | AliasInduction (depth, v, _, _, _, _, _, _, _) ->
                      ienv, depth, ETVar v
                 | AliasAlias (depth, e, _, _, _, _) ->
                      ienv, depth, e)
       | ETUnop (op, e1) ->
            alias_unop_exp venv ienv pos op e1
       | ETBinop (op, e1, e2) ->
            alias_binop_exp venv ienv pos op e1 e2

(*
 * Expand a unary operation.
 *)
and alias_unop_exp venv ienv pos op e =
   let pos = string_pos "alias_unop_exp" pos in
   let ienv, depth, e = alias_etree_exp venv ienv pos e in
      ienv, depth, ETUnop (op, e)

(*
 * Expand a binary operation.
 *)
and alias_binop_exp venv ienv pos op e1 e2 =
   let pos = string_pos "alias_binop_exp" pos in
   let ienv, depth1, e1 = alias_etree_exp venv ienv pos e1 in
   let ienv, depth2, e2 = alias_etree_exp venv ienv pos e2 in
      ienv, max_depth depth1 depth2, ETBinop (op, e1, e2)

(*
 * Perform alias analysis on the program.
 * Just run the algorithm on all the vars in
 * both environments.
 *)
let alias_prog venv ienv =
   let ienv =
      SymbolTable.fold (fun ienv v _ ->
            let pos = string_pos "alias_prog_subst" (var_exp_pos v) in
            let ienv, _ = alias_var venv ienv pos v in
               ienv) ienv venv.venv_subst
   in
   let ienv =
      SymbolTable.fold (fun ienv v _ ->
            let pos = string_pos "alias_prog_vars" (var_exp_pos v) in
            let ienv, _ = alias_var venv ienv pos v in
               ienv) ienv venv.venv_vars
   in
   let ienv =
      SymbolTable.fold (fun ienv v _ ->
            let pos = string_pos "alias_prog_ind_vars" (var_exp_pos v) in
            let ienv, _ = alias_var venv ienv pos v in
               ienv) ienv ienv.ienv_ind_vars
   in
   let aenv = ienv.ienv_vars in
      if debug debug_alias then
         Format.printf "@[<v 3>*** FIR: after initial alias analysis:%a@]@." pp_print_alias_table aenv;
      aenv


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
