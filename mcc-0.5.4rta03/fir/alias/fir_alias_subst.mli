(*
 * Term expansion for alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir
open Fir_pos
open Fir_heap
open Fir_algebra

open Fir_depth
open Fir_alias_type

val expand_var_opt : venv -> vclass -> (depth * var) option
val expand_heap : venv -> heap -> heap
val expand_etree : venv -> pos -> etree -> etree
val expand_etree_core : venv -> pos -> etree_core -> etree_core
val expand_vclass : venv -> pos -> vclass -> vclass
val expand_subscript_sum : venv -> pos -> subop -> etree_core -> etree_core -> etree_core
val expand_subscript_width : venv -> subop -> etree_core -> etree_core

val etree_of_vclass : venv -> pos -> vclass -> depth * etree
val etree_of_iclass : venv -> pos -> iclass -> depth * etree

val etree_of_vclass_core : venv -> pos -> vclass -> depth * etree_core
val etree_of_iclass_core : venv -> pos -> iclass -> depth * etree_core

val etree_of_vclass_simple : vclass -> depth * etree
val etree_of_vclass_simple_core : vclass -> depth * etree_core

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
