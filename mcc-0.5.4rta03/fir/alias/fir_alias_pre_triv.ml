(*
 * Remove "trivial" induction variables.
 * These are induction variables with a multiplier of 1.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Debug

open Fir
open Fir_exn
open Fir_pos
open Fir_heap
open Fir_state
open Fir_algebra

open Fir_alias
open Fir_alias_env
open Fir_alias_util
open Fir_alias_type
open Fir_alias_print

open Fir_alias_pre_norm
open Fir_alias_pre_cse

module Pos = MakePos (struct let name = "Fir_alias_pre_triv" end)
open Pos

(*
 * A multiplier is "trivial" if it is 1.
 *)
let emul_atom_is_trivial a =
   match a with
      AtomInt i
    | AtomEnum (_, i) ->
         i = 1
    | AtomRawInt i ->
         Rawint.is_one i
    | _ ->
         false

let emul_is_trivial e =
   match e with
      ETConst a ->
         emul_atom_is_trivial a
    | _ ->
         false

(*
 * Get a table of all induction vars for each loop.
 *)
let collect_loops gvenv =
   SymbolTable.fold (fun loops _ (ac, h) ->
         match dest_aclass_core ac with
            AliasInduction (_, v, g, _, _, _, _, step, _) ->
               SymbolTable.filter_add loops g (fun table ->
                     let table =
                        match table with
                           Some table ->
                              table
                         | None ->
                              ETreeTable.empty
                     in
                        ETreeTable.filter_add table step (fun table ->
                              let table =
                                 match table with
                                    Some table ->
                                       table
                                  | None ->
                                       SymbolTable.empty
                              in
                                 SymbolTable.add table v (ac, h)))
          | AliasAlias _
          | AliasExp _ ->
               loops) SymbolTable.empty gvenv.gvenv_values

(*
 * Search for the variable that is the primary induction
 * variable.
 *)
let sort_vars pos vars =
   (* Search for the primary var *)
   let rec primary vars =
      match vars with
         (v, ac, _) as h :: vars ->
            (match dest_aclass_core ac with
                AliasInduction (_, _, _, _, v2, _, _, _, _) ->
                   if Symbol.eq v v2 then
                      h, vars
                   else
                      let h', vars = primary vars in
                         h', h :: vars
              | _ ->
                   raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac)))))
       | [] ->
            raise Not_found
   in

   (* Search for a var with a 0 base *)
   let rec secondary vars =
      match vars with
         (_, ac, _) as h :: vars ->
            (match dest_aclass_core ac with
                AliasInduction (_, _, _, _, _, _, _, _, base) ->
                   (match base with
                       ETConst (AtomInt 0)
                     | ETConst (AtomEnum (_, 0)) ->
                          h, vars
                     | ETConst (AtomRawInt i) when Rawint.is_zero i ->
                          h, vars
                     | _ ->
                          let h', vars = secondary vars in
                             h', h :: vars)
              | _ ->
                   raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac)))))
       | [] ->
            raise Not_found
   in

   (* Search for constants *)
   let rec tertiary vars =
      match vars with
         (_, ac, _) as h :: vars ->
            (match dest_aclass_core ac with
                AliasInduction (_, _, _, _, _, _, _, _, base) ->
                   (match base with
                       ETConst (AtomInt _)
                     | ETConst (AtomEnum _)
                     | ETConst (AtomRawInt _) ->
                          h, vars
                     | _ ->
                          let h', vars = tertiary vars in
                             h', h :: vars)
              | _ ->
                   raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac)))))
       | [] ->
            raise Not_found
   in

   (* Search for labels *)
   let rec quaternary vars =
      match vars with
         (_, ac, _) as h :: vars ->
            (match dest_aclass_core ac with
                AliasInduction (_, _, _, _, _, _, _, _, base) ->
                   (match base with
                       ETConst (AtomLabel (_, i)) when Rawint.is_zero i ->
                          h, vars
                     | _ ->
                          let h', vars = quaternary vars in
                             h', h :: vars)
              | _ ->
                   raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac)))))
       | [] ->
            raise Not_found
   in

   let rec finally vars =
      match vars with
         (_, ac, _) as h :: vars ->
            (match dest_aclass_core ac with
                AliasInduction (_, _, _, _, _, _, _, _, base) ->
                   (match base with
                       ETConst _ ->
                          h, vars
                     | _ ->
                          let h', vars = finally vars in
                             h', h :: vars)
              | _ ->
                   raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac)))))
       | [] ->
            raise Not_found
   in
   let vars =
      try
         let h, vars = primary vars in
            h :: vars
      with
         Not_found ->
            try
               let h, vars = secondary vars in
                  h :: vars
            with
               Not_found ->
                  try
                     let h, vars = tertiary vars in
                        h :: vars
                  with
                     Not_found ->
                        try
                           let h, vars = quaternary vars in
                              h :: vars
                        with
                           Not_found ->
                              try
                                 let h, vars = finally vars in
                                    h :: vars
                              with
                                 Not_found ->
                                    vars
   in
      vars

(*
 * Compute all the other vars from the first var.
 *)
let squash_var gvenv aenv lenv pos v1 op base1 vars =
   let pos = string_pos "squash_var" pos in
   let off = negate_etree op base1 in
   let off = add_etree op (ETVar v1) off in
      List.fold_left (fun (gvenv, aenv, lenv) (v2, ac2, h2) ->
            let loc = loc_of_aclass ac2 in
               match dest_aclass_core ac2 with
                  AliasInduction (depth, _, _, _, _, _, _, _, base2) ->
                     let base = add_etree op off base2 in
                     let base = expand_etree_core aenv base in
                     let base = canonicalize_etree_core pos base in
                     let gvenv, _, e = cse_normalize_etree gvenv aenv pos depth base in
                     let h =
                        match e, h2 with
                           ETVar v3, Some h ->
                              if debug debug_alias then
                                 begin
                                    Format.printf "@[<hv 3>Squash_var:@ @[<hv 3>aclass =@ %a" pp_print_aclass ac2;
                                    Format.printf ";@]@ var = %a" pp_print_symbol v2;
                                    Format.printf ";@ ind_var = %a" pp_print_symbol v1;
                                    Format.printf ";@ @[<hv 3>base1 =@ %a" pp_print_etree_core base1;
                                    Format.printf ";@]@ @[<hv 3>base2 =@ %a" pp_print_etree_core base2;
                                    Format.printf ";@]@ @[<hv 3>off =@ %a" pp_print_etree_core off;
                                    Format.printf ";@]@ @[<hv 3>expr =@ %a" pp_print_etree_core e;
                                    Format.printf ";@]@ @[<hv 3>base =@ %a" pp_print_etree_core base;
                                    Format.printf ";@]@ @[<hv 3>htree =@ %a" pp_print_htree h;
                                    Format.printf ";@]@]@."
                                 end;
                              if Symbol.eq v3 v2 then
                                 dest_htree_core h
                              else
                                 HTETree e
                         | _ ->
                              HTETree e
                     in
                     let ac = make_aclass loc (AliasExp (depth, h)) in
                     let aenv = SymbolTable.add aenv v2 ac in
                     let _ = Format.printf "XXX: %a@." pp_print_symbol v2 in
                     let gvenv = gvenv_add_value gvenv v2 ac (Some (make_htree loc h)) in
                     let lenv = SymbolTable.remove lenv v2 in
                        gvenv, aenv, lenv
                | _ ->
                     raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac2))))) (**)
         (gvenv, aenv, lenv) vars

(*
 * Compute all the other vars from the first var.
 *)
let squash_vars gvenv aenv lenv pos v ac vars =
   let pos = string_pos "squash_var" pos in
      match dest_aclass_core ac with
         AliasInduction (_, _, _, op, _, _, _, _, base) ->
            squash_var gvenv aenv lenv pos v op base vars
       | _ ->
            raise (FirException (pos, StringFormatError ("not an induction var", (fun buf -> pp_print_aclass buf ac))))

(*
 * For each of the variables with the same step,
 * choose one as the representative, and compute all the
 * others from that one.
 *)
let squash_loops gvenv aenv lenv loops =
   SymbolTable.fold (fun (gvenv, aenv, lenv) g table ->
         (* Make a list of all the vars with the same step *)
         let pos = string_pos "squash_loops" (var_exp_pos g) in
            ETreeTable.fold (fun (gvenv, aenv, lenv) e table ->
                  let vars =
                     SymbolTable.fold (fun vars v (ac, h) ->
                           (v, ac, h) :: vars) [] table
                  in
                  let vars = sort_vars pos vars in
                     if debug debug_alias then
                        begin
                           Format.printf "@[<hv 3>Trivial induction variable class:@ @[<hv 3>mul =@ %a@]" pp_print_etree_core e;
                           List.iter (fun (v, ac, h) ->
                                 Format.printf "@ @[<hv 3>%a" pp_print_symbol v;
                                 Format.printf " =@ %a" pp_print_aclass ac;
                                 (match h with
                                     Some h ->
                                        Format.printf "@ @[<hv 3>htree =@ %a@]" pp_print_htree h
                                   | None ->
                                        ());
                                 Format.printf "@]") vars;
                           Format.printf "@]@."
                        end;
                     match vars with
                        (v, ac, _) :: vars ->
                           (* There should be at least one *)
                           let pos = string_pos "squash_loops" (var_exp_pos v) in
                              squash_vars gvenv aenv lenv pos v ac vars
                      | [] ->
                           gvenv, aenv, lenv) (gvenv, aenv, lenv) table) (gvenv, aenv, lenv) loops

(*
 * Rewrite all the vars to use the new info.
 *)
let rewrite_prog gvenv =
   SymbolTable.fold (fun gvenv v (ac, _) ->
         let loc = loc_of_aclass ac in
            match dest_aclass_core ac with
               AliasInduction (depth, v1, _, _, _, _, _, _, _) ->
                  if Symbol.eq v v1 then
                     gvenv
                  else
                     let h = HTETree (ETVar v1) in
                     let ac = make_aclass loc (AliasExp (depth, h)) in
                        Format.printf "YYY: %a@." pp_print_symbol v;
                        gvenv_add_value gvenv v ac (Some (make_htree loc h))
             | _ ->
                  gvenv) gvenv gvenv.gvenv_values

(*
 * Put it all together.
 *)
let triv_prog gvenv lenv =
   let loops = collect_loops gvenv in
   let aenv = SymbolTable.map fst gvenv.gvenv_values in
   let gvenv, aenv, lenv = squash_loops gvenv aenv lenv loops in
   let gvenv = rewrite_prog gvenv in
      if debug debug_alias then
         begin
            Format.printf "@[<v 3>*** FIR: alias after trivial induction removal:@ @[<v 3>Aliases:%a" (**)
               pp_print_alias_htree_table gvenv.gvenv_values;
            Format.printf "@]@ @[<v 3>Values:%a" (**)
               pp_print_alias_etree_table gvenv.gvenv_var_table;
            Format.printf "@]@ @[<v 3>Loops:%a" (**)
               pp_print_loop_table lenv;
            Format.printf "@]@]@."
         end;
      gvenv, lenv


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
