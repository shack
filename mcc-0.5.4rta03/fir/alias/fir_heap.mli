(*
 * Heap values.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fir
open Fir_pos
open Fir_env
open Fir_algebra

(*
 * The heap is a set of previous values.
 *)
type heap = SymbolSet.t

(*
 * These are the heap expressions.
 *)
type htree_core =
   HTETree of etree_core
 | HTAlloc of heap * etree_core poly_alloc_op
 | HTSubscript of heap * subop * ty * etree_core * etree_core
 | HTSetSubscript of heap * subop * ty * etree_core * etree_core * etree_core
 | HTMemcpy of heap * subop * etree_core * etree_core * etree_core * etree_core * etree_core
 | HTExt of heap * ty * string * bool * ty * ty list * etree_core list
 | HTAssert of heap * etree_core poly_pred
 | HTDepends of heap

type htree

module HTreeCompare : Mc_map.OrderedType with type t = htree
module HTreeTable : Mc_map.McMap with type key = htree

(*
 * Term operations.
 *)
val loc_of_htree : htree -> loc
val make_htree : loc -> htree_core -> htree
val dest_htree_core : htree -> htree_core

(*
 * Print a heap.
 *)
val pp_print_heap : formatter -> heap -> unit
val pp_print_htree : formatter -> htree -> unit
val pp_print_htree_core : formatter -> htree_core -> unit

(*
 * Utilities.
 *)
val type_of_htree : genv -> pos -> htree -> ty
val type_of_htree_core : genv -> pos -> htree_core -> ty

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
