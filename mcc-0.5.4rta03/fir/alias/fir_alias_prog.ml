(*
 * Alias analysis.  I will really need to document this soon (jyh).
 *
 * We combine loop analysis with alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Symbol
open Trace
open Debug

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_heap
open Fir_loop
open Fir_frame
open Fir_state
open Fir_print
open Fir_algebra

open Fir_logic.FirLogic
open Fir_loop.FirLoop

open Fir_depth
open Fir_alias_env
open Fir_alias_mem
open Fir_alias_type
open Fir_alias_util
open Fir_alias_subst
open Fir_alias_print
open Fir_alias_unify

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_alias_prog" end)
open Pos

(*
 * Make the calls easier by passing invariant args in
 * a record.
 *)
type prog_env =
   { prog_genv  : genv;
     prog_fenv  : Fir_frame.t;
     prog_fun   : var;
     prog_depth : depth;
     prog_live  : SymbolSet.t
   }

(*
 * Make up a variable that acts like an output channel.
 *)
let v_global = new_symbol_string "global"

(*
 * Add a predicate assignment.
 *)
let pred_op venv pos pred v vc =
   let _, e = etree_of_vclass venv pos vc in
      pred_assign pred v e

(*
 * Blocks that are live.
 *)
let aenv_live_blocks venv aenv pos vars =
   let pos = string_pos "aenv_live_blocks" pos in
   let live =
      List.fold_left (fun live (v, _) ->
            match dest_vclass_core (venv_lookup_var venv pos v) with
               VarAlias (_, _, _, v, s, l) ->
                  let live = SymbolSet.union live s in
                  let live = SymbolSet.add live v in
                     live
             | VarExp _ ->
                  live) SymbolSet.empty vars
   in
   let live_step live =
      SymbolSet.fold (fun live v ->
            let block = aenv_lookup_block aenv pos v in
               SymbolSet.union live block.block_default.default_alias) live live
   in
   let rec fixpoint live =
      let live' = live_step live in
         if SymbolSet.equal live live' then
            live
         else
            fixpoint live'
   in
      fixpoint live

(*
 * Unify the arguments in a function call.
 *)
let venv_call venv depth pos g label f finfo pred aenv args =
   if debug debug_alias then
      begin
         Format.printf "@[<hv 3>Adding call[%a] from function %a" pp_print_depth depth pp_print_symbol g;
         Format.printf ": %a" pp_print_symbol label;
         Format.printf ": %a" pp_print_symbol f;
         Format.printf "@]@."
      end;
   let { fun_depth = fdepth;
         fun_vars = vars
       } = finfo
   in
   let call =
      { call_pred = pred;
        call_aenv = aenv;
        call_args = args
      }
   in
   let call_loc = bogus_loc "<Fir_alias_prog.venv_call>" in
   let calls =
      SymbolTable.filter_add venv.venv_calls f (fun call' ->
            let call' =
               match call' with
                  Some call' ->
                     call'
                | None ->
                     if debug debug_alias then
                        Format.printf "Call entry was not found@.";
                     { call_loc = call_loc;
                       call_changed = true;
                       call_loop = venv_is_loop venv f;
                       call_outer = SymbolTable.empty;
                       call_inner = SymbolTable.empty;
                       call_ps_args = SymbolTable.empty;
                       call_ps_vars = SymbolSet.empty;
                       call_loop_outer = None;
                       call_loop_inner = None;
                       call_info = None
                     }
            in
               if call'.call_loop && SymbolSet.mem (venv_lookup_loop venv pos f) g then
                  begin
                     if debug debug_alias then
                        begin
                           Format.printf "@[<hv 3>It's an inner call[%b,%a]%t@." (**)
                              call'.call_loop pp_print_depth fdepth
                              (fun buf -> pp_print_call_entry buf label call)
                        end;
                     { call' with call_changed = true;
                                  call_inner = SymbolTable.add call'.call_inner label call
                     }
                  end
               else
                  begin
                     if debug debug_alias then
                        Format.printf "It's an outer call[%b,%a]@." call'.call_loop pp_print_depth fdepth;
                     { call' with call_changed = true;
                                  call_outer = SymbolTable.add call'.call_outer label call
                     }
                  end)
   in
      { venv with venv_calls = calls }

(*
 * Add a new unknown.
 * If this has a pointer type, it is basically a mistake (the
 * pointer turned up unexpectedly).  For these cases we assume no
 * aliasing.  BUG.
 *)
let fun_new_unknown penv venv aenv pos loc v ty =
   let pos = string_pos "fun_add_unknown" pos in
   let new_default () =
      Format.printf "*** Warning: pointer came out of nowhere@.";
      default_empty
   in
   let { prog_genv = genv;
         prog_depth = depth
       } = penv
   in
   let venv, aenv, _, e = alias_new_unknown genv venv aenv depth pos loc v ty new_default in
      venv, aenv, vclass_of_iclass e

(*
 * Scan a function in the loop.
 *)
let rec fun_exp penv venv aenv pred e =
   let pos = string_pos "fun_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, AtomUnop (op, a), e) ->
            fun_unop_exp penv venv aenv pred pos loc v ty op a e
       | LetAtom (v, ty, AtomBinop (op, a1, a2), e) ->
            fun_binop_exp penv venv aenv pred pos loc v ty op a1 a2 e
       | LetAtom (v, ty, a, e) ->
            fun_atom_exp penv venv aenv pred pos loc v ty a e
       | TailCall (label, f, args) ->
            fun_tailcall_exp penv venv aenv pred pos loc label f args
       | Call (label, f, args, e) ->
            fun_call_exp penv venv aenv pred pos loc label f args e
       | Match (a, cases) ->
            fun_match_exp penv venv aenv pred pos loc a cases
       | MatchDTuple (a, cases) ->
            fun_match_dtuple_exp penv venv aenv pred pos loc a cases
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            fun_typecase_exp penv venv aenv pred pos loc a1 a2 v1 v2 e1 e2
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            fun_ext_exp penv venv aenv pred pos loc v ty1 s b ty2 ty_args args e
       | LetAlloc (v, op, e) ->
            fun_alloc_exp penv venv aenv pred pos loc v op e
       | LetSubscript (op, v1, ty, v2, a3, e) ->
            fun_subscript_exp penv venv aenv pred pos loc op v1 ty v2 a3 e
       | SetSubscript (op, label, v1, a2, ty, a3, e) ->
            fun_set_subscript_exp penv venv aenv pred pos loc op label v1 a2 ty a3 e
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            fun_memcpy_exp penv venv aenv pred pos loc op label v1 a1 v2 a2 a3 e
       | Assert (label, pred', e) ->
            (* fun_assert_exp penv venv aenv pred pos label pred' e *)
            fun_exp penv venv aenv pred e
       | LetGlobal (op, v, ty, l, e) ->
            fun_global_exp penv venv aenv pred pos loc op v ty l e
       | SetGlobal (op, label, v, ty, a, e) ->
            fun_set_global_exp penv venv aenv pred pos loc op label v ty a e
       | Debug (_, e) ->
            fun_exp penv venv aenv pred e
       | SpecialCall _ ->
            venv

(*
 * Plain atom assignment.
 *)
and fun_atom_exp penv venv aenv1 pred1 pos loc v ty a e =
   let pos = string_pos "fun_atom_exp" pos in
   let a = venv_lookup_atom venv pos loc a in
   let depth', e' = etree_of_vclass_core venv pos a in
   let venv = venv_add_subst venv v depth' (make_htree loc (HTETree e')) in
   let venv, aenv2, a = venv, aenv1, a in
   let pred2 = pred_op venv pos pred1 v a in
   let venv = venv_add_var venv v a in
   let venv = venv_add_info venv v a aenv1 pred1 aenv2 pred2 in
      fun_exp penv venv aenv2 pred2 e

(*
 * Catch simple unops.
 *)
and fun_unop_exp penv venv aenv1 pred1 pos loc v ty op a e =
   let pos = string_pos "fun_unop_exp" pos in
   let a = venv_lookup_atom venv pos loc a in
   let depth', e' = etree_of_vclass_core venv pos a in
   let venv = venv_add_subst venv v depth' (make_htree loc (HTETree (ETUnop (op, e')))) in
   let venv, aenv2, a =
      match op, dest_vclass_core a with
         RawIntOfRawIntOp (p1, s1, p2, s2), _
         when p1 = p2 && s1 = s2 ->
            venv, aenv1, a
       | _, VarExp (depth, e) ->
            venv, aenv1, make_vclass loc (VarExp (depth, ETUnop (op, e)))
       | PointerOfBlockOp _, VarAlias (depth, e, off, v, s, l) ->
            venv, aenv1, make_vclass loc (VarAlias (depth, ETUnop (op, e), off, v, s, l))
       | _ ->
            if debug debug_alias then
               begin
                  Format.printf "@[<hv 3>fun_unop_exp: unknown:@ %s@ %a@]@." (**)
                     (string_of_unop op)
                     pp_print_vclass a
               end;
            fun_new_unknown penv venv aenv1 pos loc v ty
   in
   let pred2 = pred_op venv pos pred1 v a in
   let venv = venv_add_var venv v a in
   let venv = venv_add_info venv v a aenv1 pred1 aenv2 pred2 in
      fun_exp penv venv aenv2 pred2 e

(*
 * Binary operations.
 *)
and fun_binop_exp penv venv aenv1 pred1 pos loc v ty op a1 a2 e =
   let pos = string_pos "fun_binop_exp" pos in
   let a1 = venv_lookup_atom venv pos loc a1 in
   let a2 = venv_lookup_atom venv pos loc a2 in
   let depth1, e1 = etree_of_vclass_core venv pos a1 in
   let depth2, e2 = etree_of_vclass_core venv pos a2 in
   let venv = venv_add_subst venv v (max_depth depth1 depth2) (make_htree loc (HTETree (ETBinop (op, e1, e2)))) in
   let venv, aenv2, a =
      match op, dest_vclass_core a1, dest_vclass_core a2 with
         _, VarExp (depth1, a1), VarExp (depth2, a2) ->
            venv, aenv1, make_vclass loc (VarExp (max_depth depth1 depth2, ETBinop (op, a1, a2)))
       | PlusPointerOp (_, pre, signed), VarAlias (depth1, e1, off1, v1, s1, l1), VarExp (depth2, e2) ->
            let e1 = ETBinop (op, e1, e2) in
            let off1 = ETBinop (PlusRawIntOp (pre, signed), off1, e2) in
            let l1 =
               match e2 with
                  ETConst (AtomLabel ((_, l, _), _)) ->
                     SymbolSet.singleton l
                | _ ->
                     l1
            in
               venv, aenv1, make_vclass loc (VarAlias (max_depth depth1 depth2, e1, off1, v1, s1, l1))
       | _ ->
            if debug debug_alias then
               begin
                  Format.printf "@[<hv 3>fun_binop_exp: unknown:@ %s@ %a@ %a@]@." (**)
                     (string_of_binop op)
                     pp_print_vclass a1
                     pp_print_vclass a2
               end;
            fun_new_unknown penv venv aenv1 pos loc v ty
   in
   let pred2 = pred_op venv pos pred1 v a in
   let venv = venv_add_var venv v a in
   let venv = venv_add_info venv v a aenv1 pred1 aenv2 pred2 in
      fun_exp penv venv aenv2 pred2 e

(*
 * Match expression.
 *)
and fun_match_exp penv venv aenv pred1 pos loc a cases =
   let pos = string_pos "fun_match_exp" pos in
      List.fold_left (fun venv (l, s, e) ->
            let pred2 = pred_match loc pred1 a s in
               fun_exp penv venv aenv pred2 e) venv cases

(*
 * Match a dtuple.
 * BUG: we may want to use the match info in the case body.
 *)
and fun_match_dtuple_exp penv venv aenv pred1 pos loc a cases =
   let pos = string_pos "fun_match_dtuple_exp" pos in
      List.fold_left (fun venv (l, a_opt, e) ->
            fun_exp penv venv aenv pred1 e) venv cases

(*
 * Typecase.
 * BUG: we give up on the object.  We can do better: the object in v2
 * is basically the same as in a1.
 *)
and fun_typecase_exp penv venv aenv1 pred pos loc a1 a2 v1 v2 e1 e2 =
   let pos = string_pos "fun_typecase_exp" pos in
   let venv, aenv2, op = fun_new_unknown penv venv aenv1 pos loc v2 TyRawData in
   let venv = venv_add_var venv v2 op in
   let venv = venv_add_info venv v2 op aenv1 pred aenv2 pred in
   let venv = fun_exp penv venv aenv2 pred e1 in
   let venv = fun_exp penv venv aenv2 pred e2 in
      venv

(*
 * External call.
 * We assume the external call obliterates its pointer arguments.
 * Also, we assume a new heap is created by the call (in case there
 * was allocation).
 *)
and fun_ext_exp penv venv aenv1 pred pos loc v ty1 s b ty2 ty_args args e =
   let pos = string_pos "fun_ext_exp" pos in
   let args = List.map (venv_lookup_atom venv pos loc) (AtomVar v_global :: args) in
   let venv, aenv2, a = fun_new_unknown penv venv aenv1 pos loc v ty1 in
   let venv = venv_add_var venv v a in
   let aenv2, heap = alias_overwrite_args aenv2 pos v args in
   let depth, el =
      List.fold_left (fun (depth1, el) a ->
            let depth2, e = etree_of_vclass_core venv pos a in
               max_depth depth1 depth2, e :: el) (zero_depth, []) args
   in
   let venv = venv_add_subst venv v depth (make_htree loc (HTExt (heap, ty1, s, b, ty2, ty_args, List.tl (List.rev el)))) in
   let venv = venv_add_info venv v a aenv1 pred aenv2 pred in
      fun_exp penv venv aenv2 pred e

(*
 * An allocation.
 *)
and fun_alloc_exp penv venv aenv1 pred pos loc v op e =
   let pos = string_pos "fun_alloc_exp" pos in
   let block, op =
      match op with
         AllocTuple (tclass, ty, ty_vars, args) ->
            let block, el = fun_alloc_tuple penv venv aenv1 pos loc args in
            let op = AllocTuple (tclass, ty, ty_vars, el) in
               block, op
       | AllocDTuple (ty, ty_var, a, args) ->
            let block, e, el = fun_alloc_dtuple penv venv aenv1 pos loc a args in
            let op = AllocDTuple (ty, ty_var, e, el) in
               block, op
       | AllocUnion (ty, ty_vars, v, i, args) ->
            let block, el = fun_alloc_tuple penv venv aenv1 pos loc args in
            let op = AllocUnion (ty, ty_vars, v, i, el) in
               block, op
       | AllocArray (ty, args) ->
            let block, el = fun_alloc_tuple penv venv aenv1 pos loc args in
            let op = AllocArray (ty, el) in
               block, op
       | AllocVArray (ty, subop, a1, a2) ->
            let block, e1, e2 = fun_alloc_array penv venv aenv1 pos loc a1 a2 in
            let op = AllocVArray (ty, subop, e1, e2) in
               block, op
       | AllocMalloc (ty, a) ->
            let block, e = fun_malloc penv venv aenv1 pos loc a in
            let op = AllocMalloc (ty, e) in
               block, op
       | AllocFrame (v, tyl) ->
            let block = fun_alloc_frame penv pos loc v in
               block, AllocFrame (v, tyl)
   in
   let { prog_depth = depth;
         prog_live = live
       } = penv
   in
   let ic = make_vclass loc (VarAlias (depth, ETVar v, ETConst zero_subscript, v, SymbolSet.empty, SymbolSet.empty)) in
   let venv = venv_add_var venv v ic in
   let aenv2, heap =
      if SymbolSet.mem live v then
         let heap = aenv1.aenv_alloc_heap in
         let aenv = { aenv1 with aenv_alloc_heap = SymbolSet.add heap v } in
            aenv, heap
      else
         aenv1, SymbolSet.empty
   in
   let aenv2 = aenv_add_block aenv2 v block in
   let venv  = venv_add_subst venv v depth (make_htree loc (HTAlloc (heap, op))) in
   let venv  = venv_add_info venv v ic aenv1 pred aenv2 pred in
      fun_exp penv venv aenv2 pred e

(*
 * Allocate a tuple.
 *)
and fun_alloc_tuple penv venv aenv pos loc args =
   let pos = string_pos "fun_alloc_tuple" pos in
   let { prog_depth = depth } = penv in
   let el, fields, default, length =
      List.fold_left (fun (el, fields, default, index) a ->
            let a = venv_lookup_atom venv pos loc a in
            let _, e = etree_of_vclass_core venv pos a in
            let default =
               match dest_vclass_core a with
                  VarExp _ ->
                     default
                | VarAlias (_, _, _, v, s, _) ->
                     SymbolSet.union default (SymbolSet.add s v)
            in
            let el = e :: el in
            let fields = (index, IVarExp a) :: fields in
            let index = index + sizeof_pointer in
               el, fields, default, index) ([], [], SymbolSet.empty, 0) args
   in
   let default =
      { default_alias = default;
        default_heap = heap_empty
      }
   in
   let el = List.rev el in
   let fields = List.rev fields in
   let block =
      { block_loc = loc;
        block_depth = depth;
        block_mutable = true;
        block_mutable_heap = heap_empty;
        block_length = ETConst (AtomInt length);
        block_length_heap = heap_empty;
        block_default = default;
        block_int_fields = fields;
        block_lab_fields = SymbolTable.empty;
        block_var_fields = ETreeTable.empty
      }
   in
      block, el

(*
 * Allocate a dependent tuple.
 * BUG: for now this code is identical to the tuple case,
 * but we probably want to remember the tag.
 *)
and fun_alloc_dtuple penv venv aenv pos loc a args =
   let pos = string_pos "fun_alloc_dtuple" pos in
   let { prog_depth = depth } = penv in
   let a = venv_lookup_atom venv pos loc a in
   let _, e = etree_of_vclass_core venv pos a in
   let el, fields, default, length =
      List.fold_left (fun (el, fields, default, index) a ->
            let a = venv_lookup_atom venv pos loc a in
            let _, e = etree_of_vclass_core venv pos a in
            let default =
               match dest_vclass_core a with
                  VarExp _ ->
                     default
                | VarAlias (_, _, _, v, s, _) ->
                     SymbolSet.union default (SymbolSet.add s v)
            in
            let el = e :: el in
            let fields = (index, IVarExp a) :: fields in
            let index = index + sizeof_pointer in
               el, fields, default, index) ([], [], SymbolSet.empty, 0) args
   in
   let default =
      { default_alias = default;
        default_heap = heap_empty
      }
   in
   let el = List.rev el in
   let fields = List.rev fields in
   let block =
      { block_loc = loc;
        block_depth = depth;
        block_mutable = true;
        block_mutable_heap = heap_empty;
        block_length = ETConst (AtomInt length);
        block_length_heap = heap_empty;
        block_default = default;
        block_int_fields = fields;
        block_lab_fields = SymbolTable.empty;
        block_var_fields = ETreeTable.empty
      }
   in
      block, e, el

(*
 * Allocate an array.
 * BUG: we probably need some way to represent duplicate array entries.
 *)
and fun_alloc_array penv venv aenv pos loc a_size a_entry =
   let pos = string_pos "fun_alloc_array" pos in
   let { prog_depth = depth } = penv in
   let length = venv_lookup_atom venv pos loc a_size in
   let entry = venv_lookup_atom venv pos loc a_entry in
   let default =
      match dest_vclass_core entry with
         VarExp _ ->
            SymbolSet.empty
       | VarAlias (_, _, _, v, s, _) ->
            SymbolSet.add s v
   in
   let default =
      { default_alias = default;
        default_heap = heap_empty
      }
   in
   let _, length = etree_of_vclass_core venv pos length in
   let _, entry = etree_of_vclass_core venv pos entry in
   let block =
      { block_loc          = loc;
        block_depth        = depth;
        block_mutable      = true;
        block_mutable_heap = heap_empty;
        block_length       = length;
        block_length_heap  = heap_empty;
        block_default      = default;
        block_int_fields   = [];
        block_lab_fields   = SymbolTable.empty;
        block_var_fields   = ETreeTable.empty
      }
   in
      block, length, entry

(*
 * Malloc.
 *)
and fun_malloc penv venv aenv pos loc a_size =
   let pos = string_pos "fun_malloc" pos in
   let { prog_depth = depth } = penv in
   let length = venv_lookup_atom venv pos loc a_size in
   let _, length = etree_of_vclass_core venv pos length in
   let block =
      { block_loc = loc;
        block_depth = depth;
        block_mutable = true;
        block_mutable_heap = heap_empty;
        block_length = length;
        block_length_heap = heap_empty;
        block_default = default_empty;
        block_int_fields = [];
        block_lab_fields = SymbolTable.empty;
        block_var_fields = ETreeTable.empty
      }
   in
      block, length

(*
 * Control allocation.
 *)
and fun_alloc_control penv venv aenv pos loc a_size a_code =
   let pos = string_pos "fun_alloc_control" pos in
   let { prog_depth = depth } = penv in
   let length = venv_lookup_atom venv pos loc a_size in
   let _, length = etree_of_vclass_core venv pos length in
   let code = venv_lookup_atom venv pos loc a_code in
   let _, code = etree_of_vclass_core venv pos code in
   let block =
      { block_loc = loc;
        block_depth = depth;
        block_mutable = true;
        block_mutable_heap = heap_empty;
        block_length = length;
        block_length_heap = heap_empty;
        block_default = default_empty;
        block_int_fields = [];
        block_lab_fields = SymbolTable.empty;
        block_var_fields = ETreeTable.empty
      }
   in
      block, length, code

(*
 * Frame allocation.
 *)
and fun_alloc_frame penv pos loc v =
   let pos = string_pos "fun_alloc_frame" pos in
   let { prog_fenv = fenv;
         prog_depth = depth
       } = penv
   in
   let length = sizeof_frame fenv pos v in
      { block_loc = loc;
        block_depth = depth;
        block_mutable = true;
        block_mutable_heap = heap_empty;
        block_length = ETConst (AtomRawInt (Rawint.of_int precision_subscript signed_subscript length));
        block_length_heap = heap_empty;
        block_default = default_empty;
        block_int_fields = [];
        block_lab_fields = SymbolTable.empty;
        block_var_fields = ETreeTable.empty
      }

(*
 * Fetch a value from an array.
 *)
and fun_subscript_exp penv venv aenv1 pred pos loc op v1 ty v2 a3 e =
   let pos = string_pos "fun_subscript_exp" pos in
   let { prog_genv = genv;
         prog_depth = depth
       } = penv
   in
   let a2 = venv_lookup_atom venv pos loc v2 in
   let a3 = venv_lookup_atom venv pos loc a3 in
   let venv, aenv2, ic =
      match dest_vclass_core a2, dest_vclass_core a3 with
         VarAlias (_, e1, off, v2, _, _), VarExp (_, e2) ->
            alias_subscript genv venv aenv1 pos loc depth op v1 ty e1 v2 off e2
       | _ ->
            raise (FirException (pos, StringFormatError ("not an array", (fun buf -> pp_print_vclass buf a2))))
  in
  let venv = venv_add_var venv v1 ic in
  let venv = venv_add_info venv v1 ic aenv1 pred aenv2 pred in
     fun_exp penv venv aenv2 pred e

(*
 * Store a value in an array.
 *)
and fun_set_subscript_exp penv venv aenv1 pred pos loc op label v1 a2 ty a3 e =
   let pos = string_pos "fun_set_subscript_exp" pos in
   let { prog_fun = g;
         prog_depth = depth
       } = penv
   in
   let a1 = venv_lookup_atom venv pos loc v1 in
   let a2 = venv_lookup_atom venv pos loc a2 in
   let a3 = venv_lookup_atom venv pos loc a3 in
   let venv, aenv2 =
      match dest_vclass_core a1 with
         VarAlias (_, e1, off, v, s, labels) ->
            let _, e2 = etree_of_vclass_core venv pos a2 in
            let _, e3 = etree_of_vclass_core venv pos a3 in
               alias_set_subscript venv aenv1 g pos loc op label e1 e2 ty e3 v s labels off sizeof_pointer a3
       | _ ->
            let pp_print_info buf =
               Format.fprintf buf "@[<hv 3>subscript_args:@ @[<hv 3>a1 =@ %a" pp_print_vclass a1;
               Format.fprintf buf "@];@ @[<hv 3>a2 =@ %a" pp_print_vclass a2;
               Format.fprintf buf "@]@ @[<hv 3>@ %a" pp_print_vclass a3;
               Format.fprintf buf "@]@]"
            in
               raise (FirException (pos, StringFormatError ("not an array", pp_print_info)))
   in
   let venv = venv_add_info venv label (make_vclass loc (VarExp (depth, ETVar label))) aenv1 pred aenv2 pred in
      fun_exp penv venv aenv2 pred e

(*
 * Store a value in a global.
 *)
and fun_global_exp penv venv aenv1 pred pos loc op v ty l e =
   let pos = string_pos "fun_global_exp" pos in
      raise (FirException (pos, NotImplemented "fun_global_exp"))

and fun_set_global_exp penv venv aenv1 pred pos loc op label v ty a e =
   let pos = string_pos "fun_set_global_exp" pos in
      fun_exp penv venv aenv1 pred e

(*
 * Copy values from one array to another.
 *)
and fun_memcpy_exp penv venv aenv1 pred pos loc op label v1 a2 v3 a4 a5 e =
   let pos = string_pos "fun_memcpy_exp" pos in
   let a1 = venv_lookup_atom venv pos loc v1 in
   let a2 = venv_lookup_atom venv pos loc a2 in
   let a3 = venv_lookup_atom venv pos loc v3 in
   let a4 = venv_lookup_atom venv pos loc a4 in
   let a5 = venv_lookup_atom venv pos loc a5 in
   let venv, aenv2 =
      match dest_vclass_core a1, dest_vclass_core a3 with
         VarAlias (_, _, off1, v1, s1, l1), VarAlias (_, _, off3, v3, s3, _) ->
            let s1 = SymbolSet.add s1 v1 in
            let s3 = SymbolSet.add s3 v3 in
            let depth1, e1 = etree_of_vclass_core venv pos a1 in
            let depth2, e2 = etree_of_vclass_core venv pos a2 in
            let depth3, e3 = etree_of_vclass_core venv pos a3 in
            let depth4, e4 = etree_of_vclass_core venv pos a4 in
            let depth5, e5 = etree_of_vclass_core venv pos a5 in
            let depth = max_depth (max_depth (max_depth (max_depth depth1 depth2) depth3) depth4) depth5 in
               alias_memcpy venv aenv1 depth pos loc op label e1 e2 e3 e4 e5 s1 off1 l1 s3 off3
       | _ ->
            raise (FirException (pos, StringError "not an array"))
   in
   let venv, aenv2, op = fun_new_unknown penv venv aenv2 pos loc label ty_subscript in
   let venv = venv_add_info venv label op aenv1 pred aenv2 pred in
      fun_exp penv venv aenv2 pred e

(*
 * Handle an assertion.
 *)
and fun_assert_exp penv venv aenv1 pred1 pos loc label pred' e =
   let pos = string_pos "fun_assert_exp" pos in
   let { prog_depth = depth } = penv in
   let venv, aenv2, pred2 =
      match pred' with
         IsMutable v ->
            fun_assert_mutable penv venv aenv1 pred1 pos loc label v
       | Reserve (a1, a2) ->
            fun_assert_reserve penv venv aenv1 pred1 pos loc label a1 a2
       | BoundsCheck (op, v, a1, a2) ->
            fun_assert_bounds_check penv venv aenv1 pred1 pos loc label op v a1 a2
       | ElementCheck (ty, op, v, a) ->
            fun_assert_element_check penv venv aenv1 pred1 pos loc label ty op v a
   in
   let venv = venv_add_info venv label (make_vclass loc (VarExp (depth, ETVar label))) aenv1 pred1 aenv2 pred2 in
      fun_exp penv venv aenv2 pred2 e

(*
 * Mutability.
 *)
and fun_assert_mutable penv venv aenv1 pred1 pos loc label v =
   let pos = string_pos "fun_assert_mutable" pos in
   let { prog_depth = depth } = penv in
   let a = venv_lookup_atom venv pos loc v in
   let venv, aenv2 =
      match dest_vclass_core a with
         VarAlias (_, e, _, v, _, _) ->
            alias_is_mutable venv aenv1 depth pos loc label e v
       | _ ->
            raise (FirException (pos, StringFormatError ("not an array", (fun buf -> pp_print_vclass buf a))))
   in
      venv, aenv2, pred1

(*
 * Reserve some storage.
 *)
and fun_assert_reserve penv venv aenv1 pred1 pos loc label a1 a2 =
   let pos = string_pos "fun_assert_reserve" pos in
   let a1 = venv_lookup_atom venv pos loc a1 in
   let a2 = venv_lookup_atom venv pos loc a2 in
   let depth1, e1 = etree_of_vclass_core venv pos a1 in
   let depth2, e2 = etree_of_vclass_core venv pos a2 in
   let venv, aenv2 = alias_reserve venv aenv1 (max_depth depth1 depth2) pos loc label e1 e2 in
      venv, aenv2, pred1

(*
 * Perform a bounds check.
 *)
and fun_assert_bounds_check penv venv aenv1 pred1 pos loc label op v1 a2 a3 =
   let pos = string_pos "fun_assert_bounds_check" pos in
   let { prog_depth = depth } = penv in
   let a1 = venv_lookup_atom venv pos loc v1 in
   let a2 = venv_lookup_atom venv pos loc a2 in
   let a3 = venv_lookup_atom venv pos loc a3 in
   let _, e2 = etree_of_vclass_core venv pos a2 in
   let _, e3 = etree_of_vclass_core venv pos a3 in
      match dest_vclass_core a1 with
         VarAlias (_, e1, off1, v, _, _) ->
            alias_bounds_check venv aenv1 depth pos loc pred1 label op e1 off1 e2 e3 v
       | _ ->
            raise (FirException (pos, StringFormatError ("not an array", (fun buf -> pp_print_vclass buf a1))))

(*
 * Perform an element check.
 *)
and fun_assert_element_check penv venv aenv1 pred1 pos loc label ty op v a =
   let pos = string_pos "fun_assert_element_check" pos in
   let { prog_genv = genv;
         prog_depth = depth
       } = penv
   in
   let a1 = venv_lookup_atom venv pos loc v in
   let a2 = venv_lookup_atom venv pos loc a in
   let venv, aenv2 =
      match dest_vclass_core a1, dest_vclass_core a2 with
         VarAlias (_, e1, off, v2, _, _), VarExp (_, e2) ->
            alias_element_check genv venv aenv1 pos loc depth label op ty e1 v2 off e2
       | _ ->
            raise (FirException (pos, StringFormatError ("not an array", (fun buf -> pp_print_vclass buf a1))))
   in
      venv, aenv2, pred1

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and fun_tailcall_exp penv venv aenv1 pred pos loc label f args =
   let pos = string_pos "fun_tailcall_exp" pos in
   let f = var_of_fun pos f in
   let { prog_depth = depth;
         prog_fun = g
       } = penv
   in
   let finfo =
      try Some (venv_lookup_fun_exn venv f) with
         Not_found ->
            None
   in
      match finfo with
         Some finfo ->
            let args = List.map (venv_lookup_atom venv pos loc) args in
               venv_call venv depth pos g label f finfo pred aenv1 args
       | None ->
            let args = List.map (venv_lookup_atom venv pos loc) (AtomVar v_global :: args) in
            let venv, aenv2, a = fun_new_unknown penv venv aenv1 pos loc label (TyEnum 1) in
            let venv = venv_add_var venv label a in
            let aenv2, heap = alias_overwrite_args aenv2 pos label args in
            let venv = venv_add_subst venv label depth (make_htree loc (HTDepends heap)) in
            let venv = venv_add_info venv label a aenv1 pred aenv2 pred in
               venv

(*
 * For closure calls, make sure to flush the heap.
 *)
and fun_call_exp penv venv aenv pred pos loc label f args e =
   let pos = string_pos "fun_call_exp" pos in
   let f = var_of_fun pos f in
   let { prog_depth = depth;
         prog_fun = g
       } = penv
   in
   let vars =
      try Some (venv_lookup_fun_exn venv f).fun_vars with
         Not_found ->
            None
   in
   let venv =
      match vars with
         Some vars ->
            fun_call_exn penv venv aenv pred pos loc label f vars args
       | None ->
            venv
   in
      fun_exp penv venv aenv pred e

and fun_call_exn penv venv aenv1 pred pos loc label f vars args =
   let pos = string_pos "fun_call_exn" pos in
   let { prog_fun = g;
         prog_depth = depth
       } = penv
   in
   let venv, aenv2, args =
      List.fold_left2 (fun (venv, aenv, args) (v, ty) a ->
            let venv, aenv, a =
               match a with
                  Some a ->
                     venv, aenv, venv_lookup_atom venv pos loc a
                | None ->
                     fun_new_unknown penv venv aenv pos loc v ty
            in
               venv, aenv, a :: args) (venv, aenv1, []) vars args
   in
   let args = List.rev args in
      try
         let finfo = venv_lookup_fun_exn venv f in
            venv_call venv depth pos g label f finfo pred aenv2 args
      with
         Not_found ->
            venv

(*
 * The final fun analysis.  Analyze the function
 * for the current call's arguments.
 *)
let fun_call genv fenv venv changed pos f info call =
   let pos = string_pos "fun_call" pos in
   let { fun_depth = depth;
         fun_vars = vars;
         fun_body = body
       } = info
   in
   let { call_outer = outer;
         call_loc = loc
       } = call
   in
   let _ =
      if debug debug_alias then
         Format.printf "@[<hv 3>Processing function: %a@]@." pp_print_symbol f
   in
      if SymbolTable.is_empty outer then
         let _ =
            if debug debug_alias then
               Format.printf "@[<hv 3>Function %a is not ready@]@." pp_print_symbol f
         in
            changed, venv
      else
         let call, pred, venv, aenv, args = unify_call venv pos f depth vars call in
         let venv = venv_add_call venv f call in
            if call.call_changed then
               let venv =
                  List.fold_left2 (fun venv (v, _) a ->
                        venv_add_var venv v a) venv vars args
               in
               let penv =
                  { prog_genv  = genv;
                    prog_fenv  = fenv;
                    prog_fun   = f;
                    prog_depth = depth;
                    prog_live  = aenv_live_blocks venv aenv pos vars
                  }
               in
                  true, fun_exp penv venv aenv pred body
            else
               let _ =
                  if debug debug_alias then
                     Format.printf "@[<hv 3>Function %a is not changed@]@." pp_print_symbol f
               in
                  changed, venv

(*
 * The fixpoint iterates over the call set.
 *)
let fun_step genv fenv venv pos trace =
   let pos = string_pos "fun_step" pos in
      List.fold_left (fun (changed, venv) f ->
            let info =
               try
                  let info = SymbolTable.find venv.venv_funs f in
                  let call = SymbolTable.find venv.venv_calls f in
                     Some (info, call)
               with
                  Not_found ->
                     None
            in
               match info with
                  Some (info, call) ->
                     fun_call genv fenv venv changed pos f info call
                | None ->
                     changed, venv) (false, venv) trace

let fun_fixpoint genv fenv venv pos trace =
   let pos = string_pos "fun_fixpoint" pos in
   let rec fixpoint venv =
      let changed, venv = fun_step genv fenv venv pos trace in
         if changed then
            fixpoint venv
         else
            venv
   in
      fixpoint venv

(************************************************************************
 * PROGRAM ANALYSIS
 ************************************************************************)

(*
 * Build the fun element.
 *)
let build_fun_vars genv funs pos f =
   let pos = string_pos "build_fun_vars" pos in
      try
         let _, ty_vars, ty, vars, e = SymbolTable.find funs f in
         let ty_vars, _ = dest_fun_type genv pos ty in
         let vars = List.map2 (fun v ty -> v, ty) vars ty_vars in
            vars, e
      with
         Not_found ->
            raise (FirException (pos, UnboundVar f))

let build_fun genv venv loops funs pos depth f =
   let pos = string_pos "build_fun" pos in
   let vars, e = build_fun_vars genv funs pos f in
   let info =
      { fun_name = f;
        fun_vars = vars;
        fun_depth = depth;
        fun_body = e
      }
   in
   let venv = venv_add_fun venv f info in
   let loops = SymbolSet.add loops f in
      venv, loops

(*
 * When performing loop calculations,
 * collect all the functions in the loop
 * trace as well as nested loops.
 *)
let rec build_trace genv venv loops funs pos depth trace =
   match trace with
      Elem f :: trace ->
         let venv, loops = build_fun genv venv loops funs pos depth f in
            build_trace genv venv loops funs pos depth trace
    | Trace (f, trace1) :: trace2 ->
         let depth' =
            if is_zero_depth depth then
               create_depth f 1
            else
               succ_depth depth
         in
         let loops' = SymbolSet.empty in
         let venv, loops' = build_fun genv venv loops' funs pos depth' f in
         let venv, loops' = build_trace genv venv loops' funs pos depth' trace1 in
         let venv = venv_add_loop venv f loops' in
         let loops = SymbolSet.union loops loops' in
            build_trace genv venv loops funs pos depth trace2
    | [] ->
         venv, loops

(*
 * Build the program.
 * We pretend that the program is a loop,
 * with a special "root" node that calls all the
 * external functions.
 *)
let build_prog prog =
   (* Add extra calls *)
   let pos = var_exp_pos (Symbol.add "Fir_alias") in
   let prog = flow_prog prog in

   (*
    * Add a fake variable intended to represent external side-effects.
    * Esentially, we pretend that we have a string that is overwritten
    * by each external call.
    *)
   let prog =
      { prog with prog_globals = SymbolTable.add prog.prog_globals v_global (TyRawData, InitRawData (Rawint.Int8, [||])) }
   in
   let _ =
      if debug debug_alias then
         Format.printf "@[<v 3>*** FIR: alias: after cps analysis@ %a@]@." pp_print_prog prog
   in
   let cgraph = build_callgraph prog in

   (* Build the callgraph *)
   let { prog_funs = funs;
         prog_import = import;
         prog_globals = globals
       } = prog
   in
   let genv = genv_of_prog prog in
   let fenv = frame_prog prog in

   (* Initial alias environment has a single class for all pointers *)
   let root_class = SymbolSet.singleton root_alias in
   let root_loc = bogus_loc "<Fir_alias_prog.build_prog>" in
   let block = block_new_root root_loc in

   let venv = venv_empty in
   let aenv = aenv_empty root_loc in
   let aenv = aenv_add_block aenv root_alias block in

   (*
    * Add a value with a pointer type.
    * This aliases all other global vars.
    *)
   let zero_off = ETConst (AtomInt 0) in
   let build_arg genv venv aenv1 pos v ty =
      if is_pointer_type genv pos ty then
         let v_class = new_symbol_string "aclass" in
         let block = block_new_root root_loc in
         let aenv2 = aenv_add_block aenv1 v_class block in
         let aenv2 = aenv_add_alias aenv2 root_alias v_class in
         let ic = make_vclass root_loc (VarAlias (zero_depth, ETVar v, zero_off, v_class, root_class, SymbolSet.empty)) in
         let venv = venv_add_info venv v ic aenv1 pred_true aenv2 pred_true in
            venv, aenv2, ic
      else
         let ic = make_vclass root_loc (VarExp (zero_depth, ETVar v)) in
         let venv = venv_add_info venv v ic aenv1 pred_true aenv1 pred_true in
            venv, aenv1, ic
   in
   let build_args genv venv aenv pos vars ty_vars =
      let venv, aenv, args =
         List.fold_left2 (fun (venv, aenv, args) v ty ->
               let venv, aenv, arg = build_arg genv venv aenv pos v ty in
                  venv, aenv, arg :: args) (venv, aenv, []) vars ty_vars
      in
         venv, aenv, List.rev args
   in

   (* Add values all globals to the environment *)
   let { call_root = root; call_nodes = nodes } = cgraph in
   let { fnode_name = v_root; fnode_out_calls = calls } = root in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            let e = make_vclass root_loc (VarExp (zero_depth, ETVar v)) in
               venv_add_var venv v e) venv funs
   in
   let venv, aenv =
      SymbolTable.fold (fun (venv, aenv) v { import_type = ty } ->
            let pos = string_pos "build_prog" (var_exp_pos v) in
            let venv, aenv, ic = build_arg genv venv aenv pos v ty in
            let venv = venv_add_var venv v ic in
               venv, aenv) (venv, aenv) import
   in
   let venv, aenv =
      SymbolTable.fold (fun (venv, aenv) v (ty, _) ->
            let pos = string_pos "build_prog" (var_exp_pos v) in
            let venv, aenv, ic = build_arg genv venv aenv pos v ty in
            let venv = venv_add_var venv v ic in
               venv, aenv) (venv, aenv) globals
   in

   (* Add dummy functions for the root and the import functions *)
   let loc = bogus_loc "<Fir_alias_prog>" in
   let body = make_exp loc (TailCall (v_root, AtomVar v_root, [])) in
   let funs = SymbolTable.add funs v_root (loc, [], TyFun ([], TyEnum 0), [], body) in
   let venv, aenv, funs =
      SymbolTable.fold (fun (venv, aenv, funs) v import ->
            match import with
               { import_info = ImportFun _; import_type = ty } ->
                  let pos = string_pos "build_prog" (var_exp_pos v) in
                  let _, ty_vars, _ = dest_all_fun_type genv pos ty in
                  let vars = List.map (fun _ -> new_symbol_string "iarg") ty_vars in
                  let venv, aenv, args = build_args genv venv aenv pos vars ty_vars in
                  let def = loc, [], ty, vars, body in
                  let funs = SymbolTable.add funs v def in
                     venv, aenv, funs
             | _ ->
                  venv, aenv, funs) (venv, aenv, funs) import
   in

   (* Add all the calls from the root node *)
   let venv, aenv =
      SymbolTable.fold (fun (venv, aenv) _ f ->
            let pos = string_pos "build_prog" (var_exp_pos f) in
               try
                  let _, ty_vars, ty, vars, _ = SymbolTable.find funs f in
                  let ty_vars, _ = dest_fun_type genv pos ty in
                  let venv, aenv, args = build_args genv venv aenv pos vars ty_vars in
                  let call =
                     { call_pred = pred_true;
                       call_aenv = aenv;
                       call_args = args
                     }
                  in
                  let label = new_symbol_string (Symbol.to_string f ^ "_root") in
                  let info =
                     { call_loc = root_loc;
                       call_changed = true;
                       call_loop = false;
                       call_outer = SymbolTable.add SymbolTable.empty label call;
                       call_inner = SymbolTable.empty;
                       call_ps_args = SymbolTable.empty;
                       call_ps_vars = SymbolSet.empty;
                       call_loop_outer = None;
                       call_loop_inner = None;
                       call_info = None
                     }
                  in
                  let venv = venv_add_call venv f info in
                     venv, aenv
               with
                  Not_found ->
                     venv, aenv) (venv, aenv) calls
   in

   (* Compute the initial program info from the trace *)
   let trace = build_loop cgraph in
   let trace = Trace.map (fun { fnode_name = name } -> name) trace in
   let venv, _ = build_trace genv venv SymbolSet.empty funs pos zero_depth trace in

   (* Compute the loop fixpoint *)
   let trace' = Trace.to_list trace in
   let venv = fun_fixpoint genv fenv venv pos trace' in
      if debug debug_alias then
         Format.printf "@[<v 3>*** FIR: alias: after alias analysis@ %a@]@." pp_print_venv venv;
      prog, cgraph, trace, venv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
