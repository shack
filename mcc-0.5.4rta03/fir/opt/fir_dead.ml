(*
 * Fir global dead code elimination
 * Geoffrey Irving, Jason Hickey
 *)

(*
 * The code first computes the set of all live symbols using fixpoint analysis
 * across function boundaries.  Then it uses the set to perform dead code elimination.
 * Note that this removes all dead arguments to any function always called directly.
 *)

open Debug
open Symbol
open Interval_set

open Fir
open Fir_ds
open Fir_set
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_print
open Fir_state

module Pos = MakePos (struct let name = "Fir_dead" end)
open Pos

(*
 * Flags used to determine what arguments to preserve.
 *   PreserveNone: preserve only the args that are not dead
 *   PreserveFrames: preserve args that have type TyFrame
 *   PreserveAll: preserve all args
 *)
type preserve =
   PreserveNone
 | PreserveFrames
 | PreserveAll

(************************************************************************
 * LIVENESS ANALYSIS
 ************************************************************************)

(*
 * Check if an atom is live.
 *)
let rec is_live_atom live a =
   match a with
      AtomNil _
    | AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _
    | AtomConst _
    | AtomSizeof _
    | AtomLabel _ ->
         false
    | AtomVar v
    | AtomFun v
    | AtomTyUnpack v
    | AtomTyPack (v, _, _) ->
         SymbolSet.mem live v
    | AtomTyApply (a, _, _)
    | AtomUnop (_, a) ->
         is_live_atom live a
    | AtomBinop (_, a1, a2) ->
         is_live_atom live a1 || is_live_atom live a2

(*
 * Variables:
 *   live - live variable set
 *   funs - table from functions to list of arguments
 *)
let rec live_type live ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyVar _
    | TyDelayed
    | TyRawData
    | TyPointer _ ->
         live
    | TyProject (v, _) ->
         SymbolSet.add live v
    | TyFun (tl, t) ->
         live_type_list (live_type live t) tl
    | TyTuple (_, fields) ->
         live_mutable_type_list live fields
    | TyDTuple (tv, fields_opt) ->
         live_mutable_type_list_opt (SymbolSet.add live tv) fields_opt
    | TyTag (tv, fields) ->
         live_mutable_type_list (SymbolSet.add live tv) fields
    | TyFrame (tv, tl)
    | TyUnion (tv, tl, _)
    | TyApply (tv, tl) ->
         List.fold_left live_type (SymbolSet.add live tv) tl
    | TyArray t
    | TyAll (_, t)
    | TyExists (_, t)
    | TyCase t
    | TyObject (_, t) ->
         live_type live t

and live_type_list live tyl =
   List.fold_left live_type live tyl

and live_mutable_type live (ty, _) =
   live_type live ty

and live_mutable_type_list live fields =
   List.fold_left live_mutable_type live fields

and live_mutable_type_list_opt live fields =
   match fields with
      Some fields -> live_mutable_type_list live fields
    | None -> live

let live_tydef live tydef =
   match tydef with
      TyDefUnion (_, fields) ->
         List.fold_left (fun live fields ->
               List.fold_left (fun live (ty, _) ->
                     live_type live ty) live fields) live fields
    | TyDefLambda (_, ty) ->
         live_type live ty
    | TyDefDTuple _ ->
         live

(*
 * Add a var to the live set.
 * If the var is a function, then add all the function's
 * params as live vars too.
 *)
let live_var (funs : fundef SymbolTable.t) live v =
   let live = SymbolSet.add live v in
      try
         let _, _, _, vars, _ = SymbolTable.find funs v in
            SymbolSet.add_list live vars
      with
         Not_found ->
            live

let live_var_opt funs live v_opt =
   match v_opt with
      Some v -> live_var funs live v
    | None -> live

(*
 * Map a function over all the types in the unop.
 *)
let live_unop live op =
   match op with
      NotEnumOp _

      (* Negation (arithmetic and bitwise) for NAML ints *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Negation (arithmetic and bitwise) for native ints *)
    | UMinusRawIntOp _
    | NotRawIntOp _

      (*
       * RawBitFieldOp (pre, signed, offset, length):
       *    Extracts the bitfield in an integer value, which starts at bit
       *    <offset> (counting from LSB) and containing <length> bits. The
       *    bit shift and length are constant values.  To access a variable
       *    bitfield you must do a manual shift/mask; this optimization is
       *    only good for constant values.  (pre, signed) refer to the
       *    rawint precision of the result; the input should be int32.
       *)
    | RawBitFieldOp     _

      (* Unary floating-point operations *)
    | UMinusFloatOp     _
    | AbsFloatOp        _
    | SinFloatOp        _
    | CosFloatOp        _
    | TanFloatOp        _
    | ASinFloatOp       _
    | ACosFloatOp       _
    | ATanFloatOp       _
    | SinHFloatOp       _
    | CosHFloatOp       _
    | TanHFloatOp       _
    | ExpFloatOp        _
    | LogFloatOp        _
    | Log10FloatOp      _
    | SqrtFloatOp       _
    | CeilFloatOp       _
    | FloorFloatOp      _

      (*
       * Coercions:
       *    Int refers to an ML-style integer
       *    Float refers to floating-point value
       *    RawInt refers to native int, any precision
       *
       * In operators where both terms have precision qualifiers, the
       * destination precision is always specified before the source
       * precision.
       *)

      (* Coercions to int *)
    | IntOfFloatOp      _
    | IntOfRawIntOp     _

      (* Coerce to float *)
    | FloatOfIntOp      _
    | FloatOfFloatOp    _
    | FloatOfRawIntOp   _

      (* Coerce to rawint *)
    | RawIntOfIntOp     _
    | RawIntOfEnumOp    _
    | RawIntOfFloatOp   _

      (*
       * Coerce a rawint _
       * First pair is destination precision, second pair is source.
       *)
    | RawIntOfRawIntOp  _

      (*
       * Integer<->pointer coercions (only for C, not inherently safe)
       * These operators are specifically related to infix pointers...
       *)
    | RawIntOfPointerOp _
    | PointerOfRawIntOp _

      (*
       * Create an infix pointer from a block pointer.  The infix pointer
       * so that both the base and _
       * to the beginning _
       *)
    | PointerOfBlockOp _ ->
         live

      (*
       * Get the block length.
       *)
    | LengthOfBlockOp (subop, ty) ->
         live_type live ty

      (*
       * Type coercions.
       *)
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         live_mutable_type_list (SymbolSet.add live ty_var) ty_fields
    | UnionOfUnionOp (ty_var, tyl, _, _)
    | RawDataOfFrameOp (ty_var, tyl) ->
         live_type_list (SymbolSet.add live ty_var) tyl

(*
 * Map a function over all the types in the binop.
 *)
let live_binop live op =
   match op with
      (* Bitwise operations on enumerations. *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

      (* Standard binary operations on NAML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on NAML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (*
       * Standard binary operations on native ints.  The precision is
       * the result precision; the inputs should match this precision.
       *)
    | PlusRawIntOp   _
    | MinusRawIntOp  _
    | MulRawIntOp    _
    | DivRawIntOp    _
    | RemRawIntOp    _
    | SlRawIntOp     _
    | SrRawIntOp     _
    | AndRawIntOp    _
    | OrRawIntOp     _
    | XorRawIntOp    _
    | MaxRawIntOp    _
    | MinRawIntOp    _

      (*
       * RawSetBitFieldOp (pre, signed, _
       *    See comments for RawBitFieldOp. This modifies the bitfield starting
       *    at bit <_
       *       First atom is the integer containing the field.
       *       Second atom is the value to be set in the field.
       *    The resulting integer contains the revised field, with type
       *    ACRawInt (pre, signed)
       *)
    | RawSetBitFieldOp _

      (* Comparisons on native ints *)
    | EqRawIntOp     _
    | NeqRawIntOp    _
    | LtRawIntOp     _
    | LeRawIntOp     _
    | GtRawIntOp     _
    | GeRawIntOp     _
    | CmpRawIntOp    _

      (* Standard binary operations on floats *)
    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

      (* Comparisons on floats *)
    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

      (*
       * Arctangent.  This computes arctan(y/x), where y is the first atom
       * and x is the second atom given.  Handles case when x = 0 correctly.
       *)
    | ATan2FloatOp _

      (*
       * Power.  This computes x^y.
       *)
    | PowerFloatOp _

      (*
       * Float hacking.
       * This sets the exponent field _
       *)
    | LdExpFloatIntOp _

      (*
       * Pointer arithmetic. The pointer in the first argument, and the
       * returned pointer should be infix pointers (which keep the base
       * pointer as well as a pointer to anywhere within the block).
       *)
    | PlusPointerOp _ ->
         live


      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty
    | NeqEqOp ty ->
         live_type live ty

let rec live_atom funs live a =
   match a with
      AtomNil ty ->
         live_type live ty
    | AtomInt _
    | AtomEnum _
    | AtomRawInt _
    | AtomFloat _ ->
         live
    | AtomLabel ((frame, field, subfield), _) ->
         SymbolSet.add (SymbolSet.add (SymbolSet.add live frame) field) subfield
    | AtomSizeof (vars, _) ->
         List.fold_left (live_var funs) live vars
    | AtomConst (ty, v, _) ->
         live_type (live_var funs live v) ty
    | AtomVar v
    | AtomFun v
    | AtomTyUnpack v ->
         live_var funs live v
    | AtomTyApply (a, ty, tyl) ->
         let live = live_atom funs live a in
         let live = live_type live ty in
            live_type_list live tyl
    | AtomTyPack (v, ty, tyl) ->
         let live = live_var funs live v in
         let live = live_type live ty in
            live_type_list live tyl
    | AtomUnop (op, a) ->
         live_atom funs (live_unop live op) a
    | AtomBinop (op, a1, a2) ->
         live_atom funs (live_atom funs (live_binop live op) a1) a2

let live_atom_opt funs live a =
   match a with
      Some a ->
         live_atom funs live a
    | None ->
         live

let live_atoms funs args =
   List.fold_left (live_atom funs) args

let live_atom_opts funs args =
   List.fold_left (live_atom_opt funs) args

(*
 * Add live info for function arguments.
 *)
let live_args funs live pos f args =
   try
      let f = var_of_fun pos f in
      let _, _, _, vars, _ = SymbolTable.find funs f in
      let len1 = List.length vars in
      let len2 = List.length args in
         if len1 <> len2 then
            raise (FirException (pos, ArityMismatch (len1, len2)));
         List.fold_left2 (fun live v a ->
               if SymbolSet.mem live v then
                  live_atom funs live a
               else
                  live) live vars args
   with
      Not_found ->
         live_atoms funs live args

(*
 * Add live info for function arguments.
 *)
let live_opt_args funs live pos f args =
   try
      let f = var_of_fun pos f in
      let _, _, _, vars, _ = SymbolTable.find funs f in
      let len1 = List.length vars in
      let len2 = List.length args in
         if len1 <> len2 then
            raise (FirException (pos, ArityMismatch (len1, len2)));
         List.fold_left2 (fun live v a ->
               if SymbolSet.mem live v then
                  live_atom_opt funs live a
               else
                  live) live vars args
   with
      Not_found ->
         live_atom_opts funs live args

(*
 * Predicates.
 *)
let live_pred funs live pred =
   match pred with
      IsMutable v ->
         live
    | Reserve (a1, a2) ->
         live_atom funs (live_atom funs live a1) a2
    | BoundsCheck (_, v, a1, a2) ->
         if is_live_atom live v then
            live_atom funs (live_atom funs (live_atom funs live v) a1) a2
         else
            live
    | ElementCheck (ty, _, v, a) ->
         if is_live_atom live v then
            live_atom funs (live_type (live_atom funs live v) ty) a
         else
            live

(*
 * Normal allocation makes all the atoms live.
 *)
let live_alloc_op funs live op =
   match op with
      AllocTuple (_, _, ty, al)
    | AllocArray (ty, al) ->
         live_atoms funs (live_type live ty) al
    | AllocUnion (_, ty, tv, _, al) ->
         live_atoms funs (live_type (SymbolSet.add live tv) ty) al
    | AllocDTuple (ty, ty_var, a, args) ->
         live_type (SymbolSet.add (live_atom funs (live_atoms funs live args) a) ty_var) ty
    | AllocVArray (ty, _, a1, a2) ->
         live_atom funs (live_atom funs (live_type live ty) a1) a2
    | AllocMalloc (ty, a) ->
         live_atom funs (live_type live ty) a
    | AllocFrame (v, tyl) ->
         SymbolSet.add (List.fold_left live_type live tyl) v

(*
 * Expressions.
 *)
let rec live_exp funs live e =
   let pos = string_pos "live_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            let live = live_exp funs live e in
               if SymbolSet.mem live v then
                  live_atom funs (live_type live ty) a
               else
                  live
       | LetGlobal (_, v, ty, l, e) ->
            let live = live_exp funs live e in
               if SymbolSet.mem live v then
                  live_var funs (live_type live ty) l
               else
                  live
       | LetExt (v, t1, s, _, t2, ty_args, al, e) ->
            let live = live_exp funs live e in
            let live = live_atoms funs (live_type_list (live_type (live_type (live_var funs live v) t1) t2) ty_args) al in
               live
       | TailCall (_, f, args) ->
            let live = live_atom funs live f in
            let live = live_args funs live pos f args in
               live
       | SpecialCall (_, TailSysMigrate (_, aptr, aoff, f, args)) ->
            let live = live_atom funs live f in
            let live = live_atoms funs live [aptr; aoff] in
            let live = live_args funs live pos f args in
               live
       | SpecialCall (_, TailAtomic (f, c, args)) ->
            let live = live_atom funs live f in
            let live = live_atom funs live c in
            let live = live_args funs live pos f (c :: args) in

            (*
             * The first formal parameter must be added to live
             * set, since it is required by the atomic call.
             *)
            let f = var_of_fun pos f in
            let live =
               try
                  match SymbolTable.find funs f with
                     _, _, _, (reserved :: _), _ ->
                        live_var funs live reserved
                   | _, _, _, [], _ ->
                        live
               with
                  Not_found ->
                     live
            in
               live
       | SpecialCall (_, TailAtomicCommit (level, f, args)) ->
            let live = live_atom funs live f in
            let live = live_atom funs live level in
            let live = live_args funs live pos f args in
               live
       | SpecialCall (_, TailAtomicRollback (level, c)) ->
            let live = live_atoms funs live [level; c] in
               live
       | Match (a, cases) ->
            List.fold_left (fun live (_, _, e) -> live_exp funs live e) (live_atom funs live a) cases
       | MatchDTuple (a, cases) ->
            List.fold_left (fun live (_, a_opt, e) ->
                  live_exp funs (live_atom_opt funs live a_opt) e) (live_atom funs live a) cases
       | TypeCase (a1, a2, name, v, e1, e2) ->
            let live = live_atom funs live a1 in
            let live = live_atom funs live a2 in
            let live = live_exp funs live e1 in
               live_exp funs live e2
       | LetAlloc (v, op, e) ->
            let live = live_exp funs live e in
               if SymbolSet.mem live v then
                  live_alloc_op funs live op
               else
                  live
       | LetSubscript (_, v1, ty, v2, a, e) ->
            live_subscript_exp pos funs live v1 ty v2 a e
       | SetSubscript (op, _, a1, a2, t, a3, e) ->
            live_set_subscript_exp pos funs live op a1 a2 t a3 e
       | SetGlobal (_, _, v, ty, a, e) ->
            live_exp funs (live_atom funs (live_type (live_var funs live v) ty) a) e
       | Memcpy (op, _, v1, a1, v2, a2, a3, e) ->
            let live = live_atom funs live a1 in
            let live = live_atom funs live a2 in
            let live = live_atom funs live a3 in
            let live = live_atom funs live v1 in
            let live = live_atom funs live v2 in
               live_exp funs live e
       | Assert (_, pred, e) ->
            live_exp funs (live_pred funs live pred) e
       | Debug (di, e) ->
            live_exp funs live e
       | Call (_, f, args, e) ->
            let live = live_atom funs live f in
            let live = live_opt_args funs live pos f args in
               live_exp funs live e

(*
 * Subscript.
 * Be careful about frames.  If the field is live,
 * then we assume that all subfields are alive.  So
 * don't set the field, just the subfield.
 *)
and live_subscript_exp pos funs live v1 ty a2 a3 e =
   let pos = string_pos "live_subscript_exp" pos in
   let live = live_exp funs live e in
      if SymbolSet.mem live v1 then
         let live = live_atom funs live a2 in
         let live =
            (* Make only the subfield live *)
            match a3 with
               AtomLabel ((frame, field, subfield), _) ->
                  SymbolSet.add (SymbolSet.add live frame) subfield
             | _ ->
                  live_atom funs live a3
         in
         let live = live_type live ty in
            live
      else
         live

(*
 * Set a subsript.  This operation is live only if the
 * field or subfield is live.
 *)
and live_set_subscript_exp pos funs live op a1 a2 ty a3 e =
   let pos = string_pos "live_set_subscript_exp" pos in
   let live = live_exp funs live e in
   let live = live_atom funs live a1 in
      match a2 with
         AtomLabel ((frame, field, subfield), _) ->
            if SymbolSet.mem live frame && (SymbolSet.mem live field || SymbolSet.mem live subfield) then
               live_atom funs (live_atom funs (live_type live ty) a2) a3
            else
               live
       | _ ->
            live_atom funs (live_atom funs (live_type live ty) a2) a3

(*
 * Live subfields.
 *)
let live_fields live fields =
   SymbolTable.fold (fun live field subfields ->
         if SymbolSet.mem live field then
            List.fold_left (fun live (v, ty, _) ->
                  let live = SymbolSet.add live v in
                  let live = live_type live ty in
                     live) live subfields
         else
            List.fold_left (fun live (v, ty, _) ->
                  if SymbolSet.mem live v then
                     live_type live ty
                  else
                     live) live subfields) live fields

(*
 * Program live analysis.
 *)
let live_step preserve live prog =
   let { prog_funs = funs;
         prog_globals = globals;
         prog_import = import;
         prog_export = export;
         prog_names = names;
         prog_frames = frames;
         prog_types = types
       } = prog
   in

   (* Add exports *)
   let live =
      SymbolTable.fold (fun live v { export_type = ty } ->
            let live = live_var funs live v in
            let live = live_type live ty in
               live) live export
   in

   (* Add globals that are referenced within other globals *)
   let live =
      SymbolTable.fold (fun live v (_, init) ->
         if SymbolSet.mem live v then
            (* This global is live, so anything that the initialiser
               references must also be live *)
            match init with
               InitAtom a ->
                  live_atom funs live a
             | InitAlloc op ->
                  live_alloc_op funs live op
             | InitTag (ty_var, fields) ->
                  live_mutable_type_list (SymbolSet.add live ty_var) fields
             | InitRawData _ ->
                  live
             | InitNames (_, names) ->
                  List.fold_left (fun live (name, _) ->
                     live_var funs live name) live names
         else
            (* This global isn't live (yet), so ignore the initialiser *)
            live) live globals
   in

   (* Scan functions *)
   let live =
      SymbolTable.fold (fun live f def ->
            if SymbolSet.mem live f then
               let pos = string_pos "live_step" (fundef_pos f def) in
               let _, ty_vars, ty, vars, e = def in
               let ty_vars, ty_res = dest_fun_type_dir types pos ty in
               let live = live_exp funs live e in
               let live = live_type live ty_res in
               let len1 = List.length vars in
               let len2 = List.length ty_vars in
                  if len1 <> len2 then
                     raise (FirException (pos, ArityMismatch (len1, len2)));
                  if SymbolTable.mem export f then
                     List.fold_left2 (fun live v ty ->
                           let live = SymbolSet.add live v in
                              live_type live ty) live vars ty_vars
                  else
                     List.fold_left2 (fun live v ty ->
                           let live =
                              match preserve with
                                 PreserveNone ->
                                    live
                               | PreserveFrames ->
                                    if is_frame_type_dir types pos ty then
                                       SymbolSet.add live v
                                    else
                                       live
                               | PreserveAll ->
                                    SymbolSet.add live v
                           in
                              if SymbolSet.mem live v then
                                 live_type live ty
                              else
                                 live) live vars ty_vars
            else
               live) live funs
   in

   (* Scan names *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if SymbolSet.mem live v then
               live_type live ty
            else
               live) live names
   in

   (* Imports *)
   let live =
      SymbolTable.fold (fun live v { import_type = ty } ->
            if SymbolSet.mem live v then
               live_type live ty
            else
               live) live import
   in

   (* Scan types *)
   let live =
      SymbolTable.fold (fun live v tydef ->
            if SymbolSet.mem live v then
               live_tydef live tydef
            else
               live) live types
   in
   let live =
      SymbolTable.fold (fun live frame (vars, fields) ->
            if SymbolSet.mem live frame then
               live_fields live fields
            else
               live) live frames
   in
      live

(*
 * Fixpoint.
 *)
let live_prog preserve prog =
   let rec fixpoint live =
      let live' = live_step preserve live prog in
         if SymbolSet.equal live' live then
            live
         else
            fixpoint live'
   in
      fixpoint SymbolSet.empty

(************************************************************************
 * DEADCODE ELIM
 ************************************************************************)

(*
 * Filter out dead args.
 *)
let filter_vars live pos vars args =
   let pos = string_pos "filter_vars" pos in
   let len1 = List.length vars in
   let len2 = List.length args in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      let args =
         List.fold_left2 (fun args v a ->
               if SymbolSet.mem live v then
                  a :: args
               else
                  args) [] vars args
      in
         List.rev args

let filter_args funs live pos f args =
   let pos = string_pos "filter_args" pos in
      try
         let f = var_of_fun pos f in
         let _, _, _, vars, _ = SymbolTable.find funs f in
            filter_vars live pos vars args
      with
         Not_found ->
            args

(*
 * Elim dead expressions.
 *)
let rec dead_exp funs live e =
   let pos = string_pos "dead_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetAtom (v, ty, a, dead_exp funs live e))
            else
               dead_exp funs live e
       | LetExt (v, t1, s, b, t2, ty_args, args, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetExt (v, t1, s, b, t2, ty_args, args, dead_exp funs live e))
            else
               dead_exp funs live e
       | TailCall (label, f, args) ->
            let args = filter_args funs live pos f args in
               make_exp loc (TailCall (label, f, args))
       | SpecialCall (label, TailSysMigrate (ilabel, aptr, aoff, f, args)) ->
            let args = filter_args funs live pos f args in
               make_exp loc (SpecialCall (label, TailSysMigrate (ilabel, aptr, aoff, f, args)))
       | SpecialCall (label, TailAtomic (f, c, args)) ->
            let args = filter_args funs live pos f (c :: args) in
            let args =
               match args with
                  _ :: args ->
                     args
                | [] ->
                     []
            in
               make_exp loc (SpecialCall (label, TailAtomic (f, c, args)))
       | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
            let args = filter_args funs live pos f args in
               make_exp loc (SpecialCall (label, TailAtomicCommit (level, f, args)))
       | SpecialCall (label, TailAtomicRollback (level, c)) ->
            make_exp loc (SpecialCall (label, TailAtomicRollback (level, c)))
       | Call (label, f, args, e) ->
            let args = filter_args funs live pos f args in
            let e = dead_exp funs live e in
               make_exp loc (Call (label, f, args, e))
       | Match (a, cases) ->
            make_exp loc (Match (a, List.map (fun (l, s, e) -> l, s, dead_exp funs live e) cases))
       | MatchDTuple (a, cases) ->
            make_exp loc (MatchDTuple (a, List.map (fun (l, a_opt, e) -> l, a_opt, dead_exp funs live e) cases))
       | TypeCase (a1, a2, name, v, e1, e2) ->
            make_exp loc (TypeCase (a1, a2, name, v, dead_exp funs live e1, dead_exp funs live e2))
       | LetAlloc (v, op, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetAlloc (v, op, dead_exp funs live e))
            else
               dead_exp funs live e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            if SymbolSet.mem live v1 then
               make_exp loc (LetSubscript (op, v1, ty, v2, a, dead_exp funs live e))
            else
               dead_exp funs live e
       | SetSubscript (op, label, v1, a1, ty, a2, e) ->
            dead_set_subscript_exp funs live pos loc op label v1 a1 ty a2 e
       | LetGlobal (op, v, ty, l, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetGlobal (op, v, ty, l, dead_exp funs live e))
            else
               dead_exp funs live e
       | SetGlobal (op, label, v, ty, a, e) ->
            make_exp loc (SetGlobal (op, label, v, ty, a, dead_exp funs live e))
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            make_exp loc (Memcpy (op, label, v1, a1, v2, a2, a3, dead_exp funs live e))
       | Assert (label, pred, e) ->
            dead_pred_exp funs live pos loc label pred e
       | Debug (info, e) ->
            make_exp loc (Debug (info, dead_exp funs live e))

(*
 * Only keep the subscript if the field range is live.
 *)
and dead_set_subscript_exp funs live pos loc op label a1 a2 ty a3 e =
   let pos = string_pos "dead_set_subscript_exp" pos in
   let e = dead_exp funs live e in
      if is_live_atom live a1 then
         match a2 with
            AtomLabel ((_, field, subfield), _) ->
               if SymbolSet.mem live field || SymbolSet.mem live subfield then
                  make_exp loc (SetSubscript (op, label, a1, a2, ty, a3, e))
               else
                  e
          | _ ->
               make_exp loc (SetSubscript (op, label, a1, a2, ty, a3, e))
      else
         e

(*
 * Predicates.
 *)
and dead_pred_exp funs live pos loc label pred e =
   let pos = string_pos "dead_pred_exp" pos in
   let e = dead_exp funs live e in
      match pred with
         IsMutable v
       | BoundsCheck (_, v, _, _)
       | ElementCheck (_, _, v, _) ->
            if is_live_atom live v then
               make_exp loc (Assert (label, pred, e))
            else
               e
       | Reserve _ ->
            make_exp loc (Assert (label, pred, e))

(*
 * Deadcode elim for functions eliminates dead params
 * and their types.
 *)
let dead_funs (tenv : tenv) live funs =
   let dead_fun table f def =
      if SymbolSet.mem live f then
         let pos = string_pos "dead_funs" (fundef_pos f def) in
         let info, ty_vars, ty, vars, e = def in
         let ty_args, ty_res = dest_fun_type_dir tenv pos ty in
         let ty_args = filter_vars live pos vars ty_args in
         let ty_fun = TyFun (ty_args, ty_res) in
         let vars = List.filter (SymbolSet.mem live) vars in
         let e = dead_exp funs live e in
            SymbolTable.add table f (info, ty_vars, ty_fun, vars, e)
      else
         table
   in
      SymbolTable.fold dead_fun SymbolTable.empty funs

(*
 * Remove dead fields and subfields.
 *)
let dead_subfields live subfields =
   let subfields =
      List.fold_left (fun subfields subfield ->
            let v, _, _ = subfield in
               if SymbolSet.mem live v then
                  subfield :: subfields
               else
                  subfields) [] subfields
   in
      List.rev subfields

let dead_fields live fields =
   SymbolTable.fold (fun fields field subfields ->
         if SymbolSet.mem live field then
            SymbolTable.add fields field subfields
         else
            match dead_subfields live subfields with
               [] ->
                  fields
             | subfields ->
                  SymbolTable.add fields field subfields) SymbolTable.empty fields

let dead_frames live frames =
   SymbolTable.fold (fun frames frame (vars, fields) ->
         if SymbolSet.mem live frame then
            SymbolTable.add frames frame (vars, dead_fields live fields)
         else
            frames) SymbolTable.empty frames

(*
 * Generic table entry removal.
 *)
let dead_table live table =
   SymbolTable.fold (fun table v x ->
         if SymbolSet.mem live v then
            SymbolTable.add table v x
         else
            table) SymbolTable.empty table

(************************************** global function *)

let dead_prog preserve prog =
   let { prog_file = file;
         prog_import = import;
         prog_export = export;
         prog_types = types;
         prog_names = names;
         prog_globals = globals;
         prog_frames = frames;
         prog_funs = funs
       } = prog
   in
   let _ =
      if debug debug_dead then
         debug_prog "dead: before" prog
   in
   let live    = live_prog preserve prog in
   let types   = dead_table live types in
   let funs    = dead_funs types live funs in
   let import  = dead_table live import in
   let export  = dead_table live export in
   let globals = dead_table live globals in
   let names   = dead_table live names in
   let frames  = dead_frames live frames in
   let prog =
      { prog_file = file;
        prog_import = import;
        prog_export = export;
        prog_types = types;
        prog_names = names;
        prog_globals = globals;
        prog_frames = frames;
        prog_funs = funs
      }
   in
   let _ =
      if debug debug_dead then
         debug_prog "dead: after" prog
   in
      prog
