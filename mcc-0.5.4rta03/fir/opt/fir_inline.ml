(*
 * This is really simple inline expansion.  Constant folding is
 * performed by PRE.  All we do here is to inline functions
 * that are small, or are only called once.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Trace

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_loop
open Fir_type
open Fir_standardize

open Fir_loop.FirLoop

module Pos = MakePos (struct let name = "Fir_inline" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * This is the info we compute for each function.
 *)
type fun_info =
   { fun_small : bool;
     fun_cps : bool;
     fun_once : bool;
     fun_vars : var list;
     fun_body : exp
   }

(************************************************************************
 * FUNCTION SIZING
 ************************************************************************)

(*
 * Measure the "size" of a function.
 *)
let is_small_max = 25

let rec size_exp i e =
   if i >= is_small_max then
      i
   else
      match dest_exp_core e with
         LetAtom (_, _, _, e)
       | LetExt (_, _, _, _, _, _, _, e)
       | Assert (_, _, e)
       | LetSubscript (_, _, _, _, _, e)
       | SetSubscript (_, _, _, _, _, _, e)
       | LetGlobal (_, _, _, _, e)
       | SetGlobal (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, _, e) ->
            size_exp (succ i) e
       | LetAlloc (_, _, e) ->
            size_exp (i + 2) e
       | TailCall _
       | SpecialCall _ ->
            succ i
       | Match (_, cases) ->
            List.fold_left (fun i (_, _, e) ->
                  i + size_exp i e) (succ i) cases
       | MatchDTuple (_, cases) ->
            List.fold_left (fun i (_, _, e) ->
                  i + size_exp i e) (succ i) cases
       | TypeCase (_, _, _, _, e1, e2) ->
            size_exp (size_exp (i + 2) e1) e2
       | Debug (_, e)
       | Call (_, _, _, e) ->
            size_exp i e

let is_small_exp e =
   size_exp 0 e < is_small_max

(*
 * Atoms that can be inlined are atomic.
 *)
let rec is_atomic_atom a =
   match a with
      AtomInt _
    | AtomFloat _
    | AtomRawInt _
    | AtomEnum _
    | AtomVar _
    | AtomFun _
    | AtomConst _
    | AtomSizeof _
    | AtomLabel _
    | AtomNil _
    | AtomTyPack _
    | AtomTyUnpack _ ->
         true
    | AtomTyApply (a, _, _) ->
         is_atomic_atom a
    | AtomUnop _
    | AtomBinop _ ->
         false

(************************************************************************
 * COUNT VARIABLE OCCURRENCES
 ************************************************************************)

(*
 * Count number of uses of each var.
 *)
let count_empty = SymbolTable.empty

let count_lookup count v =
   try SymbolTable.find count v with
      Not_found ->
         0

let count_var count v =
   SymbolTable.filter_add count v (function
      Some i ->
         succ i
    | None ->
         1)

let count_atom count a =
   match a with
      AtomVar v -> count_var count v
    | _ -> count

let count_atom_opt count = function
   Some a -> count_atom count a
 | None -> count

let count_atoms count args =
   List.fold_left count_atom count args

let count_atom_opt_list count args =
   List.fold_left count_atom_opt count args

(*
 * Collect the number of uses of each variable.
 *)
let rec count_exp count e =
   match dest_exp_core e with
      TailCall (_, f, args) ->
         count_atoms (count_atom count f) args
    | SpecialCall (_, TailAtomicCommit (level, f, args)) ->
         count_atoms (count_atom count f) (level :: args)
    | SpecialCall (_, TailSysMigrate (_, a1, a2, f, args)) ->
         count_atoms (count_atom count f) (a1 :: a2 :: args)
    | SpecialCall (_, TailAtomic (f, a, args)) ->
         count_atoms (count_atom count f) (a :: args)
    | SpecialCall (_, TailAtomicRollback (level, a)) ->
         count_atoms count [level; a]
    | Match (a, cases) ->
         List.fold_left (fun count (_, _, e) ->
               count_exp count e) (count_atom count a) cases
    | MatchDTuple (a, cases) ->
         List.fold_left (fun count (_, a_opt, e) ->
               count_exp (count_atom_opt count a_opt) e) (count_atom count a) cases
    | TypeCase (_, _, _, _, e1, e2) ->
         count_exp (count_exp count e1) e2
    | LetAtom (_, _, a, e)
    | LetSubscript (_, _, _, _, a, e)
    | SetSubscript (_, _, _, _, _, a, e)
    | SetGlobal (_, _, _, _, a, e)
    | LetAlloc (_, AllocMalloc (_, a), e) ->
         let count = count_atom count a in
            count_exp count e
    | LetAlloc (_, AllocVArray (_, _, a1, a2), e) ->
         let count = count_atom count a1 in
         let count = count_atom count a2 in
            count_exp count e
    | LetExt (_, _, _, _, _, _, args, e)
    | LetAlloc (_, AllocTuple (_, _, _, args), e)
    | LetAlloc (_, AllocUnion (_, _, _, _, args), e)
    | LetAlloc (_, AllocArray (_, args), e) ->
         let count = count_atoms count args in
            count_exp count e
    | LetAlloc (_, AllocDTuple (_, _, a, args), e) ->
         let count = count_atom count a in
         let count = count_atoms count args in
            count_exp count e
    | LetGlobal (_, _, _, _, e)
    | LetAlloc (_, AllocFrame _, e)
    | Call (_, _, _, e)
    | Assert (_, _, e)
    | Memcpy (_, _, _, _, _, _, _, e)
    | Debug (_, e) ->
         count_exp count e

(*
 * Count all the vars in all the functions.
 *)
let count_prog prog =
   let funs = prog.prog_funs in
   let count =
      SymbolTable.fold (fun count _ (_, _, _, _, e) ->
            count_exp count e) SymbolTable.empty funs
   in

   (* Now figure out what functions are called exactly once *)
   let count =
      SymbolTable.fold (fun count v i ->
            if i = 1 && SymbolTable.mem funs v then
               SymbolSet.add count v
            else
               count) SymbolSet.empty count
   in
      count

(*
 * Now compute the function info.
 *)
let build_funs prog =
   let { prog_funs = funs } = prog in

   (* Build a trace of only the local functions *)
   let cgraph = build_callgraph prog in
   let trace = build_loop cgraph in
   let special_nodes = special_nodes trace in
   let trace =
      List.fold_left (fun trace { fnode_name = v } ->
            if SymbolTable.mem funs v then
               v :: trace
            else
               trace) [] (Trace.to_list trace)
   in
   let trace = List.rev trace in

   (* Count the number of occurrences of each function *)
   let count = count_prog prog in

   (* Collect the info for each function *)
   let nodes = cgraph.call_nodes in
   let funs =
      SymbolTable.mapi (fun f (_, _, _, vars, e) ->
            let { fnode_out_calls = ocalls } = SymbolTable.find nodes f in
               { fun_small = is_small_exp e;
                 fun_cps = SymbolTable.is_empty ocalls;
                 fun_once = SymbolSet.mem count f;
                 fun_vars = vars;
                 fun_body = e
               }) funs
   in

   (* Remove special nodes *)
   let funs =
      List.fold_left (fun funs { fnode_name = v } ->
            SymbolTable.remove funs v) funs special_nodes
   in
      funs, trace

(************************************************************************
 * SECOND PASS
 ************************************************************************)

(*
 * Function environment.
 *)
let fenv_mem = SymbolTable.mem

let fenv_lookup fenv pos v =
   try SymbolTable.find fenv v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let fenv_remove = SymbolTable.remove

(*
 * Variable environment.
 *)
let venv_empty = SymbolTable.empty

let venv_mem venv v =
   SymbolTable.mem venv v

let venv_lookup venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Add a value.
 *)
let venv_add = SymbolTable.add

let venv_ignore_var venv v =
   SymbolTable.add venv v (AtomVar v)

let venv_add_var venv v v' =
   SymbolTable.add venv v (AtomVar v')

let venv_add_args venv pos vars args =
   try List.fold_left2 SymbolTable.add venv vars args with
      Invalid_argument _ ->
         let len1 = List.length vars in
         let len2 = List.length args in
         raise (FirException (pos, ArityMismatch (len1, len2)))

(*
 * Inline a var.  The result should be a var.
 *)
let inline_var venv pos v =
   match venv_lookup venv pos v with
      AtomVar v ->
         v
    | a ->
         raise (FirException (pos, StringAtomError ("value is not a variable", a)))

(*
 * Inline a type.  We have to rewrite variables that occur
 * in a TyProject expression.
 *)
let rec inline_type venv pos ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyRawData
    | TyFrame _
    | TyVar _
    | TyDelayed
    | TyPointer _ ->
         ty
    | TyFun (args, res) ->
         TyFun (inline_type_list venv pos args, inline_type venv pos res)
    | TyUnion (v, tylist, tags) ->
         TyUnion (v, inline_type_list venv pos tylist, tags)
    | TyTuple (tclass, tylist) ->
         TyTuple (tclass, inline_mutable_type_list venv pos tylist)
    | TyDTuple (ty_var, tylist) ->
         TyDTuple (ty_var, inline_mutable_type_list_opt venv pos tylist)
    | TyTag (ty_var, tylist) ->
         TyTag (ty_var, inline_mutable_type_list venv pos tylist)
    | TyArray ty ->
         TyArray (inline_type venv pos ty)
    | TyApply (v, tylist) ->
         TyApply (v, inline_type_list venv pos tylist)
    | TyExists (vars, ty) ->
         TyExists (vars, inline_type venv pos ty)
    | TyAll (vars, ty) ->
         TyAll (vars, inline_type venv pos ty)
    | TyProject (v, i) ->
         (match venv_lookup venv pos v with
             AtomVar v ->
                TyProject (v, i)
           | a ->
                raise (FirException (pos, StringAtomError ("value is not a variable", a))))
    | TyCase ty ->
         TyCase (inline_type venv pos ty)
    | TyObject (v, ty) ->
         TyObject (v, inline_type venv pos ty)

and inline_type_list venv pos tyl =
   List.map (inline_type venv pos) tyl

and inline_mutable_type venv pos (ty, b) =
   inline_type venv pos ty, b

and inline_mutable_type_list venv pos tyl =
   List.map (inline_mutable_type venv pos) tyl

and inline_mutable_type_list_opt venv pos tyl =
   match tyl with
      Some tyl -> Some (inline_mutable_type_list venv pos tyl)
    | None -> None

(*
 * Map a function over all the types in the unop.
 *)
let inline_unop venv pos op =
   match op with
      NotEnumOp _

      (* Negation (arithmetic and bitwise) for NAML ints *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Negation (arithmetic and bitwise) for native ints *)
    | UMinusRawIntOp _
    | NotRawIntOp _

      (*
       * RawBitFieldOp (pre, signed, offset, length):
       *    Extracts the bitfield in an integer value, which starts at bit
       *    <offset> (counting from LSB) and containing <length> bits. The
       *    bit shift and length are constant values.  To access a variable
       *    bitfield you must do a manual shift/mask; this optimization is
       *    only good for constant values.  (pre, signed) refer to the
       *    rawint precision of the result; the input should be int32.
       *)
    | RawBitFieldOp     _

      (* Unary floating-point operations *)
    | UMinusFloatOp     _
    | AbsFloatOp        _
    | SinFloatOp        _
    | CosFloatOp        _
    | TanFloatOp        _
    | ASinFloatOp       _
    | ACosFloatOp       _
    | ATanFloatOp       _
    | SinHFloatOp       _
    | CosHFloatOp       _
    | TanHFloatOp       _
    | ExpFloatOp        _
    | LogFloatOp        _
    | Log10FloatOp      _
    | SqrtFloatOp       _
    | CeilFloatOp       _
    | FloorFloatOp      _

      (*
       * Coercions:
       *    Int refers to an ML-style integer
       *    Float refers to floating-point value
       *    RawInt refers to native int, any precision
       *
       * In operators where both terms have precision qualifiers, the
       * destination precision is always specified before the source
       * precision.
       *)

      (* Coercions to int *)
    | IntOfFloatOp      _
    | IntOfRawIntOp     _

      (* Coerce to float *)
    | FloatOfIntOp      _
    | FloatOfFloatOp    _
    | FloatOfRawIntOp   _

      (* Coerce to rawint *)
    | RawIntOfIntOp     _
    | RawIntOfEnumOp    _
    | RawIntOfFloatOp   _

      (*
       * Coerce a rawint _
       * First pair is destination precision, second pair is source.
       *)
    | RawIntOfRawIntOp  _

      (*
       * Integer<->pointer coercions (only for C, not inherently safe)
       * These operators are specifically related to infix pointers...
       *)
    | RawIntOfPointerOp _
    | PointerOfRawIntOp _

      (*
       * Create an infix pointer from a block pointer.  The infix pointer
       * so that both the base and _
       * to the beginning _
       *)
    | PointerOfBlockOp _ ->
         op

      (*
       * Get the block length.
       *)
    | LengthOfBlockOp (subop, ty) ->
         LengthOfBlockOp (subop, inline_type venv pos ty)

      (*
       * Type coercions.
       *)
    | DTupleOfDTupleOp (ty_var, ty_fields) ->
         DTupleOfDTupleOp (ty_var, inline_mutable_type_list venv pos ty_fields)
    | UnionOfUnionOp (ty_var, tyl, s1, s2) ->
         UnionOfUnionOp (ty_var, inline_type_list venv pos tyl, s1, s2)
    | RawDataOfFrameOp (ty_var, tyl) ->
         RawDataOfFrameOp (ty_var, inline_type_list venv pos tyl)

(*
 * Map a function over all the types in the binop.
 *)
let inline_binop venv pos op =
   match op with
      (* Bitwise operations on enumerations. *)
      AndEnumOp _
    | OrEnumOp _
    | XorEnumOp _

      (* Standard binary operations on NAML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on NAML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (*
       * Standard binary operations on native ints.  The precision is
       * the result precision; the inputs should match this precision.
       *)
    | PlusRawIntOp   _
    | MinusRawIntOp  _
    | MulRawIntOp    _
    | DivRawIntOp    _
    | RemRawIntOp    _
    | SlRawIntOp     _
    | SrRawIntOp     _
    | AndRawIntOp    _
    | OrRawIntOp     _
    | XorRawIntOp    _
    | MaxRawIntOp    _
    | MinRawIntOp    _

      (*
       * RawSetBitFieldOp (pre, signed, _
       *    See comments for RawBitFieldOp. This modifies the bitfield starting
       *    at bit <_
       *       First atom is the integer containing the field.
       *       Second atom is the value to be set in the field.
       *    The resulting integer contains the revised field, with type
       *    ACRawInt (pre, signed)
       *)
    | RawSetBitFieldOp _

      (* Comparisons on native ints *)
    | EqRawIntOp     _
    | NeqRawIntOp    _
    | LtRawIntOp     _
    | LeRawIntOp     _
    | GtRawIntOp     _
    | GeRawIntOp     _
    | CmpRawIntOp    _

      (* Standard binary operations on floats *)
    | PlusFloatOp  _
    | MinusFloatOp _
    | MulFloatOp   _
    | DivFloatOp   _
    | RemFloatOp   _
    | MaxFloatOp   _
    | MinFloatOp   _

      (* Comparisons on floats *)
    | EqFloatOp    _
    | NeqFloatOp   _
    | LtFloatOp    _
    | LeFloatOp    _
    | GtFloatOp    _
    | GeFloatOp    _
    | CmpFloatOp   _

      (*
       * Arctangent.  This computes arctan(y/x), where y is the first atom
       * and x is the second atom given.  Handles case when x = 0 correctly.
       *)
    | ATan2FloatOp _

      (*
       * Power.  This computes x^y.
       *)
    | PowerFloatOp _

      (*
       * Float hacking.
       * This sets the exponent field _
       *)
    | LdExpFloatIntOp _

      (*
       * Pointer arithmetic. The pointer in the first argument, and the
       * returned pointer should be infix pointers (which keep the base
       * pointer as well as a pointer to anywhere within the block).
       *)
    | PlusPointerOp _ ->
         op


      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty ->
         EqEqOp (inline_type venv pos ty)
    | NeqEqOp ty ->
         NeqEqOp (inline_type venv pos ty)

(*
 * Inline an atom.
 *)
let rec inline_atom venv pos a =
   let pos = string_pos "inline_atom" pos in
      match a with
         AtomNil ty ->
            AtomNil (inline_type venv pos ty)
       | AtomInt _
       | AtomEnum _
       | AtomRawInt _
       | AtomFloat _
       | AtomLabel _
       | AtomFun _ ->
            a
       | AtomConst (ty, v, i) ->
            AtomConst (inline_type venv pos ty, v, i)
       | AtomVar v ->
            venv_lookup venv pos v
       | AtomSizeof (vars, off) ->
            let vars = List.map (inline_var venv pos) vars in
               AtomSizeof (vars, off)
       | AtomTyApply (v, ty, tyl) ->
            AtomTyApply (v, inline_type venv pos ty, inline_type_list venv pos tyl)
       | AtomTyPack (v, ty, tyl) ->
            AtomTyPack (v, inline_type venv pos ty, inline_type_list venv pos tyl)
       | AtomTyUnpack v ->
            AtomTyUnpack (inline_var venv pos v)
       | AtomUnop (op, a) ->
            AtomUnop (inline_unop venv pos op, inline_atom venv pos a)
       | AtomBinop (op, a1, a2) ->
            AtomBinop (inline_binop venv pos op, inline_atom venv pos a1, inline_atom venv pos a2)

let inline_atom_opt venv pos a =
   match a with
      Some a ->
         Some (inline_atom venv pos a)
    | None ->
         None

let inline_atoms venv pos args =
   List.map (inline_atom venv pos) args

let inline_atom_opt_list venv pos args =
   List.map (inline_atom_opt venv pos) args

(*
 * Perform inlining.
 *)
let rec inline_exp venv fenv depth e =
   let pos = string_pos "inline_exp" (exp_pos e) in
   let depth = succ depth in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            inline_atom_exp venv fenv depth pos loc v ty a e
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            inline_ext_exp venv fenv depth pos loc v ty1 s b ty2 ty_args args e
       | TailCall (label, f, args) ->
            inline_tailcall_exp venv fenv depth pos loc label f args
       | SpecialCall (label, op) ->
            inline_specialcall_exp venv fenv depth pos loc label op
       | Match (a, cases) ->
            inline_match_exp venv fenv depth pos loc a cases
       | MatchDTuple (a, cases) ->
            inline_match_dtuple_exp venv fenv depth pos loc a cases
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            inline_typecase_exp venv fenv depth pos loc a1 a2 v1 v2 e1 e2
       | LetAlloc (v, op, e) ->
            inline_alloc_exp venv fenv depth pos loc v op e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            inline_subscript_exp venv fenv depth pos loc op v1 ty v2 a e
       | SetSubscript (op, label, v, a1, ty, a2, e) ->
            inline_set_subscript_exp venv fenv depth pos loc op label v a1 ty a2 e
       | LetGlobal (op, v, ty, l, e) ->
            inline_global_exp venv fenv depth pos loc op v ty l e
       | SetGlobal (op, label, v, ty, a, e) ->
            inline_set_global_exp venv fenv depth pos loc op label v ty a e
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            inline_memcpy_exp venv fenv depth pos loc op label v1 a1 v2 a2 a3 e
       | Call (label, f, args, e) ->
            inline_call_exp venv fenv depth pos loc label f args e
       | Assert (label, pred, e) ->
            inline_assert_exp venv fenv depth pos loc label pred e
       | Debug (info, e) ->
            inline_debug_exp venv fenv depth pos loc info e

(*
 * Arithmetic.
 *)
and inline_atom_exp venv fenv depth pos loc v ty a e =
   let pos = string_pos "inline_unop_exp" pos in
   let ty = inline_type venv pos ty in
   let a = inline_atom venv pos a in
      if is_atomic_atom a then
         let venv = venv_add venv v a in
         let e = inline_exp venv fenv depth e in
            e
      else
         let v' = new_symbol v in
         let venv = venv_add_var venv v v' in
         let e = inline_exp venv fenv depth e in
            make_exp loc (LetAtom (v', ty, a, e))

(*
 * External call.
 *)
and inline_ext_exp venv fenv depth pos loc v ty1 s b ty2 ty_args args e =
   let pos = string_pos "inline_ext_exp" pos in
   let args = inline_atoms venv pos args in
   let v' = new_symbol v in
   let ty1 = inline_type venv pos ty1 in
   let ty2 = inline_type venv pos ty2 in
   let ty_args = inline_type_list venv pos ty_args in
   let venv = venv_add_var venv v v' in
   let e = inline_exp venv fenv depth e in
      make_exp loc (LetExt (v', ty1, s, b, ty2, ty_args, args, e))

(*
 * Tailcall.
 *
 * A function can be inlined if:
 *    1. We are not hitting the depth bound
 *    2. Any of the following:
 *       a. The function is called only once
 *       b. The function has no out-edges, but is still small
 *)
and inline_tailcall_exp venv fenv depth pos loc label f args =
   let pos = string_pos "inline_ext_exp" pos in
   let f = var_of_fun pos f in
      if venv_mem venv f then
         let f' = inline_var venv pos f in
            if Symbol.eq f' f then
               inline_tailcall venv fenv depth pos loc label f args
            else
               inline_tailcall_exp venv fenv depth pos loc label (AtomVar f') args
      else
         inline_tailcall venv fenv depth pos loc label f args

and inline_tailcall venv fenv depth pos loc label f args =
   let pos = string_pos "inline_tailcall" pos in

   (* This is the actual inline code *)
   let args = inline_atoms venv pos args in
      if depth < is_small_max && fenv_mem fenv f then
         let info = fenv_lookup fenv pos f in
         let { fun_small = smallp;
               fun_once = oncep;
               fun_cps = cpsp;
               fun_vars = vars;
               fun_body = e
             } = info
         in
            if oncep || smallp || (cpsp && smallp) then
               let venv = venv_add_args venv pos vars args in
               let fenv =
                  if cpsp && smallp then
                     fenv
                  else
                     fenv_remove fenv f
               in
                  inline_exp venv fenv depth e
            else
               make_exp loc (TailCall (label, AtomVar f, args))
      else
         make_exp loc (TailCall (label, AtomVar f, args))

(*
 * Special calls do not get inlined.
 *)
and inline_specialcall_exp venv fenv depth pos loc label op =
   let pos = string_pos "inline_specialcall_exp" pos in
   let op =
      match op with
         TailSysMigrate (i, a1, a2, f, args) ->
            let a1 = inline_atom venv pos a1 in
            let a2 = inline_atom venv pos a2 in
            let f = inline_atom venv pos f in
            let args = inline_atoms venv pos args in
               TailSysMigrate (i, a1, a2, f, args)
       | TailAtomic (f, a, args) ->
            let f = inline_atom venv pos f in
            let a = inline_atom venv pos a in
            let args = inline_atoms venv pos args in
               TailAtomic (f, a, args)
       | TailAtomicRollback (level, a) ->
            let level = inline_atom venv pos level in
            let a = inline_atom venv pos a in
               TailAtomicRollback (level, a)
       | TailAtomicCommit (level, f, args) ->
            let level = inline_atom venv pos level in
            let f = inline_atom venv pos f in
            let args = inline_atoms venv pos args in
               TailAtomicCommit (level, f, args)
   in
      make_exp loc (SpecialCall (label, op))

(*
 * Match.
 *)
and inline_match_exp venv fenv depth pos loc a cases =
   let pos = string_pos "inline_match_exp" pos in
   let a = inline_atom venv pos a in
   let cases = List.map (fun (l, s, e) -> l, s, inline_exp venv fenv depth e) cases in
      make_exp loc (Match (a, cases))

and inline_match_dtuple_exp venv fenv depth pos loc a cases =
   let pos = string_pos "inline_match_exp" pos in
   let a = inline_atom venv pos a in
   let cases =
      List.map (fun (l, a_opt, e) ->
            l, inline_atom_opt venv pos a_opt, inline_exp venv fenv depth e) cases
   in
      make_exp loc (MatchDTuple (a, cases))

(*
 * Typecase.
 *)
and inline_typecase_exp venv fenv depth pos loc a1 a2 v1 v2 e1 e2 =
   let pos = string_pos "inline_typecase_exp" pos in
   let a1 = inline_atom venv pos a1 in
   let a2 = inline_atom venv pos a2 in
   let v1 = inline_var venv pos v1 in
   let v2' = new_symbol v2 in
   let venv = venv_add_var venv v2 v2' in
   let e1 = inline_exp venv fenv depth e1 in
   let e2 = inline_exp venv fenv depth e2 in
      make_exp loc (TypeCase (a1, a2, v1, v2', e1, e2))

(*
 * Allocation.
 *)
and inline_alloc_exp venv fenv depth pos loc v op e =
   let pos = string_pos "inline_alloc_exp" pos in
   let op =
      match op with
         AllocTuple (tclass, ty_vars, ty, args) ->
            let ty = inline_type venv pos ty in
            let args = inline_atoms venv pos args in
               AllocTuple (tclass, ty_vars, ty, args)
       | AllocDTuple (ty, ty_var, a, args) ->
            let ty = inline_type venv pos ty in
            let a = inline_atom venv pos a in
            let args = inline_atoms venv pos args in
               AllocDTuple (ty, ty_var, a, args)
       | AllocUnion (ty_vars, ty, ty_v, i, args) ->
            let ty = inline_type venv pos ty in
            let args = inline_atoms venv pos args in
               AllocUnion (ty_vars, ty, ty_v, i, args)
       | AllocArray (ty, args) ->
            let ty = inline_type venv pos ty in
            let args = inline_atoms venv pos args in
               AllocArray (ty, args)
       | AllocVArray (ty, op, a1, a2) ->
            let ty = inline_type venv pos ty in
            let a1 = inline_atom venv pos a1 in
            let a2 = inline_atom venv pos a2 in
               AllocVArray (ty, op, a1, a2)
       | AllocMalloc (ty, a) ->
            let ty = inline_type venv pos ty in
            let a = inline_atom venv pos a in
               AllocMalloc (ty, a)
       | AllocFrame _ ->
            op
   in
   let v' = new_symbol v in
   let venv = venv_add_var venv v v' in
   let e = inline_exp venv fenv depth e in
      make_exp loc (LetAlloc (v', op, e))

(*
 * Subscripts.
 *)
and inline_subscript_exp venv fenv depth pos loc op v1 ty v2 a e =
   let pos = string_pos "inline_subscript_exp" pos in
   let ty = inline_type venv pos ty in
   let a = inline_atom venv pos a in
   let v2 = inline_atom venv pos v2 in
   let v1' = new_symbol v1 in
   let venv = venv_add_var venv v1 v1' in
   let e = inline_exp venv fenv depth e in
      make_exp loc (LetSubscript (op, v1', ty, v2, a, e))

and inline_set_subscript_exp venv fenv depth pos loc op label v a1 ty a2 e =
   let pos = string_pos "inline_set_subscript_exp" pos in
   let v = inline_atom venv pos v in
   let ty = inline_type venv pos ty in
   let a1 = inline_atom venv pos a1 in
   let a2 = inline_atom venv pos a2 in
   let e = inline_exp venv fenv depth e in
      make_exp loc (SetSubscript (op, label, v, a1, ty, a2, e))

and inline_global_exp venv fenv depth pos loc op v ty l e =
   let pos = string_pos "inline_global_exp" pos in
   let ty = inline_type venv pos ty in
   let e = inline_exp venv fenv depth e in
      make_exp loc (LetGlobal (op, v, ty, l, e))

and inline_set_global_exp venv fenv depth pos loc op label v ty a e =
   let pos = string_pos "inline_set_global_exp" pos in
   let v = inline_var venv pos v in
   let ty = inline_type venv pos ty in
   let a = inline_atom venv pos a in
   let e = inline_exp venv fenv depth e in
      make_exp loc (SetGlobal (op, label, v, ty, a, e))

and inline_memcpy_exp venv fenv depth pos loc op label v1 a1 v2 a2 a3 e =
   let pos = string_pos "inline_memcpy_exp" pos in
   let v1 = inline_atom venv pos v1 in
   let a1 = inline_atom venv pos a1 in
   let v2 = inline_atom venv pos v2 in
   let a2 = inline_atom venv pos a2 in
   let a3 = inline_atom venv pos a3 in
   let e = inline_exp venv fenv depth e in
      make_exp loc (Memcpy (op, label, v1, a1, v2, a2, a3, e))

(*
 * Other call.
 *)
and inline_call_exp venv fenv depth pos loc label f args e =
   let pos = string_pos "inline_call_exp" pos in
   let f = inline_atom venv pos f in
   let args = inline_atom_opt_list venv pos args in
   let e = inline_exp venv fenv depth e in
      make_exp loc (Call (label, f, args, e))

(*
 * Assertion.
 *)
and inline_assert_exp venv fenv depth pos loc label pred e =
   let pos = string_pos "inline_assert_exp" pos in
   let pred =
      match pred with
         IsMutable v->
            IsMutable (inline_atom venv pos v)
       | Reserve (a1, a2) ->
            let a1 = inline_atom venv pos a1 in
            let a2 = inline_atom venv pos a2 in
               Reserve (a1, a2)
       | ElementCheck (ty, op, v, a) ->
            let v = inline_atom venv pos v in
            let a = inline_atom venv pos a in
               ElementCheck (ty, op, v, a)
       | BoundsCheck (op, v, a1, a2) ->
            let v = inline_atom venv pos v in
            let a1 = inline_atom venv pos a1 in
            let a2 = inline_atom venv pos a2 in
               BoundsCheck (op, v, a1, a2)
   in
   let e = inline_exp venv fenv depth e in
      make_exp loc (Assert (label, pred, e))

(*
 * Debug.
 *)
and inline_debug_exp venv fenv depth pos loc info e =
   let pos = string_pos "inline_debug_exp" pos in
   let e = inline_exp venv fenv depth e in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Program inlining.
 *)
let inline_prog prog =
   let prog = flow_prog prog in
   let { prog_funs = funs;
         prog_names = names;
         prog_import = import;
         prog_globals = globals;
         prog_frames = frames
       } = prog
   in
   let orig_funs = funs in
   let fenv, trace = build_funs prog in

   (* Add all the global vars *)
   let venv = venv_empty in
   let venv = SymbolTable.fold (fun venv f _ -> venv_ignore_var venv f) venv funs in
   let venv = SymbolTable.fold (fun venv f _ -> venv_ignore_var venv f) venv names in
   let venv = SymbolTable.fold (fun venv f _ -> venv_ignore_var venv f) venv import in
   let venv = SymbolTable.fold (fun venv f _ -> venv_ignore_var venv f) venv globals in
   let venv = SymbolTable.fold (fun venv f _ -> venv_ignore_var venv f) venv frames in
   let trace = SymbolTable.fold (fun trace f _ -> f :: trace) [] funs in
   let funs =
      List.fold_left (fun funs' f ->
            let info, ty, ty_vars, vars, e = SymbolTable.find funs f in
            let fenv = fenv_remove fenv f in
            let venv = List.fold_left venv_ignore_var venv vars in
            let e = inline_exp venv fenv 0 e in
            let def = info, ty, ty_vars, vars, e in
               SymbolTable.add funs' f def) SymbolTable.empty trace
   in

   (* Sanity check; we didn't lose anyone, did we? *)
   let _ =
      SymbolTable.iter (fun f _ ->
         if not (SymbolTable.mem funs f) then
            eprintf "warning:  FIR inlining lost a function: %s\n" (string_of_symbol f)) orig_funs
   in
   let prog = { prog with prog_funs = funs } in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
