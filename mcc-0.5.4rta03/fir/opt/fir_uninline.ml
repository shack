(*
 * Insert function calls just after branch points.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Fir
open Fir_ds
open Fir_env
open Fir_exn
open Fir_pos
open Fir_type
open Fir_standardize

open Fir_opt_util

module Pos = MakePos (struct let name = "Fir_uninline" end)
open Pos

(*
 * Liveness table.
 *)
let live_remove = SymbolTable.remove

let live_var venv pos live v =
   try SymbolTable.add live v (SymbolTable.find venv v) with
      Not_found ->
         live

let live_atom venv pos live a =
   match a with
      AtomVar v ->
         live_var venv pos live v
    | _ ->
         live

let live_atom_opt venv pos live a =
   match a with
      Some a ->
         live_atom venv pos live a
    | None ->
         live

let live_atoms venv pos live args =
   List.fold_left (live_atom venv pos) live args

let live_atom_opts venv pos live args =
   List.fold_left (live_atom_opt venv pos) live args

let live_alloc venv pos live op =
   match op with
      AllocTuple (_, _, _, args)
    | AllocArray (_, args)
    | AllocUnion (_, _, _, _, args) ->
         live_atoms venv pos live args
    | AllocDTuple (_, _, a, args) ->
         live_atoms venv pos (live_atom venv pos live a) args
    | AllocVArray (_, _, a1, a2) ->
         live_atom venv pos (live_atom venv pos live a1) a2
    | AllocMalloc (_, a) ->
         live_atom venv pos live a
    | AllocFrame _ ->
         live

let live_pred venv pos live pred =
   match pred with
      IsMutable v ->
         live_atom venv pos live v
    | Reserve (a1, a2) ->
         live_atom venv pos (live_atom venv pos live a1) a2
    | ElementCheck (_, _, v, a) ->
         live_atom venv pos (live_atom venv pos live v) a
    | BoundsCheck (_, v, a1, a2) ->
         live_atom venv pos (live_atom venv pos (live_atom venv pos live v) a1) a2

let live_debug venv pos live op =
   match op with
      DebugString _ ->
         live
    | DebugContext (_, vars) ->
         List.fold_left (fun live (v1, _, v2) ->
               live_var venv pos (live_var venv pos live v1) v2) live vars

(*
 * Break apart matches.
 *)
let rec uninline_exp venv (funs : fundef SymbolTable.t) live e =
   let pos = string_pos "uninline_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_atom venv pos live a in
            let e = LetAtom (v, ty, a, e) in
               funs, live, make_exp loc e
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_atoms venv pos live args in
            let e = LetExt (v, ty1, s, b, ty2, ty_args, args, e) in
               funs, live, make_exp loc e
       | Call (label, f, args, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_atom venv pos live f in
            let live = live_atom_opts venv pos live args in
            let e = Call (label, f, args, e) in
               funs, live, make_exp loc e
       | TailCall (label, f, args) ->
            let live = live_atom venv pos live f in
            let live = live_atoms venv pos live args in
               funs, live, e
       | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
            let live = live_atom venv pos live f in
            let live = live_atoms venv pos live (level :: args) in
               funs, live, e
       | SpecialCall (label, TailSysMigrate (_, a1, a2, f, args)) ->
            let live = live_atom venv pos live f in
            let live = live_atoms venv pos live args in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
               funs, live, e
       | SpecialCall (label, TailAtomic (f, a, args)) ->
            let live = live_atom venv pos live f in
            let live = live_atoms venv pos live args in
            let live = live_atom venv pos live a in
               funs, live, e
       | SpecialCall (label, TailAtomicRollback (level, a)) ->
            let live = live_atoms venv pos live [level; a] in
               funs, live, e
       | TypeCase (a1, a2, name, v, e1, e2) ->
            let funs, live, e1 = uninline_exp venv funs live e1 in
            let live = live_remove live v in
            let funs, live, e2 = uninline_exp venv funs live e2 in
            let live = live_var venv pos live name in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
            let e = TypeCase (a1, a2, name, v, e1, e2) in
               funs, live, make_exp loc e
       | LetAlloc (v, op, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_alloc venv pos live op in
            let e = LetAlloc (v, op, e) in
               funs, live, make_exp loc e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v1 in
            let live = live_atom venv pos live v2 in
            let live = live_atom venv pos live a in
            let e = LetSubscript (op, v1, ty, v2, a, e) in
               funs, live, make_exp loc e
       | SetSubscript (op, label, v, a1, ty, a2, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_atom venv pos live v in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
            let e = SetSubscript (op, label, v, a1, ty, a2, e) in
               funs, live, make_exp loc e
       | LetGlobal (op, v, ty, l, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let e = LetGlobal (op, v, ty, l, e) in
               funs, live, make_exp loc e
       | SetGlobal (op, label, v, ty, a, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_var venv pos live v in
            let live = live_atom venv pos live a in
            let e = SetGlobal (op, label, v, ty, a, e) in
               funs, live, make_exp loc e
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_atom venv pos live v1 in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live v2 in
            let live = live_atom venv pos live a2 in
            let live = live_atom venv pos live a3 in
            let e = Memcpy (op, label, v1, a1, v2, a2, a3, e) in
               funs, live, make_exp loc e
       | Assert (label, pred, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_pred venv pos live pred in
            let e = Assert (label, pred, e) in
               funs, live, make_exp loc e
       | Debug (info, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_debug venv pos live info in
            let e = Debug (info, e) in
               funs, live, make_exp loc e
       | Match (a, cases) ->
            let funs, live, cases =
               List.fold_left (fun (funs, live, cases) (label, set, e) ->
                     let funs, live, e = uninline_exp venv funs live e in
                     let funs, e =
                        match dest_exp_core e with
                           TailCall _ ->
                              (* Don't add more junk *)
                              funs, e
                         | _ ->
                              let ty_vars, vars =
                                 SymbolTable.fold (fun (ty_vars, vars) v ty ->
                                       ty :: ty_vars, v :: vars) ([], []) live
                              in
                              let ty_fun = TyFun (ty_vars, TyEnum 0) in
                              let f = new_symbol_string "uninline" in
                              let info = bogus_loc "<Fir_uninline>" in
                              let funs = SymbolTable.add funs f (info, [], ty_fun, vars, e) in
                              let args = List.map (fun v -> AtomVar v) vars in
                              let label = new_symbol_string "match" in
                                 funs, make_exp loc (TailCall (label, AtomFun f, args))
                     in
                     let cases = (label, set, e) :: cases in
                        funs, live, cases) (funs, live, []) cases
            in
            let cases = List.rev cases in
            let live = live_atom venv pos live a in
            let e = Match (a, cases) in
               funs, live, make_exp loc e
       | MatchDTuple (a, cases) ->
            let funs, live, cases =
               List.fold_left (fun (funs, live, cases) (label, a_opt, e) ->
                     let funs, live, e = uninline_exp venv funs live e in
                     let funs, e =
                        match dest_exp_core e with
                           TailCall _ ->
                              (* Don't add more junk *)
                              funs, e
                         | _ ->
                              let ty_vars, vars =
                                 SymbolTable.fold (fun (ty_vars, vars) v ty ->
                                       ty :: ty_vars, v :: vars) ([], []) live
                              in
                              let ty_fun = TyFun (ty_vars, TyEnum 0) in
                              let f = new_symbol_string "uninline" in
                              let info = bogus_loc "<Fir_uninline>" in
                              let funs = SymbolTable.add funs f (info, [], ty_fun, vars, e) in
                              let args = List.map (fun v -> AtomVar v) vars in
                              let label = new_symbol_string "match" in
                                 funs, make_exp loc (TailCall (label, AtomFun f, args))
                     in
                     let cases = (label, a_opt, e) :: cases in
                        funs, live, cases) (funs, live, []) cases
            in
            let cases = List.rev cases in
            let live = live_atom venv pos live a in
            let e = MatchDTuple (a, cases) in
               funs, live, make_exp loc e

(*
 * Uninline a function.
 *)
let uninline_fun venv funs f (info, ty, ty_vars, vars, e) =
   let funs, _, e = uninline_exp venv funs SymbolTable.empty e in
      SymbolTable.add funs f (info, ty, ty_vars, vars, e)

(*
 * Uninline the program.
 *)
let uninline_prog prog =
   let genv = prog_genv prog in
   let venv = venv_of_genv genv in

   (* Subtract all the globals *)
   let { prog_funs = funs;
         prog_import = import;
         prog_names = names;
         prog_globals = globals
       } = prog
   in
   let venv = SymbolTable.fold (fun venv f _ -> SymbolTable.remove venv f) venv funs in
   let venv = SymbolTable.fold (fun venv v _ -> SymbolTable.remove venv v) venv import in
   let venv = SymbolTable.fold (fun venv v _ -> SymbolTable.remove venv v) venv names in
   let venv = SymbolTable.fold (fun venv v _ -> SymbolTable.remove venv v) venv globals in

   (* Rewrite all the functions *)
   let funs = SymbolTable.fold (uninline_fun venv) funs funs in
   let prog = { prog with prog_funs = funs } in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
