(*
 * Perform optimizations on the FIR
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Debug
open Flags

open Fir
open Fir_pre
open Fir_dead
open Fir_state
open Fir_print
open Fir_infer
open Fir_inline
open Fir_extspec
open Fir_uninline

let extspec_prog prog         = Fir_state.profile "Fir_optimize.extspec_prog" extspec_prog prog
let dead_prog prog            = Fir_state.profile "Fir_optimize.dead_prog"    dead_prog PreserveNone prog
let inline_prog prog          = Fir_state.profile "Fir_optimize.inline_prog"  inline_prog prog
let pre_prog prog             = Fir_state.profile "Fir_optimize.pre_prog"     pre_prog prog

let debug_prog s prog =
   if debug_level Fir_state.debug_print_fir 2 then
      debug_prog s prog;
   prog


(*
 * Dead-code elimination.
 *)
let dead prog =
   if optimize "opt.fir.dce" then
      let prog = dead_prog prog in
         debug_prog "dead" prog
   else
      prog

let dead prog = Fir_state.profile "Fir_optimize.dead" dead prog


(*
 * Constant folding and function inlining.
 * Constant folding is also performed in pre_prog.
 *)
let inline prog =
   if optimize "opt.fir.inline" then
      let prog = inline_prog prog in
         debug_prog "inline" prog
   else
      prog

let inline prog = Fir_state.profile "Fir_optimize.inline" inline prog


(*
 * Partial-redundancy elimination.
 *)
let pre prog =
   if optimize "opt.fir.pre" then
      let prog = pre_prog prog in
         debug_prog "pre" prog
   else
      prog

let pre prog = Fir_state.profile "Fir_optimize.pre" pre prog


(*
 * External->internal function call translation.
 *)
let extspec prog =
   if optimize "opt.fir.extspec" then
      let prog = extspec_prog prog in
         debug_prog "extspec" prog
   else
      prog

let extspec prog = Fir_state.profile "Fir_optimize.extspec" extspec prog


(*
 * Main optimizer.
 *)
let optimize prog =
   let level =
      if opt_get_fir () then
         opt_get_level ()
      else
         0
   in
   let prog =
      if level < 1 then
         prog
      else
         dead (inline prog)
   in
   let prog =
      if level < 2 then
         prog
      else
         let prog = extspec prog in
            if level < 6 then
               prog
            else
               let prog = dead (pre_prog prog) in
               let prog = dead (inline prog) in
                  prog
   in
      infer_prog prog

let optimize prog = Fir_state.profile "Fir_optimize.optimize" optimize prog


(*
 * Flags environment settings
 *)
let () = std_flags_help_section_text "opt.fir"
   "Flags for enabling various FIR optimizations."

let () = std_flags_register_list_help "opt.fir"
  ["opt.fir.extspec",   FlagBool true,
                        "Enable FIR external call specialization.";
   "opt.fir.dce",       FlagBool true,
                        "Enable FIR dead code elimination.";
   "opt.fir.inline",    FlagBool true,
                        "Enable FIR inlining.";
   "opt.fir.memory",    FlagBool true,
                        "Enable FIR memory/alias analysis optimizations."]
