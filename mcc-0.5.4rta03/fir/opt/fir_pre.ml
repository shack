(*
 * Partial-redundancy elimination.
 *
 * Step 0: uninline the progrram, and compute alias info
 *    uninlining helps isolate uses
 *
 * Step 1: compute defs/uses for each function
 *    The defs are the normal defs, not including Setsubscript
 *    The uses are any uses, closed through the alias environment.
 *       In other words, the uses are _all_ the uses that are
 *       needed to compute expressions in the function.
 *
 * Step 2: perform a reverse dataflow analysis.  This is used
 *    to compute the earliest point in which an expression
 *    can be computed.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Trace
open Symbol
open Debug
open Location

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_dead
open Fir_heap
open Fir_type
open Fir_loop
open Fir_print
open Fir_alias
open Fir_state
open Fir_const
open Fir_closure
open Fir_algebra
open Fir_uninline

open Fir_alias_pre
open Fir_alias_util

open Fir_opt_util

open Fir_loop.FirLoop

open Sizeof_const

module Pos = MakePos (struct let name = "Fir_pre" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Liveness info for a function.
 *)
type live =
   { live_defs  : SymbolSet.t;
     live_uses  : SymbolSet.t;
     live_loop  : SymbolSet.t;
     live_in    : SymbolSet.t;
     live_avail : SymbolSet.t;
     live_self  : SymbolSet.t;
     live_calls : (var * SymbolSet.t) SymbolTable.t
   }

(*
 * The loop info saves induction
 * and derived variables.
 *)
type loop_var =
   { loop_base : etree;
     loop_step : etree
   }

(*
 * This is the info we keep for a loop.
 * Each loop will have two extra functions:
 *   1. A loop preheader where the induction vars are initialized
 *   2. A loop exit, where the induction variables are augmented.
 *)
type loop_info =
   { loop_loc : loc;
     loop_preheader : var;
     loop_preheader_label : label;
     loop_exit : var;
     loop_exit_label : label;
     loop_vars : loop_var SymbolTable.t;
     loop_inner : SymbolSet.t
   }

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print a set of vars.
 *)
let print_set s =
   printf "@[<b 3>";
   SymbolSet.iter (fun v ->
         printf "@ %a;" pp_print_symbol v) s;
   printf "@]"

(*
 * Print the calls.
 *)
let print_calls calls =
   SymbolTable.iter (fun label (f, s) ->
         printf "@ @[<hv 3>%a" pp_print_symbol label;
         printf ":@ %a" pp_print_symbol f;
         printf "(";
         print_set s;
         printf ")@]") calls

(*
 * Print the liveness entry.
 *)
let print_live live =
   let { live_defs  = defs;
         live_uses  = uses;
         live_in    = live_in;
         live_avail = avail;
         live_self  = self;
         live_calls = calls
       } = live
   in
      printf "@[<v 0>@[<v 3>live {@ @[<hv 3>defs = @ ";
      print_set defs;
      printf "@]@ @[<hv 3>uses =@ ";
      print_set uses;
      printf "@]@ @[<hv 3>in =@ ";
      print_set live_in;
      printf "@]@ @[<hv 3>avail =@ ";
      print_set avail;
      printf "@]@ @[<hv 3>self =@ ";
      print_set self;
      printf "@]@ @[<hv 3>calls =";
      print_calls calls;
      printf "@]@]@ }@]"

(*
 * Print the function table.
 *)
let print_funs funs =
   printf "@[<v 3>PRE function table:";
   SymbolTable.iter (fun f live ->
         printf "@ @[<hv 3>%a" pp_print_symbol f;
         printf ":@ ";
         print_live live;
         printf "@]") funs;
   printf "@]"

(*
 * Print the loop table.
 *)
let print_loop loop =
   SymbolTable.iter (fun v loop ->
         let { loop_preheader = v_header;
               loop_exit = v_exit;
               loop_vars = vars
             } = loop
         in
            printf "@ @[<hv 0>@[<hv 3>%a" pp_print_symbol v;
            printf " {@ preheader = %a" pp_print_symbol v_header;
            printf ";@ exit = %a" pp_print_symbol v_exit;
            printf ";@ @[<hv 0>@[<hv 3>vars = {";
            SymbolTable.iter (fun v info ->
                  let { loop_base = base;
                        loop_step = step
                      } = info
                  in
                     printf "@ @[<hv 3>%a" pp_print_symbol v;
                     printf ";@ @[<hv 3>base =@ %a" pp_print_etree base;
                     printf "@];@ @[<hv 3>step =@ %a" pp_print_etree step;
                     printf "@]@]") vars;
            printf "@]@ }@]@]@ }@]") loop

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Int class types.
 *)
let type_of_int_class ic =
   match ic with
      IntClass ->
         TyInt
    | RawIntClass (pre, signed) ->
         TyRawInt (pre, signed)
    | PointerClass (op, _, _) ->
         TyPointer op

(*
 * Turn the trace into a list, and remove nodes
 * we don't know about.
 *)
let clean_trace funs trace =
   let trace =
      List.fold_left (fun trace v ->
            if SymbolTable.mem funs v then
               v :: trace
            else
               trace) [] (Trace.to_list trace)
   in
      List.rev trace

(*
 * Get the expression.
 * Be careful to avoid vars that are defined as themselves.
 *)
let htree_of_aclass v1 ac =
   let loc = loc_of_aclass ac in
      match dest_aclass_core ac with
         AliasExp (_, HTETree (ETVar v2))
         when Symbol.eq v2 v1 ->
            None
       | AliasExp (_, HTDepends s)
         when SymbolSet.cardinal s = 1 && Symbol.eq (SymbolSet.choose s) v1 ->
            None
       | AliasExp (_, h) ->
            Some (make_htree loc h)
       | AliasInduction (_, v, _, _, _, _, _, _, _) ->
            Some (make_htree loc (HTETree (ETVar v)))
       | AliasAlias (_, e, _, _, _, _) ->
            Some (make_htree loc (HTETree e))

(*
 * Some aenv operations.
 *)
let aenv_lookup_label_exn aenv label =
   SymbolTable.find aenv.alias_cenv label

let aenv_lookup_label aenv pos label =
   let pos = string_pos "aenv_lookup_label" pos in
      try aenv_lookup_label_exn aenv label with
         Not_found ->
            raise (FirException (pos, UnboundVar label))

let aenv_lookup_var aenv pos v =
   let pos = string_pos "aenv_lookup_var" pos in
      try SymbolTable.find aenv.alias_aenv v with
         Not_found ->
            raise (FirException (pos, UnboundVar v))

let aenv_lookup_fun aenv pos v =
   let pos = string_pos "aenv_lookup_fun" pos in
      try SymbolTable.find aenv.alias_fenv v with
         Not_found ->
            raise (FirException (pos, UnboundVar v))

let aenv_lookup_loop_var_exn aenv v =
   SymbolTable.find aenv.alias_lenv v

let aenv_lookup_loop_var aenv pos v =
   let pos = string_pos "aenv_lookup_loop_var" pos in
      try aenv_lookup_loop_var_exn aenv v with
         Not_found ->
            raise (FirException (pos, UnboundVar v))

(*
 * Add variables in the aenv to the venv.
 *)
let aenv_genv genv pos aenv =
   let pos = string_pos "aenv_venv" pos in
   let step genv =
      SymbolTable.fold (fun (changed, genv) v1 (ac, _) ->
            if debug debug_pre then
               printf "aenv_genv: var: %a@." pp_print_symbol v1;
            if genv_mem_var genv v1 then
               changed, genv
            else
               match dest_aclass_core ac with
                  AliasExp (_, HTETree e) ->
                     (try
                         let ty = type_of_etree_core genv pos e in
                         let genv = genv_add_var genv v1 ty in
                            changed, genv
                      with
                         FirException (_, UnboundVar v) ->
                            if debug debug_pre then
                               printf "aenv_genv: unbound var: %a@." pp_print_symbol v;
                            true, genv)

                | AliasExp (_, h) ->
                     let genv = genv_add_var genv v1 (type_of_htree_core genv pos h) in
                        changed, genv

                | AliasInduction (_, v2, _, op, _, _, _, _, _) ->
                     let ty = type_of_int_class op in
                     let genv = genv_add_var genv v1 ty in
                     let genv = genv_add_var genv v2 ty in
                        changed, genv

                | AliasAlias (_, e, _, _, _, _) ->
                     let genv = genv_add_var genv v1 (type_of_etree_core genv pos e) in
                        changed, genv) (false, genv) aenv.alias_aenv
   in
   let rec fixpoint genv =
      let changed, genv = step genv in
         if changed then
            fixpoint genv
         else
            genv
   in
      fixpoint genv

(************************************************************************
 * STEP 1: LOOP PRE-HEADERS
 ************************************************************************)

(*
 * All induction variables have to be initialized before entering
 * the loop header.  In addition, derived variables are computed
 * around the loop.  POLICY: we don't carry derived induction variables
 * that have a multiplier of 1.  Just seems to waseful, since addition
 * is cheap.
 *)

(*
 * A multiplier is "trivial" if it is a power of 2.
 *)
let int_is_power_of_two i =
   let rec search k =
      (k <= i) && (k = i || search (k lsl 1))
   in
      i = 0 || search 1

let emul_atom_is_trivial a =
   match a with
      AtomInt i
    | AtomEnum (_, i) ->
         int_is_power_of_two i
    | AtomRawInt i ->
         int_is_power_of_two (Rawint.to_int i)
    | _ ->
         false

let emul_is_trivial e =
   match e with
      ETConst a ->
         emul_atom_is_trivial a
    | _ ->
         false

(*
 * Add a var to the loop table.
 *)
let loop_add_var loop g v base step =
   SymbolTable.filter_add loop g (fun info ->
         let info =
            match info with
               Some info ->
                  info
             | None ->
                  let pos = string_pos "loop_add_var" (var_exp_pos g) in
                     raise (FirException (pos, UnboundVar g))
         in
         let entry =
            { loop_base = base;
              loop_step = step
            }
         in
         let vars = SymbolTable.add info.loop_vars v entry in
            { info with loop_vars = vars })

(*
 * First, look through the aenv for induction variables.
 * Add them to the loop table, so they they can be added
 * in a loop preheader and loop exit functions.
 *
 * For derived induction variables, treat them as special
 * only if the multiplier is not a power of two.  If so,
 * make up a new name for the variable, and save it in the
 * loop header.
 *)
let loop_loc = bogus_loc "<Fir_pre>"

let loop_induction aenv loops =
   (* Add empty loop nodes *)
   let loop =
      SymbolTable.fold (fun loop g inner ->
            let s = Symbol.to_string g in
            let v_header = new_symbol_string (s ^ "_preheader") in
            let v_exit = new_symbol_string (s ^ "_exit") in
            let v_header_label = new_symbol_string (s ^ "_prelabel") in
            let v_exit_label = new_symbol_string (s ^ "_exitlabel") in
            let info =
               { loop_loc = loop_loc;
                 loop_preheader = v_header;
                 loop_preheader_label = v_header_label;
                 loop_exit = v_exit;
                 loop_exit_label = v_exit_label;
                 loop_vars = SymbolTable.empty;
                 loop_inner = inner
               }
            in
               SymbolTable.add loop g info) SymbolTable.empty loops
   in

   (* Add all the induction variables *)
   let aenv, loop =
      SymbolTable.fold (fun (aenv, loop) v (a, h) ->
            let pos = string_pos "loop_induction" (var_exp_pos v) in
            let loc = loc_of_aclass a in
               match dest_aclass_core a, h with
                  AliasInduction (depth, v, g, _, _, _, _, _, _), Some h ->
                     (try
                         let step, base = aenv_lookup_loop_var_exn aenv v in
                         let loop = loop_add_var loop g v base step in
                            aenv, loop
                      with
                         Not_found ->
                            let ac = make_aclass loc (AliasExp (depth, dest_htree_core h)) in
                            let aenv = { aenv with alias_aenv = SymbolTable.add aenv.alias_aenv v (ac, Some h) } in
                               aenv, loop)
                | AliasInduction _, None ->
                     raise (Invalid_argument "loop_induction")
                | AliasExp _, _
                | AliasAlias _, _ ->
                     aenv, loop) (aenv, loop) aenv.alias_aenv
   in
      aenv, loop

(************************************************************************
 * STEP 2: DEF/USE SETS FOR ALIAS ENVIRONMENT
 ************************************************************************)

(*
 * Walk through the alias environment and compute the one-step
 * definition step.  This means that for any variable used in
 * an etree also defines the var that is the result of the etree.
 *)
let set_add_var table v1 v2 =
   SymbolTable.filter_add table v1 (function
      Some set ->
         SymbolSet.add set v2
    | None ->
         SymbolSet.singleton v2)

let set_add_uses table v s1 =
   SymbolTable.filter_add table v (function
      Some s2 ->
         SymbolSet.union s2 s1
    | None ->
         s1)

let set_add_defs table s v1 =
   SymbolSet.fold (fun table v2 ->
         set_add_var table v2 v1) table s

(*
 * Add the one-step use definition.
 * If v is used, so are all the vars e.
 *)
let rec use_close_etree uses v e =
   match e with
      ETConst _ ->
         uses
    | ETVar v' ->
         set_add_var uses v v'
    | ETUnop (_, e) ->
         use_close_etree uses v e
    | ETBinop (_, e1, e2) ->
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
            uses

let use_close_etree_list uses v el =
   List.fold_left (fun uses e ->
         use_close_etree uses v e) uses el

let use_close_heap uses v h =
   set_add_uses uses v h

let use_close_alloc_op uses v op =
   match op with
      AllocTuple (_, _, _, el)
    | AllocUnion (_, _, _, _, el)
    | AllocArray (_, el) ->
         use_close_etree_list uses v el
    | AllocDTuple (_, _, e, el) ->
         let uses = use_close_etree uses v e in
         let uses = use_close_etree_list uses v el in
            uses
    | AllocVArray (_, _, e1, e2) ->
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
            uses
    | AllocMalloc (_, e) ->
         use_close_etree uses v e
    | AllocFrame _ ->
         uses

let use_close_pred uses v pred =
   match pred with
      IsMutable e ->
         use_close_etree uses v e
    | Reserve (e1, e2)
    | ElementCheck (_, _, e1, e2) ->
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
            uses
    | BoundsCheck (_, e1, e2, e3) ->
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
         let uses = use_close_etree uses v e3 in
            uses

let use_close_htree uses v h =
   match dest_htree_core h with
      HTETree e ->
         use_close_etree uses v e
    | HTAlloc (heap, op) ->
         let uses = use_close_heap uses v heap in
         let uses = use_close_alloc_op uses v op in
            uses
    | HTSubscript (heap, _, _, e1, e2) ->
         let uses = use_close_heap uses v heap in
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
            uses
    | HTSetSubscript (heap, _, _, e1, e2, e3) ->
         let uses = use_close_heap uses v heap in
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
         let uses = use_close_etree uses v e3 in
            uses
    | HTMemcpy (heap, _, e1, e2, e3, e4, e5) ->
         let uses = use_close_heap uses v heap in
         let uses = use_close_etree uses v e1 in
         let uses = use_close_etree uses v e2 in
         let uses = use_close_etree uses v e3 in
         let uses = use_close_etree uses v e4 in
         let uses = use_close_etree uses v e5 in
            uses
    | HTExt (heap, _, _, _, _, _, el) ->
         let uses = use_close_heap uses v heap in
         let uses = List.fold_left (fun uses e -> use_close_etree uses v e) uses el in
            uses
    | HTAssert (heap, pred) ->
         let uses = use_close_heap uses v heap in
         let uses = use_close_pred uses v pred in
            uses
    | HTDepends heap ->
         use_close_heap uses v heap

(*
 * Add the one-step def definition.
 * If e is defined, then so is v.
 *)
let rec def_close_etree defs v e =
   match e with
      ETConst _ ->
         defs
    | ETVar v' ->
         set_add_var defs v' v
    | ETUnop (_, e) ->
         def_close_etree defs v e
    | ETBinop (_, e1, e2) ->
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
            defs

let def_close_etree_list defs v el =
   List.fold_left (fun defs e ->
         def_close_etree defs v e) defs el

let def_close_heap defs v h =
   set_add_defs defs h v

let def_close_alloc_op defs v op =
   match op with
      AllocTuple (_, _, _, el)
    | AllocUnion (_, _, _, _, el)
    | AllocArray (_, el) ->
         def_close_etree_list defs v el
    | AllocDTuple (_, _, e, el) ->
         let defs = def_close_etree defs v e in
         let defs = def_close_etree_list defs v el in
            defs
    | AllocVArray (_, _, e1, e2) ->
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
            defs
    | AllocMalloc (_, e) ->
         def_close_etree defs v e
    | AllocFrame _ ->
         defs

let def_close_pred defs v pred =
   match pred with
      IsMutable e ->
         def_close_etree defs v e
    | Reserve (e1, e2)
    | ElementCheck (_, _, e1, e2) ->
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
            defs
    | BoundsCheck (_, e1, e2, e3) ->
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
         let defs = def_close_etree defs v e3 in
            defs

let def_close_htree defs v h =
   match dest_htree_core h with
      HTETree e ->
         def_close_etree defs v e
    | HTAlloc (heap, op) ->
         let defs = def_close_heap defs v heap in
         let defs = def_close_alloc_op defs v op in
            defs
    | HTSubscript (heap, _, _, e1, e2) ->
         let defs = def_close_heap defs v heap in
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
            defs
    | HTSetSubscript (heap, _, _, e1, e2, e3) ->
         let defs = def_close_heap defs v heap in
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
         let defs = def_close_etree defs v e3 in
            defs
    | HTMemcpy (heap, _, e1, e2, e3, e4, e5) ->
         let defs = def_close_heap defs v heap in
         let defs = def_close_etree defs v e1 in
         let defs = def_close_etree defs v e2 in
         let defs = def_close_etree defs v e3 in
         let defs = def_close_etree defs v e4 in
         let defs = def_close_etree defs v e5 in
            defs
    | HTExt (heap, _, _, _, _, _, el) ->
         let defs = def_close_heap defs v heap in
         let defs = List.fold_left (fun defs e -> def_close_etree defs v e) defs el in
            defs
    | HTAssert (heap, pred) ->
         let defs = def_close_heap defs v heap in
         let defs = def_close_pred defs v pred in
            defs
    | HTDepends heap ->
         def_close_heap defs v heap

(*
 * Compute the one-step defs/uses for the aenv.
 *)
let aenv_init aenv =
   SymbolTable.fold (fun (defs, uses) v (ac, _) ->
         let h = htree_of_aclass v ac in
         let defs = set_add_var defs v v in
         let uses = set_add_var uses v v in
            match h with
               Some h ->
                  let defs = def_close_htree defs v h in
                  let uses = use_close_htree uses v h in
                     defs, uses
             | None ->
                  defs, uses) (SymbolTable.empty, SymbolTable.empty) aenv.alias_aenv

(*
 * A single step of the defs closure.
 *)
let aenv_step table =
   SymbolTable.fold (fun (changed, table) v s ->
         (* Get the union of all the defs in the set *)
         let s' =
            SymbolSet.fold (fun s' v ->
                  let s'' =
                     try SymbolTable.find table v with
                        Not_found ->
                           SymbolSet.singleton v
                  in
                     SymbolSet.union s' s'') s s
         in
            (* Check if changed *)
            if SymbolSet.equal s s' then
               changed, table
            else
               let table = SymbolTable.add table v s' in
                  true, table) (false, table) table

let aenv_fixpoint aenv =
   let rec fixpoint table =
      let changed, table = aenv_step table in
         if changed then
            fixpoint table
         else
            table
   in
   let defs, uses = aenv_init aenv in
   let defs = fixpoint defs in
   let uses = fixpoint uses in
      defs, uses

(************************************************************************
 * STEP 3: LIVENESS
 ************************************************************************)

(*
 * Empty liveness.
 *)
let live_empty =
   { live_defs  = SymbolSet.empty;
     live_uses  = SymbolSet.empty;
     live_loop  = SymbolSet.empty;
     live_in    = SymbolSet.empty;
     live_avail = SymbolSet.empty;
     live_self  = SymbolSet.empty;
     live_calls = SymbolTable.empty
   }

(*
 * Add a def.
 *)
let live_def defs1 live pos v =
   let pos = string_pos "live_def" pos in
   let { live_defs = defs2 } = live in
      if SymbolSet.mem defs2 v then
         live
      else
         let defs1 =
            try SymbolTable.find defs1 v with
               Not_found ->
                  raise (FirException (pos, UnboundVar v))
         in
            { live with live_defs = SymbolSet.union defs2 defs1 }

let live_loop defs live pos v =
   let pos = string_pos "live_loop" pos in
   let { live_loop = loop } = live in
      if SymbolSet.mem loop v then
         live
      else
         let defs =
            try SymbolTable.find defs v with
               Not_found ->
                  raise (FirException (pos, UnboundVar v))
         in
            { live with live_loop = SymbolSet.union loop defs }

let live_use uses1 live pos v =
   let pos = string_pos "live_use" pos in
   let { live_uses = uses2 } = live in
      if SymbolSet.mem uses2 v then
         live
      else
         let uses1 =
            try SymbolTable.find uses1 v with
               Not_found ->
                  raise (FirException (pos, UnboundVar v))
         in
            { live with live_uses = SymbolSet.union uses2 uses1 }

(*
 * Walk through the etree.
 *)
let rec live_etree_core uses live pos e =
   let pos = string_pos "live_etree" pos in
      match e with
         ETConst _ ->
            live
       | ETVar v ->
            live_use uses live pos v
       | ETUnop (_, e) ->
            live_etree_core uses live pos e
       | ETBinop (_, e1, e2) ->
            live_etree_core uses (live_etree_core uses live pos e1) pos e2

let live_etree uses live pos e =
   live_etree_core uses live pos (dest_etree_core e)

let live_args uses live pos args =
   List.fold_left (fun live e ->
         live_etree uses live pos e) live args

let live_ps_args uses live pos args =
   SymbolTable.fold (fun live _ e ->
         live_etree uses live pos e) live args

(*
 * Liveness for an atom use.
 *)
let live_atom uses live pos a =
   match a with
      AtomVar v ->
         live_use uses live pos v
    | _ ->
         live

let live_atom_opt uses live pos a =
   match a with
      Some a ->
         live_atom uses live pos a
    | None ->
         live

let live_atoms uses live pos args =
   List.fold_left (fun live a ->
         live_atom uses live pos a) live args

let live_atom_opt_list uses live pos args =
   List.fold_left (fun live a -> live_atom_opt uses live pos a) live args

(*
 * A tailcall.
 *)
let live_call aenv uses live pos label f args =
   let pos = string_pos "live_call" pos in
   let f = var_of_fun pos f in
      try
         let args, ps_args = aenv_lookup_label_exn aenv label in
         let live = live_args uses live pos args in
         let live = live_ps_args uses live pos ps_args in
            live
      with
         Not_found ->
            let live = live_use uses live pos label in
            let live = live_use uses live pos f in
            let live = live_atoms uses live pos args in
               live

let live_call_opt aenv uses live pos label f args1 =
   let pos = string_pos "live_call_opt" pos in
   let f = var_of_fun pos f in
      try
         let args2, ps_args = aenv_lookup_label_exn aenv label in
         let live =
            List.fold_left2 (fun live arg1 arg2 ->
                  match arg1 with
                     Some _ ->
                        live_etree uses live pos arg2
                   | None ->
                        live) live args1 args2
         in
         let live = live_ps_args uses live pos ps_args in
            live
      with
         Not_found ->
            let live = live_use uses live pos label in
            let live = live_use uses live pos f in
            let live = live_atom_opt_list uses live pos args1 in
               live

(*
 * Compute liveness for an expression.
 * We can ignore almost everything.  The only things
 * that are fixed are conditionals and tailcalls.
 *)
let rec live_exp aenv defs uses live e =
   let pos = string_pos "live_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (_, _, _, e)
       | LetExt (_, _, _, _, _, _, _, e)
       | LetAlloc (_, _, e)
       | LetSubscript (_, _, _, _, _, e)
       | SetSubscript (_, _, _, _, _, _, e)
       | LetGlobal (_, _, _, _, e)
       | SetGlobal (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, _, e)
       | Assert (_, _, e)
       | Debug (_, e) ->
            live_exp aenv defs uses live e
       | TailCall (label, f, args) ->
            live_call aenv uses live pos label f args
       | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
            let live = live_atom uses live pos level in
               live_call aenv uses live pos label f args
       | SpecialCall (label, TailSysMigrate (_, a1, a2, f, args)) ->
            let live = live_atom uses live pos a1 in
            let live = live_atom uses live pos a2 in
               live_call aenv uses live pos label f args
       | SpecialCall (label, TailAtomic (f, a, args)) ->
            let live = live_atom uses live pos a in
               live_call aenv uses live pos label f args
       | SpecialCall (label, TailAtomicRollback (level, a)) ->
            live_atoms uses live pos [level; a]
       | Call (label, f, args, e) ->
            let live = live_call_opt aenv uses live pos label f args in
               live_exp aenv defs uses live e
       | Match (a, cases) ->
            let live = live_atom uses live pos a in
               List.fold_left (fun live (_, _, e) ->
                     live_exp aenv defs uses live e) live cases
       | MatchDTuple (a, cases) ->
            let live = live_atom uses live pos a in
               List.fold_left (fun live (_, a_opt, e) ->
                     live_exp aenv defs uses (live_atom_opt uses live pos a_opt) e) live cases
       | TypeCase (_, _, _, v, e1, e2) ->
            let live = live_def defs live pos v in
            let live = live_exp aenv defs uses live e1 in
               live_exp aenv defs uses live e2

(*
 * Compute liveness for a function.
 *)
let live_fun aenv loop defs uses f (_, _, _, vars, e) =
   (* Define all the parameters *)
   let pos = string_pos "live_fun" (var_exp_pos f) in
   let live = live_empty in
   let live =
      try
         let { loop_vars = lvars } = SymbolTable.find loop f in
         let live =
            SymbolTable.fold (fun live v _ ->
                  live_loop defs live pos v) live lvars
         in
         let live =
            List.fold_left (fun live v ->
                  if SymbolTable.mem lvars v then
                     live
                  else
                     live_def defs live pos v) live vars
         in
            live
      with
         Not_found ->
            List.fold_left (fun live v ->
                  live_def defs live pos v) live vars
   in
   let ps_vars = aenv_lookup_fun aenv pos f in
   let live =
      SymbolSet.fold (fun live v ->
            live_def defs live pos v) live ps_vars
   in
      live_exp aenv defs uses live e

let live_funs aenv loop defs uses funs =
   SymbolTable.mapi (live_fun aenv loop defs uses) funs

(*
 * Add the loop functions.
 *)
let live_loop defs uses funs f entry =
   let { loop_preheader = f_header;
         loop_exit = f_exit;
         loop_vars = vars
       } = entry
   in

   (* Get all the definitions *)
   let pos = string_pos "live_loop_fun" (var_exp_pos f) in
   let live = SymbolTable.find funs f in
   let live_header, live_exit, live =
      SymbolTable.fold (fun (live_header, live_exit, live) v entry ->
            let { loop_base = base;
                  loop_step = step
                } = entry
            in
            let live = live_def defs live pos v in
            let live_header = live_def defs live_header pos v in
            let live_header = live_etree uses live_header pos base in
            let live_exit = live_etree uses live_exit pos step in
               live_header, live_exit, live) (live_empty, live_empty, live) vars
   in
   let funs = SymbolTable.add funs f_header live_header in
   let funs = SymbolTable.add funs f_exit live_exit in
   let funs = SymbolTable.add funs f live in
      funs

let live_loops defs uses loop funs =
   SymbolTable.fold (live_loop defs uses) funs loop

(*
 * Clean up the liveness.
 * Don't include uses we don't know about.
 * Don't include defs that are never used.
 *)
let live_clean aenv funs export =
   (* Get all the vars in the environment *)
   let max_uses =
      SymbolTable.fold (fun s v _ ->
            SymbolSet.add s v) SymbolSet.empty aenv.alias_aenv
   in

   (* Get all the uses in the program *)
   let all_uses, funs =
      SymbolTable.fold_map (fun all_uses f live ->
            let { live_uses = uses } = live in
            let uses = SymbolSet.inter uses max_uses in
            let live = { live with live_uses = uses } in
            let all_uses = SymbolSet.union all_uses uses in
               all_uses, live) SymbolSet.empty funs
   in

   (* Remove defs that are never used, and clean up uses *)
   let funs =
      SymbolTable.mapi (fun f live ->
            let { live_defs = defs } = live in
            let defs =
               if SymbolTable.mem export f then
                  SymbolSet.union max_uses defs
               else
                  defs
            in
            let defs = SymbolSet.inter defs all_uses in
               { live with live_defs = defs }) funs
   in
      funs

(*
 * Compute liveness info for a fun.
 * Remove uses that we don't know about,
 * and remove defs that are never used.
 *)
let live_clean aenv loop defs uses funs export =
   let funs = live_loops defs uses loop funs in
      live_clean aenv funs export

(************************************************************************
 * STEP 4: UPDATE THE CALLGRAPH
 ************************************************************************)

(*
 * Add loop functions to the trace.
 *)
let rec update_trace_elem loop trace =
   match trace with
      Elem _ ->
         trace
    | Trace (f, l) ->
         let entry =
            try SymbolTable.find loop f with
               Not_found ->
                  let pos = string_pos "update_trace_elem" (var_exp_pos f) in
                     raise (FirException (pos, UnboundVar f))
         in
         let { loop_preheader = f_header;
               loop_exit = f_exit
             } = entry
         in
         let l = update_trace_list loop l in
            Trace (f_header, Elem f :: l @ [Elem f_exit])

and update_trace_list loop trace =
   List.map (update_trace_elem loop) trace

(*
 * Update the out calls.
 * For each function being called:
 *   If the function is a loop header, then:
 *      If the depth is greater than the depth of f, it is an outer call
 *      else it is an inner call.
 *)
let update_out_calls funs loop f ocalls =
   SymbolTable.fold (fun ocalls label g ->
         if SymbolTable.mem funs g then
            let g =
               try
                  let loop = SymbolTable.find loop g in
                  let { loop_preheader = g_header;
                        loop_exit = g_exit;
                        loop_inner = inner
                      } = loop
                  in
                     if SymbolSet.mem inner f then
                        g_exit
                     else
                        g_header
               with
                  Not_found ->
                     g
            in
               SymbolTable.add ocalls label g
         else
            ocalls) SymbolTable.empty ocalls

(*
 * Update the callgraph.
 * Add all the loop functions.
 * In addition, update the calls to refer to
 * the loop functions.
 *)
let update_callgraph funs loop cgraph =
   (* Don't worry about updating the in_calls, we'll do that separately *)
   let nodes =
      SymbolTable.fold (fun nodes f node ->
            if SymbolTable.mem funs f then
               let { fnode_out_calls = ocalls } = node in
               let ocalls = update_out_calls funs loop f ocalls in
               let node =
                  { node with fnode_in_calls = SymbolTable.empty;
                              fnode_out_calls = ocalls
                  }
               in
                  SymbolTable.add nodes f node
            else
               nodes) SymbolTable.empty cgraph.call_nodes
   in

   (* Add the loop nodes to the call graph *)
   let nodes =
      SymbolTable.fold (fun nodes f entry ->
            let { loop_preheader = f_header;
                  loop_preheader_label = f_header_label;
                  loop_exit = f_exit;
                  loop_exit_label = f_exit_label
                } = entry
            in
            let node_header =
               { fnode_name = f_header;
                 fnode_in_calls = SymbolTable.empty;
                 fnode_out_calls = SymbolTable.add SymbolTable.empty f_header_label f
               }
            in
            let node_exit =
               { fnode_name = f_exit;
                 fnode_in_calls = SymbolTable.empty;
                 fnode_out_calls = SymbolTable.add SymbolTable.empty f_exit_label f
               }
            in
            let nodes = SymbolTable.add nodes f_header node_header in
            let nodes = SymbolTable.add nodes f_exit node_exit in
               nodes) nodes loop
   in

   (* Reconstruct the incalls *)
   let nodes = build_incalls nodes in
      if debug debug_pre then
         begin
            eprintf "@[<v 3>*** FIR: PRE new calltable@ ";
            pp_print_calltable err_formatter nodes;
            eprintf "@]@."
         end;
      nodes

(*
 * Once loop preheaders and loop exits are added,
 * update the callgraph so each outer call branches
 * to the preheader, and each inner call banches to the
 * loop exit.
 *)
let update_trace loop funs cgraph trace =
   (* Add loop functions to the trace *)
   let trace = update_trace_list loop trace in

   (* Add loop functions to the callgraph *)
   let cgraph = update_callgraph funs loop cgraph in

   (* Flatten the trace and remove unknown functions *)
   let fset = SymbolSet.empty in
   let fset = SymbolTable.fold (fun fset f _ -> SymbolSet.add fset f) fset funs in
   let fset =
      SymbolTable.fold (fun fset _ { loop_preheader = f1; loop_exit = f2 } ->
            SymbolSet.add (SymbolSet.add fset f1) f2) fset loop
   in
   let trace =
      List.fold_left (fun trace f ->
            if SymbolSet.mem fset f then
               f :: trace
            else
               trace) [] (Trace.to_list trace)
   in
   let trace = List.rev trace in
      cgraph, trace

(************************************************************************
 * STEP 5: REVERSE DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Dataflow equations:
 *    live_in[f] = uses[f] union (isect (g in succ[f]). live_in[g])
 *)
let live_step funs nodes trace =
   List.fold_left (fun (changed, funs) f ->
         let live = SymbolTable.find funs f in
         let { live_defs = defs;
               live_uses = uses;
               live_in = live_in
             } = live
         in

         (* Intersection of all live_in sets of called functions *)
         let { fnode_out_calls = calls } = SymbolTable.find nodes f in
         let live_in' =
            SymbolTable.fold (fun live1 _ g ->
                  let { live_in = live2 } = SymbolTable.find funs g in
                     SymbolSet.union live1 live2) uses calls
         in
         let live_in' = SymbolSet.diff live_in' defs in
            (* Check if the liveness info has changed *)
            if SymbolSet.equal live_in live_in' then
               changed, funs
            else
               let live = { live with live_in = live_in' } in
               let funs = SymbolTable.add funs f live in
                  true, funs) (false, funs) trace

(*
 * Compute dataflow fixpoint.
 *)
let live_fixpoint funs cgraph trace =
   let trace = List.rev trace in
   let rec fixpoint funs =
      let changed, funs = live_step funs cgraph trace in
         if changed then
            fixpoint funs
         else
            funs
   in
      fixpoint funs

(************************************************************************
 * STEP 6: FORWARD DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Compute the "available" expressions.
 * A variable becomes available if:
 *    1. it is used in the function
 *    2. it is available on any of the incoming edges
 *)
let favail_step funs nodes trace =
   List.fold_left (fun (changed, funs) f ->
         let live = SymbolTable.find funs f in
         let { live_defs = defs;
               live_uses = uses;
               live_avail = avail;
               live_in = live_in
             } = live
         in

         (* Add defs and uses *)
         let avail' = uses in

         (* Add all available values on incoming edges *)
         let { fnode_in_calls = calls } = SymbolTable.find nodes f in
         let avail' =
            SymbolTable.fold (fun avail' _ g ->
                  let { live_avail = avail } = SymbolTable.find funs g in
                  let avail = SymbolSet.inter avail live_in in
                     SymbolSet.union avail' avail) avail' calls
         in
            (* Check if availability has changed *)
            if SymbolSet.equal avail' avail then
               changed, funs
            else
               let live = { live with live_avail = avail' } in
               let funs = SymbolTable.add funs f live in
                  true, funs) (false, funs) trace

(*
 * Compute the fixpoint.
 *)
let favail_fixpoint funs cgraph trace =
   let rec fixpoint funs =
      let changed, funs = favail_step funs cgraph trace in
         if changed then
            fixpoint funs
         else
            funs
   in
      fixpoint funs

(************************************************************************
 * STEP 7: REVERSE DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Multiset operations.
 *)
module MultiSet =
struct
   type t = int SymbolTable.t

   let empty = SymbolTable.empty

   let union m s =
      SymbolSet.fold (fun m v ->
            SymbolTable.filter_add m v (function
               Some i -> succ i
             | None -> 1)) m s

   let diff m s =
      SymbolSet.fold SymbolTable.remove m s

   let to_set m =
      SymbolTable.fold (fun s v i ->
            if i > 1 then
               SymbolSet.add s v
            else
               s) SymbolSet.empty m
end

(*
 * In the last dataflow analysis, available expressions that
 * are supposed to be available in all outgoing edges are pushed
 * back to their parents.
 *
 * { v | forall (g in succ[f]) : v in avail[g] ||
 *)
let ravail_step loop funs nodes trace =
   List.fold_left (fun (changed, funs) f ->
         let live = SymbolTable.find funs f in
         let { live_defs = defs;
               live_uses = uses;
               live_avail = avail
             } = live
         in
         let { fnode_out_calls = calls } = SymbolTable.find nodes f in

         (* First get the union of all the available requests *)
         let avail' =
            SymbolTable.fold (fun avail' _ g ->
                  let { live_avail = avail;
                        live_loop = loop;
                        live_in = live_in
                      } = SymbolTable.find funs g
                  in
                  let avail = SymbolSet.diff avail loop in
                  let avail = SymbolSet.inter avail live_in in
                     MultiSet.union avail' avail) MultiSet.empty calls
         in

         (* Next, subtract all the vars that are not available and live *)
         let avail' =
            SymbolTable.fold (fun avail' _ g ->
                  let { live_avail = avail;
                        live_loop = loop;
                        live_in = live_in
                      } = SymbolTable.find funs g
                  in
                  let avail = SymbolSet.diff avail loop in
                  let avail = SymbolSet.diff live_in avail in
                     MultiSet.diff avail' avail) avail' calls
         in
         let avail' = MultiSet.to_set avail' in
         let avail' = SymbolSet.union avail' avail in
            (* Check if availability has changed *)
            if SymbolSet.equal avail' avail then
               changed, funs
            else
               let live = { live with live_avail = avail' } in
               let funs = SymbolTable.add funs f live in
                  true, funs) (false, funs) trace

(*
 * Compute the fixpoint.
 *)
let ravail_fixpoint loop funs cgraph trace =
   let trace = List.rev trace in
   let rec fixpoint funs =
      let changed, funs = ravail_step loop funs cgraph trace in
         if changed then
            fixpoint funs
         else
            funs
   in
      fixpoint funs

(************************************************************************
 * STEP 8: VARIABLE ASSIGNMENT
 ************************************************************************)

(*
 * In the last step, assign variables to functions.
 * Here are the rules:
 *
 *    1. If a variable is in the avail set, and it is not
 *       avail in _any_ of the parents, assign it to this function.
 *
 *    2. If a variable is in the avail set, and it is
 *       avail in at least one parent, add it to the outgoing edge
 *       in all the other parents.
 *)
let assign_funs funs nodes =
   SymbolTable.fold (fun funs f _ ->
         let live = SymbolTable.find funs f in
         let { live_avail = avail;
               live_in = live_in
             } = live
         in

         (* Compute the union of all parent's avail sets *)
         let { fnode_in_calls = calls } = SymbolTable.find nodes f in
         let avail' =
            SymbolTable.fold (fun avail' _ g ->
                  let { live_avail = avail } = SymbolTable.find funs g in
                     SymbolSet.union avail' avail) SymbolSet.empty calls
         in
         let avail' = SymbolSet.inter live_in avail' in

         (* The assignment is the difference *)
         let self = SymbolSet.diff avail avail' in
         let live = { live with live_self = self } in
         let funs = SymbolTable.add funs f live in

         (* For each of the parents, add a call with any extra assignments *)
         let avail = SymbolSet.diff avail self in
         let funs =
            SymbolTable.fold (fun funs label g ->
                  let live = SymbolTable.find funs g in
                  let { live_avail = avail';
                        live_calls = calls
                      } = live
                  in
                  let avail = SymbolSet.diff avail avail' in
                  let calls = SymbolTable.add calls label (f, avail) in
                  let live = { live with live_calls = calls } in
                     SymbolTable.add funs g live) funs calls
         in
            funs) funs funs

(************************************************************************
 * STEP 9: REWRITE THE CODE
 ************************************************************************)

(*
 * Compute all the vars that haven't been defined,
 * and are supposed to be defined locally.
 *
 * Update_self_var assumes the variable is in the "self"
 * set of variables to be defined in this function.
 *)
let identity v = v

let htree_of_var aenv pos v =
   let ac, _ = aenv_lookup_var aenv pos v in
      htree_of_aclass v ac

let rec update_self_var loop genv aenv pos self v cont =
   let pos = string_pos "update_self_var" pos in
   let self = SymbolSet.remove self v in
      match htree_of_var aenv pos v with
         Some h ->
            update_htree loop genv aenv pos self v h cont
       | None ->
            cont self (AtomVar v)

(*
 * Update variable that may-or-may-not be in the self set.
 * If it is not in the self set, don't worry about it;
 * it was defined elsewhere.
 *)
and update_any_var loop genv aenv pos self v cont =
   let pos = string_pos "update_any_var" pos in
      if SymbolSet.mem self v then
         update_self_var loop genv aenv pos self v cont
      else
         cont self (AtomVar v)

(*
 * Update a variable set.
 * This version discards the results.
 *)
and update_heap loop genv aenv pos self s cont =
   let l = SymbolSet.to_list s in
   let rec update self l =
      match l with
         v :: l ->
            update_any_var loop genv aenv pos self v (fun self _ ->
                  update self l)
       | [] ->
            cont self
   in
      update self (SymbolSet.to_list s)

(*
 * Update an allocation.
 *)
and update_alloc_op loop genv aenv pos self op cont =
   let pos = string_pos "update_alloc_op" pos in
      match op with
         AllocTuple (tclass, ty, ty_vars, el) ->
            update_etree_atoms loop genv aenv pos self el (fun self el ->
                  cont self (AllocTuple (tclass, ty, ty_vars, el)))
       | AllocDTuple (ty, ty_var, e, el) ->
            update_etree_atom loop genv aenv pos self e (fun self e ->
            update_etree_atoms loop genv aenv pos self el (fun self el ->
                  cont self (AllocDTuple (ty, ty_var, e, el))))
       | AllocUnion (ty, ty_vars, v, i, el) ->
            update_etree_atoms loop genv aenv pos self el (fun self el ->
                  cont self (AllocUnion (ty, ty_vars, v, i, el)))
       | AllocArray (ty, el) ->
            update_etree_atoms loop genv aenv pos self el (fun self el ->
                  cont self (AllocArray (ty, el)))
       | AllocVArray (ty, op, e1, e2) ->
            update_etree_atom loop genv aenv pos self e1 (fun self e1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self e2 ->
                  cont self (AllocVArray (ty, op, e1, e2))))
       | AllocMalloc (ty, e) ->
            update_etree_atom loop genv aenv pos self e (fun self e ->
                  cont self (AllocMalloc (ty, e)))
       | AllocFrame (v, tyl) ->
            cont self (AllocFrame (v, tyl))

(*
 * Update a predicate.
 *)
and update_pred loop genv aenv pos self pred cont =
   let pos = string_pos "update_pred" pos in
      match pred with
         IsMutable e ->
            update_etree_atom loop genv aenv pos self e (fun self a ->
                  cont self (IsMutable a))
       | Reserve (e1, e2) ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
                  cont self (Reserve (a1, a2))))
       | ElementCheck (ty, op, e1, e2) ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
                  cont self (ElementCheck (ty, op, a1, a2))))
       | BoundsCheck (op, e1, e2, e3) ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
            update_etree_atom loop genv aenv pos self e3 (fun self a3 ->
                  cont self (BoundsCheck (op, a1, a2, a3)))))

(*
 * Update (compute) an expression.
 *)
and update_htree loop genv aenv pos self v h cont =
   let pos = string_pos "update_htree" pos in
   let loc = loc_of_htree h in
      match dest_htree_core h with
         HTETree e ->
            update_etree loop genv aenv pos loc self v e cont
       | HTAlloc (heap, op) ->
            update_heap loop genv aenv pos self heap (fun self ->
            update_alloc_op loop genv aenv pos self op (fun self op ->
                  make_exp loc (LetAlloc (v, op, cont self (AtomVar v)))))
       | HTSubscript (heap, subop, ty, e1, e2) ->
            update_heap loop genv aenv pos self heap (fun self ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
                  make_exp loc (LetSubscript (subop, v, ty, a1, a2, cont self (AtomVar v))))))
       | HTSetSubscript (heap, subop, ty, e1, e2, e3) ->
            update_heap loop genv aenv pos self heap (fun self ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
            update_etree_atom loop genv aenv pos self e3 (fun self a3 ->
                  let label = new_symbol_string "set" in
                     make_exp loc (SetSubscript (subop, label, a1, a2, ty, a3, cont self (AtomEnum (1, 0))))))))
       | HTMemcpy (heap, subop, e1, e2, e3, e4, e5) ->
            update_heap loop genv aenv pos self heap (fun self ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
            update_etree_atom loop genv aenv pos self e3 (fun self a3 ->
            update_etree_atom loop genv aenv pos self e4 (fun self a4 ->
            update_etree_atom loop genv aenv pos self e5 (fun self a5 ->
                  let label = new_symbol_string "memcpy" in
                     make_exp loc (Memcpy (subop, label, a1, a2, a3, a4, a5, cont self (AtomEnum (1, 0))))))))))
       | HTExt (heap, ty1, s, b, ty2, ty_args, el) ->
            update_heap loop genv aenv pos self heap (fun self ->
            update_etree_atoms loop genv aenv pos self el (fun self args ->
                  make_exp loc (LetExt (v, ty1, s, b, ty2, ty_args, args, cont self (AtomVar v)))))
       | HTAssert (heap, pred) ->
            update_heap loop genv aenv pos self heap (fun self ->
            update_pred loop genv aenv pos self pred (fun self pred ->
                  let label = new_symbol_string "assert" in
                     make_exp loc (Assert (label, pred, cont self (AtomEnum (1, 0))))))
       | HTDepends heap ->
            update_heap loop genv aenv pos self heap (fun self ->
                  cont self (AtomEnum (1, 0)))

(*
 * Compute an expression.
 * We don't have an intermediate variable, so this atom should
 * be either a constant or a variable.
 *)
and update_etree_atom loop genv aenv pos self e cont =
   let pos = string_pos "update_etree_atom" pos in
      match e with
         ETConst a ->
            cont self a
       | ETVar v ->
            update_any_var loop genv aenv pos self v cont
       | ETUnop _
       | ETBinop _ ->
            raise (FirException (pos, StringFormatError ("illegal etree", (fun buf -> pp_print_etree_core buf e))))

and update_etree_atoms loop genv aenv pos self el cont =
   let rec update self args' args =
      match args with
         e :: args ->
            update_etree_atom loop genv aenv pos self e (fun self a ->
                  update self (a :: args') args)
       | [] ->
            cont self (List.rev args')
   in
      update self [] el

(*
 * Compute an expression.
 *)
and update_etree loop genv aenv pos loc self v e cont =
   let pos = string_pos "update_etree" pos in
   let ty = type_of_etree_core genv pos e in
      match e with
         ETConst a ->
            cont self a
       | ETVar v' ->
            update_any_var loop genv aenv pos self v' (fun self a ->
                  make_exp loc (LetAtom (loop v, ty, a, cont self a)))
       | ETUnop (op, e1) ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
                  make_exp loc (LetAtom (v, ty, AtomUnop (op, a1), cont self (AtomVar v))))
       | ETBinop (op, e1, e2) ->
            update_etree_atom loop genv aenv pos self e1 (fun self a1 ->
            update_etree_atom loop genv aenv pos self e2 (fun self a2 ->
                  make_exp loc (LetAtom (v, ty, AtomBinop (op, a1, a2), cont self (AtomVar v)))))

(*
 * Flush all the pending variables.
 *)
let rec update_flush loop genv aenv pos self cont =
   let pos = string_pos "update_flush" pos in
      if SymbolSet.is_empty self then
         cont self
      else
         let v = SymbolSet.choose self in
            update_self_var loop genv aenv pos self v (fun self _ ->
                  update_flush loop genv aenv pos self cont)

(*
 * Translate an atom to its internal form.
 * This means: look it up, and use the translation if
 * the translation is a constant, var, or expr.
 *)
let update_var aenv pos v cont =
   let pos = string_pos "update_var" pos in
   let ac, _ = aenv_lookup_var aenv pos v in
   let h = htree_of_aclass v ac in
      match h with
         Some h ->
            (match dest_htree_core h with
                HTETree (ETConst a) ->
                   cont a
              | HTETree (ETVar v) ->
                   cont (AtomVar v)
              | _ ->
                   cont (AtomVar v))
       | _ ->
            cont (AtomVar v)

let update_fun_var aenv pos v cont =
   let pos = string_pos "update_fun_var" pos in
      try
         let ac, _ = SymbolTable.find aenv.alias_aenv v in
         let h = htree_of_aclass v ac in
            match h with
               Some h ->
                  (match dest_htree_core h with
                      HTETree (ETVar v) ->
                         cont v
                    | _ ->
                         cont v)
             | _ ->
                  cont v
      with
         Not_found ->
            cont v

(*
 * Atom versions.
 *)
let update_atom aenv pos a cont =
   let pos = string_pos "update_atom" pos in
      match a with
         AtomVar v ->
            update_var aenv pos v cont
       | _ ->
            cont a

let update_atom_opt aenv pos a cont =
   let pos = string_pos "update_atom_opt" pos in
      match a with
         Some a ->
            update_atom aenv pos a (fun a ->
                  cont (Some a))
       | None ->
            cont None

let update_atoms aenv pos args cont =
   let pos = string_pos "update_atoms" pos in
   let rec update args' args =
      match args with
         a :: args ->
            update_atom aenv pos a (fun a ->
                  update (a :: args') args)
       | [] ->
            cont (List.rev args')
   in
      update [] args

let update_atom_opt_list aenv pos args cont =
   let pos = string_pos "update_atom_opt_list" pos in
   let rec update args' args =
      match args with
         a :: args ->
            update_atom_opt aenv pos a (fun a ->
                  update (a :: args') args)
       | [] ->
            cont (List.rev args')
   in
      update [] args

let update_cases aenv pos cases cont =
   let pos = string_pos "update_cases" pos in
   let rec update cases' cases =
      match cases with
         (l, Some a, e) :: cases ->
            update_atom aenv pos a (fun a ->
                  update ((l, Some a, e) :: cases') cases)
       | (_, None, _) as case :: cases ->
            update (case :: cases') cases
       | [] ->
            cont (List.rev cases')
   in
      update [] cases

(*
 * Update a function call.
 * We flush all symbols in the call.
 *)
let update_call genv aenv fenv pos calls label f args cont =
   let pos = string_pos "update_call" pos in
   let f = fenv (var_of_fun pos f) in
   let s =
      try snd (SymbolTable.find calls f) with
         Not_found ->
            SymbolSet.empty
   in
   let args' =
      try
         let args, _ = aenv_lookup_label_exn aenv label in
            Some (List.map dest_etree_core args)
      with
         Not_found ->
            None
   in
      update_flush identity genv aenv pos s (fun _ ->
      update_fun_var aenv pos f (fun f ->
            match args' with
               Some args ->
                  update_etree_atoms identity genv aenv pos SymbolSet.empty args (fun _ args -> cont f args)
             | None ->
                  update_atoms aenv pos args (fun args -> cont f args)))

let update_call_opt genv aenv fenv pos calls label f args1 cont =
   let pos = string_pos "update_call_opt" pos in
   let f = fenv (var_of_fun pos f) in
   let s =
      try snd (SymbolTable.find calls f) with
         Not_found ->
            SymbolSet.empty
   in
   let args' =
      try
         let args, ps_args = aenv_lookup_label_exn aenv label in
            Some args
      with
         Not_found ->
            None
   in
      update_flush identity genv aenv pos s (fun _ ->
            match args' with
               Some args2 ->
                  let rec update args' args1' args2' =
                     match args1', args2' with
                        arg1 :: args1', arg2 :: args2' ->
                           (match arg1 with
                               Some _ ->
                                  update_etree_atom identity genv aenv pos SymbolSet.empty (dest_etree_core arg2) (fun _ arg2 ->
                                        update (Some arg2 :: args') args1' args2')
                             | None ->
                                  update (None :: args') args1' args2')
                      | [], [] ->
                           cont f (List.rev args')
                      | _ ->
                           raise (FirException (pos, ArityMismatch (List.length args1, List.length args2)))
                  in
                     update [] args1 args2
             | None ->
                  update_atom_opt_list aenv pos args1 (fun args1 -> cont f args1))

(*
 * Walk through the function and add the expressions as late as
 * possible.  Again, we can skip most of the expressions.
 *)
let rec update_exp genv aenv fenv calls e =
   let pos = string_pos "update_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (_, _, _, e)
       | LetExt (_, _, _, _, _, _, _, e)
       | LetAlloc (_, _, e)
       | LetSubscript (_, _, _, _, _, e)
       | SetSubscript (_, _, _, _, _, _, e)
       | LetGlobal (_, _, _, _, e)
       | SetGlobal (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, _, e)
       | Assert (_, _, e)
       | Debug (_, e) ->
            update_exp genv aenv fenv calls e
       | TailCall (label, f, args) ->
            update_call genv aenv fenv pos calls label f args (fun f args ->
                  make_exp loc (TailCall (label, AtomVar f, args)))
       | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
            update_atom aenv pos level (fun level ->
            update_call genv aenv fenv pos calls label f args (fun f args ->
                  make_exp loc (SpecialCall (label, TailAtomicCommit (level, AtomVar f, args)))))
       | SpecialCall (label, TailSysMigrate (i, a1, a2, f, args)) ->
            update_call genv aenv fenv pos calls label f args (fun f args ->
            update_atom aenv pos a1 (fun a1 ->
            update_atom aenv pos a2 (fun a2 ->
                  make_exp loc (SpecialCall (label, TailSysMigrate (i, a1, a2, AtomVar f, args))))))
       | SpecialCall (label, TailAtomic (f, a, args)) ->
            update_call genv aenv fenv pos calls label f args (fun f args ->
            update_atom aenv pos a (fun a ->
                  make_exp loc (SpecialCall (label, TailAtomic (AtomVar f, a, args)))))
       | SpecialCall (label, TailAtomicRollback (level, a)) ->
            update_atom aenv pos level (fun level ->
            update_atom aenv pos a (fun a ->
                  make_exp loc (SpecialCall (label, TailAtomicRollback (level, a)))))
       | Call (label, f, args, e) ->
            update_call_opt genv aenv fenv pos calls label f args (fun f args ->
                  make_exp loc (Call (label, AtomVar f, args, update_exp genv aenv fenv calls e)))
       | Match (a, cases) ->
            update_atom aenv pos a (fun a ->
                  let cases = List.map (fun (l, s, e) -> l, s, update_exp genv aenv fenv calls e) cases in
                     make_exp loc (Match (a, cases)))
       | MatchDTuple (a, cases) ->
            update_atom aenv pos a (fun a ->
            update_cases aenv pos cases (fun cases ->
                  let cases = List.map (fun (l, a_opt, e) -> l, a_opt, update_exp genv aenv fenv calls e) cases in
                     make_exp loc (MatchDTuple (a, cases))))
       | TypeCase (a1, a2, v3, v4, e1, e2) ->
            update_atom aenv pos a1 (fun a1 ->
            update_atom aenv pos a2 (fun a2 ->
            update_var aenv pos v3 (fun a3 ->
                  let e1 = update_exp genv aenv fenv calls e1 in
                  let e2 = update_exp genv aenv fenv calls e2 in
                  let v3 = var_of_atom pos a3 in
                     make_exp loc (TypeCase (a1, a2, v3, v4, e1, e2)))))

(*
 * Update the expression from the base values.
 *)
let update_loop_var etree_of_loop lookup genv aenv live pos loc vars body =
   let pos = string_pos "update_loop_var" pos in
   let body =
      List.fold_left (fun e (v, v', loop) ->
            let base = etree_of_loop loop in
            let ty = type_of_etree_core genv pos base in
               make_exp loc (LetAtom (v, ty, AtomVar v', e))) body vars
   in
   let rec collect vars =
      match vars with
         (v, v', loop) :: vars ->
            let base = etree_of_loop loop in
            let ty = type_of_etree_core genv pos base in
               update_etree_atom lookup genv aenv pos SymbolSet.empty base (fun _ a ->
                     let e = collect vars in
                        make_exp loc (LetAtom (v', ty, a, e)))
       | [] ->
            body
   in
      collect vars

(*
 * Create the loop header.
 *)
let create_loop_vars etree_of_loop genv aenv loc live label g f ivars args =
   let pos = string_pos "create_loop_vars" (var_exp_pos g) in
   let live = SymbolTable.find live g in
   let { live_self = self;
         live_calls = calls
       } = live
   in
   let self =
      try
         let _, s = SymbolTable.find calls label in
            SymbolSet.union self s
      with
         Not_found ->
            self
   in
   let ivars = List.map (fun (v, loop) -> v, new_symbol_string "tmp", loop) ivars in
   let lookup v1 =
      let rec search ivars =
         match ivars with
            (v2, v3, _) :: ivars ->
               if Symbol.eq v1 v2 then
                  v3
               else
                  search ivars
          | [] ->
               v1
      in
         search ivars
   in
   let body = make_exp loc (TailCall (label, AtomVar f, args)) in
   let body = update_loop_var etree_of_loop lookup genv aenv live pos loc ivars body in
      update_flush lookup genv aenv pos self (fun _ -> body)

let create_loop_header = create_loop_vars (fun { loop_base = base } -> dest_etree_core base)
let create_loop_exit   = create_loop_vars (fun { loop_step = step } -> dest_etree_core step)

(*
 * Add the loop functions.
 *)
let create_loop_funs genv aenv live loop funs =
   SymbolTable.fold (fun funs f entry ->
         let { loop_loc = loc;
               loop_preheader = f_header;
               loop_preheader_label = f_header_label;
               loop_exit = f_exit;
               loop_exit_label = f_exit_label;
               loop_vars = lvars
             } = entry
         in
         let info, ty, ty_vars, vars, _ = SymbolTable.find funs f in
         let args = List.map (fun v -> AtomVar v) vars in
         let lvars =
            SymbolTable.fold (fun lvars v info ->
                  (v, info) :: lvars) [] lvars
         in
         let e_header = create_loop_header genv aenv loc live f_header_label f_header f lvars args in
         let e_exit = create_loop_exit genv aenv loc live f_exit_label f_exit f lvars args in
         let funs = SymbolTable.add funs f_header (info, ty, ty_vars, vars, e_header) in
         let funs = SymbolTable.add funs f_exit (info, ty, ty_vars, vars, e_exit) in
            funs) funs loop

(*
 * Add all extra definitions to the function bodies.
 * We try to postpone them as much as possible (be "lazy"),
 * this will help limit the live ranges of the variables.
 *
 * If a branch point if reached, all the vars are flushed.
 *)
let update_fun genv aenv live loop f (gflag, ty, ty_vars, vars, e) =
   (* Function call renaming *)
   let pos = string_pos "update_fun" (var_exp_pos f) in
   let fenv g =
      try
         let entry = SymbolTable.find loop g in
            if SymbolSet.mem entry.loop_inner f then
               entry.loop_exit
            else
               entry.loop_preheader
      with
         Not_found ->
            g
   in

   (* Get liveness info *)
   let live = SymbolTable.find live f in
   let { live_self = self;
         live_calls = calls
       } = live
   in

   (* Rewrite the function body *)
   let e =
      update_flush identity genv aenv pos self (fun self ->
            update_exp genv aenv fenv calls e)
   in
      gflag, ty, ty_vars, vars, e

let update_funs genv aenv live loop funs =
   let funs = SymbolTable.mapi (update_fun genv aenv live loop) funs in
      create_loop_funs genv aenv live loop funs

(************************************************************************
 * MAIN FUNCTION
 ************************************************************************)

let debug_info s info =
   if debug debug_pre then
      begin
         printf "@[<v 3>*** FIR: PRE: %s@ " s;
         print_funs info;
         printf "@]@."
      end

let debug_defs s defs =
   if debug debug_pre then
      begin
         printf "@[<v 3>*** FIR: PRE: closure: %s" s;
         SymbolTable.iter (fun v s ->
               printf "@ @[<b 3>%a" pp_print_symbol v;
               printf " =";
               SymbolSet.iter (fun v ->
                     printf "@ %a" pp_print_symbol v) s;
               printf "@]") defs;
         printf "@]@."
      end

let debug_loop s loop =
   if debug debug_pre then
      begin
         printf "@[<v 3>*** FIR: PRE: loop: %s" s;
         print_loop loop;
         printf "@]@."
      end

(*
 * Main function.
 *)
let pre_prog prog =
   (* Uninline and perform alias analysis *)
   let prog = uninline_prog prog in
   let prog, cgraph, trace, aenv = alias_prog prog in
   let { prog_funs = funs;
         prog_export = export
       } = prog
   in
   let pos = string_pos "pre_prog" (var_exp_pos (new_symbol_string "Fir_pre")) in
   let genv = prog_genv prog in
   let genv = aenv_genv genv pos aenv in

   (* Step 1: collect induction vars *)
   let loops = trace_loops trace in
   let aenv, loop = loop_induction aenv loops in

   (* Step 2: defs/uses *)
   let defs, uses = aenv_fixpoint aenv in
   let _ = debug_defs "defs" defs in
   let _ = debug_defs "uses" uses in
   let _ = debug_loop "loop" loop in

   (* Step 3: collect function info *)
   let info = live_funs aenv loop defs uses funs in
   let _ = debug_info "Live funs" info in
   let info = live_clean aenv loop defs uses info export in
   let _ = debug_info "Live clean" info in

   (* Step 4: update the callgraph and trace *)
   let cgraph, trace = update_trace loop funs cgraph trace in

   (* Step 5: liveness analysis *)
   let info = live_fixpoint info cgraph trace in
   let _ = debug_info "Live fixpoint" info in

   (* Step 6: latest-point analysis *)
   let info = favail_fixpoint info cgraph trace in
   let _ = debug_info "FAvail fixpoint" info in

   (* Step 7: optimal-point analysis *)
   let info = ravail_fixpoint loop info cgraph trace in
   let _ = debug_info "RAvail fixpoint" info in

   (* Step 8: assign expressions to functions *)
   let info = assign_funs info cgraph in
   let _ = debug_info "Assign funs" info in
   let _ =
      if debug debug_pre then
         debug_prog "PRE: before rewrite" prog
   in

   (* Step 9: rewrite the code *)
   let funs = update_funs genv aenv info loop funs in
   let prog = { prog with prog_funs = funs } in
   let _ =
      if debug debug_pre then
         debug_prog "PRE: rewrite" prog
   in

   (*
    * The update function generates free vars.
    * Use closure conversion to convert back to normal.
    *)
   let prog = const_prog prog in
   let _ =
      if debug debug_pre then
         debug_prog "PRE: constant propagation" prog
   in
   let prog = dead_prog PreserveFrames prog in
   let _ =
      if debug debug_pre then
         debug_prog "PRE: dead" prog
   in
   let prog = close_prog prog in
   let _ =
      if debug debug_pre then
         debug_prog "PRE: closure conversion" prog
   in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
