(* Fir global dead code elimination
 * Geoffrey Irving
 * $Id: fir_dead.mli,v 1.3 2002/04/18 03:48:22 jyh Exp $
 *)

(* The code first computes the set of all live symbols using fixpoint analysis
 * across function boundaries.  Then it uses the set to perform dead code elimination.
 * Note that this removes all dead arguments to any function always called directly.
 * The callgraph is used to speed up the fixpoint analysis *)

open Fir

(*
 * Flags used to determine what arguments to preserve.
 *   PreserveNone: preserve only the args that are not dead
 *   PreserveFrames: preserve args that have type TyFrame
 *   PreserveAll: preserve all args
 *)
type preserve =
   PreserveNone
 | PreserveFrames
 | PreserveAll

(*
 * The bool is true iff frame arguments should be preserved.
 *)
val dead_prog : preserve -> prog -> prog

