(*
 * This is "closure conversion".  This is used to adjust
 * code where free vars have been introduced.  We _do not_ assume
 * the program is standardized; in general it will not be.
 *
 * We compute the usual dataflow analysis, then pass free vars
 * as extra arguments.
 *
 * We have to be careful with continuations; the extra arguments have
 * to be passed through the frame.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_loop
open Fir_print
open Fir_state
open Fir_standardize

open Fir_loop.FirLoop

open Fir_opt_util

open Sizeof_const
open Sizeof_type

module Pos = MakePos (struct let name = "Fir_closure" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Defs are just var sets.
 *)
type defs = SymbolSet.t

(*
 * Liveness info for a function.
 *)
type live =
   { live_defs  : SymbolSet.t;
     live_uses  : SymbolSet.t;
     live_calls : (var * SymbolSet.t) SymbolTable.t;
     live_in    : SymbolSet.t;
     live_args  : SymbolSet.t;
     live_avail : SymbolSet.t;
     live_self  : SymbolSet.t
   }

let live_empty =
   { live_defs  = SymbolSet.empty;
     live_uses  = SymbolSet.empty;
     live_in    = SymbolSet.empty;
     live_args  = SymbolSet.empty;
     live_calls = SymbolTable.empty;
     live_avail = SymbolSet.empty;
     live_self  = SymbolSet.empty
   }

(*
 * This is a table of extra vars that have to get added
 * to the frames.
 *
 *    fenv_frames: indexed by a frame type.
 *       Each entry is a (frame_vars, vars): vars are the extra variables
 *       to be stored in the frame, and frame_vars are
 *       the actual frame variables where the values
 *       should be stored.
 *    fenv_ty_fields: indexed by a frame type
 *       Each entry is a list of (field, subfield, ty)
 *       to be added to the frame record
 *    fenv_var_fields: indexed by frame variable
 *       Each entry is a variable to be stored in the frame.
 *    fenv_vars: indexed by variable
 *       Each entry is a frame to store the variable in
 *    fenv_funs: the argument index of the frame record.
 *)
type fenv =
   { fenv_frames     : (SymbolSet.t * SymbolSet.t) SymbolTable.t;
     fenv_ty_fields  : (var * var * ty) list SymbolTable.t;
     fenv_var_fields : (var * frame_label * ty) list SymbolTable.t;
     fenv_vars       : (var * frame_label * ty) list SymbolTable.t;
     fenv_funs       : int SymbolTable.t
   }

let fenv_empty =
   { fenv_frames     = SymbolTable.empty;
     fenv_ty_fields  = SymbolTable.empty;
     fenv_var_fields = SymbolTable.empty;
     fenv_vars       = SymbolTable.empty;
     fenv_funs       = SymbolTable.empty
   }

(*
 * While closing the code, we keep track of definitions.
 * When a variable is defined, we try to store it in a
 * frame as soon as possible.  If the frame is live, it gets
 * stored.  Otherwise, we save the frame record on the definitions.
 * When a frame is defined, we look for definitions that have been
 * posponed, and store them all at once.
 *
 *   def_vars: Each entry is (v, frame, label, ty), meaning
 *      store the variable v in the frame at the label
 *      whenever the frame becomes live.
 *)
type def_info =
   { def_vars : (var * frame_label * ty) SymbolTable.t;
     def_frames : SymbolSet.t
   }

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print a live set.
 *)
let print_live live =
   let { live_defs = defs;
         live_uses = uses;
         live_calls = calls;
         live_in = livein;
         live_args = args;
         live_avail = avail;
         live_self = self
       } = live
   in
      Format.printf "@[<hv 0>@[<hv 2>{@ @[<b 3>defs =";
      SymbolSet.iter (fun v ->
            Format.printf "@ %a" pp_print_symbol v) defs;
      Format.printf ";@]@ @[<b 3>uses =";
      SymbolSet.iter (fun v ->
            Format.printf "@ %a" pp_print_symbol v) uses;
      Format.printf ";@]@ @[<v 3>calls =";
      SymbolTable.iter (fun label (f, s) ->
            Format.printf "@ %a" pp_print_symbol label;
            Format.printf ": %a" pp_print_symbol f;
            Format.printf ":@[<b 0>";
            SymbolSet.iter (fun v ->
                  Format.printf "@ %a" pp_print_symbol v) s;
            Format.printf "@]") calls;
      Format.printf ";@]@ @[<b 3>in =";
      SymbolSet.iter (fun v ->
            Format.printf "@ %a" pp_print_symbol v) livein;
      Format.printf ";@]@ @[<b 3>args =";
      SymbolSet.iter (fun v ->
            Format.printf "@ %a" pp_print_symbol v) args;
      Format.printf ";@]@ @[<b 3>avail =";
      SymbolSet.iter (fun v ->
            Format.printf "@ %a" pp_print_symbol v) avail;
      Format.printf ";@]@ @[<b 3>self =";
      SymbolSet.iter (fun v ->
            Format.printf "@ %a" pp_print_symbol v) self;
      Format.printf ";@]@]@ }@]"

(*
 * Print the live sets.
 *)
let print_live funs =
   Format.printf "@[<v 3>Liveness:";
   SymbolTable.iter (fun f live ->
         Format.print_space ();
         Format.printf "@[<hv 3>%a" pp_print_symbol f;
         Format.printf ":@ ";
         print_live live;
         Format.printf "@]") funs;
   Format.printf "@]"

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Label offsets.
 *)
let zero32 = Rawint.of_int Rawint.Int32 true 0

(*
 * Filter out nodes from the callgraph that are not local.
 *)
let clean_calls funs calls =
   SymbolTable.fold (fun calls label v ->
         if SymbolTable.mem funs v then
            SymbolTable.add calls label v
         else
            calls) SymbolTable.empty calls

let clean_callgraph funs cgraph =
   SymbolTable.map (fun fnode ->
         let { fnode_in_calls = icalls;
               fnode_out_calls = ocalls
             } = fnode
         in
            { fnode with fnode_in_calls = clean_calls funs icalls;
                         fnode_out_calls = clean_calls funs ocalls
            }) cgraph.call_nodes

(*
 * Turn the trace into a list, and remove nodes
 * we don't know about.
 *)
let clean_trace funs trace =
   let trace =
      List.fold_left (fun trace { fnode_name = v } ->
            if SymbolTable.mem funs v then
               v :: trace
            else
               trace) [] (Trace.to_list trace)
   in
      List.rev trace

(*
 * Include only those calls that we know about.
 *)
let clean_live fset gset live =
   let { live_calls = calls;
         live_uses = uses
       } = live
   in
   let uses = SymbolSet.diff uses gset in
   let calls =
      SymbolTable.fold (fun calls label (f, s) ->
            if SymbolSet.mem fset f then
               SymbolTable.add calls label (f, SymbolSet.diff s gset)
            else
               calls) SymbolTable.empty calls
   in
      { live with live_uses = uses;
                  live_calls = calls
      }

(*
 * Don't allow poly in Rawdata.
 *)
let raw_sub_value_of_type genv pos ty =
   match sub_value_of_type genv pos ty with
      PolySub ->
         BlockPointerSub
    | sub ->
         sub

(************************************************************************
 * STEP 1: FRAME RENAMING
 ************************************************************************)

(*
 * Rename a variable with a frame type so it has the
 * same name as the frame.
 *)
let frename_var_type genv pos v ty =
   let pos = string_pos "frename_var_type" pos in
      match expand_type genv pos ty with
         TyFrame (v, _) ->
            v
       | _ ->
            v

let frename_var genv pos v =
   let pos = string_pos "frename_var" pos in
   let ty = genv_lookup_var genv pos v in
      frename_var_type genv pos v ty

let frename_atom genv pos a =
      match a with
         AtomVar v ->
            let v = frename_var genv pos v in
               AtomVar v
       | _ ->
            a

let frename_atom_opt genv pos a =
   match a with
      Some a ->
         Some (frename_atom genv pos a)
    | None ->
         None

let frename_atoms genv pos args =
   List.map (frename_atom genv pos) args

let frename_atom_opt_list genv pos args =
   List.map (frename_atom_opt genv pos) args

(*
 * Compute liveness for an expression.
 *)
let rec frename_exp genv e =
   let loc = loc_of_exp e in
   let pos = string_pos "live_exp" (exp_pos e) in
   let e = frename_exp_core genv pos (dest_exp_core e) in
      make_exp loc e

and frename_exp_core genv pos e =
   match e with
      LetAtom (v, ty, a, e) ->
         let a = frename_atom genv pos a in
         let e = frename_exp genv e in
         let v = frename_var genv pos v in
            LetAtom (v, ty, a, e)
    | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
         let args = frename_atoms genv pos args in
         let e = frename_exp genv e in
         let v = frename_var genv pos v in
            LetExt (v, ty1, s, b, ty2, ty_args, args, e)
    | TailCall (label, f, args) ->
         let args = frename_atoms genv pos args in
         let f = frename_atom genv pos f in
            TailCall (label, f, args)
    | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
         let level = frename_atom genv pos level in
         let args = frename_atoms genv pos args in
         let f = frename_atom genv pos f in
            SpecialCall (label, TailAtomicCommit (level, f, args))
    | SpecialCall (label, TailSysMigrate (i, a1, a2, f, args)) ->
         let a1 = frename_atom genv pos a1 in
         let a2 = frename_atom genv pos a2 in
         let f = frename_atom genv pos f in
         let args = frename_atoms genv pos args in
            SpecialCall (label, TailSysMigrate (i, a1, a2, f, args))
    | SpecialCall (label, TailAtomic (f, c, args)) ->
         let f = frename_atom genv pos f in
         let c = frename_atom genv pos c in
         let args = frename_atoms genv pos args in
            SpecialCall (label, TailAtomic (f, c, args))
    | SpecialCall (label, TailAtomicRollback (level, a)) ->
         let level = frename_atom genv pos level in
         let a = frename_atom genv pos a in
            SpecialCall (label, TailAtomicRollback (level, a))
    | Match (a, cases) ->
         let a = frename_atom genv pos a in
         let cases =
            List.map (fun (l, s, e) ->
                  l, s, frename_exp genv e) cases
         in
            Match (a, cases)
    | MatchDTuple (a, cases) ->
         let a = frename_atom genv pos a in
         let cases =
            List.map (fun (l, a_opt, e) ->
                  l, frename_atom_opt genv pos a_opt, frename_exp genv e) cases
         in
            MatchDTuple (a, cases)
    | TypeCase (a1, a2, v1, v2, e1, e2) ->
         let a1 = frename_atom genv pos a1 in
         let a2 = frename_atom genv pos a2 in
         let v1 = frename_var genv pos v1 in
         let v2 = frename_var genv pos v2 in
         let e1 = frename_exp genv e1 in
         let e2 = frename_exp genv e2 in
            TypeCase (a1, a2, v1, v2, e1, e2)
    | LetAlloc (v, op, e) ->
         let v = frename_var genv pos v in
         let op =
            match op with
               AllocTuple (tclass, ty, ty_vars, args) ->
                  let args = frename_atoms genv pos args in
                     AllocTuple (tclass, ty, ty_vars, args)
             | AllocDTuple (ty, ty_var, a, args) ->
                  let a = frename_atom genv pos a in
                  let args = frename_atoms genv pos args in
                     AllocDTuple (ty, ty_var, a, args)
             | AllocUnion (ty, ty_vars, tyv, i, args) ->
                  let args = frename_atoms genv pos args in
                     AllocUnion (ty, ty_vars, tyv, i, args)
             | AllocArray (ty, args) ->
                  let args = frename_atoms genv pos args in
                     AllocArray (ty, args)
             | AllocVArray (ty, op, a1, a2) ->
                  let a1 = frename_atom genv pos a1 in
                  let a2 = frename_atom genv pos a2 in
                     AllocVArray (ty, op, a1, a2)
             | AllocMalloc (ty, a) ->
                  let a = frename_atom genv pos a in
                     AllocMalloc (ty, a)
             | AllocFrame _ ->
                  op
         in
         let e = frename_exp genv e in
            LetAlloc (v, op, e)
    | LetSubscript (op, v1, ty, v2, a, e) ->
         let v1 = frename_var genv pos v1 in
         let v2 = frename_atom genv pos v2 in
         let a = frename_atom genv pos a in
         let e = frename_exp genv e in
            LetSubscript (op, v1, ty, v2, a, e)
    | SetSubscript (op, label, v, a1, ty, a2, e) ->
         let v = frename_atom genv pos v in
         let a1 = frename_atom genv pos a1 in
         let a2 = frename_atom genv pos a2 in
         let e = frename_exp genv e in
            SetSubscript (op, label, v, a1, ty, a2, e)
    | LetGlobal (op, v, ty, l, e) ->
         let v = frename_var genv pos v in
         let l = frename_var genv pos l in
         let e = frename_exp genv e in
            LetGlobal (op, v, ty, l, e)
    | SetGlobal (op, label, v, ty, a, e) ->
         let v = frename_var genv pos v in
         let a = frename_atom genv pos a in
         let e = frename_exp genv e in
            SetGlobal (op, label, v, ty, a, e)
    | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
         let v1 = frename_atom genv pos v1 in
         let a1 = frename_atom genv pos a1 in
         let v2 = frename_atom genv pos v2 in
         let a2 = frename_atom genv pos a2 in
         let a3 = frename_atom genv pos a3 in
         let e = frename_exp genv e in
            Memcpy (op, label, v1, a1, v2, a2, a3, e)
    | Call (label, f, args, e) ->
         let f = frename_atom genv pos f in
         let args = frename_atom_opt_list genv pos args in
         let e = frename_exp genv e in
            Call (label, f, args, e)
    | Assert (label, pred, e) ->
         let pred =
            match pred with
               IsMutable v ->
                  IsMutable (frename_atom genv pos v)
             | Reserve (a1, a2) ->
                  let a1 = frename_atom genv pos a1 in
                  let a2 = frename_atom genv pos a2 in
                     Reserve (a1, a2)
             | ElementCheck (ty, op, v, a) ->
                  let v = frename_atom genv pos v in
                  let a = frename_atom genv pos a in
                     ElementCheck (ty, op, v, a)
             | BoundsCheck (op, v, a1, a2) ->
                  let v = frename_atom genv pos v in
                  let a1 = frename_atom genv pos a1 in
                  let a2 = frename_atom genv pos a2 in
                     BoundsCheck (op, v, a1, a2)
         in
         let e = frename_exp genv e in
            Assert (label, pred, e)
    | Debug (info, e) ->
         let e = frename_exp genv e in
            Debug (info, e)

(*
 * Rename a function.
 *)
let frename_fun genv f (info, ty, ty_vars, vars, e) =
   let pos = string_pos "frename_fun" (var_exp_pos f) in
   let vars = List.map (frename_var genv pos) vars in
   let e = frename_exp genv e in
      info, ty, ty_vars, vars, e

(*
 * Rename the program.
 *)
let frename_prog prog =
   let { prog_funs = funs } = prog in
   let genv = prog_genv prog in
   let funs = SymbolTable.mapi (frename_fun genv) funs in
      { prog with prog_funs = funs }

(************************************************************************
 * STEP 1: LIVENESS
 ************************************************************************)

(*
 * Add a def.
 *)
let def_var = SymbolSet.add

(*
 * Liveness environment.
 *)
let live_var live v =
   { live with live_uses = SymbolSet.add live.live_uses v }

let live_def live v =
   { live with live_uses = SymbolSet.remove live.live_uses v }

let live_call live pos label f defs =
   let pos = string_pos "live_call" pos in
      { live with live_calls = SymbolTable.add live.live_calls label (var_of_fun pos f, defs) }

(*
 * Liveness for an atom use.
 *)
let live_atom live a =
   match a with
      AtomVar v ->
         live_var live v
    | _ ->
         live

let live_atom_opt live a =
   match a with
      Some a ->
         live_atom live a
    | None ->
         live

let live_atoms live args =
   List.fold_left (fun live a ->
         live_atom live a) live args

let live_atom_opt_list live args =
   List.fold_left (fun live a ->
         live_atom_opt live a) live args

(*
 * Compute liveness for an expression.
 *)
let rec live_exp live defs e =
   let pos = string_pos "live_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, _, a, e) ->
            let defs = def_var defs v in
            let live = live_exp live defs e in
            let live = live_def live v in
            let live = live_atom live a in
               live
       | LetExt (v, _, _, _, _, _, args, e) ->
            let defs = def_var defs v in
            let live = live_exp live defs e in
            let live = live_def live v in
            let live = live_atoms live args in
               live
       | TailCall (label, f, args) ->
            let live = live_atom live f in
            let live = live_atoms live args in
               live_call live pos label f defs
       | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
            let live = live_atom live f in
            let live = live_atoms live (level :: args) in
               live_call live pos label f defs
       | SpecialCall (label, TailSysMigrate (_, a1, a2, f, args)) ->
            let live = live_atom live a1 in
            let live = live_atom live a2 in
            let live = live_atom live f in
            let live = live_atoms live args in
               live_call live pos label f defs
       | SpecialCall (label, TailAtomic (f, c, args)) ->
            let live = live_atom live f in
            let live = live_atom live c in
            let live = live_atoms live args in
               live_call live pos label f defs
       | SpecialCall (_, TailAtomicRollback (level, a)) ->
            live_atoms live [level; a]
       | Match (a, cases) ->
            let live = live_atom live a in
               List.fold_left (fun live (_, _, e) ->
                     live_exp live defs e) live cases
       | MatchDTuple (a, cases) ->
            let live = live_atom live a in
               List.fold_left (fun live (_, a_opt, e) ->
                     live_exp (live_atom_opt live a_opt) defs e) live cases
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            let defs1 = def_var defs v2 in
            let live = live_exp live defs1 e1 in
            let live = live_def live v2 in
            let live = live_exp live defs e2 in
            let live = live_atom live a1 in
            let live = live_atom live a2 in
            let live = live_var live v1 in
               live
       | LetAlloc (v, op, e) ->
            let defs = def_var defs v in
            let live = live_exp live defs e in
            let live = live_def live v in
            let live =
               match op with
                  AllocTuple (_, _, _, args)
                | AllocUnion (_, _, _, _, args)
                | AllocArray (_, args) ->
                     live_atoms live args
                | AllocDTuple (_, _, a, args) ->
                     live_atoms (live_atom live a) args
                | AllocVArray (_, _, a1, a2) ->
                     let live = live_atom live a1 in
                        live_atom live a2
                | AllocMalloc (_, a) ->
                     live_atom live a
                | AllocFrame _ ->
                     live
            in
               live
       | LetSubscript (_, v1, _, v2, a, e) ->
            let defs = def_var defs v1 in
            let live = live_exp live defs e in
            let live = live_def live v1 in
            let live = live_atom live v2 in
            let live = live_atom live a in
               live
       | SetSubscript (_, _, v, a1, _, a2, e) ->
            let live = live_exp live defs e in
            let live = live_atom live v in
            let live = live_atom live a1 in
            let live = live_atom live a2 in
               live
       | LetGlobal (_, _, _, l, e) ->
            let live = live_exp live defs e in
            let live = live_var live l in
               live
       | SetGlobal (_, _, _, _, a, e) ->
            let live = live_exp live defs e in
            let live = live_atom live a in
               live
       | Memcpy (_, _, v1, a1, v2, a2, a3, e) ->
            let live = live_exp live defs e in
            let live = live_atom live v1 in
            let live = live_atom live a1 in
            let live = live_atom live v2 in
            let live = live_atom live a2 in
            let live = live_atom live a3 in
               live
       | Call (label, f, args, e) ->
            let live = live_exp live defs e in
            let live = live_atom live f in
            let live = live_atom_opt_list live args in
               live_call live pos label f defs
       | Assert (_, pred, e) ->
            let live = live_exp live defs e in
            let live =
               match pred with
                  IsMutable v ->
                     live_atom live v
                | Reserve (a1, a2) ->
                     let live = live_atom live a1 in
                     let live = live_atom live a2 in
                        live
                | ElementCheck (_, _, v, a) ->
                     let live = live_atom live v in
                        live_atom live a
                | BoundsCheck (_, v, a1, a2) ->
                     let live = live_atom live v in
                     let live = live_atom live a1 in
                        live_atom live a2
            in
               live
       | Debug (_, e) ->
            live_exp live defs e

(*
 * Compute liveness for a function.
 *)
let live_fun fset gset (_, _, _, vars, e) =
   let defs = List.fold_left SymbolSet.add SymbolSet.empty vars in
   let live = { live_empty with live_defs = defs } in
   let live = live_exp live defs e in
   let live = List.fold_left live_def live vars in
      clean_live fset gset live

(************************************************************************
 * STEP 2: REVERSE DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Dataflow equations:
 *    live_in[f] = uses[f] union (union ((g, defs) in succ[f]). live_in[g] - defs)
 *)
let live_step funs trace =
   List.fold_left (fun (changed, funs) f ->
         let live = SymbolTable.find funs f in
         let { live_calls = calls;
               live_defs = defs;
               live_uses = uses;
               live_in = live1
             } = live
         in

         (* Add vars from called functions *)
         let live2 =
            SymbolTable.fold (fun live2 _ (g, defs) ->
                  let live = SymbolTable.find funs g in
                  let { live_in = live } = live in
                  let live = SymbolSet.diff live defs in
                     SymbolSet.union live2 live) uses calls
         in
         let live2 = SymbolSet.diff live2 defs in
            (* Check if liveness info has changed *)
            if SymbolSet.equal live2 live1 then
               changed, funs
            else
               let live = { live with live_in = live2 } in
               let funs = SymbolTable.add funs f live in
                  true, funs) (false, funs) trace

(*
 * Compute dataflow fixpoint.
 *)
let live_fixpoint funs trace =
   let trace = List.rev trace in
   let rec fixpoint funs =
      let changed, funs = live_step funs trace in
         if changed then
            fixpoint funs
         else
            funs
   in
      fixpoint funs

(*
 * Program analysis.
 *)
let live_prog prog trace =
   let { prog_funs = funs;
         prog_names = names;
         prog_import = import;
         prog_globals = globals
       } = prog
   in
   let fset = SymbolSet.empty in
   let fset = SymbolTable.fold (fun fset f _ -> SymbolSet.add fset f) fset funs in
   let gset = SymbolTable.fold (fun fset f _ -> SymbolSet.add fset f) fset names in
   let gset = SymbolTable.fold (fun fset f _ -> SymbolSet.add fset f) gset import in
   let gset = SymbolTable.fold (fun fset f _ -> SymbolSet.add fset f) gset globals in
   let funs = SymbolTable.map (live_fun fset gset) funs in
      live_fixpoint funs trace

(************************************************************************
 * STEP 3: FRAME ASSIGNMENT
 ************************************************************************)

(*
 * Add a set to the frame environment.
 *)
let fenv_is_frame fenv v =
   SymbolTable.mem fenv.fenv_frames v

let fenv_add_frame fenv pos label frame vars =
   let frames =
      SymbolTable.filter_add fenv.fenv_frames label (function
         Some (frames, vars') ->
            SymbolSet.add frames frame, SymbolSet.union vars' vars
       | None ->
            SymbolSet.singleton frame, vars)
   in
      { fenv with fenv_frames = frames }

let fenv_lookup_frame fenv pos v =
   try SymbolTable.find fenv.fenv_frames v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Function info.
 *)
let fenv_is_fun fenv f =
   SymbolTable.mem fenv.fenv_funs f

let fenv_add_fun fenv pos f i =
   let pos = string_pos "fenv_add_fun" pos in
   let funs =
      SymbolTable.filter_add fenv.fenv_funs f (function
         Some i' ->
            if i' <> i then
               raise (FirException (pos, StringVarError ("function has a frame mismatch", f)));
            i
       | None ->
            i)
   in
      { fenv with fenv_funs = funs }

let fenv_lookup_fun_exn fenv f =
   SymbolTable.find fenv.fenv_funs f

let fenv_lookup_fun fenv pos f =
   let pos = string_pos "fenv_lookup_fun" pos in
      try fenv_lookup_fun_exn fenv f with
         Not_found ->
            raise (FirException (pos, UnboundVar f))

(*
 * Field lookup.
 *)
let fenv_mem_var_frame fenv v =
   SymbolTable.mem fenv.fenv_var_fields v

let fenv_lookup_var_exn fenv v =
   SymbolTable.find fenv.fenv_vars v

let fenv_lookup_var fenv pos v =
   try fenv_lookup_var_exn fenv v with
      Not_found ->
         let pos = string_pos "fenv_lookup_var" pos in
            raise (FirException (pos, UnboundVar v))

(*
 * For continuations, we have to store the extra arguments
 * in their frames.  For each Call, find the frame, get its
 * label, and save the free vars in a set assigned to the label.
 *)
let rec frame_exp genv fenv funs e =
   let pos = string_pos "frame_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, _, e)
       | LetExt (v, ty, _, _, _, _, _, e)
       | LetSubscript (_, v, ty, _, _, e)
       | LetGlobal (_, v, ty, _, e) ->
            let genv = genv_add_var genv v ty in
               frame_exp genv fenv funs e
       | TailCall _
       | SpecialCall _ ->
            fenv
       | Match (_, cases) ->
            List.fold_left (fun fenv (_, _, e) ->
                  frame_exp genv fenv funs e) fenv cases
       | MatchDTuple (_, cases) ->
            List.fold_left (fun fenv (_, _, e) ->
                  frame_exp genv fenv funs e) fenv cases
       | TypeCase (_, _, _, v, e1, e2) ->
            let fenv = frame_exp genv fenv funs e2 in
            let genv = genv_add_var genv v TyDelayed in
            let fenv = frame_exp genv fenv funs e1 in
               fenv
       | LetAlloc (v, op, e) ->
            let ty = type_of_alloc_op op in
            let genv = genv_add_var genv v ty in
               frame_exp genv fenv funs e
       | SetSubscript (_, _, _, _, _, _, e)
       | SetGlobal (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, _, e)
       | Assert (_, _, e)
       | Debug (_, e) ->
            frame_exp genv fenv funs e
       | Call (_, f, args, e) ->
            let fenv = frame_exp genv fenv funs e in
               frame_call genv fenv funs pos f args

(*
 * We have a call.
 * There should be exactly one argument, and it should
 * be a frame.
 *)
and frame_call genv fenv funs pos f args =
   let pos = string_pos "frame_call" pos in
   let f = var_of_fun pos f in
      if SymbolTable.mem funs f then
         frame_call_inner genv fenv funs pos f args
      else
         fenv

and frame_call_inner genv fenv funs pos f args =
   let pos = string_pos "frame_call_inner" pos in

   (* Get the frame argument *)
   let rec search i args =
      match args with
         Some (AtomVar v) :: _ ->
            i, v
       | _ :: args ->
            search (succ i) args
       | [] ->
            raise (FirException (pos, StringError "bogus call"))
   in
   let i, frame = search 0 args in

   (* Get the frame's label *)
   let ty = genv_lookup_var genv pos frame in
   let label, _ = dest_frame_type genv pos ty in

   (* Get the function *)
   let { live_in = live } = SymbolTable.find funs f in

   (* Add to the frame environment *)
   let fenv = fenv_add_frame fenv pos label frame live in
   let fenv = fenv_add_fun fenv pos f i in
      fenv

(*
 * Collect the frame info for a function.
 *)
let frame_fun genv funs fenv _ (_, _, _, _, e) =
   frame_exp genv fenv funs e

let frame_funs genv funs fenv funtab =
   SymbolTable.fold (frame_fun genv funs) fenv funtab

(*
 * Split the frame info into the three parts:
 *    1. New fields for the frame types
 *    2. Fields to be stored in frame vars
 *    3. Variable fields and frames they are to be stored in
 *)
let frame_fields genv fenv =
   let ty_fields, var_fields, var_table =
      SymbolTable.fold (fun info frame (frames, vars) ->
            SymbolSet.fold (fun (ty_fields, var_fields, var_table) v ->
                  let pos = string_pos "frame_fields" (var_exp_pos v) in
                  let field = new_symbol v in
                  let subfield = new_symbol v in
                  let label = frame, field, subfield in
                  let ty = genv_lookup_var genv pos v in
                  let ty_fields =
                     SymbolTable.filter_add ty_fields frame (fun fields ->
                           let fields =
                              match fields with
                                 Some fields ->
                                    fields
                               | None ->
                                    []
                           in
                              (field, subfield, ty) :: fields)
                  in
                  let var_fields =
                     SymbolSet.fold (fun var_fields frame ->
                           SymbolTable.filter_add var_fields frame (fun fields ->
                                 let fields =
                                    match fields with
                                       Some fields ->
                                          fields
                                     | None ->
                                          []
                                 in
                                    (v, label, ty) :: fields)) var_fields frames
                  in
                  let var_table =
                     SymbolTable.filter_add var_table v (fun fields ->
                           let fields =
                              match fields with
                                 Some fields ->
                                    fields
                               | None ->
                                    []
                           in
                              SymbolSet.fold (fun fields frame ->
                                    (frame, label, ty) :: fields) fields frames)
                  in
                     ty_fields, var_fields, var_table) info vars) (**)
         (SymbolTable.empty, SymbolTable.empty, SymbolTable.empty) fenv.fenv_frames
   in
      { fenv with fenv_ty_fields = ty_fields;
                  fenv_var_fields = var_fields;
                  fenv_vars = var_table
      }

(*
 * Remove live vars from any closure function.
 *)
let frame_live fenv funs =
   SymbolTable.mapi (fun f live ->
         if fenv_is_fun fenv f then
            { live with live_in = SymbolSet.empty }
         else
            live) funs

(*
 * Find the new frame fields, and extend the types.
 *)
let frame_prog genv funs prog =
   let { prog_funs = funtab;
         prog_frames = frames
       } = prog
   in
   let fenv = fenv_empty in
   let fenv = frame_funs genv funs fenv funtab in
   let fenv = frame_fields genv fenv in
   let funs = frame_live fenv funs in
      fenv, funs

(************************************************************************
 * FORWARD DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Compute the "available" expressions.
 * A variable becomes available if:
 *    1. it is used in the function
 *    2. it is available on any of the incoming edges
 *)
let favail_step funs nodes trace =
   List.fold_left (fun (changed, funs) f ->
         let live = SymbolTable.find funs f in
         let { live_defs = defs;
               live_uses = uses;
               live_avail = avail;
               live_in = live_in
             } = live
         in

         (* Add all available values on incoming edges *)
         let { fnode_in_calls = calls } = SymbolTable.find nodes f in
         let avail' =
            SymbolTable.fold (fun avail' _ g ->
                  let { live_avail = avail;
                        live_calls = calls
                      } = SymbolTable.find funs g
                  in
                  let avail =
                     SymbolTable.fold (fun avail _ (f', defs) ->
                           if Symbol.eq f' f then
                              SymbolSet.union avail defs
                           else
                              avail) avail calls
                  in
                  let avail = SymbolSet.inter avail live_in in
                  let avail =
                     match avail' with
                        Some avail' ->
                           SymbolSet.union avail' avail
                      | None ->
                           avail
                  in
                     Some avail) None calls
         in
         let avail' =
            match avail' with
               Some avail' ->
                  SymbolSet.union avail' uses
             | None ->
                  uses
         in
         let avail' = SymbolSet.union avail' defs in
            (* Check if availability has changed *)
            if SymbolSet.equal avail' avail then
               changed, funs
            else
               let live = { live with live_avail = avail' } in
               let funs = SymbolTable.add funs f live in
                  true, funs) (false, funs) trace

(*
 * Compute the fixpoint.
 *)
let favail_fixpoint funs cgraph trace =
   let rec fixpoint funs =
      let changed, funs = favail_step funs cgraph trace in
         if changed then
            fixpoint funs
         else
            funs
   in
      fixpoint funs

(************************************************************************
 * REVERSE DATAFLOW ANALYSIS
 ************************************************************************)

(*
 * Multiset operations.
 *)
module MultiSet =
struct
   type t = int SymbolTable.t

   let empty = SymbolTable.empty

   let union m s =
      SymbolSet.fold (fun m v ->
            SymbolTable.filter_add m v (function
               Some i -> succ i
             | None -> 1)) m s

   let diff m s =
      SymbolSet.fold SymbolTable.remove m s

   let to_set m =
      SymbolTable.fold (fun s v i ->
            if i > 1 then
               SymbolSet.add s v
            else
               s) SymbolSet.empty m
end

(*
 * In the last dataflow analysis, available expressions that
 * are supposed to be available on any outgoing edge are pushed
 * back to their parents.
 *)
let ravail_step funs nodes trace =
   List.fold_left (fun (changed, funs) f ->
         let live = SymbolTable.find funs f in
         let { live_avail = avail } = live in
         let { fnode_out_calls = calls } = SymbolTable.find nodes f in

         (* First get the union of all the available requests *)
         let avail' =
            SymbolTable.fold (fun avail' _ g ->
                  let { live_avail = avail;
                        live_in = live_in
                      } = SymbolTable.find funs g
                  in
                  let avail = SymbolSet.inter avail live_in in
                     MultiSet.union avail' avail) MultiSet.empty calls
         in
         let avail' = MultiSet.to_set avail' in
         let avail' = SymbolSet.union avail' avail in
            (* Check if availability has changed *)
            if SymbolSet.equal avail' avail then
               changed, funs
            else
               let live = { live with live_avail = avail' } in
               let funs = SymbolTable.add funs f live in
                  true, funs) (false, funs) trace

(*
 * Compute the fixpoint.
 *)
let ravail_fixpoint funs cgraph trace =
   let trace = List.rev trace in
   let rec fixpoint funs =
      let changed, funs = ravail_step funs cgraph trace in
         if changed then
            fixpoint funs
         else
            funs
   in
      fixpoint funs

(************************************************************************
 * VARIABLE ASSIGNMENT
 ************************************************************************)

(*
 * In the last step, assign variables to functions.
 * Here is the rule:
 *
 *    1. If a variable is in the avail set, and it is not
 *       avail in any of the parents, assign it to this function.
 *)
let assign_funs funs nodes =
   SymbolTable.fold (fun funs f _ ->
         let live = SymbolTable.find funs f in
         let { live_avail = avail;
               live_self = self;
               live_in = live_in;
               live_defs = defs
             } = live
         in

         (* Compute the union of all parent's avail sets *)
         let { fnode_in_calls = calls } = SymbolTable.find nodes f in
         let avail', defs' =
            SymbolTable.fold (fun (avail', defs') _ g ->
                  let { live_avail = avail;
                        live_calls = calls
                      } = SymbolTable.find funs g
                  in
                  let avail, defs' =
                     SymbolTable.fold (fun (avail', defs') _ (f', defs) ->
                           if Symbol.eq f' f then
                              let avail' = SymbolSet.union avail' defs in
                              let defs' = SymbolSet.diff defs' defs in
                                 avail', defs'
                           else
                              avail', defs') (avail, defs') calls
                  in
                  let avail' = SymbolSet.union avail' avail in
                     avail', defs') (SymbolSet.empty, defs) calls
         in
         let avail' = SymbolSet.inter live_in avail' in

         (* The assignment is the difference *)
         let self' = SymbolSet.diff avail avail' in
         let self = SymbolSet.union self self' in
         let args = SymbolSet.inter live_in avail in
         let args = SymbolSet.diff args self in
         let live =
            { live with live_self = self;
                        live_args = args;
                        live_defs = defs'
            }
         in
         let funs = SymbolTable.add funs f live in

         (* Add the var to any parents that are missing it *)
         let funs =
            SymbolTable.fold (fun funs _ g ->
                  let live = SymbolTable.find funs g in
                  let { live_avail = avail;
                        live_self = self
                      } = live
                  in
                  let avail = SymbolSet.diff args avail in
                  let self = SymbolSet.union self avail in
                  let live = { live with live_self = self } in
                     SymbolTable.add funs g live) funs calls
         in
            funs) funs funs

(************************************************************************
 * STEP 4: REWRITE THE CODE
 ************************************************************************)

(*
 * Project the values from the frame.
 *)
let subop_of_type genv pos ty =
   let pos = string_pos "subop_of_type" pos in
      { sub_block = RawDataSub;
        sub_value = raw_sub_value_of_type genv pos ty;
        sub_index = ByteIndex;
        sub_script = RawIntIndex (precision_int, true)
      }

(*
 * Close a type.  This adds a universal quantifier.
 *)
let rec close_type vars ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyRawData
    | TyDelayed
    | TyProject _
    | TyFrame _
    | TyPointer _
    | TyDTuple (_, None) ->
         vars
    | TyFun (tyl, ty) ->
         close_types (close_type vars ty) tyl
    | TyUnion (_, tyl, _)
    | TyApply (_, tyl) ->
         close_types vars tyl
    | TyTuple (_, tyl)
    | TyDTuple (_, Some tyl)
    | TyTag (_, tyl) ->
         close_mutable_types vars tyl
    | TyArray ty
    | TyCase ty
    | TyObject (_, ty) ->
         close_type vars ty
    | TyAll (vars', ty)
    | TyExists (vars', ty) ->
         let vars = close_type vars ty in
            SymbolSet.subtract_list vars vars'
    | TyVar v ->
         SymbolSet.add vars v

and close_types vars tyl =
   List.fold_left close_type vars tyl

and close_mutable_type vars (ty, _) =
   close_type vars ty

and close_mutable_types vars tyl =
   List.fold_left close_mutable_type vars tyl

let close_fun_type ty =
   let vars = close_type SymbolSet.empty ty in
      match SymbolSet.to_list vars with
         [] ->
            ty
       | vars ->
            TyAll (vars, ty)

(*
 * Close a definition.
 * Look up the variable and see if the frame is available.
 * If so, store the value and continue.  Otherwise, add the
 * var to the def list so it will be stored whenever the
 * frame is allocated.
 *)
let close_def genv fenv defs self pos loc v cont =
   let pos = string_pos "close_def" pos in
   let frames =
      try fenv_lookup_var_exn fenv v with
         Not_found ->
            []
   in
   let rec close defs frames =
      let { def_frames = dframes;
            def_vars = vars
          } = defs
      in
         match frames with
            (frame, label, ty) :: frames ->
               if SymbolSet.mem dframes frame then
                  let subop = subop_of_type genv pos ty in
                  let dlabel = new_symbol v in
                     make_exp loc (SetSubscript (subop, dlabel, AtomVar frame, AtomLabel (label, zero32), ty, AtomVar v, close defs frames))
               else
                  let defs = { defs with def_vars = SymbolTable.add vars v (frame, label, ty) } in
                     close defs frames
          | [] ->
               cont defs (SymbolSet.remove self v)
   in
      close defs frames

let close_defs genv fenv defs self pos loc vars cont =
   let pos = string_pos "close_defs" pos in
   let rec close defs self vars =
      match vars with
         v :: vars ->
            close_def genv fenv defs self pos loc v (fun defs self ->
                  close defs self vars)
       | [] ->
            cont defs self
   in
      close defs self (SymbolSet.to_list vars)

(*
 * Close an allocation.
 * If this is a frame, store all the delayed defs.
 *)
let close_alloc genv fenv defs pos loc frame cont =
   let pos = string_pos "close_alloc" pos in
   let { def_frames = dframes;
         def_vars = dvars
       } = defs
   in
   let dvars =
      SymbolTable.fold (fun dvars v (frame, label, ty) ->
            (v, frame, label, ty) :: dvars) [] dvars
   in
   let rec close dvars' dvars =
      match dvars with
         (v, dframe, dlabel, ty) as dvar :: dvars ->
            if Symbol.eq dframe frame then
               let subop = subop_of_type genv pos ty in
               let label = new_symbol_string "alloc" in
                  make_exp loc (SetSubscript (subop, label, AtomVar frame, AtomLabel (dlabel, zero32), ty, AtomVar v, close dvars' dvars))
            else
               close (dvar :: dvars') dvars
       | [] ->
            let dvars' =
               List.fold_left (fun dvars' (v, frame, label, ty) ->
                     SymbolTable.add dvars' v (frame, label, ty)) SymbolTable.empty dvars'
            in
            let defs =
               { def_frames = SymbolSet.add dframes frame;
                 def_vars = dvars'
               }
            in
               cont defs
   in
      close [] dvars

(*
 * Close a use.
 *)
let find_var_frame defs pos v frames' =
   let pos = string_pos "find_var_frame" pos in
   let dframes = defs.def_frames in
   let rec search frames =
      match frames with
         (frame, label, ty) as dframe :: frames ->
            if SymbolSet.mem dframes frame then
               dframe
            else
               search frames
       | [] ->
            let print_error buf =
               Format.fprintf buf "@[<hv 3>Looking for %a" pp_print_symbol v;
               Format.fprintf buf ":@ Available frames: ";
               SymbolSet.iter (fun v ->
                     Format.pp_print_space buf ();
                     pp_print_symbol buf v) dframes;
               Format.fprintf buf ";@ Allowable frames:";
               List.iter (fun (frame, label, ty) ->
                     Format.fprintf buf "@ @[<hv 0>@[<hv 3> {@ frame = ";
                     pp_print_symbol buf frame;
                     Format.fprintf buf ";@ label = %a" pp_print_label label;
                     Format.fprintf buf ";@ @[<hv 3>type =@ %a" pp_print_type ty;
                     Format.fprintf buf ";@]@]@ }@]") frames';
               Format.fprintf buf "@]"
            in
               raise (FirException (pos, StringFormatError ("no frame found", print_error)))
   in
      search frames'

let close_var genv fenv defs self pos loc v cont =
   let pos = string_pos "close_var" pos in
      if SymbolSet.mem self v then
         let self = SymbolSet.remove self v in
         let frames =
            try Some (fenv_lookup_var_exn fenv v) with
               Not_found ->
                  None
         in
            match frames with
               Some frames ->
                  let frame, label, ty = find_var_frame defs pos v frames in
                  let subop = subop_of_type genv pos ty in
                     make_exp loc (LetSubscript (subop, v, ty, AtomVar frame, AtomLabel (label, zero32), cont self v))
             | None ->
                  cont self v
      else
         cont self v

let close_atom genv fenv defs self pos loc a cont =
   let pos = string_pos "close_atom" pos in
      match a with
         AtomVar v ->
            close_var genv fenv defs self pos loc v (fun self v ->
                  cont self (AtomVar v))
       | _ ->
            cont self a

let close_atoms genv fenv defs self pos loc args cont =
   let pos = string_pos "close_atoms" pos in
   let rec close self args' args =
      match args with
         a :: args ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  close self (a :: args') args)
       | [] ->
            cont self (List.rev args')
   in
      close self [] args

let close_cases genv fenv defs self pos loc cases cont =
   let pos = string_pos "close_cases" pos in
   let rec close self cases' cases =
      match cases with
         (l, Some a, e) :: cases ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  close self ((l, Some a, e) :: cases') cases)
       | (_, None, _) as case :: cases ->
            close self (case :: cases') cases
       | [] ->
            cont self (List.rev cases')
   in
      close self [] cases

(*
 * Flush all the vars.
 *)
let close_flush genv fenv defs self pos loc cont =
   let pos = string_pos "close_flush" pos in
   let rec close self =
      if SymbolSet.is_empty self then
         cont self
      else
         let v = SymbolSet.choose self in
            close_var genv fenv defs self pos loc v (fun self _ ->
                  close self)
   in
      close self

(*
 * Allocation.
 *)
let close_alloc_op genv fenv defs self pos loc op cont =
   let pos = string_pos "close_alloc_op" pos in
      match op with
         AllocTuple (tclass, ty, ty_vars, args) ->
            close_atoms genv fenv defs self pos loc args (fun self args ->
                  cont self (AllocTuple (tclass, ty, ty_vars, args)))
       | AllocDTuple (ty, ty_var, a, args) ->
            close_atom genv fenv defs self pos loc a (fun self a ->
            close_atoms genv fenv defs self pos loc args (fun self args ->
                  cont self (AllocDTuple (ty, ty_var, a, args))))
       | AllocUnion (ty, ty_vars, tyv, i, args) ->
            close_atoms genv fenv defs self pos loc args (fun self args ->
                  cont self (AllocUnion (ty, ty_vars, tyv, i, args)))
       | AllocArray (ty, args) ->
            close_atoms genv fenv defs self pos loc args (fun self args ->
                  cont self (AllocArray (ty, args)))
       | AllocVArray (ty, op, a1, a2) ->
            close_atom genv fenv defs self pos loc a1 (fun self a1 ->
            close_atom genv fenv defs self pos loc a2 (fun self a2 ->
                  cont self (AllocVArray (ty, op, a1, a2))))
       | AllocMalloc (ty, a) ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  cont self (AllocMalloc (ty, a)))
       | AllocFrame _ ->
            cont self op

(*
 * Predicates.
 *)
let close_pred genv fenv defs self pos loc pred cont =
   let pos = string_pos "close_pred" pos in
      match pred with
         IsMutable v ->
            close_atom genv fenv defs self pos loc v (fun self v ->
                  cont self (IsMutable v))
       | Reserve (a1, a2) ->
            close_atom genv fenv defs self pos loc a1 (fun self a1 ->
            close_atom genv fenv defs self pos loc a2 (fun self a2 ->
                  cont self (Reserve (a1, a2))))
       | ElementCheck (ty, op, v, a) ->
            close_atom genv fenv defs self pos loc v (fun self v ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  cont self (ElementCheck (ty, op, v, a))))
       | BoundsCheck (op, v, a1, a2) ->
            close_atom genv fenv defs self pos loc v (fun self v ->
            close_atom genv fenv defs self pos loc a1 (fun self a1 ->
            close_atom genv fenv defs self pos loc a2 (fun self a2 ->
                  cont self (BoundsCheck (op, v, a1, a2)))))

(*
 * At this point we have the following:
 *    1. A list of free variables for each function
 *    2. A set of the variables to be stored in each frame
 *)
let rec close_exp genv fenv funs defs self e =
   let pos = string_pos "close_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  make_exp loc (LetAtom (v, ty, a,
                           close_def genv fenv defs self pos loc v (fun defs self ->
                                 close_exp genv fenv funs defs self e))))
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            close_atoms genv fenv defs self pos loc args (fun self args ->
                  make_exp loc (LetExt (v, ty1, s, b, ty2, ty_args, args,
                          close_def genv fenv defs self pos loc v (fun defs self ->
                                close_exp genv fenv funs defs self e))))
       | TailCall (label, f, args) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  let args = close_tailcall genv fenv funs pos loc label f args in
                     make_exp loc (TailCall (label, f, args)))
       | SpecialCall (label, TailAtomicCommit (level, f, args)) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  let args = close_tailcall genv fenv funs pos loc label f args in
                     make_exp loc (SpecialCall (label, TailAtomicCommit (level, f, args))))
       | SpecialCall (label, TailSysMigrate (i, a1, a2, f, args)) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  let args = close_tailcall genv fenv funs pos loc label f args in
                     make_exp loc (SpecialCall (label, TailSysMigrate (i, a1, a2, f, args))))
       | SpecialCall (label, TailAtomic (f, c, args)) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  let args = close_tailcall genv fenv funs pos loc label f args in
                     make_exp loc (SpecialCall (label, TailAtomic (f, c, args))))
       | SpecialCall (_, TailAtomicRollback _) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  e)
       | Match (a, cases) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  let cases =
                     List.map (fun (l, s, e) ->
                           l, s, close_exp genv fenv funs defs self e) cases
                  in
                     make_exp loc (Match (a, cases)))
       | MatchDTuple (a, cases) ->
            close_flush genv fenv defs self pos loc (fun self ->
            close_cases genv fenv defs self pos loc cases (fun self cases ->
                  let cases =
                     List.map (fun (l, a_opt, e) ->
                           l, a_opt, close_exp genv fenv funs defs self e) cases
                  in
                     make_exp loc (MatchDTuple (a, cases))))
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            close_flush genv fenv defs self pos loc (fun self ->
                  let e1 =
                     close_def genv fenv defs self pos loc v2 (fun defs self ->
                           close_exp genv fenv funs defs self e1)
                  in
                  let e2 = close_exp genv fenv funs defs self e2 in
                     make_exp loc (TypeCase (a1, a2, v1, v2, e1, e2)))
       | LetAlloc (v, op, e) ->
            close_alloc_op genv fenv defs self pos loc op (fun self op ->
                  make_exp loc (LetAlloc (v, op,
                            close_alloc genv fenv defs pos loc v (fun defs ->
                                  close_exp genv fenv funs defs self e))))
       | LetSubscript (op, v1, ty, v2, a, e) ->
            close_atom genv fenv defs self pos loc v2 (fun self v2 ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  make_exp loc (LetSubscript (op, v1, ty, v2, a,
                                close_def genv fenv defs self pos loc v1 (fun defs self ->
                                      close_exp genv fenv funs defs self e)))))
       | SetSubscript (op, label, v, a1, ty, a2, e) ->
            close_atom genv fenv defs self pos loc v (fun self v ->
            close_atom genv fenv defs self pos loc a1 (fun self a1 ->
            close_atom genv fenv defs self pos loc a2 (fun self a2 ->
                  make_exp loc (SetSubscript (op, label, v, a1, ty, a2,
                                close_exp genv fenv funs defs self e)))))
       | LetGlobal (op, v, ty, l, e) ->
            make_exp loc (LetGlobal (op, v, ty, l, close_exp genv fenv funs defs self e))
       | SetGlobal (op, label, v, ty, a, e) ->
            close_atom genv fenv defs self pos loc a (fun self a ->
                  make_exp loc (SetGlobal (op, label, v, ty, a, close_exp genv fenv funs defs self e)))
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            close_atom genv fenv defs self pos loc v1 (fun self v1 ->
            close_atom genv fenv defs self pos loc a1 (fun self a1 ->
            close_atom genv fenv defs self pos loc v2 (fun self v2 ->
            close_atom genv fenv defs self pos loc a2 (fun self a2 ->
            close_atom genv fenv defs self pos loc a3 (fun self a3 ->
                  make_exp loc (Memcpy (op, label, v1, a1, v2, a2, a3, close_exp genv fenv funs defs self e)))))))
       | Call (label, f, args, e) ->
            make_exp loc (Call (label, f, args, close_exp genv fenv funs defs self e))
       | Assert (label, pred, e) ->
            close_pred genv fenv defs self pos loc pred (fun self pred ->
                  make_exp loc (Assert (label, pred, close_exp genv fenv funs defs self e)))
       | Debug (info, e) ->
            make_exp loc (Debug (info, close_exp genv fenv funs defs self e))

(*
 * Close a tailcall.
 * If this is a function we know about, add the extra arguments.
 *)
and close_tailcall genv fenv funs pos loc label f args =
   let pos = string_pos "close_tailcall" pos in
   let f = var_of_fun pos f in
   let vars =
      try (SymbolTable.find funs f).live_args with
         Not_found ->
            SymbolSet.empty
   in
   let vars = SymbolSet.to_list vars in
   let vars = List.map (fun v -> AtomVar v) vars in
      vars @ args

(*
 * Close a function.
 *)
let close_fun genv fenv funs f (loc, ty_vars, ty, vars, e) =
   let pos = string_pos "close_fun" (var_exp_pos f) in
   let { live_args = args;
         live_defs = defvars;
         live_self = self
       } = SymbolTable.find funs f
   in

   (* Add all the frame arguments *)
   let dvars, dframes =
      List.fold_left (fun (dvars, dframes) v ->
            let frames =
               try fenv_lookup_var_exn fenv v with
                  Not_found ->
                     []
            in
            let dvars =
               List.fold_left (fun dvars (frame, label, ty) ->
                     SymbolTable.add dvars v (frame, label, ty)) dvars frames
            in
            let dframes =
               if fenv_mem_var_frame fenv v then
                  SymbolSet.add dframes v
               else
                  dframes
            in
               dvars, dframes) (SymbolTable.empty, SymbolSet.empty) vars
   in
   let dframes =
      SymbolSet.fold (fun dframes v ->
            if fenv_mem_var_frame fenv v then
               SymbolSet.add dframes v
            else
               dframes) dframes args
   in
   let defs =
      { def_frames = dframes;
        def_vars = dvars
      }
   in

   (*
    * Close the body.
    *)
   let self = List.fold_left SymbolSet.remove self vars in
   let e =
      close_defs genv fenv defs self pos loc defvars (fun defs self ->
            close_exp genv fenv funs defs self e)
   in

   (* Add the extra arguments *)
   let args = SymbolSet.to_list args in
   let tyl = List.map (fun v -> genv_lookup_var genv pos v) args in
   let ty_args, ty_res = dest_fun_type genv pos ty in
   let ty_args = tyl @ ty_args in
   let vars = args @ vars in
   let ty = close_fun_type (TyFun (ty_args, ty_res)) in
      loc, ty_vars, ty, vars, e

(*
 * Add the new frame fields.
 *)
let close_frames genv fenv frames =
   let { fenv_ty_fields = fields } = fenv in
      SymbolTable.fold (fun frames frame fields1 ->
            SymbolTable.filter_add frames frame (fun fields2 ->
                  let pos = string_pos "close_frames" (var_exp_pos frame) in
                  let vars2, fields2 =
                     match fields2 with
                        Some fields2 ->
                           fields2
                      | None ->
                           raise (FirException (pos, UnboundVar frame))
                  in
                  let fields =
                     List.fold_left (fun fields (field, subfield, ty) ->
                           let size = sizeof_type genv pos 0 ty in
                              SymbolTable.add fields field [subfield, ty, size]) fields2 fields1
                  in
                     vars2, fields)) frames fields

(*
 * Close all the funs in the program
 *)
let close_prog genv fenv funs prog =
   let { prog_funs = funtab;
         prog_frames = frames
       } = prog
   in
   let funtab = SymbolTable.mapi (close_fun genv fenv funs) funtab in
   let frames = close_frames genv fenv frames in
      { prog with prog_funs = funtab;
                  prog_frames = frames
      }

(************************************************************************
 * GLOBAL FUNCTION
 ************************************************************************)

let debug_live s funs =
   if debug debug_fir_closure then
      begin
         Format.printf "@[<hv 3>*** FIR: closure: %s:@ " s;
         print_live funs;
         Format.printf "@]@."
      end

(*
 * Close a program.
 *)
let close_prog prog =
   (* Rename all the frame vars *)
   let prog = frename_prog prog in
   let _ =
      if debug debug_fir_closure then
         debug_prog "closure framing" prog
   in

   (* Get variable types *)
   let genv = prog_genv prog in

   (* Build a trace of only the local functions *)
   let cgraph = build_callgraph prog in
   let trace = build_loop cgraph in
   let funs = prog.prog_funs in
   let nodes = clean_callgraph funs cgraph in
   let trace = clean_trace funs trace in

   (* Liveness analysis *)
   let funs = live_prog prog trace in
   let _ = debug_live "Initial liveness" funs in

   (* Compute all extra frame vars *)
   let fenv, funs = frame_prog genv funs prog in
   let _ = debug_live "Framed" funs in

   (* Latest-point analysis *)
   let funs = favail_fixpoint funs nodes trace in
   let _ = debug_live "FAvail" funs in

   (* Optimal-point analysis *)
   let funs = ravail_fixpoint funs nodes trace in
   let _ = debug_live "RAvail" funs in

   (* Assign expressions to functions *)
   let funs = assign_funs funs nodes in
   let _ = debug_live "After assignment" funs in

   (* Close the program *)
   let prog = close_prog genv fenv funs prog in
   let _ =
      if debug debug_fir_closure then
         debug_prog "nonstandard closure conversion" prog;
   in
      (* Now, finally, we can rename the vars *)
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
