(*
 * Optimization utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_env
open Fir_exn
open Fir_type

(************************************************************************
 * WORKLISTS
 ************************************************************************)

module Worklist = struct
  type t =
    { wq: var Queue.t;
      mutable ws : SymbolSet.t }

  let create () = { wq = Queue.create (); ws = SymbolSet.empty }

  let is_empty wl = SymbolSet.is_empty wl.ws

  let add wl v =
    if not (SymbolSet.mem wl.ws v) then (
      wl.ws <- SymbolSet.add wl.ws v;
      Queue.add v wl.wq)

  let take wl =
    let v = Queue.take wl.wq in
      wl.ws <- SymbolSet.remove wl.ws v;
      v

  let of_list l =
    let wl = create () in
      List.iter (add wl) l;
      wl
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
