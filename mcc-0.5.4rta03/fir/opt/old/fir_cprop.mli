(* Fir interprocedural constant propogation
 * Geoffrey Irving
 * $Id: fir_cprop.mli,v 1.1 2002/06/03 19:39:22 justins Exp $ *)

open Fir

val cprop_prog : Fir_callgraph.t -> prog -> prog
