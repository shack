(*
 * Alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol

open Fir
open Fir_exn
open Fir_type
open Fir_loop
open Fir_print
open Fir_state
open Fir_algebra
open Fir_standardize

open Sizeof

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Loop information for a variable.
 *
 * LoopInduction (v, op, a):
 *    The variable is an induction variable,
 *    offset by the loop-invariant expression a
 *    each time around the loop.
 *
 * LoopInvariant a:
 *    The value is loop-invariant
 *
 * LoopUnknown
 *    Don't know anything about the value
 *)
type lclass =
   LoopInduction of var * binop * etree
 | LoopInvariant of etree
 | LoopUnknown

(*
 * Alias information for an aribtrary memory block:
 *    block_mutable: block is mutable
 *    block_size: size of the block in bytes
 *    block_alias: the possible points of allocation
 *    block_default: what we know about default entries
 *    block_data: alias information for any elements we know
 *)
type bindex =
   IndexInt of int
 | IndexVar of var

type bclass =
   { block_mutable : bool;
     block_size : vclass;
     block_alias : SymbolSet.t;
     block_default : vclass;
     block_data : (bindex * vclass) list
   }

(*
 * This is a class for a variable.
 *
 * VarInduction (g, v, op, a) means:
 *    g: the loop the variable belongs to
 *    v: is the basice induction var
 *    op: is the addition operation
 *    a: is the induction increment
 *
 * VarDerived (g, v, op, a1, a2):
 *    g: the loop the variable belongs to
 *    op: is the addition operation
 *    the value is (v * a1 + a2)
 *
 * VarInvariant:
 *    the value is loop invariant
 *
 * VarBlock (v, props):
 *    the value is a memory block with the given properties
 *
 * VarUnknown:
 *    we don't know anything about the var
 *)
and vclass =
   VarInduction of var * var * binop * etree
 | VarDerived of var * var * binop * etree * etree
 | VarInvariant of etree
 | VarBlock of var * bclass
 | VarFunction of SymbolSet.t
 | VarVoid
 | VarUnknown

(*
 * This is the info we keep for each function.
 *    fun_loop: the name of the loop the function belongs to
 *    fun_vars: the parameters of the function
 *    fun_icall: variable classes from inner calls
 *    fun_ocall: variable classes from outer calls
 *)
type fclass =
   { fun_nest : var list;
     fun_vars : var list;
     fun_icall : vclass list;
     fun_ocall : vclass list
   }

(*
 * Alias environment.
 *)
type aenv =
   { aenv_vars : vclass SymbolTable.t;
     aenv_funs : fclass SymbolTable.t;
     aenv_fun  : var
   }

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print the loop class.
 *)
let print_lclass a =
   match a with
      LoopInduction (v, op, e) ->
         Format.printf "@[<hv 3><ind>(";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_space ();
         print_etree e;
         Format.printf ")@]"
    | LoopInvariant e ->
         Format.printf "@[<hv 3><inv>(";
         print_etree e;
         Format.printf ")@]"
    | LoopUnknown ->
         Format.printf "<unknown>"

(*
 * Print the block entry.
 *)
let print_bindex = function
   IndexInt i ->
      Format.print_int i
 | IndexVar v ->
      print_symbol v

let rec print_bclass block =
   let { block_mutable = mutablep;
         block_size = size;
         block_alias = alias;
         block_default = default;
         block_data = data
       } = block
   in
      Format.printf "@[<v 0>@[<v 3><block> {@ mutable : %b;@ size = " mutablep;
      print_vclass size;
      Format.printf ";@ @[<hv 3>alias = {";
      SymbolSet.iter (fun v ->
            Format.print_space();
            print_symbol v) alias;
      Format.printf " }@];@ default = ";
      print_vclass default;
      Format.printf ";@ @[<hv 3>entries = {";
      List.iter (fun (i, v) ->
            Format.printf "@ @[<hv 3>";
            print_bindex i;
            Format.printf " =@ ";
            print_vclass v;
            Format.printf "@]") data;
      Format.printf " }@]@]@ }@]"

(*
 * Print the loop environment entries.
 *)
and print_vclass a =
   match a with
      VarInvariant a ->
         Format.print_string "<inv>(";
         print_etree a;
         Format.print_string ")"
    | VarVoid ->
         Format.print_string "<void>"
    | VarUnknown ->
         Format.print_string "<unknown>"
    | VarInduction (g, v, op, a1) ->
         Format.open_hvbox 3;
         Format.print_string "<ind>(loop=";
         print_symbol g;
         Format.print_string ", ";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_space ();
         print_etree a1;
         Format.print_string ")";
         Format.close_box ()
    | VarDerived (g, v, op, a1, a2) ->
         Format.open_hvbox 3;
         Format.print_string "<der>(loop=";
         print_symbol g;
         Format.print_string ", ";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_string ",";
         Format.print_space ();
         print_etree a1;
         Format.print_string ",";
         Format.print_space ();
         print_etree a2;
         Format.print_string ")";
         Format.close_box ()
    | VarBlock (v, bclass) ->
         print_bclass bclass
    | VarFunction funs ->
         Format.print_string "<fun>{";
         SymbolSet.iter (fun v ->
               Format.print_string " ";
               print_symbol v;
               Format.print_string ",") funs;
         Format.print_string " }"

let print_venv venv =
   Format.open_vbox 3;
   Format.printf "Variables:";
   SymbolTable.iter (fun v a ->
         Format.print_space ();
         Format.open_hvbox 3;
         print_symbol v;
         Format.print_string " :";
         Format.print_space ();
         print_vclass a;
         Format.close_box ()) venv;
   Format.close_box ();
   Format.print_newline ()

let print_fenv fenv =
   Format.printf "@[<v 3>Functions:";
   SymbolTable.iter (fun f { fun_nest = nest;
                             fun_vars = vars;
                             fun_icall = icall;
                             fun_ocall = ocall
                         } ->
         Format.printf "@ @[<v 3>";
         print_symbol f;
         Format.printf ":@ @[<hv 3>loop-nest =";
         List.iter (fun v ->
               Format.print_space ();
               print_symbol v) nest;
         Format.printf "@];@ @[<hv 3>vars =";
         List.iter (fun v ->
               Format.print_space ();
               print_symbol v) vars;
         Format.printf "@];@ @[<hv 3>icall =";
         List.iter (fun v ->
               Format.print_space ();
               print_vclass v) icall;
         Format.printf "@];@ @[<hv 3>ocall =";
         List.iter (fun v ->
               Format.print_space ();
               print_vclass v) ocall;
         Format.printf "@];@]@]") fenv;
   Format.printf "@]"

let print_aenv aenv =
   Format.print_space ();
   print_venv aenv.aenv_vars;
   Format.print_space ();
   print_fenv aenv.aenv_funs

(************************************************************************
 * LOOP ANALYSIS
 ************************************************************************)

(*
 * Loop environment.
 *)
let lenv_add_var lenv v e =
   { lenv with loop_vars = SymbolTable.add lenv.loop_vars v e }

let lenv_lookup_var lenv pos v =
   try SymbolTable.find lenv.loop_vars v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let lenv_lookup_atom lenv pos a =
   match a with
      AtomVar v ->
         lenv_lookup_var lenv pos v
    | _ ->
         Some (ETConst a)

(*
 * Check if a function is in the loop.
 *)
let lenv_mem lenv f =
   SymbolTable.mem lenv.loop_funs f

(*
 * Unify two argument lists.
 *)
let lenv_unify_arg arg1 arg2 =
   if arg1 = arg2 then
      arg1
   else
      None

let rec lenv_unify_args args1 args2 =
   match args1, args2 with
      arg1 :: args1, arg2 :: args2 ->
         let args = lenv_unify_args args1 args2 in
         let arg = lenv_unify_arg arg1 arg2 in
            arg :: args
    | [], _ ->
         args2
    | _ :: _, [] ->
         raise (Invalid_argument "lenv_self_call")

(*
 * Unify the values in a self call.
 *)
let lenv_self_call lenv args =
   { lenv with loop_self = lenv_unify_args lenv.loop_self args }

(*
 * Unify the arguments in an inner call.
 *)
let lenv_inner_call lenv f args =
   let { loop_funs = funs;
         loop_vars = venv
       } = lenv
   in
   let _, _, vars, _ = SymbolTable.find funs f in
      List.fold_left2 (fun lenv v a2 ->
            let arg =
               try
                  let a1 = SymbolTable.find venv v in
                     lenv_unify_arg a1 a2
               with
                  Not_found ->
                     a2
            in
               lenv_add_var lenv v arg) lenv vars args

(*
 * Unify the arguments in an outer call.
 *)
let lenv_outer_call lenv f args2 =
   let args =
      try
         let args1 = SymbolTable.find lenv.loop_calls f in
            lenv_unify_args args1 args2
      with
         Not_found ->
            args2
   in
      { lenv with loop_calls = SymbolTable.add lenv.loop_calls f args }

(*
 * Scan a function in the loop.
 *)
let rec loop_exp lenv e =
   let pos = string_pos "loop_exp" (exp_pos e) in
      match e with
         LetUnop (v, _, op, a, e) ->
            loop_unop_exp lenv pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            loop_binop_exp lenv pos v op a1 a2 e
       | TailCall (f, args) ->
            loop_tailcall_exp lenv pos f args
       | Match (a, cases) ->
            loop_match_exp lenv pos a cases
       | TypeCase (_, _, _, v, e1, e2) ->
            loop_typecase_exp lenv pos v e1 e2
       | LetExt (v, _, _, _, _, e)
       | LetAlloc (v, _, e)
       | LetSubscript (_, v, _, _, _, e) ->
            let venv = lenv_add_var lenv v None in
               loop_exp lenv e
       | Assert (_, _, e)
       | SetGlobal (_, _, _, _, e)
       | SetSubscript (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, e)
       | Debug (_, e) ->
            loop_exp lenv e
       | SpecialCall _ ->
            lenv

(*
 * Catch simple unops.
 *)
and loop_unop_exp lenv pos v op a e =
   let pos = string_pos "loop_unop_exp" pos in
   let a = lenv_lookup_atom lenv pos a in
   let a =
      match op, a with
         IdOp, _ ->
            a
       | _, Some a ->
            Some (canonicalize_etree (ETUnop (op, a)))
       | _ ->
            None
   in
   let lenv = lenv_add_var lenv v a in
      loop_exp lenv e

(*
 * Binary operations.
 *)
and loop_binop_exp lenv pos v op a1 a2 e =
   let pos = string_pos "loop_binop_exp" pos in
   let a1 = lenv_lookup_atom lenv pos a1 in
   let a2 = lenv_lookup_atom lenv pos a2 in
   let a =
      match a1, a2 with
         Some a1, Some a2 ->
            Some (canonicalize_etree (ETBinop (op, a1, a2)))

       | _ ->
            None
   in
   let lenv = lenv_add_var lenv v a in
      loop_exp lenv e

(*
 * Match expression.
 *)
and loop_match_exp lenv pos a cases =
   let pos = string_pos "loop_match_exp" pos in
      List.fold_left (fun lenv (_, e) ->
            loop_exp lenv e) lenv cases

(*
 * Typecase.
 *)
and loop_typecase_exp lenv pos v e1 e2 =
   let pos = string_pos "loop_typecase_exp" pos in
   let lenv = lenv_add_var lenv v None in
   let lenv = loop_exp lenv e1 in
   let lenv = loop_exp lenv e2 in
      lenv

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and loop_tailcall_exp lenv pos f args =
   let pos = string_pos "loop_tailcall_exp" pos in
   let args = List.map (lenv_lookup_atom lenv pos) args in
      if Symbol.eq f lenv.loop_fun then
         lenv_self_call lenv args
      else if lenv_mem lenv f then
         lenv_inner_call lenv f args
      else
         lenv_outer_call lenv f args

(*
 * When performing loop calculations,
 * collect all the functions in the loop
 * trace as well as nested loops.
 *)
let rec loop_split funs loops trace =
   match trace with
      Elem f :: trace ->
         let funs = f :: funs in
            loop_split funs loops trace
    | Trace ((Elem _ :: _) as trace1) :: trace2 ->
         let funs' = loop_split [] [] trace1 in
            loop_split funs (funs' :: loops) trace2
    | Trace trace1 :: trace2 ->
         loop_split funs loops (trace1 @ trace2)
    | [] ->
         LoopBlock (List.rev funs, loops)

(*
 * Compute the fixpoint for one level of the loop.
 *)
let rec loop_fixpoint funs (LoopBlock (vars, loops)) =
   (* Compute fixpoints for inner loops *)
   let loops = loop_fixpoint funs loops in

   (* Create an environment for this level *)
   let f = List.hd funs in
   let funs' =
      List.fold_left (fun funs' v ->
            let def = SymbolTable.find funs v in
               SymbolTable.add funs' v def) SymbolTable.empty vars
   in

   (* Loop parameters are considered invariant *)
   let _, _, vars', _ = SymbolTable.find funs f in
   let venv =
      List.fold_left (fun venv v ->
            SymbolTable.add venv v (Some (ETVar v))) SymbolTable.empty vars'
   in

   (* Initial environment *)
   let lenv =
      { lenv_fun = f;
        lenv_funs = funs';
        lenv_self = [];
        lenv_vars = venv;
        lenv_calls = SymbolTable.empty
      }
   in

   (* Now, iterate over all the functions in this loop *)
   let rec fixpoint lenv =
      let changed, lenv = loop_step lenv in
         if changed then
            fixpoint lenv
         else
            lenv
   in
   let lenv = fixpoint lenv in
      lenv

(*
 * Collect loop information for the program.
 *)
let loop_prog prog =
   let { prog_funs = funs } = prog in

   (* Get the trace *)
   let trace = build_loop prog in
   let trace = loop_split [] [] trace in
   let _ =
      if debug debug_loop then
         print_loop_trace trace
   in
      trace

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Make a constant of an integer.
 *)
let const_of_int i =
   VarInvariant (ETConst (AtomRawInt (Rawint.of_int precision_native_int false i)))

(*
 * A variable environment.
 *)
let aenv_add_var aenv v info =
   { aenv with aenv_vars = SymbolTable.add aenv.aenv_vars v info }

let aenv_lookup_var aenv pos v =
   try SymbolTable.find aenv.aenv_vars v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let aenv_lookup_atom aenv pos a =
   match a with
      AtomVar v ->
         aenv_lookup_var aenv pos v
    | _ ->
         VarInvariant (ETConst a)

let aenv_add_fun aenv f info =
   { aenv with aenv_funs = SymbolTable.add aenv.aenv_funs f info }

let aenv_lookup_fun aenv f =
   SymbolTable.find aenv.aenv_funs f

(************************************************************************
 * UNIFICATION
 ************************************************************************)

(*
 * Array sizes are always int32.
 *)
let min_size_op = MinRawIntOp (precision_subscript, signed_subscript)

(*
 * Compare two indexes.
 *)
let compare_bindex i1 i2 =
   match i1, i2 with
      IndexInt i1, IndexInt i2 ->
         i1 - i2
    | IndexVar v1, IndexVar v2 ->
         Symbol.compare v1 v2
    | IndexInt _, IndexVar _ ->
         -1
    | IndexVar _, IndexInt _ ->
         1

(*
 * Unify the data elements in an array.
 * We assume the elements are sorted (variables are always last).
 * Through out knowledge not present in both lists.
 *)
let rec unify_data data1 data2 =
   match data1, data2 with
      (i1, v1) :: data1', (i2, v2) :: data2' ->
         let cmp = compare_bindex i1 i2 in
            if cmp = 0 then
               (i1, unify_vclass v1 v2) :: unify_data data1' data2'
            else if cmp > 0 then
               unify_data data1 data2'
            else (* cmp < 0 *)
               unify_data data1' data2
    | _ ->
         []

(*
 * Unify the types of two arrays.
 *)
and unify_block b1 b2 =
   let { block_mutable = m1;
         block_size = size1;
         block_alias = alias1;
         block_default = default1;
         block_data = data1
       } = b1
   in
   let { block_mutable = m2;
         block_size = size2;
         block_alias = alias2;
         block_default = default2;
         block_data = data2
       } = b2
   in
      { block_mutable = m1 && m2;
        block_size = unify_minimum size1 size2;
        block_alias = SymbolSet.union alias1 alias2;
        block_default = unify_minimum default1 default2;
        block_data = unify_data data1 data2
      }

(*
 * Take the minimum of two sizes.
 * This is only used to compute the block size.
 * We aren't too accurate here.
 *)
and unify_minimum v1 v2 =
   if v1 = v2 then
      v1
   else
      match v1, v2 with
         VarVoid, v
       | v, VarVoid ->
            v

         (* Invariants *)
       | VarInvariant e1, VarInvariant e2 ->
            VarInvariant (ETBinop (min_size_op, e1, e2))

         (* We ignore the rest, including induction variables *)
       | _ ->
            VarUnknown

(*
 * Unify two variable classes.
 * This is an outer call, so we don't create induction variables.
 *)
and unify_vclass v1 v2 =
   let v =
      if v1 = v2 then
         v1
      else
         match v1, v2 with
            (* Not set yet *)
            VarVoid, v
          | v, VarVoid ->
               v

           (* Functions *)
          | VarFunction s1, VarFunction s2 ->
               VarFunction (SymbolSet.union s1 s2)
          | VarFunction _, _
          | _, VarFunction _ ->
               VarUnknown

            (* Arrays *)
          | VarBlock (v1, b1), VarBlock (v2, b2)
            when Symbol.eq v1 v2 ->
               VarBlock (v1, unify_block b1 b2)
          | VarBlock _, _
          | _, VarBlock _ ->
               VarUnknown

            (* All other combinations destroy information *)
          | _ ->
               VarUnknown
   in
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>Unify_outer_vclass:@ v1=";
            print_vclass v1;
            Format.printf "@ v2=";
            print_vclass v2;
            Format.printf "@ v=";
            print_vclass v;
            Format.printf "@]@."
         end;
      v

(*
 * Unify the arguments of a call.
 * This is a normal unification: the variables should
 * have the same class.
 *)
let unify_call call1 call2 =
   let changed, call =
      List.fold_left2 (fun (changed, call) v1 v2 ->
            let v = unify_vclass v1 v2 in
            let changed = changed || v <> v1 in
               changed, v :: call) (false, []) call1 call2
   in
      changed, List.rev call

(*
 * Unify a variable in a loop header.
 *    v: the param
 *    iv: the inner class
 *    ov: the outer class
 *)
let unify_loop_var aenv pos g v iv ov =
   if iv = VarVoid then
      match ov with
         VarInduction _
       | VarDerived _
       | VarInvariant _
       | VarBlock _
       | VarFunction _ ->
            (*
             * If the inner var hasn't been initialized,
             * we predict that it will be loop invariant.
             * This means we just take the outer class
             * and pass it through the loop, and predict
             * it will come out the same.
             *)
            ov
       | VarVoid
       | VarUnknown ->
            (*
             * If the inner loop is not initialized,
             * and the value from the outer loop is totally
             * bogus, assume the value may be loop invariant
             * in this loop.
             *)
            VarInvariant (ETVar v)

   else if iv = ov then
      (*
       * At this point, we have rounded the loop at least once
       * and the value is the same.  Just leave it that way:
       * the value is loop invariant.
       *)
      iv

   else
      (*
       * Otherwise, we hope for the best, and assume that the
       * value is an induction variable.  These are the cases
       * where we have assumed the value is loop invariant.
       *)
      match aenv_lookup_var aenv pos v, iv with
         VarInvariant (ETVar v1), VarInvariant (ETBinop (PlusIntOp as op, ETConst c1, ETVar v2))
       | VarInvariant (ETVar v1), VarInvariant (ETBinop (PlusRawIntOp _ as op, ETConst c1, ETVar v2))
         when Symbol.eq v1 v && Symbol.eq v2 v ->
            VarInduction (g, v, op, ETConst c1)

       | VarInduction (g1, v1, op1, e1), VarDerived (g2, v2, op2, e2, e3) when
         Symbol.eq g1 g
         && Symbol.eq g2 g
         && Symbol.eq v1 v
         && Symbol.eq v2 v
         && (e1 = e3)
         && (op1 = op2) ->
            iv

       | _ ->
            (*
             * In this final case, the inner var is none of the above.
             * It must have been initialized from an outer value, so we
             * give it a local loop invariant value.
             *)
            VarInvariant (ETVar v)

(*
 * Debug version of loop param analysis.
 *)
let unify_loop_var aenv pos g v iv ov =
   let vc = unify_loop_var aenv pos g v iv ov in
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>Unify_loop_var:@ loop=";
            print_symbol g;
            Format.printf ";@ var=";
            print_symbol v;
            Format.printf ";@ inner=";
            print_vclass iv;
            Format.printf ";@ outer=";
            print_vclass ov;
            Format.printf ";@ new=";
            print_vclass vc;
            Format.printf "@]@."
         end;
      vc

(*
 * Unify the classes in a loop header.
 *)
let unify_loop_header aenv pos g info =
   let { fun_vars = vars;
         fun_icall = icall;
         fun_ocall = ocall
       } = info
   in

   (* Add updated values for all the loop params *)
   let aenv =
      Mc_list_util.fold_left3 (fun aenv v iv ov ->
            let vc = unify_loop_var aenv pos g v iv ov in
               aenv_add_var aenv v vc) aenv vars icall ocall
   in

   (* Make sure the inner values are undefined *)
   let icall = List.map (fun _ -> VarVoid) vars in
   let info = { info with fun_icall = icall } in
      aenv_add_fun aenv g info

(************************************************************************
 * VARIABLE CLASSIFICATION
 ************************************************************************)

(*
 * Scan a function in the loop.
 *)
let rec alias_exp aenv e =
   let pos = string_pos "alias_exp" (exp_pos e) in
      match e with
         LetUnop (v, _, op, a, e) ->
            alias_unop_exp aenv pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            alias_binop_exp aenv pos v op a1 a2 e
       | TailCall (f, args) ->
            alias_tailcall_exp aenv pos f args
       | Match (a, cases) ->
            alias_match_exp aenv pos a cases
       | TypeCase (_, _, _, v, e1, e2) ->
            alias_typecase_exp aenv pos v e1 e2
       | LetExt (v, _, _, _, args, e) ->
            alias_ext_exp aenv pos v args e
       | LetAlloc (v, op, e) ->
            alias_alloc_exp aenv pos v op e
       | LetSubscript (_, v1, _, v2, a, e) ->
            alias_subscript_exp aenv pos v1 v2 a e
       | Assert (label, pred, e) ->
            alias_assert_exp aenv pos label pred e
       | SetSubscript (_, v, a1, _, a2, e) ->
            alias_set_subscript_exp aenv pos v a1 a2 e
       | Memcpy (_, v1, a1, v2, a2, a3, e) ->
            alias_memcpy_exp aenv pos v1 a1 v2 a2 a3 e
       | SetGlobal (_, _, _, _, e)
       | Debug (_, e) ->
            alias_exp aenv e
       | SpecialCall _ ->
            false, aenv

(*
 * Catch simple unops.
 *)
and alias_unop_exp aenv pos v op a e =
   let pos = string_pos "alias_unop_exp" pos in
   let a = aenv_lookup_atom aenv pos a in
   let a =
      match op, a with
         IdOp, _ ->
            a
       | _, VarInvariant a ->
            VarInvariant (canonicalize_etree (ETUnop (op, a)))
       | _ ->
            VarUnknown
   in
   let aenv = aenv_add_var aenv v a in
      alias_exp aenv e

(*
 * Binary operations.
 *)
and alias_binop_exp aenv pos v op_orig a1 a2 e =
   let pos = string_pos "alias_binop_exp" pos in
   let a1 = aenv_lookup_atom aenv pos a1 in
   let a2 = aenv_lookup_atom aenv pos a2 in
   let a =
      match op_orig, a1, a2 with
         op, VarInvariant a1, VarInvariant a2 ->
            VarInvariant (canonicalize_etree (ETBinop (op, a1, a2)))

       | PlusIntOp, VarInvariant a2, VarInduction (g, v, op, _)
       | PlusIntOp, VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, ETConst (AtomInt 1), a2)

       | PlusRawIntOp (pre, signed), VarInvariant a2, VarInduction (g, v, op, _)
       | PlusRawIntOp (pre, signed), VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, ETConst (AtomRawInt (Rawint.of_int pre signed 1)), a2)

       | MinusIntOp, VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, ETConst (AtomInt 1), canonicalize_etree (ETUnop (UMinusIntOp, a2)))

       | MinusRawIntOp (pre, signed), VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op,
                        ETConst (AtomRawInt (Rawint.of_int pre signed 1)),
                        canonicalize_etree (ETUnop (UMinusRawIntOp (pre, signed), a2)))

       | MulIntOp, VarInvariant a2, VarInduction (g, v, op, _)
       | MulIntOp, VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, a2, ETConst (AtomInt 0))

       | MulRawIntOp (pre, signed), VarInvariant a2, VarInduction (g, v, op, _)
       | MulRawIntOp (pre, signed), VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, a2, ETConst (AtomRawInt (Rawint.of_int pre signed 0)))

       | PlusIntOp, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | PlusIntOp, VarDerived (g, v, op, a1, a2), VarInvariant a3
       | PlusRawIntOp _, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | PlusRawIntOp _, VarDerived (g, v, op, a1, a2), VarInvariant a3 ->
            VarDerived (g, v, op, a1, canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | MinusIntOp, VarDerived (g, v, op, a1, a2), VarInvariant a3
       | MinusRawIntOp _, VarDerived (g, v, op, a1, a2), VarInvariant a3 ->
            VarDerived (g, v, op, a1, canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | MulIntOp, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | MulIntOp, VarDerived (g, v, op, a1, a2), VarInvariant a3
       | MulRawIntOp _, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | MulRawIntOp _, VarDerived (g, v, op, a1, a2), VarInvariant a3 ->
            VarDerived (g, v, op,
                        canonicalize_etree (ETBinop (op_orig, a1, a3)),
                        canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | _ ->
            VarUnknown
   in
   let aenv = aenv_add_var aenv v a in
      alias_exp aenv e

(*
 * Match expression.
 *)
and alias_match_exp aenv pos a cases =
   let pos = string_pos "alias_match_exp" pos in
      List.fold_left (fun (changed, aenv) (_, e) ->
            let changed', aenv = alias_exp aenv e in
               changed || changed', aenv) (false, aenv) cases

(*
 * Typecase.
 *)
and alias_typecase_exp aenv pos v e1 e2 =
   let pos = string_pos "alias_typecase_exp" pos in
   let aenv = aenv_add_var aenv v VarUnknown in
   let changed1, aenv = alias_exp aenv e1 in
   let changed2, aenv = alias_exp aenv e2 in
      changed1 || changed2, aenv

(*
 * External call.
 * NOTE: we assume external functions destroy their array arguments.
 *)
and alias_ext_exp aenv pos v args e =
   let pos = string_pos "alias_ext_exp" pos in
   let aenv = aenv_add_var aenv v VarUnknown in
      alias_exp aenv e

(*
 * Allocation.
 *)
and alias_alloc_exp aenv pos v op e =
   let pos = string_pos "alias_alloc_exp" pos in
   let size, default, data = alias_alloc_op aenv pos op in
   let block =
      { block_mutable = true;
        block_size = size;
        block_alias = SymbolSet.singleton v;
        block_default = default;
        block_data = data
      }
   in
   let aenv = aenv_add_var aenv v (VarBlock (v, block)) in
      alias_exp aenv e

(*
 * Get the alias info for an allocation.
 *)
and alias_alloc_op aenv pos op =
   let pos = string_pos "alias_alloc_op" pos in
      match op with
         AllocTuple (_, _, args)
       | AllocUnion (_, _, _, args)
       | AllocArray (_, args) ->
            let data, size =
               List.fold_left (fun (data, index) a ->
                     let info = aenv_lookup_atom aenv pos a in
                     let data = (IndexInt index, info) :: data in
                     let index = index + sizeof_pointer in
                        data, index) ([], 0) args
            in
            let size = const_of_int size in
               size, VarUnknown, List.rev data
       | AllocVArray (_, _, a1, a2) ->
            let data = aenv_lookup_atom aenv pos a2 in
            let size = aenv_lookup_atom aenv pos a1 in
               size, data, []
       | AllocMalloc a ->
            let size = aenv_lookup_atom aenv pos a in
               size, VarUnknown, []

(*
 * Subscripting.
 *)
and alias_subscript_exp aenv pos v1 v2 a e =
   let pos = string_pos "alias_subscript_exp" pos in
   let aenv = aenv_add_var aenv v1 VarUnknown in
      alias_exp aenv e

and alias_set_subscript_exp aenv pos v a1 a2 e =
   let pos = string_pos "alias_set_subscript_exp" pos in
      alias_exp aenv e

and alias_memcpy_exp aenv pos v1 a1 v2 a2 a3 e =
   let pos = string_pos "alias_memcpy_exp" pos in
      alias_exp aenv e

(*
 * Assertions.
 *)
and alias_assert_exp aenv pos label pred e =
   let pos = string_pos "alias_assert_exp" pos in
      alias_exp aenv e

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and alias_tailcall_exp aenv pos f args =
   let pos = string_pos "alias_tailcall_exp" pos in
      try
         let info_call = aenv_lookup_fun aenv f in
         let info_self = aenv_lookup_fun aenv aenv.aenv_fun in
         let args = List.map (aenv_lookup_atom aenv pos) args in
         let f_called_loop = List.hd info_call.fun_nest in
         let changed, info =
            if List.mem f_called_loop info_self.fun_nest then
               (* The called function is not in a nested loop *)
               let _, icall = unify_call info_call.fun_icall args in
               let info = { info_call with fun_icall = icall } in
                  false, info
            else
               (* The called function is in a nested loop *)
               let changed, ocall = unify_call info_call.fun_ocall args in
               let info = { info_call with fun_ocall = ocall } in
                  changed, info
         in
            changed, aenv_add_fun aenv f info
      with
         Not_found ->
            false, aenv

(*
 * Classify all the funs in the trace.
 *)
let rec alias_trace funs nest fenv l =
   match l with
      Elem g ->
         (try
             let _, _, vars, _ = SymbolTable.find funs g in
             let call = List.map (fun _ -> VarVoid) vars in
             let info = { fun_nest = nest; fun_vars = vars; fun_icall = call; fun_ocall = call } in
                SymbolTable.add fenv g info
          with
             Not_found ->
                fenv)
    | Trace (Elem g :: l) ->
         let _, _, vars, _ = SymbolTable.find funs g in
         let call = List.map (fun _ -> VarVoid) vars in
         let nest = g :: nest in
         let info = { fun_nest = nest; fun_vars = vars; fun_icall = call; fun_ocall = call } in
         let fenv = SymbolTable.add fenv g info in
            alias_trace_list funs nest fenv l
    | Trace l ->
         alias_trace_list funs nest fenv l

and alias_trace_list funs nest fenv l =
   List.fold_left (alias_trace funs nest) fenv l

(*
 * Found a loop.
 *)
let alias_step aenv funs =
   SymbolTable.fold (fun (changed1, aenv) f (_, _, vars, e) ->
         let pos = string_pos "alias_step" (vexp_pos f) in
         let info =  aenv_lookup_fun aenv f in
         let { fun_nest = nest; fun_icall = icall } = info in
         let f_loop = List.hd nest in
         let aenv =
            if Symbol.eq f f_loop then
               (* This is a loop header, so unify the header *)
               unify_loop_header aenv pos f info
            else
               (* Otherwise, just carry forward the inner vars *)
               List.fold_left2 aenv_add_var aenv vars icall
         in
         let aenv = { aenv with aenv_fun = f } in
         let changed2, aenv = alias_exp aenv e in
            changed1 || changed2, aenv) (false, aenv) funs

let alias_prog prog =
   let { prog_funs = funs;
         prog_names = names;
         prog_export = export;
         prog_globals = globals;
         prog_types = tenv
       } = prog
   in

   (* Get the trace *)
   let trace = build_loop prog in
   let _ =
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>*** Alias trace:";
            Trace.print string_of_symbol trace;
            Format.printf "@]@."
         end
   in

   (* Collect all the fun info *)
   let root = new_symbol_string "root" in
   let info =
      { fun_nest = [root];
        fun_vars = [];
        fun_icall = [];
        fun_ocall = []
      }
   in
   let fenv = SymbolTable.add SymbolTable.empty root info in
   let fenv = alias_trace_list funs [root] fenv trace in

   (* Add all the globals to the aenv *)
   let aenv =
      { aenv_vars = SymbolTable.empty;
        aenv_funs = fenv;
        aenv_fun = root
      }
   in
   let aenv =
      SymbolTable.fold (fun aenv f _ ->
            aenv_add_var aenv f (VarFunction (SymbolSet.singleton f))) aenv funs
   in
   let aenv =
      SymbolTable.fold (fun aenv v _ ->
            aenv_add_var aenv v VarUnknown) aenv globals
   in
   let aenv =
      SymbolTable.fold (fun aenv v _ ->
            aenv_add_var aenv v VarUnknown) aenv names
   in

   (*
    * For exported functions, assign alias classes to all the vars.
    * We assume the worst, that there is arbitrary aliasing.  In fact,
    * we don't even trust the type system, except that we assume it
    * passes pointers where pointers are expected.
    *)
   let var_fun = VarFunction (SymbolSet.singleton (new_symbol_string "ext_fun")) in
   let v_block = new_symbol_string "block" in
   let var_block =
      { block_mutable = false;
        block_size = VarUnknown;
        block_alias = SymbolSet.singleton v_block;
        block_default = VarUnknown;
        block_data = []
      }
   in
   let aenv =
      SymbolTable.fold (fun aenv g _ ->
            if SymbolTable.mem export g then
               let _, ty, vars, _ = SymbolTable.find funs g in
               let pos = string_pos "Fir_alias.alias_prog.export" (vexp_pos g) in
               let _, ty_vars, _ = dest_fun_type tenv pos ty in
                  List.fold_left2 (fun aenv v ty ->
                        let rec var_of_type ty =
                           match tenv_expand tenv pos ty with
                              TyInt
                            | TyEnum _
                            | TyRawInt _
                            | TyFloat _
                            | TyVar _
                            | TyProject _
                            | TyDelayed ->
                                 VarUnknown
                            | TyFun _ ->
                                 var_fun
                            | TyUnion _
                            | TyTuple _
                            | TyArray _
                            | TyRawData
                            | TyPointer _
                            | TyCase _
                            | TyObject _ ->
                                 VarBlock (v, var_block)
                            | TyExists (_, ty)
                            | TyAll (_, ty)
                            | TyOption ty ->
                                 var_of_type ty
                            | TyApply _ ->
                                 raise (Invalid_argument "alias_prog.export")
                        in
                        let ty = var_of_type ty in
                           aenv_add_var aenv v ty) aenv vars ty_vars
            else
               aenv) aenv funs
   in

   (* Classify all the vars *)
   let rec fixpoint count aenv =
      let changed, aenv = alias_step aenv funs in
         if debug debug_loop then
            begin
               Format.printf "@[<v 3>*** Alias %d/%b:" count changed;
               print_aenv aenv;
               Format.printf "@]@."
            end;
         if changed then
            fixpoint (succ count) aenv
         else
            aenv
   in
   let aenv = fixpoint 1 aenv in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
