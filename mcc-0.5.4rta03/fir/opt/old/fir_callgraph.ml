(* Fir callgraph code
 * Geoffrey Irving
 * $Id: fir_callgraph.ml,v 1.3 2002/06/24 00:00:53 jyh Exp $ *)

(* See .mli for notes *)

open Symbol
open Fir
open Fir_exn
open Fir_type
open Fir_alias

(************************************** types *)

type node = var Digraph.node

type t =
  { cg_g : var Digraph.t;            (* actual graph *)
    cg_ft : node SymbolTable.t;      (* function -> node table *)
    cg_pft : var list SymbolTable.t; (* var -> possible function values table *)
    cg_entry : node;                 (* special nodes *)
    cg_exit : node
  }

let entry_sym = new_symbol_string "entry"
let exit_sym = new_symbol_string "exit"

(************************************** callgraph construction *)

let rec make_edges_expr alias cg funs n pft e =
  match e with
      LetUnop (_, _, _, _, e)
    | LetBinop (_, _, _, _, _, e)
    | LetExt (_, _, _, _, _, e)
    | LetAlloc (_, _, e)
    | Debug (_, e)
    | LetSubscript (_, _, _, _, _, e)
    | SetSubscript (_, _, _, _, _, e)
    | SetGlobal (_, _, _, _, e)
    | Memcpy (_, _, _, _, _, _, e)
    | LetAssert (_, _, _, _, e) ->
        make_edges_expr alias cg funs n pft e
    | Match (_, sel) ->
        List.fold_left (fun pft (_, e) -> make_edges_expr alias cg funs n pft e) pft sel
    | TypeCase (_, _, _, _, e1, e2) ->
        let pft = make_edges_expr alias cg funs n pft e1 in
           make_edges_expr alias cg funs n pft e2
    | TailCall (f, l) ->
        begin
        try
          Digraph.add_multi_edge n (SymbolTable.find cg.cg_ft f);
          pft
        with Not_found ->
          Digraph.add_multi_edge n cg.cg_exit;
          let i = List.length l in
          let pf = SymbolTable.fold (fun pf v _ ->
              if may_alias_fun alias i f v then (
                Digraph.add_multi_edge n (SymbolTable.find cg.cg_ft v);
                v :: pf)
              else pf)
            [] funs in
          let pft = SymbolTable.add pft f pf in
            pft
        end
    | SpecialCall (TailSysMigrate (_, aptr, aoff, f, args)) ->
        begin
        let l = aptr :: aoff :: args in
        try
          Digraph.add_multi_edge n (SymbolTable.find cg.cg_ft f);
          pft
        with Not_found ->
          Digraph.add_multi_edge n cg.cg_exit;
          let i = List.length l in
          let pf = SymbolTable.fold (fun pf v _ ->
              if may_alias_fun alias i f v then (
                Digraph.add_multi_edge n (SymbolTable.find cg.cg_ft v);
                v :: pf)
              else pf)
            [] funs in
          let pft = SymbolTable.add pft f pf in
            pft
        end
    | SpecialCall _ ->
        raise (FirException (ExnExp e, NotImplemented "make_edges_expr: SpecialCall not implemented"))

let make_edges_funs alias cg funs =
   let pft =
      SymbolTable.fold (fun pft v (_, _, _, e) ->
            let n = SymbolTable.find cg.cg_ft v in
               make_edges_expr alias cg funs n pft e) SymbolTable.empty funs
   in
      { cg with cg_pft = pft }

let make_edges_root cg funs =
   SymbolTable.iter (fun v _ ->
         let n = SymbolTable.find cg.cg_ft v in
            if Digraph.in_degree n = 0 then
               Digraph.add_multi_edge cg.cg_entry n) funs

let make_nodes g funs =
  SymbolTable.fold (fun ft v _ ->
      SymbolTable.add ft v (Digraph.add_node g v))
    SymbolTable.empty funs

let make_nodes_import ft g import =
   SymbolTable.fold (fun ft v { import_info = info } ->
         match info with
            ImportFun _ ->
               SymbolTable.add ft v (Digraph.add_node g v)
          | ImportGlobal ->
               ft) ft import

let make_edges_export cg tenv export =
   SymbolTable.iter (fun v { export_type = ty;
                             export_name = name
                         } ->
         if is_fun_type tenv (vexp_pos v) ty then
            try Digraph.add_multi_edge cg.cg_entry (SymbolTable.find cg.cg_ft v) with
               Not_found ->
                  raise (Invalid_argument ("Fir_callgraph.make_edges_export:" ^ name))) export

let make alias
    { prog_types = tenv;
      prog_export = export;
      prog_import = import;
      prog_globals = globals;
      prog_funs = funs
    } =
  let g = Digraph.create () in
  let ft = make_nodes g funs in
  let ft = make_nodes_import ft g import in
  let entry = Digraph.add_node g entry_sym in
  let exit = Digraph.add_node g exit_sym in
  let cg =
    { cg_g = g;
      cg_ft = ft;
      cg_pft = SymbolTable.empty;
      cg_entry = entry;
      cg_exit = exit
    } in
  make_edges_export cg tenv export;
  let pft = make_edges_funs alias cg funs in
     make_edges_root cg funs;
     pft

let update_inlined_fun cg alias funs n e =
  List.iter (Digraph.delete_edge n) (Digraph.succ n);
  let pft = make_edges_expr alias cg funs n cg.cg_pft e in
    { cg with cg_pft = pft }

(************************************** trivial stuff *)

let entry_node cg = cg.cg_entry
let exit_node cg = cg.cg_exit

let node_list cg = Digraph.list cg.cg_g

let node_of_fun cg = SymbolTable.find cg.cg_ft
let fun_of_node = Digraph.get

let in_degree = Digraph.in_degree
let out_degree = Digraph.out_degree
let query = Digraph.query
let pred = Digraph.pred
let succ = Digraph.succ

let query_entry cg n = Digraph.query cg.cg_entry n
let query_exit cg n = Digraph.query n cg.cg_exit
let succ_entry cg = Digraph.succ cg.cg_entry

let possible_functions cg v =
  try
    SymbolTable.find cg.cg_pft v
  with Not_found ->
    []

(************************************** structural information *)

let is_recursive cg n =
  Digraph.is_path cg.cg_g n n

let postorder_list cg =
  let l = Digraph.postorder_list cg.cg_g cg.cg_entry in
    List.rev_append (Digraph.untouched cg.cg_g) l

let rev_postorder_list cg =
  let l = Digraph.rev_postorder_list cg.cg_g cg.cg_entry in
    List.rev_append (Digraph.untouched cg.cg_g) l

(* debugging *)
let debug_callgraph s cg =
  Format.printf "************************ Callgraph: %s\n" s;
  Format.print_string "\n*** postorder ***\n";
  List.iter (fun n ->
      print_symbol (fun_of_node n);
      Format.print_string ": ";
      List.iter (fun n -> print_symbol (fun_of_node n); Format.print_char ' ') (succ n);
      Format.print_char '\n')
    (postorder_list cg);
  Format.print_string "\n*** pft ***\n";
  SymbolTable.iter (fun f fl ->
      print_symbol f;
      Format.print_string ": ";
      List.iter (fun v -> print_symbol v; Format.print_char ' ') fl;
      Format.print_char '\n')
    cg.cg_pft;
  Format.printf "\n*************\n@\n"

(*
 * Build the loop-nest tree from the callgraph.
 *)
let loop_nest cg =
   let { cg_entry = root; cg_g = graph } = cg in
   let root_var = Digraph.get root in
   let nodes =
      Digraph.filter (fun node -> not (Symbol.eq (Digraph.get node) root_var)) graph
   in
   let succ node =
      List.map Digraph.get (Digraph.succ node)
   in
   let loop = Loop.create "Fir_callgraph" Digraph.get succ root nodes in
   let loop = Loop.loop_nest loop Digraph.get in
      loop
