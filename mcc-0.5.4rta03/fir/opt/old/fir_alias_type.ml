(*
 * Alias analysis.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol

open Fir
open Fir_exn
open Fir_type
open Fir_loop
open Fir_print
open Fir_state
open Fir_algebra
open Fir_standardize

open Sizeof

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Alias information for an aribtrary memory block:
 *    block_mutable: block is mutable
 *    block_size: size of the block in bytes
 *    block_alias: the possible points of allocation
 *    block_default: what we know about default entries
 *    block_data: alias information for any elements we know
 *)
type bindex =
   IndexInt of int
 | IndexVar of var

type bclass =
   { block_mutable : bool;
     block_size : vclass;
     block_alias : SymbolSet.t;
     block_default : vclass;
     block_data : (bindex * vclass) list
   }

(*
 * This is a class for a variable.
 *
 * VarInduction (g, v, op, a) means:
 *    g: the loop the variable belongs to
 *    v: is the basice induction var
 *    op: is the addition operation
 *    a: is the induction increment
 *
 * VarDerived (g, v, op, a1, a2):
 *    g: the loop the variable belongs to
 *    op: is the addition operation
 *    the value is (v * a1 + a2)
 *
 * VarInvariant:
 *    the value is loop invariant
 *
 * VarBlock (v, props):
 *    the value is a memory block with the given properties
 *
 * VarUnknown:
 *    we don't know anything about the var
 *)
and vclass =
   VarInduction of var * var * binop * etree
 | VarDerived of var * var * binop * etree * etree
 | VarInvariant of etree
 | VarBlock of var * bclass
 | VarFunction of SymbolSet.t
 | VarVoid
 | VarUnknown
 | VarVar of var
 | VarApply of var * vclass list
 | VarLambda of var list * vclass
 | VarUnion of vclass list list

(*
 * This is the info we keep for each function.
 *    fun_loop: the name of the loop the function belongs to
 *    fun_vars: the parameters of the function
 *    fun_icall: variable classes from inner calls
 *    fun_ocall: variable classes from outer calls
 *)
type fclass =
   { fun_nest : var list;
     fun_vars : var list;
     fun_icall : vclass list;
     fun_ocall : vclass list
   }

(*
 * Alias environment.
 *)
type aenv =
   { aenv_vars : vclass SymbolTable.t;
     aenv_funs : fclass SymbolTable.t;
     aenv_fun  : var
   }

(*
 * Type classes.  These are used to assign
 * initial vclasses corresponding to types.
 *)
type tclass =
   TVarFun of tclass list * tclass
 | TVarUnion of ty_var * tclass list * int_set
 | TVarTuple of tuple_class * tclass list
 | TVarArray of tclass
 | TVarExists of int * tclass
 | TVarAll of int * tclass
 | TVarCase of tclass
 | TVarObject of tclass
 | TVarUnknown
 | TVarRawdata
 | TVarVar of int
 | TVarApply of ty_var * tclass list
 | TVarUnionLambda of int * union_type * tclass list list
 | TVarLambda of int * tclass

(*
 * The type class environment contains the alias
 * definitions for type identifiers, and type type table.
 *
 * cenv_vars:  the type class for a type identifier
 * cenv_map:   type class translation
 * cenv_types: the hash-cons type table
 *)
module TVarCompare =
struct
   type t = tclass
   let compare = Pervasives.compare
end

module TVarTable = Mc_map.McMake (TVarCompare)

type cenv =
   { cenv_vars  : var SymbolTable.t;
     cenv_map   : var SymbolTable.t;
     cenv_types : tclass TVarTable.t
   }

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print the block entry.
 *)
let print_bindex = function
   IndexInt i ->
      Format.print_int i
 | IndexVar v ->
      print_symbol v

let rec print_bclass block =
   let { block_mutable = mutablep;
         block_size = size;
         block_alias = alias;
         block_default = default;
         block_data = data
       } = block
   in
      Format.printf "@[<v 0>@[<v 3><block> {@ mutable : %b;@ size = " mutablep;
      print_vclass size;
      Format.printf ";@ @[<hv 3>alias = {";
      SymbolSet.iter (fun v ->
            Format.print_space();
            print_symbol v) alias;
      Format.printf " }@];@ default = ";
      print_vclass default;
      Format.printf ";@ @[<hv 3>entries = {";
      List.iter (fun (i, v) ->
            Format.printf "@ @[<hv 3>";
            print_bindex i;
            Format.printf " =@ ";
            print_vclass v;
            Format.printf "@]") data;
      Format.printf " }@]@]@ }@]"

(*
 * Print the loop environment entries.
 *)
and print_vclass a =
   match a with
      VarInvariant a ->
         Format.print_string "<inv>(";
         print_etree a;
         Format.print_string ")"
    | VarVoid ->
         Format.print_string "<void>"
    | VarUnknown ->
         Format.print_string "<unknown>"
    | VarInduction (g, v, op, a1) ->
         Format.open_hvbox 3;
         Format.print_string "<ind>(loop=";
         print_symbol g;
         Format.print_string ", ";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_space ();
         print_etree a1;
         Format.print_string ")";
         Format.close_box ()
    | VarDerived (g, v, op, a1, a2) ->
         Format.open_hvbox 3;
         Format.print_string "<der>(loop=";
         print_symbol g;
         Format.print_string ", ";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_string ",";
         Format.print_space ();
         print_etree a1;
         Format.print_string ",";
         Format.print_space ();
         print_etree a2;
         Format.print_string ")";
         Format.close_box ()
    | VarBlock (v, bclass) ->
         print_bclass bclass
    | VarFunction funs ->
         Format.print_string "<fun>{";
         SymbolSet.iter (fun v ->
               Format.print_string " ";
               print_symbol v;
               Format.print_string ",") funs;
         Format.print_string " }"
    | VarVar v ->
         Format.printf "<var>{";
         print_symbol v;
         Format.printf "}"
    | VarApply (v, args) ->
         Format.printf "@[<hv 3><apply>(";
         print_symbol v;
         List.iter (fun arg ->
               Format.printf ",@ ";
               print_vclass arg) args;
         Format.printf ")@]"
    | VarLambda (vars, ty) ->
         Format.printf "@[<hv 3><lambda>(";
         List.iter (fun v ->
               print_symbol v;
               Format.print_string ".") vars;
         Format.print_space ();
         print_vclass ty;
         Format.printf ")@]"
    | VarUnion fields ->
         Format.printf "@[<v 0>@[<hv 3><union> {";
         List.iter (fun fields ->
               Format.printf "@ @[<hv 3>{";
               List.iter (fun vc ->
                     Format.print_space ();
                     print_vclass vc;
                     Format.print_string ";") fields;
               Format.printf "}@]") fields;
         Format.printf "@]@ }@]"

let print_venv venv =
   Format.open_vbox 3;
   Format.printf "Variables:";
   SymbolTable.iter (fun v a ->
         Format.print_space ();
         Format.open_hvbox 3;
         print_symbol v;
         Format.print_string " :";
         Format.print_space ();
         print_vclass a;
         Format.close_box ()) venv;
   Format.close_box ();
   Format.print_newline ()

let print_fenv fenv =
   Format.printf "@[<v 3>Functions:";
   SymbolTable.iter (fun f { fun_nest = nest;
                             fun_vars = vars;
                             fun_icall = icall;
                             fun_ocall = ocall
                         } ->
         Format.printf "@ @[<v 3>";
         print_symbol f;
         Format.printf ":@ @[<hv 3>loop-nest =";
         List.iter (fun v ->
               Format.print_space ();
               print_symbol v) nest;
         Format.printf "@];@ @[<hv 3>vars =";
         List.iter (fun v ->
               Format.print_space ();
               print_symbol v) vars;
         Format.printf "@];@ @[<hv 3>icall =";
         List.iter (fun v ->
               Format.print_space ();
               print_vclass v) icall;
         Format.printf "@];@ @[<hv 3>ocall =";
         List.iter (fun v ->
               Format.print_space ();
               print_vclass v) ocall;
         Format.printf "@];@]@]") fenv;
   Format.printf "@]"

let print_aenv aenv =
   Format.print_space ();
   print_venv aenv.aenv_vars;
   Format.print_space ();
   print_fenv aenv.aenv_funs

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Make a constant of an integer.
 *)
let const_of_int i =
   VarInvariant (ETConst (AtomRawInt (Rawint.of_int precision_native_int false i)))

(*
 * A variable environment.
 *)
let aenv_add_var aenv v info =
   { aenv with aenv_vars = SymbolTable.add aenv.aenv_vars v info }

let aenv_lookup_var aenv pos v =
   try SymbolTable.find aenv.aenv_vars v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let aenv_lookup_atom aenv pos a =
   match a with
      AtomVar v ->
         aenv_lookup_var aenv pos v
    | _ ->
         VarInvariant (ETConst a)

let aenv_add_fun aenv f info =
   { aenv with aenv_funs = SymbolTable.add aenv.aenv_funs f info }

let aenv_lookup_fun aenv f =
   SymbolTable.find aenv.aenv_funs f

(************************************************************************
 * UNIFICATION
 ************************************************************************)

(*
 * Array sizes are always int32.
 *)
let min_size_op = MinRawIntOp (precision_subscript, signed_subscript)

(*
 * Compare two indexes.
 *)
let compare_bindex i1 i2 =
   match i1, i2 with
      IndexInt i1, IndexInt i2 ->
         i1 - i2
    | IndexVar v1, IndexVar v2 ->
         Symbol.compare v1 v2
    | IndexInt _, IndexVar _ ->
         -1
    | IndexVar _, IndexInt _ ->
         1

(*
 * Unify the data elements in an array.
 * We assume the elements are sorted (variables are always last).
 * Through out knowledge not present in both lists.
 *)
let rec unify_data data1 data2 =
   match data1, data2 with
      (i1, v1) :: data1', (i2, v2) :: data2' ->
         let cmp = compare_bindex i1 i2 in
            if cmp = 0 then
               (i1, unify_vclass v1 v2) :: unify_data data1' data2'
            else if cmp > 0 then
               unify_data data1 data2'
            else (* cmp < 0 *)
               unify_data data1' data2
    | _ ->
         []

(*
 * Unify the types of two arrays.
 *)
and unify_block b1 b2 =
   let { block_mutable = m1;
         block_size = size1;
         block_alias = alias1;
         block_default = default1;
         block_data = data1
       } = b1
   in
   let { block_mutable = m2;
         block_size = size2;
         block_alias = alias2;
         block_default = default2;
         block_data = data2
       } = b2
   in
      { block_mutable = m1 && m2;
        block_size = unify_minimum size1 size2;
        block_alias = SymbolSet.union alias1 alias2;
        block_default = unify_minimum default1 default2;
        block_data = unify_data data1 data2
      }

(*
 * Take the minimum of two sizes.
 * This is only used to compute the block size.
 * We aren't too accurate here.
 *)
and unify_minimum v1 v2 =
   if v1 = v2 then
      v1
   else
      match v1, v2 with
         VarVoid, v
       | v, VarVoid ->
            v

         (* Invariants *)
       | VarInvariant e1, VarInvariant e2 ->
            VarInvariant (ETBinop (min_size_op, e1, e2))

         (* We ignore the rest, including induction variables *)
       | _ ->
            VarUnknown

(*
 * Unify two variable classes.
 * This is an outer call, so we don't create induction variables.
 *)
and unify_vclass v1 v2 =
   let v =
      if v1 = v2 then
         v1
      else
         match v1, v2 with
            (* Not set yet *)
            VarVoid, v
          | v, VarVoid ->
               v

           (* Functions *)
          | VarFunction s1, VarFunction s2 ->
               VarFunction (SymbolSet.union s1 s2)
          | VarFunction _, _
          | _, VarFunction _ ->
               VarUnknown

            (* Arrays *)
          | VarBlock (v1, b1), VarBlock (v2, b2)
            when Symbol.eq v1 v2 ->
               VarBlock (v1, unify_block b1 b2)
          | VarBlock _, _
          | _, VarBlock _ ->
               VarUnknown

            (* All other combinations destroy information *)
          | _ ->
               VarUnknown
   in
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>Unify_outer_vclass:@ v1=";
            print_vclass v1;
            Format.printf "@ v2=";
            print_vclass v2;
            Format.printf "@ v=";
            print_vclass v;
            Format.printf "@]@."
         end;
      v

(*
 * Unify the arguments of a call.
 * This is a normal unification: the variables should
 * have the same class.
 *)
let unify_call call1 call2 =
   let changed, call =
      List.fold_left2 (fun (changed, call) v1 v2 ->
            let v = unify_vclass v1 v2 in
            let changed = changed || v <> v1 in
               changed, v :: call) (false, []) call1 call2
   in
      changed, List.rev call

(*
 * Unify a variable in a loop header.
 *    v: the param
 *    iv: the inner class
 *    ov: the outer class
 *)
let unify_loop_var aenv pos g v iv ov =
   if iv = VarVoid then
      match ov with
         VarInduction _
       | VarDerived _
       | VarInvariant _
       | VarBlock _
       | VarFunction _ ->
            (*
             * If the inner var hasn't been initialized,
             * we predict that it will be loop invariant.
             * This means we just take the outer class
             * and pass it through the loop, and predict
             * it will come out the same.
             *)
            ov
       | VarVoid
       | VarUnknown ->
            (*
             * If the inner loop is not initialized,
             * and the value from the outer loop is totally
             * bogus, assume the value may be loop invariant
             * in this loop.
             *)
            VarInvariant (ETVar v)

   else if iv = ov then
      (*
       * At this point, we have rounded the loop at least once
       * and the value is the same.  Just leave it that way:
       * the value is loop invariant.
       *)
      iv
   else
      (*
       * Otherwise, we hope for the best, and assume that the
       * value is an induction variable.  These are the cases
       * where we have assumed the value is loop invariant.
       *)
      match aenv_lookup_var aenv pos v, iv with
         VarInvariant (ETVar v1), VarInvariant (ETBinop (PlusIntOp as op, ETConst c1, ETVar v2))
       | VarInvariant (ETVar v1), VarInvariant (ETBinop (PlusRawIntOp _ as op, ETConst c1, ETVar v2))
         when Symbol.eq v1 v && Symbol.eq v2 v ->
            VarInduction (g, v, op, ETConst c1)

       | VarInduction (g1, v1, op1, e1), VarDerived (g2, v2, op2, e2, e3) when
         Symbol.eq g1 g
         && Symbol.eq g2 g
         && Symbol.eq v1 v
         && Symbol.eq v2 v
         && (e1 = e3)
         && (op1 = op2) ->
            iv

       | _ ->
            (*
             * In this final case, the inner var is none of the above.
             * It must have been initialized from an outer value, so we
             * give it a local loop invariant value.
             *)
            VarInvariant (ETVar v)

(*
 * Debug version of loop param analysis.
 *)
let unify_loop_var aenv pos g v iv ov =
   let vc = unify_loop_var aenv pos g v iv ov in
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>Unify_loop_var:@ loop=";
            print_symbol g;
            Format.printf ";@ var=";
            print_symbol v;
            Format.printf ";@ inner=";
            print_vclass iv;
            Format.printf ";@ outer=";
            print_vclass ov;
            Format.printf ";@ new=";
            print_vclass vc;
            Format.printf "@]@."
         end;
      vc

(*
 * Unify the classes in a loop header.
 *)
let unify_loop_header aenv pos g info =
   let { fun_vars = vars;
         fun_icall = icall;
         fun_ocall = ocall
       } = info
   in

   (* Add updated values for all the loop params *)
   let aenv =
      Mc_list_util.fold_left3 (fun aenv v iv ov ->
            let vc = unify_loop_var aenv pos g v iv ov in
               aenv_add_var aenv v vc) aenv vars icall ocall
   in

   (* Make sure the inner values are undefined *)
   let icall = List.map (fun _ -> VarVoid) vars in
   let info = { info with fun_icall = icall } in
      aenv_add_fun aenv g info

(************************************************************************
 * TYPE ALIASING
 ************************************************************************)

(*
 * Lookup a type definition.
 *)
let v_generic = new_symbol_string "generic"
let v_rawdata = new_symbol_string "rawdata"
let v_unknown = new_symbol_string "unknown"

let alias_lookup_tydef cenv v =
   try SymbolTable.find cenv.cenv_vars v with
      Not_found ->
         v_generic

(*
 * Note that types use structural equality, so we have to be careful
 * to alias types that have the same structure.  this is computed as
 * a fixpoint.
 *)
let alias_lookup_type cenv ty =
   let types = cenv.cenv_types in
      try cenv, TVarTable.find types ty with
         Not_found ->
            (* Add a new entry *)
            let vclass = TVarApply (new_symbol_string "vclass", []) in
            let cenv = { cenv with cenv_types = TVarTable.add types ty vclass } in
               cenv, vclass

(*
 * Type classification for external functions.
 * We know that some types will never alias.  Of course,
 * all Rawdata can be aliased.
 *)
let rec alias_type cenv vars ty =
   match ty with
      TyInt
    | TyEnum _
    | TyRawInt _
    | TyFloat _
    | TyProject _
    | TyDelayed
    | TyPointer _ ->
         cenv, TVarUnknown
    | TyRawData ->
         cenv, TVarRawdata
    | TyVar v ->
         let ty =
            try SymbolTable.find vars v with
               Not_found ->
                  TVarUnknown
         in
            cenv, ty
    | TyFun (ty_vars, ty_res) ->
         let cenv, ty_vars = alias_types cenv vars ty_vars in
         let cenv, ty_res = alias_type cenv vars ty_res in
            alias_lookup_type cenv (TVarFun (ty_vars, ty_res))
    | TyUnion (v, tyl, is) ->
         let cenv, tyl = alias_types cenv vars tyl in
            alias_lookup_type cenv (TVarUnion (v, tyl, is))
    | TyTuple (tclass, tyl) ->
         let cenv, tyl = alias_types cenv vars tyl in
            alias_lookup_type cenv (TVarTuple (tclass, tyl))
    | TyArray ty ->
         let cenv, ty = alias_type cenv vars ty in
            alias_lookup_type cenv (TVarArray ty)
    | TyApply (v, tyl) ->
         let v = alias_lookup_tydef cenv v in
         let cenv, tyl = alias_types cenv vars tyl in
            alias_lookup_type cenv (TVarApply (v, tyl))
    | TyExists ([], ty)
    | TyAll ([], ty)
    | TyOption ty ->
         alias_type cenv vars ty
    | TyExists (vars', ty) ->
         let cenv, ty = alias_type cenv vars ty in
            alias_lookup_type cenv (TVarExists (List.length vars', ty))
    | TyAll (vars', ty) ->
         let cenv, ty = alias_type cenv vars ty in
            alias_lookup_type cenv (TVarAll (List.length vars', ty))
    | TyCase ty ->
         let cenv, ty = alias_type cenv vars ty in
            alias_lookup_type cenv (TVarCase ty)
    | TyObject (_, ty) ->
         let cenv, ty = alias_type cenv vars ty in
            alias_lookup_type cenv (TVarObject ty)

and alias_types cenv vars tyl =
   let cenv, tyl =
      List.fold_left (fun (cenv, tyl) ty ->
            let cenv, ty = alias_type cenv vars ty in
               cenv, ty :: tyl) (cenv, []) tyl
   in
      cenv, List.rev tyl

(*
 * Alias for a type definition.
 *)
let alias_fields cenv vars tyl =
   let alias_fields cenv tyl =
      let cenv, tyl =
         List.fold_left (fun (cenv, tyl) (ty, _) ->
               let cenv, ty = alias_type cenv vars ty in
                  cenv, ty :: tyl) (cenv, []) tyl
      in
         cenv, List.rev tyl
   in
   let cenv, tyl =
      List.fold_left (fun (cenv, tyl) fields ->
            let cenv, fields = alias_fields cenv fields in
               cenv, fields :: tyl) (cenv, []) tyl
   in
      cenv, List.rev tyl

let alias_tydef cenv tydef =
   match tydef with
      TyDefUnion (vars, ut, ty_fields) ->
         let i = List.length vars in
         let vars, _ =
            List.fold_left (fun (vars, i) v ->
                  let vars = SymbolTable.add vars v (TVarVar i) in
                     vars, succ i) (SymbolTable.empty, 0) vars
         in
         let cenv, ty_fields = alias_fields cenv vars ty_fields in
            alias_lookup_type cenv (TVarUnionLambda (i, ut, ty_fields))
    | TyDefLambda (vars, ty) ->
         let i = List.length vars in
         let vars, _ =
            List.fold_left (fun (vars, i) v ->
                  let vars = SymbolTable.add vars v (TVarVar i) in
                     vars, succ i) (SymbolTable.empty, 0) vars
         in
         let cenv, ty = alias_type cenv vars ty in
            alias_lookup_type cenv (TVarLambda (i, ty))

(*
 * One step of the fixpoint computation.
 *)
let alias_step cenv tenv =
   (* Alias all the type definitions *)
   let cenv, venv =
      SymbolTable.fold (fun (cenv, venv) v ty ->
            let cenv, ty = alias_tydef cenv ty in
            let venv = SymbolTable.add venv v ty in
               cenv, venv) (cenv, SymbolTable.empty) tenv
   in

   (* Separate all the new type classes *)
   let { cenv_vars = vars;
         cenv_map = map
       } = cenv
   in
   let changed, vars, map =
      SymbolTable.fold (fun (changed, vars, map) v ty ->
            let v' =
               match ty with
                  TVarRawdata -> v_rawdata
                | TVarApply (v, []) -> v
                | _ -> v_unknown
            in
            let changed, map, v' =
               try changed, map, SymbolTable.find map v' with
                  Not_found ->
                     let v'' = new_symbol_string "type_class" in
                     let map = SymbolTable.add map v' v'' in
                        true, map, v''
            in
            let vars = SymbolTable.add vars v v' in
               changed, vars, map) (false, vars, map) venv
   in
   let cenv = { cenv with cenv_vars = vars; cenv_map = map } in
      changed, cenv

(*
 * Add alias types for all the type definitions.
 *)
let alias_types tenv =
   let cenv =
      { cenv_vars = SymbolTable.empty;
        cenv_map = SymbolTable.empty;
        cenv_types = TVarTable.empty
      }
   in

   (* Now compute the fixpoint *)
   let rec fixpoint cenv =
      let changed, cenv = alias_step cenv tenv in
         if changed then
            fixpoint cenv
         else
            cenv
   in
      fixpoint cenv

(*
 * Convert a type class to a variable class.
 *)
let rec vclass_of_tclass venv vars ty =
   match ty with
      TVarFun (ty_vars, ty_res) ->
         VarFunction SymbolSet.empty
    | TVarUnion (_, tyl, _)
    | TVarTuple (_, tyl) ->
         vclass_tuple v tyl
    | TVarArray ty ->
         vclass_array v ty
    | TVarApply (v, tyl) ->
         vclass_apply v tyl
    | TVarExists (_, v)
    | TVarAll (_, v)
    | TVarCase v
    | TVarObject v ->
         VarApply (v, [])
    | TVarUnionLambda _
    | TVarLambda _ ->
         VarUnknown

and vclass_tuple v tyl =
   let data, size =
      List.fold_left (fun (data, index) ty ->
            let vc = vclass_of_tclass venv vars ty in
            let data = (IndexInt index, vc) :: data in
            let index = index + sizeof_pointer in
               data, index) ([], 0) vars
   in
   let block =
      { block_mutable = false;
        block_size = VarInvariant (ETConst (AtomInt size));
        block_alias = SymbolSet.empty;
        block_default = VarUnknown;
        block_data = List.rev data
      }
   in
      VarBlock (v, block)

and vclass_array v1 v2 =
   let block =
      { block_mutable = false;
        block_size = VarUnknown;
        block_alias = SymbolSet.empty;
        block_default = VarApply (v2, []);
        block_data = []
      }
   in
      VarBlock (v1, block)

and vclass_apply v vars =
   let args = List.map (fun v -> VarApply (v, [])) vars in
      VarApply (v, args)

(*
 * Finally, we want to invert the type table so we can assign
 * an alias class to the parameters of exported functions.  The rule
 * here is this: and two parameters with the same type can be aliases.
 *)
let alias_init prog =
   let { prog_types = tenv;
         prog_funs = funs;
         prog_globals = globals;
         prog_names = names;
         prog_import = import;
         prog_export = export
       } = prog
   in

   (* Generate aliases for all the defined types *)
   let cenv = alias_types tenv in

   (* Add all the type definitions for all toplevel values *)
   let venv = SymbolTable.empty in
   let cenv, venv =
      SymbolTable.fold (fun (cenv, venv) f (_, ty, _, _) ->
            let cenv, ty = alias_type cenv ty in
            let venv = SymbolTable.add venv f ty in
               cenv, venv) (cenv, venv) funs
   in
   let cenv, venv =
      SymbolTable.fold (fun (cenv, venv) v (ty, _) ->
            let cenv, ty = alias_type cenv ty in
            let venv = SymbolTable.add venv v ty in
               cenv, venv) (cenv, venv) globals
   in
   let cenv, venv =
      SymbolTable.fold (fun (cenv, venv) v ty ->
            let cenv, ty = alias_type cenv ty in
            let venv = SymbolTable.add venv v ty in
               cenv, venv) (cenv, venv) names
   in
   let cenv, venv =
      SymbolTable.fold (fun (cenv, venv) v { import_type = ty } ->
            let cenv, ty = alias_type cenv ty in
            let venv = SymbolTable.add venv v ty in
               cenv, venv) (cenv, venv) import
   in

   (* Assign types to the parameters of all the exported functions *)
   let penv =
      SymbolTable.fold (fun penv f (_, ty, vars, _) ->


(************************************************************************
 * VARIABLE CLASSIFICATION
 ************************************************************************)

(*
 * Scan a function in the loop.
 *)
let rec alias_exp aenv e =
   let pos = string_pos "alias_exp" (exp_pos e) in
      match e with
         LetUnop (v, _, op, a, e) ->
            alias_unop_exp aenv pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            alias_binop_exp aenv pos v op a1 a2 e
       | TailCall (f, args) ->
            alias_tailcall_exp aenv pos f args
       | Match (a, cases) ->
            alias_match_exp aenv pos a cases
       | TypeCase (_, _, _, v, e1, e2) ->
            alias_typecase_exp aenv pos v e1 e2
       | LetExt (v, _, _, _, args, e) ->
            alias_ext_exp aenv pos v args e
       | LetAlloc (v, op, e) ->
            alias_alloc_exp aenv pos v op e
       | LetSubscript (_, v1, _, v2, a, e) ->
            alias_subscript_exp aenv pos v1 v2 a e
       | Assert (label, pred, e) ->
            alias_assert_exp aenv pos label pred e
       | SetSubscript (_, v, a1, _, a2, e) ->
            alias_set_subscript_exp aenv pos v a1 a2 e
       | Memcpy (_, v1, a1, v2, a2, a3, e) ->
            alias_memcpy_exp aenv pos v1 a1 v2 a2 a3 e
       | SetGlobal (_, _, _, _, e)
       | Debug (_, e) ->
            alias_exp aenv e
       | SpecialCall _ ->
            false, aenv

(*
 * Catch simple unops.
 *)
and alias_unop_exp aenv pos v op a e =
   let pos = string_pos "alias_unop_exp" pos in
   let a = aenv_lookup_atom aenv pos a in
   let a =
      match op, a with
         IdOp, _ ->
            a
       | _, VarInvariant a ->
            VarInvariant (canonicalize_etree (ETUnop (op, a)))
       | _ ->
            VarUnknown
   in
   let aenv = aenv_add_var aenv v a in
      alias_exp aenv e

(*
 * Binary operations.
 *)
and alias_binop_exp aenv pos v op_orig a1 a2 e =
   let pos = string_pos "alias_binop_exp" pos in
   let a1 = aenv_lookup_atom aenv pos a1 in
   let a2 = aenv_lookup_atom aenv pos a2 in
   let a =
      match op_orig, a1, a2 with
         op, VarInvariant a1, VarInvariant a2 ->
            VarInvariant (canonicalize_etree (ETBinop (op, a1, a2)))

       | PlusIntOp, VarInvariant a2, VarInduction (g, v, op, _)
       | PlusIntOp, VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, ETConst (AtomInt 1), a2)

       | PlusRawIntOp (pre, signed), VarInvariant a2, VarInduction (g, v, op, _)
       | PlusRawIntOp (pre, signed), VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, ETConst (AtomRawInt (Rawint.of_int pre signed 1)), a2)

       | MinusIntOp, VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, ETConst (AtomInt 1), canonicalize_etree (ETUnop (UMinusIntOp, a2)))

       | MinusRawIntOp (pre, signed), VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op,
                        ETConst (AtomRawInt (Rawint.of_int pre signed 1)),
                        canonicalize_etree (ETUnop (UMinusRawIntOp (pre, signed), a2)))

       | MulIntOp, VarInvariant a2, VarInduction (g, v, op, _)
       | MulIntOp, VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, a2, ETConst (AtomInt 0))

       | MulRawIntOp (pre, signed), VarInvariant a2, VarInduction (g, v, op, _)
       | MulRawIntOp (pre, signed), VarInduction (g, v, op, _), VarInvariant a2 ->
            VarDerived (g, v, op, a2, ETConst (AtomRawInt (Rawint.of_int pre signed 0)))

       | PlusIntOp, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | PlusIntOp, VarDerived (g, v, op, a1, a2), VarInvariant a3
       | PlusRawIntOp _, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | PlusRawIntOp _, VarDerived (g, v, op, a1, a2), VarInvariant a3 ->
            VarDerived (g, v, op, a1, canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | MinusIntOp, VarDerived (g, v, op, a1, a2), VarInvariant a3
       | MinusRawIntOp _, VarDerived (g, v, op, a1, a2), VarInvariant a3 ->
            VarDerived (g, v, op, a1, canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | MulIntOp, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | MulIntOp, VarDerived (g, v, op, a1, a2), VarInvariant a3
       | MulRawIntOp _, VarInvariant a3, VarDerived (g, v, op, a1, a2)
       | MulRawIntOp _, VarDerived (g, v, op, a1, a2), VarInvariant a3 ->
            VarDerived (g, v, op,
                        canonicalize_etree (ETBinop (op_orig, a1, a3)),
                        canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | _ ->
            VarUnknown
   in
   let aenv = aenv_add_var aenv v a in
      alias_exp aenv e

(*
 * Match expression.
 *)
and alias_match_exp aenv pos a cases =
   let pos = string_pos "alias_match_exp" pos in
      List.fold_left (fun (changed, aenv) (_, e) ->
            let changed', aenv = alias_exp aenv e in
               changed || changed', aenv) (false, aenv) cases

(*
 * Typecase.
 *)
and alias_typecase_exp aenv pos v e1 e2 =
   let pos = string_pos "alias_typecase_exp" pos in
   let aenv = aenv_add_var aenv v VarUnknown in
   let changed1, aenv = alias_exp aenv e1 in
   let changed2, aenv = alias_exp aenv e2 in
      changed1 || changed2, aenv

(*
 * External call.
 * NOTE: we assume external functions destroy their array arguments.
 *)
and alias_ext_exp aenv pos v args e =
   let pos = string_pos "alias_ext_exp" pos in
   let aenv = aenv_add_var aenv v VarUnknown in
      alias_exp aenv e

(*
 * Allocation.
 *)
and alias_alloc_exp aenv pos v op e =
   let pos = string_pos "alias_alloc_exp" pos in
   let size, default, data = alias_alloc_op aenv pos op in
   let block =
      { block_mutable = true;
        block_size = size;
        block_alias = SymbolSet.singleton v;
        block_default = default;
        block_data = data
      }
   in
   let aenv = aenv_add_var aenv v (VarBlock (v, block)) in
      alias_exp aenv e

(*
 * Get the alias info for an allocation.
 *)
and alias_alloc_op aenv pos op =
   let pos = string_pos "alias_alloc_op" pos in
      match op with
         AllocTuple (_, _, args)
       | AllocUnion (_, _, _, args)
       | AllocArray (_, args) ->
            let data, size =
               List.fold_left (fun (data, index) a ->
                     let info = aenv_lookup_atom aenv pos a in
                     let data = (IndexInt index, info) :: data in
                     let index = index + sizeof_pointer in
                        data, index) ([], 0) args
            in
            let size = const_of_int size in
               size, VarUnknown, List.rev data
       | AllocVArray (_, _, a1, a2) ->
            let data = aenv_lookup_atom aenv pos a2 in
            let size = aenv_lookup_atom aenv pos a1 in
               size, data, []
       | AllocMalloc a ->
            let size = aenv_lookup_atom aenv pos a in
               size, VarUnknown, []

(*
 * Subscripting.
 *)
and alias_subscript_exp aenv pos v1 v2 a e =
   let pos = string_pos "alias_subscript_exp" pos in
   let aenv = aenv_add_var aenv v1 VarUnknown in
      alias_exp aenv e

and alias_set_subscript_exp aenv pos v a1 a2 e =
   let pos = string_pos "alias_set_subscript_exp" pos in
      alias_exp aenv e

and alias_memcpy_exp aenv pos v1 a1 v2 a2 a3 e =
   let pos = string_pos "alias_memcpy_exp" pos in
      alias_exp aenv e

(*
 * Assertions.
 *)
and alias_assert_exp aenv pos label pred e =
   let pos = string_pos "alias_assert_exp" pos in
      alias_exp aenv e

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and alias_tailcall_exp aenv pos f args =
   let pos = string_pos "alias_tailcall_exp" pos in
      try
         let info_call = aenv_lookup_fun aenv f in
         let info_self = aenv_lookup_fun aenv aenv.aenv_fun in
         let args = List.map (aenv_lookup_atom aenv pos) args in
         let f_called_loop = List.hd info_call.fun_nest in
         let changed, info =
            if List.mem f_called_loop info_self.fun_nest then
               (* The called function is not in a nested loop *)
               let changed, icall = unify_call info_call.fun_icall args in
               let info = { info_call with fun_icall = icall } in
                  changed, info
            else
               (* The called function is in a nested loop *)
               let changed, ocall = unify_call info_call.fun_ocall args in
               let info = { info_call with fun_ocall = ocall } in
                  changed, info
         in
            changed, aenv_add_fun aenv f info
      with
         Not_found ->
            false, aenv

(*
 * Classify all the funs in the trace.
 *)
let rec alias_trace funs nest fenv l =
   match l with
      Elem g ->
         (try
             let _, _, vars, _ = SymbolTable.find funs g in
             let call = List.map (fun _ -> VarVoid) vars in
             let info = { fun_nest = nest; fun_vars = vars; fun_icall = call; fun_ocall = call } in
                SymbolTable.add fenv g info
          with
             Not_found ->
                fenv)
    | Trace (Elem g :: l) ->
         let _, _, vars, _ = SymbolTable.find funs g in
         let call = List.map (fun _ -> VarVoid) vars in
         let nest = g :: nest in
         let info = { fun_nest = nest; fun_vars = vars; fun_icall = call; fun_ocall = call } in
         let fenv = SymbolTable.add fenv g info in
            alias_trace_list funs nest fenv l
    | Trace l ->
         alias_trace_list funs nest fenv l

and alias_trace_list funs nest fenv l =
   List.fold_left (alias_trace funs nest) fenv l

(*
 * Found a loop.
 *)
let alias_step aenv funs =
   SymbolTable.fold (fun (changed1, aenv) f (_, _, vars, e) ->
         let pos = string_pos "alias_step" (vexp_pos f) in
         let info =  aenv_lookup_fun aenv f in
         let { fun_nest = nest; fun_icall = icall } = info in
         let f_loop = List.hd nest in
         let aenv =
            if Symbol.eq f f_loop then
               (* This is a loop header, so unify the header *)
               unify_loop_header aenv pos f info
            else
               (* Otherwise, just carry forward the inner vars *)
               List.fold_left2 aenv_add_var aenv vars icall
         in
         let aenv = { aenv with aenv_fun = f } in
         let changed2, aenv = alias_exp aenv e in
            changed1 || changed2, aenv) (false, aenv) funs

let alias_prog prog =
   let { prog_funs = funs;
         prog_names = names;
         prog_globals = globals;
         prog_types = tenv
       } = prog
   in

   (* Get the trace *)
   let trace = build_loop prog in
   let _ =
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>*** Alias trace:";
            Trace.print string_of_symbol trace;
            Format.printf "@]@."
         end
   in

   (* Collect all the fun info *)
   let root = new_symbol_string "root" in
   let info =
      { fun_nest = [root];
        fun_vars = [];
        fun_icall = [];
        fun_ocall = []
      }
   in
   let fenv = SymbolTable.add SymbolTable.empty root info in
   let fenv = alias_trace_list funs [root] fenv trace in

   (* Add all the globals to the aenv *)
   let aenv =
      { aenv_vars = SymbolTable.empty;
        aenv_funs = fenv;
        aenv_fun = root
      }
   in
   let aenv =
      SymbolTable.fold (fun aenv f _ ->
            aenv_add_var aenv f (VarFunction (SymbolSet.singleton f))) aenv funs
   in
   let aenv =
      SymbolTable.fold (fun aenv v _ ->
            aenv_add_var aenv v VarUnknown) aenv globals
   in
   let aenv =
      SymbolTable.fold (fun aenv v _ ->
            aenv_add_var aenv v VarUnknown) aenv names
   in

   (* For exported functions, assign classes to all the vars *)
   let aenv, _ =
      SymbolTable.fold (fun (aenv, aclass) g _ ->
            try
               let _, ty, vars, _ = SymbolTable.find funs g in
               let pos = string_pos "Fir_alias.alias_prog.export" (vexp_pos g) in
               let _, ty_vars, _ = dest_fun_type tenv pos ty in
                  List.fold_left2 (fun (aenv, aclass) v ty ->
                        aenv_add_aclass aenv aclass v ty) (aenv, aclass) vars ty_vars
            with
               Not_found ->
                  aenv, aclass) (aenv, SymbolTable.empty) funs
   in

   (* Classify all the vars *)
   let rec fixpoint count aenv =
      let changed, aenv = alias_step aenv funs in
         if debug debug_loop then
            begin
               Format.printf "@[<v 3>*** Alias %d/%b:" count changed;
               print_aenv aenv;
               Format.printf "@]@."
            end;
         if changed then
            fixpoint (succ count) aenv
         else
            aenv
   in
   let aenv = fixpoint 1 aenv in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
