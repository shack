(*
 * Simple loop optimizations.
 * We classify each variable as loop-invariant, as
 * an induction variable, or arbitrary.  Loop invariants
 * and induction variables are passed around the loop as
 * extra arguments.  We definitely need dead-code elim
 * after this stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Trace
open Symbol

open Fir
open Fir_exn
open Fir_type
open Fir_loop
open Fir_print
open Fir_state
open Fir_algebra
open Fir_standardize

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * This is a class for a variable.
 *
 * LoopInduction (v, op, a) means:
 *    v: is the basice induction var
 *    op: is the addition operation
 *    a: is the induction increment
 *
 * LoopDerived (v, op, a1, a2):
 *    op: is the addition operation
 *    the value is (v * a1 + a2)
 *)
type vloop =
   LoopInduction of var * binop * etree
 | LoopDerived of var * binop * etree * etree
 | LoopInvariant of etree
 | LoopUnknown

(*
 * This is the operation to perform for each variable
 * occurrence.
 *
 * BodyInduction (v, op, a1, a2) means:
 *    v: is the base induction var
 *    op: is the addition operation
 *    a1: is the offset from v on a use
 *    a2: is the induction increment
 * This means:
 *    1. every def of the var will add a1 to the induction vat
 *    2. the var will be incremented on each loop
 *)
type vbody =
   BodyOffset of var * binop * atom
 | BodyInduction of var * binop * atom
 | BodyConst of atom
 | BodyUnknown

(*
 * Wrap the environment in a record.
 *)
type henv =
   { henv_head  : (var * etree) list;
     henv_body  : vbody SymbolTable.t;
     henv_funs  : fundef SymbolTable.t;
     henv_loops : fundef SymbolTable.t;
     henv_vars  : var list;
     henv_args  : atom list;
     henv_fun   : var;
     henv_funi  : var
   }

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print the loop environment entries.
 *)
let print_vloop a =
   match a with
      LoopInvariant a ->
         Format.print_string "<inv>(";
         print_etree a;
         Format.print_string ")"
    | LoopUnknown ->
         Format.print_string "<arb>"
    | LoopInduction (v, op, a1) ->
         Format.open_hvbox 3;
         Format.print_string "<ind>(";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_space ();
         print_etree a1;
         Format.print_string ")";
         Format.close_box ()
    | LoopDerived (v, op, a1, a2) ->
         Format.open_hvbox 3;
         Format.print_string "<der>(";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_string ",";
         Format.print_space ();
         print_etree a1;
         Format.print_string ",";
         Format.print_space ();
         print_etree a2;
         Format.print_string ")";
         Format.close_box ()

let print_venv venv =
   Format.open_vbox 3;
   Format.print_string "*** Hoist: venv";
   SymbolTable.iter (fun v a ->
         Format.print_space ();
         Format.open_hvbox 3;
         print_symbol v;
         Format.print_string " :";
         Format.print_space ();
         print_vloop a;
         Format.close_box ()) venv;
   Format.close_box ();
   Format.print_newline ()

(*
 * Print the body environment entries.
 *)
let print_vbody a =
   match a with
      BodyOffset (v, op, a) ->
         Format.print_string "<bodyoff>(";
         print_symbol v;
         Format.print_string " ";
         Format.print_string (string_of_binop op);
         Format.print_string ", ";
         print_atom a;
         Format.print_string ")"
    | BodyInduction (v, op, a) ->
         Format.print_string "<bodyind>(";
         print_symbol v;
         Format.print_string ", ";
         Format.print_string (string_of_binop op);
         Format.print_string ", ";
         print_atom a;
         Format.print_string ")"
    | BodyConst a ->
         Format.print_string "<bodyconst>(";
         print_atom a;
         Format.print_string ")"
    | BodyUnknown ->
         Format.print_string "<bodyunknown>"

let print_henv henv =
   let { henv_head = head;
         henv_body = body;
         henv_vars = vars;
         henv_args = args;
         henv_fun  = f;
         henv_funi = f'
       } = henv
   in
      Format.open_vbox 3;
      Format.print_string "*** Hoist: henv:";

      Format.print_space ();
      Format.open_vbox 3;
      Format.print_string "Head:";
      List.iter (fun (v, e) ->
            Format.print_space ();
            Format.open_hvbox 3;
            print_symbol v;
            Format.print_string " =";
            Format.print_space ();
            print_etree e;
            Format.close_box ()) head;
      Format.close_box ();

      Format.print_space ();
      Format.open_vbox 3;
      Format.print_string "Body:";
      SymbolTable.iter (fun v a ->
            Format.print_space ();
            print_symbol v;
            Format.print_string " = ";
            print_vbody a) body;
      Format.close_box ();

      Format.print_space ();
      Format.open_vbox 3;
      Format.print_string "Vars:";
      List.iter (fun v ->
            Format.print_space ();
            print_symbol v) vars;
      Format.close_box ();

      Format.print_space ();
      Format.open_vbox 3;
      Format.print_string "Args:";
      List.iter (fun a ->
            Format.print_space ();
            print_atom a) args;
      Format.close_box ();

      Format.print_space ();
      Format.print_string "Fun: ";
      print_symbol f;

      Format.print_space ();
      Format.print_string "Inner: ";
      print_symbol f';

      Format.close_box ();
      Format.print_newline ()

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * A variable environment.
 *)
type venv = vloop SymbolTable.t

let venv_empty = SymbolTable.empty

let venv_add = SymbolTable.add

let venv_lookup_var venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let venv_lookup_atom venv pos a =
   match a with
      AtomVar v ->
         venv_lookup_var venv pos v
    | _ ->
         LoopInvariant (ETConst a)

(*
 * Look up the previous value for the exp.
 *)
let cse_find cs t =
   match t with
      ETConst a ->
         None
    | ETVar v ->
         Some v
    | _ ->
         try
            Some (ETreeTable.find cs t)
         with
            Not_found ->
               None

(*
 * Normalize an etree.  Insert vars for all operations.
 *)
let rec simplify_etree cenv head e =
   match cse_find cenv e with
      Some v ->
         cenv, head, ETVar v
    | None ->
         match e with
            ETConst _
          | ETVar _ ->
               cenv, head, e
          | ETUnop (op, a) ->
               let v = new_symbol_string "unop" in
               let cenv, head, a = simplify_etree cenv head a in
               let head = (v, ETUnop (op, a)) :: head in
               let cenv = ETreeTable.add cenv e v in
                  cenv, head, ETVar v
          | ETBinop (op, a1, a2) ->
               let v = new_symbol_string "binop" in
               let cenv, head, a1 = simplify_etree cenv head a1 in
               let cenv, head, a2 = simplify_etree cenv head a2 in
               let head = (v, ETBinop (op, a1, a2)) :: head in
               let cenv = ETreeTable.add cenv e v in
                  cenv, head, ETVar v

(*
 * Produce an atom from the etree, after simplification.
 * We know the etree is a constant or a var.  If it is a
 * var, add it to the vars list.
 *)
let atom_of_etree venv vars e =
   match e with
      ETConst a ->
         venv, vars, a
    | ETVar v ->
         let vars = SymbolSet.add vars v in
         let venv = venv_add venv v (BodyConst (AtomVar v)) in
            venv, vars, AtomVar v
    | _ ->
         raise (Invalid_argument "atom_of_etree")

(*
 * Walk through the venv and add precomputations.
 *    cenv: the common-subexpression table
 *    venv: the var definitions for the loop body
 *    head: expressions to be computed in the loop pre-header
 *    vars: extra vars to be added to all loop functions
 *)
let simplify_venv venv =
   let _, venv, head, vars =
      SymbolTable.fold (fun (cenv, venv, head, vars) v a ->
            match a with
               LoopInduction (v', op, a1) ->
                  let cenv, head, a1 = simplify_etree cenv head a1 in
                  let venv, vars, a1 = atom_of_etree venv vars a1 in
                  let venv = venv_add venv v (BodyInduction (v', op, a1)) in
                     cenv, venv, head, vars
             | LoopDerived (v', op, a1, a2) ->
                  (*
                   * Be careful about derived variables.
                   * If the multiplier is 1, then just base it on the
                   * induction variable.
                   *)
                  let cenv, head, a1 = simplify_etree cenv head a1 in
                  let cenv, head, a2 = simplify_etree cenv head a2 in
                  let trivialp =
                     match a1 with
                        ETConst (AtomInt 1) ->
                           true
                      | ETConst (AtomRawInt i) when Rawint.is_one i ->
                           true
                      | _ ->
                           false
                  in
                     if trivialp then
                        (* For a trivial multiplier, just add the offset *)
                        let venv, vars, a2 = atom_of_etree venv vars a2 in
                        let venv = venv_add venv v (BodyOffset (v', op, a2)) in
                           cenv, venv, head, vars
                     else
                        (* For nontrival multiplier, make an induction variable *)
                        let a2 = canonicalize_etree (ETBinop (op, a2, ETVar v')) in
                        let cenv, head, a2 = simplify_etree cenv head a2 in
                        let v1 = new_symbol_string (Symbol.to_string v' ^ "_derived") in
                        let head = (v1, a2) :: head in
                        let vars = SymbolSet.add vars v1 in
                        let venv, vars, a1 = atom_of_etree venv vars a1 in
                        let venv = venv_add venv v1 (BodyInduction (v1, op, a1)) in
                        let venv = venv_add venv v (BodyInduction (v1, op, a1)) in
                           cenv, venv, head, vars
             | LoopInvariant (ETVar v') ->
                  let venv = venv_add venv v (BodyConst (AtomVar v')) in
                     cenv, venv, head, vars
             | LoopInvariant a ->
                  let cenv, head, a = simplify_etree cenv head a in
                  let venv, vars, a = atom_of_etree venv vars a in
                  let venv = venv_add venv v (BodyConst a) in
                     cenv, venv, head, vars
             | LoopUnknown ->
                  let venv = venv_add venv v BodyUnknown in
                     cenv, venv, head, vars) (ETreeTable.empty, venv_empty, [], SymbolSet.empty) venv
   in
      venv, head, vars

(************************************************************************
 * LOOP VARIABLE CLASSIFICATION
 ************************************************************************)

(*
 * Scan a function in the loop.
 *)
let rec loop_exp venv f funs e =
   let pos = string_pos "loop_exp" (exp_pos e) in
      match e with
         LetUnop (v, _, op, a, e) ->
            loop_unop_exp venv f funs pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            loop_binop_exp venv f funs pos v op a1 a2 e
       | TailCall (g, args) ->
            loop_tailcall_exp venv f funs pos g args
       | Match (a, cases) ->
            loop_match_exp venv f funs pos a cases
       | TypeCase (_, _, _, v, e1, e2) ->
            loop_typecase_exp venv f funs pos v e1 e2
       | LetExt (v, _, _, _, _, e)
       | LetAlloc (v, _, e)
       | LetSubscript (_, v, _, _, _, e) ->
            let venv = venv_add venv v LoopUnknown in
               loop_exp venv f funs e
       | LetAssert (v1, _, v2, _, e) ->
            loop_assert_exp venv f funs pos v1 v2 e
       | SetGlobal (_, _, _, _, e)
       | SetSubscript (_, _, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, e)
       | Debug (_, e) ->
            loop_exp venv f funs e
       | SpecialCall _ ->
            false, venv

(*
 * A var copy.
 *)
and loop_assert_exp venv f funs pos v1 v2 e =
   let pos = string_pos "loop_unop_exp" pos in
   let a = venv_lookup_var venv pos v2 in
   let venv = venv_add venv v1 a in
      loop_exp venv f funs e

(*
 * Catch simple unops.
 *)
and loop_unop_exp venv f funs pos v op a e =
   let pos = string_pos "loop_unop_exp" pos in
   let a = venv_lookup_atom venv pos a in
   let a =
      match op, a with
         IdOp, _ ->
            a
       | _, LoopInvariant a ->
            LoopInvariant (canonicalize_etree (ETUnop (op, a)))
       | _ ->
            LoopUnknown
   in
   let venv = venv_add venv v a in
      loop_exp venv f funs e

(*
 * Binary operations.
 *)
and loop_binop_exp venv f funs pos v op_orig a1 a2 e =
   let pos = string_pos "loop_binop_exp" pos in
   let a1 = venv_lookup_atom venv pos a1 in
   let a2 = venv_lookup_atom venv pos a2 in
   let a =
      match op_orig, a1, a2 with
         _, LoopInvariant a1, LoopInvariant a2 ->
            LoopInvariant (canonicalize_etree (ETBinop (op_orig, a1, a2)))

       | PlusIntOp, LoopInvariant a2, LoopInduction (v, op, _)
       | PlusIntOp, LoopInduction (v, op, _), LoopInvariant a2 ->
            LoopDerived (v, op, ETConst (AtomInt 1), a2)

       | PlusRawIntOp (pre, signed), LoopInvariant a2, LoopInduction (v, op, _)
       | PlusRawIntOp (pre, signed), LoopInduction (v, op, _), LoopInvariant a2 ->
            LoopDerived (v, op, ETConst (AtomRawInt (Rawint.of_int pre signed 1)), a2)

       | MinusIntOp, LoopInduction (v, op, _), LoopInvariant a2 ->
            LoopDerived (v, op, ETConst (AtomInt 1), canonicalize_etree (ETUnop (UMinusIntOp, a2)))

       | MinusRawIntOp (pre, signed), LoopInduction (v, op, _), LoopInvariant a2 ->
            LoopDerived (v, op,
                         ETConst (AtomRawInt (Rawint.of_int pre signed 1)),
                         canonicalize_etree (ETUnop (UMinusRawIntOp (pre, signed), a2)))

       | MulIntOp, LoopInvariant a2, LoopInduction (v, op, _)
       | MulIntOp, LoopInduction (v, op, _), LoopInvariant a2 ->
            LoopDerived (v, op, a2, ETConst (AtomInt 0))

       | MulRawIntOp (pre, signed), LoopInvariant a2, LoopInduction (v, op, _)
       | MulRawIntOp (pre, signed), LoopInduction (v, op, _), LoopInvariant a2 ->
            LoopDerived (v, op, a2, ETConst (AtomRawInt (Rawint.of_int pre signed 0)))

       | PlusIntOp, LoopInvariant a3, LoopDerived (v, op, a1, a2)
       | PlusIntOp, LoopDerived (v, op, a1, a2), LoopInvariant a3
       | PlusRawIntOp _, LoopInvariant a3, LoopDerived (v, op, a1, a2)
       | PlusRawIntOp _, LoopDerived (v, op, a1, a2), LoopInvariant a3 ->
            LoopDerived (v, op, a1, canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | MinusIntOp, LoopDerived (v, op, a1, a2), LoopInvariant a3
       | MinusRawIntOp _, LoopDerived (v, op, a1, a2), LoopInvariant a3 ->
            LoopDerived (v, op, a1, canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | MulIntOp, LoopInvariant a3, LoopDerived (v, op, a1, a2)
       | MulIntOp, LoopDerived (v, op, a1, a2), LoopInvariant a3
       | MulRawIntOp _, LoopInvariant a3, LoopDerived (v, op, a1, a2)
       | MulRawIntOp _, LoopDerived (v, op, a1, a2), LoopInvariant a3 ->
            LoopDerived (v, op, canonicalize_etree (ETBinop (op_orig, a1, a3)), canonicalize_etree (ETBinop (op_orig, a2, a3)))

       | _ ->
            LoopUnknown
   in
   let venv = venv_add venv v a in
      loop_exp venv f funs e

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and loop_tailcall_exp venv f funs pos g args =
   let pos = string_pos "loop_tailcall_exp" pos in
      try
         let _, _, vars, _ = SymbolTable.find funs g in
            if Symbol.eq g f then
               loop_self_args venv pos vars args
            else
               loop_other_args venv pos vars args
      with
         Not_found ->
            false, venv
(*
 * In the loop call, check for induction variables.
 *)
and loop_self_args venv pos vars args =
   let pos = string_pos "loop_self_args" pos in
      List.fold_left2 (fun (changed, venv) v a ->
            let a2 = venv_lookup_atom venv (int_pos 1 pos) a in
            let a1 = venv_lookup_var venv (int_pos 2 pos) v in
            let changed, a =
               match a1, a2 with
                  LoopInvariant a1', LoopInvariant a2' ->
                     if a1' = a2' then
                        changed, a2
                     else
                        true, loop_self_arg a1' a2'
                | LoopInduction _, LoopInduction _
                | LoopDerived _, LoopDerived _ ->
                     if a1 = a2 then
                        changed, a2
                     else
                        true, LoopUnknown
                | LoopInduction (u1, op1, a1), LoopDerived (v1, op2, b1, b2) ->
                     loop_induction_arg changed u1 op1 a1 v1 op2 b1 b2
                | LoopUnknown, LoopUnknown ->
                     changed, a2
                | LoopInvariant _, LoopInduction _
                | LoopInvariant _, LoopDerived _
                | LoopInduction _, LoopInvariant _
                | LoopDerived _, LoopInvariant _
                | LoopDerived _, LoopInduction _
                | _, LoopUnknown
                | LoopUnknown, _ ->
                     true, LoopUnknown
            in
               if debug debug_loop then
                  begin
                     Format.printf "@[<v 3>Loop param: ";
                     print_symbol v;
                     Format.print_string " ";
                     Format.print_bool changed;
                     Format.print_space ();
                     print_vloop a1;
                     Format.print_space ();
                     print_vloop a2;
                     Format.print_space ();
                     print_vloop a;
                     Format.close_box ();
                     Format.print_newline ()
                  end;
               changed, venv_add venv v a) (false, venv) vars args

(*
 * Tie together invariants to produce induction
 * variables.
 *)
and loop_self_arg a1 a2 =
   match a1 with
      ETVar v1 ->
         (match a2 with
             ETBinop (PlusIntOp as op, ETConst c1, ETVar v2)
             when Symbol.eq v2 v1 ->
                LoopInduction (v1, op, ETConst c1)

           | ETBinop (PlusRawIntOp (pre, signed) as op, ETConst c1, ETVar v2)
             when Symbol.eq v2 v1 ->
                LoopInduction (v1, op, ETConst c1)

           | _ ->
                LoopUnknown)
    | _ ->
         LoopUnknown

(*
 * Check the induction.
 *)
and loop_induction_arg changed u1 op1 a1 v1 op2 b1 b2 =
   if Symbol.eq u1 v1 && a1 = b2 && op1 = op2 then
      match b1 with
         ETConst (AtomInt 1) ->
            changed, LoopInduction (u1, op1, a1)

       | ETConst (AtomRawInt i) when Rawint.is_one i ->
            changed, LoopInduction (u1, op1, a1)

       | _ ->
            true, LoopUnknown
   else
      true, LoopUnknown

(*
 * In an other tailcall,
 * Just assign the new values.
 *)
and loop_other_args venv pos vars args =
   let pos = string_pos "loop_other_args" pos in
      List.fold_left2 (fun (changed, venv) v a ->
            let a2 = venv_lookup_atom venv pos a in
            let changed =
               if changed then
                  changed
               else
                  try SymbolTable.find venv v <> a2 with
                     Not_found ->
                        true
            in
               changed, venv_add venv v a2) (false, venv) vars args

(*
 * Match expression.
 *)
and loop_match_exp venv f funs pos a cases =
   let pos = string_pos "loop_match_exp" pos in
      List.fold_left (fun (changed, venv) (_, e) ->
            let changed', venv = loop_exp venv f funs e in
               changed || changed', venv) (false, venv) cases

(*
 * Typecase.
 *)
and loop_typecase_exp venv f funs pos v e1 e2 =
   let pos = string_pos "loop_typecase_exp" pos in
   let venv = venv_add venv v LoopUnknown in
   let changed1, venv = loop_exp venv f funs e1 in
   let changed2, venv = loop_exp venv f funs e2 in
      changed1 || changed2, venv

(*
 * Found a loop.
 *)
let loop_step venv f funs loops =
   List.fold_left (fun (changed1, venv) (_, _, _, e) ->
         let changed2, venv = loop_exp venv f funs e in
            changed1 || changed2, venv) (false, venv) loops

let loop_henv prog f body =
   let { prog_funs = funs;
         prog_names = names;
         prog_globals = globals
       } = prog
   in

   (* Add all the globals to the venv *)
   let venv = venv_empty in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v LoopUnknown) venv funs
   in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v LoopUnknown) venv globals
   in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v LoopUnknown) venv names
   in

   (* Get the functions in the loop *)
   let labels = Trace.to_list body in
   let all_labels = f :: labels in
   let loops = List.map (SymbolTable.find funs) all_labels in

   (* Get vars for the head function *)
   let _, _, vars, _ = SymbolTable.find funs f in
   let vars' = List.map new_symbol vars in
   let venv =
      List.fold_left2 (fun venv v v' ->
            venv_add venv v (LoopInvariant (ETVar v'))) venv vars vars'
   in

   (* Classify all the vars *)
   let rec fixpoint venv =
      let changed, venv = loop_step venv f funs loops in
         if changed then
            fixpoint venv
         else
            venv
   in
   let venv_orig = fixpoint venv in

   (* Construct the henv *)
   let venv, head, vars = simplify_venv venv_orig in
   let vars = SymbolSet.to_list vars in
   let args = List.map (fun v -> AtomVar v) vars in
   let loops =
      List.fold_left2 (fun loops v fund ->
            SymbolTable.add loops v fund) SymbolTable.empty all_labels loops
   in
   let f' = new_symbol_string (Symbol.to_string f ^ "_innerloop") in
   let henv =
      { henv_head = List.rev head;
        henv_body = venv;
        henv_funs = funs;
        henv_loops = loops;
        henv_vars = vars;
        henv_args = args;
        henv_fun = f;
        henv_funi = f'
      }
   in
      if debug debug_loop then
         begin
            print_venv venv_orig;
            print_henv henv
         end;
      henv

(************************************************************************
 * HOISTING
 ************************************************************************)

(*
 * Var renaming.
 *)
let hoist_var henv pos v =
   let a =
      try SymbolTable.find henv.henv_body v with
         Not_found ->
            raise (FirException (pos, UnboundVar v))
   in
      match a with
         BodyInduction (v, _, _) ->
            AtomVar v
       | BodyConst a ->
            a
       | BodyOffset _
       | BodyUnknown ->
            AtomVar v

let hoist_vars henv pos vars =
   List.map (hoist_var henv pos) vars

let hoist_fun_var henv pos f =
   let pos = string_pos "hoist_fun_var" pos in
      match hoist_var henv pos f with
         AtomVar v ->
            v
       | a ->
            raise (FirException (pos, StringError "not a var"))

(*
 * Lookup the new value for an atom.
 *)
let hoist_atom henv pos a =
   match a with
      AtomVar v ->
         hoist_var henv pos v
    | _ ->
         a

let hoist_atoms henv pos args =
   List.map (hoist_atom henv pos) args

let hoist_args henv pos args =
   hoist_atoms henv pos args

(*
 * Hoist the predicate.
 *)
let hoist_pred henv pos pred =
   match pred with
      PredNop (v, op) ->
         PredNop (hoist_fun_var henv pos v, op)
    | PredUnop (v, op, index, a) ->
         PredUnop (hoist_fun_var henv pos v, op, index, hoist_atom henv pos a)
    | PredBinop (v, op, index, a1, a2) ->
         PredBinop (hoist_fun_var henv pos v, op, index, hoist_atom henv pos a1, hoist_atom henv pos a2)

(*
 * Hoist expressions in a function body.
 *)
let rec hoist_exp henv e =
   let pos = string_pos "hoist_exp" (exp_pos e) in
      match e with
         LetUnop (v, ty, op, a, e) ->
            hoist_unop_exp henv pos v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            hoist_binop_exp henv pos v ty op a1 a2 e
       | TailCall (g, args) ->
            hoist_tailcall_exp henv pos g args
       | Match (a, cases) ->
            hoist_match_exp henv pos a cases
       | TypeCase (a1, a2, name, v, e1, e2) ->
            hoist_typecase_exp henv pos a1 a2 name v e1 e2
       | LetExt (v, ty1, s, ty2, args, e) ->
            hoist_ext_exp henv pos v ty1 s ty2 args e
       | LetAlloc (v, op, e) ->
            hoist_alloc_exp henv pos v op e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            hoist_subscript_exp henv pos op v1 ty v2 a e
       | SetGlobal (op, v, ty, a, e) ->
            hoist_set_global_exp henv pos op v ty a e
       | SetSubscript (op, v, a1, ty, a2, e) ->
            hoist_set_subscript_exp henv pos op v a1 ty a2 e
       | Memcpy (op, v1, a1, v2, a2, a3, e) ->
            hoist_memcpy_exp henv pos op v1 a1 v2 a2 a3 e
       | LetAssert (v1, ty, v2, pred, e) ->
            hoist_assert_exp henv pos v1 ty v2 pred e
       | Debug (info, e) ->
            hoist_debug_exp henv pos info e
       | SpecialCall op ->
            hoist_special_call_exp henv pos op

(*
 * Hoist a def.  If this is the def of
 * a derived variable, do the addition.
 *)
and hoist_def henv pos v ty e =
   let pos = string_pos "hoist_def" pos in
   let e = hoist_exp henv e in
   let a =
      try SymbolTable.find henv.henv_body v with
         Not_found ->
            raise (FirException (pos, UnboundVar v))
   in
      match a with
         BodyOffset (v', op, a) ->
            LetBinop (v, ty, op, a, AtomVar v', e)
       | BodyInduction (v', _, _) ->
            LetUnop (v, ty, IdOp, AtomVar v', e)
       | BodyConst a ->
            LetUnop (v, ty, IdOp, a, e)
       | BodyUnknown ->
            e

(*
 * Unary operator.
 * The operation may be tansformed.
 *)
and hoist_unop_exp henv pos v ty op a e =
   let pos = string_pos "hoist_unop_exp" pos in
   let e = hoist_def henv pos v ty e in
      LetUnop (v, ty, op, hoist_atom henv pos a, e)

(*
 * Binary operator.
 *)
and hoist_binop_exp henv pos v ty op a1 a2 e =
   let pos = string_pos "hoist_unop_exp" pos in
   let e = hoist_def henv pos v ty e in
      LetBinop (v, ty, op, hoist_atom henv pos a1, hoist_atom henv pos a2, e)

(*
 * Tailcall.
 *)
and hoist_tailcall_exp henv pos f args =
   let pos = string_pos "hoist_tailcall_exp" pos in
      if Symbol.eq f henv.henv_fun then
         hoist_tailcall_self henv pos args
      else if SymbolTable.mem henv.henv_loops f then
         hoist_tailcall_loop henv pos f args
      else
         hoist_tailcall_other henv pos f args

(*
 * In a self tailcall, we compute new values for
 * all the induction vars.
 *)
and hoist_tailcall_self henv pos args =
   let pos = string_pos "hoist_tailcall_self" pos in
   let args = hoist_args henv pos args in
   let args = henv.henv_args @ args in
   let e = TailCall (henv.henv_funi, args) in
      List.fold_left (fun e v ->
            let a =
               try SymbolTable.find henv.henv_body v with
                  Not_found ->
                     raise (FirException (pos, UnboundVar v))
            in
               match a with
                  BodyInduction (v', op, a) ->
                     LetBinop (v', TyDelayed, op, AtomVar v', a, e)
                | BodyOffset _
                | BodyConst _
                | BodyUnknown ->
                     e) e henv.henv_vars

(*
 * In a loop tailcall, just add the extra args.
 *)
and hoist_tailcall_loop henv pos f args =
   let pos = string_pos "hoist_tailcall_loop" pos in
   let args = hoist_args henv pos args in
   let args = henv.henv_args @ args in
      TailCall (f, args)

(*
 * When exiting the loop, just rename the args.
 * Be sure to rename the function too.
 *)
and hoist_tailcall_other henv pos f args =
   let pos = string_pos "hoist_tailcall_other" pos in
   let args = hoist_args henv pos args in
   let f = hoist_fun_var henv pos f in
      TailCall (f, args)

(*
 * Hoist a match.
 *)
and hoist_match_exp henv pos a cases =
   let pos = string_pos "hoist_match_exp" pos in
   let a = hoist_atom henv pos a in
   let cases = List.map (fun (s, e) -> s, hoist_exp henv e) cases in
      Match (a, cases)

(*
 * Typecase.
 *)
and hoist_typecase_exp henv pos a1 a2 name v e1 e2 =
   let pos = string_pos "hoist_typecase_exp" pos in
   let a1 = hoist_atom henv pos a1 in
   let a2 = hoist_atom henv pos a2 in
   let name = hoist_fun_var henv pos name in
   let v = hoist_fun_var henv pos v in
   let e1 = hoist_exp henv e1 in
   let e2 = hoist_exp henv e2 in
      TypeCase (a1, a2, name, v, e1, e2)

(*
 * External call.
 *)
and hoist_ext_exp henv pos v ty1 s ty2 args e =
   let pos = string_pos "hoist_ext_exp" pos in
   let v = hoist_fun_var henv pos v in
   let args = hoist_atoms henv pos args in
   let e = hoist_exp henv e in
      LetExt (v, ty1, s, ty2, args, e)

(*
 * Allocation.
 *)
and hoist_alloc_exp henv pos v op e =
   let pos = string_pos "hoist_alloc_exp" pos in
   let v = hoist_fun_var henv pos v in
   let op = hoist_alloc_op henv pos op in
   let e = hoist_exp henv e in
      LetAlloc (v, op, e)

and hoist_alloc_op henv pos op =
   let pos = string_pos "hoist_alloc_op" pos in
      match op with
         AllocTuple (tclass, ty, args) ->
            AllocTuple (tclass, ty, hoist_atoms henv pos args)
       | AllocUnion (ty, v, i, args) ->
            AllocUnion (ty, v, i, hoist_atoms henv pos args)
       | AllocArray (ty, args) ->
            AllocArray (ty, hoist_atoms henv pos args)
       | AllocMArray (ty, args, a) ->
            AllocMArray (ty, hoist_atoms henv pos args, hoist_atom henv pos a)
       | AllocMalloc a ->
            AllocMalloc (hoist_atom henv pos a)

(*
 * Subscripting.
 *)
and hoist_subscript_exp henv pos op v1 ty v2 a e =
   let pos = string_pos "hoist_subscript_exp" pos in
   let v1 = hoist_fun_var henv pos v1 in
   let v2 = hoist_fun_var henv pos v2 in
   let a = hoist_atom henv pos a in
   let e = hoist_exp henv e in
      LetSubscript (op, v1, ty, v2, a, e)

and hoist_set_subscript_exp henv pos op v a1 ty a2 e =
   let pos = string_pos "hoist_set_subscript_exp" pos in
   let v = hoist_fun_var henv pos v in
   let a1 = hoist_atom henv pos a1 in
   let a2 = hoist_atom henv pos a2 in
   let e = hoist_exp henv e in
      SetSubscript (op, v, a1, ty, a2, e)

and hoist_set_global_exp henv pos op v ty a e =
   let pos = string_pos "hoist_set_global_exp" pos in
   let a = hoist_atom henv pos a in
   let e = hoist_exp henv e in
      SetGlobal (op, v, ty, a, e)

and hoist_memcpy_exp henv pos op v1 a1 v2 a2 a3 e =
   let pos = string_pos "hoist_memcpy_exp" pos in
   let v1 = hoist_fun_var henv pos v1 in
   let a1 = hoist_atom henv pos a1 in
   let v2 = hoist_fun_var henv pos v2 in
   let a2 = hoist_atom henv pos a2 in
   let a3 = hoist_atom henv pos a3 in
   let e = hoist_exp henv e in
      Memcpy (op, v1, a1, v2, a2, a3, e)

and hoist_assert_exp henv pos v1 ty v2 pred e =
   let pos = string_pos "hoist_assert_exp" pos in
   let v1 = hoist_fun_var henv pos v1 in
   let v2 = hoist_fun_var henv pos v2 in
   let pred = hoist_pred henv pos pred in
   let e = hoist_exp henv e in
      LetAssert (v1, ty, v2, pred, e)

(*
 * Debugging.
 *)
and hoist_debug_exp henv pos info e =
   let pos = string_pos "hoist_debug_exp" pos in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) ->
                     v1, ty, hoist_fun_var henv pos v2) vars
            in
               DebugContext (line, vars)
   in
   let e = hoist_exp henv e in
      Debug (info, e)

(*
 * Specialcall.
 *)
and hoist_special_call_exp henv pos op =
   let pos = string_pos "hoist_special_call_exp" pos in
   let op = hoist_special_call_op henv pos op in
      SpecialCall op

and hoist_special_call_op henv pos op =
   let pos = string_pos "hoist_special_call_op" pos in
      match op with
         TailSysMigrate (i, a1, a2, f, args) ->
            let a1 = hoist_atom henv pos a1 in
            let a2 = hoist_atom henv pos a2 in
            let f = hoist_fun_var henv pos f in
            let args = hoist_atoms henv pos args in
               TailSysMigrate (i, a1, a2, f, args)
       | TailAtomic (f, a, args) ->
            let f = hoist_fun_var henv pos f in
            let a = hoist_atom henv pos a in
            let args = hoist_atoms henv pos args in
               TailAtomic (f, a, args)
       | TailAtomicRollback a ->
            let a = hoist_atom henv pos a in
               TailAtomicRollback a
       | TailAtomicCommit (f, args) ->
            let f = hoist_fun_var henv pos f in
            let args = hoist_atoms henv pos args in
               TailAtomicCommit (f, args)

(*
 * Build the preheader.
 *)
let hoist_preheader henv =
   let { henv_vars = vars;
         henv_args = args;
         henv_head = head;
         henv_funs = funs;
         henv_fun  = f;
         henv_funi = f'
       } = henv
   in

   (* Translate the function arguments *)
   let info, ty, vars', _ = SymbolTable.find funs f in
   let pos = string_pos "hoist_preheader" (vexp_pos f) in
   let args = args @ hoist_vars henv pos vars' in
   let vars' = List.map (hoist_fun_var henv pos) vars' in

   (* Etree should be a const or var *)
   let atom_of_etree e =
      match e with
         ETConst a ->
            a
       | ETVar v ->
            AtomVar v
       | _ ->
            raise (Invalid_argument "hoist_preheader.atom_of_etree")
   in

   (* Add the preheader code *)
   let rec build_body head =
      match head with
         (v, a) :: head ->
            let e = build_body head in
               (match a with
                   ETUnop (op, a) ->
                      LetUnop (v, TyDelayed, op, atom_of_etree a, e)
                 | ETBinop (op, a1, a2) ->
                      LetBinop (v, TyDelayed, op, atom_of_etree a1, atom_of_etree a2, e)
                 | ETVar v' ->
                      LetUnop (v, TyDelayed, IdOp, AtomVar v', e)
                 | ETConst a ->
                      LetUnop (v, TyDelayed, IdOp, a, e))
       | [] ->
            TailCall (f', args)
   in
   let e = build_body head in
      info, ty, vars', e

(*
 * Hoist the expressions in the loop.
 *)
let hoist_loop prog f labels =
   let { prog_funs = funs;
         prog_types = tenv
       } = prog
   in

   (* Build the hoist environment *)
   let henv = loop_henv prog f labels in
   let { henv_vars = vars;
         henv_loops = loops;
         henv_funi = f'
       } = henv
   in
   let ty_extra = List.map (fun _ -> TyDelayed) vars in

   (* Build a loop-preheader *)
   let fund = hoist_preheader henv in
   let funs = SymbolTable.add funs f fund in

   (* Now hoist in the function bodies *)
   let funs =
      SymbolTable.fold (fun funs v (line, ty, vars', e) ->
            (* Add the extra arguments *)
            let pos = string_pos "hoist_loop" (exp_pos e) in
            let ty_vars, ty_args, ty_res = dest_fun_type tenv pos ty in
            let ty_args = ty_extra @ ty_args in
            let ty_fun = TyAll (ty_vars, TyFun (ty_args, ty_res)) in
            let vars' = List.map (hoist_fun_var henv pos) vars' in
            let vars = vars @ vars' in
            let e = hoist_exp henv e in
            let fund = line, ty_fun, vars, e in
            let v = if Symbol.eq v f then f' else v in
               SymbolTable.add funs v fund) funs loops
   in
      { prog with prog_funs = funs }

(*
 * Search for loops in the program.
 *)
let rec hoist_trace prog l =
   match l with
      Trace [Elem f] :: t ->
         hoist_loop prog f []
    | Trace h :: t ->
         hoist_trace (hoist_trace prog h) t
    | _ :: t ->
         hoist_trace prog t
    | [] ->
         prog

(*
 * Optimize loops.
 *)
let hoist_prog prog =
   let trace = build_loop prog in
   let prog = hoist_trace prog trace in
   let prog = standardize_prog prog in
      if debug debug_loop then
         begin
            debug_prog "Hoist" prog;
            Fir_infer.check_prog prog
         end
      else
         prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
