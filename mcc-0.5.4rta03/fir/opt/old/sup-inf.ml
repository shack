%
*************************************************************************
*                                                                       *
*    Copyrighted Cornell University 1994                                *
*                                                                       *
*                                                                       *
*                Nuprl Proof Development System                         *
*                ------------------------------                         *
*                                                                       *
*   Developed by the Nuprl group, Department of Computer Science,       *
*   Cornell University, Ithaca NY.  See the release notes for a list    *
*   of the members of the group.                                        *
*                                                                       *
*   Permission is granted to use and modify Nuprl provided this notice  *
*   is retained in derived works.                                       *
*                                                                       *
*                                                                       *
*************************************************************************
%

%[
****************************************************************************
****************************************************************************
SUP-INF.ML
****************************************************************************
****************************************************************************
Added capability for generalizing non-linear expressions March 1st 94.

]%

%
Overview of algorithm
~~~~~~~~~~~~~~~~~~~~~
The algorithm is described in:

"On the SUP-INF Method for Proving Presburger Formulas",
by Robert E. Shostak in JACM vol 24 #4, Oct 1977, pp529-543.

It is originally due to W. Bledsoe at UTexas Austin.

The core SupInf algorithm tries to find a satisfying assignment of constants
to variables in a formula P of form:

P ::= Or(Q1;...;Qn) (n >= 0)

Q ::= And(A1;...;An) (n >= 0)

A ::= 0 =< Z

and Z is an integer arithmetic expression that can be put in normal form

  z0 + z1*v1 + ... + zn*vn    

where z0...zn are integer constants, z1,...,zn non zero. v1,...,vn are 
distinct variables.

The steps are:

1. Evaluate atomic propositions with no variables. Simplify logical
structure.  (i.e. eliminate disjunct with false conjunct. Eliminate
true conjuncts.)  If there exists a choice of values for the variables
which makes the whole proposition true, then the input proposition is
false. 

2. Each disjunct is now a linear programming problem, in one or more
variables. We apply the rest of the procedure to each disjunct in turn.
If we find one which is satisfiable, we know the input proposition is false.

For each set of normalised integer inequalities:

  3. For each variable vj in an integer inequality:

     0 =<  z0 + z1*v1 + ... + zn*vn    

     Generate a rational inequality:

      either if zj > 0

         vj >= (z0/-zj) + (z1/-zj)*v1 + ... + (zj-1/-zj) 
                        + (zj+1/-zj)*vj+1 + ... + (zn/-zj)*vn

      or if zj < 0

         vj =< (z0/-zj) + (z1/-zj)*v1 + ... + (zj-1/-zj) 
                        + (zj+1/-zj)*vj+1 + ... + (zn/-zj)*vn


  4. Gather together all inequalities bounding each variable above and
     below. If vars are v1,...,vm, Then we get

       L1 =<  v1  =< U1
       .  .    .  .   .
       .  .    .  .   .
       .  .    .  .   .
       Lm =<  vm  =< Um

    where if vj has upper bounds u1,...up-j
     Uj = min(u1,...,up-j) if j >= 2
     Uj = u1               if j = 1
     Uj = +qinf            if j = 0

    and similarly for Lj.

    Do obvious simplifications to min and max expressions...??

    Define functions:

      Upper(vj) = Uj
      Lower(vj) = Lj


  5. Run Sup-Inf procedure with input v1,...,vm 
     and functions Upper, and Lower.

     
%


let si_unit_var = null_var ;;


%[
****************************************************************************
IVAR SUPPORT
****************************************************************************
We give variables integer codes to enable them to be easily ordered.
Use 1000+ to avoid some confusion with coefficients.
Could define abstract type...
]%

let si_unit_ivar = 1000 ;;

let mk_ivar_term ivar =
  mk_ab_term (`ivar`,[mk_natural_parm ivar]) []
;;

let dest_ivar t =
  let opid,par = dest_np_term t in
  if opid = `ivar` then par else failwith `dest_ivar`
;;

let si_elim_ivars_in_term tm_ivar_alist t =
  sweep_up_map
    (\t. let opid,ivar = dest_np_term t in
         if opid = `ivar` then rev_apply_alist tm_ivar_alist ivar
         else t
    )
    t
;;

let mk_ivar_term ivar =
  mk_term (`ivar`,[mk_natural_parm ivar]) []
;;

let dest_ivar t =
  let opid,par = dest_np_term t in
  if opid = `ivar` then par else failwith `dest_ivar`
;;

let ivar_qnum_pr_to_term (iv,q) =
  (iv = si_unit_ivar) 
    => q
    | mk_simple_term 
        `qmul` 
        [q;mk_ivar_term iv]
;;

% NB: duplicates not removed %

let free_ivars t =
    preorder_accumulate
    (\t acc. dest_ivar t . acc ? acc)
    []
    t
;;


%[
****************************************************************************
CONSTRUCTING AND DESTRUCTING INTEGER EXPRESSIONS
****************************************************************************
]%

let si_one = mk_integer_term 1 ;;
let si_zero = mk_integer_term 0 ;;

% first pair of output is always the constant %

% si_dest_arith_norm_exp : term -> (int # term) list
and si_mk_arith_norm_exp : (int # term) list -> term

are intended to be inverses of each other

`term' format is that output by arith_simplify_term

Assumed to be 

  e0 + e1 + ... + en 

with n>0, + right associated, only e0 possibly constant,
and integer coefficient of each ei (if not 1) as leftmost multiplicand
in right associated *.

`(int # term) list' format assumed to be

i1,t1 ... im,tm

t1 is always '1', and all other ti are non constant. ij are integer
coefficients of each addend, factored out.


Nomenclature:

  atm = arithmetic term (integer valued expression)
  norm = normalized
  lin  = linear

 %


let si_norm_atm_to_int_tm_prs e = 
  let dest_addend a = 
    (let b,c = dest_multiply a 
     in
       dest_integer b,c ? 1, a
    ) 
    ? -1,dest_minus a 
    ? 1,a 
  in 
  let addends = unreduce1 dest_add e
  in
  if is_integer_term (hd addends) then
    (dest_integer (hd addends),si_one) . map dest_addend (tl addends)
  else
    (0,si_one) . map dest_addend addends
;;
  
let si_int_tm_prs_to_norm_atm nts = 
  let mk_addend (n,t) = 
    if t = si_one then mk_integer_term n 
    if n = 1 then t
    if n = -1 then mk_minus_term t
    else mk_multiply_term (mk_integer_term n) t
  in 
  if fst (hd nts) = 0 then
  ( reduce1 mk_add_term (map mk_addend (tl nts)) ? si_zero)
  else
    reduce1 mk_add_term (map mk_addend nts)
;;

let si_lin_norm_atm_to_var_int_prs a = 
  let f (i,t) = 
    let v = dest_var t 
            ? failwith `si_lin_norm_atm_to_var_int_prs: non-lin atm` 
    in
      v,i
  in
  let int_tm_prs = si_norm_atm_to_int_tm_prs a 
  in
    (null_var, fst (hd int_tm_prs))
    . map f (tl int_tm_prs)
;;


let si_ivar_int_prs_to_lin_norm_atm (v_iv_alist : (var # int) list) prs = 
  si_int_tm_prs_to_norm_atm 
    ((snd (hd prs),si_one)
    .map (\ivar,i.i, mvt (rev_apply_alist v_iv_alist ivar)) (tl prs))
;;


%[
****************************************************************************
ELIMINATING NOTS AND SIMPLIFYING INT EXPRESSIONS
****************************************************************************
]%

% 
2a,b: Elim not, <, > .GE., !=
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
input u : term in grammar

P ::= not P
    | P1 and P2
    | P1 or P2
    | A

A ::= Z1 .GE. Z2         (greater or equal)
    | Z1 .LE. Z2         (less or equal)
    | Z1 >  Z2
    | Z1 <  Z2
    | Z1 =  Z2 in Int
    | Z1 != Z2 in Int   (not equal)

output v : term in grammar

P ::= P1 and P2
    | P1 or P2
    | A

A ::= 0 .LE. Z 
    | 0 = Z

    where Z is an integer expression in normal form
    produced by arith_simplify_term.
%

let si_elim_nots_and_norm_rels t = 

  let mk_tm = mk_simple_term
  in let mk_le a b= mk_tm `le` 
       [mk_integer_term 0
        ;arith_simplify_term (mk_tm `subtract` [b;a])]
  
  in let mk_eq a b = mk_simple_term `equal` 
       [int_term
       ;mk_integer_term 0
       ;arith_simplify_term (mk_tm `subtract` [b;a])]

  in let mk_p1 e = mk_tm `add` [e;mk_integer_term 1]
  in

  % parity true = normal, false = inverted %

  letrec simp parity t = 
    let ts = subterms t
    in let descend pty = map (simp pty) ts
    in let opid = opid_of_term t
    in 
    if parity then
    % invert case %
    ( if opid = `and` then mk_tm `or`  (descend true)
      if opid = `or`  then mk_tm `and` (descend true)
      if opid = `not` then hd (descend false)
      if opid = `le`  then mk_le (mk_p1 (second ts)) (first  ts)
      if opid = `less_than`  
                      then mk_le (second ts) (first ts)
      if opid = `equal` then mk_tm `or` 
                              [mk_le (mk_p1 (second ts)) (third  ts)
                              ;mk_le (mk_p1 (third  ts)) (second ts)]
      else failwith (`si_elim_nots_and_norm_rels: bad opid: ` ^ opid)
    )
    else
    % normal case %
    ( if opid = `and` then mk_tm `and`  (descend false)
      if opid = `or`  then mk_tm `or`   (descend false)
      if opid = `not` then hd (descend true)
      if opid = `le`  then mk_le (first ts) (second ts)
      if opid = `less_than`  then mk_le (mk_p1 (first ts)) (second ts)
      if opid = `equal` then mk_eq (second ts) (third ts)
      else failwith (`si_elim_nots_and_norm_rels: bad opid: ` ^ opid)
    )
  in
    simp false t
;;


%[
****************************************************************************
GENERALIZING NON-LINEAR INTEGER EXPRESSIONS
****************************************************************************

An expression is suitable for generalization if:

1. it is a subterm of an argument to an arithmetic relation.
2. it is an immediate subterm of either
   a) an arithmetic relation
   b) an arithmetic operator + - uminus or *.


Input at start:
1. a Proposition to process.
2. a function for inferring arithmetic
   properties of non-linear expressions.

Multi-pass:

  Each pass:
  1. i/p
      a) prop (P) containing 0 or more expressions to generalize 
         (On first pass, the original proposition,
          On later passes, only the new stuff generated by previous pass)
      b) list of already generalized exps (AGEs) (over all previous passes)
         each paired with the name of the variable it was generalized to.
  2. extract list of expressions to generalize (ETG) from P
     if e in ETG is an AGE then replace it by the corresponding var from AGE.
     otherwise, invent distinct name for var v to associate e with and replace
     e with v.
  3. Infer arithmetic properties term (AP) for each of e in ETG. 

  4. Output a) P' with all generalizable expressions generalized.
            b) All newly generated APs collected together.
            c) A list of the new v,e pairs


Looping:
  If new expression list is empty then done. Output all e,v,T triples
  and conjunct of P's output by each pass.
  Otherwise, add new e,v pairs to those input to last pass
  and use as input to next pass. Use output TP's of previous pass
  as the P input to next pass.

This always terminates. Note that any ETG's occurring in the type
of an ETG must come strictly earlier in the library. Infinite looping
behaviour would need this to be violated.

Q. at what stage of simplification do we run this?

A1: Before arithsimp done. 
     (arithsimp includes breaking not_eq, shifting all args
    to rhs of arith rels, and running arith simp procedure to put 
    exps in sum of product forms) 
    Then don't catch non-linear exps. These must be handled on separate 
    generalization pass.

A2: After arithsimp done. However here, TP's will not normally
    be in arithsimped normal form. 

Solution is to arithsimp TP's before feeding in to successive
passes. NB: putting expressions in DNF must come after this generalization
phase.
]%


%
-----------------------------------------------------------------------------
si_generalize_step (Q:term) (old_vts : (var # term) list)
  =>
  (Q' : term) , (new_vts : (var # term) list)

Generalizes non-linear arithmetic expressions in Q to variables.
Tries to use old_vts to find vars to generalize to,
but if that fails, resorts to creating new generalization binding.
-----------------------------------------------------------------------------
%

let si_map_on_arith_addends f e = 
    si_int_tm_prs_to_norm_atm (map (id # f) (si_norm_atm_to_int_tm_prs e))
;;

% assumes p in form output by si_elim_nots_and_norm_rels %

let si_map_on_arith_exps_in_prop f t = 
  letrec aux t = 
    if is_terms ``and or`` t then map_on_immediate_subterms aux t
    if is_term `le` t then 
    ( let (),[a;b] = dest_simple_term t in
      mk_simple_term `le` [a;f b]
    )
    if is_term `equal` t then 
    ( let (),[T;a;b] = dest_simple_term t in
      mk_simple_term `equal` [T;a;f b]
    )
    else failwith (`si_map_on_arith_exps_in_prop: bad opid: ` ^ opid_of_term t)
  in
    aux t
;;

letref si_var_index = 0 ;;

let si_gen_var (():unit) = 
  tok_to_var (`&` ^ int_to_tok (si_var_index := si_var_index + 1))
;;

let si_is_gen_var v = 
  hd (explode (var_to_tok v)) = `&`
;;

let si_generalize_step Q old_vts = 
  letref new_vts = [] : (var#term) list in
  
  letrec gen t = 
    let opid = opid_of_term t in
    if opid = `variable` then t
    if opid = `natural_number` then t
    else
    (  mk_var_term (rev_apply_alist old_vts t)
       ? mk_var_term (rev_apply_alist new_vts t)
       ? (let v = si_gen_var () in
            new_vts := add_to_alist_start v t new_vts
            ; mk_var_term v
         )
    )
  in
    si_map_on_arith_exps_in_prop
      (si_map_on_arith_addends gen) Q
   ,new_vts
;;

let si_generalize_step Q old_vts = 
  letref new_vts = [] : (var#term) list in
  
  letrec gen t = 
    let opid = opid_of_term t in
    if opid = `variable` then t
    if opid = `natural_number` then t
    else
    (  mk_var_term (rev_apply_alist old_vts t)
       ? mk_var_term (rev_apply_alist new_vts t)
       ? (let v = si_gen_var () in
            new_vts := add_to_alist_start v t new_vts
            ; mk_var_term v
         )
    )
  in
    si_map_on_arith_exps_in_prop
      (si_map_on_arith_addends gen) Q
   ,new_vts
;;

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_GENERALIZE
~~~~~~~~~~~~~

si_generalize (Q:term) (f:term->term # * list)
 =>
 genQ:term
 , vts : (var # term) list
 , js : * list

Generalizes non-linear expressions in Q to give part of GenQ.
Uses f to infer arithmetic properties of generalized expressions
and iteratively generalizes these too. These generalized arithmetic
properties make up the rest of GenQ. 

Assumes f returns propositions that have been run through
the arithsimp normalization procedure. 

The * list information justifies these new propositions
It is collected from each call to f, and output.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let si_generalize Q get_arith_properties = 

  letrec aux Q old_vts old_genQs js = 
    let genQ,new_vts = si_generalize_step Q old_vts
    in
    let new_properties = mapfilter (\v,t.get_arith_properties t) new_vts
    in
    if null new_properties then 
      mk_iterated_and (genQ.old_genQs) 
      , new_vts @ old_vts 
      , js
    else
      aux (mk_iterated_and (map fst new_properties)) 
          (new_vts @ old_vts) 
          (genQ.old_genQs)
          (flatten (map snd new_properties) @ js) 
  in
  let genQ,vts,js =  aux Q [] [] []
  in
    genQ
    ,vts
    ,js
;;
     



%[
****************************************************************************
PUTTING IN DNF
****************************************************************************
At this stage term is only made up of "and"s, "or"s, and "le"  terms with
zero lhs's.

input: 

  u : term in grammar

P ::= P1 and P2
    | P1 or P2
    | A

A ::= 0 .LE. Z      (less or equal)
    | 0  = Z

Output: v : term list list 

where 

 (mk_iterated_or 
  o map mk_iterated_and
 ) v 
 
is in grammar
P ::= P1 or P2
    | Q

Q ::= Q1 and Q2
    | A

A ::= 0 .LE. Z         (less or equal)
    | 0  = Z

]%
 


letrec si_put_in_dnf t =
  (let a,b = dest_or t in
     (si_put_in_dnf a) @ (si_put_in_dnf b)
  )
  ?
  (let a,b = dest_and t in
   let all_disjunct_prs = 
         all_permutations [si_put_in_dnf a; si_put_in_dnf b] in
     map (\[p;q].p @ q) all_disjunct_prs
  )
  ?
  [[t]]
;;



%[
****************************************************************************
ELIMINATING EQUALITIES
****************************************************************************
]%

%
Input is term e in arith_simplify_term norm form from equality  e = 0.

Output is binding v,e' such that 

e = 0 <=> v = e'

if no such binding can be generated, outputs null_var,-e
%

let si_orient_equality_or_invert e = 
  let ivs = si_norm_atm_to_int_tm_prs e
  in let unit_position = 
       search_list (\i,v.i = 1 or i = -1) (tl ivs) + 1 
       ? 0
  in
  if unit_position = 0 then
    null_var,si_int_tm_prs_to_norm_atm (map (\i,v.-i,v) ivs)
  else
  let i,v = nth unit_position ivs
  in 
  if i = 1 then
    dest_var v
    ,si_int_tm_prs_to_norm_atm 
      (map (\i,v.-i,v) (remove_nth unit_position ivs))
  else
    dest_var v
    ,si_int_tm_prs_to_norm_atm (remove_nth unit_position ivs)
;;



%
INPUT: 
conjunction of .le. and .eq. s.

OUTPUT 
conjunction of .le. only.

Take each equality. See if contains a var with + or -1 coefficient.
  If so, orient equality as substitution. Do substitution and 
  renormalize each arithmetic expression.

  (could collect coefs of each var and try substing out when
   one coef is within unit of gcd of all.)

  If not, split equality into two inequalities.
   0 .eq. e becomes  (0 .le e) and (0 .le. -e)


si_elim_equalities
  (cs : term list)
  =>
  (cs' : term list)
  ,sub : (var # term) list

Input prop = mk_iterated_and cs 
Output prop = mk_iterated_and (map (mk_le_term 0) cs')

%
let si_elim_equalities cs = 

  let dest t = 
    if is_term `le` t then `le`,subterm_of_term t 2
    else `equal`,subterm_of_term t 3
  in

  letrec aux cnjs_in cnjs_out sub =
    if null cnjs_in then 
       cnjs_out,sub
    else 
    let (opid,e).cs = cnjs_in 
    in
    if opid = `le` then
      aux cs (e.cnjs_out) sub
    else
    let v,e' = si_orient_equality_or_invert e
    in 
    if is_null_var v then
      aux cs (e.e'.cnjs_out) sub 
    else
    let do_sub t = 
      let t' = fo_subst [v,e'] t
      in 
        if t = t' then t else arith_simplify_term t'
    in
      aux (map (id # do_sub) cs) 
          (map do_sub cnjs_out) 
          ((v,e'). map (id # do_sub) sub)
  in
    aux (map dest cs) [] []
;;




%[
****************************************************************************
THINNING CONSTANT EXPRESSIONS
****************************************************************************
Input: u (ivar # int) list list list. 
Output: v (ivar # int) list list list. 

where 

 (mk_iterated_or 
  o map mk_iterated_and
  o map_2d (mk_le_term 0)
  o map_2d mk_iterated_sum
  o map_3d ivar_int_pair_to_term
 ) 
applied to u and v give terms in grammar

P ::= P1 or P2
    | Q

Q ::= Q1 and Q2
    | A

A ::= 0 .LE. Z         (less or equal)

with Z = z0 + z1*v1 + ... + zn*vn    

for input: n >= 0, and all vi distinct and in ascending order.
for output: n >= 1, and all vi distinct and in ascending order.
%

let si_thin_const_le_rels nf_prop =
  mapfilter
        % this applied to each disjunct of disjunction %
        (filter
           % this applied to each conjunct: an arithmetic inequality %
           (\ivar_coef_list.
              if length ivar_coef_list > 1 then true
              % length = 1. ie. int expression is constant. %

              if snd (hd ivar_coef_list) GE 0 then 
              % conjunct true. Tell filter to leave it out. % 
                  false
              % constant conjunct false. Tell mapfilter to leave out whole
                disjunct %
              else fail
           )
        )
        nf_prop
;;
   

   
   


%[
****************************************************************************
RATIONAL ARITHMETIC
****************************************************************************
The rational number n/d n,d integers with d != 0 is represented
by the nuprl abstraction 'qnum(n;d)'

qnum(1;0) denotes +infinity
qnum(-1;0) denotes -infinity

qnum(x;0) with x other than 1 or -1 is not well formed.

We add a new type of variable to nuprl; an ivar with an integer rather
than token parameter. (For easy sorting).

ivar{n:nat}

Assume the following abstractions have been defined.

qadd(a;b)
qminus(a)
qmul(a;b)

qmax(a;b)
qmin(a;b)
]%

%
Rational Constants
~~~~~~~~~~~~~~~~~~
%

let qzero_term = 
  mk_simple_term `qnum` [mk_integer_term 0;mk_integer_term 1] ;;
let qone_term = 
  mk_simple_term `qnum` [mk_integer_term 1; mk_integer_term 1] ;;
let qinf_plus_term = 
  mk_simple_term `qnum` [mk_integer_term 1; mk_integer_term 0] ;;
let qinf_minus_term = 
  mk_simple_term `qnum` [mk_integer_term (-1); mk_integer_term 0] ;;

%
Constructors and Analyzers
~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let mk_qnum_term n d =
  if d = 0 then
  ( if n>0 then qinf_plus_term
    if n<0 then qinf_minus_term
    else
      failwith `mk_q_term 0/0 is not a valid term`
  )
  else
  let g = gcd n d * sign d in
    mk_simple_term `qnum` [mk_integer_term (n/g);mk_integer_term (d/g)]
;;



% For convenience, always return non neg denominators %

let dest_qnum q = 
  let [nt;dt] = dest_simple_term_with_opid `qnum` q in
  let n,d = dest_integer nt, dest_integer dt in
  if d < 0 then (-n),(-d) else n,d
;;

let norm_qnum_term q =
  let n,d = dest_qnum q in mk_qnum_term n d
;;

let is_qinf_term t = t = qinf_plus_term or t = qinf_minus_term ;;

let int_to_qnum i = mk_qnum_term i 1
;;

let qnum_to_int q =
  let n,d = dest_qnum (norm_qnum_term q) in
  if d = 1 then
    n
  else
    failwith `qnum_to_int: qnum not integer valued`
;;

let qnum_to_int_above q =
  let n,d = dest_qnum q in
  if d = 0 then 
    failwith `qnum_to_int_above: qnum infinite`
  if n < 0 then
    n / d
  else
    n/d + 1
;;

let qnum_to_int_below q =
 qnum_to_int_above q - 1
;;

let is_int_valued_qnum q =
  let n,d = dest_qnum (norm_qnum_term q) in
    d = 1
;;


%
Relations
~~~~~~~~~
%

let q_eq q1 q2 =
  if is_qinf_term q1 then
    q1 = q2
  else
  let n1,d1 = dest_qnum q1 in
  let n2,d2 = dest_qnum q2 in
    n1*d2 = n2*d1
;;

let q_le q1 q2 =
  if q1 = qinf_minus_term or q2 = qinf_plus_term then true
  if q1 = qinf_plus_term then q2 = qinf_plus_term
  if q2 = qinf_minus_term then q1 = qinf_minus_term
  else
  let n1,d1 = dest_qnum q1 in
  let n2,d2 = dest_qnum q2 in
    n1*d2 LE n2*d1
;;

let q_lt q1 q2 =
  if q1 = qinf_plus_term or q2 = qinf_minus_term then false
  if q1 = qinf_minus_term then not q2 = qinf_minus_term
  if q2 = qinf_plus_term then not q1 = qinf_plus_term
  else
  let n1,d1 = dest_qnum q1 in
  let n2,d2 = dest_qnum q2 in
    n1*d2 < n2*d1
;;

%
Binary rational functions
~~~~~~~~~~~~~~~~~~~~~~~~~
%

let q_plus q1 q2 =
  let n1,d1 = dest_qnum q1 in
  let n2,d2 = dest_qnum q2 in
  if d1 = 0 or d2 = 0 then
  ( if d1 = 0 & d2 = 0 then
    ( if n1 * n2 < 0 then
        failwith `q_plus: cannot add opposite infinities`
      else
        mk_qnum_term n1 0
    )
    if d1 = 0 then q1
    else q2
  )
  else
    mk_qnum_term (n1*d2 + n2*d1) (d1*d2)
;;

let q_times q1 q2 =
  let n1,d1 = dest_qnum q1 in
  let n2,d2 = dest_qnum q2 in
  if (d1 = 0 or d2 = 0) & (n1 = 0 or n2 = 0)  then 
      failwith `q_times: cannot multiply 0 and infinity`
  else
    mk_qnum_term (n1*n2) (d1*d2)
;;
      
let q_negate q =
  let n,d = dest_qnum q
  in
    mk_qnum_term (-n) d
;;

let q_max q1 q2 =
  if q_le q1 q2 then q2 else q1
;;

let q_min q1 q2 =
  if q_le q1 q2 then q1 else q2
;;

%[
****************************************************************************
Common constructors:
****************************************************************************
]%

let mk_qadd_term a b = mk_simple_ab_term `qadd` [a;b] ;;
let mk_qmul_term a b = mk_simple_ab_term `qmul` [a;b] ;;

% intended for expressions cleaned up a lot by input processing code %

letrec si_int_to_rat_exp e =
  let opid = opid_of_term e
  in let descend opid' = 
       mk_simple_ab_term opid' (map si_int_to_rat_exp (subterms e))
  in 
  if opid = `multiply` then descend `qmul`
  if opid = `add`      then descend `qadd`
  if opid = `minus` then mk_qmul_term (mk_qnum_term (-1) 1) (hd (subterms e))
  if opid = `natural_number` then mk_qnum_term (dest_integer e) 1
  if opid = `variable` then e
  else
    failwith (`si_int_to_rat_exp: unexpected opid: ` ^ opid)
;;

%[
****************************************************************************
EXTRACTION OF BOUNDS FOR EACH VAR IN LP (LINEAR PROGRAMMING) PROBLEM.
****************************************************************************
The basic function defined here is:

  si_find_lbs_ubs_for_disjunct 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INPUT: u : (ivar # int) list list 

where 

u' =
  (mk_iterated_and
  o map (mk_le_term 0)
  o map mk_iterated_sum
  o map_2d ivar_int_pair_to_term
 ) u

   = A1 and A2 and ... and Am     m > 0

with

   A = 0 .LE. z0 + z1*v1 + ... + zn*vn    n > 0

   zi integer constants, 
   All vi distinct and in ascending order

~~~~~~~~~~~~~~~~~~~~~~~~~~~~
OUTPUT v : (ivar # term) list # (ivar # term) list

v = (lower_bounds, upper_bounds) 

   bounds = [v1,q1;...;vn;qn] n > 0. (never have n=0 for both upper and lower)

   All vi distinct and in ascending order
   qi is rational valued expression in vj j != i
   qi are in `qnf' or rational normal form as described below:


When viewed as implicitly existentially quantified logical propositions, 
the input u and output v are equivalent.



DEFINITION OF QNF - RATIONAL NORMAL FORM - AND LINEAR QNF
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
(the binary ops qmax qmin + * all associate to the right)

Q ::= S1 qmax S2 qmax ... qmax Sn   n > 1 
    | S1 qmin S2 qmin ... qmin Sn   n > 1 

S ::= P1 + P2 + ... + Pn + K   (n >= 0) (K != 0.)
    | P1 + P2 + ... + Pn       (n > 0)
    | 0.
    | +INF | -INF

P ::= V
    | K' * V  (K' != 0. & K' != 1.)

    where K are all finite rational constants, and the ivar's V occurring in 
    the Pi are all distinct and in ascending order left-right.

Expressions in the form given by S above are in 
LINEAR QNF 
~~~~~~~~~~~~~~~~~~~~~~~~

  si_find_lbs_ubs_for_disjunct 

   is defined in terms of the three functions

  si_calc_var_bounds
  si_condense_var_bounds
  si_mk_linear_qnf_term 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
]%

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_CALC_VAR_BOUNDS
~~~~~~~~~~~~~~~~~~

INPUT is u : (ivar # int) list 

where ((mk_le_term 0)
       o mk_iterated_add
       o map ivar_int_pair_to_term) u

is the non negative integer expression

0 =< z0 + z1*v1 + ... zn*vn    z0...zn integer constants, n > 0
                               zi != 0 for i > 0
                               vi distinct

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
OUTPUT is v : (ivar # tok # (ivar # term) list) list

with v = [bnd1;...;bndn] (one bndi for each vi in input)

     bndi = vi,bi,ti

ti is a rational upper (lower) bound on variable vi if bi is `upper` (`lower`)

(si_mk_linear_qnf_term ti) is given by:

 (z1/-zj)*v1 + ... + (zj-1/-zj)*vj-1 
                   + (zj+1/-zj)*vj+1 + ... + (zn/-zj)*vn + (z0/-zj)

and if zi > 0, bi = `lower`
    if zi < 0, bi = `upper`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let si_calc_var_bounds lin_exp =
  map
  (\vj,zj.
     let rat_tm_list =
       map_omitting_failures
       (\v',k'.
         if v' = vj then fail
         else
           v',mk_qnum_term k' (-zj)
       )
       lin_exp
     in
       vj
       ,(if zj > 0 then `lower` else `upper`)
       ,rat_tm_list
  )
  (tl lin_exp)
;;

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_MK_LINEAR_QNF_TERM
~~~~~~~~~~~~~~~~~~~~~~~

INPUT is u : (ivar # term) list

where (reduce1 (\x y.mk_simple_ab_term `qadd` [x;y])
       o map mk_qaddend) u

is the rational expression

q0 + q1*v1 + ... qn*vn    q0...qn rational constants, n >= 0
                          with qi != 0. for i > 0

~~~~~~~~~~~~~~~~~~~~~~~
OUTPUT is v : term

  v = p1 + ... + pn + q0    if q0 != 0.
  v = p1 + ... + pn         if q0 = 0. and n > 0
  v = 0.                    if q0 = 0. and n = 0       
   
  pi = vi      if qi = 1.
  pi = qi*vi   if qi != 1.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

%
Used only in function specifications 
%
%
let si_qaddend (iv,q) = 
  if iv = si_unit_ivar then q
  else mk_qmul_term q (mk_ivar_term iv)
;;
%

let si_ivar_qnum_pr_to_term (iv,q) =
  if iv = si_unit_ivar then q
  if q = qone_term then mk_ivar_term iv
  else mk_qmul_term q (mk_ivar_term iv)
;;


let si_mk_linear_qnf_term linexp =
  let ((),qnum).iv_qn_prs = linexp
  in let addends = map si_ivar_qnum_pr_to_term iv_qn_prs
  in
  if not q_eq qnum qzero_term then 
     reduce mk_qadd_term qnum addends
  if null addends then
     qzero_term
  else
     reduce1 mk_qadd_term addends
;;


%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_CONDENSE_VAR_BOUNDS
~~~~~~~~~~~~~~~~~~~~~~

INPUT is u : (ivar # tok # term) list

with u = [bnd1;...;bndn] (n > 0)

     bndi = ui,bi,pi

if bi is `upper` (`lower`), pi is a linear qnf upper (lower) bound on variable 
ui.  pi contains variables vj for j != i.

~~~~~~~~~~~~~~~~~~~~~~
OUTPUT v : (ivar # term) list # (ivar # term) list

v = (lower_bounds, upper_bounds) 

   bounds = [v1,q1;...;vm;qm] m > 0. (never have m=0 for both upper and lower)

   All vi distinct and in ascending order.
   if bounds is lower_bounds (upper_bounds)
   then qi is a qnf lower (upper) bound on the value of variable vi.


NB:

1.lower_bounds and upper_bounds don't contain entries for variables which
  have no bounds. We allow for this later on, in the functions si_upper_lower. 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let si_condense_var_bounds var_btype_qexp_list =

  let lbs,ubs = divide_list (\v,b,q.b = `lower`) var_btype_qexp_list in

  let proc_bnds bnd_list reduce_id =
    let ivar_bnd_prs = map (\x,y,z.x,z) bnd_list in
    let sorted_bnds = quicksort (\x y.fst x < fst y) ivar_bnd_prs in
      multi_reduce1
        (\x y.if fst x = fst y then
                fst x ,mk_simple_term reduce_id [snd x;snd y]
              else
                fail
        )
        sorted_bnds
  in
  (proc_bnds lbs `qmax`), (proc_bnds ubs `qmin`)
;;




%[
****************************************************************************
The Rational Simplification function
****************************************************************************
NB: all multiplications have a constant left argument.

The function si_simp : term -> term 

1. Pulls min's and max's out to the front, watching signs.
2. Distributes k* over +
3. Does arithmetic simplification.
   a. evaluates constant expressions
   b. 1.* e = e
   c. 0 + e = e + 0 = e
   d. max(e + k1;e + k2) = e + eval(max(k1;k2))
      min(e + k1;e + k2) = e + eval(min(k1;k2))
   e. max(e;-inf) = e
      min(e;+inf) = e
]%

%

Simp Spec:
~~~~~~~~~~

Input u:term from grammar

Q ::= Qmax(Q1;Q2) 
    | Qmin(Q1;Q2)
    | Q1 + Q2
    | k * Q1
    | Var
    | k

Output v:term in grammar:

Q ::= QU | QL

QL ::= Qmax(QL1;QL2) | R
QU ::= Qmin(QU1;QU2) | R

R  ::= m1 + ... + mn
       | m1 + ... + mn + k

m  ::= v | k' * v

where:
  + is associated to right i.e. (a + (b + c))
  k is a finite non-zero or infinite rational.
  k'is non-zero, non-unit finite rational
  v in mi are distinct 
  n >= 0

Output arithmetically equal to input. 

%

%
Pull min/max's to top of term
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
output v : tok # term list

v = [`?`,[t]] = t
  | [`qmin`,[t1;...;tn]]   = qmin(t1;...;tn)
  | [`qmax`,[t1;...;tn]]   = qmax(t1;...;tn)

where ti are linear terms with no occurrences of qmin or qmax abstractions.
%

letrec si_simp_min_max exp =
  let (opid,()),bterms = dest_term exp in
  let subterms = bterms_to_terms bterms in
  let resolve_limits b1 b2 =
    if b1 = `qmax` & b2 = `qmax` then `qmax`
    if b1 = `qmin` & b2 = `qmin` then `qmin`
    if b1 = `?` then b2
    if b2 = `?` then b1
    else
      failwith `si_simp_min_max: min-max clash found`
  in
  let invert_limit b =
    if b = `qmax` then `qmin`
    if b = `qmin` then `qmax`
    else `?`
  in
  if opid = `ivar` or opid = `qnum` then 
    `?`,[exp]
  if opid = `qadd` then
  ( let [b1,linexps1;b2,linexps2] = map si_simp_min_max subterms in
    let summed_exps = 
          map
            (mk_simple_term `qadd`)
            (all_permutations [linexps1;linexps2])
    in
      (resolve_limits b1 b2, summed_exps)
  )
  if opid = `qmul` then
  ( let b,linexps = si_simp_min_max (second subterms) in
    let k = first subterms in
    let fixed_linexps =
         map
         (\e. mk_simple_term `qmul` [k;e])
         linexps
    in
     (q_le qzero_term k => b | invert_limit b), fixed_linexps
  )
  if opid = `qmax` or opid = `qmin` then
  ( let [b1,linexps1;b2,linexps2] = map si_simp_min_max subterms in
      (resolve_limits opid (resolve_limits b1 b2)), (linexps1 @ linexps2)
  )
  else
    failwith (`si_simp_min_max: unrecognized opid: ` ^ opid)
;;

%
Input u:term from grammar

Q ::= Q1 + Q2
    | k * Q1
    | Var
    | k

Output v: (ivar # term) list

where
 (mk_iterated_qadd_term o map mk_qaddend) v 

has structure:

Q ::= Q1 + Q2 | R
R ::= k * Var | k
%


     
letrec si_rat_exp_to_sum_of_prods t =
  let opid = opid_of_term t 
  in let ts = subterms_of_term t
  in
  if opid = `qadd` then
     si_rat_exp_to_sum_of_prods (first ts) 
     @ si_rat_exp_to_sum_of_prods (second ts)
  if opid = `qmul` then
    map
      (id # q_times (first ts))
      (si_rat_exp_to_sum_of_prods (second ts)) 
  if opid = `ivar` then
    [dest_ivar t, qone_term]
  if opid = `qnum` then
    [si_unit_ivar, t]
  else 
    failwith (`si_rat_exp_to_sum_of_prods: unrecognized opid: ` ^ opid)
;;

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_SIMP_LINEXP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INPUT u: (ivar # term) list

where
 (mk_iterated_qadd_term o map mk_qaddend) u =

  p1 + ... + pn

  pi = k*v
     | k

k is rational constant.
Vars can occur in more than one pi.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~

OUTPUT v: (ivar # term) list

where
 (mk_iterated_qadd_term o map mk_qaddend) u =

  k0 + k1*v1 + ... + kn * vn

with  k0 is a finite or infinite rational.
      ki are finite non-zero rationals
      vi are distinct (and in order, but this doesn't matter)
      n >= 0
      (if k0 is infinite, then n=0)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let si_simp_linexp lin_exp =

  % group prod terms by variable name %

  let sorted_lin_exp = mergesort (\x y.fst x < fst y) lin_exp in
  let distinct_lin_exp =
        multi_reduce1
          (\x y.
              if fst x = fst y then
                fst x, q_plus (snd x) (snd y)
              else
                 fail
          )
          sorted_lin_exp          
  in


  % strip out terms with zero coefs %

  let nz_lin_exp =
    remove_if (\v,i. q_eq i qzero_term) distinct_lin_exp 
  in

  % normalize rational consts %

  let nz_lin_nf =
    map (id # norm_qnum_term) nz_lin_exp 
  in

  
  if null nz_lin_nf then
    
    % return 0. if empty exp. %
    [si_unit_ivar,qzero_term]
  else
  let (v,q).vqs = nz_lin_nf in

  if not v = si_unit_ivar then

    % add back in 0. const leading term if const leading term missing %

    (si_unit_ivar,qzero_term) . nz_lin_nf

  if is_qinf_term q then

    % infinity + ? = infinity %

    [v,q]

  else
    nz_lin_nf
;;

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_QLINEXP_LT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INPUT is e1 : (ivar # term) list
         e2 : (ivar # term) list

where (reduce1 (\x y.mk_simple_ab_term `qadd` [x;y])
       o map mk_qaddend) ej

is the rational expression

q0 + q1*v1 + ... qn*vn    q0...qn rational constants, n >= 0
                          with qi != 0. for i > 0
                          vi distinct and in ascending order.
                          Assumed that qi are in normal form,
                          (denom's +ve. No common factors to divide out)
                          so structural equality can be used.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
OUTPUT is b : bool


Order relation is used by sorting routine so that exps with equal non-constant
parts are made adjacent.

Use a lexical style order.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let si_qlinexp_lt e1' e2' =
  let v_q_lt (v1,q1) (v2,q2) =
      v1 < v2 
      or (v1 = v2 
          & (q_lt q1 q2))
  in
  letrec aux_lt e1 e2 =
    if null e2 then false
    if null e1 then true
    else
      v_q_lt (hd e1) (hd e2) 
      or
      ((hd e1 = hd e2) & aux_lt (tl e1) (tl e2))
  in
    aux_lt (tl e1') (tl e2')
;;

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_SIMP_LINEXPS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INPUT is lim,es : tok # (ivar # term) list list

   es = [e1;...;em]
   where (reduce1 (\x y.mk_simple_ab_term `qadd` [x;y])
          o map mk_qaddend) ej

   is the rational expression

   q0 + q1*v1 + ... qn*vn    q0...qn rational constants, n >= 0
                             with qi != 0. for i > 0
                             vi distinct and in ascending order.

   lim = `qmin` or `qmax` 
         (or `?`, in which case m = 1 and no simplification possible)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~
OUTPUT is lim,fs : tok # (ivar # term) list list

   fs = [f1;...;fr]

   where (reduce1 (\x y.mk_simple_ab_term `qadd` [x;y])
          o map mk_qaddend) fj

   is the rational expression

   q0 + q1*v1 + ... qn*vn    q0...qn rational constants, n >= 0
                             with qi != 0. for i > 0
                             vi distinct and in ascending order.


r <= m 

simplifications are 

   a. max(e + k1;e + k2) = e + eval(max(k1;k2))
      min(e + k1;e + k2) = e + eval(min(k1;k2))
   b. max(e;-inf) = e
      min(e;+inf) = e

Use a lexical style order.
Assume that if lim = `qmin` only qinf_plus_terms not qinf_minus_terms may
occur. The reverse for the other case.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%

let si_simp_linexps (lim,linexps) =
  if lim = `?` then
    lim,linexps
  else
  let limop = lim = `qmax` => q_max | q_min in
  let sorted_linexps = mergesort si_qlinexp_lt linexps in
  let reduced_linexps =
    multi_reduce1
      (\xs ys.let x.xs' = xs in
              let y.ys' = ys in
              if xs' = ys' then
                % the linexps xs and ys have the same non-constant part %
                (fst x,limop (snd x) (snd  y)). xs'
              else
                fail
      )
      sorted_linexps
  in
  % If there is any constant linexp, it will be the first in reduced_linexps %
  if is_qinf_term (snd (hd (hd reduced_linexps)))
     & not null (tl reduced_linexps)
  then
    lim,(tl reduced_linexps)
  else
    lim,reduced_linexps
;;



%
%


let si_simp t =
  let b,linexps =
   (  (id # map si_mk_linear_qnf_term)
    o si_simp_linexps
    o (id # map si_simp_linexp) 
    o (id # map si_rat_exp_to_sum_of_prods)
    o si_simp_min_max  
   ) t
  in
    reduce1 (\x y.mk_simple_term b [x;y]) linexps
;;

%
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SI_FIND_LBS_UBS_FOR_DISJUNCT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
See beginning of previous section for documentation
%

let si_find_lbs_ubs_for_disjunct disj =
  ( (map (id # si_simp) # map (id # si_simp))
  o si_condense_var_bounds
  o map (id # (id # si_mk_linear_qnf_term))
  o flatten
  o map si_calc_var_bounds
  )
  disj
;;

%[
****************************************************************************
The Sup and Inf functions
****************************************************************************
Variable names as in Shostak's presentation
]%

% ctl = `sup` gives upper. ctl = `inf` gives lower.  %

let si_upper_lower ctl var_bounds var =
  if ctl = `sup` then
  ( apply_alist (snd var_bounds) var ? qinf_plus_term)
  if ctl = `inf` then
  ( apply_alist (fst var_bounds) var ? qinf_minus_term)
  else
    failwith `si_upper_lower`
;;


% 
Input: variable v:ivar and a term 
  qexp = k1*v1 + ... kn * vn + z0

z0 is optional, and if it is present, it is non-zero.

Output: a pair b,c such that qexp = b*v + c
        with v not occurring in c.

%
  
let si_pull_out_var_in_qsum x qexp =
  let ivar_int_prs = si_rat_exp_to_sum_of_prods qexp in
  let with_xs,without_xs = divide_list (\v,i.v = x) ivar_int_prs in
  let b = snd (hd with_xs) ? qzero_term in
  let c = reduce1 (\a b.mk_simple_term `qadd` [a;b]) 
                  (map ivar_qnum_pr_to_term without_xs) 
          ? qzero_term
  in
    b,c
;;


% 
if ctl = `sup` we get supp
if ctl = `inf` we get inff

id names are as for supp.
%

let si_supp_inff ctl x y =
  let qinf_p,q_opid =
       ctl = `sup` => (qinf_plus_term,`qmin`) | (qinf_minus_term, `qmax`)
  in
  letrec aux y =
  
    let opid = opid_of_term y in
    if opid = `qnum` then
      y
    if opid = q_opid then
    ( let [a;b] = subterms_of_term y in
        mk_simple_term q_opid [aux a;aux b]
    )
    else
    let b,c = si_pull_out_var_in_qsum x y in
    if q_eq qone_term b then

    % b = 1 %
    (if not opid_of_term c = `qnum` then
      % c not a number %
      qinf_p
     if q_eq qzero_term c then
      qinf_p
     if q_le qzero_term c then
      % 0 =< c %
       qinf_plus_term
     else
      % c < 0 %
       qinf_minus_term
    )
    if q_le b qone_term then

    % b < 1  => return c/(1-b) %

    ( let bn,bd = dest_qnum b in
        mk_simple_term 
          `qmul` 
          [mk_qnum_term bd (bd - bn);c]
    )
    else
    % b > 1 %
      qinf_p
  in
    aux y
;;

% 
NB: correct operation of si_sup_inf relies on 
j on every call being in qnf normal form. (see qadd case in particular)
%
 
letrec si_sup_inf bnds ctl hvars j =
  let qb_id,ctl_inv = ctl = `sup` => (`qmin`,`inf`) | (`qmax`,`sup`) 
  in
  let opid = opid_of_term j in

  if opid = `qnum` then
    j

  if opid = qb_id then
    mk_simple_term 
      qb_id 
      (map (si_sup_inf bnds ctl hvars)
           (subterms_of_term j)
      )
  if opid = `ivar` then
  (let iv = dest_ivar j in
   if member iv hvars then
     j
   else
     let q = si_upper_lower ctl bnds iv in
     let z = si_sup_inf bnds ctl (iv.hvars) q 
     in
       si_supp_inff ctl iv (si_simp z)
  )
  if opid = `qmul` then
  (let [k;a] = subterms_of_term j in
   if q_le qzero_term k then
     mk_simple_term
       `qmul`
       [k
       ;si_sup_inf bnds ctl hvars a
       ]
   else
     mk_simple_term
       `qmul`
       [k
       ;si_sup_inf bnds ctl_inv hvars a
       ]
  )
  if opid = `qadd` then
    % we rely heavily here on structure of terms in normal form. %
    (let [r_v;B] = subterms_of_term j in 
     let [r;vterm] = dest_simple_term_with_opid `qmul` r_v ? [qone_term;r_v] in
     let v = dest_ivar vterm in
     let B' = si_sup_inf bnds ctl (v.hvars) B in
     if member v (free_ivars B') then
     ( let J' = si_simp (mk_qadd_term (si_ivar_qnum_pr_to_term (v,r)) B') 
       in
         si_sup_inf bnds ctl hvars J'
     )
     else
        mk_qadd_term 
          (si_sup_inf bnds ctl hvars (si_ivar_qnum_pr_to_term (v,r)))
          B'
    )
  else
    failwith (`si_sup_inf: unexpected opid: ` ^ opid)

;;

%[
**********************************************************************
Extracting arithmetic information from Hyps and Concl
**********************************************************************
]%

%
Basic function is:

si_proc_sequent : proof -> term # bool

Input is:
 
S = x1:H1;...;xn:Hn |- C 

Output is:

P,b

P is a proposition containing only arithmetic atomic relations 
``less_than le equal``, and the logical connectives ``and or not``.
All `equal` terms have Int type and are non-reflexive.

If sup-inf finds P unsatisfiable when non-linear arithmetic terms
have been generalized to variables, then S is provable by
linear integer arithmetic. 

b is true iff arithmetic information is extracted from C.
If b is false, there is no need to check well-formedness of C.
%



%
Simplifies arithmetic literals. Fails if not arithmetic literal.

1. ge gt nequal eliminated
2. membership and reflexive equalities over Int subset types replaced by True
3. Int subset types in non reflexive equalities replaced by Int, 
4. Simplifies away double nots and nots of True and False.
%

let si_proc_arith_literal t =

  letrec f t = 
    if is_term `not` t then
    (let t' =  map_on_immediate_subterms f t
     in let subt' = subterm t' 1 
     in if is_term `false` subt' then mk_simple_term `true`  []
        if is_term `true`  subt' then mk_simple_term `false` []
        if is_term `not`   subt' then subterm subt' 1
        else t'
    )
    if is_terms ``nequal gt ge`` t then
      f (unfold_ab t)
    if is_terms ``less_than le`` t then
      t
    if is_term `equal` t then
    ( let [T;a;b] = subterms t in
      if not is_arith_type T then
        failwith `proc_arith_literal: non-arith equality`
      if a = b then
        mk_simple_ab_term `true` []
      else 
        mk_simple_prim_term `equal` [int_term;a;b]
    )
    if is_term `member` t then
    ( let [T;a] = subterms t in
      if not is_arith_type T then
        failwith `proc_arith_literal: non-arith member term`
      else 
        mk_simple_ab_term `true` []
    )
    else
      failwith `proc_arith_literal: not arith literal`
  in
    f t
;;

let si_is_arith_literal t = 
  can si_proc_arith_literal t
;;

% Input: any proposition. 
  Output: prop built using `and` and `or` from
          arithmetic literals (``le lt equal`` under 0 or more nots)

%

let si_mk_simp_it_and as = 
  if exists (is_term `false`) as then 
    false_term
  else
    mk_iterated_and (remove_if (is_term `true`) as)
    ? true_term
;;

let si_mk_simp_it_or as = 
  if exists (is_term `true`) as then 
    true_term
  else
    mk_iterated_or (remove_if (is_term `false`) as)
    ? false_term
;;

let si_mk_simp_and a b =
  if is_term `false` a or is_term `false` b then false_term
  if is_term `true` a then b
  if is_term `true` b then a
  else mk_simple_ab_term `and` [a;b]
;;

let si_mk_simp_or a b = 
  if is_term `true` a or is_term `true` b then true_term
  if is_term `false` a then b
  if is_term `false` b then a
  else mk_simple_ab_term `or` [a;b]
;;

let si_mk_simp_not a =
  if is_term `true` a then false_term
  if is_term `false` a then true_term
  else
    mk_not_term a
;;

let si_proc_clause is_concl Q = 

  letrec aux t = 
    if is_term `and` t then
    ( let [a;b] = map aux (subterms t) in
        si_mk_simp_and a b
    )
    if is_term `or` t then
    ( let [a;b] = map aux (subterms t) in
        si_mk_simp_or a b
    )
    if si_is_arith_literal t then
      si_proc_arith_literal t 

    if is_concl then 
      false_term
    else
      true_term
  in
    aux Q 
;;



letref si_int_subset_preds =
  let mk_le_term i j = mk_simple_term `le` [i;j] in
  let mk_tm = mk_simple_term in
  let [iv;jv;kv] = map mkv ``i j k`` in
  let [i;j;k] = map mvt [iv;jv;kv] in
  let zero = mk_integer_term 0 in
  let one = mk_integer_term 1 in
  map (id # (id # bterm_to_so_lambda))

 [ `nat`,  mk_tm `nat` [],  [iv], mk_le_term zero i
 ; `nat_plus`, mk_tm `nat_plus` [], [iv], mk_le_term one i
 ; `int_nzero`, mk_tm `int_nzero` [], [jv], mk_tm `not` [mk_tm `equal` 
                                                            [int_term;j;zero]]
 ; `int_upper`, mk_tm `int_upper` [i], [jv], mk_le_term i j
 ; `int_lower`, mk_tm `int_lower` [i], [jv], mk_le_term j i
 ; `int_seg`,  mk_tm `int_seg` [i;j], [kv],  mk_and_term (mk_le_term i k) 
                                                      (mk_less_than_term k j)
 ; `int_iseg`,  mk_tm `int_iseg` [i;j], [kv],  mk_and_term (mk_le_term i k) 
                                                      (mk_le_term k j)
 ]
;;
%
 [ `nat`,  'nat()',      'so_lambda(i.le(0;i))'
 ; `nat_plus`,   'nat_plus()', 'so_lambda(i.le(1;i))'
 ; `int_upper`,  'int_upper(i)', 'so_lambda(j.le(i;j))'
 ; `int_lower`,  'int_lower(i)', 'so_lambda(j.le(j;i))'
 ; `int_seg`,  'int_seg(i;j)', 'so_lambda(k.and(le(i;k);less_than(k;j)))'
 ; `int_iseg`, 'int_iseg(i;j)', 'so_lambda(k.and(le(i;k);le(k;j)))'
 ]
%

% Succeeds for types which are subsets of integers. %

let si_int_subset_typing_to_arith_prop tm type =
  let type_id = opid_of_term type in
  let type_pat, pred_pat = 
      ( apply_alist si_int_subset_preds type_id
        ? failwith `si_int_arith_decl_to_prop: type not recognized`
      )
  in
  let sub = fo_match (free_vars type_pat) type_pat type in
  let pred = fo_subst sub pred_pat in
  let [u],pred_prop = so_lambda_to_bterm pred in
    fo_subst [u,tm] pred_prop
;;




%
Gather propositions from hyps of form x:T, a in T, a = b in T, 
where T is a proper subset of Int.

(var # term) -> term
%

let si_proc_int_subset_typing (v,t) = 
    if is_terms ``member equal`` t then
       si_int_subset_typing_to_arith_prop 
         (subterm_of_term t 2) (subterm_of_term t 1)
    if is_null_var v then
       failwith `si_proc_int_subset_typing`
    else
       si_int_subset_typing_to_arith_prop (mk_var_term v) t
;;

let si_proc_hyp (v,t) = 
  let HT = si_proc_int_subset_typing (v,t) ? true_term
  in let HP = si_proc_clause false t
  in
    si_mk_simp_and HP HT
;;

let si_proc_concl C = si_proc_clause true C ;;


%[
****************************************************************************
WF INFORMATION EXTRACTION FOR CONCL
****************************************************************************
]%

% If t is an arith literal (including arith membership goals)
  then return [[T;e]] or [[T,e1];[T,e2]] where T is the arith type, and
  the e are the arguments to the atomic predicates.

  However, if arith literal is e1 = e2 in T, return
  [[T,e1];[Int;e2]] instead.

  Otherwise return [[t]]
%

let si_proc_literal_for_wf t =
  letrec aux t = 
    let ts = subterms t in
    if is_term `not` t then
      aux (first ts)
    if is_terms ``nequal equal`` t & is_arith_type (first ts) then
      [[first ts;second ts];[first ts;third ts]]
    if is_term `member` t & is_arith_type (first ts) then
      [ts]
    if is_terms ``ge gt le less_than`` t then
      [[int_term;first ts];[int_term;second ts]]
    else 
      fail
  in
  let ts = subterms t in
  if is_term `equal` t & is_arith_type (first ts) then
      [[first ts;second ts];[int_term;third ts]]
  else
    aux t ? [[t]]
;;


letrec si_extract_concl_exps_for_wf t = 
  
    if is_terms ``and or`` t then
      flatten (map si_extract_concl_exps_for_wf (subterms t))
    else 
      si_proc_literal_for_wf t 
;;

% 
si_proc_concl_for_wf
  p : proof
  =>
  Qs,WFs : term list # term list

Qs are arithmetic predicates and WFs are membership goals that have to
be verified for concl of p to be well formed. 

Could add condition so that literal a = b in T where T is subset of int
only generates 1 rather than 2 arith predicates to check.
%


let si_proc_concl_for_wf p = 
  let mk_wf_goals e  = 
    let n = length e in
    if n = 1 then [mk_member_term (get_universe p (first e)) (first e)]
    if n = 2 then [mk_member_term int_term (second e)]
    else
      [mk_member_term int_term (second e)
      ;mk_member_term int_term (third e)]
  in 

  let mk_wf_preds e = 
    let n = length e in
    if n = 1 then fail
    % NB if n = 2 or 3 and type is Int then si_int_subset... fails as desired%
    if n = 2 then
      [si_int_subset_typing_to_arith_prop (second e) (first e)]
    else
      [si_int_subset_typing_to_arith_prop (second e) (first e)
      ;si_int_subset_typing_to_arith_prop (third e) (first e)
      ]
  in
  let exp_set = si_extract_concl_exps_for_wf (concl p)
  in
    flatten (mapfilter mk_wf_preds exp_set)
    ,flatten (mapfilter mk_wf_goals exp_set)
;;



% 
Input:

    
   H1,...Hm |- C

  1. Extracts 4 kinds of propositions:

     HPs     list of props from hyps with arith literals
     HTs     list of props from typing info in hyps
             (x:T, a in T, a = b in T where T is arith_type)
     CP      prop with arith literals of concl
     CTs   list of props from typing of concl.
     
     HPs and CP ignore int subset typing.

  2. Returns pair t,wfs

      where 
 
         t = not
             ( (AND HPs
                and AND HTs
               )
               implies
               (CP and AND CTs)
             )

           = AND HPs
             and AND HTs
             and not (CP and AND CTs)

     and

        wfs are wf membership goals that need to be checked.

if CP and AND CTs is True term, then sequent should be obviously true
by MemCD, so si_proc_sequent fails. (Could search for hyp contradiction?
Seems unnecessary.)

if CP and AND CTs is False term, then the only hope is to prove
a hyp contradiction. In this case wfs is set to []. 

t ever False? Yes, only if some HP is False. In this event wfs is set to []
as well. The t returned is 0 < 0

t ever True? No, because in this situation si_proc_sequent fails.

%

let si_arith_false_term = mk_less_than_term si_zero si_zero ;;
let si_arith_true_term = mk_less_than_term si_zero si_one ;;

let si_arithize_const_prop p = 
  if is_term `false` p then si_arith_false_term
  if is_term `true` p then si_arith_true_term
  else p
;;


let si_proc_sequent p =

  let hyp_conj = si_mk_simp_it_and (map si_proc_hyp (dest_hyps p))
  in 
  if is_term `false` hyp_conj then
    si_arith_false_term,[]
  else
  let concl_clause = si_proc_concl (concl p)
  in let concl_wf_clauses,wf_goals = si_proc_concl_for_wf p
  in let concl_conj = 
    si_mk_simp_it_and (concl_clause. concl_wf_clauses)
  in
  if is_term `true` concl_conj then
    failwith `si_proc_sequent: concl trivial, SupInf will not make progress`
  else
  let sat_problem = si_mk_simp_and hyp_conj (si_mk_simp_not concl_conj)
  in
  if is_term `true` sat_problem then
    failwith `si_proc_sequent: no arith props found`
  if is_term `false` concl_conj then
    sat_problem, []
  else
    sat_problem, map (\g.`wf`,g) wf_goals
;;



%[
****************************************************************************
Extraction of info from arithmetic lemmas
****************************************************************************
Function defined here is:

si_get_arith_info_from_lemmas
  p : proof
  t : term
  => nPs : (tok # term) list

t is some integer valued non-simple arithmetic expression

nPs is a list of pairs of form

  name,Prop

where 

name: name of lemma
Prop: proposition derived form lemma.

Prop should be provable by the tactic

   BackThruGenLemma name (-1)

Each lemma is expected to have form:

All xs:Ts A1 => ... => An => C

where C is constructed from and's or's and arith literals.
`match handle's hs are selected from C. A match handle is:
a non-simple arithmetic expression that is argument
to an atomic arithmetic relation in C and whose free vars
include all the free vars in C and the Ai.

A Lemma is used if the match to some handle of some lemma
succeeds and if the instantiated Ai are equal to hypotheses
in the sequent.

Lemmas are added to the arith database using the function:

add_arith_lemma 
  lemma_name : tok
   =>
  ():unit
;;


]%

% 
Each entry in arith_property_inf_alist is of form:

 opid,fs

where each f is of form

 inf_fun,lemma_name

where

 inf_fun: is for inferring arith property of term with opid
          inf_fun takes as its first arg a list of propositional hyps
          to use for checking against lemma antecedents

 lemma_name: is the name of the lemma used to get the property.

 lemma_name is the name of the lemma to backchain through to prove
 the property returned by inf_fun
%

letref arith_property_inf_alist = [] 
  : (tok # (tok # (term list->term->term)) list) list
;;

let si_is_simp_arith_exp t = 
  is_terms ``add multiply subtract minus natural_number variable`` t
;;

% 
Input:
     conj or disj of zero or more arith literals
Output:
     list of integer valued arguments of atomic arith relation.
%

let si_dest_arith_literal t =

  letrec f t = 
    if is_term `not` t then
      f (hd (subterms t))
    if is_terms ``gt ge less_than le`` t then
      subterms t
    if is_terms ``equal nequal`` t then
      tl (subterms t)
    else
      failwith `dest_arith_literal: not arith literal`
  in
    f t
;;

let si_extract_arith_handles t = 
  letrec get_lit_args t =
    if is_terms ``and or`` t then
      flatten (map get_lit_args (subterms t))
    if si_is_arith_literal t then
      si_dest_arith_literal t
    else
      []
  in
    mk_set (remove_if si_is_simp_arith_exp (get_lit_args t))
;;

let add_arith_lemma name = 
  
  if not (status_of_object name) = `COMPLETE` then
    failwith `add_arith_lemma: lemma incomplete`
  else
  let xTs,As,C = dest_simple_lemma name in  
  let xs = map fst xTs in
  let handles = si_extract_arith_handles C in
  let needed_vars = free_vars (mk_iterated_and (C.As)) in
  let good_handles = 
    filter (\h.subset needed_vars (free_vars h)) handles in

  let matcher h Hs t =
    let sub = match xs h t
    in let InstAs = map (subst sub) As
    in 
    if subset InstAs Hs then
      subst sub C
    else
      failwith `arith_lemma_matcher: lemma ants not satisfied`
  in
  let add_alist_entry h = 
    arith_property_inf_alist := 
      update_2d_alist 
         arith_property_inf_alist
         (opid_of_term h) 
         name
         (matcher h)
  in
    map add_alist_entry good_handles
    ; ()
;;

let si_get_hyp_props p = 
  mapfilter (\v,T,h.if is_invisible_var v & not h then T else fail) 
            (dest_full_hyps p)
;;


let si_get_arith_info_from_lemmas p = 
  let Hs = si_get_hyp_props p 
  in 
  \t.
  let opid = opid_of_term t 
  in let nfs = apply_alist arith_property_inf_alist opid
               ? failwith `si_get_arith_info_from_lemma: no info found`

  in let nPs =  mapfilter (id # \f.f Hs t) nfs
  in 
  if null nPs then
    failwith `si_get_arith_info_from_lemma: info found does not apply`
  else
    nPs
;;


let si_get_arith_info p = 
  let f = si_get_arith_info_from_lemmas p
  in let g t = `wf`,mk_member_term (get_type p t) t
  in 
  \t.
    let just_newhyp_prs = (f t ? []) @ ([g t] ? [])
    in let new_hyp_prop = 
      si_mk_simp_it_and
         (map (\j,h.si_proc_hyp (null_var,h)) just_newhyp_prs)
    in 
    if is_term `true` new_hyp_prop then
      failwith `si_get_arith_info: no info found`
    else
      %
         seems unlikely that si_proc_hyp would return `false`, but just
        in case we use the arithize function
      %
      si_elim_nots_and_norm_rels (si_arithize_const_prop new_hyp_prop)
      ,just_newhyp_prs
;;
 


%[
****************************************************************************
EXTRACTION OF ILP PROBLEMS FROM SEQUENT
****************************************************************************
Here we chain together the functions:

   si_proc_sequent
   si_elim_nots_and_norm_rels 
   si_generalize
   si_put_in_dnf 
   si_elim_equalities
   si_lin_norm_atm_to_ivar_int_prs 


si_extract_ilp_queries 
  ctl : tok list
  p   : proof
   =>
    ilps : (var # int) list list list
    ,gen_vts :(var # term) list
    ,just_goal_prs : (tok # term) list
    ,eq_subs : (var # term) list list
    ,concl_ignored : bool


Inputs:

ctl: specify options
  p: goal SupInf is run on.

Outputs:

ilps : 
 list of Integer Linear Programming Problems. If none satisfiable,
 then p is true by SupInf.

gen_vts :
  New variables and the corresponding non-linear terms that they
  generalize.
  
just_goal_prs :
  Each goal is something that is required to be proved.
 
  list of tok#term pairs: hyps inferred that were not in p to start and
           need to be verified. 
  If just is `wf` then that goal is a wf goal and should be provable by auto.
  o/w  just is a lemma name, and the goal should be provable by backchaining
  through the lemma and using Hypothesis on antecedents.

eq_subs :
  Each eq_sub corresponds to one of the ilps.
  The variables are those eliminated by using equalities, and 
  the term expressions are the variable values in terms of the remaining 
  variables.

concl_ignored :
  If this is true, then the extract calculation is not needed in the event
  all ilps are non-satisfiable.

Here are types and descriptions of variables at the intermediate stages:

p0 : term 
       Contains {and or not} connectives
               {le lt eq} atomic rels

p1 : term  
       Contains {and or} connectives, {le eq} atomic rels.
       Atomic rels of form 0 r e with e in arith_simplify_term normal form.
       p1 <=> p0

p2 : term
       Format as p1, except all non-linear exps generalized to vars.
        (sub vts p2 = p1)
        Therefore, if p2 unsatisfiable then p1 unsatisfiable.

p3 : term list list
       (mk_it_or o map mk_it_and) p3 <=> p2

p4 : term list list
       (mk_it_or o map mk_it_and o map_2d (mk_le 0)) p4 <=> p3

p5 : (var # int) list list list
       (mk_it_or 
        o map mk_it_and 
        o map_2d (mk_le 0)
        o map_2d var_int_prs_to_term
       ) p5 <=> p4

p6 : (var # int) list list list
       (mk_it_or 
        o map mk_it_and 
        o map_2d (mk_le 0)
        o map_2d var_int_prs_to_term
       ) p6 <=> p5
       Every le prop now contains at least one free var.

]%




let si_extract_ilp_queries ctl p = 

  let inf_fun = 
      if member `si_x` ctl then
        si_get_arith_info p
      else
        (\x.fail)

  in let p0,jts = si_proc_sequent p 
  in let p1 = si_elim_nots_and_norm_rels p0  
  in let p2,vts,jts' = si_generalize p1 inf_fun
  in let p3 = si_put_in_dnf p2  
  in let p4,subs = unzip (map si_elim_equalities p3)
  in let p5 = map_2d si_lin_norm_atm_to_var_int_prs p4  
  in let p6 = si_thin_const_le_rels p5 
  in
    p6
    ,vts
    ,(jts @ jts')
    ,subs
    ,null jts
;;
   

%[
****************************************************************************
Mapping to / from Ivar Terms.
****************************************************************************
]%

let si_vars_of_ilps ilps = 
  let vars_of_exp vis = map fst (tl vis)
  in let vs = (flatten o map flatten o map_2d vars_of_exp) ilps
  in 
    mk_set vs
;;

let si_mk_var_ivar_alist vars =
  (null_var,si_unit_ivar) 
  . zip vars (upto (si_unit_ivar + 1) (si_unit_ivar + length vars))
;;

let si_convert_vars_to_ivars_in_ilps ilps = 
  let vs = si_vars_of_ilps ilps
  in let v_iv_prs = si_mk_var_ivar_alist vs
  in 
    map_3d (apply_alist v_iv_prs # id) ilps
    , tl v_iv_prs
;;

% Do:
1. eliminate ivars in favour of original vars.
2. calculate values for vars elimmed using equalities.
3. restore tms that were generalized to vars
%

  
let si_pretty_up_ivar_qnum_bindings 
  iv_qtm_prs 
  gen_v_tm_prs
  eq_elim_v_tm_prs
  v_iv_prs
  = 
  let v_qtm_prs = map (rev_apply_alist v_iv_prs # id) iv_qtm_prs
  in let elimmed_v_qtm_prs = 
       map 
        (id # (si_simp o subst v_qtm_prs o si_int_to_rat_exp)) 
        eq_elim_v_tm_prs
  in let restore_tm v = 
       apply_alist gen_v_tm_prs v ? mk_var_term v
  in
    map (restore_tm # id) (v_qtm_prs @ elimmed_v_qtm_prs)
;;

  
%[
****************************************************************************
MANAGING CALLS TO SUP AND INF ROUTINES
****************************************************************************
]%


let si_check_range (lo',hi') =
  let lo,hi = si_simp lo',si_simp hi' in
  if q_lt hi lo then 
    inl()
  if lo = qinf_plus_term or hi = qinf_minus_term then
    inl()
%    failwith `si_check_range: bad range` %
  if lo = qinf_minus_term then
    inr (int_to_qnum (qnum_to_int_below hi) ? qzero_term)
  if hi = qinf_plus_term then
    inr (int_to_qnum (qnum_to_int_above lo))
  else
  let ilo = qnum_to_int_above lo in
  let ihi = qnum_to_int_below hi in
  if ihi < ilo then
    % no integer in range %
    inr lo
  else
    inr (int_to_qnum ((ilo + ihi)/2))
;;

%
Input: Set of lower and upper bounds. Set should not be empty.

Output: 
  If unsatisfiable: inl().
  If satisfiable:   inr([iv1,q1;...ivn,qn]) 

                     a satisfying assignment to all the variables occurring 
                     in the input.
%

let si_find_if_satisfiable bnds =
  let lower_bnds,upper_bnds = bnds in
  let vars = 
    union (map fst lower_bnds) (map fst upper_bnds) 
  in
  let find_bnds_for_var bnds' v =
      si_sup_inf bnds' `inf` [] (mk_ivar_term v) 
      ,si_sup_inf bnds' `sup` [] (mk_ivar_term v) 
  in
  letrec find_qnum_bnds_for_vars vs n bnds' qnum_bindings =
    if null vs then 
      inr qnum_bindings
    else
    let v.vs' = vs in
    let x = si_check_range (find_bnds_for_var bnds' v) in
    if isl x or (n = 1 & not is_int_valued_qnum (outr x)) then inl () 
    else
    let new_bnds =
      update_alist (fst bnds') v (outr x)
      ,update_alist (snd bnds') v (outr x)
    in
      find_qnum_bnds_for_vars vs' (n+1) new_bnds ((v,outr x). qnum_bindings)
  in
    find_qnum_bnds_for_vars vars 1 bnds [] 
;;

% check each bound set in turn. 
  If none satisfiable, return 0,[]
  If the n'th is satisfiable, return  n,[iv1,q1;...ivn,qn] 
     a satisfying assignment to all the variables occurring in that set.
%

let si_find_satisfiable_bnd_set bnd_sets =

  letrec aux n bsets = 
    if null bsets then 0,[]
    else
    let x = si_find_if_satisfiable (hd bsets) in
    if isr x then
      n,outr x 
    else
      aux (n+1) (tl bsets)
  in
    aux 1 bnd_sets
;;

%[
****************************************************************************
EXTRACTION CALCULATION
****************************************************************************
NB: This uses abstractions from the standard bool_1 theory. 
]%

letrec si_mk_atomic_dec_tm t = 
  if is_not_term t then
    mk_simple_term `bnot` [si_mk_atomic_dec_tm (dest_not t)]
  if is_terms ``nequal gt le ge`` t then
    si_mk_atomic_dec_tm (unfold_ab t)
  if is_term `member` t 
     or (is_term `equal` t 
         & (subterm_of_term t 2 = subterm_of_term t 3) 
        ) then
    mk_simple_ab_term `btrue` []
  if is_term `equal` t then
    mk_simple_term `eq_int` (tl (subterms_of_term t))
  if is_term `less_than` t then
    mk_simple_term `lt_int` (subterms_of_term t)
  else
    failwith `si_mk_atomic_dec_tm`
;;

letrec si_mk_decision_tm t = 
  if is_term `or` t then
    mk_simple_term
      `bor`
      (map si_mk_decision_tm (subterms t))
  if is_term `and` t then
    mk_simple_ab_term
      `band`
      (map si_mk_decision_tm (subterms t))
  if si_is_arith_literal t then
    si_mk_atomic_dec_tm t 
  else 
    mk_simple_ab_term `bfalse` []
;;
  

letrec si_mk_extract c = 
  if is_term `or` c then
  (let [a;b] = subterms_of_term c
   in
     mk_simple_term
       `ifthenelse`
       [si_mk_decision_tm a ;si_mk_extract a ;si_mk_extract b]
  )
  if is_term `and` c then
  (let [a;b] = subterms_of_term c
   in
     mk_pair_term (si_mk_extract a) (si_mk_extract b)
  )
  if not si_is_arith_literal c then
    mk_any_term axiom_term
  if is_terms ``equal less_than gt member`` c then
    axiom_term
  if is_terms ``nequal le ge not`` c then
    mk_lambda_term (mkv `x`) (mk_any_term axiom_term)
  else
    failwith `si_mk_extract`
;;

%[
****************************************************************************
THE SUPINF FRONTEND
****************************************************************************

si_check_goal
  ctl : tok list
  p : proof
  =>
  inl (tok # term) list
  +
  (term # term) list

ctl specifies what methods should be used in the preprocessing.

If result is inl([j1,C1 ;...;jn,Cn],b) 
  then p is true providing goals C1 ... Cn are proved.
  The ji give hints on how to prove goals.
  b is true if concl was ignored.
  (don't need to compute extract of goal in this case)
If result is inr([t1,q1;...;tn,qn]) 
  then t1,q1...tn,qn seems to be a counter-example based on 
   available information.
]%

let si_check_goal ctl p =

  let ilps,vts,jts,subs,c_ignored = si_extract_ilp_queries ctl p 

  in let ivar_ilps,v_iv_alist = si_convert_vars_to_ivars_in_ilps ilps 
  in let bnd_sets = map si_find_lbs_ubs_for_disjunct ivar_ilps 
  in let n,ctr_example = si_find_satisfiable_bnd_set bnd_sets 
  in if n = 0 then % default: no counter example found%
    inl (jts,c_ignored)
  else
    inr (si_pretty_up_ivar_qnum_bindings 
           ctr_example
           vts
           (nth n subs)
           v_iv_alist
        )
;;


%[
****************************************************************************
THE SUPINF TACTIC
****************************************************************************
]%

% 
This does the tail end work of the supinf tactic in the event it 
succeeds.
%

let SupInfTac (j_Qs,c_ignored) p = 

  let ProveGoal j p = 
    if j = `wf` then
       AddHiddenLabel `wf` p
    else
    ( (BackThruGenLemma j (-1) THENM Hypothesis)
      ORELSE 
      AddLabel (`SupInf: lemma failure: ` ^ j)
    ) 
    p
  in    
  letrec ProcGoals j_Q_prs p = 
  ( if null j_Q_prs then 
      Fiat  
    else
      Assert (snd (hd j_Q_prs))
      THEN IfLab `assertion`
              (ProveGoal (fst (hd j_Q_prs)))
              (Thin (-1) THEN ProcGoals (tl j_Q_prs))
  ) p
  in

  % Avoid cluttering up extract with extract from Assert's %
  if c_ignored then
  ( UseWitness axiom_term
    THEN ProcGoals j_Qs
  ) p
  else
  ( UseWitness (si_mk_extract (concl p))
    THEN ProcGoals j_Qs
  ) p
;;



letref supinf_info = [] : (term # term) list
;;



let SupInfAux1 ctl p =
  let subgoals_or_ctr_example = si_check_goal ctl p
  in if isl subgoals_or_ctr_example then
    SupInfTac (outl subgoals_or_ctr_example) p
  else
  (supinf_info := outr subgoals_or_ctr_example
   ;
   if all (is_int_valued_qnum o snd) supinf_info then
     failwith `SupInf: integer valued counterexample found. See supinf_info`
   else
     failwith `SupInf: rational valued counterexample found. See supinf_info`
  )
;;
  


%[
****************************************************************************
Test functions
****************************************************************************
These need fixing.
]%

%
1. Reduce negation of input problem to linear prog problems.
2. For each calculate sup and inf of each variable. and return...
%

%
Need to convert ivars back to vars in nuprl terms for decent display...
%

%*************

let si_test_norm (p,tm_iv_alist) =
  let v_tm_iv_trips = 
    map (\n,t,iv.tok_to_var (`$` ^ int_to_tok n),  t,  iv) 
        (number tm_iv_alist)
  in let full_v_iv_alist = 
        map (\v,t,iv.v,iv) v_tm_iv_trips @ lin_v_iv_alist
  in let sub = map (\v,t,iv.v,t) v_tm_iv_trips
  in
    map_2d (si_ivar_int_prs_to_lin_norm_atm full_v_iv_alist) p
;;


let si_proc_input prop =
  (map si_find_lbs_ubs_for_disjunct
   o si_normalize_prop 
   o mk_not_term
  ) prop
;;

let si_pretty_up_bndset tm_ivar_alist bndset =
     (map (rev_apply_alist tm_ivar_alist 
           # si_elim_ivars_in_term tm_ivar_alist)
      # map (rev_apply_alist tm_ivar_alist 
             # si_elim_ivars_in_term tm_ivar_alist)
     ) bndset
;;

let si_find_infs_and_sups prop =

  let bndsets = si_proc_input prop
  in
    map
    (\bnds.
       map
       (\v,iv.
          v
          ,si_sup_inf bnds `inf` [] (mk_ivar_term iv)
          ,si_sup_inf bnds `sup` [] (mk_ivar_term iv)
       )
       si_var_alist
    )
   bndsets

;;

*************%


%[
****************************************************************************
Adding Arithmetic Properties
****************************************************************************
Uses front and tail end of SupInf routine.
]%

let PrepArith p = 

  let ProveGoal j p = 
    if j = `wf` then
      AddHiddenLabel `wf` p
    else
    ( (BackThruGenLemma j (-1) THENM Hypothesis)
      ORELSE 
      AddLabel (`PrepArith: lemma failure: ` ^ j)
    ) 
    p
  in    
  letrec ProcGoals j_Q_prs p = 
  ( if null j_Q_prs then 
      Id
    else
      Assert (snd (hd j_Q_prs))
      THEN IfLab `assertion`
              (ProveGoal (fst (hd j_Q_prs)))
              (ProcGoals (tl j_Q_prs))
  ) p

  in let p0,jts = si_proc_sequent p 
  in let p1 = si_elim_nots_and_norm_rels p0  
  in let (),(),jts = si_generalize p1 (si_get_arith_info p)
  in
    ProcGoals jts p
;;
