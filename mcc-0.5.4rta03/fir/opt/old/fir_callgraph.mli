(* Fir callgraph code
 * Geoffrey Irving
 * $Id: fir_callgraph.mli,v 1.2 2002/04/18 03:48:23 jyh Exp $ *)

(* Notes:
 *   1. The callgraph has two special nodes: entry and exit.  entry has no predecessors,
 *      and exit has no successors.  The successors of entry are any functions which can
 *      be called externally (least main and init).  The predecessors of exit are any
 *      functions which make indirect tailcalls.
 *   2. To process an indirect jump, alias analysis information is used to determine the
 *      setup of possible functions, and edges are created to each.
 *)

open Trace
open Symbol
open Fir
open Fir_alias

type t
type node

(* construct a callgraph *)
val make : alias_info -> prog -> t

(* callgraph modification *)
(* This function, called from inlining, is rather a hack.  Do not call it
   unless you know what you are doing.  I apologize for the nastiness. *)
val update_inlined_fun : t -> alias_info -> fundef SymbolTable.t -> node -> exp -> t

(* callgraph information *)
val entry_node : t -> node
val exit_node : t -> node
val node_list : t -> node list

(* conversion between function names and nodes *)
val node_of_fun : t -> var -> node
val fun_of_node : node -> var

(* map pointers to list of function values (invalid for actual function names) *)
val possible_functions : t -> var -> var list

(* node information *)
val in_degree : node -> int
val out_degree : node -> int
val query : node -> node -> bool
val pred : node -> node list
val succ : node -> node list
val query_entry : t -> node -> bool
val query_exit : t -> node -> bool
val succ_entry : t -> node list

(* structural information *)
val is_recursive : t -> node -> bool

val postorder_list : t -> node list
val rev_postorder_list : t -> node list

(* debugging *)
val debug_callgraph : string -> t -> unit

(* Get the loop-nest tree *)
val loop_nest : t -> node trace
