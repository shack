(* Fir inlining, constant propogation, and common subexpression elimination
 * Geoffrey Irving
 * $Id: fir_inline.mli,v 1.2 2002/04/18 03:48:23 jyh Exp $ *)

open Fir
open Fir_alias

val inline_prog : Fir_callgraph.t -> alias_info -> prog -> prog
