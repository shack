(* Fir interprocedural constant propogation
 * Geoffrey Irving
 * $Id: fir_cprop.ml,v 1.2 2002/06/24 00:00:53 jyh Exp $ *)

(* Notes:
 *   1. This code does a full program forward dataflow
 *      analysis.
 *   2. The full lattice gives, for each function, all
 *      information known about each of its argument's
 *      entry to the function.
 *   3. After the dataflow analysis is complete, we
 *      process each function once, using the information
 *      gained to optimize the code.
 *   4. The code uses the following invariant: any variable
 *      not in the slat is global (either in globals or funs).
 *   5. This code assumes that no function takes no arguments.
 *)

open Symbol
open Fir
open Fir_set
open Fir_exn
open Fir_type
open Fir_opt_util
open Fir_algebra

(************************************** single variable lattices *)

type cp =
    CpBot
  | CpAtom of atom

let atom_eq a1 a2 =
  match a1, a2 with
      AtomInt i, AtomInt j
    | AtomEnum (_, i), AtomEnum (_, j)
    | AtomConst (_, _, i), AtomConst (_, _, j) -> i == j
    | AtomRawInt i, AtomRawInt j -> Rawint.compare i j == 0
    | AtomFloat x, AtomFloat y -> Rawfloat.compare x y == 0
    | AtomVar u, AtomVar v -> Symbol.eq u v
    | _ -> false

(* meet operation (returns second argument exactly if possible) *)
let meet c1 c2 =
  match c1, c2 with
      CpBot, _ | _, CpBot -> CpBot
    | CpAtom a1, CpAtom a2 ->
        if atom_eq a1 a2 then
          c2
        else
          CpBot

(************************************** full lattices and other environments *)

type slat = cp SymbolTable.t      (* variables within a single function *)
type flat = slat SymbolTable.t    (* one entry for each function *)

(************************************** debugging *)

let print_flat flat =
  Format.printf "@\n***** Constant propogation function lattice *****\n";
  let print_cp = function
      CpBot -> Format.print_string "__"
    | CpAtom a -> Fir_print.print_atom a in
  SymbolTable.iter (fun f slat ->
      print_symbol f;
      Format.print_string ": ";
      SymbolTable.iter (fun v c ->
          Format.print_char '(';
          print_symbol v;
          Format.print_string " = ";
          print_cp c;
          Format.print_string ") ")
        slat;
      Format.print_newline ())
    flat;
  Format.print_newline ()

(************************************** miscellanous small functions *)

let cp_of_var slat v =
  try
    SymbolTable.find slat v
  with
    Not_found -> CpAtom (AtomVar v)

let cp_of_atom slat a =
  match a with
      AtomVar v ->
        (try
          SymbolTable.find slat v
        with
          Not_found -> CpAtom a)
    | _ -> CpAtom a

let etree_of_atom slat = function
    AtomVar v ->
      (try
        match SymbolTable.find slat v with
            CpBot -> ETVar v
          | CpAtom a -> ETConst a
      with Not_found ->
        ETVar v)
  | a -> ETConst a

let cp_of_etree = function
    ETConst a -> CpAtom a
  | _ -> CpBot

(************************************** expression transfer function *)

let rec cprop_expr cg wl ft flat slat e =
  let cpe = cprop_expr cg wl ft in
  match e with
      LetUnop (v, _, op, a, e) ->
        let t = ETUnop (op, etree_of_atom slat a) in
        let cp = cp_of_etree (canonicalize_etree t) in
        let slat = SymbolTable.add slat v cp in
          cpe flat slat e
    | LetBinop (v, _, op, a1, a2, e) ->
        let t = ETBinop (op, etree_of_atom slat a1, etree_of_atom slat a2) in
        let cp = cp_of_etree (canonicalize_etree t) in
        let slat = SymbolTable.add slat v cp in
          cpe flat slat e
    | LetExt (v, _, _, _, _, e)
    | LetAlloc (v, _, e)
    | LetSubscript (_, v, _, _, _, e) ->
        let slat = SymbolTable.add slat v CpBot in
          cpe flat slat e
    | SetSubscript (_, _, _, _, _, e)
    | SetGlobal (_, _, _, _, e)
    | Memcpy (_, _, _, _, _, _, e)
    | Debug (_, e) ->
        cpe flat slat e
    | TailCall (f, al) ->
        (* Format.printf "--------- tailcall: "; print_symbol f; Format.print_string " -- "; *)
        let fl = match cp_of_var slat f with
            CpAtom (AtomVar f) -> [f]
          | _ -> Fir_callgraph.possible_functions cg f in
        (* List.iter print_symbol fl; Format.print_newline (); *)
        let cpl = List.map (cp_of_atom slat) al in
          List.fold_left (fun flat f ->
              let _, _, vl, _ = SymbolTable.find ft f in
              let sl =
                try
                  List.fold_left2 (fun sl v cp ->
                      let cp2 = SymbolTable.find sl v in
                      let cp = meet cp cp2 in
                        if cp != cp2 then (
                          Worklist.add wl f;
                          SymbolTable.add sl v cp)
                        else
                          sl)
                    (SymbolTable.find flat f) vl cpl
                with Not_found ->
                  Worklist.add wl f;
                  List.fold_left2 SymbolTable.add SymbolTable.empty vl cpl
              in
                SymbolTable.add flat f sl)
            flat fl
    | SpecialCall (TailSysMigrate (_, _, _, f, al)) ->
        (* Format.printf "--------- tailcall: "; print_symbol f; Format.print_string " -- "; *)
        let fl = match cp_of_var slat f with
            CpAtom (AtomVar f) -> [f]
          | _ -> Fir_callgraph.possible_functions cg f in
        (* List.iter print_symbol fl; Format.print_newline (); *)
        let cpl = List.map (cp_of_atom slat) al in
          List.fold_left (fun flat f ->
              let _, _, vl, _ = SymbolTable.find ft f in
              let sl =
                try
                  List.fold_left2 (fun sl v cp ->
                      let cp2 = SymbolTable.find sl v in
                      let cp = meet cp cp2 in
                        if cp != cp2 then (
                          Worklist.add wl f;
                          SymbolTable.add sl v cp)
                        else
                          sl)
                    (SymbolTable.find flat f) vl cpl
                with Not_found ->
                  Worklist.add wl f;
                  List.fold_left2 SymbolTable.add SymbolTable.empty vl cpl
              in
                SymbolTable.add flat f sl)
            flat fl
    | SpecialCall _ ->
        raise (FirException (ExnExp e, InternalError "cprop_expr: SpecialCall not implemented"))
    | Match (a, sel) ->
        let rec branch t = function
            [] -> flat
          | (s, e) :: sel ->
              if t s then
                cpe flat slat e
              else
                branch t sel in
        (match cp_of_atom slat a with
            CpAtom (AtomInt i | AtomEnum (_, i) | AtomConst (_, _, i)) ->
              branch (function IntSet s -> IntSet.mem_point i s | _ -> false) sel
          | CpAtom (AtomRawInt i) ->
              branch (function RawIntSet s -> RawIntSet.mem_point i s | _ -> false) sel
          | _ ->
              List.fold_left (fun flat (_, e) -> cpe flat slat e) flat sel)
    | TypeCase (a1, a2, name, v, e1, e2) ->
        let slat = SymbolTable.add slat v CpBot in
           cpe (cpe flat slat e1) slat e2
    | LetAssert (v1, _, _, _, e) ->
        let slat = SymbolTable.add slat v1 CpBot in
          cpe flat slat e

(************************************** iterative dataflow analysis *)

let init_flat export funs =
   SymbolTable.fold (fun flat f (_, _, vl, _) ->
         if SymbolTable.mem export f then
            SymbolTable.add flat f (
               List.fold_left (fun slat v -> SymbolTable.add slat v CpBot) SymbolTable.empty vl)
         else flat)
   SymbolTable.empty funs

let analyze cg ft flat =
  let rpo = List.map Fir_callgraph.fun_of_node (Fir_callgraph.rev_postorder_list cg) in
  let wl = Worklist.of_list rpo in
  let rec loop flat =
    if Worklist.is_empty wl then
      flat
    else
      let f = Worklist.take wl in
        try
          let slat = SymbolTable.find flat f in
          let _, _, _, e = SymbolTable.find ft f in
            (* Format.printf "*********************** processing "; print_symbol f; print_flat flat; *)
            loop (cprop_expr cg wl ft flat slat e)
        with Not_found ->
          (* Format.printf "******************* skipping "; print_symbol f; Format.print_newline (); *)
          loop flat
  in
    loop flat

(************************************** apply dataflow analysis results *)

let reduce_var slat v =
  try
    match SymbolTable.find slat v with
        CpAtom (AtomVar v) -> v
      | _ -> v
  with Not_found -> v

let reduce_atom slat a =
  match a with
      AtomVar v ->
        (try
          match SymbolTable.find slat v with
              CpBot -> a
            | CpAtom a -> a
        with Not_found -> a)
    | _ -> a

let rec reduce_expr cg slat e =
  let ra = reduce_atom slat in
  let ral = List.map ra in
  let rv = reduce_var slat in
  match e with
      LetUnop (v, ty, op, a, e) ->
        let t = ETUnop (op, etree_of_atom slat a) in
          (match canonicalize_etree t with
              ETConst a ->
                let slat = SymbolTable.add slat v (CpAtom a) in
                  reduce_expr cg slat e
            | _ ->
                let slat = SymbolTable.add slat v CpBot in
                  LetUnop (v, ty, op, ra a, reduce_expr cg slat e))
    | LetBinop (v, ty, op, a1, a2, e) ->
        let t = ETBinop (op, etree_of_atom slat a1, etree_of_atom slat a2) in
          (match canonicalize_etree t with
              ETConst a ->
                let slat = SymbolTable.add slat v (CpAtom a) in
                  reduce_expr cg slat e
            | _ ->
                let slat = SymbolTable.add slat v CpBot in
                  LetBinop (v, ty, op, ra a1, ra a2, reduce_expr cg slat e))
    | LetExt (v, ty, s, ty2, al, e) ->
        let slat = SymbolTable.add slat v CpBot in
          LetExt (v, ty, s, ty2, ral al, reduce_expr cg slat e)
    | TailCall (f, al) ->
        TailCall (rv f, ral al)
    | SpecialCall (TailSysMigrate (label, aptr, aoff, f, args)) ->
        SpecialCall (TailSysMigrate (label, ra aptr, ra aoff, rv f, ral args))
    | SpecialCall _ ->
        raise (FirException (ExnExp e, InternalError "cprop_expr: SpecialCall not implemented"))
    | Match (a, sel) ->
        let rec branch t = function
            [] -> raise (FirException (ExnExp e, StringError "incomplete match in cprop"))
          | (s, e) :: sel ->
              if t s then
                reduce_expr cg slat e
              else
                branch t sel in
        (match ra a with
            AtomInt i | AtomEnum (_, i) | AtomConst (_, _, i) ->
              branch (function IntSet s -> IntSet.mem_point i s | _ -> false) sel
          | AtomRawInt i ->
              branch (function RawIntSet s -> RawIntSet.mem_point i s | _ -> false) sel
          | a ->
              Match (a, List.map (fun (s, e) -> s, reduce_expr cg slat e) sel))
    | TypeCase (a1, a2, name, v, e1, e2) ->
         TypeCase (ra a1, ra a2, name, v, reduce_expr cg slat e1, reduce_expr cg slat e2)
    | LetAlloc (v, op, e) ->
        let op = match op with
            AllocTuple (tclass, t, al) -> AllocTuple (tclass, t, ral al)
          | AllocArray (t, al) -> AllocArray (t, ral al)
          | AllocMArray (t, al, a) -> AllocMArray (t, ral al, ra a)
          | AllocUnion (t, v, i, al) -> AllocUnion (t, v, i, ral al)
          | AllocMalloc a -> AllocMalloc (ra a) in
        let slat = SymbolTable.add slat v CpBot in
          LetAlloc (v, op, reduce_expr cg slat e)
    | LetSubscript (k, v, ty, v2, a, e) ->
        let slat = SymbolTable.add slat v CpBot in
          LetSubscript (k, v, ty, rv v2, ra a, reduce_expr cg slat e)
    | SetSubscript (k, v, a1, ty, a2, e) ->
        SetSubscript (k, rv v, ra a1, ty, ra a2, reduce_expr cg slat e)
    | SetGlobal (k, v, ty, a, e) ->
        SetGlobal (k, rv v, ty, ra a, reduce_expr cg slat e)
    | Memcpy (k, v1, a1, v2, a2, a3, e) ->
        Memcpy (k, rv v1, ra a1, v2, ra a2, ra a3, reduce_expr cg slat e)
    | LetAssert (v1, ty, v2, pred, e) ->
         let slat = SymbolTable.add slat v1 CpBot in
            LetAssert (v1, ty, v2, pred, reduce_expr cg slat e)
    | Debug (di, e) ->
        Debug (di, reduce_expr cg slat e)

let reduce_funs cg flat funs =
  SymbolTable.mapi (fun f (dl, ty, vl, e) ->
      try
        dl, ty, vl, reduce_expr cg (SymbolTable.find flat f) e
      with Not_found ->
        dl, ty, vl, e)
    funs

(************************************** global function *)

let cprop_prog cg prog =
  let { prog_export = export; prog_funs = funs } = prog in
  let flat = init_flat export funs in
  let flat = analyze cg funs flat in
  let funs = reduce_funs cg flat funs in
    { prog with prog_funs = funs }





