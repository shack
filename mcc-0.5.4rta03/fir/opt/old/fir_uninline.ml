(*
 * Insert function calls just after branch points.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fir
open Fir_env
open Fir_type
open Fir_exn
open Fir_standardize

(*
 * Get the type of every variable in the program.
 *)
let rec type_exp nenv venv e =
   let pos = string_pos "type_exp" (exp_pos e) in
      match e with
         LetUnop (v, ty, _, _, e)
       | LetBinop (v, ty, _, _, _, e)
       | LetExt (v, ty, _, _, _, e)
       | LetSubscript (_, v, ty, _, _, e) ->
            let venv = venv_add venv v ty in
               type_exp nenv venv e
       | LetAlloc (v, op, e) ->
            let venv = venv_add venv v (type_of_alloc_op op) in
               type_exp nenv venv e
       | TailCall _
       | SpecialCall _ ->
            venv
       | TypeCase (_, _, name, v, e1, e2) ->
            let venv = venv_add venv v (venv_lookup nenv pos name) in
               type_exp nenv venv e
       | Assert (_, _, e)
       | SetSubscript (_, _, _, _, _, e)
       | SetGlobal (_, _, _, _, e)
       | Memcpy (_, _, _, _, _, _, e)
       | Debug (_, e) ->
            type_exp nenv venv e
       | Match (_, cases) ->
            List.fold_left (fun venv (_, e) ->
                  type_exp nenv venv e) venv cases

(*
 * Type for a function.
 *)
let type_fun tenv nenv venv f (info, ty, vars, e) =
   let pos = string_pos "type_fun" (vexp_pos f) in
   let _, ty_vars, _ = dest_fun_type tenv pos ty in
   let venv = List.fold_left2 venv_add venv vars ty_vars in
      type_exp nenv venv e

(*
 * Get all the var types in the program.
 *)
let type_prog prog =
   let { prog_types = tenv;
         prog_names = nenv;
         prog_funs = funs
       } = prog
   in
      SymbolTable.fold (type_fun tenv nenv) SymbolTable.empty funs

(*
 * Liveness table.
 *)
let live_remove = SymbolTable.remove

let live_var venv pos live v =
   try SymbolTable.add live v (SymbolTable.find venv v) with
      Not_found ->
         live

let live_atom venv pos live a =
   match a with
      AtomVar v ->
         live_var venv pos live v
    | _ ->
         live

let live_atoms venv pos live args =
   List.fold_left (live_atom venv pos) live args

let live_alloc venv pos live op =
   match op with
      AllocTuple (_, _, args)
    | AllocArray (_, args)
    | AllocUnion (_, _, _, args) ->
         live_atoms venv pos live args
    | AllocVArray (_, _, a1, a2) ->
         live_atom venv pos (live_atom venv pos live a1) a2
    | AllocMalloc a ->
         live_atom venv pos live a

let live_pred venv pos live pred =
   match pred with
      PredNop (v, _) ->
         live_var venv pos live v
    | PredUnop (v, _, a) ->
         live_atom venv pos (live_var venv pos live v) a
    | PredBinop (v, _, a1, a2) ->
         live_atom venv pos (live_atom venv pos (live_var venv pos live v) a1) a2

let live_debug venv pos live op =
   match op with
      DebugString _ ->
         live
    | DebugContext (_, vars) ->
         List.fold_left (fun live (v1, _, v2) ->
               live_var venv pos (live_var venv pos live v1) v2) live vars

(*
 * Break apart matches.
 *)
let rec uninline_exp venv funs live e =
   let pos = string_pos "uninline_exp" (exp_pos e) in
      match e with
         LetUnop (v, ty, op, a, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_atom venv pos live a in
            let e = LetUnop (v, ty, op, a, e) in
               funs, live, e
       | LetBinop (v, ty, op, a1, a2, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
            let e = LetBinop (v, ty, op, a1, a2, e) in
               funs, live, e
       | LetExt (v, ty1, s, ty2, args, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_atoms venv pos live args in
            let e = LetExt (v, ty1, s, ty2, args, e) in
               funs, live, e
       | TailCall (f, args)
       | SpecialCall (TailAtomicCommit (f, args)) ->
            let live = live_var venv pos live f in
            let live = live_atoms venv pos live args in
               funs, live, e
       | SpecialCall (TailSysMigrate (_, a1, a2, f, args)) ->
            let live = live_var venv pos live f in
            let live = live_atoms venv pos live args in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
               funs, live, e
       | SpecialCall (TailAtomic (f, a, args)) ->
            let live = live_var venv pos live f in
            let live = live_atoms venv pos live args in
            let live = live_atom venv pos live a in
               funs, live, e
       | SpecialCall (TailAtomicRollback a) ->
            let live = live_atom venv pos live a in
               funs, live, e
       | TypeCase (a1, a2, name, v, e1, e2) ->
            let funs, live, e1 = uninline_exp venv funs live e1 in
            let live = live_remove live v in
            let funs, live, e2 = uninline_exp venv funs live e2 in
            let live = live_var venv pos live name in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
            let e = TypeCase (a1, a2, name, v, e1, e2) in
               funs, live, e
       | LetAlloc (v, op, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v in
            let live = live_alloc venv pos live op in
            let e = LetAlloc (v, op, e) in
               funs, live, e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_remove live v1 in
            let live = live_var venv pos live v2 in
            let live = live_atom venv pos live a in
            let e = LetSubscript (op, v1, ty, v2, a, e) in
               funs, live, e
       | SetSubscript (op, v, a1, ty, a2, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_var venv pos live v in
            let live = live_atom venv pos live a1 in
            let live = live_atom venv pos live a2 in
            let e = SetSubscript (op, v, a1, ty, a2, e) in
               funs, live, e
       | SetGlobal (op, v, ty, a, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_var venv pos live v in
            let live = live_atom venv pos live a in
            let e = SetGlobal (op, v, ty, a, e) in
               funs, live, e
       | Memcpy (op, v1, a1, v2, a2, a3, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_var venv pos live v1 in
            let live = live_atom venv pos live a1 in
            let live = live_var venv pos live v2 in
            let live = live_atom venv pos live a2 in
            let live = live_atom venv pos live a3 in
            let e = Memcpy (op, v1, a1, v2, a2, a3, e) in
               funs, live, e
       | Assert (label, pred, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_pred venv pos live pred in
            let e = Assert (label, pred, e) in
               funs, live, e
       | Debug (info, e) ->
            let funs, live, e = uninline_exp venv funs live e in
            let live = live_debug venv pos live info in
            let e = Debug (info, e) in
               funs, live, e
       | Match (a, cases) ->
            let funs, live, cases =
               List.fold_left (fun (funs, live, cases) (set, e) ->
                     let funs, live, e = uninline_exp venv funs live e in
                     let funs, e =
                        match e with
                           TailCall _ ->
                              (* Don't add more junk *)
                              funs, e
                         | _ ->
                              let ty_vars, vars =
                                 SymbolTable.fold (fun (ty_vars, vars) v ty ->
                                       ty :: ty_vars, v :: vars) ([], []) live
                              in
                              let ty_fun = TyFun (ty_vars, TyEnum 0) in
                              let f = new_symbol_string "uninline" in
                              let funs = SymbolTable.add funs f (("unknown", 0), ty_fun, vars, e) in
                              let args = List.map (fun v -> AtomVar v) vars in
                                 funs, TailCall (f, args)
                     in
                     let cases = (set, e) :: cases in
                        funs, live, cases) (funs, live, []) cases
            in
            let cases = List.rev cases in
            let live = live_atom venv pos live a in
            let e = Match (a, cases) in
               funs, live, e

(*
 * Uninline a function.
 *)
let uninline_fun venv funs f (info, ty, vars, e) =
   let funs, _, e = uninline_exp venv funs SymbolTable.empty e in
      SymbolTable.add funs f (info, ty, vars, e)

(*
 * Uninline the program.
 *)
let uninline_prog prog =
   let venv = type_prog prog in
   let funs = prog.prog_funs in
   let funs = SymbolTable.fold (uninline_fun venv) funs funs in
   let prog = { prog with prog_funs = funs } in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
