(* Optimize arithmetic calculations.
 * Geoffrey Irving
 * $Id: fir_simplify.mli,v 1.2 2002/04/18 03:48:23 jyh Exp $ *)

open Fir
 
val simplify_prog : prog -> prog
