(* Fir toplevel hoisting
 * Geoffrey Irving
 * $Id: fir_tophoist.mli,v 1.1 2002/06/03 19:39:22 justins Exp $ *)

open Fir
open Fir_alias

val tophoist_prog : alias_info -> prog -> prog
