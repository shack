(* Fir Alias Analysis
 * Geoffrey Irving
 * $Id: fir_alias.ml,v 1.3 2002/06/24 00:00:53 jyh Exp $ *)

open Symbol
open Fir
open Fir_exn

(* Current algorithm:
 *   Bjarne Steensgaard.  Points-to analysis in almost linear time.
 *   Technical Report MSR-TR-95-08, Microsoft Research, Mark 1995.
 *
 * Algorithm notes:
 *   1. This algorithm is extremely fast.  Apparent from setting up the
 *      venv, it takes O(n\alpha(n)), where \alpha is the inverse
 *      Ackermann function.
 *   2. It is one of the least accurate algorithms around.
 *
 * Implementation notes:
 *   1. Variables known to contain no pointer data are not included in the venv
 *   2. The variable environment maps straight to \alpha, since fir never takes
 *      the address of a variable.
 *   3. Statements are processed in the following order (both stages simultaneously):
 *        a. all function definitions
 *        b. all globals
 *        c. expressions (function bodies)
 *   4. The type system is slightly generalized to handle mixed functions with different
 *      arity.  Instead of \tau and \lambda, there are countably many type constructors
 *      numbered from -1 up.  -1 is \tau, and n >= 0 is a function with n arguments.  Each
 *      type constructor is a fixed arity tuple of alpha's.  Each alpha begins empty (all
 *      constructors have implicit bottom type), then grows as necessary to satisfy
 *      constraints.  Constructors are denoted \gamma_n, n >= -1.
 *   5. The join code assumes that it's never told to operate on different constructors.
 *   6. Since the alpha's contain infinitely many implicit bots.  We can no longer easily
 *      satisfy the constraints by processing all statements in order.  Instead, we construct
 *      a constraint graph for alphas and solve it at the end.  If e1 <= e2, there is an
 *      edge from e1 to e2.  Since bots are implicit, there is no longer any need for an
 *      explicit node to handle them.
 *   7. We called the constraint graph cg.  I repeat: cg does not stand for callgraph.
 *
 * Type system:
 *   \alpha = int{n} -> \gamma_n
 *   \gamma_n = \bot | gam_n(\alpha, ..., \alpha)  (n times)
 *   \tau = \gamma_{-1}, \lambda (with arity n) = \gamma_n
 *)

exception Impossible of string

(************************************** type system and union/find code *)

(* node represents both alpha and gamma *)

type node =
    Alpha of int * cnode * gamma list   (* rank * gamma list * pending *)
  | Gam of int * int * alpha array      (* rank * id * arguments *)
  | Edge of node ref

and alpha = node ref
and gamma = node ref

and cgraph = node ref Digraph.t   (* constraint graph for alphas *)
and cnode = node ref Digraph.node

let rec temp_node = Edge { contents = temp_node }

let make_alpha cg gl =
  let a = ref temp_node in
    a := Alpha (0, Digraph.add_node cg a, gl);
    a

(* get id *)
let node_id g =
  match !g with
      Gam (_, i, _) -> i
    | Alpha _ | Edge _ -> raise (Impossible "wanted the id of an alpha or edge")

(* fast find *)
let find e =
  match !e with
      (* general case: four or more edges to root *)
      Edge { contents = Edge { contents = Edge { contents = Edge f } } } ->
        let rec find e =
          match !e with
              Edge e -> find e
            | _ -> e in
        let r = find f in
        let rec compress r e =
          match !e with
              Edge n ->
                e := Edge r;
                compress r n
            | _ -> r
        in
          compress r e
      (* now I'm just being silly *)
    | Edge ({ contents = Edge { contents = Edge r } } as f) ->
        e := Edge r; f := Edge r; r
    | Edge { contents = Edge r } ->    (* two edges (is this going too far?) *)
        e := Edge r; r
    | Edge r -> r                      (* single edge from e to root *)
    | _ -> e                           (* e is root *)

let rec merge_gamma_lists lx ly p =
  match lx, ly with
      [], l | l, [] ->
        l, p
    | x :: lx', y :: ly' ->
        let x = find x in
        let y = find y in
        let n1 = node_id x in
        let n2 = node_id y in
          if n1 == n2 then
            let l, p = merge_gamma_lists lx' ly' p in
              x :: l, (x, y) :: p
          else if n1 < n2 then
            let l, p = merge_gamma_lists lx' ly  p in
              x :: l, p
          else (* n1 > n2 *)
            let l, p = merge_gamma_lists lx  ly' p in
              y :: l, p

(* force e2 <= e1 as per the partial ordering in the paper (note the order) *)
let rec cjoin e1 e2 =
  let e1 = find e1 in
  let e2 = find e2 in
    if e1 != e2 then
      match !e1, !e2 with
          Alpha (_, cn1, _), Alpha (_, cn2, _) ->
            Digraph.add_edge cn2 cn1
        | _ -> raise (Impossible "tried to cjoin a non-alpha")

(* force a to have gam_id(ar), and return ar *)
let force cg id len a =
  let a = find a in
  match !a with
      Alpha (n, cn, gl) ->
        let make gl =
          let ar = Array.init len (fun _ -> make_alpha cg []) in
          let g = ref (Gam (0, id, ar)) in
            ar, g :: gl in
        let rec loop gl =
          match gl with
              [] -> make []
            | g :: gl' ->
                let g = find g in
                  match !g with
                      Gam (_, i, ar) ->
                        if i == id then
                          ar, gl
                        else if i < id then
                          let r, gl = loop gl' in
                            r, g :: gl
                        else  (* i > id *)
                          make gl
                    | Alpha _ | Edge _ ->
                        raise (Impossible "found an alpha or an edge in a gamma list") in
        let ar, gl = loop gl in
          a := Alpha (n, cn, gl);
          ar
    | Gam _ | Edge _ ->
        raise (Impossible "called force on a gamma")

let force_lam cg len a = force cg len len a
let force_tau cg a = force cg (-1) 1 a

(* given an alpha and an id, return the associated gamma or Not_found *)
let find_gamma a id =
  match !(find a) with
      Alpha (_, _, gl) ->
        let rec loop = function
            [] -> raise Not_found
          | g :: gl ->
              let g = find g in
                if node_id g == id then
                  g
                else
                  loop gl
        in
          loop gl
    | Gam _ | Edge _ -> raise (Invalid_argument "find_gamma")

(************************************** expression inference *)

type venv = alpha SymbolTable.t

let type_of_atom venv pos a =
  match a with
      AtomVar v ->
        (try SymbolTable.find venv v
        with Not_found ->
          raise (Impossible "no pointer data in a pointer?"))
    | _ ->
        raise (FirException (pos, AtomNotAVar a))

let type_of_var venv v =
  try
    SymbolTable.find venv v
  with Not_found ->
    raise (Impossible "non-pointer variable used as a pointer")

let alpha_of_atom venv a =
  match a with
      AtomVar v ->
        (try Some (SymbolTable.find venv v)
        with Not_found -> None)
    | _ -> None

let unop_destroys_pointers = function
    UMinusIntOp | NotIntOp | UMinusFloatOp _ | FloatOfIntOp _ | IntOfFloatOp _ | AbsFloatOp _
  | SinOp _ | CosOp _ | SqrtOp _
  | RawIntOfFloatOp _ | FloatOfRawIntOp _ | FloatOfFloatOp _ -> true

  | IdOp | UMinusRawIntOp _ | NotRawIntOp _ | RawIntOfRawIntOp _
  | PointerOfRawIntOp _ | RawIntOfPointerOp _ | RawIntOfEnumOp _
  | RawBitFieldOp _ -> false

let binop_destroys_pointers = function
    PlusIntOp | MinusIntOp | MulIntOp | DivIntOp | RemIntOp | LslIntOp | LsrIntOp
  | AsrIntOp | AndIntOp | OrIntOp | XorIntOp | PlusFloatOp _ | MinusFloatOp _ |  MulFloatOp _
  | DivFloatOp _ | RemFloatOp _ | EqIntOp | NeqIntOp | LtIntOp | LeIntOp | GtIntOp | GeIntOp
  | EqEqOp | NeqEqOp | EqRawIntOp _ | NeqRawIntOp _ | LtRawIntOp _ | LeRawIntOp _ | GtRawIntOp _
  | GeRawIntOp _ | EqFloatOp _ | NeqFloatOp _ | LtFloatOp _ | LeFloatOp _ | GtFloatOp _  | GeFloatOp _
  | MinIntOp | MaxIntOp | MinRawIntOp _ | MaxRawIntOp _ | MinFloatOp _ | MaxFloatOp _ | Atan2Op _
  | CmpIntOp | CmpRawIntOp _ | CmpFloatOp _ | AndEnumOp _ | OrEnumOp _ | XorEnumOp _ -> true

  | PlusRawIntOp _ | MulRawIntOp _ | DivRawIntOp _ | MinusRawIntOp _ | RemRawIntOp _
  | SlRawIntOp _ | SrRawIntOp _ | AndRawIntOp _ | OrRawIntOp _ | XorRawIntOp _
  | RawSetBitFieldOp _ -> false

let rec infer_expr cg venv e =
  match e with
      LetUnop (v, _, op, a, e) ->
        if unop_destroys_pointers op then
          infer_expr cg venv e
        else
          (match alpha_of_atom venv a with
              None ->
                infer_expr cg venv e
            | Some a ->
                let a_v = make_alpha cg [] in
                let venv = SymbolTable.add venv v a_v in
                  cjoin a_v a;
                  infer_expr cg venv e)

      (* This version of alias analysis ignores assertions *)
    | LetAssert (v1, ty, v2, _, e) ->
         infer_expr cg venv (LetUnop (v1, ty, IdOp, AtomVar v2, e))
    | LetBinop (v, _, op, a1, a2, e) ->
        if binop_destroys_pointers op then
          infer_expr cg venv e
        else
          (match alpha_of_atom venv a1, alpha_of_atom venv a2 with
              None, None ->
                infer_expr cg venv e
            | Some a, None | None, Some a ->
                let a_v = make_alpha cg [] in
                let venv = SymbolTable.add venv v a_v in
                  cjoin a_v a;
                  infer_expr cg venv e
            | Some a1, Some a2 ->
                let a_v = make_alpha cg [] in
                let venv = SymbolTable.add venv v a_v in
                  cjoin a_v a1;
                  cjoin a_v a2;
                  infer_expr cg venv e)
    | LetExt (v, _, _, _, al, e) ->
        (* BUG: this assumes that all external functions generate no aliasing beyond that
         * which can be induced by arithmetic *)
        let rec loop al =
          match al with
              [] ->
                infer_expr cg venv e
            | AtomVar va :: _ when SymbolTable.mem venv va ->
                let a_v = make_alpha cg [] in
                let venv = SymbolTable.add venv v a_v in
                  List.iter (function
                      AtomVar v ->
                        (try
                          cjoin a_v (SymbolTable.find venv v)
                        with Not_found -> ())
                    | _ -> ()) al;
                  infer_expr cg venv e
            | _ :: al ->
                loop al
        in
          loop al
    | TailCall (f, args) ->
        let len = List.length args in
        let ar = force_lam cg len (type_of_var venv f) in
        let rec loop i args =
          match args with
              [] -> venv
            | AtomVar v :: args ->
                (try
                  cjoin ar.(i) (SymbolTable.find venv v)
                with Not_found -> ());
                loop (i+1) args
            | _ :: args ->
                loop (i+1) args in
        loop 0 args
    | SpecialCall (TailSysMigrate (_, aptr, aoff, f, args)) ->
        let args = aptr :: aoff :: args in
        let len = List.length args in
        let ar = force_lam cg len (type_of_var venv f) in
        let rec loop i args =
          match args with
              [] -> venv
            | AtomVar v :: args ->
                (try
                  cjoin ar.(i) (SymbolTable.find venv v)
                with Not_found -> ());
                loop (i+1) args
            | _ :: args ->
                loop (i+1) args in
        loop 0 args
    | SpecialCall _ ->
        raise (FirException (ExnExp e, NotImplemented "fir_alias: SpecialCall not implemented"))
    | Match (_, sel) ->
         List.fold_left (fun venv (_, e) -> infer_expr cg venv e) venv sel
    | TypeCase (_, _, _, _, e1, e2) ->
         infer_expr cg (infer_expr cg venv e1) e2
    | LetAlloc (v, aop, e) ->
        let a = make_alpha cg [] in
        let t_v = ref (Gam (0, -1, [|a|])) in
        let a_v = make_alpha cg [t_v] in
        let venv = SymbolTable.add venv v a_v in
          (match aop with
              AllocTuple (_, _, al) | AllocArray (_, al) | AllocUnion (_, _, _, al) ->
                List.iter (function
                    AtomVar v ->
                      (try
                        cjoin a (SymbolTable.find venv v)
                      with Not_found -> ())
                 | _ -> ()) al
            | AllocMArray _
            | AllocMalloc _ -> ());
          infer_expr cg venv e
    | LetSubscript (_, x, _, y, _, e) ->
        let a1 = make_alpha cg [] in
        let venv = SymbolTable.add venv x a1 in
        let a2 = type_of_var venv y in
        let ar = force_tau cg a2 in
          cjoin a1 ar.(0);
          infer_expr cg venv e
    | SetSubscript (_, x, _, _, a, e) ->
        (match a with
            AtomVar v ->
              (try
                let a1 = type_of_var venv x in
                let ar = force_tau cg a1 in
                let a2 = SymbolTable.find venv v in
                let ar = force_tau cg ar.(0) in
                  cjoin ar.(0) a2
              with Not_found -> ())
          | _ -> ());
        infer_expr cg venv e
    | SetGlobal (_, x, _, a, e) ->
        (match a with
            AtomVar v ->
              (try
                let a1 = type_of_var venv x in
                let ar = force_tau cg a1 in
                let a2 = SymbolTable.find venv v in
                let ar = force_tau cg ar.(0) in
                  cjoin ar.(0) a2
              with Not_found -> ())
          | _ -> ());
        infer_expr cg venv e
    | Memcpy (_, x, _, y, _, _, e) ->
        let ar1 = force_tau cg (type_of_var venv x) in
        let ar2 = force_tau cg (type_of_var venv y) in
          cjoin ar1.(0) ar2.(0);
          infer_expr cg venv e
    | Debug (_, e) ->
        infer_expr cg venv e

(************************************** import inference *)

let infer_import cg venv v =
  let a = type_of_var venv v in
     ignore (force_tau cg a)

let infer_imports cg venv imports =
  let venv = SymbolTable.fold (fun venv v _ ->
    SymbolTable.add venv v (make_alpha cg [])) venv imports in
  SymbolTable.iter (fun v _ -> infer_import cg venv v) imports;
  venv

(************************************** global inference *)

let infer_global cg venv v init =
  let a = type_of_var venv v in
  match init with
      InitAtom (AtomVar v) ->
        (try
          cjoin a (SymbolTable.find venv v)
        with Not_found -> ())
    | InitAtom _ -> ()
    | InitRawData _
    | InitNames _
    | InitAlloc (AllocMalloc _)
    | InitAlloc (AllocMArray _) ->
        ignore (force_tau cg a)
    | InitAlloc (AllocTuple (_, _, al) | AllocArray (_, al) | AllocUnion (_, _, _, al)) ->
        let ar = force_tau cg a in
        let a1 = ar.(0) in
          List.iter (function
              AtomVar v ->
                (try
                  cjoin a1 (SymbolTable.find venv v)
                with Not_found -> ())
            | _ -> ()) al

let infer_globals cg venv globals =
  let venv = SymbolTable.fold (fun venv v _ ->
    SymbolTable.add venv v (make_alpha cg [])) venv globals in
  SymbolTable.iter (fun v (_, init) -> infer_global cg venv v init) globals;
  venv

(************************************** function inference *)

let infer_fundefs cg venv funs =
  SymbolTable.fold (fun venv f (_, _, vl, _) ->
    let venv, al = Mc_list_util.fold_map (fun venv v ->
      let a = make_alpha cg [] in
      let venv = SymbolTable.add venv v a in
        venv, a) venv vl in
    let ar = Array.of_list al in
    let len = Array.length ar in
    let a = make_alpha cg [ref (Gam (0, len, ar))] in
    let venv = SymbolTable.add venv f a in
      venv) venv funs

let infer_bodies cg venv funs =
  SymbolTable.fold (fun venv v (_, _, _, e) ->
      (* Format.print_string "Inferring "; print_symbol v; Format.print_newline (); *)
      infer_expr cg venv e)
    venv funs

(************************************** final constraint solving *)

let wl_add wl nl = List.iter (fun n -> Queue.add (Digraph.get n) wl) nl

let wl_touch wl n = wl_add wl (Digraph.succ n)

(* join and unify (inlined) *)
let rec join wl e1 e2 =
  match !e1, !e2 with
      Gam (n1, id, ar1), Gam (n2, _, ar2) ->
        if n1 < n2 then
          e1 := Edge e2
        else if n1 > n2 then
          e2 := Edge e1
        else (
          e1 := Edge e2;
          e2 := Gam (n1 + 1, id, ar2));
        for i = 0 to Array.length ar1 - 1 do
          let e1 = find ar1.(i) in
          let e2 = find ar2.(i) in
            if e1 != e2 then
              join wl e1 e2
          done
    | Alpha (n1, cn1, gl1), Alpha (n2, cn2, gl2) ->
        (* generate a merged list and a list of node pairs that need to be joined *)
        let gl, pending = merge_gamma_lists gl1 gl2 [] in
          (if n1 < n2 then
            let cn = Digraph.coalesce_nodes cn1 cn2 e2 in
              wl_touch wl cn;
              e1 := Edge e2;
              e2 := Alpha (n2, cn, gl)
          else if n1 > n2 then
            let cn = Digraph.coalesce_nodes cn2 cn1 e1 in
              wl_touch wl cn;
              e1 := Alpha (n1, cn, gl);
              e2 := Edge e1
          else
            let cn = Digraph.coalesce_nodes cn2 cn1 e1 in
              wl_touch wl cn;
              e1 := Alpha (n2 + 1, cn, gl);
              e2 := Edge e1);
        List.iter (fun (e1, e2) ->
            let e1 = find e1 in
            let e2 = find e2 in
              if e1 != e2 then
                join wl e1 e2)
          pending
    | Edge _, _ | _, Edge _ | Alpha _, Gam _ | Gam _, Alpha _ ->
        raise (Impossible "tried to join stuff that doesn't match")

let solve cg =
  let wl = Queue.create () in
    wl_add wl (Digraph.full_rev_postorder_list cg);
    let rec loop () =
      let a = find (Queue.take wl) in
        match !a with
            Alpha (n, cn, gl) ->
              let gl', p = List.fold_left (fun (gl, p) cn2 ->
                  match !(find (Digraph.get cn2)) with
                      Alpha (_, _, gl2) ->
                        merge_gamma_lists gl gl2 p
                    | Gam _ | Edge _ -> raise (Impossible "didn't find an alpha"))
                (gl, []) (Digraph.pred cn) in
              a := Alpha (n, cn, gl');
              let f = List.fold_left (fun f (e1, e2) ->
                  let e1 = find e1 in
                  let e2 = find e2 in
                    if e1 == e2 then
                      f
                    else
                      (join wl e1 e2; true))
                (not (Mc_list_util.length_eq gl gl')) p in
              if f then
                wl_touch wl cn;
              loop ()
          | Gam _ | Edge _ -> raise (Impossible "didn't find an alpha") in
    try
      loop ()
    with Queue.Empty ->
      ()

(************************************** toplevel inference *)

let infer_prog { prog_import = import; prog_globals = globals; prog_funs = funs } =
  let cg = Digraph.create () in
  let venv = infer_fundefs cg SymbolTable.empty funs in
  let venv = infer_imports cg venv import in
  let venv = infer_globals cg venv globals in
  let venv = infer_bodies cg venv funs in
    solve cg;
    venv

(************************************** debugging code *)

let node_list venv =
  let rec loop nl e =
    if List.memq e nl then
      nl
    else
      let e = find e in
      let nl = e :: nl in
        match !e with
            Alpha (_, _, el) ->
              List.fold_left loop nl el
          | Gam (_, _, ar) ->
              Array.fold_left loop nl ar
          | Edge _ -> nl in
  SymbolTable.fold (fun nl _ e -> loop nl e) [] venv

let print_alias s cg venv =
  let next_tag = ref 1 in
  let getname s = let n = !next_tag in next_tag := n + 1; s ^ string_of_int n in
  let sid id = match id with -1 -> "r" | n -> "f" ^ string_of_int n ^ "_" in
  let rec prep_node e =
    match !e with
        Alpha (_, _, []) ->  e, getname "_a"
      | Alpha _ ->           e, getname "a"
      | Gam (_, id, _) ->    e, getname (sid id)
      | Edge _ -> prep_node (find e) in
  let ait = List.map (fun n -> prep_node (Digraph.get n)) (Digraph.list cg) in
  let gl = List.flatten (List.map (function ({contents=Alpha (_, _, gl)}, _) -> gl | _ -> []) ait) in
  let git = List.map prep_node gl in
  let it = List.append ait git in

  let f e = List.assq (find e) it in
  let p e = Format.print_string (List.assq e it) in
    Format.printf "@\n\n*************** Alias analysis: %s *****\n" s;
    Format.print_string "\n*** venv ***\n";
    SymbolTable.iter (fun v e -> print_symbol v; Format.printf ": %s\n" (f e)) venv;
    Format.print_string "\n*** type details ***\n";
    List.iter (fun (e, s) ->
        match !e with
            Edge _ -> ()
          | Alpha (_, cn, el) ->
              Format.printf "%s =" s;
              List.iter (fun e -> Format.printf " %s" (f e)) el;
              Format.print_string " <";
              List.iter (fun n -> Format.printf " %s" (f (Digraph.get n))) (Digraph.succ cn);
              Format.print_string "\n"
          | Gam (_, _, er) ->
              Format.printf "%s =" s;
              Array.iter (fun e -> Format.printf " %s" (f e)) er;
              Format.print_string "\n")
      it;
    Format.printf "\n@\n";
    flush stdout

let debug_alias s { prog_import = import; prog_globals = globals; prog_funs = funs } =
  let cg = Digraph.create () in
  let venv = infer_fundefs cg SymbolTable.empty funs in
  let venv = infer_imports cg venv import in
  let venv = infer_globals cg venv globals in
  let venv = infer_bodies cg venv funs in
    solve cg;
    print_alias "After" cg venv

(************************************** global interface *)

type alias_info = venv

let alias_prog = infer_prog

let may_alias venv n v1 v2 =
  try
    let g1 = find_gamma (SymbolTable.find venv v1) n in
    let g2 = find_gamma (SymbolTable.find venv v2) n in
      g1 == g2
  with Not_found ->
    false

let may_alias_box venv = may_alias venv (-1)
let may_alias_fun = may_alias
