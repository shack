(*
 * Compute loop invariants and induction variables.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace
open Debug

open Fir
open Fir_exn
open Fir_loop
open Fir_type
open Fir_state
open Fir_print
open Fir_logic
open Fir_algebra
open Fir_loop.FirLoop
open Fir_logic.FirLogic

open Sizeof

let vexp_pos v = string_pos "Fir_alias" (vexp_pos v)
let exp_pos e = string_pos "Fir_alias" (exp_pos e)

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * This is the inner class we use for classifying
 * values during loop analysis.
 *)
type iclass =
   VarInvariant of etree
 | VarMustAlias of var
 | VarMayAlias of SymbolSet.t
 | VarUnknown

(*
 * A block alias.
 * The VarAlias field refers to a SymbolSet that defines
 * the alias class.  Aliases include info in arrays:
 *    alias_mutable: block is mutable
 *    alias_length: length of the array
 *    alias_default: default entry class
 *    alias_int_fields: fields based on integer indices
 *    alias_var_fields: fields based on var indices
 *)
type 'a block =
   { block_mutable : bool;
     block_length : 'a;
     block_default : 'a;
     block_int_fields : (int * 'a) list;
     block_var_fields : 'a SymbolTable.t
   }

(*
 * The alias environment.
 *)
type 'a aenv =
   { aenv_blocks : 'a block SymbolTable.t;
     aenv_unknown : 'a;
     aenv_set_of : 'a -> SymbolSet.t
   }

(*
 * A function call has arguments
 * and a predicate.
 *    call_changed: this call has changed since it was last analyzed
 *    call_args: function arguments
 *    call_aenv: alias environment at point of call
 *    call_pred: predicate at point of call
 *)
type 'a call =
   { call_changed : bool;
     call_args : 'a list;
     call_aenv : 'a aenv;
     call_pred : pred
   }

(*
 * This is the info we collect for a single function.
 * It is basically a repackaing of the function.
 *
 *   fun_name: the name of the function
 *   fun_vars: the parameters of the function
 *   fun_calls: the called functions, with arguments
 *)
type fun_info =
   { fun_name : var;
     fun_vars : var list;
     fun_body : exp
   }

(*
 * Loop info:
 *    loop_name: the name of the loop header function
 *    loop_vars: the parameters of the loop
 *    loop_loops: the nested loops
 *    loop_funs: functions in the loop, but not in the nested loops
 *)
type loop_info =
   { loop_name   : var;
     loop_funs   : loop_fun SymbolTable.t;
     mutable loop_calls  : iclass call SymbolTable.t
   }

and loop_fun =
   LoopFun of fun_info
 | LoopLoop of loop_info

(*
 * This is the info collected during function analysis.
 * We keep a list of function calls.  The function
 * calls are unified if there are multiple calls
 * to the same function.
 *
 *    lenv_vars: the variable environment
 *    lenv_calls: function calls
 *)
type loop_env =
   { lenv_vars   : iclass SymbolTable.t;
     lenv_calls  : iclass call SymbolTable.t
   }

(*
 * Loop information for a variable.
 *
 * LoopInduction (op, a):
 *    The variable is an induction variable,
 *    offset by the loop-invariant expression a
 *    each time around the loop.
 *
 * LoopInvariant:
 *    The value is loop-invariant
 *
 * LoopUnknown
 *    Don't know anything about the value
 *)
type int_class =
   IntClass
 | RawIntClass of int_precision * int_signed

type aclass =
   AliasInduction of var * int_class * etree
 | AliasDerived of var * int_class * etree * etree
 | AliasInvariant of etree
 | AliasMustAlias of var
 | AliasMayAlias of SymbolSet.t
 | AliasUnknown

(*
 * The variable environment include three parts
 * for each variable:
 *    alias_value: the variable's value
 *    alias_env: the current alias environment
 *    alias_pred: the integer program at this point
 *)
type avar =
   { alias_value : aclass;
     alias_env : aclass aenv;
     alias_pred : pred
   }

type alias_env = avar SymbolTable.t

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Function parameters.
 *)
let print_vars vars =
   Format.printf "(@[<hv 0>";
   ignore (List.fold_left (fun commap v ->
                 if commap then
                    Format.printf ",@ ";
                 print_symbol v;
                 true) false vars);
   Format.printf "@])"

(*
 * Print a loop variable.
 *)
let print_int_class op =
   match op with
      IntClass ->
         Format.print_string "<int>"
    | RawIntClass (pre, signed) ->
         Format.print_string "{";
         print_int_precision pre;
         Format.print_string ",";
         print_int_signed signed;
         Format.print_string "}"

let print_aclass = function
   AliasInduction (v, op, e) ->
      Format.printf "@[<hv 3><ind>(";
      print_symbol v;
      Format.printf ", ";
      print_int_class op;
      Format.printf ",@ ";
      print_etree e;
      Format.printf ")@]"
 | AliasDerived (v, op, e1, e2) ->
      Format.printf "@[<hv 3><der>(";
      print_symbol v;
      Format.printf ", ";
      print_int_class op;
      Format.printf ",@ ";
      print_etree e1;
      Format.printf ",@ ";
      print_etree e2;
      Format.printf ")@]"
 | AliasInvariant a ->
      Format.printf "@[<hv 3><inv>(";
      print_etree a;
      Format.printf ")@]"
 | AliasUnknown ->
      Format.print_string "<unknown>"
 | AliasMustAlias v ->
      Format.print_string "<mustalias>{";
      print_symbol v;
      Format.print_string "}"
 | AliasMayAlias s ->
      Format.printf "@[<hv 0>@[<hv 3><mayalias>{";
      SymbolSet.iter (fun v ->
            Format.print_space ();
            print_symbol v) s;
      Format.printf "@]@ }@]"

(*
 * Print an inner class.
 *)
let print_iclass = function
   VarInvariant e ->
      print_etree e
 | VarMustAlias v ->
      Format.printf "<mustalias>{";
      print_symbol v;
      Format.print_string "}"
 | VarMayAlias s ->
      Format.printf "@[<hv 0>@[<hv 3><mayalias>{";
      SymbolSet.iter (fun v ->
            Format.print_space ();
            print_symbol v) s;
      Format.printf "@]@ }@]"
 | VarUnknown ->
      Format.print_string "<none>"

(*
 * Print the alias environment.
 *)
let print_block print_class block =
   let { block_mutable = mutablep;
         block_length = length;
         block_default = default;
         block_int_fields = int_fields;
         block_var_fields = var_fields
       } = block
   in
      Format.printf "@[<hv 0>@[<hv 3>block {@ mutable = %b;@ length = " mutablep;
      print_class length;
      Format.printf ";@ @[<hv 3>default =@ ";
      print_class default;
      Format.printf "@];@ @[<hv 0>@[<hv 3>int_fields = {";
      List.iter (fun (i, ic) ->
            Format.printf "@ %d = " i;
            print_class ic;
            Format.printf ";") int_fields;
      Format.printf "@]@ }@];@ @[<hv 0>@[<hv 3>var_fields = {";
      SymbolTable.iter (fun v ic ->
            Format.printf "@ ";
            print_symbol v;
            Format.printf " = ";
            print_class ic;
            Format.printf ";") var_fields;
      Format.printf "@]@ }@];@]@ }@]"

let print_aenv print_class aenv =
   Format.printf "Alias environment:";
   SymbolTable.iter (fun v block ->
         Format.printf "@ @[<hv 3>";
         print_symbol v;
         Format.printf " =@ ";
         print_block print_class block;
         Format.printf "@]") aenv.aenv_blocks

let print_call print_class { call_args = args; call_pred = pred; call_aenv = aenv } =
   Format.printf "(@[<hv 3>";
   ignore (List.fold_left (fun commap e ->
                 if commap then
                    Format.print_space ();
                 print_class e;
                 true) false args);
   Format.printf ")@]@ @[<hv 3>";
   print_pred pred;
   Format.printf "@]@ @[<hv 3>";
   print_aenv print_class aenv;
   Format.printf "@]"

let print_calls print_class calls =
   SymbolTable.iter (fun f call ->
         Format.printf "@ @[<v 3>";
         print_symbol f;
         print_call print_class call;
         Format.printf "@]") calls

(*
 * Print a function description.
 *)
let print_fun_info info =
   let { fun_name = f;
         fun_vars = vars
       } = info
   in
      Format.printf "@[<v 3>Fun: ";
      print_symbol f;
      print_vars vars;
      Format.printf "@]"

(*
 * Print a loop description.
 *)
let rec print_loop_info info =
   let { loop_name = name;
         loop_funs = funs;
         loop_calls = calls
       } = info
   in
      Format.printf "@[<hv 3>Loop: ";
      print_symbol name;
      Format.printf "@ @[<hv 3>Body:";
      SymbolTable.iter (fun _ info ->
            Format.print_space ();
            print_loop_fun info) funs;
      Format.printf "@]@ @[<hv 3>Calls:";
      print_calls print_iclass calls;
      Format.printf "@]@]"

and print_loop_fun = function
   LoopFun info ->
      print_fun_info info
 | LoopLoop info ->
      print_loop_info info

(*
 * Print the loop fixpoint environment.
 *)
let print_lenv info =
   let { lenv_vars   = vars;
         lenv_calls  = calls
       } = info
   in
      Format.printf "@[<v 3>Lenv:";
      Format.printf "@ @[<v 3>Calls:";
      print_calls print_iclass calls;
      Format.printf "@]@ @[<v 3>Vars:";
      SymbolTable.iter (fun v ic ->
            Format.printf "@ @[<hv 3>";
            print_symbol v;
            Format.printf " =@ ";
            print_iclass ic;
            Format.printf "@]") vars;
      Format.printf "@]@]"

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Error if a function lookup fails.
 *)
let lookup_fun funs pos f =
   let info =
      try SymbolTable.find funs f with
         Not_found ->
            raise (FirException (pos, UnboundVar f))
   in
      match info with
         LoopFun info ->
            info
       | LoopLoop _ ->
            raise (FirException (pos, StringVarError ("the function is a loop header", f)))

(*
 * Get an integer from an atom.
 *)
let int_of_atom = function
   AtomInt i
 | AtomEnum (_, i) ->
      Some i
 | AtomRawInt i ->
      Some (Rawint.to_int i)
 | _ ->
      None

(************************************************************************
 * SUBSTITUTION
 ************************************************************************)

(*
 * ETree substitution.
 *)
type subst = iclass SymbolTable.t

let (subst_empty : subst) = SymbolTable.empty

let subst_create vars args =
   List.fold_left2 SymbolTable.add subst_empty vars args

(*
 * ETree substitution.
 *)
exception ETreeSubstNone

let rec subst_etree subst e =
   match e with
      ETConst _ ->
         e
    | ETVar v ->
         (try
             match SymbolTable.find subst v with
                VarInvariant e ->
                   e
              | VarMustAlias _
              | VarMayAlias _
              | VarUnknown ->
                   raise ETreeSubstNone
          with
             Not_found ->
                e)
    | ETUnop (op, e) ->
         ETUnop (op, subst_etree subst e)
    | ETBinop (op, e1, e2) ->
         ETBinop (op, subst_etree subst e1, subst_etree subst e2)

let subst_var subst e =
   match e with
      VarInvariant e ->
         VarInvariant (canonicalize_etree (subst_etree subst e))
    | VarMustAlias _
    | VarMayAlias _
    | VarUnknown ->
         e

let subst_var_opt subst e =
   try subst_var subst e with
      ETreeSubstNone ->
         VarUnknown

let subst_args subst args =
   let args =
      List.fold_left (fun args e ->
            subst_var_opt subst e :: args) [] args
   in
      List.rev args

(*
 * Substitute in an alias environment.
 *)
let subst_block subst block =
   let { block_length = length;
         block_int_fields = int_fields;
         block_var_fields = var_fields
       } = block
   in
   let length = subst_var_opt subst length in
   let int_fields =
      List.fold_left (fun int_fields (i, ic) ->
            try
               let ic = subst_var subst ic in
                  (i, ic) :: int_fields
            with
               ETreeSubstNone ->
                  int_fields) [] int_fields
   in
   let var_fields =
      SymbolTable.fold (fun var_fields v ic ->
            try
               let ic = subst_var subst ic in
                  SymbolTable.add var_fields v ic
            with
               Not_found ->
                  var_fields) SymbolTable.empty var_fields
   in
      { block with block_length = length;
                   block_int_fields = List.rev int_fields;
                   block_var_fields = var_fields
      }

let subst_aenv subst aenv =
   let { aenv_blocks = blocks } = aenv in
   let blocks = SymbolTable.map (subst_block subst) blocks in
      { aenv with aenv_blocks = blocks }

(*
 * Substitute for a predicate.
 *)
let subst_pred subst pred =
   pred_true

(************************************************************************
 * UNIFICATION
 ************************************************************************)

(*
 * Unify two variable classes.
 *)
let unify_var changed arg1 arg2 =
   match arg1, arg2 with
      VarInvariant e1, VarInvariant e2 ->
         if e1 = e2 then
            changed, arg1
         else
            true, VarUnknown
    | VarInvariant _, _
    | _, VarInvariant _
    | VarUnknown, _
    | _, VarUnknown ->
         changed || arg1 <> VarUnknown, VarUnknown

    | VarMustAlias v1, VarMustAlias v2 ->
         if Symbol.eq v1 v2 then
            changed, arg1
         else
            true, VarMayAlias (SymbolSet.add (SymbolSet.singleton v1) v2)
    | VarMayAlias s, VarMustAlias v when SymbolSet.mem s v ->
         changed, arg1
    | VarMustAlias v, VarMayAlias s
    | VarMayAlias s, VarMustAlias v ->
         true, VarMayAlias (SymbolSet.add s v)
    | VarMayAlias s1, VarMayAlias s2 ->
         let s = SymbolSet.union s1 s2 in
            changed || not (SymbolSet.equal s s1), VarMayAlias s

(*
 * Unify two argument lists.
 *)
let rec unify_args changed args1 args2 =
   match args1, args2 with
      arg1 :: args1, arg2 :: args2 ->
         let changed, args = unify_args changed args1 args2 in
         let changed, arg = unify_var changed arg1 arg2 in
            changed, arg :: args
    | [], _ ->
         let changed = changed || args2 <> [] in
            changed, args2
    | _ :: _, [] ->
         raise (Invalid_argument "unify_args")

(*
 * Merge two field lists.
 *)
let rec unify_int_fields fields1 fields2 =
   match fields1, fields2 with
      field1 :: fields1', field2 :: fields2' ->
         let i1, ic1 = field1 in
         let i2, ic2 = field2 in
            if i1 = i2 then
               let _, ic = unify_var true ic1 ic2 in
                  (i1, ic) :: unify_int_fields fields1' fields2'
            else if i1 < i2 then
               field1 :: unify_int_fields fields1' fields2
            else
               field2 :: unify_int_fields fields1 fields2'
    | [], _ ->
         fields2
    | _, [] ->
         fields1

(*
 * Merge two var field tables.
 *)
let unify_var_fields fields1 fields2 =
   let fields1, fields2 =
      SymbolTable.fold (fun (fields1, fields2) v ic1 ->
            try
               let ic2 = SymbolTable.find fields2 v in
               let _, ic = unify_var true ic1 ic2 in
               let fields1 = SymbolTable.add fields1 v ic in
               let fields2 = SymbolTable.remove fields2 v in
                  fields1, fields2
            with
               Not_found ->
                  fields1, fields2) (fields1, fields2) fields1
   in
      SymbolTable.fold SymbolTable.add fields1 fields2

(*
 * Unify two blocks.
 *)
let unify_block block1 block2 =
   let { block_mutable = mutable1;
         block_length = length1;
         block_default = default1;
         block_int_fields = int_fields1;
         block_var_fields = var_fields1
       } = block1
   in
   let { block_mutable = mutable2;
         block_length = length2;
         block_default = default2;
         block_int_fields = int_fields2;
         block_var_fields = var_fields2
       } = block1
   in
   let _, length = unify_var true length1 length2 in
   let _, default = unify_var true default1 default2 in
      { block_mutable = mutable1 && mutable2;
        block_length = length;
        block_default = default;
        block_int_fields = unify_int_fields int_fields1 int_fields2;
        block_var_fields = unify_var_fields var_fields1 var_fields2
      }

(*
 * Unify two alias environments.
 *)
let unify_aenv aenv1 aenv2 =
   let { aenv_blocks = blocks1 } = aenv1 in
   let { aenv_blocks = blocks2 } = aenv2 in
   let blocks1, blocks2 =
      SymbolTable.fold (fun (blocks1, blocks2) v block1 ->
            try
               let block2 = SymbolTable.find blocks2 v in
               let block = unify_block block1 block2 in
               let blocks1 = SymbolTable.add blocks1 v block in
               let blocks2 = SymbolTable.remove blocks2 v in
                  blocks1, blocks2
            with
               Not_found ->
                  blocks1, blocks2) (blocks1, blocks2) blocks1
   in
   let blocks = SymbolTable.fold SymbolTable.add blocks1 blocks2 in
      { aenv1 with aenv_blocks = blocks1 }

(*
 * Unify a call.
 *)
let unify_call call1 call2 =
   let { call_changed = changed;
         call_args = args1;
         call_pred = pred1;
         call_aenv = aenv1
       } = call1
   in
   let { call_args = args2;
         call_pred = pred2;
         call_aenv = aenv2
       } = call2
   in
   let changed, args = unify_args changed args1 args2 in
   let pred = pred_or pred1 pred2 in
   let aenv = unify_aenv aenv1 aenv2 in
      { call_changed = changed;
        call_args = args;
        call_pred = pred;
        call_aenv = aenv
      }

(************************************************************************
 * ALIAS OPERATIONS
 ************************************************************************)

(*
 * Default block.
 *)
let block_default =
   { block_mutable = false;
     block_length = VarUnknown;
     block_default = VarUnknown;
     block_int_fields = [];
     block_var_fields = SymbolTable.empty
   }

(*
 * Get the alias set.
 *)
let aenv_var_empty =
   let alias_set_of_var ic =
      match ic with
         VarMustAlias v ->
            SymbolSet.singleton v
       | VarMayAlias s ->
            s
       | _ ->
            SymbolSet.empty
   in
      { aenv_blocks = SymbolTable.empty;
        aenv_unknown = VarUnknown;
        aenv_set_of = alias_set_of_var
      }

let aenv_alias_empty =
   let alias_set_of_alias ic =
      match ic with
         AliasMustAlias v ->
            SymbolSet.singleton v
       | AliasMayAlias s ->
            s
       | _ ->
            SymbolSet.empty
   in
      { aenv_blocks = SymbolTable.empty;
        aenv_unknown = AliasUnknown;
        aenv_set_of = alias_set_of_alias
      }

let aenv_set_of aenv a =
   aenv.aenv_set_of a

(*
 * Add an alias entry.
 *)
let aenv_lookup_block aenv pos v =
   try SymbolTable.find aenv.aenv_blocks v with
      Not_found ->
         let pos = string_pos "aenv_lookup_block" pos in
            raise (FirException (pos, UnboundVar v))

let aenv_add_block aenv v block =
   { aenv with aenv_blocks = SymbolTable.add aenv.aenv_blocks v block }

(*
 * Update all the blocks in an alias class.
 *)
let alias_map aenv pos update s =
   let pos = string_pos "alias_update" pos in
   let blocks =
      SymbolSet.fold (fun blocks v ->
            SymbolTable.filter_add blocks v (function
               Some block ->
                  update block
             | None ->
                  raise (FirException (pos, UnboundVar v)))) aenv.aenv_blocks s
   in
      { aenv with aenv_blocks = blocks }

(*
 * Fold a value over the blocks.
 * The blocks are unchanged.
 *)
let alias_fold aenv pos update x s =
   let pos = string_pos "alias_fold" pos in
   let { aenv_blocks = blocks } = aenv in
      SymbolSet.fold (fun x v ->
            let block =
               try SymbolTable.find blocks v with
                  Not_found ->
                     raise (FirException (pos, UnboundVar v))
            in
               update x block) x s

(*
 * Overwrite the entire block.
 *)
let alias_overwrite aenv pos s =
   let pos = string_pos "alias_overwrite" pos in
   let update block =
      { block with block_default = aenv.aenv_unknown;
                   block_int_fields = [];
                   block_var_fields = SymbolTable.empty
      }
   in
      alias_map aenv pos update s

(*
 * Overwrite the arguments.
 *)
let alias_overwrite_args aenv pos ics =
   let pos = string_pos "alias_overwrite_atoms" pos in
   let { aenv_set_of = set_of } = aenv in
      List.fold_left (fun aenv ic ->
            alias_overwrite aenv pos (set_of ic)) aenv ics

(*
 * Fetch an field from a block, given a function
 * to fetch the value from individual blocks.
 *)
let alias_subscript_field (aenv : aclass aenv) pos fetch s =
   let pos = string_pos "alias_subscript_field" pos in

   (* Combine the possible values from all the blocks *)
   let ic_opt =
      alias_fold aenv pos (fun ic1 block ->
            let ic2 = fetch block in
            let ic =
               match ic1 with
                  Some ic1 ->
                     let _, ic = unify_var true ic1 ic2 in
                        ic
                | None ->
                     ic2
            in
               Some ic2) None s
   in
      match ic_opt with
         Some ic -> ic
       | None -> aenv.aenv_unknown

(*
 * Fetch a field with an integer index.
 *)
let alias_subscript_int (aenv : aclass aenv) pos op s i =
   let pos = string_pos "alias_subscript_int" pos in
   let i =
      match op.sub_index with
         ByteIndex -> i
       | WordIndex -> i * sizeof_int
   in

   (* Look through the field list *)
   let rec search_int_fields default fields =
      match fields with
         (i', ic) :: fields ->
            if i' = i then
               ic
            else
               search_int_fields default fields
       | [] ->
            default
   in
   let search_block block =
      let { block_int_fields = fields;
            block_default = default
          } = block
      in
         search_int_fields default fields
   in
      alias_subscript_field aenv pos search_block s

(*
 * Fetch a field with a variable index from a block.
 *)
let alias_subscript_bvar aenv pos s v =
   let pos = string_pos "alias_subscript_bvar" pos in

   (* Fetch the field *)
   let search_var_fields default fields =
      try SymbolTable.find fields v with
         Not_found ->
            default
   in
   let search_block block =
      let { block_var_fields = fields;
            block_default = default
          } = block
      in
         search_var_fields default fields
   in
      alias_subscript_field aenv pos search_block s

let alias_subscript_var aenv pos op s v =
   let pos = string_pos "alias_subscript_var" pos in
      match op.sub_index with
         ByteIndex ->
            alias_subscript_bvar aenv pos s v
       | WordIndex ->
            aenv.aenv_unknown

(*
 * Look up a field from a block.
 *)
let alias_subscript aenv pos op s e =
   let pos = string_pos "alias_subscript" pos in
      match e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_subscript_int aenv pos op s i
       | ETConst (AtomRawInt i) ->
            alias_subscript_int aenv pos op s (Rawint.to_int i)
       | ETVar v ->
            alias_subscript_var aenv pos op s v
       | _ ->
            aenv.aenv_unknown

(*
 * Store a field with an integer index.
 *)
let alias_may_set_subscript_int aenv pos op s i ic =
   let pos = string_pos "alias_set_subscript_int" pos in
   let i =
      match op.sub_index with
         ByteIndex -> i
       | WordIndex -> i * sizeof_pointer
   in

   (* Store in an integer field *)
   let rec store_int_field fields =
      match fields with
         (i', ic') as field :: fields' ->
            if i' = i then
               let _, ic = unify_var true ic ic' in
                  (i, ic) :: fields'
            else if i' > i then
               fields
            else
               field :: store_int_field fields'
       | [] ->
            []
   in
   let store_block block =
      let { block_int_fields = fields } = block in
         { block with block_int_fields = store_int_field fields }
   in
      alias_map aenv pos store_block s

(*
 * Store a field with a variable index.
 * For now, we assume no aliasing.
 *)
let alias_may_set_subscript_bvar aenv pos s v ic =
   let pos = string_pos "alias_set_subscript_bvar" pos in

   (* Store in an variable field *)
   let store_var_field fields =
      try
         let ic' = SymbolTable.find fields v in
         let _, ic = unify_var true ic ic' in
            SymbolTable.add fields v ic
      with
         Not_found ->
            fields
   in
   let store_block block =
      let { block_var_fields = fields } = block in
         { block with block_var_fields = store_var_field fields }
   in
      alias_map aenv pos store_block s

let alias_may_set_subscript_var aenv pos op s v ic =
   let pos = string_pos "alias_set_subscript_int" pos in
      match op.sub_index with
         WordIndex ->
            alias_overwrite aenv pos s
       | ByteIndex ->
            alias_may_set_subscript_bvar aenv pos s v ic

(*
 * Store a value in an array.
 * If we can't figure out what the index is, just destory the entire array.
 *)
let alias_may_set_subscript aenv pos op s e ic =
   let pos = string_pos "alias_set_subscript" pos in
      match e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_may_set_subscript_int aenv pos op s i ic
       | ETConst (AtomRawInt i) ->
            alias_may_set_subscript_int aenv pos op s (Rawint.to_int i) ic
       | ETVar v ->
            alias_may_set_subscript_var aenv pos op s v ic
       | _ ->
            alias_overwrite aenv pos s

(*
 * Store a field with an integer index.
 *)
let alias_must_set_subscript_int aenv pos op v i ic =
   let pos = string_pos "alias_set_subscript_int" pos in
   let i =
      match op.sub_index with
         ByteIndex -> i
       | WordIndex -> i * sizeof_pointer
   in

   (* Store in an integer field *)
   let rec store_int_field fields =
      match fields with
         (i', _) as field :: fields' ->
            if i' = i then
               (i, ic) :: fields'
            else if i' > i then
               (i, ic) :: fields
            else
               field :: store_int_field fields'
       | [] ->
            [i, ic]
   in
   let block = aenv_lookup_block aenv pos v in
   let block = { block with block_int_fields = store_int_field block.block_int_fields } in
      aenv_add_block aenv v block

(*
 * Store a field with a variable index.
 * For now, we assume no aliasing.
 *)
let alias_must_set_subscript_bvar aenv pos v1 v2 ic =
   let pos = string_pos "alias_must_set_subscript_bvar" pos in
   let block = aenv_lookup_block aenv pos v1 in
   let block = { block with block_var_fields = SymbolTable.add block.block_var_fields v2 ic } in
      aenv_add_block aenv v1 block

let alias_must_set_subscript_var aenv pos op v1 v2 ic =
   let pos = string_pos "alias_must_set_subscript_var" pos in
      match op.sub_index with
         WordIndex ->
            alias_overwrite aenv pos (SymbolSet.singleton v1)
       | ByteIndex ->
            alias_must_set_subscript_bvar aenv pos v1 v2 ic

(*
 * Store a value in an array.
 * If we can't figure out what the index is, just destory the entire array.
 *)
let alias_must_set_subscript aenv pos op v1 e ic =
   let pos = string_pos "alias_set_subscript" pos in
      match e with
         ETConst (AtomInt i)
       | ETConst (AtomEnum (_, i)) ->
            alias_must_set_subscript_int aenv pos op v1 i ic
       | ETConst (AtomRawInt i) ->
            alias_must_set_subscript_int aenv pos op v1 (Rawint.to_int i) ic
       | ETVar v2 ->
            alias_must_set_subscript_var aenv pos op v1 v2 ic
       | _ ->
            alias_overwrite aenv pos (SymbolSet.singleton v1)

(*
 * Copy data from one array to another.
 * Indices and bounds are constants.
 *)
let alias_memcpy_int aenv pos op s1 i1 s2 i2 i3 =
   let pos = string_pos "alias_memcpy_int" pos in
   let i1, i2, i3 =
      match op.sub_index with
         ByteIndex -> i1, i2, i3
       | WordIndex -> i1 * sizeof_pointer, i2 * sizeof_pointer, i3 * sizeof_pointer
   in

   (* Get the fields from the blocks *)
   let limit2 = i2 + i3 in
   let collect_block src block =
      (* Get the fields in range *)
      let fields =
         List.fold_left (fun fields (i, ic) ->
               if i >= i2 && i < limit2 then
                  (i, ic) :: fields
               else
                  fields) [] block.block_int_fields
      in
      let fields = List.rev fields in
         (* Merge two field lists *)
         unify_int_fields src fields
   in

   (* Put together the fields in the sources *)
   let src = alias_fold aenv pos collect_block [] s2 in

   (* Store the fields *)
   let limit1 = i1 + i3 in
   let store_block block =
      let fields = unify_int_fields src block.block_int_fields in
         { block with block_int_fields = fields }
   in
      alias_map aenv pos store_block s1

(*
 * Copy data from one array to another.
 * Constant offsets.
 *)
let alias_memcpy_const aenv pos op s1 e1 s2 e2 e3 =
   let pos = string_pos "alias_memcpy_const" pos in
      match int_of_atom e1, int_of_atom e2, int_of_atom e3 with
         Some i1, Some i2, Some i3 ->
            alias_memcpy_int aenv pos op s1 i1 s2 i2 i3
       | _ ->
            alias_overwrite aenv pos s1

(*
 * Copy data from one array to another.
 *)
let alias_memcpy aenv pos op s1 e1 s2 e2 e3 =
   let pos = string_pos "alias_memcpy" pos in
      match e1, e2, e3 with
         ETConst e1, ETConst e2, ETConst e3 ->
            alias_memcpy_const aenv pos op s1 e1 s2 e2 e3
       | _ ->
            alias_overwrite aenv pos s1

(************************************************************************
 * LOOP ENVIRONMENT
 ************************************************************************)

(*
 * Loop environment.
 *)
let lenv_add_var lenv v e =
   { lenv with lenv_vars = SymbolTable.add lenv.lenv_vars v e }

let lenv_lookup_var lenv pos v =
   try SymbolTable.find lenv.lenv_vars v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let lenv_lookup_atom lenv pos a =
   let pos = string_pos "lenv_lookup_atom" pos in
      match a with
         AtomVar v ->
            lenv_lookup_var lenv pos v
       | _ ->
            VarInvariant (ETConst a)

(*
 * Unify the arguments in a function call.
 *)
let lenv_call lenv f call2 =
   let call =
      try
         let call1 = SymbolTable.find lenv.lenv_calls f in
            unify_call call1 call2
      with
         Not_found ->
            call2
   in
      { lenv with lenv_calls = SymbolTable.add lenv.lenv_calls f call }

(*
 * Add a predicate assignment.
 *)
let pred_op pred v a =
   match a with
      VarInvariant e ->
         pred_assign pred v e
    | VarMustAlias _
    | VarMayAlias _
    | VarUnknown ->
         pred

(************************************************************************
 * FUNCTION ANALYSIS
 ************************************************************************)

(*
 * Scan a function in the loop.
 *)
let rec fun_exp lenv aenv pred e =
   let pos = string_pos "fun_exp" (exp_pos e) in
      match e with
         LetUnop (v, _, op, a, e) ->
            fun_unop_exp lenv aenv pred pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            fun_binop_exp lenv aenv pred pos v op a1 a2 e
       | TailCall (f, args) ->
            fun_tailcall_exp lenv aenv pred pos f args
       | Call (f, args, e) ->
            fun_call_exp lenv aenv pred pos f args e
       | Match (a, cases) ->
            fun_match_exp lenv aenv pred pos a cases
       | TypeCase (_, _, _, v, e1, e2) ->
            fun_typecase_exp lenv aenv pred pos v e1 e2
       | LetExt (v, _, _, _, args, e) ->
            fun_ext_exp lenv aenv pred pos v args e
       | LetAlloc (v, op, e) ->
            fun_alloc_exp lenv aenv pred pos v op e
       | LetSubscript (op, v1, _, v2, a3, e) ->
            fun_subscript_exp lenv aenv pred pos op v1 v2 a3 e
       | SetSubscript (op, v1, a2, _, a3, e) ->
            fun_set_subscript_exp lenv aenv pred pos op v1 a2 a3 e
       | Memcpy (op, v1, a1, v2, a2, a3, e) ->
            fun_memcpy_exp lenv aenv pred pos op v1 a1 v2 a2 a3 e
       | Assert (_, pred', e) ->
            fun_assert_exp lenv aenv pred pos pred' e
       | SetGlobal (_, _, _, _, e)
       | Debug (_, e) ->
            fun_exp lenv aenv pred e
       | SpecialCall _ ->
            lenv

(*
 * Catch simple unops.
 *)
and fun_unop_exp lenv aenv pred pos v op a e =
   let pos = string_pos "fun_unop_exp" pos in
   let a = lenv_lookup_atom lenv pos a in
   let a =
      match op, a with
         IdOp, _ ->
            a
       | _, VarInvariant a ->
            VarInvariant (canonicalize_etree (ETUnop (op, a)))
       | _ ->
            VarUnknown
   in
   let lenv = lenv_add_var lenv v a in
   let pred = pred_op pred v a in
      fun_exp lenv aenv pred e

(*
 * Binary operations.
 *)
and fun_binop_exp lenv aenv pred pos v op a1 a2 e =
   let pos = string_pos "fun_binop_exp" pos in
   let a1 = lenv_lookup_atom lenv pos a1 in
   let a2 = lenv_lookup_atom lenv pos a2 in
   let a =
      match a1, a2 with
         VarInvariant a1, VarInvariant a2 ->
            VarInvariant (canonicalize_etree (ETBinop (op, a1, a2)))
       | _ ->
            VarUnknown
   in
   let lenv = lenv_add_var lenv v a in
   let pred = pred_op pred v a in
      fun_exp lenv aenv pred e

(*
 * Match expression.
 *)
and fun_match_exp lenv aenv pred pos a cases =
   let pos = string_pos "fun_match_exp" pos in
      List.fold_left (fun lenv (s, e) ->
            let pred = pred_match pred a s in
               fun_exp lenv aenv pred e) lenv cases

(*
 * Typecase.
 *)
and fun_typecase_exp lenv aenv pred pos v e1 e2 =
   let pos = string_pos "fun_typecase_exp" pos in
   let lenv = lenv_add_var lenv v VarUnknown in
   let lenv = fun_exp lenv aenv pred e1 in
   let lenv = fun_exp lenv aenv pred e2 in
      lenv

(*
 * External call.
 * We assume the external call obliterates its pointer arguments.
 *)
and fun_ext_exp lenv aenv pred pos v args e =
   let pos = string_pos "fun_ext_exp" pos in
   let lenv = lenv_add_var lenv v VarUnknown in
   let args = List.map (lenv_lookup_atom lenv pos) args in
   let aenv = alias_overwrite_args aenv pos args in
      fun_exp lenv aenv pred e

(*
 * An allocation.
 *)
and fun_alloc_exp lenv aenv pred pos v op e =
   let pos = string_pos "fun_alloc_exp" pos in
   let block =
      match op with
         AllocTuple (_, _, args)
       | AllocUnion (_, _, _, args)
       | AllocArray (_, args) ->
            fun_alloc_tuple lenv pos v args
       | AllocVArray (_, _, a1, a2) ->
            fun_alloc_array lenv pos a1 a2
       | AllocMalloc a ->
            fun_malloc lenv pos a
   in
   let lenv = lenv_add_var lenv v (VarMustAlias v) in
   let aenv = aenv_add_block aenv v block in
      fun_exp lenv aenv pred e

(*
 * Allocate a tuple.
 *)
and fun_alloc_tuple lenv pos v args =
   let pos = string_pos "fun_alloc_tuple" pos in
   let fields, length =
      List.fold_left (fun (fields, index) a ->
            let a = lenv_lookup_atom lenv pos a in
            let fields = (index, a) :: fields in
            let index = index + sizeof_pointer in
               fields, index) ([], 0) args
   in
   let fields = List.rev fields in
      { block_mutable = true;
        block_length = VarInvariant (ETConst (AtomInt length));
        block_default = VarUnknown;
        block_int_fields = fields;
        block_var_fields = SymbolTable.empty
      }

(*
 * Allocate an array.
 *)
and fun_alloc_array lenv pos a_size a_entry =
   let pos = string_pos "fun_alloc_array" pos in
   let length = lenv_lookup_atom lenv pos a_size in
   let default = lenv_lookup_atom lenv pos a_entry in
      { block_mutable = true;
        block_length = length;
        block_default = default;
        block_int_fields = [];
        block_var_fields = SymbolTable.empty
      }

(*
 * Malloc.
 *)
and fun_malloc lenv pos a_size =
   let pos = string_pos "fun_malloc" pos in
   let length = lenv_lookup_atom lenv pos a_size in
      { block_mutable = true;
        block_length = length;
        block_default = VarUnknown;
        block_int_fields = [];
        block_var_fields = SymbolTable.empty
      }

(*
 * Fetch a value from an array.
 *)
and fun_subscript_exp lenv aenv pred pos op v1 v2 a3 e =
   let pos = string_pos "fun_subscript_exp" pos in
   let a2 = lenv_lookup_var lenv pos v2 in
   let a3 = lenv_lookup_atom lenv pos a3 in
   let s = aenv_set_of aenv a2 in
   let ic =
      match a3 with
         VarInvariant e ->
            alias_subscript aenv pos op s e
       | _ ->
            VarUnknown
  in
  let lenv = lenv_add_var lenv v1 ic in
     fun_exp lenv aenv pred e

(*
 * Store a value in an array.
 *)
and fun_set_subscript_exp lenv aenv pred pos op v1 a2 a3 e =
   let pos = string_pos "fun_set_subscript_exp" pos in
   let a1 = lenv_lookup_var lenv pos v1 in
   let a2 = lenv_lookup_atom lenv pos a2 in
   let a3 = lenv_lookup_atom lenv pos a3 in
   let aenv =
      match a1, a2 with
         VarMustAlias v, VarInvariant e ->
            alias_must_set_subscript aenv pos op v e a3
       | VarMayAlias s, VarInvariant e ->
            alias_may_set_subscript aenv pos op s e a3
       | VarMustAlias v, _ ->
            alias_overwrite aenv pos (SymbolSet.singleton v)
       | VarMayAlias s, _ ->
            alias_overwrite aenv pos s
       | _ ->
            raise (FirException (pos, StringVarError ("not an array", v1)))
   in
      fun_exp lenv aenv pred e

(*
 * Copy values from one array to another.
 *)
and fun_memcpy_exp lenv aenv pred pos op v1 a2 v3 a4 a5 e =
   let pos = string_pos "fun_memcpy_exp" pos in
   let a1 = lenv_lookup_var lenv pos v1 in
   let a2 = lenv_lookup_atom lenv pos a2 in
   let a3 = lenv_lookup_var lenv pos v3 in
   let a4 = lenv_lookup_atom lenv pos a4 in
   let a5 = lenv_lookup_atom lenv pos a5 in
   let s1 = aenv_set_of aenv a1 in
   let s2 = aenv_set_of aenv a3 in
   let aenv =
      match a2, a4, a5 with
         VarInvariant e1, VarInvariant e2, VarInvariant e3 ->
            alias_memcpy aenv pos op s1 e1 s2 e2 e3
       | _ ->
            alias_overwrite aenv pos s1
   in
      fun_exp lenv aenv pred e

(*
 * Handle an assertion.
 *)
and fun_assert_exp lenv aenv pred pos pred' e =
   let pos = string_pos "fun_assert_exp" pos in
      fun_exp lenv aenv pred e

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and fun_tailcall_exp lenv aenv pred pos f args =
   let pos = string_pos "fun_tailcall_exp" pos in
   let args = List.map (lenv_lookup_atom lenv pos) args in
   let call =
      { call_changed = true;
        call_args = args;
        call_pred = pred;
        call_aenv = aenv
      }
   in
      lenv_call lenv f call

and fun_call_exp lenv aenv pred pos f args e =
   let pos = string_pos "fun_call_exp" pos in
   let args =
      List.map (fun a ->
            match a with
               Some a ->
                  lenv_lookup_atom lenv pos a
             | None ->
                  VarUnknown) args
   in
   let call =
      { call_changed = true;
        call_args = args;
        call_pred = pred;
        call_aenv = aenv
      }
   in
   let lenv = lenv_call lenv f call in
      fun_exp lenv aenv pred e

(*
 * The final fun analysis.  Analyze the function
 * for the current call's arguments.
 *)
let fun_call lenv info call =
   let { fun_vars = vars;
         fun_body = body
       } = info
   in
   let { call_args = args;
         call_aenv = aenv;
         call_pred = pred
       } = call
   in

   (* Bind the vars to the args *)
   let venv = List.fold_left2 SymbolTable.add lenv.lenv_vars vars args in
   let lenv = { lenv with lenv_vars = venv } in
      fun_exp lenv aenv pred body

(************************************************************************
 * ENVIRONMENT ABSTRACTION
 ************************************************************************)

(*
 * Make alias and arg info abstract.
 * We replace any invariant and unknown vars with
 * new invariant vars, and we construct a substitution
 * so that the old information can be reconstructed.
 *)
let rec abstract_arg subst e =
   match e with
      VarInvariant _
    | VarUnknown ->
         let v = new_symbol_string "abstract_var" in
         let subst = SymbolTable.add subst v e in
            subst, VarInvariant (ETVar v)
    | VarMustAlias _
    | VarMayAlias _ ->
         subst, e

and abstract_block subst block =
   let { block_length = length;
         block_int_fields = int_fields;
         block_var_fields = var_fields
       } = block
   in
   let subst, length = abstract_arg subst length in
   let subst, int_fields =
      List.fold_left (fun (subst, fields) (i, arg) ->
            let subst, arg = abstract_arg subst arg in
            let fields = (i, arg) :: fields in
               subst, fields) (subst, []) int_fields
   in
   let subst, var_fields =
      SymbolTable.fold_map (fun subst v arg ->
            abstract_arg subst arg) subst var_fields
   in
   let block =
      { block with block_length = length;
                   block_int_fields = List.rev int_fields;
                   block_var_fields = var_fields
      }
   in
      subst, block

(*
 * Make the arguments and the alias environment abstract.
 *)
let abstract_args aenv args =
   let { aenv_blocks = blocks } = aenv in
   let subst, args =
      List.fold_left (fun (subst, args) a ->
            let subst, a = abstract_arg subst a in
            let args = a :: args in
               subst, args) (SymbolTable.empty, []) args
   in
   let subst, blocks =
      SymbolTable.fold_map (fun subst v block ->
            abstract_block subst block) subst blocks
   in
   let aenv = { aenv with aenv_blocks = blocks } in
      subst, aenv, List.rev args

(************************************************************************
 * LOOP SELF-UNIFICATION
 ************************************************************************)

(*
 * In some cases, we just don't know the new value
 * for a variable on exit from the loop.  The variable
 * becomes unknown.
 *)
let unify_self_unknown subst ic =
   match ic with
      VarInvariant (ETVar v) ->
         SymbolTable.add subst v VarUnknown
    | _ ->
         subst

(*
 * Unify the alias environment in a loop call.
 * Any variable that has changed gets converted to
 * VarUnknown.
 *)
let unify_self_arg subst ic1 ic2 =
   match ic1 with
      VarInvariant (ETVar v1) ->
         (match ic2 with
             VarInvariant (ETVar v2)
             when Symbol.eq v1 v2 ->
                subst
           | _ ->
                SymbolTable.add subst v1 VarUnknown)
    | _ ->
         subst

(*
 * When unifying self fields, any field that got omitted
 * is now considered to be unknown.  Otherwise, we look for
 * exact matches, and do a normal argument conversion.
 *)
let rec unify_int_fields subst fields1 fields2 =
   match fields1, fields2 with
      field1 :: fields1', field2 :: fields2' ->
         let i1, ic1 = field1 in
         let i2, ic2 = field2 in
            if i1 = i2 then
               let subst = unify_self_arg subst ic1 ic2 in
                  unify_int_fields subst fields1' fields2'
            else if i1 < i2 then
               let subst = unify_self_unknown subst ic1 in
                  unify_int_fields subst fields1' fields2
            else
               unify_int_fields subst fields1 fields2'
    | _, [] ->
         List.fold_left (fun subst (_, ic) ->
               unify_self_unknown subst ic) subst fields1
    | [], _ ->
         subst

(*
 * Var fields are treated the same as int_fields, but
 * we look through the symbol table for exact matches.
 *)
let unify_var_fields subst fields1 fields2 =
   SymbolTable.fold (fun subst v ic1 ->
         try
            let ic2 = SymbolTable.find fields2 v in
               unify_self_arg subst ic1 ic2
         with
            Not_found ->
               unify_self_unknown subst ic1) subst fields1

(*
 * Walk through the block and consider each field.
 *)
let unify_self_block subst block1 block2 =
   let { block_int_fields = int_fields1;
         block_var_fields = var_fields1
       } = block1
   in
   let { block_int_fields = int_fields2;
         block_var_fields = var_fields2
       } = block2
   in
   let subst = unify_int_fields subst int_fields1 int_fields2 in
   let subst = unify_var_fields subst var_fields1 var_fields2 in
      subst

(*
 * Look through the alias environment
 * and connect all self args.
 *)
let unify_self_aenv subst aenv1 aenv2 =
   let { aenv_blocks = blocks1 } = aenv1 in
   let { aenv_blocks = blocks2 } = aenv2 in
      SymbolTable.fold (fun subst v block1 ->
            let block2 = SymbolTable.find blocks2 v in
               unify_self_block subst block1 block2) subst blocks1

(*
 * Unify the arguments to the loop.
 *)
let unify_self_args subst args1 args2 =
   List.fold_left2 unify_self_arg subst args1 args2

(************************************************************************
 * LOOP KNOTTING
 ************************************************************************)

(*
 * Once the loop computation is finished,
 * convert all variables that change in the loop
 * call to unknown variables.
 *)
let update_subst subst loop lenv aenv1 args1 =
   let { loop_name = name } = loop in
   let { lenv_calls = calls } = lenv in
   let { call_aenv = aenv2;
         call_args = args2
       } = SymbolTable.find calls name
   in
   let subst = unify_self_aenv subst aenv1 aenv2 in
   let subst = unify_self_args subst args1 args2 in
      subst

(*
 * Update a call to an outer function.
 * We just apply the subst to all the parts of the
 * call.
 *)
let update_ocall subst lenv f call =
   let { call_args = args;
         call_aenv = aenv;
         call_pred = pred
       } = call
   in
   let call =
      { call_changed = true;
        call_args = subst_args subst args;
        call_aenv = subst_aenv subst aenv;
        call_pred = subst_pred subst pred
      }
   in
      lenv_call lenv f call

(*
 * Finally, collect all of the loop exit calls.
 *    lenv1: the environment on entry to the loop
 *    lenv2: the local loop environment.
 *)
let update_lenv subst loop lenv1 lenv2 =
   let { loop_name = name;
         loop_funs = funs
       } = loop
   in
   let { lenv_calls = calls2 } = lenv2 in

   (* Collect all the calls in lenv2, and unify them with lenv1 *)
   let lenv, calls =
      SymbolTable.fold (fun (lenv, calls) f call ->
            if SymbolTable.mem funs f then
               (* This is an internal call, add it to internal calls *)
               let calls = SymbolTable.add calls f call in
                  lenv, calls
            else
               (* This is an external call, so unify it *)
               let lenv = update_ocall subst lenv f call in
                  lenv, calls) (lenv1, SymbolTable.empty) calls2
   in
      loop.loop_calls <- calls;
      lenv

(************************************************************************
 * LOOP ANALYSIS
 ************************************************************************)

(*
 * One step of the loop computation.
 *)
let rec loop_step loop lenv =
   let { loop_name = name;
         loop_funs = funs
       } = loop
   in
   let { lenv_calls = calls } = lenv in

   (* Reset the changed flags on all the calls *)
   let ucalls =
      SymbolTable.map (fun call ->
            { call with call_changed = false }) calls
   in
   let lenv = { lenv with lenv_calls = ucalls } in
      (* Update all the functions that have changed *)
      SymbolTable.fold (fun (lenv, changed) f call ->
            if Symbol.eq f name || not call.call_changed then
               (* Don't process unchanged or loop calls *)
               lenv, changed
            else
               try
                  let lenv =
                     match SymbolTable.find funs f with
                        LoopFun info ->
                           fun_call lenv info call
                      | LoopLoop info ->
                           loop_call lenv info call
                  in
                     lenv, true
               with
                  Not_found ->
                     lenv, changed) (lenv, false) calls

(*
 * Fixpoint calls the step function until there
 * are no changed.
 *)
and loop_fixpoint loop lenv =
   if debug debug_loop then
      begin
         let { loop_name = name } = loop in
            Format.printf "@[<v 3>*** FIR: Loop step: ";
            print_symbol name;
            Format.printf "@ Environment:@ ";
            print_lenv lenv;
            Format.printf "@]@."
      end;
   let lenv, changed = loop_step loop lenv in
      if changed then
         loop_fixpoint loop lenv
      else
         lenv

(*
 * The final loop analysis.  We do this in several stages.
 *    1. Abstract the arguments.  This means that we assign
 *       all vars, including array vars, to new VarInvariant
 *       values.
 *    2. Compute the loop fixpoint.
 *    3. Compute new outer calls.
 *)
and loop_call lenv1 loop call =
   let { loop_name = name;
         loop_funs = funs
       } = loop
   in
   let { call_args = args;
         call_aenv = aenv
       } = call
   in

   (* Abstract the arguments *)
   let subst, aenv, args = abstract_args aenv args in

   (* New loop environment *)
   let lenv2 =
      { lenv_vars = lenv1.lenv_vars;
        lenv_calls = SymbolTable.empty
      }
   in

   (* Call the loop function *)
   let pos = string_pos "loop_call" (vexp_pos name) in
   let info = lookup_fun funs pos name in
   let call =
      { call_changed = true;
        call_args = args;
        call_aenv = aenv;
        call_pred = pred_true
      }
   in
   let lenv2 = fun_call lenv2 info call in

   (* Compute the loop fixpoint *)
   let lenv2 = loop_fixpoint loop lenv2 in

   (* Update the substitution, converting induction variables *)
   let subst = update_subst subst loop lenv2 aenv args in
      (* Convert all calls to outer functions *)
      update_lenv subst loop lenv1 lenv2

(************************************************************************
 * PROGRAM CONSTRUCTION
 ************************************************************************)

(*
 * Build the fun element.
 *)
let build_fun funs f =
   try
      let _, _, vars, e = SymbolTable.find funs f in
         { fun_name = f;
           fun_vars = vars;
           fun_body = e
         }
   with
      Not_found ->
         let pos = string_pos "build_fun" (vexp_pos f) in
            raise (FirException (pos, UnboundVar f))

(*
 * Build the loop info.
 *)
let rec build_loop_trace funs f trace =
   let loops = build_trace funs SymbolTable.empty trace in
      { loop_name = f;
        loop_funs = loops;
        loop_calls = SymbolTable.empty
      }

(*
 * When performing loop calculations,
 * collect all the functions in the loop
 * trace as well as nested loops.
 *)
and build_trace funs loops trace =
   match trace with
      Elem f :: trace ->
         let info = build_fun funs f in
         let loops = SymbolTable.add loops f (LoopFun info) in
            build_trace funs loops trace
    | Trace ((Elem f :: _) as trace1) :: trace2 ->
         let info = build_loop_trace funs f trace1 in
         let loops = SymbolTable.add loops f (LoopLoop info) in
            build_trace funs loops trace2
    | Trace trace1 :: trace2 ->
         build_trace funs loops (trace1 @ trace2)
    | [] ->
         loops

(*
 * Build the program.
 * We pretend that the program is a loop,
 * with a special "root" node that calls all the
 * external functions.
 *)
let build_prog prog =
   (* Add extra calls *)
   let prog = flow_prog prog in
   let _ =
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>*** FIR: loop: after cps analysis@ ";
            print_prog prog;
            Format.printf "@]@."
         end
   in
   let cgraph = build_callgraph prog in

   (* Build the callgraph *)
   let { prog_funs = funs;
         prog_import = import;
         prog_globals = globals;
         prog_types = tenv
       } = prog
   in

   (* Initial alias environment has a single class for all pointers *)
   let v_alias = new_symbol_string "galias" in
   let a_class = VarMayAlias (SymbolSet.singleton v_alias) in
   let blocks = SymbolTable.add SymbolTable.empty v_alias block_default in
   let aenv = { aenv_var_empty with aenv_blocks = blocks } in

   (* Add all the calls from the root node *)
   let { call_root = root; call_nodes = nodes } = cgraph in
   let { fnode_name = v_root; fnode_out_calls = calls } = root in
   let calls =
      SymbolSet.fold (fun calls f ->
            let pos = string_pos "build_prog" (vexp_pos f) in
               try
                  let _, ty, vars, _ = SymbolTable.find funs f in
                  let _, ty_vars, _ = dest_fun_type tenv pos ty in
                  let args =
                     List.fold_left2 (fun args v ty ->
                           let arg =
                              if is_pointer_type tenv pos ty then
                                 a_class
                              else
                                 VarInvariant (ETVar v)
                           in
                              arg :: args) [] vars ty_vars
                  in
                  let call =
                     { call_changed = true;
                       call_pred = pred_true;
                       call_aenv = aenv;
                       call_args = List.rev args
                     }
                  in
                     SymbolTable.add calls f call
               with
                  Not_found ->
                     calls) SymbolTable.empty calls
   in

   (* Add values all globals to the environment *)
   let venv = SymbolTable.empty in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            SymbolTable.add venv v (VarInvariant (ETVar v))) venv funs
   in
   let venv =
      SymbolTable.fold (fun venv v { import_type = ty } ->
            let pos = string_pos "build_prog" (vexp_pos v) in
            let ic =
               if is_pointer_type tenv pos ty then
                  a_class
               else
                  VarInvariant (ETVar v)
            in
               SymbolTable.add venv v ic) venv import
   in
   let venv =
      SymbolTable.fold (fun venv v (ty, _) ->
            let pos = string_pos "build_prog" (vexp_pos v) in
            let ic =
               if is_pointer_type tenv pos ty then
                  a_class
               else
                  VarInvariant (ETVar v)
            in
               SymbolTable.add venv v ic) venv globals
   in
   let lenv =
      { lenv_vars = venv;
        lenv_calls = calls
      }
   in

   (* Add dummy functions for the root and the import functions *)
   let body = TailCall (v_root, []) in
   let info = "<alias>", 0 in
   let funs = SymbolTable.add funs v_root (info, TyFun ([], TyEnum 0), [], body) in
   let funs =
      SymbolTable.fold (fun funs v import ->
            match import with
               { import_info = ImportFun _; import_type = ty } ->
                  let pos = string_pos "build_prog" (vexp_pos v) in
                  let _, ty_vars, _ = dest_fun_type tenv pos ty in
                  let vars = List.map (fun _ -> new_symbol v) ty_vars in
                  let def = info, ty, vars, body in
                     SymbolTable.add funs v def
             | _ ->
                  funs) funs import
   in

   (* Compute the initial program info from the trace *)
   let trace = build_loop cgraph in
   let trace = Trace.map (fun { fnode_name = name } -> name) trace in
   let loops = build_trace funs SymbolTable.empty trace in
   let loop =
      { loop_name = v_root;
        loop_funs = loops;
        loop_calls = SymbolTable.empty
      }
   in
   let _ =
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>*** FIR: before fixpoint@ @[<hv 3>Program:@ ";
            print_loop_info loop;
            Format.printf "@]@ @[<v 3>Environment:@ ";
            print_lenv lenv;
            Format.printf "@]@]@."
         end
   in

   (* Compute the loop fixpoint *)
   let lenv = loop_fixpoint loop lenv in
   let _ =
      if debug debug_loop then
         begin
            Format.printf "@[<v 3>*** FIR: after fixpoint@ @[<hv 3>Program:@ ";
            print_loop_info loop;
            Format.printf "@]@ @[<v 3>Environment:@ ";
            print_lenv lenv;
            Format.printf "@]@]@."
         end
   in
      lenv

(************************************************************************
 * ALIAS ANALYSIS
 ************************************************************************)

(*
 * Alias environment.
 *)
let venv_add_var venv v a aenv pred =
   let info =
      { alias_value = a;
        alias_env = aenv;
        alias_pred = pred
      }
   in
      SymbolTable.add venv v info

let venv_lookup_var venv pos v =
   try (SymbolTable.find venv v).alias_value with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

let venv_lookup_atom venv pos a =
   match a with
      AtomVar v ->
         venv_lookup_var venv pos v
    | _ ->
         AliasInvariant (ETConst a)

(*
 * Integers from operators.
 *)
let etree_of_int op i =
   match op with
      IntClass ->
         ETConst (AtomInt i)
    | RawIntClass (pre, signed) ->
         ETConst (AtomRawInt (Rawint.of_int pre signed i))

let negate_etree op e =
   let op =
      match op with
         IntClass ->
            UMinusIntOp
       | RawIntClass (pre, signed) ->
            UMinusRawIntOp (pre, signed)
   in
      canonicalize_etree (ETUnop (op, e))

(*
 * Add a predicate assignment.
 *)
let alias_pred_op pred v a =
   match a with
      AliasInvariant e ->
         pred_assign pred v e
    | AliasInduction _
    | AliasDerived _
    | AliasMustAlias _
    | AliasMayAlias _
    | AliasUnknown ->
         pred

(*
 * Scan a function in the loop.
 *)
let rec alias_exp venv aenv pred e =
   let pos = string_pos "alias_exp" (exp_pos e) in
      match e with
         LetUnop (v, _, op, a, e) ->
            alias_unop_exp venv aenv pred pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            alias_binop_exp venv aenv pred pos v op a1 a2 e
       | TailCall (f, args) ->
            alias_tailcall_exp venv aenv pred pos f args
       | Call (f, args, e) ->
            alias_call_exp venv aenv pred pos f args e
       | Match (a, cases) ->
            alias_match_exp venv aenv pred pos a cases
       | TypeCase (_, _, _, v, e1, e2) ->
            alias_typecase_exp venv aenv pred pos v e1 e2
       | LetExt (v, _, _, _, args, e) ->
            alias_ext_exp venv aenv pred pos v args e
       | LetAlloc (v, op, e) ->
            alias_alloc_exp venv aenv pred pos v op e
       | LetSubscript (op, v1, _, v2, a3, e) ->
            alias_subscript_exp venv aenv pred pos op v1 v2 a3 e
       | SetSubscript (op, v1, a2, _, a3, e) ->
            alias_set_subscript_exp venv aenv pred pos op v1 a2 a3 e
       | Memcpy (op, v1, a1, v2, a2, a3, e) ->
            alias_memcpy_exp venv aenv pred pos op v1 a1 v2 a2 a3 e
       | Assert (_, pred', e) ->
            alias_assert_exp venv aenv pred pos pred' e
       | SetGlobal (_, _, _, _, e)
       | Debug (_, e) ->
            alias_exp venv aenv pred e
       | SpecialCall _ ->
            venv

(*
 * Catch simple unops.
 *)
and alias_unop_exp venv aenv pred pos v op a e =
   let pos = string_pos "alias_unop_exp" pos in
   let a = venv_lookup_atom venv pos a in
   let a =
      match op, a with
         IdOp, _ ->
            a
       | _, AliasInvariant a ->
            AliasInvariant (canonicalize_etree (ETUnop (op, a)))
       | UMinusIntOp, AliasInduction (v, op, e)
       | UMinusRawIntOp _, AliasInduction (v, op, e) ->
            AliasDerived (v, op, etree_of_int op (-1), etree_of_int op 0)
       | UMinusIntOp, AliasDerived (v, op, e1, e2)
       | UMinusRawIntOp _, AliasDerived (v, op, e1, e2) ->
            AliasDerived (v, op, negate_etree op e1, negate_etree op e2)
       | _ ->
            AliasUnknown
   in
   let pred = alias_pred_op pred v a in
   let venv = venv_add_var venv v a aenv pred in
      alias_exp venv aenv pred e

(*
 * Binary operations.
 *)
and alias_binop_exp venv aenv pred pos v op a1 a2 e =
   let pos = string_pos "alias_binop_exp" pos in
   let a1 = venv_lookup_atom venv pos a1 in
   let a2 = venv_lookup_atom venv pos a2 in
   let a =
      match op, a1, a2 with
         _, AliasInvariant a1, AliasInvariant a2 ->
            AliasInvariant (canonicalize_etree (ETBinop (op, a1, a2)))
       | _ ->
            AliasUnknown
   in
   let pred = alias_pred_op pred v a in
   let venv = venv_add_var venv v a aenv pred in
      alias_exp venv aenv pred e

(*
 * Match expression.
 *)
and alias_match_exp venv aenv pred pos a cases =
   let pos = string_pos "alias_match_exp" pos in
      List.fold_left (fun venv (s, e) ->
            let pred = pred_match pred a s in
               alias_exp venv aenv pred e) venv cases

(*
 * Typecase.
 *)
and alias_typecase_exp venv aenv pred pos v e1 e2 =
   let pos = string_pos "alias_typecase_exp" pos in
   let venv = venv_add_var venv v AliasUnknown aenv pred in
   let venv = alias_exp venv aenv pred e1 in
   let venv = alias_exp venv aenv pred e2 in
      venv

(*
 * External call.
 * We assume the external call obliterates its pointer arguments.
 *)
and alias_ext_exp venv aenv pred pos v args e =
   let pos = string_pos "alias_ext_exp" pos in
   let venv = venv_add_var venv v AliasUnknown aenv pred in
   let args = List.map (venv_lookup_atom venv pos) args in
   let aenv = alias_overwrite_args aenv pos args in
      alias_exp venv aenv pred e

(*
 * An allocation.
 *)
and alias_alloc_exp venv aenv pred pos v op e =
   let pos = string_pos "alias_alloc_exp" pos in
   let block =
      match op with
         AllocTuple (_, _, args)
       | AllocUnion (_, _, _, args)
       | AllocArray (_, args) ->
            alias_alloc_tuple venv pos v args
       | AllocVArray (_, _, a1, a2) ->
            alias_alloc_array venv pos a1 a2
       | AllocMalloc a ->
            alias_malloc venv pos a
   in
   let aenv = aenv_add_block aenv v block in
   let venv = venv_add_var venv v (AliasMustAlias v) aenv pred in
      alias_exp venv aenv pred e

(*
 * Allocate a tuple.
 *)
and alias_alloc_tuple venv pos v args =
   let pos = string_pos "alias_alloc_tuple" pos in
   let fields, length =
      List.fold_left (fun (fields, index) a ->
            let a = venv_lookup_atom venv pos a in
            let fields = (index, a) :: fields in
            let index = index + sizeof_pointer in
               fields, index) ([], 0) args
   in
   let fields = List.rev fields in
      { block_mutable = true;
        block_length = AliasInvariant (ETConst (AtomInt length));
        block_default = AliasUnknown;
        block_int_fields = fields;
        block_var_fields = SymbolTable.empty
      }

(*
 * Allocate an array.
 *)
and alias_alloc_array venv pos a_size a_entry =
   let pos = string_pos "alias_alloc_array" pos in
   let length = venv_lookup_atom venv pos a_size in
   let default = venv_lookup_atom venv pos a_entry in
      { block_mutable = true;
        block_length = length;
        block_default = default;
        block_int_fields = [];
        block_var_fields = SymbolTable.empty
      }

(*
 * Malloc.
 *)
and alias_malloc venv pos a_size =
   let pos = string_pos "alias_malloc" pos in
   let length = venv_lookup_atom venv pos a_size in
      { block_mutable = true;
        block_length = length;
        block_default = AliasUnknown;
        block_int_fields = [];
        block_var_fields = SymbolTable.empty
      }

(*
 * Fetch a value from an array.
 *)
and alias_subscript_exp venv aenv pred pos op v1 v2 a3 e =
   let pos = string_pos "alias_subscript_exp" pos in
   let a2 = venv_lookup_var venv pos v2 in
   let a3 = venv_lookup_atom venv pos a3 in
   let s = aenv_set_of aenv a2 in
   let ic =
      match a3 with
         AliasInvariant e ->
            alias_subscript aenv pos op s e
       | _ ->
            AliasUnknown
  in
  let venv = venv_add_var venv v1 ic in
     alias_exp venv aenv pred e

(*
 * Store a value in an array.
 *)
and alias_set_subscript_exp venv aenv pred pos op v1 a2 a3 e =
   let pos = string_pos "alias_set_subscript_exp" pos in
   let a1 = venv_lookup_var venv pos v1 in
   let a2 = venv_lookup_atom venv pos a2 in
   let a3 = venv_lookup_atom venv pos a3 in
   let aenv =
      match a1, a2 with
         AliasMustAlias v, AliasInvariant e ->
            alias_must_set_subscript aenv pos op v e a3
       | AliasMayAlias s, AliasInvariant e ->
            alias_may_set_subscript aenv pos op s e a3
       | AliasMustAlias v, _ ->
            alias_overwrite aenv pos (SymbolSet.singleton v)
       | AliasMayAlias s, _ ->
            alias_overwrite aenv pos s
       | _ ->
            raise (FirException (pos, StringVarError ("not an array", v1)))
   in
      alias_exp venv aenv pred e

(*
 * Copy values from one array to another.
 *)
and alias_memcpy_exp venv aenv pred pos op v1 a2 v3 a4 a5 e =
   let pos = string_pos "alias_memcpy_exp" pos in
   let a1 = venv_lookup_var venv pos v1 in
   let a2 = venv_lookup_atom venv pos a2 in
   let a3 = venv_lookup_var venv pos v3 in
   let a4 = venv_lookup_atom venv pos a4 in
   let a5 = venv_lookup_atom venv pos a5 in
   let s1 = aenv_set_of aenv a1 in
   let s2 = aenv_set_of aenv a3 in
   let aenv =
      match a2, a4, a5 with
         AliasInvariant e1, AliasInvariant e2, AliasInvariant e3 ->
            alias_memcpy aenv pos op s1 e1 s2 e2 e3
       | _ ->
            alias_overwrite aenv pos s1
   in
      alias_exp venv aenv pred e

(*
 * Handle an assertion.
 *)
and alias_assert_exp venv aenv pred pos pred' e =
   let pos = string_pos "alias_assert_exp" pos in
      alias_exp venv aenv pred e

(*
 * Tailcalls.
 * Match the values with the function's parameters.
 *)
and alias_tailcall_exp venv aenv pred pos f args =
   let pos = string_pos "alias_tailcall_exp" pos in
   let args = List.map (venv_lookup_atom venv pos) args in
   let call =
      { call_changed = true;
        call_args = args;
        call_pred = pred;
        call_aenv = aenv
      }
   in
      venv_call venv f call

and alias_call_exp venv aenv pred pos f args e =
   let pos = string_pos "alias_call_exp" pos in
   let args =
      List.map (fun a ->
            match a with
               Some a ->
                  venv_lookup_atom venv pos a
             | None ->
                  VarUnknown) args
   in
   let call =
      { call_changed = true;
        call_args = args;
        call_pred = pred;
        call_aenv = aenv
      }
   in
   let venv = venv_call venv f call in
      alias_exp venv aenv pred e

(*
 * Perform an alias analysis.
 *)
let alias_prog prog =
   let venv = build_prog prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
