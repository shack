(* Fir toplevel hoisting
 * Geoffrey Irving
 * $Id: fir_tophoist.ml,v 1.1 2002/06/03 19:39:22 justins Exp $ *)

(* Notes:
 *   1. This code finds constant allocated blocks that are never modified
 *      and lifts them up to toplevel as statically allocated globals.
 *   2. The code operates in two steps.  First, we generate a list of variables
 *      whose contains are (possibly) modified directly, either through
 *      SetSubscript, Memcpy, or LetExt.  When we find a constant block, we
 *      use check if any modified variables are aliased to it, and lift it
 *      out otherwise.
 *   3. The gs contains only global variables (globals and funs).
 *)

open Symbol
open Fir
open Fir_util
open Fir_alias

(************************************** compute the list of directly modified variables *)

(* BUG: safe for now, but needs to be changed at some point *)
let non_modifying _ = true

let ms_atom ms = function
    AtomVar v -> SymbolSet.add ms v
  | _ -> ms

let rec ms_expr ms e =
  match e with
      LetUnop (_, _, _, _, e)
    | LetBinop (_, _, _, _, _, e)
    | LetAlloc (_, _, e)
    | LetSubscript (_, _, _, _, _, e)
    | Debug (_, e)
    | SetGlobal (_, _, _, _, e)
    | LetAssert (_, _, _, _, e) ->
        ms_expr ms e
    | LetExt (_, _, s, _, al, e) ->
        if non_modifying s then
          ms_expr ms e
        else
          ms_expr (List.fold_left ms_atom ms al) e
    | TailCall _
    | SpecialCall _ ->
        ms
    | Match (_, sel) ->
        List.fold_left (fun ms (_, e) -> ms_expr ms e) ms sel
    | TypeCase (_, _, _, _, e1, e2) ->
        ms_expr (ms_expr ms e1) e2
    | SetSubscript (_, v, _, _, _, e)
    | Memcpy (_, v, _, _, _, _, e) ->
        ms_expr (SymbolSet.add ms v) e

let ms_funs funs =
  let ms = SymbolTable.fold (fun ms _ (_, _, _, e) -> ms_expr ms e) SymbolSet.empty funs in
    SymbolSet.to_list ms

let modified alias ms v =
  List.exists (may_alias_box alias v) ms

(************************************** hoist stuff out of expressions *)

let constant_atom gs = function
    AtomVar v when not (SymbolSet.mem gs v) -> false
  | _ -> true

let constant_aop gs = function
    AllocTuple (_, _, al) | AllocArray (_, al) | AllocUnion (_, _, _, al) ->
      List.for_all (constant_atom gs) al
  | AllocMArray (_, al, a) ->
      List.for_all (constant_atom gs) (a :: al)
  | AllocMalloc _ -> false

let rec tophoist_expr clean gs globals e =
  match e with
      LetAlloc (v, aop, e) when constant_aop gs aop && clean v ->
        let aop = map_type_alloc_op (fun _ -> TyDelayed) aop in
        let gs = SymbolSet.add gs v in
        let globals = SymbolTable.add globals v (TyDelayed, InitAlloc aop) in
          tophoist_expr clean gs globals e
    | LetUnop (v, ty, op, a, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, LetUnop (v, ty, op, a, e)
    | LetBinop (v, ty, op, a1, a2, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, LetBinop (v, ty, op, a1, a2, e)
    | LetExt (v, ty, s, ty2, al, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, LetExt (v, ty, s, ty2, al, e)
    | TailCall _
    | SpecialCall _ ->
        globals, e
    | Match (a, sel) ->
        let globals, sel = Mc_list_util.fold_map (fun globals (s, e) ->
            let globals, e = tophoist_expr clean gs globals e in
              globals, (s, e)) globals sel in
          globals, Match (a, sel)
    | TypeCase (a1, a2, name, v, e1, e2) ->
         let globals, e1 = tophoist_expr clean gs globals e1 in
         let globals, e2 = tophoist_expr clean gs globals e2 in
            globals, TypeCase (a1, a2, name, v, e1, e2)
    | LetAlloc (v, aop, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, LetAlloc (v, aop, e)
    | LetSubscript (k, v, ty, v2, a, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, LetSubscript (k, v, ty, v2, a, e)
    | SetSubscript (k, v, a1, ty, a2, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, SetSubscript (k, v, a1, ty, a2, e)
    | SetGlobal (k, v, ty, a, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, SetGlobal (k, v, ty, a, e)
    | Memcpy (k, v1, a1, v2, a2, a3, e) ->
        let globals, e = tophoist_expr clean gs globals e in
           globals, Memcpy (k, v1, a1, v2, a2, a3, e)
    | LetAssert (v1, ty, v2, pred, e) ->
         let globals, e = tophoist_expr clean gs globals e in
            globals, LetAssert (v1, ty, v2, pred, e)
    | Debug (di, e) ->
        let globals, e = tophoist_expr clean gs globals e in
          globals, Debug (di, e)

(************************************** global functions *)

let tophoist_funs clean gs globals funs =
  SymbolTable.fold_map (fun globals f (dl, ty, vl, e) ->
      let globals, e = tophoist_expr clean gs globals e in
        globals, (dl, ty, vl, e))
    globals funs

let init_gs gs st =
  SymbolTable.fold (fun gs v _ -> SymbolSet.add gs v) gs st

let tophoist_prog alias ({ prog_globals = globals; prog_funs = funs } as prog) =
  let ms = ms_funs funs in
  let clean v = not (modified alias ms v) in
  let gs = init_gs (init_gs SymbolSet.empty funs) globals in
  let globals, funs = tophoist_funs clean gs globals funs in
    { prog with prog_globals = globals; prog_funs = funs }

