(* Fir Alias Analysis
 * Geoffrey Irving
 * $Id: fir_alias.mli,v 1.2 2002/04/18 03:48:22 jyh Exp $ *)

open Fir

type alias_info

val alias_prog : prog -> alias_info

val may_alias_box : alias_info -> var -> var -> bool
val may_alias_fun : alias_info -> int -> var -> var -> bool

(* debugging *)
val debug_alias : string -> prog -> unit
