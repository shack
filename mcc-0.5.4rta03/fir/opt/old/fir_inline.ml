(* Fir inlining, constant propogation, and common subexpression elimination
 * Geoffrey Irving
 * $Id: fir_inline.ml,v 1.4 2002/08/06 03:16:12 jyh Exp $ *)

(* Notes:
 *   1. The main data structures are the st and the venv.  The st
 *      is a var -> atom table giving a valid atom representing
 *      a var.  Since inlining deletes unnecessary copying, all
 *      variables must be looked up in the st before any other use.
 *      The venv is a var -> data table describing extra information
 *      known about certain vars.
 *   2. This code currently ignores AggrSubscript operations (since
 *      the fc front end is in flux.  The same goes for LetSubCheck.
 *   3. The algorithm accepts as input a var -> var -> bool function
 *      called alias, which provides may_alias information.
 *   4. The algorithm also uses requires a callgraph, and passes
 *      around a set of globals, gs, for use in inlining heuristics.
 *      The callgraph is somewhat updated during inlining, but this
 *      is a temporary fix and details will change eventually.
 *   5. This code also does simple common subexpression elimination.
 *      Why, you ask, did I write all this stuff into the same file?
 *   6. Because I can.
 *)
open Format

open Debug
open Symbol
open Fir
open Fir_set
open Fir_exn
open Fir_util
open Fir_standardize
open Fir_algebra
open Fir_extspec
open Fir_alias

module Callgraph = Fir_callgraph   (* Fir_callgraph.cons_has_problems *)

module IntTable = Mc_map.McMake (struct
  type t = int
  let compare = (-)
end)

let block_poly_sub = BlockSub, PolySub

(************************************** environment handling code *)

type data =
    DataBox of int option * atom IntTable.t
  | DataFun of int * var list * exp
  | DataETree of etree

type st = atom SymbolTable.t
type cs = atom ETreeTable.t
type venv = data SymbolTable.t

let new_box venv v i al =
  let rec dt_of_list dt i al =
    match al with
        [] -> dt
      | a :: al ->
          dt_of_list (IntTable.add dt i a) (i+1) al in
  SymbolTable.add venv v (DataBox (Some i, dt_of_list IntTable.empty 0 al))

let inline_atom st a =
  match a with
      AtomVar v ->
        (try SymbolTable.find st v
        with Not_found -> a)
    | _ -> a

let inline_atoms st = List.map (inline_atom st)

let inline_var st v =
  try
    match SymbolTable.find st v with
        AtomVar v -> v
      | _ -> v
  with Not_found ->
        v

let inline_pred st pred =
   match pred with
      PredNop (v, op) ->
         PredNop (inline_var st v, op)
    | PredUnop (v, op, index, a) ->
         PredUnop (inline_var st v, op, index, inline_atom st a)
    | PredBinop (v, op, index, a1, a2) ->
         PredBinop (inline_var st v, op, index, inline_atom st a1, inline_atom st a2)

let etree_of_atom venv a =
  match a with
      AtomVar v ->
        (try
          match SymbolTable.find venv v with
              DataETree t -> t
            | _ -> ETVar v
        with Not_found -> ETVar v)
    | _ -> ETConst a

let cse_find cs t =
  match t with
      ETConst a -> Some a
    | ETVar v -> Some (AtomVar v)
    | _ ->
        try
          Some (ETreeTable.find cs t)
        with Not_found ->
          None

(************************************** sweep - drop corrupted information due to assignments *)

(* i < 0 means i unknown *)
let sweep alias venv v i =
  SymbolTable.mapi (fun v' d ->
    if may_alias_box alias v v' then
      match d with
          DataFun _
        | DataETree _ -> d
        | DataBox (n, dt) ->
            DataBox (n,
              if i < 0 then
                IntTable.empty
              else
                IntTable.remove dt i)
    else
      d) venv

(************************************** function inlining heuristics *)

let expr_complexity e =
  let rec ec c = function
      LetUnop (_, _, _, _, e) -> ec (c + 1) e
    | LetBinop (_, _, _, _, _, e) -> ec (c + 1) e
    | LetExt (_, _, _, _, _, e) -> ec (c + 2) e
    | TailCall _ -> c + 1
    | SpecialCall _ -> c + 1
    | Match (_, sel) -> List.fold_left (fun c (_, e) -> ec c e) (c + 3) sel
    | TypeCase (_, _, _, _, e1, e2) -> ec (ec (c + 1) e1) e2
    | LetAlloc (_, _, e)
    | LetSubscript (_, _, _, _, _, e)
    | LetAssert (_, _, _, _, e) -> ec (c + 1) e
    | SetSubscript (_, _, _, _, _, e) -> ec (c + 2) e
    | SetGlobal (_, _, _, _, e) -> ec (c + 1) e
    | Memcpy (_, _, _, _, _, _, e) -> ec (c + 10) e
    | Debug (_, e) -> ec c e
  in
    ec 0 e

let count_constants gs al =
  let rec cc n al =
    match al with
        [] -> n
      | AtomVar v :: al when SymbolSet.mem gs v -> cc (n+1) al
      | AtomVar _ :: al -> cc n al
      | _ :: al -> cc (n+1) al
  in
    cc 0 al

(************************************** expression inlining *)

let rec inline_expr alias gs venv st cs e =
  match e with
      LetUnop (v, ty, op, a, e) ->
        let a = inline_atom st a in
        let t = ETUnop (op, etree_of_atom venv a) in
        let t = canonicalize_etree t in
          (match cse_find cs t with
              None ->
                let venv = SymbolTable.add venv v (DataETree t) in
                let cs = ETreeTable.add cs t (AtomVar v) in
                  LetUnop (v, ty, op, a, inline_expr alias gs venv st cs e)
            | Some a ->
                let st = SymbolTable.add st v a in
                  inline_expr alias gs venv st cs e)
    | LetBinop (v, ty, op, a1, a2, e) ->
        let a1 = inline_atom st a1 in
        let a2 = inline_atom st a2 in
        let t = ETBinop (op, etree_of_atom venv a1, etree_of_atom venv a2) in
        let t = canonicalize_etree t in
          (match cse_find cs t with
              None ->
                let venv = SymbolTable.add venv v (DataETree t) in
                let cs = ETreeTable.add cs t (AtomVar v) in
                  LetBinop (v, ty, op, a1, a2, inline_expr alias gs venv st cs e)
            | Some a ->
                let st = SymbolTable.add st v a in
                  inline_expr alias gs venv st cs e)
    | LetExt (v, ty, s, ty2, al, e) ->
        let al = inline_atoms st al in
          LetExt (v, ty, s, ty2, al, inline_expr alias gs venv st cs e)
    | TailCall (f, al) ->
        let f = inline_var st f in
        let al = inline_atoms st al in
          (try
            match SymbolTable.find venv f with
                DataFun (h, vl, e) ->
                  let n = List.length vl in
                  let c = count_constants gs al in
                    if h * (n - c) / n > 10 then
                      raise Not_found
                    else
                      let st = List.fold_left2 SymbolTable.add st vl al in
                      let e = standardize_expr e in
                        inline_expr alias gs venv st cs e
              | _ -> raise Not_found
          with Not_found ->
            TailCall (f, al))
    | SpecialCall (TailSysMigrate (label, aptr, aoff, f, args)) ->
        let f = inline_var st f in
        let aptr = inline_atom st aptr in
        let aoff = inline_atom st aoff in
        let args = inline_atoms st args in
          SpecialCall (TailSysMigrate (label, aptr, aoff, f, args))
    | SpecialCall _ ->
        raise (FirException (ExnExp e, NotImplemented "Fir_inline: SpecialCall not implemented"))
    | Match (a, pl) ->
        let rec branch t = function
            [] ->
              raise (FirException (ExnExp e, StringError "incomplete match in inlining"))
          | (s, e) :: pl ->
              if t s then
                inline_expr alias gs venv st cs e
              else
                branch t pl in
        (match inline_atom st a with
            AtomInt i | AtomEnum (_, i) | AtomConst (_, _, i) ->
              branch (function IntSet s -> IntSet.mem_point i s | _ -> false) pl
          | AtomRawInt i ->
              branch (function RawIntSet s -> RawIntSet.mem_point i s | _ -> false) pl
          | AtomVar v as a ->
              (try
                match SymbolTable.find venv v with
                    DataBox (Some i, _) ->
                      branch (function IntSet s -> IntSet.mem_point i s | _ -> false) pl
                  | _ -> raise Not_found
              with Not_found ->
                Match (a, List.map (fun (s, e) -> s, inline_expr alias gs venv st cs e) pl))
          | _ ->
              raise (FirException (ExnExp e, StringError "tried to match on weird atom")))
    | TypeCase (a1, a2, name, v, e1, e2) ->
         let e1 = inline_expr alias gs venv st cs e1 in
         let e2 = inline_expr alias gs venv st cs e2 in
         let a1 = inline_atom st a1 in
         let a2 = inline_atom st a2 in
            TypeCase (a1, a2, name, v, e1, e2)
    | LetAlloc (v, op, e) ->
        let venv, op = match op with
            AllocTuple (tclass, ty, al) ->
              let al = inline_atoms st al in
                new_box venv v 0 al,
                AllocTuple (tclass, ty, al)
          | AllocArray (ty, al) ->
              let al = inline_atoms st al in
                new_box venv v 0 al,
                AllocArray (ty, al)
          | AllocMArray (ty, al, a) ->
              let al = inline_atoms st al in
              let a = inline_atom st a in
                (* BUG: we could form a box *)
                venv, AllocMArray (ty, al, a)
          | AllocUnion (ty, tv, i, al) ->
              let al = inline_atoms st al in
                new_box venv v i al,
                AllocUnion (ty, tv, i, al)
          | AllocMalloc a ->
              venv, AllocMalloc (inline_atom st a)
        in
          LetAlloc (v, op, inline_expr alias gs venv st cs e)
    | LetSubscript ((BlockSub, PolySub), v, ty, v2, a, e) ->
        let a = inline_atom st a in
        let v2 = inline_var st v2 in
        (match a with
            AtomInt i ->
              (try
                 match SymbolTable.find venv v2 with
                     DataBox (io, dt) ->
                       (try
                         let st = SymbolTable.add st v (IntTable.find dt i) in
                           inline_expr alias gs venv st cs e
                       with Not_found ->
                         let dt = IntTable.add dt i (AtomVar v) in
                         let venv = SymbolTable.add venv v2 (DataBox (io, dt)) in
                           LetSubscript (block_poly_sub, v,ty, v2, a, inline_expr alias gs venv st cs e))
                   | _ ->
                       raise Not_found
              with Not_found ->
                let dt = IntTable.add IntTable.empty i (AtomVar v) in
                let venv = SymbolTable.add venv v2 (DataBox (None, dt)) in
                  LetSubscript (block_poly_sub, v, ty, v2, a, inline_expr alias gs venv st cs e))
          | _ ->
              LetSubscript (block_poly_sub, v, ty, v2, a, inline_expr alias gs venv st cs e))
    | LetSubscript (subop, v, ty, v2, a, e) ->
        let a = inline_atom st a in
        let v2 = inline_var st v2 in
          LetSubscript (subop, v, ty, v2, a, inline_expr alias gs venv st cs e)
    | SetSubscript ((BlockSub, PolySub), v, a1, ty, a2, e) ->
        let a1 = inline_atom st a1 in
        let a2 = inline_atom st a2 in
        let v = inline_var st v in
        let venv =
          try
            match a1, SymbolTable.find venv v with
                AtomInt i, DataBox (n, dt) ->
                  let venv = sweep alias venv v i in
                  let dt = IntTable.add dt i a2 in
                    SymbolTable.add venv v (DataBox (n, dt))
              | _, DataBox (_, _) ->
                  sweep alias venv v (-1)
              | _ -> raise Not_found
          with Not_found ->
            match a1 with
                AtomInt i ->
                  let venv = sweep alias venv v i in
                  let dt = IntTable.add IntTable.empty i a2 in
                    SymbolTable.add venv v (DataBox (None, dt))
              | _ ->
                  sweep alias venv v (-1)
        in
          SetSubscript (block_poly_sub, v, a1, ty, a2, inline_expr alias gs venv st cs e)
    | SetSubscript (subop, v, a1, ty, a2, e) ->
        let v = inline_var st v in
        let a1 = inline_atom st a1 in
        let a2 = inline_atom st a2 in
          SetSubscript (subop, v, a1, ty, a2, inline_expr alias gs venv st cs e)
    | SetGlobal (subop, v, ty, a, e) ->
        let v = inline_var st v in
        let a = inline_atom st a in
          SetGlobal (subop, v, ty, a, inline_expr alias gs venv st cs e)
    | Memcpy (op, v1, a1, v2, a2, a3, e) ->
        let v1 = inline_var st v1 in
        let a1 = inline_atom st a1 in
        let v2 = inline_var st v2 in
        let a2 = inline_atom st a2 in
        let a3 = inline_atom st a3 in
           Memcpy (op, v1, a1, v2, a2, a3, inline_expr alias gs venv st cs e)
    | LetAssert (v1, ty, v2, pred, e) ->
         let v2 = inline_var st v2 in
         let pred = inline_pred st pred in
         let e = inline_expr alias gs venv st cs e in
            LetAssert (v1, ty, v2, pred, e)
    | Debug (di, e) ->
        Debug (di, inline_expr alias gs venv st cs e)


(************************************** global inlining *)

let inline_globals tenv gs venv st cs globals =
  let st = SymbolTable.fold (fun st v (_, i) ->
      match i with
          InitAtom a ->
            let rec reduce a =
              match a with
                  AtomVar v -> (try reduce (SymbolTable.find st v) with Not_found -> a)
                | _ -> a in
            SymbolTable.add st v (reduce a)
        | _ -> st)
    st globals in
  let (venv, globals) = SymbolTable.fold (fun (venv, globals) v tyi ->
      match tyi with
          _, InitAtom a -> venv, globals
        | _, InitRawData _
        | _, InitNames _ ->
            venv, SymbolTable.add globals v tyi
        | ty, InitAlloc (AllocMalloc a) ->
            venv, SymbolTable.add globals v (ty, InitAlloc (AllocMalloc (inline_atom st a)))
        | ty, InitAlloc (AllocTuple (tclass, ty2, al)) ->
            let al = inline_atoms st al in
            let venv = new_box venv v 0 al in
              venv, SymbolTable.add globals v (ty, InitAlloc (AllocTuple (tclass, ty2, al)))
        | ty, InitAlloc (AllocArray (ty2, al)) ->
            let al = inline_atoms st al in
              venv, SymbolTable.add globals v (ty, InitAlloc (AllocArray (ty2, al)))
        | ty, InitAlloc (AllocMArray (ty2, al, a)) ->
            let al = inline_atoms st al in
            let a = inline_atom st a in
              venv, SymbolTable.add globals v (ty, InitAlloc (AllocMArray (ty2, al, a)))
        | ty, InitAlloc (AllocUnion (ty2, tv, i, al)) ->
            let al = inline_atoms st al in
            let tbl = match SymbolTable.find tenv tv with
                  TyDefLambda _ as tyd ->
                    raise (FirException (ExnVar tv, TyDefError tyd))
                | TyDefUnion (_, _, tbll) ->
                    List.nth tbll i in
            let rec make_dt dt i tbl al =
              match tbl, al with
                  [], [] -> dt
                | (_, true) :: tbl, _ :: al ->
                    make_dt dt (i+1) tbl al
                | (_, false) :: tbl, a :: al ->
                    make_dt (IntTable.add dt i a) (i+1) tbl al
                | _ -> raise (Invalid_argument "make_dt in fir_inline.ml") in
            let venv = SymbolTable.add venv v (DataBox (Some i, make_dt IntTable.empty 0 tbl al)) in
              venv, SymbolTable.add globals v (ty, InitAlloc (AllocUnion (ty2, tv, i, al))))
    (venv, globals) globals in
  let gs = SymbolTable.fold (fun gs v _ -> SymbolSet.add gs v) gs globals in
    gs, venv, st, cs, globals

(************************************** function inlining *)

let inline_trivial_fun gs special funs st fn =
   try
      let f = Callgraph.fun_of_node fn in
         if SymbolSet.mem special f then
            raise Not_found;
         let _, _, vl, e = SymbolTable.find funs f in
            match e with
               TailCall (v, al) when SymbolSet.mem gs v ->
                  let v = inline_var st v in
                     if Symbol.eq f v || not (Mc_list_util.length_eq vl al) then raise Not_found;
                     List.iter2 (fun v -> function
                        AtomVar v' when Symbol.eq v v' -> () | _ -> raise Not_found)
                     vl al;
                     let st = SymbolTable.add st f (AtomVar v) in
                        st, false
             | _ ->
                  raise Not_found
   with
      Not_found ->
         st, true

let rec simplify_ty t =
  let st = simplify_ty in
  let stl = List.map simplify_ty in
    match t with
        TyInt | TyEnum _ | TyRawInt _ | TyFloat _ | TyRawData -> t
      | TyFun (tl, t) -> TyFun (stl tl, st t)
      | TyUnion (tv, tl, s) -> TyUnion (tv, stl tl, s)
      | TyTuple (tclass, tl) -> TyTuple (tclass, stl tl)
      | TyArray t -> TyArray (st t)
      | _ -> TyDelayed

let inline_fun cg special alias gs funs venv st cs funs' fn =
   try
      let f = Callgraph.fun_of_node fn in
      let dl, ty, vl, e = SymbolTable.find funs f in
      let e = inline_expr alias gs venv st cs e in
      let cg = Callgraph.update_inlined_fun cg alias funs fn e in

      let id = Callgraph.in_degree fn in
      let sq = Callgraph.query fn fn in
      let c = expr_complexity e in
      let h =
         if sq || SymbolSet.mem special f then
            max_int
         else if c < 4 || id < 2 then
            0
         else
            c
      in
      let venv =
         if h < 25 then
            let e' = map_type_exp simplify_ty e in
               SymbolTable.add venv f (DataFun (h, vl, e'))
         else
            venv
      in
      let funs' = SymbolTable.add funs' f (dl, ty, vl, e) in
         cg, venv, st, funs'
   with
      Not_found ->
         cg, venv, st, funs'

let inline_funs cg special alias gs venv st cs funs =
   let gs = SymbolTable.fold (fun gs f _ -> SymbolSet.add gs f) gs funs in
   let nl = Callgraph.postorder_list cg in
   let st, nl = Mc_list_util.fold_filter (inline_trivial_fun gs special funs) st nl in
   let cg, _, _, funs =
      List.fold_left (fun (cg, venv, st, funs') n ->
            inline_fun cg special alias gs funs venv st cs funs' n) (**)
         (cg, venv, st, SymbolTable.empty) (Callgraph.postorder_list cg)
   in
      funs

(************************************** toplevel functions *)

let inline_prog callgraph alias prog =
   let
      { prog_file = file;
        prog_import = import;
        prog_export = export;
        prog_types = tenv;
        prog_names = names;
        prog_globals = globals;
        prog_funs = funs
      } = prog
   in
   let gs, venv, st, cs = SymbolSet.empty, SymbolTable.empty, SymbolTable.empty, ETreeTable.empty in
   let gs, venv, st, cs, globals = inline_globals tenv gs venv st cs globals in

   (* Build the loop-nest tree, to prevent inlining of loop-headers *)
   let loop = Fir_callgraph.loop_nest callgraph in
   let special = Trace.special_nodes loop in
   let special = List.map Fir_callgraph.fun_of_node special in
   let special = List.fold_left SymbolSet.add SymbolSet.empty special in

   (* Perform inlining *)
   let funs = inline_funs callgraph special alias gs venv st cs funs in
      { prog_file = file;
        prog_import = import;
        prog_export = export;
        prog_types = tenv;
        prog_names = names;
        prog_globals = globals;
        prog_funs = funs
      }

