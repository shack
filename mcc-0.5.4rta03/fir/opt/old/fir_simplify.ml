(* Optimize arithmetic calculations.
 * Geoffrey Irving
 * $Id: fir_simplify.ml,v 1.3 2002/06/24 00:00:53 jyh Exp $ *)

(* This algorithm works in three steps.
 *   1. Generate expression trees.  This is
 *      done by computing a set ns of variables
 *      defined by and used only in unops and binops.
 *      This are referred to as nice variables.
 *   2. Canonicalize and optimize the etrees.
 *   3. Convert back to fir.
 *)

open Symbol
open Fir
open Fir_exn
open Fir_algebra

(************************************** compute the set of nice variables *)

let rec nice_set ns e =
   let ba ns = function
      AtomVar v -> SymbolSet.remove ns v
    | _ -> ns
   in
      match e with
         LetUnop (v, _, _, _, e)
       | LetBinop (v, _, _, _, _, e) ->
            nice_set (SymbolSet.add ns v) e
       | LetExt (_, _, _, _, al, e)
       | LetAlloc (_, AllocTuple (_, _, al), e)
       | LetAlloc (_, AllocArray (_, al), e)
       | LetAlloc (_, AllocUnion (_, _, _, al), e) ->
            nice_set (List.fold_left ba ns al) e
       | TailCall (_, al) ->
            List.fold_left ba ns al
       | SpecialCall (TailSysMigrate (_, aptr, aoff, _, args)) ->
            List.fold_left ba ns (aptr :: aoff :: args)
       | SpecialCall _ ->
            raise (FirException (ExnExp e, NotImplemented "Fir_simplify: SpecialCall not impl"))
       | Match (a, sel) ->
            List.fold_left (fun ns (_, e) -> nice_set ns e) (ba ns a) sel
       | TypeCase (a1, a2, _, _, e1, e2) ->
            nice_set (nice_set (ba (ba ns a1) a2) e1) e2
       | LetSubscript (_, _, _, _, a, e)
       | LetAlloc (_, AllocMalloc a, e) ->
            nice_set (ba ns a) e
       | LetAlloc (_, AllocVArray (_, _, a1, a2), e) ->
            nice_set (ba (ba ns a1) a2) e
       | SetSubscript (_, _, _, a1, _, a2, e) ->
            nice_set (ba (ba ns a1) a2) e
       | SetGlobal (_, _, v, _, a, e) ->
            nice_set (SymbolSet.add (ba ns a) v) e
       | Memcpy (_, _, _, a1, _, a2, a3, e) ->
            nice_set (ba (ba (ba ns a1) a2) a3) e
       | Call (_, _, e)
       | Assert (_, _, e)
       | Debug (_, e) ->
            nice_set ns e

(************************************** expression simplification *)

let etree_of_atom venv a =
  match a with
      AtomVar v ->
        (try SymbolTable.find venv v
        with Not_found -> ETVar v)
    | _ -> ETConst a

let rec simplify_expr ns venv e =
   match e with
      LetUnop (v, _, op, a, e) ->
         let t = ETUnop (op, etree_of_atom venv a) in
            if SymbolSet.mem ns v then
               let venv = SymbolTable.add venv v t in
                  simplify_expr ns venv e
            else
               let t = optimize_etree (canonicalize_etree t) in
               let e = simplify_expr ns venv e in
                  compute_etree v t e
    | LetBinop (v, _, op, a1, a2, e) ->
         let t = ETBinop (op, etree_of_atom venv a1, etree_of_atom venv a2) in
            if SymbolSet.mem ns v then
               let venv = SymbolTable.add venv v t in
                  simplify_expr ns venv e
            else
               let t = optimize_etree (canonicalize_etree t) in
               let e = simplify_expr ns venv e in
                  compute_etree v t e
    | LetExt (v, t1, s, t2, al, e) ->
         LetExt (v, t1, s, t2, al, simplify_expr ns venv e)
    | TailCall _
    | SpecialCall _ ->
         e
    | Match (a, sel) ->
         Match (a, List.map (fun (s, e) -> s, simplify_expr ns venv e) sel)
    | TypeCase (a1, a2, name, v, e1, e2) ->
         TypeCase (a1, a2, name, v, simplify_expr ns venv e1, simplify_expr ns venv e2)
    | LetAlloc (v, aop, e) ->
         LetAlloc (v, aop, simplify_expr ns venv e)
    | LetSubscript (k, v, t, v2, a, e) ->
         LetSubscript (k, v, t, v2, a, simplify_expr ns venv e)
    | SetSubscript (k, l, v, a1, t, a2, e) ->
         SetSubscript (k, l, v, a1, t, a2, simplify_expr ns venv e)
    | SetGlobal (k, l, v, t, a, e) ->
         SetGlobal (k, l, v, t, a, simplify_expr ns venv e)
    | Memcpy (k, l, v1, a1, v2, a2, a3, e) ->
         Memcpy (k, l, v1, a1, v2, a2, a3, simplify_expr ns venv e)
    | Assert (label, pred, e) ->
         Assert (label, pred, simplify_expr ns venv e)
    | Debug (di, e) ->
         Debug (di, simplify_expr ns venv e)
    | Call (f, args, e) ->
         Call (f, args, simplify_expr ns venv e)

let simplify_fun (dl, ty, vl, e) =
  let ns = nice_set SymbolSet.empty e in
  let e = simplify_expr ns SymbolTable.empty e in
    dl, ty, vl, e

let simplify_prog prog =
  let funs = SymbolTable.map simplify_fun prog.prog_funs in
    { prog with prog_funs = funs }

