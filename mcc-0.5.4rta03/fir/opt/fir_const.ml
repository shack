(*
 * This is really simple constant folding.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Symbol
open Trace

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_loop

open Fir_loop.FirLoop

module Pos = MakePos (struct let name = "Fir_const" end)
open Pos

(*
 * Variable environment.
 *)
let venv_empty = SymbolTable.empty

let venv_lookup venv v =
   try SymbolTable.find venv v with
      Not_found ->
         AtomVar v

(*
 * Add a value.
 *)
let venv_add = SymbolTable.add

let venv_add_var venv v v' =
   SymbolTable.add venv v (AtomVar v')

let venv_add_args venv pos vars args =
   List.fold_left2 SymbolTable.add venv vars args

(*
 * Inline a var.  The result should be a var.
 *)
let inline_var venv pos v =
   match venv_lookup venv v with
      AtomVar v ->
         v
    | a ->
         raise (FirException (pos, StringAtomError ("value is not a variable", a)))

(*
 * Inline an atom.
 *)
let inline_atom venv a =
   match a with
      AtomVar v ->
         venv_lookup venv v
    | _ ->
         a

let inline_atom_opt venv a =
   match a with
      Some a ->
         Some (inline_atom venv a)
    | None ->
         None

let inline_atoms venv args =
   List.map (inline_atom venv) args

let inline_atom_opt_list venv args =
   List.map (inline_atom_opt venv) args

(*
 * Perform inlining.
 *)
let rec inline_exp venv e =
   let pos = string_pos "inline_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            inline_atom_exp venv pos loc v ty a e
       | LetExt (v, ty1, s, b, ty2, ty_args, args, e) ->
            inline_ext_exp venv pos loc v ty1 s b ty2 ty_args args e
       | TailCall (label, f, args) ->
            inline_tailcall_exp venv pos loc label f args
       | SpecialCall (label, op) ->
            inline_specialcall_exp venv pos loc label op
       | Match (a, cases) ->
            inline_match_exp venv pos loc a cases
       | MatchDTuple (a, cases) ->
            inline_match_dtuple_exp venv pos loc a cases
       | TypeCase (a1, a2, v1, v2, e1, e2) ->
            inline_typecase_exp venv pos loc a1 a2 v1 v2 e1 e2
       | LetAlloc (v, op, e) ->
            inline_alloc_exp venv pos loc v op e
       | LetSubscript (op, v1, ty, v2, a, e) ->
            inline_subscript_exp venv pos loc op v1 ty v2 a e
       | SetSubscript (op, label, v, a1, ty, a2, e) ->
            inline_set_subscript_exp venv pos loc op label v a1 ty a2 e
       | LetGlobal (op, v, ty, l, e) ->
            inline_global_exp venv pos loc op v ty l e
       | SetGlobal (op, label, v, ty, a, e) ->
            inline_set_global_exp venv pos loc op label v ty a e
       | Memcpy (op, label, v1, a1, v2, a2, a3, e) ->
            inline_memcpy_exp venv pos loc op label v1 a1 v2 a2 a3 e
       | Call (label, f, args, e) ->
            inline_call_exp venv pos loc label f args e
       | Assert (label, pred, e) ->
            inline_assert_exp venv pos loc label pred e
       | Debug (info, e) ->
            inline_debug_exp venv pos loc info e

(*
 * Arithmetic.
 *)
and inline_atom_exp venv pos loc v ty a e =
   let pos = string_pos "inline_atom_exp" pos in
   let a = inline_atom venv a in
   let e = inline_exp venv e in
      make_exp loc (LetAtom (v, ty, a, e))

(*
 * External call.
 *)
and inline_ext_exp venv pos loc v ty1 s b ty2 ty_args args e =
   let pos = string_pos "inline_ext_exp" pos in
   let args = inline_atoms venv args in
   let e = inline_exp venv e in
      make_exp loc (LetExt (v, ty1, s, b, ty2, ty_args, args, e))

(*
 * Tailcall.
 *)
and inline_tailcall_exp venv pos loc label f args =
   let pos = string_pos "inline_ext_exp" pos in
   let args = inline_atoms venv args in
   let f = inline_atom venv f in
      make_exp loc (TailCall (label, f, args))

(*
 * Special calls do not get inlined.
 *)
and inline_specialcall_exp venv pos loc label op =
   let pos = string_pos "inline_specialcall_exp" pos in
   let op =
      match op with
         TailSysMigrate (i, a1, a2, f, args) ->
            let a1 = inline_atom venv a1 in
            let a2 = inline_atom venv a2 in
            let f = inline_atom venv f in
            let args = inline_atoms venv args in
               TailSysMigrate (i, a1, a2, f, args)
       | TailAtomic (f, a, args) ->
            let f = inline_atom venv f in
            let a = inline_atom venv a in
            let args = inline_atoms venv args in
               TailAtomic (f, a, args)
       | TailAtomicRollback (level, a) ->
            let level = inline_atom venv level in
            let a = inline_atom venv a in
               TailAtomicRollback (level, a)
       | TailAtomicCommit (level, f, args) ->
            let level = inline_atom venv level in
            let f = inline_atom venv f in
            let args = inline_atoms venv args in
               TailAtomicCommit (level, f, args)
   in
      make_exp loc (SpecialCall (label, op))

(*
 * Match.
 *)
and inline_match_exp venv pos loc a cases =
   let pos = string_pos "inline_match_exp" pos in
   let a = inline_atom venv a in
   let cases = List.map (fun (l, s, e) -> l, s, inline_exp venv e) cases in
      make_exp loc (Match (a, cases))

and inline_match_dtuple_exp venv pos loc a cases =
   let pos = string_pos "inline_match_dtuple_exp" pos in
   let a = inline_atom venv a in
   let cases = List.map (fun (l, a_opt, e) -> l, inline_atom_opt venv a_opt, inline_exp venv e) cases in
      make_exp loc (MatchDTuple (a, cases))

(*
 * Typecase.
 *)
and inline_typecase_exp venv pos loc a1 a2 v1 v2 e1 e2 =
   let pos = string_pos "inline_typecase_exp" pos in
   let a1 = inline_atom venv a1 in
   let a2 = inline_atom venv a2 in
   let v1 = inline_var venv pos v1 in
   let e1 = inline_exp venv e1 in
   let e2 = inline_exp venv e2 in
      make_exp loc (TypeCase (a1, a2, v1, v2, e1, e2))

(*
 * Allocation.
 *)
and inline_alloc_exp venv pos loc v op e =
   let pos = string_pos "inline_alloc_exp" pos in
   let op =
      match op with
         AllocTuple (tclass, ty, ty_vars, args) ->
            let args = inline_atoms venv args in
               AllocTuple (tclass, ty, ty_vars, args)
       | AllocDTuple (ty, ty_var, a, args) ->
            let a = inline_atom venv a in
            let args = inline_atoms venv args in
               AllocDTuple (ty, ty_var, a, args)
       | AllocUnion (ty, ty_vars, ty_v, i, args) ->
            let args = inline_atoms venv args in
               AllocUnion (ty, ty_vars, ty_v, i, args)
       | AllocArray (ty, args) ->
            let args = inline_atoms venv args in
               AllocArray (ty, args)
       | AllocVArray (ty, op, a1, a2) ->
            let a1 = inline_atom venv a1 in
            let a2 = inline_atom venv a2 in
               AllocVArray (ty, op, a1, a2)
       | AllocMalloc (ty, a) ->
            let a = inline_atom venv a in
               AllocMalloc (ty, a)
       | AllocFrame _ ->
            op
   in
   let e = inline_exp venv e in
      make_exp loc (LetAlloc (v, op, e))

(*
 * Subscripts.
 *)
and inline_subscript_exp venv pos loc op v1 ty v2 a e =
   let pos = string_pos "inline_subscript_exp" pos in
   let a = inline_atom venv a in
   let v2 = inline_atom venv v2 in
   let e = inline_exp venv e in
      make_exp loc (LetSubscript (op, v1, ty, v2, a, e))

and inline_set_subscript_exp venv pos loc op label v a1 ty a2 e =
   let pos = string_pos "inline_set_subscript_exp" pos in
   let v = inline_atom venv v in
   let a1 = inline_atom venv a1 in
   let a2 = inline_atom venv a2 in
   let e = inline_exp venv e in
      make_exp loc (SetSubscript (op, label, v, a1, ty, a2, e))

and inline_global_exp venv pos loc op v ty l e =
   let pos = string_pos "inline_global_exp" pos in
   let e = inline_exp venv e in
      make_exp loc (LetGlobal (op, v, ty, l, e))

and inline_set_global_exp venv pos loc op label v ty a e =
   let pos = string_pos "inline_set_global_exp" pos in
   let v = inline_var venv pos v in
   let a = inline_atom venv a in
   let e = inline_exp venv e in
      make_exp loc (SetGlobal (op, label, v, ty, a, e))

and inline_memcpy_exp venv pos loc op label v1 a1 v2 a2 a3 e =
   let pos = string_pos "inline_memcpy_exp" pos in
   let v1 = inline_atom venv v1 in
   let a1 = inline_atom venv a1 in
   let v2 = inline_atom venv v2 in
   let a2 = inline_atom venv a2 in
   let a3 = inline_atom venv a3 in
   let e = inline_exp venv e in
      make_exp loc (Memcpy (op, label, v1, a2, v2, a2, a3, e))

(*
 * Other call.
 *)
and inline_call_exp venv pos loc label f args e =
   let pos = string_pos "inline_call_exp" pos in
   let f = inline_atom venv f in
   let args = inline_atom_opt_list venv args in
   let e = inline_exp venv e in
      make_exp loc (Call (label, f, args, e))

(*
 * Assertion.
 *)
and inline_assert_exp venv pos loc label pred e =
   let pos = string_pos "inline_assert_exp" pos in
   let pred =
      match pred with
         IsMutable v->
            IsMutable (inline_atom venv v)
       | Reserve (a1, a2) ->
            let a1 = inline_atom venv a1 in
            let a2 = inline_atom venv a2 in
               Reserve (a1, a2)
       | ElementCheck (ty, op, v, a) ->
            let v = inline_atom venv v in
            let a = inline_atom venv a in
               ElementCheck (ty, op, v, a)
       | BoundsCheck (op, v, a1, a2) ->
            let v = inline_atom venv v in
            let a1 = inline_atom venv a1 in
            let a2 = inline_atom venv a2 in
               BoundsCheck (op, v, a1, a2)
   in
   let e = inline_exp venv e in
      make_exp loc (Assert (label, pred, e))

(*
 * Debug.
 *)
and inline_debug_exp venv pos loc info e =
   let pos = string_pos "inline_debug_exp" pos in
   let e = inline_exp venv e in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Program inlining.
 *)
let const_prog prog =
   let { prog_funs = funs } = prog in
   let funs =
      SymbolTable.map (fun (info, ty, ty_vars, vars, e) ->
            let e = inline_exp venv_empty e in
               info, ty, ty_vars, vars, e) funs
   in
      { prog with prog_funs = funs }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
