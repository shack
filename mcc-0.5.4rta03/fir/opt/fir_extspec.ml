(* Fir external call specialization
 * Geoffrey Irving
 *
 *)
open Symbol

open Fir
open Fir_ds
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type

module Pos = MakePos (struct let name = "Fir_extspec" end)
open Pos

let var_of_atom = function
   AtomVar v ->
      v
 | _ ->
      raise Exit

let rec specialize_cmp loc v a1 a2 t e =
   match t with
      TyInt ->
         make_exp loc (LetAtom (v, TyInt, AtomBinop (CmpIntOp, a1, a2), e))
    | TyRawInt (p, s) ->
         make_exp loc (LetAtom (v, TyInt, AtomBinop (CmpRawIntOp (p, s), a1, a2), e))
    | TyFloat p ->
         make_exp loc (LetAtom (v, TyFloat p, AtomBinop (CmpFloatOp p, a1, a2), e))
    | TyTuple (_, [t, _]) ->
         let s1 = new_symbol_string "s1" in
         let s2 = new_symbol_string "s2" in
         let subop =
            { sub_block = BlockSub;
              sub_value = PolySub;
              sub_index = ByteIndex;
              sub_script = IntIndex
            }
         in
         let e = specialize_cmp loc v (AtomVar s1) (AtomVar s2) t e in
         let e = make_exp loc (LetSubscript (subop, s2, TyDelayed, a2, AtomInt 0, e)) in
            make_exp loc (LetSubscript (subop, s1, TyDelayed, a1, AtomInt 0, e))
    | _ ->
         raise Exit

let rec extspec_exp genv e =
   let pos = string_pos "extspec_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            let genv = genv_add_var genv v ty in
               make_exp loc (LetAtom (v, ty, a, extspec_exp genv e))
       | TailCall _
       | SpecialCall _ -> e
       | Match (a, sel) ->
            make_exp loc (Match (a, List.map (fun (l, s, e) -> l, s, extspec_exp genv e) sel))
       | MatchDTuple (a, sel) ->
            make_exp loc (MatchDTuple (a, List.map (fun (l, a_opt, e) -> l, a_opt, extspec_exp genv e) sel))
       | TypeCase (a1, a2, name, v, e1, e2) ->
            make_exp loc (TypeCase (a1, a2, name, v, extspec_exp genv e1, extspec_exp genv e2))
       | LetAlloc (v, op, e) ->
            let genv = genv_add_var genv v (type_of_alloc_op op) in
               make_exp loc (LetAlloc (v, op, extspec_exp genv e))
       | LetSubscript (k, v, ty, v2, a, e) ->
            let genv = genv_add_var genv v ty in
               make_exp loc (LetSubscript (k, v, ty, v2, a, extspec_exp genv e))
       | SetSubscript (k, l, v, a1, ty, a2, e) ->
            make_exp loc (SetSubscript (k, l, v, a1, ty, a2, extspec_exp genv e))
       | LetGlobal (op, v, ty, l, e) ->
            make_exp loc (LetGlobal (op, v, ty, l, extspec_exp genv e))
       | SetGlobal (k, l, v, ty, a, e) ->
            make_exp loc (SetGlobal (k, l, v, ty, a, extspec_exp genv e))
       | Memcpy (k, l, v1, a1, v2, a2, a3, e) ->
            make_exp loc (Memcpy (k, l, v1, a1, v2, a2, a3, extspec_exp genv e))
       | Assert (label, pred, e) ->
            make_exp loc (Assert (label, pred, extspec_exp genv e))
       | Debug (di, e) ->
            make_exp loc (Debug (di, extspec_exp genv e))
       | Call (label, f, args, e) ->
            make_exp loc (Call (label, f, args, extspec_exp genv e))
       | LetExt (v, ty, s, b, ty2, ty_args, al, e) ->
            let genv = genv_add_var genv v ty in
            let e = extspec_exp genv e in
               try
                  match s, al with
                     "cmp", [a1; a2] ->
                        let t = type_of_atom genv pos a1 in
                           specialize_cmp loc v a1 a2 t e
                   | _ ->
                        raise Exit
               with
                  Exit ->
                     make_exp loc (LetExt (v, ty, s, b, ty2, ty_args, al, e))

let extspec_fun genv (dl, tyvl, ty, vl, e) =
   let pos = string_pos "extspec_fun" (exp_pos e) in
   let tl, _ = dest_fun_type genv pos ty in
   let genv = List.fold_left2 genv_add_var genv vl tl in
   let e = extspec_exp genv e in
      dl, tyvl, ty, vl, e

let extspec_prog prog =
   let genv = genv_of_prog prog in
   let funs = SymbolTable.map (extspec_fun genv) prog.prog_funs in
      { prog with prog_funs = funs }
