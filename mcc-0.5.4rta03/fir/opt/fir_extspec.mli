(* Fir external call specialization 
 * Geoffrey Irving
 * $Id: fir_extspec.mli,v 1.2 2001/08/28 04:43:32 irving Exp $ *)

open Fir

val extspec_prog : prog -> prog
