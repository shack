#  Helper rules for makefiles within the tree.
#  Copyright(c) 2002 Justin David Smith, Caltech
#


CC              = gcc
CPP             = gcc -E
VERSION         = 0.5.4rta03
PACKAGE         = mcc-$(VERSION)
BACKEND         = x86
MCC_USES_RUNTIME= 1
TOPDIR          = /home/weel/vu/ml/mcc-0.5.4rta03

prefix          = /home/weel/opt
exec_prefix     = /home/weel/opt
bindir          = /home/weel/opt/bin
libdir          = /home/weel/opt/lib
includedir      = /home/weel/opt/include

MKDIR           = /bin/bash ./mkinstalldirs
INSTALL         = /usr/bin/install -c
INSTALL_PROGRAM = ${INSTALL}
INSTALL_DATA    = ${INSTALL} -m 644
MP_PATH         = 
MC_PATH         = `pwd`

SUFOBJ          = .cmx
SUFLIB          = .cmxa


all:
	@cons -t force_bytecode=$(force_bytecode) $(TARGET) $(addsuffix $(SUFLIB),$(TARGETLIB))

clean: clean-rec
	@echo "Using cons to clear any remaining targets"
	@cons -t -r .

clean-rec: clean-local clean-phobos
	@for subdir_make in `ls */Makefile .??*/Makefile 2> /dev/null`; do \
		if [ "$$subdir_make" != "source/Makefile" ]; then \
			subdir=$${subdir_make%/Makefile}; \
			$(MAKE) -C $$subdir SUFLIB=$(SUFLIB) clean-rec || exit 1; \
		fi \
	done

clean-local:
	@rm -f *.o *.cmo *.cmi *.cmx *.a *.cmxa *.cma *~ .*~ *.bak .#* *.cph
	@if test "x$(TEST)" != "x"; then \
		echo "Extensive clean for test directory"; \
		rm -f *.exec *.o *.mo *.fo *.fir *.mir *.out *.s *.err *.pgm *.i a.out; \
	fi
	@if test "x$(TEXFILES)" != "x"; then \
		echo "Extensive clean for tex directory"; \
		rm -f *.aux *.dvi *.log *.idx *.ilg *.ind *.kfig *.toc *.out *.blg *.bbl *.brf *.lof; \
	fi
	@rm -f $(EXTRACLEAN)

clean-phobos:
	@rm -f *.cph *.output Phobos.depend

doc: doc-rec

doc-rec: doc-local
	@for subdir_make in `ls */Makefile .??*/Makefile 2> /dev/null`; do \
		if [ "$$subdir_make" != "source/Makefile" ]; then \
			subdir=$${subdir_make%/Makefile}; \
			$(MAKE) -C $$subdir SUFLIB=$(SUFLIB) doc-rec || exit 1; \
		fi \
	done

doc-local:
	@if test "x$(TEXFILES)" != "x"; then \
		$(MAKE) $(TEXFILES); \
	fi

test:
	@if test "x$(TEST)" != "x"; then \
		cons -t -k $(TEST); \
	fi

Phobos.depend:
	$(TOPDIR)/makedepend.phobos > Phobos.depend

.SUFFIXES: .tex .dvi .ps .fig .pdf .eps .pho .cph

.tex.dvi:
	latex $<
	[ "x$(ENABLEBIBTEX)" != "xyes" ] || bibtex $*
	latex $<
	[ ! -r $*.idx ] || makeindex $*.idx
	@echo ""
	@echo ""
	@echo "*** Final pass ***"
	@echo ""
	@echo ""
	latex $<

.dvi.ps:
	dvips $< -o $@ -t letter

.tex.pdf:
	pdflatex $<
	[ "x$(ENABLEBIBTEX)" != "xyes" ] || bibtex $*
	pdflatex $<
	[ ! -r $*.idx ] || makeindex $*.idx
	@echo ""
	@echo ""
	@echo "*** Final pass ***"
	@echo ""
	@echo ""
	pdflatex $<

.fig.eps:
	fig2dev -L eps $< $@

.fig.pdf:
	fig2dev -L pdf $< $@

.pho.cph:
	$(TOPDIR)/main/mcc $< -I $(TOPDIR)/test/phobos/include
