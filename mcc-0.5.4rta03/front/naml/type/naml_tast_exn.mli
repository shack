(* 
 * Naml type ast exceptions
 * Geoffrey Irving
 * 12apr01
 *)

open Symbol
open Naml_tast

exception ParseError of pos * string
exception NotImplemented of string
exception Impossible of string

exception MultipleBindings_Field of pos * symbol

exception FieldMismatch of pos * vpath * vpath * vpath
exception FieldMutability of pos * vpath
exception IncompleteRecord of pos * symbol
exception Invalid_Extern of string
exception Invalid_ExternType of string
exception Invalid_RecExpr of pos
exception Invalid_RecPattern of pos
exception TypeCons_Arity of pos * vpath * int * int
exception TypeMismatch of pos * ty * ty

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
val print_exn : exn -> unit
val print_exn_chan : out_channel -> exn -> unit
val catch : ('a -> 'b) -> 'a -> 'b

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
