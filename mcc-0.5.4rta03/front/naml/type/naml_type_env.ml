(*
 * Type Environment utils
 * 2001-04-11
 *)

open Naml_tast

module MPathKey =
struct
  type t = mpath
  let rec compare m1 m2 =
    match m1, m2 with
	[], [] -> 0
      | [], _ -> -1
      | _, [] -> 1
      | s1 :: m1, s2 :: m2 ->
	  let r = Symbol.compare s1 s2 in
	    if r = 0 then
	      compare m1 m2
	    else
	      r
end

module VPathKey =
struct
  type t = vpath
  let compare (m1, s1) (m2, s2) =
    let r = Symbol.compare s1 s2 in
      if r = 0 then
	MPathKey.compare m1 m2
      else
	r
end

module MPathTable = Mc_map.McMake (MPathKey)
module VPathTable = Mc_map.McMake (VPathKey)

