(*
vim:sts=4:sw=4
 * Utilities over the syntax.
 * Geoffrey Irving / Dylan Simon
 * 12apr01
 *)

open Format
open Symbol
open Print_util
open Naml_tast
open Naml_ast_exn

(*
 * BUG: until we fix this module.
 *)
let print_symbol = pp_print_symbol Format.std_formatter

let rec mpath_eq m1 m2 =
  match m1, m2 with
      [], [] -> true
    | s1 :: m1, s2 :: m2 -> Symbol.eq s1 s2 && mpath_eq m1 m2
    | _ -> false

let vpath_eq (m1, v1) (m2, v2) =
  Symbol.eq v1 v2 && mpath_eq m1 m2

let type_of_expr = function
    VarExpr (_, t)
  | ConstExpr (_, t)
  | TupleExpr (_, t)
  | ConsExpr (_, _, t)
  | ConsCopyExpr (_, _, t)
  | ArrayExpr (_, t)
  | RecordExpr (_, t)
  | ApplyExpr (_, _, t)
  | ProjExpr (_, _, t)
  | AssignProjExpr (_, _, _, t)
  | ArrayEltExpr (_, _, t)
  | AssignArrayEltExpr (_, _, _, t)
  | WhileExpr (_, _, t)
  | ForExpr (_, _, _, _, _, t)
  | SeqExpr (_, _, t)
  | MatchExpr (_, _, t)
  | TryExpr (_, _, t)
  | FunExpr (_, _, t)
  | LetRecExpr (_, _, t)            -> t
  | LetExtExpr _ -> raise (Invalid_argument "LetExtExpr to type_of_expr")

let type_of_pattern = function
    IdPat (_, ty)
  | AnyPat (ty)
  | ConstPat (_, ty)
  | RangePat (_, _, ty)
  | AsPat (_, _, ty)
  | AltPat (_, _, ty)
  | ConsPat (_, _, ty)
  | ConsCopyPat (_, _, ty)
  | TuplePat (_, ty)
  | ArrayPat (_, ty)
  | RecordPat (_, ty) -> ty

let tydef_info = function
    TyDefLambda (tvl, ml, _)
  | TyDefUnion (tvl, ml, _)
  | TyDefRecord (tvl, ml, _) -> tvl, ml

(************************************************************************
 * PRINTING                                                             *
 ************************************************************************)

let rec print_sep_list_fold sep printer foldme = function
   [] ->
      foldme
 | [h] ->
      printer foldme h
 | h :: t ->
      let foldme = printer foldme h in
      Format.print_string sep;
      Format.print_space ();
      print_sep_list_fold sep printer foldme t

let print_meta s =
    Format.print_string "'";
    print_symbol s

let print_mpath = print_sep_list_no_space "." print_symbol

let print_vpath = function
    (m, s) ->
	print_mpath m;
	(if m != [] then Format.print_string ".");
	print_symbol s

let rec print_reduce_meta sto s =
  match sto with
      None -> print_meta s; sto
    | Some (st, nextc) ->
	(try
	    let c = SymbolTable.find st s in
	    let i = c / 26 in
	    if i == 0 then
		Format.printf "'%c" (char_of_int (c + (int_of_char 'a')))
	    else
		Format.printf "'%c%d" (char_of_int ((c mod 26) + (int_of_char 'a'))) i;
	    sto
	with Not_found ->
	  print_reduce_meta (Some (SymbolTable.add st s nextc, succ nextc)) s)

let rec print_ty_aux c sto ty =
  match ty with
      TyInt -> Format.print_string "int"; sto
    | TyChar -> Format.print_string "char"; sto
    | TyFloat -> Format.print_string "float"; sto
    | TyExn -> Format.print_string "TyExn"; sto
    | TyVar s -> print_reduce_meta sto s

    | TyFun (tl1, t2) ->
        if c < 2 then Format.print_string "(";
	let sto = print_sep_list_fold " ->" (print_ty_aux 1) sto tl1 in
	Format.printf " ->@ ";
	let sto = print_ty_aux 2 sto t2 in
        if c < 2 then Format.print_string ")";
	sto
    | TyTuple tl ->
        if c < 1 then Format.print_string "(";
	let sto = print_sep_list_fold " *" (print_ty_aux 0) sto tl in
        if c < 1 then Format.print_string ")";
	sto
    | TyArray t ->
        let sto = print_ty_aux 0 sto t in
        Format.print_string " array";
	sto
    | TyApply (v, tl) ->
	let sto = (match tl with
	      [] -> sto
	    | [t] ->
		let sto = print_ty_aux 0 sto t in
		Format.print_space ();
		sto
	    | _ ->
		Format.print_string "(";
		let sto = print_sep_list_fold "," (print_ty_aux 5) sto tl in
		Format.print_string ")";
		Format.print_space ();
		sto) in
	print_vpath v;
	sto

let print_ty t = ignore (print_ty_aux 5 None t)
let print_reduce_ty t = ignore (print_ty_aux 5 (Some (SymbolTable.empty, 0)) t)

let print_ty_spec pf ty =
  Format.print_string "(";
  pf ();
  Format.print_string " : ";
  print_ty ty;
  Format.print_string ")"

let print_type_params = function
    [] -> ()
  | [x] ->
      print_meta x;
      Format.print_string " "
  | al ->
      Format.print_string "(";
      print_sep_list "," print_meta al;
      Format.print_string ") "

let print_tydef (s, tyd) =
  let sl, _ = tydef_info tyd in
    print_type_params sl;
    print_symbol s;
    Format.print_string " =";
    Format.print_space ();
    match tyd with
        TyDefLambda (_, _, t) -> print_ty t
      | TyDefUnion (_, _, stll) ->
          let print_cons = function
              (s, []) ->
                print_symbol s
            | (s, t) ->
                print_symbol s;
                Format.print_string " of ";
                print_sep_list " *" (fun t -> ignore (print_ty_aux 2 None t)) t in
          print_sep_list " |" print_cons stll
      | TyDefRecord (_, _, bstl) ->
          let print_field (m, s, t) =
            if m then Format.print_string "mutable ";
            print_symbol s;
            Format.print_string " :";
            Format.print_space ();
            ignore (print_ty_aux 5 None t) in
          Format.print_string "{ ";
          print_sep_list ";" print_field bstl;
          Format.print_string " }"

let print_exndef = function
      ExnVar (e, v) ->
        print_symbol e;
        Format.print_string " =";
        Format.print_space ();
        print_vpath v
    | ExnCons (e, t) ->
        print_symbol e;
        (match t with
              [] -> ()
            | _ ->
                Format.print_string " of ";
                Format.print_space ();
                print_sep_list " *" print_ty t)

let print_constexpr = function
      IntConst v ->
        Format.printf "%d" v
    | FloatConst v ->
        Format.printf "%g" v
    | CharConst v ->
        Format.printf "'%s'" (Char.escaped v)
    | CConsConst (v, t) ->
        print_ty_spec (fun () -> print_vpath v) t

let rec print_expr = function
    | VarExpr (v, t) ->
        print_ty_spec (fun () -> print_vpath v) t
    | ConstExpr (c, t) ->
        print_ty_spec (fun () -> print_constexpr c) t
    | TupleExpr (el, t) ->
        print_ty_spec (fun () -> print_sep_list "," print_expr el) t
    | ConsExpr (v, el, t) ->
        print_ty_spec (fun () ->
        print_vpath v;
        Format.print_string " (";
        print_sep_list "," print_expr el;
        Format.print_string ")") t
    | ConsCopyExpr (v, e, t) ->
        print_ty_spec (fun () ->
        Format.print_string "copy ";
        print_vpath v;
        Format.print_space ();
        print_expr e) t
    | ArrayExpr (el, t) ->
        print_ty_spec (fun () ->
        Format.print_string "[|";
        print_sep_list ";" print_expr el;
        Format.print_string "|]") t
    | RecordExpr (el, t) ->
        print_ty_spec (fun () ->
        Format.print_string "{ ";
        print_sep_list "," print_expr el;
        Format.print_string " }") t
    | ApplyExpr (f, a, t) ->
        print_ty_spec (fun () ->
        print_expr f;
        Format.print_space ();
        print_sep_list "" print_expr a) t
    | ProjExpr (e, v, t) ->
        print_ty_spec (fun () ->
        print_expr e;
        Format.print_string ".";
        print_vpath v) t
    | AssignProjExpr (e, v, e2, t) ->
        print_ty_spec (fun () ->
        print_expr e;
        Format.print_string ".";
        print_vpath v;
        Format.print_string " <-";
        Format.print_space ();
        print_expr e2) t
    | ArrayEltExpr (e, elt, t) ->
        print_ty_spec (fun () ->
        print_expr e;
        Format.print_string ".(";
        print_expr elt;
        Format.print_string ")") t
    | AssignArrayEltExpr (e, elt, e2, t) ->
        print_ty_spec (fun () ->
        print_expr e;
        Format.print_string ".(";
        print_expr elt;
        Format.print_string ") <-";
        Format.print_space ();
        print_expr e2) t
    | WhileExpr (w, d, t) ->
        Format.print_cut ();
        Format.open_box tabstop;
        Format.print_string "while ";
        print_expr w;
        Format.print_string " do";
        Format.print_space ();
        Format.open_box tabstop;
        print_expr d;
        Format.close_box ();
        Format.print_space ();
        Format.print_string "done";
        Format.close_box ()
    | ForExpr (s, f, dir, t, d, _) ->
        Format.print_cut ();
        Format.open_box tabstop;
        Format.print_string "for ";
        print_symbol s;
        Format.print_string " =";
        Format.print_space ();
        print_expr f;
        Format.print_string (if dir then " to " else " downto ");
        print_expr t;
        Format.print_string " do";
        Format.print_space ();
        Format.open_box tabstop;
        print_expr d;
        Format.close_box ();
        Format.print_space ();
        Format.print_string "done";
        Format.close_box ()
    | SeqExpr (e1, e2, t) ->
        Format.open_box tabstop;
        Format.print_string "(";
        print_expr e1;
        Format.print_string "; ";
        print_expr e2;
        Format.print_string ")";
        Format.close_box ()
    | MatchExpr (e, pm, t) ->
        Format.open_box tabstop;
        print_ty_spec (fun () ->
        Format.print_string "match ";
        print_expr e;
        Format.print_string " with";
        Format.print_space ();
        print_pmatch_list pm) t;
        Format.close_box ()
    | FunExpr (al, e, t) ->
        let print_arg (s, t) = print_ty_spec (fun () -> print_symbol s) t in
        Format.open_box tabstop;
        print_ty_spec (fun () ->
        Format.printf "fun ";
        print_sep_list "" print_arg al;
        Format.printf "->@ ";
        print_expr e) t;
        Format.close_box ()
    | TryExpr (e, pm, t) ->
        Format.open_box tabstop;
        print_ty_spec (fun () ->
        Format.print_string "try ";
        print_expr e;
        Format.print_string " with";
        Format.print_space ();
        print_pmatch_list pm) t;
        Format.close_box ()
    | LetRecExpr (bl, e, t) ->
        Format.open_box tabstop;
        let print_binding (s, stl, e, t) =
          Format.open_box tabstop;
          print_symbol s;
          Format.print_string " : ";
          print_ty t;
          Format.print_string " =";
          Format.print_space ();
	  print_expr (FunExpr (stl, e, t));
          Format.close_box () in
        Format.printf "let rec@ ";
        print_sep_list_box " and" print_binding bl;
        Format.print_space ();
        Format.printf "@ in@ ";
        print_expr e;
        Format.close_box ()
    | LetExtExpr _ -> raise (Invalid_argument "LetExtExpr to print_expr")

and print_pmatch_list pm =
    let print_pmatch (p, w, e) =
        Format.open_box tabstop;
        print_pattern p;
        (match w with
              None -> ()
            | Some w ->
                Format.print_space ();
                Format.print_string "when ";
                print_expr w);
        Format.print_string " ->";
        Format.print_space ();
        print_expr e;
        Format.close_box ()
    in
    Format.open_box tabstop;
    print_sep_list_box " |" print_pmatch pm;
    Format.close_box ()

and print_pattern = function
      IdPat (s, t) ->
        print_ty_spec (fun () -> print_symbol s) t
    | AnyPat t ->
        print_ty_spec (fun () -> Format.print_string "_") t
    | ConstPat (c, t) ->
        print_ty_spec (fun () -> print_constexpr c) t
    | RangePat (cl, ch, t) ->
        print_ty_spec (fun () ->
        print_constexpr cl;
        Format.print_string " .. ";
        print_constexpr ch) t
    | AsPat (p, s, t) ->
        print_ty_spec (fun () ->
        print_pattern p;
        Format.print_string " as ";
        print_symbol s) t
    | AltPat (p1, p2, t) ->
        print_ty_spec (fun () ->
        print_pattern p1;
        Format.print_string " |";
        Format.print_space ();
        print_pattern p2) t
    | ConsPat (v, pl, t) ->
        print_ty_spec (fun () ->
        print_vpath v;
        Format.print_string " (";
        print_sep_list "," print_pattern pl;
        Format.print_string ")") t
    | ConsCopyPat (v, p, t) ->
        print_ty_spec (fun () ->
        Format.print_string "copy ";
        print_vpath v;
        Format.print_space ();
        print_pattern p) t
    | TuplePat (pl, t) ->
        print_ty_spec (fun () ->
        print_sep_list "," print_pattern pl) t
    | RecordPat (pl, t) ->
        print_ty_spec (fun () ->
        Format.print_string "{ ";
        print_sep_list ";" print_pattern pl;
        Format.print_string " }") t
    | ArrayPat (pl, t) ->
        print_ty_spec (fun () ->
        Format.print_string "[|";
        print_sep_list ";" print_pattern pl;
        Format.print_string "|]") t

and print_def = function
      LetMod bl ->
        let print_binding (p, e) =
            Format.open_box tabstop;
            print_pattern p;
            Format.print_string " =";
            Format.print_space ();
            print_expr e;
            Format.close_box ()
        in
        Format.open_box tabstop;
        Format.printf "let@ ";
        print_sep_list_box " and" print_binding bl;
        Format.close_box ()
    | LetRecMod bl ->
        let print_binding (s, stl, e, t) =
            Format.open_box tabstop;
            print_symbol s;
            Format.print_string " : ";
            print_ty t;
            Format.printf " =@ ";
            print_expr (FunExpr (stl, e, t));
            Format.close_box ()
        in
        Format.open_box tabstop;
        Format.printf "let rec@ ";
        print_sep_list_box " and" print_binding bl;
        Format.close_box ()
    | ExternMod (s, t, e) ->
        Format.print_string "external ";
        print_symbol s;
        Format.print_string " : ";
        print_ty t;
        Format.print_string " = ";
        Format.printf "\"%s\"" (String.escaped e)
    | TypeMod tl ->
        Format.print_string "type ";
        print_sep_list_box " and" print_tydef tl
    | ExnMod e ->
        Format.print_string "exception ";
        print_exndef e

and print_def_sig = function
      LetMod bl ->
        List.iter (fun (p, _) ->
          Format.printf "val ";
          print_pattern p) bl
    | LetRecMod bl ->
        List.iter (fun (s, _, _, t) ->
          Format.printf "val ";
          print_symbol s;
          Format.print_string " : ";
          print_ty t) bl
    | ExternMod (s, t, e) -> ()
(*        Format.print_string "external ";
        print_symbol s;
        Format.print_string " : ";
        print_ty t;
        Format.print_string " = ";
        Format.printf "\"%s\"" (String.escaped e) *)
    | TypeMod tl ->
        Format.print_string "type ";
        print_sep_list_box " and" print_tydef tl
    | ExnMod e ->
        Format.print_string "exception ";
        print_exndef e

let print_prog prog =
    print_sep_list_box "" print_def prog

let print_prog_sig prog =
    print_sep_list_box "" print_def_sig prog

