(*
 * Naml polymorphic type inference
 * Geoffrey Irving / Dylan Simon
 * 8apr1
 *)

(*
 * Converts ast to typed ast.  Also returns a
 * set of non-generic type variables.
 *)

val infer : Naml_ast.prog -> Naml_tast.prog
