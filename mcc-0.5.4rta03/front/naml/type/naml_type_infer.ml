(*
 * Naml polymorphic type inference
 * Geoffrey Irving / Dylan Simon
 * 8apr1
 *)

(* Environments:
 *   tenv  - type definition environment: vpath -> tydef
 *   tvenv - type variable environment: vpath -> ty
 *   cenv  - constructor env: vpath -> vpath * symbol list * ty list
 *   fenv  - record field environment: vpath -> vpath * bool * ty list
 *   venv  - varible type env: vpath -> ty
 *)

open Symbol
open Naml_tast
open Naml_type_env
open Naml_tast_exn
open Naml_tast_util
open Naml_type_boot
open Naml_ast_util

module Ast = Naml_ast

type env = {
  tenv : tydef VPathTable.t;
  cenv : (vpath * symbol list * ty list) VPathTable.t;
  eenv : ty list VPathTable.t;
  fenv : (vpath * symbol list * bool * ty) VPathTable.t;
  venv : ty VPathTable.t;
  menv : SymbolSet.t;
  tvenv : ty SymbolTable.t;
  uenv : SymbolSet.t;
}

let option_map f = function
    None -> None 
  | Some x -> Some (f x)

let rec subst_ty tvenv ty = (****************************** SUBST *)
  let stty = subst_ty tvenv in
  match ty with
      TyInt | TyChar | TyFloat | TyExn -> ty
    | TyFun (tl1, t2) ->
	TyFun (List.map stty tl1, stty t2)
    | TyTuple (tl) ->
	TyTuple (List.map stty tl)
    | TyArray (t1) ->
	TyArray (stty t1)
    | TyApply (s, tl) ->
	TyApply (s, List.map stty tl)

    | TyVar v ->
	try
	  stty (SymbolTable.find tvenv v)
	with
	  Not_found -> ty

let subst_constexpr tvenv = function
      CConsConst (m, t) ->
	CConsConst (m, subst_ty tvenv t)
    | x -> x

let rec subst_pattern tvenv pat =
  let stty = subst_ty tvenv in
  let sce = subst_constexpr tvenv in 
  let sp = subst_pattern tvenv in
  match pat with
      IdPat (s, t) ->
	IdPat (s, stty t)
    | AnyPat t ->
	AnyPat (stty t)
    | ConstPat (ce, t) ->
	ConstPat (sce ce, stty t)
    | RangePat (ce1, ce2, t) ->
	RangePat (sce ce1, sce ce2, stty t)
    | AsPat (p, s, t) ->
	AsPat (sp p, s, stty t)
    | AltPat (p1, p2, t) ->
	AltPat (sp p1, sp p2, stty t)
    | ConsPat (m, pl, t) ->
	ConsPat (m, List.map sp pl, stty t)
    | ConsCopyPat (m, p, t) ->
	ConsCopyPat (m, sp p, stty t)
    | TuplePat (pl, t) ->
	TuplePat (List.map sp pl, stty t)
    | ArrayPat (pl, t) ->
	ArrayPat (List.map sp pl, stty t)
    | RecordPat (pl, t) ->
	RecordPat (List.map sp pl, stty t)

let rec subst_expr tvenv e =
  let stty = subst_ty tvenv in
  let sce = subst_constexpr tvenv in
  let sp = subst_pattern tvenv in
  let se = subst_expr tvenv in
  let spm = List.map (fun (p, eo, e) -> sp p, option_map se eo, se e) in
  match e with
      VarExpr (m, t) ->
	VarExpr (m, stty t)
    | ConstExpr (ce, t) ->
	ConstExpr (sce ce, stty t)
    | TupleExpr (el, t) ->
	TupleExpr (List.map se el, stty t)
    | ConsExpr (m, el, t) ->
	ConsExpr (m, List.map se el, stty t)
    | ConsCopyExpr (m, e, t) ->
	ConsCopyExpr (m, se e, stty t)
    | ArrayExpr (el, t) ->
	ArrayExpr (List.map se el, stty t)
    | RecordExpr (el, t) ->
	RecordExpr (List.map se el, stty t)
    | ApplyExpr (e, el, t) ->
	ApplyExpr (se e, List.map se el, stty t)
    | ProjExpr (e, m, t) ->
	ProjExpr (se e, m, stty t)
    | AssignProjExpr (e1, m, e2, t) ->
	AssignProjExpr (se e1, m, se e2, stty t)
    | ArrayEltExpr (e1, e2, t) ->
	ArrayEltExpr (se e1, se e2, stty t)
    | AssignArrayEltExpr (e1, e2, e3, t) ->
	AssignArrayEltExpr (se e1, se e2, se e3, stty t)
    | WhileExpr (e1, e2, t) ->
	WhileExpr (se e1, se e2, stty t)
    | ForExpr (s, e1, b, e2, e3, t) ->
	ForExpr (s, se e1, b, se e2, se e3, t)
    | SeqExpr (e1, e2, t) ->
	SeqExpr (se e1, se e2, stty t)
    | MatchExpr (e, pm, t) ->
	MatchExpr (se e, spm pm, stty t)
    | FunExpr (atl, e, t) ->
	FunExpr (List.map (fun (a, t) -> (a, stty t)) atl, se e, stty t)
    | TryExpr (e, pm, t) ->
	TryExpr (se e, spm pm, stty t)
    | LetRecExpr (bl, e, t) ->
        LetRecExpr (List.map (fun (s, stl, e, t) -> (s, List.map (fun (s, t) -> s, stty t) stl, se e, stty t)) bl, se e, stty t)
    | LetExtExpr _ -> raise (Invalid_argument "LetExtExpr to subst_expr")

let subst_venv tvenv = VPathTable.map (subst_ty tvenv)

let subst_def tvenv def =
  match def with
      LetMod pel ->
	LetMod (List.map (fun (p, e) -> subst_pattern tvenv p, subst_expr tvenv e) pel)
    | LetRecMod stel ->
	LetRecMod (List.map (fun (s, stl, e, t) -> s, List.map (fun (s, t) -> s, subst_ty tvenv t) stl, subst_expr tvenv e, subst_ty tvenv t) stel)
    | _ -> def

(********************************************************** TYPE TWIDDLING *)
let convert_vpath (m, s, p) = List.map fst m, s

let rec typify tenv ty =
  let tf = typify tenv in
  match ty with
      Ast.TypeFun (t1, t2, _) -> TyFun ([tf t1], tf t2)
    | Ast.TypeTuple (tl, _) ->   TyTuple (List.map tf tl)
    | Ast.TypeAny _ ->           raise (Impossible "typify saw TypeAny")
    | Ast.TypeVar (s, _) ->      TyVar s
    | Ast.TypeId (tl, v, pos) ->
        let v = convert_vpath v in 
        let sl, _ = tydef_info (VPathTable.find tenv v) in
	  if not (Mc_list_util.length_eq sl tl) then
	    raise (TypeCons_Arity (pos, v, List.length sl, List.length tl));
	  TyApply (v, List.map tf tl)

let rec free_tyvars fv t =
  match t with
      TyInt | TyChar | TyFloat | TyExn -> fv
    | TyFun (tl1, t2) -> free_tyvars (List.fold_left free_tyvars fv tl1) t2
    | TyTuple tl -> List.fold_left free_tyvars fv tl
    | TyArray ty -> free_tyvars fv ty
    | TyVar s -> SymbolSet.add fv s
    | TyApply (_, tl) -> List.fold_left free_tyvars fv tl

let rec standardize_ty st ty =
  let sty = standardize_ty st in
  match ty with
      TyInt | TyChar | TyFloat | TyExn -> ty
    | TyFun (tl1, t2) -> TyFun (List.map sty tl1, sty t2)
    | TyTuple tl -> TyTuple (List.map sty tl)
    | TyArray t -> TyArray (sty t)
    | TyVar s -> TyVar (SymbolTable.find st s)
    | TyApply (v, tl) -> TyApply (v, List.map sty tl)

let rec expand_ty tvenv t =
  let ete = expand_ty tvenv in
  match t with
      TyInt | TyChar | TyFloat | TyExn -> t
    | TyFun (tl1, t2) -> TyFun (List.map ete tl1, ete t2)
    | TyTuple tl -> TyTuple (List.map ete tl)
    | TyArray t -> TyArray (ete t)
    | TyVar s -> (try ete (SymbolTable.find tvenv s) with Not_found -> t)
    | TyApply (v, tl) -> TyApply (v, List.map ete tl)

let rec taint_ty_aux tenv menv t =
  let tty = taint_ty_aux tenv in
  match t with
      TyInt | TyChar | TyFloat | TyExn | TyFun _ | TyVar _ -> menv
    | TyTuple tl -> List.fold_left tty menv tl
    | TyArray t -> free_tyvars menv t
    | TyApply (v, tl) ->
        let _, bl = tydef_info (VPathTable.find tenv v) in
          List.fold_left2 (fun menv b t -> if b then free_tyvars menv t else menv) menv bl tl

let taint_ty env t = { env with menv = taint_ty_aux env.tenv env.menv t }
let taint_tyl env tl = { env with menv = List.fold_left (taint_ty_aux env.tenv) env.menv tl }

exception Unify_failure

let rec insure_tyvar_absent v t =
  let itav = insure_tyvar_absent v in
  match t with 
      TyInt | TyChar | TyFloat | TyExn -> ()
    | TyFun (tl1, t2) -> List.iter itav tl1; itav t2
    | TyTuple tl -> List.iter itav tl
    | TyArray t -> itav t
    | TyVar s ->
	if Symbol.eq v s then
	  raise Unify_failure
    | TyApply (_, tl) -> List.iter itav tl

let rec rename_tyvars_aux env st ty =
  let rn = rename_tyvars_aux env in
  match ty with
      TyInt | TyChar | TyFloat | TyExn -> st, ty
    | TyFun (tl1, t2) ->
	let st, tl1 = Mc_list_util.fold_map rn st tl1 in
	let st, t2 = rn st t2 in
	  st, TyFun (tl1, t2)
    | TyTuple tl ->
	let st, tl = Mc_list_util.fold_map rn st tl in
	  st, TyTuple tl
    | TyArray t ->
	let st, t = rn st t in
	  st, TyArray t
    | TyApply (v, tl) ->
	let st, tl = Mc_list_util.fold_map rn st tl in
	  st, TyApply (v, tl)
    | TyVar s ->
        (try
          rn st (SymbolTable.find env.tvenv s)
        with Not_found ->
	  if SymbolSet.mem env.uenv s || SymbolSet.mem env.menv s then
  	    st, TyVar s
	  else
	    (try
	      let s' = SymbolTable.find st s in
	        st, TyVar s'
	    with Not_found ->
	      let s' = new_symbol s in
	      let st = SymbolTable.add st s s' in
	        st, TyVar s'))

let rename_tyvars env ty = snd (rename_tyvars_aux env SymbolTable.empty ty)

let rec do_unify env ty1 ty2 = (****************************** UNIFY *)
  (* print_ty (expand_ty env.tvenv ty1); Format.print_string " with "; 
  print_ty (expand_ty env.tvenv ty2); Format.printf "@\n"; Format.print_newline (); *)
  match ty1, ty2 with
      x, y when x == y -> env
    | TyFun (a1, r1), TyFun (a2, r2) ->
	let rec unify_fun env a1 a2 =
	  match a1, a2 with
	      a1::al1, a2::al2 ->
		let env = do_unify env a1 a2 in
		  unify_fun env al1 al2
	    | [], [] -> do_unify env r1 r2
            | _, [] -> do_unify env (TyFun (a1, r1)) r2
            | [], _ -> do_unify env r1 (TyFun (a2, r2)) in
	unify_fun env a1 a2
    | TyTuple (tl1), TyTuple (tl2) ->
	(try
	  List.fold_left2 do_unify env tl1 tl2
	with
	  Invalid_argument _ -> raise Unify_failure)
    | TyArray (t1), TyArray (t2) ->
	do_unify env t1 t2
    | TyVar v1, TyVar v2 when Symbol.eq v1 v2 -> env (* optimization only *)
    | TyVar (v1), ty2 | ty2, TyVar (v1) ->
	(try let ty1 = SymbolTable.find env.tvenv v1 in
	  do_unify env ty1 ty2
	with Not_found ->
	  let ty2 = expand_ty env.tvenv ty2 in
	  (match ty2 with TyVar (v2) when Symbol.eq v1 v2 -> env | _ ->
	    insure_tyvar_absent v1 ty2;
	    let tvenv = SymbolTable.add env.tvenv v1 ty2 in
	    if SymbolSet.mem env.menv v1 then
	      let menv = free_tyvars env.menv ty2 in
		{ env with tvenv = tvenv ; menv = menv }
	    else if SymbolSet.mem env.uenv v1 then
	      let uenv = free_tyvars env.uenv ty2 in
		{ env with tvenv = tvenv ; uenv = uenv }
	    else
	      { env with tvenv = tvenv }))
    | TyApply (v1, tl1), TyApply (v2, tl2) ->
	if vpath_eq v1 v2 then
	  (try
	    List.fold_left2 do_unify env tl1 tl2
	  with Unify_failure ->
            (match VPathTable.find env.tenv v1 with
                TyDefLambda (sl, _, ty) ->
                  let st1 = List.fold_left2 SymbolTable.add SymbolTable.empty sl tl1 in
                  let st2 = List.fold_left2 SymbolTable.add SymbolTable.empty sl tl2 in
                  let ty1 = subst_ty st1 ty in
                  let ty2 = subst_ty st2 ty in
                    do_unify env ty1 ty2
              | _ -> raise Unify_failure))
	else
          (match VPathTable.find env.tenv v1 with
               TyDefLambda (modl1, _, ty1) ->
                 let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl1 tl1 in
                 let ty1 = subst_ty st ty1 in
                   do_unify env ty1 ty2
             | _ -> match VPathTable.find env.tenv v2 with
                   TyDefLambda (modl2, _, ty2) ->
                     let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl2 tl2 in
                     let ty2 = subst_ty st ty2 in
                       do_unify env ty1 ty2
                 | _ -> raise Unify_failure)
    | TyApply (v1, tl1), ty2 | ty2, TyApply (v1, tl1) ->
        (match VPathTable.find env.tenv v1 with
            TyDefLambda (modl, _, ty) ->
              let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl tl1 in
              let ty1 = subst_ty st ty in
                do_unify env ty1 ty2
          | _ -> raise Unify_failure)
    | _ -> raise Unify_failure

let unify env pos ty1 ty2 =
  (* Format.printf "Unification:\n  menv = "; SymbolSet.iter (fun s -> print_symbol s; Format.print_string " ") env.menv;
  Format.printf "\n  venv = "; VPathTable.iter (fun (_,s) t -> Format.print_string "("; print_symbol s; Format.print_string " ";
  print_ty (expand_ty env.tvenv t); Format.print_string ") ") env.venv; Format.print_newline (); *)
  try do_unify env ty1 ty2 with Unify_failure ->
    raise (TypeMismatch (pos, expand_ty env.tvenv ty1, expand_ty env.tvenv ty2))

let rec do_unify_into env ty1 ty2 =
  (* print_ty (expand_ty env.tvenv ty1); Format.print_string " with "; 
  print_ty (expand_ty env.tvenv ty2); Format.printf "@\n"; Format.print_newline (); *)
  match ty1, ty2 with
      x, y when x == y -> env
    | TyFun (a1, r1), TyFun (a2, r2) ->
	let rec unify_fun env a1 a2 =
	  match a1, a2 with
	      a1::al1, a2::al2 ->
		let env = do_unify_into env a1 a2 in
		  unify_fun env al1 al2
	    | [], [] -> do_unify_into env r1 r2
            | _, [] -> do_unify_into env (TyFun (a1, r1)) r2
            | [], _ -> do_unify_into env r1 (TyFun (a2, r2)) in
	unify_fun env a1 a2
    | TyTuple (tl1), TyTuple (tl2) ->
	(try
	  List.fold_left2 do_unify_into env tl1 tl2
	with
	  Invalid_argument _ -> raise Unify_failure)
    | TyArray (t1), TyArray (t2) ->
	do_unify_into env t1 t2
    | TyVar v1, TyVar v2 when Symbol.eq v1 v2 -> env (* optimization only *)
    | ty1, TyVar v2 ->
	(try let ty2 = SymbolTable.find env.tvenv v2 in
	  do_unify_into env ty1 ty2
	with Not_found ->
	  let ty1 = expand_ty env.tvenv ty1 in
	  (match ty1 with TyVar v1 when Symbol.eq v1 v2 -> env | _ ->
	    insure_tyvar_absent v2 ty1;
	    let tvenv = SymbolTable.add env.tvenv v2 ty1 in
	    if SymbolSet.mem env.menv v2 then
	      let menv = free_tyvars env.menv ty1 in
		{ env with tvenv = tvenv ; menv = menv }
	    else if SymbolSet.mem env.uenv v2 then
	      let uenv = free_tyvars env.uenv ty1 in
		{ env with tvenv = tvenv ; uenv = uenv }
	    else
	      { env with tvenv = tvenv }))
    | TyVar _, _ -> raise Unify_failure
    | TyApply (v1, tl1), TyApply (v2, tl2) ->
	if vpath_eq v1 v2 then
	  (try
	    List.fold_left2 do_unify_into env tl1 tl2
	  with Unify_failure ->
            (match VPathTable.find env.tenv v1 with
                TyDefLambda (sl, _, ty) ->
		  let st1 = List.fold_left2 SymbolTable.add SymbolTable.empty sl tl1 in
		  let st2 = List.fold_left2 SymbolTable.add SymbolTable.empty sl tl2 in
		  let ty1 = subst_ty st1 ty in 
		  let ty2 = subst_ty st2 ty in 
		    do_unify_into env ty1 ty2
              | _ -> raise Unify_failure))
	else
          (match VPathTable.find env.tenv v1 with
              TyDefLambda (modl1, _, ty1) ->
		let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl1 tl1 in
		let ty1 = subst_ty st ty1 in
		  do_unify_into env ty1 ty2
            | _ -> 
                match VPathTable.find env.tenv v2 with
                    TyDefLambda (modl2, _, ty2) ->
		      let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl2 tl2 in
		      let ty2 = subst_ty st ty2 in
		        do_unify_into env ty1 ty2
                  | _ -> raise Unify_failure)
    | TyApply (v1, tl1), ty2 ->
        (match VPathTable.find env.tenv v1 with
            TyDefLambda (modl, _, ty) ->
              let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl tl1 in
              let ty1 = subst_ty st ty in
                do_unify_into env ty1 ty2
          | _ -> raise Unify_failure)
    | ty1, TyApply (v2, tl2) ->
        (match VPathTable.find env.tenv v2 with
            TyDefLambda (modl, _, ty) ->
              let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl tl2 in
              let ty2 = subst_ty st ty in
                do_unify_into env ty1 ty2
          | _ -> raise Unify_failure)
    | _ -> raise Unify_failure

(* unify ty1 into ty2, so that ty1 \subseteq ty2, essentially a one-directional unification *)
let unify_into env pos ty1 ty2 =
  try do_unify_into env ty1 ty2 with Unify_failure ->
    raise (TypeMismatch (pos, expand_ty env.tvenv ty1, expand_ty env.tvenv ty2))

let rec unify_list env t1 = function
    [] -> env
  | (t2, pos) :: tl -> unify_list (unify env pos t1 t2) t2 tl

let make_tyapply tyv modl =
  let tyvs = List.map new_symbol modl in
  let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl tyvs in
  let ty = TyApply (tyv, List.map (fun x -> TyVar x) tyvs) in
    st, ty

let make_tyapply_bl env tyv modl bl =
  let tyvs = List.map new_symbol modl in
  let st = List.fold_left2 SymbolTable.add SymbolTable.empty modl tyvs in
  let ty = TyApply (tyv, List.map (fun x -> TyVar x) tyvs) in
  let menv = List.fold_left2 (fun menv tv b ->
      if b then menv else SymbolSet.add menv tv)
    env.menv tyvs bl in
  let env = { env with menv = menv } in
    env, st, ty

(********************************************************** INFER *)
let infer_constexpr env cexp =
  match cexp with
      Ast.IntConst (i, _) ->
	env, IntConst (i), TyInt
    | Ast.FloatConst (f, _) ->
	env, FloatConst (f), TyFloat
    | Ast.CharConst (c, _) ->
	env, CharConst (c), TyChar
    | Ast.CConsConst (v, pos) ->
	let v = convert_vpath v in
        (try
	  let tyv, modl, tyl = VPathTable.find env.cenv v in
	  if tyl != [] then
	    raise (TypeCons_Arity (pos, v, List.length tyl, 0));
	  let _, ty = make_tyapply tyv modl in
	    env, CConsConst (v, ty), ty
        with Not_found ->
          let tyl = VPathTable.find env.eenv v in
          if tyl != [] then
            raise (TypeCons_Arity (pos, v, List.length tyl, 0));
          env, CConsConst (v, TyExn), TyExn)

let rec infer_expr env exp = match exp with
    Ast.VarExpr (v, _) ->
      let v = convert_vpath v in
      let ty = VPathTable.find env.venv v in
      let ty = rename_tyvars env ty in
        env, VarExpr (v, ty), ty
  | Ast.ConstExpr (cexp, _) ->
      let env, cexp, ty = infer_constexpr env cexp in
	env, ConstExpr (cexp, ty), ty
  | Ast.TySpecExpr (e, ty, _) ->
      let pos = pos_of_expr e in
      let env, e, tye = infer_expr env e in
      let ty = typify env.tenv ty in
      let env = unify env pos tye ty in
	env, e, tye
  | Ast.TupleExpr (el, _) ->
      let env, el, tl = Mc_list_util.fold_map1to2 infer_expr env el in
      let ty = TyTuple tl in
	env, TupleExpr (el, ty), ty
  | Ast.ConsExpr (v, e, opos) ->
      let v = convert_vpath v in
      let pos = pos_of_expr e in
      let env, e, tye = infer_expr env e in
      (try
        let tyv, modl, tyl = VPathTable.find env.cenv v in
        let st, ty = make_tyapply tyv modl in
        let tyl = List.map (standardize_ty st) tyl in
        (match tyl, e with
  	    [], _ -> raise (TypeCons_Arity (opos, v, 0, -1))
  	  | [ty1], _ ->
	      let env = unify env pos tye ty1 in
	        env, ConsExpr (v, [e], ty), ty
	  | _, TupleExpr (el, tye) ->
	      if not (Mc_list_util.length_eq tyl el) then
	        raise (TypeCons_Arity (opos, v, List.length tyl, List.length el));
	      let env = unify env pos tye (TyTuple (tyl)) in
                env, ConsExpr (v, el, ty), ty
	  | _ ->
	      let env = unify env pos tye (TyTuple (tyl)) in
                env, ConsCopyExpr (v, e, ty), ty)
      with Not_found ->
        let tyl = VPathTable.find env.eenv v in
        (match tyl, e with
            [], _ -> raise (TypeCons_Arity (opos, v, 0, -1))
          | [ty1], _ ->
              let env = unify env pos tye ty1 in
                env, ConsExpr (v, [e], TyExn), TyExn
          | _, TupleExpr (el, tye) ->
              if not (Mc_list_util.length_eq tyl el) then
                raise (TypeCons_Arity (opos, v, List.length tyl, List.length el));
              let env = unify env pos tye (TyTuple (tyl)) in
                env, ConsExpr (v, el, TyExn), TyExn
          | _ ->
              let env = unify env pos tye (TyTuple (tyl)) in
                env, ConsCopyExpr (v, e, TyExn), TyExn))
  | Ast.ArrayExpr (el, pos) ->
      let env, el, tpl = Mc_list_util.fold_map1to2 (fun env e ->
	  let pos = pos_of_expr e in
	  let env, e, ty = infer_expr env e in
	    env, e, (ty, pos))
	env el in
      (match tpl with
	  [] ->
            let ty = TyArray (TyVar (new_symbol_string "a")) in
              env, ArrayExpr ([], ty), ty
	| (t,_) :: l -> 
	    let t = expand_ty env.tvenv t in
	    let env = { env with menv = free_tyvars env.menv t } in
            let env = unify_list env t l in
	    let ty = TyArray t in
	      env, ArrayExpr (el, ty), ty)
  | Ast.RecordExpr (we, vel, pos) ->
      let rv, modl, _, _ = VPathTable.find env.fenv (convert_vpath (fst (List.hd vel))) in
      let fl = match VPathTable.find env.tenv rv with
          TyDefRecord (_, _, fl) -> fl
        | _ -> raise (Impossible "non-record in fenv") in
      let st, ty = make_tyapply rv modl in
      let fet = List.fold_left (fun fet ((_, s, fpos) as v, e) ->
	  let v = convert_vpath v in
	  let rv', _, _, _ = VPathTable.find env.fenv v in
	  if vpath_eq rv rv' then
            SymbolTable.filter_add fet s (function
                None -> e
              | Some _ -> raise (MultipleBindings_Field (pos, s)))
	  else
	    raise (FieldMismatch (fpos, v, rv', rv)))
	SymbolTable.empty vel in
      (match we with
	  None ->
	    let env, el = Mc_list_util.fold_map (fun env (_, f, fty) ->
		let fty = standardize_ty st fty in
		try
		  let e = SymbolTable.find fet f in
		  let pos = pos_of_expr e in
		  let env, e, ty = infer_expr env e in
		  let env = unify env pos ty fty in
		    env, e
		with
		  Not_found -> raise (IncompleteRecord (pos, f)))
	      env fl 
            in
              env, RecordExpr (el, ty), ty
	| Some src ->
	    let srcpos = pos_of_expr src in
	    let env, src, srcty = infer_expr env src in
	    let env = unify env srcpos srcty ty in
	    let fvl = List.map (fun (_, f, _) -> new_symbol f) fl in
	    let fl = List.map (fun (x,y,ty) -> x,y,standardize_ty st ty) fl in
	    let env, el = Mc_list_util.fold_map2to1 (fun env (_, f, fty) fv ->
		try
		  let e = SymbolTable.find fet f in
		  let pos = pos_of_expr e in
		  let env, e, ty = infer_expr env e in
		  let env = unify env pos ty fty in
		    env, e
		with
		  Not_found ->
		    env, VarExpr (([], fv), fty))
	      env fl fvl 
            in
              env, MatchExpr (src, [
		RecordPat (Mc_list_util.map2to1 (fun (_,_,ty) fv -> IdPat (fv, ty)) fl fvl, ty),
		None,
		RecordExpr (el, ty)
	      ], ty), ty)
  | Ast.ApplyExpr (fe, al, _) ->
      let fpos = pos_of_expr fe in
      let env, fe, fty = infer_expr env fe in
      let env, al, atl = Mc_list_util.fold_map1to2 infer_expr env al in
      let env = taint_tyl env atl in
      let rty = TyVar (new_symbol_string "b") in
      let env = unify env fpos fty (TyFun (atl, rty)) in
	env, ApplyExpr (fe, al, rty), rty
  | Ast.ProjExpr (re, fv, _) ->
      let fv = convert_vpath fv in
      let pos = pos_of_expr re in
      let env, re, rty = infer_expr env re in
      let typ, modl, _, fty = VPathTable.find env.fenv fv in
      let st, ty = make_tyapply typ modl in
      let env = unify env pos rty ty in
      let fty = standardize_ty st fty in
	env, ProjExpr (re, fv, fty), fty
  | Ast.AssignProjExpr (re, fv, e, pos) ->
      let fv = convert_vpath fv in
      let rpos = pos_of_expr re in
      let epos = pos_of_expr e in
      let env, re, rty = infer_expr env re in
      let env, e, ety = infer_expr env e in
      let typ, modl, mut, fty = VPathTable.find env.fenv fv in
      if not mut then
	raise (FieldMutability (pos, fv));
      let st, ty = make_tyapply typ modl in
      let env = unify env rpos rty ty in
      let fty = standardize_ty st fty in
      let env = unify env epos ety fty in
	env, AssignProjExpr (re, fv, e, unit_ty), unit_ty
  | Ast.ArrayEltExpr (ae, oe, _) ->
      let opos = pos_of_expr oe in
      let apos = pos_of_expr ae in
      let env, ae, aty = infer_expr env ae in
      let env, oe, oty = infer_expr env oe in
      let ty = TyVar (new_symbol_string "a") in
      let env = unify env apos aty (TyArray ty) in
      let env = unify env opos oty TyInt in
	env, ArrayEltExpr (ae, oe, ty), ty
  | Ast.AssignArrayEltExpr (ae, oe, e, _) ->
      let opos = pos_of_expr oe in
      let apos = pos_of_expr ae in
      let epos = pos_of_expr e in
      let env, ae, aty = infer_expr env ae in
      let env, oe, oty = infer_expr env oe in
      let env, e, ety = infer_expr env e in
      let ty = TyVar (new_symbol_string "a") in
      let env = unify env apos aty (TyArray ty) in
      let env = unify env opos oty TyInt in
      let env = unify env epos ety ty in
	env, ArrayEltExpr (ae, oe, unit_ty), unit_ty
  | Ast.IfExpr (ie, te, eeo, pos) ->
      let ipos = pos_of_expr ie in
      let tpos = pos_of_expr te in
      let env, ie, ity = infer_expr env ie in
      let env, te, tty = infer_expr env te in
      let env, ee, ety = match eeo with
	  Some ee -> infer_expr env ee
	| None -> env, unit_expr, unit_ty in
      let env = unify env ipos ity bool_ty in
      let env = unify env tpos tty ety in
	env, MatchExpr (ie, [true_pat, None, te; false_pat, None, ee], tty), tty
  | Ast.WhileExpr (we, de, _) ->
      let wpos = pos_of_expr we in
      let dpos = pos_of_expr de in
      let env, we, wty = infer_expr env we in
      let env, de, dty = infer_expr env de in
      let env = unify env wpos wty bool_ty in
      let env = unify env dpos dty unit_ty in
	env, WhileExpr (we, de, unit_ty), unit_ty
  | Ast.ForExpr (iv, oe, dir, ne, e, _) ->
      let opos = pos_of_expr oe in
      let npos = pos_of_expr ne in
      let epos = pos_of_expr e in
      let env, oe, oty = infer_expr env oe in
      let env, ne, nty = infer_expr env ne in
      let env, e, ety = infer_expr { env with venv = VPathTable.add env.venv ([], iv) TyInt } e in
      let env = unify env opos oty TyInt in
      let env = unify env npos nty TyInt in
      let env = unify env epos ety unit_ty in 
	env, ForExpr (iv, oe, dir, ne, e, unit_ty), unit_ty
  | Ast.SeqExpr (e1, e2, _) ->
      let e1pos = pos_of_expr e1 in
      let env, e1, e1ty = infer_expr env e1 in
      let env = unify env e1pos e1ty unit_ty in
      let env, e2, ty = infer_expr env e2 in
        env, SeqExpr (e1, e2, ty), ty
  | Ast.GroupExpr (e, _) ->
      infer_expr env e
  | Ast.FunExpr (al, e, pos) -> (* fun al -> e *)
      let uenv = env.uenv in
      let menv = env.menv in
      let env, al = Mc_list_util.fold_map
        (fun env a ->
          let tv = new_symbol a in
          let ty = TyVar tv in
          let env = { env with uenv = SymbolSet.add env.uenv tv; venv = VPathTable.add env.venv ([], a) ty } in
            env, (a, ty)) env al in
      let env, e, ty = infer_expr env e in
      let ty = TyFun (List.map snd al, ty) in
      let env = { env with uenv = uenv ; menv = menv } in
        env, FunExpr (al, e, ty), ty
  | Ast.MatchExpr (e, pm, pos) ->
      let env, e, ty = infer_expr env e in 
      let env = taint_ty env ty in
      let env, pm, tpl = Mc_list_util.fold_map1to2 (infer_pmatch ty) env pm in
      (match tpl with
          [] -> raise (Impossible "empty pmatch")
        | (ty, _) :: tpl ->
            let env = unify_list env ty tpl in
              env, MatchExpr (e, pm, ty), ty)
  | Ast.TryExpr (e, pm, pos) ->
      let env, e, ty = infer_expr env e in
      let env, pm, tpl = Mc_list_util.fold_map1to2 (infer_pmatch TyExn) env pm in
      let env = unify_list env ty tpl in
        env, TryExpr (e, pm, ty), ty
  | Ast.LetExpr (recf, bindings, e, pos) ->
      if recf then   (* this code is not necessarily correct if the expressions are not functions *)
	let uenv = env.uenv in 
        let note_binding env (p, _, _) = match p with
            Ast.IdPat (v, _) ->
              let tv = new_symbol v in
              let ty = TyVar tv in
              let env = { env with uenv = SymbolSet.add env.uenv tv; venv = VPathTable.add env.venv ([], v) ty } in
                env, (v, ty)
          | _ -> raise (Invalid_RecPattern (pos_of_pattern p)) in
        let infer_binding env (v, ty) (_, e, _) =
          let pos = pos_of_expr e in
          let env, e, ety = infer_expr env e in
          let env = unify env pos ety ty in
	  (match e with
	    FunExpr (stl, e, fty) -> 
	      env, (v, stl, e, fty)
	    | _ -> raise (Invalid_RecExpr pos)) in
        let env, vl = Mc_list_util.fold_map note_binding env bindings in
        let env, bl = Mc_list_util.fold_map2to1 infer_binding env vl bindings in
        let env = { env with uenv = uenv } in
        let env, e, ty = infer_expr env e in
          env, LetRecExpr (bl, e, ty), ty
      else
        let rec do_lr env = function
            [] ->  infer_expr env e
          | (p, e, _) :: bl ->
              let env, p, pty = infer_pattern env p in
              let pos = pos_of_expr e in
              let env, e, ety = infer_expr env e in
              let env = taint_ty env ety in
              let env = unify env pos ety pty in
              let env, e2, ty = do_lr env bl in
                env, MatchExpr (e, [p, None, e2], ty), ty
        in 
          do_lr env bindings

and infer_pmatch ty env (p, we, e) =
  let pos = pos_of_pattern p in
  let env, p, pty = infer_pattern env p in
  let env = unify env pos pty ty in
  let env, we = match we with
      None -> env, None
    | Some we ->
        let pos = pos_of_expr we in
        let env, we, wty = infer_expr env we in
        let env = unify env pos wty bool_ty in
          env, Some we in
  let epos = pos_of_expr e in
  let env, e, ety = infer_expr env e in
    env, (p, we, e), (ety,epos)
  
and infer_pattern env pat = match pat with
    Ast.IdPat (v, pos) ->
      let vp = [], v in
      (try
        let ty = VPathTable.find env.venv vp in
          env, IdPat (v, ty), ty
      with Not_found ->
        let ty = TyVar (new_symbol v) in
        let env = { env with venv = VPathTable.add env.venv vp ty } in
          env, IdPat (v, ty), ty)
  | Ast.TySpecPat (pat, ty, pos) ->
      let env, pat, ty2 = infer_pattern env pat in
      let ty = typify env.tenv ty in
      let env = unify env pos ty2 ty in
	env, pat, ty
  | Ast.AnyPat pos ->
      let ty = TyVar (new_symbol_string "a") in
	env, AnyPat (ty), ty
  | Ast.ConstPat (cexp, pos) ->
      let env, cexp, ty = infer_constexpr env cexp in
	env, ConstPat (cexp, ty), ty
  | Ast.RangePat (cexp1, cexp2, pos) ->
      let pos = pos_of_const cexp2 in
      let env, cexp1, ty1 = infer_constexpr env cexp1 in
      let env, cexp2, ty2 = infer_constexpr env cexp2 in
      let env = unify env pos ty2 ty1 in
	env, RangePat (cexp1, cexp2, ty1), ty1
  | Ast.AsPat (pat, v, pos) ->
      let env, pat, ty = infer_pattern env pat in
      let vp = [], v in
      (try
        let ty2 = VPathTable.find env.venv vp in 
        let env = unify env pos ty2 ty in
          env, AsPat (pat, v, ty2), ty2
      with Not_found ->
        let env = { env with venv = VPathTable.add env.venv vp ty } in
	  env, AsPat (pat, v, ty), ty)
  | Ast.AltPat (pat1, pat2, pos) ->
      let pos = pos_of_pattern pat2 in
      let env, pat1, ty1 = infer_pattern env pat1 in
      let env, pat2, ty2 = infer_pattern env pat2 in
      let env = unify env pos ty2 ty1 in
	env, AltPat (pat1, pat2, ty1), ty1
  | Ast.ConsPat (v, pat, opos) ->
      let v = convert_vpath v in
      let pos = pos_of_pattern pat in
      let env, pat, typ = infer_pattern env pat in
      (try
        let tyv, modl, tyl = VPathTable.find env.cenv v in
        let st, ty = make_tyapply tyv modl in
        let tyl = List.map (standardize_ty st) tyl in
        (match tyl, pat with
	    [], _ -> raise (TypeCons_Arity (opos, v, 0, -1))
	  | [ty1], _ ->
	      let env = unify env pos typ ty1 in
	        env, ConsPat (v, [pat], ty), ty
	  | _, TuplePat (pl, typ) ->
	      if not (Mc_list_util.length_eq tyl pl) then
	        raise (TypeCons_Arity (opos, v, List.length tyl, List.length pl));
	      let env = unify env pos typ (TyTuple (tyl)) in
	        env, ConsPat (v, pl, ty), ty
	  | _ ->
	      let env = unify env pos typ (TyTuple (tyl)) in
	        env, ConsCopyPat (v, pat, ty), ty)
      with Not_found ->
        let tyl = VPathTable.find env.eenv v in
        (match tyl, pat with
            [], _ -> raise (TypeCons_Arity (opos, v, 0, -1))
          | [ty1], _ ->
              let env = unify env pos typ ty1 in
                env, ConsPat (v, [pat], TyExn), TyExn
          | _, TuplePat (pl, typ) ->
              if not (Mc_list_util.length_eq tyl pl) then
                raise (TypeCons_Arity (opos, v, List.length tyl, List.length pl));
              let env = unify env pos typ (TyTuple (tyl)) in
                env, ConsPat (v, pl, TyExn), TyExn
          | _ ->
              let env = unify env pos typ (TyTuple (tyl)) in
                env, ConsCopyPat (v, pat, TyExn), TyExn))
  | Ast.TuplePat (pl, pos) ->
      let env, pl, tl = Mc_list_util.fold_map1to2 infer_pattern env pl in
      let ty = TyTuple (tl) in
	env, TuplePat (pl, ty), ty
  | Ast.ArrayPat (pl, pos) ->
      (match pl with
          [] -> 
            let ty = TyArray (TyVar (new_symbol_string "a")) in
              env, ArrayPat ([], ty), ty
        | ph :: pt ->
            let env, ph, tyh = infer_pattern env ph in
            let env, pl, tpl = Mc_list_util.fold_map1to2 (fun env p -> 
               let pos = pos_of_pattern p in 
               let env, p, ty = infer_pattern env p in
                 env, p, (ty, pos)) env pl in
            let env = unify_list env tyh tpl in
	    let ty = TyArray (tyh) in
              env, ArrayPat (ph :: pl, ty), ty)
  | Ast.RecordPat (vpl, pos) ->
      let rv, modl, _, _ = VPathTable.find env.fenv (convert_vpath (fst (List.hd vpl))) in
      let fl = match VPathTable.find env.tenv rv with
          TyDefRecord (_, _, fl) -> fl
        | _ -> raise (Impossible "non-record in fenv") in
      let st, ty = make_tyapply rv modl in
      let fpt = List.fold_left (fun fpt ((_, s, fpos) as v, p) ->
	  let v = convert_vpath v in
	  let rv', _, _, _ = VPathTable.find env.fenv v in
	  if vpath_eq rv rv' then
            SymbolTable.filter_add fpt s (function
                None -> p
              | Some _ -> raise (MultipleBindings_Field (pos, s)))
	  else
	    raise (FieldMismatch (fpos, v, rv', rv)))
	SymbolTable.empty vpl in
      let env, pl = Mc_list_util.fold_map (fun env (_, f, fty) ->
	  let fty = standardize_ty st fty in
	  try
	    let p = SymbolTable.find fpt f in
	    let pos = pos_of_pattern p in
	    let env, p, ty = infer_pattern env p in
	    let env = unify env pos ty fty in
	      env, p
	  with
	    Not_found -> env, AnyPat fty)
	env fl in
      env, RecordPat (pl, ty), ty

let infer_def env def =
  match def with
      Ast.LetMod (recf, bindings, pos) ->
        if recf then  (* this code is not necessarily correct if the expressions are not functions *)
          let note_binding env (p, _, _) = match p with
              Ast.IdPat (v, _) ->
                let tv = new_symbol v in
                let ty = TyVar tv in
                let env = { env with uenv = SymbolSet.add env.uenv tv; venv = VPathTable.add env.venv ([], v) ty } in
                  env, (v, ty)
            | _ -> raise (Invalid_RecPattern (pos_of_pattern p)) in
          let infer_binding env (v, ty) (_, e, _) =
            let pos = pos_of_expr e in
            let env, e, ety = infer_expr env e in
	    let env = unify env pos ety ty in
	    (match e with
	      FunExpr (stl, e, fty) -> 
		env, (v, stl, e, fty)
	      | _ -> raise (Invalid_RecExpr pos)) in
          let env, vl = Mc_list_util.fold_map note_binding env bindings in
          let env, bl = Mc_list_util.fold_map2to1 infer_binding env vl bindings in
          let tvenv = env.tvenv in 
          let env = { env with uenv = SymbolSet.empty } in
            env, LetRecMod bl
        else
          let env, bl = Mc_list_util.fold_map (fun env (p, e, pos) ->
            let env, p, pty = infer_pattern env p in
            let pos = pos_of_expr e in
            let env, e, ety = infer_expr env e in
            let env = taint_ty env ety in
            let env = unify env pos ety pty in
            let env = { env with uenv = SymbolSet.empty } in
              env, (p, e)) env bindings
          in
            env, LetMod bl
    | Ast.ExternMod (v, t, s, p) ->
	let t = typify env.tenv t in
	let et = type_of_extern s in
	let env = unify_into env p t et in
	let t = expand_ty env.tvenv t in
        let env = { env with venv = VPathTable.add env.venv ([], v) t; uenv = SymbolSet.empty } in
	  env, ExternMod (v, t, s)
    | Ast.ExternTypeMod (sl', s, e, pos) ->
        (match extern_tydef e with
            ExternTySimp (sl, t) ->
              if not (Mc_list_util.length_eq sl' sl) then
                raise (TypeCons_Arity (pos, ([], s), List.length sl', List.length sl));
              let d = TyDefLambda (sl, List.map (fun _ -> false) sl, t) in
              let env = { env with tenv = VPathTable.add env.tenv ([], s) d; uenv = SymbolSet.empty } in
                env, TypeMod [s, d]
          | ExternTyCons (ss, sl, cl) ->
              let vs = [], ss in
              if not (Mc_list_util.length_eq sl' sl) then
                raise (TypeCons_Arity (pos, vs, List.length sl', List.length sl));
              let ty = TyApply (vs, List.map (fun x -> TyVar x) sl') in
              let d = TyDefLambda (sl', List.map (fun _ -> false) sl, ty) in
              let td = s, d in
              let tenv = VPathTable.add env.tenv ([], s) d in
              let env, tdl = 
                if VPathTable.mem tenv vs then
                  { env with tenv = tenv; uenv = SymbolSet.empty }, [td]
	        else
                  let d = TyDefUnion (sl, List.map (fun _ -> false) sl, cl) in
                  let tenv = VPathTable.add tenv vs d in
                  let cenv = List.fold_left (fun cenv (c, tl) -> VPathTable.add cenv ([], c) (vs, sl, tl)) env.cenv cl in
                  let env = { env with tenv = tenv; cenv = cenv; uenv = SymbolSet.empty } in
                    env, [ss, d; td]
              in
                env, TypeMod tdl)
    | Ast.TypeMod (tdl, pos) ->
	let tenv = List.fold_left (fun tenv (sl, s, _, _, _) -> VPathTable.add tenv ([], s) (TyDefLambda (sl, [], TyInt))) env.tenv tdl in
        let env, tl = Mc_list_util.fold_map (fun env (sl, s, t, tr, pos) ->
            let vs = [], s in
            match t, tr with
                Some t, None ->
                  env, TyDefLambda ([], [], typify tenv t)
              | None, Some (Ast.TypeCons (stll, _)) ->
                  let cenv, stll = Mc_list_util.fold_map (fun cenv (s, tl) ->
                      let tl = List.map (typify tenv) tl in 
                        VPathTable.add cenv ([], s) (vs, sl, tl), (s, tl))
                    env.cenv stll in
                  let env = { env with cenv = cenv } in
                    env, TyDefUnion ([], [], stll)
              | None, Some (Ast.TypeRecord (bstl, _)) ->
                  let fenv, bstl = Mc_list_util.fold_map (fun fenv (m, s, t) ->
                      let t = typify tenv t in
                        VPathTable.add fenv ([], s) (vs, sl, m, t), (m, s, t))
                    env.fenv bstl in
                  let env = { env with fenv = fenv } in
                    env, TyDefRecord ([], [], bstl)
              | _ -> raise (NotImplemented "weird type definitions"))
           env tdl in
        let step bs =
          let rec taint bs = function
              TyInt | TyChar | TyFloat | TyExn | TyFun _ | TyVar _ -> bs 
            | TyTuple tl -> List.fold_left taint bs tl
            | TyArray t -> free_tyvars bs t
            | TyApply (v, tl) ->
                let sl, _ = tydef_info (VPathTable.find tenv v) in
                  List.fold_left2 (fun bs s t -> if SymbolSet.mem bs s then free_tyvars bs t else bs) bs sl tl in
          let taint_def bs = function
              TyDefLambda (_, _, t) -> taint bs t
            | TyDefUnion (_, _, cl) -> List.fold_left (fun bs (_, t) -> List.fold_left taint bs t) bs cl
            | TyDefRecord (_, _, fl) -> List.fold_left (fun bs (m, _, t) -> if m then free_tyvars bs t else taint bs t) bs fl
          in
            List.fold_left taint_def bs tl in
        let rec iterate n bs =
          let bs = step bs in
          let m = SymbolSet.cardinal bs in
            if n < m then
              iterate m bs
            else
              bs in
        let bs = iterate 0 SymbolSet.empty in
        let tenv, tdl = Mc_list_util.fold_map2to1 (fun tenv (sl, s, _, _, _) t ->
            let bl = List.map (SymbolSet.mem bs) sl in
            let td = match t with
                TyDefLambda (_, _, t) -> TyDefLambda (sl, bl, t)
              | TyDefUnion (_, _, cl) -> TyDefUnion (sl, bl, cl)
              | TyDefRecord (_, _, fl) -> TyDefRecord (sl, bl, fl) in
            let tenv = VPathTable.add tenv ([], s) td in
              tenv, (s, td)) 
          env.tenv tdl tl in
        let env = { env with tenv = tenv; uenv = SymbolSet.empty } in
          env, TypeMod tdl  
    | Ast.ExnMod (ed, pos) ->
        (match ed with
            Ast.ExnVar (s, v, p) ->
              let v' = convert_vpath v in
              let env = { env with eenv = VPathTable.add env.eenv ([], s) (VPathTable.find env.eenv v'); uenv = SymbolSet.empty } in
                env, ExnMod (ExnVar (s, v'))
          | Ast.ExnCons (s, tl, p) -> (* exception s of tl *)
              let tl = List.map (typify env.tenv) tl in
              let env = { env with eenv = VPathTable.add env.eenv ([], s) tl; uenv = SymbolSet.empty } in
                env, ExnMod (ExnCons (s, tl)))
    | _ -> raise (NotImplemented "module type inference")

let rec add_required_types env prog types =
  match types with
      [] -> env, prog
    | e :: types -> 
        match extern_tydef e with
            ExternTySimp _ -> raise (Impossible "we don't have any Simple required types")
          | ExternTyCons (ss, _, cl) ->
              let vs = [], ss in
	      let d = TyDefUnion ([], [], cl) in
	      let tenv = VPathTable.add env.tenv vs d in
	      let cenv = List.fold_left (fun cenv (c, tl) -> VPathTable.add cenv ([], c) (vs, [], tl)) env.cenv cl in
	      let env = { env with tenv = tenv; cenv = cenv; uenv = SymbolSet.empty } in
	      add_required_types env (TypeMod [ss, d] :: prog) types

let infer prog =
  let env = {
    tenv = VPathTable.empty;
    tvenv = SymbolTable.empty;
    cenv = VPathTable.empty;
    eenv = VPathTable.empty;
    fenv = VPathTable.empty;
    venv = VPathTable.empty;
    menv = SymbolSet.empty;
    uenv = SymbolSet.empty;
  } in
  let env, prog = Mc_list_util.fold_map infer_def env prog in
  let env, prog = add_required_types env prog Naml_type_boot.required_types in
  let menv = SymbolTable.fold (fun menv s _ -> SymbolSet.remove menv s) env.menv env.tvenv in
  let tvenv = SymbolSet.fold (fun tvenv m -> SymbolTable.add tvenv m TyInt) env.tvenv menv in (* TyInt is of course arbitrary *)
  let prog = List.map (subst_def tvenv) prog in
    prog
