(*
 * Type Environment utils
 * 2001-04-11
 *)

open Naml_tast

module MPathTable : Mc_map.McMap with type key = mpath
module VPathTable : Mc_map.McMap with type key = vpath

