(*
 * Utilities for working on syntax.
 * Geoffrey Irving / Dylan Simon
 * 12apr01
 *)

open Symbol
open Naml_tast

val vpath_eq : vpath -> vpath -> bool

val type_of_expr : expr -> ty
val type_of_pattern : pattern -> ty
val tydef_info : tydef -> symbol list * bool list

(*
 * Printing
 *)
val print_vpath : vpath -> unit
val print_ty : ty -> unit
val print_expr : expr -> unit
val print_prog : prog -> unit
val print_prog_sig : prog -> unit
