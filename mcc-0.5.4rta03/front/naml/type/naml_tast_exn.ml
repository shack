(*
 * Naml type ast exceptions
 * Geoffrey Irving
 * 12apr01
 *)

open Format
open Symbol
open Naml_tast
open Naml_tast_util

(*
 * BUG: until we fix this module.
 *)
let print_symbol = pp_print_symbol Format.std_formatter

exception ParseError =     Naml_ast_exn.ParseError
exception NotImplemented = Naml_ast_exn.NotImplemented
exception Impossible =     Naml_ast_exn.Impossible

exception MultipleBindings_Field = Naml_ast_exn.MultipleBindings_Field

exception FieldMismatch of pos * vpath * vpath * vpath
exception FieldMutability of pos * vpath
exception IncompleteRecord of pos * symbol
exception Invalid_Extern of string
exception Invalid_ExternType of string
exception Invalid_RecExpr of pos
exception Invalid_RecPattern of pos
exception TypeCons_Arity of pos * vpath * int * int
exception TypeMismatch of pos * ty * ty

let print_pos = Naml_ast_exn.print_pos

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
let print_exn = function
    TypeCons_Arity (pos, v, i, j) ->
      print_pos pos;
      Format.print_string ": The constructor ";
      print_vpath v;
      if j < 0 then
        Format.print_string " is constant, but is here used as a nonconstant constructor"
      else
        Format.printf " expects %d arguments, but is here applied to %d arguments" i j
  | Invalid_Extern s ->
      Format.printf "Invalid external value string: \"%s\"" (String.escaped s)
  | Invalid_ExternType s ->
      Format.printf "Invalid external type string: \"%s\"" (String.escaped s)
  | TypeMismatch (pos, t1, t2) ->
      print_pos pos;
      Format.print_string ": This expression has type";
      Format.print_space ();
      print_ty t1;
      Format.print_newline ();
      Format.print_string "but is used with type";
      Format.print_space ();
      print_ty t2
  | FieldMismatch (pos, f, t1, t2) ->
      print_pos pos;
      Format.print_string ": The field ";
      print_vpath f;
      Format.print_string " is a member of type ";
      print_vpath t1;
      Format.print_string " but is mixed with type ";
      print_vpath t2
  | FieldMutability (pos, f) ->
      print_pos pos;
      Format.print_string ": The field ";
      print_vpath f;
      Format.print_string " is not mutable"
  | Invalid_RecExpr (pos) ->
      print_pos pos;
      Format.print_string ": This type of expression is not allowed in a let rec"
  | Invalid_RecPattern (pos) ->
      print_pos pos;
      Format.print_string ": This type of pattern is not allowed in a let rec"
  | IncompleteRecord (pos, f) ->
      print_pos pos;
      Format.print_string ": The field ";
      print_symbol f;
      Format.print_string " is not defined"

  | exn ->
      Naml_ast_exn.print_exn exn

let print_exn_chan out exn =
   Format.set_formatter_out_channel out;
   print_exn exn;
   Format.print_newline ()

let catch f x =
   try f x with
      exn ->
         print_exn_chan stderr exn;
         exit 1
