(*
 * Pervasives_boot stuff
 * Geoffrey Irving
 * 8apr1
 *)

open Symbol
open Naml_tast
open Naml_parse_misc
open Naml_tast_exn

let unit_ty_sym = Symbol.add "unit"
let bool_ty_sym = Symbol.add "bool"
let list_ty_sym = Symbol.add "list"

let unit_ty = TyApply (([], unit_ty_sym), [])
let bool_ty = TyApply (([], bool_ty_sym), [])

let unit_const = CConsConst (([], unit_sym), unit_ty)
let false_const = CConsConst (([], false_sym), bool_ty)
let true_const = CConsConst (([], true_sym), bool_ty)

let unit_expr = ConstExpr (unit_const, unit_ty)
let false_pat = ConstPat (false_const, bool_ty)
let true_pat = ConstPat (true_const, bool_ty)

let required_types = ["%unit"; "%bool"]

type extern_ty =
    ExternTySimp of symbol list * ty
  | ExternTyCons of symbol * symbol list * (symbol * ty list) list

let extern_tydef = function
    "%int"    -> ExternTySimp ([], TyInt)
  | "%char"   -> ExternTySimp ([], TyChar)
  | "%float"  -> ExternTySimp ([], TyFloat)
  | "%exn"    -> ExternTySimp ([], TyExn)
  | "%string" -> ExternTySimp ([], TyArray TyChar)
  | "%array"  ->
      let a = new_symbol_string "a" in
        ExternTySimp ([a], TyArray (TyVar a))
  | "%unit"   -> ExternTyCons (unit_ty_sym, [], [unit_sym, []])
  | "%bool"   -> ExternTyCons (bool_ty_sym, [], [false_sym,[]; true_sym,[]])
  | "%list"   ->
      let a = new_symbol_string "a" in
      let ta = TyVar a in
        ExternTyCons (list_ty_sym, [a], [list_nil_sym, []; list_cons_sym, [ta; TyApply (([],list_ty_sym),[ta])]])
  | "%format" -> ExternTySimp (List.map new_symbol_string ["a"; "b"; "c"], TyInt)  (* NOT PARTICULARLY CLEAN *)
  | s -> raise (Invalid_ExternType s)

let type_of_extern s =
  let ntv s = TyVar (new_symbol_string s) in
  match s with
    | "%raise" -> let a = ntv "a" in TyFun ([TyExn], a)
    | "%exit" -> let a = ntv "a" in TyFun ([TyInt], a)
    | "%fst" -> let b = ntv "b" in let a = ntv "a" in TyFun ([TyTuple [a; b]], a)
    | "%snd" -> let b = ntv "b" in let a = ntv "a" in TyFun ([TyTuple [a; b]], b)

    | "%cmp_eq" | "%cmp_equal" | "%cmp_ge" | "%cmp_gt" | "%cmp_le"
    | "%cmp_lt" | "%cmp_neq" | "%cmp_nequal" -> let a = ntv "a" in TyFun ([a; a], bool_ty)
    | "%cmp_compare" -> let a = ntv "a" in TyFun ([a; a], TyInt)

    | "%int_max" | "%int_min" -> TyInt

    | "%int_neg" | "%int_pred" | "%int_succ" | "%int_abs" | "%int_not" -> TyFun ([TyInt], TyInt)
    | "%int_and" | "%int_mod" | "%int_mul" | "%int_xor" | "%int_add" | "%int_div" | "%int_or"
    | "%int_sar" | "%int_shl" | "%int_shr" | "%int_sub" -> TyFun ([TyInt; TyInt], TyInt)

    | "%float_mod" | "%float_mul" | "%float_power" | "%float_atan2" | "%float_add" | "%float_div"
    | "%float_sub" -> TyFun ([TyFloat; TyFloat], TyFloat)

    | "%float_abs" | "%float_floor" | "%float_log10" | "%float_neg" | "%float_acos" | "%float_asin"
    | "%float_atan" | "%float_ceil" | "%float_cosh" | "%float_sinh" | "%float_sqrt" | "%float_tanh"
    | "%float_cos" | "%float_exp" | "%float_log" | "%float_sin"
    | "%float_tan" -> TyFun ([TyFloat], TyFloat)

    | "%float_rexp" -> TyFun ([TyFloat], TyTuple [TyFloat; TyInt])
    | "%float_ldexp" -> TyFun ([TyFloat; TyInt], TyFloat)
    | "%float_modf" -> TyFun ([TyFloat], TyTuple [TyFloat; TyFloat])
    | "%char_int" -> TyFun ([TyChar], TyInt)
    | "%float_int" -> TyFun ([TyFloat], TyInt)
    | "%float_string" -> TyFun ([TyFloat], TyArray TyChar)
    | "%int_char" -> TyFun ([TyInt], TyChar)
    | "%int_float" -> TyFun ([TyInt], TyFloat)
    | "%int_string" -> TyFun ([TyInt], TyArray TyChar)
    | "%string_float" -> TyFun ([TyArray TyChar], TyFloat)
    | "%string_int" -> TyFun ([TyArray TyChar], TyInt)
    | "%array_length" -> let a = ntv "a" in TyFun ([TyArray a], TyInt)
    | "%array_get" -> let a = ntv "a" in TyFun ([TyArray a; TyInt], a)
    | "%array_set" -> let a = ntv "a" in TyFun ([TyArray a; TyInt; a], unit_ty)
    | "%array_create" -> let a = ntv "a" in TyFun ([TyInt; a], TyArray a)
    | "%array_append" -> let a = ntv "a" in TyFun ([TyArray a; TyArray a], TyArray a)
    | "%array_sub" -> let a = ntv "a" in TyFun ([TyArray a; TyInt; TyInt], TyArray a)
    | "%array_copy" -> let a = ntv "a" in TyFun ([TyArray a], TyArray a)
    | "%array_fill" -> let a = ntv "a" in TyFun ([TyArray a; TyInt; TyInt; a], unit_ty)
    | "%array_blit" -> let a = ntv "a" in TyFun ([TyArray a; TyInt; TyArray a; TyInt; TyInt], unit_ty)

    (* stuff in the evaluator *)
    | "%print_string" -> TyFun ([TyArray TyChar], unit_ty);
    | "%print_char" -> TyFun ([TyChar], unit_ty)
    | "%print_int" -> TyFun ([TyInt], unit_ty)
    | "%output_byte" -> TyFun ([TyInt], unit_ty)

    | s -> raise (Invalid_Extern s)
