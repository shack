(*
 vim:sts=4:sw=4
 * Type-inference tester 
 * Not a toploop
 *
 * Author: Jason Hickey
 *
 * Modified for naml by Dylan Simon
 *)

(*
 * Arguments.
 *)
let print_ast = ref false

let usage = "usage: type_test [files...]"

(*
 * Compilation.
 *)
let compile name =
   let inx = open_in name in
   try
     Naml_parse_state.set_current_position (name, 1, 0, 1, 0);
     let lexbuf = Lexing.from_channel inx in
     let prog = Naml_parse.prog Naml_lex.main lexbuf in
     Parsing.clear_parser ();
     let prog = Naml_ast_standardize.standardize_prog prog in
     let prog = Naml_type_infer.infer prog in
     Format.printf "Done with inference@\n";
       Naml_tast_util.print_prog prog;
       Format.print_newline ();
       Format.printf "@\nSUMMARY:\n\n";
       Naml_tast_util.print_prog_sig prog;
       Format.print_newline ();
   with
      exn ->
         Naml_tast_exn.print_exn_chan stderr exn;
         close_in inx;
         exit 1

let _ = Arg.parse [] compile usage
