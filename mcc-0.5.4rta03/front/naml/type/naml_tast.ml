(* Typed abstract syntax for naml
 * Geoffrey Irving
 * 20jan00
 * vim:sts=4:sw=4
 *)

open Symbol

type pos = Naml_ast.pos
type mpath = symbol list
type vpath = mpath * symbol

(* important note: this code completely ignores modules and separate files *)

type ty =
    TyInt
  | TyChar
  | TyFloat

  | TyFun of ty list * ty
  | TyTuple of ty list
  | TyArray of ty

  | TyVar of symbol
  | TyApply of vpath * ty list

  | TyExn

type tydef =
    TyDefLambda of symbol list * bool list * ty
  | TyDefUnion of  symbol list * bool list * (symbol * ty list) list
  | TyDefRecord of symbol list * bool list * (bool * symbol * ty) list

type exndef = 
    ExnVar of symbol * vpath
  | ExnCons of symbol * ty list

type constexpr =
    IntConst of int
  | FloatConst of float
  | CharConst of char
  | CConsConst of vpath * ty

type expr =
    VarExpr of vpath * ty
  | ConstExpr of constexpr * ty
  | TupleExpr of expr list * ty
  | ConsExpr of vpath * expr list * ty
  | ConsCopyExpr of vpath * expr * ty
  | ArrayExpr of expr list * ty
  | RecordExpr of expr list * ty (* expressions in same order as record definition *)
  | ApplyExpr of expr * expr list * ty
  | ProjExpr of expr * vpath * ty
  | AssignProjExpr of expr * vpath * expr * ty
  | ArrayEltExpr of expr * expr * ty
  | AssignArrayEltExpr of expr * expr * expr * ty
  | WhileExpr of expr * expr * ty
  | ForExpr of symbol * expr * bool * expr * expr * ty
  | SeqExpr of expr * expr * ty
  | MatchExpr of expr * pmatch * ty
  | TryExpr of expr * pmatch * ty
  | FunExpr of (symbol * ty) list * expr * ty
  | LetRecExpr of (symbol * (symbol * ty) list * expr * ty) list * expr * ty
  | LetExtExpr of symbol * ty * string * expr (* used as temporary ExternMod in IR conversion *)

and pmatch = (pattern * expr option * expr) list

and pattern =
    IdPat of symbol * ty
  | AnyPat of ty
  | ConstPat of constexpr * ty
  | RangePat of constexpr * constexpr * ty
  | AsPat of pattern * symbol * ty
  | AltPat of pattern * pattern * ty
  | ConsPat of vpath * pattern list * ty
  | ConsCopyPat of vpath * pattern * ty
  | TuplePat of pattern list * ty
  | ArrayPat of pattern list * ty
  | RecordPat of pattern list * ty (* don't need the vpaths -- same as TyRecord *)

(***** modules *****)

type def =
    LetMod of (pattern * expr) list
  | LetRecMod of (symbol * (symbol * ty) list * expr * ty) list
  | ExternMod of symbol * ty * string
  | TypeMod of (symbol * tydef) list
  | ExnMod of exndef

type prog = def list
