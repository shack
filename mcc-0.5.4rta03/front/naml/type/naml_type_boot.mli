(*
 * Pervasives_boot stuff
 * Geoffrey Irving
 * 8apr1
 *)

open Symbol
open Naml_tast
open Naml_parse_misc

(* basic type symbols *)
val unit_ty_sym : symbol
val bool_ty_sym : symbol
val list_ty_sym : symbol

val unit_ty : ty
val bool_ty : ty

val unit_const : constexpr
val false_const : constexpr
val true_const : constexpr

val unit_expr : expr
val false_pat : pattern
val true_pat : pattern

val required_types : string list

type extern_ty = 
    ExternTySimp of symbol list * ty 
  | ExternTyCons of symbol * symbol list * (symbol * ty list) list

val extern_tydef : string -> extern_ty
val type_of_extern : string -> ty
