(*
 * TAST to IR conversion
 * Dylan Simon, Geoffrey Irving
 * 27may01
 *)

open Naml_ir

val make_prog : Naml_tast.prog -> prog
