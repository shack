(* IR to FIR conversion
 * $Id: naml_fir_ir.ml,v 1.50 2002/12/23 00:13:19 jyh Exp $
 * 2001-06-18
 *)

open Symbol
open Fir

open Naml_ir
open Naml_ir_exn
open Naml_ir_misc

open Fir_env
open Fir_exn
open Fir_pos
open Fir_type
open Fir_ty_curry
open Fir_ty_closure

module Pos = MakePos (struct let name = "Naml_fir_ir" end)
open Pos

(************************************** type handling *)

let closure_type tl =
  let a = new_symbol_string "a" in
  let av = TyVar a in
    TyExists ([a], TyTuple (NormalTuple, [av, Immutable; TyFun (av :: tl, ty_void), Immutable]))

let cont_fun_type b = closure_type [b]

let exn_fun_type () = closure_type [type_exn]

(* function before fir_type *)
let arg_types ty =
  match ty with
      TyFun (tl, _) -> tl
    | ty -> raise (IRException ("non-function type in arg_types"))

let extract_return_type ty =
  match ty with
      TyExists (_, TyTuple (NormalTuple, [_; TyFun ([_; rt], _), _])) -> rt
    | _ -> raise (IRException ("invalid type in extract_return_type"))

let rec reduce_type ty =
  match ty with
      TyInt | TyEnum _ | TyRawInt _ | TyFloat _ -> ty
    | TyUnion (tv, tl, s) -> TyUnion (tv, List.map reduce_type tl, s)
    | TyTuple (tclass, tl) -> TyTuple (tclass, List.map (fun (ty, b) -> reduce_type ty, b) tl)
    | TyArray t -> TyArray (reduce_type t)
    | _ -> TyDelayed

let rec fir_type ty =
  match ty with
      TyFun (aty, _) ->
	(match aty with
	    [a] ->
              closure_type [fir_type a]
	  | _ ->
	      let rec nest al =
		match al with
		    [_; TyFun ([rt], _)] ->
                      fir_type rt
		  | a :: al ->
                      closure_type [fir_type a; exn_fun_type (); cont_fun_type (nest al)]
		  | [] -> raise (Impossible "lost some arguments somewhere in fir_type") in
	      nest aty)
    | TyUnion (tv, tl, i) ->
	TyUnion (tv, List.map fir_type tl, i)
    | TyApply (tv, _) when Symbol.eq tv Naml_ir_misc.fini_symbol ->
        type_fini
    | TyApply (tv, tl) ->
	TyApply (tv, List.map fir_type tl)
    | TyTuple (tclass, tl) ->
	TyTuple (tclass, List.map (fun (ty, b) -> fir_type ty, b) tl)
    | TyArray t ->
	TyArray (fir_type t)
    | _ -> ty

let rec quantify ty =
   let rec ftv es fv = function
      TyFun (tl, _)
    | TyUnion (_, tl, _) -> List.fold_left (ftv es) fv tl
    | TyTuple (_, tl) -> List.fold_left (fun fv (ty, _) -> ftv es fv ty) fv tl
    | TyArray t -> ftv es fv t
    | TyVar v -> if SymbolSet.mem es v then fv else SymbolSet.add fv v
    | TyExists (tvl, t) -> ftv (SymbolSet.add_list es tvl) fv t
    | _ -> fv
   in
      SymbolSet.elements (ftv SymbolSet.empty SymbolSet.empty ty)

(************************************** operations on environments *)

let env_type = function
    [] -> ty_unit
  | [t] -> t
  | tl -> TyTuple (NormalTuple, List.map (fun ty -> ty, Immutable) tl)

let env_subscript tl va c =
  match tl with
      [] -> c []
    | [t] -> c [AtomVar va]
    | tl ->
        let vl = List.rev_map (fun _ -> new_symbol_string "a") tl in
        let e = c (List.map (fun v -> AtomVar v) vl) in
        let rec loop i = function
            [] -> e
          | v :: vl ->
              naml_exp (Fir.LetSubscript (block_poly_subop, v, TyDelayed, AtomVar va, AtomInt i,
                loop (i + 1) vl))
        in
          loop 0 vl

let env_allocate al c =
  match al with
      [] -> c (atom_unit)
    | [a] -> c a
    | al ->
        let ev = new_symbol_string "env" in
          naml_exp (Fir.LetAlloc (ev, AllocTuple (NormalTuple, [], TyDelayed, al), c (AtomVar ev)))

(************************************** helper function generation *)

(*
 * funs: std fun sym -> fundef
 * f: f
 * fh: helper array for f
 * rty: continuation atom type of f
 * apply: arguments already applied
 * rest: arguments to curry
 * i: len(apply)
 * returns: funs
 *)
let rec make_helper_funs funs dl fcl f fh apply rest i =
  match fcl with
      FunGlobalClass -> (match rest with
	  []|[_]|[_;_] -> raise (Invalid_argument "make_helper_funs")
	| [a; _; fcontty] ->
	    let myf = new_symbol f in
	    fh.(i) <- Some myf;
	    let rt = extract_return_type fcontty in
	    let prevenv = new_symbol_string "env" in
	    let arg = new_symbol_string "a" in
	    let exnh = new_symbol_string "exnh" in
	    let cont = new_symbol_string "cont" in
	    SymbolTable.add funs myf (
		true, dl, FunContClass,
		TyFun ([env_type apply; a; exn_fun_type (); cont_fun_type rt], ty_void),
		[prevenv; arg; exnh; cont],
                env_subscript apply prevenv (fun al ->
		  naml_exp (Fir.TailCall (new_symbol_string "helper", AtomVar f, al @ [AtomVar arg; AtomVar exnh; AtomVar cont])))),
            myf
	| a :: rest ->
	    match fh.(i) with
		Some f -> funs, f
	      | None ->
		  let funs, nextf = make_helper_funs funs dl fcl f fh (apply @ [a]) rest (succ i) in
		  let myf = new_symbol f in
		  fh.(i) <- Some myf;
		  let _, _, _, nextfty, _, _ = SymbolTable.find funs nextf in
		  let prevenv = new_symbol_string "env" in
		  let arg = new_symbol_string "a" in
		  let mycont = new_symbol_string "cont" in
		  let contenv = new_symbol_string "cont_env" in
		  let contf = new_symbol_string "cont_fun" in
		  let cont = new_symbol_string "cont" in
		  SymbolTable.add funs myf (
		      true, dl, FunContClass,
		      TyFun ([env_type apply; a; exn_fun_type (); cont_fun_type (TyTuple (NormalTuple, [env_type (apply @ [a]), Immutable; nextfty, Immutable]))], ty_void),
		      [prevenv; arg; new_symbol_string "exnh"; cont],
                      env_subscript apply prevenv (fun al ->
                      env_allocate (al @ [AtomVar arg]) (fun myenv ->
		        naml_exp (Fir.LetAlloc (mycont, AllocTuple (NormalTuple, [], TyDelayed, [myenv; AtomVar nextf]),
		        naml_exp (Fir.LetSubscript (block_poly_subop, contenv, TyDelayed, AtomVar cont, AtomInt 0,
		        naml_exp (Fir.LetSubscript (block_poly_subop, contf, TyDelayed, AtomVar cont, AtomInt 1,
		        naml_exp (Fir.TailCall (new_symbol_string "blah", AtomVar contf, [AtomVar contenv; AtomVar mycont]))))))))))),
                  myf)
    | FunContClass -> (match rest with
	| [a] ->
	    let myf = new_symbol f in
	    fh.(i) <- Some myf;
	    let prevenv = new_symbol_string "env" in
	    let arg = new_symbol_string "a" in
	    let argl = List.map (fun x -> new_symbol_string "a") apply in
	    SymbolTable.add funs myf (
		true, dl, FunContClass,
		TyFun ([env_type apply; a], ty_void),
		[prevenv; arg],
                env_subscript apply prevenv (fun al ->
		naml_exp (Fir.TailCall (new_symbol_string "blah", AtomVar f, al @ [AtomVar arg])))),
            myf
	| _ -> raise (IRException "can't partially apply continuations"))
    | FunLocalClass -> raise (IRException "can't close local functions")

(************************************** expression conversion *)

let fir_aop aop =
  match aop with
      AllocTuple (tclass, ty_vars, t, al) -> Fir.AllocTuple (tclass, ty_vars, reduce_type t, al)
    | AllocArray (t, al) -> Fir.AllocArray (reduce_type t, al)
    | AllocVArray (t, sub_index, a1, a2) -> Fir.AllocVArray (reduce_type t, sub_index, a1, a2)
    | AllocUnion (ty_vars, t, tv, i, al) -> Fir.AllocUnion (ty_vars, reduce_type t, tv, i, al)
    | AllocMalloc _
    | AllocFrame _
    | AllocDTuple _ -> aop

(* convert an ir expression to a fir function table and a main function body
 * funs - a function symbol -> Fir.fundef table
 * fct - a function symbol -> function type * function closure function array for each argument number table
 *)
let rec fir_expr funs fct e =
   match e with
      LetAtom (v, t, a, e) ->
         let funs, e = fir_expr funs fct e in
            funs, naml_exp (Fir.LetAtom (v, reduce_type t, a, e))
    | LetUnop (v, t, op, a, e) ->
         let funs, e = fir_expr funs fct e in
            funs, naml_exp (Fir.LetAtom (v, reduce_type t, Fir.AtomUnop (op, a), e))
    | LetBinop (v, t, op, a1, a2, e) ->
         let funs, e = fir_expr funs fct e in
            funs, naml_exp (Fir.LetAtom (v, reduce_type t, Fir.AtomBinop (op, a1, a2), e))
    | LetExt (v, t, s, ty, al, e) ->
         let funs, e = fir_expr funs fct e in
            funs, naml_exp (Fir.LetExt (v, reduce_type t, s, false, ty, [], al, e))
    | TailCall (v, al) ->
         if SymbolTable.mem fct v then
            funs, naml_exp (Fir.TailCall (new_symbol v, AtomVar v, al))
         else
            let contenv = new_symbol_string "cl_env" in
            let contf = new_symbol_string "cl_fun" in
               funs,
               naml_exp (Fir.LetSubscript (block_poly_subop, contenv, TyDelayed, AtomVar v, AtomInt 0,
               naml_exp (Fir.LetSubscript (block_poly_subop, contf, TyDelayed, AtomVar v, AtomInt 1,
               naml_exp (Fir.TailCall (new_symbol_string "blah", AtomVar contf, (AtomVar contenv) :: al))))))
    | Match (a, sel) ->
         let funs, sel = Mc_list_util.fold_map (fun funs (s, e) ->
                               let funs, e = fir_expr funs fct e in
                               let l = new_symbol_string "match" in
                                  funs, (l, s, e))
                         funs sel in
            funs, naml_exp (Fir.Match (a, sel))
    | LetAlloc (v, aop, e) ->
         let funs, e = fir_expr funs fct e in
            funs, naml_exp (Fir.LetAlloc (v, fir_aop aop, e))
    | LetSubscript (so, v, t, a, ai, e) ->
         let funs, e = fir_expr funs fct e in
            funs, naml_exp (Fir.LetSubscript (so, v, reduce_type t, AtomVar a, ai, e))
    | SetSubscript (so, a, ai, t, av, e) ->
         let funs, e = fir_expr funs fct e in
         let l = new_symbol_string "set_subscript" in
            funs, naml_exp (Fir.SetSubscript (so, l, AtomVar a, ai, reduce_type t, av, e))
    | LetFuns (fdl, e) ->
         let fct = List.fold_left (fun fct (v, _, _, _, vl, _) ->
                         SymbolTable.add fct v (Array.make (List.length vl) None))
                   fct fdl in
        (* temporary values for recursive function use *)
         let funs = List.fold_left (fun funs (v, dl, fc, t, _, _) ->
                          SymbolTable.add funs v (false, dl, fc, t, [], Naml_ir_misc.garbage_fir))
                    funs fdl in
         let funs = List.fold_left (fun funs (v, dl, fc, t, vl, e) ->
                          let funs, e = fir_expr funs fct e in
                             SymbolTable.add funs v (false, dl, fc, t, vl, e))
                    funs fdl in
            fir_expr funs fct e
    | LetClosure (v, t, f, al, e) ->
         let all = List.length al in
         let _, dl, fcl, ft, _, _ = SymbolTable.find funs f in
         let fatl = List.map fir_type (arg_types ft) in
	(* split the argument types of the function into those
         * the closure applies and those applied later *)
         let apply, rest = Mc_list_util.split all fatl in
	let fc = SymbolTable.find fct f in
	let funs, g = make_helper_funs funs dl fcl f fc apply rest all in
	(* make a tuple of all the aguments applied now *)
	let funs, e = fir_expr funs fct e in
	  funs,
          env_allocate al (fun args ->
	    naml_exp (Fir.LetAlloc (v, AllocTuple (NormalTuple, [], TyDelayed, [args; AtomVar g]),
	    e)))
    | LetApply _ | Return _ | LetExnHandler _ | Raise _ ->
	raise (IRException "it's too late for something in fir conversion")

(************************************** toplevel function *)

let fir_prog prog =
   let { prog_types = tenv;
         prog_cont = fini;
         prog_body = e
       } = prog
   in
   let main = new_symbol_string "naml_main" in
   let fct = SymbolTable.add SymbolTable.empty fini [||] in
   let funs, body = fir_expr SymbolTable.empty fct e in
   let funs =
      SymbolTable.map (fun (h, dl, fc, t, vl, e) ->
            let t =
               if h then
                  t
               else
                  TyFun (List.map fir_type (arg_types t), ty_void)
            in
            let vars = quantify t in
               dl, vars, t, vl, e)
      funs
   in
   let funs =
      SymbolTable.add funs main (**)
         (naml_loc, [], TyFun ([type_fini], ty_void), [fini], body)
   in
   let prog =
      { prog_file = { file_dir = ""; file_name = ""; file_class = FileNaml };
        Fir.prog_import = SymbolTable.empty;
        Fir.prog_export = SymbolTable.add SymbolTable.empty main (**)
           { export_name = "init";
             export_type = TyFun ([type_fini], ty_void)
           };
        Fir.prog_types = tenv;
        Fir.prog_globals = SymbolTable.empty;
        Fir.prog_funs = funs;
        Fir.prog_names = SymbolTable.empty;
        Fir.prog_frames = SymbolTable.empty
      }
   in

   (*
    * Attempt to use universal quantification rather than
    * existentials.
    *)
   let prog = ty_curry_prog prog in

   (*
    * Close types.
    *)
   let prog = ty_close_prog prog in
      prog
