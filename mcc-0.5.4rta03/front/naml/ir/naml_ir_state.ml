(* Miscellanous IR values
 * Dylan Simon, Geoffrey Irving
 * 2aug01
 *)
let opt_inc_perv = ref false
let opt_no_inline = ref false
let opt_print = Fir_state.debug_print_ir
