(* Miscellanous IR values
 * Dylan Simon, Geoffrey Irving
 * 2aug01
 *)
open Naml_ir_state
open Naml_ir_util
open Naml_ir_exn
open Naml_ir_tast
open Naml_ir_inline
open Naml_ir_dead
open Naml_ir_partial
open Naml_ir_cps
open Naml_ir_close
open Naml_fir_ir

let print_prog s prog =
  if !opt_print then (
    Format.printf "\n*** %s:\n" s;
    print_prog prog;
    Format.print_newline ())

(*
 * Compilation.
 *)
let compile name =
  let inx = open_in name in
  try
    (* lex & parse pervasives.ml from current directory *)
    let pprog = if !opt_inc_perv then
      let px = open_in "pervasives.ml" in
      Naml_parse_state.set_current_position ("pervasives.ml", 1, 0, 1, 0);
      let lexbuf = Lexing.from_channel px in
      let pprog = Naml_parse.prog Naml_lex.main lexbuf in
      close_in px;
      pprog
    else
      [] in

    (* lex & parse *)
    Naml_parse_state.set_current_position (name, 1, 0, 1, 0);
    let lexbuf = Lexing.from_channel inx in
    let prog = Naml_parse.prog Naml_lex.main lexbuf in
    Parsing.clear_parser ();

    let prog = Naml_ast_standardize.standardize_prog (pprog @ prog) in
    (* type inference *)
    let prog = Naml_type_infer.infer prog in
    if !opt_print then
      (Format.printf "*** TAST:\n";
      Naml_tast_util.print_prog prog);

    (* ir conversion *)
    let prog = make_prog prog in
    print_prog "IR" prog;
    let prog = if !opt_no_inline then prog else (
      let prog = dead_prog (inline_prog prog) in
      print_prog "Inlined IR" prog;
      prog) in
    let prog = partial_prog prog in
    print_prog "Partial application handled IR" prog;
    let prog = cps_prog prog in
    print_prog "CPS converted IR" prog;
    let prog = close_prog prog in
    print_prog "Closed IR" prog;
    fir_prog prog

  with
    exn ->
      print_exn_chan stderr exn;
      close_in inx;
      exit 1
