(*
 * Inline IR before it's screwed up by transformations
 * Geoffrey Irving
 * 24may01
 *)

open Naml_ir

val inline_prog : prog -> prog
