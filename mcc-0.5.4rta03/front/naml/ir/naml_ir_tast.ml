(* TAST to IR conversion
 * Dylan Simon, Geoffrey Irving
 * 2001-05-21
 *)

open Symbol

open Fir
open Fir_set
open Fir_type
open Naml_ir
open Naml_ir_misc
open Naml_ir_exn
open Naml_ir_util
open Naml_ir_ext

module Tast = Naml_tast

module SymbolIntTable = Mc_map.McMake (struct
  type t = symbol * int

  let compare (s1, n1) (s2, n2) =
    let n = n1 - n2 in
      if n == 0 then
        Symbol.compare s1 s2
      else
         n
end)

let empty_line = naml_loc

let rawfloat_of_float x = Rawfloat.of_float Rawfloat.Double x

let int_interval i j = IntSet.of_interval (Interval_set.Closed i) (Interval_set.Closed j)
let zero_interval n = int_interval 0 (n - 1)

let i_flag = 1 lsl 30  (* flag to indicate reduced union *)

let s_of_vp (l, s) =  (* XXX anywhere this is called *)
  if l != [] then
    raise (NotImplemented "non-trivial vpaths"); s

let rec make_type tenv ty =
  match ty with
      Tast.TyInt -> TyInt
    | Tast.TyChar -> type_char
    | Tast.TyFloat -> type_float
    | Tast.TyExn -> type_exn
    | Tast.TyFun (tl, t) -> TyFun (List.map (make_type tenv) tl, make_type tenv t)
    | Tast.TyTuple tl -> TyTuple (NormalTuple, List.map (fun ty -> make_type tenv ty, Immutable) tl)
    | Tast.TyArray t -> TyArray (make_type tenv t)
    | Tast.TyVar s -> TyVar s
    | Tast.TyApply (v, tl) ->
        let s = s_of_vp v in
        let tl = List.map (make_type tenv) tl in
        match SymbolTable.find tenv s with
            TyDefLambda _
          | TyDefDTuple _ -> TyApply (s, tl)
          | TyDefUnion (_, fields) -> TyUnion (s, tl, int_interval 0 (pred (List.length fields)))

let type_of_expr tenv e = make_type tenv (Naml_tast_util.type_of_expr e)
let type_of_pattern tenv p = make_type tenv (Naml_tast_util.type_of_pattern p)

let var_of_atom a =
  match a with
      AtomVar v -> v
    | _ -> raise (Invalid_argument "var_of_atom")

let rec explode_tuple_type tenv t =
  match t with
      TyTuple (_, tl) -> tl
    | TyApply (v, tl) -> explode_tuple_type tenv (apply_type tenv v tl)
    | _ -> raise (Invalid_argument "explode_tuple_type")

let rec enum_size tenv ty =
  match ty with
      TyEnum n -> n
    | TyApply (v, tl) -> enum_size tenv (apply_type tenv v tl)
    | _ -> raise (Invalid_argument "enum_size")

let rec union_name tenv ty =
  match ty with
      TyUnion (tv, _, _) -> tv
    | TyApply (v, tl) -> union_name tenv (apply_type tenv v tl)
    | _ -> raise (Invalid_argument "union_name")

let union_size tenv tv =
   match SymbolTable.find tenv tv with
      TyDefUnion (_, l) -> List.length l
    | TyDefDTuple _
    | TyDefLambda _ -> raise (Invalid_argument "union_size")

let rec find_pattern_vars tenv vl tl p =
  match p with
      Tast.IdPat (v, ty) -> v :: vl, make_type tenv ty :: tl
    | Tast.AnyPat _
    | Tast.ConstPat _
    | Tast.RangePat _ -> vl, tl
    | Tast.AsPat (p, v, ty) -> find_pattern_vars tenv (v :: vl) (make_type tenv ty :: tl) p
    | Tast.AltPat (p, _, _)
    | Tast.ConsCopyPat (_, p, _) -> find_pattern_vars tenv vl tl p
    | Tast.ConsPat (_, pl, _)
    | Tast.TuplePat (pl, _)
    | Tast.ArrayPat (pl, _)
    | Tast.RecordPat (pl, _) -> List.fold_left (fun (vl, tl) -> find_pattern_vars tenv vl tl) (vl, tl) pl

let rec set_of_type tenv ty =
  match ty with
      TyInt -> IntSet.max_set
    | TyEnum n -> zero_interval n
    | TyUnion (tv, _, _) -> zero_interval (union_size tenv tv)
    | TyApply (v, tl) -> set_of_type tenv (apply_type tenv v tl)
    | _ -> IntSet.empty

(* if a1 then e1 else e2 *)
let make_if_expr a1 e1 e2 =
  Match (a1, [set_one, e1; set_zero, e2])

let rec make_expr tenv ienv e vh c =
  let make_expr = make_expr tenv ienv in
  let make_expr_list = make_expr_list tenv ienv in
  match e with
      Tast.VarExpr (v, ty) ->
        c (AtomVar (s_of_vp v))
    | Tast.ConstExpr (Tast.IntConst i, _) ->
        c (AtomInt i)
    | Tast.ConstExpr (Tast.FloatConst x, _) ->
        let v = match vh with None -> new_symbol_string "float" | Some v -> v in
          LetAlloc (v, AllocTuple (NormalTuple, [], type_float, [AtomFloat (Rawfloat.of_float Rawfloat.Double x)]), c (AtomVar v))
    | Tast.ConstExpr (Tast.CharConst i, _) ->
        c (AtomEnum (256, int_of_char i))
    | Tast.ConstExpr (Tast.CConsConst (s, _), ty) ->
        let i = SymbolTable.find ienv (s_of_vp s) in
        let ty = make_type tenv ty in
          if (i land i_flag) != 0 then
            let n = enum_size tenv ty in
              c (AtomEnum (n, i land (lnot i_flag)))
          else
            let tv = union_name tenv ty in
              c (AtomConst (ty, tv, i))
    | Tast.TupleExpr (el, ty) ->
        make_expr_list el (fun al ->
          let v = match vh with None -> new_symbol_string "tuple" | Some v -> v in
            LetAlloc (v, AllocTuple (NormalTuple, [], make_type tenv ty, al), c (AtomVar v)))
    | Tast.RecordExpr (el, ty) ->
        make_expr_list el (fun al ->
          let ty = make_type tenv ty in
          let un = union_name tenv ty in
          let v = match vh with None -> new_symbol_string "record" | Some v -> v in
            LetAlloc (v, AllocUnion ([], ty, un, 0, al), c (AtomVar v)))
    | Tast.ArrayExpr (el, ty) ->
        make_expr_list el (fun al ->
          let v = match vh with None -> new_symbol_string "array" | Some v -> v in
            LetAlloc (v, AllocArray (make_type tenv ty, al), c (AtomVar v)))
    | Tast.ConsExpr (s, el, ty) ->
        let ty = make_type tenv ty in
        let s = s_of_vp s in
        let i = SymbolTable.find ienv s in
        if el == [] then
          if (i land i_flag) != 0 then
            c (AtomEnum (enum_size tenv ty, i land (lnot i_flag)))
          else
            c (AtomConst (ty, union_name tenv ty, i))
        else
          make_expr_list el (fun al ->
            let v = match vh with None -> new_symbol s | Some v -> v in
              LetAlloc (v, AllocUnion ([], ty, union_name tenv ty, i, al), c (AtomVar v)))
    | Tast.ConsCopyExpr (s, e, t) ->
	make_expr e None (fun a ->
          let s = s_of_vp s in
	  let v = match vh with None -> new_symbol s | Some v -> v in
	  let t = type_of_expr tenv e in
          let un = union_name tenv t in
	  let tl = explode_tuple_type tenv t in
          let tl = List.map fst tl in
	  let vl = List.map (fun _ -> new_symbol_string "field") tl in
          let va = var_of_atom a in
	  snd (List.fold_left2 (fun (i, e) t v ->
	      succ i, LetSubscript (block_poly_subop, v, t, va, AtomInt i, e))
	    (0, LetAlloc (v, AllocUnion ([], t, un, SymbolTable.find ienv s, List.map (fun v -> AtomVar v) vl), c (AtomVar v)))
	    tl vl))
    | Tast.ApplyExpr (e, el, ty) ->
        make_expr e None (function
            AtomVar f ->
              make_expr_list el (fun al ->
                let ty = make_type tenv ty in
                let v = match vh with None -> new_symbol_string "apply" | Some v -> v in
                  LetApply (v, ty, f, al, c (AtomVar v)))
          | _ -> raise (IRException "non-var atom used as function"))
    | Tast.ProjExpr (e, f, ty) ->
        make_expr e None (fun a ->
          let f = s_of_vp f in
          let v = match vh with None -> new_symbol f | Some v -> v in
          let ty = make_type tenv ty in
          let s = AtomInt (SymbolTable.find ienv f) in
          let va = var_of_atom a in
            LetSubscript (block_poly_subop, v, ty, va, s, c (AtomVar v)))
    | Tast.AssignProjExpr (e1, f, e2, _) ->
        make_expr e1 None (fun a1 ->
        make_expr e2 None (fun a2 ->
          let f = s_of_vp f in
          let s = AtomInt (SymbolTable.find ienv f) in
          let v = var_of_atom a1 in
            SetSubscript (block_poly_subop, v, s, type_of_expr tenv e2, a2, c atom_unit)))
    | Tast.ArrayEltExpr (e1, e2, ty) ->
        make_expr e1 None (fun a1 ->
        make_expr e2 None (fun a2 ->
          let v = match vh with None -> new_symbol_string "av" | Some v -> v in
          let ty = make_type tenv ty in
          let va = var_of_atom a1 in
	    LetSubscript (block_poly_subop, v, ty, va, a2,
	      c (AtomVar v))))
    | Tast.AssignArrayEltExpr (e1, e2, e3, ty) ->
        make_expr e1 None (fun a1 ->
        make_expr e2 None (fun a2 ->
        make_expr e3 None (fun a3 ->
          let ty = make_type tenv ty in
          let v = var_of_atom a1 in
	    SetSubscript (block_poly_subop, v, a2, ty, a3,
	      c atom_unit))))
    | Tast.WhileExpr (e1, e2, _) ->
        let f = new_symbol_string "while" in
        let b = new_symbol_string "break" in
        let tc = TailCall (f, []) in
        let fb = make_expr e1 None (fun a1 ->
	  make_if_expr a1 (make_expr e2 None (fun _ -> tc)) (TailCall (b, []))) in
        let bb = c atom_unit in
        let fty = TyFun ([], ty_void) in
          LetFuns ([f, empty_line, FunLocalClass, fty, [], fb;
                    b, empty_line, FunLocalClass, fty, [], bb], tc)
    | Tast.ForExpr (i, ef, d, et, eb, _) ->
	make_expr et None (fun at ->
	  let f = new_symbol_string "for" in
	  let b = new_symbol_string "break" in
	  let t = new_symbol_string "test" in
	  let fb =
	    LetBinop (t, ty_bool, (if d then LeIntOp else GeIntOp), AtomVar i, at,
	      make_if_expr (AtomVar t)
                (make_expr eb None (fun _ ->
                  LetBinop (i, TyInt, (if d then PlusIntOp else MinusIntOp), AtomVar i, AtomInt 1,
                    TailCall (f, [AtomVar i]))))
                (TailCall (b, []))) in
	  let bb = c atom_unit in
	    LetFuns ([
	      f, empty_line, FunLocalClass, TyFun ([TyInt], ty_void), [i], fb;
	      b, empty_line, FunLocalClass, TyFun ([], ty_void), [], bb],
	    make_expr ef None (fun af ->
	      TailCall (f, [af]))))
    | Tast.SeqExpr (e1, e2, _) ->
	make_expr e1 None (fun _ ->
	  make_expr e2 vh c)
    | Tast.MatchExpr (e1, [Tast.IdPat (v, ty), None, e2], _) ->
        make_expr e1 (Some v) (function
            AtomVar v' when Symbol.eq v v' ->
              make_expr e2 vh c
          | a ->
              LetAtom (v, make_type tenv ty, a, make_expr e2 vh c))
    | Tast.MatchExpr (e, pm, ty) ->
	make_expr e None (fun a ->
	  make_match_expr tenv ienv a (type_of_expr tenv e) (make_type tenv ty) pm vh c (fun () ->
	    Format.print_string "Warning: incomplete match (somewhere)"; Format.print_newline ();
	    Raise (raise (NotImplemented "incomplete matching"))))
    | Tast.TryExpr (e, pm, t) ->
	let f = new_symbol_string "handler" in
	let cv = new_symbol_string "cont" in
	let x = new_symbol_string "exn" in
	let v = match vh with None -> new_symbol_string "v" | Some v -> v in
        let t = make_type tenv t in
        let ty = type_of_expr tenv e in
        let xa = AtomVar x in
        let ct a = TailCall (cv, [a]) in
        let cf () = Raise xa in
	  LetFuns ([
	      cv, empty_line, FunLocalClass, TyFun ([t], ty_void), [v], c (AtomVar v);
	      f, empty_line, FunContClass, TyFun ([type_exn], ty_void), [x],
                make_match_expr tenv ienv xa type_exn ty pm None ct cf],
	    LetExnHandler (f, make_expr e None ct))
    | Tast.FunExpr (stl, e, ty) ->
	let f = match vh with None -> new_symbol_string "fun" | Some v -> v in
	let rt = type_of_expr tenv e in
	let ty = TyFun (List.map (fun (_,t) -> make_type tenv t) stl, rt) in
	let a = List.map fst stl in
	  LetFuns ([f, empty_line, FunGlobalClass, ty, a, make_expr e None (fun a -> Return a)], c (AtomVar f))
    | Tast.LetRecExpr (sfl, e, ty) ->
        let ty = make_type tenv ty in
	let fl = List.map (fun (s, stl, e, t) ->
	    let rt = type_of_expr tenv e in
	    let t = TyFun (List.map (fun (_,t) -> make_type tenv t) stl, rt) in
	    let a = List.map fst stl in
	      s, empty_line, FunGlobalClass, t, a, make_expr e None (fun a -> Return a))
	  sfl in
	LetFuns (fl, make_expr e vh c)
    | Tast.LetExtExpr (s, t, ext, e) ->
	let e = make_expr e vh c in
	let t = make_type tenv t in
	  make_extern_def s empty_line t ext e

and make_expr_list tenv ienv el c =
  match el with
      [] -> c []
    | e :: el ->
        make_expr tenv ienv e None (fun a -> make_expr_list tenv ienv el (fun al -> c (a :: al)))

and make_match_expr tenv ienv a aty rty pwel vh ct cf =
  let learn kt a s =
    match a with
        AtomVar v -> SymbolTable.add kt v s
      | _ -> kt in
  let recall kt a =
    match a with
        AtomVar v -> SymbolTable.find kt v
      | AtomInt i | AtomEnum (_, i) | AtomConst (_, _, i) ->
          IntSet.of_point i
      | _ -> raise (IRException "make_match_expr: recall") in
  let rec do_pattern_list kt st v pl ct cf =
    let rec do_index kt st i = function
	[] -> ct kt st
      | p :: pl ->
	  let c kt st v = do_pattern kt st (AtomVar v) p
	    (fun kt st -> do_index kt st (succ i) pl) cf in
          (try
            c kt st (SymbolIntTable.find st (v, i))
          with Not_found ->
            let s = new_symbol_string "sub" in
            let st = SymbolIntTable.add st (v, i) s in
            let t = type_of_pattern tenv p in
            let kt = SymbolTable.add kt s (set_of_type tenv t) in
              LetSubscript (block_poly_subop, s, t, v, AtomInt i, c kt st s)) in
    do_index kt st 0 pl
  and do_pattern kt st a p ct cf =
    match p with
	Tast.IdPat (s, ty) ->
          let ty = make_type tenv ty in
	    LetAtom (s, ty, a, ct kt st)
      | Tast.AnyPat (ty) ->
	  ct kt st
      | Tast.ConstPat (ce, ty) ->
          let do_int i =
            let is = recall kt a in
            if IntSet.mem_point i is then
              if IntSet.is_singleton is then
                ct kt st
              else
                let pis = IntSet.of_point i in
                let nis = IntSet.subtract_point is i in
                  Match (a, [
                    IntSet pis, ct (learn kt a pis) st;
                    IntSet nis, cf (learn kt a nis) st])
            else
              cf kt st in
	  (match ce with
	      Tast.IntConst i ->
                do_int i
	    | Tast.CharConst i ->
                do_int (int_of_char i)
	    | Tast.CConsConst (c, ty) ->
                let i = SymbolTable.find ienv (s_of_vp c) in
                  do_int (i land lnot i_flag)
	    | Tast.FloatConst i ->
                let x = new_symbol_string "x" in
                let s = new_symbol_string "feq" in
                let v = var_of_atom a in
                  LetSubscript (block_poly_subop, x, TyFloat Rawfloat.Double, v, AtomInt 0,
                  LetBinop (s, ty_bool, EqFloatOp Rawfloat.Double, AtomVar x,
                    AtomFloat (Rawfloat.of_float Rawfloat.Double i),
                  Match (AtomVar s, [
                    set_one, ct kt st;
                    set_zero, cf kt st]))))
      | Tast.RangePat (ce1, ce2, ty) ->
          let do_int i1 i2 =
            let is = recall kt a in
            let pis = int_interval i1 i2 in
            let nis = IntSet.negate pis in
            if IntSet.subset is pis then
              ct kt st
            else if IntSet.subset is nis then
              cf kt st
            else
              let pis = IntSet.isect pis is in
              let nis = IntSet.isect nis is in
                Match (a, [
                  IntSet pis, ct (learn kt a pis) st;
                  IntSet nis, cf (learn kt a nis) st]) in
	  (match ce1, ce2 with
	      Tast.IntConst i1, Tast.IntConst i2 -> do_int i1 i2
	    | Tast.CharConst i1, Tast.CharConst i2 -> do_int (int_of_char i1) (int_of_char i2)
	    | Tast.FloatConst i1, Tast.FloatConst i2 ->
                let x = new_symbol_string "x" in
                let s1 = new_symbol_string "fge" in
                let s2 = new_symbol_string "fle" in
                let v = var_of_atom a in
                  LetSubscript (block_poly_subop, x, TyFloat Rawfloat.Double, v, AtomInt 0,
                  LetBinop (s1, ty_bool, GeFloatOp Rawfloat.Double, AtomVar x,
                    AtomFloat (Rawfloat.of_float Rawfloat.Double i1),
                  Match (AtomVar s1, [
                    set_zero, cf kt st;
                    set_one,
                      LetBinop (s2, ty_bool, LeFloatOp Rawfloat.Double, AtomVar x,
                        AtomFloat (Rawfloat.of_float Rawfloat.Double i2),
                      Match (AtomVar s2, [set_zero, cf kt st; set_one, ct kt st]))])))
            | _ -> raise (IRException "invalid range pattern"))
      | Tast.AsPat (p, s, ty) ->
          let ty = make_type tenv ty in
	    LetAtom (s, ty, a, do_pattern kt st a p ct cf)
      | Tast.AltPat (p1, p2, ty) ->
          let ty = make_type tenv ty in
	  let vl, tl = find_pattern_vars tenv [] [] p1 in
	  let f = new_symbol_string "patf" in
	  let tc _ _ = TailCall (f, List.map (fun v -> AtomVar v) vl) in
            LetFuns ([f, empty_line, FunLocalClass, TyFun (tl, ty_void), vl, ct kt st],
	      do_pattern kt st a p1 tc (fun kt st -> do_pattern kt st a p2 tc cf))
      | Tast.ConsPat (c, pl, ty) ->
	  let i = SymbolTable.find ienv (s_of_vp c) in
	  let is = recall kt a in
          let v = var_of_atom a in
	  if IntSet.mem_point i is then
	    if IntSet.is_singleton is then
	      do_pattern_list kt st v pl ct cf
	    else
	      let pis = IntSet.of_point i in
	      let nis = IntSet.subtract_point is i in
		Match (a, [
		  IntSet pis, do_pattern_list (SymbolTable.add kt v pis) st v pl ct cf;
		  IntSet nis, cf (SymbolTable.add kt v nis) st])
	  else
	    cf kt st
      | Tast.ConsCopyPat (c, p, ty) ->
	  let i = SymbolTable.find ienv (s_of_vp c) in
	  let ct kt st = match p with
	      Tast.AnyPat _ -> ct kt st
	    | Tast.IdPat (v, ty) ->
                let ty = make_type tenv ty in
		let tl = explode_tuple_type tenv ty in
                let tl = List.map fst tl in
		let vl = List.map (fun _ -> new_symbol_string "sub") tl in
                let va = var_of_atom a in
		snd (List.fold_left2 (fun (i, e) t v ->
		    succ i, LetSubscript (block_poly_subop, v, t, va, AtomInt i, e))
		  (0, LetAlloc (v, AllocTuple (NormalTuple, [], ty, List.map (fun v -> AtomVar v) vl), ct kt st))
		  tl vl)
            | _ -> raise (IRException "invalid ConsCopyPat") in
	  let is = recall kt a in
	  if IntSet.mem_point i is then
	    if IntSet.is_singleton is then
	      ct kt st
	    else
	      let pis = IntSet.of_point i in
	      let nis = IntSet.subtract_point is i in
		Match (a, [
		  IntSet pis, ct (learn kt a pis) st;
		  IntSet nis, cf (learn kt a nis) st])
	  else
	    cf kt st
      | Tast.TuplePat (pl, ty)
      | Tast.RecordPat (pl, ty) ->
          let v = var_of_atom a in
	    do_pattern_list kt st v pl ct cf
      | Tast.ArrayPat (pl, ty) -> raise (NotImplemented "array patterns") in
  let rec no_branching = function
      Tast.IdPat _ | Tast.AnyPat _ | Tast.ConstPat _ | Tast.RangePat _ | Tast.ConsCopyPat _ ->
        true
    | Tast.AltPat _ ->
        false
    | Tast.AsPat (p, _, _) ->
        no_branching p
    | Tast.ConsPat (_, pl, _) | Tast.TuplePat (pl, _)
    | Tast.ArrayPat (pl, _) | Tast.RecordPat (pl, _) ->
        List.for_all no_branching pl in
  match pwel with
      [p, w, e] when no_branching p ->   (* optimize non-branching match *)
        let ct kt st =
          let e = make_expr tenv ienv e vh ct in
            match w with
                None -> e
              | Some w -> make_expr tenv ienv w None (fun a -> make_if_expr a e (cf ())) in
        let cf _ _ = cf () in
        let kt = learn SymbolTable.empty a (set_of_type tenv aty) in
          do_pattern kt SymbolIntTable.empty a p ct cf
    | _ -> (* branching match: requires a continuation function *)
        let f = new_symbol_string "fc" in
        let tc a = TailCall (f, [a]) in
        let rec do_match kt st a pwel =
          match pwel with
              [] -> cf ()
            | (p,w,e) :: pwel ->
      	        let cf kt st = do_match kt st a pwel in
	        let ct kt st =
                  let e = make_expr tenv ienv e None tc in
                    match w with
	              None -> e
                    | Some w -> make_expr tenv ienv w None (fun a -> make_if_expr a e (cf kt st)) in
	        do_pattern kt st a p ct cf in
        let kt = learn SymbolTable.empty a (set_of_type tenv aty) in
        let v = match vh with None -> new_symbol_string "v" | Some v -> v in
          LetFuns ([f, empty_line, FunLocalClass, TyFun ([rty], ty_void), [v], ct (AtomVar v)],
            do_match kt SymbolIntTable.empty a pwel)

let rec fold_defs tenv ienv en (eenv : mutable_ty list list) = function
    [] ->
      (tenv, ienv, en, eenv), Naml_type_boot.unit_expr
  | Tast.LetMod pel :: dl ->
      let env, e = fold_defs tenv ienv en eenv dl in
        env, List.fold_left (fun b (p, e) -> Tast.MatchExpr (e, [p, None, b], Naml_type_boot.unit_ty)) e pel
  | Tast.LetRecMod fl :: dl ->
      let env, e = fold_defs tenv ienv en eenv dl in
        env, Tast.LetRecExpr (fl, e, Naml_type_boot.unit_ty)
  | Tast.TypeMod tdl :: dl ->
      let hack_n = TyDefLambda ([], ty_void) in
      let hack_r = TyDefUnion ([], [[]]) in
      let simple = List.for_all (fun (_, l) -> l == []) in
      let tenv' = List.fold_left (fun tenv (s, td) ->
          SymbolTable.add tenv s (match td with
              Tast.TyDefLambda _ -> hack_n
            | Tast.TyDefRecord _ -> hack_r
            | Tast.TyDefUnion (_, _, ll) when simple ll -> hack_n
            | Tast.TyDefUnion (_, _, ll) -> TyDefUnion ([], List.map (fun _ -> []) ll)))
        tenv tdl in
      let tenv, ienv = List.fold_left (fun (tenv, ienv) (s, td) ->
          match td with
              Tast.TyDefLambda (sl, _, ty) ->
                let td = TyDefLambda (sl, make_type tenv' ty) in
                  SymbolTable.add tenv s td, ienv
            | Tast.TyDefUnion (sl, _, stll) ->
                if simple stll then
                  let td = TyDefLambda (sl, TyEnum (List.length stll)) in
                  let ienv, _ = List.fold_left (fun (ienv, i) (s, _) ->
                      SymbolTable.add ienv s i, i+1) (ienv, i_flag) stll in
                    SymbolTable.add tenv s td, ienv
                else
                  let td = TyDefUnion (sl, List.map (fun (s, tl) ->
                    List.map (fun t -> make_type tenv' t, Immutable) tl) stll) in
                  let ienv, _ = List.fold_left (fun (ienv, i) (s, _) ->
                      SymbolTable.add ienv s i, i+1) (ienv, 0) stll in
                    SymbolTable.add tenv s td, ienv
            | Tast.TyDefRecord (sl, _, bstl) ->
                let td = TyDefUnion (sl, [List.map (fun (b, _, t) ->
                  make_type tenv' t, if b then Mutable else Immutable) bstl]) in
                let ienv, _ = List.fold_left (fun (ienv, i) (_, s, _) ->
                    SymbolTable.add ienv s i, i+1) (ienv, 0) bstl in
                  SymbolTable.add tenv s td, ienv)
        (tenv, ienv) tdl in
      fold_defs tenv ienv en eenv dl
  | Tast.ExnMod ed :: dl ->
      (match ed with
          Tast.ExnVar (s, v) ->
            let i = SymbolTable.find ienv (s_of_vp v) in
            let ienv = SymbolTable.add ienv s i in
              fold_defs tenv ienv en eenv dl
        | Tast.ExnCons (s, tl) ->
            let ienv = SymbolTable.add ienv s en in
            let en = en + 1 in
            let eenv = eenv @ [List.map (fun t -> make_type tenv t, Immutable) tl] in
              fold_defs tenv ienv en eenv dl)
  | Tast.ExternMod (s, t, st) :: dl ->
      let env, e = fold_defs tenv ienv en eenv dl in
	env, Tast.LetExtExpr (s, t, st, e)

let make_prog tast =
  let (tenv, ienv, en, eenv), e = fold_defs SymbolTable.empty SymbolTable.empty 0 [] tast in
  let tenv = SymbolTable.add tenv exn_symbol (TyDefUnion ([], eenv)) in
  let e = make_expr tenv ienv e None (fun a -> TailCall (fini_symbol, [AtomInt 0])) in
    { prog_types = tenv;
      prog_cont = fini_symbol;
      prog_body = e }

