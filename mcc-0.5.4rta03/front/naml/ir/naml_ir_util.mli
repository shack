(*
 * Utilities for working on syntax.
 * Geoffrey Irving / Dylan Simon
 * 2001-05-27
 *)

open Symbol
open Fir
open Fir_env
open Naml_ir

val apply_type : tenv -> ty_var -> ty list -> ty
val type_of_alloc_op : alloc_op -> ty
val polymorphic : ty -> bool

(*
 * Printing
 *)
val print_expr : exp -> unit
val print_prog : prog -> unit
