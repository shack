(* naml_ir_cps.ml
 * cps conversion on naml ir
 * $Id: naml_ir_cps.ml,v 1.36 2002/12/23 00:13:19 jyh Exp $
 * 2001-06-17
 *)

open Symbol
open Fir
open Fir_type
open Naml_ir
open Naml_ir_misc
open Naml_ir_exn

let type_exnh = TyFun ([type_exn], ty_void)

(* convert to cps types -- note that this only applies to global functions -- see comment *)
let rec cps_type t =
  match t with
      TyInt | TyFloat _ | TyEnum _ | TyVar _ -> t
    | TyFun (at, rt) ->
        if rt = ty_void then    (* rather a dangerous thing to do, but probably correct *)
          TyFun (cps_type_list at, ty_void)
        else
          TyFun (cps_type_list at @ [type_exnh; TyFun ([cps_type rt], ty_void)], ty_void)
    | TyUnion (tv, tl, i) ->
        TyUnion (tv, cps_type_list tl, i)
    | TyTuple (tclass, tl) ->
        TyTuple (tclass, List.map (fun (ty, b) -> cps_type ty, b) tl)
    | TyArray t ->
        TyArray (cps_type t)
    | TyFrame (label, tl) ->
        TyFrame (label, List.map cps_type tl)
    | TyApply (tv, tl) ->
        TyApply (tv, cps_type_list tl)
    | TyDelayed ->
        TyDelayed
    | TyRawData
    | TyProject _
    | TyAll _
    | TyExists _
    | TyRawInt _
    | TyCase _
    | TyObject _
    | TyPointer _
    | TyDTuple _
    | TyTag _ ->
        raise (IRException "invalid type seen by cps_type")

and cps_type_list x = List.map cps_type x

let cps_tydef td =
  match td with
      TyDefLambda (tvl, t) ->
        TyDefLambda (tvl, cps_type t)
    | TyDefUnion (tvl, tbll) ->
         TyDefUnion (tvl, List.map (List.map (fun (t, b) -> cps_type t, b)) tbll)
    | TyDefDTuple _ ->
         td

let cps_aop = function
   AllocTuple (tclass, ty_vars, t, al) ->
      AllocTuple (tclass, ty_vars, cps_type t, al)
 | AllocDTuple (ty, ty_var, a, al) ->
      AllocDTuple (cps_type ty, ty_var, a, al)
 | AllocArray (t, al) ->
      AllocArray (cps_type t, al)
 | AllocVArray (t, sub_index, a1, a2) ->
      AllocVArray (cps_type t, sub_index, a1, a2)
 | AllocUnion (ty_vars, t, tv, i, al) ->
      AllocUnion (ty_vars, cps_type t, tv, i, al)
 | AllocMalloc _
 | AllocFrame _ as aop -> aop

(* perform CPS conversion on an expression e with continuation cs and exception handler eh
 * fct - function var => function class table *)
let rec cps_expr fct e eh cs =
   match e with
      LetAtom (v, t, a, e) ->
         LetAtom (v, cps_type t, a, cps_expr fct e eh cs)
    | LetUnop (v, t, op, a, e) ->
         LetUnop (v, cps_type t, op, a, cps_expr fct e eh cs)
    | LetBinop (v, t, op, a1, a2, e) ->
         LetBinop (v, cps_type t, op, a1, a2, cps_expr fct e eh cs)
    | LetExt (v, t, s, ty, al, e) ->
         LetExt (v, cps_type t, s, ty, al, cps_expr fct e eh cs)
    | TailCall _ -> (* up until this point, all tail calls are to local functions *)
         e
    | Match (a, sel) ->
         Match (a, List.map (fun (s, e) -> s, cps_expr fct e eh cs) sel)
    | LetAlloc (v, aop, e) ->
         LetAlloc (v, cps_aop aop, cps_expr fct e eh cs)
    | LetSubscript (so, v, t, a, ai, e) ->
         LetSubscript (so, v, cps_type t, a, ai, cps_expr fct e eh cs)
    | SetSubscript (so, a, av, t, v, e) ->
         SetSubscript (so, a, av, cps_type t, v, cps_expr fct e eh cs)
    | LetFuns (fdl, e) ->
         let fct = List.fold_left (fun fct (v, _, fc, _, _, _) -> SymbolTable.add fct v fc) fct fdl in
            LetFuns (List.map (fun (v, dl, fc, t, vl, e) ->
                           match fc with
                              FunLocalClass ->
                                 (match t with
                                     TyFun (atl, _) ->
                                        v, dl, fc, TyFun (cps_type_list atl, ty_void), vl, cps_expr fct e eh cs
                                   | _ -> raise (IRException "function not TyFun in cps_expr"))
                            | FunContClass -> (* all the cont class functions created before now were exception handlers, btw *)
                                 (match t with
                                     TyFun (atl, _) ->
                                        let eh = new_symbol eh in
                                           v, dl, fc, TyFun (type_exnh :: cps_type_list atl, ty_void), eh :: vl, cps_expr fct e eh cs
                                   | _ -> raise (IRException "function not TyFun in cps_expr"))
                            | FunGlobalClass ->
                                 let c = new_symbol_string "cont" in
                                 let eh = new_symbol eh in
                                    v, dl, fc, cps_type t, vl @ [eh; c], cps_expr fct e eh c)
                     fdl, cps_expr fct e eh cs)
    | LetClosure (v, t, f, al, e) ->
         (match SymbolTable.find fct f with
             FunContClass ->
                (match t with
                    TyFun (atl, _) ->
                       LetClosure (v, TyFun (cps_type_list atl, ty_void), f, AtomVar eh :: al, cps_expr fct e eh cs)
                  | _ -> raise (IRException "function not TyFun in cps_expr"))
           | _ ->
                LetClosure (v, cps_type t, f, al, cps_expr fct e eh cs))
    | LetApply (v, t, f, al, e) ->
         (match e with
             Return (AtomVar v') when Symbol.eq v v' ->
                TailCall (f, al @ [AtomVar eh; AtomVar cs])
           | _ ->
                let c = new_symbol v in
                let cc = new_symbol v in
                let ft = TyFun ([cps_type t], ty_void) in
                   LetFuns ([c, naml_loc, FunContClass, ft, [v], cps_expr fct e eh cs],
                            LetClosure (cc, ft, c, [],
                                        TailCall (f, al @ [AtomVar eh; AtomVar cc]))))
    | Return a ->
         TailCall (cs, [a])
    | LetExnHandler (f, e) ->
         cps_expr fct e f cs
    | Raise a ->
         TailCall (eh, [a])

let cps_prog prog =
  let { prog_types = tenv; prog_cont = fini; prog_body = e } = prog in
  let tenv = SymbolTable.map cps_tydef tenv in
  let exnh = new_symbol_string "exnh" in
  let ce = new_symbol exnh in
  let fct = SymbolTable.add SymbolTable.empty exnh FunContClass in
  let e =
    LetFuns ([exnh, naml_loc, FunContClass, type_exnh, [new_symbol_string "e"], TailCall (fini, [AtomInt 1])],
      LetClosure (ce, type_exnh, exnh, [],
        cps_expr fct e ce fini)) in
    { prog_types = tenv;
      prog_cont = fini;
      prog_body = e }
