(* Naml IR externals definitions
 * $Id: naml_ir_ext.ml,v 1.29 2002/09/02 21:10:36 jyh Exp $
 * 2001-06-23
 *)

open Symbol
open Fir
open Fir_set
open Fir_type
open Naml_ir
open Naml_ir_misc
open Naml_ir_exn

let make_atomop ty =
  let a = new_symbol_string "a" in
  let r = new_symbol_string "r" in
    [a],
    LetAtom (r, ty, AtomVar a,
    Return (AtomVar r))

let make_unop ty op =
  let a = new_symbol_string "a" in
  let r = new_symbol_string "r" in
    [a],
    LetUnop (r, ty, op, AtomVar a,
    Return (AtomVar r))

let make_binop ty op =
  let a = new_symbol_string "a" in
  let b = new_symbol_string "b" in
  let r = new_symbol_string "r" in
    [a; b],
    LetBinop (r, ty, op, AtomVar a, AtomVar b,
    Return (AtomVar r))

let make_float_unop op =
  let a = new_symbol_string "a" in
  let r = new_symbol_string "r" in
  let x = new_symbol_string "x" in
  let t = new_symbol_string "t" in
    [a],
    LetSubscript (float_subop, x, ty_float, a, AtomInt 0,
    LetUnop (r, ty_float, op, AtomVar x,
    LetAlloc (t, AllocTuple (NormalTuple, [], type_float, [AtomVar r]),
    Return (AtomVar t))))

let make_float_binop op =
  let a = new_symbol_string "a" in
  let b = new_symbol_string "b" in
  let r = new_symbol_string "r" in
  let x = new_symbol_string "x" in
  let y = new_symbol_string "y" in
  let t = new_symbol_string "t" in
    [a; b],
    LetSubscript (float_subop, x, ty_float, a, AtomInt 0,
    LetSubscript (float_subop, y, ty_float, b, AtomInt 0,
    LetBinop (r, ty_float, op, AtomVar x, AtomVar y,
    LetAlloc (t, AllocTuple (NormalTuple, [], type_float, [AtomVar r]),
    Return (AtomVar t)))))

let make_ext_unop rty op aty =
  let a = new_symbol_string "a" in
  let r = new_symbol_string "r" in
    [a],
    LetExt (r, rty, op, TyFun ([aty], rty), [AtomVar a],
    Return (AtomVar r))

let make_ext_binop rty op aty1 aty2 =
  let a = new_symbol_string "a" in
  let b = new_symbol_string "b" in
  let r = new_symbol_string "r" in
    [a; b],
    LetExt (r, rty, op, TyFun ([aty1; aty2], rty), [AtomVar a; AtomVar b],
    Return (AtomVar r))

let make_extpoly_binop ty op =
  make_ext_binop ty op TyDelayed TyDelayed

let make_comparison_binop op =
  let a = new_symbol_string "a" in
  let b = new_symbol_string "b" in
  let n = new_symbol_string "n" in
  let r = new_symbol_string "r" in
    [a; b],
    LetExt (n, TyInt, "ml_cmp", TyDelayed, [AtomVar a; AtomVar b],
    LetBinop (r, ty_bool, op, AtomVar n, AtomInt 0,
    Return (AtomVar r)))

let fundef_of_extern (* argty rty *) = function
  | "%raise" -> (* exn -> 'a *)
      let a = new_symbol_string "a" in
	[a],
	Raise (AtomVar a)
  | "%exit" -> (* int -> 'a *)
      let a = new_symbol_string "a" in
	[a],
	TailCall (fini_symbol, [AtomVar a])
  | "%fst" -> (* 'a * 'b -> 'a *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
	[a],
	LetSubscript (block_poly_subop, r, TyDelayed, a, AtomInt 0,
	Return (AtomVar r))
  | "%snd" -> (* 'a * 'b -> 'b *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
	[a],
	LetSubscript (block_poly_subop, r, TyDelayed, a, AtomInt 1,
	Return (AtomVar r))

  | "%cmp_eq" -> (* 'a -> 'a -> bool *)
      make_binop ty_bool (EqEqOp TyDelayed)
  | "%cmp_neq" -> (* 'a -> 'a -> bool *)
      make_binop ty_bool (NeqEqOp TyDelayed)

  | "%cmp_compare" -> (* 'a -> 'a -> int *)
      make_extpoly_binop TyInt "ml_cmp"
  | "%cmp_equal" -> (* 'a -> 'a -> bool *)
      make_comparison_binop EqIntOp
  | "%cmp_nequal" -> (* 'a -> 'a -> bool *)
      make_comparison_binop NeqIntOp
  | "%cmp_ge" -> (* 'a -> 'a -> bool *)
      make_comparison_binop GeIntOp
  | "%cmp_gt" -> (* 'a -> 'a -> bool *)
      make_comparison_binop GtIntOp
  | "%cmp_le" -> (* 'a -> 'a -> bool *)
      make_comparison_binop LeIntOp
  | "%cmp_lt" -> (* 'a -> 'a -> bool *)
      make_comparison_binop LtIntOp

  | "%int_neg" -> (* int -> int *)
      make_unop TyInt UMinusIntOp
  | "%int_not" -> (* int -> int *)
      make_unop TyInt NotIntOp
  | "%int_pred" -> (* int -> int *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
	[a],
	LetBinop (r, TyInt, MinusIntOp, AtomVar a, AtomInt 1,
	Return (AtomVar r))
  | "%int_succ" -> (* int -> int *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
	[a],
	LetBinop (r, TyInt, PlusIntOp, AtomVar a, AtomInt 1,
	Return (AtomVar r))
  | "%int_abs" -> (* int -> int *)
      (* This should probably be written more efficiently using x & min_int (sign bit) *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
      let pos = IntSet.of_interval (Interval_set.Closed 0) Interval_set.Infinity in
      let neg = IntSet.of_interval Interval_set.Infinity (Interval_set.Open 0) in
	[a],
	Match (AtomVar a, [
	  IntSet pos, Return (AtomVar a);
	  IntSet neg,
	    LetUnop (r, TyInt, UMinusIntOp, AtomVar a,
	    Return (AtomVar r))])
  | "%int_add" -> (* int -> int -> int *)
      make_binop TyInt PlusIntOp
  | "%int_sub" -> (* int -> int -> int *)
      make_binop TyInt MinusIntOp
  | "%int_mul" -> (* int -> int -> int *)
      make_binop TyInt MulIntOp
  | "%int_div" -> (* int -> int -> int *)
      make_binop TyInt DivIntOp
  | "%int_mod" -> (* int -> int -> int *)
      make_binop TyInt RemIntOp
  | "%int_and" -> (* int -> int -> int *)
      make_binop TyInt AndIntOp
  | "%int_or" -> (* int -> int -> int *)
      make_binop TyInt OrIntOp
  | "%int_xor" -> (* int -> int -> int *)
      make_binop TyInt XorIntOp
  | "%int_shl" -> (* int -> int -> int *)
      make_binop TyInt LslIntOp
  | "%int_shr" -> (* int -> int -> int *)
      make_binop TyInt LsrIntOp
  | "%int_sar" -> (* int -> int -> int *)
      make_binop TyInt AsrIntOp

  | "%float_neg" -> (* float -> float *)
      make_float_unop (UMinusFloatOp Rawfloat.Double)
  | "%float_abs" -> (* float -> float *)
      make_float_unop (AbsFloatOp Rawfloat.Double)
  | "%float_add" -> (* float -> float -> float *)
      make_float_binop (PlusFloatOp Rawfloat.Double)
  | "%float_sub" -> (* float -> float -> float *)
      make_float_binop (MinusFloatOp Rawfloat.Double)
  | "%float_mul" -> (* float -> float -> float *)
      make_float_binop (MulFloatOp Rawfloat.Double)
  | "%float_div" -> (* float -> float -> float *)
      make_float_binop (DivFloatOp Rawfloat.Double)
  | "%float_mod" -> (* float -> float -> float *)
      make_float_binop (RemFloatOp Rawfloat.Double)
  | "%float_power" -> (* float -> float -> float *)
      make_ext_binop type_float "fpow" type_float type_float
  | "%float_atan2" -> (* float -> float -> float *)
      make_ext_binop type_float "fatan2" type_float type_float
  | "%float_floor" -> (* float -> float *)
      make_ext_unop type_float "ffloor" type_float
  | "%float_log10" -> (* float -> float *)
      make_ext_unop type_float "flog10" type_float
  | "%float_acos" -> (* float -> float *)
      make_ext_unop type_float "facos" type_float
  | "%float_asin" -> (* float -> float *)
      make_ext_unop type_float "fasin" type_float
  | "%float_atan" -> (* float -> float *)
      make_ext_unop type_float "fatan" type_float
  | "%float_ceil" -> (* float -> float *)
      make_ext_unop type_float "fceil" type_float
  | "%float_cosh" -> (* float -> float *)
      make_ext_unop type_float "fcosh" type_float
  | "%float_sinh" -> (* float -> float *)
      make_ext_unop type_float "fsinh" type_float
  | "%float_sqrt" -> (* float -> float *)
      make_ext_unop type_float "fsqrt" type_float
  | "%float_tanh" -> (* float -> float *)
      make_ext_unop type_float "ftanh" type_float
  | "%float_cos" -> (* float -> float *)
      make_ext_unop type_float "fcos" type_float
  | "%float_exp" -> (* float -> float *)
      make_ext_unop type_float "fexp" type_float
  | "%float_log" -> (* float -> float *)
      make_ext_unop type_float "flog" type_float
  | "%float_sin" -> (* float -> float *)
      make_ext_unop type_float "fsin" type_float
  | "%float_tan" -> (* float -> float *)
      make_ext_unop type_float "ftan" type_float
  | "%float_rexp" -> (* float -> float * int *)
      (* (x, n) = frexp f <=> f = x * 2^n, 0.5 <= x < 1 *)
      (* maybe I could do this here... *)
      make_ext_unop (TyTuple (NormalTuple, [type_float, Immutable; TyInt, Immutable])) "frexp" type_float
  | "%float_ldexp" -> (* float -> int -> float *)
      (* fldexp x n = x * 2^n *)
      (* I can't really do this here, it's not as simple as (float)1<<n * x -- prec *)
      make_ext_binop type_float "fldexp" TyInt type_float
  | "%float_modf" -> (* float -> float * float *)
      make_ext_unop (TyTuple (NormalTuple, [type_float, Immutable; type_float, Immutable])) "fmodf" type_float

  | "%char_int" -> (* char -> int *)
      make_atomop TyInt
  | "%int_char" -> (* int -> char *) (* if you think this does the wrong thing, see pervasives *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
	[a],
	LetBinop (r, type_char, AndIntOp, AtomVar a, AtomInt (1 lsl 8 - 1),
	Return (AtomVar r))
  | "%float_int" -> (* float -> int *)
      let a = new_symbol_string "a" in
      let r = new_symbol_string "r" in
      let x = new_symbol_string "x" in
        [a],
        LetSubscript (float_subop, x, ty_float, a, AtomInt 0,
        LetUnop (r, TyInt, IntOfFloatOp Rawfloat.Double, AtomVar x,
        Return (AtomVar r)))
  | "%int_float" -> (* int -> float *)
      let r = new_symbol_string "r" in
      let x = new_symbol_string "x" in
      let t = new_symbol_string "t" in
        [x],
        LetUnop (r, ty_float, FloatOfIntOp Rawfloat.Double, AtomVar x,
        LetAlloc (t, AllocTuple (NormalTuple, [], type_float, [AtomVar r]),
        Return (AtomVar t)))

  (* I /could/ write these here, but I'm not going to *)
  | "%float_string" -> (* float -> string *)
      make_ext_unop type_string "string_of_float" type_float
  | "%int_string" -> (* int -> string *)
      make_ext_unop type_string "string_of_int" TyInt
  | "%string_float" -> (* string -> float *)
      make_ext_unop type_float "float_of_string" type_string
  | "%string_int" -> (* string -> int *)
      make_ext_unop TyInt "int_of_string" type_string

  | "%array_length" -> (* 'a array -> int *)
      make_ext_unop TyInt "array_length" (TyArray TyDelayed)
  | "%array_get" -> (* 'a array -> int -> 'a *)
      let a = new_symbol_string "a" in
      let i = new_symbol_string "i" in
      let r = new_symbol_string "r" in
	[a; i],
	LetSubscript (block_poly_subop, r, TyDelayed, a, AtomVar i,
	Return (AtomVar r))
  | "%array_set" -> (* 'a array -> int -> 'a -> unit *)
      let a = new_symbol_string "a" in
      let i = new_symbol_string "i" in
      let v = new_symbol_string "v" in
	[a; i; v],
	SetSubscript (block_poly_subop, a, AtomVar i, TyDelayed, AtomVar v,
	Return (atom_unit))
  (*
  | "%array_create" -> (* int -> 'a -> 'a array *)
      (* this one's gonna need to be different -- we need an actual number, or a loop (yuck) *)
      let l = new_symbol_string "l" in
      let v = new_symbol_string "v" in
      let a = new_symbol_string "a" in
	[l; v],
	LetAlloc (a, AllocArray (TyDelayed, AtomVar l, AtomVar v),
	Return (AtomVar a))
  | "%array_append" -> (* 'a array -> 'a array -> 'a array *)
  | "%array_sub" -> (* 'a array -> int -> int -> 'a array *)
  | "%array_copy" -> (* 'a array -> 'a array *)
  | "%array_fill" -> (* 'a array -> int -> int -> 'a *)
  | "%array_blit" -> (* 'a array -> int -> 'a array -> int -> int -> unit *)
  *)

  (* stuff in the evaluator *)
  | "%print_string" -> (* string -> unit *)
      make_ext_unop ty_unit "ml_print_string" (TyArray type_char)
  | "%print_char" -> (* char -> unit *)
      make_ext_unop ty_unit "ml_print_char" type_char
  | "%print_int" -> (* int -> unit *)
      make_ext_unop ty_unit "ml_print_int" TyInt
  | "%output_byte" -> (* int -> unit *)
      make_ext_unop ty_unit "ml_output_byte" TyInt

  | s -> raise (Invalid_Extern s)

let fix_type n t =
  let rec strip n = function
      TyFun (tl, t) ->
        let m = List.length tl in
          if n < m then
            let h, tl = Mc_list_util.split n tl in
              h, TyFun (tl, t)
          else if n > m then
            let h, t = strip (n - m) t in
              tl @ h, t
          else
            tl, t
    | _ -> raise (Invalid_argument "fix_type in naml_ir_ext.ml") in
  let tl, t = strip n t in
    TyFun (tl, t)

let make_extern_def s dl t ext e =
  match ext with
    (* first we do the special (non-function) ones *)
    | "%int_max" -> LetAtom (s, t, AtomInt max_int, e)
    | "%int_min" -> LetAtom (s, t, AtomInt min_int, e)
    (* and the rest are functions *)
    | _ ->
	let al, b = fundef_of_extern (* argty rty *) ext in
        let t = fix_type (List.length al) t in
	  LetFuns ([s, dl, FunGlobalClass, t, al, b], e)
