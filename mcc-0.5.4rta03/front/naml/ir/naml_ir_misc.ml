(* Miscellanous IR values
 * Dylan Simon, Geoffrey Irving
 * 24may01
 *)

open Symbol
open Location
open Fir
open Fir_ds
open Fir_set
open Fir_type
open Naml_ir

let naml_loc = bogus_loc "<naml>"
let naml_exp e = make_exp naml_loc e

let fini_symbol = new_symbol_string "fini"
let exn_symbol = new_symbol_string "exn"

let set_zero = IntSet int_set_zero
let set_one = IntSet int_set_one

let ty_float = TyFloat Rawfloat.Double

let type_char = TyEnum 256
let type_float = TyTuple (NormalTuple, [TyFloat Rawfloat.Double, Immutable])
let type_exn = TyUnion (exn_symbol, [], IntSet.max_set)
let type_string = TyArray type_char

let atom_unit = AtomEnum (1, 0)

let atom_false = AtomEnum (2, 0)
let atom_true = AtomEnum (2, 1)

let type_fini = TyFun ([TyInt], ty_void)
let type_fini_aux = TyApply (fini_symbol, [])

let garbage_fir = naml_exp (Fir.TailCall (new_symbol_string "garbage", AtomVar (new_symbol_string "garbage"), []))

let float_subop =
   { sub_block = BlockSub;
     sub_value = RawFloatSub Rawfloat.Double;
     sub_index = WordIndex;
     sub_script = IntIndex
   }

let block_poly_subop =
   { sub_block = BlockSub;
     sub_value = PolySub;
     sub_index = WordIndex;
     sub_script = IntIndex
   }
