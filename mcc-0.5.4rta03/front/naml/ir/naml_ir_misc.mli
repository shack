(* Miscellanous IR values
 * Dylan Simon, Geoffrey Irving
 * 24may01
 *)

open Symbol
open Location
open Fir
open Naml_ir

val naml_loc : loc
val naml_exp : Fir.exp_core -> Fir.exp

val fini_symbol : var
val type_fini : ty
val type_fini_aux : ty
val exn_symbol : var

val set_zero : set
val set_one : set

val type_char : ty
val type_float : ty
val type_exn : ty
val type_string : ty

val ty_float : ty

val atom_unit : atom
val atom_false : atom
val atom_true : atom

val garbage_fir : Fir.exp

val float_subop : subop
val block_poly_subop : subop
