(*
 * Simple naml ir dead code elimination
 * Geoffrey Irving
 * $Id: naml_ir_dead.ml,v 1.11 2002/12/23 00:13:19 jyh Exp $
 *)

open Symbol
open Fir
open Fir_set
open Naml_ir
open Naml_ir_misc
open Naml_ir_exn
open Naml_ir_util
open Naml_ir_standardize

let live_atom lv = function
    AtomVar v ->
      SymbolSet.add lv v
  | _ -> lv

let live_atoms = List.fold_left live_atom

let live_aop lv = function
   AllocTuple (_, _, _, al)
 | AllocArray (_, al)
 | AllocUnion (_, _, _, _, al) ->
      live_atoms lv al
 | AllocDTuple (_, _, a, al) ->
      live_atoms (live_atom lv a) al
 | AllocVArray (_, _, a1, a2) ->
      live_atoms lv [a1; a2]
 | AllocMalloc (_, a) ->
      live_atom lv a
 | AllocFrame _ ->
      lv

let rec dead_expr e =
   match e with
      LetAtom (v, ty, a, e) ->
         let lv, e = dead_expr e in
            if SymbolSet.mem lv v then
               live_atom lv a, LetAtom (v, ty, a, e)
            else
               lv, e
    | LetUnop (v, ty, op, a, e) ->
         let lv, e = dead_expr e in
            if SymbolSet.mem lv v then
               live_atom lv a, LetUnop (v, ty, op, a, e)
            else
               lv, e
    | LetBinop (v, ty, op, a1, a2, e) ->
         let lv, e = dead_expr e in
            if SymbolSet.mem lv v then
               live_atom (live_atom lv a1) a2, LetBinop (v, ty, op, a1, a2, e)
            else
               lv, e
    | LetExt (v, ty, s, ty2, al, e) ->
         let lv, e = dead_expr e in
            live_atoms lv al, LetExt (v, ty, s, ty2, al, e)
    | TailCall (f, al) ->
         live_atoms (SymbolSet.add SymbolSet.empty f) al, e
    | Match (a, sel) ->
         let lv, sel = Mc_list_util.fold_map (fun lv (s, e) ->
                             let lv', e = dead_expr e in
                                SymbolSet.union lv lv', (s, e)) SymbolSet.empty sel
         in
            live_atom lv a, Match (a, sel)
    | LetAlloc (v, aop, e) ->
         let lv, e = dead_expr e in
            if SymbolSet.mem lv v then
               live_aop lv aop, LetAlloc (v, aop, e)
            else
               lv, e
    | LetSubscript (so, v, ty, v2, a, e) ->
         let lv, e = dead_expr e in
            if SymbolSet.mem lv v then
               SymbolSet.add (live_atom lv a) v2, LetSubscript (so, v, ty, v2, a, e)
            else
               lv, e
    | SetSubscript (so, v, a1, ty, a2, e) ->
         let lv, e = dead_expr e in
            SymbolSet.add (live_atom (live_atom lv a2) a1) v, SetSubscript (so, v, a1, ty, a2, e)
    | LetFuns (fl, e) ->
         let lv, e = dead_expr e in
         let lv, fl = Mc_list_util.fold_map (fun lv (f, dl, fc, ty, vl, e) ->
                            let lv', e = dead_expr e in
                               SymbolSet.union lv lv', (f, dl, fc, ty, vl, e))
                      lv fl in
         let fl = List.filter (fun (f, _, _, _, _, _) -> SymbolSet.mem lv f) fl in
            lv, LetFuns (fl, e)
    | LetApply (v, ty, f, al, e) ->
         let lv, e = dead_expr e in
            SymbolSet.add (live_atoms lv al) f, LetApply (v, ty, f, al, e)
    | LetClosure (v, ty, f, al, e) ->
         let lv, e = dead_expr e in
            SymbolSet.add (live_atoms lv al) f, LetClosure (v, ty, f, al, e)
    | Return a
    | Raise a ->
         live_atom SymbolSet.empty a, e
    | LetExnHandler (f, e) ->
         let lv, e = dead_expr e in
            SymbolSet.add lv f, LetExnHandler (f, e)

let dead_prog prog =
  let _, e = dead_expr prog.prog_body in
    { prog with prog_body = e }

