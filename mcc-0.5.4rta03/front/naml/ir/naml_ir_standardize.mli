(* Naml IR standardization
 * Geoffrey Irving
 * $Id: naml_ir_standardize.mli,v 1.1 2001/08/01 06:01:53 irving Exp $ *)

open Fir
open Naml_ir

val standardize_body : var list -> exp -> var list * exp
