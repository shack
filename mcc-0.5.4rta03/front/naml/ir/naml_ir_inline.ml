(*
 * Inline IR before it's screwed up by transformations
 * Geoffrey Irving
 * 24may01
 *)

open Symbol
open Fir
open Fir_set
open Fir_exn
open Fir_pos
open Fir_algebra
open Naml_ir
open Naml_ir_misc
open Naml_ir_exn
open Naml_ir_util
open Naml_ir_standardize

module Pos = MakePos (struct let name = "Naml_ir_inline" end)
open Pos

let debug_pos = var_exp_pos (Symbol.add "<Naml_ir_inline>")
let canonicalize_etree = canonicalize_etree_core debug_pos

let rec expr_complexity c = function
   LetAtom (_, _, _, e)
 | LetUnop (_, _, _, _, e)
 | LetBinop (_, _, _, _, _, e)
 | LetExt (_, _, _, _, _, e)
 | LetAlloc (_, _, e)
 | LetSubscript (_, _, _, _, _, e)
 | SetSubscript (_, _, _, _, _, e)
 | LetExnHandler (_, e) -> expr_complexity (c + 1) e
 | Raise _ | Return _ -> c
 | LetFuns _ | TailCall _ | LetApply _ | Match _ | LetClosure _ -> 1000

type data =
    DataAtom of atom
  | DataBox of var * int * data option list
  | DataClosure of var * var * atom list
  | DataFun of var * var list * exp option

let get_atom = function
    DataAtom a -> a
  | DataBox (v, _, _) | DataClosure (v, _, _) | DataFun (v, _, _) -> AtomVar v

let find_var venv v =
  try SymbolTable.find venv v with Not_found -> DataAtom (AtomVar v)

let find_atom venv a =
  match a with
      AtomVar v ->
        (try SymbolTable.find venv v
        with Not_found -> DataAtom a)
    | _ -> DataAtom a

let find_atoms venv = List.map (find_atom venv)

let add_atom venv v a = SymbolTable.add venv v (DataAtom a)
let add_box venv v i al = SymbolTable.add venv v (DataBox (v, i, al))

let rec inline_type stenv ty =
  let it = inline_type stenv in
  match ty with
      TyFun (tl, t) -> TyFun (List.map it tl, it t)
    | TyUnion (tv, tl, i) -> TyUnion (tv, List.map it tl, i)
    | TyTuple (tclass, tl) -> TyTuple (tclass, List.map (fun (ty, b) -> it ty, b) tl)
    | TyArray t -> TyArray (it t)
    | TyApply (tv, tl) ->
        let tl = List.map it tl in
        if SymbolTable.mem stenv tv then
          apply_type stenv tv tl
        else
          TyApply (tv, tl)
    | _ -> ty

let inline_atom stenv venv a =
  match a with
      AtomVar v ->
        (try
          get_atom (SymbolTable.find venv v)
        with
          Not_found -> a)
    | AtomConst (t, tv, i) ->
        AtomConst (inline_type stenv t, tv, i)
    | _ -> a

let inline_var venv v =
  try
    match get_atom (SymbolTable.find venv v) with
        AtomVar v -> v
      | _ -> raise (IRException "inline_var produced an non-var atom")
  with Not_found -> v

let inline_atoms stenv venv = List.map (inline_atom stenv venv)

let cont_return _ a = Return a

let etree_of_atom e =
   match e with
      AtomVar v -> ETVar v
    | a -> ETConst a

let rec inline_expr tenv stenv venv e c =
   match e with
      LetAtom (v, ty, a, e) ->
         let a = inline_atom stenv venv a in
            (match canonicalize_etree (etree_of_atom a) with
                ETConst a ->
                   let venv = add_atom venv v a in
                      inline_expr tenv stenv venv e c
              | ETVar v' ->
                   let venv = add_atom venv v (AtomVar v') in
                      inline_expr tenv stenv venv e c
              | _ ->
                   LetAtom (v, inline_type stenv ty, a, inline_expr tenv stenv venv e c))
    | LetUnop (v, ty, op, a, e) ->
         let a = inline_atom stenv venv a in
            (match canonicalize_etree (ETUnop (op, etree_of_atom a)) with
                ETConst a ->
                   let venv = add_atom venv v a in
                      inline_expr tenv stenv venv e c
              | ETVar v' ->
                   let venv = add_atom venv v (AtomVar v') in
                      inline_expr tenv stenv venv e c
              | _ ->
                   LetUnop (v, inline_type stenv ty, op, a, inline_expr tenv stenv venv e c))
    | LetBinop (v, ty, op, a1, a2, e) ->
         let a1 = inline_atom stenv venv a1 in
         let a2 = inline_atom stenv venv a2 in
            (match canonicalize_etree (ETBinop (op, etree_of_atom a1, etree_of_atom a2)) with
                ETConst a ->
                   let venv = add_atom venv v a in
                      inline_expr tenv stenv venv e c
              | ETVar v' ->
                   let venv = add_atom venv v (AtomVar v') in
                      inline_expr tenv stenv venv e c
              | _ ->
                   LetBinop (v, inline_type stenv ty, op, a1, a2, inline_expr tenv stenv venv e c))
    | LetExt (v, ty, s, ty2, al, e) ->
         let al = inline_atoms stenv venv al in
         let ty = inline_type stenv ty in
         let ty2 = inline_type stenv ty2 in
            LetExt (v, ty, s, ty2, al, inline_expr tenv stenv venv e c)
    | TailCall (f, al) ->
         let al = inline_atoms stenv venv al in
            (match find_var venv f with
                DataClosure (_, f, al2) ->
                   TailCall (f, al2 @ al)
              | DataAtom (AtomVar f)
              | DataFun (f, _, None) ->
                   TailCall (f, al)
              | DataFun (f, vl, Some b) ->
                   let vl, b = standardize_body vl b in
                   let venv = List.fold_left2 add_atom venv vl al in
                      inline_expr tenv stenv venv b c
              | _ -> TailCall (f, al))
    | Match (a, pl) ->
         let d = find_atom venv a in
         let rec branch i = function
            [] -> raise (Impossible "non-exhaustive match in inlining")
          | (s, e) :: pl ->
               match s with
                  IntSet s ->
                     if IntSet.mem_point i s then
                        inline_expr tenv stenv venv e c
                     else
                        branch i pl
                | _ -> raise (Impossible "set type mismatch in inlining") in
            (match d with
                DataAtom (AtomInt i | AtomEnum (_, i) | AtomConst (_, _, i)) | DataBox (_, i, _) ->
         branch i pl
    | _ ->
         Match (inline_atom stenv venv a,
                List.map (fun (s, e) -> s, inline_expr tenv stenv venv e c) pl))
    | LetAlloc (v, op, e) ->
        let op = match op with
            AllocTuple (tclass, tv, ty, al) -> AllocTuple (tclass, tv, inline_type stenv ty, inline_atoms stenv venv al)
          | AllocDTuple (ty, tv, a, al) -> AllocDTuple (inline_type stenv ty, tv, inline_atom stenv venv a, inline_atoms stenv venv al)
          | AllocArray (ty, al) -> AllocArray (inline_type stenv ty, inline_atoms stenv venv al)
          | AllocUnion (ty_vars, ty, tv, i, al) -> AllocUnion (ty_vars, inline_type stenv ty, tv, i, inline_atoms stenv venv al)
          | AllocVArray (ty, si, a1, a2) -> AllocVArray (inline_type stenv ty, si, inline_atom stenv venv a1, inline_atom stenv venv a2)
          | AllocMalloc (ty, a) -> AllocMalloc (ty, inline_atom stenv venv a)
          | AllocFrame _ -> op
        in
        let venv = match op with
            AllocTuple (_, _, _, al)
          | AllocDTuple (_, _, _, al) ->
              add_box venv v 0 (List.map (fun a -> Some (find_atom venv a)) al)
          | AllocArray (_, al) ->
              add_box venv v 0 (List.map (fun x -> None) al)
          | AllocUnion (_, _, tv, i, al) ->
              let ml = match SymbolTable.find tenv tv with
                  TyDefUnion (_, tbll) -> List.map snd (List.nth tbll i)
                | TyDefLambda _
                | TyDefDTuple _ -> raise (Impossible "invalid union name in inlining") in
              let do_atom b a = if b <> Immutable then None else Some (find_atom venv a) in
                add_box venv v i (List.map2 do_atom ml al)
            (* BUG: we could make a box for the MArray *)
          | AllocVArray _
          | AllocMalloc _
          | AllocFrame _ -> venv
        in
          LetAlloc (v, op, inline_expr tenv stenv venv e c)
    | LetSubscript (so, v, ty, v2, a, e) ->
        let ty = inline_type stenv ty in
        let v2 = inline_var venv v2 in
        let a = inline_atom stenv venv a in
        (match a with
            AtomInt i ->
              (match find_var venv v2 with
                  DataBox (_, _, dl) ->
                    (match List.nth dl i with
                        None ->
                          LetSubscript (so, v, ty, v2, a, inline_expr tenv stenv venv e c)
                      | Some d ->
                          let venv = SymbolTable.add venv v d in
                            inline_expr tenv stenv venv e c)
                | _ -> LetSubscript (so, v, ty, v2, a, inline_expr tenv stenv venv e c))
          | _ -> LetSubscript (so, v, ty, v2, a, inline_expr tenv stenv venv e c))
    | SetSubscript (so, v, a1, ty, a2, e) ->
        let ty = inline_type stenv ty in
        let v = inline_var venv v in
        let a1 = inline_atom stenv venv a1 in
        let a2 = inline_atom stenv venv a2 in
          SetSubscript (so, v, a1, ty, a2, inline_expr tenv stenv venv e c)
    | LetFuns (fl, e) ->
        let fl = List.map (fun (f, dl, c, t, vl, e) ->
          f, dl, c, inline_type stenv t, vl, inline_expr tenv stenv venv e cont_return) fl in
        let venv = match fl with
            [f, _, _, t, vl, e] ->
              let eo = if not (polymorphic t) && expr_complexity 0 e < 10 then Some e else None in
                SymbolTable.add venv f (DataFun (f, vl, eo))
          | _ -> List.fold_left (fun venv (f, _, _, _, vl, _) ->
              SymbolTable.add venv f (DataFun (f, vl, None))) venv fl
        in
          LetFuns (fl, inline_expr tenv stenv venv e c)
    | LetApply (v, ty, f, al, e) ->
        let ty = inline_type stenv ty in
        let al = inline_atoms stenv venv al in
        let rec do_apply f al =
          match find_var venv f with
              DataClosure (_, f, al2) ->
                do_apply f (al2 @ al)
            | DataFun (f, vl, eo) ->
                if Mc_list_util.length_eq vl al then
                  match eo with
                      None ->
                        LetApply (v, ty, f, al, inline_expr tenv stenv venv e c)
                    | Some b ->
                        let vl, b = standardize_body vl b in
                        let venv = List.fold_left2 add_atom venv vl al in
                          inline_expr tenv stenv venv b (fun venv a ->
                            inline_expr tenv stenv (add_atom venv v a) e c)
                else
                  let venv = SymbolTable.add venv v (DataClosure (v, f, al)) in
                    LetApply (v, ty, f, al, inline_expr tenv stenv venv e c)
            | DataAtom (AtomVar f) ->
                LetApply (v, ty, f, al, inline_expr tenv stenv venv e c)
            | _ -> LetApply (v, ty, f, al, inline_expr tenv stenv venv e c)
        in
          do_apply f al
    | Return a ->
        c venv (inline_atom stenv venv a)
    | LetExnHandler (f, e) ->
	LetExnHandler (f, inline_expr tenv stenv venv e c)
    | Raise a ->
        Raise (inline_atom stenv venv a)
    | LetClosure _ -> raise (IRException "LetClosure in inlining")

let rec type_complexity c t =
  let tc = type_complexity in
  let tlc = List.fold_left tc in
  match t with
      TyInt | TyFloat _ -> c + 0
    | TyEnum _ -> c + 2
    | TyRawInt _ -> c + 3
    | TyTuple (_, tl) -> let tl = List.map fst tl in tlc (c+2+3*List.length tl) tl
    | TyFun (tl, t) -> tlc (tc (c + 3 + 3 * List.length tl) t) tl
    | TyUnion (_, tl, _)
    | TyApply (_, tl) -> tlc (c + 5 + 3 * List.length tl) tl
    | TyArray t -> tc (c + 2) t
    | TyVar _ -> c + 100000
    | _ -> raise (IRException "weird type seen by type_complexity")

(*** find small type definitions and set them up for inlining ***)
let inline_tenv tenv =
  let rec do_ty env t =
    match t with
        TyInt | TyFloat _ | TyEnum _ | TyRawInt _ | TyVar _ ->
          env, t
      | TyTuple (tclass, tl) ->
          let env, tl = Mc_list_util.fold_map (fun env (ty, b) -> let env, ty = do_ty env ty in env, (ty, b)) env tl in
            env, TyTuple (tclass, tl)
      | TyFun (tl, t) ->
          let env, tl = Mc_list_util.fold_map do_ty env tl in
          let env, t = do_ty env t in
            env, TyFun (tl, t)
      | TyUnion (v, tl, io) ->
          let env, tl = Mc_list_util.fold_map do_ty env tl in
            env, TyUnion (v, tl, io)
      | TyArray t ->
          let env, t = do_ty env t in
            env, TyArray t
      | TyApply (v, tl) ->
          let env, tl = Mc_list_util.fold_map do_ty env tl in
          let env = do_tyd env v in
          let ntenv, stenv = env in
            if SymbolTable.mem ntenv v then
              env, TyApply (v, tl)
            else (* SymbolTable.mem stenv v *)
              env, apply_type stenv v tl
      | _ -> raise (IRException "do_ty in inline_tenv say weird type")
  and do_tyd env v =
    let ntenv, stenv = env in
    if SymbolTable.mem ntenv v || SymbolTable.mem stenv v then
      env
    else
      match SymbolTable.find tenv v with
          TyDefUnion (tvl, tbll) ->
            let (ntenv, stenv), tbll = Mc_list_util.fold_map (Mc_list_util.fold_map (fun env (t, b) ->
                let env, t = do_ty env t in
                  env, (t, b)))
              env tbll in
            let tyd = TyDefUnion (tvl, tbll) in
              SymbolTable.add ntenv v tyd, stenv
        | TyDefLambda (tvl, ty) ->
            let env, ty = do_ty env ty in
            let l = List.length tvl in
            let tyd = TyDefLambda (tvl, ty) in
            let c = type_complexity 0 ty - 100000 * l in
              if c < 0 || c > 5 + 3 * l then
                SymbolTable.add ntenv v tyd, stenv
              else
                ntenv, SymbolTable.add stenv v tyd
        | TyDefDTuple _ as tyd ->
                ntenv, SymbolTable.add stenv v tyd in
  SymbolTable.fold (fun env v _ -> do_tyd env v) (SymbolTable.empty, SymbolTable.empty) tenv

let inline_prog
    { prog_types = tenv;
      prog_cont = fini;
      prog_body = e } =
  let tenv, stenv = inline_tenv tenv in
  let e = inline_expr tenv stenv SymbolTable.empty e cont_return in
    { prog_types = tenv;
      prog_cont = fini;
      prog_body = e }

