(* naml_ir_partial.ml
 * $Id: naml_ir_partial.ml,v 1.24 2002/12/23 00:13:20 jyh Exp $
 * Partial application handling for IR
 * 2001-06-17
 *)

open Symbol
open Fir
open Naml_ir
open Naml_ir_exn
open Naml_ir_util

let pa_var venv fs v c =
  if SymbolSet.mem fs v then
    let fty = SymbolTable.find venv v in
    let cs = new_symbol v in
      LetClosure (cs, fty, v, [], c cs)
  else
    c v

let pa_atom venv fs a c =
  match a with
      AtomVar v when SymbolSet.mem fs v ->
	let fty = SymbolTable.find venv v in
	let cs = new_symbol v in
	  LetClosure (cs, fty, v, [], c (AtomVar cs))
    | _ -> c a

let rec pa_atom_list venv fs al c =
  match al with
      [] -> c []
    | a::al ->
	pa_atom venv fs a (fun a -> pa_atom_list venv fs al (fun al -> c (a :: al)))

let rec strip_fun_arg tenv t =
  match t with
      TyFun ([_], r) -> r
    | TyFun (_ :: t, r) -> TyFun (t, r)
    | TyApply (tv, tl) -> strip_fun_arg tenv (apply_type tenv tv tl)
    | _ -> raise (IRException "wierd function in strip_fun_arg")

let rec make_partials tenv v f al t rt e =
  match al with
      [a] -> LetApply (v, rt, f, al, e)
    | a::al ->
	let pf = new_symbol f in
	let t = strip_fun_arg tenv t in
	LetApply (pf, t, f, [a], make_partials tenv v pf al t rt e)
    | _ -> raise (IRException "tried to apply no arguments in make_partials")

let rec pa_expr tenv venv fs e =
   match e with
      LetAtom (v, ty, a, e) ->
         pa_atom venv fs a (fun a ->
               let venv = SymbolTable.add venv v ty in
                  LetAtom (v, ty, a, pa_expr tenv venv fs e))
    | LetUnop (v, ty, op, a, e) ->
         pa_atom venv fs a (fun a ->
               let venv = SymbolTable.add venv v ty in
                  LetUnop (v, ty, op, a, pa_expr tenv venv fs e))
    | LetBinop (v, ty, op, a1, a2, e) ->
         pa_atom venv fs a1 (fun a1 ->
               pa_atom venv fs a2 (fun a2 ->
                     let venv = SymbolTable.add venv v ty in
                        LetBinop (v, ty, op, a1, a2, pa_expr tenv venv fs e)))
    | LetExt (v, ty, s, ty2, al, e) ->
         pa_atom_list venv fs al (fun al ->
               let venv = SymbolTable.add venv v ty in
                  LetExt (v, ty, s, ty2, al, pa_expr tenv venv fs e))
    | TailCall (f, al) ->
         pa_atom_list venv fs al (fun al ->
               TailCall (f, al))
    | Match (a, sel) ->
         pa_atom venv fs a (fun a ->
               Match (a, List.map (fun (s, e) -> s, pa_expr tenv venv fs e) sel))
    | LetAlloc (v, aop, e) ->
         let venv = SymbolTable.add venv v (type_of_alloc_op aop) in
            (match aop with
                AllocTuple (tclass, ty, ty_vars, al) ->
                   pa_atom_list venv fs al (fun al ->
                         LetAlloc (v, AllocTuple (tclass, ty, ty_vars, al), pa_expr tenv venv fs e))
              | AllocDTuple (ty, ty_var, a, al) ->
                   pa_atom venv fs a (fun a ->
                   pa_atom_list venv fs al (fun al ->
                         LetAlloc (v, AllocDTuple (ty, ty_var, a, al), pa_expr tenv venv fs e)))
              | AllocArray (ty, al) ->
                   pa_atom_list venv fs al (fun al ->
                         LetAlloc (v, AllocArray (ty, al), pa_expr tenv venv fs e))
              | AllocVArray (ty, si, a1, a2) ->
                   pa_atom venv fs a1 (fun a1 ->
                         pa_atom venv fs a2 (fun a2 ->
                               LetAlloc (v, AllocVArray (ty, si, a1, a2), pa_expr tenv venv fs e)))
              | AllocUnion (ty, ty_vars, tv, i, al) ->
                   pa_atom_list venv fs al (fun al ->
                         LetAlloc (v, AllocUnion (ty, ty_vars, tv, i, al), pa_expr tenv venv fs e))
              | AllocMalloc (ty, a) ->
                   pa_atom venv fs a (fun a ->
                         LetAlloc (v, AllocMalloc (ty, a), pa_expr tenv venv fs e))
              | AllocFrame _ ->
                   LetAlloc (v, aop, pa_expr tenv venv fs e))
    | LetSubscript (so, v, ty, a, ai, e) ->
         let venv = SymbolTable.add venv v ty in
            LetSubscript (so, v, ty, a, ai, pa_expr tenv venv fs e)
    | SetSubscript (so, a, ai, ty, av, e) ->
         pa_atom venv fs av (fun av ->
               SetSubscript (so, a, ai, ty, av, pa_expr tenv venv fs e))
    | LetFuns (fdl, e) ->
         let venv = List.fold_left (fun venv (v, _, _, t, _, _) -> SymbolTable.add venv v t) venv fdl in
         let fs = List.fold_left (fun fs (v, _, _, _, _, _) -> SymbolSet.add fs v) fs fdl in
            LetFuns (List.map (fun (v, dl, fc, t, vl, e) ->
                           match t with
                              TyFun (tl, _) ->
                                 let venv = List.fold_left2 SymbolTable.add venv vl tl in
                                    v, dl, fc, t, vl, pa_expr tenv venv fs e
                            | _ -> raise (IRException "non function type in pa_expr"))
                     fdl, pa_expr tenv venv fs e)
    | LetApply (v, t, f, al, e) ->
         pa_atom_list venv fs al (fun al ->
               let fty = SymbolTable.find venv f in
                  (match fty with
                      TyFun (atl, rt) when SymbolSet.mem fs f ->
                         let atll = List.length atl in
                         let all = List.length al in
                            if all < atll then
                               LetClosure (v, t, f, al, pa_expr tenv venv fs e)
                            else if all == atll then
		  LetApply (v, t, f, al, pa_expr tenv venv fs e)
		else (* if all > atll *)
		  let ps = new_symbol f in
		  let alh, alt = Mc_list_util.split atll al in
		    LetApply (ps, rt, f, alh, make_partials tenv v ps alt rt t (pa_expr tenv venv fs e))
	    | _ -> make_partials tenv v f al fty t (pa_expr tenv venv fs e)))
    | Return a ->
	pa_atom venv fs a (fun a ->
	  Return a)
    | LetExnHandler (f, e) ->
	pa_var venv fs f (fun f ->
	  LetExnHandler (f, pa_expr tenv venv fs e))
    | Raise a ->
	pa_atom venv fs a (fun a ->
	  Raise a)
    | LetClosure _ -> raise (IRException "letclosure? we haven't made them yet in pa_expr")

let partial_prog prog =
  let { prog_body = e; prog_cont = fini; prog_types = tenv } = prog in
  let fs = SymbolSet.add SymbolSet.empty fini in
  let e = pa_expr tenv SymbolTable.empty fs e in
    { prog with prog_body = e }

