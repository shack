(* $Id: naml_ir_util.ml,v 1.36 2002/12/23 00:13:20 jyh Exp $
 * Utilities for IR, like printing
 * Geoffrey Irving / Dylan Simon
 * 2001-05-27
 * mostly taken from FIR printer
 *)

open Format

open Symbol
open Print_util
open Interval_set
open Fir
open Fir_set
open Fir_print
open Naml_ir
open Naml_ir_exn

(*
 * BUG: until we fix this module.
 *)
let print_symbol = pp_print_symbol Format.std_formatter

(*
 * This printer is broken, so use these utilities.
 *)
let print_atom = pp_print_atom Format.std_formatter
let print_set = pp_print_set  Format.std_formatter

let print_unop = pp_print_unop Format.std_formatter
let print_binop = pp_print_binop Format.std_formatter
let print_ext = pp_print_ext Format.std_formatter
let print_alloc_op = pp_print_alloc_op Format.std_formatter
let print_subscript a b c = pp_print_subscript a b c Format.std_formatter
let print_type = pp_print_type Format.std_formatter
let print_tydef = pp_print_tydef Format.std_formatter

let rec subst_type st t =
  let sub = subst_type st in
  match t with
      TyFun (tl, t) -> TyFun (List.map sub tl, sub t)
    | TyUnion (c, tl, i) -> TyUnion (c, List.map sub tl, i)
    | TyTuple (tclass, tl) -> TyTuple (tclass, List.map (fun (ty, b) -> sub ty, b) tl)
    | TyArray t -> TyArray (sub t)
    | TyVar v -> (try SymbolTable.find st v with Not_found -> t)
    | TyApply (v, tl) -> TyApply (v, List.map sub tl)
    | _ -> t  (* assumes no TyAggr, TyLambda, TyExists, TySubscript, TyProject *)

let apply_type tenv v tl =
  match SymbolTable.find tenv v with
      TyDefLambda (tvl, t) ->
	let substs = List.fold_left2 SymbolTable.add SymbolTable.empty tvl tl in
	  subst_type substs t
    | _ -> raise (IRException "tried to apply a union type")

let type_of_alloc_op ao =
   match ao with
      AllocTuple (_, ty_vars, ty, _)
    | AllocUnion (ty_vars, ty, _, _, _) ->
         TyAll (ty_vars, ty)
    | AllocArray (ty, _)
    | AllocVArray (ty, _, _, _)
    | AllocMalloc (ty, _)
    | AllocDTuple (ty, _, _, _) ->
         ty
    | AllocFrame (v, tyl) ->
         TyFrame (v, tyl)

let rec polymorphic t =
  let pm = polymorphic in
  match t with
     TyFun (tl, t) -> List.exists pm tl || pm t
   | TyUnion (_, tl, _)
   | TyApply (_, tl) -> List.exists pm tl
   | TyTuple (_, tl) -> List.exists (fun (ty, _) -> pm ty) tl
   | TyArray t -> pm t
   | TyVar v -> true
   | _ -> false

(*
 * Default tabstop.
 *)
let tabstop = 2

(*
 * Separated list of fields.
 *)
let print_sep_list sep printer l =
   Format.open_hvbox 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       Format.print_string sep;
                       Format.print_space ()
                    end;
                 Format.open_hvbox tabstop;
                 printer x;
                 Format.close_box ();
                 false) true l);
   Format.close_box ()

(*
 * Separated list of fields.
 *)
let print_pre_sep_list sep printer l =
   Format.open_hvbox 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       Format.print_space ();
                       Format.print_string sep
                    end;
                 Format.open_hvbox tabstop;
                 printer x;
                 Format.close_box ();
                 false) true l);
   Format.close_box ()

(*
 * "Let" opener.
 *)
let print_let_open v ty =
   Format.open_hvbox tabstop;
   Format.print_string "let ";
   print_symbol v;
   Format.print_string " :";
   Format.print_space ();
   pp_print_type Format.std_formatter ty;
   Format.print_string " =";
   Format.print_space ()

let print_let_vars_open vars v ty =
   Format.open_hvbox tabstop;
   Format.print_string "let ";
   print_sep_list "," (fun v ->
         Format.print_string "'";
         print_symbol v) vars;
   if vars <> [] then
      Format.print_string ", ";
   print_symbol v;
   Format.print_string " :";
   Format.print_space ();
   pp_print_type Format.std_formatter ty;
   Format.print_string " =";
   Format.print_space ()

let rec print_let_close e =
   Format.print_string " in";
   Format.close_box ();
   Format.print_space ();
   print_expr e

(*
 * Rename all the vars in the expr.
 *)
and print_expr e =
   match e with
      LetAtom (v, ty, a, e) ->
         print_let_open v ty;
         print_atom a;
         print_let_close e
    | LetUnop (v, ty, op, a, e) ->
         print_let_open v ty;
         print_unop op a;
         print_let_close e
    | LetBinop (v, ty, op, a1, a2, e) ->
         print_let_open v ty;
         print_binop op a1 a2;
         print_let_close e
    | LetExt (v, ty, s, ty2, args, e) ->
         print_let_open v ty;
         print_ext s ty2 [] args;
         print_let_close e
    | TailCall (f, args) ->
         print_symbol f;
         Format.print_string "(";
         print_sep_list "," print_atom args;
         Format.print_string ")"
    | Match (a, cases) ->
         Format.open_hvbox tabstop;
         Format.print_string "match ";
         print_atom a;
         Format.print_string " with";
         List.iter (fun (set, e) ->
               Format.print_space ();
               Format.open_hvbox tabstop;
               Format.print_string "| ";
               print_set set;
               Format.print_string " ->";
               Format.print_space ();
               print_expr e;
               Format.close_box ()) cases;
	 Format.close_box ()
    | LetAlloc (v, op, e) ->
	 Format.open_hvbox tabstop;
	 Format.print_string "let ";
	 print_symbol v;
         Format.printf " =@ ";
         Format.open_hvbox tabstop;
         print_alloc_op op;
         Format.close_box ();
         print_let_close e

    | LetSubscript (so, v, ty, v2, a, e) ->
         print_let_open v ty;
         print_subscript so (AtomVar v2) a;
         print_let_close e

    | SetSubscript (so, v, a1, ty, a2, e) ->
         print_subscript so (AtomVar v) a1;
         Format.print_string " : ";
         pp_print_type Format.std_formatter ty;
         Format.print_string " <- ";
         print_atom a2;
         Format.print_string ";";
         Format.print_space ();
         print_expr e

    | LetFuns (defs, e) ->
	 Format.open_hvbox tabstop;
	 Format.print_string "let ";
         print_sep_list " and" print_function defs;
         print_let_close e

    | LetApply (v, ty, a, al, e) ->
         print_let_open v ty;
	 print_symbol a;
	 Format.print_string "(";
         print_sep_list "," print_atom al;
	 Format.print_string ")";
	 print_let_close e
    | LetClosure (v, ty, a, al, e) ->
         print_let_open v ty;
	 Format.print_string "closure ";
	 print_symbol a;
	 Format.print_string "(";
         print_sep_list "," print_atom al;
	 Format.print_string ")";
	 print_let_close e
    | Return (a) ->
    	Format.open_hvbox tabstop;
	Format.print_string "return ";
	print_atom a;
	Format.close_box ()
    | LetExnHandler (v, e) ->
    	Format.open_hvbox tabstop;
	Format.print_string "try ";
	print_expr e;
	Format.print_string "with ";
	print_symbol v;
	Format.close_box ()
    | Raise (a) ->
    	Format.open_hvbox tabstop;
	Format.print_string "raise ";
	print_atom a;
	Format.close_box ()

and print_gflag = function
    FunGlobalClass ->
      Format.print_string "$global ";
  | FunContClass ->
      Format.print_string "$continuation "
  | FunLocalClass ->
      Format.print_string "$local "

and print_function (f, _, gflag, f_ty, vars, body) =
   Format.print_string "(";
   print_gflag gflag;
   print_symbol f;
   Format.print_string " : ";
   print_type f_ty;
   Format.print_string ")";
   Format.print_string "(";
   print_sep_list "," print_symbol vars;
   Format.print_string ") =";
   Format.print_space ();
   print_expr body

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

let print_prog
    { prog_cont = fini;
      prog_types = types;
      prog_body = expr
    } =

   Format.open_hvbox tabstop;
   Format.print_string "Program cont symbol:";
   Format.print_space ();
   print_symbol fini;
   Format.close_box ();

   Format.print_space ();
   Format.open_hvbox tabstop;
   Format.print_string "Program types:";
   SymbolTable.iter (fun v tyd ->
         Format.print_space ();
         Format.open_hvbox tabstop;
         print_symbol v;
         Format.print_string " =";
         Format.print_space ();
         print_tydef tyd;
         Format.close_box ()) types;
   Format.close_box ();

   Format.print_space ();
   Format.open_hvbox tabstop;
   Format.print_string "Program body:";
   Format.print_newline ();
   print_expr expr;
   Format.close_box ();

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
