(*
 * Naml Intermediate Representation.
 * Geoffrey Irving / Dylan Simon
 *
 *)

open Symbol
open Fir
open Fir_set

(*
 * Function definition.
 *)
type fun_class =
    FunLocalClass
  | FunContClass
  | FunGlobalClass

(*
 * Expressions.
 *)
type exp =
   (* Constructs common to fir *)
   LetAtom of var * ty * atom * exp
 | LetUnop of var * ty * unop * atom * exp
 | LetBinop of var * ty * binop * atom * atom * exp
 | LetExt of var * ty * string * ty * atom list * exp
 | TailCall of var * atom list
 | Match of atom * (set * exp) list
 | LetAlloc of var * alloc_op * exp

 | LetSubscript of subop * var * ty * var * atom * exp
 | SetSubscript of subop * var * atom * ty * atom * exp

    (* Additional constructs *)
 | LetFuns of fundef list * exp
 | LetClosure of var * ty * var * atom list * exp
 | LetApply of var * ty * var * atom list * exp
 | Return of atom
 | LetExnHandler of var * exp
 | Raise of atom

and fundef = var * debug_line * fun_class * ty * var list * exp

(*
 * Program.
 *)
type prog =
   {
     prog_types : tydef SymbolTable.t;
     prog_cont : var;
     prog_body : exp
   }

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
