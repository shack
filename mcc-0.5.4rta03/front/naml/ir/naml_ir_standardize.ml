(* Naml IR standardization
 * Geoffrey Irving
 * $Id: naml_ir_standardize.ml,v 1.12 2002/12/23 00:13:20 jyh Exp $ *)

open Symbol
open Fir
open Naml_ir
open Naml_ir_exn

let rename env s =
  let s' = new_symbol s in
  let env = SymbolTable.add env s s' in
    env, s'

let st_var env v =
  try
    SymbolTable.find env v
  with Not_found ->
    v

let st_atom env = function
    AtomVar v -> AtomVar (st_var env v)
  | a -> a

let st_atoms env = List.map (st_atom env)

let st_aop env aop =
   match aop with
      AllocTuple (tclass, t, tv, al) -> AllocTuple (tclass, t, tv, st_atoms env al)
    | AllocDTuple (ty, ty_var, a, al) -> AllocDTuple (ty, ty_var, st_atom env a, st_atoms env al)
    | AllocArray (t, al) -> AllocArray (t, st_atoms env al)
    | AllocVArray (t, sub_index, a1, a2) -> AllocVArray (t, sub_index, st_atom env a1, st_atom env a2)
    | AllocUnion (tv, tv2, t, i, al) -> AllocUnion (tv, tv2, t, i, st_atoms env al)
    | AllocMalloc (t, a) -> AllocMalloc (t, st_atom env a)
    | AllocFrame _ -> aop

let rec st_expr env e =
   match e with
      LetAtom (v, t, a, e) ->
         let a = st_atom env a in
         let env, v = rename env v in
         let e = st_expr env e in
            LetAtom (v, t, a, e)
    | LetUnop (v, t, op, a, e) ->
         let a = st_atom env a in
         let env, v = rename env v in
         let e = st_expr env e in
            LetUnop (v, t, op, a, e)
    | LetBinop (v, t, op, a1, a2, e) ->
         let a1 = st_atom env a1 in
         let a2 = st_atom env a2 in
         let env, v = rename env v in
         let e = st_expr env e in
            LetBinop (v, t, op, a1, a2, e)
    | LetExt (v, t, s, ty, al, e) ->
         let al = st_atoms env al in
         let env, v = rename env v in
         let e = st_expr env e in
            LetExt (v, t, s, ty, al, e)
    | TailCall (f, al) ->
         let f = st_var env f in
         let al = st_atoms env al in
            TailCall (f, al)
    | Match (a, sel) ->
         let a = st_atom env a in
         let sel = List.map (fun (s, e) -> s, st_expr env e) sel in
            Match (a, sel)
    | LetAlloc (v, aop, e) ->
         let aop = st_aop env aop in
         let env, v = rename env v in
         let e = st_expr env e in
            LetAlloc (v, aop, e)
    | LetSubscript (so, v, t, v2, a, e) ->
         let v2 = st_var env v2 in
         let a = st_atom env a in
         let env, v = rename env v in
         let e = st_expr env e in
            LetSubscript (so, v, t, v2, a, e)
    | SetSubscript (so, v, a1, t, a2, e) ->
         let v = st_var env v in
         let a1 = st_atom env a1 in
         let a2 = st_atom env a2 in
         let e = st_expr env e in
            SetSubscript (so, v, a1, t, a2, e)
    | LetClosure (v, t, f, al, e) ->
         let f = st_var env f in
         let al = st_atoms env al in
         let env, v = rename env v in
         let e = st_expr env e in
            LetClosure (v, t, f, al, e)
    | LetApply (v, t, f, al, e) ->
         let f = st_var env f in
         let al = st_atoms env al in
         let env, v = rename env v in
         let e = st_expr env e in
            LetApply (v, t, f, al, e)
    | Return a ->
         let a = st_atom env a in
            Return a
    | LetExnHandler (f, e) ->
         let f = st_var env f in
         let e = st_expr env e in
            LetExnHandler (f, e)
    | Raise a ->
         let a = st_atom env a in
            Return a
    | LetFuns _ ->
         raise (NotImplemented "LetFuns standardization in IR")

let standardize_body vl e =
  let env, vl = Mc_list_util.fold_map rename SymbolTable.empty vl in
  let e = st_expr env e in
    vl, e
