(* 
 * Naml IR exceptions
 * Geoffrey Irving
 * 12apr01
 *)

exception IRException of string
exception NotImplemented of string
exception Impossible of string
exception Invalid_Extern of string

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
val print_exn : exn -> unit
val print_exn_chan : out_channel -> exn -> unit
val catch : ('a -> 'b) -> 'a -> 'b

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
