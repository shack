(* naml_ir_partial.mli
 * $Id: naml_ir_partial.mli,v 1.3 2001/07/05 10:14:30 dylan Exp $
 * Partial application handling for IR
 * 2001-06-17
 *)

open Symbol
open Fir
open Naml_ir

val partial_prog : prog -> prog
