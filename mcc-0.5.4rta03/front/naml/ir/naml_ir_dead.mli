(*
 * Simple naml ir dead code elimination
 * Geoffrey Irving
 * $Id: naml_ir_dead.mli,v 1.1 2001/08/26 00:42:59 irving Exp $
 *)

open Naml_ir

val dead_prog : prog -> prog
