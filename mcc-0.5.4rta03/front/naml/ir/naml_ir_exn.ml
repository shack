(*
 * Naml IR exceptions
 * Geoffrey Irving
 * 12apr01
 *)

open Format

open Symbol
open Naml_tast
open Naml_tast_util

exception IRException of string
exception NotImplemented = Naml_ast_exn.NotImplemented
exception Impossible =     Naml_ast_exn.Impossible
exception Invalid_Extern = Naml_tast_exn.Invalid_Extern

let print_pos = Naml_ast_exn.print_pos

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
let print_exn exn =
  match exn with
      IRException s ->
        Format.print_string "IRException: ";
        Format.print_string s
    | Fir_pos.FirException _ ->
        Fir_exn_print.pp_print_exn Format.std_formatter exn
    | exn ->
        Naml_tast_exn.print_exn exn

let print_exn_chan out exn =
   Format.set_formatter_out_channel out;
   print_exn exn;
   Format.print_newline ()

let catch f x =
   try f x with
      exn ->
         print_exn_chan stderr exn;
         exit 1
