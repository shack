(* naml_ir_close.mli
 * closure conversion on naml ir
 * $Id: naml_ir_close.ml,v 1.26 2002/12/23 00:13:19 jyh Exp $
 * 18jun01
 *)

open Symbol
open Fir
open Naml_ir
open Naml_ir_exn
open Naml_ir_util
open Naml_ir_misc

(*
 * BUG: until we fix this module.
 *)
let print_symbol = pp_print_symbol Format.std_formatter

module Set = SymbolSet
module Table = SymbolTable

let fv_var bv fv v =
  if Set.mem bv v then fv
  else Set.add fv v

let fv_atom bv fv = function
    AtomVar v -> fv_var bv fv v
  | _ -> fv

let fv_atoms bv fv = List.fold_left (fv_atom bv) fv

let fv_fun ft bv fv f =
  Set.fold (fv_var bv) fv (Table.find ft f)

let rec fv_expr ft bv fv e =
   match e with
      LetAtom (v, _, a, e)
    | LetUnop (v, _, _, a, e) ->
         fv_expr ft (Set.add bv v) (fv_atom bv fv a) e
    | LetBinop (v, _, _, a1, a2, e) ->
         fv_expr ft (Set.add bv v) (fv_atom bv (fv_atom bv fv a1) a2) e
    | LetExt (v, _, _, _, al, e) ->
         fv_expr ft (Set.add bv v) (fv_atoms bv fv al) e
    | TailCall (f, al) ->
         let fv = fv_atoms bv fv al in
            (try
                ft, fv_fun ft bv fv f
             with Not_found ->
                   ft, fv_var bv fv f)
    | Match (a, sel) ->
         List.fold_left (fun (ft, fv) (_, e) -> fv_expr ft bv fv e) (ft, fv_atom bv fv a) sel
    | LetAlloc (v, aop, e) ->
         let fv =
            match aop with
               AllocTuple (_, _, _, al)
             | AllocArray (_, al)
             | AllocUnion (_, _, _, _, al) ->
                  fv_atoms bv fv al
             | AllocDTuple (_, _, a, al) ->
                  fv_atoms bv fv (a :: al)
             | AllocVArray (_, _, a1, a2) ->
                  fv_atoms bv fv [a1; a2]
             | AllocMalloc (_, a) ->
                  fv_atom bv fv a
             | AllocFrame _ ->
                  fv
         in
            fv_expr ft (Set.add bv v) fv e
    | LetSubscript (_, v, _, v2, a, e) ->
         fv_expr ft (Set.add bv v) (fv_atom bv (fv_var bv fv v2) a) e
    | SetSubscript (_, v, a1, _, a2, e) ->
         fv_expr ft bv (fv_atom bv (fv_atom bv (fv_var bv fv v) a1) a2) e
    | LetFuns (fdl, e) ->
         fv_expr (fv_funs ft fdl) bv fv e
    | LetClosure (v, _, f, al, e) ->
         fv_expr ft (Set.add bv v) (fv_atoms bv (fv_fun ft bv fv f) al) e
    | LetExnHandler _ | Raise _ | LetApply _ | Return _ ->
         raise (IRException "closure conversion say, 'LetApply, Return and exceptions bad'")

and fv_funs ft fdl =
   let ft, fvl =
      Mc_list_util.fold_map (**)
         (fun ft (f, _, _, _, _, _) ->
               try
                  ft, Table.find ft f
               with Not_found ->
                     Table.add ft f Set.empty, Set.empty)
         ft fdl in
      List.fold_left2 (fun ft fv (f, _, _, _, vl, e) ->
            let vs = Set.of_list (List.sort Symbol.compare vl) in (* XXX - of_list does not sort for you! *)
            let ft, fv = fv_expr ft vs fv e in
               Table.add ft f fv)
      ft fvl fdl

let rec fv_top ft e =
   match e with
      LetAtom (_, _, _, e)
    | LetUnop (_, _, _, _, e)
    | LetBinop (_, _, _, _, _, e)
    | LetExt (_, _, _, _, _, e)
    | LetAlloc (_, _, e)
    | LetSubscript (_, _, _, _, _, e)
    | SetSubscript (_, _, _, _, _, e)
    | LetClosure (_, _, _, _, e) ->
         fv_top ft e
    | TailCall _ -> ft
    | Match (a, sel) -> List.fold_left (fun ft (_, e) -> fv_top ft e) ft sel
    | LetFuns (fdl, e) -> fv_top (fv_funs ft fdl) e
    | LetExnHandler _ | Raise _ | LetApply _ | Return _ ->
         raise (IRException "closure conversion say, 'LetApply, Return and exceptions bad'")

let measure = Table.fold (fun n _ s -> n + Set.cardinal s) 0

let solve_fv ft e =
  let rec iterate n ft =
    let ft = fv_top ft e in
    let m = measure ft in
      if m > n then
        iterate m ft
      else
        ft in
  let ft = iterate (measure ft) ft in
    Table.map (Set.fold (fun l v -> if Table.mem ft v then l else v :: l) []) ft

let print_ft ft =
  Format.print_string "Free variable list:"; Format.print_newline ();
  Table.iter (fun f v ->
    Format.print_string "  ";
    print_symbol f;
    Format.print_string ": ";
    List.iter (fun v -> print_symbol v; Format.print_string ", ") v;
    Format.print_newline ()) ft

let lookup_var venv v =
   try
      Table.find venv v
   with
      Not_found ->
         let str = Printf.sprintf "Unbound variable `%s'" (to_string v) in
         raise (IRException str)

let rec close_expr ft venv e =
   match e with
      LetAtom (v, t, a, e) ->
         LetAtom (v, t, a, close_expr ft (Table.add venv v t) e)
    | LetUnop (v, t, op, a, e) ->
         LetUnop (v, t, op, a, close_expr ft (Table.add venv v t) e)
    | LetBinop (v, t, op, a1, a2, e) ->
         LetBinop (v, t, op, a1, a2, close_expr ft (Table.add venv v t) e)
    | LetExt (v, t, s, ty, al, e) ->
         LetExt (v, t, s, ty, al, close_expr ft (Table.add venv v t) e)
    | TailCall (f, al) as e' ->
         (try
             TailCall (f, List.map (fun v -> AtomVar v) (Table.find ft f) @ al)
          with Not_found -> (* must be a variable *)
                e')
    | Match (a, sel) ->
         Match (a, List.map (fun (s, e) -> s, close_expr ft venv e) sel)
    | LetAlloc (v, aop, e) ->
         LetAlloc (v, aop, close_expr ft (Table.add venv v (type_of_alloc_op aop)) e)
    | LetSubscript (so, v, t, a, ai, e) ->
         LetSubscript (so, v, t, a, ai, close_expr ft (Table.add venv v t) e)
    | SetSubscript (so, a, av, t, v, e) ->
         SetSubscript (so, a, av, t, v, close_expr ft venv e)
    | LetFuns (fdl, e) as e' ->
         LetFuns (List.map (fun (f, dl, c, t, vl, e) ->
                        let fv = Table.find ft f in
                        let ftl = List.map (lookup_var venv) fv in
                        let atl, t = match t with
                                        TyFun (tl, rt) -> tl, TyFun (ftl @ tl, rt)
                                      | _ -> raise (IRException "non-TyFun in function definition in close_expr") in
                        let venv = List.fold_left2 Table.add venv fv ftl in
                        let venv =
                           try List.fold_left2 Table.add venv vl atl with
                              Invalid_argument _ ->
                                 raise (IRException ("Arity mismatch for " ^ (string_of_symbol f)))
                        in
                           f, dl, c, t, fv @ vl, close_expr ft venv e)
                  fdl, close_expr ft venv e)
    | LetClosure (v, t, f, al, e) ->
         LetClosure (v, t, f, List.map (fun v -> AtomVar v) (Table.find ft f) @ al,
                                                                                close_expr ft (Table.add venv v t) e)
    | LetExnHandler _ | Raise _ | LetApply _ | Return _ ->
         raise (IRException "closure conversion say, 'LetApply, Return and exceptions bad'")

let close_prog prog =
  let e = prog.prog_body in
  let fini = prog.prog_cont in
  let venv = Table.add Table.empty fini Naml_ir_misc.type_fini_aux in
(*  let ft = Table.add Table.empty fini Set.empty in *)
  let ft = solve_fv Table.empty e in
  (* print_ft ft; *)
  let e = close_expr ft venv e in
    { prog with prog_types = Table.add prog.prog_types fini_symbol (TyDefLambda ([], type_fini));
                prog_body = e }
