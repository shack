(* naml_ir_cps.mli
 * cps conversion on naml ir
 * $Id: naml_ir_cps.mli,v 1.2 2001/06/18 22:54:55 irving Exp $
 * 2001-06-18
 *)

open Naml_ir

val cps_prog : prog -> prog
