(* Naml IR externals definitions
 * $Id: naml_ir_ext.mli,v 1.2 2001/07/30 19:01:29 irving Exp $
 * 2001-06-23
 *)

open Symbol
open Fir
open Naml_ir

val make_extern_def : symbol -> debug_line -> ty -> string -> exp -> exp
