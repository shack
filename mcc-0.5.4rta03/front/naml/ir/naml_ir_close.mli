(* naml_ir_close.mli
 * closure conversion on naml ir
 * $Id: naml_ir_close.mli,v 1.1 2001/06/18 19:10:05 irving Exp $
 * 18jun01
 *)

open Naml_ir

val close_prog : prog -> prog

