#!/usr/bin/perl
# utility to convert a string representing an ocaml type to a string representing the Tast type construct
# currently does only very very simple types, and makes a lot of assumptions

use strict;

my %typesub = (
	'int' => 'TyInt',
	'char' => 'TyChar',
	'float' => 'TyFloat',
	'exn' => 'TyExn',
	'string' => 'TyArray TyChar',
	'bool' => 'bool_ty',
	'unit' => 'unit_ty'
);

sub do_base ($)
{
	local $_ = shift;
	if (/^(.*)array$/)
	{
		return "TyArray " . do_base($1)
	}
	elsif (exists $typesub{$_})
	{
		return $typesub{$_}
	}
	else
	{
		return $_
	}
}
		
sub do_tuple ($)
{
	local $_ = shift;
	my @t = split /\*/;
	if (@t > 1)
	{
		return 'TyTuple [' . join('; ', map { do_base $_ } @t) . ']';
	}
	else
	{
		return do_base @t[0];
	}
}

sub do_fun ($)
{
	local $_ = shift;
	my @t = split /->/;
	if (@t > 1)
	{
		my $ret = pop @t;
		my @args = map { do_tuple $_ } @t;
		return 'TyFun ([' . join('; ', @args) . '], ' . do_tuple($ret) . ')';
	}
	else
	{
		return do_tuple @t[0];
	}
}

sub make_type ($)
{
	local $_ = shift;
	s/\s//g; # it just gets in the way, and we don't support constructed types
	$_ = do_fun($_);
	my $a = 'a';
	while (/'$a/)
	{
		s/'$a/$a/g;
		$_ = "let $a = TyVar (new_symbol_string \"$a\") in $_";
		$a++;
	}
	return $_;
}

if (@ARGV)
{
	print make_type("@ARGV"), "\n";
	exit;
}

while (<>)
{
	chomp;
	print make_type($_), "\n";
}
