\documentclass[12pt]{article}

%\oddsidemargin=-0.25in
%\textwidth=7in
%\topmargin=-0.25in
%\headheight=0.1in
%\headsep=0in
%\textheight=9in
%\footskip=0.5in
%\pagestyle{plain}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}

\newcommand\naml{\textsf{\raisebox{0.0ex}{n}\kern-.25ex a}\kern-.3ex M\kern -.25ex L}
\newcommand\lang[1]{\textsf{#1}}
\newcommand\stage[1]{\texttt{#1}}
\newcommand\mc{\lang{mc}}
\newcommand\fc{\lang{fc}}
\newcommand\AST{\stage{AST}}
\newcommand\TAST{\stage{TAST}}
\newcommand\IR{\stage{IR}}
\newcommand\FIR{\stage{FIR}}
\newcommand\ext[1]{\texttt{.#1}}

\author{Geoffrey Irving \and Dylan Simon}
\title{CS134c Project: \naml{}}
\date{June 15, 2001}

\begin{document}

\maketitle

\section{Introduction}

The \naml{} project was originally envisioned as an independent compiler, based in some part on the work of \fc{} in CS 134b, yet still a free-standing compiler for a semi-original language.
However, it was quickly merged into the \mc{} project, which combines multiple languages and architectures into a single compiler.
Now, \naml{} is a front-end for \mc{}, which, like other front-ends such as \fc{}, takes source code and produces a functional intermediate representation, \FIR{}.
From there, \mc{} optimizes the \FIR{}, which is then sent to a back-end that produces machine code for an appropriate architecture, such as x86 or alpha.
The majority of the \mc{} project is written in OCaml.
However, it has been one of our design goals to be able to eventually compile at least the \naml{} portions of \mc{} with \naml{} itself---to bootstrap our compiler.

The \naml{} front-end parses the source code, producing an abstract syntax representation, \AST{}, performs type inference to produce \TAST{}, 
does standard CPS-style conversion to \IR{}, and finally finishes CPS, handling partial application and closure conversion to produce \FIR{}.
These stages will therefore be the focus of this paper.
First, however, we will describe the nature of the language itself.

\naml{} was inspired heavily by
Objective Caml\footnote{The Objective Caml system is copyright \copyright{} Institut National de Recherche en Informatique et en Automatique (INRIA).},
and indeed is mostly compatible with it, in as much as is implemented.
Objective Caml, in turn, is based on Standard ML, or `Meta Language', designed to do mathematical theorem proving.
Based on the idea that ML had evolved into its own, full-fledged language, good for more than just dealing with other languages, \naml{} stands for `not a Meta Language.'
It implements most of the Caml language, neglecting (by intent) only argument labels and polymorphic variants, as well as the Objective portions of OCaml.
Unlike ML, but like OCaml, it is designed as a compiled language, with hopes that, through efforts on \mc{}, it will eventually be faster than OCaml.
It implements a few features that OCaml does not, with more planned, as will be described in the next section.

\section{The Language}

% lanugage features: A x (x a tuple), plans for meta-language features
% some on parsing, with specfic implementation decisions (lists to :: early on, static short-circuiting &&, etc)
% external types and external's types (safe, unlike ocaml, val's in .mli)

\naml{} syntax and semantics differ from OCaml 3.01 standard in a few minor ways.
First, the bootstrapping syntax used in \verb+Pervasives+ is more explicit, with constructs such as \verb+external type+ to declare \verb+int+, \verb+'a list+, etc.
Special exceptions such as \verb+Match_failure+ will be handled similarly once implemented.
\naml{} also performs full type checking on inlined external constructs such as \verb+%intadd+ to insure that the external calls remain safe.
Since \naml{} will compile \ext{ml} and \ext{mli} files into a single \ext{fir} file, none of the \verb+external+ constructs are needed in \ext{mli} files, and normal \verb+val+ declarations are used instead.

Operators work mostly the same as in OCaml, with their static precedences.
While it would be nice to let them have dynamic, definable precedences as in SML, it would just be unmanageable.
Due to the rather confusing semantics of the short-circuiting \verb+&&+ and \verb+||+ operators, we decided that rather than letting them be operators with `magic' semantics until they are redefined as in OCaml, we would turn them into language constructs, giving them static meanings.

\naml{} includes all of the pattern matching capability of OCaml 3.01, including variable binding inside alternatives, as in \verb+A x | B x+.
It also supports the application of multi-argument constructors to single tuples, both in matching and expressions, as in \verb+A x+ where \verb+A+ is a multi-argument constructor and \verb+x+ will be a tuple.
\naml{} contains no built-in string type: type \verb+string+ is just an alias for \verb+char array+, and a string constant is semantically identical to an OCaml array allocation.
This removes the confusing static behavior of string constants within functions, and optimization should easily cancel the performance loss in normal cases.
Although it is not yet implemented, \naml{} will also support a general for loop construct which is intuitively similar to the C \verb+for+ loop.
For more on the planned extensions to the language, see section \ref{plans}.

Currently, \naml{}, and indeed all of \mc{}, does not support multiple file compilation, i.e. linking.
Eventually, this will be supported in the standard OCaml style, except where noted above.
Each \ext{ml} file will compose a module by the same name, with an \ext{mli} specifying the signature for the module.
However, for the most part, the \ext{mli} file will be ignored, aside from insuring the signature matches.
In general, signatures will not be implemented so there will be no data hiding.
The compiler will produce a \ext{fir} file from these which will summarize the functions, types, and exceptions in the file, and can then be used to build another dependent \ext{fir} file.
These \ext{fir} files can then be used to create a executable.
Exactly how this will work, specifically with respect to symbol name preservation, is not yet determined.
At the moment, to alleviate this problem, the standard \verb+Pervasives+ module is simply perpended to each file compiled, so that the standard functions and types are available.

Lexing and parsing is done based on these rules and as described by OCaml documentation (note that we have no parsing conflicts).
Some expansions are done at this phase, such as list expansion (\verb+[x]+ becomes \verb+:: x []+) and static short-circuiting operator conversions.
After this, we do standardization which simply renames variables so that they are all unique.
From here, type inference can proceed smoothly, which continues expansions such as converting all conditionals and non-recursive assignments to matches.

\section{Type Inference}

\naml{} uses standard implicitly polymorphic type inference, similar to that described in Cardelli's paper, using the concepts of generic and non-generic type variables \cite{type}.
Generic type variables are those quantified with $\forall$ such as $\alpha$ in
	\verb+fun x -> x+ : $\forall \alpha . \alpha \to \alpha$.
Non-generic type variables are those that may be further limited by assignment through unification in later stages.
Mutable type variables, those which cannot be generic due to their relation to dangerous mutable data, such as $\alpha$ in
	\verb+ref []+ : $\alpha$ \verb+list+ \verb+ref+,
are handled similarly to non-generic variables.
Specifically, if a mutable type variable is unified to another type, all variables within that type become mutable, just as all variables in a type unified with a non-generic variable become non-generic.  
Even though we have generic type variables, however, they are not explicitly quantified in the program.
Rather, after type inference, any unbound type variable is considered generic.

The basis for type inference is unification, which takes two types and coalesces them.
This is done by recursively unifying components of the two types until a type variable is reached.
At this point, when unifying a type variable $\alpha$ with a type $t$, the following steps occur: \begin{enumerate}
	\item	Any type variable may be trivially unified with itself ($t = \alpha$).
	\item	A type variable may not be unified with another type which contains it ($\alpha \notin t$).
	\item	Set $\alpha = t$.
	\item	If $\alpha$ is non-generic, all type variables in $t$ become non-generic.
\end{enumerate}
Using this unification algorithm, type inference simply proceeds by storing variable types and unifying as the expressions require it.
By the end of this process, every expression has been assigned an explicit type using type variables, and is stored as \TAST{}, which is just like \AST{} but with types everywhere.
If there are any non-generic (mutable) type variables left without values, they are given arbitrary assignments (for example, in \verb+ref []+ : $\alpha$ \verb+list+ \verb+ref+, if $\alpha$ was never unified, it was effectively never used, so its type does not matter).
Then, all remaining non-generic type variables in the entire program are expanded, and type inference is complete.

\section{FIR Conversion}

\FIR{} conversion is made up of the following stages: \begin{enumerate}
	\item \label{tast_ir} \TAST{} to \IR{} conversion
	\item \label{opt} Minor optimizations
	\item \label{partial} Partial application handling
	\item \label{cps} CPS conversion
	\item \label{closure} Closure conversion
	\item \label{explicit} Explicit polymorphism conversion
\end{enumerate}
Step \ref{tast_ir} is relatively straightforward continuation passing code.  Since all errors have
been caught in parsing, standardization, and type inference, no error checking is required.
The only interesting section of the code is pattern matching, which takes about 200 lines.
The algorithm is taken from Sestoft's paper \cite{match}.  Only binary matches are \FIR{}, and the task of
merging these is left for later optimizations.  To remember and use the information from previous
tests, a knowledge table is carried around mapping atoms to sets of possible tags.

At present, the first optimization phase in the compiler performs only basic inlining.  There
are two major advantages of doing this here in step \ref{opt}.  First, \IR{} conversion generates a function for
each operator declared using \verb+external+.  If these were not inlined, CPS would create continuation
functions for every basic operator use, creating significant code blowup and slowing the compiler.
Second, it is convenient to inline simple global constants and such before closure conversion
occurs, since after that point such inlining must cross function boundaries.  In the future, dead
code analysis may be performed as well.  In general, the main benefit of this inlining phase is 
faster compilation, not faster compiled code.

Due to the heavy use of partial application in a continuation passing style compiler, it is vital
that the optimizer be able to handle all aspects of its implementation.  Therefore, all partial
application is converted to tuples and raw function pointer arguments before \FIR{} in step \ref{partial}.  For example,
a function taking an argument of type $t \to t$ would instead take an argument of type
$\alpha \times (\alpha \to t \to t)$, where the $\alpha$ represents previously applied arguments.
If multiple arguments are required, for example $t \to t \to t$, it will be handled in one of
two ways.  If possible, the compiler will use $\alpha \times (\alpha \to t \to t \to t)$, which works
if and only if both arguments are always supplied at the same time.  If this restriction fails,
the type $\alpha \times (\alpha \to t \to (\beta \times (\beta \to t \to t)))$ will be used, and two
function applications will be required.  Since this implementation of this is absolutely
critical to performance, a lot of effort will be spent on it in the optimization phase.

The final three phases are straightforward.  Steps \ref{cps} and \ref{closure} are essentially identical
to those used in \fc{}, except that both will require knowledge of the partial application
handling for correctness.  Step \ref{explicit} will require simplified unification
to determine which type variables are substituted for which type.  Once these final stages
are completed, the resulting \FIR{} will be ready for optimization and the back-end.

\section{Plans}\label{plans}

As we continue to write in ML, we continue to discover more improvements we wish the language had (an advantage of writing a compiler in its own language), as well as a continual stream of possible optimizing improvements.
A major one of the latter is unboxed arrays.
Currently, all arrays are stored as arrays of boxed values, either pointers, tags, or integers.
In many cases (most specifically with strings), it would be advantageous, both from an efficiency and an integration standpoint, to be able have unboxed arrays, as in C where the records are stored sequentially in-place with a single tag for the whole array.
There are complications to this: extracting a value from the array would require boxing it, which would require allocation.
OCaml provides a partial solution to this by providing special string types which are unboxed, however we would like a more general solution.

The subject of array storage also brings up the concept of array matches.
We've found that array matches are awkward, and often rather useless.
A random idea we've had is to implement some general form of regular expressions (or some sort of compiled automata) that could be used with array matches, particularly with strings.
Whether this is really appropriate for part of the language syntax is a different subject, though it would certainly provide the convenience of many higher level scripting languages.

However, by far, the most annoying and repetitive ML task we've found is having to write folding and mapping constructs on constructed types.
Pages of code that, for the most part does nothing, such as code for going through an entire \TAST{} program and performing some mapping or collection function on all the type variables, points out an obvious opportunity for improvement.
Our first idea (which is actually mostly done) was to write a program that would just generate the identity code for a type definition (i.e., match each possible constructed type and return it unchanged) that could then be modified to perform the desired operation.
However, what would be much nicer is some way to provide generalized folding and mapping constructs inside the language itself\footnote{Ironically, implementing this would be adding meta functionality to \naml{}.}.
It is not difficult for the compiler to generate this code, as it already knows all about the types being operated upon.

Another goal is an extension of \naml{} to an explicitly parallel language along the lines of MIT's
Cilk language \cite{cilk}.  Initially, the goal would be a language for shared-memory parallelism,
with constructs similar to Cilk's spawn, inlet, abort, etc.  However, significant changes will be
required in order to fit these nicely into a stackless, functional compiler.  Once these concepts
are implemented, we hope to be able to use the tighter memory semantics of \naml{} in order to
move to distributed memory systems.  Unlike the work on mobile code and ambients, the only goal
of this project is raw speed, and therefore the two efforts should complement each other nicely.

\begin{thebibliography}{9}
\bibitem{type}
	Luca Cardelli.
	\textit{Basic Polymorphic Typechecking}.
	Sci. of Comp. Prog., April 1987.
\bibitem{match}
	Peter Sestoft.
	\textit{ML pattern match compilation and partial evaluation}.
	Dagstuhl Sem. on Part. Eval., March 5, 1996.
\bibitem{cilk}
	M. Frigo, C.E. Leiserson, and K.H. Randall. 
        \textit{The implementation of the Cilk-5 multithreaded language}. 
        ACM SIGPLAN Notices, 33(5):212-223, May 1998.
\end{thebibliography}

\end{document}
