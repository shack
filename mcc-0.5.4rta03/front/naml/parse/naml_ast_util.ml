(*
vim:sts=4:sw=4
 * Utilities over the syntax.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *
 * Modified for naml by Dylan Simon
 *)
open Format

open Naml_ast
open Symbol
open Print_util

(*
 * BUG: until we fix this module.
 *)
let print_symbol = pp_print_symbol Format.std_formatter

(************************************************************************
 * POSITIONS
 ************************************************************************)

(*
 * Combine two positions.
 *)
let union_pos
    (file1, sline1, schar1, eline1, echar1)
    (file2, sline2, schar2, eline2, echar2) =
   if file1 <> file2 then
      raise (Invalid_argument (**)
                (Printf.sprintf "union_pos: file mistmatch: \"%s\":\"%s\"" (**)
                    (String.escaped file1) (String.escaped file2)));
   let sline, schar =
      if sline1 < sline2 then
         sline1, schar1
      else if sline1 > sline2 then
         sline2, schar2
      else
         sline1, min schar1 schar2
   in
   let eline, echar =
      if eline1 > eline2 then
         eline1, echar1
      else if eline1 < eline2 then
         eline2, echar2
      else
         eline1, max echar1 echar2
   in
      file1, sline, schar, eline, echar

(*
 * Get the position info.
 *)

let rec list_last = function
      [] -> raise (Invalid_argument "list_last")
    | [a] -> a
    | _::t -> list_last t

let pos_of_mpath mpath =
    union_pos (snd (List.hd mpath)) (snd (list_last mpath))

let pos_of_empath = function
    VarEmpath (_,p)
  | ProjEmpath (_,_,p)
  | ApplyEmpath (_,_,p)	-> p

let pos_of_type = function
    TypeFun (_,_,p)
  | TypeTuple (_,p)
  | TypeId (_,_,p)
  | TypeAny p
  | TypeVar (_,p) -> p

let pos_of_modtype = function
    VarModtype (_,p)
  | SigModtype (_,p)
  | FunctorModtype (_,_,_,p)
  | ConstraintTypeModtype (_,_,_,_,p)
  | ConstraintModModtype (_,_,_,p)	-> p

let pos_of_modexp = function
      VarMod (_,p)
    | StructMod (_,p)
    | FunctorMod (_,_,_,p)
    | ApplyMod (_,_,p)
    | ConstrainMod (_,_,p)  -> p

let pos_of_tyrep = function
      TypeCons (_,p)
    | TypeRecord (_,p) -> p

let pos_of_expr = function
      VarExpr (_,p)
    | ConstExpr (_,p)
    | TySpecExpr (_,_,p)
    | TupleExpr (_,p)
    | ConsExpr (_,_,p)
    | ArrayExpr (_,p)
    | RecordExpr (_,_,p)
    | ApplyExpr (_,_,p)
    | ProjExpr (_,_,p)
    | AssignProjExpr (_,_,_,p)
    | ArrayEltExpr (_,_,p)
    | AssignArrayEltExpr (_,_,_,p)
    | IfExpr (_,_,_,p)
    | WhileExpr (_,_,p)
    | ForExpr (_,_,_,_,_,p)
    | SeqExpr (_,_,p)
    | MatchExpr (_,_,p)
    | FunExpr (_,_,p)
    | TryExpr (_,_,p)
    | GroupExpr (_,p)
    | LetExpr (_,_,_,p)            -> p

let pos_of_pattern = function
      IdPat (_,p)
    | AnyPat (p)
    | ConstPat (_,p)
    | RangePat (_,_,p)
    | AsPat (_,_,p)
    | TySpecPat (_,_,p)
    | AltPat (_,_,p)
    | ConsPat (_,_,p)
    | TuplePat (_,p)
    | RecordPat (_,p)
    | ArrayPat (_,p)		   -> p

let pos_of_const = function
      IntConst (_,p)
    | FloatConst (_,p)
    | CharConst (_,p)
    | CConsConst (_,p) -> p

(************************************************************************
 * PRINTING                                                             *
 ************************************************************************)

let print_meta s =
    Format.printf "'";
    print_symbol s

let print_mpath m =
    print_sep_list_no_space "." print_fst_symbol m

let rec print_empath = function
    VarEmpath (s, _) ->
      print_symbol s
  | ProjEmpath (m1, m2, _) ->
      print_empath m1;
      Format.print_string ".";
      print_empath m2
  | ApplyEmpath (m1, m2, _) ->
      print_empath m1;
      Format.print_string "(";
      print_empath m2;
      Format.print_string ")"

let print_vpath = function
    (m, s, _) ->
	print_mpath m;
	(if m != [] then Format.print_string ".");
	print_symbol s

let rec print_type = function
      TypeFun (t1, t2, _) ->
	print_type t1;
	Format.print_string " ->";
	Format.print_space ();
	print_type t2
    | TypeTuple (tl, _) ->
	Format.print_string "(";
	print_sep_list " *" print_type tl;
	Format.print_string ")"

    | TypeAny _ -> Format.print_string "_"
    | TypeVar (s, _) ->
	print_meta s
    | TypeId (tl, v, _) ->
	(match tl with
	      [] -> ()
	    | [t] ->
		print_type t;
		Format.print_space ()
	    | _ ->
		Format.print_string "(";
		print_sep_list "," print_type tl;
		Format.print_string ")";
		Format.print_space ());
	print_vpath v

let print_tyrep = function
      TypeCons (cl, _) ->
	let print_typecons = function
	      (s, []) ->
		print_symbol s
	    | (s, t) ->
		print_symbol s;
		Format.print_string " of ";
		print_sep_list "*" print_type t
	in print_sep_list_box " |" print_typecons cl
    | TypeRecord (rl, _) ->
	let print_typerecord (mut, s, t) =
	    if mut then Format.print_string "mutable ";
	    print_symbol s;
	    Format.print_string " :";
	    Format.print_space ();
	    print_type t
	in
	Format.print_string "{ ";
	print_sep_list ";" print_typerecord rl;
	Format.print_string " }"

let print_type_params = function
    [] -> ()
  | [x] ->
      print_meta x;
      Format.print_string " "
  | al ->
      Format.print_string "(";
      print_sep_list "," print_meta al;
      Format.print_string ") "

let print_tydef (al, s, t, r, _) =
    print_type_params al;
    print_symbol s;
    (match t with
	  None -> ()
	| Some t ->
	    Format.print_string " =";
	    Format.print_space ();
	    print_type t);
    (match r with
	  None -> ()
	| Some r ->
	    Format.print_string " =";
	    Format.print_space ();
	    print_tyrep r)

let print_exndef = function
      ExnVar (e, v, _) ->
	print_symbol e;
	Format.print_string " =";
	Format.print_space ();
	print_vpath v
    | ExnCons (e, t, _) ->
	print_symbol e;
	(match t with
	      [] -> ()
	    | _ ->
		Format.print_string " of ";
		Format.print_space ();
		print_sep_list "*" print_type t)

let print_constexpr = function
      IntConst (v, _) ->
	Format.printf "%d" v
    | FloatConst (v, _) ->
	Format.printf "%g" v
    | CharConst (v, _) ->
	Format.printf "'%s'" (Char.escaped v)
    | CConsConst (v, _) ->
	print_vpath v

let rec print_expr = function
      GroupExpr (e, _) ->
	Format.print_string "(";
	print_expr e;
	Format.print_string ")"
    | VarExpr (v, _) ->
	print_vpath v
    | ConstExpr (c, _) ->
	print_constexpr c
    | TySpecExpr (e, t, _) ->
	Format.print_string "(";
	print_expr e;
	Format.print_string " :";
	Format.print_space ();
	print_type t;
	Format.print_string ")"
    | TupleExpr (el, _) ->
	Format.print_string "(";
	print_sep_list "," print_expr el;
	Format.print_string ")"
    | ConsExpr (v, e, _) ->
	Format.print_string "(";
	print_vpath v;
	Format.print_space ();
	print_expr e;
	Format.print_string ")"
    | ArrayExpr (el, _) ->
	Format.print_string "[|";
	print_sep_list ";" print_expr el;
	Format.print_string "|]"
    | RecordExpr (w,rl, _) ->
	let print_record_elt (v, e) =
	    print_vpath v;
	    Format.print_string " =";
	    Format.print_space ();
	    print_expr e
	in
	Format.print_string "{ ";
        (match w with
            None -> ()
          | Some w -> print_expr w; Format.print_string " with ");
	print_sep_list ";" print_record_elt rl;
	Format.print_string " }"
    | ApplyExpr (f, al, _) ->
	Format.print_string "(";
	print_expr f;
	Format.print_space ();
        print_sep_list "" print_expr al;
	Format.print_string ")"
    | ProjExpr (e, v, _) ->
	print_expr e;
	Format.print_string ".";
	print_vpath v
    | AssignProjExpr (e, v, e2, _) ->
	Format.print_string "(";
	print_expr e;
	Format.print_string ".";
	print_vpath v;
	Format.print_string " <-";
	Format.print_space ();
	print_expr e2;
	Format.print_string ")"
    | ArrayEltExpr (e, elt, _) ->
	print_expr e;
	Format.print_string ".(";
	print_expr elt;
	Format.print_string ")"
    | AssignArrayEltExpr (e, elt, e2, _) ->
	Format.print_string "(";
	print_expr e;
	Format.print_string ".(";
	print_expr elt;
	Format.print_string ") <-";
	Format.print_space ();
	print_expr e2;
	Format.print_string ")"
    | IfExpr (i, t, e, _) ->
	Format.print_cut ();
	Format.open_box tabstop;
	Format.print_string "if ";
	print_expr i;
	Format.print_space ();
	Format.open_box tabstop;
	Format.print_string "then ";
	print_expr t;
	(match e with
	      None -> ()
	    | Some e ->
		Format.print_space ();
		Format.print_string "else ";
		print_expr e);
	Format.close_box ();
	Format.close_box ()
    | WhileExpr (w, d, _) ->
	Format.print_cut ();
	Format.open_box tabstop;
	Format.print_string "while ";
	print_expr w;
	Format.print_string " do";
	Format.print_space ();
	Format.open_box tabstop;
	print_expr d;
	Format.close_box ();
	Format.print_space ();
	Format.print_string "done";
	Format.close_box ()
    | ForExpr (s, f, dir, t, d, _) ->
	Format.print_cut ();
	Format.open_box tabstop;
	Format.print_string "for ";
	print_symbol s;
	Format.print_string " =";
	Format.print_space ();
	print_expr f;
	Format.print_string (if dir then " to " else " downto ");
	print_expr t;
	Format.print_string " do";
	Format.print_space ();
	Format.open_box tabstop;
	print_expr d;
	Format.close_box ();
	Format.print_space ();
	Format.print_string "done";
	Format.close_box ()
    | SeqExpr (e1, e2, _) ->
	Format.open_box tabstop;
	Format.print_string "(";
	print_expr e1;
	Format.print_string "; ";
	print_expr e2;
	Format.print_string ")";
	Format.close_box ()
    | MatchExpr (e, pm, _) ->
	Format.open_box tabstop;
	Format.print_string "match ";
	print_expr e;
	Format.print_string " with";
	Format.print_space ();
	print_pmatch_list pm;
	Format.close_box ()
    | FunExpr (al, e, _) ->
	Format.open_box tabstop;
	Format.printf "fun ";
        print_sep_list "" print_symbol al;
        Format.printf "->@ ";
        print_expr e;
	Format.close_box ()
    | TryExpr (e, pm, _) ->
	Format.open_box tabstop;
	Format.print_string "try ";
	print_expr e;
	Format.print_string " with";
	Format.print_space ();
	print_pmatch_list pm;
	Format.close_box ()
    | LetExpr (r, bl, e, _) ->
	Format.open_box tabstop;
	print_let r bl;
	Format.print_space ();
	Format.print_string "in";
	Format.print_space ();
	print_expr e;
	Format.close_box ()

and print_let r bl =
    let print_binding (p, e, _) =
	Format.open_box tabstop;
	print_pattern p;
	Format.print_string " =";
	Format.print_space ();
	print_expr e;
	Format.close_box ()
    in
    Format.open_box tabstop;
    Format.print_string "let";
    (if r then Format.print_string " rec");
    Format.print_space ();
    print_sep_list_box " and" print_binding bl;
    Format.close_box ()

and print_pmatch_list pm =
    let print_pmatch (p, w, e) =
	Format.open_box tabstop;
	print_pattern p;
	(match w with
	      None -> ()
	    | Some w ->
		Format.print_space ();
		Format.print_string "when ";
		print_expr w);
	Format.print_string " ->";
	Format.print_space ();
	print_expr e;
	Format.close_box ()
    in
    Format.open_box tabstop;
    print_sep_list_box " |" print_pmatch pm;
    Format.close_box ()

and print_pattern = function
      IdPat (s, _) ->
	print_symbol s
    | AnyPat (_) ->
	Format.print_string "_"
    | ConstPat (c, _) ->
	print_constexpr c
    | RangePat (cl, ch, _) ->
	print_constexpr cl;
        Format.print_string " .. ";
        print_constexpr ch
    | AsPat (p, s, _) ->
	print_pattern p;
	Format.print_string " as ";
	print_symbol s
    | TySpecPat (p, t, _) ->
	Format.print_string "(";
	print_pattern p;
	Format.print_string " :";
	Format.print_space ();
	print_type t;
	Format.print_string ")"
    | AltPat (p1, p2, _) ->
	Format.print_string "(";
	print_pattern p1;
	Format.print_string " |";
	Format.print_space ();
	print_pattern p2;
	Format.print_string ")"
    | ConsPat (v, p, _) ->
	Format.print_string "(";
	print_vpath v;
	Format.print_space ();
	print_pattern p;
	Format.print_string ")"
    | TuplePat (pl, _) ->
	Format.print_string "(";
	print_sep_list "," print_pattern pl;
	Format.print_string ")"
    | RecordPat (rl, _) ->
	let print_record_elt (v, p) =
	    print_vpath v;
	    Format.print_string " =";
	    Format.print_space ();
	    print_pattern p
	in
	Format.print_string "{ ";
	print_sep_list ";" print_record_elt rl;
	Format.print_string " }"
    | ArrayPat (pl, _) ->
        Format.print_string "[|";
        print_sep_list ";" print_pattern pl;
        Format.print_string "|]"

let rec print_modtype = function
      VarModtype (m, _) ->
        print_mpath m
    | SigModtype (sl, _) ->
        Format.printf "struct@ ";
        Format.open_box tabstop;
        print_sep_list_box "" print_spec sl;
        Format.close_box ();
        Format.print_cut ();
        Format.print_string " end"
    | FunctorModtype (s, at, rt, _) ->
        Format.printf "functor@ (";
        print_symbol s;
        Format.print_string " : ";
        print_modtype at;
        Format.printf ")@ ->@ ";
        print_modtype rt
    | ConstraintTypeModtype (t, tp, tc, ty, p) ->
        print_modtype t;
        Format.printf "@ with@ ";
	(match tp with
            [] -> ()
          | [x] ->
              print_meta x;
              Format.print_string " "
          | _ ->
              Format.print_string "(";
              print_sep_list "," print_meta tp;
              Format.print_string ") ");
        print_vpath tc;
        Format.printf " =@ ";
        print_type ty
    | ConstraintModModtype (t, m, em, _) ->
        print_modtype t;
        Format.printf "@ with@ ";
        print_mpath m;
        Format.printf "@ =@ ";
        print_empath em

and print_modexp = function
      VarMod (m, _) ->
	print_mpath m
    | StructMod (dl, _) ->
	Format.print_string "struct";
	Format.print_space ();
	Format.open_box tabstop;
	print_sep_list_box "" print_def dl;
	Format.close_box ();
	Format.print_cut ();
	Format.print_string " end"
    | FunctorMod (s, t, e, _) ->
        Format.printf "functor@ (";
	print_symbol s;
	Format.print_string " : ";
	print_modtype t;
        Format.printf ")@ ->@ ";
        print_modexp e
    | ApplyMod (f, a, _) ->
        print_modexp f;
        Format.printf "@ (";
        print_modexp a;
        Format.print_string ")"
    | ConstrainMod (e, t, _) ->
        Format.print_string "(";
        print_modexp e;
        Format.printf "@ :@ ";
	print_modtype t;
        Format.print_string ")"

and print_spec = function
      ValSpec (s, ty, _) ->
        Format.print_string "val ";
        print_symbol s;
        Format.print_string " : ";
        print_type ty
    | ExternSpec (s, t, e, _) ->
        Format.print_string "external ";
        print_symbol s;
        Format.print_string " : ";
         print_type t;
        Format.print_string " = ";
        Format.printf "\"%s\"" (String.escaped e)
    | ExternTypeSpec (a, s, e, _) ->
        Format.print_string "external type ";
        print_type_params a;
        print_symbol s;
        Format.print_string " = ";
        Format.printf "\"%s\"" (String.escaped e)
    | TypeSpec (tl, _) ->
        Format.print_string "type ";
        print_sep_list_box " and" print_tydef tl
    | ExnSpec (s, t, p) ->
        Format.print_string "exception ";
        print_exndef (ExnCons (s, t, p))
    | ModSpec (s, m, _) ->
        Format.print_string "module ";
        print_symbol s;
        Format.print_string " : ";
        print_modtype m
    | ModtypeSpec (s, m, _) ->
        Format.print_string "module type ";
        print_symbol s;
        (match m with
            None -> ()
          | Some m ->
              Format.print_string " = ";
              print_modtype m)
    | OpenSpec (m, _) ->
        Format.print_string "open ";
        print_mpath m
    | IncludeSpec (m, _) ->
        Format.print_string "include ";
        print_empath m

and print_def = function
      LetMod (r, bl, _) ->
	print_let r bl
    | ExternMod (s, t, e, _) ->
	Format.print_string "external ";
	print_symbol s;
	Format.print_string " : ";
	print_type t;
	Format.print_string " = ";
	Format.printf "\"%s\"" (String.escaped e)
    | ExternTypeMod (a, s, e, _) ->
        Format.print_string "external type ";
        print_type_params a;
        print_symbol s;
        Format.print_string " = ";
        Format.printf "\"%s\"" (String.escaped e)
    | TypeMod (tl, _) ->
	Format.print_string "type ";
	print_sep_list_box " and" print_tydef tl
    | ExnMod (e, _) ->
	Format.print_string "exception ";
	print_exndef e
    | ModMod (s, m, _) ->
	Format.print_string "module ";
	print_symbol s;
	Format.print_string " = ";
	print_modexp m
    | ModtypeMod (s, m, _) ->
        Format.print_string "module type ";
        print_symbol s;
        Format.print_string " = ";
        print_modtype m
    | OpenMod (m, _) ->
	Format.print_string "open ";
	print_mpath m
    | IncludeMod (m, _) ->
	Format.print_string "include ";
	print_modexp m

let print_prog prog =
    print_sep_list_box "" print_def prog

let print_interface interface =
    print_sep_list_box "" print_spec interface

