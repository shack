(* 
 * Naml ast standardization code
 * Geoffrey Irving
 * 9apr1
 *)

open Naml_ast

val standardize_prog : prog -> prog
