(*
 * Define the parser errors and print functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

open Symbol
open Naml_ast

exception ParseError of pos * string
exception NotImplemented of string
exception Impossible of string

exception MultipleBindings_TypeId of pos * symbol
exception MultipleBindings_Constr of pos * symbol
exception MultipleBindings_Field of pos * symbol
exception MultipleBindings_Var of pos * symbol
exception MultipleBindings_Pattern of pos * symbol

exception BindingMismatch of pos

exception Unbound_TypeId of pos * vpath
exception Unbound_TypeVar of pos * symbol
exception Unbound_Constr of pos * vpath
exception Unbound_Field of pos * vpath
exception Unbound_Var of pos * vpath

(*
 * Print a location in a file.
 *)
val print_pos : pos -> unit

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
val print_exn : exn -> unit
val print_exn_chan : out_channel -> exn -> unit
val catch : ('a -> 'b) -> 'a -> 'b

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
