(*
 * Naml ast standardization code
 * Geoffrey Irving
 * 9apr1
 *)

open Symbol
open Naml_ast
open Naml_ast_exn

(* Results:
 *   1.  All identifiers unique within their context (typeid, variable, etc.)
 *   2.  No unbound variables
 *   3.  (eventually) explicit vpaths added when necessary due to open
 *   4.  No multiple definitions (let x = 1 and x = 2 or match x with (y, y) -> ...)
 *)

(* module and module type names are not standardized here *)

type env = symbol SymbolTable.t
module Set = SymbolSet

let env_empty = SymbolTable.empty
let env_add = SymbolTable.add
let env_find = SymbolTable.find
let env_rename env s =
  let s' = new_symbol s in
  let env = env_add env s s' in
    env, s'
let env_rename_set env set = Set.fold (fun env s -> env_add env s (new_symbol s)) env set
let env_rename_list env list = Mc_list_util.fold_map env_rename env list

(* Environments:
 *   tenv : type definition environment
 *   tvenv : type variable environment
 *   cenv : constructor environment
 *   fenv : record field environment
 *   venv : variable environment
 *)

let s_vpath ubv env ((m, s, p) as v) =
  if m != [] then
    raise (NotImplemented "nontrivial vpaths");
  try
    (m, env_find env s, p)
  with
    Not_found -> raise (ubv p v)

let s_vpath_typeid = s_vpath (fun p v -> Unbound_TypeId (p, v))
let s_vpath_cons =   s_vpath (fun p v -> Unbound_Constr (p, v))
let s_vpath_var =    s_vpath (fun p v -> Unbound_Var (p, v))
let s_vpath_field =  s_vpath (fun p v -> Unbound_Field (p, v))

let rec s_ty free tenv tvenv ty =
  match ty with
      TypeFun (a, r, p) ->
        let tvenv, a = s_ty free tenv tvenv a in
        let tvenv, r = s_ty free tenv tvenv r in
          tvenv, TypeFun (a, r, p)
    | TypeTuple (tl, p) ->
        let tvenv, tl = s_ty_list free tenv tvenv tl in
          tvenv, TypeTuple (tl, p)
    | TypeAny p ->
        if not free then
          raise (Unbound_TypeVar (p, new_symbol_string "_"));
        tvenv, TypeVar (new_symbol_string "a", p)
    | TypeVar (s, p) ->
        (try
          tvenv, TypeVar (env_find tvenv s, p)
        with Not_found ->
          if free then
            let tvenv, s = env_rename tvenv s in
              tvenv, TypeVar (s, p)
          else
            raise (Unbound_TypeVar (p, s)))
    | TypeId (tl, v, p) ->
        let tvenv, tl = s_ty_list free tenv tvenv tl in
          tvenv, TypeId (tl, s_vpath_typeid tenv v, p)

and s_ty_list free tenv tvenv tyl =
  match tyl with
      [] -> tvenv, []
    | h :: t ->
        let tvenv, h = s_ty free tenv tvenv h in
        let tvenv, t = s_ty_list free tenv tvenv t in
          tvenv, h :: t

let note_tyrep cset fset = function
    TypeCons (cl, p) ->
      List.fold_left (fun cset (s, _) ->
        if Set.mem cset s then
          raise (MultipleBindings_Constr (p, s));
        Set.add cset s) cset cl, fset
  | TypeRecord (fl, p) ->
      cset, List.fold_left (fun fset (_, s, _) ->
        if Set.mem fset s then
          raise (MultipleBindings_Field (p, s));
        Set.add fset s) fset fl

let s_tyrep tenv tvenv cenv fenv tyrep =
  match tyrep with
      TypeCons (cl, p) ->
        TypeCons (List.map (fun (s, tl) -> env_find cenv s, snd (s_ty_list false tenv tvenv tl)) cl, p)
    | TypeRecord (fl, p) ->
        TypeRecord (List.map (fun (m, s, ty) -> m, env_find fenv s, snd (s_ty false tenv tvenv ty)) fl, p)

let s_tydef_list tenv cenv fenv tdl =
  let tset, cset, fset = List.fold_left
    (fun (tset, cset, fset) (_,s,_,tr,p) ->
      if Set.mem tset s then
        raise (MultipleBindings_TypeId (p, s));
      let tset = Set.add tset s in
      let cset, fset = match tr with
          None -> cset, fset
        | Some tr -> note_tyrep cset fset tr
      in
        tset, cset, fset)
    (Set.empty, Set.empty, Set.empty) tdl in
  let tenv = env_rename_set tenv tset in
  let cenv = env_rename_set cenv cset in
  let fenv = env_rename_set fenv fset in
  let tdl = List.map
    (fun (tvl, s, ty, tyrep, p) ->
      let s = env_find tenv s in
      let tvenv, tvl = env_rename_list env_empty tvl in
      let ty = match ty with None -> None | Some ty -> Some (snd (s_ty false tenv tvenv ty)) in
      let tyrep = match tyrep with None -> None | Some tyrep -> Some (s_tyrep tenv tvenv cenv fenv tyrep) in
        tvl, s, ty, tyrep, p) tdl
  in
    tenv, cenv, fenv, tdl

let s_exndef tenv cenv = function
    ExnVar (s, v, p) ->
      let cenv, s = env_rename cenv s in
        cenv, ExnVar (s, s_vpath_cons cenv v, p)
  | ExnCons (s, tl, p) ->
      let cenv, s = env_rename cenv s in
        cenv, ExnCons (s, snd (s_ty_list false tenv env_empty tl), p)

let s_constexpr cenv ce =
  match ce with
      CConsConst (v, p) -> CConsConst (s_vpath_cons cenv v, p)
    | _ -> ce

let rec s_expr tenv cenv fenv venv e =
  let s_e = s_expr tenv cenv fenv venv in
  let s_eo = function None -> None | Some e -> Some (s_e e) in
    match e with
        VarExpr (v, p) ->                     VarExpr (s_vpath_var venv v, p)
      | ConstExpr (ce, p) ->                  ConstExpr (s_constexpr cenv ce, p)
      | TySpecExpr (e, ty, p) ->              TySpecExpr (s_e e, snd (s_ty false tenv env_empty ty), p)
      | TupleExpr (el, p) ->                  TupleExpr (List.map s_e el, p)
      | ConsExpr (v, e, p) ->                 ConsExpr (s_vpath_cons cenv v, s_e e, p)
      | ArrayExpr (el, p) ->                  ArrayExpr (List.map s_e el, p)

      | RecordExpr (eo, fl, p) ->
          let eo = match eo with None -> None | Some e -> Some (s_e e) in
          let fl = List.map (fun (v, e) -> s_vpath_field fenv v, s_e e) fl in
            RecordExpr (eo, fl, p)

      | ApplyExpr (e, el, p) ->               ApplyExpr (s_e e, List.map s_e el, p)
      | ProjExpr (e1, v, p) ->                ProjExpr (s_e e1, s_vpath_field fenv v, p)
      | AssignProjExpr (e1, v, e2, p) ->      AssignProjExpr (s_e e1, s_vpath_field fenv v, s_e e2, p)
      | ArrayEltExpr (e1, e2, p) ->           ArrayEltExpr (s_e e1, s_e e2, p)
      | AssignArrayEltExpr (e1, e2, e3, p) -> AssignArrayEltExpr (s_e e1, s_e e2, s_e e3, p)
      | IfExpr (e1, e2, e3, p) ->             IfExpr (s_e e1, s_e e2, s_eo e3, p)
      | WhileExpr (e1, e2, p) ->              WhileExpr (s_e e1, s_e e2, p)

      | ForExpr (s, e1, d, e2, e3, p) ->
          let venv, s = env_rename venv s in
            ForExpr (s, s_e e1, d, s_e e2, s_expr tenv cenv fenv venv e3, p)

      | SeqExpr (e1, e2, p) ->                SeqExpr (s_e e1, s_e e2, p)
      | MatchExpr (e, m, p) ->                MatchExpr (s_e e, s_pmatch tenv cenv fenv venv m, p)
      | TryExpr (e, m, p) ->                  TryExpr (s_e e, s_pmatch tenv cenv fenv venv m, p)

      | FunExpr (al, e, p) ->
          let venv, al = env_rename_list venv al in
            FunExpr (al, s_expr tenv cenv fenv venv e, p)

      | LetExpr (r, bl, e, p) ->
          let venv, bl = s_binding_list tenv cenv fenv venv r bl in
            LetExpr (r, bl, s_expr tenv cenv fenv venv e, p)

      | GroupExpr (e, p) ->                   GroupExpr (s_e e, p)

and s_pmatch tenv cenv fenv venv m =
  let s_pm (p, eo, e) =
    ignore (check_pattern Set.empty p);
    let vset = note_pattern Set.empty Set.empty p in
    let venv = env_rename_set venv vset in
    let p = s_pattern tenv cenv fenv venv p in
    let eo = match eo with None -> None | Some e -> Some (s_expr tenv cenv fenv venv e) in
    let e = s_expr tenv cenv fenv venv e in
      (p, eo, e)
  in
    List.map s_pm m

and note_pattern eset vset = function
    IdPat (s, p) ->
      if Set.mem eset s then
        raise (MultipleBindings_Var (p, s))
      else
        Set.add vset s
  | AsPat (pt, s, p) ->
      if Set.mem eset s then
        raise (MultipleBindings_Var (p, s))
      else
        Set.add (note_pattern eset vset pt) s
  | AnyPat _ | ConstPat _ | RangePat _ ->
      vset
  | TySpecPat (pt, _, _) | ConsPat (_, pt, _) ->
      note_pattern eset vset pt
  | AltPat (pt1, pt2, _) ->
      note_pattern eset (note_pattern eset vset pt1) pt2
  | TuplePat (ptl, _) | ArrayPat (ptl, _) ->
      List.fold_left (note_pattern eset) vset ptl
  | RecordPat (fl, _) -> List.fold_left (fun vset (v,pt) -> note_pattern eset vset pt) vset fl

and s_pattern tenv cenv fenv venv pt =
  let sps = s_pattern tenv cenv fenv venv in
    match pt with
        IdPat (s, p) ->           IdPat (env_find venv s, p)
      | AnyPat _ ->               pt
      | ConstPat (ce, p) ->       ConstPat (s_constexpr cenv ce, p)
      | RangePat (ce1, ce2, p) -> RangePat (s_constexpr cenv ce1, s_constexpr cenv ce2, p)
      | AsPat (pt, s, p) ->       AsPat (sps pt, env_find venv s, p)
      | TySpecPat (pt, ty, p) ->  TySpecPat (sps pt, snd (s_ty true tenv env_empty ty), p)
      | AltPat (pt1, pt2, p) ->   AltPat (sps pt1, sps pt2, p)
      | ConsPat (v, pt, p) ->     ConsPat (s_vpath_cons cenv v, sps pt, p)
      | TuplePat (ptl, p) ->      TuplePat (List.map sps ptl, p)
      | ArrayPat (ptl, p) ->      ArrayPat (List.map sps ptl, p)
      | RecordPat (fl, p) ->      RecordPat (List.map (fun (v,pt) -> s_vpath_field fenv v, sps pt) fl, p)

and check_pattern vs = function
    IdPat (s, p) ->
      if Set.mem vs s then
        raise (MultipleBindings_Pattern (p, s));
      Set.add vs s
  | AsPat (pt, s, p) ->
      if Set.mem vs s then
        raise (MultipleBindings_Pattern (p, s));
      check_pattern (Set.add vs s) pt
  | AnyPat _ | ConstPat _ | RangePat _ -> vs
  | TySpecPat (p, _, _) | ConsPat (_, p, _) ->
      check_pattern vs p
  | TuplePat (pl, _) | ArrayPat (pl, _) -> List.fold_left check_pattern vs pl
  | RecordPat (fl, _) -> List.fold_left (fun vs (v, p) -> check_pattern vs p) vs fl
  | AltPat (p1, p2, pos) ->
      let vs1 = check_pattern vs p1 in
      let vs2 = check_pattern vs p2 in
        if not (Set.equal vs1 vs2) then
          raise (BindingMismatch pos);
        vs1

and s_binding_list tenv cenv fenv venv r bl =
  let sl = List.fold_left (fun vset (pt, _, _) -> note_pattern vset vset pt) Set.empty bl in
  let venv' = env_rename_set venv sl in
  let s_bind venv (pt, e, p) =
    ignore (check_pattern Set.empty pt);
    let pt = s_pattern tenv cenv fenv venv' pt in
    let e = s_expr tenv cenv fenv venv e in
      (pt, e, p)
  in
    if r then
      venv', List.map (s_bind venv') bl
    else
      venv', List.map (s_bind venv) bl

let s_def tenv cenv fenv venv = function
    LetMod (r, bl, p) ->
      let venv, bl = s_binding_list tenv cenv fenv venv r bl in
        tenv, cenv, fenv, venv, LetMod (r, bl, p)
  | ExternMod (s, ty, str, p) ->
      let venv, s = env_rename venv s in
      let _, ty = s_ty true tenv env_empty ty in
        tenv, cenv, fenv, venv, ExternMod (s, ty, str, p)
  | ExternTypeMod (a, s, e, p) ->
      let a = List.map new_symbol a in
      let cons = Naml_parse_misc.extern_tydef_cons e in
      let cenv = List.fold_left (fun cenv s -> env_add cenv s s) cenv cons in
      let tenv, s = env_rename tenv s in
        tenv, cenv, fenv, venv, ExternTypeMod (a, s, e, p)
  | TypeMod (tdl, p) ->
      let tenv, cenv, fenv, tdl = s_tydef_list tenv cenv fenv tdl in
        tenv, cenv, fenv, venv, TypeMod (tdl, p)
  | ExnMod (ed, p) ->
      let cenv, ed = s_exndef tenv cenv ed in
        tenv, cenv, fenv, venv, ExnMod (ed, p)
  | ModMod (_, _, p) | ModtypeMod (_, _, p) | OpenMod (_, p) | IncludeMod (_, p) ->
      raise (NotImplemented "module standardization")

let rec s_def_list tenv cenv fenv venv dl =
  let rec s_dl tenv cenv fenv venv = function
      [] -> tenv, cenv, fenv, venv, []
    | d :: t ->
        let tenv, cenv, fenv, venv, d = s_def tenv cenv fenv venv d in
        let tenv, cenv, fenv, venv, t = s_dl tenv cenv fenv venv t in
          tenv, cenv, fenv, venv, d :: t
  in
    s_dl tenv cenv fenv venv dl

(**** global functions *****)

let standardize_prog prog =
  let prog = Naml_parse_misc.required_types @ prog in
  let _, _, _, _, prog = s_def_list env_empty env_empty env_empty env_empty prog in
    prog

