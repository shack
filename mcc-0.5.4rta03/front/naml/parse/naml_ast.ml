(* Abstract syntax for naml
 * Geoffrey Irving
 * 20jan00
 * vim:sts=4:sw=4
 *)

open Symbol

(*
 * Position contains:
 *   1. filename
 *   2,3. starting line, starting char
 *   4,5. ending line, ending char
 *)

type pos = string * int * int * int * int

type mpath = (symbol * pos) list
type vpath = mpath * symbol * pos

type ty =
    TypeFun of ty * ty * pos
  | TypeTuple of ty list * pos

  | TypeAny of pos
  | TypeVar of symbol * pos
  | TypeId of ty list * vpath * pos

type tyrep =
    TypeCons of (symbol * ty list) list * pos
  | TypeRecord of (bool * symbol * ty) list * pos

type tydef = symbol list * symbol * ty option * tyrep option * pos

type exndef = 
    ExnVar of symbol * vpath * pos
  | ExnCons of symbol * ty list * pos

type constexpr =
    IntConst of int * pos
  | FloatConst of float * pos
  | CharConst of char * pos
  | CConsConst of vpath * pos

type expr =
    VarExpr of vpath * pos
  | ConstExpr of constexpr * pos
  | TySpecExpr of expr * ty * pos
  | TupleExpr of expr list * pos
  | ConsExpr of vpath * expr * pos
  | ArrayExpr of expr list * pos
  | RecordExpr of expr option * (vpath * expr) list * pos
  | ApplyExpr of expr * expr list * pos
  | ProjExpr of expr * vpath * pos
  | AssignProjExpr of expr * vpath * expr * pos
  | ArrayEltExpr of expr * expr * pos
  | AssignArrayEltExpr of expr * expr * expr * pos
  | IfExpr of expr * expr * expr option * pos
  | WhileExpr of expr * expr * pos 
  | ForExpr of symbol * expr * bool * expr * expr * pos
  | SeqExpr of expr * expr * pos
  | MatchExpr of expr * pmatch * pos
  | FunExpr of symbol list * expr * pos
  | TryExpr of expr * pmatch * pos
  | LetExpr of bool * binding list * expr * pos
  | GroupExpr of expr * pos

and pmatch = (pattern * expr option * expr) list
and binding = pattern * expr * pos

and pattern =
    IdPat of symbol * pos
  | AnyPat of pos
  | ConstPat of constexpr * pos
  | RangePat of constexpr * constexpr * pos
  | AsPat of pattern * symbol * pos
  | TySpecPat of pattern * ty * pos 
  | AltPat of pattern * pattern * pos
  | ConsPat of vpath * pattern * pos
  | TuplePat of pattern list * pos
  | RecordPat of (vpath * pattern) list * pos
  | ArrayPat of pattern list * pos
  
(***** modules *****)

type empath =
    VarEmpath of symbol * pos
  | ProjEmpath of empath * empath * pos
  | ApplyEmpath of empath * empath * pos

type modtype =
    VarModtype of mpath * pos
  | SigModtype of spec list * pos
  | FunctorModtype of symbol * modtype * modtype * pos
  | ConstraintTypeModtype of modtype * symbol list * vpath * ty * pos
  | ConstraintModModtype of modtype * mpath * empath * pos

and spec =
    ValSpec of symbol * ty * pos
  | ExternSpec of symbol * ty * string * pos
  | ExternTypeSpec of symbol list * symbol * string * pos
  | TypeSpec of tydef list * pos
  | ExnSpec of symbol * ty list * pos
  | ModSpec of symbol * modtype * pos
  | ModtypeSpec of symbol * modtype option * pos
  | OpenSpec of mpath * pos
  | IncludeSpec of empath * pos

type modexp =
    VarMod of mpath * pos
  | StructMod of def list * pos
  | FunctorMod of symbol * modtype * modexp * pos
  | ApplyMod of modexp * modexp * pos
  | ConstrainMod of modexp * modtype * pos

and def =
    LetMod of bool * binding list * pos 
  | ExternMod of symbol * ty * string * pos
  | ExternTypeMod of symbol list * symbol * string * pos
  | TypeMod of tydef list * pos
  | ExnMod of exndef * pos
  | ModMod of symbol * modexp * pos
  | ModtypeMod of symbol * modtype * pos
  | OpenMod of mpath * pos
  | IncludeMod of modexp * pos

type prog = def list
type interface = spec list
