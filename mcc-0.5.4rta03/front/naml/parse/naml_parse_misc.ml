(*
 * Miscellaneous parsing extras
 * Geoffrey Irving
 * 3apr1
 *)

open Symbol
open Naml_ast

(* special symbols *)
let true_sym =      Symbol.add "true"
let false_sym =     Symbol.add "false"
let unit_sym =      Symbol.add "()"
let list_nil_sym =  Symbol.add "[]"
let list_cons_sym = Symbol.add "::"

let extern_tydef_cons = function
    "%unit"   -> [unit_sym]
  | "%bool"   -> [false_sym; true_sym]
  | "%list"   -> [list_nil_sym; list_cons_sym]
  | _ -> []

let required_types = 
  let pos = "<null>", 0, 0, 0, 0 in
  let ts () = new_symbol_string "tmp" in
    [ExternTypeMod ([], ts (), "%unit", pos);
     ExternTypeMod ([], ts (), "%bool", pos)]
