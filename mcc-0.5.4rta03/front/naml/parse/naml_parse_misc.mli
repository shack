(*
 * Miscellaneous parsing extras
 * Geoffrey Irving
 * 3apr1
 *)

open Symbol
open Naml_ast

(* special symbols *)
val true_sym : symbol
val false_sym : symbol
val unit_sym : symbol
val list_nil_sym : symbol
val list_cons_sym : symbol

val extern_tydef_cons : string -> symbol list

val required_types : def list
