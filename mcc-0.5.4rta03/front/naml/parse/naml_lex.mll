(* lexer for naml language
 vim:sts=4:sw=4
 *)
{
open Naml_ast
open Naml_parse
open Naml_parse_state
open Naml_ast_exn

(* File position *)
let current_line = ref 1
let current_schar = ref 0

(* Advance a line *)
let set_next_line lexbuf =
    incr current_line;
    current_schar := Lexing.lexeme_end lexbuf

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
    let line = !current_line in
    let schar = Lexing.lexeme_start lexbuf - !current_schar in
    let echar = Lexing.lexeme_end lexbuf - !current_schar in
    let file = current_file () in
    let pos = file, line, schar, line, echar in
	set_current_position pos;
	pos

(* parse escape codes in a string with initial '"' at index n
 * this and lex_char depend on the regular expressions working correctly *)
let lex_string s n =
    let l = String.length s - n in
    let s = String.sub s n l in
    let is_digit c = c >= '0' && c <= '9' in
    let digit_of c = int_of_char c - int_of_char '0' in
    let rec skip_blanks s i o =
	match s.[i] with
            '"' -> String.sub s 0 o
	  | ' ' -> skip_blanks s (i+1) o
	  | '\t' -> skip_blanks s (i+1) o
	  | _ -> unescape s i o
    and unescape s i o =
	if s.[i] == '"' then	(* at end of string *)
	    String.sub s 0 o
	else if s.[i] != '\\' then
	    (s.[o] <- s.[i];
	     unescape s (i+1) (o+1))
	else match s.[i+1] with
	    '\n' -> skip_blanks s (i+2) o
	  | '\\' -> s.[o] <- '\\'; unescape s (i+2) (o+1)
	  | '"' ->  s.[o] <- '"';  unescape s (i+2) (o+1)
	  | 'b' ->  s.[o] <- '\b'; unescape s (i+2) (o+1)
	  | 'n' ->  s.[o] <- '\n'; unescape s (i+2) (o+1)
	  | 'r' ->  s.[o] <- '\r'; unescape s (i+2) (o+1)
	  | 't' ->  s.[o] <- '\t'; unescape s (i+2) (o+1)
	  | _ when (l > i+3) && (is_digit s.[i+1]) && (is_digit s.[i+2]) && (is_digit s.[i+3]) ->
		    s.[o] <- char_of_int (((digit_of s.[i+1] * 10 + digit_of s.[i+2]) * 10 + digit_of s.[i+3]) mod 256); unescape s (i+4) (o+1)
	  | _ ->    s.[o] <- '\\'; unescape s (i+1) (o+1)
    in unescape s 1 0

let lex_char s =
    let digit_of c = int_of_char c - int_of_char '0' in
    if s.[1] != '\\' then
	s.[1]
    else match s.[2] with
        '\\' -> '\\'
      | '\'' -> '\''
      | 'b' -> '\b'
      | 'n' -> '\n'
      | 'r' -> '\r'
      | 't' -> '\t'
      | _ -> char_of_int (((digit_of s.[2] * 10 + digit_of s.[3]) * 10 + digit_of s.[4]) mod 256)

let lex_directive lexbuf =
  let s = Lexing.lexeme lexbuf in
  let rec set_file i = 
    match s.[i] with
        '"' -> set_current_position (lex_string s i, !current_line, !current_schar, !current_line, !current_schar)
      | '\n' -> set_current_position (current_file (), !current_line, !current_schar, !current_line, !current_schar)
      | _ -> set_file (i+1) in
  let rec set_line i =
    match s.[i] with
        '0' .. '9' -> set_line (i+1)
      | _ -> current_line := int_of_string (String.sub s 1 (i-1));
             current_schar := Lexing.lexeme_end lexbuf;
             set_file i 
  in
    set_line 1

}

let whitespace = [' ' '\t' '\r' '\012']+

let digit = ['0'-'9']
let decimal = digit +
let hex = ("0x" | "0X") ['0'-'9' 'a'-'f' 'A'-'F']+
let octal = ("0o" | "0O") ['0'-'7']+
let binary = ("0b" | "0B") ['0' '1']+
let string = '"' ( [^ '"' '\\'] | '\\' _ )* '"'
let char = '\'' ([^ '\'' '\\'] | '\\' ( [ '\\' 'b' 't' 'n' 'r' '\'' ] | digit digit digit ) ) '\''
let float = digit+ ('.' digit* | ('.' digit* )? ['e' 'E'] ['+' '-']? digit+)
let comment_begin = "(*"
let comment_end = "*)"

let lc_ident_prefix = ['_' 'a'-'z']
let uc_ident_prefix = ['A'-'Z']
let ident_suffix = ( ['_' 'A'-'Z' 'a'-'z' '0'-'9' '\''] )*
	
let op = ['!' '$' '%' '&' '*' '+' '-' '.' '/' ':' '<' '=' '>' '?' '@' '^' '|' '~']

rule comment = parse
    comment_begin   { comment lexbuf; comment lexbuf }
  | comment_end	    { () }
  | eof		    { raise (ParseError(set_lexeme_position lexbuf, "Unterminated comment")) }
  | '\n'            { set_next_line lexbuf; comment lexbuf }
  | _		    { comment lexbuf }

(* Main lexer *)
and main = parse
    whitespace
	{
	    main lexbuf
	}
  | '\n'
	{   set_next_line lexbuf;  (* maintain linenumber *)
	    main lexbuf
	}
  | '#' decimal whitespace? (string whitespace?)? '\n'
	{   lex_directive lexbuf; 
            main lexbuf
        }
  | comment_begin
	{   let pos = set_lexeme_position lexbuf in (* we only save begining of comment possition *)
	      comment lexbuf; main lexbuf
	}
  | decimal | hex | octal | binary
	{   let pos = set_lexeme_position lexbuf in
	      TokInt (int_of_string (Lexing.lexeme lexbuf), pos)
	}
  | float
	{   let pos = set_lexeme_position lexbuf in
              TokFloat (float_of_string (Lexing.lexeme lexbuf), pos)
	}
  | string
	{   let pos = set_lexeme_position lexbuf in
              TokString (lex_string (Lexing.lexeme lexbuf) 0, pos)
	}
  | char
        {   let pos = set_lexeme_position lexbuf in
              TokChar (lex_char (Lexing.lexeme lexbuf), pos)
        }
  | "!="
  | ":" | ";" | ";;" | "," | "'" | "_"	    (* characters and delimeters, non-operators (mostly) *)
  | "("|")" | "["|"]" | "[|"|"|]" | "[<"|">]" | "{"|"}" | "{<"|">}"
  | "." | "::" | ".."
(*  | "`" | "#" | "~" | "?" | "??" *)
	{   let pos = set_lexeme_position lexbuf in
	    let op = Lexing.lexeme lexbuf in
	    match op with
	      | "!="	-> TokOpInL11 (Symbol.add op, pos)

	      | ":"	-> TokIs pos
	      | ";"	-> TokSemi pos    (* prec R18 *)
	      | ";;"	-> TokSemiSemi pos
	      | ","	-> TokComma pos   (* prec N15 *)
	      | "'"	-> TokMeta pos
	      | "_"	-> TokAny pos

	      | "("	-> TokLParen pos
	      | ")"	-> TokRParen pos
	      | "["	-> TokLList pos
	      | "]"	-> TokRList pos
	      | "[|"	-> TokLArray pos
	      | "|]"	-> TokRArray pos
	    (*| "[<"	-> TokLStream pos*) (* Streams: unimplimented *)
	    (*| ">]"	-> TokRStream pos*) (* Streams: unimplimented *)
	      | "{"	-> TokLRecord pos
	      | "}"	-> TokRRecord pos
            (*|	"{<"	-> TokLSelf pos*)   (* Objects: unimplimented *)
	    (*|	">}"	-> TokRSelf pos*)   (* Objects: unimplimented *)

	      | "."	-> TokDot pos		(* prec N2 *)
	      | "::"	-> TokCons pos		(* prec R9 *)
	      | ".."	-> TokDotDot pos

	    (*| "`"	-> TokPoly pos*)    (* Polymorphism: unimplimented *)
	    (*| "#"	-> TokHash pos*)    (* Objects: unimplimented *)
	    (*| "~"	-> TokLabel pos*)   (* Labels: unimplimented *)
	    (*| "?"	-> TokOptLabel pos*)	(* Lobels: unimplimented *)
	    (*|	"??"	-> TokWhatWhat pos*)	(* Streams: unimplimented *)

	      | _	-> raise (NotImplemented (Printf.sprintf "character '%s'" (String.escaped op)))
	}
  | ['!' '?' '~'] op*
	{   let pos = set_lexeme_position lexbuf in
	    TokOpPreN1 (Symbol.add (Lexing.lexeme lexbuf), pos)
	}
  | "**" op*
	{   let pos = set_lexeme_position lexbuf in
	    TokOpInR6 (Symbol.add (Lexing.lexeme lexbuf), pos)
	}
  | ['*' '/' '%'] op* | "mod"
	{   let pos = set_lexeme_position lexbuf in
	    let op = Lexing.lexeme lexbuf in
	    match op with
	      | "*"	-> TokStar pos
	      | _	-> TokOpInL7 (Symbol.add op, pos)
	}
  | ['+' '-'] op*
	{   let pos = set_lexeme_position lexbuf in
	    let op = Lexing.lexeme lexbuf in
	    match op with
	      | "-"	-> TokOpMinus pos  	(* prec N5 (prefix) or L8 (infix) - really an operator *)
	      | "-."	-> TokOpMinusDot pos	(* prec N5 (prefix) or L8 (infix) - really an operator *)
	      | "->"	-> TokRArrow pos
	      | _	-> TokOpInL8 (Symbol.add op, pos)
	}
  | ['@' '^'] op*
	{   let pos = set_lexeme_position lexbuf in
	    TokOpInR10 (Symbol.add (Lexing.lexeme lexbuf), pos)
	}
  | ":="
	{   let pos = set_lexeme_position lexbuf in
	    TokOpInR16 (Symbol.add (Lexing.lexeme lexbuf), pos)
	}
  | "&&"
	{   let pos = set_lexeme_position lexbuf in
	    TokSSAnd pos
	}
  | "||"
	{   let pos = set_lexeme_position lexbuf in
	    TokSSOr pos
	}
  | "&"
	{   let pos = set_lexeme_position lexbuf in
	    TokOpInL13 (Symbol.add (Lexing.lexeme lexbuf), pos)
	}
  | "or"
	{   let pos = set_lexeme_position lexbuf in
	    TokOpInL14 (Symbol.add "or", pos)
	}
  | ['=' '<' '>' '|' '&' '$' '%'] op*		    (* all other operators *)
  | "land" | "lor" | "lxor" | "lsl" | "lsr" | "asr"
	{   let pos = set_lexeme_position lexbuf in
	    let op = Lexing.lexeme lexbuf in
	    match op with
	      | "|"	-> TokAlt pos
	      | "<-"	-> TokLArrow pos	(* prec R16 *)
	      | "="	-> TokEq pos
	      | _	-> TokOpInL11 (Symbol.add op, pos)
	}
  | uc_ident_prefix ident_suffix		    (* upper case identifiers *)
	{   let pos = set_lexeme_position lexbuf in
	    TokCapId (Symbol.add (Lexing.lexeme lexbuf), pos)
	}
  | lc_ident_prefix ident_suffix		    (* lower case identifiers and keywords *)
        {   let pos = set_lexeme_position lexbuf in
	    let id = Lexing.lexeme lexbuf in
	    match id with
	      | "open" ->	TokOpen pos
	      | "include" ->	TokInclude pos
	      | "assert" ->	TokAssert pos

	      | "begin" ->	TokBegin pos
	      | "end" ->	TokEnd pos

	      | "type" ->	TokType pos
	      | "exception" ->	TokException pos
	      | "of" ->		TokOf pos
	      | "val" ->	TokVal pos
	      | "external" ->	TokExternal pos
	    (*| "constraint" ->	TokConstraint pos*)

	      | "let" ->	TokLet pos	(* prec N19 *)
	      | "in" ->		TokIn pos
	      | "fun" ->	TokFun pos	(* prec N19 *)
	      | "function" ->	TokFunction pos	(* prec N19 *)
	      | "rec" ->	TokRec pos
	      | "and" ->	TokAnd pos

	      | "if" ->		TokIf pos	(* prec N17 *)
	      | "then" ->	TokThen pos
	      | "else" ->	TokElse pos

	      | "for" ->	TokFor pos
	      | "to" ->		TokTo pos
	      | "downto" ->	TokDownto pos
	      | "while" ->	TokWhile pos
	      | "do" ->		TokDo pos
	      | "done" ->	TokDone pos

	      | "match" ->	TokMatch pos	(* prec N19 *)
	      | "try" ->	TokTry pos	(* prec N19 *)
	      | "with" ->	TokWith pos
	      | "as" ->		TokAs pos
	      | "when" ->	TokWhen pos

	      | "struct" ->	TokStruct pos
	      | "module" ->	TokModule pos
	      | "mutable" ->	TokMutable pos
	      | "sig" ->	TokSig pos
	      | "functor" ->	TokFunctor pos

	    (*| "lazy" ->	TokLazy pos*)
	    (*| "parser" ->	TokParser pos*)	(* Parsers: unimplimented *)
	    (*| "class" ->	TokClass pos*)	(* Objects: unimplimented *)
	    (*| "new" ->	TokNew pos*)	(* Objects: unimplimented *)
	    (*| "inherit" ->	TokInherit pos*)    (* Objects: unimplimented *)
	    (*| "method" ->	TokMethod pos*)	(* Objects: unimplimented *)
	    (*| "virtual" ->	TokVirtual pos*)    (* Objects: unimplimented *)
	    (*| "private" ->	TokPrivate pos*)    (* Objects: unimplimented *)

	    (*| "closed" ->	TokClosed pos*)	(* Unknown/valid ident *)

              | "true" ->       TokCapId (Naml_parse_misc.true_sym, pos)
              | "false" ->      TokCapId (Naml_parse_misc.false_sym, pos)
	      | _ ->	        TokId (Symbol.add id, pos)
	}
  | eof
        {   let pos = set_lexeme_position lexbuf in
            TokEOF pos
        }
  | _ 
	{   let pos = set_lexeme_position lexbuf in
            raise (ParseError (pos, Printf.sprintf "illegal character '%s'" (String.escaped (Lexing.lexeme lexbuf))))
	}

