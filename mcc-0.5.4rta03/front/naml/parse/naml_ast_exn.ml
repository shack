(*
 * Define the parser errors and print functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol
open Naml_ast
open Naml_parse_state
open Naml_ast_util

(*
 * BUG: until we fix this module.
 *)
let print_symbol = pp_print_symbol Format.std_formatter

exception ParseError of pos * string
exception NotImplemented of string
exception Impossible of string

exception MultipleBindings_TypeId of pos * symbol
exception MultipleBindings_Constr of pos * symbol
exception MultipleBindings_Field of pos * symbol
exception MultipleBindings_Var of pos * symbol
exception MultipleBindings_Pattern of pos * symbol

exception BindingMismatch of pos

exception Unbound_TypeId of pos * vpath
exception Unbound_TypeVar of pos * symbol
exception Unbound_Constr of pos * vpath
exception Unbound_Field of pos * vpath
exception Unbound_Var of pos * vpath

exception TypeCons_Arity of pos * vpath * int * int
exception Invalid_ExternType of string

(*
 * Print the position in a file.
 *)
let print_pos (name, sline, schar, eline, echar) =
   if sline = eline then
      begin
         Format.print_string name;
         Format.print_string ":";
         Format.print_int sline;
         Format.print_string ":";
         Format.print_int schar;
         Format.print_string "-";
         Format.print_int echar;
         Format.print_string ":"
      end
   else
      begin
         Format.print_string name;
         Format.print_string ":";
         Format.print_int sline;
         Format.print_string ",";
         Format.print_int schar;
         Format.print_string "-";
         Format.print_int eline;
         Format.print_string ",";
         Format.print_int echar;
         Format.print_string ":"
      end

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
let print_exn = function
   ParseError (pos, str) ->
      print_pos pos;
      Format.print_string ":";
      Format.print_space ();
      Format.print_string str
 | Parsing.Parse_error ->
      print_pos (current_position ());
      Format.print_space ();
      Format.print_string "Syntax error"
 | NotImplemented str ->
      Format.printf "Not implemented:@ %s" str
 | Impossible str ->
      Format.print_string "IMPOSSIBLE: ";
      Format.print_string str

 | MultipleBindings_TypeId (pos, s) ->
      print_pos pos;
      Format.print_string": this type constructor is bound multiple times in this type definition: ";
      print_symbol s
 | MultipleBindings_Constr (pos, s) ->
      print_pos pos;
      Format.print_string ": this constructor is bound multiple times in this type definition: ";
      print_symbol s
 | MultipleBindings_Field (pos, s) ->
      print_pos pos;
      Format.print_string ": this field is bound multiple times in this type definition: ";
      print_symbol s
 | MultipleBindings_Var (pos, s) ->
      print_pos pos;
      Format.print_string ": this variable is bound multiple times in this let: ";
      print_symbol s
 | MultipleBindings_Pattern (pos, s) ->
      print_pos pos;
      Format.print_string ": this variable is bound multiple times in this pattern: ";
      print_symbol s

 | BindingMismatch pos ->
      print_pos pos;
      Format.print_string ": the bound variables on either side of this | pattern do not match"

 | Unbound_TypeId (pos, v) ->
      print_pos pos;
      Format.print_string ": unbound type constructor: ";
      print_vpath v
 | Unbound_TypeVar (pos, s) ->
      print_pos pos;
      Format.print_string ": unbound type variable: ";
      print_meta s
 | Unbound_Constr (pos, v) ->
      print_pos pos;
      Format.print_string ": unbound constructor: ";
      print_vpath v
 | Unbound_Field (pos, v) ->
      print_pos pos;
      Format.print_string ": unbound field: ";
      print_vpath v
 | Unbound_Var (pos, v) ->
      print_pos pos;
      Format.print_string ": unbound variable: ";
      print_vpath v

 | exn ->
      print_pos (current_position ());
      Format.print_string "uncaught exception:";
      Format.print_space ();
      Format.print_string (Printexc.to_string exn)

let print_exn_chan out exn =
   Format.set_formatter_out_channel out;
   print_exn exn;
   Format.print_newline ()

let catch f x =
   try f x with
      exn ->
         print_exn_chan stderr exn;
         exit 1

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
