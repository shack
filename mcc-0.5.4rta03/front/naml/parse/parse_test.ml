(*
 vim:sts=4:sw=4
 * Parser tester is a toploop.
 * You can type an expression.
 * It is printed.
 *
 * Author: Jason Hickey
 *
 * Modified for naml by Dylan Simon
 *)

open Naml_ast
open Naml_ast_util

(*
 * Arguments.
 *)
let saw_input = ref false
let print_ast = ref false

let usage = "usage: ast_test [files...]"

let debug_lexer = Naml_lex.main

(*
 * Compilation.
 *)
let compile name =
   (* Note that we compiled a file *)
   saw_input := true;

   (* Open the input channel and set the position *)
   let inx = open_in name in
   let _ = Naml_parse_state.set_current_position (name, 1, 0, 1, 0) in

   try
      if Filename.check_suffix name ".mli" then
        (let lexbuf = Lexing.from_channel inx in
         let interface = Naml_parse.interface debug_lexer lexbuf in
  	   print_interface interface;
           Format.print_newline ())
      else
        (let lexbuf = Lexing.from_channel inx in
         let prog = Naml_parse.prog debug_lexer lexbuf in
           print_prog prog;
           Format.print_newline ())
   with
      exn ->
         Naml_ast_exn.print_exn_chan stderr exn;
         close_in inx;
         exit 1

(*
 * Toploop.
 *)
let toploop () =
   (* Set the filename *)
   let _ = Naml_parse_state.set_current_position ("<stdin>", 1, 0, 1, 0) in

   (* Create the lexer *)
   let lexbuf = Lexing.from_channel stdin in

   (* Parse and print the expressions *)
   let donep = ref false in
      while not !donep do
         (* Print a prompt *)
         output_string stdout "# ";
         flush stdout;

         (* Get the declaration *)
         try
            match Naml_parse.topstmt (*Lexer.main*)debug_lexer lexbuf with
               Some def ->
                        (* Print it *)
                        print_prog [ def ] ;
                        Format.print_newline ()

             | None ->
                  donep := true
         with
            exn ->
               Naml_ast_exn.print_exn_chan stderr exn;
               flush stderr
      done

let _ =
   Arg.parse [] compile usage;
   let rest =
      if not !saw_input then
         toploop
      else
	fun () -> ()
   in
      Naml_ast_exn.catch rest ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
