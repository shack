(* 
 * naml lexer
 * Geoffrey Irving / Dylan Simon
 *)

(*
 * Main lexing function.
 *)
val main : Lexing.lexbuf -> Naml_parse.token
