(*
 * Utilities for working on syntax.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *
 * Modified for naml by: Dylan Simon
 *)

open Symbol
open Naml_ast

(*
 * Combine two positions.
 *)
val union_pos : pos -> pos -> pos
val list_last : 'a list -> 'a
val pos_of_mpath : mpath -> pos
val pos_of_empath : empath -> pos
val pos_of_type : ty -> pos
val pos_of_modtype : modtype -> pos
val pos_of_modexp : modexp -> pos
val pos_of_tyrep : tyrep -> pos
val pos_of_expr : expr -> pos
val pos_of_pattern : pattern -> pos 
val pos_of_const : constexpr -> pos

(*
 * Printing
 *)
val print_meta : symbol -> unit
val print_mpath : mpath -> unit
val print_vpath : vpath -> unit
val print_expr : expr -> unit
val print_prog : prog -> unit
val print_interface : interface -> unit

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
