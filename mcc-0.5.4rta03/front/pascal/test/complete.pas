program var_params;

// Global types
type adam = integer;
     adam2 = single;
     pInt = ^integer;
     intProc = procedure (i: integer);
               
// Global variables 
var
    i: integer;
    foo: integer;
    myArray: array [12 .. 23, 1 .. 10] of integer;
    ptr: pInt;

// User functions and procedures 
procedure change_me_to(var a: integer; i: integer);
var
    unused: integer;
begin
    a := i;
end;

procedure print_half(i: integer);
begin
    print_int(i div 2);
end;

procedure execute_proc(proc: intProc);
var
    i: integer;
begin
    i := 12;
    proc(i);
end;

// The main program
begin
    i := 1;
    execute_proc(print_half);
    ptr := &i;
    change_me_to(foo, i);
    print_int(foo); print_string('  and  ');print_int(i);

    ptr^ := 10;
    change_me_to(foo, i); print_string('  and  ');print_int(ptr^);
    print_int(foo);
    
    print_char(#10);
end.
