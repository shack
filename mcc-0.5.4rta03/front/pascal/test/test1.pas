program test1;

procedure print_string(s: string); external;
procedure print_int(i: integer); external;
procedure print_char(c: char); external;

var
    i,j: integer;
begin
    print_string('The value of i=');
    print_int(i);
    print_string('\n');
    print_string('The value of j=');
    print_int(j);
    print_char(#13);
end.