program case_demo;

procedure print_string(s: string); external;
procedure print_int(i: integer); forward;

var
    myVar: integer;

begin
    myVar := 3;
    
    inc(myVar);
    dec(myVar, 2);
        
    case myVar of
	1:  begin print_string('1-Got you!'); print_string('I know this is far off...'); end;
	2:  print_string('Is it 2?');
	3:  print_string('Is it 3?');
        else print_string('I give up'); 
    end;
end.
