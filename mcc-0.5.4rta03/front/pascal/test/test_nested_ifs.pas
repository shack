program nested_ifs;

procedure print_string; external;

var i: integer;

begin
   if 1 < 2 then begin 
      if 2 < 3 then begin
         if 3 < 4 then begin 
	    print_string('ok');
	 end;
      end else begin 
         print_string('bad');
      end;
   end else begin 
      print_string('bad'); 
   end;

   print_string('\n');
end.