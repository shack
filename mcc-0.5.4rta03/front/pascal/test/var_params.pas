program var_params;

type adam = integer;

procedure change_me_to(var a: integer; i: integer);
var
    unused: integer;
begin
    a := i;
end;

var
    i: integer;
    foo: integer;
begin
    i := 1;
    change_me_to(foo, i);
    print_int(foo);
end.
