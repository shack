unit unit1;

uses unit2, unit3;

interface
type kind = string;

var machine_name, my_name: kind;

procedure print_string; external;

procedure print_who_am_i;

implementation

procedure print_who_am_i;
begin
   print_string('My name is ');
   print_string(my_name);
   print_string(', and this computer"s name is ');
   print_string(machine_name);
end;

// initialize global variables
begin
   machine_name := 'bendeguz';
   my_name := 'adam';
end.