open Pascal_ast_type
open Pascal_ast_util
open Pascal_ast_state

let parse_str str =
   let lexbuf = Lexing.from_string str in
   try
      let el = Pascal_parser.program Pascal_lexer.main lexbuf in
      print_expr_list (Some el);
      print_newline ()
   with Parsing.Parse_error ->
      print_string "Parse error."; print_newline(); flush stdout

let parse_stdin () =
  let lexbuf = Lexing.from_channel stdin in
  try
     let result = Pascal_parser.console Pascal_lexer.main lexbuf in
	 print_endline ">>>";
	 print_expr_list result;
	 print_newline ();
	 flush stdout
  with Parsing.Parse_error ->
     print_string "Parse error."; print_newline (); flush stdout

let parse_file file =
  let inc = open_in file in
  let lexbuf = Lexing.from_channel inc in

  try
     let result = Pascal_parser.console Pascal_lexer.main lexbuf in
	 print_endline ">>>";
	 print_expr_list result;
	 print_newline ();
	 flush stdout;
	 close_in inc	 
  with Parsing.Parse_error ->
     print_string "Parse error in "; print_pos (current_position()); print_newline (); flush stdout; close_in inc; exit 1


let _ =
    if Array.length Sys.argv < 2
       then parse_stdin ()
       else parse_file Sys.argv.(1)
