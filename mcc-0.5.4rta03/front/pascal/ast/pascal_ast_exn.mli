(*
 * Define the parser errors and print functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey, Adam Granicz
 * jyh@cs.caltech.edu, granicz@cs.caltech.edu
 *)

open Pascal_ast_type

(*
 * Print a location in a file.
 *)
val print_pos : pos -> unit

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
val print_exn : exn -> unit
val print_exn_chan : out_channel -> exn -> unit
val catch : ('a -> 'b) -> 'a -> 'b

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
