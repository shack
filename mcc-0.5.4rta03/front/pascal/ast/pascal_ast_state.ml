(*
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey, Adam Granicz
 * jyh@cs.caltech.edu, granicz@cs.caltech.edu
 *)

open Symbol
open Pascal_ast_type

(*
 * File position information.
 *)
let position = ref ("<null>", 1, 0, 1, 0)

let set_current_position pos =
   position := pos

let current_position () =
   !position

let current_file () =
   let name, _, _, _, _ = !position in
      name

(*
 * Names that are actually types.
 *)
let typedefs = ref [SymbolSet.empty, []]

(*
 * Basic checking.
 *)
let is_type s =
   match !typedefs with
      (tenv, _) :: _ ->
         SymbolSet.mem tenv s
    | [] ->
         raise (Invalid_argument "Fc_ast_state.is_type")

let add_type s =
   match !typedefs with
      (tenv, decls) :: tl ->
         if not (SymbolSet.mem tenv s) then
            typedefs := (SymbolSet.add tenv s, decls) :: tl
    | [] ->
         raise (Invalid_argument "Fc_ast_state.add_type")

let add_typedef s pos ty =
   match !typedefs with
      (tenv, decls) :: tl ->
         typedefs := (SymbolSet.add tenv s, (s, ty, pos) :: decls) :: tl
    | [] ->
         raise (Invalid_argument "Fc_ast_state.add_typedef")

(*
 * Block structure, and type scoping.
 *)
let push_tenv () =
   match !typedefs with
      (tenv, _) as h :: t ->
         typedefs := (tenv, []) :: h :: t
    | [] ->
         raise (Invalid_argument "Fc_ast_state.push_tenv")

(*
 * Return all the struct defs.
 *)
let pop_tenv () =
   match !typedefs with
      (_, decls) :: tl ->
         typedefs := tl;
         decls
    | [] ->
         raise (Invalid_argument "Fc_ast_state.pop_tenv")

(*
 * Base types.
 *)
let _ =
   List.iter add_type (**)
      (List.map Symbol.add ["bool"; "char"; "int"; "float"])

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
