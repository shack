(*
 * Define the parser errors and print functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey, Adam Granicz
 * jyh@cs.caltech.edu, granicz@cs.caltech.edu
 *)

open Format

open Symbol
open Pascal_ast_type

(*
 * Print the position in a file.
 *)
let print_pos (name, sline, schar, eline, echar) =
   if sline = eline then
      begin
         Format.print_string name;
         Format.print_string ":";
         Format.print_int sline;
         Format.print_string ":chars ";
         Format.print_int schar;
         Format.print_string "-";
         Format.print_int echar;
         Format.print_string ":"
      end
   else
      begin
         Format.print_string name;
         Format.print_string ":";
         Format.print_int sline;
         Format.print_string ",";
         Format.print_int schar;
         Format.print_string "-";
         Format.print_int eline;
         Format.print_string ",";
         Format.print_int echar;
         Format.print_string ":"
      end

(*
 * Print an evaluation error.
 *)
let print_eval_error = function
   UnboundType s ->
      Format.print_string "unbound type: ";
      print_symbol s
 | UnboundVar s ->
      Format.print_string "unbound variable: ";
      print_symbol s
 | UnboundField s ->
      Format.print_string "unbound label: ";
      print_symbol s
 | NilPointer ->
      Format.print_string "nil pointer"
 | TypeError s ->
      Format.print_string "type error: ";
      Format.print_string s
 | SubscriptOutOfBounds i ->
      Format.print_string "subscript out of bounds: ";
      Format.print_int i
 | DivideByZero ->
      Format.print_string "divide by zero"
 | NotImplemented s ->
      Format.print_string "not implemented: ";
      Format.print_string s
 | NotAnInt ->
      Format.print_string "not an integer"
 | NotAnLValue ->
      Format.print_string "not an assignable value"
 | NotAScalar ->
      Format.print_string "not a scalar"
 | NotAPointer ->
      Format.print_string "not a pointer"
 | ArityMismatch ->
      Format.print_string "function is called with the wrong number of arguments"

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
let print_exn = function
   ParseError (pos, str) ->
      print_pos pos;
      Format.print_string ":";
      Format.print_space ();
      Format.print_string str
 | Parsing.Parse_error ->
      print_pos (current_position ());
      Format.print_space ();
      Format.print_string "Syntax error"
 | EvalException (pos, e) ->
      print_pos pos;
      Format.print_string ": evaluation error:";
      Format.print_space ();
      print_eval_error e
 | Return (pos, _) ->
      print_pos pos;
      Format.print_string ": uncaught return value"
 | Break pos ->
      print_pos pos;
      Format.print_string "uncaught break"
 | exn ->
      print_pos (current_position ());
      Format.print_string "uncaught exception:";
      Format.print_space ();
      Format.print_string (Printexc.to_string exn)

let print_exn_chan out exn =
   Format.set_formatter_out_channel out;
   print_exn exn;
   Format.print_newline ()

let catch f x =
   try f x with
      exn ->
         print_exn_chan stderr exn;
         exit 1

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
