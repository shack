(*
 * Utilities over the syntax.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999,2001 Jason Hickey, Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey, Adam Granicz
 * jyh@cs.caltech.edu, granicz@cs.caltech.edu
 *)

open Format
open Symbol
open Pascal_ast_type

(************************************************************************
 * POSITIONS
 ************************************************************************)

(*
 * Combine two positions.
 *)
let union_pos
    (file1, sline1, schar1, eline1, echar1)
    (file2, sline2, schar2, eline2, echar2) =
   if file1 <> file2 then
      raise (Invalid_argument (**)
                (Printf.sprintf "union_pos: file mistmatch: \"%s\":\"%s\"" (**)
                    (String.escaped file1) (String.escaped file2)));
   let sline, schar =
      if sline1 < sline2 then
         sline1, schar1
      else if sline1 > sline2 then
         sline2, schar2
      else
         sline1, min schar1 schar2
   in
   let eline, echar =
      if eline1 > eline2 then
         eline1, echar1
      else if eline1 < eline2 then
         eline2, echar2
      else
         eline1, max echar1 echar2
   in
      file1, sline, schar, eline, echar

(*
 * Get the position info.
 *)
let pos_of_expr = function
   IntExpr (_, pos) -> pos
 | FloatExpr (_, pos) -> pos
 | CharExpr (_, pos) -> pos
 | StringExpr (_, pos) -> pos
 | VarExpr (_, pos) -> pos
 | AddrOfExpr (_, pos) -> pos
 | UArithExpr (_, _, pos) -> pos
 | UnOpExpr (_, _, pos) -> pos
 | BinOpExpr (_, _, _, pos) -> pos
 | BoolOpExpr (_, _, _, pos) -> pos
 | SubscriptExpr (_, _, pos) -> pos
 | ProjectExpr (_, _, pos) -> pos
 | ApplyExpr (_, _, pos) -> pos
 | AssignExpr (_, _, _, pos) -> pos
 | IfExpr (_, _, _, pos) -> pos
 | ForExpr (_, _, _, _, pos) -> pos
 | WhileExpr (_, _, pos) -> pos
 | SeqExpr (_, pos) -> pos
 | ReturnExpr (_, pos) -> pos
 | BreakExpr pos -> pos
 | VarDefs (_, pos) -> pos
 | FunDef (_, _, _, _, pos) -> pos
 | TypeDefs (_, pos) -> pos

let pos_of_type = function
   TypeBool pos -> pos
 | TypeChar pos -> pos
 | TypeInt pos -> pos
 | TypeFloat pos -> pos
 | TypeVar (_, pos) -> pos
(* | TypeLambda (_, _, pos) -> pos*)
 | TypeApply (_, _, pos) -> pos
 | TypeArray (_, _, pos) -> pos

 | TypeUnit pos -> pos
(* | TypeProcedure pos -> pos*)
 | TypeReference (_, pos) -> pos
 | TypePointer (_, pos) -> pos
 | TypeUnknown (_, pos) -> pos

 | TypeStruct (_, pos) -> pos
 | TypeFun (_, _, pos) -> pos

(************************************************************************
 * PRINTING                                                             *
 ************************************************************************)

(*
 * Blocks indents by this many spaces.
 *)
let tabstop = 3

(*
 * Operators.
 *)
let print_boolop op =
   let s =
      match op with
         LAndOp -> "&&"
       | LOrOp -> "||"
   in
      Format.print_string s

let print_binop op =
   let s =
      match op with
         PlusOp -> "+"
       | MinusOp -> "-"
       | TimesOp -> "*"
       | DivideOp -> "/"
       | ModOp -> "mod"
       | XorOp -> "xor"
       | OrOp -> "or"
       | AndOp -> "and"
       | IntDivideOp -> "div"
(*       | BAndOp -> "&"
       | BOrOp -> "|"
       | BXorOp -> "^" *)
       | LShiftOp -> "shl"
       | RShiftOp -> "shr"
       | EqOp -> "="
       | NotEqOp -> "<>"
       | LeOp -> "<="
       | LtOp -> "<"
       | GeOp -> ">="
       | GtOp -> ">"
   in
      Format.print_string s

(*
 * Print a list of items with a separator.
 *)
let rec print_sep_list separator printer = function
   [] ->
      ()
 | [h] ->
      printer h
 | h :: t ->
      printer h;
      Format.print_string separator;
      Format.print_space ();
      print_sep_list separator printer t

let print_sep_list separator printer l =
   Format.open_hvbox 0;
   print_sep_list separator printer l;
   Format.close_box ()

(*
 * Print a list of items with a terminator.
 *)
let rec print_term_list terminator printer = function
   [] ->
      ()
 | [h] ->
      printer h;
      Format.print_string terminator
 | h :: t ->
      printer h;
      Format.print_string terminator;
      Format.print_space ();
      print_term_list terminator printer t

(*
 * Unary expression.
 *)
let rec print_unop op e =
   let s =
      match op with
         UMinusOp -> "-"
       | UNotOp -> "!"
       | UHatOp -> "^"
   in
      Format.print_string s;
      print_expr e

and print_uarith op e =
   let pre_s, post_s =
      match op with
         PreIncrOp -> "++", ""
       | PreDecrOp -> "--", ""
(*       | PostIncrOp -> "", "++"
       | PostDecrOp -> "", "--"*)
   in
      Format.print_string pre_s;
      print_expr e;
      Format.print_string post_s

(*
 * Print expressions.
 *)
and print_expr = function
   IntExpr (i, _) ->
      Format.print_int i
 | FloatExpr (x, _) ->
      Format.print_float x
 | CharExpr (c, _) ->
      Format.print_string ("'" ^ Char.escaped c ^ "'")
 | StringExpr (s, _) ->
      Format.print_string ("\"" ^ String.escaped s ^ "\"")
 | VarExpr (v, _) ->
      print_symbol v
 | AddrOfExpr (expr, _) ->
      Format.print_string "&(";
      print_expr expr;
      Format.print_string ")"
 | UArithExpr (op, expr, _) ->
      Format.print_string "(";
      print_uarith op expr;
      Format.print_string ")"
 | UnOpExpr (op, expr, _) ->
      Format.print_string "(";
      print_unop op expr;
      Format.print_string ")"
 | BinOpExpr (op, expr1, expr2, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "(";
      print_expr expr1;
      Format.print_space ();
      print_binop op;
      Format.print_char ' ';
      print_expr expr2;
      Format.print_string ")";
      Format.close_box ()
 | BoolOpExpr (op, expr1, expr2, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "(";
      print_expr expr1;
      Format.print_space ();
      print_boolop op;
      Format.print_char ' ';
      print_expr expr2;
      Format.print_string ")";
      Format.close_box ()
 | SubscriptExpr (expr1, expr2, _) ->
      Format.open_hvbox 0;
      print_expr expr1;
      Format.print_string "[";
      print_expr expr2;
      Format.print_string "]";
      Format.close_box ()
 | ProjectExpr (expr, sym, _) ->
      Format.open_hvbox 0;
      print_expr expr;
      Format.print_string ".";
      print_symbol sym;
      Format.close_box ()
 | ApplyExpr (id, args, _) ->
      Format.open_hvbox tabstop;
      print_expr id;
      Format.print_cut ();
      Format.print_string "(";
      print_args args;
      Format.print_string ")";
      Format.close_box ()
 | AssignExpr (op, v, expr, _) ->
      Format.open_hvbox tabstop;
      print_expr v;
      Format.print_space ();
      (match op with
          Some op ->
             print_binop op
        | None ->
             ());
      Format.print_string "<- ";
      print_expr expr;
      Format.close_box ()
 | IfExpr (e1, e2, None, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "if(";
      print_expr e1;
      Format.print_string ")";
      Format.print_space ();
      print_expr e2;
      Format.close_box ()
 | IfExpr (e1, e2, Some e3, _) ->
      Format.open_hvbox 0;
      Format.open_hvbox tabstop;
      Format.print_string "if(";
      print_expr e1;
      Format.print_string ")";
      Format.print_space ();
      print_expr e2;
      Format.close_box ();
      Format.print_space ();
      Format.open_hvbox tabstop;
      Format.print_string "else";
      Format.print_space ();
      print_expr e3;
      Format.close_box ();
      Format.close_box ()
 | ForExpr (e1, e2, e3, e4, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "for(";
      print_expr e1;
      Format.print_string ";";
      Format.print_space ();
      print_expr e2;
      Format.print_string ";";
      Format.print_space ();
      print_expr e3;
      Format.print_string ")";
      Format.print_space ();
      print_expr e4;
      Format.close_box ()
 | WhileExpr (e1, e2, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "while(";
      print_expr e1;
      Format.print_string ")";
      Format.print_space ();
      print_expr e2;
      Format.close_box ()
 | SeqExpr (exprs, _) ->
      Format.open_hvbox 0;
      Format.open_hvbox tabstop;
      Format.print_string "{";
      Format.print_space ();
      print_term_list ";" print_expr exprs;
      Format.close_box ();
      Format.print_space ();
      Format.print_string "}";
      Format.close_box ()
 | ReturnExpr (expr, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "return";
      Format.print_space ();
      print_expr expr;
      Format.close_box ()
 | BreakExpr _ ->
      Format.print_string "break;"
 | VarDefs (decls, _) ->
      let print_var_decl (sym, ty, expr, _) =
         Format.open_hvbox tabstop;
         Format.print_string "var ";
         print_type ty;
         Format.print_space ();
         print_symbol sym;
         (match expr with
             None ->
                ()
           | Some expr ->
                Format.print_string " =";
                Format.print_space ();
                print_expr expr);
         Format.print_string ";";
         Format.close_box ();
         Format.print_space ()
      in
         List.iter print_var_decl decls
 | FunDef (sym, args, ty, body, _) ->
      Format.open_hvbox 0;
      print_type ty;
      Format.print_string " ";
      print_symbol sym;
      Format.print_string "(";
      print_formals args;
      Format.print_string ")";
      Format.print_space ();
      print_expr body;
      Format.close_box ();
      Format.print_string ";";
      Format.print_space ()
 | TypeDefs (decls, _) ->
      let print_type_decl (sym, ty, _) =
         Format.open_hvbox tabstop;
         Format.print_string "typedef ";
         print_symbol sym;
         Format.print_string " =";
         Format.print_space ();
         print_type ty;
         Format.close_box ();
         Format.print_string ";";
         Format.print_space ()
      in
         List.iter print_type_decl decls

(*
 * Application arguments.
 *)
and print_args args =
   print_sep_list "," print_expr args

(*
 * Print the formal parameters.
 *)
and print_formals args =
   print_sep_list "," (fun (sym, ty, _) ->
         print_type ty;
         Format.print_string " ";
         print_symbol sym) args

(*
 * Print a type.
 *)
and print_type = function
   TypeBool _ ->
      Format.print_string "boolean "
 | TypeChar _ ->
      Format.print_string "char "
 | TypeInt _ ->
      Format.print_string "integer "
 | TypeFloat _ ->
      Format.print_string "single "
 | TypeVar (sym, _) ->
      Format.print_string "'";
      print_symbol sym
(* | TypeLambda (vars, ty, _) ->
      Format.open_hvbox tabstop;
      Format.print_string "(Lambda (";
      print_sep_list "," print_symbol vars;
      Format.print_string ").";
      Format.print_space ();
      print_type ty;
      Format.print_string ")";
      Format.close_box () *)
 | TypeApply (sym, [], _) ->
      print_symbol sym
 | TypeApply (sym, tyl, _) ->
      Format.open_hvbox tabstop;
      print_symbol sym;
      Format.print_string "[";
      print_sep_list "," print_type tyl;
      Format.print_string "]";
      Format.close_box ()
 | TypeArray (ty, bound_list, _) ->
      Format.print_string "(";
      print_type ty;
      let a = List.map (fun (e1, e2) ->
          Format.print_string ")[";
          print_expr e1;
          Format.print_string "..";
          print_expr e2;
          Format.print_string "]") bound_list in ();
 | TypeStruct (fields, _) ->
      Format.open_hvbox 0;
      Format.open_hvbox tabstop;
      Format.print_string "{";
      Format.print_space ();
      print_term_list ";" (fun (v, ty, _) ->
            print_type ty;
            Format.print_space ();
            print_symbol v) fields;
      Format.close_box ();
      Format.print_space ();
      Format.print_string "}";
      Format.close_box ()
 | TypeFun (args, ty, _) ->
      Format.print_string "fun ((";
      print_sep_list "," print_type args;
      Format.print_string ") -> ";
      print_type ty;
      Format.print_string ")"
 | TypeUnit (_) ->
      Format.print_string "unit ";
 | TypeReference (ty, _) ->
      Format.print_string "ref ";
      print_type ty
 | TypeUnknown (sym, _) -> 
      Format.print_string "<unresolved type '";
      print_symbol sym;
      Format.print_string "'>"
 | TypePointer (ty, _) ->
      Format.print_string "^";
      print_type ty

and print_types types =
   print_sep_list "," print_type types

(*
 * Finally, we can print programs.
 *)
let print_prog prog =
   Format.open_hvbox 0;
   List.iter print_expr prog;
   Format.close_box ()

(*
 * Print a list of expressions
 *)
let rec print_expr_list elist = 
   match elist with
      Some n -> print_prog n
    | None -> ()
    
(*
 * Print the position in a file.
 *)
let print_pos (name, sline, schar, eline, echar) =
   if sline = eline then
      begin
         Format.print_string name;
         Format.print_string " line ";
         Format.print_int sline;
         Format.print_string " chars ";
         Format.print_int schar;
         Format.print_string "-";
         Format.print_int echar;
         Format.print_string ":\n"
      end
   else
      begin
         Format.print_string name;
         Format.print_string ":";
         Format.print_int sline;
         Format.print_string ",";
         Format.print_int schar;
         Format.print_string "-";
         Format.print_int eline;
         Format.print_string ",";
         Format.print_int echar;
         Format.print_string ":\n"
      end
  

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
