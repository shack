(*
 * Lexer for a simple Pascal grammar.
 *
 * ----------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

{
open Pascal_parser
open Pascal_ast_state
open Pascal_ast_type

(*
 * File position.
 *)
let current_line = ref 1
let current_schar = ref 0

(*
 * Advance a line.
 *)
let set_next_line lexbuf =
   incr current_line;
   current_schar := Lexing.lexeme_end lexbuf

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
   let line = !current_line in
   let schar = Lexing.lexeme_start lexbuf - !current_schar in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let file = current_file () in
   let pos = file, line, schar, line, echar in
      set_current_position pos;
      pos

(*
 * Provide a buffer for building strings.
 *)
let stringbuf = Buffer.create 19
let stringstart = ref (0, 0)

let pop_string lexbuf =
   let s = Buffer.contents stringbuf in
   let sline, schar = !stringstart in
   let eline = !current_line in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let pos = current_file (), sline, schar, eline, echar in
      Buffer.clear stringbuf;
      s, pos

let set_string_start lexbuf =
   stringstart := !current_line, Lexing.lexeme_start lexbuf - !current_schar

(*
 * Extract the char constant.
 *)
let zero_code = Char.code '0'

let lex_char s pos =
   let len = pred (String.length s) in
   let make_octal code =
      Char.chr (code land 255)
   in
   let rec collect_octal code i =
      if i = len then
         make_octal code
      else
         collect_octal (code * 8 + Char.code s.[i] - zero_code) (succ i)
  in
      match s.[1] with
         '\\' ->
            let c =
                match s.[2] with
                   '0'..'7' -> collect_octal 0 2
                 | 'n' -> '\n'
                 | 't' -> '\t'
                 | 'r' -> '\r'
                 | '\'' -> '\''
                 | '"' -> '"'
                 | _ -> raise (Invalid_argument "lex_char")
            in
               TokChar (c, pos)
       | c ->
           TokChar (c, pos)
}

(*
 * Regular expressions
 *)
let name_prefix = ['_' 'A'-'Z' 'a'-'z']
let name_suffix = ['_' 'A'-'Z' 'a'-'z' '0'-'9']
let name = name_prefix name_suffix *

let cspecial1 = ['(' ')' '[' ']' '{' '}' ';' ',' '.' '?' ':' '*' '%']
let especial = ['<' '>' '!' '='] '='
let cspecial2 = ['=' '!']
let mspecial = ['+' '-' '/' '<' '>' '&' '|']+

let decimal = ['0'-'9']+
let octal = '0' ['0'-'7']*
let hex = "0x" ['0'-'9' 'a'-'f' 'A'-'F']+
let float0 = ['0'-'9']+ '.' ['0'-'9']* (('e' | 'E') ('+' | '-')? decimal)?
let float1 = ['0'-'9']* '.' ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)?
let float2 = ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)

let a0_199 = ['0'-'1']? ['0'-'9']? ['0'-'9']
let a200_249 = '2'? ['0'-'4']? ['0'-'9']
let a250_255 = '2'? '5'? ['0'-'5']
let char_const   = '#'(a0_199 | a200_249 | a250_255)

(*
 * Main lexer.
 *)
rule main = parse 
   [' ' '\t']+ { main lexbuf }
 | '\n' { set_next_line lexbuf; main lexbuf }
 | decimal
      { let pos = set_lexeme_position lexbuf in
           TokInt (int_of_string (Lexing.lexeme lexbuf), pos)
      }
 | float0 | float1 | float2
      { let pos = set_lexeme_position lexbuf in
           TokFloat (float_of_string (Lexing.lexeme lexbuf), pos)
      }
 | name
      { let pos = set_lexeme_position lexbuf in
        let id = String.lowercase (Lexing.lexeme lexbuf) in
	   match id with 
	      "nil" -> TokNil pos
	    | "true" -> TokTrue pos
	    | "false" -> TokFalse pos
	    | "begin" -> TokBegin pos
	    | "end" -> TokEnd pos
	    | "break" -> TokBreak pos
	    | "case" -> TokCase pos
	    | "if" -> TokIf pos
	    | "then" -> TokThen pos
	    | "else" -> TokElse pos
	    | "for" -> TokFor pos
	    | "while" -> TokWhile pos
	    | "repeat" -> TokRepeat pos
	    | "until" -> TokUntil pos
	    | "do" -> TokDo pos
	    | "and" -> TokAnd pos
	    | "or" -> TokOr pos
	    | "xor" -> TokXor pos
	    | "mod" -> TokMod pos
	    | "div" -> TokDiv pos
	    | "shr" -> TokShr pos
	    | "shl" -> TokShl pos
	    | "not" -> TokNot pos
	    | "var" -> TokVar pos
	    | "const" -> TokConst pos
	    | "type" -> TokType pos
	    | "function" -> TokFunction pos
	    | "procedure" -> TokProcedure pos
	    | "program" -> TokProgram pos
	    | "uses" -> TokUses pos
	    | "interface" -> TokInterface pos
	    | "implementation" -> TokImplementation pos
	    | "unit" -> TokUnit pos
	    | "array" -> TokArray pos
	    | "record" -> TokRecord pos
	    | "of" -> TokOf pos
	    | "to" -> TokTo pos
	    | "downto" -> TokDownTo pos
	    | "forward" -> TokForward pos
	    | "external" -> TokExternal pos
	    | "inc" -> TokInc pos
	    | "dec" -> TokDec pos
	    | "integer" -> TokTypeInteger pos
	    | "single" -> TokTypeSingle pos
	    | "string" -> TokTypeString pos
	    | "char" -> TokTypeChar pos
	    | _ -> TokId (Symbol.add id, pos)
      }

   (*
    * Comments.
    *)
 | "//" [^ '\n']* '\n'
      { set_next_line lexbuf; main lexbuf }
 | "(*"
      { comment lexbuf; main lexbuf }

   (*
    * Strings and chars.
    *)
 | '\''
      { set_string_start lexbuf;
        string lexbuf
      }
 | char_const
      { let pos = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
	   lex_char s pos
      }
   (*
    * Special chars.
    *)
 | "("  { let pos = set_lexeme_position lexbuf in TokLeftParen pos }
 | ")"  { let pos = set_lexeme_position lexbuf in TokRightParen pos }
 | "["  { let pos = set_lexeme_position lexbuf in TokLeftBrack pos } 
 | "]"  { let pos = set_lexeme_position lexbuf in TokRightBrack pos }
 | "{"  { let pos = set_lexeme_position lexbuf in TokLeftBrace pos } 
 | "}"  { let pos = set_lexeme_position lexbuf in TokRightBrace pos }
 | ";"  { let pos = set_lexeme_position lexbuf in TokSemi pos }
 | ","  { let pos = set_lexeme_position lexbuf in TokComma pos }
 | ".." { let pos = set_lexeme_position lexbuf in TokTwoDots pos }
 | "."  { let pos = set_lexeme_position lexbuf in TokDot pos }     
 | ":=" { let pos = set_lexeme_position lexbuf in TokAssignEq pos }     
 | ":"  { let pos = set_lexeme_position lexbuf in TokColon pos }    
 | "="  { let pos = set_lexeme_position lexbuf in TokEq pos }     
 | "+"  { let pos = set_lexeme_position lexbuf in TokPlus pos }   
 | "-"  { let pos = set_lexeme_position lexbuf in TokMinus pos }  
 | "*"  { let pos = set_lexeme_position lexbuf in TokStar pos }   
 | "/"  { let pos = set_lexeme_position lexbuf in TokSlash pos } 
 | "<"  { let pos = set_lexeme_position lexbuf in TokLt pos }     
 | ">"  { let pos = set_lexeme_position lexbuf in TokGt pos }     
 | "&"  { let pos = set_lexeme_position lexbuf in TokAmp pos }    
 | "^"  { let pos = set_lexeme_position lexbuf in TokHat pos }  
 | "<=" { let pos = set_lexeme_position lexbuf in TokLe pos }   
 | ">=" { let pos = set_lexeme_position lexbuf in TokGe pos }   
 | "<>" { let pos = set_lexeme_position lexbuf in TokNotEq pos }

   (*
    * All other characters are a syntax error.
    *)
 | _
      { let pos = set_lexeme_position lexbuf in
           raise (ParseError (pos, Printf.sprintf "illegal char: '%s'"
              (String.escaped (Lexing.lexeme lexbuf))))
      }

 | eof
      { TokEof }

(*
 * Strings are delimited by double-quotes.
 * Chars may be escaped.
 *)
and string = parse 
   '\'' 
      { let s, pos = pop_string lexbuf in
           TokString (s, pos)
      }
 | '\\'  { escape lexbuf; string lexbuf }
 | _
      { let s = Lexing.lexeme lexbuf in
           if s.[0] = '\n' then
              set_next_line lexbuf;
           Buffer.add_string stringbuf s;
           string lexbuf
      }
 | eof
      { let s, pos = pop_string lexbuf in
           TokString (s, pos)
      }

and escape = parse 
   '\n' { set_next_line lexbuf }
 | 'n'  { Buffer.add_char stringbuf '\n' }
 | 't'  { Buffer.add_char stringbuf '\t' }
 | 'r'  { Buffer.add_char stringbuf '\r' }
 | _    { Buffer.add_string stringbuf (Lexing.lexeme lexbuf) }

(*
 * Caml-type comment may be nested.
 * C-style comments may not.
 *)
and comment = parse 
   "(*"
      { comment lexbuf; comment lexbuf }
 | "*)"
      { () }
 | eof
      { () }
 | '\n'
      { set_next_line lexbuf;
        comment lexbuf
      }
 | _
      { comment lexbuf }

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
