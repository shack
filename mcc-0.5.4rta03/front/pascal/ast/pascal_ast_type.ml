(*
 * Abstract syntax for a simple Pascal language.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey, Adam Granicz
 * jyh@cs.caltech.edu, granicz@cs.caltech.edu
 *)

open Symbol

(*
 * Position contains:
 *   1. filename
 *   2,3. starting line, starting char
 *   4,5. ending line, ending char
 *)
type pos = string * int * int * int * int

(*
 * Boolean operators.
 *)
type boolop =
   LAndOp
 | LOrOp

(*
 * Binary operators.
 *)
type binop =
   PlusOp
 | MinusOp
 | TimesOp
 | DivideOp
 | IntDivideOp
 | ModOp
 | AndOp
 | OrOp
 | XorOp
 | LShiftOp
 | RShiftOp
 | EqOp
 | NotEqOp
 | LeOp
 | LtOp
 | GeOp
 | GtOp

type sign =
   SiPlus
 | SiMinus
 
type uarithop =
   PreIncrOp
 | PreDecrOp
   
(*
 * Unary operators.
 *)
type unop =
   UMinusOp
 | UNotOp
 | UHatOp

(*
 * Types are either identified by names,
 * or they are array or function types.
 *)
type ty =
   TypeBool of pos
 | TypeChar of pos
 | TypeInt of pos
 | TypeFloat of pos
(* | TypeArray of ty * expr option * pos*)
 | TypeArray of ty * (expr * expr) list * pos
 | TypePointer of ty * pos
 | TypeStruct of ty_fields * pos
 | TypeFun of ty list * ty * pos
 | TypeVar of symbol * pos
(* | TypeLambda of symbol list * ty * pos *)
 | TypeApply of symbol * ty list * pos
 | TypeUnknown of symbol * pos
 | TypeUnit of pos 
(* | TypeProcedure of pos *)
 | TypeReference of ty * pos 

(*
 * Record fields.
 *)
and ty_fields = var_decl list

(*
 * Expressions.
 *)
and expr =
   IntExpr of int * pos
 | FloatExpr of float * pos
 | CharExpr of char * pos
 | StringExpr of string * pos
 | VarExpr of symbol * pos
 | AddrOfExpr of expr * pos
 | UArithExpr of uarithop * expr * pos
 | UnOpExpr of unop * expr * pos
 | BinOpExpr of binop * expr * expr * pos
 | BoolOpExpr of boolop * expr * expr * pos
 | SubscriptExpr of expr * expr * pos
 | ProjectExpr of expr * symbol * pos
 | ApplyExpr of expr * expr list * pos
 | AssignExpr of binop option * expr * expr * pos
 | IfExpr of expr * expr * expr option * pos
 | ForExpr of expr * expr * expr * expr * pos
 | WhileExpr of expr * expr * pos
 | SeqExpr of expr list * pos
 | ReturnExpr of expr * pos
 | BreakExpr of pos
 | VarDefs of var_init_decl list * pos
 | FunDef of symbol * var_decl list * ty * expr * pos
 | TypeDefs of var_decl list * pos

(*
 * A variable declaration.
 *)
and var_init_decl = symbol * ty * expr option * pos

(*
 * Formal parameter for a function,
 * a variable declaration, or a type
 * definition.
 *)
and var_decl = symbol * ty * pos

(*
 * A program is a list of declarations.
 * The empty program is valid.
 *)
type prog = expr list

(*
 * Parse-time exception.
 *)
exception ParseError of pos * string

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
