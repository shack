(*
 * This file defines configuration parameters.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Rawint
open Rawfloat

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Another name for a symbol.
 *)
type var = symbol
type ty_var = symbol
type label = symbol

type 'a label_option =
   Fields of 'a
 | Label of var * label

(*
 * Value precision.
 *)
type precision =
   ShortPrecision
 | NormalPrecision
 | LongPrecision

type signed = bool

(*
 * Type information.
 * status:
 *    const: the data is supposed to be immutable
 *    volatile: the data is changed by an outside mutator
 *    normal: the data is mutable, but it only changes locally
 *
 * storage:
 *    auto: normal stack allocation
 *    register: the variable should be put in a register
 *    static: the data is local to this compilation unit,
 *       and it should be placed in global storage.
 *    extern: the data is defined in another compilation unit,
 *       and it is in global storage.
 *)
type type_status =
   StatusConst
 | StatusVolatile
 | StatusNormal

type storage_class =
   StoreAuto
 | StoreRegister
 | StoreStatic
 | StoreExtern

(************************************************************************
 * DEFAULT PARAMS
 ************************************************************************)

(*
 * By default, char is unsigned.
 *)
let signed_char = false

(************************************************************************
 * PARAM MODULE
 ************************************************************************)

(*
 * Parameters for the front-end.
 *)
module type FCParamSig =
sig
   val max_field_length : int
   val char_precision : precision -> int_precision
   val int_precision : precision -> int_precision
   val float_precision : precision -> float_precision
end

module FCParam : FCParamSig =
struct
   (*
    * Length of fields.
    *)
   let max_field_length = 32

   (*
    * Default precisions.
    *)
   let char_precision = function
      ShortPrecision
    | NormalPrecision ->
         Int8
    | LongPrecision ->
         Int16

   let int_precision = function
      ShortPrecision ->
         Int16
    | NormalPrecision ->
         Int32
    | LongPrecision ->
         Int64

   let float_precision = function
      ShortPrecision ->
         Single
    | NormalPrecision ->
         Double
    | LongPrecision ->
         LongDouble
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
