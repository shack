(*
 * Put all the parts together.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug

let compile name =
   (* Parse the file *)
   let exp = Fc_parse_compile.compile name in

   (* Convert the AST *)
   let exp = Fc_ast_compile.compile exp in

   (* Convert to AIR *)
   let prog = Fc_air_compile.compile name exp in

   (* Convert to IR *)
   let prog = Fc_ir_compile.compile prog in

   (* Generate FIR *)
   let prog = Fc_fir_ir.compile prog in
      if debug_level Fir_state.debug_print_fir 2 then
         Fir_print.debug_prog "*** FC->FIR" prog;
      prog

(*
 * Catch exceptions.
 *)
let compile name =
   Fc_ir_exn_print.catch compile name

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
