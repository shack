(*
 * Generate FIR from IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fc_ir_exn
open Fc_ir_pos

open Fir
open Fir_ds
open Fir_set
open Fir_type
open Fir_subst
open Fir_infer
open Fir_ty_closure
open Fir_standardize

module Frame = Fc_air_compile.AIRFrame

module Pos = MakePos (struct let name = "Fc_fir_ir" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Nil gets mapped to this global sym.
 *)
let nil_sym = Symbol.add ".nil"

(*
 * True set contains a 1.
 *)
let zero32 = Rawint.of_int Rawint.Int32 true 0
let a_zero32 = AtomRawInt zero32
let false_set = IntSet (IntSet.of_point 0)
let true_set = IntSet (IntSet.of_point 1)

let ty_bool = TyEnum 2

let fir_relop op v ty a1 a2 e =
   LetAtom (v, ty_bool, AtomBinop (op, a1, a2), e)

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Translate a type.
 *)
let rec fir_type tenv pos ty =
   match ty with
      Fc_ir.TyUnit n ->
         TyEnum n
    | Fc_ir.TyDelayed ->
         TyDelayed
    | Fc_ir.TyInt (pre, signed) ->
         TyRawInt (pre, signed)
    | Fc_ir.TyFloat pre ->
         TyFloat pre
    | Fc_ir.TyVar v ->
         TyVar v
    | Fc_ir.TyFun (ty_args, ty_res) ->
         let ty_args = fir_type_list tenv pos ty_args in
         let ty_res = fir_type tenv pos ty_res in
            TyFun (ty_args, ty_res)
    | Fc_ir.TyPointer ty ->
         (match Fc_ir_type.tenv_expand tenv pos ty with
             Fc_ir.TyDelayed
           | Fc_ir.TyRawData _
           | Fc_ir.TyPointer _
           | Fc_ir.TyInt _
           | Fc_ir.TyUnit _
           | Fc_ir.TyFloat _
           | Fc_ir.TyTag ->
                TyRawData
           | Fc_ir.TyFun _
           | Fc_ir.TyVar _ ->
                fir_type tenv pos ty
           | ty ->
                raise (IRException (pos, StringTypeError ("illegal type pointed to", ty))))
    | Fc_ir.TyTag
    | Fc_ir.TyRawData _ ->
         TyRawData
    | Fc_ir.TyApply (v, tyl) ->
         let tyl = fir_type_list tenv pos tyl in
            TyApply (v, tyl)
    | Fc_ir.TyAll ([], ty) ->
         fir_type tenv pos ty
    | Fc_ir.TyAll (vars, ty) ->
         let ty = fir_type tenv pos ty in
            TyAll (vars, ty)
    | Fc_ir.TyFrame v ->
         TyFrame (v, [])
    | Fc_ir.TyLambda  _ ->
         raise (IRException (pos, StringTypeError ("illegal IR type", ty)))

and fir_type_list tenv pos tyl =
   List.map (fir_type tenv pos) tyl

(*
 * Sizes for frame calculations.
 *)
let _, sizeof_int = Frame.sizeof_int_const Rawint.Int32
let sizeof_pointer = sizeof_int

let rec sizeof_type tenv pos ty =
   match ty with
      Fc_ir.TyUnit _
    | Fc_ir.TyTag ->
         sizeof_int
    | Fc_ir.TyInt (pre, _) ->
         snd (Frame.sizeof_int_const pre)
    | Fc_ir.TyFloat pre ->
         snd (Frame.sizeof_float_const pre)
    | Fc_ir.TyPointer _
    | Fc_ir.TyFun _
    | Fc_ir.TyVar _
    | Fc_ir.TyDelayed
    | Fc_ir.TyFrame _ ->
         sizeof_pointer
    | Fc_ir.TyRawData size ->
         size
    | Fc_ir.TyLambda (_, ty)
    | Fc_ir.TyAll (_, ty) ->
         sizeof_type tenv pos ty
    | Fc_ir.TyApply _ ->
         sizeof_type tenv pos (Fc_ir_type.tenv_expand tenv pos ty)

(*
 * Toplevel type.
 *)
let fir_top_type tenv v ty =
   let pos = string_pos "fir_top_type" (var_exp_pos v) in
      match ty with
         Fc_ir.TyLambda (vars, ty) ->
            let ty = fir_type tenv pos ty in
               TyDefLambda (vars, ty)
       | _ ->
            let ty = fir_type tenv pos ty in
               TyDefLambda ([], ty)

(*
 * Subscript operator of type.
 *)
let sub_script = RawIntIndex (Rawint.Int32, true)

let sub_value_of_type tenv pos ty =
   match Fc_ir_type.tenv_expand tenv pos ty with
      Fc_ir.TyInt (pre, signed) ->
         RawIntSub (pre, signed)
    | Fc_ir.TyFloat pre ->
         RawFloatSub pre
    | Fc_ir.TyRawData _
    | Fc_ir.TyTag
    | Fc_ir.TyPointer _
    | Fc_ir.TyVar _
    | Fc_ir.TyFrame _ ->
         RawPointerSub
    | Fc_ir.TyAll _
    | Fc_ir.TyFun _ ->
         FunctionSub
    | ty ->
         raise (IRException (pos, StringTypeError ("illegal subscript type", ty)))

let subop_of_type tenv pos ty =
   { sub_block = RawDataSub;
     sub_value = sub_value_of_type tenv pos ty;
     sub_index = ByteIndex;
     sub_script = sub_script
   }

let memcpy_subop =
   { sub_block = RawDataSub;
     sub_value = RawIntSub (Rawint.Int8, false);
     sub_index = ByteIndex;
     sub_script = sub_script
   }

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Translate the atom.
 * An AtomNil gets resolved to the nil symbol.
 *)
let fir_atom a =
   match a with
      Fc_ir.AtomUnit (n, i) ->
         AtomEnum (n, i)
    | Fc_ir.AtomInt i ->
         AtomRawInt i
    | Fc_ir.AtomFloat x ->
         AtomFloat x
    | Fc_ir.AtomNil _ ->
         AtomVar nil_sym
    | Fc_ir.AtomVar v ->
         AtomVar v
    | Fc_ir.AtomLabel l ->
         AtomLabel (l, zero32)

let fir_atoms args =
   List.map fir_atom args

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Translate the expressions.
 *)
let rec fir_exp tenv e =
   let pos = string_pos "fir_exp" (exp_pos e) in
   let loc = Fc_ir_ds.loc_of_exp e in
      match Fc_ir_ds.dest_exp_core e with
         Fc_ir.LetVar (v, Fc_ir.TyRawData i, None, e) ->
            fir_var_exp tenv pos loc v i e
       | Fc_ir.LetFrame (v, ty, e) ->
            fir_frame_exp tenv pos loc v ty e
       | Fc_ir.LetAtom (v, ty, a, e) ->
            fir_atom_exp tenv pos loc v ty a e
       | Fc_ir.LetAddrOfVar (v1, ty, v2, e) ->
            fir_atom_exp tenv pos loc v1 ty (Fc_ir.AtomVar v2) e
       | Fc_ir.LetUnop (v, ty, op, a, e) ->
            fir_unop_exp tenv pos loc v ty op a e
       | Fc_ir.LetBinop (v, ty, op, a1, a2, e) ->
            fir_binop_exp tenv pos loc v ty op a1 a2 e
       | Fc_ir.LetExtCall (v, ty, s, b, ty', args, e) ->
            fir_extcall_exp tenv pos loc v ty s b ty' args e
       | Fc_ir.TailCall op ->
            fir_tailcall_exp tenv pos loc op
       | Fc_ir.LetMalloc (v, a, e) ->
            let pos = string_pos "LetMalloc case" pos in
               make_exp loc (LetAlloc (v, AllocMalloc (TyRawData, fir_atom a), fir_exp tenv e))
       | Fc_ir.IfThenElse (a, e1, e2) ->
            let pos = string_pos "IfThenElse case" pos in
            let false_label = new_symbol_string "false" in
            let true_label = new_symbol_string "true" in
               make_exp loc (Match (fir_atom a, [false_label, false_set, fir_exp tenv e2; true_label, true_set, fir_exp tenv e1]))
       | Fc_ir.LetSubscript (v1, ty, v2, a3, e) ->
            let pos = string_pos "LetSubscript case" pos in
            let subop = subop_of_type tenv pos ty in
               make_exp loc (LetSubscript (subop, v1, fir_type tenv pos ty, AtomVar v2, fir_atom a3, fir_exp tenv e))
       | Fc_ir.SetSubscript (v1, a2, ty, a3, e) ->
            let pos = string_pos "SetSubscript case" pos in
            let subop = subop_of_type tenv pos ty in
            let label = new_symbol_string "set_subscript" in
               make_exp loc (SetSubscript (subop, label, AtomVar v1, fir_atom a2, fir_type tenv pos ty, fir_atom a3, fir_exp tenv e))
       | Fc_ir.SetGlobal (v, ty, a, e) ->
            let pos = string_pos "SetGlobal case" pos in
            let subop = sub_value_of_type tenv pos ty in
            let label = new_symbol_string "set_global" in
               make_exp loc (SetGlobal (subop, label, v, fir_type tenv pos ty, fir_atom a, fir_exp tenv e))
       | Fc_ir.Memcpy (v1, a1, v2, a2, a3, e) ->
            let pos = string_pos "Memcpy case" pos in
            let label = new_symbol_string "memcpy" in
               make_exp loc (Memcpy (memcpy_subop, label, AtomVar v1, fir_atom a1, AtomVar v2, fir_atom a2, fir_atom a3, fir_exp tenv e))
       | Fc_ir.Debug (info, e) ->
            fir_debug_exp tenv pos loc info e
       | Fc_ir.LetProject (v1, ty, v2, v3, e) ->
            let pos = string_pos "LetProject case" pos in
            let subop = subop_of_type tenv pos ty in
               make_exp loc (LetSubscript (subop, v1, fir_type tenv pos ty, AtomVar v2, AtomLabel (v3, zero32), fir_exp tenv e))
       | Fc_ir.SetProject (v1, v2, ty, a3, e) ->
            let pos = string_pos "SetProject case" pos in
            let subop = subop_of_type tenv pos ty in
            let label = new_symbol_string "set_project" in
               make_exp loc (SetSubscript (subop, label, AtomVar v1, AtomLabel (v2, zero32), fir_type tenv pos ty, fir_atom a3, fir_exp tenv e))
       | Fc_ir.LetVar _
       | Fc_ir.LetFuns _
       | Fc_ir.LetClosure _
       | Fc_ir.LetAddrOfProject _
       | Fc_ir.SetVar _ ->
            raise (IRException (pos, InternalError "unexpected expression"))

(*
 * Translate an operation.
 *)
and fir_var_exp tenv pos loc v i e =
   let pos = string_pos "fir_var_exp" pos in
   let ty = TyRawData in
   let e = fir_exp tenv e in
      make_exp loc (LetAlloc (v, AllocMalloc (ty, AtomInt i), e))

and fir_frame_exp tenv pos loc v ty e =
   let pos = string_pos "fir_frame_exp" pos in
   let rec fir_frame ty =
      match Fc_ir_type.tenv_expand tenv pos ty with
         Fc_ir.TyPointer ty ->
            fir_frame ty
       | Fc_ir.TyFrame v ->
            v
       | _ ->
            raise (IRException (pos, StringTypeError ("illegal frame type", ty)))
   in
   let v' = fir_frame ty in
   let e = fir_exp tenv e in
      make_exp loc (LetAlloc (v, AllocFrame (v', []), e))

and fir_atom_exp tenv pos loc v ty a e =
   let pos = string_pos "fir_unop_exp" pos in
   let ty = fir_type tenv pos ty in
   let a = fir_atom a in
   let e = fir_exp tenv e in
      make_exp loc (LetAtom (v, ty, a, e))

and fir_unop_exp tenv pos loc v ty op a e =
   let pos = string_pos "fir_unop_exp" pos in
   let ty = fir_type tenv pos ty in
   let a = fir_atom a in
   let e = fir_exp tenv e in
   let e =
      match op with
         Fc_ir.UMinusIntOp (pre, signed) ->
            LetAtom (v, ty, AtomUnop (UMinusRawIntOp (pre, signed), a), e)
       | Fc_ir.NotIntOp (pre, signed) ->
            LetAtom (v, ty, AtomUnop (NotRawIntOp (pre, signed), a), e)
       | Fc_ir.BitFieldOp (pre, signed, off, len) ->
            LetAtom (v, ty, AtomUnop (RawBitFieldOp (pre, signed, off, len), a), e)
       | Fc_ir.UMinusFloatOp pre ->
            LetAtom (v, ty, AtomUnop (UMinusFloatOp pre, a), e)
       | Fc_ir.AbsFloatOp pre ->
            LetAtom (v, ty, AtomUnop (AbsFloatOp pre, a), e)
       | Fc_ir.SinOp pre ->
            LetAtom (v, ty, AtomUnop (SinFloatOp pre, a), e)
       | Fc_ir.CosOp pre ->
            LetAtom (v, ty, AtomUnop (CosFloatOp pre, a), e)
       | Fc_ir.SqrtOp pre ->
            LetAtom (v, ty, AtomUnop (SqrtFloatOp pre, a), e)
       | Fc_ir.IntOfInt (pre1, signed1, pre2, signed2) ->
            LetAtom (v, ty, AtomUnop (RawIntOfRawIntOp (pre1, signed1, pre2, signed2), a), e)
       | Fc_ir.IntOfFloat (pre1, signed1, pre2) ->
            LetAtom (v, ty, AtomUnop (RawIntOfFloatOp (pre1, signed1, pre2), a), e)
       | Fc_ir.IntOfUnit (pre1, signed1, i) ->
            LetAtom (v, ty, AtomUnop (RawIntOfEnumOp (pre1, signed1, i), a), e)
       | Fc_ir.FloatOfInt (pre1, pre2, signed2) ->
            LetAtom (v, ty, AtomUnop (FloatOfRawIntOp (pre1, pre2, signed2), a), e)
       | Fc_ir.FloatOfFloat (pre1, pre2) ->
            LetAtom (v, ty, AtomUnop (FloatOfFloatOp (pre1, pre2), a), e)
       | Fc_ir.IntOfPointer (pre, signed) ->
            LetAtom (v, ty, AtomUnop (RawIntOfPointerOp (pre, signed), a), e)
       | Fc_ir.PointerOfInt (pre, signed) ->
            LetAtom (v, ty, AtomUnop (PointerOfRawIntOp (pre, signed), a), e)
       | Fc_ir.EqPointerNilOp
       | Fc_ir.NeqPointerNilOp ->
            raise (IRException (pos, InternalError "unexpected operator"))
   in
      make_exp loc e

and fir_binop_exp tenv pos loc v ty op a1 a2 e =
   let pos = string_pos "fir_op_exp" pos in
   let ty = fir_type tenv pos ty in
   let a1 = fir_atom a1 in
   let a2 = fir_atom a2 in
   let e = fir_exp tenv e in
   let e =
      match op with
         Fc_ir.AndBoolOp ->
            LetAtom (v, ty, AtomBinop (AndEnumOp 2, a1, a2), e)
       | Fc_ir.OrBoolOp ->
            LetAtom (v, ty, AtomBinop (OrEnumOp 2, a1, a2), e)
       | Fc_ir.PlusIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (PlusRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.MinusIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (MinusRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.MulIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (MulRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.DivIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (DivRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.RemIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (RemRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.SlIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (SlRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.SrIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (SrRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.AndIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (AndRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.OrIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (OrRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.XorIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (XorRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.MinIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (MinRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.MaxIntOp (pre, signed) ->
            LetAtom (v, ty, AtomBinop (MaxRawIntOp (pre, signed), a1, a2), e)
       | Fc_ir.SetBitFieldOp (pre, signed, off, len) ->
            LetAtom (v, ty, AtomBinop (RawSetBitFieldOp (pre, signed, off, len), a1, a2), e)
       | Fc_ir.EqIntOp (pre, signed) ->
            fir_relop (EqRawIntOp (pre, signed)) v ty a1 a2 e
       | Fc_ir.NeqIntOp (pre, signed) ->
            fir_relop (NeqRawIntOp (pre, signed)) v ty a1 a2 e
       | Fc_ir.LtIntOp (pre, signed) ->
            fir_relop (LtRawIntOp (pre, signed)) v ty a1 a2 e
       | Fc_ir.LeIntOp (pre, signed) ->
            fir_relop (LeRawIntOp (pre, signed)) v ty a1 a2 e
       | Fc_ir.GtIntOp (pre, signed) ->
            fir_relop (GtRawIntOp (pre, signed)) v ty a1 a2 e
       | Fc_ir.GeIntOp (pre, signed) ->
            fir_relop (GeRawIntOp (pre, signed)) v ty a1 a2 e
       | Fc_ir.PlusFloatOp pre ->
            LetAtom (v, ty, AtomBinop (PlusFloatOp pre, a1, a2), e)
       | Fc_ir.MinusFloatOp pre ->
            LetAtom (v, ty, AtomBinop (MinusFloatOp pre, a1, a2), e)
       | Fc_ir.MulFloatOp pre ->
            LetAtom (v, ty, AtomBinop (MulFloatOp pre, a1, a2), e)
       | Fc_ir.DivFloatOp pre ->
            LetAtom (v, ty, AtomBinop (DivFloatOp pre, a1, a2), e)
       | Fc_ir.RemFloatOp pre ->
            LetAtom (v, ty, AtomBinop (RemFloatOp pre, a1, a2), e)
       | Fc_ir.MinFloatOp pre ->
            LetAtom (v, ty, AtomBinop (MinFloatOp pre, a1, a2), e)
       | Fc_ir.MaxFloatOp pre ->
            LetAtom (v, ty, AtomBinop (MaxFloatOp pre, a1, a2), e)
       | Fc_ir.Atan2Op pre ->
            LetAtom (v, ty, AtomBinop (ATan2FloatOp pre, a1, a2), e)
       | Fc_ir.EqFloatOp pre ->
            fir_relop (EqFloatOp pre) v ty a1 a2 e
       | Fc_ir.NeqFloatOp pre ->
            fir_relop (NeqFloatOp pre) v ty a1 a2 e
       | Fc_ir.LtFloatOp pre ->
            fir_relop (LtFloatOp pre) v ty a1 a2 e
       | Fc_ir.LeFloatOp pre ->
            fir_relop (LeFloatOp pre) v ty a1 a2 e
       | Fc_ir.GtFloatOp pre ->
            fir_relop (GtFloatOp pre) v ty a1 a2 e
       | Fc_ir.GeFloatOp pre ->
            fir_relop (GeFloatOp pre) v ty a1 a2 e
       | Fc_ir.EqTagOp
       | Fc_ir.EqPointerOp ->
            fir_relop (EqEqOp TyDelayed) v ty a1 a2 e
       | Fc_ir.NeqPointerOp ->
            fir_relop (NeqEqOp TyDelayed) v ty a1 a2 e
       | Fc_ir.PlusPointerOp
       | Fc_ir.MinusPointerOp
       | Fc_ir.LtPointerOp
       | Fc_ir.LePointerOp
       | Fc_ir.GtPointerOp
       | Fc_ir.GePointerOp ->
            raise (IRException (pos, InternalError "unexpected operator"))
   in
      make_exp loc e

(*
 * Noremal tail call.
 *)
and fir_tailcall_normal tenv pos loc f args =
   let label = new_symbol f in
      make_exp loc (TailCall (label, AtomFun f, fir_atoms args))

(*
 * Migration has to capture the host as a string.
 *)
and fir_tailcall_migrate tenv pos loc f host args =
   let pos = string_pos "fir_tailcall_migrate" pos in
   let host_ptr, host_off =
      match fir_atoms host with
         [host_ptr; host_off] ->
            host_ptr, host_off
       | _ ->
            raise (IRException (pos, InternalError "host has the wrong arity"))
   in
   let index = new_number () in
   let label = new_symbol_string "migrate" in
      make_exp loc (SpecialCall (label, TailSysMigrate (index, host_ptr, host_off, AtomFun f, fir_atoms args)))

and fir_tailcall_atomic tenv pos loc f c args =
   let pos = string_pos "fir_tailcall_atomic" pos in
   let c = fir_atom c in
   let args = fir_atoms args in
   let label = new_symbol_string "atomic" in
      make_exp loc (SpecialCall (label, TailAtomic (AtomFun f, c, args)))

and fir_tailcall_atomic_commit tenv pos loc level f args =
   let pos = string_pos "fir_tailcall_atomic_commit" pos in
   let level = fir_atom level in
   let args = fir_atoms args in
   let label = new_symbol_string "atomic_commit" in
      make_exp loc (SpecialCall (label, TailAtomicCommit (level, AtomFun f, args)))

and fir_tailcall_atomic_rollback tenv pos loc level c =
   let pos = string_pos "fir_tailcall_atomic_rollback" pos in
   let level = fir_atom level in
   let c = fir_atom c in
   let label = new_symbol_string "atomic_rollback" in
      make_exp loc (SpecialCall (label, TailAtomicRollback (level, c)))

(*
 * Tail calls.
 *)
and fir_tailcall_exp tenv pos loc op =
   match op with
      Fc_ir.TailNormal (f, args) ->
         fir_tailcall_normal tenv pos loc f args
    | Fc_ir.TailSysMigrate (f, host, args) ->
         fir_tailcall_migrate tenv pos loc f host args
    | Fc_ir.TailAtomic (f, c, args) ->
         fir_tailcall_atomic tenv pos loc f c args
    | Fc_ir.TailAtomicCommit (level, f, args) ->
         fir_tailcall_atomic_commit tenv pos loc level f args
    | Fc_ir.TailAtomicRollback (level, c) ->
         fir_tailcall_atomic_rollback tenv pos loc level c

(*
 * Relops require coercion.
 *)
and fir_extcall_exp tenv pos loc v ty s b ty' args e =
   let pos = string_pos "fir_op_exp" pos in
   let ty = fir_type tenv pos ty in
   let args = fir_atoms args in
   let e = fir_exp tenv e in
   let s = "fc_" ^ s in
   let ty' = fir_type tenv pos ty' in
      make_exp loc (LetExt (v, ty, s, b, ty', [], args, e))

(*
 * Convert debug.
 *)
and fir_debug_exp tenv pos loc info e =
   let pos = string_pos "fir_debug_exp" pos in
   let info =
      match info with
         Fc_ir.DebugString s ->
            DebugString s
       | Fc_ir.DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) -> v1, fir_type tenv pos ty, v2) vars
            in
               DebugContext (line, vars)
   in
      make_exp loc (Debug (info, fir_exp tenv e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Convert the program.
 *)
let compile prog =
   let { Fc_ir.prog_file = file;
         Fc_ir.prog_globals = globals;
         Fc_ir.prog_decls = decls;
         Fc_ir.prog_export = export;
         Fc_ir.prog_import = import;
         Fc_ir.prog_types = types;
         Fc_ir.prog_frames = frames;
         Fc_ir.prog_funs = funs;
         Fc_ir.prog_body = body
       } = prog
   in

   (* Convert the types *)
   let tenv =
      SymbolTable.mapi (fun v ty ->
            fir_top_type types v ty) types
   in

   (* Import/export *)
   let import =
      SymbolTable.mapi (fun v info ->
            let pos = var_exp_pos v in
            let { Fc_ir.import_name = s;
                  Fc_ir.import_type = ty;
                  Fc_ir.import_info = info
                } = info
            in
               { import_name = s;
                 import_type = fir_type types pos ty;
                 import_info = info
               }) import
   in
   let export =
      SymbolTable.mapi (fun v info ->
            let pos = var_exp_pos v in
            let { Fc_ir.export_name = s;
                  Fc_ir.export_type = ty
                } = info
            in
               { export_name = s;
                 export_type = fir_type types pos ty
               }) export
   in

   (* Check if the main function was defined *)
   let main_sym =
      SymbolTable.fold (fun main_sym v { export_name = s } ->
            if s = "main" then
               Some v
            else
               main_sym) None export
   in

   (* Convert globals *)
   let globals =
      SymbolTable.mapi (fun v (ty, _, init) ->
            let pos = var_exp_pos v in
            let init =
               match init with
                  Fc_ir.InitString (pre, a) ->
                     InitRawData (pre, a)
                | Fc_ir.InitAtom a ->
                     InitAtom (fir_atom a)
                | Fc_ir.InitBlock i ->
                     InitRawData (Rawint.Int8, Array.create i 0)
            in
            let ty = fir_type types pos ty in
               ty, init) globals
   in

   (* Add the symbol for nil *)
   let globals =
      SymbolTable.add globals nil_sym (TyRawData, InitRawData (Rawint.Int8, [||]))
   in

   (* Convert funs *)
   let funs =
      SymbolTable.mapi (fun f (line, gflag, ty, vars, e) ->
            let pos = var_exp_pos f in
            let ty_vars, ty_fun = Fc_ir_type.dest_all_type types pos ty in
            let ty_fun = fir_type types pos ty_fun in
            let e = fir_exp types e in
               line, ty_vars, ty_fun, vars, e) funs
   in

   (* Convert frames *)
   let frames =
      SymbolTable.map (fun fields ->
            let fields =
               SymbolTable.map (fun subfields ->
                     List.map (fun (v, ty) ->
                           let pos = string_pos "frame" (var_exp_pos v) in
                              v, fir_type types pos ty, sizeof_type types pos ty) subfields) fields
            in
               [], fields) frames
   in

   (* Generate initialization fun *)
   let init_string = "init" in
   let init_sym = Symbol.add init_string in
   let init_ty = TyFun ([TyFun ([TyEnum 1], TyEnum 0)], TyEnum 0) in
   let body = fir_exp types body in
   let loc = create_loc (Symbol.add file) 0 0 0 0 in
   let init_def = loc, [], init_ty, [Symbol.add "exit"], body in
   let funs = SymbolTable.add funs init_sym init_def in
   let export = SymbolTable.add export init_sym { export_name = init_string; export_type = init_ty } in
   let prog =
      (* Construct the program *)
      { prog_file = { file_dir = ""; file_name = file; file_class = FileFC };
        prog_import = import;
        prog_export = export;
        prog_types = tenv;
        prog_globals = globals;
        prog_funs = funs;
        prog_names = SymbolTable.empty;
        prog_frames = frames
      }
   in
      standardize_prog (ty_close_prog prog)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
