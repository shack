(*
 * This file performs the initial parse->ast conversion.
 * No type checking is done here; this is just a structural
 * change.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Fc_frontends

open Fc_config
open Fc_ast
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_util
open Fc_ast_type
open Fc_ast_standardize

module Pos = MakePos (struct let name = "Fc_ast_pos" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Keep track of variables in scope,
 * as well as the names of return, continue, and break symbols.
 *)
type venv =
   { venv_debug : SymbolSet.t;
     venv_scope : var SymbolTable.t
   }

let venv_empty =
   { venv_debug = SymbolSet.empty;
     venv_scope = SymbolTable.empty
   }

let venv_add_debug venv v =
   { venv with venv_debug = SymbolSet.add venv.venv_debug v }

let venv_add_scope venv v v' =
   { venv with venv_scope = SymbolTable.add venv.venv_scope v v' }

let venv_lookup_scope venv pos v =
   try SymbolTable.find venv.venv_scope v with
      Not_found ->
         raise (AstException (pos, UnboundVar v))

(*
 * Well-known symbols.
 * We branch to these functions on continue/break.
 *)
let cont_name   = new_symbol_string "continue"
let break_name  = new_symbol_string "break"
let return_name = new_symbol_string "return"
let exn_name    = new_symbol_string "exn"

let exit_sym    = new_symbol_string "exit"

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Make a new TyDelayed.
 *)
let ty_delayed pos =
   TyDelayed (pos, StatusNormal)

(*
 * Exceptions are a well-known type.
 *)
let exn_sym = Symbol.add "exn"

let ty_exn pos =
   TyApply (pos, StatusNormal, exn_sym, [])

let ty_exn_ref pos =
   TyRef (pos, StatusNormal, ty_exn pos)

let ty_exn_ptr pos =
   TyPointer (pos, StatusNormal, ty_exn pos)

(*
 * Build normal numbers.
 *)
let atom_int i pos =
   AtomInt (pos, Rawint.of_int (FCParam.int_precision NormalPrecision) true i)

(*
 * Make a new variable using the atom as a base.
 *)
let new_symbol_atom s = function
   AtomVar (_, v) ->
      new_symbol v
 | _ ->
      new_symbol_string s

let new_symbol_pattern s = function
   Fc_parse_type.VarPattern (_, v, _, _) ->
      new_symbol v
 | _ ->
      new_symbol_string s

(*
 * Get the variable of an atom.
 *)
let var_of_atom loc = function
   AtomVar (_, v) ->
      v
 | a ->
      raise (AstException (loc, StringAtomError ("atom is not a var", a)))

(*
 * Build a debug context from the variables in the set.
 *)
let build_debug_line pos =
   pos

let build_debug venv pos =
   let vars = SymbolSet.to_list venv.venv_debug in
   let vars = List.map (fun v -> (v, ty_delayed pos, v)) vars in
      DebugContext (pos, vars)

(*
 * Get the string.
 *)
let string_of_int_array a =
   let len = Array.length a in
   let s = String.create len in
      for i = 0 to pred len do
         s.[i] <- Char.chr (a.(i) land 0xff)
      done;
      s

(*
 * Make all var types refs.
 *)
let rec make_ref_pattern p =
   match p with
      Fc_parse_type.CharPattern _
    | Fc_parse_type.IntPattern _
    | Fc_parse_type.FloatPattern _
    | Fc_parse_type.StringPattern _
    | Fc_parse_type.VarPattern (_, _, _, None) ->
         p
    | Fc_parse_type.VarPattern (pos, v, label, Some ty) ->
         Fc_parse_type.VarPattern (pos, v, label, Some (Fc_parse_type.TypeRef (pos, StatusNormal, ty)))
    | Fc_parse_type.StructPattern (pos, fields) ->
         let fields =
            List.map (fun (v, p) ->
                  v, make_ref_pattern p) fields
         in
            Fc_parse_type.StructPattern (pos, fields)
    | Fc_parse_type.EnumPattern (pos, v, p) ->
         Fc_parse_type.EnumPattern (pos, v, make_ref_pattern p)
    | Fc_parse_type.AsPattern (pos, p1, p2) ->
         Fc_parse_type.AsPattern (pos, make_ref_pattern p1, make_ref_pattern p2)

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Convert a type expression.
 *)
let rec build_type venv decls ty cont =
   match ty with
      Fc_parse_type.TypeUnit (pos, status, i) ->
         cont venv decls (TyUnit (pos, status, i))
    | Fc_parse_type.TypePoly (pos, status) ->
         cont venv decls (TyDelayed (pos, status))
    | Fc_parse_type.TypeChar (pos, status, pre, signed) ->
         cont venv decls (TyInt (pos, status, pre, signed))
    | Fc_parse_type.TypeInt (pos, status, pre, signed) ->
         cont venv decls (TyInt (pos, status, pre, signed))
    | Fc_parse_type.TypeFloat (pos, status, pre) ->
         cont venv decls (TyFloat (pos, status, pre))
    | Fc_parse_type.TypePointer (pos, status, ty) ->
         build_type venv decls ty (fun venv decls ty ->
               cont venv decls (TyPointer (pos, status, ty)))
    | Fc_parse_type.TypeRef (pos, status, ty) ->
         build_type venv decls ty (fun venv decls ty ->
               cont venv decls (TyRef (pos, status, ty)))
    | Fc_parse_type.TypeArray (pos, status, ty, e1, e2) ->
         build_expr venv decls e1 (fun venv decls a1 ->
         build_expr venv decls e2 (fun venv decls a2 ->
               build_type venv decls ty (fun venv decls ty ->
                     cont venv decls (TyArray (pos, status, ty, a1, a2)))))
    | Fc_parse_type.TypeConfArray (pos, status, ty, v1, v2, v_ty) ->
         build_type venv decls ty (fun venv decls ty ->
         build_type venv decls v_ty (fun venv decls v_ty ->
               cont venv decls (TyConfArray (pos, status, ty, v1, v2, v_ty))))
    | Fc_parse_type.TypeProduct (pos, status, tyl) ->
         build_type_list venv decls tyl (fun venv decls tyl ->
               cont venv decls (TyTuple (pos, status, tyl)))
    | Fc_parse_type.TypeEnum (pos, status, fields) ->
         build_enum_type_fields venv decls (parse_type_pos ty) fields (fun venv decls fields ->
               cont venv decls (TyEnum (pos, status, fields)))
    | Fc_parse_type.TypeUEnum (pos, status, fields) ->
         build_uenum_type_fields venv decls (parse_type_pos ty) fields (fun venv decls fields ->
               cont venv decls (TyUEnum (pos, status, fields)))
    | Fc_parse_type.TypeStruct (pos, status, fields) ->
         build_struct_type_fields venv decls (parse_type_pos ty) fields (fun venv decls fields ->
               cont venv decls (TyStruct (pos, status, fields)))
    | Fc_parse_type.TypeUnion (pos, status, fields) ->
         build_struct_type_fields venv decls (parse_type_pos ty) fields (fun venv decls fields ->
               cont venv decls (TyUnion (pos, status, fields)))
    | Fc_parse_type.TypeFun (pos, status, ty_args, ty_res) ->
         build_type_list venv decls ty_args (fun venv decls ty_args ->
               build_type venv decls ty_res (fun venv decls ty_res ->
                     cont venv decls (TyFun (pos, status, ty_args, ty_res))))
    | Fc_parse_type.TypeVar (pos, status, v) ->
         cont venv decls (TyVar (pos, status, v))
    | Fc_parse_type.TypeLambda (pos, status, vars, ty) ->
         build_type venv decls ty (fun venv decls ty ->
               cont venv decls (TyLambda (pos, status, vars, ty)))
    | Fc_parse_type.TypeApply (pos, status, v, ty_args) ->
         build_type_list venv decls ty_args (fun venv decls ty_args ->
               cont venv decls (TyApply (pos, status, v, ty_args)))
    | Fc_parse_type.TypeElide pos ->
         raise (AstException (parse_type_pos ty, StringError "illegal elision"))

and build_type_opt venv decls ty_opt cont =
   match ty_opt with
      None ->
         cont venv decls None
    | Some ty ->
         build_type venv decls ty (fun venv decls ty ->
               cont venv decls (Some ty))

and build_type_list venv decls tyl cont =
   let rec collect venv decls tyl' = function
      [Fc_parse_type.TypeElide pos] ->
         let ty_char = TyInt (pos, StatusNormal, Rawint.Int8, false) in
         let ty_string = TyPointer (pos, StatusNormal, ty_char) in
         let ty = TyElide (pos, StatusNormal, ty_string :: List.rev tyl') in
            cont venv decls [ty]
    | ty :: tyl ->
         build_type venv decls ty (fun venv decls ty -> collect venv decls (ty :: tyl') tyl)
    | [] ->
         cont venv decls (List.rev tyl')
   in
      collect venv decls [] tyl

(*
 * Enum fields.
 * Collect all the fields.
 * Check that identifiers are all different,
 * and choose values for the unlabeled fields.
 *
 * BUG: this algorithm is quadratic, but should be
 * cheap enough in most cases.
 *)
and build_enum_type_fields venv decls loc fields cont =
   match fields with
      Fields fields ->
         let fields = List.map (build_enum_type_field venv) fields in
            build_enum_type_fields_aux venv decls loc fields cont
    | Label (v, label) ->
         cont venv decls (Label (v, label))

and build_enum_type_fields_aux venv decls loc fields cont =
   (* Collect all the defined values *)
   let rec collect set = function
      (pos, _, Some i) :: fields ->
         if List.mem i set then
            raise (AstException (loc_pos pos, StringInt32Error ("duplicate enumeration value", i)));
         collect (i :: set) fields
    | (_, _, None) :: fields ->
         collect set fields
    | [] ->
         set
   in
   let set = collect [] fields in

   (* Search for a value not in the set *)
   let rec next i =
      if List.mem i set then
         next (Int32.succ i)
      else
         i
   in

   (* Now assign all the fields *)
   let rec assign fields' i = function
      (pos, v, Some j) :: fields ->
         assign ((pos, v, j) :: fields') i fields
    | (pos, v, None) :: fields ->
         let i = next i in
            assign ((pos, v, i) :: fields') (Int32.succ i) fields
    | [] ->
         cont venv decls (Fields (List.rev fields'))
   in
      assign [] Int32.zero fields

and build_enum_type_field venv (pos, v, e_opt) =
   let i_opt =
      match e_opt with
         Some e -> Some (Rawint.to_int32 (build_ast_int venv pos e))
       | None -> None
   in
      pos, v, i_opt

(*
 * Union enum fields.
 * Translate the types.
 *)
and build_uenum_type_fields venv decls loc fields cont =
   match fields with
      Fields (label, fields) ->
         let s1 = Symbol.to_string label ^ "." in
         let rec collect venv decls fields' = function
            (pos, v, ty_opt) :: fields ->
               let s2 = Symbol.to_string v in
               let label = Symbol.add (s1 ^ s2) in
                  (match ty_opt with
                      Some ty ->
                         build_type venv decls ty (fun venv decls ty ->
                               collect venv decls ((pos, v, Some ty, label) :: fields') fields)
                    | None ->
                         collect venv decls ((pos, v, None, label) :: fields') fields)
          | [] ->
               cont venv decls (Fields (label, fields'))
         in
            collect venv decls [] fields
    | Label (v, label) ->
         cont venv decls (Label (v, label))

(*
 * Struct fields.
 *)
and build_struct_type_fields venv decls loc fields cont =
   match fields with
      Fields fields ->
         let rec collect venv decls fields' = function
            field :: fields ->
               build_struct_type_field venv decls loc field (fun venv decls field ->
                     collect venv decls (field :: fields') fields)
          | [] ->
               cont venv decls (Fields fields')
         in
            collect venv decls [] fields
    | Label (v, label) ->
         cont venv decls (Label (v, label))

and build_struct_type_field venv decls loc (pos, v, ty, e_opt) cont =
   let i_opt =
      match e_opt with
         Some e -> Some (Rawint.to_int (build_ast_int venv pos e))
       | None -> None
   in
      build_type venv decls ty (fun venv decls ty ->
            cont venv decls (pos, v, ty, i_opt))

(************************************************************************
 * EVALUATION
 ************************************************************************)

(*
 * To evaluate an expression,
 * build it then constant fold it.
 *)
and build_ast_int venv pos e =
   let v = new_symbol_string ".eval" in
   let _, e =
      build_expr venv [] e (fun venv decls a ->
            decls, Return (pos, v, TyInt (pos, StatusNormal, Rawint.Int32, true), a))
   in
   let e = Fc_ast_const.inline_exp e in
      (* Should have reduced to a simple return expression *)
      match value_of_expr e with
         Some (AtomInt (_, i)) ->
            i
       | _ ->
            raise (AstException (exp_pos e, StringError "non-constant expression"))

(************************************************************************
 * PATTERNS
 ************************************************************************)

(*
 * Convert the pattern.
 * This is pretty much verbatim.
 *)
and build_pattern venv decls p cont =
   let rec build_pattern_aux venv decls vars p cont =
      match p with
         Fc_parse_type.CharPattern (pos, i) ->
            cont venv decls vars (IntPattern (pos, i))
       | Fc_parse_type.IntPattern (pos, i) ->
            cont venv decls vars (IntPattern (pos, i))
       | Fc_parse_type.FloatPattern (pos, x) ->
            cont venv decls vars (FloatPattern (pos, x))
       | Fc_parse_type.StringPattern (pos, pre, s) ->
            cont venv decls vars (StringPattern (pos, pre, s))
       | Fc_parse_type.VarPattern (pos, v, label, ty_opt) ->
            build_type_opt venv decls ty_opt (fun venv decls ty_opt ->
                  cont venv decls ((v, v, label, ty_opt) :: vars) (VarPattern (pos, v, ty_opt)))
       | Fc_parse_type.StructPattern (pos, fields) ->
            build_struct_pattern venv decls vars pos [] fields cont
       | Fc_parse_type.EnumPattern (pos, v, p) ->
            build_pattern_aux venv decls vars p (fun venv decls vars p ->
                  cont venv decls vars (VEnumPattern (pos, v, p)))
       | Fc_parse_type.AsPattern (pos, p1, p2) ->
            build_pattern_aux venv decls vars p1 (fun venv decls vars p1 ->
                  build_pattern_aux venv decls vars p2 (fun venv decls vars p2 ->
                        cont venv decls vars (AsPattern (pos, p1, p2))))

   and build_struct_pattern venv decls vars pos fields' fields cont =
      match fields with
         (v, p) :: fields ->
            build_pattern_aux venv decls vars p (fun venv decls vars p ->
                  build_struct_pattern venv decls vars pos ((v, p) :: fields') fields cont)
       | [] ->
            cont venv decls vars (StructPattern (pos, List.rev fields'))
   in
      build_pattern_aux venv decls [] p cont

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Convert an expression.
 *)
and build_expr venv decls e (cont : venv -> 'a list -> atom -> 'a list * exp) =
   let loc = string_pos "build_expr" (parse_exp_pos e) in
      match e with
         Fc_parse_type.UnitExpr(pos, i1, i2) ->
            cont venv decls (AtomUnit (pos, i1, i2))
       | Fc_parse_type.CharExpr (pos, i) ->
            cont venv decls (AtomInt (pos, i))
       | Fc_parse_type.IntExpr (pos, i) ->
            cont venv decls (AtomInt (pos, i))
       | Fc_parse_type.FloatExpr (pos, x) ->
            cont venv decls (AtomFloat (pos, x))
       | Fc_parse_type.StringExpr (pos, pre, s) ->
            let v = new_symbol_string "string" in
            let decls, e = cont venv decls (AtomVar (pos, v)) in
               decls, LetString (pos, v, ty_string pre pos s, pre, s, e)
       | Fc_parse_type.VarExpr (pos, v, _) ->
            cont venv decls (AtomVar (pos, v))
       | Fc_parse_type.OpExpr (pos, op_class, v, label, args) ->
            build_op_expr venv decls loc pos op_class v label args cont
       | Fc_parse_type.IfExpr (pos, e1, e2, e3) ->
            build_if_expr venv decls loc pos e1 e2 e3 cont
       | Fc_parse_type.ForExpr (pos, e1, e2, e3, e4) ->
            build_for_expr venv decls loc pos e1 e2 e3 e4 cont
       | Fc_parse_type.WhileExpr (pos, e1, e2) ->
            build_while_expr venv decls loc pos e1 e2 cont
       | Fc_parse_type.DoExpr (pos, e1, e2) ->
            build_do_expr venv decls loc pos e1 e2 cont
       | Fc_parse_type.BreakExpr pos ->
            build_break_expr venv decls loc pos cont
       | Fc_parse_type.ContinueExpr pos ->
            build_continue_expr venv decls loc pos cont
       | Fc_parse_type.ReturnExpr (pos, e) ->
            build_return_expr venv decls loc pos e cont
       | Fc_parse_type.RaiseExpr (pos, e) ->
            build_raise_expr venv decls loc pos e cont
       | Fc_parse_type.SwitchExpr (pos, e, cases) ->
            build_switch_expr venv decls loc pos e cases cont
       | Fc_parse_type.TryExpr (pos, e1, e2, e3) ->
            build_try_expr venv decls loc pos e1 e2 e3 cont
       | Fc_parse_type.SeqExpr (pos, el) ->
            build_seq_expr venv decls loc pos el cont
       | Fc_parse_type.PascalExpr _
       | Fc_parse_type.PasqualExpr _ ->
    	    raise (Invalid_argument "unexpected PascalExpr/PasqualExpr")
       | Fc_parse_type.CaseExpr (pos, _)
       | Fc_parse_type.DefaultExpr pos ->
            raise (AstException (loc, StringError "switch case is not in a switch"))
       | Fc_parse_type.GotoExpr (pos, label) ->
            build_goto_expr venv decls loc pos label cont
       | Fc_parse_type.LabelExpr (pos, label) ->
            build_label_expr venv decls loc pos label cont
       | Fc_parse_type.ProjectExpr (pos, e, v) ->
            build_project_expr venv decls loc pos e v cont
       | Fc_parse_type.SizeofExpr (pos, e) ->
            build_sizeof_expr venv decls loc pos e cont
       | Fc_parse_type.SizeofType (pos, ty) ->
            build_sizeof_type_expr venv decls loc pos ty cont
       | Fc_parse_type.CastExpr (pos, ty, e) ->
            build_cast_expr venv decls loc pos ty e cont
       | Fc_parse_type.VarDefs (pos, defs) ->
            build_vardefs_expr venv decls loc pos defs cont
       | Fc_parse_type.TypeDefs (pos, defs) ->
            build_typedefs_expr venv decls loc pos defs cont
       | Fc_parse_type.FunDef (pos, sc, f, label, ty_vars, ty_res, e) ->
            build_fundef_expr venv decls loc pos sc f label ty_vars ty_res e cont
       | Fc_parse_type.WithExpr (pos, e1, e2) ->
	    raise (AstException (loc, StringError "with: Not implemented"))

(*
 * Convert an expression list.
 *)
and build_expr_list venv decls el cont =
   let rec collect venv decls al = function
      e :: el ->
         build_expr venv decls e (fun venv decls a -> collect venv decls (a :: al) el)
    | [] ->
         cont venv decls (List.rev al)
   in
      collect venv decls [] el

(*
 * Operation.
 * 1. If the class is conditional, evaluate the first operand.
 *    If it tests true, evaluate the second, and call the function.
 * 2. If the class is PostOp, it should be unary.
 *    Evaluate the operand, and possibly postpone the assignment.
 *)
and build_op_expr venv decls loc pos op_class v label args cont =
   match op_class, args with
      Fc_parse_type.PreOp, _ ->
         build_op venv decls loc pos v label args cont
    | Fc_parse_type.PostOp, [e] ->
         build_post_op venv decls loc pos v label e cont
    | Fc_parse_type.PostOp, [] ->
         raise (AstException (loc, StringError "postfix operation applied to null argument"))
    | Fc_parse_type.PostOp, _ ->
         raise (AstException (loc, StringError "postfix operation has too many arguments"))

(*
 * Normal operator.
 * Eval all the arguments, then apply it.
 *)
and build_op venv decls loc pos v label args cont =
   build_expr_list venv decls args (fun venv decls args ->
         let v' = new_symbol v in
         let decls, e = cont venv decls (AtomVar (pos, v')) in
         let e = LetApply (pos, v', ty_delayed pos, v, label, args, e) in
            decls, e)

(*
 * Postfix operator.
 * Evaluate the expression,
 * operate on it, but forget the operation.
 *)
and build_post_op venv decls loc pos v label e cont =
   build_expr venv decls e (fun venv decls a ->
         let v' = new_symbol v in
         let decls, e = cont venv decls (AtomVar (pos, v')) in
         let e = LetApply (pos, v', ty_delayed pos, v, label, [a; atom_int 1 pos], e) in
            decls, e)

(*
 * Conditional.
 * Wrap the continuaton in a tail call so we don't
 * have to repeat it.
 *)
and build_if_expr venv decls loc pos e1 e2 e3 cont =
   build_expr venv decls e1 (fun venv decls a1 ->
         (* Build the continuation *)
         let v = new_symbol_atom "if" a1 in
         let f = new_symbol v in
         let decls, f_body = cont venv decls (AtomVar (pos, v)) in
         let debug = pos in
         let f_ty = TyFun (pos, StatusNormal, [ty_delayed pos], ty_delayed pos) in
         let f_call op = TailCall (pos, f, [op]) in

         (* Build the true case *)
         let decls, true_case =
            build_expr venv decls e2 (fun venv decls a2 -> decls, f_call a2)
         in

         (* Build the false case *)
         let decls, false_case =
            match e3 with
               Some e3 ->
                  build_expr venv decls e3 (fun venv decls a3 -> decls, f_call a3)
             | None ->
                  decls, f_call (AtomUnit (pos, 1, 0))
         in

         (* Compare the operand to zero, and continue *)
         let e =
            let v2 = new_symbol v in
               LetTest (pos, v2, ty_delayed pos, a1,
                        IfThenElse (pos, AtomVar (pos, v2), true_case, false_case))
         in

         (* Add the function *)
         let e =
            LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [v], f_body], e)
         in
            decls, e)

(*
 * For-loop.
 * Have to be careful where we go on continue/break.
 * break leaves the loop, continue evals e3 then continues.
 *
 * The code will look like this:
 *    let rec continue () =
 *       let _ = e3 in
 *          loop ()
 *    and break () =
 *       cont ()
 *    and loop () =
 *       let a2 = e2 in
 *          if a2 then
 *             body ()
 *          else
 *             break ()
 *    end body () =
 *       let _ = e4 in
 *          continue ()
 *    in
 *    let _ = e1 in
 *       loop ()
 *)
and build_for_expr venv decls loc pos e1 e2 e3 e4 cont =
   (* Names of the functions *)
   let f_cont = new_symbol cont_name in
   let f_break = new_symbol break_name in
   let venv' = venv_add_scope venv cont_name f_cont in
   let venv' = venv_add_scope venv' break_name f_break in
   let f_loop = new_symbol_string "loop" in
   let f_body = new_symbol_string "body" in

   (* Tailcalls to the parts *)
   let call_cont = TailCall (pos, f_cont, []) in
   let call_break = TailCall (pos, f_break, []) in
   let call_loop = TailCall (pos, f_loop, []) in
   let call_body = TailCall (pos, f_body, []) in

   (* Construct the continue() function body *)
   let f_ty = TyFun (pos, StatusNormal, [], ty_delayed pos) in
   let debug = pos in
   let decls, cont_exp =
      build_expr venv' decls e3 (fun venv decls _ -> decls, call_loop)
   in

   (* Construct the break() function body *)
   let decls, break_exp =
      cont venv decls (AtomUnit (pos, 1, 0))
   in

   (* Construct the body() function *)
   let decls, body_exp =
      build_expr venv' decls e4 (fun venv decls a4 -> decls, call_cont)
   in

   (* Construct the loop function *)
   let decls, loop_exp =
      build_expr venv' decls e2 (fun venv decls a2 ->
            let v = new_symbol_string "test" in
            let e =
               LetTest (pos, v, ty_delayed pos, a2,
                        IfThenElse (pos, AtomVar (pos, v), call_body, call_break))
            in
               decls, e)
   in

   (* In the body, evaluate e1 first, the enter the loop *)
   let decls, e =
      build_expr venv' decls e1 (fun venv decls a1 -> decls, call_loop)
   in

   (* Put all the functions together *)
   let e =
      LetFuns (pos, [pos, f_cont,  f_cont,  debug, VarLocalClass, f_ty, [], cont_exp;
                     pos, f_break, f_break, debug, VarLocalClass, f_ty, [], break_exp;
                     pos, f_loop,  f_loop,  debug, VarLocalClass, f_ty, [], loop_exp;
                     pos, f_body,  f_body,  debug, VarLocalClass, f_ty, [], body_exp],
               e)
   in
      decls, e

(*
 * While-loop.
 * Have to be careful where we go on continue/break.
 * break leaves the loop, continue starts over.
 *
 * The code will look like this:
 *    let rec continue () =
 *       let a1 = e1 in
 *          if a1 then
 *             loop ()
 *          else
 *             break ()
 *    and break () =
 *       cont ()
 *    and body () =
 *       let _ = e2 in
 *          continue ()
 *    in
 *       continue ()
 *)
and build_while_expr venv decls loc pos e1 e2 cont =
   (* Names of the functions *)
   let f_cont = new_symbol cont_name in
   let f_break = new_symbol break_name in
   let venv' = venv_add_scope venv cont_name f_cont in
   let venv' = venv_add_scope venv' break_name f_break in
   let f_body = new_symbol_string "while_body" in

   (* Tailcalls to the parts *)
   let call_cont = TailCall (pos, f_cont, []) in
   let call_break = TailCall (pos, f_break, []) in
   let call_body = TailCall (pos, f_body, []) in

   (* Default function types *)
   let f_ty = TyFun (pos, StatusNormal, [], ty_delayed pos) in
   let debug = pos in

   (* Construct the break() function body *)
   let decls, break_exp =
      cont venv decls (AtomUnit (pos, 1, 0))
   in

   (* Construct the body() function *)
   let decls, body_exp =
      build_expr venv' decls e2 (fun venv decls a2 -> decls, call_cont)
   in

   (* Construct the continue function *)
   let decls, cont_exp =
      build_expr venv' decls e1 (fun venv decls a1 ->
            let v = new_symbol_string "test" in
            let e =
               LetTest (pos, v, ty_delayed pos, a1,
                        IfThenElse (pos, AtomVar (pos, v), call_body, call_break))
            in
               decls, e)
   in

   (* Put all the functions together *)
   let e =
      LetFuns (pos, [pos, f_cont,  f_cont,  debug, VarLocalClass, f_ty, [], cont_exp;
                     pos, f_break, f_break, debug, VarLocalClass, f_ty, [], break_exp;
                     pos, f_body,  f_body,  debug, VarLocalClass, f_ty, [], body_exp],
               call_cont)
   in
      decls, e

(*
 * Do-loop.
 * Have to be careful where we go on continue/break.
 * break leaves the loop, continue starts over.
 *
 * The code will look like this:
 *    let rec continue () =
 *       let a2 = e2 in
 *          if a2 then
 *             body ()
 *          else
 *             break ()
 *    and break () =
 *       cont ()
 *    and body () =
 *       let _ = e1 in
 *          continue ()
 *    in
 *    let _ = e1 in
 *       body ()
 *)
and build_do_expr venv decls loc pos e1 e2 cont =
   (* Names of the functions *)
   let f_cont = new_symbol cont_name in
   let f_break = new_symbol break_name in
   let venv' = venv_add_scope venv cont_name f_cont in
   let venv' = venv_add_scope venv' break_name f_break in
   let f_body = new_symbol_string "body" in

   (* Tailcalls to the parts *)
   let call_cont = TailCall (pos, f_cont, []) in
   let call_break = TailCall (pos, f_break, []) in
   let call_body = TailCall (pos, f_body, []) in

   (* Default function types *)
   let f_ty = TyFun (pos, StatusNormal, [], ty_delayed pos) in
   let debug = build_debug_line pos in

   (* Construct the break() function body *)
   let decls, break_exp =
      cont venv decls (AtomUnit (pos, 1, 0))
   in

   (* Construct the body() function *)
   let decls, body_exp =
      build_expr venv' decls e1 (fun venv decls a1 -> decls, call_cont)
   in

   (* Construct the continue function *)
   let decls, cont_exp =
      build_expr venv' decls e2 (fun venv decls a2 ->
            let v = new_symbol_string "test" in
            let e =
               LetTest (pos, v, ty_delayed pos, a2,
                        IfThenElse (pos, AtomVar (pos, v), call_body, call_break))
            in
               decls, e)
   in

   (* Put all the functions together *)
   let e =
      LetFuns (pos, [pos, f_cont,  f_cont,  debug, VarLocalClass, f_ty, [], cont_exp;
                     pos, f_break, f_break, debug, VarLocalClass, f_ty, [], break_exp;
                     pos, f_body,  f_body,  debug, VarLocalClass, f_ty, [], body_exp],
               call_body)
   in
      decls, e

(*
 * Break function jumps to the nearest break.
 * Note that the continuation is dropped.
 *)
and build_break_expr venv decls loc pos cont =
   let f = new_symbol_string "break_cont" in
   let f_ty = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusNormal, 1)) in
   let debug = build_debug_line pos in
   let decls, e' = cont venv decls (AtomUnit (pos, 1, 0)) in
   let e =
      LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [], e'],
               let break_sym = venv_lookup_scope venv loc break_name in
                  TailCall (pos, break_sym, []))
   in
      decls, e

and build_continue_expr venv decls loc pos cont =
   let f = new_symbol_string "continue_cont" in
   let f_ty = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusNormal, 1)) in
   let debug = build_debug_line pos in
   let decls, e' = cont venv decls (AtomUnit (pos, 1, 0)) in
   let e =
      LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [], e'],
               let cont_sym = venv_lookup_scope venv loc cont_name in
                  TailCall (pos, cont_sym, []))
   in
      decls, e

and build_raise_expr venv decls loc pos e cont =
   let f = new_symbol_string "raise_cont" in
   let f_ty = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusNormal, 1)) in
   let debug = build_debug_line pos in
   let decls, e' = cont venv decls (AtomUnit (pos, 1, 0)) in
   let decls, e =
      build_expr venv decls e (fun venv decls a ->
            decls, Raise (pos, a))
   in
   let e = LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [], e'], e) in
      decls, e

and build_return_expr venv decls loc pos e cont =
   let f = new_symbol_string "return_cont" in
   let f_ty = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusNormal, 1)) in
   let debug = build_debug_line pos in
   let decls, e' = cont venv decls (AtomUnit (pos, 1, 0)) in
   let decls, e =
      build_expr venv decls e (fun venv decls a ->
            let return_sym = venv_lookup_scope venv loc return_name in
               decls, TailCall (pos, return_sym, [a]))
   in
   let e = LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [], e'], e) in
      decls, e

and build_goto_expr venv decls loc pos label cont =
   let f = new_symbol_string "goto_cont" in
   let f_ty = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusNormal, 1)) in
   let debug = build_debug_line pos in
   let decls, e = cont venv decls (AtomUnit (pos, 1, 0)) in
   let e =
      LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [], e],
               TailCall (pos, label, []))
   in
      decls, e

(*
 * Labeled expression begins a new function block.
 *)
and build_label_expr venv decls loc pos label cont =
   let f_ty = TyFun (pos, StatusNormal, [], ty_delayed pos) in
   let debug = build_debug_line pos in
   let decls, body = cont venv decls (AtomUnit (pos, 1, 0)) in
   let fund = pos, label, label, debug, VarLocalClass, f_ty, [], body in
      (label, pos) :: decls, LetFuns (pos, [fund], TailCall (pos, label, []))

(*
 * Sequence expression.
 * Discard all but the last value.
 *)
and build_seq_expr venv decls loc pos el cont =
   let rec collect venv decls a = function
      e :: el ->
         build_expr venv decls e (fun venv decls a ->
               collect venv decls a el)
    | [] ->
         cont venv decls a
   in
      collect venv decls (AtomUnit (pos, 1, 0)) el

(*
 * Switch expression.
 * The C semantics require that each case of the
 * switch flows-through to the next case unless
 * preceded by a break statement.  We'll chain the
 * cases with tail-calls.
 *
 * The first thing we have to do is collect all the
 * cases.  These are embedded in the body of the switch.
 *)
and build_switch_expr venv decls loc pos e1 cases cont =
   (* Add the break *)
   let decls, break_exp =
      cont venv decls (AtomUnit (pos, 1, 0))
   in

   (* Build the cases *)
   let f_break, decls, funs, cases = build_switch venv decls loc pos cases break_exp in

   (* Build the default case *)
   let default_case =
      let v = new_symbol_string "default" in
      let p = VarPattern (pos, v, None) in
      let e = TailCall (pos, f_break, []) in
         p, e
   in
   let cases = cases @ [default_case] in

   (* Now put the match together *)
   (* TEMP JDS
   let ty = TyRef (pos, StatusNormal, TyDelayed (pos, StatusNormal)) in
    *)
   let ty = TyDelayed (pos, StatusNormal) in
   let decls, e =
      build_expr venv decls e1 (fun venv decls a1 ->
            decls, LetFuns (pos, funs, Match (pos, a1, ty, cases)))
   in
      decls, e

and build_switch venv decls loc pos cases break_exp =
   (* Name of the break function *)
   let f_break = new_symbol break_name in
   let venv = venv_add_scope venv break_name f_break in

   (* Default function types *)
   let f_ty = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusConst, 1)) in
   let debug = build_debug_line pos in

   (* BOGUS: we have to fake the passing of the cases *)
   let case_atom = AtomVar (pos, new_symbol_string "build_switch") in
   let make_case p e =
      Match (pos, case_atom, f_ty, [p, e])
   in
   let dest_case = function
      Match (_, _, _, [case]) ->
         case
    | _ ->
         raise (Invalid_argument "Fc_ast_parse.dest_cases")
   in

   (* At the end of each case, there is a tailcall to the next case *)
   let decls, cases, _ =
      List.fold_right (fun (p, el) (decls, cases, f_next) ->
            let f_this = new_symbol_string "case" in
            let decls, e =
               build_pattern venv decls p (fun venv decls vars p ->
                  let decls, e =
                     build_seq_expr venv decls loc pos el (fun venv decls a -> decls, TailCall (pos, f_next, []))
                  in
                  let ty = TyDelayed (pos, StatusNormal) in
                  let e =
                     List.fold_left (fun e (v, v', label, ty_opt) ->
                           let ty =
                              match ty_opt with
                                 Some ty ->
                                    ty
                               | None ->
                                    ty
                           in
                              LetVar (pos, StoreAuto, v, label, VarLocalClass, ty, InitAtom (pos, AtomVar (pos, v')), e)) e vars
                  in
                     decls, make_case p e)
            in
            let p, e = dest_case e in
            let cases = (f_this, p, e) :: cases in
               decls, cases, f_this) cases (decls, [], f_break)
   in

   (* Build the break function *)
   let funs =
      [pos, f_break, f_break, debug, VarLocalClass, f_ty, [], break_exp]
   in

   (* Build the match body *)
   let decls, cases =
      List.fold_left (fun (decls, cases) (f, p, e) ->
            let e = LetFuns (pos, [pos, f, f, debug, VarLocalClass, f_ty, [], e], TailCall (pos, f, [])) in
            let cases = (p, e) :: cases in
            let decls = (f, pos) :: decls in
               decls, cases) (decls, []) cases
   in
   let cases = List.rev cases in
      f_break, decls, funs, cases

(*
 * Match with a pattern.
 *)
and build_match param venv decls loc pos sc gflag a_opt p ty cont =
   match param, a_opt, p with
      true, InitAtom (_, a), Fc_parse_type.VarPattern (_, v, label, _) ->
         let venv = venv_add_debug venv label in
         let decls, e = cont venv decls in
         let e = LetAtom (pos, v, ty, a, e) in
            decls, e
    | _, _, Fc_parse_type.VarPattern (_, v, label, _) ->
         (* Optimize the normal case *)
         let venv = venv_add_debug venv label in
         let decls, e = cont venv decls in
         let e = LetVar (pos, sc, v, label, gflag, ty, a_opt, e) in
            decls, e
(*
 * BUG: Adam put this stuff here, but it really belongs
 * in air_copy, and it breaks the pattern renaming because
 * we _have_ to have the LetVar...
 *
            (match param, FrontEnd.parameter_copy () with
                true, CopyNone ->
                   decls, e
              | true, CopyAggregates ->
                   (match is_aggregate_type ty with
                       true ->
                          let ty = change_conformant_array ty in
                          let e = LetVar (pos, sc, v, label, gflag, ty, a_opt, e) in
                             decls, e
                     | false ->
                          decls, e)
              | true, CopyAll ->
                   let ty = change_conformant_array ty in
                   let e = LetVar (pos, sc, v, label, gflag, ty, a_opt, e) in
                      decls, e
              | _ ->
 *)
    | _, InitAtom _, _
    | _, InitArray _, _ ->
         (* Insert a switch *)
         build_pattern venv decls p (fun venv decls vars p ->
               let ty' = TyDelayed (pos, StatusNormal) in
               let venv =
                  List.fold_left (fun venv (v, _, _, _) ->
                        venv_add_debug venv v) venv vars
               in
               let decls, e = cont venv decls in
               let e =
                  List.fold_left (fun e (v, v', label, ty_opt) ->
                        let ty' =
                           match ty_opt with
                              Some ty ->
                                 ty
                            | None ->
                                 ty'
                        in
                        let e =
                           if param then
                              LetAtom (pos, v, ty', AtomVar (pos, v'), e)
                           else
                              LetVar (pos, sc, v, label, gflag, ty', InitAtom (pos, AtomVar (pos, v')), e)
                        in
                           e) e vars
               in
               let v = new_symbol_string "tmp" in
               let ty_match = TyRef (pos, StatusNormal, ty') in
               let e = Match (pos, AtomVar (pos, v), ty_match, [p, e]) in
               let e =
                  match a_opt with
                     InitAtom (_, a) ->
                        LetAtom (pos, v, ty, a, e)
                   | _ ->
                        LetVar (pos, StoreAuto, v, v, VarLocalClass, ty', a_opt, e)
               in
                  decls, e)
    | _ ->
         (* Other patterns require initializer *)
         raise (AstException (loc, StringError "pattern requires initializer"))

(*
 * Match arguments.
 *)
and build_matches param venv decls loc pos sc gflag vars patts tyl cont =
   match vars, patts, tyl with
      v :: vars, p :: patts, ty :: tyl ->
         build_match param venv decls loc pos sc gflag (InitAtom (pos, AtomVar (pos, v))) p ty (fun venv decls ->
               build_matches param venv decls loc pos sc gflag vars patts tyl cont)
    | [], [], [] ->
         cont venv decls
    | _ ->
         raise (AstException (loc, StringError "build_matches: bad match"))

(*
 * Exception handling.
 * The exception handler is a function that
 * takes the exception, and performs a match.
 *
 * The code looks like this:
 *
 * let finally exn =
 *    let _ = e2 in
 *       if exn = 0 then
 *          cont ()
 *       else
 *          raise exn
 * and break () =
 *    finally 0
 * in
 *    try
 *       let _ = e2 in
 *          finally 0
 *    with
 *       v ->
 *          match v with
 *          ...cases...
 *          default:
 *             finally v
 *)
and build_try_expr venv decls loc pos e1 cases e2 cont =
   (* Names of the functions *)
   let f_finally = new_symbol_string "finally" in

   (* Exception var *)
   let v_exn = new_symbol_string "exn" in

   (* Build the finally block *)
   let v_final = new_symbol_string "exn" in
   let ty_final = TyFun (pos, StatusNormal, [ty_exn_ptr pos], ty_delayed pos) in
   let debug = build_debug_line pos in
   let decls, final_exp =
      let v = new_symbol_string "test" in
      let decls, e = cont venv decls (AtomUnit (pos, 1, 0)) in
      let e =
         LetTest (pos, v, ty_delayed pos, AtomVar (pos, v_final),
         IfThenElse (pos, AtomVar (pos, v), Raise (pos, AtomVar (pos, v_final)), e))
      in
         match e2 with
            Some e2 ->
               build_expr venv decls e2 (fun venv decls a2 -> decls, e)
          | None ->
               decls, e
   in

   (* Build the break block *)
   let nil = AtomNil (pos, ty_delayed pos) in
   let break_exp = TailCall (pos, f_finally, [nil]) in

   (* Build the switch *)
   let _, decls, funs, cases = build_switch venv decls loc pos cases break_exp in

   (* Add a default case *)
   let v = new_symbol_string "_" in
   let v_exn_addr = new_symbol v_exn in
   let default_pattern = VarPattern (pos, v, None) in
   let default_exp =
      LetAddrOf (pos, v_exn_addr, ty_exn_ptr pos, AtomVar (pos, v_exn),
      TailCall (pos, f_finally, [AtomVar (pos, v_exn_addr)]))
   in
   let default = default_pattern, default_exp in
   let cases = cases @ [default] in

   (* Build the initial expression *)
   let decls, try_exp = build_expr venv decls e1 (fun venv decls a1 -> decls, break_exp) in

   (* Add the final function *)
   let funs = (pos, f_finally, f_finally, debug, VarLocalClass, ty_final, [v_final], final_exp) :: funs in
   let ty_match = TyDelayed (pos, StatusNormal) in
   let e =
      LetFuns (pos, funs,
      Try (pos, try_exp, v_exn,
      Match (pos, AtomVar (pos, v_exn), ty_match, cases)))
   in
      decls, e

(*
 * Sizeof of value.
 * We actually convert the expression.
 * However, the expression will not be evaluated,
 * but its type will be taken.
 *)
and build_sizeof_expr venv decls loc pos e cont =
   let v = new_symbol_string "sizeof" in
   let f = new_symbol_string "sizeof_fun" in
   let v_f_ty = new_symbol_string "sizeof_ty" in
   let sizeof_ty = TyVar (pos, StatusNormal, v_f_ty) in
   let f_ty = TyFun (pos, StatusNormal, [], sizeof_ty) in
   let debug = build_debug_line pos in
   let decls, f_body =
      build_expr venv decls e (fun venv decls a ->
            decls, Return (pos, f, ty_delayed pos, a))
   in
   let decls, e2 = cont venv decls (AtomVar (pos, v)) in
   let e =
      LetFuns (pos, [pos, f, f, debug, VarGlobalClass, f_ty, [], f_body],
      LetSizeof (pos, v, ty_delayed pos, sizeof_ty, e2))
   in
      decls, e

and build_sizeof_type_expr venv decls loc pos ty cont =
   build_type venv decls ty (fun venv decls ty ->
         let v = new_symbol_string "sizeof" in
         let decls, e = cont venv decls (AtomVar (pos, v)) in
         let e = LetSizeof (pos, v, ty_delayed pos, ty, e) in
            decls, e)

(*
 * Coercions.
 *)
and build_cast_expr venv decls loc pos ty e cont =
   build_type venv decls ty (fun venv decls ty ->
         build_expr venv decls e (fun venv decls a ->
               let v = new_symbol_atom "cast" a in
               let decls, e = cont venv decls (AtomVar (pos, v)) in
                  decls, LetCoerce (pos, v, ty, a, e)))

(*
 * Type definitions.
 * 11/06/01: FIXED: Recursive type definitions are preserved.
 *)
and build_typedefs_expr venv decls loc pos defs cont =
   let ty_list = List.map (fun (pos, v, label, ty) -> ty) defs in
   let v_label_list = List.map (fun (pos, v, label, ty) -> v, label) defs in
      build_type_list venv decls ty_list (fun venv decls ty_list ->
         let decls, e = cont venv decls (AtomUnit (pos, 1, 0)) in
         let defs = List.map2 (fun ty (v, label) -> v, label, ty) ty_list v_label_list in
            decls, LetTypes (pos, List.rev defs, e))

(*
 * Projection.
 *)
and build_project_expr venv decls loc pos e v cont =
   build_expr venv decls e (fun venv decls a ->
         let v1 = new_symbol v in
         let decls, e = cont venv decls (AtomVar (pos, v1)) in
         let e = LetProject (pos, v1, ty_delayed pos, a, v, e) in
            decls, e)

(*
 * Variable declarations.
 * Note that this also contains function declarations.
 *)
and build_vardefs_expr venv decls loc pos defs cont =
   let rec build venv decls = function
      (pos, sc, p, ty, e) :: defs ->
         (match ty, p, e with
             Fc_parse_type.TypeFun (_, _, ty_args, ty_res),
             Fc_parse_type.VarPattern (_, v, label, _),
             Fc_parse_type.InitExpr (_, Fc_parse_type.StringExpr (_, _, s)) ->
                (* This is an external declaration *)
                build_extcall decls pos sc v label ty_args ty_res s defs
           | Fc_parse_type.TypeFun _,
             Fc_parse_type.VarPattern (_, v, label, _),
             _ ->
                (* This is a function declaration *)
                build_fundecl decls pos sc v label ty defs
           | _ ->
                (* Normal declaration *)
                build_vardecl decls pos sc p ty e defs)
    | [] ->
         cont venv decls (AtomUnit (pos, 1, 0))

   and build_extcall decls pos sc v label ty_args ty_res s defs =
      let s, b =
         let len = Array.length s in
            if len > 3 && s.(0) = Char.code 'g' && s.(1) = Char.code 'c' && s.(2) = Char.code ':' then
               Array.sub s 3 (len - 3), true
            else
               s, false
      in
      build_type_list venv decls ty_args (fun venv decls ty_args ->
            build_type venv decls ty_res (fun venv decls ty_res ->
                  let vars = List.map (fun _ -> new_symbol_string "v") ty_args in
                  let args = List.map (fun v -> AtomVar (pos, v)) vars in
                  let ty = TyFun (pos, StatusNormal, ty_args, ty_res) in
                  let v_res = new_symbol_string "v_res" in
                  let gflag =
                     if sc = StoreStatic then
                        VarStaticClass
                     else
                        VarGlobalClass
                  in
                  let decls, e = build venv decls defs in
                  let s = string_of_int_array s in
                  let debug = build_debug_line pos in
                  let v' = new_symbol v in
                  let e' = LetExtCall (pos, v', ty_res, s, b, ty, args, Return (pos, v, ty_res, AtomVar (pos, v'))) in
                  let e' = LetFuns (pos, [pos, v, label, debug, gflag, ty, vars, e'], e) in
                     decls, e'))

   and build_fundecl decls pos sc v label ty defs =
      build_type venv decls ty (fun venv decls ty ->
            let gflag =
               if sc = StoreStatic then
                  VarStaticClass
               else
                  VarGlobalClass
            in
            let decls, e = build venv decls defs in
               decls, LetFunDecl (pos, sc, v, label, gflag, ty, e))

   and build_vardecl decls pos sc p ty e defs =
      let gflag =
         match sc with
            StoreStatic ->
               VarStaticClass
          | StoreAuto
          | StoreRegister
          | StoreExtern ->
               VarGlobalClass
      in
         build_type venv decls ty (fun venv decls ty ->
         build_init venv decls loc e (fun venv decls a_opt ->
         build_match false venv decls loc pos sc gflag a_opt p ty (fun venv decls ->
               build venv decls defs)))
   in
      build venv decls defs

and build_init venv decls loc init cont =
   match init with
      Fc_parse_type.InitNone ->
         cont venv decls InitNone
    | Fc_parse_type.InitExpr (pos, e) ->
         build_expr venv decls e (fun venv decls a -> cont venv decls (InitAtom (pos, a)))
    | Fc_parse_type.InitArray (pos, fields) ->
         let rec collect venv decls fields' = function
            (v_opt, init) :: fields ->
               build_init venv decls loc init (fun venv decls a ->
                     collect venv decls ((v_opt, a) :: fields') fields)
          | [] ->
               cont venv decls (InitArray (pos, List.rev fields'))
         in
            collect venv decls [] fields

(*
 * Function definition.
 *
 * We have to set up a well-known return function.
 *)
and build_fundef_expr venv decls loc pos sc f label ty_vars ty_res e cont =
   (* Build the return function *)
   let f_return = new_symbol return_name in
   let venv' = venv_add_scope venv return_name f_return in
   let v_return = new_symbol_string "v" in
   let ty_return = TyFun (pos, StatusNormal, [ty_delayed pos], ty_delayed pos) in
   let return_exp = Return (pos, f, ty_delayed pos, AtomVar (pos, v_return)) in
   let call_return a = TailCall (pos, f_return, [a]) in

   (* Build the function types *)
   build_type venv' decls ty_res (fun venv' decls ty_res ->
   build_params venv' decls loc pos ty_vars (fun venv' decls patts ty_vars ->
         (* Build the body *)
         let vars = List.map (new_symbol_pattern "arg") patts in
         let decls, body_exp =
            build_matches true venv' decls loc pos StoreAuto VarLocalClass vars patts ty_vars (fun venv' decls ->
                  build_expr venv' decls e (fun venv decls a -> decls, call_return a))
         in

         (* Introduce the explicit argument copy *)
         let args = List.map new_symbol vars in
         let body_exp =
            Mc_list_util.fold_left3 (fun e v1 v2 ty ->
                  let a = InitAtom (pos, AtomVar (pos, v2)) in
                     LetVar (pos, StoreAuto, v1, v1, VarLocalClass, ty, a, e)) body_exp vars args ty_vars
         in

         (* Build the fun *)
         let ty_f = TyFun (pos, StatusNormal, ty_vars, ty_res) in
         let debug = build_debug_line pos in
         let gflag =
            if sc <> StoreStatic then
               VarGlobalClass
            else
               VarStaticClass
         in
         let funr = pos, f_return, f_return, debug, VarLocalClass, ty_return, [v_return], return_exp in
         let body_exp = LetFuns (pos, [funr], body_exp) in
         let fund = pos, f, label, debug, gflag, ty_f, args, body_exp in
         let decls, e = cont venv decls (AtomUnit (pos, 1, 0)) in
         let e = LetFuns (pos, [fund], e) in
            decls, e))

(*
 * Collect the formal parameters.
 *)
and build_params venv decls loc pos ty_vars cont =
   let rec collect venv decls vars' ty_vars' = function
      [_, _, Fc_parse_type.TypeElide pos] ->
         let fields =
            List.fold_left (fun fields p ->
                  (None, make_ref_pattern p) :: fields) [] vars'
         in

         (* Add an extra string param *)
         let ty_char = Fc_parse_type.TypeChar (pos, StatusNormal, Rawint.Int8, false) in
         let ty_string = Fc_parse_type.TypePointer (pos, StatusNormal, ty_char) in
         let v_s = new_symbol_string "stdargs_format" in
         let fields = (None, Fc_parse_type.VarPattern (pos, v_s, v_s, Some ty_string)) :: fields in

         (* Add the string type *)
         let ty_char = TyInt (pos, StatusNormal, Rawint.Int8, false) in
         let ty_string = TyPointer (pos, StatusNormal, ty_char) in
         let ty_vars' = ty_string :: List.rev ty_vars' in
         let ty = TyElide (pos, StatusNormal, ty_vars') in

         (* Now match against the simple pattern *)
         let p = Fc_parse_type.StructPattern (pos, fields) in
            cont venv decls [p] [ty]
    | (pos, p, ty) :: ty_vars ->
         build_type venv decls ty (fun venv decls ty ->
               collect venv decls (p :: vars') (ty :: ty_vars') ty_vars)
    | [] ->
         cont venv decls (List.rev vars') (List.rev ty_vars')
   in
      collect venv decls [] [] ty_vars

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Create declarations for all the labels.
 *)
let rec close_labels e = function
   (label, pos) :: labels ->
      let ty_f = TyFun (pos, StatusNormal, [], TyUnit (pos, StatusNormal, 1)) in
      let e = LetFunDecl (pos, StoreStatic, label, label, VarStaticClass, ty_f, e) in
         close_labels e labels
 | [] ->
      e

(*
 * Drop the variable environment.
 *)
let exn_sym = Symbol.add "exn"

let build_exp e =
   let pos = Fc_parse_util.loc_of_expr e in
   let zero = AtomUnit (pos, 1, 0) in
   let decls, e = build_expr venv_empty [] e (fun venv decls _ -> decls, Return (pos, exit_sym, ty_delayed pos, zero)) in
   let e = close_labels e decls in
   let e =
      LetTypes (pos, [exn_sym, exn_sym, TyUEnum (pos, StatusNormal, Fields (exn_sym, []))], e)
   in
      standardize_expr e

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
