(*
 * Identify and update all identifiers that correspnd to TyEnum.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Location

open Fc_config

open Fc_ast
open Fc_ast_exn
open Fc_ast_pos

module Pos = MakePos (struct let name = "Fc_ast_enum" end)
open Pos

(************************************************************************
 * VARIABLE ENVIRONMENT
 ************************************************************************)

(*
 * Catch UEnum applications.
 *)
let apply_sym = Symbol.add "()"

(*
 * Info for enum identifiers.
 *)
type id_info =
   IdEnum of loc * ty * int32
 | IdUEnum of loc * ty * label

type venv = id_info SymbolTable.t

let venv_empty = SymbolTable.empty

let venv_add = SymbolTable.add

let venv_remove = SymbolTable.remove

let venv_remove_vars venv vars =
   List.fold_left venv_remove venv vars

let venv_lookup = SymbolTable.find

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Walk through all the types and collect identifiers from enums.
 *)
let rec enum_type venv ty =
   match ty with
      TyDelayed _
    | TyUnit _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _ ->
         venv
    | TyArray (_, _, ty, _, _)
    | TyPointer (_, _, ty)
    | TyRef (_, _, ty)
    | TyAll (_, _, _, ty)
    | TyLambda (_, _, _, ty) ->
         enum_type venv ty
    | TyConfArray (_, _, ty, _, _, v_ty) ->
         let venv = enum_type venv ty in
            enum_type venv v_ty
    | TyTuple (_, _, tyl)
    | TyApply (_, _, _, tyl)
    | TyElide (_, _, tyl) ->
         enum_types venv tyl
    | TyStruct (_, _, fields)
    | TyUnion (_, _, fields) ->
         enum_struct_fields venv fields
    | TyFun (_, _, tyl, ty) ->
         enum_type (enum_types venv tyl) ty
    | TyEnum (_, _, fields) ->
         enum_enum_fields venv ty fields
    | TyUEnum (_, _, fields) ->
         enum_uenum_fields venv ty fields

and enum_types venv tyl =
   List.fold_left enum_type venv tyl

and enum_struct_fields venv fields =
   match fields with
      Fields fields ->
         enum_struct_fields_aux venv fields
    | Label _ ->
         venv

and enum_struct_fields_aux venv = function
   (_, _, ty, _) :: fields ->
      enum_struct_fields_aux (enum_type venv ty) fields
 | [] ->
      venv

and enum_enum_fields venv ty fields =
   match fields with
      Fields fields ->
         enum_enum_fields_aux venv ty fields
    | Label _ ->
         venv

and enum_enum_fields_aux venv ty = function
   (pos, v, i) :: fields ->
      enum_enum_fields_aux (venv_add venv v (IdEnum (pos, ty, i))) ty fields
 | [] ->
      venv

and enum_uenum_fields venv ty fields =
   match fields with
      Fields (_, fields) ->
         enum_uenum_fields_aux venv ty fields
    | Label _ ->
         venv

and enum_uenum_fields_aux venv ty = function
   (pos, v, _, l) :: fields ->
      enum_uenum_fields_aux (venv_add venv v (IdUEnum (pos, ty, l))) ty fields
 | [] ->
      venv

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Collect enum identifiers.
 *)
and enum_atom venv a cont =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _ ->
         cont venv a
    | AtomVar (pos, v) ->
         (try
             match venv_lookup venv v with
                IdEnum (_, ty, i) ->
                   cont venv (AtomEnum (pos, ty, i))
              | IdUEnum (pos, ty, l) ->
                   (* JDS
                   let v' = new_symbol v in
                   let v'' = new_symbol v in
                   let ty' = TyPointer (pos, StatusNormal, ty) in
                   let vstar = Symbol.add "*" in
                   let e = cont venv (AtomVar (pos, v'')) in
                      LetAlloc (pos, v', ty', v, [],
                      LetApply (pos, v'', ty, vstar, vstar, [AtomVar (pos, v')], e))
                    *)
                   let v' = new_symbol v in
                   let ty' = TyPointer (pos, StatusNormal, ty) in
                   let e = cont venv (AtomVar (pos, v')) in
                      LetAlloc (pos, v', ty', v, [], e)
          with
             Not_found ->
                cont venv a)
    | AtomNil (_, ty)
    | AtomEnum (_, ty, _) ->
         cont (enum_type venv ty) a

let enum_atoms venv args cont =
   let rec collect venv args' = function
      a :: args ->
         enum_atom venv a (fun venv a ->
               collect venv (a :: args') args)
    | [] ->
         cont venv (List.rev args')
   in
      collect venv [] args

let rec enum_init venv init cont =
   match init with
      InitNone ->
         cont venv InitNone
    | InitAtom (pos, a) ->
         enum_atom venv a (fun venv a ->
               cont venv (InitAtom (pos, a)))
    | InitArray (pos, fields) ->
         let rec collect venv fields' = function
            (v_opt, init) :: fields ->
               enum_init venv init (fun venv init ->
                     collect venv ((v_opt, init) :: fields') fields)
          | [] ->
               cont venv (InitArray (pos, List.rev fields'))
         in
            collect venv [] fields

(************************************************************************
 * PATTERNS
 ************************************************************************)

(*
 * Translate the identifiers.
 *)
let rec enum_pattern venv loc p =
   match p with
      IntPattern _
    | FloatPattern _
    | StringPattern _
    | EnumPattern _
    | UEnumPattern _ ->
         venv, p
    | VarPattern (pos, v, _) ->
         let p =
            try
               match venv_lookup venv v with
                  IdEnum (_, ty, i) ->
                     EnumPattern (pos, ty, i)
                | IdUEnum (_, ty, l) ->
                     UEnumPattern (pos, ty, v, None)
            with
               Not_found ->
                  p
         in
            venv, p
    | StructPattern (pos, fields) ->
         let venv, fields =
            List.fold_left (fun (venv, fields) (v_opt, p) ->
                  let venv, p = enum_pattern venv loc p in
                  let fields = (v_opt, p) :: fields in
                     venv, fields) (venv, []) fields
         in
            venv, StructPattern (pos, List.rev fields)
    | VEnumPattern (pos, v, p) ->
         (try
             match venv_lookup venv v with
                IdUEnum (_, ty, l) ->
                   let venv, p = enum_pattern venv loc p in
                      venv, UEnumPattern (pos, ty, v, Some p)
              | IdEnum _ ->
                   raise (AstException (loc, StringVarError ("not a union enum", v)))
          with
             Not_found ->
                raise (AstException (loc, StringVarError ("not a union enum", v))))
    | AsPattern (pos, p1, p2) ->
         let venv, p1 = enum_pattern venv loc p1 in
         let venv, p2 = enum_pattern venv loc p2 in
            venv, AsPattern (pos, p1, p2)

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

let rec enum_exp venv e =
   let loc = string_pos "enum_exp" (exp_pos e) in
      match e with
         LetTypes (pos, tyl, e) ->
            let venv =
               List.fold_left (fun venv (_, _, ty) ->
                     enum_type venv ty) venv tyl
            in
               LetTypes (pos, tyl, enum_exp venv e)
       | LetFuns (pos, funs, e) ->
            let venv =
               List.fold_left (fun venv (_, f, _, _, _, _, _, _) ->
                     venv_remove venv f) venv funs
            in
            let funs =
               List.map (fun (pos, f, label, line, gflag, ty, vars, e) ->
                     let venv = enum_type venv ty in
                     let venv = venv_remove_vars venv vars in
                        pos, f, label, line, gflag, ty, vars, enum_exp venv e) funs
            in
            let e = enum_exp venv e in
               LetFuns (pos, funs, e)
       | LetFunDecl (pos, sc, v, label, gflag, ty, e) ->
            let venv = venv_remove venv v in
            let venv = enum_type venv ty in
               LetFunDecl (pos, sc, v, label, gflag, ty, enum_exp venv e)
       | LetVar (pos, sc, v, label, gflag, ty, init, e) ->
            let venv = enum_type venv ty in
               enum_init venv init (fun venv init ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetVar (pos, sc, v, label, gflag, ty, init, e))
       | LetCoerce (pos, v, ty, a, e) ->
            let venv = enum_type venv ty in
               enum_atom venv a (fun venv a ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetCoerce (pos, v, ty, a, e))
       | LetAtom (pos, v, ty, a, e) ->
            let venv = enum_type venv ty in
               enum_atom venv a (fun venv a ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetAtom (pos, v, ty, a, e))
       | LetUnop (pos, v, ty, op, a, e) ->
            let venv = enum_type venv ty in
            enum_atom venv a (fun venv a ->
                  let venv = venv_remove venv v in
                  let e = enum_exp venv e in
                     LetUnop (pos, v, ty, op, a, e))
       | LetBinop (pos, v, ty, op, a1, a2, e) ->
            let venv = enum_type venv ty in
               enum_atom venv a1 (fun venv a1 ->
               enum_atom venv a2 (fun venv a2 ->
                     let e = enum_exp venv e in
                        LetBinop (pos, v, ty, op, a1, a2, e)))
       | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
            let venv = enum_type venv ty in
            let venv = enum_type venv ty' in
               enum_atoms venv args (fun venv args ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetExtCall (pos, v, ty, s, b, ty', args, e))

       | LetApply (pos, v, ty, f, label, AtomVar (pos', f') :: args, e)
         when Symbol.eq f apply_sym ->
            let args' = List.tl args in
            (try
                match venv_lookup venv f' with
                   IdUEnum (pos, ty, label) ->
                      enum_atoms venv args (fun venv args ->
                            (* JDS
                            let venv = venv_remove venv v in
                            let v' = new_symbol v in
                            let ty' = TyPointer (pos, StatusNormal, ty) in
                            let vstar = Symbol.add "*" in
                            let e = enum_exp venv e in
                               LetAlloc (pos, v', ty', f', args',
                               LetApply (pos, v, ty, vstar, vstar, [AtomVar (pos, v')], e)))
                             *)
                            let venv = venv_remove venv v in
                            let ty' = TyPointer (pos, StatusNormal, ty) in
                            let e = enum_exp venv e in
                               LetAlloc (pos, v, ty', f', args', e))
                 | IdEnum _ ->
                      raise (AstException (loc, StringError "bogus enumeration application"))
             with
                Not_found ->
                   let venv = enum_type venv ty in
                      enum_atoms venv args (fun venv args ->
                            let venv = venv_remove venv v in
                            let e = enum_exp venv e in
                               LetApply (pos, v, ty, f, label, AtomVar (pos', f') :: args, e)))

       | LetApply (pos, v, ty, f, label, args, e) ->
            let venv = enum_type venv ty in
               enum_atoms venv args (fun venv args ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetApply (pos, v, ty, f, label, args, e))
       | LetTest (pos, v, ty, a, e) ->
            let venv = enum_type venv ty in
               enum_atom venv a (fun venv a ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetTest (pos, v, ty, a, e))
       | LetSizeof (pos, v, ty1, ty2, e) ->
            let venv = enum_type venv ty1 in
            let venv = enum_type venv ty2 in
            let venv = venv_remove venv v in
            let e = enum_exp venv e in
               LetSizeof (pos, v, ty1, ty2, e)
       | TailCall (pos, f, args) ->
            enum_atoms venv args (fun venv args ->
                  TailCall (pos, f, args))
       | Return (pos, v, ty, a) ->
            let venv = enum_type venv ty in
               enum_atom venv a (fun venv a ->
                     Return (pos, v, ty, a))
       | Raise (pos, a) ->
            enum_atom venv a (fun venv a ->
                  Raise (pos, a))
       | LetString (pos, v, ty, pre, s, e) ->
            let venv = enum_type venv ty in
            let venv = venv_remove venv v in
            let e = enum_exp venv e in
               LetString (pos, v, ty, pre, s, e)
       | LetAlloc (pos, v, ty, label, args, e) ->
            let venv = enum_type venv ty in
               enum_atoms venv args (fun venv args ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetAlloc (pos, v, ty, label, args, e))
       | LetMalloc (pos, v, a, e) ->
            enum_atom venv a (fun venv a ->
                  let venv = venv_remove venv v in
                  let e = enum_exp venv e in
                     LetMalloc (pos, v, a, e))
       | IfThenElse (pos, a, e1, e2) ->
            enum_atom venv a (fun venv a ->
                  let e1 = enum_exp venv e1 in
                  let e2 = enum_exp venv e2 in
                     IfThenElse (pos, a, e1, e2))
       | Match (pos, a, ty, cases) ->
            let venv = enum_type venv ty in
               enum_atom venv a (fun venv a ->
                  let cases =
                     List.map (fun (p, e) ->
                           let venv, p = enum_pattern venv loc p in
                           let e = enum_exp venv e in
                              p, e) cases
                  in
                     Match (pos, a, ty, cases))
       | Try (pos, e1, v, e2) ->
            let e1 = enum_exp venv e1 in
            let venv = venv_remove venv v in
            let e2 = enum_exp venv e2 in
               Try (pos, e1, v, e2)
       | LetProject (pos, v1, ty, v2, v3, e) ->
            let venv = enum_type venv ty in
            let venv = venv_remove venv v1 in
            let e = enum_exp venv e in
               LetProject (pos, v1, ty, v2, v3, e)
       | LetSubscript (pos, v1, ty, v2, ty2, v3, e) ->
            let venv = enum_type venv ty in
            let venv = enum_type venv ty2 in
            let e = enum_exp venv e in
               LetSubscript (pos, v1, ty, v2, ty2, v3, e)
       | Set (pos, a1, ty, a2, e) ->
            let venv = enum_type venv ty in
               enum_atom venv a1 (fun venv a1 ->
               enum_atom venv a2 (fun venv a2 ->
                     let e = enum_exp venv e in
                        Set (pos, a1, ty, a2, e)))
       | LetAddrOf (pos, v, ty, a, e) ->
            let venv = enum_type venv ty in
               enum_atom venv a (fun venv a ->
                     let venv = venv_remove venv v in
                     let e = enum_exp venv e in
                        LetAddrOf (pos, v, ty, a, e))
       | Debug (pos, info, e) ->
            Debug (pos, info, enum_exp venv e)

let enum_exp e =
   enum_exp venv_empty e

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
