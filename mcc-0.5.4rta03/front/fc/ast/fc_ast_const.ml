(*
 * Basic inline expansion.
 * We inline functions only if they
 * are called exactly once.  We do some simple
 * intra-procedural constant folding.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_config

open Fc_ast
open Fc_ast_env
open Fc_ast_exn
open Fc_ast_pos

module Pos = MakePos (struct let name = "Fc_ast_const" end)
open Pos

(************************************************************************
 * AVAILABLE EXPRESSIONS
 ************************************************************************)

(*
 * Simple expressions that a variable van refer to.
 *)
type avail = atom

type aenv = avail SymbolTable.t

let aenv_empty = SymbolTable.empty

let rec aenv_lookup aenv pos v =
   try
      match SymbolTable.find aenv v with
         AtomVar (pos, v) ->
            aenv_lookup aenv pos v
       | a ->
            a
   with
      Not_found ->
         AtomVar (pos, v)

let aenv_remove aenv v =
   SymbolTable.remove aenv v

let aenv_add aenv v a =
   SymbolTable.add aenv v a

(************************************************************************
 * INLINE UTILITIES
 ************************************************************************)

(*
 * Compare rawints.
 *)
let rawint_bool flag =
   Rawint.of_int (FCParam.int_precision NormalPrecision) true (if flag then 1 else 0)

let atom_bool pos flag =
   AtomInt (pos, rawint_bool flag)

let rawint_compare cmp i j =
   rawint_bool (cmp (Rawint.compare i j))

let rawfloat_compare cmp x y =
   rawint_bool (cmp (Rawfloat.compare x y))

(************************************************************************
 * SECOND PASS
 ************************************************************************)

(*
 * Inline a var.
 *)
let inline_var aenv pos v =
   match aenv_lookup aenv pos v with
      AtomVar (pos, v) -> v
    | _ -> v

(*
 * Inline an atom.
 *)
let inline_atom aenv a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomNil _
    | AtomEnum _ ->
         a
    | AtomVar (pos, v) ->
         aenv_lookup aenv pos v

let inline_atom_opt aenv = function
   Some a -> Some (inline_atom aenv a)
 | None -> None

let inline_atoms aenv args =
   List.map (inline_atom aenv) args

let rec inline_init aenv init =
   match init with
      InitNone ->
         init
    | InitAtom (pos, a) ->
         InitAtom (pos, inline_atom aenv a)
    | InitArray (pos, fields) ->
         let fields =
            List.map (fun (v_opt, init) ->
                  v_opt, inline_init aenv init) fields
         in
            InitArray (pos, fields)

(*
 * Perform inlining.
 *)
let rec inline_exp aenv e =
   let loc = string_pos "inline_exp" (exp_pos e) in
      match e with
         LetTypes (pos, types, e) ->
            LetTypes (pos, types, inline_exp aenv e)
       | LetFuns (pos, funs, e) ->
            inline_funs_exp aenv loc pos funs e
       | LetFunDecl (pos, sc, f, label, gflag, ty, e) ->
            LetFunDecl (pos, sc, f, label, gflag, ty, inline_exp aenv e)
       | LetAtom (pos, v, ty, a, e) ->
            inline_atom_exp aenv loc pos v ty a e
       | LetTest (pos, v, ty, a, e) ->
            inline_test_exp aenv loc pos v ty a e
       | LetCoerce (pos, v, ty, a, e) ->
            inline_coerce_exp aenv loc pos v ty a e
       | LetUnop (pos, v, ty, op, a, e) ->
            inline_unop_exp aenv loc pos v ty op a e
       | LetBinop (pos, v, ty, op, a1, a2, e) ->
            inline_binop_exp aenv loc pos v ty op a1 a2 e
       | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
            inline_extcall_exp aenv loc pos v ty s b ty' args e
       | TailCall (pos, f, args) ->
            inline_tailcall_exp aenv loc pos f args
       | LetAlloc (pos, v, ty, label, args, e) ->
            LetAlloc (pos, v, ty, label, inline_atoms aenv args, inline_exp aenv e)
       | LetMalloc (pos, v, a, e) ->
            LetMalloc (pos, v, inline_atom aenv a, inline_exp aenv e)
       | IfThenElse (pos, a, e1, e2) ->
            inline_if_exp aenv loc pos a e1 e2
       | LetSubscript (pos, v1, ty, v2, ty2, a3, e) ->
            LetSubscript (pos, v1, ty, inline_var aenv pos v2, ty2, inline_atom aenv a3, inline_exp aenv e)
       | LetVar (pos, sc, v, label, gflag, ty, init, e) ->
            LetVar (pos, sc, v, label, gflag, ty, inline_init aenv init, inline_exp aenv e)
       | LetAddrOf (pos, v, ty, a, e) ->
            LetAddrOf (pos, v, ty, inline_atom aenv a, inline_exp aenv e)
       | Set (pos, a1, ty, a2, e) ->
            Set (pos, inline_atom aenv a1, ty, inline_atom aenv a2, inline_exp aenv e)
       | Debug (pos, info, e) ->
            inline_debug_exp aenv loc pos info e
       | Raise (pos, a) ->
            Raise (pos, inline_atom aenv a)
       | Return (pos, f, ty, a) ->
            Return (pos, f, ty, inline_atom aenv a)
       | Try (pos, e1, v, e2) ->
            Try (pos, inline_exp aenv e1, v, inline_exp aenv e2)
       | LetApply (pos, v, ty, f, label, args, e) ->
            inline_apply_exp aenv loc pos v ty f label args e
       | LetString (pos, v, ty, pre, s, e) ->
            LetString (pos, v, ty, pre, s, inline_exp aenv e)
       | LetProject (pos, v1, ty, a2, v3, e) ->
            LetProject (pos, v1, ty, inline_atom aenv a2, v3, inline_exp aenv e)
       | LetSizeof (pos, v, ty1, ty2, e) ->
            LetSizeof (pos, v, ty1, ty2, inline_exp aenv e)
       | Match (pos, a, ty, cases) ->
            inline_match_exp aenv loc pos a ty cases

(*
 * Add the function definitions.
 * Add the functions to the inline environment.
 *)
and inline_funs_exp aenv loc pos funs e =
   let funs =
      List.fold_left (fun funs fund ->
            let pos, f, label, line, gflag, ty, vars, e = fund in
            let e = inline_exp aenv e in
            let fund = pos, f, label, line, gflag, ty, vars, e in
               fund :: funs) [] funs
   in
   let e = inline_exp aenv e in
      LetFuns (pos, List.rev funs, e)

(*
 * Operators.
 * Try to perform constant folding.
 *)
and inline_atom_exp aenv loc pos v ty a e =
   let a = inline_atom aenv a in
   let aenv = aenv_add aenv v a in
   let e = inline_exp aenv e in
      LetAtom (pos, v, ty, a, e)

(*
 * Coercions have to be preserved.
 *)
and inline_coerce_exp aenv loc pos v ty a e =
   let a = inline_atom aenv a in
   let e = inline_exp aenv e in
      LetCoerce (pos, v, ty, a, e)

and inline_test_exp aenv loc pos v ty a e =
   let a = inline_atom aenv a in
   let aenv =
      match a with
         AtomInt (pos, i) ->
            aenv_add aenv v (atom_bool pos (not (Rawint.is_zero i)))
       | AtomFloat (pos, x) ->
            aenv_add aenv v (atom_bool pos (not (Rawfloat.is_zero x)))
       | AtomNil _ ->
            aenv_add aenv v (atom_bool pos false)
       | _ ->
            aenv
   in
   let e = inline_exp aenv e in
      LetTest (pos, v, ty, a, e)

and inline_unop_exp aenv loc pos v ty op a e =
   let a = inline_atom aenv a in
   let a' =
      match op, a with
         UMinusIntOp _, AtomInt (pos, i) ->
            Some (AtomInt (pos, Rawint.neg i))
       | NotIntOp _, AtomInt (pos, i) ->
            Some (AtomInt (pos, Rawint.lognot i))

       | UMinusFloatOp _, AtomFloat (pos, x) ->
            Some (AtomFloat (pos, Rawfloat.neg x))
       | AbsFloatOp _, AtomFloat (pos, x) ->
            Some (AtomFloat (pos, Rawfloat.abs x))

       | _ ->
            (* None of the others get inlined *)
            None
   in
   let aenv =
      match a' with
         Some a ->
            aenv_add aenv v a
       | None ->
            aenv
   in
   let e = inline_exp aenv e in
      LetUnop (pos, v, ty, op, a, e)

and inline_binop_exp aenv loc pos v ty op a1 a2 e =
   let a1 = inline_atom aenv a1 in
   let a2 = inline_atom aenv a2 in
   let a' =
      match op, a1, a2 with
         PlusIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.add i j))
       | MinusIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.sub i j))
       | MulIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.mul i j))
       | DivIntOp _, AtomInt (pos, i), AtomInt (_, j) when not (Rawint.is_zero j) ->
            Some (AtomInt (pos, Rawint.div i j))
       | RemIntOp _, AtomInt (pos, i), AtomInt (_, j) when not (Rawint.is_zero j) ->
            Some (AtomInt (pos, Rawint.rem i j))
       | SlIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.shift_left i j))
       | SrIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.shift_right i j))
       | AndIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.logand i j))
       | OrIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.logor i j))
       | XorIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.logxor i j))
       | MinIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.min i j))
       | MaxIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, Rawint.max i j))
       | EqIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, rawint_compare (fun i -> i = 0) i j))
       | NeqIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, rawint_compare (fun i -> i <> 0) i j))
       | LtIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, rawint_compare (fun i -> i < 0) i j))
       | LeIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, rawint_compare (fun i -> i <= 0) i j))
       | GeIntOp _, AtomInt (pos, i), AtomInt (_, j) ->
            Some (AtomInt (pos, rawint_compare (fun i -> i >= 0) i j))

       | PlusFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomFloat (pos, Rawfloat.add x y))
       | MinusFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomFloat (pos, Rawfloat.sub x y))
       | MulFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomFloat (pos, Rawfloat.mul x y))
       | DivFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) when not (Rawfloat.is_zero y) ->
            Some (AtomFloat (pos, Rawfloat.div x y))
       | RemFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) when not (Rawfloat.is_zero y) ->
            Some (AtomFloat (pos, Rawfloat.rem x y))
       | MinFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomFloat (pos, Rawfloat.min x y))
       | MaxFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomFloat (pos, Rawfloat.max x y))
       | EqFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomInt (pos, rawfloat_compare (fun i -> i = 0) x y))
       | NeqFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomInt (pos, rawfloat_compare (fun i -> i <> 0) x y))
       | LtFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomInt (pos, rawfloat_compare (fun i -> i < 0) x y))
       | LeFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomInt (pos, rawfloat_compare (fun i -> i <= 0) x y))
       | GtFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomInt (pos, rawfloat_compare (fun i -> i > 0) x y))
       | GeFloatOp _, AtomFloat (pos, x), AtomFloat (_, y) ->
            Some (AtomInt (pos, rawfloat_compare (fun i -> i >= 0) x y))

       | EqPointerOp, AtomNil _, AtomNil _ ->
            Some (AtomInt (pos, rawint_bool true))
       | NeqPointerOp, AtomNil _, AtomNil _ ->
            Some (AtomInt (pos, rawint_bool false))

       | _ ->
            (* None of the others get inlined *)
            None
   in

   (* If IdOp, then add to avail list *)
   let aenv =
      match a' with
         Some a ->
            aenv_add aenv v a
       | _ ->
            aenv
   in
   let e = inline_exp aenv e in
      LetBinop (pos, v, ty, op, a1, a2, e)

(*
 * Conditional.
 *)
and inline_if_exp aenv loc pos a e1 e2 =
   let a = inline_atom aenv a in
      match a with
         AtomInt (_, i) ->
            if Rawint.is_zero i then
               inline_exp aenv e2
            else
               inline_exp aenv e1
       | AtomFloat (_, x) ->
            if Rawfloat.is_zero x then
               inline_exp aenv e2
            else
               inline_exp aenv e1
       | AtomNil _ ->
            inline_exp aenv e2
       | _ ->
            let e1 = inline_exp aenv e1 in
            let e2 = inline_exp aenv e2 in
               IfThenElse (pos, a, e1, e2)

(*
 * External function call.
 *)
and inline_extcall_exp aenv loc pos v ty s b ty' args e =
   let args = inline_atoms aenv args in
   let e = inline_exp aenv e in
      LetExtCall (pos, v, ty, s, b, ty', args, e)

(*
 * Inline the tailcall.
 * If the function is available, substitute and recurse.
 *)
and inline_tailcall_exp aenv loc pos f args =
   let args = inline_atoms aenv args in
      TailCall (pos, inline_var aenv pos f, args)

(*
 * Inline an application.
 *)
and inline_apply_exp aenv loc pos v ty f label args e =
   let args = inline_atoms aenv args in
   let e = inline_exp aenv e in
      LetApply (pos, v, ty, inline_var aenv pos f, label, args, e)

(*
 * Debugging.
 *)
and inline_debug_exp aenv loc pos info e =
   let e = inline_exp aenv e in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) ->
                     v1, ty, inline_var aenv pos v2) vars
            in
               DebugContext (line, vars)
   in
      Debug (pos, info, e)

(*
 * Pattern matching.
 *)
and inline_match_exp aenv loc pos a ty cases =
   let a = inline_atom aenv a in
   let cases =
      List.map (fun (p, e) -> p, inline_exp aenv e) cases
   in
      Match (pos, a, ty, cases)

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Inline the entire program.
 *)
let inline_exp exp =
   inline_exp aenv_empty exp

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
