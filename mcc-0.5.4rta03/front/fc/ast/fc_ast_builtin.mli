(*
 * Overloading table.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Fc_config

open Fc_ast
open Fc_ast_env
open Fc_ast_pos

(*
 * Function environment.
 *)
type fenv
type fun_entry

(*
 * Table operations.
 *)
val fenv_empty : fenv
val fenv_add : fenv -> var -> var -> ty -> fenv
val fenv_lookup : fenv -> pos -> loc -> var -> ty list -> ty -> (ty * fun_entry) list
val fenv_apply : fun_entry -> tenv -> pos -> loc -> var -> ty list -> ty -> atom list -> exp -> exp
val fenv_stdargs : fun_entry -> fun_entry

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
