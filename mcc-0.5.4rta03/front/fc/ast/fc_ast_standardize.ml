(*
 * Rename all the non-global variables in a program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Symbol

open Fc_config

open Fc_ast
open Fc_ast_exn

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = var SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

let rec env_add_vars env vars vars' =
   match vars, vars' with
      v :: vars, v' :: vars' ->
         env_add_vars (env_add env v v') vars vars'
    | [], [] ->
         env
    | _ ->
         raise (Invalid_argument "Fc_ir_standardize.env_add_vars")

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

(*
 * Check is a variable is defined.
 *)
let env_mem = SymbolTable.mem

(*
 * Variable environment keeps track of fields.
 *)
type venv =
   { venv_vars : env;
     venv_fields : env
   }

let venv_empty =
   { venv_vars = env_empty;
     venv_fields = env_empty
   }

let venv_add_var venv v v' =
   { venv with venv_vars = env_add venv.venv_vars v v' }

let venv_add_vars venv vars vars' =
   { venv with venv_vars = env_add_vars venv.venv_vars vars vars' }

let venv_add_field venv v v' =
   { venv with venv_fields = env_add venv.venv_fields v v' }

let venv_lookup_var venv v =
   try SymbolTable.find venv.venv_vars v with
      Not_found ->
         env_lookup venv.venv_fields v

let venv_lookup_field venv v =
   env_lookup venv.venv_fields v

(*
 * Type environment has two parts, one for
 * ids, and one for vars.
 *)
type tenv =
   { tenv_types : env;
     tenv_vars : env
   }

(*
 * Type operations.
 * fvars are the free type vars in a function.
 *)
let tenv_empty =
   { tenv_types = env_empty;
     tenv_vars = env_empty
   }

let tenv_mem tenv v =
   SymbolTable.mem tenv.tenv_types v

let tenv_add_type tenv v v' =
   { tenv with tenv_types = env_add tenv.tenv_types v v' }

let tenv_add_var tenv v v' =
   { tenv with tenv_vars = env_add tenv.tenv_vars v v' }

let tenv_add_vars tenv vars vars' =
   { tenv with tenv_vars = env_add_vars tenv.tenv_vars vars vars' }

let tenv_lookup_var tenv v =
   try
      let v' = SymbolTable.find tenv.tenv_vars v in
         tenv, v'
   with
      Not_found ->
         tenv, v

let tenv_lookup_type tenv v =
   env_lookup tenv.tenv_types v

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv venv ty =
   match ty with
      TyUnit _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyDelayed _ ->
         tenv, venv, ty
    | TyArray (pos, sc, ty, a1, a2) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
         let tenv, venv, a1 = standardize_atom tenv venv a1 in
         let tenv, venv, a2 = standardize_atom tenv venv a2 in
            tenv, venv, TyArray (pos, sc, ty, a1, a2)
    | TyConfArray (pos, sc, ty, v1, v2, v_ty) ->
	 let v1' = venv_lookup_var venv v1 in
	 let v2' = venv_lookup_var venv v2 in
         let tenv, venv, ty = standardize_type tenv venv ty in
         let tenv, venv, v_ty = standardize_type tenv venv v_ty in
            tenv, venv, TyConfArray (pos, sc, ty, v1', v2', v_ty)
    | TyPointer (pos, sc, ty) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TyPointer (pos, sc, ty)
    | TyRef (pos, sc, ty) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TyRef (pos, sc, ty)
    | TyStruct (pos, sc, fields) ->
         let tenv, venv, fields = standardize_struct_fields tenv venv fields in
            tenv, venv, TyStruct (pos, sc, fields)
    | TyUnion (pos, sc, fields) ->
         let tenv, venv, fields = standardize_struct_fields tenv venv fields in
            tenv, venv, TyUnion (pos, sc, fields)
    | TyEnum (pos, sc, fields) ->
         let tenv, venv, fields = standardize_enum_fields tenv venv fields in
            tenv, venv, TyEnum (pos, sc, fields)
    | TyUEnum (pos, sc, fields) ->
         let tenv, venv, fields = standardize_uenum_fields tenv venv fields in
            tenv, venv, TyUEnum (pos, sc, fields)
    | TyTuple (pos, sc, tyl) ->
         let tenv, venv, tyl = standardize_type_list tenv venv tyl in
            tenv, venv, TyTuple (pos, sc, tyl)
    | TyFun (pos, sc, ty_vars, ty_res) ->
         let tenv, venv, ty_vars = standardize_type_list tenv venv ty_vars in
         let tenv, venv, ty_res = standardize_type tenv venv ty_res in
            tenv, venv, TyFun (pos, sc, ty_vars, ty_res)
    | TyVar (pos, sc, v) ->
         let tenv, v = tenv_lookup_var tenv v in
            tenv, venv, TyVar (pos, sc, v)
    | TyLambda (pos, sc, vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TyLambda (pos, sc, vars', ty)
    | TyAll (pos, sc, vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TyAll (pos, sc, vars', ty)
    | TyApply (pos, sc, v, tyl) ->
         let tenv, venv, tyl = standardize_type_list tenv venv tyl in
            tenv, venv, TyApply (pos, sc, tenv_lookup_type tenv v, tyl)
    | TyElide (pos, sc, tyl) ->
         let tenv, venv, tyl = standardize_type_list tenv venv tyl in
            tenv, venv, TyElide (pos, sc, tyl)

and standardize_type_opt tenv venv = function
   Some ty ->
      let tenv, venv, ty = standardize_type tenv venv ty in
         tenv, venv, Some ty
 | None ->
      tenv, venv, None

and standardize_type_list tenv venv tyl =
   let tenv, venv, tyl =
      List.fold_left (fun (tenv, venv, ty_vars) ty ->
            let tenv, venv, ty = standardize_type tenv venv ty in
            let ty_vars = ty :: ty_vars in
               tenv, venv, ty_vars) (tenv, venv, []) tyl
   in
      tenv, venv, List.rev tyl

and standardize_struct_fields tenv venv fields =
   match fields with
      Fields fields ->
         let tenv, fields =
            List.fold_left (fun (tenv, fields) (pos, v, ty, i_opt) ->
                  let tenv, _, ty = standardize_type tenv venv ty in
                  let fields = (pos, v, ty, i_opt) :: fields in
                     tenv, fields) (tenv, []) fields
         in
            tenv, venv, Fields fields
    | Label (v, label) ->
         tenv, venv, Label (v, label)

and standardize_enum_fields tenv venv fields =
   match fields with
      Fields fields ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, i) ->
                  let v' = new_symbol v in
                  let venv = venv_add_field venv v v' in
                  let fields = (pos, v', i) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

and standardize_uenum_fields tenv venv fields =
   match fields with
      Fields (label, fields) ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, ty_opt, label) ->
                  let tenv, venv, ty_opt =
                     match ty_opt with
                        Some ty ->
                           let tenv, venv, ty = standardize_type tenv venv ty in
                              tenv, venv, Some ty
                      | None ->
                           tenv, venv, None
                  in
                  let v' = new_symbol v in
                  let venv = venv_add_field venv v v' in
                  let fields = (pos, v', ty_opt, label) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (label, List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

(*
 * Standardize a function type.
 * We use this function instead of standardize_type,
 * because here we add extra variables resulting
 * from conformant array parameters.
 *)
and standardize_function_type tenv venv ty =
   match ty with
      TyUnit _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyDelayed _ ->
         tenv, venv, ty
    | TyArray (pos, sc, ty, a1, a2) ->
         let tenv, venv, ty = standardize_function_type tenv venv ty in
         let tenv, venv, a1 = standardize_atom tenv venv a1 in
         let tenv, venv, a2 = standardize_atom tenv venv a2 in
            tenv, venv, TyArray (pos, sc, ty, a1, a2)
    | TyConfArray (pos, sc, ty, v1, v2, v_ty) ->
	 let v1' = new_symbol v1 in
	 let v2' = new_symbol v2 in
	 let venv = venv_add_var venv v1 v1' in
	 let venv = venv_add_var venv v2 v2' in
         let tenv, venv, ty = standardize_function_type tenv venv ty in
         let tenv, venv, v_ty = standardize_function_type tenv venv v_ty in
            tenv, venv, TyConfArray (pos, sc, ty, v1', v2', v_ty)
    | TyPointer (pos, sc, ty) ->
         let tenv, venv, ty = standardize_function_type tenv venv ty in
            tenv, venv, TyPointer (pos, sc, ty)
    | TyRef (pos, sc, ty) ->
         let tenv, venv, ty = standardize_function_type tenv venv ty in
            tenv, venv, TyRef (pos, sc, ty)
    | TyStruct (pos, sc, fields) ->
         let tenv, venv, fields = standardize_function_struct_fields tenv venv fields in
            tenv, venv, TyStruct (pos, sc, fields)
    | TyUnion (pos, sc, fields) ->
         let tenv, venv, fields = standardize_function_struct_fields tenv venv fields in
            tenv, venv, TyUnion (pos, sc, fields)
    | TyEnum (pos, sc, fields) ->
         let tenv, venv, fields = standardize_function_enum_fields tenv venv fields in
            tenv, venv, TyEnum (pos, sc, fields)
    | TyUEnum (pos, sc, fields) ->
         let tenv, venv, fields = standardize_function_uenum_fields tenv venv fields in
            tenv, venv, TyUEnum (pos, sc, fields)
    | TyTuple (pos, sc, tyl) ->
         let tenv, venv, tyl = standardize_function_type_list tenv venv tyl in
            tenv, venv, TyTuple (pos, sc, tyl)
    | TyFun (pos, sc, ty_vars, ty_res) ->
         let tenv, venv, ty_vars = standardize_function_type_list tenv venv ty_vars in
         let tenv, venv, ty_res = standardize_function_type tenv venv ty_res in
            tenv, venv, TyFun (pos, sc, ty_vars, ty_res)
    | TyVar (pos, sc, v) ->
         let tenv, v = tenv_lookup_var tenv v in
            tenv, venv, TyVar (pos, sc, v)
    | TyLambda (pos, sc, vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, venv, ty = standardize_function_type tenv venv ty in
            tenv, venv, TyLambda (pos, sc, vars', ty)
    | TyAll (pos, sc, vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, venv, ty = standardize_function_type tenv venv ty in
            tenv, venv, TyAll (pos, sc, vars', ty)
    | TyApply (pos, sc, v, tyl) ->
         let tenv, venv, tyl = standardize_function_type_list tenv venv tyl in
            tenv, venv, TyApply (pos, sc, tenv_lookup_type tenv v, tyl)
    | TyElide (pos, sc, tyl) ->
         let tenv, venv, tyl = standardize_function_type_list tenv venv tyl in
            tenv, venv, TyElide (pos, sc, tyl)

and standardize_function_type_list tenv venv tyl =
   let tenv, venv, tyl =
      List.fold_left (fun (tenv, venv, ty_vars) ty ->
            let tenv, venv, ty = standardize_function_type tenv venv ty in
            let ty_vars = ty :: ty_vars in
               tenv, venv, ty_vars) (tenv, venv, []) tyl
   in
      tenv, venv, List.rev tyl

and standardize_function_struct_fields tenv venv fields =
   match fields with
      Fields fields ->
         let tenv, fields =
            List.fold_left (fun (tenv, fields) (pos, v, ty, i_opt) ->
                  let tenv, _, ty = standardize_function_type tenv venv ty in
                  let fields = (pos, v, ty, i_opt) :: fields in
                     tenv, fields) (tenv, []) fields
         in
            tenv, venv, Fields (List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

and standardize_function_enum_fields tenv venv fields =
   match fields with
      Fields fields ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, i) ->
                  let v' = new_symbol v in
                  let venv = venv_add_field venv v v' in
                  let fields = (pos, v', i) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

and standardize_function_uenum_fields tenv venv fields =
   match fields with
      Fields (label, fields) ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, ty_opt, label) ->
                  let tenv, venv, ty_opt =
                     match ty_opt with
                        Some ty ->
                           let tenv, venv, ty = standardize_function_type tenv venv ty in
                              tenv, venv, Some ty
                      | None ->
                           tenv, venv, None
                  in
                  let v' = new_symbol v in
                  let venv = venv_add_field venv v v' in
                  let fields = (pos, v', ty_opt, label) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (label, List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

(*
 * Rename the vars in an atom.
 *)
and standardize_atom tenv venv atom =
   match atom with
      AtomUnit _
    | AtomInt _
    | AtomFloat _ ->
         tenv, venv, atom
    | AtomNil (pos, ty) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, AtomNil (pos, ty)
    | AtomVar (pos, v) ->
         tenv, venv, AtomVar (pos, venv_lookup_var venv v)
    | AtomEnum (pos, ty, i) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, AtomEnum (pos, ty, i)

let standardize_atoms tenv venv atoms =
   let tenv, venv, atoms =
      List.fold_left (fun (tenv, venv, atoms) a ->
            let tenv, venv, a = standardize_atom tenv venv a in
            let atoms = a :: atoms in
               tenv, venv, atoms) (tenv, venv, []) atoms
   in
      tenv, venv, List.rev atoms

let rec standardize_init tenv venv init =
   match init with
      InitNone ->
         tenv, venv, init
    | InitAtom (pos, a) ->
         let tenv, venv, a = standardize_atom tenv venv a in
            tenv, venv, InitAtom (pos, a)
    | InitArray (pos, fields) ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (v_opt, init) ->
                  let tenv, venv, init = standardize_init tenv venv init in
                     tenv, venv, (v_opt, init) :: fields) (tenv, venv, []) fields
         in
            tenv, venv, InitArray (pos, List.rev fields)

(*
 * Rename all the vars in the expr.
 *)
let rec standardize_expr tenv venv e =
   match e with
      LetTypes (pos, types, e) ->
         standardize_types_expr tenv venv pos types e
    | LetFuns (pos, funs, e) ->
         standardize_funs_expr tenv venv pos funs e
    | LetFunDecl (pos, sc, v, label, gflag, ty, e) ->
         standardize_fundecl_expr tenv venv pos sc v label gflag ty e
    | LetTest (pos, v, ty, a, e) ->
         standardize_test_expr tenv venv pos v ty a e
    | LetCoerce (pos, v, ty, a, e) ->
         standardize_coerce_expr tenv venv pos v ty a e
    | LetVar (pos, sc, v, label, gflag, ty, a, e) ->
         standardize_var_expr tenv venv pos sc v label gflag ty a e
    | LetAtom (pos, v, ty, a, el) ->
         standardize_atom_expr tenv venv pos v ty a el
    | LetUnop (pos, v, ty, op, a, el) ->
         standardize_unop_expr tenv venv pos v ty op a el
    | LetBinop (pos, v, ty, op, a1, a2, el) ->
         standardize_binop_expr tenv venv pos v ty op a1 a2 el
    | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
         standardize_extcall_expr tenv venv pos v ty s b ty' args e
    | LetApply (pos, v, ty, f, label, args, el) ->
         standardize_apply_expr tenv venv pos v ty f label args el
    | TailCall (pos, f, args) ->
         standardize_tailcall_expr tenv venv pos f args
    | Return (pos, f, ty, a) ->
         standardize_return_expr tenv venv pos f ty a
    | LetString (pos, v, ty, pre, s, e) ->
         standardize_string_expr tenv venv pos v ty pre s e
    | IfThenElse (pos, a, e1, e2) ->
         standardize_if_expr tenv venv pos a e1 e2
    | LetMalloc (pos, v, a, e) ->
         standardize_malloc_expr tenv venv pos v a e
    | LetAlloc (pos, v, ty, label, args, e) ->
         standardize_alloc_expr tenv venv pos v ty label args e
    | Debug (pos, info, e) ->
         standardize_debug_expr tenv venv pos info e
    | LetSizeof (pos, v, ty1, ty2, e) ->
         standardize_sizeof_type_expr tenv venv pos v ty1 ty2 e
    | Raise (pos, a) ->
         standardize_raise_expr tenv venv pos a
    | Try (pos, e1, v, e2) ->
         standardize_try_expr tenv venv pos e1 v e2
    | Match (pos, a, ty, cases) ->
         standardize_match_expr tenv venv pos a ty cases
    | LetProject (pos, v, ty, v1, v2, e) ->
         standardize_project_expr tenv venv pos v ty v1 v2 e
    | LetSubscript (pos, v, ty, v1, ty2, a2, e) ->
         standardize_subscript_expr tenv venv pos v ty v1 ty2 a2 e
    | Set (pos, a1, ty, a2, e) ->
         standardize_set_expr tenv venv pos a1 ty a2 e
    | LetAddrOf (pos, v, ty, a, e) ->
         standardize_addr_of_expr tenv venv pos v ty a e

(*
 * Documentation string.
 *)
and standardize_debug_vars tenv venv info =
   List.fold_left (fun (tenv, info) (v1, ty, v2) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
         let v2 = venv_lookup_var venv v2 in
         let info = (v1, ty, v2) :: info in
            tenv, info) (tenv, []) info

and standardize_debug_expr tenv venv pos info e =
   let tenv, e = standardize_expr tenv venv e in
   let tenv, info =
      match info with
         DebugString _ ->
            tenv, info
       | DebugContext (line, vars) ->
            let tenv, vars = standardize_debug_vars tenv venv vars in
               tenv, DebugContext (line, vars)
   in
      tenv, Debug (pos, info, e)

(*
 * Rename all the types.
 *)
and standardize_tydefs tenv venv types =
   (* Rename all the type vars *)
   let tenv, types =
      List.fold_left (fun (tenv, types) (v, label, tydef) ->
            (* Delete all incomplete typedefs that have been defined *)
            let v' = new_symbol v in
            let tenv = tenv_add_type tenv v v' in
               tenv, (v', label, tydef) :: types) (tenv, []) types
   in
      (* Rename all the types *)
      List.fold_left (fun (tenv, venv, types) (v, label, tydef) ->
            let tenv, venv, tydef = standardize_type tenv venv tydef in
            let types = (v, label, tydef) :: types in
               tenv, venv, types) (tenv, venv, []) types

and standardize_types_expr tenv venv pos types e =
   (* Rename the types *)
   let tenv, venv, types = standardize_tydefs tenv venv types in

   (* Rename the body *)
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetTypes (pos, types, e)

(*
 * Standardize a fun.
 * Global funs begin a new type var scope.
 *)
and standardize_fun_expr tenv (venv, fundefs) (pos, f, label, line, gflag, f_ty, vars, body) =
   (* Rename the function *)
   let f' = venv_lookup_var venv f in

   (* Rename the type vars *)
   let tenv' = tenv in

   (* Rename the vars *)
   let venv', vars' =
      List.fold_left (fun (venv, vars') v ->
            let v' = new_symbol v in
            let venv = venv_add_var venv v v' in
               venv, v' :: vars') (venv, []) vars
   in
   let vars' = List.rev vars' in

   (* Rename the body *)
   let tenv', venv', f_ty = standardize_function_type tenv venv' f_ty in
   let venv = venv' in
   let tenv', body' = standardize_expr tenv' venv' body in
      venv, (pos, f', label, line, gflag, f_ty, vars', body') :: fundefs

and standardize_funs tenv venv funs =
   (* Rename the fun names *)
   let venv =
      List.fold_left (fun venv (_, f, _, _, gflag, _, _, _) ->
    	    if env_mem venv.venv_vars f then
		venv
	    else
        	let f' = new_symbol f in
            	    venv_add_var venv f f') venv funs
   in
      (* Rename the function's vars and body *)
      List.fold_left (standardize_fun_expr tenv) (venv, []) funs

and standardize_funs_expr tenv venv pos funs e =
   (* Rename the funs *)
   let venv, funs = standardize_funs tenv venv funs in

   (* Rename the rest *)
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetFuns (pos, funs, e)

(*
 * Atom declaration.
 *)
and standardize_coerce_expr tenv venv pos v ty a e =
   let v' = new_symbol v in
   let tenv, venv, a = standardize_atom tenv venv a in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetCoerce (pos, v', ty, a, e)

and standardize_test_expr tenv venv pos v ty a e =
   let v' = new_symbol v in
   let tenv, venv, a = standardize_atom tenv venv a in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetTest (pos, v', ty, a, e)

(*
 * Copy an atom.
 *)
and standardize_fundecl_expr tenv venv pos sc v label gflag ty e =
   let v' = new_symbol v in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetFunDecl (pos, sc, v', label, gflag, ty, e)

and standardize_var_expr tenv venv pos sc v label gflag ty init e =
   let v' = new_symbol v in
   let tenv, venv, init = standardize_init tenv venv init in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetVar (pos, sc, v', label, gflag, ty, init, e)

(*
 * Function application.
 *)
and standardize_extcall_expr tenv venv pos v ty s b ty' args e =
   let v' = new_symbol v in
   let tenv, venv, args = standardize_atoms tenv venv args in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let tenv, venv, ty' = standardize_type tenv venv ty' in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetExtCall (pos, v', ty, s, b, ty', args, e)

and standardize_apply_expr tenv venv pos v ty f label args e =
   let v' = new_symbol v in
   let f = venv_lookup_var venv f in
   let tenv, venv, args = standardize_atoms tenv venv args in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetApply (pos, v', ty, f, label, args, e)

(*
 * Operator application.
 *)
and standardize_atom_expr tenv venv pos v ty a e =
   let v' = new_symbol v in
   let tenv, venv, a = standardize_atom tenv venv a in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetAtom (pos, v', ty, a, e)

and standardize_unop_expr tenv venv pos v ty op a e =
   let v' = new_symbol v in
   let tenv, venv, a = standardize_atom tenv venv a in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetUnop (pos, v', ty, op, a, e)

and standardize_binop_expr tenv venv pos v ty op a1 a2 e =
   let v' = new_symbol v in
   let tenv, venv, a1 = standardize_atom tenv venv a1 in
   let tenv, venv, a2 = standardize_atom tenv venv a2 in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetBinop (pos, v', ty, op, a1, a2, e)

(*
 * Tail call.
 *)
and standardize_tailcall_expr tenv venv pos f args =
   let f = venv_lookup_var venv f in
   let _, _, args = standardize_atoms tenv venv args in
      tenv, TailCall (pos, f, args)

(*
 * Return a value.
 *)
and standardize_return_expr tenv venv pos f ty a =
   let tenv, venv, ty = standardize_type tenv venv ty in
   let _, _, a = standardize_atom tenv venv a in
   let f = venv_lookup_var venv f in
      tenv, Return (pos, f, ty, a)

(*
 * Allocate a string.
 *)
and standardize_string_expr tenv venv pos v ty pre s e =
   let v' = new_symbol v in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetString (pos, v', ty, pre, s, e)

(*
 * Conditional expression.
 *)
and standardize_if_expr tenv venv pos a e1 e2 =
   let tenv, venv, a = standardize_atom tenv venv a in
   let tenv, e1 = standardize_expr tenv venv e1 in
   let tenv, e2 = standardize_expr tenv venv e2 in
      tenv, IfThenElse (pos, a, e1, e2)

(*
 * IR2 memory operations.
 *)
and standardize_malloc_expr tenv venv pos v a e =
   let v' = new_symbol v in
   let tenv, venv, a = standardize_atom tenv venv a in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetMalloc (pos, v', a, e)

and standardize_alloc_expr tenv venv pos v ty label args e =
   let v' = new_symbol v in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let tenv, venv, args = standardize_atoms tenv venv args in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetAlloc (pos, v, ty, label, args, e)

(*
 * Sizeof.
 *)
and standardize_sizeof_type_expr tenv venv pos v ty1 ty2 e =
   let v' = new_symbol v in
   let tenv, venv, ty1 = standardize_type tenv venv ty1 in
   let tenv, venv, ty2 = standardize_type tenv venv ty2 in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetSizeof (pos, v', ty1, ty2, e)

(*
 * Try.
 *)
and standardize_raise_expr tenv venv pos a =
   let tenv, venv, a = standardize_atom tenv venv a in
      tenv, Raise (pos, a)

and standardize_try_expr tenv venv pos e1 v e2 =
   let v' = new_symbol v in
   let tenv, e1 = standardize_expr tenv venv e1 in
   let venv = venv_add_var venv v v' in
   let tenv, e2 = standardize_expr tenv venv e2 in
      tenv, Try (pos, e1, v', e2)

(*
 * Match.
 *)
and standardize_match_expr tenv venv pos a ty cases =
   let tenv, venv, a = standardize_atom tenv venv a in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let tenv, cases = standardize_cases tenv venv cases in
      tenv, Match (pos, a, ty, cases)

and standardize_cases tenv venv cases =
   let tenv, cases =
      List.fold_left (fun (tenv, cases) (p, e) ->
            let tenv, p, e = standardize_case tenv venv p e in
               tenv, (p, e) :: cases) (tenv, []) cases
   in
      tenv, List.rev cases

and standardize_case tenv venv p e =
   let tenv, venv, p = standardize_pattern tenv venv p in
   let tenv, e = standardize_expr tenv venv e in
      tenv, p, e

and standardize_pattern tenv venv p =
   match p with
      IntPattern _
    | FloatPattern _
    | StringPattern _ ->
         tenv, venv, p
    | VarPattern (pos, v, ty_opt) ->
         let tenv, venv, ty_opt = standardize_type_opt tenv venv ty_opt in
            (try
                let v' = SymbolTable.find venv.venv_fields v in
                   tenv, venv, VarPattern (pos, v', ty_opt)
             with
                Not_found ->
                   let v' = new_symbol v in
                   let venv = venv_add_var venv v v' in
                      tenv, venv, VarPattern (pos, v', ty_opt))
    | StructPattern (pos, fields) ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (v_opt, p) ->
                  let tenv, venv, p = standardize_pattern tenv venv p in
                  let fields = (v_opt, p) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, StructPattern (pos, List.rev fields)
    | EnumPattern (pos, ty, i) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, EnumPattern (pos, ty, i)
    | UEnumPattern (pos, ty, v, p_opt) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
         let tenv, venv, p_opt = standardize_pattern_opt tenv venv p_opt in
         let v = venv_lookup_field venv v in
            tenv, venv, UEnumPattern (pos, ty, v, p_opt)
    | VEnumPattern (pos, v, p) ->
         let tenv, venv, p = standardize_pattern tenv venv p in
         let v = venv_lookup_field venv v in
            tenv, venv, VEnumPattern (pos, v, p)
    | AsPattern (pos, p1, p2) ->
         let tenv, venv, p1 = standardize_pattern tenv venv p1 in
         let tenv, venv, p2 = standardize_pattern tenv venv p2 in
            tenv, venv, AsPattern (pos, p1, p2)

and standardize_pattern_opt tenv venv = function
   Some p ->
      let tenv, venv, p = standardize_pattern tenv venv p in
         tenv, venv, Some p
 | None ->
      tenv, venv, None

(*
 * Subscripting.
 *)
and standardize_project_expr tenv venv pos v1 ty a2 v3 e =
   let v1' = new_symbol v1 in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let tenv, venv, a2 = standardize_atom tenv venv a2 in
   let venv = venv_add_var venv v1 v1' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetProject (pos, v1', ty, a2, v3, e)

and standardize_subscript_expr tenv venv pos v ty v1 ty2 a2 e =
   let v' = new_symbol v in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let v1 = venv_lookup_var venv v1 in
   let tenv, venv, ty2 = standardize_type tenv venv ty2 in
   let tenv, venv, a2 = standardize_atom tenv venv a2 in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetSubscript (pos, v', ty, v1, ty2, a2, e)

and standardize_set_expr tenv venv pos a1 ty a2 e =
   let tenv, venv, a1 = standardize_atom tenv venv a1 in
   let tenv, venv, a2 = standardize_atom tenv venv a2 in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let tenv, e = standardize_expr tenv venv e in
      tenv, Set (pos, a1, ty, a2, e)

and standardize_addr_of_expr tenv venv pos v ty a e =
   let v' = new_symbol v in
   let tenv, venv, ty = standardize_type tenv venv ty in
   let tenv, venv, a = standardize_atom tenv venv a in
   let venv = venv_add_var venv v v' in
   let tenv, e = standardize_expr tenv venv e in
      tenv, LetAddrOf (pos, v', ty, a, e)

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Rename type vars.
 *)
let standardize_type ty =
   let _, _, ty = standardize_type tenv_empty venv_empty ty in
      ty

(*
 * Global functions use a null environment.
 *)
let standardize_expr e =
   let _, e = standardize_expr tenv_empty venv_empty e in
      e

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
