(*
 * Exceptions during the AST stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_ast

(*
 * Exception values.
 *)
type ast_error =
   StringError of string
 | StringAtomError of string * atom
 | StringTypeError of string * ty
 | StringIntError of string * int
 | StringInt32Error of string * int32
 | StringVarTypeListError of string * var * ty list * ty list
 | UnboundVar of var
 | UnboundType of ty_var
 | UnboundLabel of var
 | ArityMismatch of int * int
 | TypeError2 of ty * ty
 | TypeError4 of ty * ty * ty * ty
 | NotImplemented of string
 | FieldsDontMatch of var * var
 | StructFieldLengthsDontMatch of int option * int option
 | StringStructFieldError of string * field_decl
 | UEnumFieldsDontMatch of var * ty option * var * ty option
 | EnumFieldValuesDontMatch of int32 * int32
 | StringUEnumFieldError of string * union_enum_decl
 | StringEnumFieldError of string * enum_decl
 | StringVarError of string * var

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
