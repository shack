(*
 * Exceptions during the AST stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Fc_ast
open Fc_ast_exn
open Fc_ast_pos

module Pos = MakePos (struct let name = "Fc_ast_exn_print" end)
open Pos

let tabstop = 3

(*
 * Exception printer.
 *)
let pp_print_exn buf exn =
   match exn with
      AstException (pos, exn) ->
         fprintf buf "@[<hv 3>*** AST error:@ %a@ %a@]@." (**)
            pp_print_pos pos
            pp_print_ast_exn exn
    | Fir_pos.FirException _ ->
         Fir_exn_print.pp_print_exn buf exn
    | exn ->
         Fc_parse_exn.pp_print_exn buf exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      AstException _
    | Fc_parse_type.ParseError _
    | Fc_parse_exn.ParseExpError _
    | Fc_parse_state.IncompatibleTypes _
    | Parsing.Parse_error
    | Fir_pos.FirException _ as exn ->
         pp_print_exn err_formatter exn;
         exit 2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
