(*
 * Expand bit-field types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_config

open Fc_ast
open Fc_ast_env
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_type

module Pos = MakePos (struct let name = "Fc_ast_field" end)
open Pos

(*
 * Max length of a field (and alignment).
 *)
let max_field_length = FCParam.max_field_length

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Convert a type by adding structure field lengths.
 *)
let rec field_type tenv ty =
   let loc = string_pos "field_type" (type_exp_pos ty) in
      match ty with
         TyUnit _
       | TyDelayed _
       | TyZero _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyVar _
       | TyEnum _ ->
            ty
       | TyArray (pos, status, ty, a1, a2) ->
            TyArray (pos, status, field_type tenv ty, field_atom tenv a1, field_atom tenv a2)
       | TyConfArray (pos, status, ty, v1, v2, v_ty) ->
            TyConfArray (pos, status, field_type tenv ty, v1, v2, field_type tenv v_ty)
       | TyPointer (pos, status, ty) ->
            TyPointer (pos, status, field_type tenv ty)
       | TyRef (pos, status, ty) ->
            TyRef (pos, status, field_type tenv ty)
       | TyTuple (pos, status, tyl) ->
            TyTuple (pos, status, List.map (field_type tenv) tyl)
       | TyUEnum (pos, status, fields) ->
            let fields =
               match fields with
                  Fields (label, fields) ->
                     Fields (label, List.map (fun (pos, v, ty_opt, label) ->
                                   pos, v, field_type_opt tenv ty_opt, label) fields)
                | Label _ ->
                     fields
            in
               TyUEnum (pos, status, fields)
       | TyUnion (pos, status, fields) ->
            let fields = field_union tenv loc fields in
               TyUnion (pos, status, fields)
       | TyStruct (pos, status, fields) ->
            let fields = field_struct tenv loc fields in
               TyStruct (pos, status, fields)
       | TyFun (pos, status, ty_args, ty_res) ->
            TyFun (pos, status, List.map (field_type tenv) ty_args, field_type tenv ty_res)
       | TyAll (pos, status, vars, ty) ->
            TyAll (pos, status, vars, field_type tenv ty)
       | TyLambda (pos, status, vars, ty) ->
            TyLambda (pos, status, vars, field_type tenv ty)
       | TyApply (pos, status, v, tyl) ->
            TyApply (pos, status, v, List.map (field_type tenv) tyl)
       | TyElide (pos, status, tyl) ->
            TyElide (pos, status, List.map (field_type tenv) tyl)

and field_type_opt tenv = function
   Some ty -> Some (field_type tenv ty)
 | None -> None

and field_union tenv loc fields =
   match fields with
      Fields fields ->
         let fields =
            List.map (fun (pos, v, ty, i_opt) ->
                  let ty =
                     match i_opt with
                        Some len ->
                           let pos, status, pre, signed = dest_int_type tenv loc ty in
                           let len = min len max_field_length in
                              TyField (pos, status, pre, signed, 0, len)
                      | None ->
                           ty
                  in
                     pos, v, ty, i_opt) fields
         in
            Fields fields
    | Label _ ->
         fields

and field_struct tenv loc fields =
   match fields with
      Fields fields ->
         let fields, _ =
            List.fold_left (fun (fields, off) (pos, v, ty, i_opt) ->
                  let ty, off =
                     match i_opt with
                        Some len ->
                           let pos, status, pre, signed = dest_int_type tenv loc ty in
                           let len = min len max_field_length in
                           let off =
                              if off + len > max_field_length then
                                 0
                              else
                                 off
                           in
                              TyField (pos, status, pre, signed, off, len), off + len
                      | None ->
                           ty, 0
                  in
                  let field = pos, v, ty, i_opt in
                     field :: fields, off) ([], 0) fields
         in
            Fields (List.rev fields)
    | Label _ ->
         fields

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Convert the types in the atoms.
 *)
and field_atom tenv a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomVar _ ->
         a
    | AtomNil (pos, ty) ->
         AtomNil (pos, field_type tenv ty)
    | AtomEnum (pos, ty, i) ->
         AtomEnum (pos, field_type tenv ty, i)

let field_atom_opt tenv = function
   Some a -> Some (field_atom tenv a)
 | None -> None

let field_atoms tenv args =
   List.map (field_atom tenv) args

let rec field_init tenv init =
   match init with
      InitNone ->
         init
    | InitAtom (pos, a) ->
         InitAtom (pos, field_atom tenv a)
    | InitArray (pos, fields) ->
         let fields =
            List.map (fun (v_opt, init) ->
                  v_opt, field_init tenv init) fields
         in
            InitArray (pos, fields)

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Convert all the struct types in the expression.
 *)
let rec field_exp tenv e =
   let loc = string_pos "field_exp" (exp_pos e) in
      match e with
         LetTypes (pos, types, e) ->
            field_types_exp tenv loc pos types e
       | LetFuns (pos, funs, e) ->
            field_funs_exp tenv loc pos funs e
       | LetFunDecl (pos, sc, f, label, gflag, ty, e) ->
            LetFunDecl (pos, sc, f, label, gflag, field_type tenv ty, field_exp tenv e)
       | LetVar (pos, sc, v, label, gflag, ty, init, e) ->
            LetVar (pos, sc, v, label, gflag, field_type tenv ty, field_init tenv init, field_exp tenv e)
       | LetCoerce (pos, v, ty, a, e) ->
            LetCoerce (pos, v, field_type tenv ty, field_atom tenv a, field_exp tenv e)
       | LetAtom (pos, v, ty, a, e) ->
            LetAtom (pos, v, field_type tenv ty, field_atom tenv a, field_exp tenv e)
       | LetUnop (pos, v, ty, op, a, e) ->
            LetUnop (pos, v, field_type tenv ty, op, field_atom tenv a, field_exp tenv e)
       | LetBinop (pos, v, ty, op, a1, a2, e) ->
            LetBinop (pos, v, field_type tenv ty, op, field_atom tenv a1, field_atom tenv a2, field_exp tenv e)
       | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
            LetExtCall (pos, v, field_type tenv ty, s, b, field_type tenv ty', field_atoms tenv args, field_exp tenv e)
       | LetApply (pos, v, ty, f, label, args, e) ->
            LetApply (pos, v, field_type tenv ty, f, label, field_atoms tenv args, field_exp tenv e)
       | LetTest (pos, v, ty, a, e) ->
            LetTest (pos, v, field_type tenv ty, field_atom tenv a, field_exp tenv e)
       | LetSizeof (pos, v, ty1, ty2, e) ->
            LetSizeof (pos, v, field_type tenv ty1, field_type tenv ty2, field_exp tenv e)
       | TailCall (pos, f, args) ->
            TailCall (pos, f, field_atoms tenv args)
       | Return (pos, f, ty, a) ->
            Return (pos, f, field_type tenv ty, field_atom tenv a)
       | LetString (pos, v, ty, pre, s, e) ->
            LetString (pos, v, field_type tenv ty, pre, s, field_exp tenv e)
       | LetAlloc (pos, v, ty, label, args, e) ->
            LetAlloc (pos, v, field_type tenv ty, label, field_atoms tenv args, field_exp tenv e)
       | LetMalloc (pos, v, a, e) ->
            LetMalloc (pos, v, field_atom tenv a, field_exp tenv e)
       | IfThenElse (pos, a, e1, e2) ->
            IfThenElse (pos, field_atom tenv a, field_exp tenv e1, field_exp tenv e2)
       | Match (pos, a, ty, cases) ->
            let cases = List.map (fun (p, e) -> p, field_exp tenv e) cases in
               Match (pos, field_atom tenv a, field_type tenv ty, cases)
       | Try (pos, e1, v, e2) ->
            Try (pos, field_exp tenv e1, v, field_exp tenv e2)
       | Raise (pos, a) ->
            Raise (pos, field_atom tenv a)
       | LetProject (pos, v, ty, a, label, e) ->
            LetProject (pos, v, field_type tenv ty, field_atom tenv a, label, field_exp tenv e)
       | LetSubscript (pos, v1, ty, v2, ty2, a3, e) ->
            LetSubscript (pos, v1, field_type tenv ty, v2, field_type tenv ty2, field_atom tenv a3, field_exp tenv e)
       | Set (pos, a1, ty, a2, e) ->
            Set (pos, field_atom tenv a1, field_type tenv ty, field_atom tenv a2, field_exp tenv e)
       | LetAddrOf (pos, v, ty, a, e) ->
            LetAddrOf (pos, v, field_type tenv ty, field_atom tenv a, field_exp tenv e)
       | Debug (pos, info, e) ->
            let info =
               match info with
                  DebugString _ ->
                     info
                | DebugContext (line, vars) ->
                     let vars =
                        List.map (fun (v1, ty, v2) ->
                              v1, field_type tenv ty, v2) vars
                     in
                        DebugContext (line, vars)
            in
               Debug (pos, info, field_exp tenv e)

(*
 * Convert all the types and add them to the environment.
 *)
and field_types_exp tenv loc pos types e =
   let loc = string_pos "field_types_exp" loc in
   let tenv, types =
      List.fold_left (fun (tenv, types) (v, label, ty) ->
            let ty = field_type tenv ty in
            let tenv = tenv_add tenv v ty in
               tenv, (v, label, ty) :: types) (tenv, []) types
   in
   let types = List.rev types in
   let e = field_exp tenv e in
      LetTypes (pos, types, e)

(*
 * Convert all the types in the functions.
 *)
and field_funs_exp tenv loc pos funs e =
   let loc = string_pos "field_funs_exp" loc in
   let funs =
      List.map (fun (pos, f, label, line, gflag, ty, vars, e) ->
            let ty = field_type tenv ty in
            let e = field_exp tenv e in
               pos, f, label, line, gflag, ty, vars, e) funs
   in
   let e = field_exp tenv e in
      LetFuns (pos, funs, e)

(************************************************************************
 * GLOBAL
 ************************************************************************)

let field_exp e =
   field_exp tenv_empty e

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
