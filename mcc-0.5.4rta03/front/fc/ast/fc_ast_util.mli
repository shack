(*
 * Utilities on the AST.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Fc_config

open Fc_ast

val loc_of_type : ty -> loc
val loc_of_atom : atom -> loc
val loc_of_expr : exp -> loc

(*
 * Get the returned value.
 *)
val value_of_expr : exp -> atom option

(*
 * Status.
 *)
val status_of_type : ty -> type_status

(*
 * Map a function over types.
 *)
val map_type_exp : (ty -> ty) -> exp -> exp

(*
 * "Squash" all the types in the program by
 * turning array arguments into pointer arguments.
 *)
val squash_type_exp : exp -> exp

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
