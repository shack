(*
 * Conformant-array elimination.
 * First we expand all function definitions:
 *   (.., conf_array, ...)  -> (..., u, v, conf_array, ...),
 * where u and v are the lower and upper indexes of the
 * conformant array parameter.
 * At the same time, we are adding the appropriate two
 * extra arguments to each function call involving conformant
 * arrays.
 * Last, we replace TyConfArray with TyArray.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *
 *)
open Format

open Symbol
open Fc_ast
open Fc_config
open Fc_ast_type
open Fc_ast_env
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_print

module Pos = MakePos (struct let name = "Fc_ast_confarray" end)
open Pos

let rawint_zero = Rawint.of_int Rawint.Int32 true 0

let rec vars_contained ty =
   match ty with
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _
    | TyEnum _
    | TyStruct (_, _, Label _)
    | TyUnion (_, _, Label _)
    | TyUEnum (_, _, Label _) ->
         []
    | TyArray (_, _, ty, _, _) ->
         vars_contained ty
    | TyConfArray (_, _, ty, v1, v2, v_ty) ->
         (v2, v_ty) :: (v1, v_ty) :: vars_contained ty
    | TyPointer (_, _, ty) ->
         vars_contained ty
    | TyRef (_, _, ty) ->
         vars_contained ty
    | TyTuple (_, _, ty_list)
    | TyElide (_, _, ty_list) ->
         vars_contained_list ty_list
    | TyUEnum (_, _, Fields (label, fields)) ->
         let ty_list =
            List.filter (fun (_, _, ty_opt, _) ->
                  match ty_opt with
                     Some _ ->
                        true
                   | None ->
                        false) fields
         in
         let ty_list =
            List.map (fun (_, _, ty_opt, _) ->
                  match ty_opt with
                     Some ty ->
                        ty
                   | None ->
                        raise (Invalid_argument "contains_conf_array")) ty_list
         in
            vars_contained_list ty_list
    | TyUnion (_, _, Fields fields) ->
         let ty_list = List.map (fun (_, _, ty, _) -> ty) fields in
            vars_contained_list ty_list
    | TyStruct (_, _, Fields fields) ->
         let ty_list = List.map (fun (_, _, ty, _) -> ty) fields in
            vars_contained_list ty_list
    | TyFun (_, _, ty_list, _) ->
         vars_contained_list ty_list
    | TyAll (_, _, _, ty) ->
         vars_contained ty
    | TyLambda (_, _, _, ty) ->
         vars_contained ty
    | TyApply (_, _, _, ty_list) ->
         vars_contained_list ty_list

and vars_contained_list ty_list =
   let vars_list =
      List.map (fun ty ->
            vars_contained ty) ty_list
   in
      List.flatten vars_list

(*
 * Add two extra parameters (.., u, v, array, ...) for each
 * conformant array parameter.
 *)
let update_formal_parameters ty_list =
   let rec loop ty_list final =
      match ty_list with
         ty :: ty_rest ->
            let vars = vars_contained ty in
            let ty_list = List.map snd vars in
               loop ty_rest ([ty] @ ty_list @ final)
       | [] ->
            final
   in
      List.rev (loop ty_list [])

let update_formal_parameters_with_names ty_list args =
   let rec loop ty_list args final_tys final_args =
      match ty_list, args with
         ty :: ty_rest, a :: a_rest ->
            let vars = vars_contained ty in
            let ty_list = List.map snd vars in
            let vars = List.map fst vars in
               loop ty_rest a_rest ([ty] @ ty_list @ final_tys) ([a] @ vars @ final_args)
       | [], [] ->
            final_tys, final_args
       | _ ->
            raise (Invalid_argument "Fc_ast_confarray.ml: update_formal_parameters_with_names")
   in
   let l1, l2 = loop ty_list args [] [] in
      List.rev l1, List.rev l2

(*
 * Function formal parameters had been already expanded,
 * now it is time to expand arguments accordingly.
 *
 * BUG: This code will not work for cases, where any parameter
 *      introduces more than 2 extra parameters. Cases like that
 *      are: multi-dimensional arrays, weird aggregate parameters
 *      involving functions, among others.
 *)
let update_arguments venv pos loc ty_list args =
   let rec loop ty_list args final =
      match ty_list, args with
         ty :: ty_rest, a :: a_rest ->
            (match ty, type_of_atom venv loc a with
               (* Regular conformant array parameter *)
               TyConfArray _, TyConfArray(_, _, _, u1, u2, u_ty) ->
                  let final = AtomVar(pos, u1) :: AtomVar(pos, u2) :: a :: final in
                  let ty_rest = List.tl (List.tl ty_rest) in
                     loop ty_rest a_rest final
             | TyConfArray _, TyArray(_, _, _, a1, a2) ->
                  let final = a1 :: a2 :: a :: final in
                  let ty_rest = List.tl (List.tl ty_rest) in
                     loop ty_rest a_rest final
               (* Mutable conformant array parameter *)
             | TyRef(_, _, TyConfArray _), TyConfArray(_, _, _, u1, u2, u_ty) ->
                  let final = AtomVar(pos, u1) :: AtomVar(pos, u2) :: a :: final in
                  let ty_rest = List.tl (List.tl ty_rest) in
                     loop ty_rest a_rest final
             | TyRef(_, _, TyConfArray _), TyArray(_, _, _, a1, a2) ->
                  let final = a1 :: a2 :: a :: final in
                  let ty_rest = List.tl (List.tl ty_rest) in
                     loop ty_rest a_rest final
               (* C: pointer or reference passed as array *)
             | TyRef(_, _, TyConfArray _), TyRef _
             | TyRef(_, _, TyConfArray _), TyPointer _
             | TyConfArray _, TyRef _
             | TyConfArray _, TyPointer _ ->
                  let final = AtomInt(pos, rawint_zero) :: AtomInt(pos, rawint_zero) :: a :: final in
                  let ty_rest = List.tl (List.tl ty_rest) in
                     loop ty_rest a_rest final
             | TyConfArray _, _ ->
                  raise (Invalid_argument "Fc_ast_confarray.ml: update_arguments: Invalid argument passed as array")
             | _ ->
                  loop ty_rest a_rest (a :: final)
            )
       | _, [] ->
            final
       | _ ->
            raise (Invalid_argument "Fc_ast_confarray.ml: update_arguments")
   in
   let args = List.rev args in
   let ty_list = List.rev ty_list in
   let args = loop ty_list args [] in
      venv, args

(***********************************************************
 * Expand function definitions and function calls with
 * the appropriate lower and upper index for each
 * conformant array parameter.
 ***********************************************************)
let rec conf_expr tenv venv e =
    let loc = string_pos "conf_expr" (exp_pos e) in
       match e with
          LetTypes(pos, types, e) ->
             conf_add_types tenv venv pos types e
        | LetFuns(pos, funs, e) ->
             conf_funs_expr tenv venv pos funs e
        | LetFunDecl(pos, sc, v, l, gflag, ty, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetFunDecl(pos, sc, v, l, gflag, ty, e)
        | LetVar(pos, sc, v, l, gflag, ty, a_opt, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetVar(pos, sc, v, l, gflag, ty, a_opt, e)
        | LetCoerce(pos, v, ty, a, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetCoerce(pos, v, ty, a, e)
        | LetAtom(pos, v, ty, a, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetAtom(pos, v, ty, a, e)
        | LetUnop(pos, v, ty, op, a, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetUnop(pos, v, ty, op, a, e)
        | LetBinop(pos, v, ty, op, a1, a2, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetBinop(pos, v, ty, op, a1, a2, e)
        | LetExtCall(pos, v, ty, s, b, ty', args, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetExtCall(pos, v, ty, s, b, ty', args, e)
        (*
         * We update the argument list.
         *)
        | LetApply(pos, v, ty, f, label, args, e) ->
            let venv = venv_add venv v ty in
            let f_ty = venv_lookup venv loc f in
            let _, ty_list, _ = dest_fun_type tenv loc f_ty in
            let venv, args' = update_arguments venv pos loc ty_list args in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetApply(pos, v, ty, f, label, args', e)
            | LetTest(pos, v, ty, a, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetTest(pos, v, ty, a, e)
        | LetSizeof (pos, v, ty1, ty2, e) ->
            let venv = venv_add venv v ty1 in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetSizeof (pos, v, ty1, ty2, e)

        | TailCall _
        | Return _ ->
            tenv, venv, e

        | LetString(pos, v, ty, pre, s, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetString(pos, v, ty, pre, s, e)
        | LetAlloc (pos, v, ty, label, args, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetAlloc(pos, v, ty, label, args, e)
        | LetMalloc(pos, v, a, e) ->
            let venv = venv_add venv v (TyPointer(pos, StatusNormal, TyInt(pos, StatusNormal, Rawint.Int8, false))) in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetMalloc(pos, v, a, e)
        | IfThenElse(pos, a, e1, e2) ->
            let _, _, e1 = conf_expr tenv venv e1 in
            let _, _, e2 = conf_expr tenv venv e2 in
                tenv, venv, IfThenElse(pos, a, e1, e2)

        | Match(pos, a, ty, cases) ->
            tenv, venv, e
        | Try(pos, e1, v, e2) ->
            let _, _, e1 = conf_expr tenv venv e1 in
            let _, _, e2 = conf_expr tenv venv e2 in
                tenv, venv, Try(pos, e1, v, e2)
        | Raise(pos, a) ->
            tenv, venv, e
        | LetProject(pos, v1, ty, a2, v3, e) ->
            let venv = venv_add venv v1 ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetProject(pos, v1, ty, a2, v3, e)
        | LetSubscript(pos, v1, ty, v2, ty2, a, e) ->
            let venv = venv_add venv v1 ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetSubscript(pos, v1, ty, v2, ty2, a, e)
        | Set(pos, a1, ty, a2, e) ->
            let _, _, e = conf_expr tenv venv e in
                 tenv, venv, Set(pos, a1, ty, a2, e)
        | LetAddrOf(pos, v, ty, a, e) ->
            let venv = venv_add venv v ty in
            let _, _, e = conf_expr tenv venv e in
                tenv, venv, LetAddrOf(pos, v, ty, a, e)
        | Debug(pos, info, e') ->
            tenv, venv, e

(*
 * Add types to the type environment.
 *)
and conf_add_types tenv venv pos types e =
    let tenv = List.fold_left (fun tenv (v, l, ty) ->
        tenv_add tenv v ty) tenv types
    in
        let _, _, e = conf_expr tenv venv e in
        tenv, venv, LetTypes(pos, types, e)

(*
 * Add function names to venv,
 * process their bodies.
 *)
and conf_funs_expr tenv venv pos funs e =
    (* Add all function names to variable environment *)
    let venv = List.fold_left (fun venv (_, f, _, _, _, ty_f, _, _) ->
        match ty_f with
              TyFun(pos, status, ty_list, ty_res) ->
                let ty_list = update_formal_parameters ty_list in
                    venv_add venv f (TyFun(pos, status, ty_list, ty_res))
            | _ ->
                raise (Invalid_argument "conf_funs_expr1: not a function")) venv funs
    in

    let funs = List.map (fun (pos, f, l, dl, vc, ty_f, args, body) ->
        match ty_f with
              TyFun(pos, status, ty_list, ty_res) ->
                let ty_list, args = update_formal_parameters_with_names ty_list args in

                (* Add formal parameters to venv *)
                let venv = List.fold_left2 venv_add venv args ty_list in
                let _, _, body = conf_expr tenv venv body in
                    pos, f, l, dl, vc, TyFun(pos, status, ty_list, ty_res), args, body
            | _ ->
                raise (Invalid_argument "conf_funs_expr: not a function")) funs
    in
    let _, _, e = conf_expr tenv venv e in
        tenv, venv, LetFuns(pos, funs, e)

(***********************************************************
 * Eliminate TyConfArray.
 ***********************************************************)
let rec conf_elim_ty ty =
   match ty with
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyEnum _
    | TyStruct (_, _, Label _)
    | TyUnion (_, _, Label _)
    | TyUEnum (_, _, Label _) ->
         ty
    | TyArray(pos, status, ty, a1, a2) ->
         TyArray(pos, status, conf_elim_ty ty, a1, a2)
    | TyConfArray(pos, status, ty, v1, v2, v_ty) ->
         TyArray(pos, status, conf_elim_ty ty, AtomVar(pos, v1), AtomVar(pos, v2))
    | TyPointer(pos, status, ty) ->
         TyPointer(pos, status, conf_elim_ty ty)
    | TyRef(pos, status, ty) ->
         TyRef(pos, status, conf_elim_ty ty)
    | TyTuple(pos, status, ty_list) ->
         TyTuple(pos, status, conf_elim_ty_list ty_list)
    | TyElide(pos, status, ty_list) ->
         TyElide(pos, status, conf_elim_ty_list ty_list)
    | TyUEnum(pos, status, Fields (label, fields)) ->
         let fields =
            List.map (fun (pos, v, ty_opt, l) ->
                  pos, v, conf_elim_ty_opt ty_opt, l) fields
         in
            TyUEnum(pos, status, Fields (label, fields))
    | TyUnion(pos, status, Fields fields) ->
         let fields =
            List.map (fun (pos, v, ty, io) ->
                  pos, v, conf_elim_ty ty, io) fields
         in
            TyUnion(pos, status, Fields fields)
    | TyStruct(pos, status, Fields fields) ->
         let fields =
            List.map (fun (pos, v, ty, io) ->
                  pos, v, conf_elim_ty ty, io) fields
         in
            TyStruct(pos, status, Fields fields)
    | TyFun(pos, status, ty_list, ty_res) ->
         TyFun(pos, status, conf_elim_ty_list ty_list, conf_elim_ty ty_res)
    | TyVar _ ->
         ty
    | TyAll(pos, status, vars, ty) ->
         TyAll(pos, status, vars, conf_elim_ty ty)
    | TyLambda(pos, status, vars, ty) ->
         TyLambda(pos, status, vars, conf_elim_ty ty)
    | TyApply(pos, status, ty_var, ty_list) ->
         TyApply(pos, status, ty_var, conf_elim_ty_list ty_list)

and conf_elim_ty_list ty_list =
   List.map (fun ty ->
         conf_elim_ty ty) ty_list

and conf_elim_ty_opt = function
   Some ty ->
      Some (conf_elim_ty ty)
 | None ->
      None

let rec conf_elim_expr e =
   let loc = string_pos "conf_elim_expr" (exp_pos e) in
      match e with
         LetTypes(pos, types, e) ->
            conf_elim_types pos types e
       | LetFuns(pos, funs, e) ->
            conf_elim_funs pos funs e
       | LetFunDecl(pos, sc, v, l, gflag, ty, e) ->
            LetFunDecl(pos, sc, v, l, gflag, conf_elim_ty ty, conf_elim_expr e)
       | LetVar(pos, sc, v, l, gflag, ty, a_opt, e) ->
            LetVar(pos, sc, v, l, gflag, conf_elim_ty ty, a_opt, conf_elim_expr e)
       | LetCoerce(pos, v, ty, a, e) ->
            LetCoerce(pos, v, conf_elim_ty ty, a, conf_elim_expr e)
       | LetAtom(pos, v, ty, a, e) ->
            LetAtom(pos, v, conf_elim_ty ty, a, conf_elim_expr e)
       | LetUnop(pos, v, ty, op, a, e) ->
            LetUnop(pos, v, conf_elim_ty ty, op, a, conf_elim_expr e)
       | LetBinop(pos, v, ty, op, a1, a2, e) ->
            LetBinop(pos, v, conf_elim_ty ty, op, a1, a2, conf_elim_expr e)
       | LetExtCall(pos, v, ty, s, b, ty', args, e) ->
            LetExtCall(pos, v, conf_elim_ty ty, s, b, ty', args, conf_elim_expr e)
       | LetApply(pos, v, ty, f, label, args, e) ->
            LetApply(pos, v, conf_elim_ty ty, f, label, args, conf_elim_expr e)
       | LetTest(pos, v, ty, a, e) ->
            LetTest(pos, v, conf_elim_ty ty, a, conf_elim_expr e)
       | LetSizeof (pos, v, ty1, ty2, e) ->
            LetSizeof (pos, v, conf_elim_ty ty1, conf_elim_ty ty2, conf_elim_expr e)

       | TailCall _
       | Return _ ->
            e

       | LetString(pos, v, ty, pre, s, e) ->
            LetString(pos, v, conf_elim_ty ty, pre, s, conf_elim_expr e)
       | LetAlloc(pos, v, ty, label, args, e) ->
            LetAlloc(pos, v, conf_elim_ty ty, label, args, conf_elim_expr e)
       | LetMalloc(pos, v, a, e) ->
            LetMalloc(pos, v, a, conf_elim_expr e)
       | IfThenElse(pos, a, e1, e2) ->
            IfThenElse(pos, a, conf_elim_expr e1, conf_elim_expr e2)

       | Match(pos, a, ty, cases) ->
            e
       | Try(pos, e1, v, e2) ->
            Try(pos, conf_elim_expr e1, v, conf_elim_expr e2)
       | Raise(pos, a) ->
            e
       | LetProject(pos, v1, ty, a2, v3, e) ->
            LetProject(pos, v1, conf_elim_ty ty, a2, v3, conf_elim_expr e)
       | LetSubscript(pos, v1, ty, v2, ty2, a, e) ->
            LetSubscript(pos, v1, conf_elim_ty ty, v2, ty2, a, conf_elim_expr e)
       | Set(pos, a1, ty, a2, e) ->
            Set(pos, a1, conf_elim_ty ty, a2, conf_elim_expr e)
       | LetAddrOf(pos, v, ty, a, e) ->
            LetAddrOf(pos, v, conf_elim_ty ty, a, conf_elim_expr e)
       | Debug(pos, info, e') ->
            e

and conf_elim_types pos types e =
   let types =
      List.map (fun (v, l, ty) ->
            v, l, conf_elim_ty ty) types
   in
      LetTypes(pos, types, conf_elim_expr e)

and conf_elim_funs pos funs e =
   let funs =
      List.map (fun (pos, f, l, dl, vc, ty_f, args, body) ->
            pos, f, l, dl, vc, conf_elim_ty ty_f, args, conf_elim_expr body) funs
   in
      LetFuns(pos, funs, conf_elim_expr e)

(***********************************************************
 * At last, do the following:
 *  I. Add extra arguments and parameters
 * II. Eliminate TyConfArray's
 ***********************************************************)
let compile prog =
    let _, _, e = conf_expr tenv_empty venv_empty prog in
        conf_elim_expr e

