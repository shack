(*
 * Parse->AST conversion.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Fc_ast_print

let debug_print_exp name exp =
   if debug Fir_state.debug_print_ast then
      debug_expr name exp

let compile exp =
   let exp = Fc_ast_parse.build_exp exp in
   let _ = debug_print_exp "build" exp in
   let exp = Fc_ast_field.field_exp exp in
   let _ = debug_print_exp "field" exp in
   let exp = Fc_ast_enum.enum_exp exp in
   let _ = debug_print_exp "enum" exp in
   let exp = Fc_ast_apply.apply_exp exp in
   let _ = debug_print_exp "apply" exp in
   let exp = Fc_ast_const.inline_exp exp in
   let _ = debug_print_exp "inline" exp in
   let exp = Fc_ast_dead.dead_exp exp in
   let _ = debug_print_exp "dead" exp in
   let exp = Fc_ast_infer.infer_exp exp in
   let _ = debug_print_exp "infer" exp in
   let exp = Fc_ast_confarray.compile exp in
   let _ = debug_print_exp "conf_array" exp in
      exp

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
