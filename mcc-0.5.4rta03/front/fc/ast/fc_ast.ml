(*
 * IR types for FC.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Rawint
open Rawfloat
open Location

open Fc_config

(*
 * Generic symbol names.
 *)
type var = symbol
type ty_var = symbol

(*
 * Function classes:
 *    FunDeclClass: function declaration
 *    FunGlobalClass: user-defined function, may be global or static
 *    FunContClass: continuation
 *    FunLocalClass: compiler-defined local function
 *)
type var_class =
   VarGlobalClass
 | VarStaticClass
 | VarLocalClass

(*
 * Builtin operators.
 *)
type unop =
   (* Native ints *)
   UMinusIntOp of int_precision * int_signed
 | NotIntOp    of int_precision * int_signed

   (* Floats *)
 | UMinusFloatOp of float_precision
 | AbsFloatOp    of float_precision

   (* Pointers *)
 | EqPointerNilOp
 | NeqPointerNilOp

   (* Floating-point operators *)
 | CosOp of float_precision
 | SinOp of float_precision
 | SqrtOp of float_precision

type binop =
   (* Booleans *)
 | AndBoolOp
 | OrBoolOp
 | EqBoolOp
 | NeqBoolOp

   (* Native ints *)
 | PlusIntOp   of int_precision * int_signed
 | MinusIntOp  of int_precision * int_signed
 | MulIntOp    of int_precision * int_signed
 | DivIntOp    of int_precision * int_signed
 | RemIntOp    of int_precision * int_signed
 | SlIntOp     of int_precision * int_signed
 | SrIntOp     of int_precision * int_signed
 | AndIntOp    of int_precision * int_signed
 | OrIntOp     of int_precision * int_signed
 | XorIntOp    of int_precision * int_signed

 | MinIntOp    of int_precision * int_signed
 | MaxIntOp    of int_precision * int_signed

 | EqIntOp     of int_precision * int_signed
 | NeqIntOp    of int_precision * int_signed
 | LtIntOp     of int_precision * int_signed
 | LeIntOp     of int_precision * int_signed
 | GtIntOp     of int_precision * int_signed
 | GeIntOp     of int_precision * int_signed

   (* Native floats *)
 | PlusFloatOp   of float_precision
 | MinusFloatOp  of float_precision
 | MulFloatOp    of float_precision
 | DivFloatOp    of float_precision
 | RemFloatOp    of float_precision

 | MinFloatOp    of float_precision
 | MaxFloatOp    of float_precision

 | EqFloatOp     of float_precision
 | NeqFloatOp    of float_precision
 | LtFloatOp     of float_precision
 | LeFloatOp     of float_precision
 | GtFloatOp     of float_precision
 | GeFloatOp     of float_precision

   (* Pointer operations *)
 | PlusPointerOp
 | MinusPointerOp

 | EqPointerOp
 | NeqPointerOp
 | LtPointerOp
 | LePointerOp
 | GtPointerOp
 | GePointerOp

   (* Floating-pointer operations *)
 | Atan2Op of float_precision

(*
 * Types.
 * TyUnit is used to represent small integer enumerations.
 *)
type ty =
   TyUnit        of loc * type_status * int
 | TyDelayed     of loc * type_status
 | TyZero        of loc * type_status
 | TyInt         of loc * type_status * int_precision * int_signed
 | TyField       of loc * type_status * int_precision * int_signed * int * int
 | TyFloat       of loc * type_status * float_precision
 | TyArray       of loc * type_status * ty * atom * atom
 | TyConfArray   of loc * type_status * ty * var * var * ty
 | TyPointer     of loc * type_status * ty
 | TyRef         of loc * type_status * ty
 | TyTuple       of loc * type_status * ty list
 | TyEnum        of loc * type_status * enum_decl list label_option
 | TyUEnum       of loc * type_status * (label * union_enum_decl list) label_option
 | TyUnion       of loc * type_status * field_decl list label_option
 | TyStruct      of loc * type_status * field_decl list label_option
 | TyFun         of loc * type_status * ty list * ty
 | TyVar         of loc * type_status * var
 | TyAll         of loc * type_status * var list * ty
 | TyLambda      of loc * type_status * var list * ty
 | TyApply       of loc * type_status * ty_var * ty list
 | TyElide       of loc * type_status * ty list

and enum_decl = loc * var * int32

and union_enum_decl = loc * var * ty option * label

and field_decl = loc * var * ty * int option

(*
 * Values.
 *)
and atom =
   (* AtomUnit (_, bound, i) *)
   AtomUnit  of loc * int * int
 | AtomInt   of loc * rawint
 | AtomFloat of loc * rawfloat
 | AtomNil   of loc * ty
 | AtomVar   of loc * var
 | AtomEnum  of loc * ty * int32

(*
 * Debugging info.
 *)
and debug_vars = (var * ty  * var) list

and debug_info =
   DebugString of string
 | DebugContext of loc * debug_vars

(*
 * Patterns.
 *)
and pattern =
   IntPattern    of loc * rawint
 | FloatPattern  of loc * rawfloat
 | StringPattern of loc * precision * int array
 | VarPattern    of loc * var * ty option
 | StructPattern of loc * (var option * pattern) list
 | EnumPattern   of loc * ty * int32
 | UEnumPattern  of loc * ty * label * pattern option
 | VEnumPattern  of loc * var * pattern
 | AsPattern     of loc * pattern * pattern

(*
 * Intermediate code expressions.
 *)
and exp =
   (* Declarations *)
   LetTypes of loc * types * exp
 | LetFuns of loc * fundef list * exp
 | LetFunDecl of loc * storage_class * var * label * var_class * ty * exp
 | LetVar of loc * storage_class * var * label * var_class * ty * init * exp

   (* Arithmetic *)
 | LetCoerce of loc * var * ty * atom * exp
 | LetAtom of loc * var * ty * atom * exp
 | LetUnop of loc * var * ty * unop * atom * exp
 | LetBinop of loc * var * ty * binop * atom * atom * exp
 | LetExtCall of loc * var * ty * string * bool * ty * atom list * exp
 | LetApply of loc * var * ty * var * var * atom list * exp
 | LetTest of loc * var * ty * atom * exp
 | LetSizeof of loc * var * ty * ty * exp
 | TailCall of loc * var * atom list
 | Return of loc * var * ty * atom

   (* Allocation *)
 | LetString of loc * var * ty * precision * int array * exp
 | LetAlloc  of loc * var * ty * var * atom list * exp
 | LetMalloc of loc * var * atom * exp

   (* Conditionals *)
 | IfThenElse of loc * atom * exp * exp
 | Match of loc * atom * ty * (pattern * exp) list

   (* try e1 with v -> e2 *)
 | Try of loc * exp * var * exp
 | Raise of loc * atom

   (* subscripting *)
 | LetProject of loc * var * ty * atom * var * exp
 | LetSubscript of loc * var * ty * var * ty * atom * exp

   (* Assignment *)
 | Set of loc * atom * ty * atom * exp
 | LetAddrOf of loc * var * ty * atom * exp

   (* Documentation *)
 | Debug of loc * debug_info * exp

(*
 * Initialization.
 *)
and init =
   InitNone
 | InitAtom of loc * atom
 | InitArray of loc * (label option * init) list

(*
 * A type declaration has:
 *    1. a type name
 *    2. a definition
 *)
and types = (ty_var * label * ty) list

(*
 * A function definition has:
 *    1. a name
 *    2. source location
 *    3. flag for function class
 *    4. its type
 *    5. its arguments
 *    6. the body
 *)
and fundef = loc * var * label * loc * var_class * ty * var list * exp

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
