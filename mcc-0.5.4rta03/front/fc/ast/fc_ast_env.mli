(*
 * Standard FIR environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_config

open Fc_ast
open Fc_ast_pos

(*
 * Environments are abstract.
 *)
type tenv = ty SymbolTable.t
type venv = ty SymbolTable.t

(*
 * Type environment operations.
 *)
val tenv_empty : tenv
val tenv_add : tenv -> var -> ty -> tenv
val tenv_lookup : tenv -> pos -> var -> ty
val tenv_fold : ('a -> var -> ty -> 'a) -> 'a -> tenv -> 'a

(*
 * Variable environment operations.
 *)
val venv_empty : venv
val venv_add : venv -> var -> ty -> venv
val venv_mem : venv -> var -> bool
val venv_lookup : venv -> pos -> var -> ty
val venv_lookup_opt : venv -> var -> ty option

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
