(*
 * Type inference.  This pass eliminates all TyDelayed.
 *
 * Here is how it works.
 *
 *   1. Replace all TyDelayed with (TyVar v) where v is new.
 *
 *   2. Perform type unification.
 *
 *   3. It is not possible to figure out the types
 *      for subscript objects during unification (a LetSubscript
 *      might be applied to a Aggr, Tuple, Union, or Array).
 *      In the third pass, unify against the exact type.
 *
 *   4. Type inference is now complete, so perform the
 *      substitution for all the vars introduced in the
 *      first step.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fc_config

open Fc_ast
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_env
open Fc_ast_util
open Fc_ast_type
open Fc_ast_print
open Fc_ast_subst
open Fc_ast_parse
open Fc_ast_builtin
open Fc_ast_standardize

module Pos = MakePos (struct let name = "Fc_ast_infer" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * In the first pass, we collect all the function calls
 * and projections.
 *)
type call =
   { call_loc     : pos;
     call_pos     : loc;
     call_var     : var;
     call_name    : var;
     call_ty_res  : ty;
     call_ty_args : ty list;
     call_tenv    : tenv;
     call_venv    : venv;
     call_fenv    : fenv
   }

type proj =
   { proj_loc    : pos;
     proj_pos    : loc;
     proj_ty_arg : ty;
     proj_ty_res : ty;
     proj_label  : var;
     proj_tenv   : tenv;
     proj_venv   : venv
   }

type info =
   Call of call
 | Proj of proj

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Some well-known types.
 *)
let native_char_precision = FCParam.char_precision NormalPrecision
let native_int_precision = FCParam.int_precision NormalPrecision

let ty_bool pos =
   TyUnit (pos, StatusConst, 2)

let ty_int pos =
   TyInt (pos, StatusConst, native_int_precision, true)

let ty_char_ptr pos =
   TyPointer (pos, StatusNormal, TyInt (pos, StatusNormal, native_char_precision, false))

(*
 * Exceptions are a well-known type.
 *)
let exn_sym = Symbol.add "exn"
let ty_exn pos =
   TyApply (pos, StatusNormal, exn_sym, [])

(*
 * Membership in an assoc list.
 *)
let mem_assoc = List.mem_assoc

let rec rev_mem_assoc v = function
   (_, v') :: l ->
      v = v' || rev_mem_assoc v l
 | [] ->
      false

let rec all_mem_assoc v = function
   (v', v'') :: l ->
      v = v' || v = v'' || all_mem_assoc v l
 | [] ->
      false

(*
 * Equality of vars.
 *)
let rec mem_eq v1 v2 = function
   (v1', v2') :: tl ->
      (v1 = v1' && v2 = v2') || (v1 = v2' && v2 = v1') || mem_eq v1 v2 tl
 | [] ->
      false

(*
 * Add the vars to the venv (wich is a assoc list).
 *)
let fold_vars eq_vars pos vars1' vars2' =
   let rec loop eq_vars vars1 vars2 =
      match vars1, vars2 with
         v1 :: vars1, v2 :: vars2 ->
            loop ((v1, v2) :: eq_vars) vars1 vars2
       | [], [] ->
            eq_vars
       | _ ->
            raise (AstException (pos, ArityMismatch (List.length vars1', List.length vars2')))
   in
      loop eq_vars vars1' vars2'

(*
 * Expand the substitution just enough to expose the outer type.
 *)
let rec subst_expand tenv subst pos ty =
   let pos = string_pos "subst_expand" pos in
   let ty = tenv_expand tenv pos ty in
      match ty with
         TyVar (_, _, v) ->
            (try subst_expand tenv subst pos (subst_lookup subst v) with
                Not_found ->
                   ty)
       | _ ->
            ty

(*
 * Check if a TyVar resolves to a TyVar.
 *)
let rec is_poly_type subst ty =
   match ty with
      TyVar (_, _, v) ->
         (try is_poly_type subst (subst_lookup subst v) with
             Not_found ->
                true)
    | _ ->
         false

(*
 * Break apart a function type.
 *)
let rec dest_pointer_type tenv subst pos ty =
   let pos = string_pos "dest_pointer_type" pos in
      match subst_expand tenv subst pos ty with
         TyPointer (_, _, ty) ->
            ty
       | ty ->
            raise (AstException (pos, StringTypeError ("not a pointer type", ty)))

let rec dest_fun_type tenv subst pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match subst_expand tenv subst pos ty with
         TyFun (_, _, ty_args, ty_res) ->
            [], ty_args, ty_res
       | TyAll (_, _, vars, ty) ->
            let vars', ty_args, ty_res = dest_fun_type tenv subst pos ty in
               vars @ vars', ty_args, ty_res
       | ty ->
            raise (AstException (pos, StringTypeError ("not a function type", ty)))

let dest_struct_union_type tenv subst loc ty =
   match subst_expand tenv subst loc ty with
      TyStruct (_, _, Fields s_fields) ->
         s_fields
    | TyUnion (_, _, Fields u_fields) ->
         u_fields
    | TyTuple (_, _, fields) ->
         List.map (fun ty ->
               loc_of_type ty, new_symbol_string "tuple", ty, None) fields
    | TyElide (_, _, fields) ->
         List.map (fun ty ->
               loc_of_type ty, new_symbol_string "elide", ty, None) fields
    | TyStruct (_, _, Label (_, label))
    | TyUnion (_, _, Label (_, label)) ->
         raise (AstException (loc, StringVarError ("incomplete struct or union", label)))
    | ty ->
         raise (AstException (loc, StringTypeError ("not a struct or union", ty)))

let rec find_struct_field loc label = function
   (_, label', ty, _) :: fields ->
      if Symbol.eq label label' then
         ty
      else
         find_struct_field loc label fields
 | [] ->
      raise (AstException (loc, StringVarError ("unbound label", label)))

let dest_enum_type tenv subst loc ty =
   match subst_expand tenv subst loc ty with
      TyEnum (_, _, Fields e_fields) ->
         e_fields
    | TyEnum (_, _, Label (_, label)) ->
         raise (AstException (loc, StringVarError ("incomplete union", label)))
    | ty ->
         raise (AstException (loc, StringTypeError ("not an enum", ty)))

let rec dest_uenum_type tenv subst loc ty =
   match subst_expand tenv subst loc ty with
      TyUEnum (_, _, Fields (_, e_fields)) ->
         e_fields
    | TyRef (_, _, ty) ->
         dest_uenum_type tenv subst loc ty
    | TyUEnum (_, _, Label (_, label)) ->
         raise (AstException (loc, StringVarError ("incomplete union", label)))
    | ty ->
         raise (AstException (loc, StringTypeError ("not a union enum", ty)))

let dest_tuple_type tenv subst loc ty =
   match subst_expand tenv subst loc ty with
      TyTuple (_, _, fields) ->
         fields
    | ty ->
         raise (AstException (loc, StringTypeError ("not a tuple", ty)))

(************************************************************************
 * TyDelayed ELIMINATION
 ************************************************************************)

(*
 * Eliminate all TyDelayed.
 *)
let rec new_type ty =
   match ty with
      TyDelayed (pos, status) ->
         let v = new_symbol_string "ty" in
            TyVar (pos, status, v)

    | TyUnit _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _
    | TyEnum _
    | TyStruct (_, _, Label _)
    | TyUnion (_, _, Label _)
    | TyUEnum (_, _, Label _) ->
         ty

    | TyArray (pos, status, ty, a1, a2) ->
         let ty = new_type ty in
            TyArray (pos, status, ty, a1, a2)
    | TyConfArray (pos, status, ty, v1, v2, v_ty) ->
         let ty = new_type ty in
         let v_ty = new_type v_ty in
            TyConfArray (pos, status, ty, v1, v2, v_ty)

    | TyPointer (pos, status, ty) ->
         let ty = new_type ty in
            TyPointer (pos, status, ty)

    | TyRef (pos, status, ty) ->
         let ty = new_type ty in
            TyRef (pos, status, ty)

    | TyTuple (pos, status, tyl) ->
         let tyl = List.map new_type tyl in
            TyTuple (pos, status, tyl)

    | TyElide (pos, status, tyl) ->
         let tyl = List.map new_type tyl in
            TyElide (pos, status, tyl)

    | TyUEnum (pos, status, Fields (label, fields)) ->
         let fields = new_uenum_type_fields fields in
            TyUEnum (pos, status, Fields (label, fields))

    | TyUnion (pos, status, Fields fields) ->
         let fields = new_struct_type_fields fields in
            TyUnion (pos, status, Fields fields)

    | TyStruct (pos, status, Fields fields) ->
         let fields = new_struct_type_fields fields in
            TyStruct (pos, status, Fields fields)

    | TyFun (pos, status, ty_args, ty_res) ->
         let ty_args = List.map new_type ty_args in
         let ty_res = new_type ty_res in
            TyFun (pos, status, ty_args, ty_res)

    | TyLambda (pos, status, vars, ty) ->
         let ty = new_type ty in
            TyLambda (pos, status, vars, ty)

    | TyAll (pos, status, vars, ty) ->
         let ty = new_type ty in
            TyAll (pos, status, vars, ty)

    | TyApply (pos, status, v, tyl) ->
         let tyl = List.map new_type tyl in
            TyApply (pos, status, v, tyl)

and new_type_opt = function
   Some ty -> Some (new_type ty)
 | None -> None

and new_uenum_type_fields fields =
   List.map (fun (pos, v, ty_opt, label) ->
         pos, v, new_type_opt ty_opt, label) fields

and new_struct_type_fields fields =
   List.map (fun (pos, v, ty, i_opt) ->
         pos, v, new_type ty, i_opt) fields

(************************************************************************
 * TYPE QUANTIFICATION
 ************************************************************************)

(*
 * Get the free vars of a type.
 *)
let rec free_vars subst fvars ty =
   match ty with
      TyDelayed _
    | TyUnit _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyEnum _
    | TyStruct (_, _, Label _)
    | TyUnion (_, _, Label _)
    | TyUEnum (_, _, Label _) ->
         fvars
    | TyVar (_, _, v) ->
         (try free_vars subst fvars (subst_lookup subst v) with
             Not_found ->
                SymbolSet.add fvars v)

    | TyArray (_, _, ty, _, _) ->
         free_vars subst fvars ty

    | TyConfArray (_, _, ty, _, _, v_ty) ->
         let fvars = free_vars subst fvars ty in
            free_vars subst fvars v_ty

    | TyPointer (_, _, ty)
    | TyRef (_, _, ty) ->
         free_vars subst fvars ty

    | TyTuple (_, _, tyl)
    | TyElide (_, _, tyl) ->
         free_vars_list subst fvars tyl

    | TyUEnum (_, _, Fields (_, fields)) ->
         free_vars_uenum_fields subst fvars fields

    | TyUnion (_, _, Fields fields)
    | TyStruct (_, _, Fields fields) ->
         free_vars_struct_fields subst fvars fields

    | TyFun (_, _, ty_args, ty_res) ->
         let fvars = free_vars_list subst fvars ty_args in
            free_vars subst fvars ty_res

    | TyLambda (_, _, vars, ty)
    | TyAll (_, _, vars, ty) ->
         let fvars = free_vars subst fvars ty in
            SymbolSet.subtract_list fvars vars

    | TyApply (_, _, _, tyl) ->
         free_vars_list subst fvars tyl

and free_vars_opt subst fvars = function
   Some ty -> free_vars subst fvars ty
 | None -> fvars

and free_vars_list subst fvars tyl =
   List.fold_left (free_vars subst) fvars tyl

and free_vars_uenum_fields subst fvars fields =
   List.fold_left (fun fvars (_, _, ty_opt, _) ->
         free_vars_opt subst fvars ty_opt) fvars fields

and free_vars_struct_fields subst fvars fields =
   List.fold_left (fun fvars (_, _, ty, _) ->
         free_vars subst fvars ty) fvars fields


(*
 * Quantify all vars that are free.
 *)
let quantify subst ty =
   let pos = loc_of_type ty in
   let vars = free_vars subst SymbolSet.empty ty in
   let vars = SymbolSet.to_list vars in
      TyAll (pos, StatusNormal, vars, ty)

(************************************************************************
 * UNIFICATION
 ************************************************************************)

(*
 * Unify classes.
 * We want to know how many types were coerced during unification.
 * We keep a count of:
 *    specializations: number of times the left arg was specialized
 *    unspecializations: number of time the right arg was specialized
 *    losses: number of times a coercion happened.
 *)
type unify_info =
   { unify_specialize : int;
     unify_unspecialize : int;
     unify_lossy_coercions : int;
     unify_lossless_coercions : int
   }

(*
 * Initial info.
 *)
let unify_empty =
   { unify_specialize = 0;
     unify_unspecialize = 0;
     unify_lossy_coercions = 0;
     unify_lossless_coercions = 0
   }

let unify_is_exact { unify_specialize = specialize;
                     unify_unspecialize = unspecialize;
                     unify_lossy_coercions = lossy;
                     unify_lossless_coercions = lossless
    } =
   specialize = 0 && unspecialize = 0 && lossy = 0 && lossless = 0

let unify_does_coerce
    { unify_lossy_coercions = lossy1;
                        unify_lossless_coercions = lossless1
    }
    { unify_lossy_coercions = lossy2;
      unify_lossless_coercions = lossless2
    } =
   lossy1 <> lossy2 || lossless1 <> lossless2

(*
 * One unification is "better than" another, for the purpose of
 * overloading, if:
 *    1. it has fewer lossy coercions
 *    2. otherwise, it has fewer lossless coercions
 *    3. otherwise, it has fewer specializations
 *    4. otherwise, it has fewer unspecializations.
 *)
let unify_compare info1 info2 =
   let { unify_specialize = spec1;
         unify_unspecialize = unspec1;
         unify_lossy_coercions = losses1;
         unify_lossless_coercions = unlosses1
       } = info1
   in
   let { unify_specialize = spec2;
         unify_unspecialize = unspec2;
         unify_lossy_coercions = losses2;
         unify_lossless_coercions = unlosses2
       } = info2
   in
   let spec = spec1 - spec2 in
   let unspec = unspec1 - unspec2 in
   let losses = losses1 - losses2 in
   let unlosses = unlosses1 - unlosses2 in
      if losses <> 0 then
         losses
      else if unlosses <> 0 then
         unlosses
      else if spec <> 0 then
         spec
      else
         unspec

(*
 * Manipulate statistics.
 *)
let unify_specialize info =
   { info with unify_specialize = succ info.unify_specialize }

let unify_unspecialize info =
   { info with unify_unspecialize = succ info.unify_unspecialize }

let unify_lossy info =
   { info with unify_lossy_coercions = succ info.unify_lossy_coercions }

let unify_lossless info =
   { info with unify_lossless_coercions = succ info.unify_lossless_coercions }

(*
 * Unify two types.
 * In addition to a substitution, we calculate statistics.
 *)
let rec unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 =
   let pos = string_pos "unify_types" pos in
   let pos =
      if debug Fir_state.debug_pos then
         error_pos (TypeError4 (ty1, ty2, subst_type subst ty1, subst_type subst ty2)) pos
      else
         pos
   in
      match ty1, ty2 with
         (* BUG: we are just hoping zero will be resolved later *)
         TyVar _, TyZero _
       | TyZero _, TyVar _ ->
            info, subst

         (* Variables *)
       | TyVar (_, _, v1), TyVar (_, _, v2) ->
            let pos = string_pos "var_var" pos in
               if v1 = v2 || mem_eq v1 v2 eq_vars then
                  info, subst
               else if all_mem_assoc v1 eq_vars || all_mem_assoc v2 eq_vars then
                  raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)))
               else
                  (try
                      let ty1 = subst_lookup subst v1 in
                         unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
                   with
                      Not_found ->
                         try
                            let ty2 = subst_lookup subst v2 in
                               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
                         with
                            Not_found ->
                               let info = unify_specialize info in
                               let subst = subst_add subst v1 ty2 in
                                  info, subst)
       | TyVar (_, _, v1), _ ->
            let pos = string_pos "var_any" pos in
               if all_mem_assoc v1 eq_vars then
                  raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
               (try
                   let ty1 = subst_lookup subst v1 in
                      unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
                with
                   Not_found ->
                      let info = unify_specialize info in
                      let subst = subst_add subst v1 ty2 in
                         info, subst)
       | _, TyVar (_, _, v2) ->
            let pos = string_pos "any_var" pos in
               if all_mem_assoc v2 eq_vars then
                  raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
               (try
                   let ty2 = subst_lookup subst v2 in
                      unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
                with
                   Not_found ->
                      let info = unify_unspecialize info in
                      let subst = subst_add subst v2 ty1 in
                         info, subst)

         (*
          * Unit (enumeration) types.
          *)
       | TyUnit (_, _, n1), TyUnit (_, _, n2) ->
            let info =
               if n1 = n2 then
                  info
               else if n1 < n2 then
                  unify_lossy info
               else
                  unify_lossless info
            in
               info, subst

         (*
          * We allow all the scalar types to unify.
          * Coercions are handled in the second stage.
          *)
       | TyField (_, _, pre1, signed1, off1, len1), TyField (_, _, pre2, signed2, off2, len2) ->
            let info =
               if pre1 = pre2 then
                  if signed1 = signed2 then
                     info
                  else if signed1 then
                     unify_lossy info
                  else
                     unify_lossless info
               else
                  match pre1, pre2 with
                     Rawint.Int8, _
                   | Rawint.Int16, Rawint.Int32
                   | Rawint.Int16, Rawint.Int64
                   | Rawint.Int32, Rawint.Int64 ->
                        unify_lossy info
                   | _ ->
                        unify_lossless info
            in
               info, subst

       | TyInt  (_, _, pre1, signed1), TyInt  (_, _, pre2, signed2) ->
            let info =
               if pre1 = pre2 then
                  if signed1 = signed2 then
                     info
                  else if signed1 then
                     unify_lossy info
                  else
                     unify_lossless info
               else
                  match pre1, pre2 with
                     Rawint.Int8, _
                   | Rawint.Int16, Rawint.Int32
                   | Rawint.Int16, Rawint.Int64
                   | Rawint.Int32, Rawint.Int64 ->
                        unify_lossy info
                   | _ ->
                        unify_lossless info
            in
               info, subst

       | TyEnum (_, _, Fields fields1), TyEnum (_, _, Fields fields2) ->
            let pos = string_pos "unify_enum" pos in
            let subst = unify_enum_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 in
               info, subst

       | TyFloat (_, _, pre1), TyFloat (_, _, pre2) ->
            let info =
               if pre1 = pre2 then
                  info
               else
                  match pre1, pre2 with
                     Rawfloat.Single, _
                   | Rawfloat.Double, Rawfloat.LongDouble ->
                        unify_lossy info
                   | _ ->
                        unify_lossless info
            in
               info, subst

         (* Zero coercions are always lossless *)
       | TyInt _, TyZero _
       | TyFloat _, TyZero _ ->
            info, subst

       | TyUnit _, TyField _
       | TyUnit _, TyInt _
       | TyUnit _, TyEnum _
       | TyUnit _, TyFloat _
       | TyField _, TyInt _
       | TyField _, TyEnum _
       | TyField _, TyFloat _
       | TyEnum _, TyField _
       | TyEnum _, TyInt _
       | TyEnum _, TyFloat _
       | TyInt _, TyFloat _ ->
            unify_lossy info, subst
       | TyField _, TyUnit _
       | TyInt _, TyUnit _
       | TyInt _, TyField _
       | TyInt _, TyEnum _
       | TyFloat _, TyUnit _
       | TyFloat _, TyField _
       | TyFloat _, TyInt _
       | TyFloat _, TyEnum _ ->
            unify_lossless info, subst

         (* Trivial polymorphism *)
       | TyAll (_, _, [], ty1), _
       | TyLambda (_, _, [], ty1), _ ->
            unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
       | _, TyAll (_, _, [], ty2)
       | _, TyLambda (_, _, [], ty2) ->
            unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2

         (* Functions: swap the vars to get subtyping right *)
       | TyFun (_, _, ty_vars1, ty_res1), TyFun (_, _, ty_vars2, ty_res2) ->
            let pos = string_pos "unify_fun" pos in
            let info, subst = unify_type_lists ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty_vars2 ty_vars1 in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty_res1 ty_res2

         (* Arrays: elements must be exact, dimensions don't matter *)
       | TyArray (pos1, _, ty1, a1, aa1), TyArray (pos2, _, ty2, a2, aa2) ->
            let pos = string_pos "unify_array" pos in
            let ty_a1 = type_of_atom venv pos a1 in
            let ty_a2 = type_of_atom venv pos a2 in
            let ty_aa1 = type_of_atom venv pos aa1 in
            let ty_aa2 = type_of_atom venv pos aa2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids (ty_int pos1) ty_a1 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids (ty_int pos2) ty_a2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids (ty_int pos1) ty_aa1 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids (ty_int pos2) ty_aa2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               info, subst

         (* Conformant arrays *)
       | TyConfArray (pos1, _, ty1, v1, vv1, v_ty1), TyConfArray (pos2, _, ty2, v2, vv2, v_ty2) ->
            let pos = string_pos "unify_conf_array" pos in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids v_ty1 v_ty2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               info, subst
       | TyConfArray (pos1, _, ty1, v1, vv1, v_ty1), TyArray (pos2, _, ty2, a1, a2) ->
            let pos = string_pos "unify_conf_array" pos in
            let ty_a1 = type_of_atom venv pos a1 in
            let ty_a2 = type_of_atom venv pos a2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               info, subst
       | TyArray (pos1, _, ty1, a1, a2), TyConfArray (pos2, _, ty2, v1, v2, v_ty) ->
            let pos = string_pos "unify_conf_array" pos in
            let ty_a1 = type_of_atom venv pos a1 in
            let ty_a2 = type_of_atom venv pos a2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids (ty_int pos1) ty_a1 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids (ty_int pos2) ty_a2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               info, subst

       | TyPointer (_, _, ty1), TyPointer (_, _, ty2) ->
            let pos = string_pos "unify_pointer" pos in
               unify_types_nocoerce ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
       | TyPointer (_, _, ty1), TyArray (pos2, _, ty2, a2, aa2) ->
            let pos = string_pos "unify_pointer_array" pos in
            let ty_a2 = type_of_atom venv pos a2 in
            let ty_aa2 = type_of_atom venv pos aa2 in
            let info, subst = unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids (ty_int pos2) ty_a2 in
            let info, subst = unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids (ty_int pos2) ty_aa2 in
            let info, subst = unify_types_nocoerce ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 in
               info, subst
         (* Pointer <-> ConfArray *)
       | TyPointer (_, _, ty1), TyConfArray (pos2, _, ty2, v1, v2, v_ty) ->
            let pos = string_pos "unify_pointer_conf_array" pos in
            let info, subst = unify_types_nocoerce ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 in
               info, subst
       | TyPointer (_, _, ty1), TyFun _
       | TyPointer (_, _, ty1), TyAll _ ->
            let pos = string_pos "unify_pointer_fun" pos in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
       | TyFun _, TyPointer (_, _, ty2)
       | TyAll _, TyPointer (_, _, ty2) ->
            let pos = string_pos "unify_fun_pointer" pos in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2

         (* Zero can always be coerced to a pointer *)
       | TyPointer _, TyZero _ ->
            info, subst

         (* References can be coerced *)
       | TyRef (_, _, ty1), TyRef (_, _, ty2) ->
            let pos = string_pos "unify_ref_ref" pos in
               unify_types_nocoerce ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
       | TyRef (_, _, ty1), ty2 ->
            let pos = string_pos "unify_ref_any" pos in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2
       | ty1, TyRef (_, _, ty2) ->
            let pos = string_pos "unify_any_ref" pos in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2

         (* Tuple types *)
       | TyTuple (_, _, tyl1), TyTuple (_, _, tyl2) ->
            let pos = string_pos "unify_tuple" pos in
            let subst = unify_type_lists_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids tyl1 tyl2 in
               info, subst
       | TyElide (_, _, tyl1), TyElide (_, _, tyl2) ->
            let pos = string_pos "unify_elide" pos in
            let subst = unify_type_lists_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids tyl1 tyl2 in
               info, subst

         (* Structs, unions, and enumerations *)
       | TyStruct (_, _, Fields fields1), TyStruct (_, _, Fields fields2) ->
            let pos = string_pos "unify_struct" pos in
            let subst = unify_struct_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 in
               info, subst
       | TyUnion (_, _, Fields fields1), TyUnion (_, _, Fields fields2) ->
            let pos = string_pos "unify_struct" pos in
            let subst = unify_struct_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 in
               info, subst
       | TyUEnum (_, _, Fields (label1, fields1)), TyUEnum (_, _, Fields (label2, fields2)) ->
            let pos = string_pos "unify_enum" pos in
               if Symbol.eq label1 label2 then
                  let subst = unify_uenum_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 in
                     info, subst
               else
                  raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)))
       | TyStruct (_, _, Label (_, label1)), TyStruct (_, _, Label (_, label2))
       | TyUnion (_, _, Label (_, label1)), TyUnion (_, _, Label (_, label2))
       | TyUEnum (_, _, Label (_, label1)), TyUEnum (_, _, Label (_, label2))
       | TyEnum (_, _, Label (_, label1)), TyEnum (_, _, Label (_, label2)) ->
            info, subst

         (* Lambdas *)
       | TyLambda (_, _, vars1, ty1), TyLambda (_, _, vars2, ty2) ->
            let pos = string_pos "unify_poly" pos in
            let eq_vars = fold_vars eq_vars pos vars1 vars2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               info, subst

       | TyAll (_, _, vars1, ty1), TyAll (_, _, vars2, ty2) ->
            let pos = string_pos "unify_poly" pos in
            let eq_vars = fold_vars eq_vars pos vars1 vars2 in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               info, subst

         (* Defined types *)
       | TyApply (_, _, v1, args1), TyApply (_, _, v2, args2) ->
            let pos = string_pos "unify_apply_apply" pos in
               if v1 = v2 || mem_eq v1 v2 eq_ids then
                  unify_type_lists ty1' ty2' tenv venv subst info pos eq_vars eq_ids args1 args2
               else if List.length args1 = List.length args2 then
                  let eq_ids = (v1, v2) :: eq_ids in
                     unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids (**)
                        (apply_type tenv pos v1 args1)
                        (apply_type tenv pos v2 args2)
               else
                  unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids (**)
                     (apply_type tenv pos v1 args1) ty2
       | TyApply (_, _, v1, args1), _ ->
            let pos = string_pos "unify_apply_any" pos in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids (**)
                  (apply_type tenv pos v1 args1) ty2
       | _, TyApply (_, _, v2, args2) ->
            let pos = string_pos "unify_any_apply" pos in
               unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids (**)
                  ty1 (apply_type tenv pos v2 args2)

         (* All others fail *)
       | _ ->
            raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)))

(*
 * Exact matches.
 *)
and unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 =
   let info, subst = unify_types ty1' ty2' tenv venv subst unify_empty pos eq_vars eq_ids ty1 ty2 in
      if not (unify_is_exact info) then
         raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
      subst

and unify_types_nocoerce ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 =
   let info', subst = unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 in
      if unify_does_coerce info info' then
         raise (AstException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
      info', subst

and unify_type_lists_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids tyl1 tyl2 =
   let info, subst = unify_type_lists ty1' ty2' tenv venv subst unify_empty pos eq_vars eq_ids tyl1 tyl2 in
      if not (unify_is_exact info) then
         raise (AstException (pos, TypeError2 (subst_type subst ty1', subst_type subst ty2')));
      subst

(*
 * List unification.
 *)
and unify_type_lists ty1' ty2' tenv venv subst info pos eq_vars eq_ids tyl1 tyl2 =
   let pos = string_pos "unify_type_lists" pos in
      match tyl1, tyl2 with
         ty1 :: tyl1, ty2 :: tyl2 ->
            let info, subst = unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 in
               unify_type_lists ty1' ty2' tenv venv subst info pos eq_vars eq_ids tyl1 tyl2
       | [], [] ->
            info, subst
       | ty :: _, [] ->
            raise (AstException (pos, StringTypeError ("this type is not present", ty)))
       | [], ty :: _ ->
            raise (AstException (pos, StringTypeError ("too many types", ty)))

(*
 * List unification.
 *)
and unify_type_lists_short ty1' ty2' tenv venv subst info pos eq_vars eq_ids tyl1 tyl2 =
   let pos = string_pos "unify_type_lists_short" pos in
      match tyl1, tyl2 with
         ty1 :: tyl1, ty2 :: tyl2 ->
            let info, subst = unify_types ty1' ty2' tenv venv subst info pos eq_vars eq_ids ty1 ty2 in
               unify_type_lists_short ty1' ty2' tenv venv subst info pos eq_vars eq_ids tyl1 tyl2
       | [], _ ->
            info, subst
       | ty :: _, [] ->
            raise (AstException (pos, StringTypeError ("this type is not present", ty)))

(*
 * Struct field unification.
 *)
and unify_struct_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 =
   let pos = string_pos "unify_struct_fields" pos in
      match fields1, fields2 with
         (_, label1, ty1, i_opt1) :: fields1, (_, label2, ty2, i_opt2) :: fields2 ->
            if label1 <> label2 then
               raise (AstException (pos, FieldsDontMatch (label1, label2)));
            let _ =
               match i_opt1, i_opt2 with
                  Some _, None
                | None, Some _ ->
                     raise (AstException (pos, StructFieldLengthsDontMatch (i_opt1, i_opt2)))
                | Some i1, Some i2 when i1 <> i2 ->
                     raise (AstException (pos, StructFieldLengthsDontMatch (i_opt1, i_opt2)))
                | _ ->
                     ()
            in
            let subst = unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2 in
               unify_struct_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2
       | [], [] ->
            subst
       | field1 :: _, [] ->
            raise (AstException (pos, StringStructFieldError ("this field is not present", field1)))
       | [], field2 :: _ ->
            raise (AstException (pos, StringStructFieldError ("too many fields", field2)))

(*
 * Enum field unification.
 *)
and unify_enum_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 =
   let pos = string_pos "unify_struct_fields" pos in
      match fields1, fields2 with
         (_, label1, i1) :: fields1, (_, label2, i2) :: fields2 ->
            if label1 <> label2 then
               raise (AstException (pos, FieldsDontMatch (label1, label2)));
            if i1 <> i2 then
               raise (AstException (pos, EnumFieldValuesDontMatch (i1, i2)));
            unify_enum_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2
       | [], [] ->
            subst
       | field1 :: _, [] ->
            raise (AstException (pos, StringEnumFieldError ("this field is not present", field1)))
       | [], field2 :: _ ->
            raise (AstException (pos, StringEnumFieldError ("too many fields", field2)))

(*
 * Union-enum field unification.
 * The fields that have the same labels should have the same types.
 *)
and unify_uenum_fields ty1' ty2' tenv venv subst pos eq_vars eq_ids fields1 fields2 =
   let pos = string_pos "unify_struct_fields" pos in
   let rec search2 label = function
      (_, label2, ty_opt2, name2) :: fields2 ->
         if Symbol.eq label label2 then
            ty_opt2, name2
         else
            search2 label fields2
    | [] ->
         raise Not_found
   in
   let rec collect subst = function
      (_, label1, ty_opt1, name1) :: fields1 ->
         (try
             let ty_opt2, name2 = search2 label1 fields2 in
                if name1 <> name2 then
                   raise (AstException (pos, FieldsDontMatch (name1, name2)));
                let subst =
                   match ty_opt1, ty_opt2 with
                      Some ty1, Some ty2 ->
                         unify_types_exact ty1' ty2' tenv venv subst pos eq_vars eq_ids ty1 ty2
                    | Some _, None
                    | None, Some _ ->
                         raise (AstException (pos, UEnumFieldsDontMatch (label1, ty_opt1, label1, ty_opt2)))
                    | None, None ->
                         subst
                in
                   collect subst fields1
          with
             Not_found ->
                collect subst fields1)
    | [] ->
         subst
   in
      collect subst fields1

(*
 * Wrap toplevel calls.
 *)
let unify_types_info tenv venv subst pos ty1 ty2 =
   unify_types ty1 ty2 tenv venv subst unify_empty pos [] [] ty1 ty2

let unify_types tenv venv subst pos ty1 ty2 =
   snd (unify_types_info tenv venv subst pos ty1 ty2)

let unify_types_opt tenv venv subst pos ty1 ty2 =
   match ty1 with
      Some ty1 -> unify_types tenv venv subst pos ty1 ty2
    | None -> subst

let unify_type_lists_info tenv venv subst pos tyl1 tyl2 =
   let pos = string_pos "unify_type_lists" pos in
      match tyl1, tyl2 with
         ty1 :: _, ty2 :: _ ->
            unify_type_lists ty1 ty2 tenv venv subst unify_empty pos [] [] tyl1 tyl2
       | [], [] ->
            unify_empty, subst
       | _ ->
            raise (AstException (pos, ArityMismatch (List.length tyl1, List.length tyl2)))

let unify_type_lists_short_info tenv venv subst pos tyl1 tyl2 =
   let pos = string_pos "unify_type_lists" pos in
      match tyl1, tyl2 with
         ty1 :: _, ty2 :: _ ->
            unify_type_lists_short ty1 ty2 tenv venv subst unify_empty pos [] [] tyl1 tyl2
       | [], [] ->
            unify_empty, subst
       | _ ->
            raise (AstException (pos, ArityMismatch (List.length tyl1, List.length tyl2)))

let unify_type_lists tenv venv subst pos tyl1 tyl2 =
   let pos = string_pos "unify_type_lists" pos in
      snd (unify_type_lists_info tenv venv subst pos tyl1 tyl2)

(*
 * Allow the use of varargs.
 *)
let unify_type_apply_info tenv venv subst make pos tyl1 tyl2 =
   let pos = string_pos "unify_type_apply_info" pos in
      match tyl1 with
         [ty] when is_elide_type tenv pos ty ->
            let tyl1 = dest_elide_type tenv pos ty in
            let info, subst = unify_type_lists_short_info tenv venv subst pos (List.tl tyl1) tyl2 in
            let make = fenv_stdargs make in
               info, subst, make
       | _ ->
            let info, subst = unify_type_lists_info tenv venv subst pos tyl1 tyl2 in
               info, subst, make

(************************************************************************
 * INFERENCE UTILITIES
 ************************************************************************)

(*
 * Unify the atom list with the type list.
 *)
let unify_atom_list tenv venv subst loc tyl args =
   let len1 = List.length tyl in
   let len2 = List.length args in
      if len1 <> len2 then
         raise (AstException (loc, ArityMismatch (len1, len2)));
      List.fold_left2 (fun subst ty a ->
            let ty' = type_of_atom venv loc a in
               unify_types tenv venv subst loc ty ty') subst tyl args

(*
 * Partition a field list by labels.
 * The result is a pair of lists.  The
 * first list is the initial sequence of unlabeled fields,
 * the second is a list of fields starting with a label.
 *)
let rec partition_struct_pattern_fields = function
   field :: fields ->
      let unlabeled, labeled = partition_struct_pattern_fields fields in
         (match field with
             Some v, p ->
                [], (v, p :: unlabeled) :: labeled
           | None, p ->
                p :: unlabeled, labeled)
 | [] ->
      [], []

(*
 * Find a struct field by label.
 * If there are unlabeled fields, don't bother with the labeled.
 *)
let find_struct_pattern_field label = function
   (p :: unlabeled), labeled ->
      Some p, (unlabeled, labeled)
 | [], labeled ->
      let rec search = function
         (v, p :: patterns) as hd :: labeled ->
            if Symbol.eq v label then
               Some p, patterns, labeled
            else
               let p, patterns, labeled = search labeled in
               let labeled = hd :: labeled in
                  p, patterns, labeled
       | (v, []) :: labeled ->
            raise (Invalid_argument "find_struct_pattern_field")
       | [] ->
            None, [], []
      in
      let p, unlabeled, labeled = search labeled in
         p, (unlabeled, labeled)

(*
 * Find an enumeration field.
 *)
let rec find_uenum_field loc label = function
   (_, v, ty_opt, _) :: fields ->
      if Symbol.eq v label then
         ty_opt
      else
         find_uenum_field loc label fields
 | [] ->
      raise (AstException (loc, StringVarError ("union enum field is not found", label)))

(*
 * Infer a pattern.
 * The pattern may contain binding occurrences,
 * so add them to the venv.
 *)
let rec infer_pattern tenv venv subst loc ty p =
   let loc = string_pos "infer_pattern" loc in
      match p with
         VarPattern (pos, v, ty_p) ->
            (* Make up a new type for v *)
            let ty_p =
               match ty_p with
                  Some ty ->
                     ty
                | None ->
                     TyVar (pos, StatusNormal, new_symbol v)
            in
            let subst = unify_types tenv venv subst loc ty_p ty in
            let venv = venv_add venv v ty_p in
               subst, venv
       | IntPattern (pos, i) ->
            let pre = Rawint.precision i in
            let signed = Rawint.signed i in
            let ty_p = TyInt (pos, StatusConst, pre, signed) in
               infer_const_pattern tenv venv subst loc ty ty_p
       | FloatPattern (pos, x) ->
            let pre = Rawfloat.precision x in
            let ty_p =  TyFloat (pos, StatusConst, pre) in
               infer_const_pattern tenv venv subst loc ty ty_p
       | StringPattern (pos, pre, _) ->
            let pre = FCParam.char_precision pre in
            let ty_p = TyPointer (pos, StatusConst, TyInt (pos, StatusConst, pre, false)) in
               infer_const_pattern tenv venv subst loc ty ty_p
       | StructPattern (pos, fields) ->
            infer_struct_pattern tenv venv subst loc pos ty fields
       | VEnumPattern (pos, label, p) ->
            infer_uenum_pattern tenv venv subst loc pos ty label p
       | EnumPattern (pos, ty_e, _)
       | UEnumPattern (pos, ty_e, _, None) ->
            let subst = unify_types tenv venv subst loc ty ty_e in
               subst, venv
       | UEnumPattern (pos, ty_e, v, Some p) ->
            let subst = unify_types tenv venv subst loc ty ty_e in
               infer_uenum_pattern tenv venv subst loc pos ty v p
       | AsPattern (pos, p1, p2) ->
            let subst, venv = infer_pattern tenv venv subst loc ty p1 in
            let subst, venv = infer_pattern tenv venv subst loc ty p2 in
               subst, venv

(*
 * Infer against a constant pattern.
 *)
and infer_const_pattern tenv venv subst loc ty ty_p =
   let loc = string_pos "infer_const_pattern" loc in
   let subst = unify_types tenv venv subst loc ty_p ty in
      subst, venv

(*
 * A struct pattern may match a struct or union.
 * We allow the same patterns for structs and unions.
 * This seems silly for unions, but C semantics of unions
 * are silly anyway.  In any case, it is consistent to implement
 * unions as structs.
 *)
and infer_struct_pattern tenv venv subst loc pos ty fields =
   (* Get the defined fields *)
   let loc = string_pos "infer_struct_pattern" loc in
   let ty_fields = dest_struct_union_type tenv subst loc ty in

   (* Separate the pattern fields into groups *)
   let fields = partition_struct_pattern_fields fields in

   (* Now match them up *)
   let rec infer subst venv ty_fields fields =
      match ty_fields with
         (_, label, ty, _) :: ty_fields ->
            let field, fields = find_struct_pattern_field label fields in
            let subst, venv =
               match field with
                  Some p ->
                     infer_pattern tenv venv subst loc ty p
                | None ->
                     subst, venv
            in
               infer subst venv ty_fields fields
       | [] ->
            match fields with
               [], [] ->
                  subst, venv
             | _ ->
                  raise (AstException (loc, StringError "too many struct fields"))
   in
      infer subst venv ty_fields fields

(*
 * An enum pattern must match an enum.
 *)
and infer_uenum_pattern tenv venv subst loc pos ty label p =
   (* Get the defined fields *)
   let loc = string_pos "infer_uenum_pattern" loc in
   let ty_fields = dest_uenum_type tenv subst loc ty in

   (* Look up the enum field *)
   let ty_opt = find_uenum_field loc label ty_fields in

      (* Match the body *)
      match ty_opt with
         Some ty ->
            infer_pattern tenv venv subst loc ty p
       | None ->
            raise (AstException (loc, StringVarError ("enum does not have fields", label)))

(************************************************************************
 * TYPE INFERENCE
 ************************************************************************)

(*
 * Perform initial rudementary type inference,
 * and collect all the operator calls so that
 * we can resolve overloading.
 *)
let rec infer_exp tenv venv fenv calls subst e =
   let loc = string_pos "infer_exp" (exp_pos e) in
      match e with
         LetTypes (_, types, e) ->
            infer_types_exp tenv venv fenv calls subst loc types e
       | LetFuns (_, funs, e) ->
            infer_funs_exp tenv venv fenv calls subst loc funs e
       | LetFunDecl (pos, sc, v, label, gflag, ty, e) ->
            infer_fundecl_exp tenv venv fenv calls subst loc pos sc v label gflag ty e
       | LetCoerce (_, v, ty, a, e) ->
            infer_coerce_exp tenv venv fenv calls subst loc v ty a e
       | LetTest (pos, v, ty, a, e) ->
            infer_test_exp tenv venv fenv calls subst loc pos v ty a e
       | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
            infer_extcall_exp tenv venv fenv calls subst loc pos v ty s b ty' args e
       | LetApply (pos, v, ty, f, label, args, e) ->
            infer_apply_exp tenv venv fenv calls subst loc pos v ty f label args e
       | TailCall (_, f, args) ->
            infer_tailcall_exp tenv venv fenv calls subst loc f args
       | Return (pos, f, ty, a) ->
            infer_return_exp tenv venv fenv calls subst loc pos f ty a
       | LetString (pos, v, ty, pre, s, e) ->
            infer_string_exp tenv venv fenv calls subst loc pos v ty pre s e
       | LetAlloc (pos, v, ty, label, args, e) ->
            infer_alloc_exp tenv venv fenv calls subst loc pos v ty label args e
       | LetMalloc (pos, v, a, e) ->
            infer_malloc_exp tenv venv fenv calls subst loc pos v a e
       | IfThenElse (pos, a, e1, e2) ->
            infer_if_exp tenv venv fenv calls subst loc pos a e1 e2
       | Match (pos, a, ty, cases) ->
            infer_match_exp tenv venv fenv calls subst loc pos a ty cases
       | Try (pos, e1, v, e2) ->
            infer_try_exp tenv venv fenv calls subst loc pos e1 v e2
       | Raise (pos, a) ->
            infer_raise_exp tenv venv fenv calls subst loc pos a
       | LetProject (pos, v1, ty, a2, v3, e) ->
            infer_project_exp tenv venv fenv calls subst loc pos v1 ty a2 v3 e
       | LetSizeof (pos, v, ty1, ty2, e) ->
            infer_sizeof_type_exp tenv venv fenv calls subst loc pos v ty1 ty2 e
       | LetVar (pos, sc, v, label, gflag, ty, a_opt, e) ->
            infer_var_exp tenv venv fenv calls subst loc pos sc v label gflag ty a_opt e
       | Debug (pos, info, e) ->
            infer_debug_exp tenv venv fenv calls subst loc pos info e
       | LetAddrOf (pos, v, ty, a, e) ->
            infer_addr_of_exp tenv venv fenv calls subst loc pos v ty a e
       | LetAtom _
       | LetUnop _
       | LetBinop _
       | LetSubscript _
       | Set _ ->
            raise (AstException (loc, NotImplemented "infer_exp"))

and infer_type venv ty =
    match ty with
       TyUnit _
     | TyDelayed _
     | TyZero _
     | TyInt _
     | TyField _
     | TyFloat _
     | TyEnum _
     | TyStruct (_, _, Label _)
     | TyUnion (_, _, Label _)
     | TyUEnum (_, _, Label _) ->
         venv, ty
     | TyArray (pos, status, ty, a1, a2) ->
         let venv, ty = infer_type venv ty in
             venv, TyArray (pos, status, ty, a1, a2)
     | TyConfArray (pos, status, ty, v1, v2, v_ty) ->
         let venv, v_ty = infer_type venv v_ty in
         let venv = venv_add venv v1 v_ty in
         let venv = venv_add venv v2 v_ty in
         let venv, ty = infer_type venv ty in
             venv, TyConfArray (pos, status, ty, v1, v2, v_ty)
     | TyPointer (pos, status, ty) ->
         let venv, ty = infer_type venv ty in
             venv, TyPointer (pos, status, ty)
     | TyRef (pos, status, ty) ->
         let venv, ty = infer_type venv ty in
             venv, TyRef (pos, status, ty)
     | TyTuple (pos, status, ty_list) ->
         let venv, ty_list = infer_type_list venv ty_list in
             venv, TyTuple (pos, status, ty_list)
     | TyElide (pos, status, ty_list) ->
         let venv, ty_list = infer_type_list venv ty_list in
             venv, TyElide (pos, status, ty_list)
     | TyUEnum (pos, status, Fields (label, fields)) ->
         let venv, fields = List.fold_left (fun (venv, fields) (pos, var, ty_opt, label) ->
             let venv, ty_opt = infer_type_option venv ty_opt in
                 venv, (pos, var, ty_opt, label) :: fields) (venv, []) fields
         in
             venv, TyUEnum(pos, status, Fields (label, fields))
     | TyUnion (pos, status, Fields fields) ->
         let venv, fields = List.fold_left (fun (venv, fields) (pos, var, ty, io) ->
             let venv, ty = infer_type venv ty in
                 venv, (pos, var, ty, io) :: fields) (venv, []) fields
         in
             venv, TyUnion (pos, status, Fields fields)
     | TyStruct (pos, status, Fields fields) ->
         let venv, fields = List.fold_left (fun (venv, fields) (pos, var, ty, io) ->
             let venv, ty = infer_type venv ty in
                 venv, (pos, var, ty, io) :: fields) (venv, []) fields
         in
             venv, TyStruct (pos, status, Fields fields)
     | TyFun (pos, status, ty_list, ty) ->
         let venv, ty_list = infer_type_list venv ty_list in
         let venv, ty = infer_type venv ty in
             venv, TyFun (pos, status, ty_list, ty)
     | TyVar _ ->
         venv, ty
     | TyAll (pos, status, vars, ty) ->
         let venv, ty = infer_type venv ty in
             venv, TyAll (pos, status, vars, ty)
     | TyLambda (pos, status, vars, ty) ->
         let venv, ty = infer_type venv ty in
             venv, TyLambda (pos, status, vars, ty)
     | TyApply (pos, status, v, ty_list) ->
         let venv, ty_list = infer_type_list venv ty_list in
             venv, TyApply (pos, status, v, ty_list)

and infer_type_option venv ty =
    match ty with
       Some ty ->
          let venv, ty = infer_type venv ty in
             venv, Some ty
     | None ->
          venv, None

and infer_type_list venv ty_list =
    let venv, ty_list = List.fold_left (fun (venv, ty_list) ty ->
        let venv, ty = infer_type venv ty in
            venv, ty :: ty_list) (venv, []) ty_list
    in
        venv, ty_list

(*
 * Type definitions.
 *)
and infer_types_exp tenv venv fenv calls subst loc types e =
   let loc = string_pos "infer_types_exp" loc in
   let subst, tenv =
      List.fold_left (fun (subst, tenv) (v, label, ty) ->
            let subst =
               try
                  let ty' = SymbolTable.find tenv label in
                     unify_types tenv venv subst loc ty' ty
               with
                  Not_found ->
                     subst
            in
            let tenv = tenv_add tenv v ty in
            let tenv = tenv_add tenv label ty in
               subst, tenv) (subst, tenv) types
   in
      infer_exp tenv venv fenv calls subst e

(*
 * Function definitions.
 * First, add all the functions to the venv.
 * Then infer their bodies.
 *)
and infer_funs_exp tenv venv fenv calls subst loc funs e =
   let loc = string_pos "infer_funs_exp" loc in

   (* Add all the functions to the venv *)
   let venv, fenv, subst =
      List.fold_left (fun (venv, fenv, subst) (pos, f, label, _, _, ty_f, vars, _) ->
            let loc = loc_pos pos in
            let ty_vars =
               List.map (fun v -> TyVar (pos, StatusNormal, new_symbol v)) vars
            in
            let ty_res = TyVar (pos, StatusNormal, new_symbol f) in
            let ty_f' = TyFun (pos, StatusNormal, ty_vars, ty_res) in
            let subst = unify_types tenv venv subst loc ty_f' ty_f in
            let ty_f = quantify subst ty_f in
            let venv = venv_add venv f ty_f in
            let fenv = fenv_add fenv label f ty_f in
               venv, fenv, subst) (venv, fenv, subst) funs
   in

   (*
    * Infer their bodies.
    * Add all the vars with the argument types.
    * Add the return type to then venv.
    * Then infer the exp.
    *)
   let calls, subst =
      List.fold_left (fun (calls, subst) (pos, f, _, _, gflag, ty_f, vars, e) ->
            let loc = loc_pos pos in
            let _, ty_vars, ty_res = dest_fun_type tenv subst loc ty_f in

            (* Add all the vars *)
            let venv =
               List.fold_left2 (fun venv v ty ->
                    let venv, ty = infer_type venv ty in
                        venv_add venv v ty) venv vars ty_vars
            in

            (* Infer the body *)
            let calls, subst, ty_res' = infer_exp tenv venv fenv calls subst e in

            (* Unify with the return type *)
            let subst = unify_types tenv venv subst loc ty_res ty_res' in
               calls, subst) (calls, subst) funs
   in
      (*
       * Now that all the functions have been inferred,
       * the result type is the body expression.
       *)
      infer_exp tenv venv fenv calls subst e

(*
 * Function declaration.
 *)
and infer_fundecl_exp tenv venv fenv calls subst loc pos sc f label gflag ty e =
   let loc = string_pos "infer_fundecl_exp" loc in
   let venv = venv_add venv f ty in
   let fenv = fenv_add fenv label f ty in
      infer_exp tenv venv fenv calls subst e

(*
 * Coercion.
 * Don't worry about the exact coercion here.
 *)
and infer_coerce_exp tenv venv fenv calls subst loc v ty a e =
   let loc = string_pos "infer_coerce_op" loc in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

(*
 * Testing.
 * The result is always TyInt.
 *)
and infer_test_exp tenv venv fenv calls subst loc pos v ty a e =
   let loc = string_pos "infer_coerce_op" loc in
   let subst = unify_types tenv venv subst loc (ty_bool pos) ty in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

(*
 * External application.
 *)
and infer_extcall_exp tenv venv fenv calls subst loc pos v ty s b ty' args e =
   let loc = string_pos "infer_extcall_exp" loc in
   let ty_args = List.map (type_of_atom venv loc) args in
   let ty_fun = TyFun (pos, StatusNormal, ty_args, ty) in
   let subst = unify_types tenv venv subst loc ty' ty_fun in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

(*
 * For a function application, don't do anything now.
 * Just add the call to the call list, and continue.
 * This is a special case for the () operator, which may get unfolded.
 *)
and infer_apply_exp tenv venv fenv calls subst loc pos v ty f label args e =
   let loc = string_pos "infer_apply_exp" loc in
   let ty_args = List.map (type_of_atom venv loc) args in
   let call =
      { call_loc = loc;
        call_pos = pos;
        call_var = v;
        call_name = label;
        call_ty_res = ty;
        call_ty_args = ty_args;
        call_tenv = tenv;
        call_venv = venv;
        call_fenv = fenv
      }
   in
   let calls = Call call :: calls in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

(*
 * Tailcalls are different.
 * We know the type of the function,
 * so we can perform inference right away.
 *)
and infer_tailcall_exp tenv venv fenv calls subst loc f args =
   let loc = string_pos "infer_tailcall_exp" loc in
   let f_ty = venv_lookup venv loc f in
   let f_ty = standardize_type f_ty in
   let _, ty_args, ty_res = dest_fun_type tenv subst loc f_ty in
   let subst = unify_atom_list tenv venv subst loc ty_args args in
      calls, subst, ty_res

and infer_return_exp tenv venv fenv calls subst loc pos f ty a =
   let loc = string_pos "infer_return_exp" loc in
   let f_ty = venv_lookup venv loc f in
   let _, _, r_ty = dest_fun_type tenv subst loc f_ty in
   let a_ty = type_of_atom venv loc a in
   let subst = unify_types tenv venv subst loc r_ty ty in
   let subst = unify_types tenv venv subst loc r_ty a_ty in

   (* Make up a new type for the return *)
   let ty_v = TyVar (pos, StatusNormal, new_symbol_string "return") in
      calls, subst, ty_v

(*
 * Allocation.
 *)
and infer_string_exp tenv venv fenv calls subst loc pos v ty pre s e =
   let loc = string_pos "infer_string_exp" loc in
   let ty' = ty_string pre pos s in
   let subst = unify_types tenv venv subst loc ty ty' in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

and infer_malloc_exp tenv venv fenv calls subst loc pos v a e =
   let loc = string_pos "infer_string_exp" loc in
   let ty_a = type_of_atom venv loc a in
   let subst = unify_types tenv venv subst loc (ty_int pos) ty_a in
   let venv = venv_add venv v (ty_char_ptr pos) in
      infer_exp tenv venv fenv calls subst e

and infer_alloc_exp tenv venv fenv calls subst loc pos v ty label args e =
   let loc = string_pos "infer_alloc_exp" loc in

   (* Get the field type *)
   let ty' = dest_pointer_type tenv subst loc ty in
   let fields = dest_uenum_type tenv subst loc ty' in
   let ty_opt = find_struct_field loc label fields in

   (* Check the arguments *)
   let subst =
      match ty_opt, args with
         Some ty, [a] ->
            let ty_a = type_of_atom venv loc a in
               unify_types tenv venv subst loc ty ty_a
       | None, [] ->
            subst
       | Some _, _ ->
            raise (AstException (loc, ArityMismatch (1, List.length args)))
       | None, _ ->
            raise (AstException (loc, ArityMismatch (0, List.length args)))
   in

   (* Infer the rest *)
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

(*
 * Conditionals.
 * For if-then-else, don't worry about the type
 * of the condition right now.  We'll resolve it during the
 * overloading phase.
 *)
and infer_if_exp tenv venv fenv calls subst loc pos a e1 e2 =
   let loc = string_pos "infer_if_exp" loc in
   let calls, subst, ty1 = infer_exp tenv venv fenv calls subst e1 in
   let calls, subst, ty2 = infer_exp tenv venv fenv calls subst e2 in
   let subst = unify_types tenv venv subst loc ty1 ty2 in
      calls, subst, ty1

(*
 * In a switch statement, we resolve as many of the
 * patterns as possible.  If the patterns are integers, the
 * argument may be an enum.  If the patterns contain a StructPattern
 * the argument _must_ be a struct, and we force it early.
 *)
and infer_match_exp tenv venv fenv calls subst loc pos a ty cases =
   let loc = string_pos "infer_switch_exp" loc in
   let ty_a = type_of_atom venv loc a in
   let subst = unify_types tenv venv subst loc ty ty_a in
   let ty_res = TyVar (pos, StatusNormal, new_symbol_string "switch") in
   let calls, subst =
      List.fold_left (fun (calls, subst) (p, e) ->
            let subst, venv = infer_pattern tenv venv subst loc ty_a p in
            let calls, subst, ty' = infer_exp tenv venv fenv calls subst e in
            let subst = unify_types tenv venv subst loc ty_res ty' in
               calls, subst) (calls, subst) cases
   in
      calls, subst, ty_res

(*
 * In a try expression, the variable always has exception type.
 * The two expressions must have the same type.
 *)
and infer_try_exp tenv venv fenv calls subst loc pos e1 v e2 =
   let loc = string_pos "infer_try_exp" loc in
   let calls, subst, ty1 = infer_exp tenv venv fenv calls subst e1 in
   let venv = venv_add venv v (ty_exn pos) in
   let calls, subst, ty2 = infer_exp tenv venv fenv calls subst e2 in
   let subst = unify_types tenv venv subst loc ty1 ty2 in
      calls, subst, ty1

(*
 * The argument to the raise must have exn type.
 * The result of the raise is arbitrary.
 *)
and infer_raise_exp tenv venv fenv calls subst loc pos a =
   let loc = string_pos "infer_raise_exp" loc in
   let ty_a = type_of_atom venv loc a in
   let ty_a = dest_pointer_type tenv subst loc ty_a in
   let subst =
      match subst_expand tenv subst loc ty_a with
         TyUEnum _ ->
            subst
       | ty_a ->
            raise (AstException (loc, StringTypeError ("not a union enum", ty_a)))
   in
   let ty_res = TyVar (pos, StatusNormal, new_symbol_string "raise") in
      calls, subst, ty_res

(*
 * Projection.
 * The argument better have a struct type.
 *)
and infer_project_exp tenv venv fenv calls subst loc pos v1 ty a2 v3 e =
   let loc = string_pos "infer_project_exp" loc in
   let ty_arg = type_of_atom venv loc a2 in
   let proj =
      { proj_loc = loc;
        proj_pos = pos;
        proj_ty_arg = ty_arg;
        proj_ty_res = ty;
        proj_label = v3;
        proj_tenv = tenv;
        proj_venv = venv
      }
   in
   let calls = Proj proj :: calls in
   let venv = venv_add venv v1 ty in
      infer_exp tenv venv fenv calls subst e

(*
 * Sizeof.
 * The sizeof needs the type of expression e1.
 * We don't do anything much for now, just infer the parts.
 * The result must be an int.
 *)
and infer_sizeof_type_exp tenv venv fenv calls subst loc pos v ty1 ty2 e =
   let loc = string_pos "infer_sizeof_type_exp" loc in
   let subst = unify_types tenv venv subst loc (ty_int pos) ty1 in
   let venv = venv_add venv v ty1 in
      infer_exp tenv venv fenv calls subst e

(*
 * Variable operations.
 *)
and infer_var_exp tenv venv fenv calls subst loc pos _ v _ _ ty init e =
   let loc = string_pos "infer_var_exp" loc in
   let subst = infer_init tenv venv subst loc pos ty init in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

and infer_init tenv venv subst loc pos ty init =
   match init with
      InitNone ->
         subst
    | InitAtom (_, a) ->
         unify_types tenv venv subst loc ty (type_of_atom venv loc a)
    | InitArray (_, fields) ->
         infer_init_array tenv venv subst loc pos ty fields

and infer_init_array tenv venv subst loc pos ty fields =
   match subst_expand tenv subst loc ty with
      TyPointer _
    | TyArray _ ->
         subst
    | _ ->
         infer_init_array_struct tenv venv subst loc pos ty fields

and infer_init_array_struct tenv venv subst loc pos ty fields =
   (* Get the defined fields *)
   let ty_fields = dest_struct_union_type tenv subst loc ty in

   (* Separate the initializer fields into groups *)
   let fields = partition_struct_pattern_fields fields in

   (* Now match them up *)
   let rec infer subst venv ty_fields fields =
      match ty_fields with
         (_, label, ty, _) :: ty_fields ->
            let field, fields = find_struct_pattern_field label fields in
            let subst =
               match field with
                  Some (InitAtom (_, a)) ->
                     unify_types tenv venv subst loc ty (type_of_atom venv loc a)
                | Some (InitArray (_, fields)) ->
                     infer_init_array tenv venv subst loc pos ty fields
                | Some InitNone
                | None ->
                     subst
            in
               infer subst venv ty_fields fields
       | [] ->
            match fields with
               [], [] ->
                  subst
             | _ ->
                  raise (AstException (loc, StringError "too many struct fields"))
   in
      infer subst venv ty_fields fields

(*
 * Address-of.
 *)
and infer_addr_of_exp tenv venv fenv calls subst loc pos v ty a e =
   let loc = string_pos "infer_addr_of_exp" loc in
   let ty_a = type_of_atom venv loc a in
   let subst = unify_types tenv venv subst loc ty (TyPointer (pos, StatusNormal, ty_a)) in
   let venv = venv_add venv v ty in
      infer_exp tenv venv fenv calls subst e

(*
 * Debugging.
 * Check all the vars.
 *)
and infer_debug_exp tenv venv fenv calls subst loc pos info e =
   let loc = string_pos "infer_debug_exp" loc in
   let subst =
      match info with
         DebugString _ ->
            subst
       | DebugContext (_, vars) ->
            List.fold_left (fun subst (_, ty, v) ->
                  let ty' = venv_lookup venv loc v in
                     unify_types tenv venv subst loc ty ty') subst vars
   in
      infer_exp tenv venv fenv calls subst e

(*
 * Wrapp the outermost version.
 *)
let infer_exp exp =
   let pos = loc_of_expr exp in
   let ty_unit = TyUnit (pos, StatusNormal, 1) in
   let venv = venv_add venv_empty exit_sym (TyFun (pos, StatusNormal, [ty_unit], ty_unit)) in
   let calls, subst, _ = infer_exp tenv_empty venv fenv_empty [] subst_empty exp in
   let exp = map_type_exp (subst_type subst) exp in
      calls, exp

(************************************************************************
 * OVERLOADING
 ************************************************************************)

(*
 * Resolve all function applications.
 *)
let rec fsubst_exp fsubst e =
   match e with
      LetTypes (pos, types, e) ->
         LetTypes (pos, types, fsubst_exp fsubst e)
    | LetFuns (pos, funs, e) ->
         let funs =
            List.map (fun (pos, g, label, line, gflag, ty, vars, e) ->
                  pos, g, label, line, gflag, ty, vars, fsubst_exp fsubst e) funs
         in
            LetFuns (pos, funs, fsubst_exp fsubst e)
    | LetFunDecl (pos, sc, v, label, gflag, ty, e) ->
         LetFunDecl (pos, sc, v, label, gflag, ty, fsubst_exp fsubst e)
    | LetVar (pos, sc, v, label, gflag, ty, a_opt, e) ->
         LetVar (pos, sc, v, label, gflag, ty, a_opt, fsubst_exp fsubst e)
    | LetTest (pos, v, ty, a, e) ->
         LetTest (pos, v, ty, a, fsubst_exp fsubst e)
    | LetCoerce (pos, v, ty, a, e) ->
         LetCoerce (pos, v, ty, a, fsubst_exp fsubst e)
    | LetAtom (pos, v, ty, a, e) ->
         LetAtom (pos, v, ty, a, fsubst_exp fsubst e)
    | LetUnop (pos, v, ty, op, a, e) ->
         LetUnop (pos, v, ty, op, a, fsubst_exp fsubst e)
    | LetBinop (pos, v, ty, op, a1, a2, e) ->
         LetBinop (pos, v, ty, op, a1, a2, fsubst_exp fsubst e)
    | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
         LetExtCall (pos, v, ty, s, b, ty', args, fsubst_exp fsubst e)
    | LetApply (pos, v, ty, g, label, args, e') ->
         let loc = string_pos "fsubst_exp" (exp_pos e) in
            fsubst_apply_exp fsubst loc pos v ty g label args e'
    | TailCall _
    | Return _
    | Raise _ ->
         e
    | LetString (pos, v, ty, pre, s, e) ->
         LetString (pos, v, ty, pre, s, fsubst_exp fsubst e)
    | LetAlloc (pos, v, ty, label, args, e) ->
         LetAlloc (pos, v, ty, label, args, fsubst_exp fsubst e)
    | LetMalloc (pos, v, a, e) ->
         LetMalloc (pos, v, a, fsubst_exp fsubst e)
    | IfThenElse (pos, a, e1, e2) ->
         IfThenElse (pos, a, fsubst_exp fsubst e1, fsubst_exp fsubst e2)
    | Match (pos, a, ty, cases) ->
         let cases = List.map (fun (p, e) -> p, fsubst_exp fsubst e) cases in
            Match (pos, a, ty, cases)
    | Try (pos, e1, v, e2) ->
         Try (pos, fsubst_exp fsubst e1, v, fsubst_exp fsubst e2)
    | LetSizeof (pos, v, ty1, ty2, e) ->
         LetSizeof (pos, v, ty1, ty2, fsubst_exp fsubst e)
    | Debug (pos, info, e) ->
         Debug (pos, info, fsubst_exp fsubst e)
    | LetProject (pos, v, ty, v1, v2, e) ->
         let ty = TyRef (pos, StatusNormal, ty) in
            LetProject (pos, v, ty, v1, v2, fsubst_exp fsubst e)
    | LetSubscript (pos, v, ty, v1, ty_bag, a2, e) ->
         LetSubscript (pos, v, ty, v1, ty_bag, a2, fsubst_exp fsubst e)
    | Set (pos, a1, ty, a2, e) ->
         Set (pos, a1, ty, a2, fsubst_exp fsubst e)
    | LetAddrOf (pos, v, ty, a, e) ->
         LetAddrOf (pos, v, ty, a, fsubst_exp fsubst e)

(*
 * Replace the application with the selected function.
 *)
and fsubst_apply_exp fsubst loc pos v ty f label args e =
   let loc = string_pos "fsubst_apply_exp" loc in
   let make, tenv, ty_args =
      try SymbolTable.find fsubst v with
         Not_found ->
            raise (AstException (loc, UnboundVar v))
   in
   let e = fsubst_exp fsubst e in
      fenv_apply make tenv loc pos v ty_args ty args e

(*
 * After the initial type inference,
 * we now have a partially inferred program,
 * but overloading has not been resolved.  At this
 * point, we have a list of operator applications that we have
 * to resolve.
 *
 * The algorithm is deterministic: work from the start to the end
 * of the program, resolving operators one-by-one.
 *
 * The overloading is resolved by the "closest" match,
 * according to unification statistics.
 *)
let tabstop = 3

let print_resolve s f ty_args info ty_args' =
   if debug Fir_state.debug_print_ast then
      let buf = err_formatter in
         pp_open_hvbox buf tabstop;
         pp_print_string buf s;
         pp_print_string buf ":";
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_type ty_args;
         pp_print_string buf ")";
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "lossy=";
         pp_print_int buf info.unify_lossy_coercions;
         pp_print_string buf " lossless=";
         pp_print_int buf info.unify_lossless_coercions;
         pp_print_string buf " specializations=";
         pp_print_int buf info.unify_specialize;
         pp_print_string buf " unspecializations=";
         pp_print_int buf info.unify_unspecialize;
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_type ty_args';
         pp_print_string buf ")";
         pp_close_box buf ();
         pp_close_box buf ();
         pp_print_newline buf ()

let resolve_call fsubst subst call =
   let { call_tenv = tenv;
         call_venv = venv;
         call_fenv = fenv;
         call_ty_res = ty_res;
         call_ty_args = ty_args;
         call_name = flabel;
         call_var = v;
         call_loc = loc;
         call_pos = pos
       } = call
   in
   let loc = string_pos "resolve_call" loc in
   let funs = fenv_lookup fenv loc pos flabel ty_args ty_res in

   (* Search for a better match *)
   let rec search_info info' subst' ty_res' make' = function
      (f_ty, make'') :: funs ->
         let f_ty = standardize_type f_ty in
         let loc = string_pos "search_info" loc in
         let _, ty_args'', ty_res'' = dest_fun_type tenv subst loc f_ty in
            (try
                let info'', subst'', make'' = unify_type_apply_info tenv venv subst make'' loc ty_args'' ty_args in
                let info', subst', ty_res', make' =
                   if unify_compare info'' info' <= 0 then
                      begin
                         print_resolve "Better choice" flabel ty_args info'' ty_args'';
                         info'', subst'', ty_res'', make''
                      end
                   else
                      info', subst', ty_res', make'
                in
                   search_info info' subst' ty_res' make' funs
             with
                AstException _ ->
                   search_info info' subst' ty_res' make' funs)
    | [] ->
         subst', ty_res', make'
   in

   (* Search for a match *)
   let rec search = function
      (f_ty, make) :: funs ->
         let f_ty = standardize_type f_ty in
         let loc = string_pos "search" loc in
         let _, ty_args', ty_res' = dest_fun_type tenv subst loc f_ty in
            (try
                let info', subst', make = unify_type_apply_info tenv venv subst make loc ty_args' ty_args in
                   print_resolve "Initial choice" flabel ty_args info' ty_args';
                   search_info info' subst' ty_res' make funs
             with
                AstException _ ->
                   search funs)
    | [] ->
         let funs = List.map fst funs in
         let ty_args = List.map (subst_type subst) ty_args in
            raise (AstException (loc, StringVarTypeListError ("unbound function", flabel, ty_args, funs)))
   in

   (* Find the match and get the substitution *)
   let subst, ty_res', make = search funs in
   let subst = unify_types tenv venv subst loc ty_res ty_res' in
   let fsubst = SymbolTable.add fsubst v (make, tenv, ty_args) in
      fsubst, subst

(*
 * If all arguments are polymorphic, delay the resolution.
 *)
let rec resolve_poly_call fsubst subst = function
   call :: calls ->
      let { call_ty_args = ty_args } = call in
         if List.for_all (is_poly_type subst) ty_args then
            let fsubst, subst, calls, changed = resolve_poly_call fsubst subst calls in
               fsubst, subst, call :: calls, changed
         else
            let fsubst, subst = resolve_call fsubst subst call in
               fsubst, subst, calls, true
 | [] ->
      fsubst, subst, [], false

(*
 * Same thing, but for projections.
 *)
let resolve_proj subst proj =
   let { proj_tenv = tenv;
         proj_venv = venv;
         proj_ty_res = ty_res;
         proj_ty_arg = ty_arg;
         proj_label = label;
         proj_loc = loc;
         proj_pos = pos
       } = proj
   in
   let loc = string_pos "resolve_proj" loc in

   (* Find the named field *)
   let rec field_lookup = function
      (_, label', ty, _) :: fields ->
         if Symbol.eq label' label then
            ty
         else
            field_lookup fields
    | [] ->
         raise (AstException (loc, UnboundLabel label))
   in

   (* Supposed to be a pointer type *)
   let ty_arg = dest_pointer_type tenv subst loc ty_arg in

      (* Argument must be a struct or union type *)
      match subst_expand tenv subst loc ty_arg with
         TyUnion (_, _, Fields fields)
       | TyStruct (_, _, Fields fields) ->
            (* Look up the label and do the substitution *)
            let ty_res' = field_lookup fields in
               unify_types tenv venv subst loc ty_res ty_res'

       | TyUnion (_, _, Label _)
       | TyStruct (_, _, Label _) ->
            raise (AstException (loc, StringVarError ("incomplete struct/union type", label)))

       | ty ->
            raise (AstException (loc, StringTypeError ("not a union type", ty)))

(*
 * If all arguments are polymorphic, delay the resolution.
 *)
let rec resolve_poly_proj subst = function
   proj :: projs ->
      let { proj_ty_arg = ty_arg } = proj in
         if is_poly_type subst ty_arg then
            let subst, projs, changed = resolve_poly_proj subst projs in
               subst, proj :: projs, changed
      else
         let subst = resolve_proj subst proj in
            subst, projs, true
 | [] ->
      subst, [], false

(*
 * Keep resolving until we reach a fixpoint.
 *)
let rec resolve_fixpoint fsubst subst calls projs =
   let subst, projs, changed1 = resolve_poly_proj subst projs in
   let fsubst, subst, calls, changed2 = resolve_poly_call fsubst subst calls in
   let changed = changed1 || changed2 in
      match calls, projs, changed with
         [], [], _ ->
            fsubst, subst
       | _, _, true ->
            resolve_fixpoint fsubst subst calls projs
       | call :: calls, _, false ->
            (* Print a warning message *)
            let { call_name = name; call_pos = pos } = call in
            let _ =
               if calls <> [] then
                  let buf = err_formatter in
                     pp_print_location buf pos;
                     pp_print_space buf ();
                     pp_print_string buf "warning: specializing ";
                     pp_print_symbol buf name;
                     pp_print_newline buf ()
            in
            let fsubst, subst = resolve_call fsubst subst call in
               resolve_fixpoint fsubst subst calls projs
       | [], { proj_loc = loc; proj_ty_arg = ty } :: _, false ->
            (* Can't resolve this projection *)
            raise (AstException (loc, StringTypeError ("not a struct or union type2", ty)))

(*
 * Resolve all the calls in the program.
 *)
let resolve_exp exp calls =
   let calls, projs =
      List.fold_left (fun (calls, projs) info ->
            match info with
               Call call ->
                  call :: calls, projs
             | Proj proj ->
                  calls, proj :: projs) ([], []) calls
   in
   let fsubst, subst = resolve_fixpoint SymbolTable.empty subst_empty calls projs in
   let exp = map_type_exp (subst_type subst) exp in
   let fsubst =
      SymbolTable.map (fun (make, tenv, ty_args) ->
            make, tenv, List.map (subst_type subst) ty_args) fsubst
   in
      fsubst_exp fsubst exp

(************************************************************************
 * LINKING
 ************************************************************************)

(*
 * Rename the atom if in the env.
 *)
let link_var fenv v =
   try SymbolTable.find fenv v with
      Not_found ->
         v

let link_atom fenv a =
   match a with
      AtomVar (pos, v) ->
         AtomVar (pos, link_var fenv v)
    | _ ->
         a

let link_atom_opt fenv = function
   Some a -> Some (link_atom fenv a)
 | None -> None

let link_atoms fenv args =
   List.map (link_atom fenv) args

let rec link_init fenv = function
   InitNone ->
      InitNone
 | InitAtom (pos, a) ->
      InitAtom (pos, link_atom fenv a)
 | InitArray (pos, fields) ->
      let fields =
         List.map (fun (v_opt, init) ->
               v_opt, link_init fenv init) fields
      in
         InitArray (pos, fields)

(*
 * Once type inference is done,
 * link function declarations with functions that have the
 * same types.
 *)
let rec link_exp tenv venv fenv decls e =
   let loc = string_pos "link_exp" (exp_pos e) in
      match e with
         LetTypes (pos, types, e) ->
            link_types_exp tenv venv fenv decls loc pos types e
       | LetFuns (pos, funs, e) ->
            link_funs_exp tenv venv fenv decls loc pos funs e
       | LetFunDecl (pos, sc, v, label, gflag, ty, e) ->
            link_fundecl_exp tenv venv fenv decls loc pos sc v label gflag ty e
       | LetVar (pos, sc, v, label, gflag, ty, init, e) ->
            LetVar (pos, sc, v, label, gflag, ty, link_init fenv init, link_exp tenv venv fenv decls e)
       | LetTest (pos, v, ty, a, e) ->
            LetTest (pos, v, ty, link_atom fenv a, link_exp tenv venv fenv decls e)
       | LetCoerce (pos, v, ty, a, e) ->
            LetCoerce (pos, v, ty, link_atom fenv a, link_exp tenv venv fenv decls e)
       | LetAtom (pos, v, ty, a, e) ->
            LetAtom (pos, v, ty, link_atom fenv a, link_exp tenv venv fenv decls e)
       | LetUnop (pos, v, ty, op, a, e) ->
            LetUnop (pos, v, ty, op, link_atom fenv a, link_exp tenv venv fenv decls e)
       | LetBinop (pos, v, ty, op, a1, a2, e) ->
            LetBinop (pos, v, ty, op, link_atom fenv a1, link_atom fenv a2, link_exp tenv venv fenv decls e)
       | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
            LetExtCall (pos, v, ty, s, b, ty', link_atoms fenv args, e)
       | LetApply (pos, v, ty, g, label, args, e) ->
            LetApply (pos, v, ty, g, label, link_atoms fenv args, link_exp tenv venv fenv decls e)
       | TailCall (pos, g, args) ->
            TailCall (pos, link_var fenv g, link_atoms fenv args)
       | Return (pos, v, ty, a) ->
            Return (pos, link_var fenv v, ty, link_atom fenv a)
       | LetString (pos, v, ty, pre, s, e) ->
            LetString (pos, v, ty, pre, s, link_exp tenv venv fenv decls e)
       | LetAlloc (pos, v, ty, label, args, e) ->
            LetAlloc (pos, v, ty, label, link_atoms fenv args, link_exp tenv venv fenv decls e)
       | LetMalloc (pos, v, a, e) ->
            LetMalloc (pos, v, link_atom fenv a, link_exp tenv venv fenv decls e)
       | IfThenElse (pos, a, e1, e2) ->
            IfThenElse (pos, link_atom fenv a, link_exp tenv venv fenv decls e1, link_exp tenv venv fenv decls e2)
       | Match (pos, a, ty, cases) ->
            Match (pos, link_atom fenv a, ty, List.map (fun (p, e) -> p, link_exp tenv venv fenv decls e) cases)
       | Try (pos, e1, v, e2) ->
            Try (pos, link_exp tenv venv fenv decls e1, v, link_exp tenv venv fenv decls e2)
       | Raise (pos, a) ->
            Raise (pos, link_atom fenv a)
       | LetSizeof (pos, v, ty1, ty2, e) ->
            LetSizeof (pos, v, ty1, ty2, link_exp tenv venv fenv decls e)
       | Debug (pos, info, e) ->
            Debug (pos, link_debug_info fenv info, link_exp tenv venv fenv decls e)
       | LetProject (pos, v, ty, a1, v2, e) ->
            LetProject (pos, v, ty, link_atom fenv a1, v2, link_exp tenv venv fenv decls e)
       | LetSubscript (pos, v, ty, v1, ty_bag, a2, e) ->
            LetSubscript (pos, v, ty, link_var fenv v1, ty_bag, link_atom fenv a2, link_exp tenv venv fenv decls e)
       | Set (pos, a1, ty, a2, e) ->
            Set (pos, link_atom fenv a1, ty, link_atom fenv a2, link_exp tenv venv fenv decls e)
       | LetAddrOf (pos, v, ty, a, e) ->
            LetAddrOf (pos, v, ty, link_atom fenv a, link_exp tenv venv fenv decls e)

(*
 * Add the types to the type environment.
 *)
and link_types_exp tenv venv fenv decls loc pos types e =
   let tenv =
      List.fold_left (fun tenv (v, _, ty) ->
            tenv_add tenv v ty) tenv types
   in
   let e = link_exp tenv venv fenv decls e in
      LetTypes (pos, types, e)

(*
 * Try to link the function against a declaration.
 *)
and link_funs_exp tenv venv fenv decls loc pos funs e =
   let loc = string_pos "link_funs_exp" loc in

   (* Resolve all the functions, but don't convert the bodies *)
   let fenv, funs =
      List.fold_left (fun (fenv, funs) fund ->
            let fenv, fund = link_fun tenv venv fenv decls loc fund in
               fenv, fund :: funs) (fenv, []) funs
   in

   (* Convert the bodies *)
   let funs =
      List.fold_left (fun funs fund ->
            let pos, f, label, line, gflag, f_ty, vars, e = fund in
            let e = link_exp tenv venv fenv decls e in
            let fund = pos, f, label, line, gflag, f_ty, vars, e in
               fund :: funs) [] funs
   in
   let e = link_exp tenv venv fenv decls e in
      LetFuns (pos, funs, e)

and link_fun tenv venv fenv decls loc (pos, f, label, line, gflag, f_ty, vars, e) =
   let loc = string_pos "link_fun" loc in

   (* Search for a better match *)
   let rec search_info info v f_ty' = function
      (f_ty'', v') :: funs ->
         let loc = string_pos "search_info" loc in
            (try
                let info', _ = unify_types_info tenv venv subst_empty loc f_ty'' f_ty in
                let info', v', f_ty' =
                   if unify_compare info' info <= 0 then
                      info', v', f_ty''
                   else
                      info, v, f_ty'
                in
                   search_info info' v' f_ty' funs
             with
                AstException _ ->
                   search_info info v f_ty' funs)
    | [] ->
         info, f_ty', v
   in

   (* Search for a match *)
   let rec search = function
      (f_ty', v') :: funs ->
         let loc = string_pos "search" loc in
            (try
                let info', _ = unify_types_info tenv venv subst_empty loc f_ty' f_ty in
                   search_info info' v' f_ty' funs
             with
                AstException _ ->
                   search funs)
    | [] ->
         (* No matching declaration *)
         unify_empty, f_ty, f
   in

   (*
    * Find the best declaration.
    * If the declaration is different, use the declared type.
    *)
   let entries =
      try SymbolMTable.find_all decls label with
         Not_found ->
            []
   in
   let info, f_ty, f' = search entries in
   let _ =
      if not (unify_is_exact info) then
         let buf = err_formatter in
            pp_print_location buf pos;
            pp_print_space buf ();
            pp_print_string buf "warning: definition does not match declaration ";
            pp_print_symbol buf label;
            pp_print_newline buf ()
   in

   (* Add to the function map; don't link the body yet *)
   let fenv = SymbolTable.add fenv f f' in
   let fund = pos, f', label, line, gflag, f_ty, vars, e in
      fenv, fund

(*
 * Add a new declaration to the list.
 *)
and link_fundecl_exp tenv venv fenv decls loc pos sc v label gflag ty e =
   let loc = string_pos "link_fundecl_exp" loc in
   let decls = SymbolMTable.add decls label (ty, v) in
   let e = link_exp tenv venv fenv decls e in
      LetFunDecl (pos, sc, v, label, gflag, ty, e)

(*
 * Rename the debug info.
 *)
and link_debug_info fenv info =
   match info with
      DebugString _ ->
         info
    | DebugContext (line, vars) ->
         let vars =
            List.map (fun (v1, ty, v2) -> v1, ty, link_var fenv v2) vars
         in
            DebugContext (line, vars)

(*
 * Outer function.
 *)
let link_exp e =
   link_exp tenv_empty venv_empty SymbolTable.empty SymbolMTable.empty e

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Type inference.
 *    1. Replace all TyDelayed with TyVar v (new v)
 *    2. Apply first pass of type inference.
 *       This will infer all types except for subscripts.
 *    3. Infer types for subscripts.
 *    4. Apply type substitution
 *)
let infer_exp exp =
   let _ =
      if debug Fir_state.debug_print_ast then
         Fc_ast_print.debug_expr "*** Fc_ast_infer.check_prog: begin" exp
   in

   let exp = map_type_exp new_type exp in
   let _ =
      if debug Fir_state.debug_print_ast then
         Fc_ast_print.debug_expr "*** Fc_ast_infer.check_exp: new_types" exp
   in

   let calls, exp = infer_exp exp in
   let _ =
      if debug Fir_state.debug_print_ast then
         Fc_ast_print.debug_expr "*** Fc_ast_infer.check_exp: infer" exp
   in

   let exp = resolve_exp exp calls in
   let _ =
      if debug Fir_state.debug_print_ast then
         Fc_ast_print.debug_expr "*** Fc_ast_infer.check_exp: resolve" exp
   in


   let exp = link_exp exp in
   let _ =
      if debug Fir_state.debug_print_ast then
         Fc_ast_print.debug_expr "*** Fc_ast_infer.link_exp: link" exp
   in
      exp

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
