(*
 * Exceptions during the AST stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fc_config

open Fc_ast
open Fc_ast_exn
open Fc_ast_util
open Fc_ast_print

(*
 * Location of an exception.
 *)
type item =
   Symbol        of symbol
 | String        of string
 | Exp           of exp
 | Type          of ty
 | Error         of ast_error

 | ParseExp      of Fc_parse_type.expr
 | ParseType     of Fc_parse_type.ty

type pos = item Position.pos

(*
 * Error during translation.
 *)
exception AstException of pos * ast_error

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the source location for an exception.
 *)
let string_loc = bogus_loc "<Fc_ast_pos>"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_expr e
    | Type ty ->
         loc_of_type ty
    | Symbol _
    | String _
    | Error _ ->
         string_loc
    | ParseExp e ->
         Fc_parse_util.loc_of_expr e
    | ParseType ty ->
         Fc_parse_util.loc_of_type ty

(*
 * Print debugging info.
 *)
let tabstop = 3

(*
 * Field printing.
 *)
let pp_print_field_length buf = function
   Some i ->
      pp_print_int buf i
 | None ->
      pp_print_string buf "<normal-length>"

let pp_print_field buf (_, l, ty, i_opt) =
   pp_print_symbol buf l;
   pp_print_string buf " : ";
   pp_print_type buf ty;
   match i_opt with
      Some i ->
         pp_print_string buf " : ";
         pp_print_int buf i
    | None ->
         ()

let pp_print_ty_opt buf = function
   Some ty ->
      pp_print_string buf " : ";
      pp_print_type buf ty
 | None ->
      ()

let pp_print_enum_field buf (_, l, i) =
   pp_print_symbol buf l;
   pp_print_string buf " = ";
   pp_print_string buf (Int32.to_string i)

let pp_print_uenum_field buf (_, l, ty_opt, v) =
   pp_print_symbol buf l;
   pp_print_ty_opt buf ty_opt;
   pp_print_string buf " = ";
   pp_print_symbol buf v

(*
 * Value printing.
 *)
let rec pp_print_value buf x =
   match x with
      Exp e ->
         pp_print_expr buf e

    | Type t ->
         pp_print_type buf t

    | Symbol v ->
         pp_print_symbol buf v

    | String s ->
         pp_print_string buf s

    | Error e ->
         pp_print_ast_exn buf e

    | ParseExp e ->
         Fc_parse_print.pp_print_expr buf e

    | ParseType ty ->
         Fc_parse_print.pp_print_type buf ty

(*
 * Exception printer.
 *)
and pp_print_ast_exn buf = function
   StringError s ->
      pp_print_string buf s
 | StringAtomError (s, a) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_atom buf a
 | StringVarError (s, v) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_symbol buf v
 | StringTypeError (s, ty) ->
      pp_print_string buf s;
      pp_print_string buf ":";
      pp_print_space buf ();
      pp_print_type buf ty
 | StringIntError (s, i) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_int buf i
 | StringInt32Error (s, i) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_string buf (Int32.to_string i)
 | StringVarTypeListError (s, v, tyl1, tyl2) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_symbol buf v;
      pp_print_string buf "(";
      pp_print_sep_list buf "," pp_print_type tyl1;
      pp_print_string buf ");";
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "options are:";
      List.iter (fun ty ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf " : ";
            pp_print_type buf ty) tyl2;
      pp_close_box buf ()
 | UnboundVar v ->
      pp_print_string buf "unbound variable: ";
      pp_print_symbol buf v
 | UnboundLabel v ->
      pp_print_string buf "unbound label: ";
      pp_print_symbol buf v
 | UnboundType v ->
      pp_print_string buf "unbound type: ";
      pp_print_symbol buf v
 | ArityMismatch (i, j) ->
      pp_print_string buf "arity mismatch: wanted ";
      pp_print_int buf i;
      pp_print_string buf ", got ";
      pp_print_int buf j
 | TypeError2 (ty1, ty2) ->
      pp_open_vbox buf tabstop;
      pp_print_string buf "type error:";
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "this expression has type:";
      pp_print_space buf ();
      pp_print_type buf ty2;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "but is used with type:";
      pp_print_space buf ();
      pp_print_type buf ty1;
      pp_close_box buf ();
      pp_close_box buf ()
 | TypeError4 (ty1a, ty2a, ty1b, ty2b) ->
      pp_open_vbox buf tabstop;
      pp_print_string buf "type error:";
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "this expression has type:";
      pp_print_space buf ();
      pp_print_type buf ty2a;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "but is used with type:";
      pp_print_space buf ();
      pp_print_type buf ty1a;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "this type:";
      pp_print_space buf ();
      pp_print_type buf ty2b;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "should have type:";
      pp_print_space buf ();
      pp_print_type buf ty1b;
      pp_close_box buf ();
      pp_close_box buf ()
 | NotImplemented s ->
      pp_print_string buf "not implemented: ";
      pp_print_string buf s
 | FieldsDontMatch (l1, l2) ->
      pp_print_string buf "field labels don't match: ";
      pp_print_symbol buf l1;
      pp_print_space buf();
      pp_print_symbol buf l2
 | StructFieldLengthsDontMatch (i_opt1, i_opt2) ->
      pp_print_string buf "field lengths don't match: ";
      pp_print_field_length buf i_opt2;
      pp_print_string buf " should be ";
      pp_print_field_length buf i_opt2
 | EnumFieldValuesDontMatch (i1, i2) ->
      pp_print_string buf "field values don't match: ";
      pp_print_string buf (Int32.to_string i2);
      pp_print_string buf " should be ";
      pp_print_string buf (Int32.to_string i1)
 | StringStructFieldError (s, field) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_field buf field
 | StringUEnumFieldError (s, field) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_uenum_field buf field
 | StringEnumFieldError (s, field) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_enum_field buf field
 | UEnumFieldsDontMatch (l1, ty_opt1, l2, ty_opt2) ->
      pp_print_string buf "enum fields don't match: ";
      pp_print_space buf ();
      pp_print_symbol buf l2;
      pp_print_ty_opt buf ty_opt2;
      pp_print_space buf ();
      pp_print_string buf "should be";
      pp_print_space buf ();
      pp_print_symbol buf l1;
      pp_print_ty_opt buf ty_opt1

(************************************************************************
 * CONSTRUCTION
 ************************************************************************)

module type PosSig =
sig
   val loc_pos : loc -> pos

   val exp_pos : exp -> pos
   val var_exp_pos : var -> pos
   val type_exp_pos : ty -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos
   val type_pos : ty -> pos -> pos
   val error_pos : ast_error -> pos -> pos

   val parse_exp_pos   : Fc_parse_type.expr -> pos
   val parse_type_pos  : Fc_parse_type.ty -> pos

   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let exp_pos e = base_pos (Exp e)
   let var_exp_pos v = base_pos (Symbol v)
   let type_exp_pos ty = base_pos (Type ty)
   let string_exp_pos s = base_pos (String s)
   let var_pos = symbol_pos
   let type_pos ty pos = cons_pos (Type ty) pos
   let error_pos e pos = cons_pos (Error e) pos

   let parse_exp_pos p = base_pos (ParseExp p)
   let parse_type_pos p = base_pos (ParseType p)
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
