(*
 * Utilities on AST.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_config

open Fc_ast
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_env
open Fc_ast_util
open Fc_ast_subst

module Pos = MakePos (struct let name = "Fc_ast_type" end)
open Pos

(*
 * Make a new string type.
 *)
let ty_string pre pos s =
   let pre = FCParam.char_precision pre in
      TyArray (pos, StatusConst,
               TyInt (pos, StatusConst, pre, false),
               AtomInt (pos, Rawint.of_int Rawint.Int32 true 0),
               AtomInt (pos, Rawint.of_int Rawint.Int32 true (succ (Array.length s))))

(*
 * Atom should be a var.
 *)
let var_of_atom pos a =
   match a with
      AtomVar (_, v) ->
         v
    | _ ->
         raise (AstException (pos, StringAtomError ("atom is not a var", a)))

(*
 * Get the type of the atom.
 *)
let type_of_atom venv pos a =
   match a with
      AtomUnit (pos, n, _) ->
         TyUnit (pos, StatusConst, n)
    | AtomInt (pos, i) ->
         if Rawint.is_zero i then
            TyZero (pos, StatusConst)
         else
            TyInt (pos, StatusConst, Rawint.precision i, Rawint.signed i)
    | AtomFloat (pos, x) ->
         TyFloat (pos, StatusConst, Rawfloat.precision x)
    | AtomNil (pos, ty) ->
         TyPointer (pos, StatusConst, ty)
    | AtomVar (_, v) ->
         venv_lookup venv pos v
    | AtomEnum (_, ty, _) ->
         ty

(*
 * Apply the type, and return the expansion.
 * The result _is not_ a TyApply.
 *)
let apply_type tenv pos v tyl =
   match tenv_lookup tenv pos v with
      TyLambda (_, _, vars, ty) ->
         if vars = [] then
            ty
         else
            let tvenv =
               let len1 = List.length vars in
               let len2 = List.length tyl in
                  if len1 <> len2 then
                     raise (AstException (pos, ArityMismatch (len1, len2)));
                  List.fold_left2 subst_add subst_empty vars tyl
            in
               subst_type tvenv ty
    | ty ->
         ty

(*
 * Expand a type one level.
 *)
let rec tenv_expand tenv pos ty =
   match ty with
      TyApply (_, _, v, tyl) ->
         tenv_expand tenv pos (apply_type tenv pos v tyl)
    | TyLambda (_, _, [], ty) ->
         tenv_expand tenv pos ty
    | _ ->
         ty

(*
 * Return the parts of a function type.
 *)
let rec dest_fun_type_aux tenv pos ty =
   match tenv_expand tenv pos ty with
      TyFun (_, _, ty_vars, ty_res) ->
         [], ty_vars, ty_res
    | TyAll (_, _, vars, ty) ->
         let vars', ty_vars, ty_res = dest_fun_type_aux tenv pos ty in
            vars @ vars', ty_vars, ty_res
    | ty ->
         raise (AstException (pos, StringTypeError ("not a function type", ty)))

let dest_fun_type tenv pos ty =
   match tenv_expand tenv pos ty with
      TyPointer (_, _, ty) -> dest_fun_type_aux tenv pos ty
    | ty -> dest_fun_type_aux tenv pos ty

(*
 * Elisions.
 *)
let is_elide_type tenv pos ty =
   match tenv_expand tenv pos ty with
      TyElide _ ->
         true
    | _ ->
         false

let dest_elide_type tenv pos ty =
   match tenv_expand tenv pos ty with
      TyElide (_, _, tyl) ->
         tyl
    | _ ->
         raise (AstException (pos, StringTypeError ("not an elision type", ty)))

(*
 * Add a reference if one is not already there.
 *)
let is_ref_type tenv pos ty =
   match tenv_expand tenv pos ty with
      TyRef _ ->
         true
    | _ ->
         false

let dest_ref_type tenv pos ty =
   match tenv_expand tenv pos ty with
      TyRef (_, _, ty) ->
         ty
    | _ ->
         raise (AstException (pos, StringTypeError ("not an ref type", ty)))

let make_ref_type tenv loc pos status ty =
   match tenv_expand tenv loc ty with
      TyRef _ ->
         ty
    | _ ->
         TyRef (pos, status, ty)

let dest_maybe_ref_type tenv loc ty =
   match tenv_expand tenv loc ty with
      TyRef (_, _, ty) ->
         ty
    | _ ->
         ty

(*
 * Integer types.
 *)
let dest_int_type tenv pos ty =
   match tenv_expand tenv pos ty with
      TyInt (pos, status, pre, signed) ->
         pos, status, pre, signed
    | ty ->
         raise (AstException (pos, StringTypeError ("not an integer type", ty)))

(*
 * Aggregate types.
 *)
let is_aggregate_type = function
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _ ->
        false
    | TyArray _
    | TyConfArray _ ->
        true
    | TyPointer _
    | TyRef _ ->
        false
    | TyTuple _
    | TyEnum _
    | TyUEnum _
    | TyUnion _
    | TyStruct _
    | TyElide _ ->
        true
    | TyFun _
    | TyVar _
    | TyAll _
    | TyLambda _
    | TyApply _ ->
        false

let rec change_conformant_array ty =
   match ty with
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _ ->
         ty
    | TyArray (pos, status, ty, a1, a2) ->
         TyArray (pos, status, change_conformant_array ty, a1, a2)
    | TyConfArray (pos, status, ty, v1, v2, v_ty) ->
         TyArray (pos, status, change_conformant_array ty, AtomVar (pos, v1), AtomVar (pos, v2))
    | TyPointer (pos, status, ty) ->
         TyPointer (pos, status, change_conformant_array ty)
    | TyRef (pos, status, ty) ->
         TyRef (pos, status, change_conformant_array ty)
    | TyTuple (pos, status, ty_list) ->
         TyTuple (pos, status, change_conformant_array_in_list ty_list)
    | TyEnum _ ->
         ty
    | TyUEnum (pos, status, fields) ->
         let fields =
            match fields with
               Fields (label, fields) ->
                  let fields =
                     List.map (fun (pos, var, ty_opt, label) ->
                           pos, var, change_conformant_array_option ty_opt, label) fields
                  in
                     Fields (label, fields)
             | Label _ ->
                  fields
         in
            TyUEnum (pos, status, fields)
    | TyUnion (pos, status, fields) ->
         let fields =
            match fields with
               Fields fields ->
                  Fields (List.map (fun (pos, var, ty, io) ->
                                pos, var, change_conformant_array ty, io) fields)
             | Label _ ->
                  fields
         in
            TyUnion (pos, status, fields)
    | TyStruct (pos, status, fields) ->
         let fields =
            match fields with
               Fields fields ->
                  Fields (List.map (fun (pos, var, ty, io) ->
                                pos, var, change_conformant_array ty, io) fields)
             | Label _ ->
                  fields
         in
            TyStruct (pos, status, fields)
    | TyFun (pos, status, ty_list, ty) ->
         TyFun (pos, status, change_conformant_array_in_list ty_list, change_conformant_array ty)
    | TyVar _ ->
         ty
    | TyAll (pos, status, vars, ty) ->
         TyAll (pos, status, vars, change_conformant_array ty)
    | TyLambda (pos, status, vars, ty) ->
         TyLambda (pos, status, vars, change_conformant_array ty)
    | TyApply (pos, status, var, ty_list) ->
         TyApply (pos, status, var, change_conformant_array_in_list ty_list)
    | TyElide (pos, status, ty_list) ->
         TyElide (pos, status, change_conformant_array_in_list ty_list)

and change_conformant_array_option ty_opt =
   match ty_opt with
      Some ty ->
         Some (change_conformant_array ty)
    | None ->
         None

and change_conformant_array_in_list ty_list =
   List.map (fun ty ->
         change_conformant_array ty) ty_list

(*
 * Get the index for argument passing.
 *)
let rec stdarg_type tenv pos ty =
   let pos = string_pos "stdarg_type" pos in
      match tenv_expand tenv pos ty with
         TyUnit _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyEnum _ ->
            Char.code 'i'
       | TyArray _
       | TyConfArray _
       | TyPointer _
       | TyRef _
       | TyTuple _
       | TyUEnum _
       | TyUnion _
       | TyStruct _
       | TyFun _
       | TyVar _
       | TyElide _ ->
            Char.code 'p'
       | TyAll (_, _, _, ty)
       | TyLambda (_, _, _, ty) ->
            stdarg_type tenv pos ty
       | TyApply _
       | TyDelayed _
       | TyZero _ ->
            raise (AstException (pos, StringTypeError ("unexpected type", ty)))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
