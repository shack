(*
 * Fix function applications to defined functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Fc_ast

(************************************************************************
 * Pass 1.
 *
 * Collect all the function names.
 *)
let builtins =
    let venv = SymbolSet.empty in
    let venv = SymbolSet.add venv (Symbol.add "min") in
    let venv = SymbolSet.add venv (Symbol.add "max") in
    let venv = SymbolSet.add venv (Symbol.add "abs") in
    let venv = SymbolSet.add venv (Symbol.add "sin") in
    let venv = SymbolSet.add venv (Symbol.add "cos") in
    let venv = SymbolSet.add venv (Symbol.add "sqrt") in
    let venv = SymbolSet.add venv (Symbol.add "atan2") in
    let venv = SymbolSet.add venv (Symbol.add "pasq_||") in
    let venv = SymbolSet.add venv (Symbol.add "pasq_&&") in
    let venv = SymbolSet.add venv (Symbol.add "pasq_/") in
	venv

let rec collect_exp venv e =
   match e with
      LetTypes (_, _, e)
    | LetVar (_, _, _, _, _, _, _, e)
    | LetCoerce (_, _, _, _, e)
    | LetAtom (_, _, _, _, e)
    | LetUnop (_, _, _, _, _, e)
    | LetBinop (_, _, _, _, _, _, e)
    | LetExtCall (_, _, _, _, _, _, _, e)
    | LetApply (_, _, _, _, _, _, e)
    | LetTest (_, _, _, _, e)
    | LetSizeof (_, _, _, _, e)
    | LetString (_, _, _, _, _, e)
    | LetAlloc (_, _, _, _, _, e)
    | LetMalloc (_, _, _, e)
    | LetProject (_, _, _, _, _, e)
    | LetSubscript (_, _, _, _, _, _, e)
    | Set (_, _, _, _, e)
    | LetAddrOf (_, _, _, _, e)
    | Debug (_, _, e) ->
         collect_exp venv e
    | LetFunDecl (_, _, f, _, _, _, e) ->
         collect_exp (SymbolSet.add venv f) e
    | LetFuns (_, funs, e) ->
         let venv =
            List.fold_left (fun venv (_, f, _, _, _, _, _, e) ->
                  collect_exp (SymbolSet.add venv f) e) venv funs
         in
            collect_exp venv e
    | IfThenElse (_, _, e1, e2)
    | Try (_, e1, _, e2) ->
         collect_exp (collect_exp venv e1) e2
    | Match (_, _, _, cases) ->
         List.fold_left (fun venv (_, e) ->
               collect_exp venv e) venv cases
    | TailCall _
    | Return _
    | Raise _ ->
         venv

(************************************************************************
 * Pass 2.
 *
 * Adjust all the LetApply.
 *)
let apply_sym = Symbol.add "()"

let rec apply_exp venv e =
   match e with
      LetTypes (pos, tyl, e) ->
         LetTypes (pos, tyl, apply_exp venv e)
    | LetFuns (pos, funs, e) ->
         let funs =
            List.map (fun (pos, f, label, line, gflag, ty, vars, e) ->
                  pos, f, label, line, gflag, ty, vars, apply_exp venv e) funs
         in
         let e = apply_exp venv e in
            LetFuns (pos, funs, e)
    | LetFunDecl (pos, sc, v, label, gflag, ty, e) ->
         LetFunDecl (pos, sc, v, label, gflag, ty, apply_exp venv e)
    | LetVar (pos, sc, v, label, gflag, ty, a_opt, e) ->
         LetVar (pos, sc, v, label, gflag, ty, a_opt, apply_exp venv e)
    | LetCoerce (pos, v, ty, a, e) ->
         LetCoerce (pos, v, ty, a, apply_exp venv e)
    | LetAtom (pos, v, ty, a, e) ->
         LetAtom (pos, v, ty, a, apply_exp venv e)
    | LetUnop (pos, v, ty, op, a, e) ->
         LetUnop (pos, v, ty, op, a, apply_exp venv e)
    | LetBinop (pos, v, ty, op, a1, a2, e) ->
         LetBinop (pos, v, ty, op, a1, a2, apply_exp venv e)
    | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
         LetExtCall (pos, v, ty, s, b, ty', args, apply_exp venv e)
    | LetApply (pos, v, ty, f, label, AtomVar (_, f') :: AtomVar (_, flabel) :: args, e)
      when Symbol.eq f apply_sym && SymbolSet.mem venv f' ->
         LetApply (pos, v, ty, f', flabel, args, apply_exp venv e)
    | LetApply (pos, v, ty, f, label, args, e) ->
         LetApply (pos, v, ty, f, label, args, apply_exp venv e)
    | LetTest (pos, v, ty, a, e) ->
         LetTest (pos, v, ty, a, apply_exp venv e)
    | LetSizeof (pos, v, ty1, ty2, e) ->
         LetSizeof (pos, v, ty1, ty2, apply_exp venv e)
    | TailCall _
    | Return _
    | Raise _ ->
         e
    | LetString (pos, v, ty, pre, s, e) ->
         LetString (pos, v, ty, pre, s, apply_exp venv e)
    | LetAlloc (pos, v, ty, label, args, e) ->
         LetAlloc (pos, v, ty, label, args, apply_exp venv e)
    | LetMalloc (pos, v, a, e) ->
         LetMalloc (pos, v, a, e)
    | IfThenElse (pos, a, e1, e2) ->
         IfThenElse (pos, a, apply_exp venv e1, apply_exp venv e2)
    | Match (pos, a, ty, cases) ->
         let cases = List.map (fun (p, e) -> p, apply_exp venv e) cases in
            Match (pos, a, ty, cases)
    | Try (pos, e1, v, e2) ->
         Try (pos, apply_exp venv e1, v, apply_exp venv e2)
    | LetProject (pos, v1, ty, v2, v3, e) ->
         LetProject (pos, v1, ty, v2, v3, apply_exp venv e)
    | LetSubscript (pos, v1, ty, v2, ty2, v3, e) ->
         LetSubscript (pos, v1, ty, v2, ty2, v3, apply_exp venv e)
    | Set (pos, a1, ty, a2, e) ->
         Set (pos, a1, ty, a2, apply_exp venv e)
    | LetAddrOf (pos, v, ty, a, e) ->
         LetAddrOf (pos, v, ty, a, apply_exp venv e)
    | Debug (pos, info, e) ->
         Debug (pos, info, apply_exp venv e)

let apply_exp e =
   let venv = collect_exp builtins e in
      apply_exp venv e

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
