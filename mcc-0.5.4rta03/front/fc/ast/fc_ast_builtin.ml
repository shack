(*
 * Expand builtin functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Fc_config

open Fc_ast
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_env
open Fc_ast_type

(*
 * Keep a table of overloaded operators.
 * Save builtin operators.
 *)
type fun_entry = tenv -> pos -> loc -> var -> ty -> ty list -> atom list -> exp -> exp

type fenv = (ty * fun_entry) SymbolMTable.t

(************************************************************************
 * BUILTIN OPERATORS
 ************************************************************************)

(*
 * Some types.
 *)
let pos = bogus_loc "<Fc_ast_builtin>"

let pre_char  = FCParam.char_precision NormalPrecision
let pre_short = FCParam.int_precision  ShortPrecision
let pre_int   = FCParam.int_precision  NormalPrecision
let pre_long  = FCParam.int_precision  LongPrecision

let signed_char  = false
let signed_short = true
let signed_int   = true
let signed_long  = true

let pre_float = FCParam.float_precision ShortPrecision
let pre_double = FCParam.float_precision NormalPrecision
let pre_long_double = FCParam.float_precision LongPrecision

let ty_bool  = TyUnit(pos, StatusNormal, 2)
let ty_char  = TyInt (pos, StatusNormal, pre_char, signed_char)
let ty_short = TyInt (pos, StatusNormal, pre_short, signed_short)
let ty_field = TyField (pos, StatusNormal, pre_int, signed_int, 0, 32)
let ty_int   = TyInt (pos, StatusNormal, pre_int,   signed_int)
let ty_long  = TyInt (pos, StatusNormal, pre_long,  signed_int)

let ty_uchar  = TyInt (pos, StatusNormal, pre_char,  false)
let ty_ushort = TyInt (pos, StatusNormal, pre_short, false)
let ty_uint   = TyInt (pos, StatusNormal, pre_int,   false)
let ty_ulong  = TyInt (pos, StatusNormal, pre_long,  false)

let ty_char_ref  = TyRef (pos, StatusNormal, ty_char)
let ty_short_ref = TyRef (pos, StatusNormal, ty_short)
let ty_field_ref = TyRef (pos, StatusNormal, ty_field)
let ty_int_ref   = TyRef (pos, StatusNormal, ty_int)
let ty_long_ref  = TyRef (pos, StatusNormal, ty_long)

let ty_uchar_ref  = TyRef (pos, StatusNormal, ty_uchar)
let ty_ushort_ref = TyRef (pos, StatusNormal, ty_ushort)
let ty_uint_ref   = TyRef (pos, StatusNormal, ty_uint)
let ty_ulong_ref  = TyRef (pos, StatusNormal, ty_ulong)

let ty_float       = TyFloat (pos, StatusNormal, pre_float)
let ty_double      = TyFloat (pos, StatusNormal, pre_double)
let ty_long_double = TyFloat (pos, StatusNormal, pre_long_double)

let ty_float_ref       = TyRef (pos, StatusNormal, ty_float)
let ty_double_ref      = TyRef (pos, StatusNormal, ty_double)
let ty_long_double_ref = TyRef (pos, StatusNormal, ty_long_double)

let v_ty = new_symbol_string "a"
let ty_var = TyVar (pos, StatusNormal, v_ty)
let ty_var_ref = TyRef (pos, StatusNormal, ty_var)
let ty_pointer = TyPointer (pos, StatusNormal, ty_var)
let ty_pointer_ref = TyRef (pos, StatusNormal, ty_pointer)

let v_ty1 = new_symbol_string "a"
let v_ty2 = new_symbol_string "b"
let ty_var1 = TyVar (pos, StatusNormal, v_ty1)
let ty_var2 = TyVar (pos, StatusNormal, v_ty2)

let zero_int pre signed pos =
   AtomInt (pos, Rawint.of_int pre signed 0)

let zero_float pre pos =
   AtomFloat (pos, Rawfloat.of_float pre 0.0)

let one_int pos pre signed = AtomInt (pos, Rawint.of_int pre signed 1)

let dest_pointer_type ty =
   match ty with
      TyPointer (_, _, pty) ->
         pty
    | _ ->
	 raise (Invalid_argument "not a pointer type")

(*
 * Argumnent parsing.
 *)
let dest_atom1 loc = function
   [a] ->
      a
 | args ->
      raise (AstException (loc, ArityMismatch (1, List.length args)))

let dest_atom2 loc = function
   [a1; a2] ->
      a1, a2
 | args ->
      raise (AstException (loc, ArityMismatch (2, List.length args)))

let dest_ty2 = dest_atom2

(*
 * Function applications.
 *)
let apply_sym = Symbol.add "()"

let make_apply f label tenv loc pos v ty ty_args args e =
   LetApply (pos, v, ty, f, label, args, e)

let make_apply2 label tenv loc pos v ty ty_args args e =
   match args with
      AtomVar (_, f) :: args ->
         LetApply (pos, v, ty, f, label, args, e)
    | _ ->
         raise (AstException (loc, ArityMismatch (1, List.length args)))

let make_apply_template label loc pos ty_args ty_res =
   match ty_args with
      ty_f :: ty_args ->
         let ty_f_args = List.map (fun _ -> TyVar (pos, StatusNormal, new_symbol_string "a")) ty_args in
         let ty_f = TyFun (pos, StatusNormal, ty_f_args, ty_res) in
         let ty1 = TyFun (pos, StatusNormal, ty_f :: ty_f_args, ty_res) in
            [ty1, make_apply2 label]
    | [] ->
         raise (AstException (loc, ArityMismatch (1, List.length ty_args)))

let make_apply_stdargs make tenv loc pos v ty ty_args args e =
   (* Build the format string *)
   let v_string = new_symbol_string "stdargs_format" in
   let ty_char = TyInt (pos, StatusNormal, Rawint.Int8, false) in
   let ty_string = TyPointer (pos, StatusNormal, ty_char) in
   let s = List.map (stdarg_type tenv loc) ty_args in
   let s = Array.of_list s in

   (* Collect the tuple *)
   let v_tuple = new_symbol_string "stdargs" in
   let ty_arg = TyElide (pos, StatusNormal, ty_string :: ty_args) in
   let args = List.map (fun a -> None, InitAtom (pos, a)) args in
   let args = (None, InitAtom (pos, AtomVar (pos, v_string))) :: args in
   let init = InitArray (pos, args) in
      LetString (pos, v_string, ty_string, ShortPrecision, s,
      LetVar (pos, StoreAuto, v_tuple, v_tuple, VarLocalClass, ty_arg, init,
      make tenv loc pos v ty [ty_arg] [AtomVar (pos, v_tuple)] e))

(*
 * Make a builtin operator.
 *)
let make_builtin_unop op tenv loc pos v ty ty_args args e =
   match args with
      [a] ->
         LetUnop (pos, v, ty, op, a, e)
    | _ ->
         raise (AstException (loc, ArityMismatch (1, List.length args)))

let make_builtin_binop op tenv loc pos v ty ty_args args e =
   match args with
      [a1; a2] ->
         LetBinop (pos, v, ty, op, a1, a2, e)
    | _ ->
         raise (AstException (loc, ArityMismatch (2, List.length args)))

let make_builtin_binop2 op a2 tenv loc pos v ty ty_args args e =
   let a2 = a2 pos in
      match args with
         [a1] ->
            LetBinop (pos, v, ty, op, a1, a2, e)
       | _ ->
            raise (AstException (loc, ArityMismatch (1, List.length args)))

let make_builtin_assignop op tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v' = new_symbol v in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v, ty_ref, a1,
      LetBinop (pos, v', ty, op, a1, a2,
      Set (pos, AtomVar (pos, v), ty_ref, AtomVar (pos, v'), e)))

let make_builtin_assignop_pointer_minus tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v, ty_ref, a1,
      LetUnop (pos, v2, ty_int, UMinusIntOp (pre_int, signed_int), a2,
      LetBinop (pos, v1, ty, PlusPointerOp, a1, AtomVar (pos, v2),
      Set (pos, AtomVar (pos, v), ty_ref, AtomVar (pos, v1), e))))

let make_pre_int_incr_op pre signed tenv loc pos v ty ty_args args e =
   let a = dest_atom1 loc args in
   let v1 = new_symbol v in
   let one_int = AtomInt (pos, Rawint.of_int pre signed 1) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a,
      LetBinop (pos, v, ty, PlusIntOp (pre, signed), a, one_int,
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e)))

let make_pre_float_incr_op pre tenv loc pos v ty ty_args args e =
   let a = dest_atom1 loc args in
   let v1 = new_symbol v in
   let one_float = AtomFloat (pos, Rawfloat.of_float pre 1.0) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a,
      LetBinop (pos, v, ty, PlusFloatOp pre, a, one_float,
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e)))

let make_pre_pointer_incr_op tenv loc pos v ty ty_args args e =
   let a = dest_atom1 loc args in
   let v1 = new_symbol v in
   let one_int = AtomInt (pos, Rawint.of_int pre_int signed_int 1) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a,
      LetBinop (pos, v, ty, PlusPointerOp, a, one_int,
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e)))

let make_post_int_incr_op pre signed tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let ty, _ = dest_ty2 loc ty_args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let v3 = new_symbol v in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetVar (pos, StoreAuto, v, v, VarLocalClass, ty, InitAtom (pos, a1),
      LetAddrOf (pos, v2, ty_ref, a1,
      LetBinop (pos, v3, ty, PlusIntOp (pre, signed), a1, a2,
      Set (pos, AtomVar (pos, v2), ty_ref, AtomVar (pos, v3),
      e))))

let make_post_float_incr_op pre tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let v3 = new_symbol v in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetVar (pos, StoreAuto, v, v, VarLocalClass, ty, InitAtom (pos, a1),
      LetAddrOf (pos, v2, ty_ref, a1,
      LetBinop (pos, v3, ty, PlusFloatOp pre, a1, a2,
      Set (pos, AtomVar (pos, v2), ty_ref, AtomVar (pos, v3),
      e))))

let make_post_pointer_incr_op tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let ty, _ = dest_ty2 loc ty_args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let v3 = new_symbol v in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetVar (pos, StoreAuto, v, v, VarLocalClass, ty, InitAtom (pos, a1),
      LetAddrOf (pos, v2, ty_ref, a1,
      LetBinop (pos, v3, ty, PlusPointerOp, a1, a2,
      Set (pos, AtomVar (pos, v2), ty_ref, AtomVar (pos, v3),
      e))))

let make_pre_int_decr_op pre signed tenv loc pos v ty ty_args args e =
   let a = dest_atom1 loc args in
   let v1 = new_symbol v in
   let one_int = AtomInt (pos, Rawint.of_int pre signed 1) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a,
      LetBinop (pos, v, ty, MinusIntOp (pre, signed), a, one_int,
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e)))

let make_pre_float_decr_op pre tenv loc pos v ty ty_args args e =
   let a = dest_atom1 loc args in
   let v1 = new_symbol v in
   let one_float = AtomFloat (pos, Rawfloat.of_float pre 1.0) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a,
      LetBinop (pos, v, ty, MinusFloatOp pre, a, one_float,
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e)))

let make_pre_pointer_decr_op tenv loc pos v ty ty_args args e =
   let a = dest_atom1 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol_string "minus_one" in
   let one_int = AtomInt (pos, Rawint.of_int pre_int signed_int 1) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a,
      LetUnop (pos, v2, ty_int, UMinusIntOp (pre_int, signed_int), one_int,
      LetBinop (pos, v, ty, PlusPointerOp, a, AtomVar (pos, v2),
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e))))

let make_pointer_decr_int_op tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol_string "minus_int" in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetAddrOf (pos, v1, ty_ref, a1,
      LetUnop (pos, v2, ty_int, UMinusIntOp (pre_int, signed_int), a2,
      LetBinop (pos, v, ty, PlusPointerOp, a1, AtomVar (pos, v2),
      Set (pos, AtomVar (pos, v1), ty_ref, AtomVar (pos, v),
      e))))

let make_post_int_decr_op pre signed tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let v3 = new_symbol v in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetVar (pos, StoreAuto, v, v, VarLocalClass, ty, InitAtom (pos, a1),
      LetAddrOf (pos, v2, ty_ref, a1,
      LetBinop (pos, v3, ty, MinusIntOp (pre, signed), a1, a2,
      Set (pos, AtomVar (pos, v2), ty_ref, AtomVar (pos, v3),
      e))))

let make_post_float_decr_op pre tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let v3 = new_symbol v in
   let one_float = AtomFloat (pos, Rawfloat.of_float pre 1.0) in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetVar (pos, StoreAuto, v, v, VarLocalClass, ty, InitAtom (pos, a1),
      LetAddrOf (pos, v2, ty_ref, a1,
      LetBinop (pos, v3, ty, MinusFloatOp pre, a1, one_float,
      Set (pos, AtomVar (pos, v2), ty_ref, AtomVar (pos, v3),
      e))))

let make_post_pointer_decr_op tenv loc pos v ty ty_args args e =
   let a1, a2 = dest_atom2 loc args in
   let v1 = new_symbol v in
   let v2 = new_symbol v in
   let v3 = new_symbol v in
   let v4 = new_symbol_string "minus_xxx" in
   let ty, ty_ref =
      if is_ref_type tenv loc ty then
         dest_ref_type tenv loc ty, ty
      else
         ty, TyRef (pos, StatusNormal, ty)
   in
      LetVar (pos, StoreAuto, v, v, VarLocalClass, ty, InitAtom (pos, a1),
      LetAddrOf (pos, v2, ty_ref, a1,
      LetUnop (pos, v4, ty_int, UMinusIntOp (pre_int, signed_int), a2,
      LetBinop (pos, v3, ty, PlusPointerOp, a1, AtomVar (pos, v4),
      Set (pos, AtomVar (pos, v2), ty_ref, AtomVar (pos, v3),
      e)))))

(*
 * Here is the basic table.
 *)
let builtin =
   ["+", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (PlusIntOp (pre_int, signed_int));
    "+", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (PlusIntOp (pre_int, false));
    "+", [], [ty_long; ty_long], ty_long, make_builtin_binop (PlusIntOp (pre_long, signed_long));
    "+", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (PlusIntOp (pre_long, false));
    "+", [], [ty_float; ty_float], ty_float, make_builtin_binop (PlusFloatOp pre_float);
    "+", [], [ty_double; ty_double], ty_double, make_builtin_binop (PlusFloatOp pre_double);
    "+", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (PlusFloatOp pre_long_double);

    "-", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (MinusIntOp (pre_int, signed_int));
    "-", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (MinusIntOp (pre_int, false));
    "-", [], [ty_long; ty_long], ty_long, make_builtin_binop (MinusIntOp (pre_long, signed_long));
    "-", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (MinusIntOp (pre_long, false));
    "-", [], [ty_float; ty_float], ty_float, make_builtin_binop (MinusFloatOp pre_float);
    "-", [], [ty_double; ty_double], ty_double, make_builtin_binop (MinusFloatOp pre_double);
    "-", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (MinusFloatOp pre_long_double);

    "-", [], [ty_int],  ty_int,  make_builtin_unop (UMinusIntOp (pre_int, signed_int));
    "-", [], [ty_uint], ty_uint, make_builtin_unop (UMinusIntOp (pre_int, false));
    "-", [], [ty_long], ty_long, make_builtin_unop (UMinusIntOp (pre_long, signed_long));
    "-", [], [ty_ulong], ty_ulong, make_builtin_unop (UMinusIntOp (pre_long, false));
    "-", [], [ty_float], ty_float, make_builtin_unop (UMinusFloatOp pre_float);
    "-", [], [ty_double], ty_double, make_builtin_unop (UMinusFloatOp pre_double);
    "-", [], [ty_long_double], ty_long_double, make_builtin_unop (UMinusFloatOp pre_long_double);

    "~", [], [ty_int],  ty_int,  make_builtin_unop (NotIntOp (pre_int, signed_int));
    "~", [], [ty_uint], ty_uint, make_builtin_unop (NotIntOp (pre_int, false));
    "~", [], [ty_long], ty_long, make_builtin_unop (NotIntOp (pre_long, signed_long));
    "~", [], [ty_ulong], ty_ulong, make_builtin_unop (NotIntOp (pre_long, false));

    "*", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (MulIntOp (pre_int, signed_int));
    "*", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (MulIntOp (pre_int, false));
    "*", [], [ty_long; ty_long], ty_long, make_builtin_binop (MulIntOp (pre_long, signed_long));
    "*", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (MulIntOp (pre_long, false));
    "*", [], [ty_float; ty_float], ty_float, make_builtin_binop (MulFloatOp pre_float);
    "*", [], [ty_double; ty_double], ty_double, make_builtin_binop (MulFloatOp pre_double);
    "*", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (MulFloatOp pre_long_double);


    "/", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (DivIntOp (pre_int, signed_int));
    "/", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (DivIntOp (pre_int, false));
    "/", [], [ty_long; ty_long], ty_long, make_builtin_binop (DivIntOp (pre_long, signed_long));
    "/", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (DivIntOp (pre_long, false));
    "/", [], [ty_float; ty_float], ty_float, make_builtin_binop (DivFloatOp pre_float);
    "/", [], [ty_double; ty_double], ty_double, make_builtin_binop (DivFloatOp pre_double);
    "/", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (DivFloatOp pre_long_double);

    "pasq_/", [], [ty_int;  ty_int],  ty_float,  make_builtin_binop (DivFloatOp pre_float);
    "pasq_/", [], [ty_uint; ty_uint], ty_float, make_builtin_binop (DivFloatOp pre_float);
    "pasq_/", [], [ty_long; ty_long], ty_double, make_builtin_binop (DivFloatOp pre_double);
    "pasq_/", [], [ty_ulong; ty_ulong], ty_double, make_builtin_binop (DivFloatOp pre_double);
    "pasq_/", [], [ty_float; ty_float], ty_float, make_builtin_binop (DivFloatOp pre_float);
    "pasq_/", [], [ty_double; ty_double], ty_double, make_builtin_binop (DivFloatOp pre_double);
    "pasq_/", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (DivFloatOp pre_long_double);

    "%", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (RemIntOp (pre_int, signed_int));
    "%", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (RemIntOp (pre_int, false));
    "%", [], [ty_long; ty_long], ty_long, make_builtin_binop (RemIntOp (pre_long, signed_long));
    "%", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (RemIntOp (pre_long, false));
    "%", [], [ty_float; ty_float], ty_float, make_builtin_binop (RemFloatOp pre_float);
    "%", [], [ty_double; ty_double], ty_double, make_builtin_binop (RemFloatOp pre_double);
    "%", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (RemFloatOp pre_long_double);

    "<<", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (SlIntOp (pre_int, signed_int));
    "<<", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (SlIntOp (pre_int, false));
    "<<", [], [ty_long; ty_long], ty_long, make_builtin_binop (SlIntOp (pre_long, signed_long));
    "<<", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (SlIntOp (pre_long, false));

    ">>", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (SrIntOp (pre_int, signed_int));
    ">>", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (SrIntOp (pre_int, false));
    ">>", [], [ty_long; ty_long], ty_long, make_builtin_binop (SrIntOp (pre_long, signed_long));
    ">>", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (SrIntOp (pre_long, false));

    "&", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (AndIntOp (pre_int, signed_int));
    "&", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (AndIntOp (pre_int, false));
    "&", [], [ty_long; ty_long], ty_long, make_builtin_binop (AndIntOp (pre_long, signed_long));
    "&", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (AndIntOp (pre_long, false));

    "|", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (OrIntOp (pre_int, signed_int));
    "|", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (OrIntOp (pre_int, false));
    "|", [], [ty_long; ty_long], ty_long, make_builtin_binop (OrIntOp (pre_long, signed_long));
    "|", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (OrIntOp (pre_long, false));

    "^", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (XorIntOp (pre_int, signed_int));
    "^", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (XorIntOp (pre_int, false));
    "^", [], [ty_long; ty_long], ty_long, make_builtin_binop (XorIntOp (pre_long, signed_long));
    "^", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (XorIntOp (pre_long, false));

    "+=", [], [ty_char_ref;  ty_char],  ty_char_ref,  make_builtin_assignop (PlusIntOp (pre_char, signed_int));
    "+=", [], [ty_uchar_ref; ty_uchar], ty_uchar_ref, make_builtin_assignop (PlusIntOp (pre_char, false));
    "+=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (PlusIntOp (pre_int, signed_int));
    "+=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (PlusIntOp (pre_int, false));
    "+=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (PlusIntOp (pre_long, signed_long));
    "+=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (PlusIntOp (pre_long, false));
    "+=", [], [ty_float_ref; ty_float], ty_float_ref, make_builtin_assignop (PlusFloatOp pre_float);
    "+=", [], [ty_double_ref; ty_double], ty_double_ref, make_builtin_assignop (PlusFloatOp pre_double);
    "+=", [], [ty_long_double_ref; ty_long_double], ty_long_double_ref, make_builtin_assignop (PlusFloatOp pre_long_double);
    "+=", [], [ty_pointer_ref; ty_int], ty_pointer_ref, make_builtin_assignop PlusPointerOp;

    "-=", [], [ty_char_ref;  ty_char],  ty_char_ref,  make_builtin_assignop (MinusIntOp (pre_char, signed_int));
    "-=", [], [ty_uchar_ref; ty_uchar], ty_uchar_ref, make_builtin_assignop (MinusIntOp (pre_char, false));
    "-=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (MinusIntOp (pre_int, signed_int));
    "-=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (MinusIntOp (pre_int, false));
    "-=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (MinusIntOp (pre_long, signed_long));
    "-=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (MinusIntOp (pre_long, false));
    "-=", [], [ty_float_ref; ty_float], ty_float_ref, make_builtin_assignop (MinusFloatOp pre_float);
    "-=", [], [ty_double_ref; ty_double], ty_double_ref, make_builtin_assignop (MinusFloatOp pre_double);
    "-=", [], [ty_long_double_ref; ty_long_double], ty_long_double_ref, make_builtin_assignop (MinusFloatOp pre_long_double);
    "-=", [], [ty_pointer_ref; ty_int], ty_pointer_ref, make_builtin_assignop_pointer_minus;

    "*=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (MulIntOp (pre_int, signed_int));
    "*=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (MulIntOp (pre_int, false));
    "*=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (MulIntOp (pre_long, signed_long));
    "*=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (MulIntOp (pre_long, false));
    "*=", [], [ty_float_ref; ty_float], ty_float_ref, make_builtin_assignop (MulFloatOp pre_float);
    "*=", [], [ty_double_ref; ty_double], ty_double_ref, make_builtin_assignop (MulFloatOp pre_double);
    "*=", [], [ty_long_double_ref; ty_long_double], ty_long_double_ref, make_builtin_assignop (MulFloatOp pre_long_double);

    "/=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (DivIntOp (pre_int, signed_int));
    "/=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (DivIntOp (pre_int, false));
    "/=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (DivIntOp (pre_long, signed_long));
    "/=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (DivIntOp (pre_long, false));
    "/=", [], [ty_float_ref; ty_float], ty_float_ref, make_builtin_assignop (DivFloatOp pre_float);
    "/=", [], [ty_double_ref; ty_double], ty_double_ref, make_builtin_assignop (DivFloatOp pre_double);
    "/=", [], [ty_long_double_ref; ty_long_double], ty_long_double_ref, make_builtin_assignop (DivFloatOp pre_long_double);

    "%=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (RemIntOp (pre_int, signed_int));
    "%=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (RemIntOp (pre_int, false));
    "%=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (RemIntOp (pre_long, signed_long));
    "%=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (RemIntOp (pre_long, false));
    "%=", [], [ty_float_ref; ty_float], ty_float_ref, make_builtin_assignop (RemFloatOp pre_float);
    "%=", [], [ty_double_ref; ty_double], ty_double_ref, make_builtin_assignop (RemFloatOp pre_double);
    "%=", [], [ty_long_double_ref; ty_long_double], ty_long_double_ref, make_builtin_assignop (RemFloatOp pre_long_double);

    "<<=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (SlIntOp (pre_int, signed_int));
    "<<=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (SlIntOp (pre_int, false));
    "<<=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (SlIntOp (pre_long, signed_long));
    "<<=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (SlIntOp (pre_long, false));

    ">>=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (SrIntOp (pre_int, signed_int));
    ">>=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (SrIntOp (pre_int, false));
    ">>=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (SrIntOp (pre_long, signed_long));
    ">>=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (SrIntOp (pre_long, false));

    "&=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (AndIntOp (pre_int, signed_int));
    "&=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (AndIntOp (pre_int, false));
    "&=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (AndIntOp (pre_long, signed_long));
    "&=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (AndIntOp (pre_long, false));

    "|=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (OrIntOp (pre_int, signed_int));
    "|=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (OrIntOp (pre_int, false));
    "|=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (OrIntOp (pre_long, signed_long));
    "|=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (OrIntOp (pre_long, false));

    "^=", [], [ty_int_ref;  ty_int],  ty_int_ref,  make_builtin_assignop (XorIntOp (pre_int, signed_int));
    "^=", [], [ty_uint_ref; ty_uint], ty_uint_ref, make_builtin_assignop (XorIntOp (pre_int, false));
    "^=", [], [ty_long_ref; ty_long], ty_long_ref, make_builtin_assignop (XorIntOp (pre_long, signed_long));
    "^=", [], [ty_ulong_ref; ty_ulong], ty_ulong_ref, make_builtin_assignop (XorIntOp (pre_long, false));

    "min", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (MinIntOp (pre_int, signed_int));
    "min", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (MinIntOp (pre_int, false));
    "min", [], [ty_long; ty_long], ty_long, make_builtin_binop (MinIntOp (pre_long, signed_long));
    "min", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (MinIntOp (pre_long, false));
    "min", [], [ty_float; ty_float], ty_float, make_builtin_binop (MinFloatOp pre_float);
    "min", [], [ty_double; ty_double], ty_double, make_builtin_binop (MinFloatOp pre_double);
    "min", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (MinFloatOp pre_long_double);

    "max", [], [ty_int;  ty_int],  ty_int,  make_builtin_binop (MaxIntOp (pre_int, signed_int));
    "max", [], [ty_uint; ty_uint], ty_uint, make_builtin_binop (MaxIntOp (pre_int, false));
    "max", [], [ty_long; ty_long], ty_long, make_builtin_binop (MaxIntOp (pre_long, signed_long));
    "max", [], [ty_ulong; ty_ulong], ty_ulong, make_builtin_binop (MaxIntOp (pre_long, false));
    "max", [], [ty_float; ty_float], ty_float, make_builtin_binop (MaxFloatOp pre_float);
    "max", [], [ty_double; ty_double], ty_double, make_builtin_binop (MaxFloatOp pre_double);
    "max", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (MaxFloatOp pre_long_double);

    "abs", [], [ty_int],  ty_double,  make_builtin_unop (AbsFloatOp pre_double);
    "abs", [], [ty_uint], ty_double, make_builtin_unop (AbsFloatOp pre_double);
    "abs", [], [ty_long], ty_double, make_builtin_unop (AbsFloatOp pre_double);
    "abs", [], [ty_ulong], ty_double, make_builtin_unop (AbsFloatOp pre_double);
    "abs", [], [ty_float], ty_double, make_builtin_unop (AbsFloatOp pre_double);
    "abs", [], [ty_double], ty_double, make_builtin_unop (AbsFloatOp pre_double);
    "abs", [], [ty_double; ty_long_double], ty_long_double, make_builtin_unop (AbsFloatOp pre_long_double);

    "sin", [], [ty_int],  ty_double,  make_builtin_unop (SinOp pre_double);
    "sin", [], [ty_uint], ty_double, make_builtin_unop (SinOp pre_double);
    "sin", [], [ty_long], ty_double, make_builtin_unop (SinOp pre_double);
    "sin", [], [ty_ulong], ty_double, make_builtin_unop (SinOp pre_double);
    "sin", [], [ty_float], ty_double, make_builtin_unop (SinOp pre_double);
    "sin", [], [ty_double], ty_double, make_builtin_unop (SinOp pre_double);
    "sin", [], [ty_long_double], ty_long_double, make_builtin_unop (SinOp pre_long_double);

    "cos", [], [ty_int],  ty_double,  make_builtin_unop (CosOp pre_double);
    "cos", [], [ty_uint], ty_double, make_builtin_unop (CosOp pre_double);
    "cos", [], [ty_long], ty_double, make_builtin_unop (CosOp pre_double);
    "cos", [], [ty_ulong], ty_double, make_builtin_unop (CosOp pre_double);
    "cos", [], [ty_float], ty_double, make_builtin_unop (CosOp pre_double);
    "cos", [], [ty_double], ty_double, make_builtin_unop (CosOp pre_double);
    "cos", [], [ty_long_double], ty_long_double, make_builtin_unop (CosOp pre_long_double);

    "sqrt", [], [ty_int],  ty_double,  make_builtin_unop (SqrtOp pre_double);
    "sqrt", [], [ty_uint], ty_double, make_builtin_unop (SqrtOp pre_double);
    "sqrt", [], [ty_long], ty_double, make_builtin_unop (SqrtOp pre_double);
    "sqrt", [], [ty_ulong], ty_double, make_builtin_unop (SqrtOp pre_double);
    "sqrt", [], [ty_float], ty_double, make_builtin_unop (SqrtOp pre_double);
    "sqrt", [], [ty_double], ty_double, make_builtin_unop (SqrtOp pre_double);
    "sqrt", [], [ty_double; ty_long_double], ty_long_double, make_builtin_unop (SqrtOp pre_long_double);

    "atan2", [], [ty_int;  ty_int],  ty_double,  make_builtin_binop (Atan2Op pre_double);
    "atan2", [], [ty_uint; ty_uint], ty_double, make_builtin_binop (Atan2Op pre_double);
    "atan2", [], [ty_long; ty_long], ty_double, make_builtin_binop (Atan2Op pre_double);
    "atan2", [], [ty_ulong; ty_ulong], ty_double, make_builtin_binop (Atan2Op pre_double);
    "atan2", [], [ty_float; ty_float], ty_double, make_builtin_binop (Atan2Op pre_double);
    "atan2", [], [ty_double; ty_double], ty_double, make_builtin_binop (Atan2Op pre_double);
    "atan2", [], [ty_long_double; ty_long_double], ty_long_double, make_builtin_binop (Atan2Op pre_long_double);

    "==", [], [ty_int;  ty_int],  ty_bool, make_builtin_binop (EqIntOp (pre_int, signed_int));
    "==", [], [ty_uint; ty_uint], ty_bool, make_builtin_binop (EqIntOp (pre_int, false));
    "==", [], [ty_long; ty_long], ty_bool, make_builtin_binop (EqIntOp (pre_long, signed_long));
    "==", [], [ty_ulong; ty_ulong], ty_bool, make_builtin_binop (EqIntOp (pre_long, false));
    "==", [], [ty_float; ty_float], ty_bool, make_builtin_binop (EqFloatOp pre_float);
    "==", [], [ty_double; ty_double], ty_bool, make_builtin_binop (EqFloatOp pre_double);
    "==", [], [ty_long_double; ty_long_double], ty_bool, make_builtin_binop (EqFloatOp pre_long_double);
    "==", [v_ty], [ty_pointer; ty_pointer], ty_bool, make_builtin_binop EqPointerOp;
    "==", [v_ty], [ty_pointer; ty_int], ty_bool, make_builtin_binop EqPointerOp;
    "==", [v_ty], [ty_int; ty_pointer], ty_bool, make_builtin_binop EqPointerOp;

    "!=", [], [ty_int;  ty_int],  ty_bool, make_builtin_binop (NeqIntOp (pre_int, signed_int));
    "!=", [], [ty_uint; ty_uint], ty_bool, make_builtin_binop (NeqIntOp (pre_int, false));
    "!=", [], [ty_long; ty_long], ty_bool, make_builtin_binop (NeqIntOp (pre_long, signed_long));
    "!=", [], [ty_ulong; ty_ulong], ty_bool, make_builtin_binop (NeqIntOp (pre_long, false));
    "!=", [], [ty_float; ty_float], ty_bool, make_builtin_binop (NeqFloatOp pre_float);
    "!=", [], [ty_double; ty_double], ty_bool, make_builtin_binop (NeqFloatOp pre_double);
    "!=", [], [ty_long_double; ty_long_double], ty_bool, make_builtin_binop (NeqFloatOp pre_long_double);
    "!=", [v_ty], [ty_pointer; ty_pointer], ty_bool, make_builtin_binop NeqPointerOp;
    "!=", [v_ty], [ty_pointer; ty_int], ty_bool, make_builtin_binop NeqPointerOp;
    "!=", [v_ty], [ty_int; ty_pointer], ty_bool, make_builtin_binop NeqPointerOp;

    "<", [], [ty_int;  ty_int],  ty_bool, make_builtin_binop (LtIntOp (pre_int, signed_int));
    "<", [], [ty_uint; ty_uint], ty_bool, make_builtin_binop (LtIntOp (pre_int, false));
    "<", [], [ty_long; ty_long], ty_bool, make_builtin_binop (LtIntOp (pre_long, signed_long));
    "<", [], [ty_ulong; ty_ulong], ty_bool, make_builtin_binop (LtIntOp (pre_long, false));
    "<", [], [ty_float; ty_float], ty_bool, make_builtin_binop (LtFloatOp pre_float);
    "<", [], [ty_double; ty_double], ty_bool, make_builtin_binop (LtFloatOp pre_double);
    "<", [], [ty_long_double; ty_long_double], ty_bool, make_builtin_binop (LtFloatOp pre_long_double);
    "<", [v_ty], [ty_pointer; ty_pointer], ty_bool, make_builtin_binop LtPointerOp;

    "<=", [], [ty_int;  ty_int],  ty_bool,  make_builtin_binop (LeIntOp (pre_int, signed_int));
    "<=", [], [ty_uint; ty_uint], ty_bool, make_builtin_binop (LeIntOp (pre_int, false));
    "<=", [], [ty_long; ty_long], ty_bool, make_builtin_binop (LeIntOp (pre_long, signed_long));
    "<=", [], [ty_ulong; ty_ulong], ty_bool, make_builtin_binop (LeIntOp (pre_long, false));
    "<=", [], [ty_float; ty_float], ty_bool, make_builtin_binop (LeFloatOp pre_float);
    "<=", [], [ty_double; ty_double], ty_bool, make_builtin_binop (LeFloatOp pre_double);
    "<=", [], [ty_long_double; ty_long_double], ty_bool, make_builtin_binop (LeFloatOp pre_long_double);
    "<=", [v_ty], [ty_pointer; ty_pointer], ty_bool, make_builtin_binop LePointerOp;

    ">", [], [ty_int;  ty_int],  ty_bool,  make_builtin_binop (GtIntOp (pre_int, signed_int));
    ">", [], [ty_uint; ty_uint], ty_bool, make_builtin_binop (GtIntOp (pre_int, false));
    ">", [], [ty_long; ty_long], ty_bool, make_builtin_binop (GtIntOp (pre_long, signed_long));
    ">", [], [ty_ulong; ty_ulong], ty_bool, make_builtin_binop (GtIntOp (pre_long, false));
    ">", [], [ty_float; ty_float], ty_bool, make_builtin_binop (GtFloatOp pre_float);
    ">", [], [ty_double; ty_double], ty_bool, make_builtin_binop (GtFloatOp pre_double);
    ">", [], [ty_long_double; ty_long_double], ty_bool, make_builtin_binop (GtFloatOp pre_long_double);
    ">", [v_ty], [ty_pointer; ty_pointer], ty_bool, make_builtin_binop GtPointerOp;

    ">=", [], [ty_int;  ty_int],  ty_bool,  make_builtin_binop (GeIntOp (pre_int, signed_int));
    ">=", [], [ty_uint; ty_uint], ty_bool, make_builtin_binop (GeIntOp (pre_int, false));
    ">=", [], [ty_long; ty_long], ty_bool, make_builtin_binop (GeIntOp (pre_long, signed_long));
    ">=", [], [ty_ulong; ty_ulong], ty_bool, make_builtin_binop (GeIntOp (pre_long, false));
    ">=", [], [ty_float; ty_float], ty_bool, make_builtin_binop (GeFloatOp pre_float);
    ">=", [], [ty_double; ty_double], ty_bool, make_builtin_binop (GeFloatOp pre_double);
    ">=", [], [ty_long_double; ty_long_double], ty_bool, make_builtin_binop (GeFloatOp pre_long_double);
    ">=", [v_ty], [ty_pointer; ty_pointer], ty_bool, make_builtin_binop GePointerOp;

    "!", [], [ty_int],  ty_bool, make_builtin_binop2 (EqIntOp (pre_int, signed_int)) (zero_int pre_int signed_int);
    "!", [], [ty_uint], ty_bool, make_builtin_binop2 (EqIntOp (pre_int, false)) (zero_int pre_int false);
    "!", [], [ty_long], ty_bool, make_builtin_binop2 (EqIntOp (pre_long, signed_long)) (zero_int pre_long signed_long);
    "!", [], [ty_ulong], ty_bool, make_builtin_binop2 (EqIntOp (pre_long, false)) (zero_int pre_long false);
    "!", [], [ty_float], ty_bool, make_builtin_binop2 (EqFloatOp pre_float) (zero_float pre_float);
    "!", [], [ty_double], ty_bool, make_builtin_binop2 (EqFloatOp pre_double) (zero_float pre_double);
    "!", [], [ty_long_double], ty_bool, make_builtin_binop2 (EqFloatOp pre_long_double) (zero_float pre_long_double);
    "!", [v_ty], [ty_pointer], ty_bool, make_builtin_unop EqPointerNilOp;

    "=", [v_ty], [ty_var_ref; ty_var], ty_var_ref, (fun tenv loc pos v ty ty_args args e ->
          let a1, a2 = dest_atom2 loc args in
             LetAddrOf (pos, v, ty, a1,
             Set (pos, AtomVar (pos, v), ty, a2, e)));

    "&",  [], [ty_int; ty_int],    ty_int,  make_builtin_binop (AndIntOp (pre_int, signed_int));
    "&&", [], [ty_bool; ty_bool],  ty_bool, make_builtin_binop AndBoolOp;
    "|",  [], [ty_int; ty_int],    ty_int,  make_builtin_binop (OrIntOp (pre_int, signed_int));
    "||", [], [ty_bool; ty_bool],  ty_bool, make_builtin_binop OrBoolOp;

    "pasq_&&", [], [ty_int; ty_int],   ty_int,  make_builtin_binop (AndIntOp (pre_int, signed_int));
    "pasq_&&", [], [ty_bool; ty_bool], ty_bool, make_builtin_binop AndBoolOp;
    "pasq_||", [], [ty_int; ty_int],   ty_int,  make_builtin_binop (OrIntOp (pre_int, signed_int));
    "pasq_||", [], [ty_bool; ty_bool], ty_bool, make_builtin_binop OrBoolOp;

    "+", [v_ty], [ty_pointer; ty_int], ty_pointer, make_builtin_binop PlusPointerOp;
    "-", [v_ty], [ty_pointer; ty_int], ty_pointer, make_pointer_decr_int_op;
    "-", [v_ty], [ty_pointer; ty_pointer], ty_int, make_builtin_binop MinusPointerOp;

    "[]", [v_ty], [ty_pointer; ty_int], ty_var_ref, (fun tenv loc pos v ty ty_args args e ->
          let a1, a2 = dest_atom2 loc args in
	  let ty1, _ = dest_atom2 loc ty_args in
             LetSubscript (pos, v, ty, var_of_atom loc a1, ty1, a2, e));
    "*", [v_ty], [ty_pointer], ty_var_ref, (fun tenv loc pos v ty ty_args args e ->
          let a = dest_atom1 loc args in
	  let ty1 = dest_atom1 loc ty_args in
          let zero = AtomInt (pos, Rawint.of_int pre_int signed_int 0) in
             LetSubscript (pos, v, ty, var_of_atom loc a, ty1, zero, e));
    "&", [v_ty], [ty_var_ref], ty_pointer, (fun tenv loc pos v ty ty_args args e ->
          let a = dest_atom1 loc args in
             LetAddrOf (pos, v, ty, a, e));
    "->", [v_ty], [ty_pointer], ty_var_ref, (fun tenv loc pos v ty ty_args args e ->
          let a = dest_atom1 loc args in
	  let ty1 = dest_atom1 loc ty_args in
          let zero = AtomInt (pos, Rawint.of_int pre_int signed_int 0) in
             LetSubscript (pos, v, ty, var_of_atom loc a, ty1, zero, e));
    ",",  [v_ty1; v_ty2], [ty_var1; ty_var2], ty_var2, (fun tenv loc pos v ty ty_args args e ->
          let _, a2 = dest_atom2 loc args in
             LetAtom (pos, v, ty, a2, e));
    "++", [], [ty_char_ref],        ty_char,          make_pre_int_incr_op pre_char  signed_char;
    "++", [], [ty_short_ref],       ty_short,         make_pre_int_incr_op pre_short signed_short;
    "++", [], [ty_field_ref],       ty_int,           make_pre_int_incr_op pre_int   signed_int;
    "++", [], [ty_int_ref],         ty_int,           make_pre_int_incr_op pre_int   signed_int;
    "++", [], [ty_long_ref],        ty_long,          make_pre_int_incr_op pre_long  signed_long;
    "++", [], [ty_uchar_ref],       ty_uchar,         make_pre_int_incr_op pre_char  false;
    "++", [], [ty_ushort_ref],      ty_ushort,        make_pre_int_incr_op pre_short false;
    "++", [], [ty_uint_ref],        ty_uint,          make_pre_int_incr_op pre_int   false;
    "++", [], [ty_ulong_ref],       ty_ulong,         make_pre_int_incr_op pre_long  false;
    "++", [], [ty_float_ref],       ty_float,         make_pre_float_incr_op pre_float;
    "++", [], [ty_double_ref],      ty_double,        make_pre_float_incr_op pre_double;
    "++", [], [ty_long_double_ref], ty_long_double,   make_pre_float_incr_op pre_long_double;
    "++", [v_ty], [ty_pointer],     ty_pointer,       make_pre_pointer_incr_op;

    "--", [], [ty_char_ref],        ty_char,          make_pre_int_decr_op pre_char  signed_char;
    "--", [], [ty_short_ref],       ty_short,         make_pre_int_decr_op pre_short signed_short;
    "--", [], [ty_int_ref],         ty_int,           make_pre_int_decr_op pre_int   signed_int;
    "--", [], [ty_long_ref],        ty_long,          make_pre_int_decr_op pre_long  signed_long;
    "--", [], [ty_uchar_ref],       ty_uchar,         make_pre_int_decr_op pre_char  false;
    "--", [], [ty_ushort_ref],      ty_ushort,        make_pre_int_decr_op pre_short false;
    "--", [], [ty_uint_ref],        ty_uint,          make_pre_int_decr_op pre_int   false;
    "--", [], [ty_ulong_ref],       ty_ulong,         make_pre_int_decr_op pre_long  false;
    "--", [], [ty_float_ref],       ty_float,         make_pre_float_decr_op pre_float;
    "--", [], [ty_double_ref],      ty_double,        make_pre_float_decr_op pre_double;
    "--", [], [ty_long_double_ref], ty_long_double,   make_pre_float_decr_op pre_long_double;
    "--", [v_ty], [ty_pointer],     ty_pointer,       make_pre_pointer_decr_op;

    "++", [], [ty_char_ref; ty_int],        ty_char,          make_post_int_incr_op pre_char  signed_char;
    "++", [], [ty_short_ref; ty_int],       ty_short,         make_post_int_incr_op pre_short signed_short;
    "++", [], [ty_int_ref; ty_int],         ty_int,           make_post_int_incr_op pre_int   signed_int;
    "++", [], [ty_field_ref; ty_int],       ty_int,           make_post_int_incr_op pre_int   signed_int;
    "++", [], [ty_long_ref; ty_int],        ty_long,          make_post_int_incr_op pre_long  signed_long;
    "++", [], [ty_uchar_ref; ty_int],       ty_uchar,         make_post_int_incr_op pre_char  false;
    "++", [], [ty_ushort_ref; ty_int],      ty_ushort,        make_post_int_incr_op pre_short false;
    "++", [], [ty_long_ref; ty_int],        ty_uint,          make_post_int_incr_op pre_int   false;
    "++", [], [ty_ulong_ref; ty_int],       ty_ulong,         make_post_int_incr_op pre_long  false;
    "++", [], [ty_float_ref; ty_int],       ty_float,         make_post_float_incr_op pre_float;
    "++", [], [ty_double_ref; ty_int],      ty_double,        make_post_float_incr_op pre_double;
    "++", [], [ty_long_double_ref; ty_int], ty_long_double,   make_post_float_incr_op pre_long_double;
    "++", [v_ty], [ty_pointer; ty_int],     ty_pointer,       make_post_pointer_incr_op;

    "--", [], [ty_char_ref; ty_int],        ty_char,          make_post_int_decr_op pre_char  signed_char;
    "--", [], [ty_short_ref; ty_int],       ty_short,         make_post_int_decr_op pre_short signed_short;
    "--", [], [ty_int_ref; ty_int],         ty_int,           make_post_int_decr_op pre_int   signed_int;
    "--", [], [ty_long_ref; ty_int],        ty_long,          make_post_int_decr_op pre_long  signed_long;
    "--", [], [ty_uchar_ref; ty_int],       ty_uchar,         make_post_int_decr_op pre_char  false;
    "--", [], [ty_ushort_ref; ty_int],      ty_ushort,        make_post_int_decr_op pre_short false;
    "--", [], [ty_uint_ref; ty_int],        ty_uint,          make_post_int_decr_op pre_int   false;
    "--", [], [ty_ulong_ref; ty_int],       ty_ulong,         make_post_int_decr_op pre_long  false;
    "--", [], [ty_float_ref; ty_int],       ty_float,         make_post_float_decr_op pre_float;
    "--", [], [ty_double_ref; ty_int],      ty_double,        make_post_float_decr_op pre_double;
    "--", [], [ty_long_double_ref; ty_int], ty_long_double,   make_post_float_decr_op pre_long_double;
    "--", [v_ty], [ty_pointer; ty_int],     ty_pointer,       make_post_pointer_decr_op;
   ]

(*
 * The initial overloading table.
 *)
let fenv_empty =
   List.fold_left (fun fenv (name, vars, ty_vars, ty_res, op) ->
         let ty = TyAll (pos, StatusNormal, vars, TyFun (pos, StatusNormal, ty_vars, ty_res)) in
            SymbolMTable.add fenv (Symbol.add name) (ty, op)) SymbolMTable.empty builtin

(*
 * Add a function to the overloads list.
 *)
let fenv_add fenv label f ty =
   SymbolMTable.add fenv label (ty, make_apply f label)

(*
 * Look up the function in the overloads list.
 * If we fall through, build the default function type.
 *)
let fenv_lookup fenv loc pos label ty_args ty_res =
   try SymbolMTable.find_all fenv label with
      Not_found ->
         if Symbol.eq label apply_sym then
            make_apply_template label loc pos ty_args ty_res
         else
            raise (AstException (loc, UnboundVar label))

(*
 * Apply the overload.
 *)
let fenv_apply make tenv loc pos v ty_args ty_res args e =
   make tenv loc pos v ty_res ty_args args e

(*
 * A wrapper for varargs.
 *)
let fenv_stdargs = make_apply_stdargs

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
