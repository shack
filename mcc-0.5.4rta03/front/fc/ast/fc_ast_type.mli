(*
 * Utilities on the AST.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Rawint
open Location

open Fc_config

open Fc_ast
open Fc_ast_pos
open Fc_ast_env

(*
 * Some type constructors.
 *)
val ty_string : precision -> loc -> int array -> ty

(*
 * Atom should be a variable.
 *)
val var_of_atom : pos -> atom -> var
val type_of_atom : venv -> pos -> atom -> ty

(*
 * Expand a type definition so that it is not
 * a type application.
 *)
val apply_type : tenv -> pos -> ty_var -> ty list -> ty
val tenv_expand : tenv -> pos -> ty -> ty

(*
 * Type destructors.
 *)
val is_elide_type : tenv -> pos -> ty -> bool
val dest_elide_type : tenv -> pos -> ty -> ty list

val is_ref_type : tenv -> pos -> ty -> bool
val dest_ref_type : tenv -> pos -> ty -> ty
val dest_maybe_ref_type : tenv -> pos -> ty -> ty
val make_ref_type : tenv -> pos -> loc -> type_status -> ty -> ty

val dest_fun_type : tenv -> pos -> ty -> var list * ty list * ty
val dest_int_type : tenv -> pos -> ty -> loc * type_status * int_precision * int_signed

val is_aggregate_type : ty -> bool

val change_conformant_array : ty -> ty

(*
 * Stdarg argument type.
 *)
val stdarg_type : tenv -> pos -> ty -> int

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
