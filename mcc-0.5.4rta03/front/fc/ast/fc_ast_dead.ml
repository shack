(*
 * Deadcode elimination.
 * We want to eliminate useless arguments too,
 * so we first construct a dataflow graph, then calculate
 * the fixpoint.  Also, we try to eliminate useless types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_config
open Fc_ast
open Fc_ast_exn
open Fc_ast_pos
open Fc_ast_type

module Pos = MakePos (struct let name = "Fc_ast_dead" end)
open Pos

(*
 * Live set is a symbol set.
 *)
module LiveSet = SymbolSet

(************************************************************************
 * LIVENESS INITIALIZATION
 ************************************************************************)

(*
 * Add all nested functions to make sure we keep labeled functions.
 *)
let rec live_init live nest = function
   LetTypes (_, _, e)
 | LetString (_, _, _, _, _, e)
 | LetAtom (_, _, _, _, e)
 | LetTest (_, _, _, _, e)
 | LetCoerce (_, _, _, _, e)
 | LetUnop (_, _, _, _, _, e)
 | LetBinop (_, _, _, _, _, _, e)
 | LetExtCall (_, _, _, _, _, _, _, e)
 | LetAlloc (_, _, _, _, _, e)
 | LetMalloc (_, _, _, e)
 | LetSubscript (_, _, _, _, _, _, e)
 | LetVar (_, _, _, _, _, _, _, e)
 | LetAddrOf (_, _, _, _, e)
 | Set (_, _, _, _, e)
 | Debug (_, _, e)
 | LetApply (_, _, _, _, _, _, e)
 | LetProject (_, _, _, _, _, e)
 | LetSizeof (_, _, _, _, e) ->
      live_init live nest e

 | LetFunDecl (_, _, _, label, _, _, e) ->
      live_init (LiveSet.add live label) nest e

 | Raise _
 | Return _
 | TailCall _ ->
      live

 | Try (_, e1, _, e2)
 | IfThenElse (_, _, e1, e2) ->
      live_init (live_init live nest e1) nest e2

 | Match (_, _, _, cases) ->
      List.fold_left (fun live (_, e) ->
            live_init live nest e) live cases

 | LetFuns (_, fundefs, e) ->
      let live =
         List.fold_left (fun live (_, f, label, _, gflag, _, _, e) ->
               let nest = f :: nest in
               let live =
                  if LiveSet.mem live label then
                     List.fold_left LiveSet.add live nest
                  else
                     live
               in
                  live_init live nest e) live fundefs
      in
         live_init live nest e

let live_init_exp e =
   live_init LiveSet.empty [] e

(************************************************************************
 * TYPES
 ************************************************************************)
(*
 * Collect uses of the type.
 *)
let rec live_type live = function
   TyUnit _
 | TyDelayed _
 | TyZero _
 | TyInt _
 | TyField _
 | TyFloat _
 | TyVar _
 | TyEnum _
 | TyStruct (_, _, Label _)
 | TyUnion (_, _, Label _)
 | TyUEnum (_, _, Label _) ->
      live
 | TyPointer (_, _, ty)
 | TyRef (_, _, ty) ->
      live_type live ty
 | TyArray (_, _, ty, index1, index2) ->
      live_type (live_atom live index1) ty;
      live_type (live_atom live index2) ty
 | TyConfArray (_, _, ty, v1, v2, v_ty) ->
      live_type live ty;
      live_type live v_ty
 | TyFun (_, _, ty_args, ty_res) ->
      let live = List.fold_left live_type live ty_args in
         live_type live ty_res
 | TyLambda (_, _, _, ty)
 | TyAll (_, _, _, ty) ->
      live_type live ty
 | TyApply (_, _, v, tyl) ->
      let live = List.fold_left live_type live tyl in
         LiveSet.add live v
 | TyStruct (_, _, Fields fields)
 | TyUnion (_, _, Fields fields) ->
      List.fold_left (fun live (_, _, ty, _) ->
            live_type live ty) live fields
 | TyUEnum (_, _, Fields (label, fields)) ->
      List.fold_left (fun live (_, _, ty_opt, _) ->
            live_type_opt live ty_opt) (live_var live label) fields
 | TyTuple (_, _, tyl)
 | TyElide (_, _, tyl) ->
      live_types live tyl

and live_type_opt live = function
   Some ty -> live_type live ty
 | None -> live

and live_types live types =
   List.fold_left live_type live types

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Variable.
 *)
and live_var live v =
   LiveSet.add live v

(*
 * Atoms.
 *)
and live_atom live = function
   AtomVar (_, v) -> live_var live v
 | _ -> live

and live_atom_opt live = function
   Some a -> live_atom live a
 | None -> live

and live_atoms live atoms =
   List.fold_left live_atom live atoms

and live_init live = function
   InitNone ->
      live
 | InitAtom (_, a) ->
      live_atom live a
 | InitArray (_, fields) ->
      List.fold_left (fun live (v_opt, init) ->
            let live =
               match v_opt with
                  Some v -> live_var live v
                | None -> live
            in
               live_init live init) live fields

(*
 * Global declarations keep all their arguments.
 *)
and live_global_declare live gflag f =
   match gflag with
      VarGlobalClass ->
         LiveSet.add live f
    | VarStaticClass
    | VarLocalClass ->
         live

(*
 * Add types for live function arguments.
 *)
and live_args live vars ty =
   match ty with
      TyFun (_, _, ty_args, ty_res) ->
         let live =
            List.fold_left2 (fun live v ty ->
                  if LiveSet.mem live v then
                     live_type live ty
                  else
                     live) live vars ty_args
         in
            live_type live ty_res
    | ty ->
         live_type live ty

(*
 * Calculate defs/uses along each branch to a LetClosure or TailCall.
 *)
and live_exp live = function
   LetTypes (_, types, e) ->
      live_types_exp live types e
 | LetFuns (_, fundefs, e) ->
      live_funs_exp live fundefs e
 | LetFunDecl (_, _, v, _, _, ty, e)
 | LetString (_, v, ty, _, _, e) ->
      live_var_type_exp live v ty e
 | LetAtom (_, v, ty, a, e)
 | LetTest (_, v, ty, a, e)
 | LetCoerce (_, v, ty, a, e) ->
      live_atom_exp live v ty a e
 | LetUnop (_, v, ty, op, a, e) ->
      live_unop_exp live v ty op a e
 | LetBinop (_, v, ty, op, a1, a2, e) ->
      live_binop_exp live v ty op a1 a2 e
 | LetExtCall (_, v, ty, s, _, ty', args, e) ->
      live_extcall_exp live v ty s ty' args e
 | TailCall (_, f, args) ->
      live_tailcall_exp live f args
 | LetAlloc (_, v, ty, label, args, e) ->
      live_alloc_exp live v ty label args e
 | LetMalloc (_, v, a, e) ->
      live_malloc_exp live v a e
 | IfThenElse (_, a, e1, e2) ->
      live_if_exp live a e1 e2
 | LetSubscript (_, v, ty, v2, ty2, a3, e) ->
      live_subscript_exp live v ty v2 ty2 a3 e
 | LetVar (_, _, v, _, vc, ty, a_opt, e) ->
      live_var_exp live v vc ty a_opt e
 | LetAddrOf (_, v, ty, a, e) ->
      live_addr_of_exp live v ty a e
 | Set (_, a1, ty, a2, e) ->
      live_set_exp live a1 ty a2 e
 | Debug (_, info, e) ->
      live_debug_exp live info e
 | Raise (_, a) ->
      live_atom live a
 | Return (_, f, ty, a) ->
      live_return_exp live f ty a
 | Try (_, e1, v, e2) ->
      live_try_exp live e1 v e2
 | LetApply (_, v, ty, f, label, args, e) ->
      live_apply_exp live v ty f label args e
 | LetProject (_, v, ty, a, label, e) ->
      live_project_exp live v ty a label e
 | LetSizeof (_, v, ty1, ty2, e) ->
      live_sizeof_type_exp live v ty1 ty2 e
 | Match (_, a, ty, cases) ->
      live_match_exp live a ty cases

(*
 * Type definitions.
 *)
and live_types_exp live types e =
   let live = live_exp live e in
   let rec fixpoint live types =
       let live', fundefs =
         List.fold_left (fun (live, types) (ty_v, label, ty) ->
               if LiveSet.mem live ty_v || LiveSet.mem live label then
                  let live = live_type live ty in
                     live, types
                  else
                     live, (ty_v, label, ty) :: types) (live, []) types
      in
         if types <> [] && LiveSet.cardinal live' <> LiveSet.cardinal live then
            fixpoint live' types
         else
            live'
   in
      fixpoint live types

(*
 * Function definitions.
 * Calculate fixpoint based on live functions.
 *)
and live_funs_exp live fundefs e =
   let live = live_exp live e in
   let rec fixpoint live fundefs =
      let live', fundefs =
         List.fold_left (fun (live, fundefs) fund ->
               let _, f, _, _, gflag, ty, vars, e = fund in
               let live = List.fold_left LiveSet.add live vars in
               let live = live_global_declare live gflag f in
                  if LiveSet.mem live f then
                     let live = live_args live vars ty in
                     let live = live_exp live e in
                        live, fundefs
                  else
                     live, fund :: fundefs) (live, []) fundefs
      in
         if fundefs <> [] && LiveSet.cardinal live' <> LiveSet.cardinal live then
            fixpoint live' fundefs
         else
            live'
   in
      fixpoint live fundefs

(*
 * Operator.
 * Add the def.
 *)
and live_atom_exp live v ty a e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

and live_unop_exp live v ty op a e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

and live_binop_exp live v ty op a1 a2 e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_atom live a1 in
         let live = live_atom live a2 in
         let live = live_type live ty in
            live
      else
         live

(*
 * Don't ever delete extcalls--they may have side-effects.
 *)
and live_extcall_exp live v ty s ty' args e =
   let live = live_exp live e in
   let live = live_var live v in
   let live = live_type live ty' in
   let live = live_atoms live args in
   let live = live_type live ty in
      live

(*
 * Don't remove applications because they may have side-effects.
 *)
and live_apply_exp live v ty f label args e =
   let live = live_exp live e in
   let live = live_var live v in
   let live = live_var live f in
   let live = live_var live label in (* BUG: is this necessary? *)
   let live = live_atoms live args in
   let live = live_type live ty in
      live

(*
 * Return.
 *)
and live_return_exp live f ty a =
   let live = live_type live ty in
   let live = live_atom live a in
   let live = live_var live f in
      live

(*
 * Closure.
 * Some of the arguments may have been dropped.
 *)
and live_closure_exp live v f args e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         live_tailcall_exp live f args
      else
         live

(*
 * Tailcall.
 * Some of the arguments may have been dropped.
 *)
and live_tailcall_exp live f args =
   let live = live_var live f in
      live_atoms live args

(*
 * Memory allocations.
 *)
and live_alloc_exp live v ty label args e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_var live label in
         let live = live_atoms live args in
         let live = live_type live ty in
            live
      else
         live

and live_malloc_exp live v a e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
            live
      else
         live

and live_var_type_exp live v ty e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
            live
      else
         live

(*
 * Exceptions.
 *)
and live_try_exp live e1 v e2 =
   let live = live_exp live e2 in
      live_exp live e1

(*
 * Conditional.
 *)
and live_if_exp live a e1 e2 =
   let live = live_exp live e1 in
   let live = live_exp live e2 in
      live_atom live a

(*
 * Subscripting.
 * We can't eliminate side-effects.
 *)
and live_subscript_exp live v ty v2 ty2 a3 e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_type live ty2 in
         let live = live_var live v2 in
         let live = live_atom live a3 in
            live
      else
         live

and live_project_exp live v ty a label e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_atom live a in
         let live = live_var live label in
            live
      else
         live

(*
 * Variable operations.
 *)
and live_var_exp live v vc ty init e =
   let live = live_exp live e in
      if vc = VarGlobalClass || LiveSet.mem live v then
         let live = live_var live v in
         let live = live_type live ty in
         let live = live_init live init in
            live
      else
         live

and live_addr_of_exp live v ty a e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_atom live a in
            live
      else
         live

and live_set_exp live a1 ty a2 e =
   let live = live_exp live e in
   let live = live_atom live a1 in
   let live = live_type live ty in
   let live = live_atom live a2 in
      live

(*
 * Sizeof.
 *)
and live_sizeof_type_exp live v ty1 ty2 e =
   let live = live_exp live e in
      if LiveSet.mem live v then
         let live = live_type live ty1 in
         let live = live_type live ty2 in
            live
      else
         live

(*
 * Match.
 *)
and live_match_exp live a ty cases =
   let live = live_atom live a in
   let live = live_type live ty in
      List.fold_left (fun live (p, e) ->
            live_exp live e) live cases

(*
 * Debugging variables are preserved.
 *)
and live_debug_exp live info e =
   let live = live_exp live e in
      match info with
         DebugString _ ->
            live
       | DebugContext (_, vars) ->
            List.fold_left (fun live (_, ty, v) ->
                  live_var (live_type live ty) v) live vars

(************************************************************************
 * FIXPOINT
 ************************************************************************)

(*
 * Calculate the live variable fixpoint.
 *)
let fixpoint f live =
   let rec loop n live =
      let live = f live in
      let m = LiveSet.cardinal live in
         if m <> n then
            loop m live
         else
            live
   in
      loop (LiveSet.cardinal live) live

(*
 * Calculate the live sets.
 *)
let live_prog prog =
   fixpoint (fun live -> live_exp live prog) (live_init_exp prog)

(************************************************************************
 * DEADCODE ELIMINATION
 ************************************************************************)

(*
 * Eliminate dead fields from the frame.
 *)
let rec dead_type live ty =
   match ty with
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _
    | TyEnum _
    | TyStruct (_, _, Label _)
    | TyUnion (_, _, Label _)
    | TyUEnum (_, _, Label _) ->
         ty
    | TyPointer (pos, status, ty) ->
         TyPointer (pos, status, dead_type live ty)
    | TyRef (pos, status, ty) ->
         TyRef (pos, status, dead_type live ty)
    | TyArray (pos, status, ty, start, size) ->
         TyArray (pos, status, dead_type live ty, start, size)
    | TyConfArray (pos, status, ty, v1, v2, v_ty) ->
         TyConfArray (pos, status, dead_type live ty, v1, v2, dead_type live v_ty)
    | TyFun (pos, status, ty_args, ty_res) ->
         let ty_args = List.map (dead_type live) ty_args in
         let ty_res = dead_type live ty_res in
            TyFun (pos, status, ty_args, ty_res)
    | TyLambda (pos, status, vars, ty) ->
         TyLambda (pos, status, vars, dead_type live ty)
    | TyAll (pos, status, vars, ty) ->
         TyAll (pos, status, vars, dead_type live ty)
    | TyApply (pos, status, v, tyl) ->
         let tyl = List.map (dead_type live) tyl in
            TyApply (pos, status, v, tyl)
    | TyStruct (pos, status, Fields fields) ->
         let fields =
            List.map (fun (pos, v, ty, i_opt) ->
                  pos, v, dead_type live ty, i_opt) fields
         in
            TyStruct (pos, status, Fields fields)
    | TyUnion (pos, status, Fields fields) ->
         let fields =
            List.map (fun (pos, v, ty, i_opt) ->
                  pos, v, dead_type live ty, i_opt) fields
         in
            TyUnion (pos, status, Fields fields)
    | TyUEnum (pos, status, Fields (label, fields)) ->
         let fields =
            List.map (fun (pos, v, ty_opt, label) ->
                  pos, v, dead_type_opt live ty_opt, label) fields
         in
            TyUEnum (pos, status, Fields (label, fields))
    | TyTuple (pos, status, tyl) ->
         TyTuple (pos, status, List.map (dead_type live) tyl)
    | TyElide (pos, status, tyl) ->
         TyElide (pos, status, List.map (dead_type live) tyl)

and dead_type_opt live = function
   Some ty -> Some (dead_type live ty)
 | None -> None

(*
 * Eliminate dead expressions.
 *)
let rec dead_exp live e =
   let loc = string_pos "dead_exp" (exp_pos e) in
      match e with
         LetTypes (pos, types, e) ->
            dead_types_exp live loc pos types e
       | LetFuns (pos, fundefs, e) ->
            dead_funs_exp live loc pos fundefs e
       | LetFunDecl (pos, sc, f, label, gflag, ty, e) ->
            dead_fundecl_exp live loc pos sc f label gflag ty e
       | LetAtom (pos, v, ty, a, e) ->
            dead_atom_exp live loc pos v ty a e
       | LetTest (pos, v, ty, a, e) ->
            dead_test_exp live loc pos v ty a e
       | LetCoerce (pos, v, ty, a, e) ->
            dead_coerce_exp live loc pos v ty a e
       | LetUnop (pos, v, ty, op, a, e) ->
            dead_unop_exp live loc pos v ty op a e
       | LetBinop (pos, v, ty, op, a1, a2, e) ->
            dead_binop_exp live loc pos v ty op a1 a2 e
       | LetExtCall (pos, v, ty, s, b, ty', args, e) ->
            dead_extcall_exp live loc pos v ty s b ty' args e
       | TailCall (pos, f, args) ->
            dead_tailcall_exp live loc pos f args
       | LetAlloc (pos, v, ty, label, args, e) ->
            dead_alloc_exp live loc pos v ty label args e
       | LetMalloc (pos, v, a, e) ->
            dead_malloc_exp live loc pos v a e
       | LetString (pos, v, ty, pre, s, e) ->
            dead_string_exp live loc pos v ty pre s e
       | IfThenElse (pos, a, e1, e2) ->
            dead_if_exp live loc pos a e1 e2
       | LetSubscript (pos, v, ty, v2, ty2, a3, e) ->
            dead_subscript_exp live loc pos v ty v2 ty2 a3 e
       | LetProject (pos, v, ty, a, label, e) ->
            dead_project_exp live loc pos v ty a label e
       | LetVar (pos, sc, v, label, gflag, ty, a_opt, e) ->
            dead_var_exp live loc pos sc v label gflag ty a_opt e
       | LetAddrOf (pos, v, ty, a, e) ->
            dead_addr_of_exp live loc pos v ty a e
       | Set (pos, a1, ty, a2, e) ->
            dead_set_exp live loc pos a1 ty a2 e
       | Debug (pos, info, e) ->
            dead_debug_exp live loc pos info e
       | Raise _
       | Return _ ->
            e
       | LetApply (pos, v, ty, f, label, args, e) ->
            dead_apply_exp live loc pos v ty f label args e
       | Try (pos, e1, v, e2) ->
            dead_try_exp live loc pos e1 v e2
       | LetSizeof (pos, v, ty1, ty2, e) ->
            dead_sizeof_type_exp live loc pos v ty1 ty2 e
       | Match (pos, a, ty, cases) ->
            dead_match_exp live loc pos a ty cases

(*
 * Dead types.
 * Some types may be omitted.
 *)
and dead_types_exp live loc pos types e =
   let e = dead_exp live e in
   let types =
      List.fold_left (fun types (ty_v, label, ty) ->
            if LiveSet.mem live ty_v || LiveSet.mem live label then
               let ty = dead_type live ty in
                  (ty_v, label, ty) :: types
            else
               types) [] types
   in
      if types = [] then
         e
      else
         LetTypes (pos, types, e)

(*
 * Dead functions.
 * Some functions may be omitted.
 *)
and dead_funs_exp live loc pos fundefs e =
   let e = dead_exp live e in
   let fundefs =
      List.fold_left (fun fundefs (pos, f, label, line, gflag, ty, vars, e) ->
            if LiveSet.mem live f then
               let e = dead_exp live e in
               let ty = dead_type live ty in
               let fund = pos, f, label, line, gflag, ty, vars, e in
                  fund :: fundefs
            else
               fundefs) [] fundefs
   in
      if fundefs = [] then
         e
      else
         LetFuns (pos, List.rev fundefs, e)

(*
 * Function declaration.
 *)
and dead_fundecl_exp live loc pos sc f label gflag ty e =
   let e = dead_exp live e in
      if LiveSet.mem live f then
         LetFunDecl (pos, sc, f, label, gflag, dead_type live ty, e)
      else
         e

(*
 * Dead operator.
 *)
and dead_atom_exp live loc pos v ty a e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetAtom (pos, v, dead_type live ty, a, e)
      else
         e

and dead_test_exp live loc pos v ty a e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetTest (pos, v, dead_type live ty, a, e)
      else
         e

and dead_coerce_exp live loc pos v ty a e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetCoerce (pos, v, dead_type live ty, a, e)
      else
         e

and dead_unop_exp live loc pos v ty op a e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetUnop (pos, v, dead_type live ty, op, a, e)
      else
         e

and dead_binop_exp live loc pos v ty op a1 a2 e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetBinop (pos, v, dead_type live ty, op, a1, a2, e)
      else
         e

and dead_extcall_exp live loc pos v ty s b ty' args e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetExtCall (pos, v, dead_type live ty, s, b, dead_type live ty', args, e)
      else
         e

and dead_apply_exp live loc pos v ty f label args e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetApply (pos, v, dead_type live ty, f, label, args, e)
      else
         e

(*
 * Tailcall.
 * Again, be careful about dead arguments.
 *)
and dead_tailcall_exp live loc pos f args =
   TailCall (pos, f, args)

(*
 * Memory allocation.
 *)
and dead_alloc_exp live loc pos v ty label args e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetAlloc (pos, v, dead_type live ty, label, args, e)
      else
         e

and dead_malloc_exp live loc pos v a e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetMalloc (pos, v, a, e)
      else
         e

and dead_string_exp live loc pos v ty pre s e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetString (pos, v, dead_type live ty, pre, s, e)
      else
         e

(*
 * Conditional.
 *)
and dead_if_exp live loc pos a e1 e2 =
   let e1 = dead_exp live e1 in
   let e2 = dead_exp live e2 in
      IfThenElse (pos, a, e1, e2)

and dead_try_exp live loc pos e1 v e2 =
   let e1 = dead_exp live e1 in
   let e2 = dead_exp live e2 in
      Try (pos, e1, v, e2)

(*
 * Subscripting.
 *)
and dead_subscript_exp live loc pos v ty v2 ty2 a3 e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetSubscript (pos, v, dead_type live ty, v2, dead_type live ty2, a3, e)
      else
         e

and dead_project_exp live loc pos v ty a label e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetProject (pos, v, dead_type live ty, a, label, e)
      else
         e

(*
 * Variable operations.
 *)
and dead_var_exp live loc pos sc v label gflag ty a_opt e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetVar (pos, sc, v, label, gflag, dead_type live ty, a_opt, e)
      else
         e

and dead_addr_of_exp live loc pos v ty a e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetAddrOf (pos, v, dead_type live ty, a, e)
      else
         e

and dead_set_exp live loc pos a1 ty a2 e =
   let e = dead_exp live e in
      Set (pos, a1, dead_type live ty, a2, e)

(*
 * Sizeof.
 *)
and dead_sizeof_type_exp live loc pos v ty1 ty2 e =
   let e = dead_exp live e in
      if LiveSet.mem live v then
         LetSizeof (pos, v, dead_type live ty1, dead_type live ty2, e)
      else
         e

(*
 * Matching.
 *)
and dead_match_exp live loc pos a ty cases =
   let ty = dead_type live ty in
   let cases =
      List.map (fun (p, e) ->
            p, dead_exp live e) cases
   in
      Match (pos, a, ty, cases)

(*
 * Debug.
 *)
and dead_debug_exp live loc pos info e =
   let e = dead_exp live e in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) ->
                     v1, dead_type live ty, v2) vars
            in
               DebugContext (line, vars)
   in
      Debug (pos, info, e)

(************************************************************************
 * GLOBAL FUNCTION
 ************************************************************************)

(*
 * Dead program.
 *)
let dead_exp exp =
   (* Initial live variables *)
   let live = live_prog exp in
      dead_exp live exp

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
