(*
 * Utilities on AST.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_config
open Fc_ast

(*
 * Status information.
 *)
let status_of_type = function
   TyDelayed (_, status)
 | TyUnit (_, status, _)
 | TyZero (_, status)
 | TyInt (_, status, _, _)
 | TyField (_, status, _, _, _, _)
 | TyFloat (_, status, _)
 | TyVar (_, status, _)
 | TyArray (_, status, _, _, _)
 | TyConfArray (_, status, _, _, _, _)
 | TyPointer (_, status, _)
 | TyRef (_, status, _)
 | TyTuple (_, status, _)
 | TyEnum (_, status, _)
 | TyUEnum (_, status, _)
 | TyUnion (_, status, _)
 | TyStruct (_, status, _)
 | TyFun (_, status, _, _)
 | TyLambda (_, status, _, _)
 | TyAll (_, status, _, _)
 | TyApply (_, status, _, _)
 | TyElide (_, status, _) ->
      status

(*
 * Position information.
 *)
let loc_of_type = function
   TyDelayed (loc, _)
 | TyUnit (loc, _, _)
 | TyZero (loc, _)
 | TyInt (loc, _, _, _)
 | TyField (loc, _, _, _, _, _)
 | TyFloat (loc, _, _)
 | TyVar (loc, _, _)
 | TyArray (loc, _, _, _, _)
 | TyConfArray (loc, _, _, _, _, _)
 | TyPointer (loc, _, _)
 | TyRef (loc, _, _)
 | TyTuple (loc, _, _)
 | TyEnum (loc, _, _)
 | TyUEnum (loc, _, _)
 | TyUnion (loc, _, _)
 | TyStruct (loc, _, _)
 | TyFun (loc, _, _, _)
 | TyLambda (loc, _, _, _)
 | TyAll (loc, _, _, _)
 | TyApply (loc, _, _, _)
 | TyElide (loc, _, _) ->
      loc

let loc_of_atom = function
   AtomUnit (loc, _, _)
 | AtomInt (loc, _)
 | AtomFloat (loc, _)
 | AtomNil (loc, _)
 | AtomVar (loc, _)
 | AtomEnum (loc, _, _) ->
      loc

let loc_of_expr = function
   Return (loc, _, _, _)
 | Raise  (loc, _)

 | LetTypes (loc, _, _)
 | LetFuns  (loc, _, _)
 | Match    (loc, _, _, _)
 | TailCall (loc, _, _)
 | Debug    (loc, _, _)

 | Try         (loc, _, _, _)
 | LetMalloc   (loc, _, _, _)
 | LetAlloc    (loc, _, _, _, _, _)
 | IfThenElse  (loc, _, _, _)

 | LetTest       (loc, _, _, _, _)
 | LetCoerce     (loc, _, _, _, _)
 | LetString     (loc, _, _, _, _, _)
 | LetSizeof (loc, _, _, _, _)
 | LetProject (loc, _, _, _, _, _)

 | LetAtom  (loc, _, _, _, _)
 | LetUnop  (loc, _, _, _, _, _)
 | LetBinop (loc, _, _, _, _, _, _)
 | LetExtCall (loc, _, _, _, _, _, _, _)
 | LetApply (loc, _, _, _, _, _, _)
 | LetVar   (loc, _, _, _, _, _, _, _)
 | LetFunDecl (loc, _, _, _, _, _, _)

 | Set (loc, _, _, _, _)
 | LetSubscript (loc, _, _, _, _, _, _)
 | LetAddrOf (loc, _, _, _, _) ->
      loc

(*
 * Value finds the nearest return statement.
 *)
let rec value_of_expr = function
   Return (_, _, _, AtomVar _)
 | TailCall _
 | Match _
 | IfThenElse _
 | Raise  _
 | LetTypes _
 | LetFuns _
 | Try _
 | LetExtCall _
 | Set _ ->
      None

 |  Return (_, _, _, a) ->
      Some a

 | Debug         (_, _, e)
 | LetMalloc     (_, _, _, e)
 | LetAlloc      (_, _, _, _, _, e)
 | LetTest       (_, _, _, _, e)
 | LetCoerce     (_, _, _, _, e)
 | LetString     (_, _, _, _, _, e)
 | LetSizeof (_, _, _, _, e)
 | LetProject    (_, _, _, _, _, e)
 | LetAtom       (_, _, _, _, e)
 | LetUnop       (_, _, _, _, _, e)
 | LetBinop      (_, _, _, _, _, _, e)
 | LetApply      (_, _, _, _, _, _, e)
 | LetVar        (_, _, _, _, _, _, _, e)
 | LetFunDecl    (_, _, _, _, _, _, e)
 | LetSubscript  (_, _, _, _, _, _, e)
 | LetAddrOf     (_, _, _, _, e) ->
      value_of_expr e

(*
 * Map a function over all types.
 *)
let map_type_opt f = function
   Some t ->
      Some (f t)
 | None ->
      None

let map_type_atom f a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomVar _ ->
         a
    | AtomNil (loc, ty) ->
         AtomNil (loc, f ty)
    | AtomEnum (loc, ty, i) ->
         AtomEnum (loc, f ty, i)

let map_type_atom_opt f = function
   Some a -> Some (map_type_atom f a)
 | None -> None

let rec map_type_init f init =
   match init with
      InitNone ->
         init
    | InitAtom (loc, a) ->
         InitAtom (loc, map_type_atom f a)
    | InitArray (loc, fields) ->
         let fields =
            List.map (fun (v_opt, init) ->
                  v_opt, map_type_init f init) fields
         in
            InitArray (loc, fields)

let map_type_debug f info =
   match info with
      DebugString _ -> info
    | DebugContext (line, vars) ->
         DebugContext (line, List.map (fun (v1, ty, v2) -> v1, f ty, v2) vars)

let rec map_type_patt f p =
   match p with
      IntPattern _
    | FloatPattern _
    | StringPattern _ ->
         p
    | VarPattern (loc, v, ty_opt) ->
         VarPattern (loc, v, map_type_opt f ty_opt)
    | StructPattern (loc, fields) ->
         let fields = List.map (fun (v, p) -> v, map_type_patt f p) fields in
            StructPattern (loc, fields)
    | EnumPattern (loc, ty, i) ->
         EnumPattern (loc, f ty, i)
    | UEnumPattern (loc, ty, l, p_opt) ->
         UEnumPattern (loc, f ty, l, map_type_patt_opt f p_opt)
    | VEnumPattern (loc, v, p) ->
         VEnumPattern (loc, v, map_type_patt f p)
    | AsPattern (loc, p1, p2) ->
         AsPattern (loc, map_type_patt f p1, map_type_patt f p2)

and map_type_patt_opt f = function
   Some p ->
      Some (map_type_patt f p)
 | None ->
      None

let rec map_type_exp f e =
   match e with
      LetTypes (loc, types, e) ->
         LetTypes (loc, List.map (fun (v, label, ty) -> v, label, f ty) types, map_type_exp f e)
    | LetFuns (loc, funs, e) ->
         let funs =
            List.map (fun (loc, g, label, line, gflag, ty, vars, e) ->
                  loc, g, label, line, gflag, f ty, vars, map_type_exp f e) funs
         in
            LetFuns (loc, funs, map_type_exp f e)
    | LetFunDecl (loc, sc, v, label, gflag, ty, e) ->
         LetFunDecl (loc, sc, v, label, gflag, f ty, map_type_exp f e)
    | LetVar (loc, sc, v, label, gflag, ty, init, e) ->
         LetVar (loc, sc, v, label, gflag, f ty, map_type_init f init, map_type_exp f e)
    | LetTest (loc, v, ty, a, e) ->
         LetTest (loc, v, f ty, map_type_atom f a, map_type_exp f e)
    | LetCoerce (loc, v, ty, a, e) ->
         LetCoerce (loc, v, f ty, map_type_atom f a, map_type_exp f e)
    | LetAtom (loc, v, ty, a, e) ->
         LetAtom (loc, v, f ty, map_type_atom f a, map_type_exp f e)
    | LetUnop (loc, v, ty, op, a, e) ->
         LetUnop (loc, v, f ty, op, map_type_atom f a, map_type_exp f e)
    | LetBinop (loc, v, ty, op, a1, a2, e) ->
         LetBinop (loc, v, f ty, op, map_type_atom f a1, map_type_atom f a2, map_type_exp f e)
    | LetExtCall (loc, v, ty, s, b, ty', args, e) ->
         LetExtCall (loc, v, f ty, s, b, f ty', List.map (map_type_atom f) args, e)
    | LetApply (loc, v, ty, g, label, args, e) ->
         LetApply (loc, v, f ty, g, label, List.map (map_type_atom f) args, map_type_exp f e)
    | TailCall (loc, g, args) ->
         TailCall (loc, g, List.map (map_type_atom f) args)
    | Return (loc, v, ty, a) ->
         Return (loc, v, f ty, map_type_atom f a)
    | LetString (loc, v, ty, pre, s, e) ->
         LetString (loc, v, f ty, pre, s, map_type_exp f e)
    | LetAlloc (loc, v, ty, label, args, e) ->
         LetAlloc (loc, v, f ty, label, List.map (map_type_atom f) args, map_type_exp f e)
    | LetMalloc (loc, v, a, e) ->
         LetMalloc (loc, v, map_type_atom f a, map_type_exp f e)
    | IfThenElse (loc, a, e1, e2) ->
         IfThenElse (loc, map_type_atom f a, map_type_exp f e1, map_type_exp f e2)
    | Match (loc, a, ty, cases) ->
         Match (loc, map_type_atom f a, f ty, List.map (fun (p, e) -> map_type_patt f p, map_type_exp f e) cases)
    | Try (loc, e1, v, e2) ->
         Try (loc, map_type_exp f e1, v, map_type_exp f e2)
    | Raise (loc, a) ->
         Raise (loc, map_type_atom f a)
    | LetSizeof (loc, v, ty1, ty2, e) ->
         LetSizeof (loc, v, f ty1, f ty2, map_type_exp f e)
    | Debug (loc, info, e) ->
         Debug (loc, map_type_debug f info, map_type_exp f e)
    | LetProject (loc, v, ty, v1, v2, e) ->
         LetProject (loc, v, f ty, v1, v2, map_type_exp f e)
    | LetSubscript (loc, v, ty, v1, ty2, a2, e) ->
         LetSubscript (loc, v, f ty, v1, f ty2, map_type_atom f a2, map_type_exp f e)
    | Set (loc, a1, ty, a2, e) ->
         Set (loc, map_type_atom f a1, f ty, map_type_atom f a2, map_type_exp f e)
    | LetAddrOf (loc, v, ty, a, e) ->
         LetAddrOf (loc, v, f ty, map_type_atom f a, map_type_exp f e)


(*
 * "Squash" all function types so that array parameters
 * become pointers.
 *)
let rec squash_type ty =
   match ty with
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _
    | TyEnum _ ->
         ty
    | TyPointer (loc, status, ty) ->
         TyPointer (loc, status, squash_fun_type ty)
    | TyRef (loc, status, ty) ->
         TyRef (loc, status, squash_fun_type ty)
    | TyTuple (loc, status, tyl) ->
         TyTuple (loc, status, List.map squash_fun_type tyl)
    | TyUEnum (loc, status, fields) ->
         TyUEnum (loc, status, squash_uenum_fields fields)
    | TyUnion (loc, status, fields) ->
         TyUnion (loc, status, squash_struct_fields fields)
    | TyStruct (loc, status, fields) ->
         TyStruct (loc, status, squash_struct_fields fields)
    | TyLambda (loc, status, vars, ty) ->
         TyLambda (loc, status, vars, squash_fun_type ty)
    | TyApply (loc, status, v, tyl) ->
         TyApply (loc, status, v, List.map squash_fun_type tyl)
    | TyArray (loc, status, ty, _, _) ->
         TyPointer (loc, status, squash_fun_type ty)
    | TyConfArray (loc, status, ty, _, _, _) ->
         TyPointer (loc, status, squash_fun_type ty)
    | TyFun (loc, status, ty_args, ty_res) ->
         let ty_args = List.map squash_type ty_args in
         let ty_res = squash_type ty_res in
            TyFun (loc, status, ty_args, ty_res)
    | TyAll (loc, status, vars, ty) ->
         TyAll (loc, status, vars, squash_fun_type ty)
    | TyElide (loc, status, tyl) ->
         TyElide (loc, status, List.map squash_fun_type tyl)

and squash_fun_type ty =
   match ty with
      TyUnit _
    | TyDelayed _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _
    | TyEnum _ ->
         ty
    | TyPointer (loc, status, ty) ->
         TyPointer (loc, status, squash_fun_type ty)
    | TyRef (loc, status, ty) ->
         TyRef (loc, status, squash_fun_type ty)
    | TyTuple (loc, status, tyl) ->
         TyTuple (loc, status, List.map squash_fun_type tyl)
    | TyUEnum (loc, status, fields) ->
         TyUEnum (loc, status, squash_uenum_fields fields)
    | TyUnion (loc, status, fields) ->
         TyUnion (loc, status, squash_struct_fields fields)
    | TyStruct (loc, status, fields) ->
         TyStruct (loc, status, squash_struct_fields fields)
    | TyLambda (loc, status, vars, ty) ->
         TyLambda (loc, status, vars, squash_fun_type ty)
    | TyApply (loc, status, v, tyl) ->
         TyApply (loc, status, v, List.map squash_fun_type tyl)
    | TyArray (loc, status, ty, a1, a2) ->
         TyArray (loc, status, squash_fun_type ty, squash_type_atom a1, squash_type_atom a2)
    | TyConfArray (loc, status, ty, v1, v2, v_ty) ->
         TyArray (loc, status, squash_fun_type ty, AtomVar(loc, v1), AtomVar(loc, v2))
    | TyFun (loc, status, ty_args, ty_res) ->
         let ty_args = List.map squash_type ty_args in
         let ty_res = squash_type ty_res in
            TyFun (loc, status, ty_args, ty_res)
    | TyAll (loc, status, vars, ty) ->
         TyAll (loc, status, vars, squash_fun_type ty)
    | TyElide (loc, status, tyl) ->
         TyElide (loc, status, List.map squash_fun_type tyl)

and squash_uenum_field (loc, v, ty_opt, label) =
   let ty_opt =
      match ty_opt with
         Some ty -> Some (squash_fun_type ty)
       | None -> None
   in
      loc, v, ty_opt, label

and squash_uenum_fields fields =
   match fields with
      Fields (label, fields) ->
         Fields (label, List.map squash_uenum_field fields)
    | Label _ ->
         fields

and squash_struct_field (loc, v, ty, i_opt) =
   loc, v, squash_fun_type ty, i_opt

and squash_struct_fields fields =
   match fields with
      Fields fields ->
         Fields (List.map squash_struct_field fields)
    | Label _ ->
         fields

and squash_type_opt = function
   Some ty ->
      Some (squash_type ty)
 | None ->
      None

(*
 * Squash the function types in the atom.
 *)
and squash_type_atom a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomVar _ ->
         a
    | AtomNil (loc, ty) ->
         AtomNil (loc, squash_fun_type ty)
    | AtomEnum (loc, ty, i) ->
         AtomEnum (loc, squash_fun_type ty, i)

and squash_type_atom_opt = function
   Some a -> Some (squash_type_atom a)
 | None -> None

let rec squash_type_init init =
   match init with
      InitNone ->
         init
    | InitAtom (loc, a) ->
         InitAtom (loc, squash_type_atom a)
    | InitArray (loc, fields) ->
         let fields =
            List.map (fun (v_opt, init) ->
                  v_opt, squash_type_init init) fields
         in
            InitArray (loc, fields)

(*
 * Squash the types in the debug info.
 *)
let squash_type_debug info =
   match info with
      DebugString _ -> info
    | DebugContext (line, vars) ->
         DebugContext (line, List.map (fun (v1, ty, v2) -> v1, squash_fun_type ty, v2) vars)

(*
 * Squash types in a pattern.
 *)
let rec squash_type_patt p =
   match p with
      IntPattern _
    | FloatPattern _
    | StringPattern _ ->
         p
    | VarPattern (loc, v, ty_opt) ->
         VarPattern (loc, v, squash_type_opt ty_opt)
    | StructPattern (loc, fields) ->
         let fields = List.map (fun (v, p) -> v, squash_type_patt p) fields in
            StructPattern (loc, fields)
    | EnumPattern (loc, ty, i) ->
         EnumPattern (loc, squash_type ty, i)
    | UEnumPattern (loc, ty, l, p_opt) ->
         UEnumPattern (loc, ty, l, squash_type_patt_opt p_opt)
    | VEnumPattern (loc, v, p) ->
         VEnumPattern (loc, v, squash_type_patt p)
    | AsPattern (loc, p1, p2) ->
         AsPattern (loc, squash_type_patt p1, squash_type_patt p2)

and squash_type_patt_opt = function
   Some p ->
      Some (squash_type_patt p)
 | None ->
      None

(*
 * Squash the function types in the expression.
 *)
let rec squash_type_exp e =
   match e with
      LetTypes (loc, types, e) ->
         LetTypes (loc, List.map (fun (v, label, ty) -> v, label, squash_fun_type ty) types, squash_type_exp e)
    | LetFuns (loc, funs, e) ->
         let funs =
            List.map (fun (loc, g, label, line, gflag, ty, vars, e) ->
                  loc, g, label, line, gflag, squash_fun_type ty, vars, squash_type_exp e) funs
         in
            LetFuns (loc, funs, squash_type_exp e)
    | LetFunDecl (loc, sc, v, label, gflag, ty, e) ->
         LetFunDecl (loc, sc, v, label, gflag, squash_fun_type ty, squash_type_exp e)
    | LetVar (loc, sc, v, label, gflag, ty, init, e) ->
         LetVar (loc, sc, v, label, gflag, squash_fun_type ty, squash_type_init init, squash_type_exp e)
    | LetTest (loc, v, ty, a, e) ->
         LetTest (loc, v, squash_fun_type ty, squash_type_atom a, squash_type_exp e)
    | LetCoerce (loc, v, ty, a, e) ->
         LetCoerce (loc, v, squash_fun_type ty, squash_type_atom a, squash_type_exp e)
    | LetAtom (loc, v, ty, a, e) ->
         LetAtom (loc, v, squash_fun_type ty, squash_type_atom a, squash_type_exp e)
    | LetUnop (loc, v, ty, op, a, e) ->
         LetUnop (loc, v, squash_fun_type ty, op, squash_type_atom a, squash_type_exp e)
    | LetBinop (loc, v, ty, op, a1, a2, e) ->
         LetBinop (loc, v, squash_fun_type ty, op, squash_type_atom a1, squash_type_atom a2, squash_type_exp e)
    | LetExtCall (loc, v, ty, s, b, ty', args, e) ->
         LetExtCall (loc, v, squash_fun_type ty, s, b, squash_fun_type ty', List.map squash_type_atom args, squash_type_exp e)
    | LetApply (loc, v, ty, g, label, args, e) ->
         LetApply (loc, v, squash_fun_type ty, g, label, List.map squash_type_atom args, squash_type_exp e)
    | TailCall (loc, g, args) ->
         TailCall (loc, g, List.map squash_type_atom args)
    | Return (loc, v, ty, a) ->
         Return (loc, v, squash_fun_type ty, squash_type_atom a)
    | LetString (loc, v, ty, pre, s, e) ->
         LetString (loc, v, squash_type ty, pre, s, squash_type_exp e)
    | LetAlloc (loc, v, ty, label, args, e) ->
         LetAlloc (loc, v, squash_type ty, label, List.map squash_type_atom args, squash_type_exp e)
    | LetMalloc (loc, v, a, e) ->
         LetMalloc (loc, v, squash_type_atom a, squash_type_exp e)
    | IfThenElse (loc, a, e1, e2) ->
         IfThenElse (loc, squash_type_atom a, squash_type_exp e1, squash_type_exp e2)
    | Match (loc, a, ty, cases) ->
         Match (loc, squash_type_atom a, squash_type ty, List.map (fun (p, e) -> squash_type_patt p, squash_type_exp e) cases)
    | Try (loc, e1, v, e2) ->
         Try (loc, squash_type_exp e1, v, squash_type_exp e2)
    | Raise (loc, a) ->
         Raise (loc, squash_type_atom a)
    | LetSizeof (loc, v, ty1, ty2, e) ->
         LetSizeof (loc, v, squash_type ty1, squash_fun_type ty2, squash_type_exp e)
    | Debug (loc, info, e) ->
         Debug (loc, squash_type_debug info, squash_type_exp e)
    | LetProject (loc, v, ty, v1, v2, e) ->
         LetProject (loc, v, squash_type ty, v1, v2, squash_type_exp e)
    | LetSubscript (loc, v, ty, v1, ty2, a2, e) ->
         LetSubscript (loc, v, squash_type ty, v1, squash_type ty2, squash_type_atom a2, squash_type_exp e)
    | Set (loc, a1, ty, a2, e) ->
         Set (loc, squash_type_atom a1, squash_type ty, squash_type_atom a2, squash_type_exp e)
    | LetAddrOf (loc, v, ty, a, e) ->
         LetAddrOf (loc, v, squash_type ty, squash_type_atom a, squash_type_exp e)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
