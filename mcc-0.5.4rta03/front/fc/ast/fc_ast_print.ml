(*
 * Utilities for the IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fc_config
open Fc_ast

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Default tabstop.
 *)
let tabstop = Fir_state.tabstop

(*
 * Inherit printing.
 *)
let pp_print_status = Fc_parse_print.pp_print_type_status
let pp_print_storage_class = Fc_parse_print.pp_print_storage_class

(*
 * Separated list of fields.
 *)
let pp_print_sep_list buf sep printer l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       pp_print_string buf sep;
                       pp_print_space buf ()
                    end;
                 pp_open_hvbox buf tabstop;
                 printer buf x;
                 pp_close_box buf ();
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_pre_sep_list buf sep printer l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       pp_print_space buf ();
                       pp_print_string buf sep
                    end;
                 pp_open_hvbox buf tabstop;
                 printer buf x;
                 pp_close_box buf ();
                 false) true l);
   pp_close_box buf ()

(*
 * Prefixed list of items.
 *)
let pp_print_prefix_list buf sep printer l =
   pp_open_hvbox buf 0;
   List.iter (fun x ->
         pp_print_string buf sep;
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         printer buf x;
         pp_close_box buf ()) l;
   pp_close_box buf ()

(*
 * Precisions.
 *)
let pp_print_int_precision buf pre signed =
   let s =
      match pre, signed with
         Rawint.Int8,  true -> "signed char"
       | Rawint.Int16, true -> "short"
       | Rawint.Int32, true -> "int"
       | Rawint.Int64, true -> "long"
       | Rawint.Int8,  false -> "unsigned char"
       | Rawint.Int16, false -> "unsigned short"
       | Rawint.Int32, false -> "unsigned int"
       | Rawint.Int64, false -> "unsigned long"
   in
      pp_print_string buf s

let pp_print_float_precision buf pre =
   let s =
      match pre with
         Rawfloat.Single -> "float"
       | Rawfloat.Double -> "double"
       | Rawfloat.LongDouble -> "long double"
   in
      pp_print_string buf s

(*
 * Print a type.
 * Use precedences to reduce the
 * amount of parentheses.
 *)
let prec_none   = 0
let prec_fun    = 1
let prec_struct = 2
let prec_array  = 3

let rec pp_print_type buf pre = function
   TyUnit (_, status, 0) ->
      pp_print_status buf status;
      pp_print_string buf "void"
 | TyUnit (_, status, 1) ->
      pp_print_status buf status;
      pp_print_string buf "unit"
 | TyUnit (_, status, 2) ->
      pp_print_status buf status;
      pp_print_string buf "bool"
 | TyUnit (_, status, i) ->
      pp_print_status buf status;
      pp_print_string buf "unit{";
      pp_print_int buf i;
      pp_print_string buf "}"
 | TyZero (_, status) ->
      pp_print_status buf status;
      pp_print_string buf "$zero"
 | TyInt (_, status, pre, signed) ->
      pp_print_status buf status;
      pp_print_int_precision buf pre signed
 | TyField (_, status, pre, signed, off, len) ->
      pp_print_status buf status;
      pp_print_int_precision buf pre signed;
      pp_print_string buf "{off:";
      pp_print_int buf off;
      pp_print_string buf "; len:";
      pp_print_int buf len;
      pp_print_string buf "}"
 | TyFloat (_, status, pre) ->
      pp_print_status buf status;
      pp_print_float_precision buf pre
 | TyArray (_, status, ty, a1, a2) ->
      pp_print_status buf status;
      pp_print_type buf prec_array ty;
      pp_print_string buf "[";
      pp_print_atom buf a1;
      pp_print_string buf "..";
      pp_print_atom buf a2;
      pp_print_string buf "]"
 | TyConfArray (_, status, ty, v1, v2, v_ty) ->
      pp_print_status buf status;
      pp_print_type buf prec_array ty;
      pp_print_string buf "{conf}[";
      pp_print_symbol buf v1;
      pp_print_string buf "..";
      pp_print_symbol buf v2;
      pp_print_string buf " : ";
      pp_print_type buf prec_array v_ty;
      pp_print_string buf "]"
 | TyPointer (_, status, ty) ->
      pp_print_status buf status;
      pp_print_string buf "*";
      pp_print_type buf prec_array ty
 | TyRef (_, status, ty) ->
      pp_print_status buf status;
      pp_print_string buf "&";
      pp_print_type buf prec_array ty
 | TyStruct (_, status, fields) ->
      pp_open_hvbox buf tabstop;
      if pre >= prec_struct then
         pp_print_string buf "(";
      pp_print_struct buf "struct" status fields;
      if pre >= prec_struct then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyUnion (_, status, fields) ->
      pp_open_hvbox buf tabstop;
      if pre >= prec_struct then
         pp_print_string buf "(";
      pp_print_struct buf "union" status fields;
      if pre >= prec_struct then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyEnum (_, status, fields) ->
      pp_open_hvbox buf tabstop;
      if pre >= prec_struct then
         pp_print_string buf "(";
      pp_print_enum buf status fields;
      if pre >= prec_struct then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyUEnum (_, status, fields) ->
      pp_open_hvbox buf tabstop;
      if pre >= prec_struct then
         pp_print_string buf "(";
      pp_print_uenum buf status fields;
      if pre >= prec_struct then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyTuple (_, status, fields) ->
      pp_open_hvbox buf tabstop;
      if pre >= prec_struct then
         pp_print_string buf "(";
      pp_print_tuple buf status fields;
      if pre >= prec_struct then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyFun (_, status, ty_vars, ty_res) ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      pp_print_status buf status;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(";
      pp_open_hvbox buf 0;
      pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_vars;
      pp_print_string buf ") ->";
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_type buf prec_fun ty_res;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyVar (_, status, v) ->
      pp_print_status buf status;
      pp_print_string buf "'";
      pp_print_symbol buf v
 | TyLambda (_, status, ty_vars, ty) ->
      pp_open_hvbox buf tabstop;
      pp_print_status buf status;
      pp_print_string buf "(lambda (";
      pp_print_sep_list buf "," (fun buf v ->
            pp_print_string buf "'";
            pp_print_symbol buf v) ty_vars;
      pp_print_string buf ").";
      pp_print_space buf ();
      pp_print_type buf prec_none ty;
      pp_print_string buf ")";
      pp_close_box buf ()
 | TyAll (_, status, ty_vars, ty) ->
      pp_open_hvbox buf tabstop;
      pp_print_status buf status;
      pp_print_string buf "(all (";
      pp_print_sep_list buf "," (fun buf v ->
            pp_print_string buf "'";
            pp_print_symbol buf v) ty_vars;
      pp_print_string buf ").";
      pp_print_space buf ();
      pp_print_type buf prec_none ty;
      pp_print_string buf ")";
      pp_close_box buf ()
 | TyApply (_, status, v, ty_list) ->
      pp_open_hvbox buf tabstop;
      pp_print_status buf status;
      pp_print_symbol buf v;
      if ty_list <> [] then
         begin
            pp_print_string buf "[";
            pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_list;
            pp_print_string buf "]"
         end;
      pp_close_box buf ()
 | TyElide (_, status, ty_list) ->
      pp_open_hvbox buf tabstop;
      pp_print_status buf status;
      pp_print_string buf "...";
      if ty_list <> [] then
         begin
            pp_print_string buf "{";
            pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_list;
            pp_print_string buf "}"
         end;
      pp_close_box buf ()
 | TyDelayed _ ->
      pp_print_string buf "delayed"

and pp_print_struct buf name status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf name;
   pp_print_string buf " {";
   (match fields with
       Fields fields ->
          List.iter (fun (_, v, ty, i_opt) ->
                pp_print_space buf ();
                pp_print_type buf prec_none ty;
                pp_print_string buf " ";
                pp_print_symbol buf v;
                (match i_opt with
                    Some i ->
                       pp_print_string buf " : ";
                       pp_print_int buf i
                  | None ->
                       ());
                pp_print_string buf ";") fields
     | Label (v, label) ->
          pp_print_string buf "<";
          pp_print_symbol buf v;
          pp_print_string buf ",";
          pp_print_symbol buf label;
          pp_print_string buf ">");
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_enum buf status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "enum {";
   (match fields with
       Fields fields ->
          List.iter (fun (_, v, i) ->
                pp_print_space buf ();
                pp_print_symbol buf v;
                pp_print_string buf " = ";
                pp_print_string buf (Int32.to_string i);
                pp_print_string buf ";") fields
     | Label (v, label) ->
          pp_print_string buf "<";
          pp_print_symbol buf v;
          pp_print_string buf ",";
          pp_print_symbol buf label;
          pp_print_string buf ">");
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_uenum buf status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "union enum";
   (match fields with
       Fields (label, fields) ->
          pp_print_string buf "[";
          pp_print_symbol buf label;
          pp_print_string buf "] {";
          List.iter (fun (_, v, ty_opt, label) ->
                pp_print_space buf ();
                (match ty_opt with
                    Some ty ->
                       pp_print_type buf prec_none ty;
                       pp_print_string buf " "
                  | None ->
                       ());
                pp_print_symbol buf v;
                pp_print_string buf " = ";
                pp_print_symbol buf label;
                pp_print_string buf ";") fields
     | Label (v, label) ->
          pp_print_string buf "<";
          pp_print_symbol buf v;
          pp_print_string buf ",";
          pp_print_symbol buf label;
          pp_print_string buf ">");
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_tuple buf status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "tuple {";
   List.iter (fun ty ->
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf ";") fields;
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

(*
 * Atoms.
 *)
and pp_print_atom buf = function
   AtomUnit (_, 1, 0) ->
      pp_print_string buf "unit"
 | AtomUnit (_, 2, 0) ->
      pp_print_string buf "false"
 | AtomUnit (_, 2, 1) ->
      pp_print_string buf "true"
 | AtomUnit (_, i1, i2) ->
      pp_print_string buf "unit(";
      pp_print_int buf i1;
      pp_print_string buf ", ";
      pp_print_int buf i2;
      pp_print_string buf ")"
 | AtomInt (_, i) ->
      pp_print_string buf (Rawint.to_string i)
 | AtomFloat (_, x) ->
      pp_print_string buf (Rawfloat.to_string x)
 | AtomNil (_, ty) ->
      pp_print_string buf "nil[";
      pp_print_type buf prec_none ty;
      pp_print_string buf "]"
 | AtomVar (_, v) ->
      pp_print_symbol buf v
 | AtomEnum (_, ty, i) ->
      pp_print_string buf "enum{";
      pp_print_string buf (Int32.to_string i);
      pp_print_string buf " : ";
      pp_print_type buf prec_none ty;
      pp_print_string buf "}"

let pp_print_type buf = pp_print_type buf prec_none

(*
 * Initializer.
 *)
let rec pp_print_init buf = function
   InitNone ->
      pp_print_string buf "uninitialized"
 | InitAtom (_, a) ->
      pp_print_string buf "copy(";
      pp_print_atom buf a;
      pp_print_string buf ")"
 | InitArray (_, fields) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "init_array {";
      pp_print_sep_list buf "," (fun buf (v_opt, init) ->
            (match v_opt with
                Some v ->
                   pp_print_symbol buf v;
                   pp_print_string buf ": "
              | None ->
                   ());
            pp_print_init buf init) fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()

(*
 * Patterns.
 *)
let rec pp_print_pattern buf = function
   IntPattern (_, i) ->
      pp_print_string buf (Rawint.to_string i)
 | FloatPattern (_, x) ->
      pp_print_string buf (Rawfloat.to_string x)
 | StringPattern (_, _, s) ->
      Fc_parse_print.pp_print_parse_string buf s
 | VarPattern (_, v, ty_opt) ->
      pp_print_symbol buf v;
      (match ty_opt with
          Some ty ->
             pp_print_string buf " : ";
             pp_print_type buf ty
        | None ->
             ())
 | StructPattern (_, fields) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "{ ";
      pp_print_sep_list buf "," (fun buf (v_opt, p) ->
            (match v_opt with
                Some v ->
                   pp_print_symbol buf v;
                   pp_print_string buf ": "
              | None ->
                   ());
            pp_print_pattern buf p) fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | EnumPattern (_, ty, i) ->
      pp_print_string buf "enum{ ";
      pp_print_string buf (Int32.to_string i);
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf "}"
 | UEnumPattern (_, ty, l, p_opt) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "union-enum{ ";
      pp_print_symbol buf l;
      (match p_opt with
          Some p ->
             pp_print_space buf ();
             pp_print_pattern buf p
        | None ->
             ());
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf "}";
      pp_close_box buf ();
 | VEnumPattern (_, v, p) ->
      pp_print_string buf "var.";
      pp_print_symbol buf v;
      pp_print_space buf ();
      pp_print_pattern buf p
 | AsPattern (_, p1, p2) ->
      pp_print_pattern buf p1;
      pp_print_space buf ();
      pp_print_string buf "as ";
      pp_print_pattern buf p2

(*
 * Global flag.
 *)
let pp_print_gflag buf gflag =
   match gflag with
      VarGlobalClass ->
         pp_print_string buf "$global "
    | VarStaticClass ->
         pp_print_string buf "$static "
    | VarLocalClass ->
         pp_print_string buf "$local "

(*
 * Operators.
 *)
let pp_print_unop buf op =
   let s =
      match op with
         UMinusIntOp _ 	 -> "-"
       | NotIntOp _ 	 -> "!"
       | UMinusFloatOp _ -> "-."
       | AbsFloatOp _ 	 -> "abs"
       | EqPointerNilOp  -> "==nil"
       | NeqPointerNilOp -> "!=nil"
       | SinOp _ 	 -> "sin"
       | CosOp _ 	 -> "cos"
       | SqrtOp _	 -> "sqrt"
   in
      pp_print_string buf s

let pp_print_binop buf op =
   let s =
      match op with
         PlusIntOp   _ -> "+"
       | MinusIntOp  _ -> "-"
       | MulIntOp    _ -> "*"
       | DivIntOp    _ -> "/"
       | RemIntOp    _ -> "%"
       | SlIntOp     _ -> "<<"
       | SrIntOp     _ -> ">>"
       | AndIntOp    _ -> "&"
       | OrIntOp     _ -> "|"
       | XorIntOp    _ -> "^"

       | MinIntOp    _ -> "min"
       | MaxIntOp    _ -> "max"

       | AndBoolOp     -> "&&"
       | OrBoolOp      -> "||"
       | EqBoolOp      -> "==b"
       | NeqBoolOp     -> "!=b"

       | EqIntOp     _ -> "=="
       | NeqIntOp    _ -> "!="
       | LtIntOp     _ -> "<"
       | LeIntOp     _ -> "<="
       | GtIntOp     _ -> ">"
       | GeIntOp     _ -> ">="

       | PlusFloatOp   _ -> "+."
       | MinusFloatOp  _ -> "-."
       | MulFloatOp    _ -> "*."
       | DivFloatOp    _ -> "/."
       | RemFloatOp    _ -> "%."

       | MinFloatOp    _ -> "min."
       | MaxFloatOp    _ -> "max."

       | EqFloatOp     _ -> "==."
       | NeqFloatOp    _ -> "!=."
       | LtFloatOp     _ -> "<."
       | LeFloatOp     _ -> "<=."
       | GtFloatOp     _ -> ">."
       | GeFloatOp     _ -> ">=."

       | Atan2Op       _ -> "atan2"

         (* Pointer operations *)
       | PlusPointerOp -> "+*"
       | MinusPointerOp -> "-*"

       | EqPointerOp -> "==*"
       | NeqPointerOp -> "!=*"
       | LtPointerOp -> "<*"
       | LePointerOp -> "<=*"
       | GtPointerOp -> ">*"
       | GePointerOp -> ">=*"
   in
      pp_print_string buf s

(*
 * Debug info.
 *)
let pp_print_debug_line buf (file, line) =
   pp_print_string buf "# ";
   pp_print_int buf line;
   pp_print_string buf (" \"" ^ file ^ "\"")

let pp_print_debug_vars buf info =
   pp_print_string buf "# Debug vars: ";
   pp_print_sep_list buf "," (fun buf (v1, ty, v2) ->
         pp_print_symbol buf v1;
         pp_print_string buf "=";
         pp_print_symbol buf v2;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf ty) info

let pp_print_debug buf info =
   match info with
      DebugString s ->
         pp_print_string buf "# ";
         pp_print_string buf s
    | DebugContext (line, vars) ->
         pp_print_location buf line;
         pp_print_space buf ();
         pp_print_debug_vars buf vars

(*
 * "let" opener.
 *)
let pp_print_let_type buf ty =
   match ty with
      TyDelayed _ ->
         ()
    | ty ->
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf ty

let pp_print_let_open buf v ty =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_symbol buf v;
   pp_print_let_type buf ty;
   pp_print_string buf " =";
   pp_print_space buf ()

let pp_print_let_vars_open buf vars v ty =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_sep_list buf "," (fun buf v ->
         pp_print_string buf "'";
         pp_print_symbol buf v) vars;
   if vars <> [] then
      pp_print_string buf ", ";
   pp_print_symbol buf v;
   pp_print_let_type buf ty;
   pp_print_string buf " =";
   pp_print_space buf ()

let rec pp_print_let_close buf e =
   pp_print_string buf " in";
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_expr buf e

(*
 * Print an expression.
 *)
and pp_print_expr buf = function
   LetTypes (_, types, e) ->
      pp_open_hvbox buf 0;
      pp_print_string buf "let type ";
      pp_print_pre_sep_list buf " and " (fun buf (v, label, ty) ->
            pp_print_symbol buf v;
            pp_print_string buf "{";
            pp_print_symbol buf label;
            pp_print_string buf "} =";
            pp_print_space buf ();
            pp_print_type buf ty) types;
      pp_print_string buf " in";
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_expr buf e

 | LetFuns (_, funs, e) ->
      pp_open_hvbox buf 0;
      pp_print_string buf "let ";
      pp_print_sep_list buf " and " pp_print_function funs;
      pp_print_string buf " in";
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_expr buf e

 | LetCoerce (_, v, ty, a, e) ->
      pp_print_let_open buf v ty;
      pp_print_atom buf a;
      pp_print_let_close buf e

 | LetTest (_, v, ty, a, e) ->
      pp_print_let_open buf v ty;
      pp_print_string buf "test(";
      pp_print_atom buf a;
      pp_print_string buf ")";
      pp_print_let_close buf e

 | LetFunDecl (_, sc, v, label, gflag, ty, e) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "let ";
      pp_print_storage_class buf sc;
      pp_print_gflag buf gflag;
      pp_print_symbol buf v;
      pp_print_string buf "[";
      pp_print_symbol buf label;
      pp_print_string buf "] :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf " = extern";
      pp_print_let_close buf e

 | LetVar (_, sc, v, label, gflag, ty, init, e) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "let ";
      pp_print_storage_class buf sc;
      pp_print_gflag buf gflag;
      pp_print_symbol buf v;
      pp_print_string buf "{";
      pp_print_symbol buf label;
      pp_print_string buf "} :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_init buf init;
      pp_print_let_close buf e

 | LetAtom (_, v, ty, a, e) ->
      pp_print_let_open buf v ty;
      pp_print_atom buf a;
      pp_print_let_close buf e

 | LetUnop (_, v, ty, op, a, e) ->
      pp_print_let_open buf v ty;
      pp_print_unop buf op;
      pp_print_string buf "(";
      pp_print_atom buf a;
      pp_print_string buf ")";
      pp_print_let_close buf e

 | LetBinop (_, v, ty, op, a1, a2, e) ->
      pp_print_let_open buf v ty;
      pp_print_atom buf a1;
      pp_print_string buf " ";
      pp_print_binop buf op;
      pp_print_string buf " ";
      pp_print_atom buf a2;
      pp_print_let_close buf e

 | LetExtCall (_, v, ty, s, b, ty', args, e) ->
      pp_print_let_open buf v ty;
      if b then
         fprintf buf "\"gc:%s\"" (String.escaped s)
      else
         fprintf buf "\"%s\"" (String.escaped s);
      pp_print_space buf ();
      pp_print_type buf ty';
      pp_print_string buf ")(";
      pp_print_sep_list buf "," pp_print_atom args;
      pp_print_string buf ")";
      pp_print_let_close buf e

 | LetApply (_, v, ty, f, label, args, e) ->
      pp_print_let_open buf v ty;
      pp_print_symbol buf f;
      pp_print_string buf "{";
      pp_print_symbol buf label;
      pp_print_string buf "}(";
      pp_print_sep_list buf "," pp_print_atom args;
      pp_print_string buf ")";
      pp_print_let_close buf e

 | TailCall (_, f, args) ->
      pp_print_symbol buf f;
      pp_print_string buf "(";
      pp_print_sep_list buf "," pp_print_atom args;
      pp_print_string buf ")"

 | Return (_, f, ty, a) ->
      pp_print_string buf "return ";
      pp_print_symbol buf f;
      pp_print_string buf "::(";
      pp_print_atom buf a;
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | LetString (_, v, ty, _, s, e) ->
      pp_print_let_open buf v ty;
      Fc_parse_print.pp_print_parse_string buf s;
      pp_print_let_close buf e

 | IfThenElse (_, a, e1, e2) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "if ";
      pp_print_atom buf a;
      pp_print_string buf " then";
      pp_print_space buf ();
      pp_print_expr buf e1;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "else";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_close_box buf ()

 | LetMalloc (pos, v, a, e) ->
      pp_print_let_open buf v (TyDelayed (pos, StatusNormal));
      pp_print_string buf "malloc(";
      pp_open_hvbox buf 0;
      pp_print_atom buf a;
      pp_close_box buf ();
      pp_print_string buf ")";
      pp_print_let_close buf e

 | Debug (_, info, e) ->
      pp_print_debug buf info;
      pp_print_space buf ();
      pp_print_expr buf e

 | LetSizeof (_, v, ty1, ty2, e) ->
      pp_print_let_open buf v ty1;
      pp_print_string buf "sizeof(";
      pp_print_type buf ty2;
      pp_print_string buf ")";
      pp_print_let_close buf e

 | Raise (_, a) ->
      pp_print_string buf "raise ";
      pp_print_atom buf a

 | Try (_, e1, v, e2) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "try ";
      pp_print_expr buf e1;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "with ";
      pp_print_symbol buf v;
      pp_print_string buf " ->";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_close_box buf ();
      pp_close_box buf ()

 | Match (_, a, ty, cases) ->
      pp_open_hvbox buf 1;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "match ";
      pp_print_atom buf a;
      pp_print_space buf ();
      pp_print_string buf ": ";
      pp_print_type buf ty;
      pp_print_string buf " with";
      pp_close_box buf ();
      List.iter (fun (p, e) ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "| ";
            pp_print_pattern buf p;
            pp_print_string buf " ->";
            pp_print_space buf ();
            pp_print_expr buf e;
            pp_close_box buf ()) cases;
      pp_close_box buf ()

 | LetAlloc (_, v, ty, label, args, e) ->
      pp_print_let_open buf v ty;
      pp_print_string buf "$alloc ";
      pp_print_symbol buf label;
      pp_print_string buf "(";
      pp_print_sep_list buf "," pp_print_atom args;
      pp_print_string buf ")";
      pp_print_let_close buf e

 | LetProject (_, v1, ty, a2, v3, e) ->
      pp_print_let_open buf v1 ty;
      pp_print_atom buf a2;
      pp_print_string buf ".";
      pp_print_symbol buf v3;
      pp_print_let_close buf e

 | LetSubscript (_, v, ty, v1, ty2, a2, e) ->
      pp_print_let_open buf v ty;
      pp_print_string buf "(";
      pp_print_symbol buf v1;
      pp_print_string buf " : ";
      pp_print_type buf ty2;
      pp_print_string buf ")[";
      pp_print_atom buf a2;
      pp_print_string buf "]";
      pp_print_let_close buf e

 | Set (_, a1, ty, a2, e) ->
      pp_print_atom buf a1;
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf " <- ";
      pp_print_atom buf a2;
      pp_print_string buf ";";
      pp_print_space buf ();
      pp_print_expr buf e

 | LetAddrOf (_, v, ty, a, e) ->
      pp_print_let_open buf v ty;
      pp_print_string buf "&";
      pp_print_atom buf a;
      pp_print_let_close buf e

and pp_print_function buf (_, f, label, debug, gflag, f_ty, vars, body) =
   pp_print_string buf "(";
   pp_print_gflag buf gflag;
   pp_print_symbol buf f;
   pp_print_string buf "{";
   pp_print_symbol buf label;
   pp_print_string buf "} : ";
   pp_print_type buf f_ty;
   pp_print_string buf ")(";
   pp_print_sep_list buf "," pp_print_symbol vars;
   pp_print_string buf ") =";
   pp_print_space buf ();
   pp_print_location buf debug;
   pp_print_space buf ();
   pp_print_expr buf body

(*
 * Wrap in a box.
 *)
let pp_print_expr buf exp =
   pp_open_vbox buf 0;
   pp_print_expr buf exp;
   pp_close_box buf ()

(*
 * Debug version.
 *)
let debug_expr debug exp =
   let buf = err_formatter in
      pp_open_vbox buf 0;
      pp_print_string buf "*** AST: ";
      pp_print_string buf debug;
      pp_print_space buf ();
      pp_print_expr buf exp;
      pp_close_box buf ();
      pp_print_newline buf ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
