(*
 * Define a type substitution.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_ast
open Fc_config

type subst = ty SymbolTable.t

(*
 * Type substitution.
 *)
let subst_empty = SymbolTable.empty

let subst_add = SymbolTable.add

let subst_remove = SymbolTable.remove

let subst_lookup = SymbolTable.find

let rec subst_type subst ty =
   match ty with
      TyUnit _
    | TyZero _
    | TyInt _
    | TyField _
    | TyFloat _
    | TyDelayed _
    | TyEnum _ ->
         ty
    | TyFun (pos, status, ty_vars, ty_res) ->
         let ty_vars = List.map (subst_type subst) ty_vars in
         let ty_res = subst_type subst ty_res in
            TyFun (pos, status, ty_vars, ty_res)
    | TyArray (pos, status, ty, a1, a2) ->
         let ty = subst_type subst ty in
            TyArray (pos, status, ty, a1, a2)
    | TyConfArray (pos, status, ty, v1, v2, v_ty) ->
         let ty = subst_type subst ty in
         let v_ty = subst_type subst v_ty in
            TyConfArray (pos, status, ty, v1, v2, v_ty)
    | TyPointer (pos, status, ty) ->
         let ty = subst_type subst ty in
            TyPointer (pos, status, ty)
    | TyRef (pos, status, ty) ->
         let ty = subst_type subst ty in
            TyRef (pos, status, ty)
    | TyUEnum (pos, status, fields) ->
         let fields = subst_uenum_fields subst fields in
            TyUEnum (pos, status, fields)
    | TyUnion (pos, status, fields) ->
         TyUnion (pos, status, subst_struct_fields subst fields)
    | TyStruct (pos, status, fields) ->
         TyStruct (pos, status, subst_struct_fields subst fields)
    | TyTuple (pos, status, tl) ->
         TyTuple (pos, status, List.map (subst_type subst) tl)
    | TyVar (_, _, v) ->
         (try subst_type subst (subst_lookup subst v) with
             Not_found ->
                ty)
    | TyApply (pos, status, v, tyl) ->
         TyApply (pos, status, v, List.map (subst_type subst) tyl)
    | TyLambda (pos, status, vars, ty) ->
         let subst = List.fold_left subst_remove subst vars in
            TyLambda (pos, status, vars, subst_type subst ty)
    | TyAll (pos, status, vars, ty) ->
         let subst = List.fold_left subst_remove subst vars in
            TyAll (pos, status, vars, subst_type subst ty)
    | TyElide (pos, status, tyl) ->
         TyElide (pos, status, List.map (subst_type subst) tyl)

and subst_ty_opt subst = function
   Some ty -> Some (subst_type subst ty)
 | None -> None

and subst_uenum_fields subst fields =
   match fields with
      Fields (label, fields) ->
         let fields =
            List.map (fun (pos, v, ty_opt, label) ->
                  let ty_opt = subst_ty_opt subst ty_opt in
                     pos, v, ty_opt, label) fields
         in
            Fields (label, fields)
    | Label _ ->
         fields

and subst_struct_fields subst fields =
   match fields with
      Fields fields ->
         let fields =
            List.map (fun (pos, v, ty, i_opt) ->
                  let ty = subst_type subst ty in
                     pos, v, ty, i_opt) fields
         in
            Fields fields
    | Label _ ->
         fields

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
