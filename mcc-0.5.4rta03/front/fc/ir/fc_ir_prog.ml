(*
 * Convert an expression to a program.
 * We assume that all functions are closed.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_ir
open Fc_ir_ds
open Fc_ir_standardize

(*
 * Flatten the expression.
 *)
let rec prog_exp prog e =
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            prog_funs_exp prog funs e
       | LetAtom (v, ty, a, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetAtom (v, ty, a, e))
       | LetUnop (v, ty, op, a, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetUnop (v, ty, op, a, e))
       | LetBinop (v, ty, op, a1, a2, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetBinop (v, ty, op, a1, a2, e))
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetExtCall (v, ty, s, b, ty', args, e))
       | LetClosure (v, f, args, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetClosure (v, f, args, e))
       | TailCall _ ->
            prog, e
       | LetMalloc (v, a, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetMalloc (v, a, e))
       | IfThenElse (a, e1, e2) ->
            let prog, e1 = prog_exp prog e1 in
            let prog, e2 = prog_exp prog e2 in
               prog, make_exp loc (IfThenElse (a, e1, e2))
       | LetSubscript (v, ty, v1, a, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetSubscript (v, ty, v1, a, e))
       | SetSubscript (v1, a2, ty, a3, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (SetSubscript (v1, a2, ty, a3, e))
       | SetGlobal (v, ty, a, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (SetGlobal (v, ty, a, e))
       | Memcpy (v1, a1, v2, a2, a3, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (Memcpy (v1, a1, v2, a2, a3, e))
       | LetProject (v1, ty, v2, v3, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetProject (v1, ty, v2, v3, e))
       | LetAddrOfProject (v1, ty, v2, v3, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetAddrOfProject (v1, ty, v2, v3, e))
       | SetProject (v1, v2, ty, a3, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (SetProject (v1, v2, ty, a3, e))
       | LetVar (v, ty, a_opt, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetVar (v, ty, a_opt, e))
       | LetFrame (v, ty, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetFrame (v, ty, e))
       | LetAddrOfVar (v1, ty, v2, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (LetAddrOfVar (v1, ty, v2, e))
       | SetVar (v, ty, a, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (SetVar (v, ty, a, e))
       | Debug (info, e) ->
            let prog, e = prog_exp prog e in
               prog, make_exp loc (Debug (info, e))

(*
 * Add all the functions to the toplevel.
 *)
and prog_funs_exp prog funs e =
   let prog =
      List.fold_left (fun prog (f, line, gflag, ty, vars, e) ->
            let prog, e = prog_exp prog e in
               { prog with prog_funs = SymbolTable.add prog.prog_funs f (line, gflag, ty, vars, e) }) prog funs
   in
      prog_exp prog e

(*
 * Toplevel prog.
 *)
let prog_prog prog =
   let prog, body = prog_exp prog prog.prog_body in
   let prog = { prog with prog_body = body} in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
