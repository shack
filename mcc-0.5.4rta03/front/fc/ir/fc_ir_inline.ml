(*
 * Basic inline expansion.
 * We inline functions only if they
 * are called exactly once.  We do some simple
 * intra-procedural constant folding.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Trace

open Fc_ir
open Fc_ir_ds
open Fc_ir_env
open Fc_ir_exn
open Fc_ir_pos
open Fc_ir_type
open Fc_ir_print
open Fc_ir_standardize

module Pos = MakePos (struct let name = "Fc_ir_inline" end)
open Pos

(************************************************************************
 * AVAILABLE EXPRESSIONS
 ************************************************************************)

type ('a, 'b) option2 =
   Some2 of 'a
 | Some1 of 'b
 | None0

(*
 * Simple expressions that a variable van refer to.
 *)
type avail =
   AvailVar of var * ty
 | AvailAtom of atom
 | AvailAddressOf of var * var
 | AvailSubscript of var * var * atom
 | AvailClosure of var * atom list
 | AvailFunction of var * var list * exp
 | AvailFrame of var * (frame_label * avail) list

type aenv = avail SymbolTable.t

let aenv_empty = SymbolTable.empty

let rec aenv_lookup aenv v =
   try
      match SymbolTable.find aenv v with
         AvailAtom (AtomVar v)
       | AvailVar (v, _) ->
            (* eprintf "aenv_lookup: %s%t" (string_of_symbol v) eflush; *)
            aenv_lookup aenv v
       | a ->
            a
   with
      Not_found ->
         AvailAtom (AtomVar v)

let rec aenv_lookup_atom aenv = function
   AtomVar v ->
      aenv_lookup aenv v
 | a ->
      AvailAtom a

let aenv_add aenv v a =
   SymbolTable.add aenv v a

let atom_of_avail = function
   AvailVar (v, _)
 | AvailAddressOf (v, _)
 | AvailSubscript (v, _, _)
 | AvailFunction (v, _, _)
 | AvailClosure (v, _)
 | AvailFrame (v, _) ->
      AtomVar v
 | AvailAtom a ->
      a

let rec type_of_var aenv pos v =
   try
      match SymbolTable.find aenv v with
         AvailAtom (AtomVar v) ->
            type_of_var aenv pos v
       | AvailVar (_, ty) ->
            ty
       | _ ->
            raise Not_found
   with
      Not_found ->
         raise (IRException (pos, StringVarError ("unknown type", v)))

let aenv_field_atom fields label =
   match List.assoc label fields with
      AvailAtom a -> a
    | _ -> raise Not_found

let aenv_set_field aenv v label a =
   let a = AvailAtom a in
      match aenv_lookup aenv v with
         AvailFrame (v', fields) ->
            aenv_add aenv v (AvailFrame (v', (label, a) :: fields))
       | _ ->
            aenv_add aenv v (AvailFrame (v, [label, a]))

let aenv_bind_vars aenv pos vars args =
   let pos = string_pos "aenv_bind_vars" pos in
   let rec collect aenv vars' args' =
      match vars', args' with
         v :: vars', a :: args' ->
            collect (aenv_add aenv v (AvailAtom a)) vars' args'
       | [], [] ->
            aenv
       | _ ->
            raise (IRException (pos, ArityMismatch (List.length vars, List.length args)))
   in
      collect aenv vars args

(************************************************************************
 * FUNCTION SIZING
 ************************************************************************)

(*
 * Measure the "size" of a function.
 *)
let is_small_max = 25

let rec size_exp fenv i e =
   if i >= is_small_max then
      i
   else
      match dest_exp_core e with
         LetFuns (funs, e) ->
            let i =
               List.fold_left (fun i (f, _, _, _, _, e) ->
                     let _, degree, _ = SymbolTable.find fenv f in
                        if degree > 1 then
                           is_small_max
                        else
                           size_exp fenv i e) i funs
            in
               size_exp fenv i e
       | LetClosure (_, _, _, e)
       | LetAtom (_, _, _, e)
       | LetUnop (_, _, _, _, e)
       | LetBinop (_, _, _, _, _, e)
       | LetVar (_, _, _, e)
       | LetFrame (_, _, e) ->
            size_exp fenv (succ i) e
       | LetMalloc (_, _, e) ->
            size_exp fenv (i + 5) e
       | TailCall _ ->
            succ i
       | IfThenElse (_, e1, e2) ->
            size_exp fenv (size_exp fenv (i + 2) e1) e2
       | LetExtCall (_, _, _, _, _, _, e) ->
            size_exp fenv (i + 5) e
       | LetSubscript (_, _, _, _, e)
       | SetSubscript (_, _, _, _, e)
       | SetGlobal (_, _, _, e)
       | LetProject (_, _, _, _, e)
       | SetProject (_, _, _, _, e) ->
            size_exp fenv (i + 2) e
       | Memcpy (_, _, _, _, _, e) ->
            size_exp fenv (i + 2) e
       | LetAddrOfProject (_, _, _, _, e)
       | LetAddrOfVar (_, _, _, e)
       | SetVar (_, _, _, e) ->
            size_exp fenv (i + 1) e
       | Debug (_, e) ->
            size_exp fenv i e

let is_small_exp fenv e =
   size_exp fenv 0 e < is_small_max

(************************************************************************
 * LOOP-NEST TREE
 ************************************************************************)

(*
 * This is the node we pass to the loop-nest algorithm.
 *)
type node =
   { node_name : var;
     node_succ : var list
   }

(*
 * Count number of uses of each var.
 *)
let count_empty = SymbolTable.empty

let count_lookup count v =
   try SymbolTable.find count v with
      Not_found ->
         0

let count_var count v =
   SymbolTable.add count v (succ (count_lookup count v))

let count_atom count a =
   match a with
      AtomVar v -> count_var count v
    | _ -> count

let count_atom_opt count = function
   Some a -> count_atom count a
 | None -> count

let count_atoms count args =
   List.fold_left count_atom count args

(*
 * Live variable set.
 *)
let live_empty = SymbolSet.empty

(*
 * Add a function occurrence.
 *)
let live_var live f =
   SymbolSet.add live f

let live_atom live = function
   AtomVar v -> live_var live v
 | _ -> live

let live_atom_opt live = function
   Some a -> live_atom live a
 | None -> live

let live_atoms live args =
   List.fold_left live_atom live args

(*
 * Function table keeps live set for each function
 *)
let fenv_empty = SymbolTable.empty

let fenv_add fenv f vars =
   SymbolTable.add fenv f vars

let fenv_mem fenv f =
   SymbolTable.mem fenv f

(*
 * Check if a node1 is an ancestor for node2.
 *)
let rec is_ancestor fenv name1 name2 =
   if Symbol.eq name1 name2 then
      true
   else
      let name3, _, _ = SymbolTable.find fenv name2 in
         if Symbol.eq name3 name2 then
            false
         else
            is_ancestor fenv name1 name3

(*
 * Print ancestor list.
 *)
let rec pp_print_ancestors buf fenv (name, degree, loop) =
   pp_print_space buf ();
   pp_print_string buf ": ";
   pp_print_int buf degree;
   pp_print_string buf " ";
   pp_print_symbol buf name;
   if loop then
      pp_print_string buf " <loop-header>";
   let name', degree, loop = SymbolTable.find fenv name in
      if not (Symbol.eq name' name) then
         pp_print_ancestors buf fenv (name', degree, loop)

(*
 * Collect the number of uses of each variable.
 *)
let rec call_exp fenv count live e =
   match dest_exp_core e with
      LetFuns (funs, e) ->
         let fenv, count =
            List.fold_left (fun (fenv, count) (f, _, _, _, _, e) ->
                  let fenv, count, live = call_exp fenv count live_empty e in
                  let fenv = fenv_add fenv f live in
                     fenv, count) (fenv, count) funs
         in
            call_exp fenv count live e
    | LetClosure (_, f, args, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) args in
            fenv, count, live
    | TailCall (TailNormal (f, args)) ->
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) args in
            fenv, count, live
    | TailCall (TailSysMigrate (f, host, args)) ->
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) (host @ args) in
            fenv, count, live
    | TailCall (TailAtomic (f, c, args)) ->
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) (c :: args) in
            fenv, count, live
    | TailCall (TailAtomicCommit (level, f, args)) ->
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) (level :: args) in
            fenv, count, live
    | TailCall (TailAtomicRollback (level, c)) ->
         let live = live_atoms live [level; c] in
            fenv, count, live
    | IfThenElse (_, e1, e2) ->
         let fenv, count, live = call_exp fenv count live e1 in
            call_exp fenv count live e2
    | LetAtom (_, _, a, e)
    | LetUnop (_, _, _, a, e)
    | LetMalloc (_, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom count a in
         let live = live_atom live a in
            fenv, count, live
    | LetBinop (_, _, _, a1, a2, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom count a1) a2 in
         let live = live_atom (live_atom live a1) a2 in
            fenv, count, live
    | LetVar (_, _, a_opt, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom_opt count a_opt in
         let live = live_atom_opt live a_opt in
            fenv, count, live
    | LetFrame (_, _, e) ->
         call_exp fenv count live e
    | LetExtCall (_, _, _, _, _, args, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atoms count args in
         let live = live_atoms live args in
            fenv, count, live
    | LetSubscript (_, _, v1, a2, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_var count v1) a2 in
         let live = live_atom (live_var live v1) a2 in
            fenv, count, live
    | SetSubscript (v1, a2, _, a3, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom (count_var count v1) a2) a3 in
         let live = live_atom (live_atom (live_var live v1) a2) a3 in
            fenv, count, live
    | SetGlobal (v, _, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_var count v) a in
         let live = live_atom (live_var live v) a in
            fenv, count, live
    | Memcpy (v1, a1, v2, a2, a3, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom (count_var (count_atom (count_var count v1) a1) v2) a2) a3 in
         let live = live_atom (live_atom (live_var (live_atom (live_var live v1) a1) v2) a2) a3 in
            fenv, count, live
    | LetProject (_, _, v, _, e)
    | LetAddrOfProject (_, _, v, _, e)
    | LetAddrOfVar (_, _, v, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_var count v in
         let live = live_var live v in
            fenv, count, live
    | SetProject (v, _, _, a, e)
    | SetVar (v, _, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_var count v) a in
         let live = live_atom (live_var live v) a in
            fenv, count, live
    | Debug (_, e) ->
         call_exp fenv count live e

(*
 * Filter the live set so that it contains only the
 * function names.
 *)
let filter_live fenv live =
   SymbolSet.fold (fun live v ->
         if fenv_mem fenv v then
            v :: live
         else
            live) [] live

(*
 * Construct the call-graph.
 *)
let root_sym = new_symbol_string "root"

let call_prog prog =
   let { prog_funs = funs;
         prog_body = body
       } = prog
   in

   (* Toplevel functions *)
   let fenv, count =
      SymbolTable.fold (fun (fenv, count) f (_, _, _, _, e) ->
            let fenv, count, live = call_exp fenv count live_empty e in
            let fenv = fenv_add fenv f live in
               fenv, count) (fenv_empty, count_empty) funs
   in

   (* Root is the initialization code *)
   let fenv, count, live = call_exp fenv count live_empty body in

   (* Add successor nodes to all global functions *)
   let live =
      SymbolTable.fold (fun live f (_, gflag, _, _, _) ->
         match gflag with
            VarGlobalClass ->
               live_var live f
          | VarLocalClass
          | VarContClass ->
               live) live funs
   in

   (* Create successor table by filtering out live vars that are functions *)
   let nodes =
      SymbolTable.fold (fun nodes f live ->
            let node = f, filter_live fenv live in
               node :: nodes) [] fenv
   in
   let root_node = root_sym, filter_live fenv live in

   (* Debugging *)
   let _ =
      if debug Fir_state.debug_print_ir then
         let buf = err_formatter in
            pp_open_vbox buf tabstop;
            pp_print_string buf "*** Call graph:";
            List.iter (fun (f, succ) ->
                  pp_print_space buf ();
                  pp_print_symbol buf f;
                  pp_print_string buf ":";
                  List.iter (fun v ->
                        pp_print_string buf " ";
                        pp_print_symbol buf v) succ) (root_node :: nodes);
            pp_close_box buf ();
            pp_print_newline buf ()
   in

   (* Build the dominator tree *)
   let loop = Loop.create "Fc_ir_inline" fst snd root_node nodes in
   let fenv = Loop.dominators loop fst in
   let nest = Loop.loop_nest loop fst in
   let head =
      List.fold_left (fun head (f, _) ->
            SymbolSet.add head f) SymbolSet.empty (Trace.special_nodes nest)
   in
   let fenv =
      SymbolTable.mapi (fun f v ->
            v, count_lookup count f, SymbolSet.mem head f) fenv
   in
   let _ =
      if debug Fir_state.debug_print_ir then
         let buf = err_formatter in
            pp_open_vbox buf tabstop;
            pp_print_string buf "*** Dominator indexes:";
            SymbolTable.iter (fun f name ->
                  pp_print_space buf ();
                  pp_print_symbol buf f;
                  pp_print_string buf ": ";
                  pp_print_ancestors buf fenv name) fenv;
            pp_close_box buf ();
            pp_print_newline buf ()
   in
      fenv

(*
 * Construct the initial available list for the functions.
 *)
let aenv_function fenv aenv f gflag vars e =
   let _, degree, loop = SymbolTable.find fenv f in
      if not loop && is_small_exp fenv e then
         aenv_add aenv f (AvailFunction (f, vars, e))
      else
         begin
            if debug Fir_state.debug_print_ir then
               eprintf "aenv_function: rejected function %s because it is too big: %d@." (**)
                  (string_of_symbol f) (size_exp fenv 0 e);
            aenv
         end

let avail_prog prog =
   let { prog_funs = funs;
         prog_body = body;
         prog_globals = globals;
       } = prog
   in
   let fenv = call_prog prog in
   let aenv =
      SymbolTable.fold (fun aenv f (_, gflag, _, vars, e) ->
            aenv_function fenv aenv f gflag vars e) aenv_empty funs
   in
   let aenv =
      SymbolTable.fold (fun aenv v (_, volp, init) ->
            match init with
               InitAtom a ->
                  if volp then
                     aenv
                  else
                     aenv_add aenv v (AvailAtom a)
             | InitString _
             | InitBlock _ ->
                  aenv) aenv globals
   in
      fenv, aenv

(************************************************************************
 * INLINE UTILITIES
 ************************************************************************)

(*
 * Compare rawints.
 *)
let rawint_bool tenv pos ty flag =
   match tenv_expand tenv pos ty with
      TyInt (pre, signed) ->
         if flag then 1 else 0
    | ty ->
         raise (IRException (pos, StringTypeError ("not an int type", ty)))

let rawint_compare tenv pos ty cmp i j =
   if cmp (Rawint.compare i j) then 1 else 0

let rawfloat_compare tenv pos ty cmp x y =
   if cmp (Rawfloat.compare x y) then 1 else 0

(************************************************************************
 * SECOND PASS
 ************************************************************************)

(*
 * Inline a var.
 *)
let inline_var aenv v =
   match aenv_lookup aenv v with
      AvailAtom (AtomVar v)
    | AvailVar (v, _)
    | AvailAddressOf (v, _)
    | AvailSubscript (v, _, _)
    | AvailFunction (v, _, _)
    | AvailClosure (v, _)
    | AvailFrame (v, _) ->
         v
    | AvailAtom _ ->
         v

(*
 * Inline an atom.
 *)
let inline_atom aenv a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomNil _
    | AtomLabel _ ->
         a
    | AtomVar v ->
         match aenv_lookup aenv v with
            AvailAtom a ->
               a
          | AvailVar (v, _)
          | AvailAddressOf (v, _)
          | AvailSubscript (v, _, _)
          | AvailFunction (v, _, _)
          | AvailClosure (v, _)
          | AvailFrame (v, _) ->
               AtomVar v

let inline_atom_opt aenv = function
   Some a -> Some (inline_atom aenv a)
 | None -> None

let inline_atoms aenv args =
   List.map (inline_atom aenv) args

(*
 * Perform inlining.
 *)
let rec inline_exp tenv fenv aenv name e =
   let pos = string_pos "inline_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            inline_funs_exp tenv fenv aenv name pos loc funs e
       | LetAtom (v, ty, a, e) ->
            inline_atom_exp tenv fenv aenv name pos loc v ty a e
       | LetUnop (v, ty, op, a, e) ->
            inline_unop_exp tenv fenv aenv name pos loc v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            inline_binop_exp tenv fenv aenv name pos loc v ty op a1 a2 e
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            inline_extcall_exp tenv fenv aenv name pos loc v ty s b ty' args e
       | LetClosure (v, f, args, e) ->
            inline_closure_exp tenv fenv aenv name pos loc v f args e
       | TailCall op ->
            inline_tailcall_exp tenv fenv aenv name pos loc op
       | LetMalloc (v, a, e) ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailVar (v', ty_malloc)) in
               make_exp loc (LetMalloc (v', inline_atom aenv a, inline_exp tenv fenv aenv name e))
       | IfThenElse (a, e1, e2) ->
            inline_if_exp tenv fenv aenv name pos loc a e1 e2
       | LetSubscript (v, ty, v2, a3, e) ->
            inline_subscript_exp tenv fenv aenv name pos loc v ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            inline_set_subscript_exp tenv fenv aenv name pos loc v1 a2 ty a3 e
       | SetGlobal (v, ty, a, e) ->
            inline_set_global_exp tenv fenv aenv name pos loc v ty a e
       | Memcpy (v1, a1, v2, a2, a3, e) ->
            make_exp loc (Memcpy (inline_var aenv v1,
                                  inline_atom aenv a1,
                                  inline_var aenv v2,
                                  inline_atom aenv a2,
                                  inline_atom aenv a3,
                                  inline_exp tenv fenv aenv name e))
       | LetProject (v1, ty, v2, label, e) ->
            inline_project_exp tenv fenv aenv name pos loc v1 ty v2 label e
       | SetProject (v1, label, ty, a4, e) ->
            inline_set_project_exp tenv fenv aenv name pos loc v1 label ty a4 e
       | LetAddrOfProject (v1, ty, v2, label, e) ->
            let v1' = new_symbol v1 in
            let aenv = aenv_add aenv v1 (AvailVar (v1', ty)) in
               make_exp loc (LetAddrOfProject (v1', ty, inline_var aenv v2, label, inline_exp tenv fenv aenv name e))
       | LetVar (v, ty, a_opt, e) ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailVar (v', ty)) in
               make_exp loc (LetVar (v', ty, inline_atom_opt aenv a_opt, inline_exp tenv fenv aenv name e))
       | LetFrame (v, ty, e) ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailVar (v', ty)) in
               make_exp loc (LetFrame (v', ty, inline_exp tenv fenv aenv name e))
       | LetAddrOfVar (v1, ty, v2, e) ->
            inline_addr_of_exp tenv fenv aenv name pos loc v1 ty v2 e
       | SetVar (v, ty, a, e) ->
            make_exp loc (SetVar (inline_var aenv v, ty, inline_atom aenv a, inline_exp tenv fenv aenv name e))
       | Debug (info, e) ->
            inline_debug_exp tenv fenv aenv name pos loc info e

(*
 * Add the function definitions.
 * Add the functions to the inline environment.
 *)
and inline_funs_exp tenv fenv aenv name pos loc funs e =
   let pos = string_pos "inline_funs_exp" pos in
   let funs =
      List.fold_left (fun funs fund ->
            let f, line, gflag, ty, vars, e = fund in
            let e = inline_exp tenv fenv aenv f e in
            let fund = f, line, gflag, ty, vars, e in
               fund :: funs) [] funs
   in
(*
   let aenv =
      List.fold_left (fun aenv (f, _, gflag, _, vars, e) ->
            aenv_function fenv aenv f gflag vars e) aenv funs
   in
*)
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (LetFuns (List.rev funs, e))

(*
 * Operators.
 * Try to perform constant folding.
 *)
and inline_atom_exp tenv fenv aenv name pos loc v ty a e =
   let pos = string_pos "inline_atom_exp" pos in
   let a = inline_atom aenv a in
   let aenv = aenv_add aenv v (AvailAtom a) in
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (LetAtom (v, ty, a, e))

and inline_unop_exp tenv fenv aenv name pos loc v ty op a e =
   let pos = string_pos "inline_unop_exp" pos in
   let a = inline_atom aenv a in
   let a' =
      match op, a with
         UMinusIntOp _, AtomInt i ->
            Some (AtomInt (Rawint.neg i))
       | NotIntOp _, AtomInt i ->
            Some (AtomInt (Rawint.lognot i))
       | BitFieldOp (_, _, off, len), AtomInt i ->
            Some (AtomInt (Rawint.field i off len))

       | UMinusFloatOp _, AtomFloat x ->
            Some (AtomFloat (Rawfloat.neg x))
       | AbsFloatOp _, AtomFloat x ->
            Some (AtomFloat (Rawfloat.abs x))

       | IntOfInt (pre1, signed1, _, _), AtomInt i ->
            Some (AtomInt (Rawint.of_rawint pre1 signed1 i))
       | IntOfFloat (pre, signed, _), AtomFloat x ->
            Some (AtomInt (Rawint.of_float pre signed (Rawfloat.to_float x)))
       | FloatOfInt (pre, _, _), AtomInt i ->
            Some (AtomFloat (Rawfloat.of_rawint pre i))
       | FloatOfFloat (pre, _), AtomFloat x ->
            Some (AtomFloat (Rawfloat.of_rawfloat pre x))

       | _ ->
            (* None of the others get inlined *)
            None
   in
   let v' = new_symbol v in
   let aenv =
      match a' with
         Some a ->
            aenv_add aenv v (AvailAtom a)
       | None ->
            aenv_add aenv v (AvailVar (v', ty))
   in
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (LetUnop (v', ty, op, a, e))

and inline_binop_exp tenv fenv aenv name pos loc v ty op a1 a2 e =
   let pos = string_pos "inline_binop_exp" pos in
   let avail1 = aenv_lookup_atom aenv a1 in
   let avail2 = aenv_lookup_atom aenv a2 in
   let a1 = atom_of_avail avail1 in
   let a2 = atom_of_avail avail2 in
   let a' =
      match op, a1, a2 with
         PlusIntOp _, _, AtomInt j
       | MinusIntOp _, _, AtomInt j
       | PlusPointerOp, _, AtomInt j
         when Rawint.is_zero j ->
            Some2 avail1
       | PlusIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.add i j))
       | MinusIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.sub i j))
       | MulIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.mul i j))
       | DivIntOp _, AtomInt i, AtomInt j when not (Rawint.is_zero j) ->
            Some1 (AtomInt (Rawint.div i j))
       | RemIntOp _, AtomInt i, AtomInt j when not (Rawint.is_zero j) ->
            Some1 (AtomInt (Rawint.rem i j))
       | SlIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.shift_left i j))
       | SrIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.shift_right i j))
       | AndIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.logand i j))
       | OrIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.logor i j))
       | XorIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.logxor i j))
       | MinIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.min i j))
       | MaxIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.max i j))
       | EqIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i = 0) i j))
       | NeqIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i <> 0) i j))
       | LtIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i < 0) i j))
       | LeIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i <= 0) i j))
       | GeIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i >= 0) i j))
       | SetBitFieldOp (_, _, off, len), AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.set_field i off len j))

       | PlusFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.add x y))
       | MinusFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.sub x y))
       | MulFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.mul x y))
       | DivFloatOp _, AtomFloat x, AtomFloat y when not (Rawfloat.is_zero y) ->
            Some1 (AtomFloat (Rawfloat.div x y))
       | RemFloatOp _, AtomFloat x, AtomFloat y when not (Rawfloat.is_zero y) ->
            Some1 (AtomFloat (Rawfloat.rem x y))
       | MinFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.min x y))
       | MaxFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.max x y))
       | EqFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i = 0) x y))
       | NeqFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i <> 0) x y))
       | LtFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i < 0) x y))
       | LeFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i <= 0) x y))
       | GtFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i > 0) x y))
       | GeFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i >= 0) x y))

       | EqPointerOp, AtomNil _, AtomNil _ ->
            Some1 (AtomUnit (2, 1))
       | NeqPointerOp, AtomNil _, AtomNil _ ->
            Some1 (AtomUnit (2, 0))

       | _ ->
            (* None of the others get inlined *)
            None0
   in

   (* If IdOp, then add to avail list *)
   let v' = new_symbol v in
   let aenv =
      match a' with
         Some2 avail ->
            aenv_add aenv v avail
       | Some1 a ->
            aenv_add aenv v (AvailAtom a)
       | None0 ->
            aenv_add aenv v (AvailVar (v', ty))
   in
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (LetBinop (v', ty, op, a1, a2, e))

(*
 * Conditionals.
 * If the test can be matched, drop a branch.
 *)
and inline_if_exp tenv fenv aenv name pos loc a e1 e2 =
   let pos = string_pos "inline_if_exp" pos in
   let a = inline_atom aenv a in
      match a with
         AtomUnit (_, i) ->
            if i = 0 then
               inline_exp tenv fenv aenv name e2
            else
               inline_exp tenv fenv aenv name e1
       | AtomInt i ->
            if Rawint.is_zero i then
               inline_exp tenv fenv aenv name e2
            else
               inline_exp tenv fenv aenv name e1
       | AtomFloat x ->
            if Rawfloat.is_zero x then
               inline_exp tenv fenv aenv name e2
            else
               inline_exp tenv fenv aenv name e1
       | AtomNil _ ->
            inline_exp tenv fenv aenv name e2
       | AtomVar _
       | AtomLabel _ ->
            let e1 = inline_exp tenv fenv aenv name e1 in
            let e2 = inline_exp tenv fenv aenv name e2 in
               make_exp loc (IfThenElse (a, e1, e2))

(*
 * External call.
 * Never inlined.
 *)
and inline_extcall_exp tenv fenv aenv name pos loc v ty s b ty' args e =
   let pos = string_pos "inline_extcall_exp" pos in
   let args = inline_atoms aenv args in
   let v' = new_symbol v in
   let aenv = aenv_add aenv v (AvailVar (v', ty)) in
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (LetExtCall (v', ty, s, b, ty', args, e))

(*
 * Inline the closure.
 * Add it to the available expressions.
 *)
and inline_closure_exp tenv fenv aenv name pos loc v f args e =
   let pos = string_pos "inline_closure_exp" pos in
   let args = inline_atoms aenv args in
   let aenv = aenv_add aenv v (AvailClosure (f, args)) in
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (LetClosure (v, f, args, e))

(*
 * Inline the tailcall.
 * If the function is available, substitute and recurse.
 *)
and inline_tailcall_normal tenv fenv aenv name pos loc f args =
   let pos = string_pos "inline_tailcall_normal" pos in
   let args = inline_atoms aenv args in
      if Symbol.eq f name || is_ancestor fenv f name then
         make_exp loc (TailCall (TailNormal (inline_var aenv f, args)))
      else
         match aenv_lookup aenv f with
            AvailFunction (f, vars, e) as avail ->
               if debug Fir_state.debug_print_ir then
                  eprintf "Inlining %s from %s@." (string_of_symbol f) (string_of_symbol name);
               let aenv = aenv_bind_vars aenv pos vars args in
                  inline_exp tenv fenv aenv f e
          | AvailClosure (f, args') ->
               inline_tailcall_normal tenv fenv aenv name pos loc f (args' @ args)
          | AvailAtom (AtomVar v) when not (Symbol.eq v f) ->
               inline_tailcall_normal tenv fenv aenv name pos loc v args
          | _ ->
               make_exp loc (TailCall (TailNormal (inline_var aenv f, args)))

(*
 * Migration doesn't allow inlining.
 *)
and inline_tailcall_migrate tenv fenv aenv name pos loc f host args =
   let pos = string_pos "inline_tailcall_migrate" pos in
   let host = inline_atoms aenv host in
   let args = inline_atoms aenv args in
      make_exp loc (TailCall (TailSysMigrate (inline_var aenv f, host, args)))

and inline_tailcall_atomic tenv fenv aenv name pos loc f c args =
   let pos = string_pos "inline_tailcall_atomic" pos in
   let c = inline_atom aenv c in
   let args = inline_atoms aenv args in
      make_exp loc (TailCall (TailAtomic (inline_var aenv f, c, args)))

and inline_tailcall_atomic_commit tenv fenv aenv name pos loc level f args =
   let pos = string_pos "inline_tailcall_atomic_commit" pos in
   let level = inline_atom aenv level in
   let args = inline_atoms aenv args in
      make_exp loc (TailCall (TailAtomicCommit (level, inline_var aenv f, args)))

and inline_tailcall_atomic_rollback tenv fenv aenv name pos loc level c =
   let pos = string_pos "inline_tailcall_atomic_rollback" pos in
   let level = inline_atom aenv level in
   let c = inline_atom aenv c in
      make_exp loc (TailCall (TailAtomicRollback (level, c)))

(*
 * General tailcall inlining.
 *)
and inline_tailcall_exp tenv fenv aenv name pos loc op =
   let pos = string_pos "inline_tailcall_exp" pos in
      match op with
         TailNormal (f, args) ->
            inline_tailcall_normal tenv fenv aenv name pos loc f args
       | TailSysMigrate (f, host, args) ->
            inline_tailcall_migrate tenv fenv aenv name pos loc f host args
       | TailAtomic (f, c, args) ->
            inline_tailcall_atomic tenv fenv aenv name pos loc f c args
       | TailAtomicCommit (level, f, args) ->
            inline_tailcall_atomic_commit tenv fenv aenv name pos loc level f args
       | TailAtomicRollback (level, c) ->
            inline_tailcall_atomic_rollback tenv fenv aenv name pos loc level c

(*
 * Cancel addr-of and subscripting.
 *)
and inline_subscript_exp tenv fenv aenv name pos loc v ty v2 a3 e =
   let pos = string_pos "inline_subscript_exp" pos in
   let a3 = inline_atom aenv a3 in
   let e =
      match aenv_lookup aenv v2, a3 with
         AvailAddressOf (_, v2), AtomInt i when Rawint.is_zero i ->
            let aenv = aenv_add aenv v (AvailAtom (AtomVar v2)) in
               LetAtom (v, ty, AtomVar v2, inline_exp tenv fenv aenv name e)
       | AvailAtom (AtomVar v2), _ ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailSubscript (v', v2, a3)) in
               LetSubscript (v', ty, v2, a3, inline_exp tenv fenv aenv name e)
       | AvailAtom a, AtomInt i when Rawint.is_zero i ->
            let aenv = aenv_add aenv v (AvailAtom a) in
               LetAtom (v, ty, a, inline_exp tenv fenv aenv name e)
       | _ ->
            let v' = new_symbol v in
            let v2 = inline_var aenv v2 in
            let aenv = aenv_add aenv v (AvailSubscript (v', v2, a3)) in
               LetSubscript (v', ty, v2, a3, inline_exp tenv fenv aenv name e)
   in
      make_exp loc e

and inline_set_subscript_exp tenv fenv aenv name pos loc v1 a2 ty a3 e =
   let pos = string_pos "inline_set_subscript_exp" pos in
   let a2 = inline_atom aenv a2 in
   let a3 = inline_atom aenv a3 in
   let e =
      match aenv_lookup aenv v1, a2 with
         AvailAddressOf (_, v1), AtomInt i when Rawint.is_zero i ->
            SetVar (inline_var aenv v1, ty, a3, inline_exp tenv fenv aenv name e)
       | _ ->
            SetSubscript (inline_var aenv v1, a2, ty, a3, inline_exp tenv fenv aenv name e)
   in
      make_exp loc e

and inline_set_global_exp tenv fenv aenv name pos loc v ty a e =
   let pos = string_pos "inline_set_global_exp" pos in
      make_exp loc (SetGlobal (inline_var aenv v, ty, inline_atom aenv a, inline_exp tenv fenv aenv name e))

and inline_addr_of_exp tenv fenv aenv name pos loc v1 ty v2 e =
   let pos = string_pos "inline_addr_of_exp" pos in
   let e =
      match aenv_lookup aenv v2 with
         AvailSubscript (_, v, a) ->
            LetBinop (v1, ty, PlusPointerOp, AtomVar v, a, inline_exp tenv fenv aenv name e)
       | _ ->
            let ty' = type_of_var aenv pos v2 in
            let v1' = new_symbol v1 in
            let v2 = inline_var aenv v2 in
            let aenv =
               if is_scalar_type tenv pos ty' then
                  aenv_add aenv v1 (AvailAddressOf (v1', v2))
               else
                  aenv_add aenv v1 (AvailVar (v1', ty))
            in
               LetAddrOfVar (v1', ty, inline_var aenv v2, inline_exp tenv fenv aenv name e)
   in
      make_exp loc e

(*
 * Projections.
 *)
and inline_set_project_exp tenv fenv aenv name pos loc v1 label ty a4 e =
   let pos = string_pos "inline_set_project_exp" pos in
   let a4 = inline_atom aenv a4 in
   let v1 = inline_var aenv v1 in
   let aenv = aenv_set_field aenv v1 label a4 in
   let e = inline_exp tenv fenv aenv name e in
      make_exp loc (SetProject (v1, label, ty, a4, e))

and inline_project_exp tenv fenv aenv name pos loc v1 ty v2 label e =
   let pos = string_pos "inline_project_exp" pos in
   let e =
      match aenv_lookup aenv v2 with
(* TEMP JDS BUG (2002.09.06):
   If this is enabled, then test/fc/simple/ref.c will fail.  This code does
   not take into account the fact that the frame label may have been modified
   indirectly by a SetSubscript call (if the label's address was taken).
         AvailFrame (v2, fields) ->
            (try
                let v2 = aenv_field_atom fields label in
                   LetAtom (v1, ty, v2, inline_exp tenv fenv aenv name e)
             with
                Not_found ->
                   let v1' = new_symbol v1 in
                   let aenv = aenv_add aenv v1 (AvailVar (v1', ty)) in
                      LetProject (v1', ty, v2, label, inline_exp tenv fenv aenv name e))
 *)
       | AvailAtom (AtomVar v2) ->
            let v1' = new_symbol v1 in
            let aenv = aenv_add aenv v1 (AvailVar (v1', ty)) in
               LetProject (v1', ty, v2, label, inline_exp tenv fenv aenv name e)
       | _ ->
            let v1' = new_symbol v1 in
            let aenv = aenv_add aenv v1 (AvailVar (v1', ty)) in
               LetProject (v1', ty, inline_var aenv v2, label, inline_exp tenv fenv aenv name e)
   in
      make_exp loc e

(*
 * Debugging.
 *)
and inline_debug_exp tenv fenv aenv name pos loc info e =
   let pos = string_pos "inline_debug_exp" pos in
   let e = inline_exp tenv fenv aenv name e in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) ->
                     let v2 =
                        match aenv_lookup aenv v2 with
                           AvailAtom (AtomVar v2) -> v2
                         | _ -> v2
                     in
                        v1, ty, v2) vars
            in
               DebugContext (line, vars)
   in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Inline the entire program.
 *)
let inline_prog prog =
   let { prog_types = tenv;
         prog_funs = funs;
         prog_body = body
       } = prog
   in
   let fenv, aenv = avail_prog prog in

   (*
    * Inline each of the funs.
    *)
   let funs =
      SymbolTable.mapi (fun f (line, gflag, ty, vars, e) ->
            line, gflag, ty, vars, inline_exp tenv fenv aenv f e) funs
   in

   (* Inline the program body *)
   let body = inline_exp tenv fenv aenv root_sym body in
   let prog =
      { prog with prog_funs = funs;
                  prog_body = body
      }
   in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
