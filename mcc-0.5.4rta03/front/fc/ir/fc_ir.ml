(*
 * Type definitions for the FC IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Rawint
open Rawfloat
open Attribute
open Location

type var = symbol
type label = symbol
type ty_var = symbol

type 'core simple_subst = (loc, 'core) simple_term

(*
 * Function classes:
 *    VarGlobalClass: user-defined function, may be global or static, may escape
 *    VarContClass: continuation, may escape
 *    VarLocalClass: compiler-defined local function, never escapes
 *)
type var_class =
   VarGlobalClass
 | VarLocalClass
 | VarContClass

(*
 * During most of the translation, we use these
 * types, where all aggregate types have been
 * folded into TyRawData, and all TyRef have been
 * converted to TyPointer.
 *
 * TyFrame is a temporary type used for frame allocation.
 * We deal with it because we want to be able to perform
 * dead-field elimination on the frame.
 * It gets introduced during closure conversion,
 * and eliminated in fc_ir_frame.
 *)
type ty =
   TyDelayed
 | TyUnit        of int
 | TyInt         of int_precision * int_signed
 | TyFloat       of float_precision
 | TyTag
 | TyPointer     of ty
 | TyRawData     of int
 | TyFun         of ty list * ty
 | TyVar         of var
 | TyLambda      of var list * ty
 | TyAll         of var list * ty
 | TyApply       of ty_var * ty list
 | TyFrame       of label

(*
 * A frame has a list of fields,
 * each of which has a list of subfields.
 *)
type frame = (var * ty) list SymbolTable.t

(*
 * A frame label is a triple (frame, field, subfield)
 *)
type frame_label = label * label * label

(*
 * Values.
 *)
type atom =
   AtomUnit  of int * int
 | AtomInt   of rawint
 | AtomFloat of rawfloat
 | AtomNil   of ty
 | AtomVar   of var
 | AtomLabel of frame_label

(*
 * Operators.
 *)
and unop =
   (* Native ints *)
   UMinusIntOp of int_precision * int_signed
 | NotIntOp    of int_precision * int_signed
 | BitFieldOp  of int_precision * int_signed * int * int

   (* Floats *)
 | UMinusFloatOp of float_precision
 | AbsFloatOp    of float_precision
 | SinOp	 of float_precision
 | CosOp	 of float_precision
 | SqrtOp	 of float_precision

 | EqPointerNilOp
 | NeqPointerNilOp

   (* Coercions *)
 | IntOfInt     of int_precision * int_signed * int_precision * int_signed
 | IntOfFloat   of int_precision * int_signed * float_precision
 | IntOfUnit    of int_precision * int_signed * int
 | FloatOfInt   of float_precision * int_precision * int_signed
 | FloatOfFloat of float_precision * float_precision
 | IntOfPointer of int_precision * int_signed
 | PointerOfInt of int_precision * int_signed

and binop =
   AndBoolOp
 | OrBoolOp

 | PlusIntOp   of int_precision * int_signed
 | MinusIntOp  of int_precision * int_signed
 | MulIntOp    of int_precision * int_signed
 | DivIntOp    of int_precision * int_signed
 | RemIntOp    of int_precision * int_signed
 | SlIntOp     of int_precision * int_signed
 | SrIntOp     of int_precision * int_signed
 | AndIntOp    of int_precision * int_signed
 | OrIntOp     of int_precision * int_signed
 | XorIntOp    of int_precision * int_signed

 | MinIntOp    of int_precision * int_signed
 | MaxIntOp    of int_precision * int_signed

 | SetBitFieldOp of int_precision * int_signed * int * int

 | EqIntOp     of int_precision * int_signed
 | NeqIntOp    of int_precision * int_signed
 | LtIntOp     of int_precision * int_signed
 | LeIntOp     of int_precision * int_signed
 | GtIntOp     of int_precision * int_signed
 | GeIntOp     of int_precision * int_signed

 | PlusFloatOp   of float_precision
 | MinusFloatOp  of float_precision
 | MulFloatOp    of float_precision
 | DivFloatOp    of float_precision
 | RemFloatOp    of float_precision

 | MinFloatOp    of float_precision
 | MaxFloatOp    of float_precision

 | EqFloatOp     of float_precision
 | NeqFloatOp    of float_precision
 | LtFloatOp     of float_precision
 | LeFloatOp     of float_precision
 | GtFloatOp     of float_precision
 | GeFloatOp     of float_precision

 | Atan2Op       of float_precision

   (* Tags are only compared with equality *)
 | EqTagOp

   (* Pointer operations *)
 | PlusPointerOp
 | MinusPointerOp

 | EqPointerOp
 | NeqPointerOp
 | LtPointerOp
 | LePointerOp
 | GtPointerOp
 | GePointerOp

(*
 * Debugging info.
 *)
and debug_line = loc

and debug_vars = (var * ty  * var) list

and debug_info =
   DebugString of string
 | DebugContext of debug_line * debug_vars

(*
 * Tail call operators for the SpecialCall construct.  The SpecialCall
 * is used by constructs which will eventually call some other local
 * function (say, f), and as such they resemble normal tailcalls. How-
 * ever, these constructs require some special processing first, such
 * as process migration or atomicity.  This construct identifies which
 * case is occurring.  Please note these are more thoroughly documented
 * in the FIR type file.
 *
 * The special ops are briefly described below; check the MIR or runtime
 * docs for a more thorough description however.
 *
 *    TailSimple (f, args)
 *       This is a normal tailcall to f(args)
 *
 *    TailSysMigrate (f, host, args)
 *       Perform a migration between systems.  The destination is
 *       described by the string host. After migration, the function
 *       f is called with args given; if args = (v1, v2, ..., vn) then
 *       we will call f(v1, v2, ..., vn).
 *
 * Atomic levels are documented in the FIR type file.  Please see that
 * file for more detailed information.
 *
 *    TailAtomic (f, c, args)
 *       Enter a new atomic block, then call f.  c is (currently) an
 *       integer constant that is passed to f; args are the variables
 *       which are normally passed to f; note that on rollback, f will
 *       be called again; it will receive the same args but the value
 *       of c will change.  If args = (v1, v2, ..., vn) then we will
 *       call f(c, v1, v2, ..., vn)
 *
 *    TailAtomicRollback (level, c)
 *       Rollback the atomic level indicated, and all later levels.  Call
 *       the function f associated with level again, with a new argument.
 *
 *    TailAtomicCommit (level, g, args)
 *       Commits (exits) the atomic level indicated, then calls function
 *       g with the args given.  Note that g is not same as function f
 *       above; this can be an arbitrary function, and it receives only
 *       the args given here.  The level given is folded with the previous
 *       atomic level.
 *)
type tailop =
   TailNormal of var * atom list
 | TailSysMigrate of var * atom list * atom list
 | TailAtomic of var * atom * atom list
 | TailAtomicRollback of atom * atom
 | TailAtomicCommit of atom * var * atom list

(*
 * Intermediate code expressions.
 *)
and exp = exp_core simple_subst

and exp_core =
   (* Declarations *)
   LetFuns of fundef list * exp

   (* Arithmetic *)
 | LetAtom of var * ty * atom * exp
 | LetUnop of var * ty * unop * atom * exp
 | LetBinop of var * ty * binop * atom * atom * exp
 | LetExtCall of var * ty * string * bool * ty * atom list * exp
 | LetClosure of var * var * atom list * exp
 | TailCall of tailop

   (* Allocation *)
 | LetMalloc of var * atom * exp

   (* Conditionals *)
 | IfThenElse of atom * exp * exp

   (* Subscript operations *)
 | LetSubscript of var * ty * var * atom * exp
 | SetSubscript of var * atom * ty * atom * exp
 | SetGlobal of var * ty * atom * exp
 | Memcpy of var * atom * var * atom * atom * exp

   (* Frame operations *)
 | LetProject of var * ty * var * frame_label * exp
 | LetAddrOfProject of var * ty * var * frame_label * exp
 | SetProject of var * frame_label * ty * atom * exp

   (* Variable operations *)
 | LetVar of var * ty * atom option * exp
 | LetAddrOfVar of var * ty * var * exp
 | SetVar of var * ty * atom * exp
 | LetFrame of var * ty * exp

   (* Documentation *)
 | Debug of debug_info * exp

(*
 * A function definition has:
 *    1. a name
 *    2. source location
 *    3. flag for function class
 *    4. its type
 *    5. its arguments
 *    6. the body
 *)
and fundef = var * debug_line * var_class * ty * var list * exp

(*
 * Global def does not include function name.
 *)
type gfundef = debug_line * var_class * ty * var list * exp

(*
 * Global initializer.
 *)
type init =
   InitString of int_precision * int array
 | InitAtom of atom
 | InitBlock of int

(*
 * The bool is set if the value is volatile.
 *)
type global = ty * bool * init

type import =
   { import_name : string;
     import_type : ty;
     import_info : Fir.import_info
   }

type export =
   { export_name : string;
     export_type : ty
   }

(*
 * A program has:
 *    1. globals
 *    2. functions
 *    3. an expression
 *)
type prog =
   { prog_file    : string;
     prog_globals : global SymbolTable.t;
     prog_decls   : ty SymbolTable.t;
     prog_import  : import SymbolTable.t;
     prog_export  : export SymbolTable.t;
     prog_types   : ty SymbolTable.t;
     prog_funs    : gfundef SymbolTable.t;
     prog_frames  : frame SymbolTable.t;
     prog_body    : exp
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
