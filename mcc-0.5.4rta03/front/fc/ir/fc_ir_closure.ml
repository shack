(*
 * This is the first part of closure conversion.
 * Convert functions so they have no free variables.
 *
 * There are several passes here.
 *    1. First, collect _all_ the definitions in each global function.
 *       This will be a superset of the function's frame.
 *    2. Next, construct the dataflow equations for each function.
 *    3. Solve the dataflow equations.  Global variables are never
 *       free in a function, and global functions never let any
 *       of their definitions (regardless of scope) to escape.
 *    4. Construct the set of escaping variables.  A variable escapes
 *       if it is free in a continuation or a global function.  Build
 *       a table that maps escaping vars to the frames in which they
 *       were defined.
 *    5. Close the program.  All references to escaping variables
 *       are converted to frame operations or operations on the
 *       globals.  Note that globals are _always_ pointers, so we
 *       have to insert subscript operations to access them.
 *
 *       Each global function gets a frame, and the frames are
 *       linked in a list in the nesting order.  All escaping functions
 *       get a frame.  All closures, fetch all the frames they can access.
 *
 *       Local functions are treated as if they don't have frames.
 *       Free variables are passed as extra arguments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_config

open Fc_ir
open Fc_ir_ds
open Fc_ir_exn
open Fc_ir_pos
open Fc_ir_env
open Fc_ir_type
open Fc_ir_print
open Fc_ir_standardize

module Pos = MakePos (struct let name = "Fc_ir_closure" end)
open Pos

(************************************************************************
 * ENVIRONMENTS
 ************************************************************************)

let native_char_precision = FCParam.char_precision NormalPrecision

(*
 * In the first pass, we collect all the function names.
 *)
module DefSet = SymbolSet
module UseSet = SymbolSet
module EscSet = SymbolSet
module CallTable = SymbolTable
module DefTable = SymbolTable
module FunTable = SymbolTable

(*
 * This is the info that is recorded for each function.
 *)
type entry =
   { entry_nest : var list;
     entry_defs : ty DefTable.t;
     entry_esc : EscSet.t;
     entry_frame : var
   }

(*
 * These are synthesized attributes.
 *    dflow_defs: _all_ the defs in the function body (used only for global funs)
 *    dflow_uses: the uses (free vars) of the function
 *    dflow_calls: the calls (with defs) for each function call
 *    dflow_esc: variables kthat _must_ escape
 *)
type dflow =
   { dflow_class : var_class;
     dflow_entry : entry;
     dflow_calls : DefSet.t CallTable.t;
     dflow_uses : UseSet.t;
     dflow_top : UseSet.t
   }

(*
 * After analysis, this is the in info we keep for each function.
 *   fun_nest: list of nested functions, hd is parent
 *   fun_uses: free variables
 *   fun_info: extra info
 *      GlobalInfo (frame, fields): name of the frame and its fields
 *)
type fun_info =
   GlobalInfo of var * (var * ty) DefTable.t * ty SymbolTable.t
 | LocalInfo

type fun_desc =
   { fun_nest : var list;
     fun_uses : var list;
     fun_info : fun_info;
     fun_gflag : var_class
   }

(*
 * This is the info we carry around during conversion.
 *    close_esc: table of escaping vars, and the frame they belong to
 *    close_funs: info for fun conversion
 *)
type close_var =
   GlobalVar
 | ExternVar
 | FrameVar of var * var

type close_info =
   { close_esc : (close_var * ty) SymbolTable.t;
     close_funs : fun_desc FunTable.t
   }

let cenv_lookup_fun { close_funs = funs } pos f =
   try SymbolTable.find funs f with
      Not_found ->
         raise (IRException (pos, UnboundVar f))

let fun_table_lookup fenv pos f =
   try FunTable.find fenv f with
      Not_found ->
         raise (IRException (pos, UnboundVar f))

let table_lookup venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (IRException (pos, UnboundVar v))

(*
 * Utilities.
 *)
let hd pos = function
   h :: _ -> h
 | _ -> raise (IRException (pos, StringError "hd on empty list"))

(************************************************************************
 * INITIAL DEFS
 ************************************************************************)

(*
 * Add a variable definition to the defs.
 *)
let defs_var tenv esc defs pos v ty =
   let esc =
      if is_aggregate_type tenv pos ty then
         EscSet.add esc v
      else
         esc
   in
   let defs = DefTable.add defs v ty in
      esc, defs

(*
 * Collect the info for the functions.
 *)
let rec defs_exp tenv fenv nest esc defs e =
   let pos = string_pos "defs_exp" (exp_pos e) in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            defs_funs_exp tenv fenv nest esc defs pos funs e
       | LetAtom (v, ty, _, e)
       | LetUnop (v, ty, _, _, e)
       | LetBinop (v, ty, _, _, _, e)
       | LetExtCall (v, ty, _, _, _, _, e)
       | LetSubscript (v, ty, _, _, e)
       | LetVar (v, ty, _, e)
       | LetFrame (v, ty, e)
       | LetProject (v, ty, _, _, e)
       | LetAddrOfProject (v, ty, _, _, e) ->
            let esc, defs = defs_var tenv esc defs pos v ty in
               defs_exp tenv fenv nest esc defs e
       | LetMalloc (v, _, e) ->
            let esc, defs = defs_var tenv esc defs pos v (TyPointer (TyInt (native_char_precision, false))) in
               defs_exp tenv fenv nest esc defs e
       | IfThenElse (_, e1, e2) ->
            let fenv, esc, defs = defs_exp tenv fenv nest esc defs e1 in
               defs_exp tenv fenv nest esc defs e2
       | SetSubscript (_, _, _, _, e)
       | SetGlobal (_, _, _,e)
       | SetProject (_, _, _, _, e)
       | SetVar (_, _, _, e)
       | Memcpy (_, _, _, _, _, e)
       | Debug (_, e) ->
            defs_exp tenv fenv nest esc defs e
       | LetAddrOfVar (v1, ty, v2, e) ->
            let esc = EscSet.add esc v2 in
            let esc, defs = defs_var tenv esc defs pos v1 ty in
               defs_exp tenv fenv nest esc defs e
       | TailCall _ ->
            fenv, esc, defs
       | LetClosure _ ->
            raise (IRException (pos, StringError "unexpected LetClosure"))

(*
 * Collect the definitions in the functions.
 *)
and defs_funs_exp tenv fenv nest esc defs pos funs e =
   let pos = string_pos "defs_funs_exp" pos in
   let fenv, esc, defs =
      List.fold_left (fun (fenv, esc, defs) (f, _, gflag, ty, vars, e) ->
            match gflag with
               VarGlobalClass ->
                  let _, ty_args, _ = dest_fun_type tenv pos ty in
                  let defs' = List.fold_left2 DefTable.add DefTable.empty vars ty_args in
                  let frame = new_symbol f in
                  let fenv, esc', defs' = defs_exp tenv fenv (frame :: nest) EscSet.empty defs' e in
                  let entry =
                     { entry_nest = nest;
                       entry_defs = defs';
                       entry_esc = esc';
                       entry_frame = frame
                     }
                  in
                  let fenv = FunTable.add fenv f entry in
                     fenv, esc, defs
             | VarContClass
             | VarLocalClass ->
                  (* Add a dummy entry so the function will be detected *)
                  let entry =
                     { entry_nest = nest;
                       entry_defs = DefTable.empty;
                       entry_esc = EscSet.empty;
                       entry_frame = f
                     }
                  in
                  (* Add the defs to parent's frame *)
                  let _, ty_args, _ = dest_fun_type tenv pos ty in
                  let defs = List.fold_left2 DefTable.add defs vars ty_args in
                  let fenv = FunTable.add fenv f entry in
                     defs_exp tenv fenv nest esc defs e) (fenv, esc, defs) funs
   in
      defs_exp tenv fenv nest esc defs e

(************************************************************************
 * DATAFLOW GRAPH
 ************************************************************************)

(*
 * Use a var, this adds to the uses.
 * If this is a function, add it to the call table.
 * Otherwise, add the use.
 *)
let dflow_use_var fenv defs calls uses v =
   if FunTable.mem fenv v then
      let calls = CallTable.add calls v defs in
         calls, uses
   else
      let uses = UseSet.add uses v in
         calls, uses

(*
 * Atom uses.
 *)
let dflow_use_atom fenv defs calls uses a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomNil _
    | AtomLabel _ ->
         calls, uses
    | AtomVar v ->
         dflow_use_var fenv defs calls uses v

let dflow_use_atom_opt fenv defs calls uses = function
   Some a -> dflow_use_atom fenv defs calls uses a
 | None -> calls, uses

let dflow_use_atoms fenv defs calls uses atoms =
   List.fold_left (fun (calls, uses) ->
         dflow_use_atom fenv defs calls uses) (calls, uses) atoms

(*
 * Transform an expression.
 *)
let rec dflow_exp fenv nest dflow defs calls uses e =
   let pos = string_pos "dflow_exp" (exp_pos e) in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            dflow_funs_exp fenv nest dflow defs calls uses pos funs e
       | LetAtom (v, _, a, e)
       | LetUnop (v, _, _, a, e) ->
            dflow_def_arg_exp fenv nest dflow defs calls uses pos v a e
       | LetBinop (v, _, _, a1, a2, e) ->
            dflow_def_args_exp fenv nest dflow defs calls uses pos v [a1; a2] e
       | LetExtCall (v, _, _, _, _, args, e) ->
            dflow_def_args_exp fenv nest dflow defs calls uses pos v args e
       | TailCall (TailNormal (f, args)) ->
            dflow_tailcall_exp fenv nest dflow defs calls uses pos (Some f) args
       | TailCall (TailSysMigrate (f, host, args)) ->
            dflow_tailcall_exp fenv nest dflow defs calls uses pos (Some f) (host @ args)
       | TailCall (TailAtomic (f, c, args)) ->
            dflow_tailcall_exp fenv nest dflow defs calls uses pos (Some f) (c :: args)
       | TailCall (TailAtomicCommit (level, f, args)) ->
            dflow_tailcall_exp fenv nest dflow defs calls uses pos (Some f) (level :: args)
       | TailCall (TailAtomicRollback (level, c)) ->
            dflow_tailcall_exp fenv nest dflow defs calls uses pos None [level; c]
       | LetMalloc (v, a, e) ->
            dflow_def_arg_exp fenv nest dflow defs calls uses pos v a e
       | IfThenElse (a, e1, e2) ->
            dflow_if_exp fenv nest dflow defs calls uses pos a e1 e2
       | LetSubscript (v1, _, v2, a3, e) ->
            dflow_subscript_exp fenv nest dflow defs calls uses pos v1 v2 a3 e
       | SetSubscript (v1, a2, _, a3, e) ->
            dflow_set_subscript_exp fenv nest dflow defs calls uses pos v1 a2 a3 e
       | SetGlobal (v, _, a, e) ->
            dflow_set_global_exp fenv nest dflow defs calls uses pos v a e
       | Memcpy (v1, a1, v2, a2, a3, e) ->
            dflow_memcpy_exp fenv nest dflow defs calls uses pos v1 a1 v2 a2 a3 e
       | LetVar (v, _, a_opt, e) ->
            dflow_var_exp fenv nest dflow defs calls uses pos v a_opt e
       | LetFrame (v, _, e) ->
            dflow_frame_exp fenv nest dflow defs calls uses pos v e
       | LetAddrOfVar (v1, _, v2, e) ->
            dflow_addr_of_var_exp fenv nest dflow defs calls uses pos v1 v2 e
       | SetVar (v, _, a, e) ->
            dflow_set_var_exp fenv nest dflow defs calls uses pos v a e
       | Debug (info, e) ->
            dflow_debug_exp fenv nest dflow defs calls uses pos info e
       | LetClosure _
       | LetProject _
       | LetAddrOfProject _
       | SetProject _ ->
            raise (IRException (pos, StringError "unexpected expression"))

(*
 * Dataflow calculation for functions.
 *)
and dflow_funs_exp fenv nest dflow defs calls uses pos funs e =
   let pos = string_pos "dflow_funs_exp" pos in

   (* Walk through the body of each function *)
   let dflow =
      List.fold_left (fun dflow (f, line, gflag, ty, vars, e) ->
            dflow_fun fenv nest dflow pos funs f gflag vars e) dflow funs
   in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e in
   let uses =
      List.fold_left (fun uses (f, _, _, _, _, _) ->
            UseSet.remove uses f) uses funs
   in
      dflow, calls, uses

(*
 * Global function starts a new dflow environment.
 *)
and dflow_fun fenv nest dflow pos funs f gflag vars e =
   let pos = string_pos "dflow_global_fun" pos in

   (* Get the function's entry *)
   let nest, entry =
      match gflag with
         VarGlobalClass ->
            f :: nest, fun_table_lookup fenv pos f
       | VarContClass
       | VarLocalClass ->
            nest, fun_table_lookup fenv pos (hd pos nest)
   in

   (* Reset the defs and calls *)
   let defs = DefSet.of_list vars in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs CallTable.empty UseSet.empty e in
   let uses = UseSet.subtract_list uses vars in
   let uses =
      List.fold_left (fun uses (f, _, _, _, _, _) ->
            UseSet.remove uses f) uses funs
   in

   (* Add the dflow for the function to the env *)
   let entry =
      { dflow_class = gflag;
        dflow_entry = entry;
        dflow_calls = calls;
        dflow_uses = uses;
        dflow_top = uses
      }
   in
      FunTable.add dflow f entry

(*
 * An operation like a function call.
 *)
and dflow_def_args_exp fenv nest dflow defs calls uses pos v args e =
   let pos = string_pos "dflow_def_args_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses e in
   let uses = UseSet.remove uses v in
   let calls, uses = dflow_use_atoms fenv defs calls uses args in
      dflow, calls, uses

(*
 * A simpler one-atom operation.
 *)
and dflow_def_arg_exp fenv nest dflow defs calls uses pos v a e =
   let pos = string_pos "dflow_def_arg_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses e in
   let uses = UseSet.remove uses v in
   let calls, uses = dflow_use_atom fenv defs calls uses a in
      dflow, calls, uses

(*
 * A tailcall.
 *)
and dflow_tailcall_exp fenv nest dflow defs calls uses pos f args =
   let pos = string_pos "dflow_tailcall_exp" pos in
   let calls, uses =
      match f with
         Some f ->
            dflow_use_var fenv defs calls uses f
       | None ->
            calls, uses
   in
   let calls, uses = dflow_use_atoms fenv defs calls uses args in
      dflow, calls, uses

(*
 * Two branches.
 *)
and dflow_if_exp fenv nest dflow defs calls uses pos a e1 e2 =
   let pos = string_pos "dflow_if_exp" pos in
   let calls, uses = dflow_use_atom fenv defs calls uses a in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e1 in
      dflow_exp fenv nest dflow defs calls uses e2

(*
 * Subscript.
 *)
and dflow_subscript_exp fenv nest dflow defs calls uses pos v1 v2 a3 e =
   let pos = string_pos "dflow_subscript_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow (DefSet.add defs v1) calls uses e in
   let uses = UseSet.remove uses v1 in
   let calls, uses = dflow_use_var fenv defs calls uses v2 in
   let calls, uses = dflow_use_atom fenv defs calls uses a3 in
      dflow, calls, uses

and dflow_set_subscript_exp fenv nest dflow defs calls uses pos v1 a2 a3 e =
   let pos = string_pos "dflow_set_subscript_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e in
   let calls, uses = dflow_use_var fenv defs calls uses v1 in
   let calls, uses = dflow_use_atom fenv defs calls uses a2 in
   let calls, uses = dflow_use_atom fenv defs calls uses a3 in
      dflow, calls, uses

and dflow_set_global_exp fenv nest dflow defs calls uses pos v a e =
   let pos = string_pos "dflow_set_global_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e in
   let calls, uses = dflow_use_var fenv defs calls uses v in
   let calls, uses = dflow_use_atom fenv defs calls uses a in
      dflow, calls, uses

and dflow_memcpy_exp fenv nest dflow defs calls uses pos v1 a1 v2 a2 a3 e =
   let pos = string_pos "dflow_memcpy_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e in
   let calls, uses = dflow_use_var fenv defs calls uses v1 in
   let calls, uses = dflow_use_atom fenv defs calls uses a1 in
   let calls, uses = dflow_use_var fenv defs calls uses v2 in
   let calls, uses = dflow_use_atom fenv defs calls uses a2 in
   let calls, uses = dflow_use_atom fenv defs calls uses a3 in
      dflow, calls, uses

(*
 * Var operations.
 * Aggregate vars always get lifted to toplevel defs.
 *)
and dflow_var_exp fenv nest dflow defs calls uses pos v a_opt e =
   let pos = string_pos "dflow_var_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses e in
   let uses = UseSet.remove uses v in
   let calls, uses = dflow_use_atom_opt fenv defs calls uses a_opt in
      dflow, calls, uses

and dflow_frame_exp fenv nest dflow defs calls uses pos v e =
   let pos = string_pos "dflow_frame_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses e in
   let uses = UseSet.remove uses v in
      dflow, calls, uses

and dflow_addr_of_var_exp fenv nest dflow defs calls uses pos v1 v2 e =
   let pos = string_pos "dflow_addr_of_var_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow (DefSet.add defs v1) calls uses e in
   let uses = UseSet.remove uses v1 in
   let calls, uses = dflow_use_var fenv defs calls uses v2 in
      dflow, calls, uses

and dflow_set_var_exp fenv nest dflow defs calls uses pos v a e =
   let pos = string_pos "dflow_set_var_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e in
   let calls, uses = dflow_use_var fenv defs calls uses v in
   let calls, uses = dflow_use_atom fenv defs calls uses a in
      dflow, calls, uses

(*
 * Debug uses all the vars.
 *)
and dflow_debug_exp fenv nest dflow defs calls uses pos info e =
   let pos = string_pos "dflow_debug_exp" pos in
   let dflow, calls, uses = dflow_exp fenv nest dflow defs calls uses e in
   let calls, uses =
      match info with
         DebugString _ ->
            calls, uses
       | DebugContext (_, vars) ->
            List.fold_left (fun (calls, uses) (_, _, v) ->
                  dflow_use_var fenv defs calls uses v) (calls, uses) vars
   in
      dflow, calls, uses

(************************************************************************
 * DATAFLOW CALCULATION
 ************************************************************************)

(*
 * Print data flow.
 *)
let pp_print_dflow buf dflow =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "DFlow:";
   FunTable.iter (fun f info ->
         let { dflow_calls = calls;
               dflow_uses = uses;
               dflow_top = top;
             } = info
         in
            pp_print_space buf ();
            pp_print_symbol buf f;

            pp_open_hvbox buf tabstop;
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "Calls:";
            CallTable.iter (fun g defs ->
                  pp_print_space buf ();
                  pp_print_symbol buf g) calls;
            pp_close_box buf ();

            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "Uses: ";
            UseSet.iter (fun v ->
                  pp_print_string buf " ";
                  pp_print_symbol buf v) uses;
            pp_close_box buf ();

            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "Top: ";
            UseSet.iter (fun v ->
                  pp_print_string buf " ";
                  pp_print_symbol buf v) top;
            pp_close_box buf ();

            pp_close_box buf ()) dflow;
   pp_close_box buf ();
   pp_print_newline buf ()

(*
 * Solve the dataflow equations.
 *
 *    vars[n] = uses[n] + union ((p, defs) in calls[n]. uses[p] - defs)
 *
 * Solve by iteration.
 *)
let dflow_solve globals dflow =
   (* Vars: one sweep of the dataflow computation *)
   let step_uses dflow =
      FunTable.fold (fun (dflow, changed) f info ->
            let { dflow_class = gflag;
                  dflow_entry = entry;
                  dflow_calls = calls;
                  dflow_uses = uses;
                  dflow_top = top
                } = info
            in

            (* Add call uses to uses *)
            let top' =
               CallTable.fold (fun top' g defs ->
                     let pos = var_exp_pos g in
                     let { dflow_uses = uses } = fun_table_lookup dflow pos g in
                     let top = DefSet.fold UseSet.remove uses defs in
                        UseSet.union top' top) top calls
            in

            (* Remove global defs *)
            (* BUG: this is too expensive, think about how to fix it *)
            let top' = UseSet.subtract_list top' globals in
            let top' =
               FunTable.fold (fun top' f _ ->
                     UseSet.remove top' f) top' dflow
            in

            (* Remove defs in global function *)
            let uses' =
               match gflag with
                  VarGlobalClass ->
                     DefTable.fold (fun uses v _ ->
                           UseSet.remove uses v) top' entry.entry_defs
                | VarContClass
                | VarLocalClass ->
                     top'
            in
               (* If nothing changed, don't do anything *)
               if UseSet.equal top top' then
                  dflow, changed
               else
                  let entry = { info with dflow_uses = uses'; dflow_top = top' } in
                  let dflow = FunTable.add dflow f entry in
                     dflow, true) (dflow, false) dflow
   in

   (* Vars: calculate fixpoint *)
   let rec fixpoint_uses dflow =
      let dflow, changed = step_uses dflow in
         if changed then
            fixpoint_uses dflow
         else
            dflow
   in
   (* Now call them *)
   let dflow = fixpoint_uses dflow in
      if debug Fir_state.debug_print_ir then
         pp_print_dflow err_formatter dflow;
      dflow

(************************************************************************
 * FRAME ASSIGNMENT
 ************************************************************************)

(*
 * Build a table that assiciates all variables with their frame.
 *)
let frame_all tenv import globals dflow =
   (* Add all the globals without a frame *)
   let frame =
      SymbolTable.fold (fun frame v ty ->
            let pos = var_exp_pos v in
            let info = GlobalVar, ty in
               SymbolTable.add frame v info) SymbolTable.empty globals
   in
   let frame =
      SymbolTable.fold (fun frame v { import_type = ty } ->
            SymbolTable.add frame v (ExternVar, ty)) frame import
   in

   (* Add all the defined vars *)
   let frame =
      FunTable.fold (fun frame f info ->
            let { dflow_class = gflag;
                  dflow_entry = entry
                } = info
            in
               match gflag with
                  VarGlobalClass ->
                     let { entry_frame = f';
                           entry_defs = defs
                         } = entry
                     in
                        DefTable.fold (fun frame v ty ->
                              let f' = FrameVar (f', new_symbol v) in
                                 SymbolTable.add frame v (f', ty)) frame defs
                | VarContClass
                | VarLocalClass ->
                     frame) frame dflow
   in
      frame

(*
 * Define all the escaping variables.
 * A variable escapes if it is used by any global function
 * or continuation, or if it is in the escape set already.
 *)
let esc_uses pos esc frame uses =
   let pos = string_pos "esc_uses" pos in
      UseSet.fold (fun esc v ->
            SymbolTable.add esc v (table_lookup frame pos v)) esc uses

let frame_esc pos frame globals dflow =
   let pos = string_pos "frame_esc" pos in
   let esc =
      SymbolTable.fold (fun esc v _ ->
            SymbolTable.add esc v (table_lookup frame pos v)) SymbolTable.empty globals
   in
   let pos = string_pos "frame_esc_fun" pos in
   let esc =
      FunTable.fold (fun esc f info ->
            let { dflow_class = gflag;
                  dflow_uses = uses;
                  dflow_entry = { entry_esc = vars }
                } = info
            in
               match gflag with
                  VarGlobalClass ->
                     let esc = esc_uses pos esc frame vars in
                     let esc = esc_uses pos esc frame uses in
                        esc
                | VarContClass ->
                     esc_uses pos esc frame uses
                | VarLocalClass ->
                     esc) esc dflow
   in
      esc

(*
 * Rebuild the dataflow table.
 * Global functions get at most one use: their parent's frame.
 * Local functions get all their uses, plus all the frames in scope.
 *)
let rebuild_dflow esc dflow =
   FunTable.mapi (fun f info ->
         let { dflow_class = gflag;
               dflow_entry = entry;
               dflow_uses = uses;
               dflow_top = top
             } = info
         in
         let { entry_nest = nest;
               entry_defs = defs;
               entry_esc = vars;
               entry_frame = frame
             } = entry
         in
         let pos = var_exp_pos f in
         let nest, uses, info =
            match gflag with
               VarGlobalClass ->
                  (* Collect only those defs that have escaped *)
                  let free, defs =
                     SymbolTable.fold (fun (free, defs) v ty ->
                           try
                              let gflag, _ = SymbolTable.find esc v in
                              let defs =
                                 match gflag with
                                    FrameVar (_, label) ->
                                       SymbolTable.add defs v (label, ty)
                                  | GlobalVar
                                  | ExternVar ->
                                       defs
                              in
                                 free, defs
                           with
                              Not_found ->
                                 let free =
                                    if UseSet.mem top v then
                                       SymbolTable.add free v ty
                                    else
                                       free
                                 in
                                    free, defs) (SymbolTable.empty, SymbolTable.empty) defs
                  in

                  (* Add the parent's frame if there is one *)
                  let defs, uses =
                     match nest with
                        g :: _ ->
                           let defs = SymbolTable.add defs g (g, TyFrame g) in
                           let uses = [g] in
                              defs, uses
                      | [] ->
                           defs, []
                  in
                  let info = GlobalInfo (frame, defs, free) in
                     nest, uses, info

             | VarContClass ->
                  (* Parent's frame is the only free var *)
                  let uses = [frame] in
                     frame :: nest, uses, LocalInfo

             | VarLocalClass ->
                  (* Filter the argument list to exclude esc vars *)
                  let uses =
                     SymbolSet.fold (fun uses v ->
                           if SymbolTable.mem esc v then
                              uses
                           else
                              SymbolSet.add uses v) SymbolSet.empty uses
                  in

                  (* Add all parent frames as uses *)
                  let uses = SymbolSet.add_list uses (frame :: nest) in
                     nest, UseSet.to_list uses, LocalInfo
         in
            { fun_nest = nest;
              fun_uses = uses;
              fun_info = info;
              fun_gflag = gflag
            }) dflow

(************************************************************************
 * CLOSURE CONVERSION
 ************************************************************************)

(*
 * Normal zero.
 *)
let zero = AtomInt (Rawint.of_int Rawint.Int32 true 0)

(*
 * Defaults for various types.
 *)
let default_atom tenv pos ty =
   match tenv_expand tenv pos ty with
      TyUnit n->
         AtomUnit (n, 0)
    | TyInt (pre, signed) ->
         AtomInt (Rawint.of_int pre signed 0)
    | TyFloat pre ->
         AtomFloat (Rawfloat.of_float pre 0.0)
    | TyPointer ty ->
         AtomNil ty
    | ty ->
         raise (IRException (pos, StringTypeError ("no default type", ty)))

(*
 * Var reference.
 * If the var escapes, change this to a projection.
 *)
let close_var cenv loc v cont =
   try
      let frame, ty = SymbolTable.find cenv.close_esc v in
      let v' = new_symbol v in
         match frame with
            GlobalVar ->
               cont v
          | ExternVar ->
               (* This is an external variable *)
               cont v
          | FrameVar (frame, label) ->
               (* This is in one of the frames *)
               make_exp loc (LetProject (v', ty, frame, (frame, label, label), cont v'))
   with
      Not_found ->
         (* May be a function variable *)
         try
            let { fun_uses = uses } = SymbolTable.find cenv.close_funs v in
               if uses = [] then
                  cont v
               else
                  let v' = new_symbol v in
                     make_exp loc (LetClosure (v', v, List.map (fun v -> AtomVar v) uses, cont v'))
         with
            Not_found ->
               (* Just a normal variable *)
               cont v

(*
 * Addressed var reference.
 * If the var escapes, change this to a projection.
 *)
let close_addr_of_var cenv pos loc v cont =
   try
      let frame, ty = SymbolTable.find cenv.close_esc v in
         match frame with
            GlobalVar ->
               cont v
          | ExternVar ->
               (* BUG: can't take the address of extern variables *)
               raise (IRException (pos, StringVarError ("can't take the address of extern variables (for now)", v)))
          | FrameVar (frame, label) ->
               (* This is in one of the frames *)
               let v' = new_symbol v in
                  make_exp loc (LetAddrOfProject (v', TyPointer ty, frame, (frame, label, label), cont v'))
   with
      Not_found ->
         raise (IRException (pos, StringVarError ("illegal address", v)))

(*
 * Set a var reference.
 *)
let close_set_var cenv pos loc v ty v' e =
   let e =
      try
         let frame, ty = SymbolTable.find cenv.close_esc v in
            match frame with
               GlobalVar
             | ExternVar ->
                  SetGlobal (v, ty, AtomVar v', e)
             | FrameVar (frame, label) ->
                  SetProject (frame, (frame, label, label), ty, AtomVar v', e)
      with
         Not_found ->
            LetAtom (v, ty, AtomVar v', e)
   in
      make_exp loc e

let rec close_set_vars cenv pos loc vars e =
   let rec set e = function
      v :: vars ->
         let e =
            try
               let frame, ty = SymbolTable.find cenv.close_esc v in
                  match frame with
                     GlobalVar
                   | ExternVar ->
                        make_exp loc (SetGlobal (v, ty, AtomVar v, e))
                   | FrameVar (frame, label) ->
                        make_exp loc (SetProject (frame, (frame, label, label), ty, AtomVar v, e))
            with
               Not_found ->
                  e
         in
            set e vars
    | [] ->
         e
   in
      set e vars

(*
 * Atom reference.
 *)
let close_atom cenv loc a cont =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomNil _
    | AtomLabel _ ->
         cont a
    | AtomVar v ->
         close_var cenv loc v (fun v -> cont (AtomVar v))

let close_atom_opt cenv loc a_opt cont =
   match a_opt with
      Some a -> close_atom cenv loc a (fun a -> cont (Some a))
    | None -> cont None

let close_atoms cenv loc atoms cont =
   let rec collect atoms' = function
      a :: atoms ->
         close_atom cenv loc a (fun a ->
               collect (a :: atoms') atoms)
    | [] ->
         cont (List.rev atoms')
   in
      collect [] atoms


(*
 * Project all the nested frames.
 *)
let rec close_project_frames loc frame nest e =
   match nest with
      frame' :: nest ->
         make_exp loc (LetProject (frame', TyFrame frame', frame, (frame, frame', frame'),
                                   close_project_frames loc frame' nest e))
    | [] ->
         e

(*
 * Close an expression.
 *)
let rec close_exp tenv venv cenv e =
   let pos = string_pos "close_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            close_funs_exp tenv venv cenv pos loc funs e
       | LetAtom (v, ty, a, e) ->
            close_atom_exp tenv venv cenv pos loc v ty a e
       | LetUnop (v, ty, op, a, e) ->
            close_unop_exp tenv venv cenv pos loc v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            close_binop_exp tenv venv cenv pos loc v ty op a1 a2 e
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            close_extcall_exp tenv venv cenv pos loc v ty s b ty' args e
       | TailCall op ->
            close_tailcall_exp tenv venv cenv pos loc op
       | LetMalloc (v, a, e) ->
            close_malloc_exp tenv venv cenv pos loc v a e
       | IfThenElse (a, e1, e2) ->
            close_if_exp tenv venv cenv pos loc a e1 e2
       | LetSubscript (v1, ty, v2, a3, e) ->
            close_subscript_exp tenv venv cenv pos loc v1 ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            close_set_subscript_exp tenv venv cenv pos loc v1 a2 ty a3 e
       | SetGlobal (v, ty, a, e) ->
            close_set_global_exp tenv venv cenv pos loc v ty a e
       | Memcpy (v1, a1, v2, a2, a3, e) ->
            close_memcpy_exp tenv venv cenv pos loc v1 a1 v2 a2 a3 e
       | LetVar (v, ty, a_opt, e) ->
            close_var_exp tenv venv cenv pos loc v ty a_opt e
       | LetFrame (v, ty, e) ->
            close_frame_exp tenv venv cenv pos loc v ty e
       | LetAddrOfVar (v1, ty, v2, e) ->
            close_addr_of_var_exp tenv venv cenv pos loc v1 ty v2 e
       | SetVar (v, ty, a, e) ->
            close_set_var_exp tenv venv cenv pos loc v ty a e
       | Debug (info, e) ->
            close_debug_exp tenv venv cenv pos loc info e
       | LetProject _
       | LetAddrOfProject _
       | SetProject _
       | LetClosure _ ->
            raise (IRException (pos, StringError "unexpected expression"))

(*
 * Close the function definitions.
 *)
and close_funs_exp tenv venv cenv pos loc funs e =
   let pos = string_pos "close_funs_exp" pos in
   let funs = List.map (close_fun tenv venv cenv pos) funs in
   let e = close_exp tenv venv cenv e in
      make_exp loc (LetFuns (funs, e))

and close_fun tenv venv cenv pos (f, loc, gflag, ty, vars, e) =
   let pos = string_pos "close_fun" pos in
   let info = cenv_lookup_fun cenv pos f in
   let { fun_nest = nest;
         fun_uses = uses;
         fun_info = info
       } = info
   in

   (* Adjust the arguments *)
   let all_vars, ty_vars, ty_res = dest_fun_type tenv pos ty in
   let ty_vars' = List.map (venv_lookup venv pos) uses in
   let ty_vars, vars =
      match gflag with
         VarLocalClass ->
            (* Local functions can include functions that are target of a
               SpecialCall; they must APPEND the uses to the normal args. *)
            let ty_vars = ty_vars @ ty_vars' in
            let vars = vars @ uses in
               ty_vars, vars
       | VarGlobalClass
       | VarContClass ->
            (* Other functions can escape and in particular may turn up in
               a LetClosure with their uses given as arguments; they must
               PREPEND the uses to the normal args. *)
            let ty_vars = ty_vars' @ ty_vars in
            let vars = uses @ vars in
               ty_vars, vars
   in
   let venv = List.fold_left2 venv_add venv vars ty_vars in
   let ty = TyAll (all_vars, TyFun (ty_vars, ty_res)) in

   (* Add a function header to the body *)
   let e =
      match gflag, info with
         VarGlobalClass, GlobalInfo (frame, fields, free) ->
            close_global_fun tenv venv cenv pos loc nest frame vars ty_vars fields free e
       | VarContClass, _ ->
            close_cont_fun tenv venv cenv pos loc nest vars ty_vars e
       | VarLocalClass, _ ->
            close_local_fun tenv venv cenv pos loc nest e
       | VarGlobalClass, _ ->
            raise (IRException (pos, InternalError "global info does not match"))
   in
      f, loc, gflag, ty, vars, e

(*
 * Global functions have to allocate a frame.
 *    1. Allocate the frame
 *    2. Store the parent frame
 *    3. Project all the frames
 *)
and close_global_fun tenv venv cenv pos loc nest frame vars ty_vars fields free e =
   let pos = string_pos "close_global_fun" pos in
   let v_frame = new_symbol frame in

   (* Add the frame to the environment *)
   let ty_frame = TyFrame frame in
   let venv = venv_add venv frame ty_frame in

   (* Add all the free vars to the environment *)
   let venv = SymbolTable.fold venv_add venv free in

   (* Convert the body *)
   let e = close_exp tenv venv cenv e in

   (* Add all the free vars *)
   let e =
      SymbolTable.fold (fun e v ty ->
            make_exp loc (LetAtom (v, ty, default_atom tenv pos ty, e))) e free
   in

   (* Project all the frames *)
   let e = close_project_frames loc frame nest e in

   (* Store all the arguments *)
   let e =
      List.fold_left2 (fun e v ty ->
            try
               let label, _ = SymbolTable.find fields v in
                  make_exp loc (SetProject (frame, (frame, label, label), ty, AtomVar v, e))
            with
               Not_found ->
                  e) e vars ty_vars
   in

   (* Store the parent frame *)
   let e =
      match nest with
         parent :: _ ->
            make_exp loc (SetProject (frame, (frame, parent, parent), TyFrame parent, AtomVar parent, e))
       | [] ->
            e
   in
      (* Allocate the frame *)
      make_exp loc (LetFrame (frame, ty_frame, e))

(*
 * Continuation projects all the frames.
 *)
and close_cont_fun tenv venv cenv pos loc nest vars ty_vars e =
   let pos = string_pos "close_cont_fun" pos in

   (* Convert the body *)
   let e = close_exp tenv venv cenv e in

   (* Project all the frames *)
   let frame, e =
      match nest with
         frame :: nest ->
            frame, close_project_frames loc frame nest e
       | [] ->
            raise (IRException (pos, InternalError "continuation is not nested"))
   in
      (* Store all the arguments *)
      close_set_vars cenv pos loc vars e

(*
 * Local function needs no function header.
 *)
and close_local_fun tenv venv cenv pos loc nest e =
   close_exp tenv venv cenv e

(*
 * Operator.
 *)
and close_atom_exp tenv venv cenv pos loc v ty a e =
   let pos = string_pos "close_unop_exp" pos in
   let venv = venv_add venv v ty in
      close_atom cenv loc a (fun a ->
            let e = close_exp tenv venv cenv e in
            let v' = new_symbol v in
            let e = close_set_var cenv pos loc v ty v' e in
            let e = LetAtom (v', ty, a, e) in
               make_exp loc e)

and close_unop_exp tenv venv cenv pos loc v ty op a e =
   let pos = string_pos "close_unop_exp" pos in
   let venv = venv_add venv v ty in
      close_atom cenv loc a (fun a ->
            let e = close_exp tenv venv cenv e in
            let v' = new_symbol v in
            let e = close_set_var cenv pos loc v ty v' e in
            let e = LetUnop (v', ty, op, a, e) in
               make_exp loc e)

and close_binop_exp tenv venv cenv pos loc v ty op a1 a2 e =
   let pos = string_pos "close_unop_exp" pos in
   let venv = venv_add venv v ty in
      close_atom cenv loc a1 (fun a1 ->
            close_atom cenv loc a2 (fun a2 ->
                  let e = close_exp tenv venv cenv e in
                  let v' = new_symbol v in
                  let e = close_set_var cenv pos loc v ty v' e in
                  let e = LetBinop (v', ty, op, a1, a2, e) in
                     make_exp loc e))

and close_extcall_exp tenv venv cenv pos loc v ty s b ty' args e =
   let pos = string_pos "close_op_exp" pos in
   let venv = venv_add venv v ty in
      close_atoms cenv loc args (fun args ->
            let e = close_exp tenv venv cenv e in
            let v' = new_symbol v in
            let e = close_set_var cenv pos loc v ty v' e in
            let e = LetExtCall (v', ty, s, b, ty', args, e) in
               make_exp loc e)
(*
 * Tailcall.
 *)
and close_tailcall tenv venv cenv pos loc f args tailop =
   let pos = string_pos "close_tailcall" pos in
      close_atoms cenv loc args (fun args ->
            try
               let { fun_gflag = gflag;
                     fun_uses = uses
                   } = SymbolTable.find cenv.close_funs f
               in
                  if uses = [] then
                     make_exp loc (TailCall (tailop f args))
                  else
                     let f' = new_symbol f in
                     let vars = List.map (fun v -> AtomVar v) uses in
                     let args =
                        (* See comments in close_fun, above, for details. *)
                        match gflag with
                           VarLocalClass ->
                              args @ vars
                         | VarGlobalClass
                         | VarContClass ->
                              vars @ args
                     in
                        make_exp loc (TailCall (tailop f args))
            with
               Not_found ->
                  close_var cenv loc f (fun f ->
                        make_exp loc (TailCall (tailop f args))))

and close_tailcall_normal tenv venv cenv pos loc f args =
   let pos = string_pos "close_tailcall_normal" pos in
      close_tailcall tenv venv cenv pos loc f args (fun f args ->
            TailNormal (f, args))

and close_tailcall_migrate tenv venv cenv pos loc f host args =
   let pos = string_pos "close_tailcall_migrate" pos in
      close_atoms cenv loc host (fun host ->
            close_tailcall tenv venv cenv pos loc f args (fun f args ->
                  TailSysMigrate (f, host, args)))

and close_tailcall_atomic tenv venv cenv pos loc f c args =
   let pos = string_pos "close_tailcall_atomic" pos in
      close_atom cenv loc c (fun c ->
            close_tailcall tenv venv cenv pos loc f args (fun f args ->
                  TailAtomic (f, c, args)))

and close_tailcall_atomic_commit tenv venv cenv pos loc level f args =
   let pos = string_pos "close_tailcall_atomic_commit" pos in
      close_atom cenv loc level (fun level ->
            close_tailcall tenv venv cenv pos loc f args (fun f args ->
                  TailAtomicCommit (level, f, args)))

and close_tailcall_atomic_rollback tenv venv cenv pos loc level c =
   let pos = string_pos "close_tailcall_atomic_rollback" pos in
      close_atom cenv loc level (fun level ->
            close_atom cenv loc c (fun c ->
                  make_exp loc (TailCall (TailAtomicRollback (level, c)))))

and close_tailcall_exp tenv venv cenv pos loc op =
   let pos = string_pos "close_tailcall_exp" pos in
      match op with
         TailNormal (f, args) ->
            close_tailcall_normal tenv venv cenv pos loc f args
       | TailSysMigrate (f, host, args) ->
            close_tailcall_migrate tenv venv cenv pos loc f host args
       | TailAtomic (f, c, args) ->
            close_tailcall_atomic tenv venv cenv pos loc f c args
       | TailAtomicCommit (level, f, args) ->
            close_tailcall_atomic_commit tenv venv cenv pos loc level f args
       | TailAtomicRollback (level, c) ->
            close_tailcall_atomic_rollback tenv venv cenv pos loc level c

(*
 * Malloc.
 *)
and close_malloc_exp tenv venv cenv pos loc v a e =
   let pos = string_pos "close_malloc_exp" pos in
   let ty = TyPointer (TyInt (native_char_precision, false)) in
   let venv = venv_add venv v ty in
      close_atom cenv loc a (fun a ->
            let e = close_exp tenv venv cenv e in
            let v' = new_symbol v in
            let e = close_set_var cenv pos loc v ty v' e in
            let e = LetMalloc (v', a, e) in
               make_exp loc e)

(*
 * Conditional.
 *)
and close_if_exp tenv venv cenv pos loc a e1 e2 =
   let pos = string_pos "close_if_exp" pos in
      close_atom cenv loc a (fun a ->
            let e1 = close_exp tenv venv cenv e1 in
            let e2 = close_exp tenv venv cenv e2 in
               make_exp loc (IfThenElse (a, e1, e2)))

(*
 * Subscripting.
 *)
and close_subscript_exp tenv venv cenv pos loc v1 ty v2 a3 e =
   let pos = string_pos "close_subscript_exp" pos in
   let venv = venv_add venv v1 ty in
      close_var cenv loc v2 (fun v2 ->
            close_atom cenv loc a3 (fun a3 ->
                  let e = close_exp tenv venv cenv e in
                  let v1' = new_symbol v1 in
                  let e = close_set_var cenv pos loc v1 ty v1' e in
                  let e = LetSubscript (v1', ty, v2, a3, e) in
                     make_exp loc e))

and close_set_subscript_exp tenv venv cenv pos loc v1 a2 ty a3 e =
   let pos = string_pos "close_set_subscript_exp" pos in
      close_var cenv loc v1 (fun v1 ->
            close_atom cenv loc a2 (fun a2 ->
                  close_atom cenv loc a3 (fun a3 ->
                        let e = close_exp tenv venv cenv e in
                           make_exp loc (SetSubscript (v1, a2, ty, a3, e)))))

and close_set_global_exp tenv venv cenv pos loc v ty a e =
   let pos = string_pos "close_set_global_exp" pos in
      close_var cenv loc v (fun v ->
            close_atom cenv loc a (fun a ->
                  let e = close_exp tenv venv cenv e in
                     make_exp loc (SetGlobal (v, ty, a, e))))

and close_memcpy_exp tenv venv cenv pos loc v1 a1 v2 a2 a3 e =
   let pos = string_pos "close_memcpy_exp" pos in
      close_var cenv loc v1 (fun v1 ->
            close_var cenv loc v2 (fun v2 ->
                  close_atom cenv loc a1 (fun a1 ->
                        close_atom cenv loc a2 (fun a2 ->
                              close_atom cenv loc a3 (fun a3 ->
                                    make_exp loc (Memcpy (v1, a1, v2, a2, a3, close_exp tenv venv cenv e)))))))

(*
 * Variable operations.
 * LetVar is special: if there is no initializer, and the variable
 * escapes, then the LetVar is omitted.
 *)
and close_var_exp tenv venv cenv pos loc v ty a_opt e =
   let pos = string_pos "close_var_exp" pos in
   let venv = venv_add venv v ty in
      close_atom_opt cenv loc a_opt (fun a_opt ->
            let e = close_exp tenv venv cenv e in
               match a_opt with
                  Some a ->
                     let v' = new_symbol v in
                     let e = close_set_var cenv pos loc v ty v' e in
                     let e = LetAtom (v', ty, a, e) in
                        make_exp loc e
                | None ->
                     if SymbolTable.mem cenv.close_esc v then
                        e
                     else
                        make_exp loc (LetAtom (v, ty, default_atom tenv pos ty, e)))

and close_frame_exp tenv venv cenv pos loc v ty e =
   let pos = string_pos "close_frame_exp" pos in
   let venv = venv_add venv v ty in
   let e = close_exp tenv venv cenv e in
      if SymbolTable.mem cenv.close_esc v then
         e
      else
         make_exp loc (LetAtom (v, ty, default_atom tenv pos ty, e))

and close_set_var_exp tenv venv cenv pos loc v1 ty a2 e =
   let pos = string_pos "close_set_var_exp" pos in
      close_atom cenv loc a2 (fun a2 ->
            let e = close_exp tenv venv cenv e in
            let v1' = new_symbol v1 in
            let e = close_set_var cenv pos loc v1 ty v1' e in
            let e = LetAtom (v1', ty, a2, e) in
               make_exp loc e)

and close_addr_of_var_exp tenv venv cenv pos loc v1 ty v2 e =
   let pos = string_pos "close_addr_of_var_exp" pos in
   let venv = venv_add venv v1 ty in
      close_addr_of_var cenv pos loc v2 (fun v2 ->
            let e = close_exp tenv venv cenv e in
            let v1' = new_symbol v1 in
            let e = close_set_var cenv pos loc v1 ty v1' e in
            let e = LetAtom (v1', ty, AtomVar v2, e) in
               make_exp loc e)

(*
 * Debugging.
 *)
and close_debug_exp tenv venv cenv pos loc info e =
   let pos = string_pos "close_debug_exp" pos in
   let e = close_exp tenv venv cenv e in
      match info with
         DebugString _ ->
            make_exp loc (Debug (info, e))
       | DebugContext (loc, vars) ->
            let rec collect vars' = function
               (v1, ty, v2) :: vars ->
                  close_var cenv loc v2 (fun v2 ->
                        collect ((v1, ty, v2) :: vars') vars)
             | [] ->
                  make_exp loc (Debug (DebugContext (loc, List.rev vars'), e))
            in
               collect [] vars

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Closure conversion.
 *)
let close_prog prog =
   let { prog_body = body;
         prog_types = tenv;
         prog_globals = globdefs;
	 prog_decls = decls;
         prog_export = export;
         prog_import = import
       } = prog
   in

   (* Get all the defs in the program *)
   let fenv, _, globals = defs_exp tenv FunTable.empty [] EscSet.empty DefTable.empty body in

   (* Add the globals already defined *)
   let globals =
      SymbolTable.fold (fun globals v (ty, _, _) ->
            DefTable.add globals v ty) globals globdefs
   in
   let globals =
      SymbolTable.fold (fun globals v { import_type = ty } ->
            DefTable.add globals v ty) globals import
   in

   (* Build the dataflow graph *)
   let globals_list = SymbolTable.fold (fun l v _ -> v :: l) [] globals in
   let globals_list = SymbolTable.fold (fun l v _ -> v :: l) globals_list import in
   let dflow, _, _ = dflow_exp fenv [] SymbolTable.empty DefSet.empty CallTable.empty UseSet.empty body in
   let dflow = dflow_solve globals_list dflow in

   (* Construct the function frame descriptions *)
   let frame = frame_all tenv import globals dflow in
   let pos = string_pos "Frame" (var_exp_pos (Symbol.add "frame")) in
   let esc = frame_esc pos frame globals dflow in
   let info = rebuild_dflow esc dflow in

   (* Add the frames *)
   let frames =
      FunTable.fold (fun frames _ { fun_info = info } ->
            match info with
               GlobalInfo (frame, defs, _) ->
                  let fields =
                     SymbolTable.fold (fun fields _ (v, ty) ->
                           SymbolTable.add fields v [v, ty]) SymbolTable.empty defs
                  in
                     SymbolTable.add frames frame fields
             | LocalInfo ->
                  frames) SymbolTable.empty info
   in

   (* Close the expressions *)
   let cenv = { close_esc = esc; close_funs = info } in
   let body = close_exp tenv globals cenv body in
   let prog =
      { prog with prog_frames = frames;
                  prog_body = body
      }
   in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
