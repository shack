(*
 * Fc_semant exception handling.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fc_ir
open Fc_ir_ds
open Fc_ir_exn
open Fc_ir_print

(*
 * Location of an exception.
 *)
type item =
   Exp    of exp
 | Symbol of var
 | String of string

 | AirExp of Fc_air.exp

type pos = item Position.pos

(*
 * Bogus intermediate code.
 *)
exception IRException of pos * ir_error

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the source location for an exception.
 *)
let string_loc = bogus_loc "<Fc_ir_pos>"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_exp e
    | AirExp e ->
         Fc_air_ds.loc_of_exp e
    | Symbol _
    | String _ ->
         string_loc

(*
 * Print debugging info.
 *)
let rec pp_print_value buf x =
   match x with
      Exp e ->
         pp_print_expr buf e

    | Symbol v ->
         pp_print_symbol buf v

    | String s ->
         pp_print_string buf s

    | AirExp e ->
         Fc_air_print.pp_print_expr buf e

(************************************************************************
 * CONSTRUCTION
 ************************************************************************)

module type PosSig =
sig
   val loc_pos : loc -> pos

   val exp_pos : exp -> pos
   val var_exp_pos : var -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos

   val air_exp_pos    : Fc_air.exp -> pos

   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let exp_pos e = base_pos (Exp e)
   let var_exp_pos v = base_pos (Symbol v)
   let string_exp_pos s = base_pos (String s)
   let var_pos = symbol_pos

   let air_exp_pos p = base_pos (AirExp p)
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
