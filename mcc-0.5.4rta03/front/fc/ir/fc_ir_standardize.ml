(*
 * Rename all the non-global variables in a program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_ir
open Fc_ir_ds
open Fc_ir_exn

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = var SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

let rec env_add_vars env vars vars' =
   match vars, vars' with
      v :: vars, v' :: vars' ->
         env_add_vars (env_add env v v') vars vars'
    | [], [] ->
         env
    | _ ->
         raise (Invalid_argument "Fc_ir_standardize.env_add_vars")

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

(*
 * Check is a variable is defined.
 *)
let env_mem = SymbolTable.mem

(*
 * A variable environment has a separate map for frame fields.
 *)
type venv =
   { venv_vars : env;
     venv_frames : var list SymbolListTable.t
   }

let venv_empty =
   { venv_vars = env_empty;
     venv_frames = SymbolListTable.empty
   }

let venv_add venv v v' =
   { venv with venv_vars = env_add venv.venv_vars v v' }

let venv_lookup venv v =
   env_lookup venv.venv_vars v

let venv_mem venv v =
   env_mem venv.venv_vars v

(*
 * Frame elements are triples.
 *)
let venv_add_frame venv l l' =
   { venv with venv_frames = SymbolListTable.add venv.venv_frames l l' }

let venv_lookup_frame venv v =
   try SymbolListTable.find venv.venv_frames v with
      Not_found ->
         v

let venv_lookup_frame3 venv (frame, field, subfield) =
   match venv_lookup_frame venv [frame; field; subfield] with
      [frame; field; subfield] ->
         frame, field, subfield
    | _ ->
         raise (Invalid_argument "venv_lookup_frame3")

let venv_lookup_field venv frame field =
   match venv_lookup_frame venv [frame; field] with
      [_; field] ->
         field
    | _ ->
         raise (Invalid_argument "venv_lookup_field")

let venv_lookup_subfield venv frame field subfield =
   match venv_lookup_frame venv [frame; field; subfield] with
      [_; _; subfield] ->
         subfield
    | _ ->
         raise (Invalid_argument "venv_lookup_subfield")

(*
 * Type environment has two parts, one for
 * ids, and one for vars.
 *)
type tenv =
   { tenv_types : env;
     tenv_vars : env;
     tenv_fvars : env
   }

(*
 * Type operations.
 *)
let tenv_empty =
   { tenv_types = env_empty;
     tenv_vars = env_empty;
     tenv_fvars = env_empty
   }

let tenv_push_scope tenv =
   { tenv with tenv_vars = env_empty;
               tenv_fvars = env_empty
   }

let tenv_pop_scope tenv tenv' =
   { tenv with tenv_fvars = tenv'.tenv_fvars }

let tenv_add_type tenv v v' =
   { tenv with tenv_types = env_add tenv.tenv_types v v' }

let tenv_add_var tenv v v' =
   { tenv with tenv_vars = env_add tenv.tenv_vars v v' }

let tenv_add_vars tenv vars vars' =
   { tenv with tenv_vars = env_add_vars tenv.tenv_vars vars vars' }

let tenv_lookup_var tenv v =
   try
      let v' = SymbolTable.find tenv.tenv_vars v in
         tenv, v'
   with
      Not_found ->
         try
            let v' = SymbolTable.find tenv.tenv_fvars v in
               tenv, v'
         with
            Not_found ->
               let v' = new_symbol v in
               let tenv = { tenv with tenv_fvars = env_add tenv.tenv_fvars v v' } in
                  tenv, v'

let tenv_lookup_type tenv v =
   env_lookup tenv.tenv_types v

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv ty =
   match ty with
      TyUnit _
    | TyTag
    | TyInt _
    | TyFloat _
    | TyDelayed
    | TyRawData _ ->
         tenv, ty
    | TyPointer ty ->
         let tenv, ty = standardize_type tenv ty in
            tenv, TyPointer ty
    | TyFun (ty_vars, ty_res) ->
         let tenv, ty_vars = standardize_type_list tenv ty_vars in
         let tenv, ty_res = standardize_type tenv ty_res in
            tenv, TyFun (ty_vars, ty_res)
    | TyVar v ->
         let tenv, v = tenv_lookup_var tenv v in
            tenv, TyVar v
    | TyLambda (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, ty = standardize_type tenv ty in
            tenv, TyLambda (vars', ty)
    | TyAll (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, ty = standardize_type tenv ty in
            tenv, TyAll (vars', ty)
    | TyApply (v, tyl) ->
         let tenv, tyl = standardize_type_list tenv tyl in
            tenv, TyApply (tenv_lookup_type tenv v, tyl)
    | TyFrame v ->
         tenv, TyFrame (tenv_lookup_type tenv v)

and standardize_type_list tenv tyl =
   let tenv, tyl =
      List.fold_left (fun (tenv, ty_vars) ty ->
            let tenv, ty = standardize_type tenv ty in
            let ty_vars = ty :: ty_vars in
               tenv, ty_vars) (tenv, []) tyl
   in
      tenv, List.rev tyl

(*
 * Be careful to maintain scoping.
 *)
let standardize_type tenv ty =
   let tenv', ty = standardize_type tenv ty in
   let tenv = tenv_pop_scope tenv tenv' in
      tenv, ty

let standardize_type_list tenv tyl =
   let tenv', tyl = standardize_type_list tenv tyl in
   let tenv = tenv_pop_scope tenv tenv' in
      tenv, tyl

(*
 * Rename the vars in an atom.
 *)
let standardize_atom tenv venv atom =
   match atom with
      AtomUnit _
    | AtomInt _
    | AtomFloat _ ->
         tenv, atom
    | AtomNil ty ->
         let tenv, ty = standardize_type tenv ty in
            tenv, AtomNil ty
    | AtomVar v ->
         tenv, AtomVar (venv_lookup venv v)
    | AtomLabel label ->
         tenv, AtomLabel (venv_lookup_frame3 venv label)

let standardize_atom_opt tenv venv a =
   match a with
      Some a ->
         let tenv, a = standardize_atom tenv venv a in
            tenv, Some a
    | None ->
         tenv, None

let standardize_atoms tenv venv atoms =
   let tenv, atoms =
      List.fold_left (fun (tenv, atoms) a ->
            let tenv, a = standardize_atom tenv venv a in
            let atoms = a :: atoms in
               tenv, atoms) (tenv, []) atoms
   in
      tenv, List.rev atoms

(*
 * Rename all the vars in the expr.
 *)
let rec standardize_expr tenv venv e =
   let loc = loc_of_exp e in
   let tenv, venv, e = standardize_expr_core tenv venv (dest_exp_core e) in
      tenv, venv, make_exp loc e

and standardize_expr_core tenv venv e =
   match e with
      LetFuns (funs, e) ->
         standardize_funs_expr tenv venv funs e
    | LetAtom (v, ty, a, e) ->
         standardize_atom_expr tenv venv v ty a e
    | LetUnop (v, ty, op, a, e) ->
         standardize_unop_expr tenv venv v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         standardize_binop_expr tenv venv v ty op a1 a2 e
    | LetExtCall (v, ty, s, b, ty', args, e) ->
         standardize_extcall_expr tenv venv v ty s b ty' args e
    | LetClosure (v, f, args, e) ->
         standardize_closure_expr tenv venv v f args e
    | TailCall op ->
         standardize_tailcall_expr tenv venv op
    | IfThenElse (a, e1, e2) ->
         standardize_if_expr tenv venv a e1 e2
    | LetVar (v, ty, a_opt, e) ->
         standardize_var_expr tenv venv v ty a_opt e
    | LetFrame (v, ty, e) ->
         standardize_frame_expr tenv venv v ty e
    | SetVar (v, ty, a, e) ->
         standardize_set_var_expr tenv venv v ty a e
    | LetAddrOfVar (v1, ty, v2, e) ->
         standardize_addr_of_var_expr tenv venv v1 ty v2 e
    | SetSubscript (a1, a2, ty, a3, e) ->
         standardize_set_subscript_expr tenv venv a1 a2 ty a3 e
    | SetGlobal (v, ty, a, e) ->
         standardize_set_global_expr tenv venv v ty a e
    | LetSubscript (v, ty, a1, a2, e) ->
         standardize_subscript_expr tenv venv v ty a1 a2 e
    | Memcpy (v1, a1, v2, a2, a3, e) ->
         standardize_memcpy_expr tenv venv v1 a1 v2 a2 a3 e
    | LetProject (v1, ty, v2, label, e) ->
         standardize_project_expr tenv venv v1 ty v2 label e
    | LetAddrOfProject (v1, ty, v2, label, e) ->
         standardize_addr_of_project_expr tenv venv v1 ty v2 label e
    | SetProject (v1, label, ty, a4, e) ->
         standardize_set_project_expr tenv venv v1 label ty a4 e
    | LetMalloc (v, a, e) ->
         standardize_malloc_expr tenv venv v a e
    | Debug (info, e) ->
         standardize_debug_expr tenv venv info e

(*
 * Standardize a fun.
 * Global funs begin a new type var scope.
 *)
and standardize_fun_expr tenv venv (f, line, gflag, f_ty, vars, body) =
   (* Rename the function *)
   let f' = venv_lookup venv f in

   (* Rename the type vars *)
   let tenv' =
      match gflag with
         VarGlobalClass ->
            tenv_push_scope tenv
       | VarContClass
       | VarLocalClass ->
            tenv
   in

   (* Rename the vars *)
   let venv', vars' =
      List.fold_left (fun (venv, vars') v ->
            let v' = new_symbol v in
            let venv = venv_add venv v v' in
               venv, v' :: vars') (venv, []) vars
   in
   let vars' = List.rev vars' in

   (* Rename the body *)
   let tenv', _, body' = standardize_expr tenv' venv' body in
   let tenv', f_ty = standardize_type tenv' f_ty in
      f', line, gflag, f_ty, vars', body'

and standardize_funs tenv venv funs =
   (* Rename the fun names *)
   let venv =
      List.fold_left (fun venv (f, _, _, _, _, _) ->
            if venv_mem venv f then
               venv
            else
               let f' = new_symbol f in
                  venv_add venv f f') venv funs
   in
      (* Rename the function's vars and body *)
      venv, List.map (standardize_fun_expr tenv venv) funs

and standardize_funs_expr tenv venv funs e =
   (* Rename the funs *)
   let venv, funs = standardize_funs tenv venv funs in

   (* Rename the rest *)
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, LetFuns (funs, e)

(*
 * Standardize an operator.
 *)
and standardize_atom_expr tenv venv v ty a e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetAtom (v', ty, a, e)

and standardize_unop_expr tenv venv v ty op a e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetUnop (v', ty, op, a, e)

and standardize_binop_expr tenv venv v ty op a1 a2 e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, a1 = standardize_atom tenv venv a1 in
   let tenv, a2 = standardize_atom tenv venv a2 in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetBinop (v', ty, op, a1, a2, e)

and standardize_extcall_expr tenv venv v ty s b ty' args e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, args = standardize_atoms tenv venv args in
   let tenv, ty = standardize_type tenv ty in
   let tenv, ty' = standardize_type tenv ty' in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetExtCall (v', ty, s, b, ty', args, e)

(*
 * Closure is a lot like LetOp.
 *)
and standardize_closure_expr tenv venv v f args e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let f = venv_lookup venv f in
   let tenv, args = standardize_atoms tenv venv args in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetClosure (v', f, args, e)

(*
 * LetVar.
 *)
and standardize_var_expr tenv venv v ty a_opt e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, a_opt = standardize_atom_opt tenv venv a_opt in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetVar (v', ty, a_opt, e)

and standardize_frame_expr tenv venv v ty e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetFrame (v', ty, e)

(*
 * Tail call.
 *)
and standardize_tailcall_normal tenv venv f args =
   let f = venv_lookup venv f in
   let _, args = standardize_atoms tenv venv args in
      tenv, venv, TailCall (TailNormal (f, args))

and standardize_tailcall_migrate tenv venv f host args =
   let f = venv_lookup venv f in
   let _, host = standardize_atoms tenv venv host in
   let _, args = standardize_atoms tenv venv args in
      tenv, venv, TailCall (TailSysMigrate (f, host, args))

and standardize_tailcall_atomic tenv venv f c args =
   let f = venv_lookup venv f in
   let _, c = standardize_atom tenv venv c in
   let _, args = standardize_atoms tenv venv args in
      tenv, venv, TailCall (TailAtomic (f, c, args))

and standardize_tailcall_atomic_commit tenv venv level f args =
   let _, level = standardize_atom tenv venv level in
   let f = venv_lookup venv f in
   let _, args = standardize_atoms tenv venv args in
      tenv, venv, TailCall (TailAtomicCommit (level, f, args))

and standardize_tailcall_atomic_rollback tenv venv level c =
   let _, level = standardize_atom tenv venv level in
   let _, c = standardize_atom tenv venv c in
      tenv, venv, TailCall (TailAtomicRollback (level, c))

and standardize_tailcall_expr tenv venv op =
   match op with
      TailNormal (f, args) ->
         standardize_tailcall_normal tenv venv f args
    | TailSysMigrate (f, host, args) ->
         standardize_tailcall_migrate tenv venv f host args
    | TailAtomic (f, c, args) ->
         standardize_tailcall_atomic tenv venv f c args
    | TailAtomicCommit (level, f, args) ->
         standardize_tailcall_atomic_commit tenv venv level f args
    | TailAtomicRollback (level, c) ->
         standardize_tailcall_atomic_rollback tenv venv level c

(*
 * Conditional expression.
 *)
and standardize_if_expr tenv venv a e1 e2 =
   let tenv, a = standardize_atom tenv venv a in
   let tenv, _, e1 = standardize_expr tenv venv e1 in
   let tenv, _, e2 = standardize_expr tenv venv e2 in
      tenv, venv, IfThenElse (a, e1, e2)

(*
 * Set a variable.
 *)
and standardize_set_var_expr tenv venv v ty a e =
   let v = venv_lookup venv v in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, SetVar (v, ty, a, e)

(*
 * Get address of a variable.
 *)
and standardize_addr_of_var_expr tenv venv v1 ty v2 e =
   let v1' = new_symbol v1 in
   let v2 = venv_lookup venv v2 in
   let venv' = venv_add venv v1 v1' in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetAddrOfVar (v1', ty, v2, e)

(*
 * Array operations.
 *)
and standardize_subscript_expr tenv venv v ty v2 a3 e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let v2 = venv_lookup venv v2 in
   let tenv, a3 = standardize_atom tenv venv a3 in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetSubscript (v', ty, v2, a3, e)

and standardize_set_subscript_expr tenv venv v a1 ty a2 e =
   let v = venv_lookup venv v in
   let tenv, a1 = standardize_atom tenv venv a1 in
   let tenv, a2 = standardize_atom tenv venv a2 in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, SetSubscript (v, a1, ty, a2, e)

and standardize_set_global_expr tenv venv v ty a e =
   let v = venv_lookup venv v in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, SetGlobal (v, ty, a, e)

(*
 * Frame operations.
 *)
and standardize_project_expr tenv venv v1 ty v2 label e =
   let v1' = new_symbol v1 in
   let venv' = venv_add venv v1 v1' in
   let v2 = venv_lookup venv v2 in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
   let label = venv_lookup_frame3 venv label in
      tenv, venv, LetProject (v1', ty, v2, label, e)

and standardize_addr_of_project_expr tenv venv v1 ty v2 label e =
   let v1' = new_symbol v1 in
   let venv' = venv_add venv v1 v1' in
   let v2 = venv_lookup venv v2 in
   let tenv, ty = standardize_type tenv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
   let label = venv_lookup_frame3 venv label in
      tenv, venv, LetAddrOfProject (v1', ty, v2, label, e)

and standardize_set_project_expr tenv venv v1 label ty a4 e =
   let tenv, ty = standardize_type tenv ty in
   let v1 = venv_lookup venv v1 in
   let tenv, a4 = standardize_atom tenv venv a4 in
   let tenv, venv, e = standardize_expr tenv venv e in
   let label = venv_lookup_frame3 venv label in
      tenv, venv, SetProject (v1, label, ty, a4, e)

(*
 * Memory operations.
 *)
and standardize_malloc_expr tenv venv v a e =
   let v' = new_symbol v in
   let venv' = venv_add venv v v' in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetMalloc (v', a, e)

and standardize_memcpy_expr tenv venv v1 a1 v2 a2 a3 e =
   let v1 = venv_lookup venv v1 in
   let v2 = venv_lookup venv v2 in
   let tenv, a1 = standardize_atom tenv venv a1 in
   let tenv, a2 = standardize_atom tenv venv a2 in
   let tenv, a3 = standardize_atom tenv venv a3 in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, Memcpy (v1, a1, v2, a2, a3, e)

(*
 * Documentation string.
 *)
and standardize_debug_vars tenv venv info =
   List.fold_left (fun (tenv, info) (v1, ty, v2) ->
         let tenv, ty = standardize_type tenv ty in
         let v2 = venv_lookup venv v2 in
         let info = (v1, ty, v2) :: info in
            tenv, info) (tenv, []) info

and standardize_debug_expr tenv venv info e =
   let tenv, info =
      match info with
         DebugString _ ->
            tenv, info
       | DebugContext (line, vars) ->
            let tenv, vars = standardize_debug_vars tenv venv vars in
               tenv, DebugContext (line, vars)
   in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, Debug (info, e)

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Rename all vars in the program.
 *)
let standardize_prog prog =
   let { prog_file = file;
         prog_globals = globals;
         prog_decls = decls;
         prog_export = export;
         prog_import = import;
         prog_types = types;
         prog_frames = frames;
         prog_funs = funs;
         prog_body = body
       } = prog
   in

   (* Rename all the types *)
   let tenv =
      SymbolTable.fold (fun tenv v _ ->
            tenv_add_type tenv v (new_symbol v)) tenv_empty types
   in
   let tenv, types =
      SymbolTable.fold (fun (tenv, types) v ty ->
            let v' = tenv_lookup_type tenv v in
            let tenv, ty = standardize_type tenv ty in
            let types = SymbolTable.add types v' ty in
               tenv, types) (tenv, SymbolTable.empty) types
   in

   (* Initial environment renames all the top-level vars *)
   let venv = venv_empty in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v (new_symbol v)) venv globals
   in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v (new_symbol v)) venv import
   in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v (new_symbol v)) venv decls
   in
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v (new_symbol v)) venv funs
   in

   (* Rename all the fields *)
   let tenv, venv =
      SymbolTable.fold (fun (tenv, venv) v1 frame ->
            let v1' = new_symbol v1 in
            let tenv = tenv_add_type tenv v1 v1' in
            let venv =
               SymbolTable.fold (fun venv v2 vars ->
                     let v2' = new_symbol v2 in
                     let venv = venv_add_frame venv [v1; v2] [v1'; v2'] in
                     let venv =
                        List.fold_left (fun venv (v3, _) ->
                              let v3' = new_symbol v3 in
                                 venv_add_frame venv [v1; v2; v3] [v1'; v2'; v3']) venv vars
                     in
                        venv) venv frame
            in
               tenv, venv) (tenv, venv) frames
   in

   (* Adjust all the funs *)
   let funs =
      SymbolTable.fold (fun funs f (line, gflag, ty, vars, e) ->
            let f, line, gflag, ty, vars, e =
               standardize_fun_expr tenv venv (f, line, gflag, ty, vars, e)
            in
               SymbolTable.add funs f (line, gflag, ty, vars, e)) SymbolTable.empty funs
   in

   (* Adjust the body *)
   let _, venv, body = standardize_expr tenv venv body in

   (* Adjust all the frames *)
   let tenv, frames =
      SymbolTable.fold (fun (tenv, frames) v1 fields ->
            let v1' = tenv_lookup_type tenv v1 in
            let tenv, fields =
               SymbolTable.fold (fun (tenv, fields) v2 subfields ->
                     let v2' = venv_lookup_field venv v1 v2 in
                     let tenv, subfields =
                        List.fold_left (fun (tenv, subfields) (v3, ty) ->
                              let v3' = venv_lookup_subfield venv v1 v2 v3 in
                              let tenv, ty = standardize_type tenv ty in
                              let subfields = (v3', ty) :: subfields in
                                 tenv, subfields) (tenv, []) subfields
                     in
                     let fields = SymbolTable.add fields v2' (List.rev subfields) in
                        tenv, fields) (tenv, SymbolTable.empty) fields
            in
            let frames = SymbolTable.add frames v1' fields in
               tenv, frames) (tenv, SymbolTable.empty) frames
   in

   (* Adjust all the globals *)
   let tenv, globals =
      SymbolTable.fold (fun (tenv, globals) v (ty, volp, init) ->
            let v' = venv_lookup venv v in
            let tenv, ty = standardize_type tenv ty in
            let globals = SymbolTable.add globals v' (ty, volp, init) in
               tenv, globals) (tenv, SymbolTable.empty) globals
   in
   let tenv, decls =
      SymbolTable.fold (fun (tenv, decls) v ty ->
            let v' = venv_lookup venv v in
            let tenv, ty = standardize_type tenv ty in
            let decls = SymbolTable.add decls v' ty in
               tenv, decls) (tenv, SymbolTable.empty) decls
   in
   let tenv, import =
      SymbolTable.fold (fun (tenv, import) v info ->
            let v' = venv_lookup venv v in
            let tenv, ty = standardize_type tenv info.import_type in
            let info = { info with import_type = ty } in
            let import = SymbolTable.add import v' info in
               tenv, import) (tenv, SymbolTable.empty) import
   in
   let tenv, export =
      SymbolTable.fold (fun (tenv, export) v info ->
            let v' = venv_lookup venv v in
            let tenv, ty = standardize_type tenv info.export_type in
            let info = { info with export_type = ty } in
            let export = SymbolTable.add export v' info in
               tenv, export) (tenv, SymbolTable.empty) export
   in
      { prog_file = file;
        prog_globals = globals;
        prog_decls = decls;
        prog_export = export;
        prog_import = import;
        prog_types = types;
        prog_frames = frames;
        prog_funs = funs;
        prog_body = body
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
