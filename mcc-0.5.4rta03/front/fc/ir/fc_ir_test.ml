(*
 * FIR test function.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Debug
open Fc_compile
open Fir_marshal

(*
 * Arguments.
 *)
let debug_eval_ir = ref false

let usage =
   Printf.sprintf "usage: compile [options] [files...] [-- <program arguments>]"

(*
 * Program arguments.
 *)
let rev_argv = ref []
let prog = ref None
let output_fir = ref None

(*
 * Output object FIR.
 *)
let output_prog prog =
   match !output_fir with
      None ->
         ()
    | Some s ->
         let out = open_out_bin s in
            ChannelMarshal.marshal_prog out prog;
            close_out out

(*
 * Compile and save the program.
 *)
let compile1 name =
   let prog' = Fc_ir_exn.catch compile name in
   let _ =
      match !output_fir with
         None ->
            ()
       | Some s ->
            let out = open_out_bin s in
            let prog' = ChannelMarshal.marshal_of_fir_prog prog' in
               ChannelMarshal.marshal_prog out prog';
               close_out out
   in
      prog := Some prog'

let compile2 () =
   if debug debug_eval_ir then
      match !prog with
         Some prog ->
            let argv = List.rev !rev_argv in
               eprintf "evaluating: argc=%d argv={ " (List.length argv);
               List.iter (eprintf "%s; ") argv;
               eprintf "}%t" eflush;
               Fir_eval.eval_main prog argv

       | None ->
            ()

(*
 * Call the main program.
 *)
let spec =
   ["-o", Arg.String (fun s -> output_fir := Some s),    "output binary fir code";
    "-O",             Arg.Set Fir_state.optimize_flag,   "optimize fir code";
    "-g",             Arg.Set Fir_state.debug_stab,      "add debugging info";
    "-pp_print_ast buf",     Arg.Set Fir_state.debug_print_ast, "print expressions";
    "-pp_print_ir buf",      Arg.Set Fir_state.debug_print_ir,  "print intermediate code";
    "-check_ir",      Arg.Set Fir_state.debug_check_ir,  "typecheck intermediate code";
    "-eval",          Arg.Set Fir_state.debug_eval_ir,   "evaluate intermediate code";
    "-debug_parith",  Arg.Set Fc_ir_state.debug_parith,  "trace pointer arithmetic";
    "-debug_pos",     Arg.Set Fir_state.debug_pos,       "print position information";
    "-trace_pos",     Arg.Set Fir_state.debug_trace_pos, "trace function calls";
    "-debug_unify",   Arg.Set Fir_state.debug_unify,     "trace unification";
    "-debug_infer",   Arg.Set Fir_state.debug_infer,     "debug fir type inference";
    "-debug_closure", Arg.Set Fc_ir_state.debug_closure, "print closure equations";
    "--",             Arg.Rest (fun s -> rev_argv := s :: !rev_argv), "program arguments"]

let _ =
   Arg.parse spec compile1 usage;
   Fc_ir_exn.catch compile2 ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
