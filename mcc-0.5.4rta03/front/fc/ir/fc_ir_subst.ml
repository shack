(*
 * Define a type substitution.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Fc_ir

type subst = ty SymbolTable.t

(*
 * Type substitution.
 *)
let subst_empty = SymbolTable.empty

let subst_add = SymbolTable.add

let subst_remove = SymbolTable.remove

let subst_lookup = SymbolTable.find

let rec subst_type subst ty =
   match ty with
      TyUnit _
    | TyTag
    | TyInt _
    | TyFloat _
    | TyDelayed
    | TyRawData _
    | TyFrame _ ->
         ty
    | TyFun (ty_vars, ty_res) ->
         let ty_vars = List.map (subst_type subst) ty_vars in
         let ty_res = subst_type subst ty_res in
            TyFun (ty_vars, ty_res)
    | TyPointer ty ->
         let ty = subst_type subst ty in
            TyPointer (ty)
    | TyVar v ->
         (try subst_type subst (subst_lookup subst v) with
             Not_found ->
                ty)
    | TyApply (v, tyl) ->
         TyApply (v, List.map (subst_type subst) tyl)
    | TyLambda (vars, ty) ->
         let substatust = List.fold_left subst_remove subst vars in
            TyLambda (vars, subst_type subst ty)
    | TyAll (vars, ty) ->
         let substatust = List.fold_left subst_remove subst vars in
            TyAll (vars, subst_type subst ty)

and subst_ty_opt subst = function
   Some ty -> Some (subst_type subst ty)
 | None -> None

and subst_enum_fields subst fields =
   List.map (fun (v, ty_opt, i) ->
         let ty_opt = subst_ty_opt subst ty_opt in
            v, ty_opt, i) fields

and subst_struct_fields subst fields =
   List.map (fun (v, ty, i_opt) ->
         let ty = subst_type subst ty in
            v, ty, i_opt) fields

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
