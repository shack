(*
 * Convert AIR->IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Fc_ir
open Fc_ir_print

module Build = Fc_ir_air.MakeBuild (Fc_air_compile.AIRFrame)

let debug_prog name prog =
   if debug Fir_state.debug_print_ir then
      debug_prog name prog

let compile prog =
   (* Build the IR from the AIR *)
   let prog = Build.build_prog prog in
   let _ = debug_prog "build" prog in

   (* Optimizations to make frame allocation more efficient *)
   let prog = Fc_ir_dead.dead_prog prog in
   let _ = debug_prog "dead" prog in

   (* Do a closure conversion and convert to a program *)
   let prog = Fc_ir_closure.close_prog prog in
   let _ = debug_prog "closure" prog in
   let prog = Fc_ir_prog.prog_prog prog in
   let _ = debug_prog "prog" prog in

   (* Pointer-arithmetic elimination *)
   let prog = Fc_ir_pointer.ptr_prog prog in
   let _ = debug_prog "pointer" prog in
   let prog = Fc_ir_inline.inline_prog prog in
   let _ = debug_prog "inline" prog in
   let prog = Fc_ir_dead.dead_prog prog in
   let _ = debug_prog "dead" prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
