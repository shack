(*
 * Convert AIR->IR.
 * This step does a CPS conversion.
 * All LetApply/Return and Try/Raise are eliminated,
 * Every function gets two extra arguments: a
 * continuation, and an exception handler.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Fc_air_frame

open Fc_ir
open Fc_ir_ds
open Fc_ir_env
open Fc_ir_exn
open Fc_ir_pos
open Fc_ir_type
open Fc_ir_standardize

module Pos = MakePos (struct let name = "Fc_ir_air" end)
open Pos

module type MakeIRSig =
sig
   val build_prog : Fc_air.prog -> Fc_ir.prog
end

module MakeBuild (Frame : AIRFrameSig) : MakeIRSig =
struct
   (************************************************************************
    * UTILITIES
    ************************************************************************)

   (*
    * Default name of the continuation argument.
    *)
   let exit_sym = Symbol.add "exit"
   let exnh_sym = Symbol.add "exnh"

   let zero = AtomInt (Rawint.of_int Rawint.Int32 true 0)
   let atom_of_int i = AtomInt (Rawint.of_int Rawint.Int32 true i)

   let ty_int = TyInt (Rawint.Int32, true)

   (*
    * Table of resturn functions.
    *)
   type renv =
      { renv_cps : var SymbolTable.t;
        renv_local : var SymbolTable.t
      }

   let renv_empty =
      { renv_cps = SymbolTable.empty;
        renv_local = SymbolTable.empty
      }

   let renv_add_cps renv v v' =
      { renv with renv_cps = SymbolTable.add renv.renv_cps v v' }

   let renv_add_fun renv v v' =
      { renv with renv_local = SymbolTable.add renv.renv_local v v' }

   let renv_lookup_cps renv pos f =
      try SymbolTable.find renv.renv_cps f with
         Not_found ->
            raise (IRException (pos, UnboundVar f))

   let renv_lookup_fun renv f =
      SymbolTable.find renv.renv_local f

   (************************************************************************
    * TYPES
    ************************************************************************)

   (*
    * Type destructors.
    *)
   let rec dest_fun_type pos ty =
      let pos = string_pos "dest_fun_type" pos in
         match ty with
            Fc_air.TyFun (ty_vars, ty_res) ->
               [], ty_vars, ty_res
          | Fc_air.TyAll (vars, ty) ->
               let vars', ty_vars, ty_res = dest_fun_type pos ty in
                  vars @ vars', ty_vars, ty_res
          | Fc_air.TyPointer ty ->
               dest_fun_type pos ty
          | _ ->
               raise (IRException (pos, StringError "not a function type"))

   (*
    * This is the type of an exception handler.
    *)
   let ty_exn = TyFun ([TyPointer TyTag], TyUnit 0)

   (*
    * Function transformation.
    * Functions get a continuation argument.
    *)
   let rec cps_type tenv pos ty =
      let pos = string_pos "cps_type" pos in
         match ty with
            Fc_air.TyUnit n ->
               TyUnit n
          | Fc_air.TyTag ->
               TyTag
          | Fc_air.TyInt (pre, signed) ->
               TyInt (pre, signed)
          | Fc_air.TyFloat pre ->
               TyFloat pre
          | Fc_air.TyApply (v, tyl) ->
               TyApply (v, List.map (cps_type tenv pos) tyl)
          | Fc_air.TyVar v ->
               TyVar v
          | Fc_air.TyPointer ty
          | Fc_air.TyRef ty ->
               TyPointer (cps_type tenv pos ty)
          | Fc_air.TyTuple _
          | Fc_air.TyElide _
          | Fc_air.TyEnum _
          | Fc_air.TyUEnum _
          | Fc_air.TyStruct _ ->
               TyRawData (Frame.sizeof_const tenv ty)
          | Fc_air.TyArray (_, Fc_air.ArrayInt _, Fc_air.ArrayInt _) ->
               TyRawData (Frame.sizeof_const tenv ty)
          | Fc_air.TyArray (ty, _, _) ->
	       TyPointer (cps_type tenv pos ty)
          | Fc_air.TyFun (ty_args, ty_res) ->
               let ty_args = List.map (cps_type tenv pos) ty_args in
               let ty_res = cps_type tenv pos ty_res in
               let ty_cont = TyPointer (TyFun ([ty_res], TyUnit 0)) in
               let ty_args = ty_args @ [ty_cont; ty_exn] in
                  TyFun (ty_args, TyUnit 0)
          | Fc_air.TyLambda (vars, ty) ->
               TyLambda (vars, cps_type tenv pos ty)
          | Fc_air.TyAll (vars, ty) ->
               TyAll (vars, cps_type tenv pos ty)
          | Fc_air.TyDelayed ->
               raise (IRException (pos, StringError "unexpected TyDelayed"))
          | Fc_air.TyField _ ->
               raise (IRException (pos, StringError "unexpected TyField"))
          | Fc_air.TyZero ->
               raise (IRException (pos, StringError "unexpected TyZero"))

   (*
    * This is mostly the same, but we don't
    * change the outermost fun.
    *)
   let rec cps_notype tenv pos ty =
      let pos = string_pos "cps_type" pos in
         match ty with
            Fc_air.TyFun (ty_args, ty_res) ->
               let ty_args = List.map (cps_type tenv pos) ty_args in
               let ty_res = cps_type tenv pos ty_res in
                  TyFun (ty_args, ty_res)
          | _ ->
               cps_type tenv pos ty

   (*
    * This is the wrapper for local functions.
    *)
   let cps_local_type tenv pos ty =
      let pos = string_pos "cps_local_type" pos in
      let vars, ty_vars, ty_res = dest_fun_type pos ty in
      let ty_vars = List.map (cps_type tenv pos) ty_vars in
      let ty_res = cps_type tenv pos ty_res in
      let ty_cont = TyPointer (TyFun ([ty_res], TyUnit 0)) in
      let ty_vars = ty_vars @ [ty_cont] in
         TyFun (ty_vars, TyUnit 0)

   (************************************************************************
    * ATOMS
    ************************************************************************)

   (*
    * Atom conversion.
    * All we do really is convert the type
    * in the AtomNil.
    *)
   let cps_atom tenv pos a =
      let pos = string_pos "cps_atom" pos in
         match a with
            Fc_air.AtomUnit (n, i) ->
               AtomUnit (n, i)
          | Fc_air.AtomInt i ->
               AtomInt i
          | Fc_air.AtomFloat x ->
               AtomFloat x
          | Fc_air.AtomNil ty ->
               AtomNil (cps_type tenv pos ty)
          | Fc_air.AtomVar v ->
               AtomVar v

   let cps_atom_opt tenv pos = function
      Some a -> Some (cps_atom tenv pos a)
    | None -> None

   let cps_atoms tenv pos atoms =
      List.map (cps_atom tenv pos) atoms

   (************************************************************************
    * OPERATORS
    ************************************************************************)

   (*
    * Map over the types in the operator.
    *)
   let cps_unop tenv pos op =
      let pos = string_pos "cps_unop" pos in
         match op with
            (* Integers *)
            Fc_air.UMinusIntOp (pre, signed) -> UMinusIntOp (pre, signed)
          | Fc_air.NotIntOp (pre, signed) -> NotIntOp (pre, signed)
          | Fc_air.BitFieldOp (pre, signed, off, len) -> BitFieldOp (pre, signed, off, len)

            (* Floats *)
          | Fc_air.UMinusFloatOp pre -> UMinusFloatOp pre
          | Fc_air.AbsFloatOp    pre -> AbsFloatOp    pre
          | Fc_air.SinOp 	 pre -> SinOp	      pre
          | Fc_air.CosOp 	 pre -> CosOp         pre
          | Fc_air.SqrtOp    	 pre -> SqrtOp        pre

          | Fc_air.EqPointerNilOp -> EqPointerNilOp
          | Fc_air.NeqPointerNilOp -> NeqPointerNilOp

            (* Coercions *)
          | Fc_air.IntOfInt (pre1, signed1, pre2, signed2) -> IntOfInt (pre1, signed1, pre2, signed2)
          | Fc_air.IntOfFloat (pre1, signed1, pre2) -> IntOfFloat (pre1, signed1, pre2)
          | Fc_air.IntOfUnit (pre1, signed1, i) -> IntOfUnit (pre1, signed1, i)
          | Fc_air.FloatOfInt (pre1, pre2, signed2) -> FloatOfInt (pre1, pre2, signed2)
          | Fc_air.FloatOfFloat (pre1, pre2) -> FloatOfFloat (pre1, pre2)
          | Fc_air.IntOfPointer (pre, signed) -> IntOfPointer (pre, signed)
          | Fc_air.PointerOfInt (pre, signed) -> PointerOfInt (pre, signed)


   let cps_binop tenv pos op =
      let pos = string_pos "cps_binop" pos in
         match op with
	    Fc_air.AndBoolOp		     -> AndBoolOp
	  | Fc_air.OrBoolOp		     -> OrBoolOp
	  | Fc_air.EqBoolOp		     -> raise (Invalid_argument "EqBoolOp: not implemented")
	  | Fc_air.NeqBoolOp		     -> raise (Invalid_argument "NeqBoolOp: not implemented")

          | Fc_air.PlusIntOp   (pre, signed) -> PlusIntOp   (pre, signed)
          | Fc_air.MinusIntOp  (pre, signed) -> MinusIntOp  (pre, signed)
          | Fc_air.MulIntOp    (pre, signed) -> MulIntOp    (pre, signed)
          | Fc_air.DivIntOp    (pre, signed) -> DivIntOp    (pre, signed)
          | Fc_air.RemIntOp    (pre, signed) -> RemIntOp    (pre, signed)
          | Fc_air.SlIntOp     (pre, signed) -> SlIntOp     (pre, signed)
          | Fc_air.SrIntOp     (pre, signed) -> SrIntOp     (pre, signed)
          | Fc_air.AndIntOp    (pre, signed) -> AndIntOp    (pre, signed)
          | Fc_air.OrIntOp     (pre, signed) -> OrIntOp     (pre, signed)
          | Fc_air.XorIntOp    (pre, signed) -> XorIntOp    (pre, signed)

          | Fc_air.MinIntOp    (pre, signed) -> MinIntOp    (pre, signed)
          | Fc_air.MaxIntOp    (pre, signed) -> MaxIntOp    (pre, signed)

          | Fc_air.SetBitFieldOp (pre, signed, off, len) -> SetBitFieldOp (pre, signed, off, len)

          | Fc_air.EqIntOp     (pre, signed) -> EqIntOp     (pre, signed)
          | Fc_air.NeqIntOp    (pre, signed) -> NeqIntOp    (pre, signed)
          | Fc_air.LtIntOp     (pre, signed) -> LtIntOp     (pre, signed)
          | Fc_air.LeIntOp     (pre, signed) -> LeIntOp     (pre, signed)
          | Fc_air.GtIntOp     (pre, signed) -> GtIntOp     (pre, signed)
          | Fc_air.GeIntOp     (pre, signed) -> GeIntOp     (pre, signed)

          | Fc_air.PlusFloatOp   pre -> PlusFloatOp   pre
          | Fc_air.MinusFloatOp  pre -> MinusFloatOp  pre
          | Fc_air.MulFloatOp    pre -> MulFloatOp    pre
          | Fc_air.DivFloatOp    pre -> DivFloatOp    pre
          | Fc_air.RemFloatOp    pre -> RemFloatOp    pre

          | Fc_air.MinFloatOp    pre -> MinFloatOp    pre
          | Fc_air.MaxFloatOp    pre -> MaxFloatOp    pre

          | Fc_air.EqFloatOp     pre -> EqFloatOp     pre
          | Fc_air.NeqFloatOp    pre -> NeqFloatOp    pre
          | Fc_air.LtFloatOp     pre -> LtFloatOp     pre
          | Fc_air.LeFloatOp     pre -> LeFloatOp     pre
          | Fc_air.GtFloatOp     pre -> GtFloatOp     pre
          | Fc_air.GeFloatOp     pre -> GeFloatOp     pre
          | Fc_air.Atan2Op       pre -> Atan2Op       pre

            (* Tag operations *)
          | Fc_air.EqTagOp -> EqTagOp

            (* Pointer operations *)
          | Fc_air.PlusPointerOp -> PlusPointerOp
          | Fc_air.MinusPointerOp -> MinusPointerOp

          | Fc_air.EqPointerOp -> EqPointerOp
          | Fc_air.NeqPointerOp -> NeqPointerOp
          | Fc_air.LtPointerOp -> LtPointerOp
          | Fc_air.LePointerOp -> LePointerOp
          | Fc_air.GtPointerOp -> GtPointerOp
          | Fc_air.GePointerOp -> GePointerOp

   (************************************************************************
    * EXPRESSIONS
    ************************************************************************)

   (*
    * Transform an expression.
    *)
   let rec cps_expr tenv renv line e =
      let pos = string_pos "cps_expr" (air_exp_pos e) in
      let loc = Fc_air_ds.loc_of_exp e in
         match Fc_air_ds.dest_exp_core e with
            Fc_air.LetFuns (funs, e) ->
               cps_funs_expr tenv renv line pos loc funs e
          | Fc_air.LetAtom (v, ty, a, e) ->
               make_exp loc (LetAtom (v, cps_type tenv pos ty, cps_atom tenv pos a, cps_expr tenv renv line e))
          | Fc_air.LetUnop (v, ty, op, a, e) ->
               make_exp loc (LetUnop (v, cps_type tenv pos ty, cps_unop tenv pos op, cps_atom tenv pos a, cps_expr tenv renv line e))
          | Fc_air.LetBinop (v, ty, op, a1, a2, e) ->
               make_exp loc (LetBinop (v, cps_type tenv pos ty, cps_binop tenv pos op, cps_atom tenv pos a1, cps_atom tenv pos a2, cps_expr tenv renv line e))
          | Fc_air.LetExtCall (v, ty, s, b, ty', args, e) ->
               cps_extcall_expr tenv renv line pos loc v ty s b ty' args e
          | Fc_air.LetApply (v, ty, f, args, e) ->
               cps_apply_expr tenv renv line pos loc v ty f args e
          | Fc_air.TailCall (f, args) ->
               make_exp loc (TailCall (TailNormal (f, cps_atoms tenv pos args)))
          | Fc_air.Return (f, a) ->
               cps_return_expr tenv renv line pos loc f a
          | Fc_air.LetMalloc (v, a, e) ->
               make_exp loc (LetMalloc (v, cps_atom tenv pos a, cps_expr tenv renv line e))
          | Fc_air.IfThenElse (a, e1, e2) ->
               make_exp loc (IfThenElse (cps_atom tenv pos a, cps_expr tenv renv line e1, cps_expr tenv renv line e2))
          | Fc_air.Try (e1, v, e2) ->
               cps_try_expr tenv renv line pos loc e1 v e2
          | Fc_air.Raise a ->
               cps_raise_expr tenv renv line pos loc a
          | Fc_air.LetSubscript (v, ty, v1, a2, e) ->
               make_exp loc (LetSubscript (v, cps_type tenv pos ty, v1, cps_atom tenv pos a2, cps_expr tenv renv line e))
          | Fc_air.SetSubscript (v1, a2, ty, a3, e) ->
               make_exp loc (SetSubscript (v1, cps_atom tenv pos a2, cps_type tenv pos ty, cps_atom tenv pos a3, cps_expr tenv renv line e))
          | Fc_air.Memcpy (v1, a, v2, e) ->
               make_exp loc (Memcpy (v1, zero, v2, zero, cps_atom tenv pos a, cps_expr tenv renv line e))
          | Fc_air.LetVar (v, ty, a_opt, e) ->
               make_exp loc (LetVar (v, cps_type tenv pos ty, cps_atom_opt tenv pos a_opt, cps_expr tenv renv line e))
          | Fc_air.LetAddrOfVar (v1, ty, v2, e) ->
               make_exp loc (LetAddrOfVar (v1, cps_type tenv pos ty, v2, cps_expr tenv renv line e))
          | Fc_air.SetVar (v, ty, a, e) ->
               make_exp loc (SetVar (v, cps_type tenv pos ty, cps_atom tenv pos a, cps_expr tenv renv line e))
          | Fc_air.Debug (info, e) ->
               cps_debug_expr tenv renv line pos loc info e

   (*
    * Convert the functions.
    * Local functions do not get the extra
    * arguments.
    *)
   and cps_funs_expr tenv renv line pos loc funs e =
      let pos = string_pos "cps_funs_expr" pos in

      (* Define local names for all the functions *)
      let local_fun (f, line, gflag, ty, vars, e) =
         let f' = new_symbol_string (Symbol.to_string f ^ "_local") in
            f, f', line, gflag, ty, vars, e
      in
      let make_fun (f, f', line, gflag, ty, vars, e) =
            match gflag with
               Fc_air.VarGlobalClass
             | Fc_air.VarStaticClass ->
                  let ty_global = cps_type tenv pos ty in
                  let ty_local = cps_local_type tenv pos ty in
                  let cps_sym = new_symbol_string "cps" in
                  let renv = renv_add_cps renv f cps_sym in
                  let renv = renv_add_fun renv f f' in
                  let vars_local = vars @ [cps_sym] in
                  let vars_global = vars @ [cps_sym; exnh_sym] in
                  let args_local = List.map (fun v -> AtomVar v) vars_local in
                  let e' = make_exp loc (TailCall (TailNormal (f', args_local))) in
                  let e' = make_exp loc (LetFuns ([f', line, VarGlobalClass, ty_local, vars_local, cps_expr tenv renv line e], e')) in
                     f, line, VarGlobalClass, ty_global, vars_global, e'
             | Fc_air.VarLocalClass ->
                  let _, ty_vars', _ = dest_fun_type pos ty in
                  let ty_vars' = List.map (cps_type tenv pos) ty_vars' in
                  let ty' = TyFun (ty_vars', TyUnit 0) in
                     f, line, VarLocalClass, ty', vars, cps_expr tenv renv line e
      in
      let funs = List.map local_fun funs in
      let funs = List.map make_fun funs in
         make_exp loc (LetFuns (funs, cps_expr tenv renv line e))

   (*
    * Convert a function application to a tail call.
    *)
   and cps_apply_expr tenv renv line pos loc v ty f args e =
      let pos = string_pos "cps_apply_expr" pos in
      let args = cps_atoms tenv pos args in
      let e =
         match Fc_air_ds.dest_exp_core e with
            Fc_air.Return (g, Fc_air.AtomVar v') when Symbol.eq v' v ->
               let cps_sym = renv_lookup_cps renv pos g in
                  TailCall (TailNormal (f, args @ [AtomVar cps_sym; AtomVar exnh_sym]))
          | _ ->
               let f' = new_symbol_string (Symbol.to_string f ^ "_cont") in
               let call =
                  try
                     let f = renv_lookup_fun renv f in
                        TailCall (TailNormal (f, args @ [AtomVar f']))
                  with
                     Not_found ->
                        TailCall (TailNormal (f, args @ [AtomVar f'; AtomVar exnh_sym]))
               in
               let call = make_exp loc call in
               let f_ty = TyFun ([cps_type tenv pos ty], TyUnit 0) in
                  LetFuns ([f', line, VarContClass, f_ty, [v], cps_expr tenv renv line e], call)
      in
         make_exp loc e

   (*
    * A return becomes a call-current-continuation.
    *)
   and cps_return_expr tenv renv line pos loc f a =
      let a = cps_atom tenv pos a in
         make_exp loc (TailCall (TailNormal (renv_lookup_cps renv pos f, [a])))

   (*
    * Catch special operators in the extcall.
    *)
   and cps_extcall_expr tenv renv line pos loc v ty1 s b ty2 args e =
      let pos = string_pos "cps_migrate_expr" pos in
      let args = cps_atoms tenv pos args in
         match s, args with
            "migrate", [host] ->
               let f' = new_symbol_string (s ^ "_cont") in
               let f_ty = TyFun ([cps_type tenv pos ty1], TyUnit 0) in
               let migarg = AtomInt (Rawint.of_int Rawint.Int32 true 0) in
               let e' = make_exp loc (TailCall (TailSysMigrate (f', [host], [migarg]))) in
                  make_exp loc (LetFuns ([f', line, VarLocalClass, f_ty, [v], cps_expr tenv renv line e], e'))
          | "atomic_entry", [intval] ->
               let f' = new_symbol_string (s ^ "_cont") in
               let f_ty = TyFun ([cps_type tenv pos ty1], TyUnit 0) in
               let e' = make_exp loc (TailCall (TailAtomic (f', intval, []))) in
                  make_exp loc (LetFuns ([f', line, VarLocalClass, f_ty, [v], cps_expr tenv renv line e], e'))
          | "atomic_commit", [] ->
               (* When given zero arguments, assume we are commiting
                  the most recent level -- this corresponds to level 0. *)
               let f' = new_symbol_string (s ^ "_cont") in
               let f_ty = TyFun ([cps_type tenv pos ty1], TyUnit 0) in
               let comarg = AtomInt (Rawint.of_int Rawint.Int32 true 0) in
               let level = AtomInt (Rawint.of_int Rawint.Int32 true 0) in
               let e' = make_exp loc (TailCall (TailAtomicCommit (level, f', [comarg]))) in
                  make_exp loc (LetFuns ([f', line, VarLocalClass, f_ty, [v], cps_expr tenv renv line e], e'))
          | "atomic_commit_level", [level] ->
               let f' = new_symbol_string (s ^ "_cont") in
               let f_ty = TyFun ([cps_type tenv pos ty1], TyUnit 0) in
               let comarg = AtomInt (Rawint.of_int Rawint.Int32 true 0) in
               let e' = make_exp loc (TailCall (TailAtomicCommit (level, f', [comarg]))) in
                  make_exp loc (LetFuns ([f', line, VarLocalClass, f_ty, [v], cps_expr tenv renv line e], e'))
          | "atomic_rollback", [intval] ->
               (* When given zero arguments, assume we are commiting
                  the most recent level -- this corresponds to level 0. *)
               (* Note: code following this extcall can NEVER be reached. *)
               let level = AtomInt (Rawint.of_int Rawint.Int32 true 0) in
                  make_exp loc (TailCall (TailAtomicRollback (level, intval)))
          | "atomic_rollback_level", [level; intval] ->
               (* Note: code following this extcall can NEVER be reached. *)
               make_exp loc (TailCall (TailAtomicRollback (level, intval)))
          | _ ->
               (* Normal external call *)
               make_exp loc (LetExtCall (v, cps_notype tenv pos ty1, s, b, cps_notype tenv pos ty2, args, cps_expr tenv renv line e))

   (*
    * Try produces a new exception handler.
    *)
   and cps_try_expr tenv renv line pos loc e1 v e2 =
      let pos = string_pos "cps_try_expr" pos in
      let f_exn = new_symbol_string "with" in
      let e = make_exp loc (LetAtom (exnh_sym, ty_exn, AtomVar f_exn, cps_expr tenv renv line e1)) in
         make_exp loc (LetFuns ([f_exn, line, VarContClass, ty_exn, [v], cps_expr tenv renv line e2], e))

   (*
    * Raise calls the exception handler.
    *)
   and cps_raise_expr tenv renv line pos loc a =
      let pos = string_pos "cps_raise_expr" pos in
         make_exp loc (TailCall (TailNormal (exnh_sym, [cps_atom tenv pos a])))

   (*
    * Debugging statement.
    *)
   and cps_debug_expr tenv renv line pos loc info e =
      let pos = string_pos "cps_debug_expr" pos in
      let line, info =
         match info with
            Fc_air.DebugString s ->
               line, DebugString s
          | Fc_air.DebugContext (line, vars) ->
               let vars =
                  List.map (fun (v1, ty, v2) -> v1, cps_type tenv pos ty, v2) vars
               in
                  line, DebugContext (line, vars)
      in
      let e = cps_expr tenv renv line e in
         make_exp loc (Debug (info, e))

   (************************************************************************
    * GLOBAL FUNCTIONS
    ************************************************************************)

   (*
    * Build the program.
    *)
   let zero_loc = bogus_loc "<Fc_ir_air>"

   let build_prog prog =
      let { Fc_air.prog_file = file;
            Fc_air.prog_globals = globals;
            Fc_air.prog_decls = decls;
            Fc_air.prog_export = export;
            Fc_air.prog_import = import;
            Fc_air.prog_types = types;
            Fc_air.prog_body = body
          } = prog
      in

      (* Map the globals *)
      let globals =
         SymbolTable.mapi (fun v (ty, volp, init) ->
               let pos = var_exp_pos v in
               let init =
                  match init with
                     Fc_air.InitString (pre, s) -> InitString (pre, s)
                   | Fc_air.InitBlock i -> InitBlock i
                   | Fc_air.InitAtom a -> InitAtom (cps_atom types pos a)
               in
               let ty = cps_type types pos ty in
                  ty, volp, init) globals
      in
      let decls =
         SymbolTable.mapi (fun v ty ->
               let pos = var_exp_pos v in
               let ty = cps_type types pos ty in
                  ty) decls
      in

      (* External definitions *)
      let export =
         SymbolTable.mapi (fun v info ->
               let { Fc_air.export_name = s;
                     Fc_air.export_type = ty
                   } = info
               in
                  { export_name = s;
                    export_type = cps_type types (var_exp_pos v) ty
                  }) export
      in
      let import =
         SymbolTable.mapi (fun v info ->
               let { Fc_air.import_name = s;
                     Fc_air.import_type = ty;
                     Fc_air.import_info = info
                   } = info
               in
                  { import_name = s;
                    import_type = cps_type types (var_exp_pos v) ty;
                    import_info = info
                  }) import
      in

      (* Type definitions *)
      let types' =
         SymbolTable.mapi (fun v ty ->
               cps_type types (var_exp_pos v) ty) types
      in

      (* Body *)
      let renv = renv_add_cps renv_empty Fc_ast_parse.exit_sym exit_sym in
      let body = cps_expr types renv zero_loc body in
      let prog =
         { prog_file = file;
           prog_globals = globals;
           prog_decls = decls;
           prog_export = export;
           prog_import = import;
           prog_types = types';
           prog_frames = SymbolTable.empty;
           prog_funs = SymbolTable.empty;
           prog_body = body
         }
      in
         standardize_prog prog
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
