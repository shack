(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_config

open Fc_ir
open Fc_ir_env
open Fc_ir_exn
open Fc_ir_pos
open Fc_ir_subst

open Sizeof_const

module Pos = MakePos (struct let name = "Fc_ir_type" end)
open Pos

(*
 * Type of malloc.
 *)
let native_char_precision = FCParam.char_precision NormalPrecision

let ty_malloc = TyPointer (TyInt (native_char_precision, false))

(*
 * Atom should be a var.
 *)
let var_of_atom pos = function
   AtomVar v ->
      v
 | a ->
      raise (IRException (pos, StringAtomError ("not a var", a)))

(*
 * Get the type of an atom.
 *)
let ty_label = TyInt (precision_subscript, signed_subscript)

let type_of_atom venv pos a =
   match a with
      AtomUnit (n, _) ->
         TyUnit n
    | AtomInt i ->
         let pre = Rawint.precision i in
         let signed = Rawint.signed i in
            TyInt (pre, signed)
    | AtomFloat x ->
         let pre = Rawfloat.precision x in
            TyFloat pre
    | AtomNil ty ->
         TyPointer ty
    | AtomVar v ->
         venv_lookup venv pos v
    | AtomLabel _ ->
         ty_label

(*
 * Apply the type, and return the expansion.
 * The result _is not_ a TyApply.
 *)
let apply_type tenv pos v tyl =
   match tenv_lookup tenv pos v with
      TyLambda (vars, ty) ->
         let tvenv =
            let len1 = List.length vars in
            let len2 = List.length tyl in
               if len1 <> len2 then
                  raise (IRException (pos, ArityMismatch (len1, len2)));
               List.fold_left2 subst_add subst_empty vars tyl
         in
            subst_type tvenv ty
    | ty ->
         ty

(*
 * Expand a type one level.
 *)
let rec tenv_expand tenv pos ty =
   let pos = string_pos "tenv_expand" pos in
      match ty with
         TyApply (v, tyl) ->
            tenv_expand tenv pos (apply_type tenv pos v tyl)
       | TyLambda ([], ty) ->
            tenv_expand tenv pos ty
       | _ ->
            ty

(*
 * Check if a type is a non-scalar.
 *)
let is_scalar_type tenv pos ty =
   let pos = string_pos "is_scalar_type" pos in
      match tenv_expand tenv pos ty with
         TyUnit _
       | TyInt _
       | TyFloat _ ->
            true
       | _ ->
            false

let is_aggregate_type tenv pos ty =
   let pos = string_pos "is_aggregate_type" pos in
      match tenv_expand tenv pos ty with
         TyRawData _
       | TyFrame _ ->
            true
       | _ ->
            false

(*
 * Get the parts of a universal type.
 *)
let dest_all_type tenv pos ty =
   let pos = string_pos "dest_all_type" pos in
      match tenv_expand tenv pos ty with
         TyAll (vars, ty) ->
            vars, ty
       | ty ->
            raise (IRException (pos, StringTypeError ("not a universal type", ty)))

(*
 * Get the types of the fun.
 *)
let rec is_fun_type_aux tenv pos ty =
   let pos = string_pos "is_fun_type" pos in
      match tenv_expand tenv pos ty with
         TyFun _ -> true
       | TyAll (_, ty) -> is_fun_type_aux tenv pos ty
       | _ -> false

let is_fun_type tenv pos ty =
   let pos = string_pos "is_fun_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer ty -> is_fun_type_aux tenv pos ty
       | ty -> is_fun_type_aux tenv pos ty

let rec dest_fun_type_aux tenv pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match tenv_expand tenv pos ty with
         TyFun (ty_args, ty_res) ->
            [], ty_args, ty_res
       | TyAll (vars, ty) ->
            let vars', ty_args, ty_res = dest_fun_type_aux tenv pos ty in
               vars @ vars', ty_args, ty_res
       | ty ->
            raise (IRException (pos, StringTypeError ("not a function type", ty)))

let dest_fun_type tenv pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer ty -> dest_fun_type_aux tenv pos ty
       | ty -> dest_fun_type_aux tenv pos ty

(*
 * Environments are pointers to frames.
 *)
let rec is_frame_type tenv pos ty =
   let pos = string_pos "is_frame_type" pos in
      match tenv_expand tenv pos ty with
         TyFrame _ ->
            true
       | _ ->
            false

let rec is_env_type tenv pos ty =
   let pos = string_pos "is_env_type" pos in
      match tenv_expand tenv pos ty with
         TyFrame _ ->
            true
       | TyPointer ty ->
            is_frame_type tenv pos ty
       | _ ->
            false

(*
 * Get the type of the pointer.
 *)
let is_pointer_type tenv pos ty =
   let pos = string_pos "is_pointer_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer _ -> true
       | _ -> false

let is_pointer_nonenv_type tenv pos ty =
   let pos = string_pos "is_pointer_nonenv_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer ty -> not (is_frame_type tenv pos ty)
       | _ -> false

let is_pointer_nonfun_type tenv pos ty =
   let pos = string_pos "is_pointer_nonenv_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer ty -> not (is_frame_type tenv pos ty || is_fun_type tenv pos ty)
       | _ -> false

let dest_pointer_type tenv pos ty =
   let pos = string_pos "dest_pointer_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer ty -> ty
       | ty -> raise (IRException (pos, StringTypeError ("not a pointer type", ty)))

(*
 * Closures are functions or pointers to functions.
 *)
let rec is_closure_type tenv pos ty =
   let pos = string_pos "is_closure_type" pos in
      match tenv_expand tenv pos ty with
         TyFun _ ->
            true
       | TyAll (_, ty) ->
            is_closure_type tenv pos ty
       | TyPointer ty ->
            is_fun_type_aux tenv pos ty
       | _ ->
            false

let rec dest_closure_type tenv pos ty =
   let pos = string_pos "dest_closure_type" pos in
      match tenv_expand tenv pos ty with
         TyFun (ty_args, ty_res) ->
            [], ty_args, ty_res
       | TyAll (vars, ty) ->
            let vars', ty_args, ty_res = dest_closure_type tenv pos ty in
               vars @ vars', ty_args, ty_res
       | TyPointer ty ->
            dest_fun_type_aux tenv pos ty
       | ty ->
            raise (IRException (pos, StringTypeError ("not a closure type", ty)))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
