(*
 * Deadcode elimination.
 * We want to eliminate useless arguments too,
 * so we first construct a dataflow graph, then calculate
 * the fixpoint.  Also, we try to eliminate useless types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_ir
open Fc_ir_ds
open Fc_ir_exn
open Fc_ir_pos
open Fc_ir_type

module Pos = MakePos (struct let name = "Fc_ir_dead" end)
open Pos

(*
 * Live set is a symbol set.
 *)
module LiveSet = SymbolSet
module FunTable = SymbolTable

(************************************************************************
 * LIVENESS INITIALIZATION
 ************************************************************************)

(*
 * Add all nested functions to make sure we keep labeled functions.
 *)
let rec live_init decls live nest e =
   match dest_exp_core e with
      LetAtom (_, _, _, e)
    | LetUnop (_, _, _, _, e)
    | LetBinop (_, _, _, _, _, e)
    | LetExtCall (_, _, _, _, _, _, e)
    | LetMalloc (_, _, e)
    | LetSubscript (_, _, _, _, e)
    | LetVar (_, _, _, e)
    | LetAddrOfVar (_, _, _, e)
    | LetFrame (_, _, e)
    | SetVar (_, _, _, e)
    | Debug (_, e)
    | SetSubscript (_, _, _, _, e)
    | SetGlobal (_, _, _, e)
    | LetProject (_, _, _, _, e)
    | LetAddrOfProject (_, _, _, _, e)
    | SetProject (_, _, _, _, e)
    | Memcpy (_, _, _, _, _, e)
    | LetClosure (_, _, _, e) ->
         live_init decls live nest e

    | TailCall _ ->
         live

    | IfThenElse (_, e1, e2) ->
         live_init decls (live_init decls live nest e1) nest e2

    | LetFuns (fundefs, e) ->
         let live =
            List.fold_left (fun live (f, _, gflag, _, _, e) ->
                  let nest = f :: nest in
                  let live =
                     if SymbolTable.mem decls f then
                        List.fold_left LiveSet.add live nest
                     else
                        live
                  in
                     live_init decls live nest e) live fundefs
         in
            live_init decls live nest e

let live_init decls e =
   live_init decls LiveSet.empty [] e

(************************************************************************
 * LIVENESS
 ************************************************************************)

(*
 * Collect uses of the type.
 *)
let rec live_type live = function
   TyUnit _
 | TyDelayed
 | TyTag
 | TyInt _
 | TyFloat _
 | TyRawData _
 | TyVar _ ->
      live
 | TyPointer ty ->
      live_type live ty
 | TyFun (ty_args, ty_res) ->
      let live = List.fold_left live_type live ty_args in
         live_type live ty_res
 | TyLambda (_, ty)
 | TyAll (_, ty) ->
      live_type live ty
 | TyApply (v, tyl) ->
      let live = List.fold_left live_type live tyl in
         LiveSet.add live v
 | TyFrame label ->
      LiveSet.add live label

(*
 * Calculate uses for each type def.
 *)
let live_types types =
   SymbolTable.map (live_type LiveSet.empty) types

(*
 * Frames.  All the subfields are marked live if the field
 * is live.
 *)
let live_frame_all live vars =
   List.fold_left (fun live (v, ty) ->
         let live = LiveSet.add live v in
            live_type live ty) live vars

let live_frame_some live vars =
   List.fold_left (fun live (v, ty) ->
         if LiveSet.mem live v then
            live_type live ty
         else
            live) live vars

let live_frame live frame =
   SymbolTable.fold (fun live v vars ->
         if LiveSet.mem live v then
            live_frame_all live vars
         else
            live_frame_some live vars) live frame

(*
 * Variable.
 *)
let live_var live v =
   LiveSet.add live v

(*
 * Atoms.
 *)
let live_atom live = function
   AtomVar v ->
      live_var live v
 | AtomLabel (frame, field, subfield) ->
      SymbolSet.add (SymbolSet.add (SymbolSet.add live frame) field) subfield
 | _ ->
      live

let live_atom_opt live = function
   Some a -> live_atom live a
 | None -> live

let live_atoms live atoms =
   List.fold_left live_atom live atoms

(*
 * Global declarations keep all their arguments.
 *)
let live_global_declare export live gflag f vars =
   match gflag with
      VarGlobalClass ->
         if SymbolTable.mem export f then
            List.fold_left LiveSet.add (LiveSet.add live f) vars
         else
            live
    | VarContClass
    | VarLocalClass ->
         live

let live_cont_declare live gflag f vars =
   match gflag with
      VarContClass
    | VarGlobalClass ->
         List.fold_left LiveSet.add (LiveSet.add live f) vars
    | VarLocalClass ->
         live

(*
 * Add types for live function arguments.
 *)
let rec live_args live vars ty =
   match ty with
      TyFun (ty_args, ty_res) ->
         let live =
            List.fold_left2 (fun live v ty ->
                  if LiveSet.mem live v then
                     live_type live ty
                  else
                     live) live vars ty_args
         in
            live_type live ty_res
    | ty ->
         live_type live ty

(*
 * Calculate defs/uses along each branch to a LetClosure or TailCall.
 *)
let rec live_exp export funs live e =
   match dest_exp_core e with
      LetFuns (fundefs, e) ->
         live_funs_exp export funs live fundefs e
    | LetAtom (v, ty, a, e) ->
         live_atom_exp export funs live v ty a e
    | LetUnop (v, ty, op, a, e) ->
         live_unop_exp export funs live v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         live_binop_exp export funs live v ty op a1 a2 e
    | LetExtCall (v, ty, s, b, ty', args, e) ->
         live_extcall_exp export funs live v ty s ty' args e
    | LetClosure (v, f, args, e) ->
         live_closure_exp export funs live v f args e
    | TailCall op ->
         live_tailcall_exp export funs live op
    | LetMalloc (v, a, e) ->
         live_malloc_exp export funs live v a e
    | IfThenElse (a, e1, e2) ->
         live_if_exp export funs live a e1 e2
    | LetSubscript (v, ty, v2, a3, e) ->
         live_subscript_exp export funs live v ty v2 a3 e
    | SetSubscript (v1, a2, ty, a3, e) ->
         live_set_subscript_exp export funs live v1 a2 ty a3 e
    | SetGlobal (v, ty, a, e) ->
         live_set_global_exp export funs live v ty a e
    | Memcpy (v1, a1, v2, a2, a3, e) ->
         live_memcpy_exp export funs live v1 a1 v2 a2 a3 e
    | LetVar (v, ty, a_opt, e) ->
         live_var_exp export funs live v ty a_opt e
    | LetFrame (v, ty, e) ->
         live_frame_exp export funs live v ty e
    | LetAddrOfVar (v1, ty, v2, e) ->
         live_addr_of_var_exp export funs live v1 ty v2 e
    | SetVar (v, ty, a, e) ->
         live_set_var_exp export funs live v ty a e
    | Debug (info, e) ->
         live_debug_exp export funs live info e
    | LetProject (v1, ty, v2, label, e) ->
         live_project_exp export funs live v1 ty v2 label e
    | LetAddrOfProject (v1, ty, v2, label, e) ->
         live_addr_of_project_exp export funs live v1 ty v2 label e
    | SetProject (v, label, ty, a, e) ->
         live_set_project_exp export funs live v label ty a e

(*
 * Function definitions.
 * Calculate fixpoint based on live functions.
 *)
and live_funs_exp export funs live fundefs e =
   let live = live_exp export funs live e in
   let rec fixpoint live fundefs =
      let live', fundefs =
         List.fold_left (fun (live, fundefs) fund ->
               let f, _, gflag, ty, vars, e = fund in
               let live = live_global_declare export live gflag f vars in
                  if LiveSet.mem live f then
                     let live = live_cont_declare live gflag f vars in
                     let live = live_args live vars ty in
                     let live = live_exp export funs live e in
                        live, fundefs
                  else
                     live, fund :: fundefs) (live, []) fundefs
      in
         if fundefs <> [] && LiveSet.cardinal live' <> LiveSet.cardinal live then
            fixpoint live' fundefs
         else
            live'
   in
      fixpoint live fundefs

(*
 * Operator.
 * Add the def.
 *)
and live_atom_exp export funs live v ty a e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

and live_unop_exp export funs live v ty op a e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

and live_binop_exp export funs live v ty op a1 a2 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a1 in
         let live = live_atom live a2 in
         let live = live_type live ty in
            live
      else
         live

(*
 * Don't ever delete extcalls--they may have side-effects.
 *)
and live_extcall_exp export funs live v ty s ty' args e =
   let live = live_exp export funs live e in
   let live = live_var live v in
   let live = live_type live ty' in
   let live = live_atoms live args in
   let live = live_type live ty in
      live

(*
 * Closure.
 * Some of the arguments may have been dropped.
 *)
and live_closure_exp export funs live v f args e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         live_tailcall_normal export funs live f [] args
      else
         live

(*
 * Tailcall.
 * Some of the arguments may have been dropped.
 *)
and live_tailcall_normal export funs live f eargs args =
   let live = live_var live f in
   let live = live_atoms live eargs in
      try
         let vars = FunTable.find funs f in
         let rec collect live vars args =
            match vars, args with
               v :: vars, a :: args ->
                  let live =
                     if LiveSet.mem live v then
                        live_atom live a
                     else
                        live
                  in
                     collect live vars args
             | _ ->
                  live
         in
            collect live vars args
      with
         Not_found ->
            live_atoms live args

(*
 * Atomic has a special ``first argument'' to the function, which must
 * always be present (even if it is dead).
 *)
and live_tailcall_atomic export funs live f c args =
   let live =
      try
         match FunTable.find funs f with
            reserved :: _ ->
               live_var live reserved
          | [] ->
               live
      with
         Not_found ->
            live
   in
      live_tailcall_normal export funs live f [] (c :: args)

and live_tailcall_exp export funs live op =
   match op with
      TailNormal (f, args) ->
         live_tailcall_normal export funs live f [] args
    | TailSysMigrate (f, host, args) ->
         live_tailcall_normal export funs live f host args
    | TailAtomic (f, c, args) ->
         live_tailcall_atomic export funs live f c args
    | TailAtomicCommit (level, f, args) ->
         live_tailcall_normal export funs live f [level] args
    | TailAtomicRollback (level, c) ->
         live_atoms live [level; c]

(*
 * Memory allocations.
 *)
and live_malloc_exp export funs live v a e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
            live
      else
         live

(*
 * Conditional.
 *)
and live_if_exp export funs live a e1 e2 =
   let live = live_exp export funs live e1 in
   let live = live_exp export funs live e2 in
      live_atom live a

(*
 * Subscripting.
 * We can't eliminate side-effects.
 *)
and live_subscript_exp export funs live v ty v2 a3 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_var live v2 in
         let live = live_atom live a3 in
            live
      else
         live

and live_set_subscript_exp export funs live v1 a2 ty a3 e =
   let live = live_exp export funs live e in
   let live = live_atom live a3 in
   let live = live_type live ty in
   let live = live_atom live a2 in
   let live = live_var live v1 in
      live

and live_set_global_exp export funs live v ty a e =
   let live = live_exp export funs live e in
   let live = live_atom live a in
   let live = live_type live ty in
   let live = live_var live v in
      live

and live_memcpy_exp export funs live v1 a1 v2 a2 a3 e =
   let live = live_exp export funs live e in
   let live = live_var live v1 in
   let live = live_atom live a1 in
   let live = live_var live v2 in
   let live = live_atom live a2 in
   let live = live_atom live a3 in
      live

(*
 * Projections on frames.
 * Liveness on frames works in a strange way.
 *
 * v3 is the field name, and v4 is the subfield.
 * LetProject marks only the subfield,
 * but LetAddrOfProject marks the field itself to
 * indicate that all subfields are live.
 *
 * SetProject only checks the subfield.
 *)
and live_project_exp export funs live v1 ty v2 (frame, field, subfield) e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v1 then
         let live = live_type live ty in
         let live = live_var live v2 in
         let live = live_var live frame in
         let live = live_var live subfield in
            live
      else
         live

and live_addr_of_project_exp export funs live v1 ty v2 (frame, field, subfield) e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v1 then
         let live = live_type live ty in
         let live = live_var live v2 in
         let live = live_var live frame in
         let live = live_var live field in
         let live = live_var live subfield in
            live
      else
         live

and live_set_project_exp export funs live v1 (frame, field, subfield) ty a4 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live subfield then
         let live = live_var  live v1 in
         let live = live_type live ty in
         let live = live_var  live frame in
         let live = live_atom live a4 in
            live
      else
         live

(*
 * Variable operations.
 *)
and live_var_exp export funs live v ty a_opt e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_atom_opt live a_opt in
            live
      else
         live

and live_frame_exp export funs live v ty e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
            live
      else
         live

and live_addr_of_var_exp export funs live v1 ty v2 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v1 then
         let live = live_type live ty in
         let live = live_var live v2 in
            live
      else
         live

and live_set_var_exp export funs live v ty a e =
   let live = live_exp export funs live e in
   let live = live_atom live a in
   let live = live_type live ty in
   let live = live_var live v in
      live

(*
 * Debugging variables are preserved.
 *)
and live_debug_exp export funs live info e =
   let live = live_exp export funs live e in
      match info with
         DebugString _ ->
            live
       | DebugContext (_, vars) ->
            List.fold_left (fun live (_, ty, v) ->
                  live_var (live_type live ty) v) live vars

(************************************************************************
 * FIXPOINT
 ************************************************************************)

(*
 * Calculate the live variable fixpoint.
 *)
let fixpoint f live =
   let rec loop n live =
      let live = f live in
      let m = LiveSet.cardinal live in
         if m <> n then
            loop m live
         else
            live
   in
      loop (LiveSet.cardinal live) live

(*
 * Liveness for a program.
 *)
let live_prog_step funs prog live =
   let { prog_globals = globals;
         prog_export = export;
         prog_decls = decls;
         prog_types = types;
         prog_frames = frames;
         prog_funs = fundefs;
         prog_body = body
       } = prog
   in

   (* Calculate globals *)
   let live =
      SymbolTable.fold (fun live v (ty, _, _) ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live globals
   in

   (* Calculate decls *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live decls
   in

   (* Calculate functions *)
   let live =
      SymbolTable.fold (fun live f (_, gflag, ty, vars, e) ->
            let live = live_global_declare export live gflag f vars in
               if LiveSet.mem live f then
                  let live = live_cont_declare live gflag f vars in
                  let live = live_exp export funs live e in
                  let live = live_type live ty in
                     live
               else
                  live) live fundefs
   in

   (* Type definitions *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live types
   in

   (* Labels *)
   let live =
      SymbolTable.fold (fun live v frame ->
            if LiveSet.mem live v then
               live_frame live frame
            else
               live) live frames
   in
      live_exp export funs live body

(*
 * Calculate the live sets.
 *)
let live_prog prog =
   let { prog_funs = fundefs;
         prog_export = export;
         prog_decls = decls;
         prog_body = body
       } = prog
   in

   (* Function table saves all function vars *)
   let funs =
      SymbolTable.fold (fun funs f (_, _, _, vars, _) ->
            FunTable.add funs f vars) FunTable.empty fundefs
   in

   (* Find all the declared funs *)
   let live = live_init decls body in

   (* Add all the exports to the live set *)
   let live =
      SymbolTable.fold (fun live v _ ->
            live_var live v) live export
   in

   (* Add all global functions to the live set *)
   let live =
      SymbolTable.fold (fun live f (_, gflag, _, _, _) ->
            match gflag with
               VarGlobalClass ->
                  LiveSet.add live f
             | VarLocalClass
             | VarContClass ->
                  live) live fundefs
   in
   let live = fixpoint (live_prog_step funs prog) live in
      funs, live

(************************************************************************
 * DEADCODE ELIMINATION
 ************************************************************************)

(*
 * Remove dead fields from frames.
 *)
let dead_frame live frame =
   SymbolTable.fold (fun frame v vars ->
         let vars =
            List.fold_left (fun vars (v, ty) ->
                  if LiveSet.mem live v then
                     (v, ty) :: vars
                  else
                     vars) [] vars
         in
            if vars = [] then
               if LiveSet.mem live v then
                  SymbolTable.add frame v []
               else
                  frame
            else
               SymbolTable.add frame v (List.rev vars)) SymbolTable.empty frame

(*
 * Eliminate dead arguments.
 *)
let dead_apply funs live pos f args =
   try
      let vars = FunTable.find funs f in
      let rec collect args' vars2 args2 =
         match vars2, args2 with
            v :: vars2, a :: args2 ->
               let args' =
                  if LiveSet.mem live v then
                     a :: args'
                  else
                     args'
               in
                  collect args' vars2 args2
          | _ ->
               List.rev args'
      in
         collect [] vars args
   with
      Not_found ->
         args

(*
 * Eliminate dead expressions.
 *)
let rec dead_exp tenv funs live e =
   let pos = string_pos "dead_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (fundefs, e) ->
            dead_funs_exp tenv funs live pos loc fundefs e
       | LetAtom (v, ty, a, e) ->
            dead_atom_exp tenv funs live pos loc v ty a e
       | LetUnop (v, ty, op, a, e) ->
            dead_unop_exp tenv funs live pos loc v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            dead_binop_exp tenv funs live pos loc v ty op a1 a2 e
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            dead_extcall_exp tenv funs live pos loc v ty s b ty' args e
       | LetClosure (v, f, args, e) ->
            dead_closure_exp tenv funs live pos loc v f args e
       | TailCall op ->
            dead_tailcall_exp tenv funs live pos loc op
       | LetMalloc (v, a, e) ->
            dead_malloc_exp tenv funs live pos loc v a e
       | IfThenElse (a, e1, e2) ->
            dead_if_exp tenv funs live pos loc a e1 e2
       | LetSubscript (v, ty, v2, a3, e) ->
            dead_subscript_exp tenv funs live pos loc v ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            dead_set_subscript_exp tenv funs live pos loc v1 a2 ty a3 e
       | SetGlobal (v, ty, a, e) ->
            dead_set_global_exp tenv funs live pos loc v ty a e
       | Memcpy (v1, a1, v2, a2, a3, e) ->
            dead_memcpy_exp tenv funs live pos loc v1 a1 v2 a2 a3 e
       | LetProject (v1, ty, v2, label, e) ->
            dead_project_exp tenv funs live pos loc v1 ty v2 label e
       | LetAddrOfProject (v1, ty, v2, label, e) ->
            dead_addr_of_project_exp tenv funs live pos loc v1 ty v2 label e
       | SetProject (v, label, ty, a, e) ->
            dead_set_project_exp tenv funs live pos loc v label ty a e
       | LetVar (v, ty, a_opt, e) ->
            dead_var_exp tenv funs live pos loc v ty a_opt e
       | LetFrame (v, ty, e) ->
            dead_frame_exp tenv funs live pos loc v ty e
       | LetAddrOfVar (v1, ty, v2, e) ->
            dead_addr_of_var_exp tenv funs live pos loc v1 ty v2 e
       | SetVar (v, ty, a, e) ->
            dead_set_var_exp tenv funs live pos loc v ty a e
       | Debug (info, e) ->
            dead_debug_exp tenv funs live pos loc info e

(*
 * Dead functions.
 * Some functions may be omitted.
 *)
and dead_funs_exp tenv funs live pos loc fundefs e =
   let funs =
      List.fold_left (fun funs (f, _, _, _, vars, _) ->
            FunTable.add funs f vars) funs fundefs
   in
   let e = dead_exp tenv funs live e in
   let fundefs =
      List.fold_left (fun fundefs (f, line, gflag, ty, vars, e) ->
            if LiveSet.mem live f then
               let all_vars, ty_args, ty_res = dest_fun_type tenv pos ty in
               let ty_args, vars =
                  List.fold_left2 (fun (ty_args, vars) ty v ->
                        if LiveSet.mem live v then
                           ty :: ty_args, v :: vars
                        else
                           ty_args, vars) ([], []) ty_args vars
               in
               let vars = List.rev vars in
               let ty = TyAll (all_vars, TyFun (List.rev ty_args, ty_res)) in
               let e = dead_exp tenv funs live e in
               let fund = f, line, gflag, ty, vars, e in
                  fund :: fundefs
            else
               fundefs) [] fundefs
   in
      if fundefs = [] then
         e
      else
         make_exp loc (LetFuns (List.rev fundefs, e))

(*
 * Dead operator.
 *)
and dead_atom_exp tenv funs live pos loc v ty a e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetAtom (v, ty, a, e))
      else
         e

and dead_unop_exp tenv funs live pos loc v ty op a e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetUnop (v, ty, op, a, e))
      else
         e

and dead_binop_exp tenv funs live pos loc v ty op a1 a2 e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetBinop (v, ty, op, a1, a2, e))
      else
         e

and dead_extcall_exp tenv funs live pos loc v ty s b ty' args e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetExtCall (v, ty, s, b, ty', args, e))
      else
         e

(*
 * Dead closure.
 * Be careful about which args to include.
 *)
and dead_closure_exp tenv funs live pos loc v f args e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         let args = dead_apply funs live pos f args in
            make_exp loc (LetClosure (v, f, args, e))
      else
         e

(*
 * Tailcall.
 * Again, be careful about dead arguments.
 *)
and dead_tailcall_normal tenv funs live pos loc f args =
   let args = dead_apply funs live pos f args in
      make_exp loc (TailCall (TailNormal (f, args)))

and dead_tailcall_migrate tenv funs live pos loc f host args =
   let args = dead_apply funs live pos f args in
      make_exp loc (TailCall (TailSysMigrate (f, host, args)))

and dead_tailcall_atomic tenv funs live pos loc f c args =
   let args = dead_apply funs live pos f (c :: args) in
   let args =
      match args with
         _ :: args ->
            args
       | [] ->
            []
   in
      make_exp loc (TailCall (TailAtomic (f, c, args)))

and dead_tailcall_atomic_commit tenv funs live pos loc level f args =
   let args = dead_apply funs live pos f args in
      make_exp loc (TailCall (TailAtomicCommit (level, f, args)))

and dead_tailcall_atomic_rollback tenv funs live pos loc level c =
   make_exp loc (TailCall (TailAtomicRollback (level, c)))

and dead_tailcall_exp tenv funs live pos loc op =
   match op with
      TailNormal (f, args) ->
         dead_tailcall_normal tenv funs live pos loc f args
    | TailSysMigrate (f, host, args) ->
         dead_tailcall_migrate tenv funs live pos loc f host args
    | TailAtomic (f, c, args) ->
         dead_tailcall_atomic tenv funs live pos loc f c args
    | TailAtomicCommit (level, f, args) ->
         dead_tailcall_atomic_commit tenv funs live pos loc level f args
    | TailAtomicRollback (level, c) ->
         dead_tailcall_atomic_rollback tenv funs live pos loc level c

(*
 * Memory allocation.
 *)
and dead_malloc_exp tenv funs live pos loc v a e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetMalloc (v, a, e))
      else
         e

(*
 * Conditional.
 *)
and dead_if_exp tenv funs live pos loc a e1 e2 =
   let e1 = dead_exp tenv funs live e1 in
   let e2 = dead_exp tenv funs live e2 in
      make_exp loc (IfThenElse (a, e1, e2))

(*
 * Subscripting.
 *)
and dead_subscript_exp tenv funs live pos loc v ty v2 a3 e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetSubscript (v, ty, v2, a3, e))
      else
         e

and dead_set_subscript_exp tenv funs live pos loc v1 a2 ty a3 e =
   let e = dead_exp tenv funs live e in
      make_exp loc (SetSubscript (v1, a2, ty, a3, e))

and dead_set_global_exp tenv funs live pos loc v ty a e =
   let e = dead_exp tenv funs live e in
      make_exp loc (SetGlobal (v, ty, a, e))

and dead_memcpy_exp tenv funs live pos loc v1 a1 v2 a2 a3 e =
   let e = dead_exp tenv funs live e in
      make_exp loc (Memcpy (v1, a1, v2, a2, a3, e))

(*
 * Projections.
 *)
and dead_project_exp tenv funs live pos loc v1 ty v2 label e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v1 then
         make_exp loc (LetProject (v1, ty, v2, label, e))
      else
         e

and dead_addr_of_project_exp tenv funs live pos loc v1 ty v2 label e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v1 then
         make_exp loc (LetAddrOfProject (v1, ty, v2, label, e))
      else
         e

and dead_set_project_exp tenv funs live pos loc v label ty a e =
   let e = dead_exp tenv funs live e in
   let _, _, subfield = label in
      if LiveSet.mem live subfield then
         make_exp loc (SetProject (v, label, ty, a, e))
      else
         e

(*
 * Variable operations.
 *)
and dead_var_exp tenv funs live pos loc v ty a_opt e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetVar (v, ty, a_opt, e))
      else
         e

and dead_frame_exp tenv funs live pos loc v ty e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetFrame (v, ty, e))
      else
         e

and dead_addr_of_var_exp tenv funs live pos loc v1 ty v2 e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v1 then
         make_exp loc (LetAddrOfVar (v1, ty, v2, e))
      else
         e

and dead_set_var_exp tenv funs live pos loc v ty a e =
   let e = dead_exp tenv funs live e in
      make_exp loc (SetVar (v, ty, a, e))

(*
 * Debug.
 *)
and dead_debug_exp tenv funs live pos loc info e =
   let e = dead_exp tenv funs live e in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTION
 ************************************************************************)

(*
 * Dead program.
 *)
let dead_prog prog =
   let { prog_file = file;
         prog_globals = globals;
         prog_decls = decls;
         prog_export = export;
         prog_import = import;
         prog_types = types;
         prog_frames = frames;
         prog_funs = fundefs;
         prog_body = body
       } = prog
   in

   (* Initial live variables *)
   let funs, live = live_prog prog in

   (* Prune globals *)
   let globals =
      SymbolTable.fold (fun globals v x ->
            if LiveSet.mem live v then
               SymbolTable.add globals v x
            else
               globals) SymbolTable.empty globals
   in

   (* Prune decls *)
   let decls =
      SymbolTable.fold (fun decls v x ->
            if LiveSet.mem live v then
               SymbolTable.add decls v x
            else
               decls) SymbolTable.empty decls
   in

   (* Prune types *)
   let types =
      SymbolTable.fold (fun types v ty ->
            if LiveSet.mem live v then
               SymbolTable.add types v ty
            else
               types) SymbolTable.empty types
   in

   (* Prune frames *)
   let frames =
      SymbolTable.fold (fun frames v frame ->
            if LiveSet.mem live v then
               SymbolTable.add frames v (dead_frame live frame)
            else
               frames) SymbolTable.empty frames
   in

   (* Prune functions *)
   let fundefs =
      SymbolTable.fold (fun fundefs f (line, gflag, ty, vars, e) ->
            if LiveSet.mem live f then
               let e = dead_exp types funs live e in
               let all_vars, ty_vars, ty_res = dest_fun_type types (var_exp_pos f) ty in
               let ty_vars, vars =
                  List.fold_left2 (fun (ty_vars, vars) ty v ->
                        if LiveSet.mem live v then
                           ty :: ty_vars, v :: vars
                        else
                           ty_vars, vars) ([], []) ty_vars vars
               in
               let vars = List.rev vars in
               let ty = TyAll (all_vars, TyFun (List.rev ty_vars, ty_res)) in
               let fund = line, gflag, ty, vars, e in
                  SymbolTable.add fundefs f fund
            else
               fundefs) SymbolTable.empty fundefs
   in

   (* Prune initialization *)
   let body = dead_exp types funs live body in
      { prog_file = file;
        prog_globals = globals;
        prog_decls = decls;
        prog_export = export;
        prog_import = import;
        prog_types = types;
        prog_frames = frames;
        prog_funs = fundefs;
        prog_body = body
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
