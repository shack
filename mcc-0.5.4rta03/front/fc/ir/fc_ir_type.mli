(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_ir
open Fc_ir_env
open Fc_ir_pos

(*
 * Pervasive types.
 *)
val ty_malloc : ty

(*
 * Get the atom type.
 *)
val var_of_atom : pos -> atom -> var
val type_of_atom : venv -> pos -> atom -> ty

(*
 * Eliminate TyApply.
 *)
val apply_type : tenv -> pos -> var -> ty list -> ty
val tenv_expand : tenv -> pos -> ty -> ty

(*
 * Type destructors.
 *)
val is_scalar_type : tenv -> pos -> ty -> bool
val is_aggregate_type : tenv -> pos -> ty -> bool
val is_fun_type : tenv -> pos -> ty -> bool
val is_closure_type : tenv -> pos -> ty -> bool
val is_env_type : tenv -> pos -> ty -> bool
val is_pointer_type : tenv -> pos -> ty -> bool
val is_pointer_nonenv_type : tenv -> pos -> ty -> bool
val is_pointer_nonfun_type : tenv -> pos -> ty -> bool

val dest_all_type : tenv -> pos -> ty -> var list * ty
val dest_fun_type : tenv -> pos -> ty -> var list * ty list * ty
val dest_closure_type : tenv -> pos -> ty -> var list * ty list * ty
val dest_pointer_type : tenv -> pos -> ty -> ty

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
