(*
 * Utilities for the IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fc_config

open Fc_ir
open Fc_ir_ds

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Default tabstop.
 *)
let tabstop = Fir_state.tabstop

(*
 * Separated list of fields.
 *)
let pp_print_sep_list buf sep printer l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       pp_print_string buf sep;
                       pp_print_space buf ()
                    end;
                 pp_open_hvbox buf tabstop;
                 printer buf x;
                 pp_close_box buf ();
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_pre_sep_list buf sep printer l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       pp_print_space buf ();
                       pp_print_string buf sep
                    end;
                 pp_open_hvbox buf tabstop;
                 printer buf x;
                 pp_close_box buf ();
                 false) true l);
   pp_close_box buf ()

(*
 * Prefixed list of items.
 *)
let pp_print_prefix_list buf sep printer l =
   pp_open_hvbox buf 0;
   List.iter (fun x ->
         pp_print_string buf sep;
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         printer buf x;
         pp_close_box buf ()) l;
   pp_close_box buf ()

(*
 * Print precisions.
 *)
let pp_print_int_precision buf pre signed =
   let s =
      match pre, signed with
         Rawint.Int8, false -> "unsigned char"
       | Rawint.Int8, true -> "signed char"
       | Rawint.Int16, false -> "unsigned short"
       | Rawint.Int16, true -> "short"
       | Rawint.Int32, false -> "unsigned"
       | Rawint.Int32, true -> "int"
       | Rawint.Int64, false -> "unsigned long"
       | Rawint.Int64, true -> "long"
   in
      pp_print_string buf s

let pp_print_float_precision buf pre =
   let s =
      match pre with
         Rawfloat.Single -> "float"
       | Rawfloat.Double -> "double"
       | Rawfloat.LongDouble -> "long double"
   in
      pp_print_string buf s

let pp_print_volatile buf p =
   if p then
      pp_print_string buf "volatile "

(*
 * Print a frame label.
 *)
let pp_print_frame_label buf (frame, field, subfield) =
   pp_print_string buf ".[";
   pp_print_symbol buf frame;
   pp_print_string buf ",";
   pp_print_symbol buf field;
   pp_print_string buf ",";
   pp_print_symbol buf subfield;
   pp_print_string buf "]"

(*
 * A basic arithmetic expression.
 *)
let prec_none   = 0
let prec_fun    = 1
let prec_struct = 2
let prec_array  = 3

(*
 * Print a type.
 * Use precedences to reduce the
 * amount of parentheses.
 *)
let rec pp_print_type buf pre = function
   TyUnit 0 ->
      pp_print_string buf "void"
 | TyUnit 1 ->
      pp_print_string buf "unit"
 | TyUnit 2 ->
      pp_print_string buf "bool"
 | TyUnit i ->
      pp_print_string buf "enum{";
      pp_print_int buf i;
      pp_print_string buf "}"
 | TyInt (pre, signed) ->
      pp_print_int_precision buf pre signed
 | TyFloat pre ->
      pp_print_float_precision buf pre
 | TyRawData i ->
      pp_print_string buf "rawdata[";
      pp_print_int buf i;
      pp_print_string buf "]"
 | TyTag ->
      pp_print_string buf "$tag"
 | TyPointer ty ->
      pp_print_string buf "*";
      pp_print_type buf prec_array ty
 | TyFun (ty_vars, ty_res) ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(";
      pp_open_hvbox buf 0;
      pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_vars;
      pp_print_string buf ") ->";
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_type buf prec_fun ty_res;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyVar v ->
      pp_print_string buf "'";
      pp_print_symbol buf v
 | TyLambda (ty_vars, ty) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(lambda (";
      pp_print_sep_list buf "," (fun buf v ->
            pp_print_string buf "'";
            pp_print_symbol buf v) ty_vars;
      pp_print_string buf ").";
      pp_print_space buf ();
      pp_print_type buf prec_none ty;
      pp_print_string buf ")";
      pp_close_box buf ()
 | TyAll (ty_vars, ty) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(all (";
      pp_print_sep_list buf "," (fun buf v ->
            pp_print_string buf "'";
            pp_print_symbol buf v) ty_vars;
      pp_print_string buf ").";
      pp_print_space buf ();
      pp_print_type buf prec_none ty;
      pp_print_string buf ")";
      pp_close_box buf ()
 | TyApply (v, ty_list) ->
      pp_open_hvbox buf tabstop;
      pp_print_symbol buf v;
      if ty_list <> [] then
         begin
            pp_print_string buf "[";
            pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_list;
            pp_print_string buf "]"
         end;
      pp_close_box buf ()
 | TyDelayed ->
      pp_print_string buf "delayed"
 | TyFrame label ->
      pp_print_string buf "frame{";
      pp_print_symbol buf label;
      pp_print_string buf "}"

and pp_print_struct buf name fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf name;
   pp_print_string buf " {";
   List.iter (fun (v, ty, i_opt) ->
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf " ";
         pp_print_symbol buf v;
         (match i_opt with
             Some i ->
                pp_print_string buf " : ";
                pp_print_int buf i
           | None ->
                ());
         pp_print_string buf ";") fields;
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_enum buf fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "enum {";
   List.iter (fun (v, ty_opt, i) ->
         pp_print_space buf ();
         (match ty_opt with
             Some ty ->
                pp_print_type buf prec_none ty;
                pp_print_string buf " "
           | None ->
                ());
         pp_print_symbol buf v;
         pp_print_string buf " = ";
         pp_print_string buf (Int32.to_string i);
         pp_print_string buf ";") fields;
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_tuple buf fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "tuple {";
   List.iter (fun ty ->
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf ";") fields;
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

(*
 * Atoms.
 *)
and pp_print_atom buf = function
   AtomUnit (n, i) ->
      pp_print_int buf i;
      pp_print_string buf "{";
      pp_print_int buf n;
      pp_print_string buf "}"
 | AtomInt i ->
      pp_print_string buf (Rawint.to_string i)
 | AtomFloat x ->
      pp_print_string buf (Rawfloat.to_string x)
 | AtomNil ty ->
      pp_print_string buf "nil[";
      pp_print_type buf prec_none ty;
      pp_print_string buf "]"
 | AtomVar v ->
      pp_print_symbol buf v
 | AtomLabel label ->
      pp_print_frame_label buf label


let pp_print_type buf = pp_print_type buf prec_none

(*
 * Global flag.
 *)
let string_of_unop = function
   UMinusIntOp _
 | UMinusFloatOp _   -> "-"
 | AbsFloatOp _      -> "abs"
 | SinOp _	     -> "sin"
 | CosOp _	     -> "cos"
 | SqrtOp _	     -> "sqrt"
 | NotIntOp _        -> "~"
 | IntOfFloat _
 | IntOfUnit _
 | IntOfPointer _    -> "(int)"
 | FloatOfInt _
 | FloatOfFloat _    -> "(float)"
 | PointerOfInt _    -> "(void *)"
 | EqPointerNilOp   -> "==nil"
 | NeqPointerNilOp   -> "!=nil"
 | IntOfInt (dpre, dsigned, spre, ssigned) ->
      let get_precision = function
         Rawint.Int8,  true  -> "int8"
       | Rawint.Int8,  false -> "uint8"
       | Rawint.Int16, true  -> "int16"
       | Rawint.Int16, false -> "uint16"
       | Rawint.Int32, true  -> "int32"
       | Rawint.Int32, false -> "uint32"
       | Rawint.Int64, true  -> "int64"
       | Rawint.Int64, false -> "uint64"
      in
         "(rawint " ^ get_precision (dpre, dsigned) ^ "_of_" ^ get_precision (spre, ssigned) ^ ")"
 | BitFieldOp (_, _, off, len) ->
      sprintf "field{off:%d; len:%d}" off len

let string_of_binop = function
   PlusPointerOp     -> "+*"
 | PlusIntOp _
 | PlusFloatOp _     -> "+"
 | MinusPointerOp    -> "-*"
 | MinusIntOp _
 | MinusFloatOp _    -> "-"
 | MulIntOp _
 | MulFloatOp _      -> "*"
 | DivIntOp _
 | DivFloatOp _      -> "/"
 | RemIntOp _
 | RemFloatOp _      -> "%"
 | SlIntOp _         -> "<<"
 | SrIntOp _         -> ">>"
 | AndBoolOp         -> "and"
 | AndIntOp _        -> "&"
 | OrBoolOp          -> "or"
 | OrIntOp _         -> "|"
 | XorIntOp _        -> "^"
 | EqPointerOp       -> "==*"
 | EqIntOp _         -> "==i"
 | EqFloatOp _       -> "==."
 | EqTagOp           -> "==t"
 | NeqPointerOp      -> "!=*"
 | NeqFloatOp _      -> "!=."
 | NeqIntOp _        -> "!=i"
 | LtPointerOp       -> "<*"
 | LtIntOp _
 | LtFloatOp _       -> "<"
 | LePointerOp       -> "<=*"
 | LeIntOp _
 | LeFloatOp _       -> "<="
 | GtPointerOp       -> ">*"
 | GtIntOp _
 | GtFloatOp _       -> ">"
 | GePointerOp       -> ">=*"
 | GeFloatOp _
 | GeIntOp _         -> ">="
 | Atan2Op _         -> "atan2"
 | MinIntOp _
 | MinFloatOp _      -> "min"
 | MaxIntOp _
 | MaxFloatOp _      -> "max"
 | SetBitFieldOp (_, _, off, len) ->
      sprintf "setfield{off:%d; len:%d}" off len

(*
 * Print operators.
 *)
let pp_print_unop buf op a =
   fprintf buf "(%s " (string_of_unop op);
   pp_print_atom buf a;
   pp_print_char buf ')'

let pp_print_binop buf op a1 a2 =
   pp_print_char buf '(';
   pp_print_atom buf a1;
   fprintf buf " %s " (string_of_binop op);
   pp_print_atom buf a2;
   pp_print_char buf ')'

(*
 * Global flag.
 *)
let pp_print_gflag buf gflag =
   match gflag with
      VarGlobalClass ->
         pp_print_string buf "$global "
    | VarLocalClass ->
         pp_print_string buf "$local "
    | VarContClass ->
         pp_print_string buf "$cont "

(*
 * Debug info.
 *)
let pp_print_debug_line buf loc =
   pp_print_location buf loc

let pp_print_debug_vars buf info =
   pp_print_string buf "# Debug vars: ";
   pp_print_sep_list buf "," (fun buf (v1, ty, v2) ->
         pp_print_symbol buf v1;
         pp_print_string buf "=";
         pp_print_symbol buf v2;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf ty) info

let pp_print_debug buf info =
   match info with
      DebugString s ->
         pp_print_string buf "# ";
         pp_print_string buf s
    | DebugContext (line, vars) ->
         pp_print_debug_line buf line;
         pp_print_space buf ();
         pp_print_debug_vars buf vars

(*
 * "let" opener.
 *)
let pp_print_let_open buf v ty =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_symbol buf v;
   pp_print_string buf " :";
   pp_print_space buf ();
   pp_print_type buf ty;
   pp_print_string buf " =";
   pp_print_space buf ()

let pp_print_let_vars_open buf vars v ty =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_sep_list buf "," (fun buf v ->
         pp_print_string buf "'";
         pp_print_symbol buf v) vars;
   if vars <> [] then
      pp_print_string buf ", ";
   pp_print_symbol buf v;
   pp_print_string buf " :";
   pp_print_space buf ();
   pp_print_type buf ty;
   pp_print_string buf " =";
   pp_print_space buf ()

let rec pp_print_let_close buf e =
   pp_print_string buf " in";
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_expr buf e

(*
 * Print an expression.
 *)
and pp_print_expr buf e =
   match dest_exp_core e with
      LetFuns (funs, e) ->
         pp_print_string buf "let ";
         pp_print_sep_list buf "and " pp_print_function funs;
         pp_print_string buf " in";
         pp_print_space buf ();
         pp_print_expr buf e

    | LetAtom (v, ty, a, e) ->
         pp_print_let_open buf v ty;
         pp_print_atom buf a;
         pp_print_let_close buf e

    | LetUnop (v, ty, op, a, e) ->
         pp_print_let_open buf v ty;
         pp_print_unop buf op a;
         pp_print_let_close buf e

    | LetBinop (v, ty, op, a1, a2, e) ->
         pp_print_let_open buf v ty;
         pp_print_binop buf op a1 a2;
         pp_print_let_close buf e

    | LetExtCall (v, ty, s, b, ty', args, e) ->
         pp_print_let_open buf v ty;
         pp_print_string buf "(";
         if b then
            fprintf buf "gc:\"%s\"" (String.escaped s)
         else
            fprintf buf "\"%s\"" (String.escaped s);
         pp_print_type buf ty';
         pp_print_string buf ")(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf ")";
         pp_print_let_close buf e

    | LetClosure (v, f, vars, e) ->
         pp_print_let_open buf v TyDelayed;
         pp_print_string buf "closure(";
         pp_print_sep_list buf "," pp_print_atom (AtomVar f :: vars);
         pp_print_string buf ")";
         pp_print_let_close buf e

    | TailCall (TailNormal (f, args)) ->
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf ")"

    | TailCall (TailSysMigrate (f, host, args)) ->
         pp_print_string buf "$sysmigrate[";
         pp_print_sep_list buf "," pp_print_atom host;
         pp_print_string buf "](";
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf "))"

    | TailCall (TailAtomic (f, c, args)) ->
         pp_print_string buf "$atomic[";
         pp_print_atom buf c;
         pp_print_string buf "](";
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf "))"

    | TailCall (TailAtomicCommit (level, f, args)) ->
         pp_print_string buf "$atomiccommit(";
         pp_print_atom buf level;
         pp_print_string buf ", ";
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf "))"

    | TailCall (TailAtomicRollback (level, c)) ->
         pp_print_string buf "$atomicrollback[";
         pp_print_sep_list buf "," pp_print_atom [level; c];
         pp_print_string buf "]"

    | LetMalloc (v, a, e) ->
         pp_print_let_open buf v TyDelayed;
         pp_print_string buf "malloc(";
         pp_open_hvbox buf 0;
         pp_print_atom buf a;
         pp_close_box buf ();
         pp_print_string buf ")";
         pp_print_let_close buf e

    | IfThenElse (a, e1, e2) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "if ";
         pp_print_atom buf a;
         pp_print_string buf " then";
         pp_print_space buf ();
         pp_print_expr buf e1;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_string buf "else";
         pp_print_space buf ();
         pp_print_expr buf e2;
         pp_close_box buf ()

    | LetSubscript (v, ty, v2, a3, e) ->
         pp_print_let_open buf v ty;
         pp_print_symbol buf v2;
         pp_print_string buf "[";
         pp_print_atom buf a3;
         pp_print_string buf "]";
         pp_print_let_close buf e

    | SetSubscript (v1, a2, ty, a3, e) ->
         pp_print_symbol buf v1;
         pp_print_string buf "[";
         pp_print_atom buf a2;
         pp_print_string buf "] : ";
         pp_print_type buf ty;
         pp_print_string buf " <- ";
         pp_print_atom buf a3;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_expr buf e

    | SetGlobal (v1, ty, a2, e) ->
         pp_print_symbol buf v1;
         pp_print_string buf " : ";
         pp_print_type buf ty;
         pp_print_string buf " <-g ";
         pp_print_atom buf a2;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_expr buf e

    | Memcpy (v1, a1, v2, a2, a3, e) ->
         pp_print_string buf "memcpy(";
         pp_print_symbol buf v1;
         pp_print_string buf "+";
         pp_print_atom buf a1;
         pp_print_string buf ", ";
         pp_print_symbol buf v2;
         pp_print_string buf "+";
         pp_print_atom buf a2;
         pp_print_string buf ", ";
         pp_print_atom buf a3;
         pp_print_string buf ");";
         pp_print_space buf ();
         pp_print_expr buf e

    | LetProject (v1, ty, v2, label, e) ->
         pp_print_let_open buf v1 ty;
         pp_print_symbol buf v2;
         pp_print_frame_label buf label;
         pp_print_let_close buf e

    | LetAddrOfProject (v1, ty, v2, label, e) ->
         pp_print_let_open buf v1 ty;
         pp_print_string buf "&";
         pp_print_symbol buf v2;
         pp_print_frame_label buf label;
         pp_print_let_close buf e

    | SetProject (v1, label, ty, a4, e) ->
         pp_print_symbol buf v1;
         pp_print_frame_label buf label;
         pp_print_string buf "] : ";
         pp_print_type buf ty;
         pp_print_string buf " <- ";
         pp_print_atom buf a4;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_expr buf e

    | LetVar (v, ty, a_opt, e) ->
         pp_print_let_open buf v ty;
         (match a_opt with
             Some a ->
                pp_print_string buf "copy(";
                pp_print_atom buf a;
                pp_print_string buf ")"
           | None ->
                pp_print_string buf "uninitialized");
         pp_print_let_close buf e

    | LetFrame (v, ty, e) ->
         pp_print_let_open buf v ty;
         pp_print_string buf "frame";
         pp_print_let_close buf e

    | LetAddrOfVar (v1, ty, v2, e) ->
         pp_print_let_open buf v1 ty;
         pp_print_string buf "&";
         pp_print_symbol buf v2;
         pp_print_let_close buf e

    | SetVar (v, ty, a, e) ->
         pp_print_symbol buf v;
         pp_print_string buf " : ";
         pp_print_type buf ty;
         pp_print_string buf " <- ";
         pp_print_atom buf a;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_expr buf e

    | Debug (info, e) ->
         pp_print_debug buf info;
         pp_print_space buf ();
         pp_print_expr buf e

and pp_print_function buf (f, debug, gflag, f_ty, vars, body) =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "(";
   pp_print_gflag buf gflag;
   pp_print_symbol buf f;
   pp_print_string buf " : ";
   pp_print_type buf f_ty;
   pp_print_string buf ")(";
   pp_print_sep_list buf "," pp_print_symbol vars;
   pp_print_string buf ") =";
   pp_print_space buf ();
   pp_print_debug_line buf debug;
   pp_print_space buf ();
   pp_print_expr buf body;
   pp_close_box buf ()

(*
 * Wrap in a box.
 *)
let pp_print_expr buf exp =
   pp_open_vbox buf 0;
   pp_print_expr buf exp;
   pp_close_box buf ()

(*
 * Initialization.
 *)
let pp_print_init buf = function
   InitString (pre, s) ->
      let pre =
         match pre with
            Rawint.Int8 -> "char "
          | Rawint.Int16 -> "wchar_t "
          | Rawint.Int32 -> "unsigned int "
          | Rawint.Int64 -> "unsigned long "
      in
         pp_print_string buf pre;
         Fc_parse_print.pp_print_parse_string buf s
 | InitAtom a ->
      pp_print_atom buf a
 | InitBlock i ->
      pp_print_string buf "block[";
      pp_print_int buf i;
      pp_print_string buf "]"

(*
 * Print a frame.
 *)
let pp_print_frame buf frame =
   SymbolTable.iter (fun v subfields ->
         fprintf buf "@ @[<hv 0>@[<hv 3>";
         pp_print_symbol buf v;
         fprintf buf " {";
         List.iter (fun (v, ty) ->
               fprintf buf "@ @[<hv 3>";
               pp_print_symbol buf v;
               fprintf buf " :@ ";
               pp_print_type buf ty;
               fprintf buf ";@]") subfields;
         fprintf buf "@]@ }@]") frame

(*
 * Print a program.
 *)
let pp_print_prog buf prog =
   let { prog_globals = globals;
         prog_export = export;
         prog_import = import;
         prog_types = types;
         prog_funs = funs;
         prog_decls = decls;
         prog_frames = frames;
         prog_body = body
       } = prog
   in
      pp_open_vbox buf 0;

      (* Print the types *)
      pp_open_hvbox buf tabstop;
      pp_print_string buf "Types:";
      SymbolTable.iter (fun v ty ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_symbol buf v;
            pp_print_string buf " =";
            pp_print_space buf ();
            pp_print_type buf ty;
            pp_close_box buf ()) types;
      pp_close_box buf ();

      (* Print the frames *)
      fprintf buf "@ @[<v 3>Frames:";
      SymbolTable.iter (fun v frame ->
            fprintf buf "@ @[<hv 0>@[<hv 3>";
            pp_print_symbol buf v;
            fprintf buf " {";
            pp_print_frame buf frame;
            fprintf buf "@]@ }@]") frames;
      fprintf buf "@]";

      (* Print the types *)
      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Import:";
      SymbolTable.iter (fun v info ->
            let { import_name = s;
                  import_type = ty;
                  import_info = info
                } = info
            in
               pp_print_space buf ();
               pp_open_hvbox buf tabstop;
               pp_print_symbol buf v;
               pp_print_string buf (" = \"" ^ String.escaped s ^ "\"");
               pp_print_space buf ();
               pp_print_string buf ": ";
               pp_print_type buf ty;
               pp_print_space buf ();
               pp_print_string buf ": ";
               Fir_print.pp_print_import_info buf info;
               pp_close_box buf ()) import;
      pp_close_box buf ();

      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Export:";
      SymbolTable.iter (fun v info ->
            let { export_name = s;
                  export_type = ty
                } = info
            in
               pp_print_space buf ();
               pp_open_hvbox buf tabstop;
               pp_print_symbol buf v;
               pp_print_string buf (" = \"" ^ String.escaped s ^ "\"");
               pp_print_space buf ();
               pp_print_string buf ": ";
               pp_print_type buf ty;
               pp_close_box buf ()) export;
      pp_close_box buf ();

      (* Print the globals *)
      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Globals:";
      SymbolTable.iter (fun v (ty, volp, init) ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_volatile buf volp;
            pp_print_symbol buf v;
            pp_print_string buf " : ";
            pp_print_type buf ty;
            pp_print_string buf " =";
            pp_print_space buf ();
            pp_print_init buf init;
            pp_close_box buf ()) globals;
      pp_close_box buf ();

      (* Print the declarations *)
      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Decls:";
      SymbolTable.iter (fun v ty ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_symbol buf v;
            pp_print_string buf " : ";
            pp_print_type buf ty;
            pp_close_box buf ()) decls;
      pp_close_box buf ();

      (* Print the functions *)
      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Functions:";
      SymbolTable.iter (fun f (line, gflag, ty, vars, e) ->
            pp_print_space buf ();
            pp_print_function buf (f, line, gflag, ty, vars, e)) funs;
      pp_close_box buf ();

      (* Print the body *)
      pp_print_space buf ();
      pp_print_string buf "Body:";
      pp_print_space buf ();
      pp_print_expr buf body;

      (* Terminate the outermost box *)
      pp_close_box buf ()

(*
 * Debug version.
 *)
let debug_prog debug prog =
   let buf = err_formatter in
      pp_set_margin buf 120;
      pp_open_vbox buf 0;
      pp_print_string buf "*** IR: ";
      pp_print_string buf debug;
      pp_print_space buf ();
      pp_print_prog buf prog;
      pp_close_box buf ();
      pp_print_newline buf ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
