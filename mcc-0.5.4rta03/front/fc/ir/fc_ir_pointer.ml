(*
 * Pointer arithmetic elimination.
 *
 * All pointers and closures are passed as pairs.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_ir
open Fc_ir_ds
open Fc_ir_env
open Fc_ir_exn
open Fc_ir_pos
open Fc_ir_type

module Pos = MakePos (struct let name = "Fc_ir_pointer" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Zero as an offset.
 *)
let zero_off = AtomInt (Rawint.of_int Rawint.Int32 true 0)

(*
 * Pointer environment.
 *)
type ptr_entry =
   ClosureVar of var
 | PointerVar of var

type penv =
   { penv_vars : ptr_entry SymbolTable.t;
     penv_labels : ptr_entry SymbolListTable.t
   }

let penv_empty =
   { penv_vars = SymbolTable.empty;
     penv_labels = SymbolListTable.empty
   }

let penv_add_var penv v e =
   { penv with penv_vars = SymbolTable.add penv.penv_vars v e }

let penv_add_label penv (frame, field, subfield) e =
   { penv with penv_labels = SymbolListTable.add penv.penv_labels [frame; field; subfield] e }

let penv_lookup_pointer penv pos v =
   let pos = string_pos "penv_lookup_pointer" pos in
      try
         match SymbolTable.find penv.penv_vars v with
            PointerVar v ->
               v
          | ClosureVar _ ->
               raise (IRException (pos, StringVarError ("closure is not a pointer", v)))
      with
         Not_found ->
            raise (IRException (pos, StringVarError ("not a pointer", v)))

let penv_lookup_atom_pointer penv pos a =
   match a with
      AtomVar v ->
         penv_lookup_pointer penv pos v
    | _ ->
         raise (IRException (pos, StringAtomError ("not a var", a)))

let penv_lookup_closure penv pos v =
   let pos = string_pos "penv_lookup_closure" pos in
      try
         match SymbolTable.find penv.penv_vars v with
            ClosureVar v -> v
          | PointerVar _ -> raise (IRException (pos, StringError "not a closure"))
      with
         Not_found ->
            raise (IRException (pos, StringError "not a closure"))

let penv_lookup_atom_closure penv pos a =
   let pos = string_pos "penv_lookup_atom_closure" pos in
      match a with
         AtomVar v ->
            penv_lookup_closure penv pos v
       | _ ->
            raise (IRException (pos, StringAtomError ("not a var", a)))

let penv_lookup_label_pointer penv pos (frame, field, subfield) =
   let pos = string_pos "penv_lookup_label_pointer" pos in
      try
         match SymbolListTable.find penv.penv_labels [frame; field; subfield] with
            PointerVar v ->
               v
          | ClosureVar _ ->
               raise (IRException (pos, StringError "label is not a pointer"))
      with
         Not_found ->
            raise (IRException (pos, StringError "not a pointer"))

let penv_lookup_label_closure penv pos (frame, field, subfield) =
   let pos = string_pos "penv_lookup_label_closure" pos in
      try
         match SymbolListTable.find penv.penv_labels [frame; field; subfield] with
            ClosureVar v -> v
          | PointerVar _ -> raise (IRException (pos, StringError "not a closure"))
      with
         Not_found ->
            raise (IRException (pos, StringError "not a closure"))
(*
 * Generic indexes.
 *)
let pre_off = Rawint.Int32
let signed_off = true
let ty_off = TyInt (pre_off, true)

let sizeof_pointer = AtomInt (Rawint.of_int pre_off signed_off 4)

let ty_bool = TyUnit 2

let zero = Rawint.of_int pre_off signed_off 0
let zero_off = AtomInt zero

let plus_op  = PlusIntOp  (pre_off, signed_off)
let minus_op = MinusIntOp (pre_off, signed_off)
let eq_op    = EqIntOp    (pre_off, signed_off)
let neq_op   = NeqIntOp   (pre_off, signed_off)
let lt_op    = LtIntOp    (pre_off, signed_off)
let le_op    = LeIntOp    (pre_off, signed_off)
let gt_op    = GtIntOp    (pre_off, signed_off)
let ge_op    = GeIntOp    (pre_off, signed_off)

(*
 * Translate pointer ops.
 *)
let int_op_of_ptr_op pos = function
   PlusPointerOp  -> plus_op
 | MinusPointerOp -> minus_op
 | EqPointerOp    -> eq_op
 | NeqPointerOp   -> neq_op
 | LtPointerOp    -> lt_op
 | LePointerOp    -> le_op
 | GtPointerOp    -> gt_op
 | GePointerOp    -> ge_op
 | _ -> raise (IRException (pos, StringError "not a pointer operation"))

(************************************************************************
 * POINTER SEARCH
 ************************************************************************)

(*
 * Add a closure variable to the pointer environment.
 *)
let add_closure penv v =
   let v' = new_symbol v in
      penv_add_var penv v (ClosureVar v')

let add_pointer penv v =
   let v' = new_symbol v in
      penv_add_var penv v (PointerVar v')

let add_label_closure penv label =
   let _, _, v = label in
   let v' = new_symbol v in
      penv_add_label penv label (ClosureVar v')

let add_label_pointer penv label =
   let _, _, v = label in
   let v' = new_symbol v in
      penv_add_label penv label (PointerVar v')

(*
 * Add a variable if it is a pointer or closure.
 *)
let search_var tenv penv pos v ty =
   let pos = string_pos "search_var" pos in
      if is_closure_type tenv pos ty then
         add_closure penv v
      else if is_pointer_nonenv_type tenv pos ty then
         add_pointer penv v
      else
         penv

(*
 * Add a variable only if it is a pointer.
 *)
let search_var_ptr tenv penv pos v ty =
   let pos = string_pos "search_var" pos in
      if is_pointer_nonenv_type tenv pos ty then
         add_pointer penv v
      else
         penv

(*
 * Classify the frame labels.
 *)
let search_frame tenv penv pos frame fields =
   let pos = string_pos "search_frame" pos in
      SymbolTable.fold (fun penv field vars ->
            List.fold_left (fun penv (subfield, ty) ->
                  if is_closure_type tenv pos ty then
                     add_label_closure penv (frame, field, subfield)
                  else if is_pointer_nonenv_type tenv pos ty then
                     add_label_pointer penv (frame, field, subfield)
                  else
                     penv) penv vars) penv fields

(*
 * Look through all the code for pointers and closures.
 *)
let rec search_exp tenv penv e =
   let pos = string_pos "search_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, _, e)
       | LetUnop (v, ty, _, _, e)
       | LetBinop (v, ty, _, _, _, e)
       | LetExtCall (v, ty, _, _, _, _, e)
       | LetSubscript (v, ty, _, _, e)
       | LetProject (v, ty, _, _, e)
       | LetAddrOfProject (v, ty, _, _, e)
       | LetVar (v, ty, _, e)
       | LetFrame (v, ty, e)
       | LetAddrOfVar (v, ty, _, e) ->
            search_var_exp tenv penv pos v ty e
       | LetMalloc (v, _, e) ->
            search_var_exp tenv penv pos v ty_malloc e
       | LetClosure (v, _, _, e) ->
            let penv = add_closure penv v in
               search_exp tenv penv e
       | TailCall _ ->
            penv
       | IfThenElse (_, e1, e2) ->
            search_exp tenv (search_exp tenv penv e1) e2
       | SetGlobal (_, _, _, e)
       | SetSubscript (_, _, _, _, e)
       | Memcpy (_, _, _, _, _, e)
       | SetProject (_, _, _, _, e)
       | SetVar (_, _, _, e)
       | Debug (_, e) ->
            search_exp tenv penv e
       | LetFuns _ ->
            raise (IRException (pos, StringError "unexpected LetFuns"))

(*
 * A generic variable definition.
 *)
and search_var_exp tenv penv pos v ty e =
   let penv = search_var tenv penv pos v ty in
      search_exp tenv penv e

(*
 * Classify all the variables in the program.
 *)
let search_prog prog =
   let { prog_globals = globals;
         prog_import = import;
         prog_frames = frames;
         prog_types = tenv;
         prog_funs = funs;
         prog_body = body
       } = prog
   in

   (* Add extra fields to frames *)
   let penv =
      SymbolTable.fold (fun penv v frame ->
            search_frame tenv penv (var_exp_pos v) v frame) penv_empty frames
   in

   (* Classify all the globals *)
   let penv =
      SymbolTable.fold (fun penv v (ty, _, _) ->
            search_var tenv penv (var_exp_pos v) v ty) penv globals
   in

   (* Classify all the imports *)
   let penv =
      SymbolTable.fold (fun penv v { import_type = ty } ->
            search_var_ptr tenv penv (var_exp_pos v) v ty) penv import
   in

   (* Look through all the functions *)
   let penv =
      SymbolTable.fold (fun penv f (_, _, ty, vars, e) ->
            let pos = string_pos "search_prog" (exp_pos e) in
            let _, ty_vars, _ = dest_fun_type tenv pos ty in
            let penv =
               List.fold_left2 (fun penv v ty ->
                     search_var tenv penv pos v ty) penv vars ty_vars
            in
               search_exp tenv penv e) penv funs
   in

   (* Classify the vars in the initialization *)
   let penv = search_exp tenv penv body in
      penv

(************************************************************************
 * CONVERSION
 ************************************************************************)

(*
 * Actually add the extra fields to the types.
 *)
let rec ptr_type tenv penv pos ty =
   match ty with
      TyUnit _
    | TyDelayed
    | TyTag
    | TyInt _
    | TyFloat _
    | TyVar _
    | TyRawData _
    | TyFrame _ ->
         ty
    | TyPointer ty ->
         TyPointer (ptr_type tenv penv pos ty)
    | TyFun (ty_args, ty_res) ->
         let vars, ty_args = ptr_type_list tenv penv pos ty_args in
         let ty_res = ptr_type tenv penv pos ty_res in
         let ty = TyFun (ty_args, ty_res) in
         let ty =
            match vars with
               [] -> ty
             | _ -> TyAll (vars, ty)
         in
            ty
    | TyLambda (vars, ty) ->
         TyLambda (vars, ptr_type tenv penv pos ty)
    | TyAll (vars, ty) ->
         TyAll (vars, ptr_type tenv penv pos ty)
    | TyApply (v, tyl) ->
         TyApply (v, List.map (ptr_type tenv penv pos) tyl)

and ptr_type_list tenv penv pos = function
   ty :: tyl ->
      let vars, tyl = ptr_type_list tenv penv pos tyl in
         if is_closure_type tenv pos ty then
            let ty = ptr_type tenv penv pos ty in
            let all_vars, ty_vars, ty_res = dest_closure_type tenv pos ty in
            let v_env = new_symbol_string "env" in
            let ty_env = TyVar v_env in
            let ty_fun = TyAll (all_vars, TyFun (ty_env :: ty_vars, ty_res)) in
               v_env :: vars, ty_fun :: ty_env :: tyl
         else if is_pointer_nonenv_type tenv pos ty then
            let ty = ptr_type tenv penv pos ty in
               vars, ty :: ty_off :: tyl
         else
            let ty = ptr_type tenv penv pos ty in
               vars, ty :: tyl
 | [] ->
      [], []

and ptr_type_fun tenv penv pos v_env ty =
   let ty = ptr_type tenv penv pos ty in
   let all_vars, ty_vars, ty_res = dest_closure_type tenv pos ty in
   let ty_env = TyVar v_env in
      TyAll (all_vars, TyFun (ty_env :: ty_vars, ty_res))

(*
 * Add the extra fields to the frame.
 *)
let ptr_frame tenv penv pos frame fields =
   let pos = string_pos "ptr_type_frame" pos in
      SymbolTable.mapi (fun field vars ->
            let vars =
               List.fold_left (fun vars (v, ty) ->
                     try
                        match SymbolListTable.find penv.penv_labels [frame; field; v] with
                           ClosureVar v' ->
                              let field1 = v, ptr_type_fun tenv penv pos v' ty in
                              let field2 = v', TyVar v' in
                                 field2 :: field1 :: vars
                         | PointerVar v' ->
                              let field1 = v, ptr_type tenv penv pos ty in
                              let field2 = v', ty_off in
                                 field2 :: field1 :: vars
                     with
                        Not_found ->
                           let field = v, ptr_type tenv penv pos ty in
                              field :: vars) [] vars
            in
               List.rev vars) fields

(*
 * Argument expansion.
 *)
let rec ptr_vars penv vars =
   match vars with
      v :: vars ->
         let vars = ptr_vars penv vars in
            (try
                match SymbolTable.find penv.penv_vars v with
                   ClosureVar v'
                 | PointerVar v' ->
                      v :: v' :: vars
             with
                Not_found ->
                   v :: vars)
    | [] ->
         []

(*
 * Atom expansion.
 *)
let ptr_atom penv a args =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomLabel _ ->
         a :: args
    | AtomNil _ ->
         AtomInt zero :: a :: args
    | AtomVar v ->
         try
            match SymbolTable.find penv.penv_vars v with
               ClosureVar v'
             | PointerVar v' ->
                  AtomVar v' :: a :: args
         with
            Not_found ->
               a :: args

(*
 * Add extra arguments to the argument list.
 *)
let ptr_args penv args =
   let rec collect args' = function
      a :: args ->
         collect (ptr_atom penv a args') args
    | [] ->
         List.rev args'
   in
      collect [] args

(*
 * Translate an atom that is a pointer.
 *)
let ptr_atom_pointer penv pos a =
   let pos = string_pos "ptr_atom_pointer" pos in
      match a with
         AtomUnit _
       | AtomInt _
       | AtomFloat _
       | AtomLabel _ ->
            raise (IRException (pos, StringAtomError ("not a pointer", a)))
       | AtomNil _ ->
            AtomInt zero
       | AtomVar v ->
            AtomVar (penv_lookup_pointer penv pos v)

(*
 * Translate an atom that is a closure.
 *)
let ptr_atom_closure penv pos a =
   let pos = string_pos "ptr_atom_closure" pos in
      match a with
         AtomUnit _
       | AtomInt _
       | AtomFloat _
       | AtomNil _
       | AtomLabel _ ->
            raise (IRException (pos, StringAtomError ("not a closure", a)))
       | AtomVar v ->
            let v_env = penv_lookup_closure penv pos v in
               TyVar v_env, AtomVar v_env

(*
 * Convert an expression for duplex pointers.
 *)
let rec ptr_exp tenv penv e =
   let pos = string_pos "ptr_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            ptr_atom_exp tenv penv pos loc v ty a e
       | LetUnop (v, ty, op, a, e) ->
            ptr_unop_exp tenv penv pos loc v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            ptr_binop_exp tenv penv pos loc v ty op a1 a2 e
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            ptr_extcall_exp tenv penv pos loc v ty s b ty' args e
       | LetClosure (v, f, args, e) ->
            ptr_closure_exp tenv penv pos loc v f args e
       | TailCall op ->
            ptr_tailcall_exp tenv penv pos loc op
       | LetMalloc (v, a, e) ->
            ptr_malloc_exp tenv penv pos loc v a e
       | IfThenElse (a, e1, e2) ->
            ptr_if_exp tenv penv pos loc a e1 e2
       | LetSubscript (v1, ty, v2, a3, e) ->
            ptr_subscript_exp tenv penv pos loc v1 ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            ptr_set_subscript_exp tenv penv pos loc v1 a2 ty a3 e
       | SetGlobal (v, ty, a, e) ->
            ptr_set_global_exp tenv penv pos loc v ty a e
       | Memcpy (v1, a1, v2, a2, a3, e) ->
            ptr_memcpy_exp tenv penv pos loc v1 a2 v2 a2 a3 e
       | LetProject (v1, ty, v2, v3, e) ->
            ptr_project_exp tenv penv pos loc v1 ty v2 v3 e
       | LetAddrOfProject (v1, ty, v2, v3, e) ->
            ptr_addr_of_project_exp tenv penv pos loc v1 ty v2 v3 e
       | SetProject (v1, v2, ty, a3, e) ->
            ptr_set_project_exp tenv penv pos loc v1 v2 ty a3 e
       | Debug (info, e) ->
            ptr_debug_exp tenv penv pos loc info e
       | LetVar (v, ty, a_opt, e) ->
            ptr_var_exp tenv penv pos loc v ty a_opt e
       | LetFrame (v, ty, e) ->
            ptr_frame_exp tenv penv pos loc v ty e
       | LetAddrOfVar (v1, ty, v2, e) ->
            ptr_addr_of_var_exp tenv penv pos loc v1 ty v2 e
       | SetVar _
       | LetFuns _ ->
            raise (IRException (pos, StringError "unexpected expression"))

(*
 * Eliminate pointer operations.
 *)
and ptr_atom_exp tenv penv pos loc v ty a e =
   let pos = string_pos "ptr_atom_exp" pos in
   let e = ptr_exp tenv penv e in
      if is_closure_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v_env = penv_lookup_closure penv pos v in
         let _, a_env = ptr_atom_closure penv pos a in
         let e = make_exp loc (LetAtom (v_env, TyVar v_env, a_env, e)) in
            make_exp loc (LetAtom (v, ty, a, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v_off = penv_lookup_pointer penv pos v in
         let a_off = ptr_atom_pointer penv pos a in
         let e = make_exp loc (LetAtom (v_off, ty_off, a_off, e)) in
            make_exp loc (LetAtom (v, ty, a, e))
      else
         make_exp loc (LetAtom (v, ty, a, e))

and ptr_var_exp tenv penv pos loc v ty a_opt e =
   let pos = string_pos "ptr_var_exp" pos in
      match a_opt with
         Some a ->
            ptr_atom_exp tenv penv pos loc v ty a e
       | None ->
            let e = ptr_exp tenv penv e in
               if is_closure_type tenv pos ty then
                  let ty = ptr_type tenv penv pos ty in
                  let v_env = penv_lookup_closure penv pos v in
                  let e = make_exp loc (LetVar (v_env, TyVar v_env, None, e)) in
                     make_exp loc (LetVar (v, ty, None, e))
               else if is_pointer_nonenv_type tenv pos ty then
                  let ty = ptr_type tenv penv pos ty in
                  let v_off = penv_lookup_pointer penv pos v in
                  let e = make_exp loc (LetVar (v_off, ty_off, None, e)) in
                     make_exp loc (LetVar (v, ty, None, e))
               else
                  let ty = ptr_type tenv penv pos ty in
                     make_exp loc (LetVar (v, ty, None, e))

and ptr_frame_exp tenv penv pos loc v ty e =
   let pos = string_pos "ptr_frame_exp" pos in
   let e = ptr_exp tenv penv e in
   let ty = ptr_type tenv penv pos ty in
      make_exp loc (LetFrame (v, ty, e))

and ptr_addr_of_var_exp tenv penv pos loc v1 ty v2 e =
   let pos = string_pos "ptr_addr_of_var_exp" pos in
   let e = ptr_exp tenv penv e in
   let ty = ptr_type tenv penv pos ty in
      if is_pointer_nonenv_type tenv pos ty then
         let v1_off = penv_lookup_pointer penv pos v1 in
         let e = make_exp loc (LetAtom (v1_off, ty_off, AtomInt zero, e)) in
            make_exp loc (LetAddrOfVar (v1, ty, v2, e))
      else
         make_exp loc (LetAddrOfVar (v1, ty, v2, e))

and ptr_unop_exp tenv penv pos loc v1 ty op a e =
   let pos = string_pos "ptr_unop_exp" pos in
   let ty = ptr_type tenv penv pos ty in
   let e = ptr_exp tenv penv e in
      match op, a with
         EqPointerNilOp, a2_base ->
            let a2_off = ptr_atom_pointer penv pos a2_base in
            let v_ptr = new_symbol_string "cmp_ptr" in
            let v_off = new_symbol_string "cmp_off" in
            let e = make_exp loc (LetBinop (v1, ty, AndBoolOp, AtomVar v_ptr, AtomVar v_off, e)) in
            let e = make_exp loc (LetBinop (v_off, ty_bool, eq_op, a2_off, AtomInt zero, e)) in
               make_exp loc (LetBinop (v_ptr, ty_bool, EqPointerOp, a2_base, AtomNil ty_malloc, e))
       | NeqPointerNilOp, a2_base ->
            let a2_off = ptr_atom_pointer penv pos a2_base in
            let v_ptr = new_symbol_string "cmp_ptr" in
            let v_off = new_symbol_string "cmp_off" in
            let e = make_exp loc (LetBinop (v1, ty, OrBoolOp, AtomVar v_ptr, AtomVar v_off, e)) in
            let e = make_exp loc (LetBinop (v_off, ty_bool, neq_op, a2_off, AtomInt zero, e)) in
               make_exp loc (LetBinop (v_ptr, ty_bool, NeqPointerOp, a2_base, AtomNil ty_malloc, e))
       | _ ->
            make_exp loc (LetUnop (v1, ty, op, a, e))

and ptr_binop_exp tenv penv pos loc v1 ty op a1 a2 e =
   let pos = string_pos "ptr_op_exp" pos in
   let ty = ptr_type tenv penv pos ty in
   let e = ptr_exp tenv penv e in
      match op, a1, a2 with
         PlusPointerOp, a2_base, a3 ->
            let v1_off = penv_lookup_pointer penv pos v1 in
            let a2_off = ptr_atom_pointer penv pos a2_base in
            let e = make_exp loc (LetBinop (v1_off, ty_off, plus_op, a2_off, a3, e)) in
               make_exp loc (LetAtom (v1, ty, a2_base, e))
       | MinusPointerOp, a2_base, a3_base ->
            let a2_off = ptr_atom_pointer penv pos a2_base in
            let a3_off = ptr_atom_pointer penv pos a3_base in
               make_exp loc (LetBinop (v1, ty, minus_op, a2_off, a3_off, e))
       | EqPointerOp,  a2_base, a3_base
       | LtPointerOp,  a2_base, a3_base
       | LePointerOp,  a2_base, a3_base
       | GtPointerOp,  a2_base, a3_base
       | GePointerOp,  a2_base, a3_base ->
            let a2_off = ptr_atom_pointer penv pos a2_base in
            let a3_off = ptr_atom_pointer penv pos a3_base in
            let v_ptr = new_symbol_string "cmp_ptr" in
            let v_off = new_symbol_string "cmp_off" in
            let op = int_op_of_ptr_op pos op in
            let e = make_exp loc (LetBinop (v1, ty, AndBoolOp, AtomVar v_ptr, AtomVar v_off, e)) in
            let e = make_exp loc (LetBinop (v_off, ty_bool, op, a2_off, a3_off, e)) in
               make_exp loc (LetBinop (v_ptr, ty_bool, EqPointerOp, a2_base, a3_base, e))
       | NeqPointerOp, a2_base, a3_base ->
            let a2_off = ptr_atom_pointer penv pos a2_base in
            let a3_off = ptr_atom_pointer penv pos a3_base in
            let v_ptr = new_symbol_string "cmp_ptr" in
            let v_off = new_symbol_string "cmp_off" in
            let op = int_op_of_ptr_op pos op in
            let e = make_exp loc (LetBinop (v1, ty, OrBoolOp, AtomVar v_ptr,AtomVar v_off, e)) in
            let e = make_exp loc (LetBinop (v_off, ty_bool, op, a2_off, a3_off, e)) in
               make_exp loc (LetBinop (v_ptr, ty_bool, NeqPointerOp, a2_base, a3_base, e))
       | _ ->
            make_exp loc (LetBinop (v1, ty, op, a1, a2, e))

and ptr_extcall_exp tenv penv pos loc v ty s b ty' args e =
   let pos = string_pos "ptr_extcall_exp" pos in
   let args = ptr_args penv args in
   let ty' = ptr_type tenv penv pos ty' in
   let all_vars, ty_args, ty_res = dest_fun_type tenv pos ty' in
   let ty' = TyAll (all_vars, TyFun (ty_args, ty_res)) in
   let e = ptr_exp tenv penv e in
      if is_closure_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v_env = penv_lookup_closure penv pos v in
         let e = make_exp loc (LetVar (v_env, TyVar v_env, None, e)) in
            make_exp loc (LetExtCall (v, ty, s, b, ty', args, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v_off = penv_lookup_pointer penv pos v in
         let e = make_exp loc (LetAtom (v_off, ty_off, zero_off, e)) in
            make_exp loc (LetExtCall (v, ty, s, b, ty', args, e))
      else
         let ty = ptr_type tenv penv pos ty in
            make_exp loc (LetExtCall (v, ty, s, b, ty', args, e))

(*
 * Closure is just a paring operation.
 *)
and ptr_closure_exp tenv penv pos loc v f args e =
   let pos = string_pos "ptr_closure_exp" pos in
      match args with
         [a] ->
            let v' = penv_lookup_closure penv pos v in
            let e = make_exp loc (LetAtom (v', TyVar v', a, ptr_exp tenv penv e)) in
               make_exp loc (LetAtom (v, TyDelayed, AtomVar f, e))
       | _ ->
            raise (IRException (pos, ArityMismatch (1, List.length args)))

(*
 * Add extra arguments to tailcall.
 *)
and ptr_tailcall_normal tenv penv pos loc f args =
   let pos = string_pos "ptr_tailcall_normal" pos in
   let args = ptr_args penv args in
      try
         match SymbolTable.find penv.penv_vars f with
            ClosureVar v_env ->
               make_exp loc (TailCall (TailNormal (f, AtomVar v_env :: args)))
          | PointerVar _ ->
               raise (IRException (pos, StringError "pointer should be a closure"))
      with
         Not_found ->
            make_exp loc (TailCall (TailNormal (f, args)))

and ptr_tailcall_migrate tenv penv pos loc f host args =
   let pos = string_pos "ptr_tailcall_migrate" pos in
   let host = ptr_args penv host in
   let args = ptr_args penv args in
   let e =
      try
         match SymbolTable.find penv.penv_vars f with
            ClosureVar v_env ->
               TailCall (TailSysMigrate (f, host, AtomVar v_env :: args))
          | PointerVar _ ->
               raise (IRException (pos, StringError "pointer should be a closure"))
      with
         Not_found ->
            TailCall (TailSysMigrate (f, host, args))
   in
      make_exp loc e

and ptr_tailcall_atomic tenv penv pos loc f c args =
   let pos = string_pos "ptr_tailcall_atomic" pos in
   let args = ptr_args penv args in
   let e =
      try
         match SymbolTable.find penv.penv_vars f with
            ClosureVar v_env ->
               TailCall (TailAtomic (f, c, AtomVar v_env :: args))
          | PointerVar _ ->
               raise (IRException (pos, StringError "pointer should be a closure"))
      with
         Not_found ->
            TailCall (TailAtomic (f, c, args))
   in
      make_exp loc e

and ptr_tailcall_atomic_commit tenv penv pos loc level f args =
   let pos = string_pos "ptr_tailcall_atomic_commit" pos in
   let args = ptr_args penv args in
   let e =
      try
         match SymbolTable.find penv.penv_vars f with
            ClosureVar v_env ->
               TailCall (TailAtomicCommit (level, f, AtomVar v_env :: args))
          | PointerVar _ ->
               raise (IRException (pos, StringError "pointer should be a closure"))
      with
         Not_found ->
            TailCall (TailAtomicCommit (level, f, args))
   in
      make_exp loc e

and ptr_tailcall_atomic_rollback tenv penv pos loc level c =
   let pos = string_pos "ptr_tailcall_atomic_rollback" pos in
      make_exp loc (TailCall (TailAtomicRollback (level, c)))

and ptr_tailcall_exp tenv penv pos loc op =
   let pos = string_pos "ptr_tailcall_exp" pos in
      match op with
         TailNormal (f, args) ->
            ptr_tailcall_normal tenv penv pos loc f args
       | TailSysMigrate (f, host, args) ->
            ptr_tailcall_migrate tenv penv pos loc f host args
       | TailAtomic (f, c, args) ->
            ptr_tailcall_atomic tenv penv pos loc f c args
       | TailAtomicCommit (level, f, args) ->
            ptr_tailcall_atomic_commit tenv penv pos loc level f args
       | TailAtomicRollback (level, c) ->
            ptr_tailcall_atomic_rollback tenv penv pos loc level c

(*
 * During allocation, the pointer is assigned,
 * and the offset is zero.
 *)
and ptr_malloc_exp tenv penv pos loc v_base a e =
   let pos = string_pos "ptr_malloc_exp" pos in
   let v_off = penv_lookup_pointer penv pos v_base in
   let e = ptr_exp tenv penv e in
   let e = make_exp loc (LetAtom (v_off, ty_off, AtomInt zero, e)) in
      make_exp loc (LetMalloc (v_base, a, e))

(*
 * Conditional.
 *)
and ptr_if_exp tenv penv pos loc a e1 e2 =
   let pos = string_pos "ptr_if_exp" pos in
   let e1 = ptr_exp tenv penv e1 in
   let e2 = ptr_exp tenv penv e2 in
      make_exp loc (IfThenElse (a, e1, e2))

(*
 * Subscripting.
 * When pointer/closures are accessed, we have to get the
 * two parts.
 *)
and ptr_subscript_exp tenv penv pos loc v1 ty v2_base a3 e =
   let pos = string_pos "ptr_subscript_exp" pos in
   let v2_off = penv_lookup_pointer penv pos v2_base in
   let v_off1 = new_symbol v2_base in
   let e = ptr_exp tenv penv e in
   let e =
      if is_closure_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v1_env = penv_lookup_closure penv pos v1 in
         let v_off2 = new_symbol v_off1 in
         let e = make_exp loc (LetSubscript (v1_env, TyVar v1_env, v2_base, AtomVar v_off2, e)) in
         let e = make_exp loc (LetBinop (v_off2, ty_off, plus_op, AtomVar v_off1, sizeof_pointer, e)) in
            make_exp loc (LetSubscript (v1, ty, v2_base, AtomVar v_off1, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v1_off = penv_lookup_pointer penv pos v1 in
         let v_off2 = new_symbol v_off1 in
         let e = make_exp loc (LetSubscript (v1_off, ty_off, v2_base, AtomVar v_off2, e)) in
         let e = make_exp loc (LetBinop (v_off2, ty_off, plus_op, AtomVar v_off1, sizeof_pointer, e)) in
            make_exp loc (LetSubscript (v1, ty, v2_base, AtomVar v_off1, e))
      else
         let ty = ptr_type tenv penv pos ty in
            make_exp loc (LetSubscript (v1, ty, v2_base, AtomVar v_off1, e))
   in
      make_exp loc (LetBinop (v_off1, ty_off, plus_op, AtomVar v2_off, a3, e))

and ptr_set_subscript_exp tenv penv pos loc v1_base a2 ty a3 e =
   let pos = string_pos "ptr_set_subscript_exp" pos in
   let v1_off = penv_lookup_pointer penv pos v1_base in
   let v_off1 = new_symbol v1_base in
   let e = ptr_exp tenv penv e in
   let e =
      if is_closure_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let ty_a3, a3_env = ptr_atom_closure penv pos a3 in
         let v_off2 = new_symbol v_off1 in
         let e = make_exp loc (SetSubscript (v1_base, AtomVar v_off2, ty_a3, a3_env, e)) in
         let e = make_exp loc (LetBinop (v_off2, ty_off, plus_op, AtomVar v_off1, sizeof_pointer, e)) in
            make_exp loc (SetSubscript (v1_base, AtomVar v_off1, ty, a3, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let a3_off = ptr_atom_pointer penv pos a3 in
         let v_off2 = new_symbol v_off1 in
         let e = make_exp loc (SetSubscript (v1_base, AtomVar v_off2, ty_off, a3_off, e)) in
         let e = make_exp loc (LetBinop (v_off2, ty_off, plus_op, AtomVar v_off1, sizeof_pointer, e)) in
            make_exp loc (SetSubscript (v1_base, AtomVar v_off1, ty, a3, e))
      else
         let ty = ptr_type tenv penv pos ty in
            make_exp loc (SetSubscript (v1_base, AtomVar v_off1, ty, a3, e))
   in
      make_exp loc (LetBinop (v_off1, ty_off, plus_op, AtomVar v1_off, a2, e))

and ptr_set_global_exp tenv penv pos loc v1_base ty a3 e =
   let pos = string_pos "ptr_set_global_exp" pos in
   let e = ptr_exp tenv penv e in
      if is_closure_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v1_env = penv_lookup_closure penv pos v1_base in
         let v3_env = penv_lookup_atom_closure penv pos a3 in
         let e = make_exp loc (SetGlobal (v1_env, TyVar v3_env, AtomVar v3_env, e)) in
            make_exp loc (SetGlobal (v1_base, ty, a3, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v1_off = penv_lookup_pointer penv pos v1_base in
         let v3_off = penv_lookup_atom_pointer penv pos a3 in
         let e = make_exp loc (SetGlobal (v1_off, ty_off, AtomVar v3_off, e)) in
            make_exp loc (SetGlobal (v1_base, ty, a3, e))
      else
         let ty = ptr_type tenv penv pos ty in
            make_exp loc (SetGlobal (v1_base, ty, a3, e))

and ptr_memcpy_exp tenv penv pos loc v1_base a1_off v2_base a2_off a3 e =
   let pos = string_pos "ptr_memcpy_exp" pos in
   let v1_off = penv_lookup_pointer penv pos v1_base in
   let v2_off = penv_lookup_pointer penv pos v2_base in
   let v1_off' = new_symbol v1_off in
   let v2_off' = new_symbol v2_off in
   let e = ptr_exp tenv penv e in
   let e = make_exp loc (Memcpy (v1_base, AtomVar v1_off', v2_base, AtomVar v2_off', a3, e)) in
   let e = make_exp loc (LetBinop (v2_off', ty_off, plus_op, a2_off, AtomVar v2_off, e)) in
      make_exp loc (LetBinop (v1_off', ty_off, plus_op, a1_off, AtomVar v1_off, e))

(*
 * Frame projections.
 * Frames get extra fields for pointers and closures.
 *)
and ptr_project_exp tenv penv pos loc v1 ty v2_base v3 e =
   let pos = string_pos "ptr_project_exp" pos in
   let e = ptr_exp tenv penv e in
   let frame, field, _ = v3 in
      if is_closure_type tenv pos ty then
         let v3_env = penv_lookup_label_closure penv pos v3 in
         let v3_env = frame, field, v3_env in
         let v1_env = penv_lookup_closure penv pos v1 in
         let ty = ptr_type_fun tenv penv pos v1_env ty in
         let e = make_exp loc (LetProject (v1_env, TyVar v1_env, v2_base, v3_env, e)) in
            make_exp loc (LetProject (v1, ty, v2_base, v3, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v3_off = penv_lookup_label_pointer penv pos v3 in
         let v3_off = frame, field, v3_off in
         let v1_off = penv_lookup_pointer penv pos v1 in
         let e = make_exp loc (LetProject (v1_off, ty_off, v2_base, v3_off, e)) in
            make_exp loc (LetProject (v1, ty, v2_base, v3, e))
      else
         let ty = ptr_type tenv penv pos ty in
            make_exp loc (LetProject (v1, ty, v2_base, v3, e))

and ptr_addr_of_project_exp tenv penv pos loc v1_base ty v2_base v3 e =
   let pos = string_pos "ptr_project_exp" pos in
   let ty = ptr_type tenv penv pos ty in
   let e = ptr_exp tenv penv e in
   let v1_off = penv_lookup_pointer penv pos v1_base in
   let e = make_exp loc (LetAtom (v1_off, ty_off, AtomLabel v3, e)) in
      make_exp loc (LetAtom (v1_base, TyDelayed, AtomVar v2_base, e))

and ptr_set_project_exp tenv penv pos loc v1_base v2 ty a3 e =
   let pos = string_pos "ptr_project_exp" pos in
   let e = ptr_exp tenv penv e in
   let frame, field, _ = v2 in
      if is_closure_type tenv pos ty then
         let v2_env = penv_lookup_label_closure penv pos v2 in
         let v2_label = frame, field, v2_env in
         let ty = ptr_type_fun tenv penv pos v2_env ty in
         let _, a3_env = ptr_atom_closure penv pos a3 in
         let e = make_exp loc (SetProject (v1_base, v2_label, TyVar v2_env, a3_env, e)) in
            make_exp loc (SetProject (v1_base, v2, ty, a3, e))
      else if is_pointer_nonenv_type tenv pos ty then
         let ty = ptr_type tenv penv pos ty in
         let v2_off = penv_lookup_label_pointer penv pos v2 in
         let v2_off = frame, field, v2_off in
         let a3_off = ptr_atom_pointer penv pos a3 in
         let e = make_exp loc (SetProject (v1_base, v2_off, ty_off, a3_off, e)) in
            make_exp loc (SetProject (v1_base, v2, ty, a3, e))
      else
         let ty = ptr_type tenv penv pos ty in
            make_exp loc (SetProject (v1_base, v2, ty, a3, e))

(*
 * Debugging.
 *)
and ptr_debug_exp tenv penv pos loc info e =
   let pos = string_pos "ptr_var_exp" pos in
   let e = ptr_exp tenv penv e in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Convert pointer expressions in the program.
 *)
let ptr_prog prog =
   let { prog_file = file;
         prog_globals = globals;
         prog_decls = decls;
         prog_export = export;
         prog_import = import;
         prog_frames = frames;
         prog_types = tenv;
         prog_funs = funs;
         prog_body = body
       } = prog
   in

   (* Generate the variable map *)
   let penv = search_prog prog in

   (* Translate the types *)
   let tenv =
      SymbolTable.mapi (fun v ty ->
            let pos = var_exp_pos v in
               ptr_type tenv penv pos ty) tenv
   in

   (* Translate the frames *)
   let frames =
      SymbolTable.mapi (fun frame fields ->
            let pos = var_exp_pos frame in
               ptr_frame tenv penv pos frame fields) frames
   in

   (* Add offset fields for globals *)
   let globals =
      SymbolTable.fold (fun globals v (ty, volp, init) ->
            let pos = var_exp_pos v in
               if is_pointer_nonenv_type tenv pos ty then
                  let v' = penv_lookup_pointer penv pos v in
                     SymbolTable.add globals v' (ty_off, volp, InitAtom (AtomInt zero))
               else
                  globals) globals globals
   in

   (* Translate extern *)
   let decls =
      SymbolTable.fold (fun decls v ty ->
            let pos = var_exp_pos v in
               if is_pointer_nonenv_type tenv pos ty then
                  let ty = ptr_type tenv penv pos ty in
                  let v_off = penv_lookup_pointer penv pos v in
                  let decls = SymbolTable.add decls v ty in
                  let decls = SymbolTable.add decls v_off ty_off in
                     decls
               else
                  let ty = ptr_type tenv penv pos ty in
                     SymbolTable.add decls v ty) SymbolTable.empty decls
   in
   let import =
      SymbolTable.fold (fun import v info ->
            let pos = var_exp_pos v in
            let { import_name = s;
                  import_type = ty;
                  import_info = info
                } = info
            in
               if is_pointer_nonenv_type tenv pos ty then
                  let ty = ptr_type tenv penv pos ty in
                  let v_off = penv_lookup_pointer penv pos v in
                  let info1 =
                     { import_name = s ^ ".ptr";
                       import_type = ty;
                       import_info = info
                     }
                  in
                  let info2 =
                     { import_name = s ^ ".off";
                       import_type = ty_off;
                       import_info = Fir.ImportGlobal
                     }
                  in
                  let import = SymbolTable.add import v     info1 in
                  let import = SymbolTable.add import v_off info2 in
                     import
               else
                  let ty = ptr_type tenv penv pos ty in
                  let info =
                     { import_name = s;
                       import_type = ty;
                       import_info = info
                     }
                  in
                     SymbolTable.add import v info) SymbolTable.empty import
   in
   let export =
      SymbolTable.fold (fun export v info ->
            let pos = var_exp_pos v in
            let { export_name = s;
                  export_type = ty
                } = info
            in
               if is_pointer_nonenv_type tenv pos ty then
                  let ty = ptr_type tenv penv pos ty in
                  let v_off = penv_lookup_pointer penv pos v in
                  let info1 =
                     { export_name = s ^ ".ptr";
                       export_type = ty
                     }
                  in
                  let info2 =
                     { export_name = s ^ ".off";
                       export_type = ty_off
                     }
                  in
                  let export = SymbolTable.add export v     info1 in
                  let export = SymbolTable.add export v_off info2 in
                     export
               else
                  let ty = ptr_type tenv penv pos ty in
                  let info =
                     { export_name = s;
                       export_type = ty
                     }
                  in
                     SymbolTable.add export v info) SymbolTable.empty export
   in

   (* Translate the functions *)
   let funs =
      SymbolTable.mapi (fun f (line, gflag, ty, vars, e) ->
            let pos = var_exp_pos f in
            let ty = ptr_type tenv penv pos ty in
            let vars = ptr_vars penv vars in
            let e = ptr_exp tenv penv e in
               line, gflag, ty, vars, e) funs
   in

   (* Translate the body *)
   let body = ptr_exp tenv penv body in
      { prog_file = file;
        prog_globals = globals;
        prog_decls = decls;
        prog_export = export;
        prog_import = import;
        prog_frames = frames;
        prog_types = tenv;
        prog_funs = funs;
        prog_body = body
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
