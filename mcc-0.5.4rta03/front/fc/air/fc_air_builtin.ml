(*
 * Catch some external builtin ops early.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_air
open Fc_air_ds
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_type
open Fc_air_frame
open Fc_air_print

module Pos = MakePos (struct let name = "Fc_air_builtin" end)
open Pos

module type AIRBuiltinSig =
sig
   val builtin_prog : prog -> prog
end

module MakeAIRBuiltin (Frame : AIRFrameSig) : AIRBuiltinSig =
struct
   open Frame

   (*
    * Translate the expressions.
    * Add function headers to copy arguments.
    *)
   let rec builtin_exp e =
      let pos = string_pos "builtin_exp" (exp_pos e) in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetFuns (funs, e) ->
               builtin_funs_exp pos loc funs e
          | LetAtom (v, ty, a, e) ->
               make_exp loc (LetAtom (v, ty, a, builtin_exp e))
          | LetUnop (v, ty, op, a, e) ->
               make_exp loc (LetUnop (v, ty, op, a, builtin_exp e))
          | LetBinop (v, ty, op, a1, a2, e) ->
               make_exp loc (LetBinop (v, ty, op, a1, a2, builtin_exp e))
          | LetExtCall (v, ty, s, b, ty', args, e) ->
               builtin_extcall_exp pos loc v ty s b ty' args e
          | LetApply (v, ty, f, args, e) ->
               make_exp loc (LetApply (v, ty, f, args, builtin_exp e))
          | TailCall _
          | Return _
          | Raise _ ->
               e
          | LetMalloc (v, a, e) ->
               make_exp loc (LetMalloc (v, a, builtin_exp e))
          | IfThenElse (a, e1, e2) ->
               make_exp loc (IfThenElse (a, builtin_exp e1, builtin_exp e2))
          | Try (e1, v, e2) ->
               make_exp loc (Try (builtin_exp e1, v, builtin_exp e2))
          | LetSubscript (v, ty, v1, a2, e) ->
               make_exp loc (LetSubscript (v, ty, v1, a2, builtin_exp e))
          | SetSubscript (v1, a2, ty, a3, e) ->
               make_exp loc (SetSubscript (v1, a2, ty, a3, builtin_exp e))
          | Memcpy (v1, a, v2, e) ->
               make_exp loc (Memcpy (v1, a, v2, builtin_exp e))
          | SetVar (v, ty, a, e) ->
               make_exp loc (SetVar (v, ty, a, builtin_exp e))
          | LetVar (v, ty, a_opt, e) ->
               make_exp loc (LetVar (v, ty, a_opt, builtin_exp e))
          | LetAddrOfVar (v1, ty, v2, e) ->
               make_exp loc (LetAddrOfVar (v1, ty, v2, builtin_exp e))
          | Debug (info, e) ->
               make_exp loc (Debug (info, builtin_exp e))

   (*
    * Insert function headers to copy aggregate arguments.
    *)
   and builtin_funs_exp pos loc funs e =
      let pos = string_pos "builtin_funs_exp" pos in

      (* Examine all the arguments *)
      let funs =
         List.map (fun (f, loc, gflag, ty, vars, e) ->
               let e = builtin_exp e in
                  f, loc, gflag, ty, vars, e) funs
      in

      (* Copy the expression *)
      let e = builtin_exp e in
         make_exp loc (LetFuns (funs, e))

   (*
    * External calls.
    *)
   and builtin_extcall_exp pos loc v ty s b ty' args e =
      let pos = string_pos "builtin_extcall_exp" pos in
      let e = builtin_exp e in
      let e =
         match s, args with
            "malloc", [a] ->
               LetMalloc (v, a, e)
          | _ ->
               LetExtCall (v, ty, s, b, ty', args, e)
      in
         make_exp loc e

   (************************************************************************
    * GLOBAL FUNCTIONS
    ************************************************************************)

   (*
    * Copy the program.
    *)
   let builtin_prog prog =
      let { prog_body = body } = prog in
      let body = builtin_exp body in
         { prog with prog_body = body }
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
