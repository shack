(*
 * AIR stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug

open Fc_config
open Fc_air_frame
open Fc_air_ast
open Fc_air_copy
open Fc_air_builtin
open Fc_air_print

module AIRFrame = MakeAIRFrame (FCParam)
module AIR = MakeAIR (AIRFrame)
module AIRCopy = MakeAIRCopy (AIRFrame)
module AIRBuiltin = MakeAIRBuiltin (AIRFrame)

(*
 * Debugging.
 *)
let debug_prog name prog =
   if debug Fir_state.debug_print_air then
      debug_prog name prog

(*
 * Call them in sequence.
 *)
let compile name exp =
   let prog = AIR.build_prog name exp in
   let _ = debug_prog "build" prog in
   let prog = AIRCopy.copy_prog prog in
   let _ = debug_prog "copy" prog in
   let prog = AIRBuiltin.builtin_prog prog in
   let _ = debug_prog "builtin" prog in
   let prog = Fc_air_dead.dead_prog prog in
   let _ = debug_prog "dead" prog in
   let prog = Fc_air_inline.inline_prog prog in
   let _ = debug_prog "inline" prog in
   let prog = Fc_air_dead.dead_prog prog in
   let _ = debug_prog "dead" prog in
   let prog = Fc_air_field.field_prog prog in
   let _ = debug_prog "field" prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
