(*
 * Insert explicit copying for aggregates.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_type
open Fc_air_frame
open Fc_air_print

module Pos = MakePos (struct let name = "Fc_air_copy" end)
open Pos

module type AIRCopySig =
sig
   val copy_prog : prog -> prog
end

module MakeAIRCopy (Frame : AIRFrameSig) : AIRCopySig =
struct
   open Frame

   (************************************************************************
    * UTILITIES
    ************************************************************************)

   let exp_pos e = string_pos "Fc_air_copy" (exp_pos e)

   (*
    * Zero.
    *)
   let zero = AtomInt (Rawint.of_int index_precision index_signed 0)

   (*
    * Wrap the sizeof call.
    *)
   let build_sizeof_exp tenv pos loc ty cont =
      let pos = string_pos "build_sizeof_exp" pos in
         snd (build_sizeof tenv pos loc ty (fun size -> (), cont size))

   (************************************************************************
    * TRANSLATE TYPES
    ************************************************************************)

   (*
    * Pass aggregates with pointers.
    *)
   let rec copy_type tenv pos ty =
      let pos = string_pos "copy_type" pos in
         match ty with
            TyUnit _
          | TyDelayed
          | TyZero
          | TyInt _
          | TyTag
          | TyField _
          | TyFloat _
          | TyVar _
          | TyEnum _
          | TyUEnum (Label _)
          | TyStruct (Label _) ->
               ty
          | TyPointer ty
          | TyRef ty ->
               TyPointer (copy_type tenv pos ty)
          | TyArray (ty, e1, e2) ->
               TyArray (copy_type tenv pos ty, e1, e2)
          | TyTuple { tuple_size = size; tuple_fields = fields } ->
               let fields = List.map (fun (ty, e) -> copy_type tenv pos ty, e) fields in
                  TyTuple { tuple_size = size; tuple_fields = fields }
          | TyElide { tuple_size = size; tuple_fields = fields } ->
               let fields = List.map (fun (ty, e) -> copy_fun_type tenv pos ty, e) fields in
                  TyElide { tuple_size = size; tuple_fields = fields }
          | TyUEnum (Fields { uenum_size = size; uenum_fields = fields }) ->
               let fields =
                  SymbolTable.map (fun (ty_opt, label) ->
                        copy_type_opt tenv pos ty_opt, label) fields
               in
                  TyUEnum (Fields { uenum_size = size; uenum_fields = fields })
          | TyStruct (Fields { struct_size = size; struct_root = root; struct_fields = fields }) ->
               let fields =
                  SymbolTable.map (fun field ->
                        { field with field_type = copy_type tenv pos field.field_type }) fields
               in
                  TyStruct (Fields { struct_size = size; struct_root = root; struct_fields = fields })
          | TyFun (ty_args, ty_res) ->
               let ty_args = List.map (copy_fun_type tenv pos) ty_args in
               let ty_res = copy_fun_type tenv pos ty_res in
                  TyFun (ty_args, ty_res)
          | TyLambda (vars, ty) ->
               TyLambda (vars, copy_type tenv pos ty)
          | TyAll (vars, ty) ->
               TyAll (vars, copy_type tenv pos ty)
          | TyApply (v, tyl) ->
               TyApply (v, List.map (copy_type tenv pos) tyl)

   and copy_type_opt tenv pos = function
      Some ty -> Some (copy_type tenv pos ty)
    | None -> None

   (*
    * Types for function arguments.
    *)
   and copy_fun_type tenv pos ty =
      let pos = string_pos "copy_fun_type" pos in
      let ty = copy_type tenv pos ty in
         match tenv_expand tenv pos ty with
            TyArray (ty, _, _) ->
               TyPointer ty
          | TyTuple _
          | TyEnum _
          | TyStruct _
          | TyElide _ ->
               TyPointer ty
          | _ ->
               ty

   (*
    * Types for copy statements.
    * Arrays do not require a copy.
    *)
   and copy_copy_type tenv pos ty =
      let pos = string_pos "copy_copy_type" pos in
      let ty = copy_type tenv pos ty in
         match tenv_expand tenv pos ty with
            TyArray (ty, _, _) ->
               TyPointer ty
          | _ ->
               ty

   (************************************************************************
    * TRANSLATE EXPRESSIONS
    ************************************************************************)

   (*
    * Translate the expressions.
    * Add function headers to copy arguments.
    *)
   let rec copy_exp tenv e =
      let pos = string_pos "copy_exp" (exp_pos e) in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetFuns (funs, e) ->
               copy_funs_exp tenv pos loc funs e
          | LetAtom (v, ty, a, e) ->
               copy_atom_exp tenv pos loc v ty a e
          | LetUnop (v, ty, op, a, e) ->
               copy_unop_exp tenv pos loc v ty op a e
          | LetBinop (v, ty, op, a1, a2, e) ->
               copy_binop_exp tenv pos loc v ty op a1 a2 e
          | LetExtCall (v, ty, s, b, ty', args, e) ->
               copy_extcall_exp tenv pos loc v ty s b ty' args e
          | LetApply (v, ty, f, args, e) ->
               copy_apply_exp tenv pos loc v ty f args e
          | TailCall _
          | Return _
          | Raise _ ->
               e
          | LetMalloc (v, a, e) ->
               make_exp loc (LetMalloc (v, a, copy_exp tenv e))
          | IfThenElse (a, e1, e2) ->
               make_exp loc (IfThenElse (a, copy_exp tenv e1, copy_exp tenv e2))
          | Try (e1, v, e2) ->
               make_exp loc (Try (copy_exp tenv e1, v, copy_exp tenv e2))
          | LetSubscript (v, ty, v1, a2, e) ->
               copy_subscript_exp tenv pos loc v ty v1 a2 e
          | SetSubscript (v1, a2, ty, a3, e) ->
               copy_set_subscript_exp tenv pos loc v1 a2 ty a3 e
          | Memcpy (v1, a, v2, e) ->
               make_exp loc (Memcpy (v1, a, v2, copy_exp tenv e))
          | SetVar (v, ty, a, e) ->
               copy_set_var_exp tenv pos loc v ty a e
          | LetVar (v, ty, a_opt, e) ->
               copy_var_exp tenv pos loc v ty a_opt e
          | LetAddrOfVar (v1, ty, v2, e) ->
               copy_addr_of_var_exp tenv pos loc v1 ty v2 e
          | Debug (info, e) ->
               copy_debug_exp tenv pos loc info e

   (*
    * Insert function headers to copy aggregate arguments.
    *)
   and copy_funs_exp tenv pos loc funs e =
      let pos = string_pos "copy_funs_exp" pos in

      (* Examine all the arguments *)
      let funs = List.map (copy_fun tenv pos) funs in

      (* Copy the expression *)
      let e = copy_exp tenv e in
         make_exp loc (LetFuns (funs, e))

   and copy_fun tenv pos (f, loc, gflag, ty, vars, e) =
      let pos = string_pos "copy_fun" pos in
      let ty_vars, ty_args, ty_res = dest_fun_type tenv pos ty in

      (* Copy the arguments *)
      let rec collect vars' ty_args' =
         match vars', ty_args' with
            v :: vars', ty :: ty_args' ->
               let (ty_args, vars), body = collect vars' ty_args' in
               let ty = copy_fun_type tenv pos ty in
                  if is_aggregate_copy_type tenv pos ty then
                     build_sizeof tenv pos loc ty (fun size ->
                           let v1 = new_symbol v in
                           let v2 = new_symbol v in
                           let ty_p = TyPointer ty in
                           let body = make_exp loc (Memcpy (v, size, v1, body)) in
                           let body = make_exp loc (LetAddrOfVar (v, ty_p, v2, body)) in
                           let body = make_exp loc (LetVar (v2, ty, None, body)) in
                              (ty_p :: ty_args, v1 :: vars), body)
                  else
                     (ty :: ty_args, v :: vars), body
          | [], [] ->
               ([], []), copy_exp tenv e
          | _ ->
               raise (AirException (pos, ArityMismatch (List.length ty_args, List.length vars)))
      in
      let (ty_args, vars), body = collect vars ty_args in

      (* Rebuild the function *)
      let ty_res = copy_fun_type tenv pos ty_res in
      let ty = TyAll (ty_vars, TyFun (ty_args, ty_res)) in
         f, loc, gflag, ty, vars, body

   (*
    * Operators.
    * The only operator we have to fix is the
    * ExternalOp.
    *)
   and copy_atom_exp tenv pos loc v ty a e =
      let pos = string_pos "copy_atom_exp" pos in
      let e = copy_exp tenv e in
         make_exp loc (LetAtom (v, copy_type tenv pos ty, a, e))

   and copy_unop_exp tenv pos loc v ty op a e =
      let pos = string_pos "copy_unop_exp" pos in
      let e = copy_exp tenv e in
         make_exp loc (LetUnop (v, copy_type tenv pos ty, op, a, e))

   and copy_binop_exp tenv pos loc v ty op a1 a2 e =
      let pos = string_pos "copy_binop_exp" pos in
      let e = copy_exp tenv e in
         make_exp loc (LetBinop (v, copy_type tenv pos ty, op, a1, a2, e))

   and copy_extcall_exp tenv pos loc v ty s b ty' args e =
      let pos = string_pos "copy_op_exp" pos in
         make_exp loc (LetExtCall (v, copy_type tenv pos ty, s, b, copy_type tenv pos ty', args, copy_exp tenv e))

   (*
    * Copy the application.
    *)
   and copy_apply_exp tenv pos loc v ty f args e =
      let pos = string_pos "copy_apply_exp" pos in
      let e = copy_exp tenv e in
      let ty = copy_type tenv pos ty in
         if is_aggregate_type tenv pos ty then
            let v1 = new_symbol v in
            let v2 = new_symbol v in
               build_sizeof_exp tenv pos loc ty (fun size ->
                     let e = make_exp loc (Memcpy (v, size, v2, e)) in
                     let e = make_exp loc (LetApply (v2, TyPointer ty, f, args, e)) in
                     let e = make_exp loc (LetAddrOfVar (v, TyPointer ty, v1, e)) in
                     let e = make_exp loc (LetVar (v1, ty, None, e)) in
                        e)
         else
            make_exp loc (LetApply (v, ty, f, args, e))

   (*
    * Subscript operation.
    *)
   and copy_subscript_exp tenv pos loc v ty v1 a2 e =
      let pos = string_pos "copy_subscript_exp" pos in
      let e = copy_exp tenv e in
      let ty = copy_type tenv pos ty in
         if is_aggregate_type tenv pos ty then
            let v2 = new_symbol v in
            let v3 = new_symbol v in
               build_sizeof_exp tenv pos loc ty (fun size ->
                     let e = make_exp loc (Memcpy (v, size, v3, e)) in
                     let e = make_exp loc (LetBinop (v3, TyPointer ty, PlusPointerOp, AtomVar v1, a2, e)) in
                     let e = make_exp loc (LetAddrOfVar (v, TyPointer ty, v2, e)) in
                     let e = make_exp loc (LetVar (v2, ty, None, e)) in
                        e)
         else
            make_exp loc (LetSubscript (v, ty, v1, a2, e))

   (*
    * Set subscript operation.
    *)
   and copy_set_subscript_exp tenv pos loc v1 a2 ty a3 e =
      let pos = string_pos "copy_subscript_exp" pos in
      let e = copy_exp tenv e in
      let ty = copy_type tenv pos ty in
         if is_aggregate_type tenv pos ty then
            let v2 = new_symbol v1 in
               build_sizeof_exp tenv pos loc ty (fun size ->
                     let e = make_exp loc (Memcpy (v2, size, var_of_atom pos a3, e)) in
                     let e = make_exp loc (LetBinop (v2, TyPointer ty, PlusPointerOp, AtomVar v1, a2, e)) in
                        e)
         else
            make_exp loc (SetSubscript (v1, a2, ty, a3, e))

   (*
    * Setvar may become Memcpy.
    *)
   and copy_set_var_exp tenv pos loc v ty a e =
      let pos = string_pos "copy_set_var_exp" pos in
      let e = copy_exp tenv e in
      let ty = copy_type tenv pos ty in
         if is_aggregate_type tenv pos ty then
            build_sizeof_exp tenv pos loc ty (fun size ->
                  make_exp loc (Memcpy (v, size, var_of_atom pos a, e)))
         else
            make_exp loc (SetVar (v, ty, a, e))

   (*
    * LetVar.
    * This introduces an explicit copy for aggregate types.
    *)
   and copy_var_exp tenv pos loc v ty a_opt e =
      let pos = string_pos "copy_var_exp" pos in
      let e = copy_exp tenv e in
         match a_opt with
            Some a ->
               (* Some aggregates (like TyElide) don't need to be copied *)
               let ty = copy_copy_type tenv pos ty in
                  if is_aggregate_noncopy_type tenv pos ty then
                     make_exp loc (LetVar (v, TyPointer ty, a_opt, e))
                  else if is_aggregate_type tenv pos ty then
                     let v1 = new_symbol v in
                     let ty_p = TyPointer ty in
                     let e =
                        build_sizeof_exp tenv pos loc ty (fun size ->
                              make_exp loc (Memcpy (v, size, var_of_atom pos a, e)))
                     in
                     let e = make_exp loc (LetAddrOfVar (v, ty_p, v1, e)) in
                     let e = make_exp loc (LetVar (v1, ty, None, e)) in
                        e
                  else
                     make_exp loc (LetVar (v, ty, a_opt, e))
          | None ->
               (* This is an allocation *)
               let ty = copy_type tenv pos ty in
                  if is_aggregate_type tenv pos ty then
                     let v1 = new_symbol v in
                     let ty_p = TyPointer ty in
                     let e = make_exp loc (LetAddrOfVar (v, ty_p, v1, e)) in
                     let e = make_exp loc (LetVar (v1, ty, None, e)) in
                        e
                  else
                     make_exp loc (LetVar (v, ty, a_opt, e))

   (*
    * Address-of.
    * If the value is an aggregate, then this is a nop.
    *)
   and copy_addr_of_var_exp tenv pos loc v1 ty v2 e =
      let pos = string_pos "copy_addr_of_var_exp" pos in
      let e = copy_exp tenv e in
      let ty = copy_type tenv pos ty in
      let ty' = dest_pointer_ref_type tenv pos ty in
         if is_aggregate_type tenv pos ty' then
            make_exp loc (LetAtom (v1, ty, AtomVar v2, e))
         else
            make_exp loc (LetAddrOfVar (v1, ty, v2, e))

   (*
    * Copy debug.
    *)
   and copy_debug_exp tenv pos loc info e =
      let pos = string_pos "copy_debug_exp" pos in
      let e = copy_exp tenv e in
      let info =
         match info with
            DebugString _ ->
               info
          | DebugContext (line, vars) ->
               let vars =
                  List.map (fun (v1, ty, v2) -> v1, copy_type tenv pos ty, v2) vars
               in
                  DebugContext (line, vars)
      in
         make_exp loc (Debug (info, e))

   (************************************************************************
    * GLOBAL FUNCTIONS
    ************************************************************************)

   (*
    * Copy the program.
    *)
   let copy_prog prog =
      let { prog_file = file;
            prog_globals = globals;
            prog_decls = decls;
            prog_export = export;
            prog_import = import;
            prog_types = types;
            prog_body = body
          } = prog
      in
      let globals =
         SymbolTable.mapi (fun v (ty, volp, init) ->
               copy_fun_type types (var_exp_pos v) ty, volp, init) globals
      in
      let decls =
         SymbolTable.mapi (fun v ty ->
               copy_fun_type types (var_exp_pos v) ty) decls
      in
      let export =
         SymbolTable.mapi (fun v info ->
               let ty = copy_fun_type types (var_exp_pos v) info.export_type in
                  { info with export_type = ty }) export
      in
      let import =
         SymbolTable.mapi (fun v info ->
               let ty = copy_fun_type types (var_exp_pos v) info.import_type in
                  { info with import_type = ty }) import
      in
      let tenv =
         SymbolTable.mapi (fun v ty ->
               copy_type types (var_exp_pos v) ty) types
      in
      let body = copy_exp tenv body in
         { prog_file = file;
           prog_globals = globals;
           prog_decls = decls;
           prog_export = export;
           prog_import = import;
           prog_types = tenv;
           prog_body = body
         }
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
