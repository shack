(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_config

open Fc_air
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_exp
open Fc_air_subst

module Pos = MakePos (struct let name = "Fc_air_type" end)
open Pos

(*
 * Atom should be a var.
 *)
let is_var_atom = function
   AtomVar _ -> true
 | _ -> false

let var_of_atom pos = function
   AtomVar v ->
      v
 | a ->
      raise (AirException (pos, StringAtomError ("not a var", a)))

(*
 * Get the type of an atom.
 *)
let type_of_atom venv pos a =
   match a with
      AtomUnit (n, _) ->
         TyUnit n
    | AtomInt i ->
         let pre = Rawint.precision i in
         let signed = Rawint.signed i in
            TyInt (pre, signed)
    | AtomFloat x ->
         let pre = Rawfloat.precision x in
            TyFloat pre
    | AtomNil ty ->
         TyPointer ty
    | AtomVar v ->
         venv_lookup venv pos v

(*
 * Type of operators.
 *)
let ty_bool = TyUnit 2

let type_of_unop ty = function
   (* Native ints *)
   UMinusIntOp (pre, signed)
 | NotIntOp    (pre, signed)
 | BitFieldOp  (pre, signed, _, _) ->
      let ty_int = TyInt (pre, signed) in
         ty_int, ty_int

   (* Floats *)
 | UMinusFloatOp pre
 | AbsFloatOp    pre
 | SinOp pre
 | CosOp pre
 | SqrtOp pre ->
      let ty_float = TyFloat pre in
         ty_float, ty_float
 | EqPointerNilOp ->
      TyPointer (TyUnit 0), ty
 | NeqPointerNilOp ->
      TyPointer (TyUnit 0), ty

 | IntOfInt (pre1, signed1, pre2, signed2) ->
      let ty_int1 = TyInt (pre1, signed1) in
      let ty_int2 = TyInt (pre2, signed2) in
         ty_int2, ty_int1
 | IntOfUnit (pre1, signed1, i) ->
      let ty_int1 = TyInt (pre1, signed1) in
      let ty_unit = TyUnit (i) in
         ty_unit, ty_int1
 | IntOfFloat (pre1, signed1, pre2) ->
      let ty_int = TyInt (pre1, signed1) in
      let ty_float = TyFloat pre2 in
         ty_float, ty_int
 | FloatOfInt (pre1, pre2, signed2) ->
      let ty_int = TyInt (pre2, signed2) in
      let ty_float = TyFloat pre1 in
         ty_int, ty_float
 | FloatOfFloat (pre1, pre2) ->
      let ty_float1 = TyFloat pre1 in
      let ty_float2 = TyFloat pre2 in
         ty_float2, ty_float1
 | IntOfPointer (pre, signed) ->
      let ty_int = TyInt (pre, signed) in
      let ty_ptr = TyPointer (TyUnit 0) in
         ty_ptr, ty_int
 | PointerOfInt (pre, signed) ->
      let ty_int = TyInt (pre, signed) in
      let ty_ptr = TyPointer (TyUnit 0) in
         ty_int, ty_ptr

let type_of_binop ty = function
   AndBoolOp
 | OrBoolOp
 | EqBoolOp
 | NeqBoolOp ->
     ty_bool, ty_bool, ty_bool

 | PlusIntOp   (pre, signed)
 | MinusIntOp  (pre, signed)
 | MulIntOp    (pre, signed)
 | DivIntOp    (pre, signed)
 | RemIntOp    (pre, signed)
 | SlIntOp     (pre, signed)
 | SrIntOp     (pre, signed)
 | AndIntOp    (pre, signed)
 | OrIntOp     (pre, signed)
 | XorIntOp    (pre, signed)
 | MinIntOp    (pre, signed)
 | MaxIntOp    (pre, signed)
 | SetBitFieldOp  (pre, signed, _, _) ->
      let ty_int = TyInt (pre, signed) in
         ty_int, ty_int, ty_int

 | EqIntOp     (pre, signed)
 | NeqIntOp    (pre, signed)
 | LtIntOp     (pre, signed)
 | LeIntOp     (pre, signed)
 | GtIntOp     (pre, signed)
 | GeIntOp     (pre, signed) ->
      let ty_int = TyInt (pre, signed) in
         ty_int, ty_int, ty_bool


 | PlusFloatOp   pre
 | MinusFloatOp  pre
 | MulFloatOp    pre
 | DivFloatOp    pre
 | RemFloatOp    pre
 | MinFloatOp    pre
 | MaxFloatOp    pre
 | Atan2Op       pre ->
      let ty_float = TyFloat pre in
         ty_float, ty_float, ty_float

 | EqFloatOp     pre
 | NeqFloatOp    pre
 | LtFloatOp     pre
 | LeFloatOp     pre
 | GtFloatOp     pre
 | GeFloatOp     pre ->
      let ty_float = TyFloat pre in
         ty_float, ty_float, ty_bool

   (* Pointer operations *)
 | PlusPointerOp ->
      ty, TyInt (Rawint.Int32, true), ty
 | MinusPointerOp ->
      let ty_int = TyInt (Rawint.Int32, true) in
      let ty_ptr = TyPointer (TyUnit 0) in
         ty_ptr, ty_ptr, ty_int

 | EqPointerOp
 | NeqPointerOp
 | LtPointerOp
 | LePointerOp
 | GtPointerOp
 | GePointerOp ->
      let ty_ptr = TyPointer (TyUnit 0) in
         ty_ptr, ty_ptr, ty_bool

 | EqTagOp ->
      TyTag, TyTag, ty_bool

(*
 * Apply the type, and return the expansion.
 * The result _is not_ a TyApply.
 *)
let apply_type tenv pos v tyl =
   match tenv_lookup tenv pos v with
      TyLambda (vars, ty) ->
         let tvenv =
            let len1 = List.length vars in
            let len2 = List.length tyl in
               if len1 <> len2 then
                  raise (AirException (pos, ArityMismatch (len1, len2)));
               List.fold_left2 subst_add subst_empty vars tyl
         in
            subst_type tvenv ty
    | ty ->
         ty

(*
 * Expand a type one level.
 *)
let rec tenv_expand tenv pos ty =
   let pos = string_pos "tenv_expand" pos in
      match ty with
         TyApply (v, tyl) ->
            tenv_expand tenv pos (apply_type tenv pos v tyl)
       | TyLambda ([], ty)
       | TyAll ([], ty) ->
            tenv_expand tenv pos ty
       | _ ->
            ty

(*
 * This is not an aggregate type.
 *)
let is_scalar_type tenv pos ty =
   let pos = string_pos "is_scalar_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyAll _
       | TyZero ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyUnit _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyTag
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _
       | TyEnum _ ->
            true
       | TyArray _
       | TyTuple _
       | TyElide _
       | TyUEnum _
       | TyStruct _ ->
            false

(*
 * This is an aggregate type.
 *)
let is_aggregate_type tenv pos ty =
   let pos = string_pos "is_aggregate_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyAll _
       | TyZero ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyUnit _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyTag
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _
       | TyEnum _ ->
            false
       | TyArray _
       | TyTuple _
       | TyElide _
       | TyUEnum _
       | TyStruct _ ->
            true

(*
 * These are the aggregates that have to be copied
 * by a LetVar.
 *)
let is_aggregate_copy_type tenv pos ty =
   let pos = string_pos "is_copy_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyAll _
       | TyZero ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyUnit _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyTag
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _
       | TyEnum _
       | TyElide _ ->
            false
       | TyArray _
       | TyTuple _
       | TyUEnum _
       | TyStruct _ ->
            true

(*
 * These are the values that are aggregate but
 * don't need to be copied by a LetVar.
 *)
let is_aggregate_noncopy_type tenv pos ty =
   let pos = string_pos "is_pointer_copy_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyAll _
       | TyZero ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyElide _ ->
            true
       | TyUnit _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyTag
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _
       | TyEnum _
       | TyArray _
       | TyTuple _
       | TyUEnum _
       | TyStruct _ ->
            false

(*
 * These are the aggregates that are passed by
 * value (not reference).
 *)
let is_aggregate_nonarray_type tenv pos ty =
   let pos = string_pos "is_nonarray_copy_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyAll _
       | TyZero ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyUnit _
       | TyInt _
       | TyField _
       | TyFloat _
       | TyTag
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _
       | TyEnum _
       | TyArray _
       | TyElide _ ->
            false
       | TyTuple _
       | TyUEnum _
       | TyStruct _ ->
            true

(*
 * globals have to be wrapped in a reference.
 *)
let is_global_type tenv pos ty =
   let pos = string_pos "is_scalar_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyZero ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyUnit _
       | TyInt _
       | TyTag
       | TyField _
       | TyFloat _
       | TyEnum _ ->
            false
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _
       | TyAll _
       | TyArray _
       | TyTuple _
       | TyUEnum _
       | TyStruct _
       | TyElide _ ->
            true

(*
 * Atom initializer for a type.
 *)
let default_atom_of_type tenv pos ty =
   let pos = string_pos "default_atom_of_type" pos in
      match tenv_expand tenv pos ty with
         TyDelayed
       | TyApply _
       | TyLambda _
       | TyZero
       | TyUnit 0
       | TyAll _
       | TyVar _
       | TyArray _
       | TyFun _
       | TyTuple _
       | TyElide _
       | TyUEnum _
       | TyStruct _
       | TyRef _
       | TyPointer _
       | TyTag ->
            raise (AirException (pos, StringTypeError ("unexpected type", ty)))
       | TyUnit n ->
            AtomUnit (n, 0)
       | TyEnum _ ->
            AtomInt (Rawint.of_int Rawint.Int32 false 0)
       | TyInt (pre, signed)
       | TyField (pre, signed, _, _) ->
            AtomInt (Rawint.of_int pre signed 0)
       | TyFloat pre ->
            AtomFloat (Rawfloat.of_float pre 0.0)

(*
 * Check for a TyElide.
 *)
let is_elide_type tenv pos ty =
   let pos = string_pos "is_elide_type" pos in
      match tenv_expand tenv pos ty with
         TyElide _ ->
            true
       | _ ->
            false

(*
 * Return the parts of a function type.
 *)
let rec is_fun_type tenv pos ty =
   let pos = string_pos "is_fun_type" pos in
      match tenv_expand tenv pos ty with
         TyFun _ ->
            true
       | TyAll (_, ty) ->
            is_fun_type tenv pos ty
       | _ ->
            false

let rec dest_fun_type_aux tenv pos ty =
   let pos = string_pos "dest_fun_type_aux" pos in
      match tenv_expand tenv pos ty with
         TyFun (ty_vars, ty_res) ->
            [], ty_vars, ty_res
       | TyAll (vars, ty) ->
            let vars', ty_vars, ty_res = dest_fun_type_aux tenv pos ty in
               vars @ vars', ty_vars, ty_res
       | ty ->
            raise (AirException (pos, StringTypeError ("not a function type", ty)))

let dest_fun_type tenv pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match tenv_expand tenv pos ty with
         TyPointer ty -> dest_fun_type_aux tenv pos ty
       | ty -> dest_fun_type_aux tenv pos ty

(*
 * Get all the types in the tuple.
 *)
let dest_tuple_type tenv pos ty =
   let pos = string_pos "dest_tuple_type" pos in
      match tenv_expand tenv pos ty with
         TyTuple { tuple_fields = fields } ->
            List.map fst fields
       | ty ->
            raise (AirException (pos, StringTypeError ("not a tuple type", ty)))

(*
 * Get the tag for the enumeration.
 *)
let dest_uenum_tag tenv pos v ty =
   let pos = string_pos "dest_uenum_tag" pos in
      match tenv_expand tenv pos ty with
         TyUEnum (Fields { uenum_fields = fields }) ->
            (try SymbolTable.find fields v with
                Not_found ->
                   raise (AirException (pos, UnboundLabel v)))
       | ty ->
            raise (AirException (pos, StringTypeError ("not a union-enum type", ty)))

(*
 * Get the inner type for a pointer or ref.
 *)
let is_array_ref_type tenv pos ty =
   let pos = string_pos "is_array_ref_type" pos in
      match tenv_expand tenv pos ty with
         TyArray _
       | TyRef _ ->
            true
       | _ ->
            false

let is_pointer_ref_type tenv pos ty =
   let pos = string_pos "is_pointer_ref_type" pos in
      match tenv_expand tenv pos ty with
         TyArray _
       | TyPointer _
       | TyRef _ ->
            true
       | _ ->
            false

let dest_pointer_ref_type tenv pos ty =
   let pos = string_pos "dest_pointer_ref_type" pos in
      match tenv_expand tenv pos ty with
         TyArray (ty, _, _)
       | TyPointer ty
       | TyRef ty ->
            ty
       | ty ->
            raise (AirException (pos, StringTypeError ("not a pointer type", ty)))

let rec dest_pointer_allrefs_type tenv pos ty =
   let pos = string_pos "dest_pointer_allrefs_type" pos in
   let rec dest_pointer_type ty =
      match tenv_expand tenv pos ty with
         TyArray (ty, _, _)
       | TyPointer ty ->
            dest_ref_type ty
       | TyRef ty ->
            dest_pointer_type ty
       | ty ->
            raise (AirException (pos, StringTypeError ("not a pointer type", ty)))
   and dest_ref_type ty =
      match tenv_expand tenv pos ty with
         TyRef ty ->
            dest_ref_type ty
       | ty ->
            ty
   in
      dest_pointer_type ty

(*
 * Get the structure field.
 *)
let struct_field_lookup info pos v =
   let pos = string_pos "struct_field_lookup" pos in
      try SymbolTable.find info.struct_fields v with
         Not_found ->
            raise (AirException (pos, UnboundLabel v))

(*
 * Build a list of the struct fields.
 * First, we find the root node.
 * Then we link them.
 *)
let list_of_struct_fields pos root fields =
   let rec collect fields' root =
      match root with
         None ->
            List.rev fields'
       | Some v ->
            (try
                let field = SymbolTable.find fields v in
                   collect ((v, field) :: fields') field.field_next
             with
                Not_found ->
                   raise (AirException (pos, UnboundLabel v)))
   in
      collect [] root

(*
 * Build a list of the enum fields.
 * The order is arbitrary, but enums with the
 * same field name should sort in the same order.
 *)
let list_of_enum_fields fields =
   SymbolTable.fold (fun fields v i ->
         (v, i) :: fields) [] fields

let list_of_uenum_fields fields =
   SymbolTable.fold (fun fields v (ty_opt, label) ->
         (v, ty_opt, label) :: fields) [] fields

(*
 * Equality of vars.
 *)
let rec mem_eq v1 v2 = function
   (v1', v2') :: tl ->
      (v1 = v1' && v2 = v2') || (v1 = v2' && v2 = v1') || mem_eq v1 v2 tl
 | [] ->
      false

(*
 * Add the vars to the venv (wich is a assoc list).
 *)
let fold_vars eq_vars pos vars1' vars2' =
   let rec loop eq_vars vars1 vars2 =
      match vars1, vars2 with
         v1 :: vars1, v2 :: vars2 ->
            loop ((v1, v2) :: eq_vars) vars1 vars2
       | [], [] ->
            eq_vars
       | _ ->
            raise (AirException (pos, ArityMismatch (List.length vars1', List.length vars2')))
   in
      loop eq_vars vars1' vars2'

(*
 * Compare types for equality.
 *)
let rec equal_types tenv pos eq_vars eq_ids ty1 ty2 =
   let pos = string_pos "equal_types" pos in
      match ty1, ty2 with
         TyVar v1, TyVar v2 ->
            let pos = string_pos "equal_var_var" pos in
               v1 = v2 || mem_eq v1 v2 eq_vars

         (*
          * Base types.
          *)
       | TyUnit n1, TyUnit n2 ->
            n1 = n2
       | TyZero, TyZero ->
            true
       | TyInt (pre1, signed1), TyInt (pre2, signed2) ->
            pre1 = pre2 && signed1 = signed2
       | TyField (pre1, signed1, off1, len1), TyField (pre2, signed2, off2, len2) ->
            pre1 = pre2 && signed1 = signed2 && off1 = off2 && len1 = len2
       | TyFloat pre1, TyFloat pre2 ->
            pre1 = pre2

         (* Functions: swap the vars to get subtyping right *)
       | TyFun (ty_vars1, ty_res1), TyFun (ty_vars2, ty_res2) ->
            (equal_type_lists tenv pos eq_vars eq_ids ty_vars2 ty_vars1)
            && (equal_types tenv pos eq_vars eq_ids ty_res1 ty_res2)

         (* Arrays: elements must be exact, dimensions don't matter *)
       | TyPointer ty1, TyPointer ty2 ->
            equal_types tenv pos eq_vars eq_ids ty1 ty2
       | TyArray (ty1, a1, aa1), TyArray (ty2, a2, aa2) ->
            (*a1 = a2 && *)(equal_types tenv pos eq_vars eq_ids ty1 ty2)

         (* Tuple types *)
       | TyTuple fields1, TyTuple fields2 ->
            equal_tuple_fields tenv pos eq_vars eq_ids fields1 fields2
       | TyElide fields1, TyElide fields2 ->
            equal_tuple_fields tenv pos eq_vars eq_ids fields1 fields2

         (* Structs, unions, and enumerations *)
       | TyStruct (Fields fields1), TyStruct (Fields fields2) ->
            equal_struct_fields tenv pos eq_vars eq_ids fields1 fields2
       | TyEnum (Fields fields1), TyEnum (Fields fields2) ->
            equal_enum_fields tenv pos eq_vars eq_ids fields1 fields2
       | TyUEnum (Fields fields1), TyUEnum (Fields fields2) ->
            equal_uenum_fields tenv pos eq_vars eq_ids fields1 fields2
       | TyStruct (Label (v1, _)), TyStruct (Label (v2, _))
       | TyEnum (Label (v1, _)), TyEnum (Label (v2, _))
       | TyUEnum (Label (v1, _)), TyUEnum (Label (v2, _)) ->
            Symbol.eq v1 v2

         (* Lambdas *)
       | TyLambda (vars1, ty1), TyLambda (vars2, ty2) ->
            let len1 = List.length vars1 in
            let len2 = List.length vars2 in
               if len1 = len2 then
                  let eq_vars = fold_vars eq_vars pos vars1 vars2 in
                     equal_types tenv pos eq_vars eq_ids ty1 ty2
               else
                  false

         (* Defined types *)
       | TyApply (v1, args1), TyApply (v2, args2) ->
            let pos = string_pos "equal_apply_apply" pos in
               if v1 = v2 || mem_eq v1 v2 eq_ids then
                  equal_type_lists tenv pos eq_vars eq_ids args1 args2
               else if List.length args1 = List.length args2 then
                  let eq_ids = (v1, v2) :: eq_ids in
                     equal_types tenv pos eq_vars eq_ids (**)
                        (apply_type tenv pos v1 args1)
                        (apply_type tenv pos v2 args2)
               else
                  equal_types tenv pos eq_vars eq_ids (**)
                     (apply_type tenv pos v1 args1) ty2
       | TyApply (v1, args1), _ ->
            let pos = string_pos "equal_apply_any" pos in
               equal_types tenv pos eq_vars eq_ids (**)
                  (apply_type tenv pos v1 args1) ty2
       | _, TyApply (v2, args2) ->
            let pos = string_pos "equal_any_apply" pos in
               equal_types tenv pos eq_vars eq_ids (**)
                  ty1 (apply_type tenv pos v2 args2)

         (* All others fail *)
       | _ ->
            false

(*
 * List unification.
 *)
and equal_type_lists tenv pos eq_vars eq_ids tyl1 tyl2 =
   let pos = string_pos "equal_type_lists" pos in
      match tyl1, tyl2 with
         ty1 :: tyl1, ty2 :: tyl2 ->
            (equal_types tenv pos eq_vars eq_ids ty1 ty2)
            && (equal_type_lists tenv pos eq_vars eq_ids tyl1 tyl2)
       | [], [] ->
            true
       | _ :: _, []
       | [], _ :: _ ->
            false

(*
 * Tuple field equality.
 *)
and equal_tuple_fields tenv pos eq_vars eq_ids info1 info2 =
   let pos = string_pos "equal_tuple_fields" pos in
   let { tuple_fields = fields1; tuple_size = size1 } = info1 in
   let { tuple_fields = fields2; tuple_size = size2 } = info2 in
   let rec search fields1 fields2 =
      match fields1, fields2 with
         (ty1, e1) :: fields1, (ty2, e2) :: fields2 ->
            (e1 = e2)
            && (equal_types tenv pos eq_vars eq_ids ty1 ty2)
            && (search fields1 fields2)
       | [], [] ->
            true
       | _ ->
            false
   in
      search fields1 fields2

(*
 * Struct field unification.
 *)
and equal_struct_fields tenv pos eq_vars eq_ids info1 info2 =
   let pos = string_pos "equal_struct_fields" pos in
   let { struct_fields = fields1; struct_root = root1; struct_size = size1 } = info1 in
   let { struct_fields = fields2; struct_root = root2; struct_size = size2 } = info2 in
   let fields1 = list_of_struct_fields pos root1 fields1 in
   let fields2 = list_of_struct_fields pos root2 fields2 in
   let rec search fields1 fields2 =
      match fields1, fields2 with
         ((label1, { field_type = ty1; field_offset = off1; field_bits = bits1 }) :: fields1),
         ((label2, { field_type = ty2; field_offset = off2; field_bits = bits2 }) :: fields2) ->
            if label1 <> label2 || off1 <> off2 || bits1 <> bits2 then
               false
            else
               (equal_types tenv pos eq_vars eq_ids ty1 ty2)
               && (search fields1 fields2)
       | [], [] ->
            true
       | _ :: _, []
       | [], _ :: _ ->
            false
   in
      search fields1 fields2

(*
 * Enum field unification.
 *)
and equal_enum_fields tenv pos eq_vars eq_ids info1 info2 =
   let pos = string_pos "equal_struct_fields" pos in
   let { enum_fields = fields1 } = info1 in
   let { enum_fields = fields2 } = info2 in
   let fields1 = list_of_enum_fields fields1 in
   let fields2 = list_of_enum_fields fields2 in
   let rec search fields1 fields2 =
      match fields1, fields2 with
         (label1, i1) :: fields1, (label2, i2) :: fields2 ->
            if label1 <> label2 || i1 <> i2 then
               false
            else
               search fields1 fields2
       | [], [] ->
            true
       | _ :: _, []
       | [], _ :: _ ->
            false
   in
      search fields1 fields2

(*
 * Union enum field unification.
 *)
and equal_uenum_fields tenv pos eq_vars eq_ids info1 info2 =
   let pos = string_pos "equal_struct_fields" pos in
   let { uenum_fields = fields1 } = info1 in
   let { uenum_fields = fields2 } = info2 in
   let fields1 = list_of_uenum_fields fields1 in
   let fields2 = list_of_uenum_fields fields2 in
   let rec search fields1 fields2 =
      match fields1, fields2 with
         (v1, ty_opt1, label1) :: fields1, (v2, ty_opt2, label2) :: fields2 ->
            if v1 <> v2 || label1 <> label2 then
               false
            else
               let eq_ty =
                  match ty_opt1, ty_opt2 with
                     Some ty1, Some ty2 ->
                        equal_types tenv pos eq_vars eq_ids ty1 ty2
                   | Some _, None
                   | None, Some _ ->
                        false
                   | None, None ->
                        true
               in
                  eq_ty && search fields1 fields2
       | [], [] ->
            true
       | _ :: _, []
       | [], _ :: _ ->
            false
   in
      search fields1 fields2

(*
 * Global wrappers.
 *)
let equal_types tenv pos ty1 ty2 =
   equal_types tenv pos [] [] ty1 ty2

let check_types tenv pos ty1 ty2 =
   if not (equal_types tenv pos ty1 ty2) then
      raise (AirException (pos, TypeError2 (ty1, ty2)))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
