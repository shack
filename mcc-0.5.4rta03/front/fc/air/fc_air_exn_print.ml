(*
 * Fc_semant exception handling.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fc_air
open Fc_air_pos
open Fc_air_exn
open Fc_air_print

module Pos = MakePos (struct let name = "Fc_air_exn_print" end)
open Pos

let tabstop = 3

(*
 * Semant exception printer.
 *)
let pp_print_sem_exn buf = function
   UnboundVar v ->
      pp_print_string buf "unbound variable: ";
      pp_print_symbol buf v
 | UnboundLabel v ->
      pp_print_string buf "unbound label: ";
      pp_print_symbol buf v
 | UnboundType v ->
      pp_print_string buf "unbound type: ";
      pp_print_symbol buf v
 | StringError s ->
      pp_print_string buf s
 | StringAtomError (s, a) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_atom buf a
 | StringVarError (s, v) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_symbol buf v
 | StringVar2Error (s, v1, v2) ->
      pp_print_string buf s;
      pp_print_string buf ": expected : ";
      pp_print_symbol buf v1;
      pp_print_string buf " got: ";
      pp_print_symbol buf v2
 | StringTypeError (s, ty) ->
      pp_print_string buf s;
      pp_print_string buf ":";
      pp_print_space buf ();
      pp_print_type buf ty
 | StringIntError (s, i) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_int buf i
 | StringRawintError (s, i) ->
      pp_print_string buf s;
      pp_print_string buf ": ";
      pp_print_string buf (Rawint.to_string i)
 | ArityMismatch (i, j) ->
      pp_print_string buf "arity mismatch: wanted ";
      pp_print_int buf i;
      pp_print_string buf ", got ";
      pp_print_int buf j
 | TypeError2 (ty1, ty2) ->
      pp_open_vbox buf 0;
      pp_print_string buf "type error:";
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "this expression has type:";
      pp_print_space buf ();
      pp_print_type buf ty2;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "but is used with type:";
      pp_print_space buf ();
      pp_print_type buf ty1;
      pp_close_box buf ();
      pp_print_space buf ()
 | InternalError s ->
      pp_print_string buf "internal error: ";
      pp_print_string buf s
 | NotImplemented s ->
      pp_print_string buf "not implemented: ";
      pp_print_string buf s

(*
 * Exception printer.
 *)
let pp_print_exn buf exn =
   match exn with
      AirException (pos, exn) ->
         pp_open_vbox buf 0;
         pp_print_string buf "*** AIR error:";
         pp_print_space buf ();
         pp_print_pos buf pos;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "*** Error: ";
         pp_print_sem_exn buf exn;
         pp_close_box buf ();
         pp_close_box buf ();
         pp_print_newline buf ()
    | exn ->
         Fc_ast_exn_print.pp_print_exn buf exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      AirException _
    | Fc_ast_pos.AstException _
    | Fc_parse_type.ParseError _
    | Fc_parse_exn.ParseExpError _
    | Fc_parse_state.IncompatibleTypes _
    | Parsing.Parse_error
    | Fir_pos.FirException _ as exn ->
         pp_print_exn err_formatter exn;
         exit 2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
