(*
 * Type and variable environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Symbol

open Fc_air
open Fc_air_pos
open Fc_air_exn

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Type and variable environments.
 *)
type tenv = ty SymbolTable.t
type venv = ty SymbolTable.t

(************************************************************************
 * TYPE ENVIRONMENT
 ************************************************************************)

(*
 * Empty table.
 *)
let tenv_empty = SymbolTable.empty

(*
 * Add a type definition.
 *)
let tenv_add tenv v ty =
   SymbolTable.add tenv v ty

(*
 * Lookup a type.
 *)
let tenv_lookup tenv pos v =
   try SymbolTable.find tenv v with
      Not_found ->
         raise (AirException (pos, UnboundType v))

(************************************************************************
 * VARIABLE ENVIRONMENT
 ************************************************************************)

(*
 * Empty table.
 *)
let venv_empty = SymbolTable.empty

(*
 * Add a variable to the table.
 *)
let venv_add = SymbolTable.add

(*
 * Lookup a var.
 *)
let venv_mem = SymbolTable.mem

let venv_lookup venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (AirException (pos, UnboundVar v))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
