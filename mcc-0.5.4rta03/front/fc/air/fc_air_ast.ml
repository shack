(*
 * Translate AST-IR.
 * In addition to the translation, we perform several non-trivial
 * operations:
 *    1. variables with StoreStatic become globals
 *    2. coercions become explicit
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

open Symbol
open Interval_set

open Fir_set

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_type
open Fc_air_frame

module Pos = MakePos (struct let name = "Fc_air_ast" end)
open Pos

let rawint_zero = Rawint.of_int Rawint.Int32 true 0

let signed_int = true
let pre_int = Rawint.Int32
let ty_int = TyInt (pre_int, signed_int)

(*
 * String sets.
 *)
module StringElt =
struct
   type t = int array

   let compare s1 s2 =
      let len1 = Array.length s1 in
      let len2 = Array.length s2 in
      let len = min len1 len2 in
      let rec search i =
         if i = len then
            len1 - len2
         else
            let c = s1.(i) - s2.(i) in
               if c = 0 then
                  search (succ i)
               else
                  i
      in
         search 0
end

module StringSet = DenseIntervalSet (StringElt)

(*
 * IR conversion.
 *)
module type MakeAIRSig =
sig
   val build_prog : string -> Fc_ast.exp -> prog
end

module MakeAIR (Frame : AIRFrameSig) : MakeAIRSig =
struct
   open Frame

   (************************************************************************
    * TYPES
    ************************************************************************)

   (*
    * This is the context for the expression translation.
    *)
   type info =
      { info_tenv : (var * ty) list;
        info_globals : (var * ty * bool * init) list;
        info_decls   : (var * ty) list;
        info_strings : (var * ty * Rawint.int_precision * int array) list;
        info_import : (var * string * ty * Fir.import_info) list;
        info_export : (var * string * ty) list
      }

   let info_empty =
      { info_tenv = [];
        info_globals = [];
        info_decls = [];
        info_strings = [];
        info_import = [];
        info_export = []
      }

   (*
    * Return is defined inside functions.
    *)
   let return_sym = new_symbol_string "return"

   (*
    * Tags become globals.
    *)
   let info_add_tag info tag =
      let s = Symbol.to_string tag in
      let len = String.length s in
      let a = Array.create len 0 in
      let _ =
         for i = 0 to pred len do
            a.(i) <- Char.code s.[i]
         done
      in
      let init = InitString (Rawint.Int8, a) in
      let field = (tag, TyTag, false, init) in
         { info with info_globals = field :: info.info_globals }

   (*
    * Add an export based on the declaration.
    *)
   let info_add_export tenv venv info pos v label ty sc gflag =
      let pos = string_pos "info_add_export" pos in
         match sc, gflag with
            StoreExtern, _
          | _, Fc_ast.VarStaticClass ->
(*
               let ty =
                  if is_scalar_type tenv pos ty then
                     TyRef ty
                  else
                     ty
               in
*)
               let info = { info with info_decls = (v, ty) :: info.info_decls } in
                  true, ty, info
          | _, Fc_ast.VarGlobalClass ->
               let info = { info with info_decls = (v, ty) :: info.info_decls } in
                  if SymbolTable.mem venv return_sym then
                     false, ty, info
                  else
                     let s = Symbol.to_string label in
                        true, ty, { info with info_export = (v, s, ty) :: info.info_export }
          | _, Fc_ast.VarLocalClass ->
               false, ty, info

   let info_add_import tenv info pos v label ty = function
      Fc_ast.VarGlobalClass ->
         let s = Symbol.to_string label in
         let info' = import_info_of_type tenv pos ty in
            { info with info_import = (v, s, ty, info') :: info.info_import }
    | Fc_ast.VarStaticClass
    | Fc_ast.VarLocalClass ->
         info

   (************************************************************************
    * UTILITIES
    ************************************************************************)

   (*
    * Some constants.
    *)
   let native_char_precision = char_precision NormalPrecision
   let native_int_precision = int_precision NormalPrecision

   let bool_true = AtomInt (Rawint.of_int native_int_precision int_signed 1)
   let bool_false = AtomInt (Rawint.of_int native_int_precision int_signed 0)

   let int_zero pre signed =
      AtomInt (Rawint.of_int pre signed 0)

   let int_zero_normal =
      int_zero (int_precision NormalPrecision) int_signed

   let float_zero pre =
      AtomFloat (Rawfloat.of_float pre 0.0)

   let off_tag =
      AtomInt (Rawint.of_int native_int_precision true sizeof_tag)

   (*
    * Some operators.
    *)
   let string_equal_name = "strcmp"

   let string_equal_ty =
      let ty_char = TyInt (Rawint.Int8, false) in
      let ty_string = TyPointer ty_char in
      let ty_bool = TyInt (int_precision NormalPrecision, int_signed) in
         TyFun ([ty_string; ty_string], ty_bool)

   (*
    * Translate a function class.
    *)
   let build_var_class = function
      Fc_ast.VarGlobalClass ->
         VarGlobalClass
    | Fc_ast.VarStaticClass ->
         VarStaticClass
    | Fc_ast.VarLocalClass ->
         VarLocalClass

   (*
    * Make up a new symbol name.
    *)
   let new_symbol_atom s = function
      AtomVar v ->
         new_symbol v
    | _ ->
         new_symbol_string s

   (************************************************************************
    * TRANSLATE ATOM
    ************************************************************************)

   let build_atom tenv pos a cont =
      match a with
         Fc_ast.AtomUnit (_, n, i) ->
            cont (AtomUnit (n, i))
       | Fc_ast.AtomInt (_, i) ->
            cont (AtomInt i)
       | Fc_ast.AtomFloat (_, x) ->
            cont (AtomFloat x)
       | Fc_ast.AtomNil (_, ty) ->
            build_type tenv pos ty (fun ty ->
                  cont (AtomNil ty))
       | Fc_ast.AtomVar (_, v) ->
            cont (AtomVar v)
       | Fc_ast.AtomEnum (_, _, i) ->
            cont (AtomInt (Rawint.of_int32 native_int_precision true i))

   let build_atoms tenv pos atoms cont =
      let rec collect atoms' = function
         a :: atoms ->
            build_atom tenv pos a (fun a -> collect (a :: atoms') atoms)
       | [] ->
            cont (List.rev atoms')
      in
         collect [] atoms

   (*
    * Option form.
    *)
   let build_atom_opt tenv pos a_opt cont =
      match a_opt with
         Some a ->
            build_atom tenv pos a (fun a ->
                  cont (Some a))
       | None ->
            cont None

   (*
    * Initialization.
    *)
   type init =
      XInitNone
    | XInitAtom of atom
    | XInitArray of (label option * init) list

   let rec build_init tenv pos init cont =
      match init with
         Fc_ast.InitNone ->
            cont None XInitNone
       | Fc_ast.InitAtom (_, a) ->
            build_atom tenv pos a (fun a ->
                  cont (Some a) (XInitAtom a))
       | Fc_ast.InitArray (_, fields) ->
            let rec collect fields' = function
               (v_opt, init) :: fields ->
                  build_init tenv pos init (fun _ init ->
                        collect ((v_opt, init) :: fields') fields)
             | [] ->
                  cont None (XInitArray (List.rev fields'))
            in
               collect [] fields

   (*
    * Default initializer based on type.
    *)


   (*
    * Initialization form.
    *)
   let build_global_init tenv pos a_opt ty =
      let pos = string_pos "build_global_init" pos in
         match a_opt with
            None
          | Some (AtomVar _) ->
               if is_pointer_ref_type tenv pos ty then
                  let ty = dest_pointer_ref_type tenv pos ty in
                     InitBlock (build_sizeof_const tenv pos ty)
               else
                  InitAtom (default_atom_of_type tenv pos ty)
          | Some a ->
               InitAtom a

   (*
    * Assign an initializer.
    *)
   let rec assign_init_fields tenv venv info pos loc a ty e fields =
      let pos = string_pos "assign_init_fields" pos in

      (* Assign each of the parts *)
      let rec assign info field_info fields =
         match fields with
            (label_opt, init) :: fields ->
               let index, ty, field_info = fetch_field_info field_info pos label_opt in
               let field_info' = next_field_info field_info pos in
               let info, e = assign info field_info' fields in
                  build_let_addr_of_subscript venv a field_info pos loc (fun venv a ->
                        assign_init tenv venv info pos loc a ty e init)
          | [] ->
               info, e
      in
      let field_info = build_field_info tenv pos ty in
         assign info field_info fields

   and assign_init tenv venv info pos loc a ty e init =
      let pos = string_pos "assign_init" pos in
         match init with
            XInitNone ->
               info, e
          | XInitAtom a' ->
               let v = var_of_atom pos a in
               let e = make_exp loc (SetSubscript (v, int_zero_normal, ty, a', e)) in
                  info, e
          | XInitArray fields ->
               assign_init_fields tenv venv info pos loc a ty e fields

   (*
    * Coerce an atom.
    *)
   let rec coerce_atom tenv venv pos loc a ty cont =
      let pos = string_pos "coerce_atom" pos in
      let ty = tenv_expand tenv pos ty in
      let ty_a = tenv_expand tenv pos (type_of_atom venv pos a) in
         match ty, ty_a with
            TyDelayed, _
          | _, TyDelayed ->
               raise (AirException (pos, InternalError "unexpected TyDelayed"))

            (* Coerce anything to void *)
          | TyUnit 0, _
          | TyUnit 1, _ ->
               cont venv (AtomUnit (1, 0))

            (* Booleans require testing *)
          | TyUnit 2, TyZero ->
               cont venv bool_false
          | TyUnit 2, TyArray _ ->
               cont venv bool_true
          | TyUnit 2, TyUnit 2 ->
               cont venv a
          | TyUnit 2, TyUnit i ->
               let v = new_symbol_atom "bool_of_unit" a in
               let venv = venv_add venv v ty_bool in
               let info, e = cont venv (AtomVar v) in
               let pre = native_int_precision in
               let e = make_exp loc (LetBinop (v, ty_bool, NeqIntOp (pre, int_signed), a, int_zero pre int_signed, e)) in
                  info, e
          | TyUnit 2, TyInt (pre, signed) ->
               let v = new_symbol_atom "bool_of_int" a in
               let venv = venv_add venv v ty_bool in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetBinop (v, ty_bool, NeqIntOp (pre, signed), a, int_zero pre signed, e)) in
                  info, e
          | TyUnit 2, TyFloat pre ->
               let v = new_symbol_atom "bool_of_float" a in
               let venv = venv_add venv v ty_bool in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetBinop (v, ty_bool, NeqFloatOp pre, a, float_zero pre, e)) in
                  info, e
          | TyUnit 2, TyPointer ty ->
               let v = new_symbol_atom "bool_of_ptr" a in
               let venv = venv_add venv v ty_bool in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetUnop (v, ty_bool, NeqPointerNilOp, a, e)) in
                  info, e
          | TyUnit 2, TyRef ty ->
               let v = new_symbol_atom "bool_of_ref" a in
               let venv = venv_add venv v ty in
               let info, e = coerce_atom tenv venv pos loc (AtomVar v) ty_bool cont in
               let e = make_exp loc (LetSubscript (v, ty, var_of_atom pos a, int_zero_normal, e)) in
                  info, e

            (* Integer coercions *)
          | TyInt (pre, signed), TyZero ->
               cont venv (AtomInt (Rawint.of_int pre signed 0))
          | TyInt (pre, signed), TyUnit i ->
               let v = new_symbol_atom "int_of_unit" a in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetUnop (v, ty, IntOfUnit (pre, signed, i), a, e)) in
                  info, e
          | TyInt (pre, signed), TyEnum i ->
               let v = new_symbol_atom "int_of_enum" a in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetUnop (v, ty, IntOfInt (pre, signed, enum_precision, enum_signed), a, e)) in
                  info, e
          | TyEnum _, TyInt (pre, signed) ->
               if pre = enum_precision && signed = enum_signed then
                  cont venv a
               else
                  let v = new_symbol_atom "enum_of_int" a in
                  let venv = venv_add venv v ty in
                  let info, e = cont venv (AtomVar v) in
                  let e = make_exp loc (LetUnop (v, ty, IntOfInt (enum_precision, enum_signed, pre, signed), a, e)) in
                     info, e

          | TyInt (pre1, signed1), TyInt (pre2, signed2)
          | TyInt (pre1, signed1), TyField (pre2, signed2, _, _)
          | TyField (pre1, signed1, _, _), TyInt (pre2, signed2)
          | TyField (pre1, signed1, _, _), TyField (pre2, signed2, _, _) ->
               if pre1 = pre2 && signed1 = signed2 then
                  cont venv a
               else
                  let v = new_symbol_atom "int_of_int" a in
                  let venv = venv_add venv v ty in
                  let info, e = cont venv (AtomVar v) in
                  let e = make_exp loc (LetUnop (v, ty, IntOfInt (pre1, signed1, pre2, signed2), a, e)) in
                     info, e
          | TyInt (pre1, signed1), TyFloat pre2 ->
               let v = new_symbol_atom "int_of_float" a in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetUnop (v, ty, IntOfFloat (pre1, signed1, pre2), a, e)) in
                  info, e
          | TyInt (pre, signed), TyPointer ty2 ->
               let v = new_symbol_atom "int_of_ptr" a in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetUnop (v, ty, IntOfPointer (pre, signed), a, e)) in
                  info, e
          | TyFloat pre1, TyInt (pre2, signed2) ->
               let v = new_symbol_atom "float_of_int" a in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetUnop (v, ty, FloatOfInt (pre1, pre2, signed2), a, e)) in
                  info, e
          | TyFloat pre1, TyFloat pre2 ->
               if pre1 = pre2 then
                  cont venv a
               else
                  let v = new_symbol_atom "float_of_float" a in
                  let venv = venv_add venv v ty in
                  let info, e = cont venv (AtomVar v) in
                  let e = make_exp loc (LetUnop (v, ty, FloatOfFloat (pre1, pre2), a, e)) in
                     info, e
          | TyPointer ty1, TyZero ->
               cont venv (AtomNil ty1)
          | TyPointer ty1, TyInt (pre, signed) ->
               (match a with
                   AtomInt i when Rawint.is_zero i ->
                      cont venv (AtomNil ty1)
                 | _ ->
                      let v = new_symbol_atom "ptr_of_int" a in
                      let venv = venv_add venv v ty in
                      let info, e = cont venv (AtomVar v) in
                      let e = make_exp loc (LetUnop (v, ty, PointerOfInt (pre, signed), a, e)) in
                         info, e)
          | TyPointer ty1, TyPointer ty2 ->
               if equal_types tenv pos ty1 ty2 then
                  cont venv a
               else
                  let v = new_symbol_atom "ptr_of_ptr" a in
                  let venv = venv_add venv v ty in
                  let info, e = cont venv (AtomVar v) in
                  let e = make_exp loc (LetAtom (v, ty, a, e)) in
                     info, e

            (* Pointers, arrays, and refs *)
          | TyPointer ty1, TyArray (ty2, _, _)
          | TyArray (ty1, _, _), TyArray (ty2, _, _)
          | TyArray (ty1, _, _), TyRef ty2 ->
               if equal_types tenv pos ty1 ty2 then
                  cont venv a
               else
                  let v = new_symbol_atom "ptr_of_ptr" a in
                  let venv = venv_add venv v ty in
                  let info, e = cont venv (AtomVar v) in
                  let e = make_exp loc (LetAtom (v, ty, a, e)) in
                     info, e

            (* Reference coercions *)
          | TyRef ty1, TyRef ty2 ->
               if equal_types tenv pos ty1 ty2 then
                  cont venv a
               else
                  raise (AirException (pos, StringError "illegal ref coercion"))
          | TyRef ty1, ty2 ->
               let v = new_symbol_atom "ref_of_any" a in
               let v2 = var_of_atom pos a in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
                  info, make_exp loc (LetAddrOfVar (v, ty, v2, e))
          | ty1, TyRef ty2 ->
               let v = new_symbol_atom "any_of_ref" a in
               let venv = venv_add venv v ty2 in
               let info, e = coerce_atom tenv venv pos loc (AtomVar v) ty1 cont in
               let v2 = var_of_atom pos a in
                  info, make_exp loc (LetSubscript (v, ty2, v2, int_zero_normal, e))

            (* We have to box scalars to make them polymorphic *)
          | TyVar _, TyUnit _ ->
               let v = new_symbol_atom "box_unit" a in
               let ty' = ty_boxed_int native_int_precision true in
               let venv = venv_add venv v ty' in
               let info, e = cont venv (AtomVar v) in
               let _, size = sizeof_int_const native_int_precision in
               let e = make_exp loc (SetSubscript (v, int_zero_normal, ty, a, e)) in
               let e = make_exp loc (LetMalloc (v, AtomInt (Rawint.of_int native_int_precision true size), e)) in
                  info, e
          | TyVar _, TyInt (pre, signed) ->
               let v = new_symbol_atom "box_int" a in
               let ty' = ty_boxed_int pre signed in
               let venv = venv_add venv v ty' in
               let info, e = cont venv (AtomVar v) in
               let _, size = sizeof_int_const pre in
               let e = make_exp loc (SetSubscript (v, int_zero_normal, ty, a, e)) in
               let e = make_exp loc (LetMalloc (v, AtomInt (Rawint.of_int native_int_precision true size), e)) in
                  info, e
          | TyVar _, TyFloat pre ->
               let v = new_symbol_atom "box_float" a in
               let ty' = ty_boxed_float pre in
               let venv = venv_add venv v ty' in
               let info, e = cont venv (AtomVar v) in
               let _, size = sizeof_float_const pre in
               let e = make_exp loc (SetSubscript (v, int_zero_normal, ty, a, e)) in
               let e = make_exp loc (LetMalloc (v, AtomInt (Rawint.of_int native_int_precision true size), e)) in
                  info, e

            (* Pointers are already boxed *)
          | TyVar _, TyPointer _ ->
               cont venv a

            (* When converting back, we have to unbox *)
          | TyUnit _, TyVar _ ->
               let v1 = var_of_atom pos a in
               let v = new_symbol v1 in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetSubscript (v, ty, v1, int_zero_normal, e)) in
                  info, e
          | TyInt (pre, signed), TyVar _ ->
               let v1 = var_of_atom pos a in
               let v = new_symbol v1 in
               let ty = TyInt (pre, signed) in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetSubscript (v, ty, v1, int_zero_normal, e)) in
                  info, e
          | TyFloat pre, TyVar _ ->
               let v1 = var_of_atom pos a in
               let v = new_symbol v1 in
               let ty = TyFloat pre in
               let venv = venv_add venv v ty in
               let info, e = cont venv (AtomVar v) in
               let e = make_exp loc (LetSubscript (v, ty, v1, int_zero_normal, e)) in
                  info, e
          | TyPointer _, TyVar _ ->
               cont venv a

            (* Functions and function pointers *)
          | TyPointer _, TyFun _
          | TyFun _, TyPointer _ ->
               cont venv a

            (* Elides *)
          | TyElide _, TyElide _ ->
               cont venv a

            (* All others are ok as long as the types are the same *)
          | ty1, ty2 ->
               check_types tenv pos ty1 ty2;
               cont venv a

   let coerce_atom_opt tenv venv pos loc a_opt ty cont =
      match a_opt with
         Some a ->
            coerce_atom tenv venv pos loc a ty (fun venv a ->
                  cont venv (Some a))
       | None ->
            cont venv None

   (*
    * Coerce an argument list.
    *)
   let coerce_atoms tenv venv pos loc atoms tyl cont =
      let pos = string_pos "coerce_atoms" pos in
      let rec collect venv atoms'' atoms' tyl' =
         match atoms', tyl' with
            a :: atoms', ty :: tyl' ->
               coerce_atom tenv venv pos loc a ty (fun venv a ->
                     collect venv (a :: atoms'') atoms' tyl')
          | [], [] ->
               cont venv (List.rev atoms'')
          | _ ->
               raise (AirException (pos, ArityMismatch (List.length tyl, List.length atoms)))
      in
         collect venv [] atoms tyl

   (*
    * Don't allow refs in the type.
    *)
   let coerce_atom_noref tenv venv pos loc a ty cont =
      let ty =
         match tenv_expand tenv pos ty with
            TyRef ty ->
               ty
          | _ ->
               ty
      in
         coerce_atom tenv venv pos loc a ty (fun venv a ->
               cont venv a ty)

   (************************************************************************
    * TRANSLATE OPERATOR
    ************************************************************************)

   (*
    * Operator translation is basically an identity.
    *)
   let build_unop = function
      (* Native ints *)
      Fc_ast.UMinusIntOp (pre, signed) -> UMinusIntOp (pre, signed)
    | Fc_ast.NotIntOp    (pre, signed) -> NotIntOp (pre, signed)

      (* Floats *)
    | Fc_ast.UMinusFloatOp pre -> UMinusFloatOp pre
    | Fc_ast.AbsFloatOp    pre -> AbsFloatOp pre
    | Fc_ast.SinOp         pre -> SinOp pre
    | Fc_ast.CosOp         pre -> CosOp pre
    | Fc_ast.SqrtOp        pre -> SqrtOp pre

      (* Pointers *)
    | Fc_ast.EqPointerNilOp -> EqPointerNilOp
    | Fc_ast.NeqPointerNilOp -> NeqPointerNilOp

   let build_binop = function
      Fc_ast.AndBoolOp                 -> AndBoolOp
    | Fc_ast.OrBoolOp                  -> OrBoolOp
    | Fc_ast.EqBoolOp                  -> EqBoolOp
    | Fc_ast.NeqBoolOp                 -> NeqBoolOp

    | Fc_ast.PlusIntOp   (pre, signed) -> PlusIntOp (pre, signed)
    | Fc_ast.MinusIntOp  (pre, signed) -> MinusIntOp (pre, signed)
    | Fc_ast.MulIntOp    (pre, signed) -> MulIntOp (pre, signed)
    | Fc_ast.DivIntOp    (pre, signed) -> DivIntOp (pre, signed)
    | Fc_ast.RemIntOp    (pre, signed) -> RemIntOp (pre, signed)
    | Fc_ast.SlIntOp     (pre, signed) -> SlIntOp (pre, signed)
    | Fc_ast.SrIntOp     (pre, signed) -> SrIntOp (pre, signed)
    | Fc_ast.AndIntOp    (pre, signed) -> AndIntOp (pre, signed)
    | Fc_ast.OrIntOp     (pre, signed) -> OrIntOp (pre, signed)
    | Fc_ast.XorIntOp    (pre, signed) -> XorIntOp (pre, signed)

    | Fc_ast.MinIntOp    (pre, signed) -> MinIntOp (pre, signed)
    | Fc_ast.MaxIntOp    (pre, signed) -> MaxIntOp (pre, signed)

    | Fc_ast.EqIntOp     (pre, signed) -> EqIntOp (pre, signed)
    | Fc_ast.NeqIntOp    (pre, signed) -> NeqIntOp (pre, signed)
    | Fc_ast.LtIntOp     (pre, signed) -> LtIntOp (pre, signed)
    | Fc_ast.LeIntOp     (pre, signed) -> LeIntOp (pre, signed)
    | Fc_ast.GtIntOp     (pre, signed) -> GtIntOp (pre, signed)
    | Fc_ast.GeIntOp     (pre, signed) -> GeIntOp (pre, signed)

    | Fc_ast.PlusFloatOp   pre -> PlusFloatOp pre
    | Fc_ast.MinusFloatOp  pre -> MinusFloatOp pre
    | Fc_ast.MulFloatOp    pre -> MulFloatOp pre
    | Fc_ast.DivFloatOp    pre -> DivFloatOp pre
    | Fc_ast.RemFloatOp    pre -> RemFloatOp pre

    | Fc_ast.MinFloatOp    pre -> MinFloatOp pre
    | Fc_ast.MaxFloatOp    pre -> MaxFloatOp pre

    | Fc_ast.EqFloatOp     pre -> EqFloatOp pre
    | Fc_ast.NeqFloatOp    pre -> NeqFloatOp pre
    | Fc_ast.LtFloatOp     pre -> LtFloatOp pre
    | Fc_ast.LeFloatOp     pre -> LeFloatOp pre
    | Fc_ast.GtFloatOp     pre -> GtFloatOp pre
    | Fc_ast.GeFloatOp     pre -> GeFloatOp pre
    | Fc_ast.Atan2Op       pre -> Atan2Op pre

      (* Pointer operations *)
    | Fc_ast.PlusPointerOp -> PlusPointerOp
    | Fc_ast.MinusPointerOp -> MinusPointerOp

    | Fc_ast.EqPointerOp    -> EqPointerOp
    | Fc_ast.NeqPointerOp   -> NeqPointerOp
    | Fc_ast.LtPointerOp    -> LtPointerOp
    | Fc_ast.LePointerOp    -> LePointerOp
    | Fc_ast.GtPointerOp    -> GtPointerOp
    | Fc_ast.GePointerOp    -> GePointerOp

   (************************************************************************
    * TRANSLATE PATTERN
    ************************************************************************)

   (*
    * Partial-match information.
    * Based on the paper "ML pattern match compilation and partial evaluation",
    * by Peter Sestoft, in Dagstuhl Seminar on Partial Evaluation, LNCS, Springer.
    *)

   (*
    * There is positive and negative information for each term that we've seen.
    *)
   type term_info =
      { pos_int_info : RawIntSet.t;
        neg_int_info : RawIntSet.t;
        pos_string_info : StringSet.t;
        neg_string_info : StringSet.t;
        struct_info : term_info list;
        pos_uenum_info : (label * term_info) list;
        neg_uenum_info : label list
      }

   (*
    * Default term info.
    *)
   let rawint_empty = RawIntSet.empty Rawint.Int64 true

   let term_info_empty =
      { pos_int_info = rawint_empty;
        neg_int_info = rawint_empty;
        pos_string_info = StringSet.empty;
        neg_string_info = StringSet.empty;
        struct_info = [];
        pos_uenum_info = [];
        neg_uenum_info = []
      }

   (*
    * Replace a term_info in the list.
    *)
   let rec replace_nth i info = function
      info' :: infos ->
         if i = 0 then
            info :: infos
         else
            info' :: replace_nth (pred i) info infos
    | [] ->
         if i = 0 then
            [info]
         else
            term_info_empty :: replace_nth (pred i) info []

   let rec nth_info i = function
      info :: infos ->
         if i = 0 then
            info
         else
            nth_info (pred i) infos
    | [] ->
         term_info_empty

   (*
    * Term info for an integer match.
    *)
   let term_info_of_int term_info i =
      { term_info with pos_int_info = RawIntSet.of_point i;
                       neg_int_info = RawIntSet.empty (Rawint.precision i) (Rawint.signed i)
      }

   let term_info_of_string term_info s =
      { term_info with pos_string_info = StringSet.of_point s;
                       neg_string_info = StringSet.empty
      }

   let term_info_of_struct term_info term_infos =
      { term_info with struct_info = term_infos }

   let term_info_of_tag term_info tag =
      { term_info with pos_uenum_info = [tag, term_info_empty] }

   (*
    * Term info for an integer mismatch.
    *)
   let term_info_remove_int term_info i =
      let { pos_int_info = pos_set;
            neg_int_info = neg_set
          } = term_info
      in
      let pos_set = RawIntSet.subtract_point pos_set i in
      let neg_set = RawIntSet.add_point pos_set i in
         { term_info with pos_int_info = pos_set;
                          neg_int_info = neg_set
         }

   let term_info_remove_string term_info s =
      let { pos_string_info = pos_set;
            neg_string_info = neg_set
          } = term_info
      in
      let pos_set = StringSet.subtract_point pos_set s in
      let neg_set = StringSet.add_point pos_set s in
         { term_info with pos_string_info = pos_set;
                          neg_string_info = neg_set
         }

   let term_info_remove_tag term_info tag =
      let { pos_uenum_info = pos_info;
            neg_uenum_info = neg_info
          } = term_info
      in
      let pos_info = List.remove_assoc tag pos_info in
      let neg_info =
         if List.mem tag neg_info then
            neg_info
         else
            tag :: neg_info
      in
         { term_info with pos_uenum_info = pos_info;
                          neg_uenum_info = neg_info
         }

   (*
    * Perform a pattern match.
    *)
   let rec build_patt tenv venv info pos a term_info patt fail cont =
      let pos = string_pos "build_patt" pos in
         match patt with
            Fc_ast.IntPattern (loc, i) ->
               build_patt_int tenv venv info pos loc a term_info i fail cont
          | Fc_ast.EnumPattern (loc, _, i) ->
               let i = Rawint.of_int32 native_int_precision true i in
                  build_patt_int tenv venv info pos loc a term_info i fail cont
          | Fc_ast.FloatPattern (loc, x) ->
               build_patt_float tenv venv info pos loc a term_info x fail cont
          | Fc_ast.StringPattern (loc, pre, s) ->
               build_patt_string tenv venv info pos loc a term_info pre s fail cont
          | Fc_ast.StructPattern (loc, fields) ->
               build_patt_struct tenv venv info pos loc a term_info fields fail cont
          | Fc_ast.UEnumPattern (loc, _, v, p) ->
               build_patt_uenum tenv venv info pos loc a term_info v p fail cont
          | Fc_ast.VEnumPattern (loc, v, p) ->
               raise (AirException (pos, InternalError "unexpected VEnumPattern"))
          | Fc_ast.VarPattern (loc, v, _) ->
               build_patt_var tenv venv info pos loc a term_info v cont
          | Fc_ast.AsPattern (loc, p1, p2) ->
               let cont venv info a term_info' =
                  build_patt tenv venv info pos a term_info' p2 fail cont
               in
                  build_patt tenv venv info pos a term_info p1 fail cont

   (*
    * Var pattern always matches.
    *)
   and build_patt_var tenv venv info pos loc a term_info v cont =
      let pos = string_pos "build_patt_var" pos in
      let ty = type_of_atom venv pos a in
      let venv = venv_add venv v ty in
      let info, e = cont venv info a term_info in
      let e = make_exp loc (LetAtom (v, ty, a, e)) in
         info, e

   (*
    * Check against an integer constant.
    *)
   and build_patt_int tenv venv info pos loc a term_info i fail cont =
      let pos = string_pos "build_patt_int" pos in
      let { pos_int_info = pos_set;
            neg_int_info = neg_set
          } = term_info
      in
         if RawIntSet.is_singleton pos_set && RawIntSet.dest_singleton pos_set = i then
            (* We definitely have this case *)
            cont venv info a term_info
         else if RawIntSet.mem_point i neg_set then
            (* Definitely don't have this case *)
            fail info a term_info
         else
            (* Might have this set *)
            let v' = new_symbol_string "build_patt_int" in
            let true_info = term_info_of_int term_info i in
            let false_info = term_info_remove_int term_info i in
            let info, e_true = cont venv info a true_info in
            let info, e_false = fail info a false_info in
            let e = make_exp loc (IfThenElse (AtomVar v', e_true, e_false)) in
            let pre = Rawint.precision i in
            let signed = Rawint.signed i in
            let ty = TyInt (pre, signed) in
               coerce_atom tenv venv pos loc a ty (fun venv a ->
                     let e = make_exp loc (LetBinop (v', ty_bool, EqIntOp (pre, signed), a, AtomInt i, e)) in
                        info, e)

   (*
    * For floats, we always check against the constant.
    *)
   and build_patt_float tenv venv info pos loc a term_info x fail cont =
      let pos = string_pos "build_patt_float" pos in
      let v' = new_symbol_string "build_patt_float" in
      let pre = Rawfloat.precision x in
      let info, e_true = cont venv info a term_info in
      let info, e_false = fail info a term_info in
      let e = make_exp loc (IfThenElse (AtomVar v', e_true, e_false)) in
      let e = make_exp loc (LetBinop (v', ty_bool, EqFloatOp pre, a, AtomFloat x, e)) in
         info, e

   (*
    * For strings, we have to place the string in the global area,
    * the use a string comparison.
    *)
   and build_patt_string tenv venv info pos loc a term_info pre s fail cont =
      let pos = string_pos "build_patt_string" pos in
      let { pos_string_info = pos_set;
            neg_string_info = neg_set
          } = term_info
      in
         if StringSet.is_singleton pos_set && StringSet.dest_singleton pos_set = s then
            cont venv info a term_info
         else if StringSet.mem_point s neg_set then
            fail info a term_info
         else
            (* Add the string to the globals *)
            let v_string = new_symbol_string "string" in
            let pre = char_precision pre in
            let ty_string = TyPointer (TyInt (pre, char_signed)) in
            let info = { info with info_strings = (v_string, ty_string, pre, s) :: info.info_strings } in

            (* Now perform the comparison *)
            let v' = new_symbol_string "build_patt_string" in
            let true_info = term_info_of_string term_info s in
            let false_info = term_info_remove_string term_info s in
            let info, e_true = cont venv info a true_info in
            let info, e_false = fail info a false_info in
            let e = make_exp loc (IfThenElse (AtomVar v', e_true, e_false)) in
               coerce_atom tenv venv pos loc a ty_string (fun venv a ->
                     let e = make_exp loc (LetExtCall (v', ty_bool, string_equal_name, false, string_equal_ty, [a; AtomVar v_string], e)) in
                     info, e)

   (*
    * Struct pattern.
    * We project all the parts, then match on each of them.
    *)
   and build_patt_struct tenv venv info pos loc a term_info fields fail cont =
      let pos = string_pos "build_patt_struct" pos in

      (* Match against each of the parts *)
      let rec trans venv info field_info term_infos fields =
         match fields with
            (label, p) :: fields ->
               let index, ty, field_info = fetch_field_info field_info pos label in
               let term_info = nth_info index term_infos in
               let cont' venv info a term_info' =
                  let term_infos = replace_nth index term_info' term_infos in
                  let field_info = next_field_info field_info pos in
                     trans venv info field_info term_infos fields
               in
               let fail' info a term_info' =
                  let term_infos = replace_nth index term_info' term_infos in
                  let term_info = term_info_of_struct term_info term_infos in
                     fail info a term_info
               in
                  build_let_ref_of_subscript venv a field_info pos loc (fun venv a ->
                        build_patt tenv venv info pos a term_info p fail' cont')
          | [] ->
               let term_info = term_info_of_struct term_info term_infos in
                  cont venv info a term_info
      in
      let ty = type_of_atom venv pos a in
      let field_info = build_field_info tenv pos ty in
      let term_infos = term_info.struct_info in
         trans venv info field_info term_infos fields

   (*
    * Enum pattern.
    * We have to translate the tag.
    *)
   and build_patt_uenum tenv venv info pos loc a term_info v p_opt fail cont =
      let pos = string_pos "build_patt_uenum" pos in

      (* Get the enum info *)
      let ty = type_of_atom venv pos a in
      let ty_opt, tag = dest_uenum_tag tenv pos v ty in
      let info = info_add_tag info tag in
      let { pos_uenum_info = pos_info;
            neg_uenum_info = neg_info
          } = term_info
      in
         match pos_info with
            [tag', term_info] when Symbol.eq tag' tag ->
               cont venv info a term_info
          | _ ->
               if List.mem tag neg_info then
                  fail info a term_info
               else
                  (* Do the tag test *)
                  let v = var_of_atom pos a in
                  let v1 = new_symbol v in
                  let true_info = term_info_of_tag term_info tag in
                  let false_info = term_info_remove_tag term_info tag in
                  let info, e_true =
                     match ty_opt, p_opt with
                        Some ty, Some p ->
                           let v = var_of_atom pos a in
                           let v' = new_symbol v in
                           let v'' = new_symbol v in
                           let venv = venv_add venv v' (TyPointer ty) in
                           let venv = venv_add venv v'' ty in
                           let info, e = build_patt tenv venv info pos (AtomVar v'') term_info p fail cont in
                           let e = make_exp loc (LetSubscript (v'', ty, v', AtomInt (Rawint.of_int Rawint.Int32 false 0), e)) in
                           let e = make_exp loc (LetBinop (v', TyPointer ty, PlusPointerOp, a, off_tag, e)) in
                              info, e
                      | None, None ->
                           cont venv info a true_info
                      | _ ->
                           let len1 = if ty_opt = None then 0 else 1 in
                           let len2 = if p_opt = None then 0 else 1 in
                              raise (AirException (pos, ArityMismatch (len1, len2)))
                  in
                  let info, e_false = fail info a false_info in
                     build_let_tag a pos loc (fun a ->
                           let e = make_exp loc (IfThenElse (AtomVar v1, e_true, e_false)) in
                           let e = make_exp loc (LetBinop (v1, ty_bool, EqTagOp, a, AtomVar tag, e)) in
                              info, e)

   (************************************************************************
    * TRANSLATE EXPRESSION
    ************************************************************************)

   (*
    * Translate an AST expression.
    *)
   let rec build_exp tenv venv info e =
      let pos = string_pos "build_exp" (ast_exp_pos e) in
         match e with
            Fc_ast.LetTypes (loc, types, e) ->
               build_types_exp tenv venv info pos loc types e
          | Fc_ast.LetFuns (loc, funs, e) ->
               build_funs_exp tenv venv info pos loc funs e
          | Fc_ast.LetFunDecl (loc, sc, f, label, gflag, ty, e) ->
               build_fundecl_exp tenv venv info pos loc sc f label gflag ty e
          | Fc_ast.LetVar (loc, sc, v, label, gflag, ty, a_opt, e) ->
               build_var_exp tenv venv info pos loc sc v label gflag ty a_opt e
          | Fc_ast.LetCoerce (loc, v, ty, a, e) ->
               build_coerce_exp tenv venv info pos loc v ty a e
          | Fc_ast.LetAtom (loc, v, ty, a, e) ->
               build_atom_exp tenv venv info pos loc v ty a e
          | Fc_ast.LetUnop (loc, v, ty, op, a, e) ->
               build_unop_exp tenv venv info pos loc v ty op a e
          | Fc_ast.LetBinop (loc, v, ty, op, a1, a2, e) ->
               build_binop_exp tenv venv info pos loc v ty op a1 a2 e
          | Fc_ast.LetExtCall (loc, v, ty, s, b, ty', args, e) ->
               build_extcall_exp tenv venv info pos loc v ty s b ty' args e
          | Fc_ast.LetApply (loc, v, ty, f, label, args, e) ->
               build_apply_exp tenv venv info pos loc v ty f label args e
          | Fc_ast.LetTest (loc, v, ty, a, e) ->
               build_test_exp tenv venv info pos loc v ty a e
          | Fc_ast.TailCall (loc, f, args) ->
               build_tailcall_exp tenv venv info pos loc f args
          | Fc_ast.Return (loc, f, ty, a) ->
               build_return_exp tenv venv info pos loc f ty a
          | Fc_ast.LetString (loc, v, ty, pre, s, e) ->
               build_string_exp tenv venv info pos loc v ty pre s e
          | Fc_ast.LetAlloc (loc, v, ty, label, args, e) ->
               build_alloc_exp tenv venv info pos loc v ty label args e
          | Fc_ast.LetMalloc (loc, v, a, e) ->
               build_malloc_exp tenv venv info pos loc v a e
          | Fc_ast.IfThenElse (loc, a, e1, e2) ->
               build_if_exp tenv venv info pos loc a e1 e2
          | Fc_ast.Match (loc, a, ty, cases) ->
               build_match_exp tenv venv info pos loc a ty cases
          | Fc_ast.Try (loc, e1, v, e2) ->
               build_try_exp tenv venv info pos loc e1 v e2
          | Fc_ast.Raise (loc, a) ->
               build_raise_exp tenv venv info pos loc a
          | Fc_ast.Debug (loc, debug, e) ->
               build_debug_exp tenv venv info pos loc debug e
          | Fc_ast.LetProject (loc, v1, ty, a2, v3, e) ->
               build_project_exp tenv venv info pos loc v1 ty a2 v3 e
          | Fc_ast.LetSubscript (loc, v, ty, v1, ty2, a2, e) ->
               build_subscript_exp tenv venv info pos loc v ty v1 ty2 a2 e
          | Fc_ast.Set (loc, a1, ty, a2, e) ->
               build_set_exp tenv venv info pos loc a1 ty a2 e
          | Fc_ast.LetAddrOf (loc, v1, ty, a, e) ->
               build_addr_of_exp tenv venv info pos loc v1 ty a e
          | Fc_ast.LetSizeof (loc, v1, ty1, ty2, e) ->
               build_sizeof_exp tenv venv info pos loc v1 ty1 ty2 e

   (*
    * Add all the type definitions to the toplevel type environment.
    *)
   and build_types_exp tenv venv info pos loc types e =
      let pos = string_pos "build_types_exp" pos in
      let rec collect tenv types' = function
         (v, label, ty) :: types ->
            build_type tenv pos ty (fun ty ->
                  let tenv = tenv_add tenv v ty in
                  let tenv = tenv_add tenv label ty in
                  let types' = (v, ty) :: types' in
                     collect tenv types' types)
       | [] ->
            let info = { info with info_tenv = types' @ info.info_tenv } in
               build_exp tenv venv info e
      in
         collect tenv info.info_tenv types

   (*
    * Add the function definitions.
    *)
   and build_funs_exp tenv venv info pos loc funs e =
      let pos = string_pos "build_funs_exp" pos in

      (* Add the functions to the variable environment *)
      let rec collect venv funs' = function
         (_, f, label, line, gflag, ty, vars, e) :: funs ->
            build_type tenv pos ty (fun ty ->
                  let fund = f, label, line, gflag, ty, vars, e in
                  let funs' = fund :: funs' in
                  let venv = venv_add venv f ty in
                     collect venv funs' funs)
       | [] ->
            (* Collect the functions *)
            let info, funs =
               List.fold_left (fun (info, funs) fund ->
                     let info, fund = build_fun tenv venv info pos fund in
                        info, fund :: funs) (info, []) funs'
            in

            (* Collect the rest *)
            let info, e = build_exp tenv venv info e in
               info, make_exp loc (LetFuns (funs, e))
      in
         collect venv [] funs

   and build_fun tenv venv info pos (f, label, line, gflag, f_ty, vars, e) =
      let _, ty_vars, ty_res = dest_fun_type tenv pos f_ty in
      let _, _, info = info_add_export tenv venv info pos f label f_ty StoreAuto gflag in
      let venv = List.fold_left2 venv_add venv vars ty_vars in
      let venv = venv_add venv return_sym ty_res in
      let info, e = build_exp tenv venv info e in
      let gflag = build_var_class gflag in
         info, (f, line, gflag, f_ty, vars, e)

   (*
    * Function declaration.
    *)
   and build_fundecl_exp tenv venv info pos loc sc v label gflag ty e =
      let pos = string_pos "build_fundecl_exp" pos in
         build_type tenv pos ty (fun ty ->
               let venv = venv_add venv v ty in
(*
 * Don't add export for declarations.
 * The export will be added only if there is a definition
 * somewhere in the file.
               let _, _, info = info_add_export tenv venv info pos v label ty gflag in
 *)
               let info = { info with info_decls = (v, ty) :: info.info_decls } in
                  match sc with
                     StoreStatic ->
                        let info = { info with info_decls = (v, ty) :: info.info_decls } in
                           build_exp tenv venv info e
                   | StoreExtern
                   | StoreAuto
                   | StoreRegister ->
                        let info = info_add_import tenv info pos v label ty gflag in
                           build_exp tenv venv info e)

   (*
    * Variable declaration.
    * If this is static, add it to the globals.
    *)
   and build_var_exp tenv venv info pos loc sc v label gflag ty init e =
      let pos = string_pos "build_var_exp" pos in
      let volp = Fc_ast_util.status_of_type ty = StatusVolatile in
         build_type tenv pos ty (fun ty ->
               let eflag, ty', info = info_add_export tenv venv info pos v label ty sc gflag in
               let venv = venv_add venv v ty' in
                  match sc with
                     StoreExtern ->
                        build_extern_var tenv venv info pos loc v label gflag ty ty' init e
                   | StoreStatic
                   | StoreAuto
                   | StoreRegister ->  (* Register declarations are ignored *)
                        build_var tenv venv info pos loc v ty eflag ty' volp init e)

   (*
    * Extern vars.
    *)
   and build_extern_var tenv venv info pos loc v label gflag ty ty' init e =
      let pos = string_pos "build_extern_var" pos in
         match init with
            Fc_ast.InitNone ->
               let info = info_add_import tenv info pos v label ty' gflag in
                  build_exp tenv venv info e
          | Fc_ast.InitAtom _
          | Fc_ast.InitArray _ ->
               raise (AirException (pos, StringError "can't initialize extern"))

   (*
    * Local vars are global if they are toplevel.
    *)
   and build_var tenv venv info pos loc v ty eflag ty' volp init e =
      let pos = string_pos "build_auto_var" pos in
         build_init tenv pos init (fun a_opt init ->
         coerce_atom_opt tenv venv pos loc a_opt ty (fun venv a_opt ->
               let info =
                  if eflag then
                     let init = build_global_init tenv pos a_opt ty' in
                        { info with info_globals = (v, ty', volp, init) :: info.info_globals }
                  else
                     info
               in
               let info, e = build_exp tenv venv info e in
               let info, e =
                  match a_opt with
                     Some _ ->
                        info, e
                   | None ->
                        let v' = new_symbol v in
                        let info, e = assign_init tenv venv info pos loc (AtomVar v') ty e init in
                        let e = make_exp loc (LetAddrOfVar (v', TyPointer ty, v, e)) in
                           info, e
               in
               let e =
                  if eflag then
                     match a_opt with
                        Some a when is_var_atom a ->
                           make_exp loc (SetVar (v, ty, a, e))
                      | _ ->
                           e
                  else
                     make_exp loc (LetVar (v, ty, a_opt, e))
               in
                  info, e))

   (*
    * This is an explicit coercion.
    * We invoke the generic coercion routine.
    *)
   and build_coerce_exp tenv venv info pos loc v ty a e =
      let pos = string_pos "build_coerce_exp" pos in
         build_type tenv pos ty (fun ty ->
               build_atom tenv pos a (fun a ->
                     let venv = venv_add venv v ty in
                     let info, e = build_exp tenv venv info e in
                        coerce_atom tenv venv pos loc a ty (fun venv a ->
                              info, make_exp loc (LetAtom (v, ty, a, e)))))

   (*
    * Compute the size of a type.
    *)
   and build_sizeof_exp tenv venv info pos loc v ty1 ty2 e =
      let pos = string_pos "build_sizeof_exp" pos in
         build_type tenv pos ty1 (fun ty1 ->
         build_type tenv pos ty2 (fun ty2 ->
               let ty2 =
                  match tenv_expand tenv pos ty2 with
                     TyRef ty ->
                        ty
                   | _ ->
                        ty2
               in
                  build_sizeof tenv pos loc ty2 (fun a ->
                        let venv = venv_add venv v ty1 in
                        let info, e = build_exp tenv venv info e in
                           info, make_exp loc (LetAtom (v, ty1, a, e)))))

   (*
    * This is special type of coercion,
    * for testing against 0.
    *)
   and build_test_exp tenv venv info pos loc v ty a e =
      let pos = string_pos "build_coerce_exp" pos in
         build_type tenv pos ty (fun ty ->
         build_atom tenv pos a (fun a ->
               let venv = venv_add venv v ty in
               let info, e = build_exp tenv venv info e in
                  coerce_atom tenv venv pos loc a ty_bool (fun venv a ->
                  coerce_atom tenv venv pos loc a ty (fun venv a ->
                        info, make_exp loc (LetAtom (v, ty, a, e))))))

   (*
    * Function call.
    * Make sure to coerce the arguments.
    *)
   and build_atom_exp tenv venv info pos loc v ty a e =
      let pos = string_pos "build_op_exp" pos in
      build_type tenv pos ty (fun ty ->
            let venv = venv_add venv v ty in
            let info, e = build_exp tenv venv info e in
               build_atom tenv pos a (fun a ->
               coerce_atom tenv venv pos loc a ty (fun venv a ->
                     (* Coerce the result *)
                     let e = make_exp loc (LetAtom (v, ty, a, e)) in
                        info, e)))

   and build_unop_exp tenv venv info pos loc v ty op a e =
      let pos = string_pos "build_unop_exp" pos in
      build_type tenv pos ty (fun ty ->
            let op = build_unop op in
            let ty_arg, ty_res = type_of_unop ty op in
            let venv' = venv_add venv v ty in
            let info, e = build_exp tenv venv' info e in
               build_atom tenv pos a (fun a ->
               coerce_atom tenv venv pos loc a ty_arg (fun venv a ->
                     (* Coerce the result *)
                     let v' = new_symbol v in
                     let venv = venv_add venv v' ty_res in
                     let info, e =
                        coerce_atom tenv venv pos loc (AtomVar v') ty (fun venv a ->
                              info, make_exp loc (LetAtom (v, ty, a, e)))
                     in
                     let e = make_exp loc (LetUnop (v', ty_res, op, a, e)) in
                        info, e)))

   and build_binop_exp tenv venv info pos loc v ty op a1 a2 e =
      let pos = string_pos "build_binop_exp" pos in
      build_type tenv pos ty (fun ty ->
            let op = build_binop op in
            let ty_arg1, ty_arg2, ty_res = type_of_binop ty op in
            let venv' = venv_add venv v ty in
            let info, e = build_exp tenv venv' info e in
               build_atom tenv pos a1 (fun a1 ->
               build_atom tenv pos a2 (fun a2 ->
               coerce_atom tenv venv pos loc a1 ty_arg1 (fun venv a1 ->
               coerce_atom tenv venv pos loc a2 ty_arg2 (fun venv a2 ->
                     (* Coerce the result *)
                     let v' = new_symbol v in
                     let venv = venv_add venv v' ty_res in
                     let info, e =
                        coerce_atom tenv venv pos loc (AtomVar v') ty (fun venv a ->
                              info, make_exp loc (LetAtom (v, ty, a, e)))
                     in

                     (* Check for PlusPointerOp *)
                     match op with
                        PlusPointerOp ->
                           let ty_pointed_to = dest_pointer_allrefs_type tenv pos ty_arg1 in
                              build_sizeof tenv pos loc ty_pointed_to (fun size ->
                                    let v_size = new_symbol_string "times_size" in
                                    let e = make_exp loc (LetBinop (v', ty_res, op, a1, AtomVar v_size, e)) in
                                    let e = make_exp loc (LetBinop (v_size, ty_int, MulIntOp (pre_int, signed_int), a2, size, e)) in
                                       info, e)

                      | _ ->
                           let e = make_exp loc (LetBinop (v', ty_res, op, a1, a2, e)) in
                              info, e)))))

   (*
    * External call.
    * Make sure to coerce the arguments.
    *)
   and build_extcall_exp tenv venv info pos loc v ty s b ty' args e =
      let pos = string_pos "build_extcall_exp" pos in
      build_type tenv pos ty (fun ty ->
      build_type tenv pos ty' (fun f_ty ->
            let _, ty_args, ty_res = dest_fun_type tenv pos f_ty in
            let venv' = venv_add venv v ty in
            let info, e = build_exp tenv venv' info e in
               build_atoms tenv pos args (fun args ->
               coerce_atoms tenv venv pos loc args ty_args (fun venv args ->
                     (* Coerce the result *)
                     let v' = new_symbol v in
                     let venv = venv_add venv v' ty_res in
                     let info, e =
                        coerce_atom tenv venv pos loc (AtomVar v') ty (fun venv a ->
                              info, make_exp loc (LetAtom (v, ty, a, e)))
                     in
                     let e = make_exp loc (LetExtCall (v', ty_res, s, b, f_ty, args, e)) in
                        info, e))))

   (*
    * Operator.
    * Make sure to coerce the arguments.
    * Here we drop the label.
    *)
   and build_apply_exp tenv venv info pos loc v ty f label args e =
      let pos = string_pos "build_op_exp" pos in
      build_type tenv pos ty (fun ty ->
            let f_ty = venv_lookup venv pos f in
            let _, ty_args, ty_res = dest_fun_type tenv pos f_ty in
            let venv' = venv_add venv v ty in
            let info, e = build_exp tenv venv' info e in
               build_atoms tenv pos args (fun args ->
               coerce_atoms tenv venv pos loc args ty_args (fun venv args ->
                     (* Coerce the result *)
                     let v' = new_symbol v in
                     let venv = venv_add venv v' ty_res in
                     let info, e =
                        coerce_atom tenv venv pos loc (AtomVar v') ty (fun venv a ->
                              info, make_exp loc (LetAtom (v, ty, a, e)))
                     in
                     let e = make_exp loc (LetApply (v', ty_res, f, args, e)) in
                        info, e)))

   (*
    * Normal tailcall.
    *)
   and build_tailcall_exp tenv venv info pos loc f args =
      let pos = string_pos "build_tailcall_exp" pos in
      let f_ty = venv_lookup venv pos f in
      let _, ty_args, _ = dest_fun_type tenv pos f_ty in
         build_atoms tenv pos args (fun args ->
         coerce_atoms tenv venv pos loc args ty_args (fun venv args ->
               let e = make_exp loc (TailCall (f, args)) in
                  info, e))

   (*
    * Return statement.
    *)
   and build_return_exp tenv venv info pos loc f ty a =
      let pos = string_pos "build_return_exp" pos in
         build_type tenv pos ty (fun ty ->
               build_atom tenv pos a (fun a ->
                     coerce_atom tenv venv pos loc a ty (fun venv a ->
                           info, make_exp loc (Return (f, a)))))

   (*
    * Strings are allocated in the globals.
    *)
   and build_string_exp tenv venv info pos loc v ty pre s e =
      let pos = string_pos "build_string_exp" pos in
         build_type tenv pos ty (fun ty ->
               let info, v' = build_string tenv venv info pos ty pre s in
               let venv = venv_add venv v ty in
               let info, e = build_exp tenv venv info e in
               let e = make_exp loc (LetAtom (v, ty, AtomVar v', e)) in
                  info, e)

   and build_string tenv venv info pos ty pre s =
      let pos = string_pos "build_string" pos in
      let pre = char_precision pre in
      let v = new_symbol_string "string" in
      let info = { info with info_strings = (v, ty, pre, s) :: info.info_strings } in
         info, v

   (*
    * Tuple allocation.
    *)
   and build_alloc_exp tenv venv info pos loc v ty label args e =
      let pos = string_pos "build_alloc_exp" pos in
         build_type tenv pos ty (fun ty ->
               let ty = dest_pointer_ref_type tenv pos ty in
               let ty_args, label = dest_uenum_tag tenv pos label ty in
               let info = info_add_tag info label in
               let venv' = venv_add venv v ty in
               let info, e = build_exp tenv venv' info e in
                  match ty_args, args with
                     Some ty', [a] ->
                        build_atom tenv pos a (fun a ->
                        coerce_atom tenv venv pos loc a ty' (fun venv a ->
                        build_sizeof tenv pos loc ty (fun size ->
                              let e = make_exp loc (SetSubscript (v, off_tag, ty', a, e)) in
                              let e = make_exp loc (SetSubscript (v, int_zero_normal, TyTag, AtomVar label, e)) in
                              let e = make_exp loc (LetMalloc (v, size, e)) in
                                 info, e)))
                   | None, [] ->
                        build_sizeof tenv pos loc ty (fun size ->
                              let e = make_exp loc (SetSubscript (v, int_zero_normal, TyTag, AtomVar label, e)) in
                              let e = make_exp loc (LetMalloc (v, size, e)) in
                                 info, e)
                   | _ ->
                        let len1 =
                           match ty_args with
                              Some _ -> 1
                            | None -> 0
                        in
                        let len2 = List.length args in
                           raise (AirException (pos, ArityMismatch (len1, len2))))

   (*
    * Malloc.
    *)
   and build_malloc_exp tenv venv info pos loc v a e =
      let pos = string_pos "build_malloc_exp" pos in
      let venv = venv_add venv v (TyPointer (TyInt (native_char_precision, false))) in
      let info, e = build_exp tenv venv info e in
         build_atom tenv pos a (fun a ->
               let e = make_exp loc (LetMalloc (v, a, e)) in
                  info, e)

   (*
    * If-then-else.
    *)
   and build_if_exp tenv venv info pos loc a e1 e2 =
      let pos = string_pos "build_if_exp" pos in
      let info, e1 = build_exp tenv venv info e1 in
      let info, e2 = build_exp tenv venv info e2 in
         build_atom tenv pos a (fun a ->
               let e = make_exp loc (IfThenElse (a, e1, e2)) in
                  info, e)

   (*
    * Pattern matching.
    *)
   and build_match_exp tenv venv info pos loc a ty cases =
      let pos = string_pos "build_match_exp" pos in
      let rec collect venv info a term_info = function
         (p, e) :: cases ->
            let success venv info _ _ =
               build_exp tenv venv info e
            in
            let fail info a term_info =
               collect venv info a term_info cases
            in
               build_patt tenv venv info pos a term_info p fail success
       | [] ->
            raise (AirException (pos, StringError "match is not exhaustive"))
      in
         build_type tenv pos ty (fun ty ->
         build_atom tenv pos a (fun a ->
         coerce_atom tenv venv pos loc a ty (fun venv a ->
               collect venv info a term_info_empty cases)))

   (*
    * Exception handling.
    *)
   and build_try_exp tenv venv info pos loc e1 v e2 =
      let pos = string_pos "build_try_exp" pos in
      let info, e1 = build_exp tenv venv info e1 in
      let venv' = venv_add venv v ty_exn in
      let info, e2 = build_exp tenv venv' info e2 in
      let e = make_exp loc (Try (e1, v, e2)) in
         info, e

   and build_raise_exp tenv venv info pos loc a =
      let pos = string_pos "build_raise_exp" pos in
         build_atom tenv pos a (fun a ->
               info, make_exp loc (Raise a))

   (*
    * Debugging info.
    *)
   and build_debug_exp tenv venv info pos loc debug e =
      let pos = string_pos "build_debug_exp" pos in
      let info, e = build_exp tenv venv info e in
         match debug with
            Fc_ast.DebugString s ->
               info, make_exp loc (Debug (DebugString s, e))
          | Fc_ast.DebugContext (line, vars) ->
               let rec collect vars' = function
                  (v1, ty, v2) :: vars ->
                     build_type tenv pos ty (fun ty ->
                           collect ((v1, ty, v2) :: vars') vars)
                | [] ->
                     let e = make_exp loc (Debug (DebugContext (line, List.rev vars'), e)) in
                        info, e
               in
                  collect [] vars

   (*
    * Projection.
    * If the argument is a ref, this is pointer arithmetic.
    *)
   and build_project_exp tenv venv info pos loc v1 ty a2 v3 e =
      let pos = string_pos "build_project_exp" pos in
         build_type tenv pos ty (fun ty ->
         build_atom tenv pos a2 (fun a2 ->
               let ty2 = type_of_atom venv pos a2 in
               let ty2 = dest_pointer_ref_type tenv pos ty2 in
               let field_info = build_field_info tenv pos ty2 in
               let _, _, field_info = fetch_field_info field_info pos (Some v3) in
               let venv = venv_add venv v1 ty in
                  match tenv_expand tenv pos ty with
                     TyRef ty' ->
                        build_let_addr_of_subscript venv a2 field_info pos loc (fun venv a ->
                              let info, e = build_exp tenv venv info e in
                              let e = make_exp loc (LetAtom (v1, ty, a, e)) in
                                 info, e)
                   | _ ->
                        build_let_subscript venv a2 field_info pos loc (fun venv a ->
                              let info, e = build_exp tenv venv info e in
                              let e = make_exp loc (LetAtom (v1, ty, a, e)) in
                                 info, e)))

   (*
    * Subscripting.
    * If the argument is a ref, this is actually pointer arithmetic.
    *)
   and build_subscript_exp tenv venv info pos loc v ty v1 ty2 a2 e =
      let pos = string_pos "build_subscript_exp" pos in

      (* Grab lower index atom as an option *)
      let less =
         match ty with
            Fc_ast.TyConfArray(_, _, _, v1, _, _) ->
               Some (Fc_ast.AtomVar(loc, v1))
          | Fc_ast.TyArray(_, _, _, a1, _) ->
               Some a1
          | _ ->
               None
      in
         (* Convert the arguments *)
         build_type tenv pos ty (fun ty ->
         build_type tenv pos ty2 (fun ty2 ->
         build_atom tenv pos a2 (fun a2 ->
         build_atom_opt tenv pos less (fun less ->
         coerce_atom tenv venv pos loc a2 ty_index (fun venv a2 ->
         coerce_atom_noref tenv venv pos loc (AtomVar v1) ty2 (fun venv a1 ty2 ->

         (* Get the array type *)
         let ty' = dest_pointer_ref_type tenv pos ty2 in

         (* Get the element size *)
         build_sizeof tenv pos loc ty' (fun size ->

         (* New index *)
         let v3 = new_symbol v in
         let venv = venv_add venv v3 ty_int in

         (* Multiply the index by the size *)
         let venv = venv_add venv v ty in
         let info, e = build_exp tenv venv info e in
         let v1 = new_symbol v1 in
         let v2 = new_symbol v in

         let e =
            match tenv_expand tenv pos ty with
               TyRef _ ->
                  (* Refs are just pointer arithmetic *)
                  LetBinop (v, ty, PlusPointerOp, AtomVar v1, AtomVar v2, e)
             | _ ->
                  (* Otherwise a normal subscript *)
                  LetSubscript (v, ty, v1, AtomVar v2, e)
         in
         let e = make_exp loc e in
            match less with
               Some a3 ->
                  (* Subtract start index *)
                  let e = make_exp loc (LetBinop (v2, ty_index, MulIntOp (index_precision, index_signed), AtomVar v3, size, e)) in
                  let e = make_exp loc (LetBinop (v3, ty_int, MinusIntOp (Rawint.Int32, true), a2, a3, e)) in
                     info, e
             | None ->
                  (* Otherwise just do the subscripting as before *)
                  let e = make_exp loc (LetBinop (v2, ty_index, MulIntOp (index_precision, index_signed), a2, size, e)) in
                  let e = make_exp loc (LetAtom (v1, ty2, a1, e)) in
                     info, e)))))))

   (*
    * Set a value.
    * The argument is a ref.
    *)
   and build_set_exp tenv venv info pos loc a1 ty a2 e =
      let pos = string_pos "build_set_exp" pos in
         build_type tenv pos ty (fun ty ->
         build_atom tenv pos a1 (fun a1 ->
         build_atom tenv pos a2 (fun a2 ->
         coerce_atom tenv venv pos loc a1 ty (fun venv a1 ->
         let ty' = dest_pointer_ref_type tenv pos ty in
         coerce_atom tenv venv pos loc a2 ty' (fun venv a2 ->
               let v1 = var_of_atom pos a1 in
               let info, e = build_exp tenv venv info e in
               let e = make_exp loc (SetSubscript (v1, int_zero_normal, ty', a2, e)) in
                  info, e)))))

   (*
    * Get the address of a value.
    *)
   and build_addr_of_exp tenv venv info pos loc v ty a e =
      let pos = string_pos "build_addr_of_exp" pos in
         build_type tenv pos ty (fun ty ->
         build_atom tenv pos a  (fun a ->
         let ty_ref = TyRef (dest_pointer_ref_type tenv pos ty) in
         coerce_atom tenv venv pos loc a ty_ref (fun venv a ->
               let venv = venv_add venv v ty in
               let info, e = build_exp tenv venv info e in
               let e = make_exp loc (LetAtom (v, ty, a, e)) in
                  info, e)))

   (************************************************************************
    * GLOBAL FUNCTIONS
    ************************************************************************)

   (*
    * Build the program.
    *)
   let build_prog file exp =
      let info, exp = build_exp tenv_empty venv_empty info_empty exp in
      let { info_tenv = tenv;
            info_globals = globals;
            info_decls = decls;
            info_strings = strings;
            info_export = export;
            info_import = import
          } = info
      in

      (* Collect types *)
      let tenv =
         List.fold_left (fun tenv (v, ty) ->
               SymbolTable.add tenv v ty) SymbolTable.empty tenv
      in

      (* Collect globals *)
      let globals =
         List.fold_left (fun globals (v, ty, volp, init) ->
               let pos = var_exp_pos v in
                  SymbolTable.add globals v (ty, volp, init)) SymbolTable.empty globals
      in
      let globals =
         List.fold_left (fun globals (v, ty, pre, s) ->
               SymbolTable.add globals v (ty, false, InitString (pre, s))) globals strings
      in
      let decls =
         List.fold_left (fun decls (v, ty) ->
               SymbolTable.add decls v ty) SymbolTable.empty decls
      in

      (* Collect extern *)
      let export =
         List.fold_left (fun export (v, s, ty) ->
               let info =
                  { export_name = s;
                    export_type = ty
                  }
               in
                  SymbolTable.add export v info) SymbolTable.empty export
      in
      let import =
         List.fold_left (fun import (v, s, ty, info) ->
               let info =
                  { import_name = s;
                    import_type = ty;
                    import_info = info
                  }
               in
                  SymbolTable.add import v info) SymbolTable.empty import
      in
         { prog_file = file;
           prog_globals = globals;
           prog_decls = decls;
           prog_export = export;
           prog_import = import;
           prog_types = tenv;
           prog_body = exp
         }
end

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
