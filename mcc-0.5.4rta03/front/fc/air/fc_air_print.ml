(*
 * Utilities for the IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_exp

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Default tabstop.
 *)
let tabstop = Fir_state.tabstop

(*
 * BUG: inherit until we fix this file.
 *)
(* TEMP:  Jason, what the heck were you thinking here?  -JDS
let pp_print_symbol buf = pp_print_symbol Format.std_formatter
 *)

(*
 * Separated list of fields.
 *)
let pp_print_sep_list buf sep printer l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       pp_print_string buf sep;
                       pp_print_space buf ()
                    end;
                 pp_open_hvbox buf tabstop;
                 printer buf x;
                 pp_close_box buf ();
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_pre_sep_list buf sep printer l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    begin
                       pp_print_space buf ();
                       pp_print_string buf sep
                    end;
                 pp_open_hvbox buf tabstop;
                 printer buf x;
                 pp_close_box buf ();
                 false) true l);
   pp_close_box buf ()

(*
 * Prefixed list of items.
 *)
let pp_print_prefix_list buf sep printer l =
   pp_open_hvbox buf 0;
   List.iter (fun x ->
         pp_print_string buf sep;
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         printer buf x;
         pp_close_box buf ()) l;
   pp_close_box buf ()

(*
 * Precisions.
 *)
let pp_print_int_precision buf pre signed =
   let s =
      match pre, signed with
         Rawint.Int8, false -> "unsigned char"
       | Rawint.Int8, true -> "signed char"
       | Rawint.Int16, false -> "unsigned short"
       | Rawint.Int16, true -> "short"
       | Rawint.Int32, false -> "unsigned"
       | Rawint.Int32, true -> "int"
       | Rawint.Int64, false -> "unsigned long"
       | Rawint.Int64, true -> "long"
   in
      pp_print_string buf s

let pp_print_float_precision buf pre =
   let s =
      match pre with
         Rawfloat.Single -> "float32"
       | Rawfloat.Double -> "float64"
       | Rawfloat.LongDouble -> "float80"
   in
      pp_print_string buf s

let pp_print_volatile buf p =
   if p then
      pp_print_string buf "volatile "

(*
 * A basic arithmetic expression.
 *)
let prec_none   = 0

let prec_add    = 1
let prec_mul    = 2

let prec_fun    = 1
let prec_struct = 2
let prec_array  = 3

let rec pp_print_aexp buf pre = function
   AInt i ->
      pp_print_int buf i
 | AVar v ->
      pp_print_symbol buf v
 | APlus (e1, e2) ->
      if pre > prec_add then
         pp_print_string buf "(";
      pp_print_aexp buf prec_add e1;
      pp_print_string buf " + ";
      pp_print_aexp buf prec_add e2;
      if pre > prec_add then
         pp_print_string buf ")"
 | AMinus (e1, e2) ->
      if pre > prec_add then
         pp_print_string buf "(";
      pp_print_aexp buf prec_add e1;
      pp_print_string buf " - ";
      pp_print_aexp buf prec_add e2;
      if pre > prec_add then
         pp_print_string buf ")"
 | ATimes (e1, e2) ->
      if pre > prec_mul then
         pp_print_string buf "(";
      pp_print_aexp buf prec_add e1;
      pp_print_string buf " * ";
      pp_print_aexp buf prec_add e2;
      if pre > prec_mul then
         pp_print_string buf ")"
 | ARoundUp (e, i) ->
      pp_print_string buf "roundup(";
      pp_print_aexp buf prec_none e;
      pp_print_string buf ", ";
      pp_print_int buf i;
      pp_print_string buf ")"
 | AMax (e1, e2) ->
      pp_print_string buf "max(";
      pp_print_aexp buf prec_none e1;
      pp_print_string buf ", ";
      pp_print_aexp buf prec_none e2;
      pp_print_string buf ")"

let pp_print_aexp buf = pp_print_aexp buf prec_none

let rec pp_print_type buf pre ty =
   match ty with
      TyUnit 0 ->
         pp_print_string buf "void"
    | TyUnit 1 ->
         pp_print_string buf "unit"
    | TyUnit 2 ->
         pp_print_string buf "bool"
    | TyUnit i ->
         pp_print_string buf "enum{";
         pp_print_int buf i;
         pp_print_string buf "}"
    | TyZero ->
         pp_print_string buf "$zero"
    | TyInt (pre, signed) ->
         pp_print_int_precision buf pre signed
    | TyField (pre, signed, off, len) ->
         pp_print_int_precision buf pre signed;
         pp_print_string buf "{off:";
         pp_print_int buf off;
         pp_print_string buf "; len:";
         pp_print_int buf len;
         pp_print_string buf "}"
    | TyFloat pre ->
         pp_print_float_precision buf pre
    | TyTag ->
         pp_print_string buf "$tag"
    | TyPointer ty ->
         pp_print_string buf "*";
         pp_print_type buf prec_array ty;
    | TyArray (ty, a1, a2) ->
         pp_print_type buf prec_array ty;
         pp_print_string buf "[";
         pp_print_array_index buf a1;
         pp_print_string buf "..";
         pp_print_array_index buf a2;
         pp_print_string buf "]"
    | TyRef ty ->
         pp_print_string buf "&";
         pp_print_type buf prec_array ty
    | TyStruct fields ->
         if pre >= prec_struct then
            pp_print_string buf "(";
         pp_print_struct buf fields;
         if pre >= prec_struct then
            pp_print_string buf ")"
    | TyEnum fields ->
         if pre >= prec_struct then
            pp_print_string buf "(";
         pp_print_enum buf fields;
         if pre >= prec_struct then
            pp_print_string buf ")"
    | TyUEnum fields ->
         if pre >= prec_struct then
            pp_print_string buf "(";
         pp_print_uenum buf fields;
         if pre >= prec_struct then
            pp_print_string buf ")"
    | TyTuple fields ->
         if pre >= prec_struct then
            pp_print_string buf "(";
         pp_print_tuple buf fields;
         if pre >= prec_struct then
            pp_print_string buf ")"
    | TyElide fields ->
         if pre >= prec_struct then
            pp_print_string buf "(";
         pp_print_elide buf fields;
         if pre >= prec_struct then
            pp_print_string buf ")"
    | TyFun (ty_vars, ty_res) ->
         if pre >= prec_fun then
            pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "(";
         pp_open_hvbox buf 0;
         pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_vars;
         pp_print_string buf ") ->";
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_type buf prec_fun ty_res;
         if pre > prec_fun then
            pp_print_string buf ")";
         pp_close_box buf ()
    | TyVar v ->
         pp_print_string buf "'";
         pp_print_symbol buf v
    | TyLambda (ty_vars, ty) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "(lambda (";
         pp_print_sep_list buf "," (fun buf v ->
               pp_print_string buf "'";
               pp_print_symbol buf v) ty_vars;
         pp_print_string buf ").";
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf ")";
         pp_close_box buf ()
    | TyAll (ty_vars, ty) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "(all (";
         pp_print_sep_list buf "," (fun buf v ->
               pp_print_string buf "'";
               pp_print_symbol buf v) ty_vars;
         pp_print_string buf ").";
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf ")";
         pp_close_box buf ()
    | TyApply (v, ty_list) ->
         pp_open_hvbox buf tabstop;
         pp_print_symbol buf v;
         if ty_list <> [] then
            begin
               pp_print_string buf "[";
               pp_print_sep_list buf "," (fun buf -> pp_print_type buf prec_none) ty_list;
               pp_print_string buf "]"
            end;
         pp_close_box buf ()
    | TyDelayed ->
         pp_print_string buf "delayed"

and pp_print_array_index buf = function
   ArrayInt i ->
      pp_print_int buf i
 | ArrayVar v ->
      pp_print_symbol buf v

and pp_print_struct buf = function
   Fields info ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "struct[";
      pp_print_aexp buf info.struct_size;
      pp_print_string buf "] {";
      SymbolTable.iter (fun v field ->
            let { field_type = ty;
                  field_offset = off;
                  field_bits = bits;
                  field_next = next
                } = field
            in
               pp_print_space buf ();
               pp_print_type buf prec_none ty;
               pp_print_string buf " ";
               pp_print_symbol buf v;
               pp_print_string buf " off=";
               pp_print_aexp buf off;
               (match bits with
                   Some (_, _, off, len) ->
                      pp_print_string buf " : (off=";
                      pp_print_int buf off;
                      pp_print_string buf ", len=";
                      pp_print_int buf len;
                      pp_print_string buf ")"
                 | None ->
                      ());
               pp_print_string buf ";") info.struct_fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | Label (v, label) ->
      pp_print_string buf "<";
      pp_print_symbol buf v;
      pp_print_string buf ",";
      pp_print_symbol buf label;
      pp_print_string buf ">"

and pp_print_enum buf = function
   Fields info ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "enum[";
      pp_print_aexp buf info.enum_size;
      pp_print_string buf "] {";
      SymbolTable.iter (fun v i ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf " = ";
            pp_print_string buf (Int32.to_string i);
            pp_print_string buf ";") info.enum_fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | Label (v, label) ->
      pp_print_string buf "<";
      pp_print_symbol buf v;
      pp_print_string buf ",";
      pp_print_symbol buf label;
      pp_print_string buf ">"

and pp_print_uenum buf = function
   Fields info ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "union_enum[";
      pp_print_aexp buf info.uenum_size;
      pp_print_string buf "] {";
      SymbolTable.iter (fun v (ty_opt, label) ->
            pp_print_space buf ();
            (match ty_opt with
                Some ty ->
                   pp_print_type buf prec_none ty;
                   pp_print_string buf " "
              | None ->
                   ());
            pp_print_symbol buf v;
            pp_print_string buf " : ";
            pp_print_symbol buf label;
            pp_print_string buf ";") info.uenum_fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | Label (v, label) ->
      pp_print_string buf "<";
      pp_print_symbol buf v;
      pp_print_string buf ",";
      pp_print_symbol buf label;
      pp_print_string buf ">"

and pp_print_tuple buf info =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "tuple[";
   pp_print_aexp buf info.tuple_size;
   pp_print_string buf "] {";
   List.iter (fun (ty, e) ->
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf " (off=";
         pp_print_aexp buf e;
         pp_print_string buf ");") info.tuple_fields;
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_elide buf info =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_string buf "elide[";
   pp_print_aexp buf info.tuple_size;
   pp_print_string buf "] {";
   List.iter (fun (ty, e) ->
         pp_print_space buf ();
         pp_print_type buf prec_none ty;
         pp_print_string buf " (off=";
         pp_print_aexp buf e;
         pp_print_string buf ");") info.tuple_fields;
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

(*
 * Atoms.
 *)
and pp_print_atom buf = function
   AtomUnit (n, i) ->
      pp_print_int buf i;
      pp_print_string buf "{";
      pp_print_int buf n;
      pp_print_string buf "}"
 | AtomInt i ->
      pp_print_int_precision buf (Rawint.precision i) (Rawint.signed i);
      pp_print_string buf "(";
      pp_print_string buf (Rawint.to_string i);
      pp_print_string buf ")"
 | AtomFloat x ->
      pp_print_float_precision buf (Rawfloat.precision x);
      pp_print_string buf "(";
      pp_print_string buf (Rawfloat.to_string x);
      pp_print_string buf ")"
 | AtomNil ty ->
      pp_print_string buf "nil[";
      pp_print_type buf prec_none ty;
      pp_print_string buf "]"
 | AtomVar v ->
      pp_print_symbol buf v

let pp_print_type buf = pp_print_type buf prec_none

(*
 * Global flag.
 *)
let string_of_unop = function
   UMinusIntOp _
 | UMinusFloatOp _   -> "-"
 | AbsFloatOp _      -> "abs"
 | SinOp _	     -> "sin"
 | CosOp _	     -> "cos"
 | SqrtOp _ 	     -> "sqrt"
 | NotIntOp _        -> "~"
 | IntOfFloat _
 | IntOfUnit _
 | IntOfPointer _    -> "(int)"
 | FloatOfInt _
 | FloatOfFloat _    -> "(float)"
 | PointerOfInt _    -> "(ptr-of-int)"
 | EqPointerNilOp   -> "==nil"
 | NeqPointerNilOp   -> "!=nil"
 | IntOfInt (dpre, dsigned, spre, ssigned) ->
      let get_precision = function
         Rawint.Int8,  true  -> "int8"
       | Rawint.Int8,  false -> "uint8"
       | Rawint.Int16, true  -> "int16"
       | Rawint.Int16, false -> "uint16"
       | Rawint.Int32, true  -> "int32"
       | Rawint.Int32, false -> "uint32"
       | Rawint.Int64, true  -> "int64"
       | Rawint.Int64, false -> "uint64"
      in
         "(rawint " ^ get_precision (dpre, dsigned) ^ "_of_" ^ get_precision (spre, ssigned) ^ ")"
 | BitFieldOp (_, _, off, len) ->
      sprintf "field{off:%d; len:%d}" off len

let string_of_binop = function
   PlusPointerOp     -> "+ptr"
 | PlusIntOp _
 | PlusFloatOp _     -> "+"
 | MinusPointerOp    -> "-ptr"
 | MinusIntOp _
 | MinusFloatOp _    -> "-"
 | MulIntOp _
 | MulFloatOp _      -> "*"
 | DivIntOp _
 | DivFloatOp _      -> "/"
 | RemIntOp _
 | RemFloatOp _      -> "%"
 | SlIntOp _         -> "<<"
 | SrIntOp _         -> ">>"
 | AndIntOp _        -> "&"
 | OrIntOp _         -> "|"
 | AndBoolOp         -> "&&"
 | OrBoolOp          -> "||"
 | XorIntOp _        -> "^"
 | EqPointerOp       -> "==ptr"
 | EqIntOp _         -> "=="
 | EqBoolOp          -> "==b"
 | EqFloatOp _       -> "==."
 | EqTagOp           -> "==t"
 | NeqPointerOp      -> "!=*"
 | NeqFloatOp _      -> "!=."
 | NeqIntOp _        -> "!="
 | NeqBoolOp         -> "!=b"
 | LtPointerOp	     -> "<ptr"
 | LtIntOp _
 | LtFloatOp _       -> "<"
 | LePointerOp       -> "<=ptr"
 | LeIntOp _
 | LeFloatOp _       -> "<="
 | GtPointerOp       -> ">ptr"
 | GtIntOp _
 | GtFloatOp _       -> ">"
 | GePointerOp       -> ">=ptr"
 | GeFloatOp _
 | GeIntOp _         -> ">="
 | MinIntOp _
 | MinFloatOp _      -> "min"
 | MaxIntOp _
 | MaxFloatOp _      -> "max"
 | Atan2Op _         -> "atan2"
 | SetBitFieldOp (_, _, off, len) ->
      sprintf "setfield{off:%d; len:%d}" off len

(*
 * Print operators.
 *)
let pp_print_unop buf op a =
   fprintf buf "(%s " (string_of_unop op);
   pp_print_atom buf a;
   pp_print_char buf ')'

let pp_print_binop buf op a1 a2 =
   pp_print_char buf '(';
   pp_print_atom buf a1;
   fprintf buf " %s " (string_of_binop op);
   pp_print_atom buf a2;
   pp_print_char buf ')'

(*
 * Global flag.
 *)
let pp_print_gflag buf gflag =
   match gflag with
      VarGlobalClass ->
         pp_print_string buf "$global "
    | VarStaticClass ->
         pp_print_string buf "$static "
    | VarLocalClass ->
         pp_print_string buf "$local "

(*
 * Debug info.
 *)
let pp_print_debug_line buf (file, line) =
   pp_print_string buf "# ";
   pp_print_int buf line;
   pp_print_string buf (" \"" ^ file ^ "\"")

let pp_print_debug_vars buf info =
   pp_print_string buf "# Debug vars: ";
   pp_print_sep_list buf "," (fun buf (v1, ty, v2) ->
         pp_print_symbol buf v1;
         pp_print_string buf "=";
         pp_print_symbol buf v2;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf ty) info

let pp_print_debug buf info =
   match info with
      DebugString s ->
         pp_print_string buf "# ";
         pp_print_string buf s
    | DebugContext (line, vars) ->
         pp_print_location buf line;
         pp_print_space buf ();
         pp_print_debug_vars buf vars

(*
 * "let" opener.
 *)
let pp_print_let_open buf v ty =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_symbol buf v;
   pp_print_string buf " :";
   pp_print_space buf ();
   pp_print_type buf ty;
   pp_print_string buf " =";
   pp_print_space buf ()

let pp_print_let_open_notype buf v =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_symbol buf v;
   pp_print_string buf " =";
   pp_print_space buf ()

let pp_print_let_vars_open buf vars v ty =
   pp_open_hvbox buf tabstop;
   pp_print_string buf "let ";
   pp_print_sep_list buf "," (fun buf v ->
         pp_print_string buf "'";
         pp_print_symbol buf v) vars;
   if vars <> [] then
      pp_print_string buf ", ";
   pp_print_symbol buf v;
   pp_print_string buf " :";
   pp_print_space buf ();
   pp_print_type buf ty;
   pp_print_string buf " =";
   pp_print_space buf ()

let rec pp_print_let_close buf e =
   pp_print_string buf " in";
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_expr buf e

(*
 * Print an expression.
 *)
and pp_print_expr buf e =
   match dest_exp_core e with
      LetFuns (funs, e) ->
         pp_print_string buf "let ";
         pp_print_sep_list buf "and " pp_print_function funs;
         pp_print_string buf " in";
         pp_print_space buf ();
         pp_print_expr buf e

    | LetAtom (v, ty, a, e) ->
         pp_print_let_open buf v ty;
         pp_print_atom buf a;
         pp_print_let_close buf e

    | LetUnop (v, ty, op, a, e) ->
         pp_print_let_open buf v ty;
         pp_print_unop buf op a;
         pp_print_let_close buf e

    | LetBinop (v, ty, op, a1, a2, e) ->
         pp_print_let_open buf v ty;
         pp_print_binop buf op a1 a2;
         pp_print_let_close buf e

    | LetExtCall (v, ty, f, b, f_ty, args, e) ->
         pp_print_let_open buf v ty;
         pp_print_string buf "(";
         if b then
            fprintf buf "\"gc:%s\"" (String.escaped f)
         else
            fprintf buf "\"nogc:%s\"" (String.escaped f);
         pp_print_type buf f_ty;
         pp_print_string buf ")(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf ")";
         pp_print_let_close buf e

    | LetApply (v, ty, f, args, e) ->
         pp_print_let_open buf v ty;
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf ")";
         pp_print_let_close buf e

    | TailCall (f, args) ->
         pp_print_symbol buf f;
         pp_print_string buf "(";
         pp_print_sep_list buf "," pp_print_atom args;
         pp_print_string buf ")"

    | Return (f, a) ->
         pp_print_string buf "return ";
         pp_print_symbol buf f;
         pp_print_string buf "::(";
         pp_print_atom buf a;
         pp_print_string buf ")"

    | LetMalloc (v, a, e) ->
         pp_print_let_open_notype buf v;
         pp_print_string buf "malloc(";
         pp_open_hvbox buf 0;
         pp_print_atom buf a;
         pp_close_box buf ();
         pp_print_string buf ")";
         pp_print_let_close buf e

    | IfThenElse (a, e1, e2) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "if ";
         pp_print_atom buf a;
         pp_print_string buf " then";
         pp_print_space buf ();
         pp_print_expr buf e1;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_string buf "else";
         pp_print_space buf ();
         pp_print_expr buf e2;
         pp_close_box buf ()

    | Try (e1, v, e2) ->
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "try {";
         pp_print_space buf ();
         pp_print_expr buf e1;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "}";
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_string buf "with ";
         pp_print_symbol buf v;
         pp_print_string buf " -> {";
         pp_print_space buf ();
         pp_print_expr buf e2;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "}";
         pp_close_box buf ()

    | Raise a ->
         pp_print_string buf "raise(";
         pp_print_atom buf a;
         pp_print_string buf ")"

    | LetSubscript (v, ty, v2, a3, e) ->
         pp_print_let_open buf v ty;
         pp_print_symbol buf v2;
         pp_print_string buf "[";
         pp_print_atom buf a3;
         pp_print_string buf "]";
         pp_print_let_close buf e

    | SetSubscript (v1, a2, ty, a3, e) ->
         pp_print_symbol buf v1;
         pp_print_string buf "[";
         pp_print_atom buf a2;
         pp_print_string buf "] : ";
         pp_print_type buf ty;
         pp_print_string buf " <- ";
         pp_print_atom buf a3;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_expr buf e

    | Memcpy (v1, a, v2, e) ->
         pp_print_string buf "memcpy(";
         pp_print_symbol buf v1;
         pp_print_string buf ", ";
         pp_print_symbol buf v2;
         pp_print_string buf ", ";
         pp_print_atom buf a;
         pp_print_string buf ");";
         pp_print_space buf ();
         pp_print_expr buf e

    | LetVar (v, ty, a_opt, e) ->
         pp_print_let_open buf v ty;
         (match a_opt with
             Some a ->
                pp_print_string buf "copy(";
                pp_print_atom buf a;
                pp_print_string buf ")"
           | None ->
                pp_print_string buf "uninitialized");
         pp_print_let_close buf e

    | LetAddrOfVar (v1, ty, v2, e) ->
         pp_print_let_open buf v1 ty;
         pp_print_string buf "&";
         pp_print_symbol buf v2;
         pp_print_let_close buf e

    | SetVar (v, ty, a, e) ->
         pp_print_symbol buf v;
         pp_print_string buf " : ";
         pp_print_type buf ty;
         pp_print_string buf " <- ";
         pp_print_atom buf a;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_expr buf e

    | Debug (info, e) ->
         pp_print_debug buf info;
         pp_print_space buf ();
         pp_print_expr buf e

and pp_print_function buf (f, debug, gflag, f_ty, vars, body) =
   pp_print_string buf "(";
   pp_print_gflag buf gflag;
   pp_print_symbol buf f;
   pp_print_string buf " : ";
   pp_print_type buf f_ty;
   pp_print_string buf ")(";
   pp_print_sep_list buf "," pp_print_symbol vars;
   pp_print_string buf ") =";
   pp_print_space buf ();
   pp_print_location buf debug;
   pp_print_space buf ();
   pp_print_expr buf body

(*
 * Wrap in a box.
 *)
let pp_print_expr buf exp =
   pp_open_vbox buf 0;
   pp_print_expr buf exp;
   pp_close_box buf ()

(*
 * Initialization.
 *)
let pp_print_init buf = function
   InitString (pre, s) ->
      Fc_parse_print.pp_print_parse_string buf s
 | InitBlock i ->
      pp_print_string buf "block[";
      pp_print_int buf i;
      pp_print_string buf "]"
 | InitAtom a ->
      pp_print_atom buf a

(*
 * Print a program.
 *)
let pp_print_prog buf prog =
   let { prog_globals = globals;
         prog_types = types;
         prog_export = export;
         prog_import = import;
         prog_decls = decls;
         prog_body = body
       } = prog
   in
      pp_open_vbox buf 0;

      (* Print the types *)
      pp_open_vbox buf tabstop;
      pp_print_string buf "Types:";
      SymbolTable.iter (fun v ty ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_symbol buf v;
            pp_print_string buf " =";
            pp_print_space buf ();
            pp_print_type buf ty;
            pp_close_box buf ()) types;
      pp_close_box buf ();

      (* Print the globals *)
      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Globals:";
      SymbolTable.iter (fun v (ty, volp, init) ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_volatile buf volp;
            pp_print_symbol buf v;
            pp_print_string buf " : ";
            pp_print_type buf ty;
            pp_print_string buf " =";
            pp_print_space buf ();
            pp_print_init buf init;
            pp_close_box buf ()) globals;
      pp_close_box buf ();

      (* Import/export *)
      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Import:";
      SymbolTable.iter (fun v info ->
            let { import_name = s;
                  import_type = ty;
                  import_info = info
                } = info
            in
               pp_print_space buf ();
               pp_print_symbol buf v;
               pp_print_string buf (" = \"" ^ String.escaped s ^ "\"");
               pp_print_space buf ();
               pp_print_string buf ": ";
               pp_print_type buf ty;
               pp_print_space buf ();
               pp_print_string buf ": ";
               Fir_print.pp_print_import_info buf info) import;
      pp_close_box buf ();

      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Export:";
      SymbolTable.iter (fun v { export_name = s; export_type = ty } ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf (" = \"" ^ String.escaped s ^ "\"");
            pp_print_space buf ();
            pp_print_string buf ": ";
            pp_print_type buf ty) export;
      pp_close_box buf ();

      pp_print_space buf ();
      pp_open_vbox buf tabstop;
      pp_print_string buf "Decls:";
      SymbolTable.iter (fun v ty ->
            pp_print_space buf ();
            pp_print_symbol buf v;
            pp_print_string buf " : ";
            pp_print_type buf ty) decls;
      pp_close_box buf ();

      (* Print the body *)
      pp_print_space buf ();
      pp_print_expr buf body;

      (* Terminate the outermost box *)
      pp_close_box buf ()

(*
 * Debug version.
 *)
let debug_prog debug prog =
   let buf = err_formatter in
      pp_set_margin buf 120;
      pp_open_vbox buf 0;
      pp_print_string buf "*** AIR: ";
      pp_print_string buf debug;
      pp_print_space buf ();
      pp_print_prog buf prog;
      pp_close_box buf ();
      pp_print_newline buf ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
