(*
 * Rename all the non-global variables in a program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Symbol

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_exn

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = var SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

let rec env_add_vars env vars vars' =
   match vars, vars' with
      v :: vars, v' :: vars' ->
         env_add_vars (env_add env v v') vars vars'
    | [], [] ->
         env
    | _ ->
         raise (Invalid_argument "Fc_ir_standardize.env_add_vars")

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

(*
 * Check is a variable is defined.
 *)
let env_mem = SymbolTable.mem

(*
 * Type environment has two parts, one for
 * ids, and one for vars.
 *)
type tenv =
   { tenv_types : env;
     tenv_vars : env;
     tenv_fvars : env
   }

(*
 * Type operations.
 *)
let tenv_empty =
   { tenv_types = env_empty;
     tenv_vars = env_empty;
     tenv_fvars = env_empty
   }

let tenv_push_scope tenv =
   { tenv with tenv_vars = env_empty;
               tenv_fvars = env_empty
   }

let tenv_pop_scope tenv tenv' =
   { tenv with tenv_fvars = tenv'.tenv_fvars }

let tenv_add_type tenv v v' =
   { tenv with tenv_types = env_add tenv.tenv_types v v' }

let tenv_add_var tenv v v' =
   { tenv with tenv_vars = env_add tenv.tenv_vars v v' }

let tenv_add_vars tenv vars vars' =
   { tenv with tenv_vars = env_add_vars tenv.tenv_vars vars vars' }

let tenv_lookup_var tenv v =
   try
      let v' = SymbolTable.find tenv.tenv_vars v in
         tenv, v'
   with
      Not_found ->
         try
            let v' = SymbolTable.find tenv.tenv_fvars v in
               tenv, v'
         with
            Not_found ->
               let v' = new_symbol v in
               let tenv = { tenv with tenv_fvars = env_add tenv.tenv_fvars v v' } in
                  tenv, v'

let tenv_lookup_type tenv v =
   env_lookup tenv.tenv_types v

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

(*
 * Standardize array dimensions.
 *)
let rec standardize_aexp venv e =
   match e with
      AInt _ ->
         e
    | AVar v ->
         AVar (env_lookup venv v)
    | APlus (e1, e2) ->
         APlus (standardize_aexp venv e1, standardize_aexp venv e2)
    | AMinus (e1, e2) ->
         AMinus (standardize_aexp venv e1, standardize_aexp venv e2)
    | ATimes (e1, e2) ->
         ATimes (standardize_aexp venv e1, standardize_aexp venv e2)
    | AMax (e1, e2) ->
         AMax (standardize_aexp venv e1, standardize_aexp venv e2)
    | ARoundUp (e, i) ->
         ARoundUp (standardize_aexp venv e, i)

(*
 * Array index.
 *)
let standardize_array_index venv index =
    let index =
   match index with
      ArrayInt _ -> index
    | ArrayVar v ->
	ArrayVar (env_lookup venv v)
    in
	index
(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv venv ty =
   match ty with
      TyUnit _
    | TyTag
    | TyZero
    | TyInt _
    | TyField _
    | TyFloat _
    | TyDelayed
    | TyEnum _
    | TyUEnum (Label _)
    | TyStruct (Label _) ->
         tenv, ty
    | TyPointer ty ->
         let tenv, ty = standardize_type tenv venv ty in
            tenv, TyPointer ty
    | TyRef ty ->
         let tenv, ty = standardize_type tenv venv ty in
            tenv, TyRef ty
    | TyArray (ty, index1, index2) ->
         let tenv, ty = standardize_type tenv venv ty in
         let index1 = standardize_array_index venv index1 in
         let index2 = standardize_array_index venv index2 in
            tenv, TyArray (ty, index1, index2)
    | TyFun (ty_vars, ty_res) ->
         let tenv, ty_vars = standardize_type_list tenv venv ty_vars in
         let tenv, ty_res = standardize_type tenv venv ty_res in
            tenv, TyFun (ty_vars, ty_res)
    | TyVar v ->
         let tenv, v = tenv_lookup_var tenv v in
            tenv, TyVar v
    | TyLambda (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, ty = standardize_type tenv venv ty in
            tenv, TyLambda (vars', ty)
    | TyAll (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, ty = standardize_type tenv venv ty in
            tenv, TyAll (vars', ty)
    | TyApply (v, tyl) ->
         let tenv, tyl = standardize_type_list tenv venv tyl in
             tenv, TyApply (tenv_lookup_type tenv v, tyl)
    | TyTuple { tuple_fields = fields; tuple_size = size } ->
         let tenv, fields =
            List.fold_left (fun (tenv, fields) (ty, e) ->
                  let tenv, ty = standardize_type tenv venv ty in
                  let e = standardize_aexp venv e in
                     tenv, (ty, e) :: fields) (tenv, []) fields
         in
         let size = standardize_aexp venv size in
            tenv, TyTuple { tuple_fields = List.rev fields; tuple_size = size }
    | TyElide { tuple_fields = fields; tuple_size = size } ->
         let tenv, fields =
            List.fold_left (fun (tenv, fields) (ty, e) ->
                  let tenv, ty = standardize_type tenv venv ty in
                  let e = standardize_aexp venv e in
                     tenv, (ty, e) :: fields) (tenv, []) fields
         in
         let size = standardize_aexp venv size in
            tenv, TyElide { tuple_fields = List.rev fields; tuple_size = size }
    | TyUEnum (Fields { uenum_fields = fields; uenum_size = size }) ->
         let tenv, fields =
            SymbolTable.fold_map (fun tenv _ (ty_opt, label) ->
                  let tenv, ty_opt = standardize_type_opt tenv venv ty_opt in
                     tenv, (ty_opt, label)) tenv fields
         in
         let size = standardize_aexp venv size in
            tenv, TyUEnum (Fields { uenum_fields = fields; uenum_size = size })
    | TyStruct (Fields { struct_fields = fields; struct_root = root; struct_size = size }) ->
         let tenv, fields =
            SymbolTable.fold_map (fun tenv _ info ->
                  let tenv, info = standardize_struct_field tenv venv info in
                     tenv, info) tenv fields
         in
         let size = standardize_aexp venv size in
            tenv, TyStruct (Fields { struct_fields = fields; struct_root = root; struct_size = size })

and standardize_struct_field tenv venv info =
   let { field_type = ty; field_offset = off } = info in
   let tenv, ty = standardize_type tenv venv ty in
   let off = standardize_aexp venv off in
      tenv, { info with field_type = ty; field_offset = off }

and standardize_type_opt tenv venv = function
   Some ty ->
      let tenv, ty = standardize_type tenv venv ty in
         tenv, Some ty
 | None ->
      tenv, None

and standardize_type_list tenv venv tyl =
   let tenv, tyl =
      List.fold_left (fun (tenv, ty_vars) ty ->
            let tenv, ty = standardize_type tenv venv ty in
            let ty_vars = ty :: ty_vars in
               tenv, ty_vars) (tenv, []) tyl
   in
      tenv, List.rev tyl

(*
 * Be careful to maintain scoping.
 *)
let standardize_type tenv venv ty =
   let tenv', ty = standardize_type tenv venv ty in
   let tenv = tenv_pop_scope tenv tenv' in
      tenv, ty

let standardize_type_list tenv venv tyl =
   let tenv', tyl = standardize_type_list tenv venv tyl in
   let tenv = tenv_pop_scope tenv tenv' in
      tenv, tyl

(*
 * Rename the vars in an atom.
 *)
let standardize_atom tenv venv atom =
   match atom with
      AtomUnit _
    | AtomInt _
    | AtomFloat _ ->
         tenv, atom
    | AtomNil ty ->
         let tenv, ty = standardize_type tenv venv ty in
            tenv, AtomNil ty
    | AtomVar v ->
         tenv, AtomVar (env_lookup venv v)

let standardize_atom_opt tenv venv = function
   Some a ->
      let tenv, a = standardize_atom tenv venv a in
         tenv, Some a
 | None ->
      tenv, None

let standardize_atoms tenv venv atoms =
   let tenv, atoms =
      List.fold_left (fun (tenv, atoms) a ->
            let tenv, a = standardize_atom tenv venv a in
            let atoms = a :: atoms in
               tenv, atoms) (tenv, []) atoms
   in
      tenv, List.rev atoms

(*
 * Rename all the vars in the expr.
 *)
let rec standardize_expr tenv venv e =
   let loc = loc_of_exp e in
   let tenv, venv, e = standardize_expr_core tenv venv (dest_exp_core e) in
      tenv, venv, make_exp loc e

and standardize_expr_core tenv venv e =
   match e with
      LetFuns (funs, e) ->
         standardize_funs_expr tenv venv funs e
    | LetAtom (v, ty, a, e) ->
	 let tenv', ty = standardize_type tenv venv ty in
            standardize_atom_expr tenv' venv v ty a e
    | LetUnop (v, ty, op, a, e) ->
         standardize_unop_expr tenv venv v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         standardize_binop_expr tenv venv v ty op a1 a2 e
    | LetExtCall (v, ty, s, b, ty', args, e) ->
         standardize_extcall_expr tenv venv v ty s b ty' args e
    | TailCall (f, args) ->
         standardize_tailcall_expr tenv venv f args
    | IfThenElse (a, e1, e2) ->
         standardize_if_expr tenv venv a e1 e2
    | LetVar (v, ty, a_opt, e) ->
	 let tenv', ty = standardize_type tenv venv ty in
            standardize_var_expr tenv' venv v ty a_opt e
    | SetVar (v, ty, a, e) ->
	 let tenv', ty = standardize_type tenv venv ty in
            standardize_set_var_expr tenv' venv v ty a e
    | LetAddrOfVar (v1, ty, v2, e) ->
         standardize_addr_of_var_expr tenv venv v1 ty v2 e
    | SetSubscript (a1, a2, ty, a3, e) ->
         standardize_set_subscript_expr tenv venv a1 a2 ty a3 e
    | LetSubscript (v, ty, a1, a2, e) ->
         standardize_subscript_expr tenv venv v ty a1 a2 e
    | Memcpy (v1, a1, v2, e) ->
         standardize_memcpy_expr tenv venv v1 a1 v2 e
    | LetMalloc (v, a, e) ->
         standardize_malloc_expr tenv venv v a e
    | Debug (info, e) ->
         standardize_debug_expr tenv venv info e
    | Raise a ->
         let tenv, a = standardize_atom tenv venv a in
            tenv, venv, Raise a
    | Return (f, a) ->
         let tenv, a = standardize_atom tenv venv a in
         let f = env_lookup venv f in
            tenv, venv, Return (f, a)
    | Try (e1, v, e2) ->
         standardize_try_expr tenv venv e1 v e2
    | LetApply (v, ty, f, args, e) ->
         standardize_apply_expr tenv venv v ty f args e

(*
 * Standardize a fun.
 * Global funs begin a new type var scope.
 *)
and standardize_fun_expr tenv venv (f, line, gflag, f_ty, vars, body) =
   (* Rename the function *)
   let f' = env_lookup venv f in

   (* Rename the type vars *)
   let tenv' =
      match gflag with
         VarGlobalClass ->
            tenv_push_scope tenv
       | VarStaticClass
       | VarLocalClass ->
            tenv
   in

   (* Rename the vars *)
   let venv', vars' =
      List.fold_left (fun (venv, vars') v ->
            let v' = new_symbol v in
            let venv = env_add venv v v' in
               venv, v' :: vars') (venv, []) vars
   in
   let vars' = List.rev vars' in

   (* Rename the body *)
   let tenv', _, body' = standardize_expr tenv' venv' body in
   let tenv', f_ty = standardize_type tenv' venv f_ty in
      f', line, gflag, f_ty, vars', body'

and standardize_funs tenv venv funs =
   (* Rename the fun names *)
   let venv =
      List.fold_left (fun venv (f, _, _, _, _, _) ->
    	    if env_mem venv f then
		venv
	    else
        	let f' = new_symbol f in
            	    env_add venv f f') venv funs
   in
      (* Rename the function's vars and body *)
      venv, List.map (standardize_fun_expr tenv venv) funs

and standardize_funs_expr tenv venv funs e =
   (* Rename the funs *)
   let venv, funs = standardize_funs tenv venv funs in

   (* Rename the rest *)
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, LetFuns (funs, e)

(*
 * Standardize an operator.
 *)
and standardize_atom_expr tenv venv v ty a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetAtom (v', ty, a, e)

and standardize_unop_expr tenv venv v ty op a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetUnop (v', ty, op, a, e)

and standardize_binop_expr tenv venv v ty op a1 a2 e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, a1 = standardize_atom tenv venv a1 in
   let tenv, a2 = standardize_atom tenv venv a2 in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetBinop (v', ty, op, a1, a2, e)

and standardize_extcall_expr tenv venv v ty s b ty' args e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, args = standardize_atoms tenv venv args in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, ty' = standardize_type tenv venv ty' in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetExtCall (v', ty, s, b, ty', args, e)

and standardize_apply_expr tenv venv v ty f args e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let f = env_lookup venv f in
   let tenv, args = standardize_atoms tenv venv args in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetApply (v', ty, f, args, e)

(*
 * LetVar.
 *)
and standardize_var_expr tenv venv v ty a_opt e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, a_opt = standardize_atom_opt tenv venv a_opt in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetVar (v', ty, a_opt, e)

(*
 * Tail call.
 *)
and standardize_tailcall_expr tenv venv f args =
   let f = env_lookup venv f in
   let _, args = standardize_atoms tenv venv args in
      tenv, venv, TailCall (f, args)

(*
 * Conditional expression.
 *)
and standardize_if_expr tenv venv a e1 e2 =
   let tenv, a = standardize_atom tenv venv a in
   let tenv, _, e1 = standardize_expr tenv venv e1 in
   let tenv, _, e2 = standardize_expr tenv venv e2 in
      tenv, venv, IfThenElse (a, e1, e2)

and standardize_try_expr tenv venv e1 v e2 =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, _, e1 = standardize_expr tenv venv e1 in
   let tenv, _, e2 = standardize_expr tenv venv' e2 in
      tenv, venv, Try (e1, v', e2)

(*
 * Set a variable.
 *)
and standardize_set_var_expr tenv venv v ty a e =
   let v = env_lookup venv v in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, SetVar (v, ty, a, e)

(*
 * Get address of a variable.
 *)
and standardize_addr_of_var_expr tenv venv v1 ty v2 e =
   let v1' = new_symbol v1 in
   let v2 = env_lookup venv v2 in
   let venv' = env_add venv v1 v1' in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetAddrOfVar (v1', ty, v2, e)

(*
 * Array operations.
 *)
and standardize_subscript_expr tenv venv v ty v2 a3 e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let v2 = env_lookup venv v2 in
   let tenv, a3 = standardize_atom tenv venv a3 in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetSubscript (v', ty, v2, a3, e)

and standardize_set_subscript_expr tenv venv v a1 ty a2 e =
   let v = env_lookup venv v in
   let tenv, a1 = standardize_atom tenv venv a1 in
   let tenv, a2 = standardize_atom tenv venv a2 in
   let tenv, ty = standardize_type tenv venv ty in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, SetSubscript (v, a1, ty, a2, e)

(*
 * Memory operations.
 *)
and standardize_malloc_expr tenv venv v a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let tenv, a = standardize_atom tenv venv a in
   let tenv, venv, e = standardize_expr tenv venv' e in
      tenv, venv, LetMalloc (v', a, e)

and standardize_memcpy_expr tenv venv v1 a1 v2 e =
   let v1 = env_lookup venv v1 in
   let v2 = env_lookup venv v2 in
   let tenv, a1 = standardize_atom tenv venv a1 in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, Memcpy (v1, a1, v2, e)

(*
 * Documentation string.
 *)
and standardize_debug_vars tenv venv info =
   List.fold_left (fun (tenv, info) (v1, ty, v2) ->
         let tenv, ty = standardize_type tenv venv ty in
         let v2 = env_lookup venv v2 in
         let info = (v1, ty, v2) :: info in
            tenv, info) (tenv, []) info

and standardize_debug_expr tenv venv info e =
   let tenv, info =
      match info with
         DebugString _ ->
            tenv, info
       | DebugContext (line, vars) ->
            let tenv, vars = standardize_debug_vars tenv venv vars in
               tenv, DebugContext (line, vars)
   in
   let tenv, venv, e = standardize_expr tenv venv e in
      tenv, venv, Debug (info, e)

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Rename all vars in the program.
 *)
let standardize_prog prog =
   let { prog_file = file;
         prog_globals = globals;
         prog_decls = decls;
         prog_export = export;
         prog_import = import;
         prog_types = types;
         prog_body = body
       } = prog
   in

   (* Rename all the types *)
   let tenv =
      SymbolTable.fold (fun tenv v _ ->
	    let new_v = new_symbol v in
            tenv_add_type tenv v new_v) tenv_empty types
   in
   let tenv, types =
      SymbolTable.fold (fun (tenv, types) v ty ->
            let v' = tenv_lookup_type tenv v in
            let tenv, ty = standardize_type tenv env_empty ty in
            let types = SymbolTable.add types v' ty in
               tenv, types) (tenv, SymbolTable.empty) types
   in

   (* Initial environment renames all the top-level vars *)
   let venv =
      SymbolTable.fold (fun venv v _ ->
	    let new_v = new_symbol v in
            env_add venv v new_v) SymbolTable.empty (*globals*) decls
   in
   let venv =
      SymbolTable.fold (fun venv v _ ->
	    let new_v = new_symbol v in
            env_add venv v new_v) venv import
   in

   (* Adjust the body *)
   let _, venv, body = standardize_expr tenv venv body in

   (* Adjust all the globals *)
   let tenv, globals =
      SymbolTable.fold (fun (tenv, globals) v (ty, volp, init) ->
            let v' = env_lookup venv v in
            let tenv, ty = standardize_type tenv env_empty ty in
            let globals = SymbolTable.add globals v' (ty, volp, init) in
               tenv, globals) (tenv, SymbolTable.empty) globals
   in
   let tenv, decls =
      SymbolTable.fold (fun (tenv, decls) v ty ->
            let v' = env_lookup venv v in
            let tenv, ty = standardize_type tenv env_empty ty in
            let decls = SymbolTable.add decls v' ty in
               tenv, decls) (tenv, SymbolTable.empty) decls
   in
   let tenv, import =
      SymbolTable.fold (fun (tenv, import) v info ->
            let v' = env_lookup venv v in
            let tenv, ty = standardize_type tenv env_empty info.import_type in
            let info = { info with import_type = ty } in
            let import = SymbolTable.add import v' info in
               tenv, import) (tenv, SymbolTable.empty) import
   in
   let tenv, export =
      SymbolTable.fold (fun (tenv, export) v info ->
            let v' = env_lookup venv v in
            let tenv, ty = standardize_type tenv env_empty info.export_type in
            let info = { info with export_type = ty } in
            let export = SymbolTable.add export v' info in
               tenv, export) (tenv, SymbolTable.empty) export
   in
      { prog_file = file;
        prog_globals = globals;
        prog_decls = decls;
        prog_export = export;
        prog_import = import;
        prog_types = types;
        prog_body = body
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
