(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_config

open Fc_air
open Fc_air_env
open Fc_air_pos

(*
 * Get the atom type.
 *)
val is_var_atom : atom -> bool
val var_of_atom : pos -> atom -> var
val type_of_atom : venv -> pos -> atom -> ty
val type_of_unop : ty -> unop -> ty * ty
val type_of_binop : ty -> binop -> ty * ty * ty

(*
 * some types have default initializers.
 *)
val default_atom_of_type : tenv -> pos -> ty -> atom

(*
 * Eliminate TyApply.
 *)
val apply_type : tenv -> pos -> var -> ty list -> ty
val tenv_expand : tenv -> pos -> ty -> ty

(*
 * Type analysis.
 *)
val is_scalar_type : tenv -> pos -> ty -> bool
val is_global_type : tenv -> pos -> ty -> bool

(*
 * Classifying aggregate (mainly used during
 * explicit copy introduction).
 *
 *    is_aggregate_type: any aggregate, including arrays and elisions
 *    is_aggregate_copy_type: any aggregate that must be copied
 *       (doesn't include arrays or elisions)
 *    is_aggregate_noncopy_type: any aggregate that is never copied
 *       (doesn't include elisions)
 *    is_aggregate_nonarray_type: any aggregate that is
 *       not an array
 *)
val is_aggregate_type : tenv -> pos -> ty -> bool
val is_aggregate_copy_type : tenv -> pos -> ty -> bool
val is_aggregate_noncopy_type : tenv -> pos -> ty -> bool
val is_aggregate_nonarray_type : tenv -> pos -> ty -> bool

(*
 * Type destructors.
 *)
val is_elide_type : tenv -> pos -> ty -> bool
val is_fun_type : tenv -> pos -> ty -> bool
val dest_fun_type : tenv -> pos -> ty -> var list * ty list * ty
val dest_tuple_type : tenv -> pos -> ty -> ty list
val dest_uenum_tag : tenv -> pos -> var -> ty -> ty option * label
val dest_pointer_ref_type : tenv -> pos -> ty -> ty
val dest_pointer_allrefs_type : tenv -> pos -> ty -> ty
val is_pointer_ref_type : tenv -> pos -> ty -> bool
val is_array_ref_type : tenv -> pos -> ty -> bool

(*
 * Structure access.
 *)
val struct_field_lookup : struct_info -> pos -> var -> field_info

(*
 * Type equality.
 *)
val equal_types : tenv -> pos -> ty -> ty -> bool
val check_types : tenv -> pos -> ty -> ty -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
