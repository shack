(*
 * Type definitions for the FC IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Rawint
open Rawfloat
open Location
open Attribute

open Fc_config

type var = symbol
type label = symbol
type ty_var = symbol

type 'core simple_subst = (loc, 'core) simple_term

(*
 * Function classes:
 *    FunGlobalClass: user-defined function, may be global or static, may escape
 *    FunContClass: continuation, may escape
 *    FunLocalClass: compiler-defined local function, never escapes
 *)
type var_class =
   VarGlobalClass
 | VarStaticClass
 | VarLocalClass

(*
 * Simple arithmetic involving variables.
 *)
type aexp =
   AInt of int
 | AVar of var
 | APlus of aexp * aexp
 | AMinus of aexp * aexp
 | ATimes of aexp * aexp
 | ARoundUp of aexp * int
 | AMax of aexp * aexp

(*
 * These are the initial types used during
 * translation from the AST.  The types contain info
 * about size and offset, as well as structural info.
 * This info gets dropped in translation in IR, and
 * all aggregate blocks get folded into TyRawData.
 *)
type ty =
   TyDelayed
 | TyUnit       of int
 | TyZero
 | TyInt        of int_precision * int_signed
 | TyField      of int_precision * int_signed * int * int
 | TyFloat      of float_precision
 | TyTag
 | TyPointer    of ty
 | TyRef        of ty
 | TyArray      of ty * array_index * array_index
 | TyTuple      of tuple_info
 | TyElide      of tuple_info
 | TyEnum       of enum_info label_option
 | TyUEnum      of uenum_info label_option
 | TyStruct     of struct_info label_option
 | TyFun        of ty list * ty
 | TyVar        of var
 | TyLambda     of var list * ty
 | TyAll        of var list * ty
 | TyApply      of ty_var * ty list

(*
 * Array indexes can be integers or variables.
 *)
and array_index =
   ArrayInt of int
 | ArrayVar of var

(*
 * Tuple info include the fields,
 * and the total size.
 *)
and tuple_info =
   { tuple_fields : (ty * aexp) list;
     tuple_size : aexp
   }

(*
 * Enumeration info keeps track of the tags
 * and the field types.
 *)
and enum_info =
   { enum_fields : int32 SymbolTable.t;
     enum_size : aexp
   }

and uenum_info =
   { uenum_fields : (ty option * label) SymbolTable.t;
     uenum_size : aexp
   }

(*
 * Struct info keeps track of field sizes,
 * offsets, ordering, etc, as well as the
 * total field ordering.
 *)
and struct_info =
   { struct_fields : field_info SymbolTable.t;
     struct_root : var option;
     struct_size : aexp
   }

and field_info =
   { field_type : ty;
     field_index : int;
     field_offset : aexp;
     field_bits : (int_precision * int_signed * int * int) option;
     field_next : var option
   }

(*
 * Values.
 *)
type atom =
   AtomUnit  of int * int
 | AtomInt   of rawint
 | AtomFloat of rawfloat
 | AtomNil   of ty
 | AtomVar   of var

(*
 * Operators.
 *)
and unop =
   (* Native ints *)
   UMinusIntOp of int_precision * int_signed
 | NotIntOp    of int_precision * int_signed
 | BitFieldOp  of int_precision * int_signed * int * int

   (* Floats *)
 | UMinusFloatOp of float_precision
 | AbsFloatOp    of float_precision

   (* Comparisons *)
 | EqPointerNilOp
 | NeqPointerNilOp

   (* Coercions *)
 | IntOfInt of int_precision * int_signed * int_precision * int_signed
 | IntOfFloat of int_precision * int_signed * float_precision
 | IntOfUnit of int_precision * int_signed * int
 | FloatOfInt of float_precision * int_precision * int_signed
 | FloatOfFloat of float_precision * float_precision
 | IntOfPointer of int_precision * int_signed
 | PointerOfInt of int_precision * int_signed

   (* Floating-pointer operators *)
 | SinOp of float_precision
 | CosOp of float_precision
 | SqrtOp of float_precision

and binop =
   AndBoolOp
 | OrBoolOp
 | EqBoolOp
 | NeqBoolOp

 | PlusIntOp   of int_precision * int_signed
 | MinusIntOp  of int_precision * int_signed
 | MulIntOp    of int_precision * int_signed
 | DivIntOp    of int_precision * int_signed
 | RemIntOp    of int_precision * int_signed
 | SlIntOp     of int_precision * int_signed
 | SrIntOp     of int_precision * int_signed
 | AndIntOp    of int_precision * int_signed
 | OrIntOp     of int_precision * int_signed
 | XorIntOp    of int_precision * int_signed

 | MinIntOp    of int_precision * int_signed
 | MaxIntOp    of int_precision * int_signed

 | SetBitFieldOp of int_precision * int_signed * int * int

 | EqIntOp     of int_precision * int_signed
 | NeqIntOp    of int_precision * int_signed
 | LtIntOp     of int_precision * int_signed
 | LeIntOp     of int_precision * int_signed
 | GtIntOp     of int_precision * int_signed
 | GeIntOp     of int_precision * int_signed

 | PlusFloatOp   of float_precision
 | MinusFloatOp  of float_precision
 | MulFloatOp    of float_precision
 | DivFloatOp    of float_precision
 | RemFloatOp    of float_precision

 | MinFloatOp    of float_precision
 | MaxFloatOp    of float_precision

 | EqFloatOp     of float_precision
 | NeqFloatOp    of float_precision
 | LtFloatOp     of float_precision
 | LeFloatOp     of float_precision
 | GtFloatOp     of float_precision
 | GeFloatOp     of float_precision

   (* Tags are only tested for equality *)
 | EqTagOp

   (* Pointer operations *)
 | PlusPointerOp
 | MinusPointerOp

 | Atan2Op of float_precision

 | EqPointerOp
 | NeqPointerOp
 | LtPointerOp
 | LePointerOp
 | GtPointerOp
 | GePointerOp

(*
 * Debugging info.
 *)
and debug_vars = (var * ty  * var) list

and debug_info =
   DebugString of string
 | DebugContext of loc * debug_vars

(*
 * Intermediate code expressions.
 *)
and exp = exp_core simple_subst

and exp_core =
   (* Declarations *)
   LetFuns of fundef list * exp

   (* Arithmetic *)
 | LetAtom of var * ty * atom * exp
 | LetUnop of var * ty * unop * atom * exp
 | LetBinop of var * ty * binop * atom * atom * exp
 | LetExtCall of var * ty * string * bool * ty * atom list * exp
 | LetApply of var * ty * var * atom list * exp
 | TailCall of var * atom list
 | Return of var * atom

   (* Allocation *)
 | LetMalloc of var * atom * exp

   (* Conditionals *)
 | IfThenElse of atom * exp * exp

   (* Exception handling *)
 | Try of exp * var * exp
 | Raise of atom

   (* Subscript operations *)
 | LetSubscript of var * ty * var * atom * exp
 | SetSubscript of var * atom * ty * atom * exp
 | Memcpy of var * atom * var * exp

   (* Variable operations *)
 | SetVar of var * ty * atom * exp
 | LetVar of var * ty * atom option * exp
 | LetAddrOfVar of var * ty * var * exp

   (* Documentation *)
 | Debug of debug_info * exp

(*
 * A function definition has:
 *    1. a name
 *    2. source location
 *    3. flag for function class
 *    4. its type
 *    5. its arguments
 *    6. the body
 *)
and fundef = var * loc * var_class * ty * var list * exp

(*
 * Global initializer.
 *)
type init =
   InitString of int_precision * int array
 | InitAtom of atom
 | InitBlock of int

(*
 * The bool is true if the data is volatile.
 *)
type global = ty * bool * init

type import =
   { import_name : string;
     import_type : ty;
     import_info : Fir.import_info
   }

type export =
   { export_name : string;
     export_type : ty
   }

(*
 * A program has:
 *    1. globals
 *    2. functions
 *    3. an expression
 *)
type prog =
   { prog_file    : string;
     prog_globals : global SymbolTable.t;
     prog_decls   : ty SymbolTable.t;
     prog_import  : import SymbolTable.t;
     prog_export  : export SymbolTable.t;
     prog_types   : ty SymbolTable.t;
     prog_body    : exp
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
