(*
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Rawint
open Rawfloat
open Location

open Sizeof_const
open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_exp
open Fc_air_type

module Pos = MakePos (struct let name = "Fc_air_frame" end)
open Pos

(************************************************************************
 * FRAME
 ************************************************************************)

module type AIRFrameSig =
sig
   type field_info

   (*
    * Default sizes.
    *)
   val char_precision : precision -> int_precision
   val char_signed : int_signed
   val int_precision : precision -> int_precision
   val int_signed : int_signed
   val enum_precision : int_precision
   val enum_signed : int_signed
   val float_precision : precision -> float_precision

   (*
    * Array index sizes.
    *)
   val index_precision : int_precision
   val index_signed : bool
   val ty_index : ty

   (*
    * Some default types.
    *)
   val ty_bool : ty
   val ty_boxed_int : int_precision -> signed -> ty
   val ty_boxed_float : float_precision -> ty
   val ty_tag : ty
   val ty_exn : ty

   (*
    * Constant transations.
    *)
   val build_char : precision -> signed -> int -> rawint
   val build_int : precision -> signed -> int64 -> rawint
   val build_float : precision -> Float80.float80 -> rawfloat

   (*
    * Sizes.
    *)
   val sizeof_tag : int
   val sizeof_int_const : int_precision -> int * int
   val sizeof_float_const : float_precision -> int * int
   val sizeof_const : tenv -> ty -> int

   (*
    * Translate an AST type to an atype.
    *)
   val build_type : tenv -> pos -> Fc_ast.ty -> (ty -> 'a * exp) -> 'a * exp
   val build_sizeof : tenv -> pos -> loc -> ty -> (atom -> 'a * exp) -> 'a * exp
   val build_sizeof_const : tenv -> pos -> ty -> int

   val import_info_of_type : tenv -> pos -> ty -> Fir.import_info

   (*
    * Field computations.
    *)
   val build_field_info : tenv -> pos -> ty -> field_info
   val fetch_field_info : field_info -> pos -> var option -> int * ty * field_info
   val next_field_info : field_info -> pos -> field_info
   val build_let_tag : atom -> pos -> loc -> (atom -> 'a * exp) -> 'a * exp
   val build_let_subscript : venv -> atom -> field_info -> pos -> loc -> (venv -> atom -> 'a * exp) -> 'a * exp
   val build_let_ref_of_subscript : venv -> atom -> field_info -> pos -> loc -> (venv -> atom -> 'a * exp) -> 'a * exp
   val build_let_addr_of_subscript : venv -> atom -> field_info -> pos -> loc -> (venv -> atom -> 'a * exp) -> 'a * exp
end

module MakeAIRFrame (Param : FCParamSig) : AIRFrameSig =
struct
   let exp_pos e = string_pos "Fc_air_frame" (exp_pos e)

   (************************************************************************
    * TYPES
    ************************************************************************)

   (*
    * We need to keep info for accessing the data structure.
    * This info encodes a position into the data structure.
    * We need enough information to find a field from a label
    * (for structures), and to access the next field from the
    * current position.
    *)
   type field_info =
      ArrayInfo of ty * aexp * int
    | TupleInfo of tuple_info * (ty * aexp) list * int
    | StructInfo of struct_info * var option

   (************************************************************************
    * SIZEOF
    ************************************************************************)

   let sizeof_tag = sizeof_native_int
   let sizeof_int_bits = sizeof_native_int * 8
   let sizeof_pointer = 2 * sizeof_native_int

   (* Size based on precision *)
   let sizeof_int_const pre =
      match pre with
         Int8  -> align_int8,  sizeof_int8
       | Int16 -> align_int16, sizeof_int16
       | Int32 -> align_int32, sizeof_int32
       | Int64 -> align_int64, sizeof_int64

   let sizeof_float_const pre =
      match pre with
         Single -> align_single, sizeof_single
       | Double -> align_double, sizeof_double
       | LongDouble -> align_long_double, sizeof_long_double

   let sizeof_int_pre pre =
      let align, size = sizeof_int_const pre in
         align, AInt size

   let sizeof_float_pre pre =
      let align, size = sizeof_float_const pre in
         align, AInt size

   let aexp_of_array_index = function
      ArrayInt i ->
         AInt i
    | ArrayVar v ->
         AVar v

   (*
    * Compute an expression for the size of the type.
    *)
   let rec sizeof tenv pos ty =
      match tenv_expand tenv pos ty with
         TyUnit _ ->
            align_int, AInt sizeof_int
       | TyZero ->
            raise (AirException (pos, StringError "unexpected TyZero"))
       | TyDelayed ->
            raise (AirException (pos, StringError "unexpected TyDelayed"))
       | TyLambda _ ->
            raise (AirException (pos, StringError "unexpected TyLambda"))
       | TyApply _ ->
            raise (AirException (pos, InternalError "unexpected TyApply"))
       | TyInt (pre, _)
       | TyField (pre, _, _, _) ->
            sizeof_int_pre pre
       | TyFloat pre ->
            sizeof_float_pre pre
       | TyAll (_, ty) ->
            sizeof tenv pos ty
       | TyTag ->
            sizeof_int_pre Rawint.Int32
       | TyPointer _
       | TyRef _
       | TyFun _
       | TyVar _ ->
            align_pointer, AInt sizeof_pointer
       | TyArray (ty, index1, index2) ->
	    let index1 = aexp_of_array_index index1 in
	    let index2 = aexp_of_array_index index2 in
               align_aggr, aexp_times (aexp_minus index2 index1) (snd (sizeof tenv pos ty))
       | TyTuple { tuple_size = size }
       | TyElide { tuple_size = size }
       | TyEnum (Fields { enum_size = size })
       | TyUEnum (Fields { uenum_size = size })
       | TyStruct (Fields { struct_size = size }) ->
            align_aggr, size
       | TyEnum (Label (_, label))
       | TyUEnum (Label (_, label))
       | TyStruct (Label (_, label)) ->
            raise (AirException (pos, StringVarError ("incomplete type", label)))

   (*
    * Constant-sized blocks.
    *)
   let sizeof_const_pos = var_exp_pos (Symbol.add "sizeof_const")

   let sizeof_const tenv ty =
      let pos = sizeof_const_pos in
         match sizeof tenv pos ty with
            _, AInt i -> i
          | _ -> raise (AirException (pos, StringTypeError ("type is not constant", ty)))

   (************************************************************************
    * TYPE UTILITIES
    ************************************************************************)

   (*
    * Normal precisions.
    *)
   let char_precision = Param.char_precision
   let char_signed = false
   let int_precision = Param.int_precision
   let int_signed = true
   let float_precision = Param.float_precision
   let enum_precision = Rawint.Int32
   let enum_signed = true

   (*
    * Array indexes.
    *)
   let index_precision = int_precision NormalPrecision
   let index_signed = true
   let ty_index = TyInt (index_precision, index_signed)

   (*
    * Translate values from constants.
    *)
   let build_char pre signed i =
      let pre = Param.char_precision pre in
         Rawint.of_int pre signed i

   let build_int pre signed i =
      let pre = Param.int_precision pre in
         Rawint.of_int64 pre signed i

   let build_float pre x =
      let pre = Param.float_precision pre in
         Rawfloat.of_float80 pre x

   (*
    * Boxed numbers.
    *)
   let ty_boxed_int pre signed =
      let _, size = sizeof_int_pre pre in
         TyTuple { tuple_fields = [TyInt (pre, signed), AInt 0]; tuple_size = size }

   let ty_boxed_float pre =
      let _, size = sizeof_float_pre pre in
         TyTuple { tuple_fields = [TyFloat pre, AInt 0]; tuple_size = size }

   (*
    * Translate types from parameters.
    *)
   let pre_int = FCParam.int_precision NormalPrecision

   let ty_bool = TyUnit 2
   let ty_int_norm = TyInt (pre_int, true)
   let ty_tag = TyTag

   let ty_exn =
      TyApply (Symbol.add "exn", [])

   let build_norm_int i =
      Rawint.of_int Int32 true i

   let var_of_ast_atom pos = function
      Fc_ast.AtomInt _
    | Fc_ast.AtomUnit _
    | Fc_ast.AtomFloat _
    | Fc_ast.AtomNil _ ->
         raise (Invalid_argument "Fc_air_frame: var_of_ast_atom: not an AtomVar")
    | Fc_ast.AtomVar(_, v) ->
         v
    | Fc_ast.AtomEnum _ ->
         raise (AirException (pos, StringError "array bound"))

   let air_array_bound = function
      Fc_ast.AtomUnit(_, n, i) ->
         ArrayInt i
    | Fc_ast.AtomInt(_, i) ->
         ArrayInt (Rawint.to_int i)
    | Fc_ast.AtomFloat(_, x) ->
         ArrayInt (Rawfloat.to_int x)
    | Fc_ast.AtomVar(_, v) ->
         ArrayVar v
    | Fc_ast.AtomEnum(_, _, i) ->
         ArrayInt (Int32.to_int i)
    | _ ->
         raise (Invalid_argument "air_atom_of_array_bound")

   (************************************************************************
    * TYPE TRANSLATION
    ************************************************************************)

   (*
    * Type translation drops the
    * type status.
    *)
   let rec build_type tenv pos ty cont =
      match ty with
         Fc_ast.TyUnit (_, _, n) ->
            cont (TyUnit n)
       | Fc_ast.TyDelayed _ ->
            cont TyDelayed
       | Fc_ast.TyZero _ ->
            cont TyZero
       | Fc_ast.TyInt (_, _, pre, signed) ->
            cont (TyInt (pre, signed))
       | Fc_ast.TyField (_, _, pre, signed, off, len) ->
            cont (TyField (pre, signed, off, len))
       | Fc_ast.TyFloat (_, _, pre) ->
            cont (TyFloat pre)
       | Fc_ast.TyArray (_, _, ty, a1, a2) ->
            build_type tenv pos ty (fun ty ->
                  cont (TyArray(ty, air_array_bound a1, air_array_bound a2)))
       | Fc_ast.TyConfArray _ ->
	    raise (Invalid_argument "Fc_air_frame: build_type: TyConfArray found")
       | Fc_ast.TyPointer (_, _, ty) ->
            build_type tenv pos ty (fun ty ->
                  cont (TyPointer ty))
       | Fc_ast.TyRef (_, _, ty) ->
            build_type tenv pos ty (fun ty ->
                  cont (TyRef ty))
       | Fc_ast.TyTuple (_, _, tyl) ->
            build_tuple tenv pos tyl cont
       | Fc_ast.TyElide (_, _, tyl) ->
            build_elide tenv pos tyl cont
       | Fc_ast.TyFun (_, _, ty_args, ty_res) ->
            build_type_list tenv pos ty_args (fun ty_args ->
                  build_type tenv pos ty_res (fun ty_res ->
                        cont (TyFun (ty_args, ty_res))))
       | Fc_ast.TyVar (_, _, v) ->
            cont (TyVar v)
       | Fc_ast.TyLambda (_, _, vars, ty) ->
            build_type tenv pos ty (fun ty ->
                  cont (TyLambda (vars, ty)))
       | Fc_ast.TyAll (_, _, vars, ty) ->
            build_type tenv pos ty (fun ty ->
                  cont (TyAll (vars, ty)))
       | Fc_ast.TyApply (_, _, v, tyl) ->
            build_type_list tenv pos tyl (fun tyl ->
                  cont (TyApply (v, tyl)))
       | Fc_ast.TyEnum _ ->
    	    cont (TyInt (enum_precision, enum_signed))
(*            build_enum tenv pos fields cont *)
       | Fc_ast.TyUEnum (_, _, Fields (_, fields)) ->
            build_uenum tenv pos fields cont
       | Fc_ast.TyUnion (_, _, Fields fields) ->
            build_union tenv pos fields cont
       | Fc_ast.TyStruct (_, _, Fields fields) ->
            build_struct tenv pos fields cont
       | Fc_ast.TyUEnum (_, _, Label (v, label)) ->
            cont (TyUEnum (Label (v, label)))
       | Fc_ast.TyUnion (_, _, Label (v, label)) ->
            cont (TyStruct (Label (v, label)))
       | Fc_ast.TyStruct (_, _, Label (v, label)) ->
            cont (TyStruct (Label (v, label)))

   (*
    * Translate a list of types.
    *)
   and build_type_list tenv pos tyl cont =
      let rec collect tyl' = function
         ty :: tyl ->
            build_type tenv pos ty (fun ty ->
                  collect (ty :: tyl') tyl)
       | [] ->
            cont (List.rev tyl')
      in
         collect [] tyl

   (*
    * Build a tuple description.
    *)
   and build_tuple tenv pos tyl cont =
      let rec collect fields size = function
         ty :: tyl ->
            build_type tenv pos ty (fun ty ->
                  let align, size' = sizeof tenv pos ty in
                  let size' = aexp_plus (aexp_roundup size align) size' in
                     collect ((ty, size) :: fields) size' tyl)
       | [] ->
            let info =
               { tuple_fields = List.rev fields;
                 tuple_size = size
               }
            in
               cont (TyTuple info)
      in
         collect [] (AInt 0) tyl

   (*
    * Elisions eliminate array args.
    *)
   and build_elide tenv pos tyl cont =
      let rec collect fields size = function
         ty :: tyl ->
            build_type tenv pos ty (fun ty ->
                  let ty =
                     match ty with
                        TyArray (ty, _, _) ->
                           TyPointer ty
                      | _ ->
                           ty
                  in
                  let align, size' = sizeof tenv pos ty in
                  let size' = aexp_plus (aexp_roundup size align) size' in
                     collect ((ty, size) :: fields) size' tyl)
       | [] ->
            let info =
               { tuple_fields = List.rev fields;
                 tuple_size = size
               }
            in
               cont (TyElide info)
      in
         collect [] (AInt 0) tyl

   (*
    * Build a struct description.
    *)
   and build_struct tenv pos fields cont =
      (* They have to be linked, so add the link *)
      let fields, root =
         List.fold_right (fun (_, v, ty, i_opt) (fields, next) ->
               (v, ty, i_opt, next) :: fields, Some v) fields ([], None)
      in

      (* Now calculate sizes and offsets *)
      let rec collect fields' size off index = function
         (v, ty, i_opt, next) :: fields ->
            build_type tenv pos ty (fun ty ->
                  let info, size, bitoffset =
                     match tenv_expand tenv pos ty, i_opt with
                        TyInt (pre, signed), Some i
                      | TyField (pre, signed, _, _), Some i ->
                           (* This is a bit field, so calculate the offset *)
                           let size', off =
                              if off + i > sizeof_int_bits then
                                 let size = aexp_plus size (AInt sizeof_int) in
                                    size, 0
                              else
                                 size, off
                           in
                           let info =
                              { field_type = ty;
                                field_index = index;
                                field_offset = size;
                                field_bits = Some (pre, signed, off, i);
                                field_next = next
                              }
                           in
                              info, size', off + i

                      | _ ->
                           (* This is a normal field *)
                           let align, size' = sizeof tenv pos ty in
                           let size = aexp_roundup size align in
                           let info =
                              { field_type = ty;
                                field_index = index;
                                field_offset = size;
                                field_bits = None;
                                field_next = next
                              }
                           in
                           let size = aexp_plus size size' in
                              info, size, 0
                  in
                  let fields' = SymbolTable.add fields' v info in
                     collect fields' size bitoffset (index + 1) fields)
       | [] ->
            let size = aexp_roundup (aexp_plus size (AInt ((off + 7) / 8))) align_aggr in
            let info =
               { struct_fields = fields';
                 struct_root = root;
                 struct_size = size
               }
            in
               cont (TyStruct (Fields info))
      in
         collect SymbolTable.empty (AInt 0) 0 0 fields

   (*
    * Build a union description.
    * I know this is a needless duplication of code,
    * but we have to do slightly different things.
    *)
   and build_union tenv pos fields cont =
      (* They have to be linked, so add the link *)
      let fields, root =
         List.fold_right (fun (_, v, ty, i_opt) (fields, next) ->
               (v, ty, i_opt, next) :: fields, Some v) fields ([], None)
      in

      (* Now calculate sizes and offsets *)
      let rec collect fields' maxsize index = function
         (v, ty, i_opt, next) :: fields ->
            build_type tenv pos ty (fun ty ->
                  let info, size =
                     match tenv_expand tenv pos ty, i_opt with
                        TyInt (pre, signed), Some i ->
                           (* This is a bit field, so calculate the offset *)
                           let info =
                              { field_type = ty;
                                field_index = index;
                                field_offset = AInt 0;
                                field_bits = Some (pre, signed, 0, i);
                                field_next = next
                              }
                           in
                              info, AInt sizeof_int

                      | _ ->
                           (* This is a normal field *)
                           let align, size = sizeof tenv pos ty in
                           let info =
                              { field_type = ty;
                                field_index = index;
                                field_offset = AInt 0;
                                field_bits = None;
                                field_next = next
                              }
                           in
                              info, size
                  in
                  let fields' = SymbolTable.add fields' v info in
                  let maxsize = aexp_max maxsize size in
                     collect fields' maxsize (index + 1) fields)
       | [] ->
            let info =
               { struct_fields = fields';
                 struct_root = root;
                 struct_size = maxsize
               }
            in
               cont (TyStruct (Fields info))
      in
         collect SymbolTable.empty (AInt 0) 0 fields

   (*
    * Build an enum description.
    *)
   and build_enum tenv pos fields cont =
      (* Calculate sizes and tags *)
      let rec collect fields' maxsize = function
         (_, v, i) :: fields ->
            let fields' = SymbolTable.add fields' v i in
               collect fields' maxsize fields

       | [] ->
            let info =
               { enum_fields = fields';
                 enum_size = maxsize
               }
            in
               cont (TyEnum (Fields info))
      in
         collect SymbolTable.empty (AInt sizeof_int) fields

   (*
    * Build an enum description.
    * This is a lot like a union, but each element has a tag.
    *)
   and build_uenum tenv pos fields cont =
      (* Calculate sizes and tags *)
      let rec collect fields' maxsize = function
         (_, v, Some ty, label) :: fields ->
            build_type tenv pos ty (fun ty ->
                  let align, size = sizeof tenv pos ty in
                  let size = aexp_plus (AInt sizeof_int) size in
                  let info = Some ty, label in
                  let fields' = SymbolTable.add fields' v info in
                  let maxsize = aexp_max maxsize size in
                     collect fields' maxsize fields)
       | (_, v, None, label) :: fields ->
            let info = None, label in
            let fields' = SymbolTable.add fields' v info in
               collect fields' maxsize fields

       | [] ->
            let info =
               { uenum_fields = fields';
                 uenum_size = maxsize
               }
            in
               cont (TyUEnum (Fields info))
      in
         collect SymbolTable.empty (AInt sizeof_int) fields

   (************************************************************************
    * IMPORT INFO
    ************************************************************************)

   (*
    * Convert the type to import info.
    *)
   let native_int_precision = int_precision NormalPrecision

   let import_arg_of_type tenv pos ty =
      let pos = string_pos "import_arg_of_type" pos in
         match tenv_expand tenv pos ty with
            TyUnit _
          | TyEnum _ ->
               Fir.ArgRawInt (native_int_precision, false)
          | TyInt (pre, signed)
          | TyField (pre, signed, _, _) ->
               Fir.ArgRawInt (pre, signed)
          | TyFloat pre ->
               Fir.ArgRawFloat pre
          | TyTag
          | TyPointer _
          | TyRef _
          | TyArray _
          | TyTuple _
          | TyElide _
          | TyUEnum _
          | TyStruct _
          | TyVar _ ->
               Fir.ArgPointer
          | TyLambda _
          | TyAll _
          | TyFun _ ->
               Fir.ArgFunction
          | TyApply _
          | TyDelayed
          | TyZero ->
               raise (AirException (pos, StringTypeError ("unexpected type", ty)))

   let import_info_of_type tenv pos ty =
      let pos = string_pos "import_info_of_type" pos in
         if is_fun_type tenv pos ty then
            match dest_fun_type tenv pos ty with
               _, [arg], _ when is_elide_type tenv pos arg ->
                  Fir.ImportFun (true, [import_arg_of_type tenv pos arg])
             | _, args, _ ->
                  Fir.ImportFun (false, List.map (import_arg_of_type tenv pos) args)
         else
            Fir.ImportGlobal

   (************************************************************************
    * ACCESSING
    ************************************************************************)

   (*
    * Get the field info for the type.
    *)
   let rec build_field_info tenv pos ty =
      let pos = string_pos "build_field_info" pos in
         match tenv_expand tenv pos ty with
            TyArray (ty, _, _) ->
               let _, size = sizeof tenv pos ty in
                  ArrayInfo (ty, size, 0)
          | TyTuple info ->
               TupleInfo (info, info.tuple_fields, 0)
          | TyElide info ->
               TupleInfo (info, info.tuple_fields, 0)
          | TyStruct (Fields info) ->
               StructInfo (info, info.struct_root)
          | TyRef ty ->
               build_field_info tenv pos ty
          | ty ->
               raise (AirException (pos, StringTypeError ("type is not an aggregate", ty)))

   (*
    * Given the info, and an optional label,
    * get the info for the field.
    *)
   let fetch_field_info info pos label =
      match info with
         ArrayInfo (ty, size, index) ->
            index, ty, info
       | StructInfo (info, name) ->
            (* Move the position *)
            let name =
               match label, name with
                  Some name, _
                | _, Some name ->
                     name
                | None, None ->
                     raise (AirException (pos, StringError "too many struct fields"))
            in

            (* Fetch the field info *)
            let field = struct_field_lookup info pos name in
            let { field_index = index; field_type = ty } = field in
               index, ty, StructInfo (info, Some name)
       | TupleInfo (_, (ty, _) :: _, index) ->
            index, ty, info
       | TupleInfo (_, [], _) ->
            raise (AirException (pos, StringError "too many tuple fields"))

   (*
    * Given the info, and an optional label,
    * calculate the info for the next field.
    *)
   let next_field_info info pos =
      match info with
         ArrayInfo (ty, size, index) ->
            ArrayInfo (ty, size, index + 1)
       | StructInfo (info, name) ->
            let name =
               match name with
                  Some name ->
                     (struct_field_lookup info pos name).field_next
                | None ->
                     None
            in
               StructInfo (info, name)
       | TupleInfo (info, _ :: fields, index) ->
            TupleInfo (info, fields, index + 1)
       | TupleInfo (info, [], index) ->
            TupleInfo (info, [], index + 1)

   (*
    * Build code the the expression.
    *)
   let int_pre = int_precision NormalPrecision
   let plus_op = PlusIntOp (int_pre, int_signed)
   let minus_op = MinusIntOp (int_pre, int_signed)
   let times_op = MulIntOp (int_pre, int_signed)
   let and_op = AndIntOp (int_pre, int_signed)
   let max_op = MaxIntOp (int_pre, int_signed)
   let sl_op = SlIntOp (int_pre, int_signed)
   let sr_op = SrIntOp (int_pre, int_signed)

   let build_int_atom i =
      AtomInt (Rawint.of_int int_pre int_signed i)

   let rec build_aexp e loc cont =
      match e with
         AInt i ->
            cont (build_int_atom i)
       | AVar v ->
            cont (AtomVar v)
       | APlus (e1, e2) ->
            build_aexp e1 loc (fun a1 ->
                  build_aexp e2 loc (fun a2 ->
                        let v = new_symbol_string "aexp_sum" in
                        let info, e = cont (AtomVar v) in
                        let e = make_exp loc (LetBinop (v, ty_int_norm, plus_op, a1, a2, e)) in
                           info, e))
       | AMinus (e1, e2) ->
            build_aexp e1 loc (fun a1 ->
                  build_aexp e2 loc (fun a2 ->
                        let v = new_symbol_string "aexp_minus" in
                        let info, e = cont (AtomVar v) in
                        let e = make_exp loc (LetBinop (v, ty_int_norm, minus_op, a1, a2, e)) in
                           info, e))
       | ATimes (e1, e2) ->
            build_aexp e1 loc (fun a1 ->
                  build_aexp e2 loc (fun a2 ->
                        let v = new_symbol_string "aexp_mul" in
                        let info, e = cont (AtomVar v) in
                        let e = make_exp loc (LetBinop (v, ty_int_norm, times_op, a1, a2, e)) in
                           info, e))
       | ARoundUp (e, i) ->
            build_aexp e loc (fun a ->
                  let v1 = new_symbol_string "aexp_roundup" in
                  let v2 = new_symbol v1 in
                  let off = build_int_atom (i - 1) in
                  let mask = build_int_atom (lnot i) in
                  let info, e = cont (AtomVar v2) in
                  let e = make_exp loc (LetBinop (v2, ty_int_norm, and_op, a, mask, e)) in
                  let e = make_exp loc (LetBinop (v1, ty_int_norm, plus_op, a, off, e)) in
                     info, e)

       | AMax (e1, e2) ->
            build_aexp e1 loc (fun a1 ->
                  build_aexp e2 loc (fun a2 ->
                        let v = new_symbol_string "aexp_max" in
                        let info, e = cont (AtomVar v) in
                        let e = make_exp loc (LetBinop (v, ty_int_norm, max_op, a1, a2, e)) in
                           info, e))

   (*
    * Get the tag of a block.
    *)
   let build_let_tag a pos loc cont =
      let pos = string_pos "build_let_tag" pos in
      let v = var_of_atom pos a in
      let v' = new_symbol v in
      let info, e = cont (AtomVar v') in
      let e = make_exp loc (LetSubscript (v', ty_tag, v, build_int_atom 0, e)) in
         info, e

   (*
    * Build the code for loading a value from the field.
    *)
   let build_let_subscript venv a info pos loc cont =
      let v = var_of_atom pos a in
      let v' = new_symbol v in
         match info with
            ArrayInfo (ty, size, off) ->
               let e = aexp_times size (AInt off) in
                  build_aexp e loc (fun a ->
                        let venv = venv_add venv v' ty in
                        let info, e = cont venv (AtomVar v') in
                        let e = make_exp loc (LetSubscript (v', ty, v, a, e)) in
                           info, e)
          | TupleInfo (info, (ty, e) :: _, _) ->
               build_aexp e loc (fun a ->
                     let venv = venv_add venv v' ty in
                     let info, e = cont venv (AtomVar v') in
                     let e = make_exp loc (LetSubscript (v', ty, v, a, e)) in
                        info, e)
          | StructInfo (info, Some label) ->
               let field = struct_field_lookup info pos label in
               let { field_type = ty;
                     field_offset = e;
                     field_bits = bits
                   } = field
               in
                  build_aexp e loc (fun a ->
                        let ty =
                           match bits with
                              Some (pre, signed, off, len) ->
                                 TyField (pre, signed, off, len)
                            | None ->
                                 ty
                        in
                        let venv = venv_add venv v' ty in
                        let info, e = cont venv (AtomVar v') in
                        let e = make_exp loc (LetSubscript (v', ty, v, a, e)) in
                           info, e)
          | TupleInfo (_, [], _)
          | StructInfo (_, None) ->
               raise (AirException (pos, StringError "illegal field specification"))

   (*
    * Build the code for getting the address of a field.
    *)
   let build_let_addr_of_subscript_aux make_type venv a info pos loc cont =
      let v' = new_symbol_string "addr_of" in
         match info with
            ArrayInfo (ty, size, off) ->
               let e = aexp_times size (AInt off) in
               let ty = make_type ty in
                  build_aexp e loc (fun a' ->
                        let venv = venv_add venv v' ty in
                        let info, e = cont venv (AtomVar v') in
                        let e = make_exp loc (LetBinop (v', ty, PlusPointerOp, a, a', e)) in
                           info, e)
          | TupleInfo (info, (ty, e) :: _, _) ->
               build_aexp e loc (fun a' ->
                     let ty = make_type ty in
                     let venv = venv_add venv v' ty in
                     let info, e = cont venv (AtomVar v') in
                     let e = make_exp loc (LetBinop (v', ty, PlusPointerOp, a, a', e)) in
                        info, e)
          | StructInfo (info, Some label) ->
               let field = struct_field_lookup info pos label in
               let { field_type = ty;
                     field_offset = e;
                     field_bits = bits
                   } = field
               in
                  build_aexp e loc (fun a' ->
                        let ty =
                           match bits with
                              Some (pre, signed, off, len) ->
                                 TyField (pre, signed, off, len)
                            | None ->
                                 ty
                        in
                        let ty = make_type ty in
                        let venv = venv_add venv v' ty in
                        let info, e = cont venv (AtomVar v') in
                        let e = make_exp loc (LetBinop (v', ty, PlusPointerOp, a, a', e)) in
                           info, e)
          | TupleInfo (_, [], _)
          | StructInfo (_, None) ->
               raise (AirException (pos, StringError "illegal field specification"))

   let build_let_addr_of_subscript venv a info pos loc cont =
      build_let_addr_of_subscript_aux (fun ty -> TyPointer ty) venv a info pos loc cont

   let build_let_ref_of_subscript venv a info pos loc cont =
      build_let_addr_of_subscript_aux (fun ty -> TyRef ty) venv a info pos loc cont

   (*
    * Build the expression for sizeof.
    *)
   let build_sizeof tenv pos loc ty cont =
      let pos = string_pos "build_sizeof" pos in
      let _, exp = sizeof tenv pos ty in
         build_aexp exp loc cont

   let build_sizeof_const tenv pos ty =
      let pos = string_pos "build_sizeof" pos in
      let _, aexp = sizeof tenv pos ty in
         match aexp with
            AInt i -> i
          | _ -> raise (AirException (pos, StringError "non-constant size for global"))
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
