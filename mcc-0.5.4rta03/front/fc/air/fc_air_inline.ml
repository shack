(*
 * Basic inline expansion.
 * We inline functions only if they
 * are called exactly once.  We do some simple
 * intra-procedural constant folding.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Trace
open Symbol
open Location

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_type
open Fc_air_print
open Fc_air_standardize
open Fc_air_exp

module Pos = MakePos (struct let name = "Fc_air_inline" end)
open Pos

let tabstop = 3

(************************************************************************
 * AVAILABLE EXPRESSIONS
 ************************************************************************)

type ('a, 'b) option2 =
   Some2 of 'a
 | Some1 of 'b
 | None0

(*
 * Simple expressions that a variable can refer to.
 *)
type avail =
   AvailVar of var * ty
 | AvailAtom of atom
 | AvailAddressOf of var * var
 | AvailSubscript of var * var * atom
 | AvailFunction of var * var_class * var list * exp

type aenv = avail SymbolTable.t

let aenv_empty = SymbolTable.empty

let aenv_mem = SymbolTable.mem

let rec aenv_lookup aenv v =
   try
      match SymbolTable.find aenv v with
         AvailAtom (AtomVar v)
       | AvailVar (v, _) ->
            aenv_lookup aenv v
       | a ->
            a
   with
      Not_found ->
         AvailAtom (AtomVar v)

let aenv_lookup_atom aenv = function
   AtomVar v ->
      aenv_lookup aenv v
 | a ->
      AvailAtom a

let aenv_add aenv v a =
   SymbolTable.add aenv v a

let atom_of_avail = function
   AvailVar (v, _)
 | AvailAddressOf (v, _)
 | AvailSubscript (v, _, _)
 | AvailFunction (v, _, _, _) ->
      AtomVar v
 | AvailAtom a ->
      a

let rec type_of_var aenv pos v =
   let pos = string_pos "type_of_var" pos in
      try
         match SymbolTable.find aenv v with
            AvailAtom (AtomVar v) ->
               type_of_var aenv pos v
          | AvailVar (_, ty) ->
               ty
          | _ ->
               raise Not_found
      with
         Not_found ->
            raise (AirException (pos, StringVarError ("unknown type for variable", v)))

let aenv_bind_vars aenv pos vars args =
   let pos = string_pos "aenv_bind_vars" pos in
   let rec collect aenv vars' args' =
      match vars', args' with
         v :: vars', a :: args' ->
            collect (aenv_add aenv v (AvailAtom a)) vars' args'
       | [], [] ->
            aenv
       | _ ->
            raise (AirException (pos, ArityMismatch (List.length vars, List.length args)))
   in
      collect aenv vars args

(************************************************************************
 * FUNCTION SIZING
 ************************************************************************)

(*
 * Measure the "size" of a function.
 *)
let is_small_max = 25

let rec size_exp fenv i e =
   if i >= is_small_max then
      i
   else
      match dest_exp_core e with
         LetFuns (funs, e) ->
            let i =
               List.fold_left (fun i (f, _, _, _, _, e) ->
                     let _, degree, _ = SymbolTable.find fenv f in
                        if degree > 1 then
                           is_small_max
                        else
                           size_exp fenv i e) i funs
            in
               size_exp fenv i e
       | LetAtom (_, _, _, e)
       | LetUnop (_, _, _, _, e)
       | LetBinop (_, _, _, _, _, e)
       | LetVar (_, _, _, e) ->
            size_exp fenv (succ i) e
       | LetMalloc (_, _, e) ->
            size_exp fenv (i + 1) e
       | TailCall _ ->
            succ i
       | IfThenElse (_, e1, e2) ->
            size_exp fenv (size_exp fenv (i + 2) e1) e2
       | LetExtCall (_, _, _, _, _, _, e) ->
            size_exp fenv (succ i) e
       | LetSubscript (_, _, _, _, e)
       | SetSubscript (_, _, _, _, e) ->
            size_exp fenv (i + 1) e
       | Memcpy (_, _, _, e) ->
            size_exp fenv (i + 5) e
       | LetAddrOfVar (_, _, _, e)
       | SetVar (_, _, _, e) ->
            size_exp fenv (i + 1) e
       | Debug (_, e) ->
            size_exp fenv i e
       | Try (e1, _, e2) ->
            size_exp fenv (size_exp fenv (i + 5) e1) e2
       | Raise _ ->
            i + 1
       | Return _ ->
            succ i
       | LetApply (_, _, _, _, e) ->
            size_exp fenv (succ i) e

let is_small_exp fenv e =
   size_exp fenv 0 e < is_small_max

(************************************************************************
 * LOOP-NEST TREE
 ************************************************************************)

(*
 * This is the node we pass to the loop-nest algorithm.
 *)
type node =
   { node_name : var;
     node_succ : var list
   }

(*
 * Name of the root node.
 *)
let root_sym = new_symbol_string "root"

(*
 * Count number of uses of each var.
 *)
let count_empty = SymbolTable.empty

let count_lookup count v =
   try SymbolTable.find count v with
      Not_found ->
         0

let count_var count v =
   SymbolTable.add count v (succ (count_lookup count v))

let count_atom count a =
   match a with
      AtomVar v -> count_var count v
    | _ -> count

let count_atom_opt count = function
   Some a -> count_atom count a
 | None -> count

let count_atoms count args =
   List.fold_left count_atom count args

(*
 * Live variable set.
 *)
let live_empty = SymbolSet.empty

(*
 * Add a function occurrence.
 *)
let live_var live f =
   SymbolSet.add live f

let live_atom live = function
   AtomVar v -> live_var live v
 | _ -> live

let live_atom_opt live = function
   Some a -> live_atom live a
 | None -> live

let live_atoms live args =
   List.fold_left live_atom live args

(*
 * Function table keeps live set for each function
 *)
let fenv_empty = SymbolTable.empty

let fenv_add fenv f vars =
   SymbolTable.add fenv f vars

let fenv_mem fenv f =
   SymbolTable.mem fenv f

(*
 * Check if a node1 is an ancestor for node2.
 *)
let rec is_ancestor fenv name1 name2 =
   if Symbol.eq name1 name2 then
      true
   else
      let name3, _, _ = SymbolTable.find fenv name2 in
         if Symbol.eq name3 name2 then
            false
         else
            is_ancestor fenv name1 name3

(*
 * Print ancestor list.
 *)
let rec pp_print_ancestors buf fenv (name, degree, headp) =
   pp_print_space buf ();
   pp_print_string buf ": ";
   pp_print_int buf degree;
   pp_print_string buf " ";
   pp_print_symbol buf name;
   if headp then
      pp_print_string buf " <loop-header>";
   let name', degree, headp = SymbolTable.find fenv name in
      if not (Symbol.eq name' name) then
         pp_print_ancestors buf fenv (name', degree, headp)

(*
 * Collect the number of uses of each variable.
 *)
let rec call_exp fenv count live e =
   match dest_exp_core e with
      LetFuns (funs, e) ->
         let fenv, count, live =
            List.fold_left (fun (fenv, count, live) (f, _, gflag, _, _, e) ->
                  let fenv, count, live' = call_exp fenv count live_empty e in
                  let fenv = fenv_add fenv f live' in
                  let live =
                     match gflag with
                        VarGlobalClass ->
                           live_var live f
                      | VarStaticClass
                      | VarLocalClass ->
                           live
                  in
                     fenv, count, live) (fenv, count, live) funs
         in
            call_exp fenv count live e
    | TailCall (f, args) ->
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) args in
            fenv, count, live
    | IfThenElse (_, e1, e2)
    | Try (e1, _, e2) ->
         let fenv, count, live = call_exp fenv count live e1 in
            call_exp fenv count live e2
    | LetAtom (_, _, a, e)
    | LetUnop (_, _, _, a, e)
    | LetMalloc (_, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom count a in
         let live = live_atom live a in
            fenv, count, live
    | LetBinop (_, _, _, a1, a2, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom count a1) a2 in
         let live = live_atom (live_atom live a1) a2 in
            fenv, count, live
    | LetVar (_, _, a_opt, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom_opt count a_opt in
         let live = live_atom_opt live a_opt in
            fenv, count, live
    | LetApply (_, _, f, args, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atoms (count_var count f) args in
         let live = live_atoms (live_var live f) args in
            fenv, count, live
    | LetExtCall (_, _, _, _, _, args, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atoms count args in
         let live = live_atoms live args in
            fenv, count, live
    | LetSubscript (_, _, v1, a2, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_var count v1) a2 in
         let live = live_atom (live_var live v1) a2 in
            fenv, count, live
    | SetSubscript (v1, a2, _, a3, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom (count_var count v1) a2) a3 in
         let live = live_atom (live_atom (live_var live v1) a2) a3 in
            fenv, count, live
    | Memcpy (v1, a1, v2, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_var (count_atom (count_var count v1) a1) v2 in
         let live = live_var (live_atom (live_var live v1) a1) v2 in
            fenv, count, live
    | LetAddrOfVar (_, _, v, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_var count v in
         let live = live_var live v in
            fenv, count, live
    | SetVar (v, _, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_var count v) a in
         let live = live_atom (live_var live v) a in
            fenv, count, live
    | Debug (_, e) ->
         call_exp fenv count live e
    | Return (f, a) ->
         fenv, count_atom count a, live_atom live a
    | Raise a ->
         fenv, count_atom count a, live_atom live a

(*
 * Filter the live set so that it contains only the
 * function names.
 *)
let filter_live fenv live =
   SymbolSet.fold (fun live v ->
         if fenv_mem fenv v then
            v :: live
         else
            live) [] live

(*
 * Construct the call-graph.
 *)
let call_prog prog =
   let { prog_body = body;
         prog_decls = decls
       } = prog
   in

   (* Toplevel functions *)
   let fenv = fenv_empty in

   (* Root is the initialization code *)
   let fenv, count, live = call_exp fenv count_empty live_empty body in

   (* Create successor table by filtering out live vars that are functions *)
   let nodes =
      SymbolTable.fold (fun nodes f live ->
            let node = f, filter_live fenv live in
               node :: nodes) [] fenv
   in
   let root_node = root_sym, filter_live fenv live in

   (* Debugging *)
   let _ =
      if debug Fir_state.debug_print_air then
         let buf = err_formatter in
            pp_open_vbox buf tabstop;
            pp_print_string buf "*** Call graph:";
            List.iter (fun (f, succ) ->
                  pp_print_space buf ();
                  pp_print_symbol buf f;
                  pp_print_string buf ":";
                  List.iter (fun v ->
                        pp_print_string buf " ";
                        pp_print_symbol buf v) succ) (root_node :: nodes);
            pp_close_box buf ();
            pp_print_newline buf ()
   in

   (* Use the dominators *)
   let loop = Loop.create "Fc_air_inline" fst snd root_node nodes in
   let fenv = Loop.dominators loop fst in
   let nest = Loop.loop_nest loop fst in
   let head =
      List.fold_left (fun head (f, _) ->
            SymbolSet.add head f) SymbolSet.empty (Trace.special_nodes nest)
   in
   let fenv =
      SymbolTable.mapi (fun f v ->
            v, count_lookup count f, SymbolSet.mem head f) fenv
   in
   let _ =
      if debug Fir_state.debug_print_air then
         let buf = err_formatter in
            pp_open_vbox buf tabstop;
            pp_print_string buf "*** Dominator indexes:";
            SymbolTable.iter (fun f name ->
                  pp_print_space buf ();
                  pp_print_symbol buf f;
                  pp_print_string buf ": ";
                  pp_print_ancestors buf fenv name) fenv;
            pp_close_box buf ();
            pp_print_newline buf ()
   in
      fenv

(*
 * Construct the initial available list for the functions.
 *)
let aenv_function fenv aenv f gflag vars e =
   let _, degree, headp = SymbolTable.find fenv f in
      if not headp && is_small_exp fenv e && (degree <= 1 || gflag <> VarLocalClass) then
         aenv_add aenv f (AvailFunction (f, gflag, vars, e))
      else
         begin
            if debug Fir_state.debug_print_air then
               eprintf "aenv_function: rejected function %s because it is too big: %d@." (**)
                  (string_of_symbol f) (size_exp fenv 0 e);
            aenv
         end

let avail_prog prog =
   let { prog_body = body;
         prog_globals = globals;
       } = prog
   in
   let fenv = call_prog prog in
   let aenv =
      SymbolTable.fold (fun aenv v (ty, volp, init) ->
            match init with
               InitAtom a ->
                  if volp then
                     aenv
                  else
                     aenv_add aenv v (AvailAtom a)
             | InitString _
             | InitBlock _ ->
                  aenv) aenv_empty globals
   in
      fenv, aenv

(************************************************************************
 * INLINE UTILITIES
 ************************************************************************)

let rawint_bool tenv pos ty flag =
   match tenv_expand tenv pos ty with
      TyUnit 2 ->
         if flag then
            1
         else
            0
    | TyInt (pre, signed) ->
         raise (Invalid_argument "fc_air_inline:rawint_bool: result is not a bool, but an int")
    | ty ->
         raise (AirException (pos, StringTypeError ("not an int type", ty)))

let rawint_compare tenv pos ty cmp i j =
   rawint_bool tenv pos ty (cmp (Rawint.compare i j))

let rawfloat_compare tenv pos ty cmp x y =
   rawint_bool tenv pos ty (cmp (Rawfloat.compare x y))

(************************************************************************
 * SECOND PASS
 ************************************************************************)

(*
 * Type of a malloc.
 *)
let ty_malloc = TyPointer (TyInt (Rawint.Int8, false))

(*
 * Inline a var.
 *)
let inline_var aenv v =
   match aenv_lookup aenv v with
      AvailAtom (AtomVar v)
    | AvailVar (v, _)
    | AvailAddressOf (v, _)
    | AvailSubscript (v, _, _)
    | AvailFunction (v, _, _, _) ->
         v
    | AvailAtom _ ->
         v

(*
 * Inline an aexp.
 *)
let rec inline_aexp aenv e =
   (* If aexp contains a global constant, replace it.
      It'd better be an integer.
    *)
   match e with
      AInt _ ->
         e
    | AVar v ->
         let avail = aenv_lookup aenv v in
            (match avail with
               AvailAtom (AtomVar _) ->
                  AVar (inline_var aenv v)
             | AvailAtom _ ->
                  (match atom_of_avail avail with
                     AtomInt i ->
                        AInt (Rawint.to_int i)
                   | _ ->
                        AVar (inline_var aenv v)
                  )
             | _ ->
                  AVar (inline_var aenv v)
            )
    | APlus (e1, e2) ->
         APlus (inline_aexp aenv e1, inline_aexp aenv e2)
    | AMinus (e1, e2) ->
         AMinus (inline_aexp aenv e1, inline_aexp aenv e2)
    | ATimes (e1, e2) ->
         ATimes (inline_aexp aenv e1, inline_aexp aenv e2)
    | ARoundUp (e1, i) ->
         ARoundUp (inline_aexp aenv e1, i)
    | AMax (e1, e2) ->
         AMax (inline_aexp aenv e1, inline_aexp aenv e2)

(*
 * Process a type.
 * We rename variables in array bounds and aexp's, and
 * also try to evaluate aexp's.
 *)
let rec inline_type aenv ty =
   let inline_bound bound =
      match bound with
         Fc_air.ArrayVar v ->
            let avail = aenv_lookup aenv v in
               (match avail with
                   AvailAtom (AtomVar _) ->
                      ArrayVar (inline_var aenv v)
                 | AvailAtom _ ->
                      (match atom_of_avail avail with
                          AtomInt i ->
                             ArrayInt (Rawint.to_int i)
                        | _ ->
                             ArrayVar (inline_var aenv v)
                      )
                 | _ ->
                      ArrayVar (inline_var aenv v)
               )
       | _ ->
            bound
   in
      match ty with
         TyDelayed
       | TyUnit _
       | TyZero
       | TyInt _
       | TyField _
       | TyFloat _
       | TyTag
       | TyEnum (Label _)
       | TyUEnum (Label _)
       | TyStruct (Label _) ->
            ty
       | TyPointer ty' ->
            TyPointer (inline_type aenv ty')
       | TyRef ty' ->
            TyRef (inline_type aenv ty')
       | TyArray (ty', bound1, bound2) ->
            TyArray (ty', inline_bound bound1, inline_bound bound2)
       | TyTuple tuples ->
            let tfields =
               List.map (fun (ty, aexp) ->
                     let aexp = inline_aexp aenv aexp in
                     let aexp = match aexp_evaluate aexp with
                                   Some i ->
                                      AInt i
                                 | _ ->
                                      aexp
                     in
                        inline_type aenv ty, aexp) tuples.tuple_fields in
            let tsize = inline_aexp aenv tuples.tuple_size in
            let tsize =
               match aexp_evaluate tsize with
                  Some i ->
                     AInt i
                | _ ->
                     tsize
            in
               TyTuple { tuple_fields = tfields; tuple_size = tsize }
       | TyElide tuples ->
            let tfields =
               List.map (fun (ty, aexp) ->
                     let aexp = inline_aexp aenv aexp in
                     let aexp = match aexp_evaluate aexp with
                                   Some i ->
                                      AInt i
                                 | _ ->
                                      aexp
                     in
                        inline_type aenv ty, aexp) tuples.tuple_fields in
            let tsize = inline_aexp aenv tuples.tuple_size in
            let tsize =
               match aexp_evaluate tsize with
                  Some i ->
                     AInt i
                | _ ->
                     tsize
            in
               TyElide { tuple_fields = tfields; tuple_size = tsize }
       | TyEnum (Fields enums) ->
            let efields = enums.enum_fields in
            let esize = inline_aexp aenv enums.enum_size in
            let esize =
               match aexp_evaluate esize with
                  Some i ->
                     AInt i
                | _ ->
                     esize
            in
               TyEnum (Fields { enum_fields = efields; enum_size = esize })
       | TyUEnum (Fields enums) ->
            let uefields = enums.uenum_fields in
            let uefields =
               SymbolTable.map (fun (ty_opt, label) ->
                     inline_type_opt aenv ty_opt, label) uefields
            in
            let uesize = inline_aexp aenv enums.uenum_size in
            let uesize =
               match aexp_evaluate uesize with
                  Some i ->
                     AInt i
                | _ ->
                     uesize
            in
               TyUEnum  (Fields { uenum_fields = uefields; uenum_size = uesize })
       | TyStruct (Fields fields) ->
            let sfields = fields.struct_fields in
            let sfields =
               SymbolTable.map (fun field ->
                     let { field_type = ft;
                           field_index = fi;
                           field_offset = fo;
                           field_bits = fb;
                           field_next = fn } = field
                     in
                     let fo = inline_aexp aenv fo in
                     let fo =
                        match aexp_evaluate fo with
                           Some i ->
                              AInt i
                         | _ ->
                              fo
                     in
                        { field_type = inline_type aenv ft;
                          field_index = fi;
                          field_offset = fo;
                          field_bits = fb;
                          field_next = fn }) sfields
            in
            let sroot = fields.struct_root in
            let ssize = inline_aexp aenv fields.struct_size in
            let ssize =
               match aexp_evaluate ssize with
                  Some i ->
                     AInt i
                | _ ->
                     ssize
            in
               TyStruct (Fields { struct_fields = sfields; struct_root = sroot; struct_size = ssize })
       | TyFun (ty_list, ty') ->
            TyFun (inline_type_list aenv ty_list, inline_type aenv ty')

         (* Type variables didn't change *)
       | TyVar v ->
            ty
       | TyLambda (vars, ty') ->
            TyLambda (vars, inline_type aenv ty')
       | TyAll (vars, ty') ->
            TyAll (vars, inline_type aenv ty')
       | TyApply (var, ty_list) ->
            TyApply (var, inline_type_list aenv ty_list)

and inline_type_opt aenv ty =
   match ty with
      Some ty' ->
         Some (inline_type aenv ty')
    | None ->
         ty

and inline_type_list aenv ty_list =
   List.map (fun ty ->
         inline_type aenv ty) ty_list

(*
 * Inline an atom.
 *)
let inline_atom aenv a =
   match a with
      AtomUnit _
    | AtomInt _
    | AtomFloat _
    | AtomNil _ ->
         a
    | AtomVar v ->
         match aenv_lookup aenv v with
            AvailAtom a ->
               a
          | AvailVar (v, _)
          | AvailAddressOf (v, _)
          | AvailSubscript (v, _, _)
          | AvailFunction (v, _, _, _) ->
               AtomVar v

let inline_atom_opt aenv = function
   Some a -> Some (inline_atom aenv a)
 | None -> None

let inline_atoms aenv args =
   List.map (inline_atom aenv) args

(*
 * Perform inlining.
 *)
let rec inline_exp tenv fenv renv aenv name e =
   let pos = string_pos "inline_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            inline_funs_exp tenv fenv renv aenv name pos loc funs e
       | LetAtom (v, ty, a, e) ->
            let ty = inline_type aenv ty in
               inline_atom_exp tenv fenv renv aenv name pos loc v ty a e
       | LetUnop (v, ty, op, a, e) ->
            let ty = inline_type aenv ty in
               inline_unop_exp tenv fenv renv aenv name pos loc v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            let ty = inline_type aenv ty in
               inline_binop_exp tenv fenv renv aenv name pos loc v ty op a1 a2 e
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            let ty = inline_type aenv ty in
            let ty' = inline_type aenv ty' in
               inline_extcall_exp tenv fenv renv aenv name pos loc v ty s b ty' args e
       | TailCall (f, args) ->
            inline_tailcall_exp tenv fenv renv aenv name pos loc f args
       | LetMalloc (v, a, e) ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailVar (v', ty_malloc)) in
               make_exp loc (LetMalloc (v', inline_atom aenv a, inline_exp tenv fenv renv aenv name e))
       | IfThenElse (a, e1, e2) ->
            make_exp loc (IfThenElse (inline_atom aenv a,
                                      inline_exp tenv fenv renv aenv name e1,
                                      inline_exp tenv fenv renv aenv name e2))
       | LetSubscript (v, ty, v2, a3, e) ->
            let ty = inline_type aenv ty in
               inline_subscript_exp tenv fenv renv aenv name pos loc v ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            let ty = inline_type aenv ty in
               inline_set_subscript_exp tenv fenv renv aenv name pos loc v1 a2 ty a3 e
       | Memcpy (v1, a1, v2, e) ->
            make_exp loc (Memcpy (inline_var aenv v1,
                                  inline_atom aenv a1,
                                  inline_var aenv v2,
                                  inline_exp tenv fenv renv aenv name e))
       | LetVar (v, ty, a_opt, e) ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailVar (v', ty)) in
            let ty = inline_type aenv ty in
               make_exp loc (LetVar (v', ty, inline_atom_opt aenv a_opt, inline_exp tenv fenv renv aenv name e))
       | LetAddrOfVar (v1, ty, v2, e) ->
            let ty = inline_type aenv ty in
               inline_addr_of_exp tenv fenv renv aenv name pos loc v1 ty v2 e
       | SetVar (v, ty, a, e) ->
            let ty = inline_type aenv ty in
               make_exp loc (SetVar (inline_var aenv v, ty, inline_atom aenv a, inline_exp tenv fenv renv aenv name e))
       | Debug (info, e) ->
            inline_debug_exp tenv fenv renv aenv name pos loc info e
       | Raise a ->
            make_exp loc (Raise (inline_atom aenv a))
       | Return (f, a) ->
            inline_return_exp tenv fenv renv aenv name pos loc f a
       | Try (e1, v, e2) ->
            make_exp loc (Try (inline_exp tenv fenv renv aenv name e1, v, inline_exp tenv fenv renv aenv name e2))
       | LetApply (v, ty, f, args, e) ->
            let ty = inline_type aenv ty in
               inline_apply_exp tenv fenv renv aenv name pos loc v ty f args e

(*
 * Add the function definitions.
 * Add the functions to the inline environment.
 *)
and inline_funs_exp tenv fenv renv aenv name pos loc funs e =
   let aenv, funs =
      List.fold_left (fun (aenv, funs) fund ->
            let f, line, gflag, ty, vars, e = fund in
               if aenv_mem aenv f then
                  aenv, funs
               else
                  let _, ty_vars, _ = dest_fun_type tenv pos ty in
                  let aenv = aenv_function fenv aenv f gflag vars e in
                  let funs = fund :: funs in
                     aenv, funs) (aenv, []) funs
   in
   let funs =
      List.fold_left (fun funs fund ->
            let f, line, gflag, ty, vars, e = fund in
            let vars' = List.map new_symbol vars in
            let _, ty_vars, _ = dest_fun_type tenv pos ty in
            let aenv =
               Mc_list_util.fold_left3 (fun aenv v v' ty ->
                     aenv_add aenv v (AvailVar (v', ty))) aenv vars vars' ty_vars
            in
            let e = inline_exp tenv fenv renv aenv f e in
            let fund = f, line, gflag, ty, vars', e in
               fund :: funs) [] funs
   in
   let e = inline_exp tenv fenv renv aenv name e in
      make_exp loc (LetFuns (funs, e))

(*
 * Operators.
 * Try to perform constant folding.
 *)
and inline_atom_exp tenv fenv renv aenv name pos loc v ty a e =
   let avail = aenv_lookup_atom aenv a in
   let aenv = aenv_add aenv v avail in
   let a = atom_of_avail avail in
   let e = inline_exp tenv fenv renv aenv name e in
      make_exp loc (LetAtom (v, ty, a, e))

and inline_unop_exp tenv fenv renv aenv name pos loc v ty op a e =
   let a = inline_atom aenv a in
   let a' =
      match op, a with
         UMinusIntOp _, AtomInt i ->
            Some (AtomInt (Rawint.neg i))
       | NotIntOp _, AtomInt i ->
            Some (AtomInt (Rawint.lognot i))
       | BitFieldOp (_, _, off, len), AtomInt i ->
            Some (AtomInt (Rawint.field i off len))

       | UMinusFloatOp _, AtomFloat x ->
            Some (AtomFloat (Rawfloat.neg x))
       | AbsFloatOp _, AtomFloat x ->
            Some (AtomFloat (Rawfloat.abs x))

       | IntOfInt (pre1, signed1, _, _), AtomInt i ->
            Some (AtomInt (Rawint.of_rawint pre1 signed1 i))
       | IntOfFloat (pre, signed, _), AtomFloat x ->
            Some (AtomInt (Rawint.of_float pre signed (Rawfloat.to_float x)))
       | FloatOfInt (pre, _, _), AtomInt i ->
            Some (AtomFloat (Rawfloat.of_rawint pre i))
       | FloatOfFloat (pre, _), AtomFloat x ->
            Some (AtomFloat (Rawfloat.of_rawfloat pre x))

       | _ ->
            (* None of the others get inlined *)
            None
   in
   let v' = new_symbol v in
   let aenv =
      match a' with
         Some a ->
            aenv_add aenv v (AvailAtom a)
       | None ->
            aenv_add aenv v (AvailVar (v', ty))
   in
   let e = inline_exp tenv fenv renv aenv name e in
      make_exp loc (LetUnop (v', ty, op, a, e))

and inline_binop_exp tenv fenv renv aenv name pos loc v ty op a1 a2 e =
   let avail1 = aenv_lookup_atom aenv a1 in
   let avail2 = aenv_lookup_atom aenv a2 in
   let a1 = atom_of_avail avail1 in
   let a2 = atom_of_avail avail2 in
   let a' =
      match op, a1, a2 with
(*
 * Don't include pointer ops because they
 * often include pointer type conversions.
 * It is ok to put these off; they'll be picked
 * up in the IR phase when all the pointer types
 * are just TyRawData.
 *
         PlusIntOp _, _, AtomInt j
       | MinusIntOp _, _, AtomInt j
       | PlusPointerOp, _, AtomInt j
         when Rawint.is_zero j ->
            Some2 avail1
*)
       | PlusIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.add i j))
       | MinusIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.sub i j))
       | MulIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.mul i j))
       | DivIntOp _, AtomInt i, AtomInt j when not (Rawint.is_zero j) ->
            Some1 (AtomInt (Rawint.div i j))
       | RemIntOp _, AtomInt i, AtomInt j when not (Rawint.is_zero j) ->
            Some1 (AtomInt (Rawint.rem i j))
       | SlIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.shift_left i j))
       | SrIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.shift_right i j))
       | AndIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.logand i j))
       | OrIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.logor i j))
       | AndBoolOp, AtomUnit (2, c1), AtomUnit (2, c2) ->
            Some1 (AtomUnit (2, c1 land c2))
       | OrBoolOp, AtomUnit (2, c1), AtomUnit (2, c2) ->
            Some1 (AtomUnit (2, c1 lor c2))
       | XorIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.logxor i j))
       | MinIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.min i j))
       | MaxIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.max i j))
       | EqIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i = 0) i j))
       | NeqIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i <> 0) i j))
       | LtIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i < 0) i j))
       | LeIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i <= 0) i j))
       | GtIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i > 0) i j))
       | GeIntOp _, AtomInt i, AtomInt j ->
            Some1 (AtomUnit (2, rawint_compare tenv pos ty (fun i -> i >= 0) i j))
       | SetBitFieldOp (_, _, off, len), AtomInt i, AtomInt j ->
            Some1 (AtomInt (Rawint.set_field i off len j))

       | PlusFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.add x y))
       | MinusFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.sub x y))
       | MulFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.mul x y))
       | DivFloatOp _, AtomFloat x, AtomFloat y when not (Rawfloat.is_zero y) ->
            Some1 (AtomFloat (Rawfloat.div x y))
       | RemFloatOp _, AtomFloat x, AtomFloat y when not (Rawfloat.is_zero y) ->
            Some1 (AtomFloat (Rawfloat.rem x y))
       | MinFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.min x y))
       | MaxFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomFloat (Rawfloat.max x y))
       | EqFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i = 0) x y))
       | NeqFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i <> 0) x y))
       | LtFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i < 0) x y))
       | LeFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i <= 0) x y))
       | GtFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i > 0) x y))
       | GeFloatOp _, AtomFloat x, AtomFloat y ->
            Some1 (AtomUnit (2, rawfloat_compare tenv pos ty (fun i -> i >= 0) x y))

       | EqPointerOp, AtomNil _, AtomNil _ ->
            Some1 (AtomUnit (2, rawint_bool tenv pos ty true))
       | NeqPointerOp, AtomNil _, AtomNil _ ->
            Some1 (AtomUnit (2, rawint_bool tenv pos ty false))

       | _ ->
            (* None of the others get inlined *)
            None0
   in

   (* If IdOp, then add to avail list *)
   let v' = new_symbol v in
   let aenv =
      match a' with
         Some2 avail ->
            aenv_add aenv v avail
       | Some1 a ->
            aenv_add aenv v (AvailAtom a)
       | None0 ->
            aenv_add aenv v (AvailVar (v', ty))
   in
   let e = inline_exp tenv fenv renv aenv name e in
      make_exp loc (LetBinop (v', ty, op, a1, a2, e))

and inline_extcall_exp tenv fenv renv aenv name pos loc v ty s b ty' args e =
   let args = inline_atoms aenv args in
   let v' = new_symbol v in
   let aenv = aenv_add aenv v (AvailVar (v', ty)) in
   let e = inline_exp tenv fenv renv aenv name e in
      make_exp loc (LetExtCall (v', ty, s, b, ty', args, e))

(*
 * Inline the tailcall.
 * If the function is available, substitute and recurse.
 *)
and inline_tailcall_exp tenv fenv renv aenv name pos loc f args =
   let args = inline_atoms aenv args in
      if debug Fir_state.debug_print_air then
         eprintf "Tailcall: checking %s from %s@." (string_of_symbol f) (string_of_symbol name);
      if Symbol.eq f name || is_ancestor fenv f name then
         make_exp loc (TailCall (inline_var aenv f, args))
      else
         match aenv_lookup aenv f with
            AvailFunction (f, gflag, vars, e) as avail ->
               if debug Fir_state.debug_print_air then
                  eprintf "Tailcall: inlining %s from %s@." (string_of_symbol f) (string_of_symbol name);
               let aenv = aenv_bind_vars aenv pos vars args in
                  inline_exp tenv fenv renv aenv f e
          | AvailAtom (AtomVar v) when not (Symbol.eq v f) ->
               inline_tailcall_exp tenv fenv renv aenv name pos loc v args
          | _ ->
               make_exp loc (TailCall (f, args))

(*
 * Inline an application.
 *)
and inline_apply_exp tenv fenv renv aenv name pos loc v ty f args e =
   let args = inline_atoms aenv args in
      if debug Fir_state.debug_print_air then
         eprintf "Apply: checking %s from %s@." (string_of_symbol f) (string_of_symbol name);
      if Symbol.eq f name || is_ancestor fenv f name then
         let v' = new_symbol v in
         let aenv = aenv_add aenv v (AvailVar (v', ty)) in
         let e = inline_exp tenv fenv renv aenv name e in
            if debug Fir_state.debug_print_air then
               eprintf "Apply: rejected ancestor %s from %s@." (string_of_symbol f) (string_of_symbol name);
            make_exp loc (LetApply (v', ty, inline_var aenv f, args, e))
      else
         match aenv_lookup aenv f with
            AvailFunction (f, gflag, vars, e') as avail ->
               if debug Fir_state.debug_print_air then
                  eprintf "Apply: inlining %s from %s@." (string_of_symbol f) (string_of_symbol name);
               let aenv = aenv_bind_vars aenv pos vars args in
               let renv =
                  SymbolTable.add renv f (fun tenv fenv aenv _ pos g a ->
                        if not (Symbol.eq g f) then
                           raise (AirException (pos, StringVar2Error ("unexpected return", f, g)));
                        let aenv = aenv_add aenv v (AvailAtom a) in
                           inline_exp tenv fenv renv aenv name e)
               in
                  inline_exp tenv fenv renv aenv f e'
          | AvailAtom (AtomVar f') when not (Symbol.eq f' f) ->
               if debug Fir_state.debug_print_air then
                  eprintf "Apply: recursive inline %s from %s@." (string_of_symbol f') (string_of_symbol name);
               inline_apply_exp tenv fenv renv aenv name pos loc v ty f' args e
          | _ ->
               let v' = new_symbol v in
               let aenv = aenv_add aenv v (AvailVar (v', ty)) in
               let e = inline_exp tenv fenv renv aenv name e in
                  make_exp loc (LetApply (v', ty, f, args, e))

(*
 * Inline a return.
 *)
and inline_return_exp tenv fenv renv aenv name pos loc f a =
   try
      let cont = SymbolTable.find renv f in
         cont tenv fenv aenv name pos f a
   with
      Not_found ->
         make_exp loc (Return (inline_var aenv f, inline_atom aenv a))

(*
 * Cancel addr-of and subscripting.
 *)
and inline_subscript_exp tenv fenv renv aenv name pos loc v ty v2 a3 e =
   let a3 = inline_atom aenv a3 in
   let e =
      match aenv_lookup aenv v2, a3 with
         AvailAddressOf (_, v2), AtomInt i when Rawint.is_zero i ->
            let aenv = aenv_add aenv v (AvailAtom (AtomVar v2)) in
               LetAtom (v, ty, AtomVar v2, inline_exp tenv fenv renv aenv name e)
       | AvailAtom (AtomVar v2), _ ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailSubscript (v', v2, a3)) in
               LetSubscript (v', ty, v2, a3, inline_exp tenv fenv renv aenv name e)
       | AvailAtom a, AtomInt i when Rawint.is_zero i ->
            let aenv = aenv_add aenv v (AvailAtom a) in
               LetAtom (v, ty, a, inline_exp tenv fenv renv aenv name e)
       | _ ->
            let v' = new_symbol v in
            let v2 = inline_var aenv v2 in
            let aenv = aenv_add aenv v (AvailSubscript (v', v2, a3)) in
               LetSubscript (v', ty, v2, a3, inline_exp tenv fenv renv aenv name e)
   in
      make_exp loc e

and inline_set_subscript_exp tenv fenv renv aenv name pos loc v1 a2 ty a3 e =
   let a2 = inline_atom aenv a2 in
   let a3 = inline_atom aenv a3 in
   let e =
      match aenv_lookup aenv v1, a2 with
         AvailAddressOf (_, v1), AtomInt i when Rawint.is_zero i ->
            SetVar (inline_var aenv v1, ty, a3, inline_exp tenv fenv renv aenv name e)
       | _ ->
            SetSubscript (inline_var aenv v1, a2, ty, a3, inline_exp tenv fenv renv aenv name e)
   in
      make_exp loc e

and inline_addr_of_exp tenv fenv renv aenv name pos loc v1 ty v2 e =
   let pos = string_pos "inline_addr_of_exp" pos in
   let e =
      match aenv_lookup aenv v2 with
         AvailSubscript (_, v, a) ->
            LetBinop (v1, ty, PlusPointerOp, AtomVar v, a, inline_exp tenv fenv renv aenv name e)
       | AvailAtom _ ->
            let v1' = new_symbol v1 in
            let v2 = inline_var aenv v2 in
            let aenv =
               aenv_add aenv v1 (AvailAddressOf (v1', v2))
            in
               LetAddrOfVar (v1', ty, v2, inline_exp tenv fenv renv aenv name e)
       | _ ->
            let ty' = type_of_var aenv pos v2 in
            let v1' = new_symbol v1 in
            let v2 = inline_var aenv v2 in
            let aenv =
               if is_scalar_type tenv pos ty' then
                  aenv_add aenv v1 (AvailAddressOf (v1', v2))
               else
                  aenv_add aenv v1 (AvailVar (v1', ty))
            in
               LetAddrOfVar (v1', ty, v2, inline_exp tenv fenv renv aenv name e)
   in
      make_exp loc e

(*
 * Debugging.
 *)
and inline_debug_exp tenv fenv renv aenv name pos loc info e =
   let e = inline_exp tenv fenv renv aenv name e in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) ->
                     let v2 =
                        match aenv_lookup aenv v2 with
                           AvailAtom (AtomVar v2) -> v2
                         | _ -> v2
                     in
                        v1, ty, v2) vars
            in
               DebugContext (line, vars)
   in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Inline the entire program.
 *)
let zero_loc = bogus_loc "<Fc_air_inline>"

let inline_prog prog =
   let { prog_types = tenv;
         prog_body = body
       } = prog
   in
   let fenv, aenv = avail_prog prog in

   (* Add return expression for initialization body *)
   let renv =
      SymbolTable.add SymbolTable.empty Fc_ast_parse.exit_sym (fun tenv fenv aenv _ pos g a ->
            make_exp zero_loc (Return (inline_var aenv g, inline_atom aenv a)))
   in

   (* Inline the expression *)
   let body = inline_exp tenv fenv renv aenv root_sym body in
   let tenv, tenv' =
      SymbolTable.fold (fun (tenv, tenv') v ty ->
         let ty = inline_type aenv ty in
         let tenv' = SymbolTable.add tenv' v ty in
            tenv, tenv') (tenv, SymbolTable.empty) tenv
   in
   let prog = { prog with prog_body = body; prog_types = tenv' } in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
