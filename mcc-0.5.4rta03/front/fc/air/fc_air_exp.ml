(*
 * Arithmetic expressions involving integers, basic arithmetic,
 * and vars.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fc_config
open Fc_air

(*
 * Basic operations.
 *)
let aexp_plus e1 e2 =
   match e1, e2 with
      AInt i1, AInt i2 ->
         AInt (i1 + i2)
    | _ ->
         APlus (e1, e2)

let aexp_minus e1 e2 =
   match e1, e2 with
      AInt i1, AInt i2 ->
         AInt (i1 - i2)
    | _ ->
         AMinus (e1, e2)

let aexp_times e1 e2 =
   match e1, e2 with
      AInt i1, AInt i2 ->
         AInt (i1 * i2)
    | _ ->
         ATimes (e1, e2)

let aexp_roundup e j =
   match e with
      AInt i ->
         AInt ((i + j - 1) land (lnot (j - 1)))
    | _ ->
         ARoundUp (e, j)

let aexp_max e1 e2 =
   match e1, e2 with
      AInt i1, AInt i2 ->
         AInt (max i1 i2)
    | _ ->
         AMax (e1, e2)

let rec aexp_evaluate = function
      AInt i ->
        Some i
    | AVar _ ->
	None
    | APlus (e1, e2) ->
	(match aexp_evaluate e1, aexp_evaluate e2 with
	      Some i1, Some i2 ->
	        Some (i1+i2)
	    | _ ->
		None)
    | AMinus (e1, e2) ->
	(match aexp_evaluate e1, aexp_evaluate e2 with
	      Some i1, Some i2 ->
	        Some (i1-i2)
	    | _ ->
		None)
    | ATimes (e1, e2) ->
	(match aexp_evaluate e1, aexp_evaluate e2 with
	      Some i1, Some i2 ->
	        Some (i1*i2)
	    | _ ->
		None)
    | ARoundUp (e1, i) ->
	(match aexp_evaluate e1 with
	      Some i1 ->
	        Some (int_of_float ((ceil (float_of_int i1) /. (float_of_int i)) *. (float_of_int i)))
	    | _ ->
		None)
    | AMax (e1, e2) ->
	(match aexp_evaluate e1, aexp_evaluate e2 with
	      Some i1, Some i2 ->
	        Some (max i1 i2)
	    | _ ->
		None)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
