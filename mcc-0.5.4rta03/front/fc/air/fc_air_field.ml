(*
 * Insert explicit copying for aggregates.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Symbol

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_env
open Fc_air_exn
open Fc_air_pos
open Fc_air_type
open Fc_air_frame
open Fc_air_print

module Pos = MakePos (struct let name = "Fc_air_field" end)
open Pos

(************************************************************************
 * TRANSLATE TYPES
 ************************************************************************)

(*
 * Pass aggregates with pointers.
 *)
let rec field_type ty =
   match ty with
      TyField (pre, signed, _, _) ->
         TyInt (pre, signed)
    | TyUnit _
    | TyDelayed
    | TyTag
    | TyZero
    | TyInt _
    | TyFloat _
    | TyVar _
    | TyEnum _
    | TyUEnum (Label _)
    | TyStruct (Label _) ->
         ty
    | TyPointer ty
    | TyRef ty ->
         TyPointer (field_type ty)
    | TyArray (ty, e1, e2) ->
         TyArray (field_type ty, e1, e2)
    | TyTuple { tuple_size = size; tuple_fields = fields } ->
         let fields = List.map (fun (ty, e) -> field_type ty, e) fields in
            TyTuple { tuple_size = size; tuple_fields = fields }
    | TyElide { tuple_size = size; tuple_fields = fields } ->
         let fields = List.map (fun (ty, e) -> field_type ty, e) fields in
            TyElide { tuple_size = size; tuple_fields = fields }
    | TyUEnum (Fields { uenum_size = size; uenum_fields = fields }) ->
         let fields =
            SymbolTable.map (fun (ty_opt, label) ->
                  field_type_opt ty_opt, label) fields
         in
            TyUEnum (Fields { uenum_size = size; uenum_fields = fields })
    | TyStruct (Fields { struct_size = size; struct_root = root; struct_fields = fields }) ->
         let fields =
            SymbolTable.map (fun field ->
                  { field with field_type = field_type field.field_type }) fields
         in
            TyStruct (Fields { struct_size = size; struct_root = root; struct_fields = fields })
    | TyFun (ty_args, ty_res) ->
         let ty_args = List.map (field_type) ty_args in
         let ty_res = field_type ty_res in
            TyFun (ty_args, ty_res)
    | TyLambda (vars, ty) ->
         TyLambda (vars, field_type ty)
    | TyAll (vars, ty) ->
         TyAll (vars, field_type ty)
    | TyApply (v, tyl) ->
         TyApply (v, List.map field_type tyl)

and field_type_opt = function
   Some ty -> Some (field_type ty)
 | None -> None

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Copy the types in the atom.
 *)
let field_atom a =
   match a with
      AtomNil ty ->
         AtomNil (field_type ty)
    | _ ->
         a

let field_atom_opt = function
   Some a -> Some (field_atom a)
 | None -> None

let field_atoms args =
   List.map field_atom args

(************************************************************************
 * TRANSLATE EXPRESSIONS
 ************************************************************************)

(*
 * Translate the expressions.
 * Add function headers to copy arguments.
 *)
let rec field_exp tenv e =
   let pos = string_pos "field_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            field_funs_exp tenv pos loc funs e
       | LetAtom (v, ty, a, e) ->
            make_exp loc (LetAtom (v, field_type ty, field_atom a, field_exp tenv e))
       | LetUnop (v, ty, op, a, e) ->
            make_exp loc (LetUnop (v, field_type ty, op, field_atom a, field_exp tenv e))
       | LetBinop (v, ty, op, a1, a2, e) ->
            make_exp loc (LetBinop (v, field_type ty, op, field_atom a1, field_atom a2, field_exp tenv e))
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            make_exp loc (LetExtCall (v, field_type ty, s, b, field_type ty', field_atoms args, field_exp tenv e))
       | LetApply (v, ty, f, args, e) ->
            make_exp loc (LetApply (v, field_type ty, f, field_atoms args, field_exp tenv e))
       | TailCall (f, args) ->
            make_exp loc (TailCall (f, field_atoms args))
       | Return (f, a) ->
            make_exp loc (Return (f, field_atom a))
       | Raise a ->
            make_exp loc (Raise (field_atom a))
       | LetMalloc (v, a, e) ->
            make_exp loc (LetMalloc (v, field_atom a, field_exp tenv e))
       | IfThenElse (a, e1, e2) ->
            make_exp loc (IfThenElse (field_atom a, field_exp tenv e1, field_exp tenv e2))
       | Try (e1, v, e2) ->
            make_exp loc (Try (field_exp tenv e1, v, field_exp tenv e2))
       | LetSubscript (v, ty, v1, a2, e) ->
            field_subscript_exp tenv pos loc v ty v1 a2 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            field_set_subscript_exp tenv pos loc v1 a2 ty a3 e
       | Memcpy (v1, a, v2, e) ->
            make_exp loc (Memcpy (v1, field_atom a, v2, field_exp tenv e))
       | SetVar (v, ty, a, e) ->
            make_exp loc (SetVar (v, field_type ty, field_atom a, field_exp tenv e))
       | LetVar (v, ty, a_opt, e) ->
            make_exp loc (LetVar (v, field_type ty, field_atom_opt a_opt, field_exp tenv e))
       | LetAddrOfVar (v1, ty, v2, e) ->
            make_exp loc (LetAddrOfVar (v1, field_type ty, v2, field_exp tenv e))
       | Debug (info, e) ->
            field_debug_exp tenv pos loc info e

(*
 * Insert function headers to copy aggregate arguments.
 *)
and field_funs_exp tenv pos loc funs e =
   let pos = string_pos "field_funs_exp" pos in

   (* Examine all the arguments *)
   let funs =
      List.map (fun (f, loc, gflag, ty, vars, e) ->
            f, loc, gflag, field_type ty, vars, field_exp tenv e) funs
   in

   (* Copy the expression *)
   let e = field_exp tenv e in
      make_exp loc (LetFuns (funs, e))

(*
 * Subscript operation.
 *)
and field_subscript_exp tenv pos loc v ty v1 a2 e =
   let pos = string_pos "field_subscript_exp" pos in
   let ty' = field_type ty in
   let e = field_exp tenv e in
   let a2 = field_atom a2 in
      match tenv_expand tenv pos ty with
         TyField (pre, signed, off, len) ->
            (* Extract the field *)
            let v' = new_symbol v in
            let e = make_exp loc (LetUnop (v, ty', BitFieldOp (pre, signed, off, len), AtomVar v', e)) in
            let e = make_exp loc (LetSubscript (v', ty', v1, a2, e)) in
               e
       | _ ->
            make_exp loc (LetSubscript (v, ty', v1, a2, e))

(*
 * Set subscript operation.
 *)
and field_set_subscript_exp tenv pos loc v1 a2 ty a3 e =
   let pos = string_pos "field_subscript_exp" pos in
   let a2 = field_atom a2 in
   let ty' = field_type ty in
   let a3 = field_atom a3 in
   let e = field_exp tenv e in
      match tenv_expand tenv pos ty with
         TyField (pre, signed, off, len) ->
            (* Insert the field *)
            let v1a = new_symbol v1 in
            let v1b = new_symbol v1 in
            let e = make_exp loc (SetSubscript (v1, a2, ty', AtomVar v1b, e)) in
            let e = make_exp loc (LetBinop (v1b, ty', SetBitFieldOp (pre, signed, off, len), AtomVar v1a, a3, e)) in
            let e = make_exp loc (LetSubscript (v1a, ty', v1, a2, e)) in
               e
       | _ ->
            make_exp loc (SetSubscript (v1, a2, ty', a3, e))

(*
 * Copy debug.
 *)
and field_debug_exp tenv pos loc info e =
   let pos = string_pos "field_debug_exp" pos in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) -> v1, field_type ty, v2) vars
            in
               DebugContext (line, vars)
   in
   let e = field_exp tenv e in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Copy the program.
 *)
let field_prog prog =
   let { prog_file = file;
         prog_globals = globals;
         prog_decls = decls;
         prog_export = export;
         prog_import = import;
         prog_types = types;
         prog_body = body
       } = prog
   in
   let globals =
      SymbolTable.map (fun (ty, volp, init) ->
            field_type ty, volp, init) globals
   in
   let decls =
      SymbolTable.map (fun ty ->
            field_type ty) decls
   in
   let export =
      SymbolTable.map (fun info ->
            { info with export_type = field_type info.export_type }) export
   in
   let import =
      SymbolTable.map (fun info ->
            { info with import_type = field_type info.import_type }) import
   in
   let tenv =
      SymbolTable.map (fun ty ->
            field_type ty) types
   in
   let body = field_exp types body in
      { prog_file = file;
        prog_globals = globals;
        prog_decls = decls;
        prog_export = export;
        prog_import = import;
        prog_types = tenv;
        prog_body = body
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
