(*
 * Define a type substitution.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fc_config

open Fc_air
open Fc_air_ds

type subst = ty SymbolTable.t

(*
 * Type substitution.
 *)
let subst_empty = SymbolTable.empty

let subst_add = SymbolTable.add

let subst_remove = SymbolTable.remove

let subst_lookup = SymbolTable.find

let rec subst_type subst ty =
   match ty with
      TyUnit _
    | TyZero
    | TyInt _
    | TyField _
    | TyFloat _
    | TyDelayed
    | TyEnum _
    | TyStruct (Label _)
    | TyUEnum (Label _)
    | TyTag ->
         ty
    | TyFun (ty_vars, ty_res) ->
         let ty_vars = List.map (subst_type subst) ty_vars in
         let ty_res = subst_type subst ty_res in
            TyFun (ty_vars, ty_res)
    | TyPointer ty ->
         let ty = subst_type subst ty in
            TyPointer ty
    | TyArray (ty, a, aa) ->
         let ty = subst_type subst ty in
            TyArray (ty, a, aa)
    | TyRef (ty) ->
         let ty = subst_type subst ty in
            TyRef (ty)
    | TyUEnum (Fields fields) ->
         let fields = subst_uenum_fields subst fields in
            TyUEnum (Fields fields)
    | TyStruct (Fields fields) ->
         TyStruct (Fields (subst_struct_fields subst fields))
    | TyTuple fields ->
         TyTuple (subst_tuple_fields subst fields)
    | TyElide fields ->
         TyElide (subst_tuple_fields subst fields)
    | TyVar v ->
         (try subst_type subst (subst_lookup subst v) with
             Not_found ->
                ty)
    | TyApply (v, tyl) ->
         TyApply (v, List.map (subst_type subst) tyl)
    | TyLambda (vars, ty) ->
         let subst = List.fold_left subst_remove subst vars in
            TyLambda (vars, subst_type subst ty)
    | TyAll (vars, ty) ->
         let subst = List.fold_left subst_remove subst vars in
            TyAll (vars, subst_type subst ty)

and subst_ty_opt subst = function
   Some ty -> Some (subst_type subst ty)
 | None -> None

and subst_tuple_fields subst info =
   let { tuple_fields = fields; tuple_size = size } = info in
   let fields =
      List.map (fun (ty, e) ->
            subst_type subst ty, e) fields
   in
      { tuple_fields = fields; tuple_size = size }

and subst_uenum_fields subst info =
   let { uenum_fields = fields; uenum_size = size } = info in
   let fields =
      SymbolTable.map (fun (ty_opt, label) ->
            subst_ty_opt subst ty_opt, label) fields
   in
      { uenum_fields = fields; uenum_size = size }

and subst_struct_fields subst info =
   let { struct_fields = fields } = info in
   let fields =
      SymbolTable.map (fun field ->
            { field with field_type = subst_type subst field.field_type }) fields
   in
      { info with struct_fields = fields }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
