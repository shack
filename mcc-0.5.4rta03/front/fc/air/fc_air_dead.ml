(*
 * Deadcode elimination.
 * We want to eliminate useless arguments too,
 * so we first construct a dataflow graph, then calculate
 * the fixpoint.  Also, we try to eliminate useless types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Symbol

open Fc_config

open Fc_air
open Fc_air_ds
open Fc_air_exn
open Fc_air_pos
open Fc_air_type

module Pos = MakePos (struct let name = "Fc_air_dead" end)
open Pos

(*
 * Live set is a symbol set.
 *)
module LiveSet = SymbolSet
module FunTable = SymbolTable

(************************************************************************
 * LIVENESS INITIALIZATION
 ************************************************************************)

(*
 * Add all nested functions to make sure we keep labeled functions.
 *)
let rec live_init decls live nest e =
   match dest_exp_core e with
      LetAtom (_, _, _, e)
    | LetUnop (_, _, _, _, e)
    | LetBinop (_, _, _, _, _, e)
    | LetExtCall (_, _, _, _, _, _, e)
    | LetMalloc (_, _, e)
    | LetSubscript (_, _, _, _, e)
    | LetVar (_, _, _, e)
    | LetAddrOfVar (_, _, _, e)
    | SetVar (_, _, _, e)
    | Debug (_, e)
    | LetApply (_, _, _, _, e)
    | SetSubscript (_, _, _, _, e)
    | Memcpy (_, _, _, e) ->
         live_init decls live nest e

    | Raise _
    | Return _
    | TailCall _ ->
         live

    | Try (e1, _, e2)
    | IfThenElse (_, e1, e2) ->
         live_init decls (live_init decls live nest e1) nest e2

    | LetFuns (fundefs, e) ->
         let live =
            List.fold_left (fun live (f, _, gflag, _, _, e) ->
                  let nest = f :: nest in
                  let live =
                     if SymbolTable.mem decls f then
                        List.fold_left LiveSet.add live nest
                     else
                        live
                  in
                     live_init decls live nest e) live fundefs
         in
            live_init decls live nest e

let live_init decls e =
   live_init decls LiveSet.empty [] e

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Array index.
 *)
let live_array_index live = function
   ArrayVar v -> LiveSet.add live v
 | ArrayInt _ -> live

(*
 * Live expression.
 *)
let rec live_aexp live = function
   AInt _ ->
      live
 | AVar v ->
      LiveSet.add live v
 | APlus (a1, a2)
 | AMinus (a1, a2)
 | ATimes (a1, a2)
 | AMax (a1, a2) ->
      live_aexp (live_aexp live a1) a2
 | ARoundUp (a, _) ->
      live_aexp live a

(*
 * Collect uses of the type.
 *)
let rec live_type live = function
   TyUnit _
 | TyDelayed
 | TyTag
 | TyZero
 | TyInt _
 | TyField _
 | TyFloat _
 | TyVar _
 | TyEnum _
 | TyUEnum (Label _)
 | TyStruct (Label _) ->
      live
 | TyPointer ty
 | TyRef ty ->
      live_type live ty
 | TyArray (ty, index1, index2) ->
      let live = live_array_index live index1 in
         live_type (live_array_index live index2) ty
 | TyFun (ty_args, ty_res) ->
      let live = List.fold_left live_type live ty_args in
         live_type live ty_res
 | TyLambda (_, ty)
 | TyAll (_, ty) ->
      live_type live ty
 | TyApply (v, tyl) ->
      let live = List.fold_left live_type live tyl in
         LiveSet.add live v
 | TyTuple { tuple_fields = fields; tuple_size = size }
 | TyElide { tuple_fields = fields; tuple_size = size } ->
      live_tuple_fields (live_aexp live size) fields
 | TyUEnum (Fields { uenum_fields = fields; uenum_size = size }) ->
      live_uenum_fields (live_aexp live size) fields
 | TyStruct (Fields { struct_fields = fields; struct_size = size }) ->
      live_struct_fields (live_aexp live size) fields

and live_type_opt live = function
   Some ty -> live_type live ty
 | None -> live

and live_tuple_fields live = function
   (ty, size) :: fields ->
      live_tuple_fields (live_type (live_aexp live size) ty) fields
 | [] ->
      live

and live_uenum_fields live fields =
   SymbolTable.fold (fun live _ (ty_opt, label) ->
         live_type_opt (LiveSet.add live label) ty_opt) live fields

and live_struct_fields live fields =
   SymbolTable.fold (fun live _ { field_type = ty; field_offset = offset } ->
         live_aexp (live_type live ty) offset) live fields

(*
 * Calculate uses for each type def.
 *)
let live_types types =
   SymbolTable.map (live_type LiveSet.empty) types

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Variable.
 *)
let live_var live v =
   LiveSet.add live v

(*
 * Atoms.
 *)
let live_atom live = function
   AtomVar v -> live_var live v
 | _ -> live

let live_atom_opt live = function
   Some a -> live_atom live a
 | None -> live

let live_atoms live atoms =
   List.fold_left live_atom live atoms

(*
 * Global declarations keep all their arguments.
 *)
let live_global_declare export live gflag f vars =
   match gflag with
      VarGlobalClass ->
         if SymbolTable.mem export f then
            List.fold_left LiveSet.add (LiveSet.add live f) vars
         else
            live
    | VarStaticClass
    | VarLocalClass ->
         live

let live_cont_declare live gflag f vars =
   match gflag with
      VarStaticClass
    | VarGlobalClass ->
         List.fold_left LiveSet.add (LiveSet.add live f) vars
    | VarLocalClass ->
         live

(*
 * Add types for live function arguments.
 *)
let rec live_args live vars ty =
   match ty with
      TyFun (ty_args, ty_res) ->
         let live =
            List.fold_left2 (fun live v ty ->
                  if LiveSet.mem live v then
                     live_type live ty
                  else
                     live) live vars ty_args
         in
            live_type live ty_res
    | ty ->
         live_type live ty

(*
 * Calculate defs/uses along each branch to a LetClosure or TailCall.
 *)
let rec live_exp export funs live e =
   match dest_exp_core e with
      LetFuns (fundefs, e) ->
         live_funs_exp export funs live fundefs e
    | LetAtom (v, ty, a, e) ->
         live_atom_exp export funs live v ty a e
    | LetUnop (v, ty, op, a, e) ->
         live_unop_exp export funs live v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         live_binop_exp export funs live v ty op a1 a2 e
    | LetExtCall (v, ty, s, _, ty', args, e) ->
         live_extcall_exp export funs live v ty s ty' args e
    | TailCall (f, args) ->
         live_tailcall_exp export funs live f args
    | LetMalloc (v, a, e) ->
         live_malloc_exp export funs live v a e
    | IfThenElse (a, e1, e2) ->
         live_if_exp export funs live a e1 e2
    | LetSubscript (v, ty, v2, a3, e) ->
         live_subscript_exp export funs live v ty v2 a3 e
    | SetSubscript (v1, a2, ty, a3, e) ->
         live_set_subscript_exp export funs live v1 a2 ty a3 e
    | Memcpy (v1, a1, v2, e) ->
         live_memcpy_exp export funs live v1 a1 v2 e
    | LetVar (v, ty, a_opt, e) ->
         live_var_exp export funs live v ty a_opt e
    | LetAddrOfVar (v1, ty, v2, e) ->
         live_addr_of_var_exp export funs live v1 ty v2 e
    | SetVar (v, ty, a, e) ->
         live_set_var_exp export funs live v ty a e
    | Debug (info, e) ->
         live_debug_exp export funs live info e
    | Raise a ->
         live_atom live a
    | Return (f, a) ->
         live_atom (live_var live f) a
    | Try (e1, v, e2) ->
         live_try_exp export funs live e1 v e2
    | LetApply (v, ty, f, args, e) ->
         live_apply_exp export funs live v ty f args e

(*
 * Function definitions.
 * Calculate fixpoint based on live functions.
 *)
and live_funs_exp export funs live fundefs e =
   let live = live_exp export funs live e in
   let rec fixpoint live fundefs =
      let live', fundefs =
         List.fold_left (fun (live, fundefs) fund ->
               let f, _, gflag, ty, vars, e = fund in
               let live = live_global_declare export live gflag f vars in
                  if LiveSet.mem live f then
                     let live = live_cont_declare live gflag f vars in
                     let live = live_args live vars ty in
                     let live = live_exp export funs live e in
                        live, fundefs
                  else
                     live, fund :: fundefs) (live, []) fundefs
      in
         if fundefs <> [] && LiveSet.cardinal live' <> LiveSet.cardinal live then
            fixpoint live' fundefs
         else
            live'
   in
      fixpoint live fundefs

(*
 * Operator.
 * Add the def.
 *)
and live_atom_exp export funs live v ty a e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

and live_unop_exp export funs live v ty op a e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

and live_binop_exp export funs live v ty op a1 a2 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a1 in
         let live = live_atom live a2 in
         let live = live_type live ty in
            live
      else
         live

(*
 * Don't ever delete extcalls--they may have side-effects.
 *)
and live_extcall_exp export funs live v ty s ty' args e =
   let live = live_exp export funs live e in
   let live = live_var live v in
   let live = live_type live ty' in
   let live = live_atoms live args in
   let live = live_type live ty in
      live

(*
 * Don't remove applications because they may have side-effects.
 *)
and live_apply_exp export funs live v ty f args e =
   let live = live_exp export funs live e in
   let live = live_var live v in
   let live = live_var live f in
   let live = live_atoms live args in
   let live = live_type live ty in
      live

(*
 * Closure.
 * Some of the arguments may have been dropped.
 *)
and live_closure_exp export funs live v f args e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         live_tailcall_exp export funs live f args
      else
         live

(*
 * Tailcall.
 * Some of the arguments may have been dropped.
 *)
and live_tailcall_exp export funs live f args =
   let live = live_var live f in
      try
         let vars = FunTable.find funs f in
         let rec collect live vars args =
            match vars, args with
               v :: vars, a :: args ->
                  let live =
                     if LiveSet.mem live v then
                        live_atom live a
                     else
                        live
                  in
                     collect live vars args
             | _ ->
                  live
         in
            collect live vars args
      with
         Not_found ->
            live_atoms live args

(*
 * Memory allocations.
 *)
and live_tuple_exp export funs live v ty args e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atoms live args in
         let live = live_type live ty in
            live
      else
         live

and live_malloc_exp export funs live v a e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_atom live a in
            live
      else
         live

(*
 * Exceptions.
 *)
and live_try_exp export funs live e1 v e2 =
   let live = live_exp export funs live e2 in
      live_exp export funs live e1

(*
 * Conditional.
 *)
and live_if_exp export funs live a e1 e2 =
   let live = live_exp export funs live e1 in
   let live = live_exp export funs live e2 in
      live_atom live a

(*
 * Subscripting.
 * We can't eliminate side-effects.
 *)
and live_subscript_exp export funs live v ty v2 a3 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_var live v2 in
         let live = live_atom live a3 in
            live
      else
         live

and live_set_subscript_exp export funs live v1 a2 ty a3 e =
   let live = live_exp export funs live e in
   let live = live_atom live a3 in
   let live = live_type live ty in
   let live = live_atom live a2 in
   let live = live_var live v1 in
      live

and live_memcpy_exp export funs live v1 a1 v2 e =
   let live = live_exp export funs live e in
   let live = live_var live v1 in
   let live = live_atom live a1 in
   let live = live_var live v2 in
      live

(*
 * Variable operations.
 *)
and live_var_exp export funs live v ty a_opt e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v then
         let live = live_type live ty in
         let live = live_atom_opt live a_opt in
            live
      else
         live

and live_addr_of_var_exp export funs live v1 ty v2 e =
   let live = live_exp export funs live e in
      if LiveSet.mem live v1 then
         let live = live_type live ty in
         let live = live_var live v2 in
            live
      else
         live

and live_set_var_exp export funs live v ty a e =
   let live = live_exp export funs live e in
   let live = live_atom live a in
   let live = live_type live ty in
   let live = live_var live v in
      live

(*
 * Debugging variables are preserved.
 *)
and live_debug_exp export funs live info e =
   let live = live_exp export funs live e in
      match info with
         DebugString _ ->
            live
       | DebugContext (_, vars) ->
            List.fold_left (fun live (_, ty, v) ->
                  live_var (live_type live ty) v) live vars

(************************************************************************
 * FIXPOINT
 ************************************************************************)

(*
 * Calculate the live variable fixpoint.
 *)
let fixpoint f live =
   let rec loop n live =
      let live = f live in
      let m = LiveSet.cardinal live in
         if m <> n then
            loop m live
         else
            live
   in
      loop (LiveSet.cardinal live) live

(*
 * Liveness for a program.
 *)
let live_prog_step funs prog live =
   let { prog_globals = globals;
         prog_import = import;
         prog_export = export;
         prog_decls = decls;
         prog_types = types;
         prog_body = body
       } = prog
   in

   (* Calculate imports *)
   let live =
      SymbolTable.fold (fun live v { import_type = ty } ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live import
   in

   (* Calculate exports *)
   let live =
      SymbolTable.fold (fun live v { export_type = ty } ->
            let live = LiveSet.add live v in
               live_type live ty) live export
   in

   (* Calculate globals *)
   let live =
      SymbolTable.fold (fun live v (ty, _, _) ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live globals
   in

   (* Calculate decls *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live decls
   in

   (* Type definitions *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if LiveSet.mem live v then
               live_type live ty
            else
               live) live types
   in
      live_exp export funs live body

(*
 * Calculate the live sets.
 *)
let live_prog prog =
   let funs = FunTable.empty in
   let live = fixpoint (live_prog_step funs prog) (live_init prog.prog_decls prog.prog_body) in
      funs, live

(************************************************************************
 * DEADCODE ELIMINATION
 ************************************************************************)

(*
 * Eliminate dead fields from the frame.
 *)
let rec dead_type live ty =
   match ty with
      TyUnit _
    | TyDelayed
    | TyTag
    | TyZero
    | TyInt _
    | TyField _
    | TyFloat _
    | TyVar _
    | TyEnum _
    | TyUEnum (Label _)
    | TyStruct (Label _) ->
         ty
    | TyPointer ty ->
         TyPointer (dead_type live ty)
    | TyRef ty ->
         TyRef (dead_type live ty)
    | TyArray (ty, lower, upper) ->
         TyArray (dead_type live ty, lower, upper)
    | TyFun (ty_args, ty_res) ->
         let ty_args = List.map (dead_type live) ty_args in
         let ty_res = dead_type live ty_res in
            TyFun (ty_args, ty_res)
    | TyLambda (vars, ty) ->
         TyLambda (vars, dead_type live ty)
    | TyAll (vars, ty) ->
         TyAll (vars, dead_type live ty)
    | TyApply (v, tyl) ->
         let tyl = List.map (dead_type live) tyl in
            TyApply (v, tyl)
    | TyTuple { tuple_fields = fields; tuple_size = size } ->
         let fields = List.map (fun (ty, size) -> dead_type live ty, size) fields in
            TyTuple { tuple_fields = fields; tuple_size = size }
    | TyElide { tuple_fields = fields; tuple_size = size } ->
         let fields = List.map (fun (ty, size) -> dead_type live ty, size) fields in
            TyElide { tuple_fields = fields; tuple_size = size }
    | TyUEnum (Fields { uenum_fields = fields; uenum_size = size }) ->
         let fields =
            SymbolTable.map (fun (ty_opt, label) ->
                  dead_type_opt live ty_opt, label) fields
         in
            TyUEnum (Fields { uenum_fields = fields; uenum_size = size })
    | TyStruct (Fields { struct_fields = fields; struct_root = root; struct_size = size }) ->
         let fields =
            SymbolTable.map (fun field ->
                  { field with field_type = dead_type live field.field_type }) fields
         in
            TyStruct (Fields { struct_fields = fields; struct_root = root; struct_size = size })

and dead_type_opt live = function
   Some ty -> Some (dead_type live ty)
 | None -> None

(*
 * Eliminate dead arguments.
 *)
let dead_apply funs live pos f args =
   try
      let vars = FunTable.find funs f in
      let rec collect args' vars2 args2 =
         match vars2, args2 with
            v :: vars2, a :: args2 ->
               let args' =
                  if LiveSet.mem live v then
                     a :: args'
                  else
                     args'
               in
                  collect args' vars2 args2
          | _ ->
               List.rev args'
      in
         collect [] vars args
   with
      Not_found ->
         args

(*
 * Eliminate dead expressions.
 *)
let rec dead_exp tenv funs live e =
   let pos = string_pos "dead_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (fundefs, e) ->
            dead_funs_exp tenv funs live pos loc fundefs e
       | LetAtom (v, ty, a, e) ->
            dead_atom_exp tenv funs live pos loc v ty a e
       | LetUnop (v, ty, op, a, e) ->
            dead_unop_exp tenv funs live pos loc v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            dead_binop_exp tenv funs live pos loc v ty op a1 a2 e
       | LetExtCall (v, ty, s, b, ty', args, e) ->
            dead_extcall_exp tenv funs live pos loc v ty s b ty' args e
       | TailCall (f, args) ->
            dead_tailcall_exp tenv funs live pos loc f args
       | LetMalloc (v, a, e) ->
            dead_malloc_exp tenv funs live pos loc v a e
       | IfThenElse (a, e1, e2) ->
            dead_if_exp tenv funs live pos loc a e1 e2
       | LetSubscript (v, ty, v2, a3, e) ->
            dead_subscript_exp tenv funs live pos loc v ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            dead_set_subscript_exp tenv funs live pos loc v1 a2 ty a3 e
       | Memcpy (v1, a1, v2, e) ->
            dead_memcpy_exp tenv funs live pos loc v1 a1 v2 e
       | LetVar (v, ty, a_opt, e) ->
            dead_var_exp tenv funs live pos loc v ty a_opt e
       | LetAddrOfVar (v1, ty, v2, e) ->
            dead_addr_of_var_exp tenv funs live pos loc v1 ty v2 e
       | SetVar (v, ty, a, e) ->
            dead_set_var_exp tenv funs live pos loc v ty a e
       | Debug (info, e) ->
            dead_debug_exp tenv funs live pos loc info e
       | Raise _
       | Return _ ->
            e
       | LetApply (v, ty, f, args, e) ->
            dead_apply_exp tenv funs live pos loc v ty f args e
       | Try (e1, v, e2) ->
            dead_try_exp tenv funs live pos loc e1 v e2


(*
 * Dead functions.
 * Some functions may be omitted.
 *)
and dead_funs_exp tenv funs live pos loc fundefs e =
   let funs =
      List.fold_left (fun funs (f, _, _, _, vars, _) ->
            FunTable.add funs f vars) funs fundefs
   in
   let e = dead_exp tenv funs live e in
   let fundefs =
      List.fold_left (fun fundefs (f, line, gflag, ty, vars, e) ->
            if LiveSet.mem live f then
               let all_vars, ty_args, ty_res = dest_fun_type tenv pos ty in
               let ty_args, vars =
                  List.fold_left2 (fun (ty_args, vars) ty v ->
                        if LiveSet.mem live v then
                           dead_type live ty :: ty_args, v :: vars
                        else
                           ty_args, vars) ([], []) ty_args vars
               in
               let vars = List.rev vars in
               let ty = TyAll (all_vars, TyFun (List.rev ty_args, ty_res)) in
               let e = dead_exp tenv funs live e in
               let fund = f, line, gflag, ty, vars, e in
                  fund :: fundefs
            else
               fundefs) [] fundefs
   in
      if fundefs = [] then
         e
      else
         make_exp loc (LetFuns (List.rev fundefs, e))

(*
 * Dead operator.
 *)
and dead_atom_exp tenv funs live pos loc v ty a e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetAtom (v, dead_type live ty, a, e))
      else
         e

and dead_unop_exp tenv funs live pos loc v ty op a e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetUnop (v, dead_type live ty, op, a, e))
      else
         e

and dead_binop_exp tenv funs live pos loc v ty op a1 a2 e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetBinop (v, dead_type live ty, op, a1, a2, e))
      else
         e

and dead_extcall_exp tenv funs live pos loc v ty s b ty' args e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetExtCall (v, dead_type live ty, s, b, dead_type live ty', args, e))
      else
         e

and dead_apply_exp tenv funs live pos loc v ty f args e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetApply (v, dead_type live ty, f, args, e))
      else
         e

(*
 * Tailcall.
 * Again, be careful about dead arguments.
 *)
and dead_tailcall_exp tenv funs live pos loc f args =
   let args = dead_apply funs live pos f args in
      make_exp loc (TailCall (f, args))

(*
 * Memory allocation.
 *)
and dead_malloc_exp tenv funs live pos loc v a e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetMalloc (v, a, e))
      else
         e

(*
 * Conditional.
 *)
and dead_if_exp tenv funs live pos loc a e1 e2 =
   let e1 = dead_exp tenv funs live e1 in
   let e2 = dead_exp tenv funs live e2 in
      make_exp loc (IfThenElse (a, e1, e2))

and dead_try_exp tenv funs live pos loc e1 v e2 =
   let e1 = dead_exp tenv funs live e1 in
   let e2 = dead_exp tenv funs live e2 in
      make_exp loc (Try (e1, v, e2))

(*
 * Subscripting.
 *)
and dead_subscript_exp tenv funs live pos loc v ty v2 a3 e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetSubscript (v, dead_type live ty, v2, a3, e))
      else
         e

and dead_set_subscript_exp tenv funs live pos loc v1 a2 ty a3 e =
   let e = dead_exp tenv funs live e in
      make_exp loc (SetSubscript (v1, a2, dead_type live ty, a3, e))

and dead_memcpy_exp tenv funs live pos loc v1 a1 v2 e =
   let e = dead_exp tenv funs live e in
      make_exp loc (Memcpy (v1, a1, v2, e))

(*
 * Variable operations.
 *)
and dead_var_exp tenv funs live pos loc v ty a_opt e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v then
         make_exp loc (LetVar (v, dead_type live ty, a_opt, e))
      else
         e

and dead_addr_of_var_exp tenv funs live pos loc v1 ty v2 e =
   let e = dead_exp tenv funs live e in
      if LiveSet.mem live v1 then
         make_exp loc (LetAddrOfVar (v1, dead_type live ty, v2, e))
      else
         e

and dead_set_var_exp tenv funs live pos loc v ty a e =
   let e = dead_exp tenv funs live e in
      make_exp loc (SetVar (v, dead_type live ty, a, e))

(*
 * Debug.
 *)
and dead_debug_exp tenv funs live pos loc info e =
   let e = dead_exp tenv funs live e in
   let info =
      match info with
         DebugString _ ->
            info
       | DebugContext (line, vars) ->
            let vars =
               List.map (fun (v1, ty, v2) ->
                     v1, dead_type live ty, v2) vars
            in
               DebugContext (line, vars)
   in
      make_exp loc (Debug (info, e))

(************************************************************************
 * GLOBAL FUNCTION
 ************************************************************************)

(*
 * Dead program.
 *)
let dead_prog prog =
   let { prog_file = file;
         prog_globals = globals;
         prog_decls = decls;
         prog_export = export;
         prog_import = import;
         prog_types = types;
         prog_body = body
       } = prog
   in

   (* Initial live variables *)
   let funs, live = live_prog prog in

   (* Prune imports *)
   let import =
      SymbolTable.fold (fun import v info ->
            if LiveSet.mem live v then
               let info = { info with import_type = dead_type live info.import_type } in
                  SymbolTable.add import v info
            else
               import) SymbolTable.empty import
   in

   (* Prune globals *)
   let globals =
      SymbolTable.fold (fun globals v (ty, b, init) ->
            if LiveSet.mem live v then
               SymbolTable.add globals v (dead_type live ty, b, init)
            else
               globals) SymbolTable.empty globals
   in

   (* Prune decls *)
   let decls =
      SymbolTable.fold (fun decls v ty ->
            if LiveSet.mem live v then
               SymbolTable.add decls v (dead_type live ty)
            else
               decls) SymbolTable.empty decls
   in

   (* Prune types *)
   let types =
      SymbolTable.fold (fun types v ty ->
            if LiveSet.mem live v then
               SymbolTable.add types v (dead_type live ty)
            else
               types) SymbolTable.empty types
   in

   (* Prune initialization *)
   let body = dead_exp types funs live body in
      { prog_file = file;
        prog_globals = globals;
        prog_decls = decls;
        prog_export = export;
        prog_import = import;
        prog_types = types;
        prog_body = body
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
