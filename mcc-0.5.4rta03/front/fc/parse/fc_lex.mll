(*
 * Lexer for the simple grammar.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

{
open Format

open Symbol
open Location

open Fc_config
open Fc_parse
open Fc_parse_type
open Fc_parse_state
open Fc_parse_exn

(*
 * Numbers.
 *)
let eight64   = Int64.of_int 8
let ten64     = Int64.of_int 10
let sixteen64 = Int64.of_int 16

(*
 * File position.
 *)
let current_line = ref 1
let current_schar = ref 0

(*
 * Advance a line.
 *)
let set_next_line lexbuf =
   incr current_line;
   current_schar := Lexing.lexeme_end lexbuf

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
   let line = !current_line in
   let schar = Lexing.lexeme_start lexbuf - !current_schar in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let file = current_file () in
   let pos = create_loc file line schar line echar in
      set_current_position pos;
      pos

let set_line_number lexbuf line file =
   let file = Symbol.add file in
   let _ = current_line := line in
   let _ = current_schar := Lexing.lexeme_end lexbuf in
   let pos = create_loc file line 0 line 0 in
      set_current_position pos

(*
 * Read a number from the string.
 *)
let zero_code = Char.code '0'

let string_of_int_array a =
   let len = Array.length a in
   let s = String.create len in
      for i = 0 to pred len do
         s.[i] <- Char.chr (a.(i) land 0xff)
      done;
      s

let int_of_line s =
   let len = String.length s in
   let rec loop i j =
      if i = len then
         j
      else
         let c = s.[i] in
            match c with
               '0'..'9' -> loop (succ i) (j * 10 + (Char.code c - zero_code))
              | _ -> loop (succ i) j
   in
      loop 0 0

(*
 * Provide a buffer for building strings.
 *)
let string_start = ref (0, 0)
let stringbuf = ref []

let string_add_int i =
   stringbuf := i :: !stringbuf

let string_add_char c =
   string_add_int (Char.code c)

let pop_string lexbuf =
   let sline, schar = !string_start in
   let eline = !current_line in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let pos = create_loc (current_file ()) sline schar eline echar in
   let s = Array.of_list (List.rev !stringbuf) in
      stringbuf := [];
      s, pos

let set_string_start lexbuf =
   string_start := !current_line, Lexing.lexeme_start lexbuf - !current_schar

(*
 * Integer conversion functions.
 * All lexing integers are computed in Int64.
 *)
let int64_of_radix radix s =
   let len = String.length s in
   let add_char x c1 c2 off =
      let i = Char.code c1 - Char.code c2 + off in
         Int64.add (Int64.mul x radix) (Int64.of_int i)
   in
   let rec loop pre signed x i =
      if i = len then
         let pre = FCParam.int_precision pre in
            Rawint.of_int64 pre signed x
      else
         let c = s.[i] in
            match s.[i] with
               'l' | 'L' -> loop LongPrecision signed x (succ i)
             | 'u' | 'U' -> loop pre false x (succ i)
             | 'a'..'f' -> loop pre signed (add_char x c 'a' 10) (succ i)
             | 'A'..'F' -> loop pre signed (add_char x c 'A' 10) (succ i)
             | '0'..'9' -> loop pre signed (add_char x c '0' 0)  (succ i)
             | _ -> loop pre signed x (succ i)
   in
      loop NormalPrecision true Int64.zero 0

let int64_of_decimal = int64_of_radix (Int64.of_int 10)
let int64_of_octal   = int64_of_radix (Int64.of_int 8)
let int64_of_hex     = int64_of_radix (Int64.of_int 16)

(*
 * Same for normal integers.
 *)
let int_of_radix radix s i =
   let len = String.length s in
   let add_char x c1 c2 off =
      let i = Char.code c1 - Char.code c2 + off in
         (x * radix) + i
   in
   let rec loop x i =
      if i = len then
         x
      else
         let c = s.[i] in
            match s.[i] with
               'a'..'f' -> loop (add_char x c 'a' 10) (succ i)
             | 'A'..'F' -> loop (add_char x c 'A' 10) (succ i)
             | '0'..'9' -> loop (add_char x c '0' 0)  (succ i)
             | _ -> loop x (succ i)
   in
      loop 0 i

let int_of_decimal = int_of_radix 10
let int_of_octal   = int_of_radix 8
let int_of_hex     = int_of_radix 16

(*
 * Extract the char constant.
 *)
let lex_char s =
   match s.[0] with
      '\\' ->
         (match s.[1] with
             '0'..'7' -> int_of_octal s 2
           | 'x' | 'X' -> int_of_hex s 2
           | 'n' ->  Char.code '\n'
           | 't' ->  Char.code '\t'
           | 'v' ->  Char.code '\011'
           | 'b' ->  Char.code '\b'
           | 'r' ->  Char.code '\r'
           | 'f' ->  Char.code '\012'
           | 'a' ->  Char.code '\007'
           | '\\' -> Char.code '\\'
           | '\'' -> Char.code '\''
           | '"' ->  Char.code '"'
           | c ->    Char.code c)
    | c ->
        Char.code c

(*
 * Floating-point constants.
 * the constant may be terminated by a 'f' or 'l'.
 * 'f' means float, 'l' means long double.
 *)
let lex_float s =
   let pre, x =
      let len = String.length s in
         if len = 0 then
            NormalPrecision, Float80.zero
         else
            let pre, s =
               match s.[pred len] with
                  'f' | 'F' -> ShortPrecision, String.sub s 0 (pred len)
                | 'l' | 'L' -> LongPrecision, String.sub s 0 (pred len)
                | _ -> NormalPrecision, s
            in
               pre, Float80.of_string s
   in
   let pre = FCParam.float_precision pre in
      Rawfloat.of_float80 pre x
   
(*
 * Keyword table.
 *)
let special =
   ["auto",     (fun pos v -> TokAuto pos);
    "break",    (fun pos v -> TokBreak pos);
    "case",     (fun pos v -> TokCase pos);
    "char",     (fun pos v -> TokTypeChar pos);
    "const",    (fun pos v -> TokConst pos);
    "continue", (fun pos v -> TokContinue pos);
    "default",  (fun pos v -> TokDefault pos);
    "do",       (fun pos v -> TokDo pos);
    "double",   (fun pos v -> TokTypeDouble pos);
    "else",     (fun pos v -> TokElse pos);
    "enum",     (fun pos v -> TokEnum pos);
    "extern",   (fun pos v -> TokExtern pos);
    "float",    (fun pos v -> TokTypeFloat pos);
    "for",      (fun pos v -> TokFor pos);
    "goto",     (fun pos v -> TokGoto pos);
    "if",       (fun pos v -> TokIf pos);
    "int",      (fun pos v -> TokTypeInt pos);
    "long",     (fun pos v -> TokLong pos);
    "register", (fun pos v -> TokRegister pos);
    "return",   (fun pos v -> TokReturn pos);
    "short",    (fun pos v -> TokShort pos);
    "signed",   (fun pos v -> TokSigned pos);
    "static",   (fun pos v -> TokStatic pos);
    "struct",   (fun pos v -> TokStruct pos);
    "switch",   (fun pos v -> TokSwitch pos);
    "typedef",  (fun pos v -> TokTypedef pos);
    "union",    (fun pos v -> TokUnion pos);
    "unsigned", (fun pos v -> TokUnsigned pos);
    "void",     (fun pos v -> TokTypeVoid pos);
    "volatile", (fun pos v -> TokVolatile pos);
    "while",    (fun pos v -> TokWhile pos);

    "(",        (fun pos v -> TokLeftParen pos);
    ")",        (fun pos v -> TokRightParen pos);
    "[",        (fun pos v -> TokLeftBrack pos);
    "]",        (fun pos v -> TokRightBrack pos);
    "{",        (fun pos v -> TokLeftBrace pos);
    "}",        (fun pos v -> TokRightBrace pos);

    ";",        (fun pos v -> TokSemi pos);
    ",",        (fun pos v -> TokComma pos);

    "=",        (fun pos v -> TokEq pos);
    "+=",       (fun pos v -> TokOp1 (pos, v));
    "-=",       (fun pos v -> TokOp1 (pos, v));
    "*=",       (fun pos v -> TokOp1 (pos, v));
    "/=",       (fun pos v -> TokOp1 (pos, v));
    "%=",       (fun pos v -> TokOp1 (pos, v));
    "&=",       (fun pos v -> TokOp1 (pos, v));
    "^=",       (fun pos v -> TokOp1 (pos, v));
    "|=",       (fun pos v -> TokOp1 (pos, v));
    "<<=",      (fun pos v -> TokOp1 (pos, v));
    ">>=",      (fun pos v -> TokOp1 (pos, v));

    "?",        (fun pos v -> TokQuest  pos);
    ":",        (fun pos v -> TokColon  pos);

    "&&",       (fun pos v -> TokAnd pos);
    "||",       (fun pos v -> TokOr  pos);

    "|",        (fun pos v -> TokOp4 (pos, v));
    "^",        (fun pos v -> TokOp5 (pos, v));
    "&",        (fun pos v -> TokAddr (pos, v));

    "==",       (fun pos v -> TokOp7 (pos, v));
    "!=",       (fun pos v -> TokOp7 (pos, v));

    "<=",       (fun pos v -> TokOp8 (pos, v));
    ">=",       (fun pos v -> TokOp8 (pos, v));
    "<",        (fun pos v -> TokOp8 (pos, v));
    ">",        (fun pos v -> TokOp8 (pos, v));

    "<<",       (fun pos v -> TokOp9 (pos, v));
    ">>",       (fun pos v -> TokOp9 (pos, v));

    "+",        (fun pos v -> TokOp10 (pos, v));
    "-",        (fun pos v -> TokOp10 (pos, v));

    "*",        (fun pos v -> TokStar pos);
    "%",        (fun pos v -> TokOp11 (pos, v));
    "/",        (fun pos v -> TokOp11 (pos, v));

    "!",        (fun pos v -> TokPreOp12 (pos, v));
    "~",        (fun pos v -> TokPreOp12 (pos, v));
    "++",       (fun pos v -> TokPrePostOp12 (pos, v));
    "--",       (fun pos v -> TokPrePostOp12 (pos, v));
    "sizeof",   (fun pos v -> TokSizeof pos);

    "->",       (fun pos v -> TokOp13 (pos, v));
    ".",        (fun pos v -> TokDot (pos, v));

    "...",	(fun pos v -> TokElide pos);

    "__poly",     (fun pos v -> TokPoly pos);
    "__try",      (fun pos v -> TokTry pos);
    "__catch",    (fun pos v -> TokCatch pos);
    "__finally",  (fun pos v -> TokFinally pos);
    "__operator", (fun pos v -> TokOperator pos);
    "__tuple",    (fun pos v -> TokTuple pos);
    "__throw",    (fun pos v -> TokRaise pos)]

let extra_special =
   ["poly",     (fun pos v -> TokPoly pos);
    "try",      (fun pos v -> TokTry pos);
    "catch",    (fun pos v -> TokCatch pos);
    "finally",  (fun pos v -> TokFinally pos);
    "operator", (fun pos v -> TokOperator pos);
    "tuple",    (fun pos v -> TokTuple pos);
    "throw",    (fun pos v -> TokRaise pos)]

let symtab =
   let table =
      List.fold_left (fun table (name, f) ->
         SymbolTable.add table (Symbol.add name) f) SymbolTable.empty special
   in
   let table =
      if !Fc_parse_state.fc_extensions then
         List.fold_left (fun table (name, f) ->
            SymbolTable.add table (Symbol.add name) f) table extra_special
      else
         table
   in
      table

(*
 * Look up the string, and return a symbol if
 * the lookup fails.
 *)
let lex_symbol s pos =
   let sym = Symbol.add s in
      try (SymbolTable.find symtab sym) pos sym with
         Not_found ->
            if is_type sym then
               TokTypeId (pos, sym)
            else
               TokId (pos, sym)

(*
 * Look up the operator.
 * Syntax error if the lookup fails.
 *)
let lex_operator s pos =
   let sym = Symbol.add s in
      try (SymbolTable.find symtab sym) pos sym with
         Not_found ->
            raise (ParseError (pos, "illegal operator"))
}

let white = [' ' '\t' '\r' '\012']+

(*
 * Identifiers and keywords.
 *)
let name_prefix = ['_' 'A'-'Z' 'a'-'z']
let name_suffix = ['_' 'A'-'Z' 'a'-'z' '0'-'9']
let name = name_prefix name_suffix *

(*
 * Operators.
 * cspecial: operators that are always single-character.
 * ospecial: normal infix operators.
 * especial: operators terminated with an '=' (either a relation or a assignment)
 * rspecial: Boolean operators
 *)

(* These operators are always single character *)
let cspecial = ['(' ')' '[' ']' '{' '}' ';' ',' '.' '?' ':' '~']
let ospecial = (['+' '-' '*' '/' '%' '&' '^' '|' '<' '>' '!' '='] | "<<" | ">>")
let especial = ospecial '='
let rspecial = "&&" | "||" | "--" | "++" | "->" | "..."
let special = cspecial | ospecial | especial | rspecial

(*
 * Numbers.
 *)
let decimal_char = ['0'-'9']
let octal_char = ['0'-'7']
let hex_char = ['0'-'9' 'a'-'f' 'A'-'F']

let decimal = ['1'-'9'] decimal_char*
let octal = '0' octal_char*
let hex = "0x" hex_char*
let number = decimal | octal
let float0 = ['0'-'9']+ '.' ['0'-'9']* (('e' | 'E') ('+' | '-')? '0'* decimal)?
let float1 = ['0'-'9']* '.' ['0'-'9']+ (('e' | 'E') ('+' | '-')? '0'* decimal)?
let float2 = ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)

let float_suffix = ['f' 'F' 'l' 'L']
let float_const = (float0 | float1 | float2) float_suffix?

let int_u_suffix = ['U' 'u']
let int_l_suffix = ['L' 'l']
let int_ll_suffix = "LL" | "Ll" | "lL" | "ll"
let int_l_or_ll_suffix = int_l_suffix | int_ll_suffix
let int_suffix = (int_l_or_ll_suffix int_u_suffix?) | (int_u_suffix int_l_or_ll_suffix?)
let decimal_const = decimal int_suffix?
let octal_const   = octal int_suffix?
let hex_const     = hex int_suffix?

(*
 * Character constants.
 *)
let char_octal   = "\\" octal_char octal_char? octal_char?
let char_hex     = "\\x" hex_char hex_char? hex_char?
let char_escape  = "\\" ['a'-'z' '\'' '\\' '"']
let char_normal  = [' '-'\255']
let char_cases   = char_octal | char_hex | char_escape | char_normal
let char_const   = "'" char_cases "'"

(*
 * String constants.
 * Be kind to DOS.
 *)
let string_break = "\\\n" | "\\\n\r"

(*
 * Polymorphic identifiers.
 *)
let poly_type    = "'" name

(*
 * Main lexer.
 *)
rule main = parse
   white
      { main lexbuf }
 | '\n'
      { set_next_line lexbuf; main lexbuf }

 | '#' white? number white? '"'
      { let line = int_of_line (Lexing.lexeme lexbuf) in
           set_string_start lexbuf;
           string lexbuf;
           skip_line lexbuf;
           let file, _ = pop_string lexbuf in
           let file = string_of_int_array file in
              set_line_number lexbuf line file;
              main lexbuf
      }
 | octal_const
      { let pos = set_lexeme_position lexbuf in
        let i = int64_of_octal (Lexing.lexeme lexbuf) in
           TokInt (pos, i)
      }
 | hex_const
      { let pos = set_lexeme_position lexbuf in
        let i = int64_of_hex (Lexing.lexeme lexbuf) in
           TokInt (pos, i)
      }
 | decimal_const
      { let pos = set_lexeme_position lexbuf in
        let i = int64_of_decimal (Lexing.lexeme lexbuf) in
           TokInt (pos, i)
      }
 | float_const
      { let pos = set_lexeme_position lexbuf in
        let x = lex_float (Lexing.lexeme lexbuf) in
           TokFloat (pos, x)
      }
 | name
      { let pos = set_lexeme_position lexbuf in
        let id = Lexing.lexeme lexbuf in
           lex_symbol id pos
      }

   (*
    * Comments.
    *)
 | "//" [^ '\n']* '\n'
      { set_next_line lexbuf; main lexbuf }
 | "/*"
      { comment lexbuf; main lexbuf }

   (*
    * Strings and chars.
    *)
 | "L\""
      { set_string_start lexbuf;
        string lexbuf;
        let s, pos = pop_string lexbuf in
           TokString (pos, LongPrecision, s)
      }

 | '"'
      { set_string_start lexbuf;
        string lexbuf;
        let s, pos = pop_string lexbuf in
           TokString (pos, NormalPrecision, s)
      }
 | 'L' char_const
      { let pos = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
        let len = String.length s in
        let s = String.sub s 2 (len - 2) in
        let c = lex_char s in
        let pre = FCParam.char_precision LongPrecision in
        let c = Rawint.of_int pre false c in
           TokChar (pos, c)
      }
 | char_const
      { let pos = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
        let len = String.length s in
        let s = String.sub s 1 (len - 1) in
        let c = lex_char s in
        let pre = FCParam.char_precision NormalPrecision in
        let c = Rawint.of_int pre false c in
           TokChar (pos, c)
      }
 | poly_type
      { let pos = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
        let sym = Symbol.add (String.sub s 1 (pred (String.length s))) in
           TokTypeVar (pos, sym)
      }

   (*
    * Operators.
    *)
 | special
      { let pos = set_lexeme_position lexbuf in
           lex_operator (Lexing.lexeme lexbuf) pos
      }

   (*
    * All other characters are a syntax error.
    *)
 | _
      { let pos = set_lexeme_position lexbuf in
           raise (ParseError (pos, Printf.sprintf "illegal char: '%s'"
              (String.escaped (Lexing.lexeme lexbuf))))
      }

 | eof
      { TokEof }

(*
 * Strings are delimited by double-quotes.
 * Chars may be escaped.
 *)
and string = parse
   '"' | eof
      { () }

   (* Escaped newlines are ignored *)
 | string_break
      { set_next_line lexbuf;
        string lexbuf
      }

   (* Not ANSI standard, but newlines are allowed inside a string *)
 | '\n'
      { set_next_line lexbuf;
        string_add_char '\n';
        string lexbuf
      }

   (* Other characters (including escape sequences) *)
 | char_cases
      { let c = lex_char (Lexing.lexeme lexbuf) in
        string_add_int c;
        string lexbuf
      }

   (* All other are literal *)
 | _
      { string_add_char (Lexing.lexeme lexbuf).[0];
        string lexbuf
      }

(*
 * Skip all chars to the eol.
 *)
and skip_line = parse
   '\n' { () }
 | eof { () }
 | _ { skip_line lexbuf }

(*
 * C-style comments are not nested.
 *)
and comment = parse
   "*/" | eof
      { () }
 | '\n'
      { set_next_line lexbuf;
        comment lexbuf
      }
 | _
      { comment lexbuf }

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
