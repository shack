(*
 * AST printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open Fc_config
open Fc_parse_type

(*
 * Print a program.
 *)
val pp_print_parse_char    : formatter -> int -> unit
val pp_print_parse_string  : formatter -> int array -> unit

val pp_print_type_status   : formatter -> type_status -> unit
val pp_print_storage_class : formatter -> storage_class -> unit
val pp_print_type          : formatter -> ty -> unit
val pp_print_expr          : formatter -> expr -> unit
val pp_print_debug         : formatter -> string -> expr -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
