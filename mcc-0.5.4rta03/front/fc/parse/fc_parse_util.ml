(*
 * Utilities over the syntax.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol
open Fc_parse_type

(*
 * Get the locition info.
 *)
let loc_of_pattern = function
   CharPattern (loc, _)
 | IntPattern (loc, _)
 | FloatPattern (loc, _)
 | StringPattern (loc, _, _)
 | VarPattern (loc, _, _, _)
 | StructPattern (loc, _)
 | EnumPattern (loc, _, _)
 | AsPattern (loc, _, _) ->
      loc

let loc_of_expr = function
   UnitExpr (loc, _, _)
 | CharExpr (loc, _)
 | IntExpr (loc, _)
 | FloatExpr (loc, _)
 | StringExpr (loc, _, _)
 | VarExpr (loc, _, _)

 | OpExpr (loc, _, _, _, _)
 | ProjectExpr (loc, _, _)
 | SizeofExpr (loc, _)
 | SizeofType (loc, _)
 | CastExpr (loc, _, _)

 | IfExpr (loc, _, _, _)
 | ForExpr (loc, _, _, _, _)
 | WhileExpr (loc, _, _)
 | DoExpr (loc, _, _)
 | TryExpr (loc, _, _, _)
 | SwitchExpr (loc, _, _)
 | LabelExpr (loc, _)
 | CaseExpr (loc, _)
 | DefaultExpr loc
 | ReturnExpr (loc, _)
 | RaiseExpr (loc, _)
 | BreakExpr loc
 | ContinueExpr loc
 | GotoExpr (loc, _)

 | SeqExpr (loc, _)
 | PascalExpr (loc, _)
 | PasqualExpr (loc, _)

 | VarDefs (loc, _)
 | FunDef (loc, _, _, _, _, _, _)
 | TypeDefs (loc, _)
 | WithExpr (loc, _, _) ->
      loc

let loc_of_type = function
   TypeUnit (loc, _, _)
 | TypePoly (loc, _)
 | TypeChar (loc, _, _, _)
 | TypeInt (loc, _, _, _)
 | TypeFloat (loc, _, _)
 | TypeVar (loc, _, _)
 | TypeLambda (loc, _, _, _)
 | TypeApply (loc, _, _, _)
 | TypeArray (loc, _, _, _, _)
 | TypeConfArray (loc, _, _, _, _, _)
 | TypePointer (loc, _, _)
 | TypeRef (loc, _, _)
 | TypeEnum (loc, _, _)
 | TypeUEnum (loc, _, _)
 | TypeStruct (loc, _, _)
 | TypeUnion (loc, _, _)
 | TypeFun (loc, _, _, _)
 | TypeProduct (loc, _, _)
 | TypeElide loc ->
      loc

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
