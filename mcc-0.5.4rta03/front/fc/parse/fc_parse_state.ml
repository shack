(*
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Location

open Fc_config

open Fc_parse_type

exception IncompatibleTypes of loc * ty * ty

(*
 * Enable extensions.
 *)
let fc_extensions = ref true

(*
 * File position information.
 *)
let position = ref (bogus_loc "<Fc_parse_state>")

let set_current_position pos =
   position := pos

let current_position () =
   !position

let current_file () =
   let name, _, _, _, _ = dest_loc !position in
      name

(*
 * Names that are actually types.
 *)
let typedefs = ref [SymbolSet.empty, []]

let init_types () =
    typedefs := [SymbolSet.empty, []]

(*
 * Basic checking.
 *)
let is_type s =
   match !typedefs with
      (tenv, _) :: _ ->
         SymbolSet.mem tenv s
    | [] ->
         raise (Invalid_argument ("Fc_parse_state.is_type: " ^ string_of_symbol s))

let add_typedef s =
   match !typedefs with
      (tenv, decls) :: tl ->
         typedefs := (SymbolSet.add tenv s, decls) :: tl
    | [] ->
         raise (Invalid_argument "Fc_parse_state.add_typedef")

let add_type s pos ty =
   match !typedefs with
      (tenv, decls) :: tl ->
         typedefs := (tenv, (pos, s, s, ty) :: decls) :: tl
    | [] ->
         raise (Invalid_argument "Fc_parse_state.add_typedef_nobind")

(*
 * Block structure, and type scoping.
 *)
let push_tenv () =
   match !typedefs with
      (tenv, _) as h :: t ->
         typedefs := (tenv, []) :: h :: t
    | [] ->
         raise (Invalid_argument "Fc_parse_state.push_tenv")

(*
 * Combine duplicate aggregate definitions.
 *)
let rec combine_types pos label1 ty1 ty2 =
   match ty1, ty2 with
      TypeLambda (_, _, [], ty1), _ ->
         combine_types pos label1 ty1 ty2
    | _, TypeLambda (_, _, [], ty2) ->
         combine_types pos label1 ty1 ty2
    | TypeEnum   (_, _, Label _), TypeEnum   (_, _, Label _)
    | TypeUEnum  (_, _, Label _), TypeUEnum  (_, _, Label _)
    | TypeStruct (_, _, Label _), TypeStruct (_, _, Label _)
    | TypeUnion  (_, _, Label _), TypeUnion  (_, _, Label _)
    | TypeEnum   (_, _, Fields _), TypeEnum   (_, _, Label _)
    | TypeUEnum  (_, _, Fields _), TypeUEnum  (_, _, Label _)
    | TypeStruct (_, _, Fields _), TypeStruct (_, _, Label _)
    | TypeUnion  (_, _, Fields _), TypeUnion  (_, _, Label _) ->
         ty1
    | TypeEnum   (_, _, Label _), TypeEnum   (_, _, Fields _)
    | TypeUEnum  (_, _, Label _), TypeUEnum  (_, _, Fields _)
    | TypeStruct (_, _, Label _), TypeStruct (_, _, Fields _)
    | TypeUnion  (_, _, Label _), TypeUnion  (_, _, Fields _) ->
         ty2
    | _, TypeApply (_, _, label2, []) when Symbol.eq label1 label2 ->
         ty1
    | _, _ ->
         raise (IncompatibleTypes (pos, ty1, ty2))

let squash_decls pos decls =
   let decls =
      List.fold_left (fun decls (pos, v, label, ty) ->
         try
            let _, _, ty' = SymbolTable.find decls v in
            let ty' = combine_types pos v ty ty' in
               SymbolTable.add decls v (pos, label, ty')
         with
            Not_found ->
               SymbolTable.add decls v (pos, label, ty)) SymbolTable.empty decls
   in
      SymbolTable.fold (fun decls v (pos, label, ty) ->
            (pos, v, label, ty) :: decls) [] decls

(*
 * Return all the struct defs.
 *)
let pop_tenv pos =
   match !typedefs with
      (_, decls) :: tl ->
         typedefs := tl;
         squash_decls pos decls
    | [] ->
         raise (Invalid_argument "Fc_parse_state.pop_tenv")

(*
 * Keep track of enum constants.
 *)

let enums = ref SymbolSet.empty

let add_enum_label label pos =
   if (SymbolSet.mem !enums label) then
      raise (ParseError (pos, "duplicate enum label \"" ^ string_of_symbol label ^ "\""))
   else
      enums := SymbolSet.add !enums label;
      !enums

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
