(*
 * Printing the AST.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Debug

open Fc_config
open Fc_parse_type

(************************************************************************
 * PRINTING                                                             *
 ************************************************************************)

(*
 * Blocks indents by this many spaces.
 *)
let tabstop = Fir_state.tabstop

(*
 * Print a string.
 *)
let n_code = Char.code '\n'
let t_code = Char.code '\t'
let v_code = Char.code '\011'
let b_code = Char.code '\b'
let r_code = Char.code '\r'
let f_code = Char.code '\012'
let a_code = Char.code '\007'
let slash_code = Char.code '\\'
let quote_code = Char.code '\''
let dquote_code = Char.code '\022'

let zero_code = Char.code '0'
let a_code = Char.code 'A' - zero_code
let space_code = Char.code ' '
let del_code = 126

let octal_string c =
   let s = String.create 4 in
      s.[0] <- '\\';
      s.[1] <- Char.chr (((c lsr 6) land 7) + zero_code);
      s.[2] <- Char.chr (((c lsr 3) land 7) + zero_code);
      s.[3] <- Char.chr ((c land 7) + zero_code);
      s

let hex_char c =
   if c < 10 then
      Char.chr (c + zero_code)
   else
      Char.chr (c + a_code)

let hex_string c =
   let s = String.create 5 in
      s.[0] <- '\\';
      s.[1] <- 'x';
      s.[2] <- hex_char ((c lsr 8) land 15);
      s.[3] <- hex_char ((c lsr 4) land 15);
      s.[5] <- hex_char (c land 15);
      s

let pp_print_parse_char buf c =
   let s =
      if c = n_code then
         "\\n"
      else if c = t_code then
         "\\t"
      else if c = v_code then
         "\\v"
      else if c = b_code then
         "\\b"
      else if c = r_code then
         "\\r"
      else if c = f_code then
         "\\f"
      else if c = a_code then
         "\\a"
      else if c = slash_code then
         "\\"
      else if c = quote_code then
         "'"
      else if c = dquote_code then
         "\022"
      else if c < space_code then
         octal_string c
      else if c > del_code then
         hex_string c
      else
         String.make 1 (Char.chr c)
   in
      pp_print_string buf s

let pp_print_parse_string buf s =
   let len = Array.length s in
      pp_print_char buf '"';
      for i = 0 to pred len do
         pp_print_parse_char buf s.(i)
      done;
      pp_print_char buf '"'

(*
 * Print an operator class.
 *)
let pp_print_op_class buf = function
   PreOp -> ()
 | PostOp -> pp_print_string buf "$post "

(*
 * Print a storage class.
 *)
let pp_print_storage_class buf = function
   StoreAuto -> ()
 | StoreRegister -> pp_print_string buf "register "
 | StoreStatic -> pp_print_string buf "static "
 | StoreExtern -> pp_print_string buf "extern "

(*
 * Type status.
 *)
let pp_print_type_status buf = function
   StatusConst -> pp_print_string buf "const "
 | StatusVolatile -> pp_print_string buf "volatile "
 | StatusNormal -> ()

(*
 * Print a list of items with a separator.
 *)
let rec pp_print_sep_list buf separator printer = function
   [] ->
      ()
 | [h] ->
      printer buf h
 | h :: t ->
      printer buf h;
      pp_print_string buf separator;
      pp_print_space buf ();
      pp_print_sep_list buf separator printer t

let pp_print_sep_list buf separator printer l =
   pp_open_hvbox buf 0;
   pp_print_sep_list buf separator printer l;
   pp_close_box buf ()

(*
 * Print a list of items with a terminator.
 *)
let rec pp_print_term_list buf terminator printer = function
   [] ->
      ()
 | [h] ->
      printer buf h;
      pp_print_string buf terminator
 | h :: t ->
      printer buf h;
      pp_print_string buf terminator;
      pp_print_space buf ();
      pp_print_term_list buf terminator printer t

(*
 * Print patterns.
 *)
let rec pp_print_pattern buf = function
   CharPattern (_, i) ->
      pp_print_char buf (Char.chr ((Rawint.to_int i) land 0xff))
 | IntPattern (_, i) ->
      pp_print_string buf (Rawint.to_string i)
 | FloatPattern (_, x) ->
      pp_print_string buf (Rawfloat.to_string x)
 | StringPattern (_, _, s) ->
      pp_print_parse_string buf s
 | VarPattern (_, v, label, ty_opt) ->
      pp_print_symbol buf v;
      pp_print_string buf "{";
      pp_print_symbol buf label;
      pp_print_string buf "}";
      (match ty_opt with
          Some ty ->
             pp_print_string buf " : ";
             pp_print_type buf ty
        | None ->
             ())
 | StructPattern (_, fields) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "{ ";
      pp_print_sep_list buf "," (fun buf (v, p) ->
            (match v with
                Some v ->
                   pp_print_symbol buf v;
                   pp_print_string buf ":";
                   pp_print_space buf ()
              | None ->
                   ());
            pp_print_pattern buf p) fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | EnumPattern (_, v, p) ->
      pp_print_symbol buf v;
      pp_print_space buf ();
      pp_print_pattern buf p
 | AsPattern (_, p1, p2) ->
      pp_print_pattern buf p1;
      pp_print_space buf ();
      pp_print_string buf "as ";
      pp_print_pattern buf p2

(*
 * Print a case list.
 *)
and pp_print_cases buf cases =
   List.iter (fun (p, el) ->
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_string buf "case ";
         pp_print_pattern buf p;
         pp_print_string buf ":";
         pp_print_space buf ();
         pp_print_term_list buf ";" pp_print_expr el;
         pp_close_box buf ()) cases

(*
 * Print expressions.
 *)
and pp_print_expr buf = function
   UnitExpr(_, 1, 0) ->
      pp_print_string buf "()"
 | UnitExpr(_, 2, 0) ->
      pp_print_string buf "false"
 | UnitExpr(_, 2, 1) ->
      pp_print_string buf "true"
 | UnitExpr(_, i1, i2) ->
      pp_print_string buf "unit(";
      pp_print_int buf i1;
      pp_print_string buf ", ";
      pp_print_int buf i2;
      pp_print_string buf ")"
 | CharExpr (_, i) ->
      pp_print_char buf (Char.chr ((Rawint.to_int i) land 0xff))
 | IntExpr (_, i) ->
      pp_print_string buf (Rawint.to_string i)
 | FloatExpr (_, x) ->
      pp_print_string buf (Rawfloat.to_string x)
 | StringExpr (_, _, s) ->
      pp_print_parse_string buf s
 | VarExpr (_, v, label) ->
      pp_print_symbol buf v;
      pp_print_string buf "{";
      pp_print_symbol buf label;
      pp_print_string buf "}"
 | OpExpr (_, op_class, v, label, el) ->
      pp_open_hvbox buf tabstop;
      pp_print_op_class buf op_class;
      pp_print_symbol buf v;
      pp_print_string buf "{";
      pp_print_symbol buf label;
      pp_print_string buf "}(";
      pp_print_sep_list buf "," pp_print_expr el;
      pp_print_string buf ")";
      pp_close_box buf ()
 | ProjectExpr (_, e, v) ->
      pp_print_string buf "(";
      pp_print_expr buf e;
      pp_print_string buf ").";
      pp_print_symbol buf v
 | SizeofExpr (_, e) ->
      pp_print_string buf "sizeof(";
      pp_print_expr buf e;
      pp_print_string buf ")"
 | SizeofType (_, ty) ->
      pp_print_string buf "sizeof(";
      pp_print_type buf ty;
      pp_print_string buf ")"
 | IfExpr (_, e1, e2, None) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "if ";
      pp_print_expr buf e1;
      pp_print_string buf " then";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_close_box buf ()
 | IfExpr (_, e1, e2, Some e3) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "if ";
      pp_print_expr buf e1;
      pp_print_string buf " then";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "else";
      pp_print_space buf ();
      pp_print_expr buf e3;
      pp_close_box buf ();
      pp_close_box buf ()
 | ForExpr (_, e1, e2, e3, e4) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "for(";
      pp_print_expr buf e1;
      pp_print_string buf ";";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_print_string buf ";";
      pp_print_space buf ();
      pp_print_expr buf e3;
      pp_print_string buf ")";
      pp_print_space buf ();
      pp_print_expr buf e4;
      pp_close_box buf ()
 | WhileExpr (_, e1, e2) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "while(";
      pp_print_expr buf e1;
      pp_print_string buf ")";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_close_box buf ()
 | DoExpr (_, e1, e2) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "do";
      pp_print_space buf ();
      pp_print_expr buf e1;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "while ";
      pp_print_expr buf e2;
      pp_close_box buf ()
 | SwitchExpr (_, e, cases) ->
      pp_open_hvbox buf 0;
      pp_print_string buf "switch(";
      pp_print_expr buf e;
      pp_print_string buf ")";
      pp_print_space buf ();
      pp_print_cases buf cases;
      pp_close_box buf ()
 | TryExpr (_, e1, cases, e2) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "try";
      pp_print_space buf ();
      pp_print_expr buf e1;
      pp_close_box buf ();
      List.iter (fun (p, el) ->
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "catch ";
            pp_print_pattern buf p;
            pp_print_string buf " ->";
            pp_print_space buf ();
            pp_print_term_list buf ";" pp_print_expr el;
            pp_close_box buf ()) cases;
      (match e2 with
          Some e2 ->
             pp_print_space buf ();
             pp_open_hvbox buf tabstop;
             pp_print_string buf "finally";
             pp_print_space buf ();
             pp_print_expr buf e2;
             pp_close_box buf ()
        | None ->
             ());
      pp_close_box buf ()
 | LabelExpr (_, label) ->
      pp_print_symbol buf label;
      pp_print_string buf ":"
 | CaseExpr (_, p) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "case ";
      pp_print_pattern buf p;
      pp_print_string buf ":";
      pp_close_box buf ()
 | DefaultExpr _ ->
      pp_print_string buf "default:"
 | SeqExpr (_, exprs) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "{";
      pp_print_space buf ();
      pp_print_term_list buf ";" pp_print_expr exprs;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | PascalExpr (_, exprs) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "Pascal {";
      pp_print_space buf ();
      pp_print_term_list buf ";" pp_print_expr exprs;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | PasqualExpr (_, exprs) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "Pasqual {";
      pp_print_space buf ();
      pp_print_term_list buf ";" pp_print_expr exprs;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
 | ReturnExpr (_, expr) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "return";
      pp_print_space buf ();
      pp_print_expr buf expr;
      pp_close_box buf ()
 | RaiseExpr (_, expr) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "raise";
      pp_print_space buf ();
      pp_print_expr buf expr;
      pp_close_box buf ()
 | BreakExpr _ ->
      pp_print_string buf "break;"
 | ContinueExpr _ ->
      pp_print_string buf "continue;"
 | GotoExpr (_, label) ->
      pp_print_string buf "goto ";
      pp_print_symbol buf label;
      pp_print_string buf ";"
 | VarDefs (_, decls) ->
      let print_var_decl (_, sclass, p, ty, expr) =
         pp_open_hvbox buf tabstop;
         pp_print_storage_class buf sclass;
         pp_print_string buf " ";
         pp_print_type buf ty;
         pp_print_space buf ();
         pp_print_pattern buf p;
         pp_print_space buf ();
         pp_print_init_expr buf expr;
         pp_print_string buf ";";
         pp_close_box buf ();
         pp_print_space buf ()
      in
         List.iter print_var_decl decls
 | FunDef (_, store, sym, label, args, ty, body) ->
      pp_open_hvbox buf 0;
      pp_print_storage_class buf store;
      pp_print_type buf ty;
      pp_print_string buf " ";
      pp_print_symbol buf sym;
      pp_print_string buf "{";
      pp_print_symbol buf label;
      pp_print_string buf "}(";
      pp_print_formals buf args;
      pp_print_string buf ")";
      pp_print_space buf ();
      pp_print_expr buf body;
      pp_close_box buf ();
      pp_print_string buf ";";
      pp_print_space buf ()
 | TypeDefs (_, decls) ->
      pp_print_string buf "typedef ";
      let print_type_decl (_, sym, label, ty) =
         pp_open_hvbox buf tabstop;
         pp_print_string buf "[typedef] ";
         pp_print_symbol buf sym;
         pp_print_string buf "{";
         pp_print_symbol buf label;
         pp_print_string buf "} =";
         pp_print_space buf ();
         pp_print_type buf ty;
         pp_close_box buf ();
         pp_print_string buf ";";
         pp_print_space buf ()
      in
         List.iter print_type_decl decls
 | CastExpr (_, ty, e) ->
      pp_print_string buf "(";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_print_space buf ();
      pp_print_expr buf e
 | WithExpr (_, e1, e2) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "with (";
      pp_print_expr buf e1;
      pp_print_string buf ") do ";
      pp_print_space buf ();
      pp_print_expr buf e2;
      pp_close_box buf ()

(*
 * Application arguments.
 *)
and pp_print_args buf args =
   pp_print_sep_list buf "," pp_print_expr args

(*
 * Print the formal parameters.
 *)
and pp_print_formals buf args =
   pp_print_sep_list buf "," (fun buf (_, p, ty) ->
         pp_print_type buf ty;
         pp_print_string buf " ";
         pp_print_pattern buf p) args

(*
 * Initializer expressions.
 *)
and pp_print_init_expr buf init =
   match init with
      InitNone ->
         ()
    | InitExpr _
    | InitArray _ ->
         pp_print_string buf " =";
         pp_print_init buf init

and pp_print_init buf = function
   InitNone ->
      pp_print_string buf "0 /* none */"
 | InitExpr (_, e) ->
      pp_print_expr buf e
 | InitArray (_, fields) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "{";
      pp_print_space buf ();
      pp_print_sep_list buf "," (fun buf (v, e) ->
            (match v with
                Some v ->
                   pp_print_symbol buf v;
                   pp_print_string buf " =";
                   pp_print_space buf ()
              | None ->
                   ());
            pp_print_init buf e) fields;
      pp_close_box buf ();
      pp_print_string buf "}";
      pp_close_box buf ()

(*
 * Precisions.
 *)
and pp_print_int_precision buf pre signed =
   let s =
      match pre, signed with
         Rawint.Int8,  true -> "signed char"
       | Rawint.Int16, true -> "short"
       | Rawint.Int32, true -> "int"
       | Rawint.Int64, true -> "long"
       | Rawint.Int8,  false -> "unsigned char"
       | Rawint.Int16, false -> "unsigned short"
       | Rawint.Int32, false -> "unsigned int"
       | Rawint.Int64, false -> "unsigned long"
   in
      pp_print_string buf s

and pp_print_float_precision buf pre =
   let s =
      match pre with
         Rawfloat.Single -> "float"
       | Rawfloat.Double -> "double"
       | Rawfloat.LongDouble -> "long double"
   in
      pp_print_string buf s

(*
 * Print a type.
 *)
and pp_print_type buf = function
   TypeUnit (_, status, i) ->
      pp_print_type_status buf status;
      pp_print_string buf "unit [";
      pp_print_int buf i;
      pp_print_string buf "]"
 | TypePoly (_, status) ->
      pp_print_type_status buf status;
      pp_print_string buf "poly"
 | TypeChar (_, status, pre, signed) ->
      pp_print_type_status buf status;
      pp_print_int_precision buf pre signed
 | TypeInt (_, status, pre, signed) ->
      pp_print_type_status buf status;
      pp_print_int_precision buf pre signed
 | TypeFloat (_, status, pre) ->
      pp_print_type_status buf status;
      pp_print_float_precision buf pre
 | TypeVar (_, status, sym) ->
      pp_print_type_status buf status;
      pp_print_string buf "'";
      pp_print_symbol buf sym
 | TypeLambda (_, status, vars, ty) ->
      pp_open_hvbox buf tabstop;
      pp_print_type_status buf status;
      pp_print_string buf "(Lambda (";
      pp_print_sep_list buf "," pp_print_symbol vars;
      pp_print_string buf ").";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()
 | TypeApply (_, status, sym, []) ->
      pp_print_type_status buf status;
      pp_print_symbol buf sym
 | TypeApply (_, status, sym, tyl) ->
      pp_open_hvbox buf tabstop;
      pp_print_type_status buf status;
      pp_print_symbol buf sym;
      pp_print_string buf "[";
      pp_print_sep_list buf "," pp_print_type tyl;
      pp_print_string buf "]";
      pp_close_box buf ()
 | TypeArray (_, status, ty, e1, e2) ->
      pp_print_type_status buf status;
      pp_print_string buf "(";
      pp_print_type buf ty;
      pp_print_string buf ")[";
      pp_print_expr buf e1;
      pp_print_string buf "..";
      pp_print_expr buf e2;
      pp_print_string buf "]"
 | TypeConfArray (_, status, ty, v1, v2, v_ty) ->
      pp_print_type_status buf status;
      pp_print_string buf "(";
      pp_print_type buf ty;
      pp_print_string buf ")[";
      pp_print_symbol buf v1;
      pp_print_string buf "..";
      pp_print_symbol buf v2;
      pp_print_string buf ":";
      pp_print_type buf v_ty;
      pp_print_string buf "]"
 | TypePointer (_, status, ty) ->
      pp_print_type_status buf status;
      pp_print_string buf "*";
      pp_print_type buf ty
 | TypeRef (_, status, ty) ->
      pp_print_type_status buf status;
      pp_print_string buf "&";
      pp_print_type buf ty
 | TypeStruct (_, status, fields) ->
      pp_print_type_fields buf "struct" status fields
 | TypeUnion (_, status, fields) ->
      pp_print_type_fields buf "union" status fields
 | TypeEnum (_, status, fields) ->
      pp_print_enum_fields buf "enum" status fields
 | TypeUEnum (_, status, fields) ->
      pp_print_uenum_fields buf "enum" status fields
 | TypeFun (_, status, args, ty) ->
      pp_print_type_status buf status;
      pp_print_string buf "((";
      pp_print_sep_list buf "," pp_print_type args;
      pp_print_string buf ") -> ";
      pp_print_type buf ty;
      pp_print_string buf ")"
 | TypeProduct (_, status, tyl) ->
      pp_print_type_status buf status;
      pp_print_string buf "(";
      pp_print_sep_list buf "#" pp_print_type tyl;
      pp_print_string buf ")"
 | TypeElide _ ->
      pp_print_string buf "..."

and pp_print_types buf types =
   pp_print_sep_list buf "," pp_print_type types

and pp_print_type_opt buf = function
   Some ty ->
      pp_print_space buf ();
      pp_print_string buf ": ";
      pp_print_type buf ty
 | None ->
      ()

and pp_print_type_fields buf name status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_type_status buf status;
   pp_print_string buf name;
   pp_print_string buf " {";
   pp_print_space buf ();
   (match fields with
       Fields fields ->
          pp_print_term_list buf ";" (fun buf (_, v, ty, width) ->
                pp_print_type buf ty;
                pp_print_space buf ();
                pp_print_symbol buf v;
                match width with
                   Some w ->
                      pp_print_string buf " : ";
                      pp_print_expr buf w
                 | None ->
                      ()) fields
     | Label (v, label) ->
          pp_print_string buf "<";
          pp_print_symbol buf v;
          pp_print_string buf ",";
          pp_print_symbol buf label;
          pp_print_string buf ">");
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_enum_fields buf name status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_type_status buf status;
   pp_print_string buf name;
   pp_print_string buf " {";
   pp_print_space buf ();
   (match fields with
       Fields fields ->
          pp_print_term_list buf ";" (fun buf (_, v, width) ->
                pp_print_symbol buf v;
                match width with
                   Some w ->
                      pp_print_string buf " = ";
                      pp_print_expr buf w
                 | None ->
                      ()) fields
     | Label (v, label) ->
          pp_print_string buf "<";
          pp_print_symbol buf v;
          pp_print_string buf ",";
          pp_print_symbol buf label;
          pp_print_string buf ">");
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

and pp_print_uenum_fields buf name status fields =
   pp_open_hvbox buf 0;
   pp_open_hvbox buf tabstop;
   pp_print_type_status buf status;
   pp_print_string buf name;
   pp_print_string buf " {";
   pp_print_space buf ();
   (match fields with
       Fields (_, fields) ->
          pp_print_term_list buf ";" (fun buf (_, v, ty) ->
                (match ty with
                    Some ty ->
                       pp_print_type buf ty
                  | None ->
                       ());
                pp_print_space buf ();
                pp_print_symbol buf v) fields
     | Label (v, label) ->
          pp_print_string buf "<";
          pp_print_symbol buf v;
          pp_print_string buf ",";
          pp_print_symbol buf label;
          pp_print_string buf ">");
   pp_close_box buf ();
   pp_print_space buf ();
   pp_print_string buf "}";
   pp_close_box buf ()

(*
 * Finally, we can print programs.
 *)
let pp_print_debug buf name exp =
   pp_open_hvbox buf 0;
   pp_print_string buf "*** Fc_parse: ";
   pp_print_string buf name;
   pp_print_space buf ();
   pp_print_expr buf exp;
   pp_close_box buf ();
   pp_print_newline buf ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
