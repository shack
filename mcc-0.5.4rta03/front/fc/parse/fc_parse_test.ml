(*
 * Parser tester is a toploop.
 * You can type an expression.
 * It is printed.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Debug
open Fc_parse_type
open Fc_parse_util
open Fc_parse_eval
open Fc_parse_exn

(*
 * Arguments.
 *)
let saw_input = ref false
let print_ast = ref false
let debug_eval = ref false

let usage =
   "usage: compile [-print_ast] [files...] [-- program-args...]"

(*
 * Environment.
 *)
let env = ref Fc_parse_eval.empty_env

let rev_argv = ref []

(*
 * Parser wrappers.
 *)
let parse_prog inx =
   Fc_parse_state.push_tenv ();
   let lexbuf = Lexing.from_channel inx in
   let prog = Fc_parse_parse.prog Fc_parse_lex.main lexbuf in
   let prog = Fc_parse_standardize.standardize_prog prog in
   let decls = Fc_parse_state.pop_tenv () in
      match decls with
         (_, _, pos) :: _ -> TypeDefs (decls, pos) :: prog
       | [] -> prog

let parse_stmt lexbuf =
   Fc_parse_state.push_tenv ();
   let stmt = Fc_parse_parse.topstmt Fc_parse_lex.main lexbuf in
   let decls = Fc_parse_state.pop_tenv () in
      match stmt, decls with
         Some stmt, (_, _, pos) :: _ -> [TypeDefs (decls, pos); stmt]
       | Some stmt, [] -> [stmt]
       | None, _ -> []

(*
 * Compilation.
 *)
let compile name =
   (* Note that we compiled a file *)
   saw_input := true;

   (* Open the input channel and set the position *)
   let inx = open_in name in
   let _ = Fc_parse_state.set_current_position (name, 1, 0, 1, 0) in

   try
      (* Get the program *)
      let prog = parse_prog inx in
         if !print_ast then
            begin
               print_prog prog;
               pp_print_newline buf ()
            end;

         (* Evaluate it *)
         if debug debug_eval then
            env := Fc_parse_eval.eval_prog !env prog;
         close_in inx
   with
      exn ->
         Fc_parse_exn.print_exn_chan stderr exn;
         close_in inx;
         exit 1

(*
 * Toploop.
 *)
let toploop () =
   (* Set the filename *)
   let _ = Fc_parse_state.set_current_position ("<stdin>", 1, 0, 1, 0) in

   (* Create the lexer *)
   let lexbuf = Lexing.from_channel stdin in

   (* Parse and print the expressions *)
   let donep = ref false in
      while not !donep do
         (* Print a prompt *)
         output_string stdout "# ";
         flush stdout;

         (* Get the declaration *)
         try
            match parse_stmt lexbuf with
               [] ->
                  donep := true
             | exprs ->
                  List.iter (fun expr ->
                        if !print_ast then
                           begin
                              (* Print it *)
                              pp_print_expr buf expr;
                              pp_print_newline buf ()
                           end;

                        (* Evaluate it *)
                        let env', v = eval !env expr in
                           print_value v;
                           pp_print_newline buf ();
                           env := env') exprs
         with
            exn ->
               Fc_parse_exn.print_exn_chan stderr exn;
               flush stderr
      done

(*
 * Evaluate the main program.
 *)
let eval_main () =
   if debug debug_eval then
      begin
         if !rev_argv = [] then
            begin
               eprintf "No program arguments: use the -- option%t" eflush;
               exit 1
            end;
         Fc_parse_eval.eval_main !env (Array.of_list (List.rev !rev_argv))
      end

(*
 * Call the main program.
 *)
let spec =
   ["-print_ast", Arg.Unit (fun () -> print_ast := true), "print expressions";
    "-eval", Arg.Unit (fun () -> debug_eval := true), "evaluate the program";
    "--", Arg.Rest (fun s -> rev_argv := s :: !rev_argv), "program arguments"]

let _ =
   Arg.parse spec compile usage;
   let rest =
      if !saw_input then
         eval_main
      else
         toploop
   in
      Fc_parse_exn.catch rest ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
