(*
 * File state.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Location

open Fc_parse_type
open Fc_config

exception IncompatibleTypes of loc * ty * ty

(*
 * configuration flags.
 *)
val fc_extensions : bool ref

(*
 * A table of type names
 *)
val is_type : symbol -> bool

(*
 * Environments.
 *)
val init_types : unit -> unit
val add_typedef : symbol -> unit
val add_type : symbol -> loc -> ty -> unit
val push_tenv : unit -> unit
val pop_tenv : unit -> type_decl list

(*
 * Save the lexer's current position so we
 * can print out exceptions intelligently.
 *)
val set_current_position : loc -> unit
val current_position : unit -> loc
val current_file : unit -> symbol

val add_enum_label : symbol -> loc -> SymbolSet.t

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
