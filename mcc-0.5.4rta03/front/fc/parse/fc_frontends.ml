(*
 * Set front-end specific parameters.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *
 *)

(************************************************************************
 * PARAMETER PASSING
 ************************************************************************)

type copy_option =
      CopyAll
    | CopyAggregates
    | CopyNone

(*
 * Default: no parameters are copied.
 *)
let param_copy = ref CopyNone
let parsing_pasqual_program = ref true

(************************************************************************
 * FRONT-END MODULE
 ************************************************************************)

(*
 * Parameters for the front-end.
 *)
module type FrontEndSig =
sig
   val parameter_copy : unit -> copy_option
   val set_parameter_copying : copy_option -> unit
   
   val pasqual_program : unit -> bool
   val set_pasqual_parsing : unit -> unit
   val set_pascal_parsing : unit -> unit
end

module FrontEnd : FrontEndSig =
struct
    let parameter_copy () = !param_copy

    let set_parameter_copying cp =
	param_copy := cp

    let pasqual_program () = !parsing_pasqual_program

    let set_pasqual_parsing () =
	parsing_pasqual_program := true
    let set_pascal_parsing () =
	parsing_pasqual_program := false
end
