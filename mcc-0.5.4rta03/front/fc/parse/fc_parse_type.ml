(*
 * Abstract syntax for the CS237 language.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Rawint
open Rawfloat
open Location

open Fc_config

(*
 * Labels are external names.
 *)
type label = symbol
type var = symbol

(*
 * Operators are classified into three classes:
 * PrefixOp: normal functions
 * BooleanOp: short-circuit functions
 * PostfixOp: delayed operations (like i++)
 *)
type op_class =
   PreOp
 | PostOp

(*
 * Types are either identified by names,
 * or they are array or function types.
 *)
type ty =
   TypeUnit    of loc * type_status * int
 | TypePoly    of loc * type_status
 | TypeChar    of loc * type_status * int_precision * int_signed
 | TypeInt     of loc * type_status * int_precision * int_signed
 | TypeFloat   of loc * type_status * float_precision
 | TypeArray   of loc * type_status * ty * expr * expr
 | TypeConfArray
               of loc * type_status * ty * symbol * symbol * ty
 | TypePointer of loc * type_status * ty
 | TypeRef     of loc * type_status * ty
 | TypeProduct of loc * type_status * ty list
 | TypeEnum    of loc * type_status * enum_decl list label_option
 | TypeUEnum   of loc * type_status * (label * union_enum_decl list) label_option
 | TypeStruct  of loc * type_status * field_decl list label_option
 | TypeUnion   of loc * type_status * field_decl list label_option
 | TypeFun     of loc * type_status * ty list * ty
 | TypeVar     of loc * type_status * symbol
 | TypeLambda  of loc * type_status * symbol list * ty
 | TypeApply   of loc * type_status * symbol * ty list
 | TypeElide   of loc

(*
 * Record fields have an optional field width (in bits)
 *)
and enum_decl  = loc * symbol * expr option

and union_enum_decl = loc * symbol * ty option

and field_decl = loc * symbol * ty * expr option

(*
 * Patterns.
 *)
and pattern =
   CharPattern    of loc * rawint
 | IntPattern     of loc * rawint
 | FloatPattern   of loc * rawfloat
 | StringPattern  of loc * precision * int array
 | VarPattern     of loc * symbol * label * ty option
 | StructPattern  of loc * (symbol option * pattern) list
 | EnumPattern    of loc * symbol * pattern
 | AsPattern      of loc * pattern * pattern

(*
 * Expressions.
 *)
and expr =
   (* Base expressions *)
   UnitExpr       of loc * int * int
 | CharExpr       of loc * rawint
 | IntExpr        of loc * rawint
 | FloatExpr      of loc * rawfloat
 | StringExpr     of loc * precision * int array
 | VarExpr        of loc * symbol * symbol

   (* Functions and operators *)
 | OpExpr         of loc * op_class * var * label * expr list
 | ProjectExpr    of loc * expr * var
 | SizeofExpr     of loc * expr
 | SizeofType     of loc * ty
 | CastExpr       of loc * ty * expr

   (* Structured expressions *)
 | IfExpr         of loc * expr * expr * expr option
 | ForExpr        of loc * expr * expr * expr * expr
 | WhileExpr      of loc * expr * expr
 | DoExpr         of loc * expr * expr
 | TryExpr        of loc * expr * (pattern * expr list) list * expr option
 | SwitchExpr     of loc * expr * (pattern * expr list) list
 | LabelExpr      of loc * symbol
 | CaseExpr       of loc * pattern
 | DefaultExpr    of loc
 | ReturnExpr     of loc * expr
 | RaiseExpr      of loc * expr
 | BreakExpr      of loc
 | ContinueExpr   of loc
 | GotoExpr       of loc * symbol
 | WithExpr 	  of loc * expr * expr

   (* Comlocition *)
 | SeqExpr        of loc * expr list
 | PascalExpr	  of loc * expr list
 | PasqualExpr    of loc * expr list

   (* Definitions *)
 | VarDefs        of loc * var_decl list
 | FunDef         of loc * storage_class * symbol * label * patt_decl list * ty * expr
 | TypeDefs       of loc * type_decl list

(*
 * Initializers.
 *)
and init_expr =
   InitNone
 | InitExpr of loc * expr
 | InitArray of loc * (symbol option * init_expr) list

(*
 * A variable declaration.
 *)
and var_decl = loc * storage_class * pattern * ty * init_expr

(*
 * Formal parameter for a function or
 * a variable declaration.
 *)
and patt_decl = loc * pattern * ty

(*
 * Formal parameter for a function,
 * a variable declaration, or a type
 * definition.
 *)
and type_decl = loc * symbol * symbol * ty

(*
 * A program is a list of declarations.
 * The empty program is valid.
 *)
type prog = expr list

(*
 * Parse-time exception.
 *)
exception ParseError of loc * string

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
