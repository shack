(*
 * Define the parser errors and print functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol
open Location

open Fc_parse_type
open Fc_parse_print
open Fc_parse_state

exception ParseExpError of loc * string * expr

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
let pp_print_exn buf = function
   ParseError (pos, str) ->
      pp_print_location buf pos;
      pp_print_string buf ":";
      pp_print_space buf ();
      pp_print_string buf str;
      pp_print_newline buf ()
 | ParseExpError (pos, str, exp) ->
      pp_print_location buf pos;
      pp_print_string buf ":";
      pp_print_space buf ();
      pp_print_string buf str;
      pp_print_space buf ();
      pp_print_expr buf exp;
      pp_print_newline buf ()
 | IncompatibleTypes (pos, ty1, ty2) ->
      pp_open_vbox buf 3;
      pp_print_location buf pos;
      pp_print_string buf "incompatible types:";
      pp_print_space buf ();
      pp_print_type buf ty1;
      pp_print_space buf ();
      pp_print_type buf ty2;
      pp_close_box buf ();
      pp_print_newline buf ()
 | Parsing.Parse_error ->
      pp_print_location buf (current_position ());
      pp_print_space buf ();
      pp_print_string buf "Syntax error";
      pp_print_newline buf ()
 | exn ->
      let loc = current_position () in
         pp_print_location buf loc;
         pp_print_string buf "Fc_parse_exn: uncaught exception:";
         pp_print_space buf ();
         pp_print_string buf (Printexc.to_string exn);
         pp_print_newline buf ()

let catch f x =
   try f x with
      ParseError _
    | ParseExpError _
    | Parsing.Parse_error as exn ->
         pp_print_exn err_formatter exn;
         exit 1

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
