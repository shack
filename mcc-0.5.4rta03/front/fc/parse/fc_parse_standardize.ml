(*
 * Rename all the non-global variables in a program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Symbol
open Fc_config
open Fc_parse_type

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

let apply_sym = Symbol.add "()"

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = symbol SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

let rec env_add_vars env vars vars' =
   match vars, vars' with
      v :: vars, v' :: vars' ->
         env_add_vars (env_add env v v') vars vars'
    | [], [] ->
         env
    | _ ->
         raise (Invalid_argument "Fc_ir_standardize.env_add_vars")

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

(*
 * Check is a variable is defined.
 *)
let env_mem = SymbolTable.mem

(*
 * Variable environment has two parts:
 *    Renames for labels
 *    Renames for variables.
 *    Renames for struct fields.
 *)
type venv =
   { venv_labels : env;
     venv_vars : env;
     venv_fields : env
   }

let venv_empty =
   { venv_labels = env_empty;
     venv_vars = env_empty;
     venv_fields = env_empty
   }

let venv_mem venv v =
   env_mem venv.venv_vars v

let venv_add_var venv v v' =
   { venv with venv_vars = env_add venv.venv_vars v v' }

let venv_add_label venv v v' =
   { venv with venv_labels = env_add venv.venv_labels v v' }

let venv_add_field venv v v' =
   { venv with venv_fields = env_add venv.venv_fields v v' }

let venv_lookup_var venv v =
   try SymbolTable.find venv.venv_vars v with
      Not_found ->
         env_lookup venv.venv_fields v

let venv_lookup_label venv v =
   env_lookup venv.venv_labels v

(*
 * Type environment has two parts:
 *    Renames for type identifiers,
 *    renames for type vars.
 *)
type tenv =
   { tenv_types : env;
     tenv_vars : env
   }

let tenv_empty =
   { tenv_types = env_empty;
     tenv_vars = env_empty
   }

let tenv_mem tenv v =
   SymbolTable.mem tenv.tenv_types v

let tenv_scope tenv =
   { tenv with tenv_vars = env_empty }

let tenv_add_type tenv v v' =
   { tenv with tenv_types = env_add tenv.tenv_types v v' }

let tenv_add_var tenv v v' =
   { tenv with tenv_vars = env_add tenv.tenv_vars v v' }

let tenv_add_vars tenv vars vars' =
   { tenv with tenv_vars = env_add_vars tenv.tenv_vars vars vars' }

let tenv_lookup_type tenv v =
   env_lookup tenv.tenv_types v

let tenv_lookup_var tenv v =
   try
      let v = SymbolTable.find tenv.tenv_vars v in
         tenv, v
   with
      Not_found ->
         let v' = new_symbol v in
         let tenv = tenv_add_var tenv v v' in
            tenv, v'

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

(*
 * Search for all label definitions, and add them
 * to the venv.
 *)
let rec standardize_labels venv e =
   match e with
      UnitExpr _
    | CharExpr _
    | IntExpr _
    | FloatExpr _
    | StringExpr _
    | BreakExpr _
    | ContinueExpr _
    | VarExpr _
    | FunDef _
    | TypeDefs _
    | GotoExpr _
    | SizeofType _
    | CaseExpr _
    | DefaultExpr _ ->
         venv
    | ProjectExpr (_, e, _)
    | ReturnExpr (_, e)
    | RaiseExpr (_, e)
    | CastExpr (_, _, e) ->
         standardize_labels venv e
    | WhileExpr (_, e1, e2)
    | DoExpr (_, e1, e2) ->
         let venv = standardize_labels venv e1 in
            standardize_labels venv e2
    | SwitchExpr (_, e, cases)
    | TryExpr (_, e, cases, None) ->
         let venv = standardize_labels venv e in
         let venv = standardize_label_cases venv cases in
            venv
    | TryExpr (_, e1, cases, Some e2) ->
         let venv = standardize_labels venv e1 in
         let venv = standardize_labels venv e2 in
         let venv = standardize_label_cases venv cases in
            venv
    | IfExpr (_, e1, e2, e3) ->
         let venv = standardize_labels venv e1 in
         let venv = standardize_labels venv e2 in
            standardize_label_opt venv e3
    | ForExpr (_, e1, e2, e3, e4) ->
         let venv = standardize_labels venv e1 in
         let venv = standardize_labels venv e2 in
         let venv = standardize_labels venv e3 in
            standardize_labels venv e4
    | SeqExpr (_, el)
    | PascalExpr (_, el)
    | PasqualExpr (_, el)
    | OpExpr (_, _, _, _, el) ->
         standardize_label_list venv el
    | SizeofExpr (_, e) ->
         standardize_labels venv e
    | VarDefs (_, vars) ->
         List.fold_left (fun venv (_, _, _, _, e_init) ->
               standardize_label_init venv e_init) venv vars
    | LabelExpr (_, label) ->
         let label' = new_symbol label in
         let venv = venv_add_label venv label label' in
            venv
    | WithExpr (_, e1, e2) ->
         let venv = standardize_labels venv e1 in
         let venv = standardize_labels venv e2 in
            venv

(*
 * Optional expression.
 *)
and standardize_label_opt venv = function
   Some e ->
      standardize_labels venv e
 | None ->
      venv

(*
 * Initialization expressions.
 *)
and standardize_label_init venv = function
   InitNone ->
      venv
 | InitExpr (_, e) ->
      standardize_labels venv e
 | InitArray (_, el) ->
      List.fold_left (fun venv (_, e) ->
            standardize_label_init venv e) venv el

(*
 * Cases expression.
 *)
and standardize_label_cases venv cases =
   List.fold_left (fun venv (_, el) ->
         standardize_label_list venv el) venv cases

(*
 * Block expression.
 * Increase nested to indicate nesting.
 *)
and standardize_label_list venv el =
   List.fold_left standardize_labels venv el

(*
 * Check if a type is incomplete.
 *)
let rec is_incomplete_type ty =
   match ty with
      TypeEnum (_, _, Label _)
    | TypeUEnum (_, _, Label _)
    | TypeStruct (_, _, Label _)
    | TypeUnion (_, _, Label _) ->
         true
    | TypeLambda (_, _, [], ty) ->
         is_incomplete_type ty
    | _ ->
         false

(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv venv ty =
   match ty with
      TypeUnit _
    | TypePoly _
    | TypeChar _
    | TypeInt _
    | TypeFloat _
    | TypeElide _ ->
         tenv, venv, ty
    | TypeArray (pos, status, ty, e1, e2) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
         let tenv, _, e1 = standardize_expr tenv venv e1 in
         let tenv, _, e2 = standardize_expr tenv venv e2 in
            tenv, venv, TypeArray (pos, status, ty, e1, e2)
     (*
      * Index variables are binding.
      *)
    | TypeConfArray (pos, status, ty, v1, v2, v_ty) ->
	 let v1' = new_symbol v1 in
	 let v2' = new_symbol v2 in
	 let venv = venv_add_var venv v1 v1' in
	 let venv = venv_add_var venv v2 v2' in
	 let tenv, venv, ty = standardize_type tenv venv ty in
	 let tenv, venv, v_ty = standardize_type tenv venv v_ty in
	    tenv, venv, TypeConfArray (pos, status, ty, v1', v2', v_ty)
    | TypePointer (pos, status, ty) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TypePointer (pos, status, ty)
    | TypeRef (pos, status, ty) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TypeRef (pos, status, ty)
    | TypeStruct (pos, status, fields) ->
         let tenv, venv, fields = standardize_fields tenv venv fields in
            tenv, venv, TypeStruct (pos, status, fields)
    | TypeUnion (pos, status, fields) ->
         let tenv, venv, fields = standardize_fields tenv venv fields in
            tenv, venv, TypeUnion (pos, status, fields)
    | TypeEnum (pos, status, fields) ->
         let tenv, venv, fields = standardize_enum_fields tenv venv fields in
            tenv, venv, TypeEnum (pos, status, fields)
    | TypeUEnum (pos, status, fields) ->
         let tenv, venv, fields = standardize_uenum_fields tenv venv fields in
            tenv, venv, TypeUEnum (pos, status, fields)
    | TypeFun (pos, status, ty_vars, ty_res) ->
         let tenv, venv, ty_vars = standardize_type_list tenv venv ty_vars in
         let tenv, venv, ty_res = standardize_type tenv venv ty_res in
            tenv, venv, TypeFun (pos, status, ty_vars, ty_res)
    | TypeVar (pos, status, v) ->
         let tenv, v = tenv_lookup_var tenv v in
            tenv, venv, TypeVar (pos, status, v)
    | TypeLambda (pos, status, vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = tenv_add_vars tenv vars vars' in
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, TypeLambda (pos, status, vars', ty)
    | TypeApply (pos, status, v, tyl) ->
         let v = tenv_lookup_type tenv v in
         let tenv, venv, tyl = standardize_type_list tenv venv tyl in
            tenv, venv, TypeApply (pos, status, v, tyl)
    | TypeProduct (pos, status, tyl) ->
         let tenv, venv, tyl = standardize_type_list tenv venv tyl in
            tenv, venv, TypeProduct (pos, status, tyl)

and standardize_type_opt tenv venv = function
   Some ty ->
      let tenv, venv, ty = standardize_type tenv venv ty in
         tenv, venv, Some ty
 | None ->
      tenv, venv, None

and standardize_type_list tenv venv tyl =
   let tenv, venv, tyl =
      List.fold_left (fun (tenv, venv, tyl) ty ->
            let tenv, venv, ty = standardize_type tenv venv ty in
            let tyl = ty :: tyl in
               tenv, venv, tyl) (tenv, venv, []) tyl
   in
      tenv, venv, List.rev tyl

and standardize_fields tenv venv fields =
   match fields with
      Fields fields ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, ty, e_opt) ->
                  let tenv, venv, ty = standardize_type tenv venv ty in
                  let tenv, _, e_opt = standardize_expr_opt tenv venv e_opt in
                  let fields = (pos, v, ty, e_opt) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

and standardize_enum_fields tenv venv fields =
   match fields with
      Fields fields ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, e_opt) ->
                  let tenv, _, e_opt = standardize_expr_opt tenv venv e_opt in
                  let v' = new_symbol v in
                  let venv = venv_add_field venv v v' in
                  let fields = (pos, v', e_opt) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

and standardize_uenum_fields tenv venv fields =
   match fields with
      Fields (label, fields) ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (pos, v, ty) ->
                  let tenv, venv, ty =
                     match ty with
                        Some ty ->
                           let tenv, venv, ty = standardize_type tenv venv ty in
                              tenv, venv, Some ty
                      | None ->
                           tenv, venv, None
                  in
                  let v' = new_symbol v in
                  let venv = venv_add_field venv v v' in
                  let fields = (pos, v', ty) :: fields in
                     tenv, venv, fields) (tenv, venv, []) fields
         in
            tenv, venv, Fields (label, List.rev fields)
    | Label (v, label) ->
         tenv, venv, Label (v, label)

(*
 * Rename all the vars in the pattern.
 *)
and standardize_pattern tenv venv p =
   match p with
      CharPattern _
    | IntPattern _
    | FloatPattern _
    | StringPattern _ ->
         tenv, venv, p
    | VarPattern (pos, v, label, ty_opt) ->
         let tenv, venv, ty_opt = standardize_type_opt tenv venv ty_opt in
            (try
                let v' = SymbolTable.find venv.venv_fields v in
                   tenv, venv, VarPattern (pos, v', label, ty_opt)
             with
                Not_found ->
                   let v' = new_symbol v in
                   let venv = venv_add_var venv v v' in
                      tenv, venv, VarPattern (pos, v', label, ty_opt))
    | StructPattern (pos, fields) ->
         let tenv, venv, fields =
            List.fold_left (fun (tenv, venv, fields) (v, p) ->
                  let tenv, venv, p = standardize_pattern tenv venv p in
                     tenv, venv, (v, p) :: fields) (tenv, venv, []) fields
         in
            tenv, venv, StructPattern (pos, List.rev fields)
    | EnumPattern (pos, v, p) ->
         let v = venv_lookup_var venv v in
         let tenv, venv, p = standardize_pattern tenv venv p in
            tenv, venv, EnumPattern (pos, v, p)
    | AsPattern (pos, p1, p2) ->
         let tenv, venv, p1 = standardize_pattern tenv venv p1 in
         let tenv, venv, p2 = standardize_pattern tenv venv p2 in
            tenv, venv, AsPattern (pos, p1, p2)

(*
 * Rename all the vars in the expr.
 *)
and standardize_expr tenv venv e =
   match e with
      UnitExpr _
    | CharExpr _
    | IntExpr _
    | FloatExpr _
    | StringExpr _
    | BreakExpr _
    | ContinueExpr _
    | DefaultExpr _ ->
         tenv, venv, e
    | OpExpr (pos, op_class, v, label, el) ->
	 (* If we have a function application, don't rename label
	  * of function to be applied (2nd argument)
	  *)
	 if Symbol.eq v apply_sym then begin
	    if List.length el < 2 then
		raise (Invalid_argument "OpExpr: bad number of arguments")
	    else
		let tenv, venv, e1 = standardize_expr tenv venv (List.hd el) in
		let e2 = List.hd (List.tl el) in
		let tenv, venv, rest = standardize_expr_list tenv venv (List.tl (List.tl el)) in
		   tenv, venv, OpExpr (pos, op_class, v, label, e1 :: e2 :: rest)
	 end else
         let tenv, venv, el = standardize_expr_list tenv venv el in
            tenv, venv, OpExpr (pos, op_class, v, label, el)
    | ProjectExpr (pos, e, v) ->
         let tenv, venv, e = standardize_expr tenv venv e in
            tenv, venv, ProjectExpr (pos, e, v)
    | SizeofExpr (pos, e) ->
         let tenv, venv, e = standardize_expr tenv venv e in
            tenv, venv, SizeofExpr (pos, e)
    | SizeofType (pos, ty) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
            tenv, venv, SizeofType (pos, ty)
    | CaseExpr (pos, p) ->
         let tenv, venv, p = standardize_pattern tenv venv p in
            tenv, venv, CaseExpr (pos, p)
    | SwitchExpr (pos, e, cases) ->
         let tenv, venv', e = standardize_expr tenv venv e in
         let tenv, _, cases = standardize_cases tenv venv' cases in
            tenv, venv, SwitchExpr (pos, e, cases)
    | VarExpr (pos, v, label) ->
         tenv, venv, VarExpr (pos, venv_lookup_var venv v, label)
    | IfExpr (pos, e1, e2, e3) ->
         let tenv, _, e1 = standardize_expr tenv venv e1 in
         let tenv, _, e2 = standardize_expr tenv venv e2 in
         let tenv, _, e3 = standardize_expr_opt tenv venv e3 in
            tenv, venv, IfExpr (pos, e1, e2, e3)
    | TryExpr (pos, e1, cases, e3) ->
         let tenv, _, e1 = standardize_expr tenv venv e1 in
         let tenv, _, cases = standardize_cases tenv venv cases in
         let tenv, _, e3 = standardize_expr_opt tenv venv e3 in
            tenv, venv, TryExpr (pos, e1, cases, e3)
    | ForExpr (pos, e1, e2, e3, e4) ->
         let tenv, venv', e1 = standardize_expr tenv venv e1 in
         let tenv, _, e2 = standardize_expr tenv venv' e2 in
         let tenv, _, e3 = standardize_expr tenv venv' e3 in
         let tenv, _, e4 = standardize_expr tenv venv' e4 in
            tenv, venv, ForExpr (pos, e1, e2, e3, e4)
    | WhileExpr (pos, e1, e2) ->
         let tenv, venv', e1 = standardize_expr tenv venv e1 in
         let tenv, _, e2 = standardize_expr tenv venv' e2 in
            tenv, venv, WhileExpr (pos, e1, e2)
    | DoExpr (pos, e1, e2) ->
         let tenv, venv', e1 = standardize_expr tenv venv e1 in
         let tenv, _, e2 = standardize_expr tenv venv' e2 in
            tenv, venv, DoExpr (pos, e1, e2)
    | ReturnExpr (pos, e) ->
         let tenv, _, e = standardize_expr tenv venv e in
            tenv, venv, ReturnExpr (pos, e)
    | RaiseExpr (pos, e) ->
         let tenv, _, e = standardize_expr tenv venv e in
            tenv, venv, RaiseExpr (pos, e)
    | SeqExpr (pos, el) ->
         let tenv, venv, el = standardize_expr_list tenv venv el in
            tenv, venv, SeqExpr (pos, el)
    | PascalExpr (pos, el) ->
         let tenv, venv, el = standardize_expr_list tenv venv el in
            tenv, venv, PascalExpr (pos, el)
    | PasqualExpr (pos, el) ->
         let tenv, venv, el = standardize_expr_list tenv venv el in
            tenv, venv, PasqualExpr (pos, el)
    | VarDefs (pos, vars) ->
         standardize_var_defs_expr tenv venv vars pos
    | FunDef (pos, store, v, label, vars, ty, e) ->
         standardize_fun_def_expr tenv venv store v label vars ty e pos
    | TypeDefs (pos, decls) ->
         standardize_typedefs_expr tenv venv decls pos
    | CastExpr (pos, ty, e) ->
         let tenv, venv, ty = standardize_type tenv venv ty in
         let tenv, venv, e = standardize_expr tenv venv e in
            tenv, venv, CastExpr (pos, ty, e)
    | GotoExpr (pos, label) ->
         let label = venv_lookup_label venv label in
            tenv, venv, GotoExpr (pos, label)
    | LabelExpr (pos, label) ->
         let label = venv_lookup_label venv label in
            tenv, venv, LabelExpr (pos, label)
    | WithExpr (pos, e1, e2) ->
         let tenv, venv', e1 = standardize_expr tenv venv e1 in
         let tenv, _, e2 = standardize_expr tenv venv' e2 in
            tenv, venv, WithExpr (pos, e1, e2)

(*
 * Optional expression.
 *)
and standardize_expr_opt tenv venv = function
   Some e ->
      let tenv, venv, e = standardize_expr tenv venv e in
         tenv, venv, Some e
 | None ->
      tenv, venv, None

(*
 * Initializer.
 *)
and standardize_expr_init tenv venv = function
   InitNone ->
      tenv, venv, InitNone
 | InitExpr (pos, e) ->
      let tenv, venv, e = standardize_expr tenv venv e in
         tenv, venv, InitExpr (pos, e)
 | InitArray (pos, el) ->
      let tenv, venv, el =
         List.fold_left (fun (tenv, venv, el) (v, e) ->
               let tenv, venv, e = standardize_expr_init tenv venv e in
                  tenv, venv, (v, e) :: el) (tenv, venv, []) el
      in
         tenv, venv, InitArray (pos, List.rev el)

(*
 * Cases.
 *)
and standardize_cases tenv venv = function
   (p, el) :: cases ->
      let tenv, venv, p = standardize_pattern tenv venv p in
      let tenv, venv, el = standardize_expr_list tenv venv el in
      let tenv, venv, cases = standardize_cases tenv venv cases in
         tenv, venv, (p, el) :: cases
 | [] ->
      tenv, venv, []

(*
 * Block expression.
 * Increase to indicate nesting.
 *)
and standardize_expr_list tenv venv = function
   e :: el ->
      let tenv, venv, e = standardize_expr tenv venv e in
      let tenv, venv, el = standardize_expr_list tenv venv el in
         tenv, venv, e :: el
 | [] ->
      tenv, venv, []

(*
 * Variable declarations.
 *)
and standardize_var_defs_expr tenv venv vars pos =
   let tenv, venv, vars =
      List.fold_left (fun (tenv, venv, vars) (pos, store, p, ty, eo) ->
            let tenv, venv, ty = standardize_type tenv venv ty in
            let tenv, _, eo = standardize_expr_init tenv venv eo in
            let tenv, venv, p = standardize_pattern tenv venv p in
               tenv, venv, (pos, store, p, ty, eo) :: vars) (tenv, venv, []) vars
   in
      tenv, venv, VarDefs (pos, List.rev vars)

(*
 * Function definition.
 *)
and standardize_fun_def_expr tenv venv store f label vars ty e pos =
   (* Change the function name *)
   let f' = new_symbol f in
   let venv = venv_add_var venv f f' in

   (* Rename the type *)
   let tenv' = tenv_scope tenv in
   let tenv', venv', ty = standardize_type tenv' venv ty in

   (* Rename all the labels *)
   let venv' = standardize_labels venv e in

   (* Rename the vars *)
   let tenv', venv', vars =
      List.fold_left (fun (tenv, venv, vars) (pos, p, ty) ->
            let tenv, venv, p = standardize_pattern tenv venv p in
            let tenv, venv, ty = standardize_type tenv venv ty in
               tenv, venv, (pos, p, ty) :: vars) (tenv', venv', []) vars
   in
   let vars = List.rev vars in

   (* Rename the body *)
   let _, _, e = standardize_expr tenv' venv' e in
      tenv, venv, FunDef (pos, store, f', label, vars, ty, e)

(*
 * Type definition.
 *)
and standardize_typedefs_expr tenv venv decls pos =
   (* First, change just the type names *)
   let tenv, decls =
      List.fold_left (fun (tenv, decls) (pos, v, label, ty) ->
            if tenv_mem tenv v && is_incomplete_type ty then
               tenv, decls
            else
               let v' = new_symbol v in
               let tenv = tenv_add_type tenv v v' in
                  tenv, (pos, v', label, ty) :: decls) (tenv, []) decls
   in

   (* Now change the definitions *)
   let tenv, venv, decls =
      List.fold_left (fun (tenv, venv, decls) (pos, v, label, ty) ->
            let tenv, venv, ty = standardize_type tenv venv ty in
            let decls = (pos, v, label, ty) :: decls in
               tenv, venv, decls) (tenv, venv, []) decls
   in
      tenv, venv, TypeDefs (pos, decls)

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Global functions use a null environment.
 *)
let standardize_expr e =
   let _, _, e = standardize_expr tenv_empty venv_empty e in
      e

let standardize_prog body =
   let _, _, el = standardize_expr_list tenv_empty venv_empty body in
      el

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
