/*
 * Parser for FC.
 * There should be exactly 2 shift/reduce conflicts,
 * one for if statements, and the other for try statements.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 */
%{
open Symbol
open Location

open Fc_config
open Fc_parse_type
open Fc_parse_util
open Fc_parse_state
open Fc_parse_exn

(*
 * Struct declarations have three cases:
 * structs, unions, or union enums
 *)
type struct_case =
   StructCase
 | UnionCase

(*
 * Type modifiers.
 *)
type spec =
   AutoSpec
 | RegisterSpec
 | StaticSpec
 | ExternSpec
 | ConstSpec
 | VolatileSpec
 | ShortSpec
 | LongSpec
 | SignedSpec
 | UnsignedSpec

(*
 * Builtin types.
 *)
type builtin =
   VoidType
 | CharType
 | IntType
 | FloatType
 | DoubleType
 | PolyType
 | VarType of symbol
 | ApplyType of symbol * ty list

(*
 * Well-known symbols.
 *)
let comma_sym     = Symbol.add ","
let star_sym      = Symbol.add "*"
let addr_of_sym   = Symbol.add "&"
let sizeof_sym    = Symbol.add "sizeof"
let eq_sym        = Symbol.add "="
let bang_sym      = Symbol.add "!"
let or_sym        = Symbol.add "||"
let and_sym       = Symbol.add "&&"
let subscript_sym = Symbol.add "[]"
let apply_sym     = Symbol.add "()"

let zero_expr loc =
   IntExpr (loc, Rawint.of_int Rawint.Int32 true 0)

let one_expr loc =
   IntExpr (loc, Rawint.of_int Rawint.Int32 true 1)

(*
 * Var name in a declaration.
 *)
type var_name =
   VarNameId      of loc * symbol option
 | VarNamePattern of loc * pattern
 | VarNameApply   of loc * symbol * ty list
 | VarNameArray   of loc * (loc * spec) list * var_name * expr
 | VarNamePointer of loc * (loc * spec) list * var_name
 | VarNameRef     of loc * (loc * spec) list * var_name
 | VarNameFun     of loc * var_name * (pattern option * ty) list

let loc_of_var_name = function
   VarNameId      (loc, _) -> loc
 | VarNamePattern (loc, _) -> loc
 | VarNameApply   (loc, _, _) -> loc
 | VarNameArray   (loc, _, _, _) -> loc
 | VarNamePointer (loc, _, _) -> loc
 | VarNameRef     (loc, _, _) -> loc
 | VarNameFun     (loc, _, _) -> loc

(*
 * Extract the status from the specifiers.
 *)
let status_of_spec spec =
   let collect status (_, spec) =
      match spec with
         ConstSpec -> StatusConst
       | VolatileSpec -> StatusVolatile
       | AutoSpec
       | RegisterSpec
       | StaticSpec
       | ExternSpec
       | ShortSpec
       | LongSpec
       | SignedSpec
       | UnsignedSpec -> status
   in
      List.fold_left collect StatusNormal spec

let storage_class_of_spec spec =
   let collect store (_, spec) =
      match spec with
         AutoSpec -> StoreAuto
       | RegisterSpec -> StoreRegister
       | StaticSpec -> StoreStatic
       | ExternSpec -> StoreExtern
       | ConstSpec
       | VolatileSpec
       | ShortSpec
       | LongSpec
       | SignedSpec
       | UnsignedSpec  -> store
   in
      List.fold_left collect StoreAuto spec

(*
 * Function parameter declarations.
 *)
let rec make_param_decl ty = function
   VarNameId (loc, Some id) ->
      Some (VarPattern (loc, id, id, Some ty)), ty
 | VarNameId (_, None) ->
      None, ty
 | VarNamePattern (_, p) ->
      Some p, ty
 | VarNameApply (loc, _, _) ->
      raise (ParseError (loc, "illegal parameter definition"))
 | VarNameArray (loc, spec, v, e) ->
      make_param_decl (TypeArray (loc, status_of_spec spec, ty, zero_expr loc, e)) v
 | VarNamePointer (loc, spec, v) ->
      make_param_decl (TypePointer (loc, status_of_spec spec, ty)) v
 | VarNameRef (loc, spec, v) ->
      make_param_decl (TypeRef (loc, status_of_spec spec, ty)) v
 | VarNameFun (loc, v, args) ->
      make_param_decl (TypeFun (loc, StatusNormal, make_param_decls args, ty)) v

and make_param_decls args =
   List.map snd args

let make_param_decl ty p =
   match p with
      VarNameId (loc, Some id) ->
         Some (VarPattern (loc, id, id, Some ty)), ty
    | _ ->
         make_param_decl ty p

(*
 * Build a variable declaration from the syntax.
 *)
let make_var_init_decls store ty defs =
   (* Build the declaration with an initializer *)
   let rec make_def ty e = function
      VarNameId (loc, Some n) ->
         loc, store, n, ty, e
    | VarNamePattern (loc, _)
    | VarNameId (loc, None)
    | VarNameApply (loc, _, _) ->
         raise (ParseError (loc, "illegal variable declaration"))
    | VarNameArray (loc, spec, v, e') ->
         make_def (TypeArray (loc, status_of_spec spec, ty, zero_expr loc, e')) e v
    | VarNamePointer (loc, spec, v) ->
         make_def (TypePointer (loc, status_of_spec spec, ty)) e v
    | VarNameRef (loc, spec, v) ->
         make_def (TypeRef (loc, status_of_spec spec, ty)) e v
    | VarNameFun (loc, v, args) ->
         make_def (TypeFun (loc, StatusNormal, make_param_decls args, ty)) e v
   in

   (* Initial type *)
   let make_init_def (v, e) =
      make_def ty e v
   in
      List.map make_init_def defs

let make_patt_init_decls store ty defs =
   (* Build the declaration with an initializer *)
   let rec make_def ty e = function
      VarNameId (loc, Some n) ->
         loc, store, VarPattern (loc, n, n, Some ty), ty, e
    | VarNamePattern (loc, p) ->
         loc, store, p, ty, e
    | VarNameId (loc, None)
    | VarNameApply (loc, _, _) ->
         raise (ParseError (loc, "illegal variable declaration"))
    | VarNameArray (loc, spec, v, e') ->
         make_def (TypeArray (loc, status_of_spec spec, ty, zero_expr loc, e')) e v
    | VarNamePointer (loc, spec, v) ->
         make_def (TypePointer (loc, status_of_spec spec, ty)) e v
    | VarNameRef (loc, spec, v) ->
         make_def (TypeRef (loc, status_of_spec spec, ty)) e v
    | VarNameFun (loc, v, args) ->
         make_def (TypeFun (loc, StatusNormal, make_param_decls args, ty)) e v
   in

   (* Initial type *)
   let make_init_def (v, e) =
      make_def ty e v
   in
      List.map make_init_def defs

(*
 * Build a variable declaration from the syntax.
 *)
let rec make_var_decl ty = function
   VarNameId (loc, Some n) ->
      loc, n, ty
 | VarNamePattern (loc, _)
 | VarNameId (loc, None)
 | VarNameApply (loc, _, _) ->
      raise (ParseError (loc, "illegal variable declaration"))
 | VarNameArray (loc, spec, v, e) ->
      make_var_decl (TypeArray (loc, status_of_spec spec, ty, zero_expr loc, e)) v
 | VarNamePointer (loc, spec, v) ->
      make_var_decl (TypePointer (loc, status_of_spec spec, ty)) v
 | VarNameRef (loc, spec, v) ->
      make_var_decl (TypeRef (loc, status_of_spec spec, ty)) v
 | VarNameFun (loc, v, args) ->
      make_var_decl (TypeFun (loc, StatusNormal, make_param_decls args, ty)) v

let make_var_decls ty decls =
   List.map (make_var_decl ty) decls

(*
 * Struct decls have an optional width.
 *)
let make_struct_decls (store, ty) decls =
   List.map (fun (decl, width) ->
      let loc, n, ty = make_var_decl ty decl in
         loc, n, ty, width) decls

let make_uenum_ty_decls (store, ty) decls =
   List.map (fun decl ->
      let loc, n, ty = make_var_decl ty decl in
         loc, n, Some ty) decls

(*
 * Basic enumeration declaration.
 *)
let make_enum_decl v e_opt loc =
    loc, v, e_opt

(*
 * Add some type definitions.
 *)
let make_type_lambda loc tyl ty =
   let vars =
      List.map (function
         TypeVar (_, _, v) -> v
       | _ -> raise (ParseError (loc, "not a type variable"))) tyl
   in
      TypeLambda (loc, StatusNormal, vars, ty)

let rec make_type_decl ty = function
   VarNameId (loc, Some n) ->
      loc, n, ty, []
 | VarNameId (loc, None)
 | VarNamePattern (loc, _) ->
      raise (ParseError (loc, "illegal type declaration"))
 | VarNameApply (loc, n, tyl) ->
      loc, n, ty, tyl
 | VarNameArray (loc, spec, v, e) ->
      make_type_decl (TypeArray (loc, status_of_spec spec, ty, zero_expr loc, e)) v
 | VarNamePointer (loc, spec, v) ->
      make_type_decl (TypePointer (loc, status_of_spec spec, ty)) v
 | VarNameRef (loc, spec, v) ->
      make_type_decl (TypeRef (loc, status_of_spec spec, ty)) v
 | VarNameFun (loc, v, args) ->
      make_type_decl (TypeFun (loc, StatusNormal, make_param_decls args, ty)) v

let make_type_decl ty decl =
   let loc, n, ty, tyl = make_type_decl ty decl in
   let ty = make_type_lambda loc tyl ty in
      loc, n, n, ty

let make_type_decls ty decls =
   List.map (make_type_decl ty) decls

(*
 * A function definition.
 *)
let get_fun_var (p, ty) =
   let loc = loc_of_type ty in
   let p =
      match p with
         Some p ->
            p
       | None ->
            let v = new_symbol_string "unnamed_parameter" in
               VarPattern (loc, v, v, Some ty)
   in
      loc, p, ty

let rec make_fun_def (store, ty) decl body =
   let loc = union_loc (loc_of_type ty) (loc_of_expr body) in
      match decl with
         VarNameFun (_, res, vars) ->
            let vars = List.map get_fun_var vars in
            let _, f, ty = make_var_decl ty res in
               FunDef (loc, store, f, f, vars, ty, body)
       | VarNameArray (loc', spec, decl, e) ->
            make_fun_def (store, TypeArray (loc', status_of_spec spec, ty, zero_expr loc, e)) decl body
       | VarNamePointer (loc', spec, decl) ->
            make_fun_def (store, TypePointer (loc', status_of_spec spec, ty)) decl body
       | VarNameRef (loc', spec, decl) ->
            make_fun_def (store, TypeRef (loc', status_of_spec spec, ty)) decl body
       | VarNameId _
       | VarNamePattern _
       | VarNameApply _ ->
            raise (ParseError (loc, "not a function"))

(*
 * Operators.
 *)
let make_op loc op_class v el =
   OpExpr (loc, op_class, v, v, el)

let make_unop op_class (loc, v) expr =
   make_op (union_loc loc (loc_of_expr expr)) op_class v [expr]

let make_binop (loc, v) expr1 expr2 =
   let loc = union_loc loc (loc_of_expr expr1) in
   let loc = union_loc loc (loc_of_expr expr2) in
      make_op loc PreOp v [expr1; expr2]

let make_project (pos1, v) e (pos2, label) =
   let loc = union_loc (loc_of_expr e) pos2 in
      ProjectExpr (loc, OpExpr (loc, PreOp, addr_of_sym, addr_of_sym, [e]), label)

(*
 * Boolean expressions.
 *)
let make_and_op expr1 expr2 =
   let loc = union_loc (loc_of_expr expr1) (loc_of_expr expr2) in
   let v' = new_symbol_string "and" in
   let p = VarPattern (loc, v', v', None) in
   let step1 = VarDefs (loc, [loc, StoreAuto, p, TypePoly (loc, StatusNormal), InitExpr (loc, expr1)]) in
   let test_expr = make_op loc PreOp bang_sym [VarExpr (loc, v', v')] in
   let true_expr = make_op loc PreOp and_sym [VarExpr (loc, v', v'); expr2] in
   let step2 = IfExpr (loc, test_expr, zero_expr loc, Some true_expr) in
      SeqExpr (loc, [step1; step2])

let make_or_op expr1 expr2 =
   let loc = union_loc (loc_of_expr expr1) (loc_of_expr expr2) in
   let v' = new_symbol_string "or" in
   let p = VarPattern (loc, v', v', None) in
   let step1 = VarDefs (loc, [loc, StoreAuto, p, TypePoly (loc, StatusNormal), InitExpr (loc, expr1)]) in
   let test_expr = make_op loc PreOp bang_sym [VarExpr (loc, v', v')] in
   let true_expr = make_op loc PreOp or_sym [VarExpr (loc, v', v'); expr2] in
   let step2 = IfExpr (loc, test_expr, true_expr, Some (one_expr loc)) in
      SeqExpr (loc, [step1; step2])

(*
 * Optional expression.
 *)
let make_opt_expr opt_expr def_expr =
   match opt_expr with
      Some expr -> expr
    | None -> def_expr

(*
 * Types.
 *)
let make_ty_lambda loc vars ty =
   let vars = List.map (fun (_, v) -> v) vars in
      TypeLambda (loc, StatusNormal, vars, ty)

let make_struct_or_union struct_flag fields loc =
   match struct_flag with
      StructCase ->
         TypeStruct (loc, StatusNormal, Fields fields)
    | UnionCase ->
         TypeUnion (loc, StatusNormal, Fields fields)

let make_enum fields loc =
   TypeEnum (loc, StatusConst, Fields fields)

let make_uenum label fields loc =
   TypeUEnum (loc, StatusConst, Fields (label, fields))

let make_tuple fields loc =
   TypeProduct (loc, StatusNormal, fields)

(*
 * Parse the type specification.
 *)
let parse_builtin_type spec loc =
   let loc, store, status, pre, signed =
      List.fold_left (fun (loc, store, status, pre, signed) (loc', spec) ->
         let loc =
            match loc with
               Some loc -> Some (union_loc loc loc')
             | None -> Some loc'
         in
            match spec with
               AutoSpec ->
                  loc, StoreAuto, status, pre, signed
             | RegisterSpec ->
                  loc, StoreRegister, status, pre, signed
             | StaticSpec ->
                  loc, StoreStatic, status, pre, signed
             | ExternSpec ->
                  loc, StoreExtern, status, pre, signed
             | ConstSpec ->
                  loc, store, StatusConst, pre, signed
             | VolatileSpec ->
                  loc, store, StatusVolatile, pre, signed
             | ShortSpec ->
                  loc, store, status, ShortPrecision, signed
             | LongSpec ->
                  loc, store, status, LongPrecision, signed
             | SignedSpec ->
                  loc, store, status, pre, Some true
             | UnsignedSpec ->
                  loc, store, status, pre, Some false) (loc, StoreAuto, StatusNormal, NormalPrecision, None) spec
   in
   let loc =
      match loc with
         Some loc -> loc
       | None -> current_position ()
   in
      loc, store, status, pre, signed

(*
 * Construct a builtin integer type.
 *)
let make_int_type spec =
   let loc, store, status, pre, signed = parse_builtin_type spec None in
   let pre = FCParam.int_precision pre in
   let signed =
      match signed with
         Some signed -> signed
       | None -> true
   in
      store, TypeInt (loc, status, pre, signed)

(*
 * Construct a polymorphic type.
 *)
let make_poly_type spec =
   let loc, store, status, pre, signed = parse_builtin_type spec None in
      store, TypePoly (loc, status)

(*
 * Construct a generic builtin type.
 *)
let rec make_defined_type spec (loc, ty) =
   let loc, store, status, pre, signed = parse_builtin_type spec (Some loc) in
   let ty =
      match ty with
         VoidType ->
          TypeUnit (loc, status, 1)
       | CharType ->
          let pre = FCParam.char_precision pre in
          let signed =
             match signed with
                Some signed -> signed
              | None -> false
          in
             TypeChar (loc, status, pre, signed)
       | IntType ->
          let pre = FCParam.int_precision pre in
          let signed =
             match signed with
                Some signed -> signed
              | None -> true
          in
             TypeInt (loc, status, pre, signed)
       | FloatType ->
            let pre =
               match pre with
                  ShortPrecision
                | NormalPrecision ->
                   ShortPrecision
                | LongPrecision ->
                   NormalPrecision
            in
            let pre = FCParam.float_precision pre in
               TypeFloat (loc, status, pre)
       | DoubleType ->
            let pre = FCParam.float_precision pre in
               TypeFloat (loc, status, pre)
       | VarType v ->
            TypeVar (loc, status, v)
       | ApplyType (v, args) ->
            TypeApply (loc, status, v, args)
       | PolyType ->
            TypePoly (loc, status)
   in
      store, ty

(*
 * Collect the switch cases.
 * This doesn't do any translation, it just does
 * the search.  Note, we have to be a little careful
 * because labels may be interleaved with cases.
 *)
let rec normalize_cases loc e =
   match e with
      SeqExpr (loc, el) ->
         collect_cases loc el
    | _ ->
         raise (ParseError (loc, "switch body is not a compound block"))

and collect_cases loc el =
   if el = [] then
      []
   else
      let case, el = collect_case loc el in
      let cases = collect_cases loc el in
         case :: cases

and collect_case loc el =
   match el with
      CaseExpr (_, p) :: el ->
         let el1, el2 = collect_case_list el in
            (p, el1), el2
    | DefaultExpr loc' :: el ->
         let v = new_symbol_string "_" in
         let p = VarPattern (loc', v, v, None) in
         let el1, el2 = collect_case_list el in
            (p, el1), el2
    | LabelExpr _ as e :: el ->
         let case, el = collect_case loc el in
         let p, el' = case in
         let case = p, e :: el' in
            case, el
    | _ ->
         raise (ParseError (loc, "switch body does not start with a case statement"))

and collect_case_list el =
   match el with
      CaseExpr _ :: _
    | DefaultExpr _ :: _ ->
         [], el
    | e :: el ->
         let el1, el2 = collect_case_list el in
            e :: el1, el2
    | [] ->
         [], []

(*
 * Add unit to the stmt.
 *)
let wrap_stmt e =
   let loc = loc_of_expr e in
      SeqExpr (loc, [e; UnitExpr(loc, 1, 0)])
%}

/*
 * End-of-file is a token.
 */
%token TokEof

/*
 * Binary operators return position.
 */
%token <Location.loc> TokLeftParen
%token <Location.loc> TokRightParen
%token <Location.loc> TokLeftBrack
%token <Location.loc> TokRightBrack
%token <Location.loc> TokLeftBrace
%token <Location.loc> TokRightBrace
%token <Location.loc> TokElide

%token <Location.loc> TokSemi
%token <Location.loc> TokComma
%token <Location.loc> TokQuest
%token <Location.loc> TokColon
%token <Location.loc> TokStar
%token <Location.loc> TokEq
%token <Location.loc> TokAnd
%token <Location.loc> TokOr

%token <Location.loc * Symbol.symbol> TokOp1
%token <Location.loc * Symbol.symbol> TokOp4
%token <Location.loc * Symbol.symbol> TokOp5
%token <Location.loc * Symbol.symbol> TokAddr
%token <Location.loc * Symbol.symbol> TokOp7
%token <Location.loc * Symbol.symbol> TokOp8
%token <Location.loc * Symbol.symbol> TokOp9
%token <Location.loc * Symbol.symbol> TokOp10
%token <Location.loc * Symbol.symbol> TokOp11
%token <Location.loc * Symbol.symbol> TokPreOp12
%token <Location.loc * Symbol.symbol> TokPrePostOp12
%token <Location.loc * Symbol.symbol> TokOp13
%token <Location.loc * Symbol.symbol> TokDot

/*
 * Keywords.
 */
%token <Location.loc> TokAuto
%token <Location.loc> TokBreak
%token <Location.loc> TokCase
%token <Location.loc> TokTypeChar
%token <Location.loc> TokConst
%token <Location.loc> TokContinue
%token <Location.loc> TokDefault
%token <Location.loc> TokDo
%token <Location.loc> TokTypeDouble
%token <Location.loc> TokElse
%token <Location.loc> TokEnum
%token <Location.loc> TokExtern
%token <Location.loc> TokTypeFloat
%token <Location.loc> TokFor
%token <Location.loc> TokGoto
%token <Location.loc> TokIf
%token <Location.loc> TokTypeInt
%token <Location.loc> TokLong
%token <Location.loc> TokRegister
%token <Location.loc> TokReturn
%token <Location.loc> TokRaise
%token <Location.loc> TokShort
%token <Location.loc> TokSigned
%token <Location.loc> TokSizeof
%token <Location.loc> TokStatic
%token <Location.loc> TokStruct
%token <Location.loc> TokTuple
%token <Location.loc> TokSwitch
%token <Location.loc> TokTypedef
%token <Location.loc> TokUnion
%token <Location.loc> TokUnsigned
%token <Location.loc> TokTypeVoid
%token <Location.loc> TokVolatile
%token <Location.loc> TokWhile

%token <Location.loc> TokPoly
%token <Location.loc> TokTry
%token <Location.loc> TokCatch
%token <Location.loc> TokFinally
%token <Location.loc> TokOperator


/*
 * Terminal tokens.
 */
%token <Location.loc * Symbol.symbol> TokId
%token <Location.loc * Symbol.symbol> TokTypeId
%token <Location.loc * Symbol.symbol> TokTypeVar
%token <Location.loc * Fc_config.precision * int array> TokString
%token <Location.loc * Rawint.rawint> TokChar
%token <Location.loc * Rawint.rawint> TokInt
%token <Location.loc * Rawfloat.rawfloat> TokFloat

/*
 * Precedences.
 */
%left TokComma
%right TokOp1 TokEq
%right TokQuest TokColon
%left TokOp2
%left TokAnd TokOr
%left TokOp4
%left TokOp5
%left TokAddr
%left TokOp7
%left TokOp8
%left TokOp9
%left TokOp10
%left TokStar TokOp11
%right prec_unary prec_cast TokPreOp12 TokPrePostOp12
%left prec_apply prec_subscript TokOp13 TokDot TokLeftParen TokRightParen TokLeftBrack TokRightBrack

/* Annihilating the if/then shift reduce conflict :) */
%nonassoc   prec_ifthen
%nonassoc   TokElse prec_ifthenelse
%left       TokCatch TokFinally
%nonassoc   TokTry

/*
 * A complete program.
 */
%start prog
%type <Fc_parse_type.expr list> prog

%%

/************************************************************************
 * TOPLEVEL PRODUCTIONS
 ************************************************************************/

/*
 * A program is a sequence of str_items.
 */
prog:     all_defs TokEof
          { let defs = pop_tenv () in
            let name = current_file () in
            let loc = create_loc name 1 0 1 0 in
               match defs with
                  [] -> $1
                | _ -> TypeDefs (loc, List.rev defs) :: $1
          }
        ;

/************************************************************************
 * DECLARATIONS AND DEFINITIONS
 ************************************************************************/

/*
 * Definitions.
 */
all_defs:
          rev_all_defs
          { List.rev $1 }
        ;

rev_all_defs:
          /* empty */
          { [] }
        | rev_all_defs all_def
          { $2 :: $1 }
        ;

all_def:  var_defs
          { $1 }
        | fun_def
          { $1 }
        | type_defs
          { $1 }
        ;

/************************************************************************
 * TYPE SPECIFIERS
 ************************************************************************/

/*
 * Type parameter lists.
 */
opt_type_params:
          /* empty */
          { [] }
        | type_params
          { $1 }
        ;

type_params:
          TokTypeVar
          { [$1] }
        | TokLeftParen type_param_list TokRightParen
          { List.rev $2 }
        ;

type_param_list:
          TokTypeVar
          { [$1] }
        | type_param_list TokComma TokTypeVar
          { $3 :: $1 }
        ;

/*
 * General declaration specifier.
 */
decl_spec:
          simp_decl_spec
          { $1 }
        | struct_decl_spec
          { $1 }
        ;

simp_decl_spec:
          decl_specifiers
          { make_int_type $1 }
        | decl_specifiers_opt type_builtin decl_specifiers_opt
          { make_defined_type ($1 @ $3) $2 }
        | simp_type_spec decl_type_specifiers_opt
          { make_defined_type $2 $1 }
        | decl_type_specifiers simp_type_spec decl_type_specifiers_opt
          { make_defined_type ($1 @ $3) $2 }
        ;

struct_decl_spec:
          struct_type_spec decl_type_specifiers_opt
          { make_defined_type $2 $1 }
        | decl_type_specifiers struct_type_spec decl_type_specifiers_opt
          { make_defined_type ($1 @ $3) $2 }
        ;

/*
 * Modifiers for builtin types.
 */
decl_specifiers_opt:
          /* empty */
          { [] }
        | decl_specifiers
          { $1 }
        | decl_type_specifiers
          { $1 }
        ;

decl_specifiers:
          type_mod
          { [$1] }
        | decl_type_specifiers type_mod
          { $2 :: $1 }
        | decl_specifiers decl_specifier
          { $2 :: $1 }
        ;

decl_specifier:
          storage_class_spec
          { $1 }
        | type_qual
          { $1 }
        | type_mod
          { $1 }
        ;

/*
 * Modifiers for defined types.
 */
decl_type_specifiers_opt:
          /* empty */
          { [] }
        | decl_type_specifiers
          { $1 }
        ;

decl_type_specifiers:
          decl_type_specifier
          { [$1] }
        | decl_type_specifiers decl_type_specifier
          { $2 :: $1 }
        ;

decl_type_specifier:
          storage_class_spec
          { $1 }
        | type_qual
          { $1 }
        ;

/*
 * Modifiers for defined types.
 */
decl_struct_type_specifiers_opt:
          /* empty */
          { [] }
        | decl_struct_type_specifiers
          { $1 }
        ;

decl_struct_type_specifiers:
          decl_struct_type_specifier
          { [$1] }
        | decl_struct_type_specifiers decl_struct_type_specifier
          { $2 :: $1 }
        ;

decl_struct_type_specifier:
          type_qual
          { $1 }
        ;

/*
 * Storage class specification.
 */
storage_class_spec:
          TokAuto
          { $1, AutoSpec }
        | TokRegister
          { $1, RegisterSpec }
        | TokStatic
          { $1, StaticSpec }
        | TokExtern
          { $1, ExternSpec }
        ;

/*
 * Type qualifier.
 */
type_qual:
          TokConst
          { $1, ConstSpec }
        | TokVolatile
          { $1, VolatileSpec }
        ;

/*
 * Type modifiers.
 */
type_mod:
          TokShort
          { $1, ShortSpec }
        | TokLong
          { $1, LongSpec }
        | TokSigned
          { $1, SignedSpec }
        | TokUnsigned
          { $1, UnsignedSpec }
        ;

/*
 * Builtin types.
 */
type_builtin:
          TokTypeVoid
          { $1, VoidType }
        | TokTypeChar
          { $1, CharType }
        | TokTypeInt
          { $1, IntType }
        | TokTypeFloat
          { $1, FloatType }
        | TokTypeDouble
          { $1, DoubleType }
        ;

/*
 * Monmorphic type specification.
 */
simp_type_spec:
          TokTypeVar
          { let loc, id = $1 in
               loc, VarType id
          }
        | TokTypeId
          { let loc, id = $1 in
               loc, ApplyType (id, [])
          }
        | TokPoly
          { let loc = $1 in
               loc, PolyType
          }
        | TokPoly TokTypeId TokLeftParen opt_param_list TokRightParen
          { let _, id = $2 in
            let loc = union_loc $1 $5 in
            let args = List.map snd $4 in
               loc, ApplyType (id, args)
          }
        ;

struct_type_spec:
          struct_spec
          { let loc, id = $1 in
               loc, ApplyType (id, [])
          }
        | enum_spec
          { let loc, id = $1 in
               loc, ApplyType (id, [])
          }
        | uenum_spec
          { let loc, id = $1 in
               loc, ApplyType (id, [])
          }
        | tuple_spec
          { let loc, id = $1 in
               loc, ApplyType (id, [])
          }
        ;

/*
 * Tuple specification.
 */
tuple_spec:
          TokTuple opt_type_params tuple_declaration_list
          { let pos2, fields = $3 in
            let loc = union_loc $1 pos2 in
            let id = new_symbol_string "tuple" in
            let ty = make_ty_lambda loc $2 (make_tuple fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | tuple_spec_id tuple_declaration_list
          { let pos1, args, id = $1 in
            let pos2, fields = $2 in
            let loc = union_loc pos1 pos2 in
            let ty = make_ty_lambda loc args (make_tuple fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | tuple_spec_id
          { let loc, _, id = $1 in
               loc, id
          }
        ;

tuple_spec_id:
          TokTuple opt_type_params id
          { let pos2, id = $3 in
            let loc = union_loc $1 pos2 in
               loc, $2, id
          }
        ;

/*
 * Enumeration specification.
 */
enum_spec:
          TokEnum opt_type_params enum_declaration_list
          { let pos2, fields = $3 in
            let loc = union_loc $1 pos2 in
            let id = new_symbol_string "enum" in
            let ty = make_ty_lambda loc $2 (make_enum fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | enum_spec_id enum_declaration_list
          { let pos1, args, id = $1 in
            let pos2, fields = $2 in
            let loc = union_loc pos1 pos2 in
            let ty = make_ty_lambda loc args (make_enum fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | enum_spec_id
          { let loc, _, id = $1 in
               loc, id
          }
        ;

enum_spec_id:
          TokEnum opt_type_params id
          { let pos2, id = $3 in
            let loc = union_loc $1 pos2 in
               loc, $2, id
          }
        ;

/*
 * Monomorphic struct specification.
 */
uenum_spec:
          union_enum opt_type_params uenum_declaration_list
          { let pos1 = $1 in
            let pos2, fields = $3 in
            let loc = union_loc pos1 pos2 in
            let id = new_symbol_string "struct" in
            let ty = make_ty_lambda loc $2 (make_uenum id fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | uenum_spec_id uenum_declaration_list
          { let pos1, vars, id = $1 in
            let pos2, fields = $2 in
            let loc = union_loc pos1 pos2 in
            let ty = make_ty_lambda loc vars (make_uenum id fields loc) in
            let name = String.lowercase (Symbol.to_string id) in
	       Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | uenum_spec_id
          { let loc, vars, id = $1 in
               if vars <> [] then
                  raise (ParseError (loc, "illegal union enum parameters"));
               loc, id
          }
        ;

union_enum:
          TokEnum TokUnion
          { union_loc $1 $2 }
        | TokUnion TokEnum
          { union_loc $1 $2 }
        ;

uenum_spec_id:
          union_enum opt_type_params id
          { let pos1 = $1 in
            let pos2, id = $3 in
            let loc = union_loc pos1 pos2 in
               Fc_parse_state.add_type id loc (TypeUEnum (loc, StatusNormal, Label (new_symbol id, id)));
               loc, $2, id
          }
        ;

/*
 * Monomorphic struct specification.
 */
struct_spec:
          struct_or_union opt_type_params struct_declaration_list
          { let pos1, struct_flag = $1 in
            let pos2, fields = $3 in
            let loc = union_loc pos1 pos2 in
            let id = new_symbol_string "struct" in
            let ty = make_ty_lambda loc $2 (make_struct_or_union struct_flag fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | struct_spec_id struct_declaration_list
          { let pos1, struct_flag, vars, id = $1 in
            let pos2, fields = $2 in
            let loc = union_loc pos1 pos2 in
            let ty = make_ty_lambda loc vars (make_struct_or_union struct_flag fields loc) in
               Fc_parse_state.add_type id loc ty;
               loc, id
          }
        | struct_spec_id
          { let loc, struct_flag, vars, id = $1 in
               if vars <> [] then
                  raise (ParseError (loc, "illegal struct parameters"));
               loc, id
          }
        ;

struct_or_union:
          TokStruct
          { $1, StructCase }
        | TokUnion
          { $1, UnionCase }
        ;

struct_spec_id:
          struct_or_union opt_type_params id
          { let pos1, struct_flag = $1 in
            let pos2, id = $3 in
            let loc = union_loc pos1 pos2 in
            let ty =
               match struct_flag with
                  StructCase -> TypeStruct (loc, StatusNormal, Label (new_symbol id, id))
                | UnionCase -> TypeUnion (loc, StatusNormal, Label (new_symbol id, id))
            in
               Fc_parse_state.add_type id loc ty;
               loc, struct_flag, $2, id
          }
        ;

/************************************************************************
 * DECLARATORS
 ************************************************************************/

/*
 * Tuple field declarations.
 */
tuple_declaration_list:
          TokLeftBrace rev_tuple_decl_list TokRightBrace
          { let loc = union_loc $1 $3 in
            let fields = List.rev $2 in
               loc, fields
          }
        ;

rev_tuple_decl_list:
          tuple_decl
          { [$1] }
        | rev_tuple_decl_list tuple_decl
          { $2 :: $1 }
        ;

tuple_decl:
          simp_decl_spec TokSemi
          { snd $1 }
        | struct_decl_spec TokSemi
          { snd $1 }
        ;

/*
 * Enumeration field declarations.
 */
enum_declaration_list:
          TokLeftBrace TokRightBrace
          { union_loc $1 $2, [] }
        | TokLeftBrace rev_enum_decl_list opt_comma TokRightBrace
          { union_loc $1 $4, List.rev $2 }
        ;

rev_enum_decl_list:
          enum_decl
          { [$1] }
        | rev_enum_decl_list TokComma enum_decl
          { $3 :: $1 }
        ;

enum_decl:
          TokId
          { let loc, id = $1 in
               make_enum_decl id None loc
          }
        | TokId TokEq nc_expr
          { let loc, id = $1 in
            let loc = union_loc loc (loc_of_expr $3) in
               make_enum_decl id (Some $3) loc
          }
        ;

/*
 * Union enum field declarations.
 */
uenum_declaration_list:
          TokLeftBrace uenum_declaration_list_cat TokRightBrace
          { let loc = union_loc $1 $3 in
               loc, $2
          }
        ;

uenum_declaration_list_cat:
          /* empty */
          { [] }
        | uenum_declaration_list_cat uenum_declaration
          { $1 @ $2 }
        ;

uenum_declaration:
          simp_decl_spec uenum_decl_list TokSemi
          { make_uenum_ty_decls $1 $2 }
        | struct_decl_spec uenum_decl_list TokSemi
          { make_uenum_ty_decls $1 $2 }
        | TokId TokSemi
          { let loc, id = $1 in
               [loc, id, None]
          }
        ;

uenum_decl_list:
          rev_uenum_decl_list
          { List.rev $1 }
        ;

rev_uenum_decl_list:
          id_decl
          { [$1] }
        | rev_uenum_decl_list TokComma id_decl
          { $3 :: $1 }
        ;

/*
 * Structure field declarations.
 */
struct_declaration_list:
          TokLeftBrace struct_declaration_list_cat TokRightBrace
          { let loc = union_loc $1 $3 in
               loc, $2
          }
        ;

struct_declaration_list_cat:
          /* empty */
          { [] }
        | struct_declaration_list_cat struct_declaration
          { $1 @ $2  }
        ;

struct_declaration:
          simp_decl_spec struct_decl_list TokSemi
          { make_struct_decls $1 $2 }
        | struct_decl_spec struct_decl_list TokSemi
          { make_struct_decls $1 $2 }
        ;

struct_decl_list:
          rev_struct_decl_list
          { List.rev $1 }
        ;

rev_struct_decl_list:
          struct_decl
          { [$1] }
        | rev_struct_decl_list TokComma struct_decl
          { $3 :: $1 }
        ;

struct_decl:
          id_decl
          { $1, None }
        | TokColon nc_expr
          { let id = new_symbol_string "pad" in
               VarNameId ($1, Some id), Some $2
          }
        | id_decl TokColon nc_expr
          { $1, Some $3 }
        ;

/*
 * Variable definitions.
 */
var_defs:
          struct_decl_spec id_init_decl_list TokSemi
          { let store, ty = $1 in
            let loc = union_loc (loc_of_type ty) $3 in
               VarDefs (loc, make_patt_init_decls store ty $2)
          }
        | simp_decl_spec patt_init_decl_list TokSemi
          { let store, ty = $1 in
            let loc = union_loc (loc_of_type ty) $3 in
               VarDefs (loc, make_patt_init_decls store ty $2)
          }
        ;

/*
 * Declarator with an optional initializer.
 */
id_init_decl_list:
          rev_id_init_decl_list
          { List.rev $1 }
        ;

rev_id_init_decl_list:
          id_init_decl
          { [$1] }
        | rev_id_init_decl_list TokComma id_init_decl
          { $3 :: $1 }
        ;

id_init_decl:
          id_decl
          { $1, InitNone }
        | id_decl TokEq init
          { $1, $3 }
        ;

/*
 * Pattern declarator with an optional initializer.
 */
patt_init_decl_list:
          rev_patt_init_decl_list
          { List.rev $1 }
        ;

rev_patt_init_decl_list:
          patt_init_decl
          { [$1] }
        | rev_patt_init_decl_list TokComma patt_init_decl
          { $3 :: $1 }
        ;

patt_init_decl:
          patt_decl
          { $1, InitNone }
        | patt_decl TokEq init
          { $1, $3 }
        ;

/*
 * Type declarators may be quantified.
 */
type_decl_list:
          rev_type_decl_list
          { List.rev $1 }
        ;

rev_type_decl_list:
          type_decl
          { [$1] }
        | rev_type_decl_list TokComma type_decl
          { $3 :: $1 }
        ;

type_decl:
          type_direct_decl
          { $1 }
        | TokStar decl_struct_type_specifiers_opt type_decl
          { let loc = union_loc $1 (loc_of_var_name $3) in
               VarNamePointer (loc, $2, $3)
          }
        | TokAddr decl_struct_type_specifiers_opt type_decl
          { let loc, _ = $1 in
            let loc = union_loc loc (loc_of_var_name $3) in
               VarNameRef (loc, $2, $3)
          }
        ;

type_direct_decl:
          TokId
          { let loc, id = $1 in
               VarNameId (loc, Some id)
          }
        | TokPoly TokTypeId TokLeftParen param_list TokRightParen
          { let loc, id = $2 in
            let loc = union_loc $1 $5 in
            let args = List.map snd $4 in
               VarNameApply (loc, id, args)
          }
        | TokLeftParen type_decl TokRightParen
          { $2 }
        | type_direct_decl TokLeftBrack opt_expr TokRightBrack %prec prec_subscript
          { let loc = union_loc (loc_of_var_name $1) $4 in
               match $3 with
                  Some e -> VarNameArray (loc, [], $1, e)
                | None -> VarNamePointer (loc, [], $1)
          }
        | type_direct_decl TokLeftParen opt_param_list TokRightParen %prec prec_apply
          { let loc = union_loc (loc_of_var_name $1) $4 in
               VarNameFun (loc, $1, $3)
          }
        ;

/*
 * Pattern declarations.
 */
patt_decl:
          pattern
          { VarNamePattern (loc_of_pattern $1, $1) }
        | patt_decl2
          { $1 }
        ;

patt_decl2:
          patt_direct_decl
          { $1 }
        | TokStar decl_struct_type_specifiers_opt patt_decl2
          { let loc = union_loc $1 (loc_of_var_name $3) in
               VarNamePointer (loc, $2, $3)
          }
        | TokStar decl_struct_type_specifiers_opt TokId
          { let loc, id = $3 in
            let id = VarNameId (loc, Some id) in
            let loc = union_loc $1 loc in
               VarNamePointer (loc, $2, id)
          }
        | TokAddr decl_struct_type_specifiers_opt patt_decl2
          { let loc, _ = $1 in
            let loc = union_loc loc (loc_of_var_name $3) in
               VarNameRef (loc, $2, $3)
          }
        | TokAddr decl_struct_type_specifiers_opt TokId
          { let pos1, _ = $1 in
            let pos2, id = $3 in
            let id = VarNameId (pos2, Some id) in
            let loc = union_loc pos1 pos2 in
               VarNameRef (loc, $2, id)
          }
        ;

patt_direct_decl:
          TokLeftParen patt_decl2 TokRightParen
          { $2 }
        | patt_direct_decl TokLeftBrack opt_expr TokRightBrack %prec prec_subscript
          { let loc = union_loc (loc_of_var_name $1) $4 in
               match $3 with
                  Some e -> VarNameArray (loc, [], $1, e)
                | None -> VarNamePointer (loc, [], $1)
          }
        | patt_direct_decl TokLeftParen opt_param_list TokRightParen %prec prec_apply
          { let loc = union_loc (loc_of_var_name $1) $4 in
               VarNameFun (loc, $1, $3)
          }
        | TokId TokLeftBrack opt_expr TokRightBrack %prec prec_subscript
          { let loc, id = $1 in
            let id = VarNameId (loc, Some id) in
            let loc = union_loc loc $4 in
               match $3 with
                  Some e -> VarNameArray (loc, [], id, e)
                | None -> VarNamePointer (loc, [], id)
          }
        | TokId TokLeftParen opt_param_list TokRightParen %prec prec_apply
          { let loc, id = $1 in
            let id = VarNameId (loc, Some id) in
            let loc = union_loc loc $4 in
               VarNameFun (loc, id, $3)
          }
        ;

/*
 * Declarations that require an identifier.
 */
id_decl:  direct_decl
          { $1 }
        | TokStar decl_struct_type_specifiers_opt id_decl
          { let loc = union_loc $1 (loc_of_var_name $3) in
               VarNamePointer (loc, $2, $3)
          }
        | TokAddr decl_struct_type_specifiers_opt id_decl
          { let loc, _ = $1 in
            let loc = union_loc loc (loc_of_var_name $3) in
               VarNameRef (loc, $2, $3)
          }
        ;

direct_decl:
          TokId
          { let loc, id = $1 in
               VarNameId (loc, Some id)
          }
        | TokLeftParen id_decl TokRightParen
          { $2 }
        | direct_decl TokLeftBrack opt_expr TokRightBrack %prec prec_subscript
          { let loc = union_loc (loc_of_var_name $1) $4 in
               match $3 with
                  Some e -> VarNameArray (loc, [], $1, e)
                | None -> VarNamePointer (loc, [], $1)
          }
        | direct_decl TokLeftParen opt_param_list TokRightParen %prec prec_apply
          { let loc = union_loc (loc_of_var_name $1) $4 in
               VarNameFun (loc, $1, $3)
          }
        ;

/*
 * Function parameters.
 */
opt_param_list:
          /* empty */
          { [] }
        | param_list
          { $1 }
        ;

param_list:
          rev_param_list
          { List.rev $1 }
        ;

rev_param_list:
          param_decl
          { [$1] }
        | rev_param_list TokComma param_decl
          { $3 :: $1 }
        ;

param_decl:
          simp_decl_spec patt_decl
          { make_param_decl (snd $1) $2 }
        | struct_decl_spec id_decl
          { make_param_decl (snd $1) $2 }
        | simp_decl_spec abstract_decl
          { make_param_decl (snd $1) $2 }
        | struct_decl_spec abstract_decl
          { make_param_decl (snd $1) $2 }
        | simp_decl_spec
          { None, snd $1 }
        | struct_decl_spec
          { None, snd $1 }
        | TokElide
          { None, TypeElide $1 }
        ;

cast_decl:
          simp_decl_spec abstract_decl
          { make_param_decl (snd $1) $2 }
        | struct_decl_spec abstract_decl
          { make_param_decl (snd $1) $2 }
        | decl_spec
          { None, snd $1 }
        ;

fun_param_list:
          TokLeftParen opt_param_list TokRightParen
          { let loc = union_loc $1 $3 in
               loc, $2
          }
        ;

abstract_decl:
          direct_abstract_decl
          { $1 }
        | TokStar decl_struct_type_specifiers_opt
          { let loc = $1 in
               VarNamePointer (loc, $2, VarNameId (loc, None))
          }
        | TokStar decl_struct_type_specifiers_opt abstract_decl
          { let loc = union_loc $1 (loc_of_var_name $3) in
               VarNamePointer (loc, $2, $3)
          }
        | TokAddr decl_struct_type_specifiers_opt
          { let loc, _ = $1 in
               VarNameRef (loc, $2, VarNameId (loc, None))
          }
        | TokAddr decl_struct_type_specifiers_opt abstract_decl
          { let loc, _ = $1 in
            let loc = union_loc loc (loc_of_var_name $3) in
               VarNameRef (loc, $2, $3)
          }
        ;

direct_abstract_decl:
          TokLeftParen abstract_decl TokRightParen
          { $2 }
        | direct_abstract_decl TokLeftBrack opt_expr TokRightBrack %prec prec_subscript
          { let loc = union_loc (loc_of_var_name $1) $4 in
               match $3 with
                  Some e -> VarNameArray (loc, [], $1, e)
                | None -> VarNamePointer (loc, [], $1)
          }
        | TokLeftBrack opt_expr TokRightBrack %prec prec_subscript
          { let loc = union_loc $1 $3 in
               match $2 with
                  Some e -> VarNameArray (loc, [], VarNameId (loc, None), e)
                | None -> VarNamePointer (loc, [], VarNameId (loc, None))
          }
        | direct_abstract_decl fun_param_list %prec prec_apply
          { let loc, params = $2 in
            let loc = union_loc (loc_of_var_name $1) loc in
               VarNameFun (loc, $1, params)
          }
        | fun_param_list %prec prec_apply
          { let loc, params = $1 in
               VarNameFun (loc, VarNameId (loc, None), params)
          }
        ;

/*
 * Declare mutually recursive functions.
 */
fun_def:  simp_decl_spec TokId TokLeftParen opt_param_list TokRightParen TokLeftBrace stmt_list TokRightBrace
          { let loc, id = $2 in
            let id = VarNameId (loc, Some id) in
            let loc = union_loc loc $5 in
            let id = VarNameFun (loc, id, $4) in
            let loc = union_loc $6 $8 in
            let body = wrap_stmt (SeqExpr (loc, $7)) in
               make_fun_def $1 id body
          }
        | struct_decl_spec id_decl TokLeftBrace stmt_list TokRightBrace
          { let loc = union_loc $3 $5 in
            let body = wrap_stmt (SeqExpr (loc, $4)) in
               make_fun_def $1 $2 body
          }
        ;

/*
 * Declare some mutually recursive types.
 */
type_defs:
          TokTypedef decl_spec type_decl_list TokSemi
          { let loc = union_loc $1 $4 in
            let types = make_type_decls (snd $2) $3 in
               List.iter (fun (loc, v, _, ty) ->
                   Fc_parse_state.add_type v loc ty;
	           Fc_parse_state.add_typedef v) types;
               TypeDefs (loc, [])
          }
        | simp_decl_spec TokSemi
          { (* Ignore them *)
            TypeDefs (loc_of_type (snd $1), [])
          }
        | struct_decl_spec TokSemi
          { (* Ignore them *)
            TypeDefs (loc_of_type (snd $1), [])
          }
        ;

/************************************************************************
 * EXPRESSIONS
 ************************************************************************/

/*
 * Initialization expr.
 * This _constructs_ the data.
 */
init:
         nc_expr
         { InitExpr (loc_of_expr $1, $1) }
       | TokLeftBrace rev_init_list opt_comma TokRightBrace
         { let loc = union_loc $1 $4 in
              InitArray (loc, List.rev $2)
         }
       ;

init_field:
         init
         { None, $1 }
       | id TokColon init
         { Some (snd $1), $3 }
       ;

rev_init_list:
         init_field
         { [$1] }
       | rev_init_list TokComma init_field
         { $3 :: $1 }
       ;

/*
 * Patterns.
 * This _destructs_ the data.
 * Patterns can be used in switch statements.
 */
pattern:
          TokChar
          { let loc, c = $1 in
               CharPattern (loc, c)
          }
        | TokInt
          { let loc, i = $1 in
               IntPattern (loc, i)
          }
        | TokFloat
          { let loc, x = $1 in
               FloatPattern (loc, x)
          }
        | TokString
          { let loc, pre, s = $1 in
               StringPattern (loc, pre, s)
          }
        | TokLeftBrace rev_struct_field_pattern_list opt_comma TokRightBrace
          { let loc = union_loc $1 $4 in
               StructPattern (loc, List.rev $2)
          }
        | TokId
          { let loc, v = $1 in
               VarPattern (loc, v, v, None)
          }
        | TokId pattern
          { let loc, id = $1 in
            let loc = union_loc loc (loc_of_pattern $2) in
               EnumPattern (loc, id, $2)
          }
        | TokLeftParen pattern TokRightParen
          { $2 }
        ;

rev_struct_field_pattern_list:
          struct_field_pattern
          { [$1] }
        | rev_struct_field_pattern_list TokComma struct_field_pattern
          { $3 :: $1 }
        ;

struct_field_pattern:
          pattern
          { None, $1 }
        | id TokColon pattern
          { Some (snd $1), $3 }
        ;

/*
 * Expressions.
 * Note:
 *   To avoid a shift/reduce conflict between
 *   function application and array creation, we allow
 *   arbitrary function names
 *       expr ( arglist )
 *   Since the grammar allows only
 *       id ( arglist )
 *   we check in the semantic action for the error.
 */
expr:
          nc_expr
          { $1 }
        | expr TokComma nc_expr
          { make_binop ($2, comma_sym) $1 $3 }
        ;

nc_expr:  TokChar
          { let loc, c = $1 in
               CharExpr (loc, c)
          }
        | TokInt
          { let loc, i = $1 in
               IntExpr (loc, i)
          }
        | TokFloat
          { let loc, x = $1 in
               FloatExpr (loc, x)
          }
        | TokString
          { let loc, pre, s = $1 in
               StringExpr (loc, pre, s)
          }
        | TokId
          { let loc, v = $1 in
               VarExpr (loc, v, v)
          }
        | TokOperator operator
          { let loc, v = $2 in
            let loc = union_loc $1 loc in
               VarExpr (loc, v, v)
          }

          /* Unary operations */
        | TokAddr nc_expr %prec prec_unary
          { make_unop PreOp $1 $2 }
        | TokOp10 nc_expr %prec prec_unary
          { make_unop PreOp $1 $2 }
        | TokStar nc_expr %prec prec_unary
          { make_unop PreOp ($1, star_sym) $2 }
        | TokPreOp12 nc_expr %prec prec_unary
          { make_unop PreOp $1 $2 }
        | TokPrePostOp12 nc_expr %prec prec_unary
          { make_unop PreOp $1 $2 }
        | nc_expr TokPrePostOp12 %prec prec_unary
          { make_unop PostOp $2 $1 }

          /* Binary operations */
        | nc_expr TokEq nc_expr
          { make_binop ($2, eq_sym) $1 $3 }
        | nc_expr TokOp1 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokAnd nc_expr
          { make_and_op $1 $3 }
        | nc_expr TokOr nc_expr
          { make_or_op $1 $3 }
        | nc_expr TokOp4 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokOp5 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokAddr nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokOp7 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokOp8 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokOp9 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokOp10 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokStar nc_expr
          { make_binop ($2, star_sym) $1 $3 }
        | nc_expr TokOp11 nc_expr
          { make_binop $2 $1 $3 }
        | nc_expr TokDot id
          { make_project $2 $1 $3 }
        | nc_expr TokOp13 id
          { make_project $2 (make_unop PreOp $2 $1) $3 }

          /* Parens */
        | TokLeftParen expr TokRightParen
          { $2 }
        | TokLeftParen stmt TokRightParen
          { $2 }

          /* Other misc operations */
        | TokSizeof nc_expr %prec prec_unary
          { let loc = union_loc $1 (loc_of_expr $2) in
               SizeofExpr (loc, $2)
          }
        | TokSizeof TokLeftParen cast_decl TokRightParen %prec prec_unary
          { let loc = union_loc $1 $4 in
            let _, ty = $3 in
               SizeofType (loc, ty)
          }
        | TokLeftParen cast_decl TokRightParen nc_expr %prec prec_cast
          { let loc = union_loc $1 (loc_of_expr $4) in
            let _, ty = $2 in
               CastExpr (loc, ty, $4)
          }
        | nc_expr TokLeftBrack nc_expr TokRightBrack %prec prec_subscript
          { let loc = union_loc (loc_of_expr $1) $4 in
               make_binop ($2, subscript_sym) $1 $3
          }
        | nc_expr TokLeftParen args TokRightParen %prec prec_apply
          { let loc = union_loc (loc_of_expr $1) $4 in
               make_op loc PreOp apply_sym ($1 :: $1 :: $3)
          }
        | nc_expr TokQuest nc_expr TokColon nc_expr
          { let loc = union_loc (loc_of_expr $1) (loc_of_expr $5) in
               IfExpr (loc, $1, $3, Some $5)
          }
        ;

/*
 * An optional expression.
 */
opt_expr: /* empty */
          { None }
        | expr
          { Some $1 }
        ;

/*
 * A statement is a terminated expression.
 */
stmt:     TokSemi
          { SeqExpr ($1, []) }
        | expr TokSemi
          { $1 }
        | TokIf TokLeftParen expr TokRightParen stmt %prec prec_ifthen
          { let loc = union_loc $1 (loc_of_expr $5) in
            let stmt = wrap_stmt $5 in
               IfExpr (loc, $3, stmt, None)
          }
        | TokIf TokLeftParen expr TokRightParen stmt TokElse stmt %prec prec_ifthenelse
          { let loc = union_loc $1 (loc_of_expr $7) in
            let stmt1 = wrap_stmt $5 in
            let stmt2 = wrap_stmt $7 in
               IfExpr (loc, $3, stmt1, Some stmt2)
          }
        | TokSwitch TokLeftParen expr TokRightParen stmt
          { let loc = union_loc $1 (loc_of_expr $5) in
            let cases = normalize_cases loc $5 in
               SwitchExpr (loc, $3, cases)
          }
        | TokFor TokLeftParen opt_expr TokSemi opt_expr TokSemi opt_expr TokRightParen stmt
          { let loc = union_loc $1 (loc_of_expr $9) in
            let init = make_opt_expr $3 (one_expr $4) in
            let test = make_opt_expr $5 (one_expr $6) in
            let step = make_opt_expr $7 (one_expr $8) in
            let body = wrap_stmt $9 in
               ForExpr (loc, init, test, step, body)
          }
        | TokWhile TokLeftParen expr TokRightParen stmt
          { let loc = union_loc $1 (loc_of_expr $5) in
            let body = wrap_stmt $5 in
               WhileExpr (loc, $3, body)
          }
        | TokDo stmt TokWhile TokLeftParen expr TokRightParen TokSemi
          { let loc = union_loc $1 $7 in
            let body = wrap_stmt $2 in
               DoExpr (loc, body, $5)
          }
        | TokReturn expr TokSemi
          { let loc = union_loc $1 (loc_of_expr $2) in
               ReturnExpr (loc, $2)
          }
        | TokReturn TokSemi
          { let loc = union_loc $1 $2 in
            let e = SeqExpr (loc, []) in
               ReturnExpr (loc, e)
          }
        | TokRaise expr TokSemi
          { let loc = union_loc $1 (loc_of_expr $2) in
               RaiseExpr (loc, $2)
          }
        | TokBreak TokSemi
          { let loc = union_loc $1 $2 in
               BreakExpr loc
          }
        | TokContinue TokSemi
          { let loc = union_loc $1 $2 in
               ContinueExpr loc
          }
        | TokGoto id TokSemi
          { let pos2, label = $2 in
            let loc = union_loc $1 pos2 in
               GotoExpr (loc, label)
          }
        | open_block stmt_list close_block
          { let loc, defs = $3 in
            let loc = union_loc $1 loc in
            let body =
               match defs with
                  [] -> $2
                | _ -> TypeDefs (loc, defs) :: $2
            in
               SeqExpr (loc, body)
          }
        | all_def
          { $1 }
        | id TokColon
          { let loc, id = $1 in
            let loc = union_loc loc $2 in
               LabelExpr (loc, id)
          }
        | TokCase pattern TokColon
          { let loc = union_loc $1 $3 in
               CaseExpr (loc, $2)
          }
        | TokDefault TokColon
          { let loc = union_loc $1 $2 in
               DefaultExpr loc
          }
        | TokTry stmt
          { let loc = union_loc $1 (loc_of_expr $2) in
               TryExpr (loc, $2, [], None)
          }
        | stmt TokCatch TokLeftParen pattern TokRightParen stmt %prec TokCatch
          { let loc = union_loc (loc_of_expr $1) (loc_of_expr $6) in
               match $1 with
                  TryExpr (loc, e, cases, finally) ->
                     let cases = cases @ [$4, [$6; BreakExpr loc]] in
                        TryExpr (loc, e, cases, finally)
                | _ ->
                     raise (ParseExpError (loc, "illegal catch", $1))
          }
        | stmt TokFinally stmt
          { let loc = union_loc (loc_of_expr $1) (loc_of_expr $3) in
               match $1 with
                  TryExpr (loc, e, cases, finally) ->
                     let finally =
                        match finally with
                           Some _ -> raise (ParseError (loc, "duplicate finally"))
                         | None -> Some $3
                     in
                        TryExpr (loc, e, cases, finally)
                | _ ->
                     raise (ParseExpError (loc, "illegal finally", $1))
          }
        ;

stmt_list:
          rev_stmt_list
          { List.rev $1 }
        ;

rev_stmt_list:
          /* empty */
          { [] }
        | rev_stmt_list stmt
          { $2 :: $1 }
        ;

/*
 * Environment pushing.
 */
open_block:
          TokLeftBrace
          { Fc_parse_state.push_tenv ();
            $1
          }
        ;

close_block:
          TokRightBrace
          { $1, Fc_parse_state.pop_tenv () }
        ;

/*
 * Arguments are comma-separated list of expressions.
 */
args:     /* empty */
          { [] }
        | rev_args
          { List.rev $1 }
        ;

rev_args: nc_expr
          { [$1] }
        | rev_args TokComma nc_expr
          { $3 :: $1 }
        ;

/*
 * Identifiers.
 */
id:       TokId
          { $1 }
        | TokTypeId
          { $1 }
        ;

/*
 * Generic operators.
 */
operator:
          TokComma
          { $1, comma_sym }
        | TokEq
          { $1, eq_sym }
        | TokOp1
          { $1 }
        | TokAnd
          { $1, and_sym }
        | TokOr
          { $1, or_sym }
        | TokOp4
          { $1 }
        | TokOp5
          { $1 }
        | TokAddr
          { $1 }
        | TokOp7
          { $1 }
        | TokOp8
          { $1 }
        | TokOp9
          { $1 }
        | TokOp10
          { $1 }
        | TokStar
          { $1, star_sym }
        | TokOp11
          { $1 }
        | TokPreOp12
          { $1 }
        | TokPrePostOp12
          { $1 }
        | TokOp13
          { $1 }
        ;

/*
 * Optional comma.
 */
opt_comma:
          /* empty */
          { () }
        | TokComma
          { () }
        ;
