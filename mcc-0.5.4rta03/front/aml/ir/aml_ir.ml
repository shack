(*
 * Functional Intermediate Representation.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Location
open Attribute
open Field_table

open Aml_set

(************************************************************************
 * SYMBOLS
 ************************************************************************)

(*
 * Inherit some types.
 *)
type ext_var   = Aml_tast.ext_var

type mt_var    = Aml_tast.mt_var
type me_var    = Aml_tast.me_var
type ty_var    = Aml_tast.ty_var
type var       = Aml_tast.var
type const_var = Aml_tast.const_var
type tag_var   = Aml_tast.tag_var
type label     = Aml_tast.label

(************************************************************************
 * Names and sets
 ************************************************************************)

(*
 * Sets are used for Match statements.
 *)
type int_set    = IntSet.t
type char_set   = CharSet.t
type string_set = StringSet.t
type const_set  = SymbolSet.t

type set =
   CharSet of char_set
 | IntSet of int_set
 | ConstSet of ty_var * const_set

(*
 * An object of union type also includes a field selection
 * that narrows the possible tags the object can have.
 *    IndexAny: nothing is known about which case the object is
 *    IndexPos label: the object has this tag
 *    IndexNeg labels: the object does _not_ have these tags
 *)
type union_index =
   AnyIndex
 | PosIndex of const_var
 | NegIndex of const_set

(************************************************************************
 * SUBSTITUTION                                                         *
 ************************************************************************)

(*
 * Free vars.
 *)
and free_vars =
   { fv_fo_vars   : SymbolSet.t;
     fv_vars      : SymbolSet.t
   }

(*
 * For terms that don't subst, use this form.
 *)
type 'core no_subst =
   { ns_loc : loc;
     ns_core : 'core
   }

(*
 * Substitution.
 *)
type subst =
   { (*
      * Type variables get replaced with actual types.
      *)
     sub_fo_vars  : ty SymbolTable.t;

     (*
      * If this is set, we're supposed to replace every
      * location in the term with this one.
      *)
     sub_loc      : loc option
   }

(*
 * Delayed substitutions.
 *)
and 'core delayed_subst = (loc, free_vars, subst, 'core) term

(************************************************************************
 * TYPES                                                                *
 ************************************************************************)

and ty = ty_core delayed_subst

and ty_core =
   (* Base types *)
   TyVoid
 | TyInt
 | TyChar
 | TyString
 | TyFloat

   (* Functions *)
 | TyFun of ty list * ty

   (* Tuples *)
 | TyTuple of ty list
 | TyArray of ty

   (* Dependent tuples, used for exceptions *)
 | TyTag of ty_var * ty list
 | TyDTuple of ty_var * ty list option

   (* Parameterization *)
 | TyVar of ty_var
 | TyAll of ty_var list * ty
 | TyExists of ty_var list * ty
 | TyProject of var * int

   (*
    * The other type applications are used to
    * refer to top-level type definitions.  They
    * have _two_ sets of arguments.  See the comment
    * in tydef_core below.
    *)
 | TyApply  of ty_var * ty list
 | TyUnion  of ty_var * ty list * union_index
 | TyRecord of ty_var * ty list
 | TyFrame  of ty_var * ty list
 | TyModule of ty_var * ty list

   (* External types are used for foreign objects *)
 | TyExternal of ty * string

   (* Format strings *)
 | TyFormat of ty * ty * ty

(*
   (* Ambients *)
 | TyOpen of (var * ty) list
 *)

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Unions have external and internal names.
 *)
type union_field = ty list
type union_fields = union_field FieldTable.t

(*
 * Record fields have names,
 * and they may be mutable.
 *)
type record_field  = bool * ty
type record_fields = record_field FieldTable.t

(*
 * Modules are like records, but they contain a naming
 * table that maps external to internal label names.
 *)
type module_names = var FieldTable.t
type module_fields = ty SymbolTable.t

(*
 * Type definitions.
 *
 * These few types are lifted to top-level, including
 * any parameterized types, unions, records, and module types.
 * These _are not_ types, but there are types that correspond
 * to them:
 *
 *    TyDefLambda     <-->    TyApply
 *    TyDefUnion      <-->    TyUnion
 *    TyDefRecord     <-->    TyRecord
 *    TyDefModule     <-->    TyModule
 *
 * The reason why these types, and not others, are lifted is
 * that these types contain names, and it is easier to deal
 * with the naming issue when the types are all in one place.
 *
 * Note that top-level types can't contain module names, because
 * they must be closed (see the comment about tydef_core below).
 *)
type tydef_inner_core =
   TyDefLambda  of ty
 | TyDefUnion   of union_fields
 | TyDefDTuple  of ty_var
 | TyDefRecord  of record_fields
 | TyDefFrame   of record_fields
 | TyDefModule  of module_names * module_fields

type tydef_inner = tydef_inner_core delayed_subst

(*
 * The core type definitions have two sets of parameters.
 * The tydef_fo_vars are normal type parameters (kind = 0), and
 * tydef_so_vars are higher-order type constructors.
 *
 * However, the tydef_so_vars are generally used by type
 * closure conversion (implemented in Aml_tast_ast) to replace
 * module names in the type.  Top-level type definitions must be
 * closed; they cannot contain module or variable names.  Each
 * form of type application supplies two lists of type arguments,
 * one for the normal parameters, and one for the names.
 * The main reason to split them is that it is easier to
 * keep track of the parameters this way.
 *)
type tydef =
   { tydef_fo_vars : ty_var list;
     tydef_inner   : tydef_inner
   }

(************************************************************************
 * OPERATORS
 ************************************************************************)

(*
 * Unary operators.
 *)
type unop =
   (* Booleans *)
   NotBoolOp

   (* Arithmetic *)
 | UMinusIntOp
 | NotIntOp
 | AbsIntOp

   (* Floats *)
 | UMinusFloatOp
 | AbsFloatOp
 | SqrtFloatOp
 | ExpFloatOp
 | LogFloatOp
 | Log10FloatOp
 | CosFloatOp
 | SinFloatOp
 | TanFloatOp
 | ACosFloatOp
 | ASinFloatOp
 | ATanFloatOp
 | CosHFloatOp
 | SinHFloatOp
 | TanHFloatOp
 | CeilFloatOp
 | FloorFloatOp

   (* Coercions *)
 | CharOfIntOp
 | IntOfCharOp
 | IntOfFloatOp
 | FloatOfIntOp

   (* Block operations *)
 | LengthOfStringOp
 | LengthOfBlockOp of ty

(*
 * Binary operators.
 *)
type binop =
   (* Booleans (not shoirt circuit) *)
   OrBoolOp
 | AndBoolOp

   (* Comparisons on ML ints *)
 | EqCharOp
 | NeqCharOp
 | LtCharOp
 | LeCharOp
 | GtCharOp
 | GeCharOp
 | CmpCharOp                (* Similar to ML's ``compare'' function. *)

   (* Standard binary operations on ML ints *)
 | PlusIntOp
 | MinusIntOp
 | MulIntOp
 | DivIntOp
 | RemIntOp
 | LslIntOp
 | LsrIntOp
 | AsrIntOp
 | AndIntOp
 | OrIntOp
 | XorIntOp
 | MaxIntOp
 | MinIntOp

   (* Comparisons on ML ints *)
 | EqIntOp
 | NeqIntOp
 | LtIntOp
 | LeIntOp
 | GtIntOp
 | GeIntOp
 | CmpIntOp                (* Similar to ML's ``compare'' function. *)

   (* Standard binary operations on floats *)
 | PlusFloatOp
 | MinusFloatOp
 | MulFloatOp
 | DivFloatOp
 | RemFloatOp
 | MaxFloatOp
 | MinFloatOp
 | PowerFloatOp
 | ATan2FloatOp
 | LdExpFloatIntOp

   (* Comparisons on floats *)
 | EqFloatOp
 | NeqFloatOp
 | LtFloatOp
 | LeFloatOp
 | GtFloatOp
 | GeFloatOp
 | CmpFloatOp

   (* Pointer (in)equality.  Arguments must have the given type *)
 | EqEqOp of ty
 | NeqEqOp of ty

(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)

(*
 * Normal values.
 *)
type atom =
   (* Constants and variables *)
   AtomInt of int
 | AtomChar of char
 | AtomFloat of float
 | AtomVar of var
 | AtomNil of ty

   (* Type operations *)
 | AtomTyConstrain of atom * ty
 | AtomTyApply of atom * ty * ty list
 | AtomTyPack of var * ty * ty list
 | AtomTyUnpack of var

   (* Constant union values *)
 | AtomConst of ty * ty_var * const_var

   (* Record labels *)
 | AtomFrameLabel  of ty_var * label
 | AtomRecordLabel of ty_var * label
 | AtomModuleLabel of ty_var * ext_var

   (* Arithmetic *)
 | AtomUnop of unop * atom
 | AtomBinop of binop * atom * atom

(*
 * Allocation values.
 *
 * The Tuple, Union, and Record versions can be polymorphic.
 * The ty_var list are binding occurrences.
 *)
type alloc_op =
   AllocTuple  of ty_var list * ty * atom list
 | AllocUnion  of ty_var list * ty * ty_var * const_var * atom list
 | AllocRecord of ty_var list * ty * ty_var * (label * atom) list
 | AllocDTuple of ty * ty_var * atom * atom list
 | AllocArray  of ty * atom list
 | AllocVArray of ty * atom * atom
 | AllocModule of ty * ty_var * atom SymbolTable.t
 | AllocFrame  of ty * ty_var * ty list

(*
 * All the expressions.
 *
 * LetApply (v, f, args, e)
 *    1. function f may be a constant or a closure
 *    2. A function call f(args) may result in multiple function calls
 *       For example:
 *
 *          let f a b =
 *             let g c d e =
 *                c + d + e
 *             in
 *                 g(a, b)
 *          in
 *          let v = f(1, 2, 3) in
 *             return v
 *
 *       The call f(1, 2, 3) will cause two function calls, as in:
 *          let v' = f(1, 2) in
 *          let v = v'(3) in
 *             return v
 *
 * TailCall (f, args)
 *    f is a a constant, or a closure with exactly (List.length args)
 *    remaining before actual application.
 *
 * Sub-language properties:
 *
 * FIR_partial:
 *    All LetApply are partial
 *
 * FIR_cps = FIR_partial +
 *    No TailCall or Return
 *
 * FIR_closure = FIR_partial +
 *    All functions are closed
 *)
type exp = exp_core delayed_subst

and exp_core =
   (* Standard operations *)
   LetAtom of var * ty * atom * exp
 | LetExt of var * ty * string * ty * ty list * atom list * exp
 | TailCall of atom * atom list

   (* Simplified pattern matching *)
 | Match of atom * (set * exp) list

   (* Dependent tuple matching *)
 | MatchDTuple of atom * (atom option * exp) list

   (* Allocation *)
 | LetAlloc of var * alloc_op * exp

   (* Projections *)
 | SetSubscript of atom * atom * atom * ty * exp
 | LetSubscript of var * ty * atom * atom * exp

   (* Global values *)
 | SetGlobal of var * ty * atom * exp

   (* These are eliminated in closure conversion *)
 | LetRec of letrecs * exp
 | LetFuns of fundef list * exp
 | LetClosure of var * atom * ty list * atom list * exp
 | LetApply of var * ty * atom * atom list * exp
 | Return of atom

   (* Control flow *)
 | Try of exp * var * exp
 | Raise of atom * ty

(*
   (* Ambient operations *)
 | LetName of var * ty * exp
 | Ambient of var * var * exp
 | Thread  of var * exp
 | Migrate of var * atom * exp
*)

(*
 * A function definition has:
 *    1. a name
 *    2. its type
 *    3. type arguments
 *    4. its arguments
 *    5. the body.
 *
 * FunGlobalClass: this is a source-level function, and it may escape.
 *    Before cps it is only called with LetApply.
 * FunTopLevelClass: this is a source top-level function, and it may escape.
 *    Before cps it is only called with LetApply.
 * FunPartialClass: before cps, this is called only with LetApply.
 *    It is used to identify helper functions (like the currying functions
 *    generated by Aml_ir_partial).  It has the properties of a global
 *    function, but it should be inlined whenever possible.
 * FunContClass: this is a continuation function
 * FunLocalClass: this is an internally-generated function, it never escapes,
 *    and it is called only with TailCall.
 *)
and fun_class =
   FunGlobalClass
 | FunTopLevelClass
 | FunPartialClass
 | FunContClass
 | FunLocalClass

and fundef  = var * loc * fun_class * ty_var list * ty * var list * exp
and gfundef = loc * fun_class * ty_var list * ty * var list * exp

(*
 * A letrec-block contains recursive variable
 * definitions.
 *)
and letrecs = (var * exp * ty) list

(************************************************************************
 * PROGRAMS
 ************************************************************************)

(*
 * Type information.
 *)
type boot =
   { boot_ty_unit     : ty_core;
     boot_ty_unit_var : ty_var;
     boot_unit_var    : const_var;

     boot_ty_bool     : ty_core;
     boot_ty_bool_var : ty_var;
     boot_true_var    : const_var;
     boot_false_var   : const_var;
     boot_true_set    : set;
     boot_false_set   : set;

     boot_ty_exn      : ty_core;
     boot_ty_exn_var  : ty_var;
     boot_match_failure_var : const_var
   }

(*
 * Global initializer.
 *)
type init =
   InitString of string
 | InitAlloc of alloc_op
 | InitAtom of atom

(*
 * The bool is set if the value is volatile.
 *)
type global = ty * init

type import =
   { import_type : ty;
     import_info : Fir.import_info
   }

type export =
   { export_name : ext_var;
     export_type : ty
   }

(*
 * Program:
 *    prog_names: map from internal to external names
 *    prog_types: top-level type definitions
 *    prog_import: external modules
 *       the hash is a version number
 *       the me_var is the internal name of the module
 *       the ty is the type of the module
 *    prog_items: the top-level struct definitions in the program
 *
 *    prog_ty_unit, prog_ty_bool: names of pervasive types
 *)
type hash = int

type prog =
   { prog_name    : me_var;
     prog_names   : ext_var SymbolTable.t;
     prog_types   : tydef SymbolTable.t;
     prog_globals : global SymbolTable.t;
     prog_tags    : (ty_var * ty list) SymbolTable.t;
     prog_import  : import SymbolTable.t;
     prog_export  : export SymbolTable.t;
     prog_funs    : gfundef SymbolTable.t;
     prog_boot    : boot
   }

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
