(*
 * Base environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_ir
open Aml_ir_env_type

(*
 * Get the pervasive types.
 *)
val genv_ty_unit    : genv -> loc -> ty
val genv_unit_atom  : genv -> loc -> atom

val genv_ty_bool    : genv -> loc -> ty
val genv_true_atom  : genv -> loc -> atom
val genv_false_atom : genv -> loc -> atom
val genv_true_set   : genv -> set
val genv_false_set  : genv -> set

val genv_ty_exn     : genv -> loc -> ty
val genv_exn_var    : genv -> ty_var
val genv_match_failure_var : genv -> const_var

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
