(*
 * The "global" environment.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Location
open Field_table

open Aml_ir

(*
 * A variable environment gives the type of each variable.
 *)
type venv = ty SymbolTable.t

(*
 * The "names" record maps external (AST) names to
 * internal names.  This is just like standardization:
 * for each original variable in the environment, there
 * is a corresponding internal name, and <em>all</em>
 * the internal names are different.
 *)
type mt_names =
   { names_ty     : ty_var SymbolTable.t;
     names_var    : var    FieldTable.t
   }

(*
 * A type value is a function, and it may
 * be abstract.
 *)
type tyfun = ty_var list * Aml_tast.ty option

(*
 * Translucent sums represent module types.
 * They are just like records, but they have type fields.
 * The module has some abstract types, some defined types,
 * and some variables.
 *
 * Next, the types and vars use internal names.  We have
 * to keep track of their original (external) names so
 * that we can translate labels from the AST.
 *)
type module_type =
   { mt_types        : tyfun SymbolTable.t;
     mt_vars         : var_type SymbolTable.t;
     mt_names        : mt_names
   }

(*
 * A variable can be one of several kinds.
 *    VarTypeNormal ty: a normal variable with type ty
 *
 *    VarTypeModule mt: a module with type mt
 *
 *    VarTypeFun (vars, ty): a type function (like 'a list)
 *
 *    VarTypeFunctor (v, ty_arg, ty_res): a functor variable
 *       The translation of the ty_res is delayed.
 *       To figure out the return type of the functor,
 *       bind a specific argument to ty_var, then translate
 *       the result type.
 *)
and var_type =
   VarTypeNormal of ty
 | VarTypeDelayed of Aml_tast.ty
 | VarTypeFun of tyfun
 | VarTypeModule of ty * module_type
 | VarTypeFunctor of ty * ty_var * var_type * Aml_tast.ty

(*
 * The variable environment gives the type
 * of each variable.
 *)
type tenv =
   { tenv_tydefs  : Aml_tast.tydef SymbolTable.t;
     tenv_vars    : var_type SymbolTable.t;
     tenv_fo_vars : var_type SymbolTable.t;
     tenv_so_vars : tyfun SymbolTable.t;
     tenv_names   : mt_names
   }

(*
 * The global environment is a lot like a program.
 *
 *     genv_types: type definitions
 *     genv_globals: global definitions (collected during translation)
 *     genv_type_memo: cached type translations
 *     genv_boot: the standard definitions for unit, bool, exn
 *)
type genv =
   { genv_types     : tydef SymbolTable.t;
     genv_ty_apply  : (loc -> ty list -> ty) Aml_tast_type.TyApplyTable.t;
     genv_globals   : global SymbolTable.t;
     genv_tags      : (ty_var * ty list) SymbolTable.t;
     genv_boot      : boot
   }

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
