(*
 * CPS conversion.
 * Each function gets four new arguments:
 *     raise_fun: the function to raise an exception
 *     raise_env: the environment for the raise function
 *     cont_fun: the function to call the continuation
 *     cont_env: the environment for the continuation
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol
open Field_table

open Fir_state

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_env
open Aml_ir_exn
open Aml_ir_type
open Aml_ir_print
open Aml_ir_standardize

module Pos = MakePos (struct let name = "Aml_ir_cps" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Names for the raise and cont vars.
 *)
let raise_var  = new_symbol_string "raise"
let exit_var   = new_symbol_string "cont"

(*
 * For naming.
 *)
let rec string_of_atom a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _ ->
         "v"
    | AtomNil _ ->
         "nil"
    | AtomVar v
    | AtomConst (_, _, v)
    | AtomFrameLabel (_, v)
    | AtomRecordLabel (_, v)
    | AtomModuleLabel (_, v)
    | AtomTyPack (v, _, _)
    | AtomTyUnpack v ->
         Symbol.to_string v
    | AtomTyConstrain (a, _)
    | AtomTyApply (a, _, _)
    | AtomUnop (_, a)
    | AtomBinop (_, a, _) ->
         string_of_atom a

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Translate a type.
 * The only types that change are function types.
 *)
let rec cps_type genv ty =
   let loc = loc_of_type ty in
   let pos = string_pos "cps_type" (type_exp_pos ty) in
   let ty = dest_type_core ty in
   let ty = cps_type_core genv pos loc ty in
   let ty = make_type loc ty in
      ty

and cps_types genv tyl =
   List.map (cps_type genv) tyl

and cps_types_opt genv tyl_opt =
   match tyl_opt with
      Some tyl -> Some (cps_types genv tyl)
    | None -> None

and cps_type_core genv pos loc ty =
   match ty with
      TyVoid
    | TyInt
    | TyChar
    | TyString
    | TyFloat
    | TyVar _
    | TyProject _ ->
         ty

    | TyFun (tyl, ty) ->
         let ty = cps_type genv ty in
         let tyl = cps_types genv tyl in

         (* Add the extra arguments *)
         let ty_void = make_type loc TyVoid in
         let ty_exn = genv_ty_exn genv loc in
         let ty_cont = make_type loc (TyFun ([ty], ty_void)) in
         let ty_raise = make_type loc (TyFun ([ty_exn], ty_void)) in
         let tyl = ty_cont :: ty_raise :: tyl in
            TyFun (tyl, ty_void)

    | TyTuple tyl ->
         TyTuple (cps_types genv tyl)
    | TyArray ty ->
         TyArray (cps_type genv ty)

    | TyAll (vars, ty) ->
         TyAll (vars, cps_type genv ty)
    | TyExists (vars, ty) ->
         TyExists (vars, cps_type genv ty)

    | TyApply (ty_var, tyl) ->
         TyApply (ty_var, cps_types genv tyl)
    | TyUnion (ty_var, tyl, s) ->
         TyUnion (ty_var, cps_types genv tyl, s)
    | TyFrame (ty_var, tyl) ->
         TyFrame (ty_var, cps_types genv tyl)
    | TyRecord (ty_var, tyl) ->
         TyRecord (ty_var, cps_types genv tyl)
    | TyModule (ty_var, tyl) ->
         TyModule (ty_var, cps_types genv tyl)
    | TyTag (ty_var, tyl) ->
         TyTag (ty_var, cps_types genv tyl)
    | TyDTuple (ty_var, tyl_opt) ->
         TyDTuple (ty_var, cps_types_opt genv tyl_opt)

      (* We don't need external types any more *)
    | TyExternal (ty, s) ->
         dest_type_core (cps_type genv ty)

    | TyFormat (ty1, ty2, ty3) ->
         TyFormat (cps_type genv ty1, cps_type genv ty2, cps_type genv ty3)

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Record definition.
 *)
let cps_record_type genv pos fields =
   FieldTable.map (fun (mflag, ty) ->
         mflag, cps_type genv ty) fields

(*
 * Union definition.
 *)
let cps_union_type genv pos fields =
   FieldTable.map (cps_types genv) fields

(*
 * Module definition.
 *)
let cps_module_type genv pos names fields =
   let fields = SymbolTable.map (cps_type genv) fields in
      names, fields

(*
 * Type definition.
 *)
let cps_tydef genv tydef =
   let { tydef_inner = inner } = tydef in
   let loc = loc_of_tydef_inner inner in
   let pos = string_pos "cps_tydef" (loc_pos loc) in
   let inner =
      match dest_tydef_inner_core inner with
         TyDefLambda ty ->
            TyDefLambda (cps_type genv ty)
       | TyDefFrame fields ->
            TyDefFrame (cps_record_type genv pos fields)
       | TyDefRecord fields ->
            TyDefRecord (cps_record_type genv pos fields)
       | TyDefUnion fields ->
            TyDefUnion (cps_union_type genv pos fields)
       | TyDefModule (names, fields) ->
            let names, fields = cps_module_type genv pos names fields in
               TyDefModule (names, fields)
       | TyDefDTuple ty_var ->
            TyDefDTuple ty_var
   in
      { tydef with tydef_inner = make_tydef_inner loc inner }

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Modify the types in the atoms.
 *)
let rec cps_atom genv pos a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomVar _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomTyUnpack _ ->
         a
    | AtomNil ty ->
         AtomNil (cps_type genv ty)
    | AtomTyConstrain (a, ty) ->
         AtomTyConstrain (cps_atom genv pos a, cps_type genv ty)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (cps_atom genv pos a, cps_type genv ty, cps_types genv tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, cps_type genv ty, cps_types genv tyl)
    | AtomConst (ty, ty_var, const_var) ->
         AtomConst (cps_type genv ty, ty_var, const_var)
    | AtomUnop (op, a) ->
         AtomUnop (op, cps_atom genv pos a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, cps_atom genv pos a1, cps_atom genv pos a2)

let cps_atom_opt genv pos a_opt =
   match a_opt with
      Some a -> Some (cps_atom genv pos a)
    | None -> None

let cps_atoms genv pos atoms =
   List.map (cps_atom genv pos) atoms

(*
 * Allocations.
 *)
let cps_alloc_op genv pos op =
   match op with
      AllocTuple (ty_vars, ty, atoms) ->
         AllocTuple (ty_vars, cps_type genv ty, cps_atoms genv pos atoms)
    | AllocUnion (ty_vars, ty, ty_var, const_var, atoms) ->
         AllocUnion (ty_vars, cps_type genv ty, ty_var, const_var, cps_atoms genv pos atoms)
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         let fields = List.map (fun (label, a) -> label, cps_atom genv pos a) fields in
            AllocRecord (ty_vars, cps_type genv ty, ty_var, fields)
    | AllocDTuple (ty, ty_var, tag_var, atoms) ->
         AllocDTuple (cps_type genv ty, ty_var, tag_var, cps_atoms genv pos atoms)
    | AllocArray (ty, atoms) ->
         AllocArray (cps_type genv ty, cps_atoms genv pos atoms)
    | AllocVArray (ty, a1, a2) ->
         AllocVArray (cps_type genv ty, cps_atom genv pos a1, cps_atom genv pos a2)
    | AllocFrame (ty, ty_var, tyl) ->
         AllocFrame (cps_type genv ty, ty_var, cps_types genv tyl)
    | AllocModule (ty, ty_var, fields) ->
         let ty = cps_type genv ty in
         let fields = SymbolTable.map (cps_atom genv pos) fields in
            AllocModule (ty, ty_var, fields)

(*
 * Every function call gets new arguments.
 *)
let rec cps_exp genv e =
   let pos = string_pos "cps_exp" (exp_pos e) in
   let loc = loc_of_exp e in
   let e = cps_exp_core genv pos loc (dest_exp_core e) in
      make_exp loc e

and cps_exp_core genv pos loc e =
   match e with
     LetAtom (v, ty, a, e) ->
         let ty = cps_type genv ty in
         let e = cps_exp genv e in
            LetAtom (v, ty, a, e)
    | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
         let ty = cps_type genv ty1 in
         let ty_args = cps_types genv ty_args in
         let e = cps_exp genv e in
            LetExt (v, ty1, s, ty2, ty_args, args, e)
    | TailCall (f, args) ->
         TailCall (cps_atom genv pos f, cps_atoms genv pos args)
    | Match (a, cases) ->
         let cases = List.map (fun (s, e) -> s, cps_exp genv e) cases in
            Match (cps_atom genv pos a, cases)
    | MatchDTuple (a, cases) ->
         let cases = List.map (fun (a_opt, e) -> cps_atom_opt genv pos a_opt, cps_exp genv e) cases in
            MatchDTuple (cps_atom genv pos a, cases)
    | LetAlloc (v, op, e) ->
         LetAlloc (v, cps_alloc_op genv pos op, cps_exp genv e)
    | SetSubscript (a1, a2, a3, ty, e) ->
         SetSubscript (cps_atom genv pos a1, cps_atom genv pos a2, cps_atom genv pos a3, cps_type genv ty, cps_exp genv e)
    | LetSubscript (v, ty, a1, a2, e) ->
         LetSubscript (v, cps_type genv ty, cps_atom genv pos a1, cps_atom genv pos a2, cps_exp genv e)
    | LetRec (letrecs, e) ->
         let letrecs = List.map (fun (v, e, ty) -> v, cps_exp genv e, cps_type genv ty) letrecs in
            LetRec (letrecs, cps_exp genv e)
    | LetFuns (funs, e) ->
         cps_funs_exp genv pos funs e
    | LetApply (v, ty, a, args, e) ->
         cps_apply_exp genv pos loc v ty a args e
    | Return a ->
         cps_return_exp genv pos a
    | Try (e1, v, e2) ->
         cps_try_exp genv pos loc e1 v e2
    | Raise (a, ty) ->
         cps_raise_exp genv pos a ty
    | SetGlobal (v, ty, a, e) ->
         SetGlobal (v, cps_type genv ty, cps_atom genv pos a, cps_exp genv e)
    | LetClosure _ ->
         raise (IRException (pos, StringError "unexpected expression"))

(*
 * Each "global" function gets the four new arguments.
 *)
and cps_funs_exp genv pos funs e =
   let pos = string_pos "cps_funs_exp" pos in
   let funs =
      List.map (fun (f, loc, gflag, ty, ty_vars, vars, e) ->
            let loc, gflag, ty, ty_vars, vars, e = cps_fun genv f (loc, gflag, ty, ty_vars, vars, e) in
               f, loc, gflag, ty, ty_vars, vars, e) funs
   in
   let e = cps_exp genv e in
      LetFuns (funs, e)

and cps_fun genv f def =
   let pos = string_pos "cps_funs_exp" (fundef_pos f def) in
   let loc, gflag, ty_vars, ty, vars, e = def in
      match gflag with
         FunGlobalClass
       | FunTopLevelClass
       | FunPartialClass ->
            (* Add the extra arguments *)
            let vars = exit_var :: raise_var :: vars in
            let ty = cps_type genv ty in
            let e = cps_exp genv e in
               loc, gflag, ty_vars, ty, vars, e
       | FunLocalClass ->
            (* Do not add extra arguments for local functions *)
            let ty_args, _ = dest_fun_type genv pos ty in
            let ty_args = cps_types genv ty_args in
            let ty_void = make_type loc TyVoid in
            let ty = make_type loc (TyFun (ty_args, ty_void)) in
            let e = cps_exp genv e in
               loc, gflag, ty_vars, ty, vars, e
       | FunContClass ->
            (* This hasn't been introduced yet... *)
            raise (IRException (pos, InternalError))

(*
 * CPS convert a function application.
 * The function _must_ be global (we NEVER apply local functions).
 * Build a new continuation; the raise function is unchanged.
 *)
and cps_apply_exp genv pos loc v ty a args e =
   let pos = string_pos "cps_apply_exp" pos in

   (* Convert the parts *)
   let ty = cps_type genv ty in
   let a = cps_atom genv pos a in
   let args = cps_atoms genv pos args in
   let e = cps_exp genv e in

   (* Place the remaining code in a function *)
   let s = string_of_atom a in
   let f = new_symbol_string (s ^ "_cont") in
   let ty_void = make_type loc TyVoid in
   let ty_fun = make_type loc (TyFun ([ty], ty_void)) in

   (* Now pass the extra arguments to the function *)
   let args = AtomVar f :: AtomVar raise_var :: args in
   let cont_exp = make_exp loc (TailCall (a, args)) in
      LetFuns ([f, loc, FunContClass, [], ty_fun, [v], e], cont_exp)

(*
 * CPS convert a return.
 * This is just a call to the continuation.
 *)
and cps_return_exp genv pos a =
   let pos = string_pos "cps_return_exp" pos in
      TailCall (AtomVar exit_var, [cps_atom genv pos a])

(*
 * Exception handler.  We define a new raise function.
 * This is done with _two_ functions to avoid the recursive
 * call.
 *)
and cps_try_exp genv pos loc e1 v e2 =
   let pos = string_pos "cps_try_exp" pos in
   let e1 = cps_exp genv e1 in
   let e2 = cps_exp genv e2 in

   (* Place e2 in a function named by the raise handler *)
   let handle_var = new_symbol_string "handle" in
   let ty_void = make_type loc TyVoid in
   let ty_exn = genv_ty_exn genv loc in
   let ty_raise_fun = make_type loc (TyFun ([ty_exn], ty_void)) in
   let e = make_exp loc (TailCall (AtomVar handle_var, [AtomVar v])) in
   let e = make_exp loc (LetFuns ([raise_var, loc, FunContClass, [], ty_raise_fun, [v], e], e1)) in
      LetFuns ([handle_var, loc, FunLocalClass, [], ty_raise_fun, [v], e2], e)

(*
 * When an exception is raised,
 * just call the exception handler.
 *)
and cps_raise_exp genv pos a _ =
   let pos = string_pos "cps_raise_exp" pos in
      TailCall (AtomVar raise_var, [a])

(*
 * Function table conversion.
 *)
let cps_funs genv funs =
   SymbolTable.mapi (cps_fun genv) funs

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Convert the program.
 *)
let cps_prog prog =
   let { prog_types = types;
         prog_import = import;
         prog_export = export;
         prog_globals = globals;
         prog_funs = funs
       } = prog
   in
   let genv = genv_of_prog prog in
   let types = SymbolTable.map (cps_tydef genv) types in
   let import =
      SymbolTable.map (fun import ->
            { import with import_type = cps_type genv import.import_type }) import
   in
   let export =
      SymbolTable.map (fun export ->
            { export with export_type = cps_type genv export.export_type }) export
   in
   let globals =
      SymbolTable.map (fun (ty, init) ->
            cps_type genv ty, init) globals
   in
   let funs = cps_funs genv funs in
   let prog =
      { prog with prog_types = types;
                  prog_import = import;
                  prog_export = export;
                  prog_globals = globals;
                  prog_funs = funs
      }
   in
      if debug debug_print_ir then
         debug_prog "Aml_ir_cps.before_standardize" prog;
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
