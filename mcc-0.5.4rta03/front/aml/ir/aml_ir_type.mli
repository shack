(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_ir
open Aml_ir_pos
open Aml_ir_env_type

(*
 * Eliminate outermost TyProject.
 *)
val expand_type : genv -> pos -> ty -> ty
val expand_type_core : genv -> pos -> ty -> ty_core

(*
 * Types.
 *)
val type_of_unop         : genv -> loc -> unop -> ty * ty
val type_of_binop        : genv -> loc -> binop -> ty * ty * ty
val type_of_atom         : genv -> venv -> pos -> loc -> atom -> ty
val type_of_alloc_op     : alloc_op -> ty

(*
 * Predicates.
 *)
val is_fun_type          : genv -> pos -> ty -> bool
val is_frame_type        : genv -> pos -> ty -> bool

(*
 * Type destructors.
 *)
val dest_all_type        : genv -> pos -> ty -> ty_var list * ty
val dest_exists_type     : genv -> pos -> ty -> ty_var list * ty
val dest_all_fun_type    : genv -> pos -> ty -> ty_var list * ty list * ty
val dest_fun_type        : genv -> pos -> ty -> ty list * ty
val dest_array_type      : genv -> pos -> ty -> ty
val dest_tuple_type      : genv -> pos -> ty -> ty list
val dest_dtuple_type     : genv -> pos -> ty -> ty_var * ty list option
val dest_tag_type        : genv -> pos -> ty -> ty_var * ty list
val dest_pair_type       : genv -> pos -> ty -> ty * ty

(*
 * Constructors.
 *)
val make_all_type        : loc -> ty_var list -> ty -> ty
val make_ty_apply_atom   : atom -> ty -> ty list -> atom

(*
 * Rename the quantified vars in the type.
 *)
val rename_all_vars      : genv -> pos -> ty -> ty_var list -> ty

(*
 * Utilities.
 *)
val var_of_atom          : pos -> atom -> var

(*
 * Variable env.
 * This contains the type of the top-level variables.
 *)
val venv_of_prog : prog -> venv

(*
 * This contains the type of _all_ the variables in the program.
 *)
val venv_all_of_prog : genv -> prog -> venv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
