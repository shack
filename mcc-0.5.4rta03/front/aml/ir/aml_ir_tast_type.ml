(*
 * Translate the TAST types.
 * This is complicated.
 * TODO: document this as soon as it is finished.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_env
open Aml_ir_const

module Pos = MakePos (struct let name = "Aml_ir_tast_type" end)
open Pos

(************************************************************************
 * TYPE OPERATIONS
 ************************************************************************)

(*
 * Expand a var_type.
 * This evaluates any VarTypeDelayed.
 *)
let rec expand_var_type genv tenv ty =
   match ty with
      VarTypeDelayed ty
    | VarTypeFun ([], Some ty) ->
         eval_type genv tenv ty
    | VarTypeNormal _
    | VarTypeModule _
    | VarTypeFunctor _
    | VarTypeFun _ ->
         genv, ty

(*
 * Get the type of a var_type.
 *)
and type_of_var_type genv tenv pos ty =
   let pos = string_pos "type_of_var_type" pos in
   let genv, ty = expand_var_type genv tenv ty in
      match ty with
         VarTypeNormal ty
       | VarTypeModule (ty, _)
       | VarTypeFunctor (ty, _, _, _) ->
            genv, ty
       | VarTypeDelayed _
       | VarTypeFun _ ->
            raise (IRException (string_pos "type_of_var_type" pos, StringVarTypeError ("not a type", ty)))

and type_of_var_type_list genv tenv pos tyl =
   let pos = string_pos "type_of_var_type_list" pos in
   let genv, tyl =
      List.fold_left (fun (genv, tyl) ty ->
            let genv, ty = type_of_var_type genv tenv pos ty in
               genv, ty :: tyl) (genv, []) tyl
   in
      genv, List.rev tyl

(*
 * Type destructors.
 *)
and dest_normal_var_type genv tenv pos ty =
   let genv, ty = expand_var_type genv tenv ty in
      match ty with
         VarTypeNormal ty ->
            genv, ty
       | VarTypeDelayed _
       | VarTypeModule _
       | VarTypeFunctor _
       | VarTypeFun _ ->
            raise (IRException (string_pos "dest_normal_var_type" pos, StringVarTypeError ("not a normal var", ty)))

and dest_module_var_type genv tenv pos ty =
   let genv, ty = expand_var_type genv tenv ty in
      match ty with
         VarTypeModule (ty, mt) ->
            genv, ty, mt
       | VarTypeDelayed _
       | VarTypeNormal _
       | VarTypeFunctor _
       | VarTypeFun _ ->
            raise (IRException (string_pos "dest_module_var_type" pos, StringVarTypeError ("not a module type", ty)))

and dest_functor_var_type genv tenv pos ty =
   let genv, ty = expand_var_type genv tenv ty in
      match ty with
         VarTypeFunctor (ty, v, mt1, mt2) ->
            genv, ty, v, mt1, mt2
       | VarTypeDelayed _
       | VarTypeNormal _
       | VarTypeModule _
       | VarTypeFun _ ->
            raise (IRException (string_pos "dest_functor_var_type" pos, StringVarTypeError ("not a functor type", ty)))

(*
 * Project the type from the module.
 *)
and module_lookup_var genv tenv pos loc ty ext_var =
   let pos = string_pos "module_lookup_var" pos in
   let genv, _, mt = dest_module_var_type genv tenv pos ty in
   let { mt_names = names;
         mt_vars  = vars
       } = mt
   in
   let v =
      try FieldTable.find names.names_var ext_var with
         Not_found ->
            raise (IRException (pos, UnboundVar ext_var))
   in
   let ty =
      try SymbolTable.find vars v with
         Not_found ->
            raise (IRException (pos, UnboundVar v))
   in
      genv, ty

(*
 * Project the variable from the module.
 *)
and module_lookup_type genv tenv pos loc ty ext_var =
   let pos = string_pos "module_lookup_type" pos in
   let genv, _, mt = dest_module_var_type genv tenv pos ty in
   let { mt_names = names;
         mt_types = types
       } = mt
   in
   let v =
      try SymbolTable.find names.names_ty ext_var with
         Not_found ->
            raise (IRException (pos, UnboundType ext_var))
   in
   let ty_vars, ty_opt =
      try SymbolTable.find types v with
         Not_found ->
            raise (IRException (pos, UnboundType v))
   in
      genv, ty_vars, v, ty_opt

(************************************************************************
 * TYPE NAMES
 ************************************************************************)

(*
 * Project types from modules.
 *)
and eval_mod_name genv tenv pos loc me_name =
   let pos = string_pos "eval_mod_name" pos in
      match me_name with
         Aml_tast.ModNameEnv v ->
            genv, tenv_lookup_var tenv pos v
       | Aml_tast.ModNameSelf (_, ext_var) ->
            raise (Failure "eval_mod_name: self not implemented")
       | Aml_tast.ModNameProj (me_name, ext_var) ->
            let genv, ty = eval_mod_name genv tenv pos loc me_name in
            let genv, ty = module_lookup_var genv tenv pos loc ty ext_var in
               genv, ty
       | Aml_tast.ModNameApply (me_fun, me_arg) ->
            let genv, ty_fun = eval_mod_name genv tenv pos loc me_fun in
            let genv, ty_arg = eval_mod_name genv tenv pos loc me_arg in
            let genv, _, v, _, ty_res = dest_functor_var_type genv tenv pos ty_fun in
            let tenv = tenv_add_var tenv v ty_arg in
               eval_type genv tenv ty_res

(*
 * Resolve a type from its name.
 *)
and eval_type_name genv tenv pos loc ty_name =
   let pos = string_pos "eval_type_name" pos in
      match ty_name with
         Aml_tast.TyNameSoVar v ->
            let ty_vars, ty = tenv_lookup_so_var tenv pos v in
               genv, ty_vars, v, ty
       | Aml_tast.TyNameSelf (_, ext_var) ->
            let ty_var, (ty_vars, ty) = tenv_lookup_so_var_ext tenv pos ext_var in
               genv, ty_vars, ty_var, ty
       | Aml_tast.TyNamePath (me_name, ext_var) ->
            let genv, ty = eval_mod_name genv tenv pos loc me_name in
               module_lookup_type genv tenv pos loc ty ext_var

(************************************************************************
 * TRANSLATION
 ************************************************************************)

(*
 * Translate a type.
 * Most types are essentially the same.
 *)
and eval_type genv tenv ty_tast =
   let loc = Aml_tast_ds.loc_of_type ty_tast in
   let pos = string_pos "eval_type" (tast_type_pos ty_tast) in
   let ty = Aml_tast_ds.dest_type_core ty_tast in
      eval_type_core genv tenv pos loc ty

and eval_type_core genv tenv pos loc ty =
   match ty with
      (* The first segment is all the easy cases *)
      Aml_tast.TyInt ->
         genv, VarTypeNormal (make_type loc TyInt)
    | Aml_tast.TyChar ->
         genv, VarTypeNormal (make_type loc TyChar)
    | Aml_tast.TyString ->
         genv, VarTypeNormal (make_type loc TyString)
    | Aml_tast.TyFloat ->
         genv, VarTypeNormal (make_type loc TyFloat)
    | Aml_tast.TyFun (tyl, ty) ->
         let genv, tyl = build_types genv tenv pos tyl in
         let genv, ty = build_type genv tenv pos ty in
         let ty = VarTypeNormal (make_type loc (TyFun (tyl, ty))) in
            genv, ty
    | Aml_tast.TyFormat (ty1, ty2, ty3) ->
         let genv, ty1 = build_type genv tenv pos ty1 in
         let genv, ty2 = build_type genv tenv pos ty2 in
         let genv, ty3 = build_type genv tenv pos ty3 in
         let ty = VarTypeNormal (make_type loc (TyFormat (ty1, ty2, ty3))) in
            genv, ty
    | Aml_tast.TyExternal (ty, s) ->
         let genv, ty = build_type genv tenv pos ty in
         let ty = VarTypeNormal (make_type loc (TyExternal (ty, s))) in
            genv, ty
    | Aml_tast.TyOpen _ ->
         raise (IRException (pos, NotImplemented "ambient types"))
    | Aml_tast.TyProd tyl ->
         let genv, tyl = build_types genv tenv pos tyl in
         let ty = VarTypeNormal (make_type loc (TyTuple tyl)) in
            genv, ty
    | Aml_tast.TyArray ty ->
         let genv, ty = build_type genv tenv pos ty in
         let ty = VarTypeNormal (make_type loc (TyArray ty)) in
            genv, ty
    | Aml_tast.TyAll (vars, ty) ->
         let genv, ty = build_type genv tenv pos ty in
         let ty = VarTypeNormal (make_type loc (TyAll (vars, ty))) in
            genv, ty
    | Aml_tast.TyVar v ->
         (try genv, tenv_lookup_fo_var_exn tenv v with
             Not_found ->
                let ty = VarTypeNormal (make_type loc (TyVar v)) in
                   genv, ty)

      (* Each of these types requires more work *)
    | Aml_tast.TyProject (ty_name, tyl) ->
         eval_project_type genv tenv pos loc ty_name tyl
    | Aml_tast.TyFunctor (v, ty1, ty2) ->
         eval_functor_type genv tenv pos loc v ty1 ty2
    | Aml_tast.TySOVar (ty_var, tyl) ->
         eval_sovar_type genv tenv pos loc ty_var tyl
    | Aml_tast.TyApply (ty_var, tyl, kindl) ->
         eval_tydef genv tenv pos loc ty_var tyl kindl eval_apply_tydef
    | Aml_tast.TyRecord (ty_var, tyl, kindl) ->
         eval_tydef genv tenv pos loc ty_var tyl kindl eval_record_tydef
    | Aml_tast.TyUnion (ty_var, tyl, kindl) ->
         eval_tydef genv tenv pos loc ty_var tyl kindl eval_union_tydef
    | Aml_tast.TyModule (ty_var, tyl, kindl) ->
         eval_tydef genv tenv pos loc ty_var tyl kindl eval_module_tydef

(*
 * Build a projection type.
 * Look up the types from the environment.
 *)
and eval_project_type genv tenv pos loc ty_name tyl =
   let pos = string_pos "eval_project_type" pos in
   let genv, tyl = eval_types genv tenv pos tyl in
   let genv, ty_vars, ty_var, ty_opt = eval_type_name genv tenv pos loc ty_name in
      match ty_opt with
         Some ty ->
            let tenv = tenv_add_fo_vars tenv pos ty_vars tyl in
               eval_type genv tenv ty
       | None ->
            let genv, tyl = type_of_var_type_list genv tenv pos tyl in
               genv, VarTypeNormal (make_type loc (TyApply (ty_var, tyl)))

(*
 * Second-order type variable.
 * The variable _must be_ bound in the environment.
 *)
and eval_sovar_type genv tenv pos loc ty_var tyl =
   let pos = string_pos "eval_sovar_type" pos in
   let genv, tyl = eval_types genv tenv pos tyl in
   let ty_vars, ty_opt = tenv_lookup_so_var tenv pos ty_var in
      match ty_opt with
         Some ty ->
            let tenv = tenv_add_fo_vars tenv pos ty_vars tyl in
               eval_type genv tenv ty
       | None ->
            raise (IRException (pos, NotImplemented "abstract types"))

(*
 * Build a functor type.
 * The functor become a function.
 * The argument is an existentially-quantified module type.
 * Curry the type arguments, and expand them in the result.
 *)
and eval_functor_type genv tenv pos loc v ty1_tast ty2_tast =
   let pos = string_pos "eval_functor_type" pos in
   let genv, ty1 = eval_type genv tenv ty1_tast in
   let genv, ty2 = eval_type genv tenv ty2_tast in
   let genv, ty1' = type_of_var_type genv tenv pos ty1 in
   let genv, ty2' = type_of_var_type genv tenv pos ty2 in
   let ty_fun = make_type loc (TyFun ([ty1'], ty2')) in
   let { fv_fo_vars = ty_vars } = free_vars_type ty_fun in
   let ty = make_type loc (TyAll (SymbolSet.to_list ty_vars, ty_fun)) in
   let ty = VarTypeFunctor (ty, v, ty1, ty2_tast) in
      genv, ty

(*
 * Lists and kinds.
 *)
and eval_types genv tenv pos tyl =
   let genv, tyl =
      List.fold_left (fun (genv, tyl) ty ->
            let genv, ty = eval_type genv tenv ty in
               genv, ty :: tyl) (genv, []) tyl
   in
      genv, List.rev tyl

(*
 * Augment the position info.
 *)
and build_type genv tenv pos ty =
   let pos = string_pos "build_type" pos in
   let genv, ty = eval_type genv tenv ty in
      type_of_var_type genv tenv pos ty

and build_types genv tenv pos tyl =
   let pos = string_pos "build_types" pos in
   let genv, tyl = eval_types genv tenv pos tyl in
   let genv, tyl =
      List.fold_left (fun (genv, tyl) ty ->
            let genv, ty = type_of_var_type genv tenv pos ty in
               genv, ty :: tyl) (genv, []) tyl
   in
      genv, List.rev tyl

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * This is a generic type application.
 * Evaluate the type argument, so_var arguments,
 * add them to the variable environment, and evaluate the tydef.
 *)
and eval_tydef genv tenv pos loc ty_var tyl kinds cont =
   let pos = string_pos "eval_tydef" pos in
   let genv, tyl = eval_types genv tenv pos tyl in
      try
         let make_ty_apply = genv_lookup_ty_apply_exn genv ty_var kinds in
         let genv, tyl = type_of_var_type_list genv tenv pos tyl in
         let ty = make_ty_apply loc tyl in
            genv, VarTypeNormal ty
      with
         Not_found ->
            (*
             * Add an entry to the table _now_, so that recursive
             * types will not loop forever.  Allow the trivial case
             * to keep its name, but rename all the rest.
             *)
            let ty_var' =
               if kinds = [] then
                  ty_var
               else
                  new_symbol ty_var
            in

            (* Get the type definition and build the type *)
            let tydef = tenv_lookup_tydef tenv pos ty_var in
            let { Aml_tast.tydef_fo_vars = fo_vars;
                  Aml_tast.tydef_so_vars = so_vars;
                  Aml_tast.tydef_inner = inner
                } = tydef
            in
            (* let tenv = tenv_add_fo_vars tenv pos fo_vars tyl in *)
            let genv, kinds' = eval_kinds genv tenv pos loc kinds in
            let so_vars = List.map (fun { Aml_tast.kind_value = v } -> v) so_vars in
            let tenv = tenv_add_so_vars tenv pos so_vars kinds' in
               cont genv tenv pos loc ty_var ty_var' fo_vars inner kinds

(*
 * Evaluate a kind.
 * The kind value should be a type function.
 *)
and eval_kind genv tenv pos loc kind =
   let pos = string_pos "eval_kind" pos in
   let { Aml_tast.kind_value = ty_name } = kind in
      eval_type_name genv tenv pos loc ty_name

and eval_kinds genv tenv pos loc kinds =
   let pos = string_pos "eval_kinds" pos in
   let genv, kinds =
      List.fold_left (fun (genv, kinds) kind ->
            let genv, ty_vars, ty_var, ty = eval_kind genv tenv pos loc kind in
            let kinds = (ty_vars, ty) :: kinds in
               genv, kinds) (genv, []) kinds
   in
      genv, List.rev kinds

(*
 * Type application.
 *)
and eval_apply_tydef genv tenv pos loc ty_var ty_var' fo_vars inner kinds =
   let pos = string_pos "eval_apply_tydef" pos in
      match Aml_tast_ds.dest_tydef_inner_core inner with
         Aml_tast.TyDefLambda ty ->
            build_apply_tydef genv tenv pos loc ty_var ty_var' fo_vars ty kinds
       | _ ->
            raise (IRException (pos, StringTAstTydefInnerError ("not a parameterized type", inner)))

and build_apply_tydef genv tenv pos loc ty_var ty_var' fo_vars ty kinds =
   let pos = string_pos "build_apply_tydef" pos in

   (* Add the definition now *)
   let make_ty_apply loc tyl =
      make_type loc (TyApply (ty_var', tyl))
   in
   let genv = genv_add_ty_apply genv ty_var kinds make_ty_apply in

   (* Now build the type *)
   let genv, ty = build_type genv tenv pos ty in
   let inner = make_tydef_inner loc (TyDefLambda ty) in
   let tydef =
      { tydef_fo_vars = fo_vars;
        tydef_inner = inner
      }
   in
   let genv = genv_add_tydef genv ty_var' tydef in
   let tyl = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_ty_apply loc tyl in
      genv, VarTypeNormal ty

(*
 * Records.
 *)
and eval_record_tydef genv tenv pos loc ty_var ty_var' fo_vars inner kinds =
   let pos = string_pos "eval_record_tydef" pos in
      match Aml_tast_ds.dest_tydef_inner_core inner with
         Aml_tast.TyDefRecord fields ->
            build_record_tydef genv tenv pos loc ty_var ty_var' fo_vars fields kinds
       | _ ->
            raise (IRException (pos, StringTAstTydefInnerError ("not a record type", inner)))

and build_record_tydef genv tenv pos loc ty_var ty_var' fo_vars fields kinds =
   let pos = string_pos "build_record_tydef" pos in

   (* Add the definition now *)
   let make_ty_apply loc tyl =
      make_type loc (TyRecord (ty_var', tyl))
   in
   let genv = genv_add_ty_apply genv ty_var kinds make_ty_apply in

   (* Convert all the fields *)
   let genv, fields =
      FieldTable.fold_map (fun genv label (mflag, ty) ->
            let genv, ty = build_type genv tenv pos ty in
               genv, (mflag, ty)) genv fields
   in

   (* Build the actual definition *)
   let tydef = make_tydef_inner loc (TyDefRecord fields) in
   let tydef =
      { tydef_fo_vars = fo_vars;
        tydef_inner = tydef
      }
   in
   let genv = genv_add_tydef genv ty_var' tydef in
   let tyl = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_ty_apply loc tyl in
      genv, VarTypeNormal ty

(*
 * Unions.
 *)
and eval_union_tydef genv tenv pos loc ty_var ty_var' fo_vars inner kinds =
   let pos = string_pos "eval_union_tydef" pos in
      match Aml_tast_ds.dest_tydef_inner_core inner with
         Aml_tast.TyDefUnion (Aml_tast.NormalUnion, fields) ->
            build_union_tydef genv tenv pos loc ty_var ty_var' fo_vars fields kinds
       | Aml_tast.TyDefUnion (Aml_tast.ExnUnion, _) ->
            build_exn_tydef genv tenv pos loc ty_var ty_var' fo_vars kinds
       | _ ->
            raise (IRException (pos, StringTAstTydefInnerError ("not a union type", inner)))

and build_union_tydef genv tenv pos loc ty_var ty_var' fo_vars fields kinds =
   let pos = string_pos "build_union_tydef" pos in

   (* Add the definition now *)
   let make_ty_apply loc tyl =
      make_type loc (TyUnion (ty_var', tyl, AnyIndex))
   in
   let genv = genv_add_ty_apply genv ty_var kinds make_ty_apply in

   (* Convert all the fields *)
   let genv, fields =
      FieldTable.fold_map (fun genv label tyl ->
            build_types genv tenv pos tyl) genv fields
   in

   (* Build the actual definition *)
   let tydef = make_tydef_inner loc (TyDefUnion fields) in
   let tydef =
      { tydef_fo_vars = fo_vars;
        tydef_inner = tydef
      }
   in
   let genv = genv_add_tydef genv ty_var' tydef in
   let tyl = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_ty_apply loc tyl in
      genv, VarTypeNormal ty

and build_exn_tydef genv tenv pos loc ty_var ty_var' fo_vars kinds =
   let pos = string_pos "build_exn_tydef" pos in

   (* Add the definition now *)
   let make_ty_apply loc tyl =
      match tyl with
         [] -> make_type loc (TyDTuple (ty_var', None))
       | _ -> raise (IRException (pos, StringError "exceptions are not polymorphic"))
   in
   let genv = genv_add_ty_apply genv ty_var kinds make_ty_apply in

   (* Build the actual type definition *)
   let tydef = make_tydef_inner loc (TyDefDTuple ty_var') in
   let tydef =
      { tydef_fo_vars = fo_vars;
        tydef_inner = tydef
      }
   in
   let genv = genv_add_tydef genv ty_var' tydef in
   let tyl = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_ty_apply loc tyl in
      genv, VarTypeNormal ty

(*
 * Modules are different.
 * We don't add a module definition to the environment: we wvaluate the module
 * instead.
 *)
and eval_module_tydef genv tenv pos loc ty_var ty_var' fo_vars inner kinds =
   let pos = string_pos "eval_module_tydef" pos in
      match Aml_tast_ds.dest_tydef_inner_core inner with
         Aml_tast.TyDefModule mt ->
            build_module_tydef genv tenv pos loc ty_var ty_var' fo_vars mt kinds
       | _ ->
            raise (IRException (pos, StringTAstTydefInnerError ("not a module type", inner)))

and build_module_names names =
   let { Aml_tast.names_mt    = names_mt;
         Aml_tast.names_me    = names_me;
         Aml_tast.names_ty    = names_ty;
         Aml_tast.names_var   = names_var
       } = names
   in

   (* Build the names record *)
   let ty_names = SymbolTable.empty in
   let ty_names =
      SymbolTable.fold (fun ty_names mt_var (mt, _) ->
            SymbolTable.add ty_names mt_var mt) ty_names names_mt
   in
   let ty_names = SymbolTable.fold SymbolTable.add ty_names names_ty in

   let var_names = FieldTable.empty in
   let me_names_count = FieldTable.cardinal names_me in
   let var_names =
      FieldTable.fold_index (fun var_names v v' i ->
            FieldTable.add_index var_names v v' i) var_names names_me
   in
   let var_names =
      FieldTable.fold_index (fun var_names v v' i ->
            FieldTable.add_index var_names v v' (i + me_names_count)) var_names names_var
   in
      { names_ty = ty_names; names_var = var_names }

and build_module_tydef genv tenv pos loc ty_var ty_var' fo_vars mt kinds =
   let pos = string_pos "build_module_tydef" pos in

   (* Add the definition now *)
   let make_ty_apply loc tyl =
      make_type loc (TyModule (ty_var', tyl))
   in
   let genv = genv_add_ty_apply genv ty_var kinds make_ty_apply in

   (* Build the module type *)
   let { Aml_tast.mt_mt_types = mt_types;
         Aml_tast.mt_me_vars  = me_vars;
         Aml_tast.mt_types    = ty_types;
         Aml_tast.mt_vars     = var_vars;
         Aml_tast.mt_names    = names
       } = mt
   in
   let { Aml_tast.names_mt    = names_mt;
         Aml_tast.names_me    = names_me;
         Aml_tast.names_ty    = names_ty;
         Aml_tast.names_var   = names_var
       } = names
   in

   (* Build the names record *)
   let names = build_module_names names in
   let tenv = tenv_add_names tenv names in

   (* Add types of all the values to the environment *)
   let tenv =
      SymbolTable.fold (fun tenv mt_var mt ->
            tenv_add_so_var tenv mt_var [] (Some mt)) tenv mt_types
   in
   let tenv =
      SymbolTable.fold (fun tenv me_var mt ->
            tenv_add_var tenv me_var (VarTypeDelayed mt)) tenv me_vars
   in
   let tenv =
      SymbolTable.fold (fun tenv ty_var (ty_vars, ty) ->
            tenv_add_so_var tenv ty_var ty_vars ty) tenv ty_types
   in
   let tenv =
      SymbolTable.fold (fun tenv v ty ->
            tenv_add_var tenv v (VarTypeDelayed ty)) tenv var_vars
   in

   (* Build normal types *)
   let types = SymbolTable.empty in
   let types =
      SymbolTable.fold (fun types ty_var ty ->
            SymbolTable.add types ty_var ([], Some ty)) types mt_types
   in
   let types =
      SymbolTable.fold (fun types ty_var info ->
            SymbolTable.add types ty_var info) types ty_types
   in

   (* Build variables *)
   let vars = SymbolTable.empty in
   let genv, vars =
      SymbolTable.fold (fun (genv, vars) me_var ty ->
            let genv, ty = eval_type genv tenv ty in
            let vars = SymbolTable.add vars me_var ty in
               genv, vars) (genv, vars) me_vars
   in
   let genv, vars =
      SymbolTable.fold (fun (genv, vars) v ty ->
            let genv, ty = eval_type genv tenv ty in
            let vars = SymbolTable.add vars v ty in
               genv, vars) (genv, vars) var_vars
   in

   (* Finally, we have the module type *)
   let mt =
      { mt_types = types;
        mt_vars = vars;
        mt_names = names
      }
   in

   (* Make the module type *)
   let genv, vars =
      SymbolTable.fold_map (fun genv v ty ->
            type_of_var_type genv tenv pos ty) genv vars
   in
   let inner = make_tydef_inner loc (TyDefModule (names.names_var, vars)) in

   (* Add it to the environment with a new name *)
   let tydef =
      { tydef_fo_vars = fo_vars;
        tydef_inner = inner
      }
   in
   let genv = genv_add_tydef genv ty_var' tydef in

   (* The result type *)
   let tyl = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_ty_apply loc tyl in
      genv, VarTypeModule (ty, mt)

(*
 * Optional conversion.
 *)
let build_type_opt genv tenv pos ty_opt =
   let pos = string_pos "build_type_opt" pos in
      match ty_opt with
         Some ty ->
            let genv, ty = build_type genv tenv pos ty in
               genv, Some ty
       | None ->
            genv, None

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
