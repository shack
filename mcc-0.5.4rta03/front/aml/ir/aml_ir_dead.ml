(*
 * AML IR dead-expression elimination.
 * The code first computes the set of all live symbols using fixpoint analysis
 * across function boundaries.  Then it uses the set to perform dead code elimination.
 * Note that this removes all dead arguments to any function always called directly.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol
open Field_table

open Fir_state

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_type
open Aml_ir_print

module Pos = MakePos (struct let name = "Aml_ir_dead" end)
open Pos

(*
 * Flags used to determine what arguments to preserve.
 *   PreserveNone: preserve only the args that are not dead
 *   PreserveFrames: preserve args that have type TyFrame
 *   PreserveAll: preserve all args
 *)
type preserve =
   PreserveNone
 | PreserveFrames
 | PreserveAll

(************************************************************************
 * LIVENESS ANALYSIS
 ************************************************************************)

(*
 * Check if an atom is live.
 *)
let rec is_live_atom live a =
   match a with
      AtomNil _
    | AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomConst _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _ ->
         false
    | AtomVar v
    | AtomTyUnpack v
    | AtomTyPack (v, _, _) ->
         SymbolSet.mem live v
    | AtomTyConstrain (a, _)
    | AtomTyApply (a, _, _)
    | AtomUnop (_, a) ->
         is_live_atom live a
    | AtomBinop (_, a1, a2) ->
         is_live_atom live a1 || is_live_atom live a2

(*
 * Variables:
 *   live - live variable set
 *   funs - table from functions to list of arguments
 *)
let rec live_type live ty =
   live_type_core live (dest_type_core ty)

and live_type_core live ty =
   match ty with
      TyVoid
    | TyInt
    | TyChar
    | TyString
    | TyFloat
    | TyVar _ ->
         live
    | TyProject (v, _)
    | TyDTuple (v, None) ->
         SymbolSet.add live v
    | TyFun (ty_args, ty_res) ->
         List.fold_left live_type (live_type live ty_res) ty_args
    | TyTuple tyl ->
         List.fold_left live_type live tyl
    | TyFrame (tv, tyl)
    | TyUnion (tv, tyl, _)
    | TyApply (tv, tyl)
    | TyRecord (tv, tyl)
    | TyModule (tv, tyl)
    | TyTag (tv, tyl)
    | TyDTuple (tv, Some tyl) ->
         List.fold_left live_type (SymbolSet.add live tv) tyl
    | TyArray ty
    | TyAll (_, ty)
    | TyExists (_, ty)
    | TyExternal (ty, _) ->
         live_type live ty
    | TyFormat (ty1, ty2, ty3) ->
         live_type (live_type (live_type live ty1) ty2) ty3

let live_type_list live tyl =
   List.fold_left live_type live tyl

let live_tydef live tydef =
   match dest_tydef_inner_core tydef.tydef_inner with
      TyDefUnion fields ->
         FieldTable.fold (fun live _ tyl ->
               live_type_list live tyl) live fields
    | TyDefRecord fields
    | TyDefFrame fields ->
         FieldTable.fold (fun live _ (_, ty) ->
               live_type live ty) live fields
    | TyDefModule (_, fields) ->
         SymbolTable.fold (fun live _ ty ->
               live_type live ty) live fields
    | TyDefLambda ty ->
         live_type live ty
    | TyDefDTuple v ->
         SymbolSet.add live v

(*
 * Add a var to the live set.
 * If the var is a function, then add all the function's
 * params as live vars too.
 *)
let live_var (funs : gfundef SymbolTable.t) live v =
   let live = SymbolSet.add live v in
      try
         let _, _, _, _, vars, _ = SymbolTable.find funs v in
            SymbolSet.add_list live vars
      with
         Not_found ->
            live

let live_unop live op =
   match op with
      (* Booleans *)
      NotBoolOp

      (* Arithmetic *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Floats *)
    | UMinusFloatOp
    | AbsFloatOp
    | SqrtFloatOp
    | ExpFloatOp
    | LogFloatOp
    | Log10FloatOp
    | CosFloatOp
    | SinFloatOp
    | TanFloatOp
    | ACosFloatOp
    | ASinFloatOp
    | ATanFloatOp
    | CosHFloatOp
    | SinHFloatOp
    | TanHFloatOp
    | CeilFloatOp
    | FloorFloatOp

      (* Coercions *)
    | CharOfIntOp
    | IntOfCharOp
    | IntOfFloatOp
    | FloatOfIntOp

      (* Block operations *)
    | LengthOfStringOp ->
         live

    | LengthOfBlockOp ty ->
         live_type live ty

let live_binop live op =
   match op with
      (* Booleans *)
      OrBoolOp
    | AndBoolOp

      (* Comparisons on ML ints *)
    | EqCharOp
    | NeqCharOp
    | LtCharOp
    | LeCharOp
    | GtCharOp
    | GeCharOp
    | CmpCharOp                (* Similar to ML's ``compare'' function. *)

      (* Standard binary operations on ML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on ML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (* Standard binary operations on floats *)
    | PlusFloatOp
    | MinusFloatOp
    | MulFloatOp
    | DivFloatOp
    | RemFloatOp
    | MaxFloatOp
    | MinFloatOp
    | PowerFloatOp
    | ATan2FloatOp
    | LdExpFloatIntOp

      (* Comparisons on floats *)
    | EqFloatOp
    | NeqFloatOp
    | LtFloatOp
    | LeFloatOp
    | GtFloatOp
    | GeFloatOp
    | CmpFloatOp ->
         live

      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty
    | NeqEqOp ty ->
         live_type live ty

let rec live_atom funs live a =
   match a with
      AtomNil ty ->
         live_type live ty
    | AtomInt _
    | AtomChar _
    | AtomFloat _ ->
         live
    | AtomFrameLabel (ty_var, label)
    | AtomRecordLabel (ty_var, label)
    | AtomModuleLabel (ty_var, label) ->
         SymbolSet.add (SymbolSet.add live ty_var) label
    | AtomConst (ty, ty_var, const_var) ->
         live_type (SymbolSet.add (SymbolSet.add live const_var) ty_var) ty
    | AtomVar v
    | AtomTyUnpack v ->
         live_var funs live v
    | AtomTyConstrain (a, ty) ->
         let live = live_atom funs live a in
            live_type live ty
    | AtomTyApply (a, ty, tyl) ->
         let live = live_atom funs live a in
         let live = live_type live ty in
            live_type_list live tyl
    | AtomTyPack (v, ty, tyl) ->
         let live = live_var funs live v in
         let live = live_type live ty in
            live_type_list live tyl
    | AtomUnop (op, a) ->
         live_unop (live_atom funs live a) op
    | AtomBinop (op, a1, a2) ->
         live_binop (live_atom funs (live_atom funs live a1) a2) op

let live_atom_opt funs live a_opt =
   match a_opt with
      Some a -> live_atom funs live a
    | None -> live

let live_atoms funs args =
   List.fold_left (live_atom funs) args

(*
 * Add live info for function arguments.
 *)
let live_args funs live pos f args =
   try
      let f = var_of_atom pos f in
      let _, _, _, _, vars, _ = SymbolTable.find funs f in
      let len1 = List.length vars in
      let len2 = List.length args in
         if len1 <> len2 then
            raise (IRException (pos, ArityMismatch (len1, len2)));
         List.fold_left2 (fun live v a ->
               if SymbolSet.mem live v then
                  live_atom funs live a
               else
                  live) live vars args
   with
      Not_found ->
         live_atoms funs live args

(*
 * Normal allocation makes all the atoms live.
 *)
let live_alloc_op funs live op =
   match op with
      AllocTuple (_, ty, args)
    | AllocArray (ty, args) ->
         live_atoms funs (live_type live ty) args
    | AllocUnion (_, ty, ty_var, const_var, args) ->
         live_atoms funs (live_type (SymbolSet.add (SymbolSet.add live const_var) ty_var) ty) args
    | AllocDTuple (ty, ty_var, tag, args) ->
         live_atoms funs (live_type (SymbolSet.add (live_atom funs live tag) ty_var) ty) args
    | AllocVArray (ty, a1, a2) ->
         live_atom funs (live_atom funs (live_type live ty) a1) a2
    | AllocModule (ty, ty_var, fields) ->
         let live =
            SymbolTable.fold (fun live v a ->
                  live_atom funs (SymbolSet.add live v) a) live fields
         in
            live_type (SymbolSet.add live ty_var) ty
    | AllocRecord (_, ty, ty_var, fields) ->
         let live =
            List.fold_left (fun live (label, a) ->
                  live_atom funs (SymbolSet.add live label) a) live fields
         in
            live_type (SymbolSet.add live ty_var) ty
    | AllocFrame (ty, ty_var, tyl) ->
         live_type_list (live_type (SymbolSet.add live ty_var) ty) tyl

(*
 * Expressions.
 *)
let rec live_exp genv preserve export funs live e =
   let pos = string_pos "live_exp" (exp_pos e) in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            let live = live_exp genv preserve export funs live e in
               if SymbolSet.mem live v then
                  live_atom funs (live_type live ty) a
               else
                  live
       | LetExt (v, t1, s, t2, ty_args, al, e) ->
            let live = live_exp genv preserve export funs live e in
            let live = live_atoms funs (live_type_list (live_type (live_type (live_var funs live v) t1) t2) ty_args) al in
               live
       | TailCall (f, args) ->
            let live = live_atom funs live f in
            let live = live_args funs live pos f args in
               live
       | Match (a, cases) ->
            List.fold_left (fun live (_, e) -> live_exp genv preserve export funs live e) (live_atom funs live a) cases
       | MatchDTuple (a, cases) ->
            List.fold_left (fun live (a_opt, e) ->
                  live_exp genv preserve export funs (live_atom_opt funs live a_opt) e) (live_atom funs live a) cases

       | LetAlloc (v, op, e) ->
            let live = live_exp genv preserve export funs live e in
               if SymbolSet.mem live v then
                  live_alloc_op funs live op
               else
                  live
       | LetSubscript (v, ty, a1, a2, e) ->
            live_subscript_exp genv preserve export pos funs live v ty a1 a2 e
       | SetSubscript (a1, a2, a3, ty, e) ->
            live_set_subscript_exp genv preserve export pos funs live a1 a2 ty a3 e
       | SetGlobal (v, ty, a, e) ->
            live_exp genv preserve export funs (live_atom funs (live_type (live_var funs live v) ty) a) e
       | LetFuns (fundefs, e) ->
            live_funs_exp genv preserve export funs live fundefs e
       | LetApply (v, ty, f, args, e) ->
            (* We can't drop LetApply because of side-effects *)
            let live = live_type live ty in
            let live = live_atom funs live f in
            let live = live_args funs live pos f args in
               live_exp genv preserve export funs live e
       | Return a ->
            live_atom funs live a
       | Try (e1, v, e2) ->
            let live = live_exp genv preserve export funs live e1 in
            let live = live_exp genv preserve export funs live e2 in
               live
       | Raise (a, ty) ->
            let live = live_atom funs live a in
               live_type live ty
       | LetClosure _
       | LetRec _ ->
            raise (IRException (pos, InternalError))

(*
 * Function definitions.
 * Calculate fixpoint based on live functions.
 *)
and live_funs_exp genv preserve export funs live fundefs e =
   let live = live_exp genv preserve export funs live e in
      live_funs genv preserve export funs live fundefs

and live_funs genv preserve export funs live fundefs =
   let step live =
      List.fold_left (fun live def ->
            let f, loc, gflag, ty_vars, ty, vars, e = def in
               if SymbolSet.mem live f then
                  let pos = string_pos "live_step" (fundef_pos f (loc, gflag, ty_vars, ty, vars, e)) in
                  let ty_vars, ty_res = dest_fun_type genv pos ty in
                  let live = live_exp genv preserve export funs live e in
                  let live = live_type live ty_res in
                  let len1 = List.length vars in
                  let len2 = List.length ty_vars in
                     if len1 <> len2 then
                        raise (IRException (pos, ArityMismatch (len1, len2)));
                     if SymbolTable.mem export f then
                        List.fold_left2 (fun live v ty ->
                              let live = SymbolSet.add live v in
                                 live_type live ty) live vars ty_vars
                     else
                        List.fold_left2 (fun live v ty ->
                              let live =
                                 match preserve with
                                    PreserveNone ->
                                       live
                                  | PreserveFrames ->
                                       if is_frame_type genv pos ty then
                                          SymbolSet.add live v
                                       else
                                          live
                                  | PreserveAll ->
                                       SymbolSet.add live v
                              in
                                 if SymbolSet.mem live v then
                                    live_type live ty
                                 else
                                    live) live vars ty_vars
               else
                  live) live fundefs
   in
   let rec fixpoint live =
      let live' = step live in
         if SymbolSet.equal live' live then
            live
         else
            fixpoint live'
   in
      fixpoint live

(*
 * Subscript.
 * Be careful about subscript labels.
 *)
and live_subscript_exp genv preserve export pos funs live v ty a1 a2 e =
   let pos = string_pos "live_subscript_exp" pos in
   let live = live_exp genv preserve export funs live e in
      if SymbolSet.mem live v then
         let live = live_atom funs live a1 in
         let live =
            match a2 with
               AtomFrameLabel (frame, field)
             | AtomRecordLabel (frame, field)
             | AtomModuleLabel (frame, field) ->
                  SymbolSet.add (SymbolSet.add live frame) field
             | _ ->
                  live_atom funs live a2
         in
         let live = live_type live ty in
            live
      else
         live

(*
 * Set a subsript.  This operation is live only if the
 * field is live.
 *)
and live_set_subscript_exp genv preserve export pos funs live a1 a2 ty a3 e =
   let pos = string_pos "live_set_subscript_exp" pos in
   let live = live_exp genv preserve export funs live e in
   let live = live_atom funs live a1 in
      match a2 with
         AtomFrameLabel (frame, field)
       | AtomModuleLabel (frame, field)
       | AtomRecordLabel (frame, field) ->
            if SymbolSet.mem live frame && SymbolSet.mem live field then
               live_atom funs (live_atom funs (live_type live ty) a2) a3
            else
               live
       | _ ->
            live_atom funs (live_atom funs (live_type live ty) a2) a3

(*
 * Live subfields.
 *)
let live_fields live fields =
   SymbolTable.fold (fun live field ty ->
         if SymbolSet.mem live field then
            live_type live ty
         else
            live) live fields

(*
 * Live initialiser.
 *)
let live_init funs live init =
   match init with
      InitAtom a ->
         live_atom funs live a
    | InitString _ ->
         live
    | InitAlloc op ->
         live_alloc_op funs live op

(*
 * Program live analysis.
 *)
let live_step preserve live prog =
   let { prog_funs = funs;
         prog_globals = globals;
         prog_tags = tags;
         prog_import = import;
         prog_export = export;
         prog_types = types
       } = prog
   in
   let genv = genv_of_prog prog in

   (* Add exports *)
   let live =
      SymbolTable.fold (fun live v { export_type = ty } ->
            let live = live_var funs live v in
            let live = live_type live ty in
               live) live export
   in

   (* Add globals that are referenced within other globals *)
   let live =
      SymbolTable.fold (fun live v (ty, init) ->
         if SymbolSet.mem live v then
            (*
             * This global is live, so anything that the initialiser
             * references must also be live.
             *)
            let live = live_type live ty in
            let live = live_init funs live init in
               live
         else
            (* This global isn't live (yet), so ignore the initialiser *)
            live) live globals
   in

   (* Scan functions *)
   let fundefs =
      SymbolTable.fold (fun fundefs f (loc, gflag, ty, ty_vars, vars, e) ->
            (f, loc, gflag, ty, ty_vars, vars, e) :: fundefs) [] funs
   in
   let live = live_funs genv preserve export funs live fundefs in

   (* Imports *)
   let live =
      SymbolTable.fold (fun live v { import_type = ty } ->
            if SymbolSet.mem live v then
               live_type live ty
            else
               live) live import
   in

   (* Tags *)
   let live =
      SymbolTable.fold (fun live v (ty_var, tyl) ->
            if SymbolSet.mem live v then
               live_type_list (SymbolSet.add live ty_var) tyl
            else
               live) live tags
   in

   (* Scan types *)
   let live =
      SymbolTable.fold (fun live v tydef ->
            if SymbolSet.mem live v then
               live_tydef live tydef
            else
               live) live types
   in
      live

(*
 * Fixpoint.
 *)
let live_prog preserve prog =
   let rec fixpoint live =
      let live' = live_step preserve live prog in
         if SymbolSet.equal live' live then
            live
         else
            fixpoint live'
   in

   (* Add boot types *)
   let { boot_ty_unit_var = ty_unit_var;
         boot_ty_bool_var = ty_bool_var;
         boot_ty_exn_var = ty_exn_var
       } = prog.prog_boot
   in
   let live = SymbolSet.empty in
   let live = SymbolSet.add live ty_unit_var in
   let live = SymbolSet.add live ty_bool_var in
   let live = SymbolSet.add live ty_exn_var in
      fixpoint live

(************************************************************************
 * DEADCODE ELIM
 ************************************************************************)

(*
 * Filter out dead args.
 *)
let filter_vars live pos vars args =
   let pos = string_pos "filter_vars" pos in
   let len1 = List.length vars in
   let len2 = List.length args in
      if len1 <> len2 then
         raise (IRException (pos, ArityMismatch (len1, len2)));
      let args =
         List.fold_left2 (fun args v a ->
               if SymbolSet.mem live v then
                  a :: args
               else
                  args) [] vars args
      in
         List.rev args

let filter_args funs live pos f args =
   let pos = string_pos "filter_args" pos in
      try
         let f = var_of_atom pos f in
         let _, _, _, _, vars, _ = SymbolTable.find funs f in
            filter_vars live pos vars args
      with
         Not_found ->
            args

(*
 * Elim dead expressions.
 *)
let rec dead_exp genv funs live e =
   let pos = string_pos "dead_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetAtom (v, ty, a, dead_exp genv funs live e))
            else
               dead_exp genv funs live e
       | LetExt (v, t1, s, t2, ty_args, args, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetExt (v, t1, s, t2, ty_args, args, dead_exp genv funs live e))
            else
               dead_exp genv funs live e
       | TailCall (f, args) ->
            let args = filter_args funs live pos f args in
               make_exp loc (TailCall (f, args))
       | Match (a, cases) ->
            make_exp loc (Match (a, List.map (fun (s, e) -> s, dead_exp genv funs live e) cases))
       | MatchDTuple (a, cases) ->
            make_exp loc (MatchDTuple (a, List.map (fun (a_opt, e) -> a_opt, dead_exp genv funs live e) cases))
       | LetAlloc (v, op, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetAlloc (v, op, dead_exp genv funs live e))
            else
               dead_exp genv funs live e
       | LetSubscript (v, ty, a1, a2, e) ->
            if SymbolSet.mem live v then
               make_exp loc (LetSubscript (v, ty, a1, a2, dead_exp genv funs live e))
            else
               dead_exp genv funs live e
       | SetSubscript (a1, a2, a3, ty, e) ->
            dead_set_subscript_exp genv funs live pos loc a1 a2 a3 ty e
       | SetGlobal (v, ty, a, e) ->
            make_exp loc (SetGlobal (v, ty, a, dead_exp genv funs live e))
       | LetFuns (fundefs, e) ->
            dead_funs_exp genv funs live pos loc fundefs e
       | LetApply (v, ty, f, args, e) ->
            let args = filter_args funs live pos f args in
            let e = dead_exp genv funs live e in
               make_exp loc (LetApply (v, ty, f, args, e))
       | Try (e1, v, e2) ->
            let e1 = dead_exp genv funs live e1 in
            let e2 = dead_exp genv funs live e2 in
               make_exp loc (Try (e1, v, e2))
       | Return _
       | Raise _ ->
            e
       | LetRec _
       | LetClosure _ ->
            raise (IRException (pos, InternalError))

(*
 * Dead functions.
 * Some functions may be omitted.
 *)
and dead_funs_exp genv funs live pos loc fundefs e =
   let pos = string_pos "dead_funs_exp" pos in
   let funs =
      List.fold_left (fun funs (f, loc, gflag, ty, ty_vars, vars, e) ->
            SymbolTable.add funs f (loc, gflag, ty, ty_vars, vars, e)) funs fundefs
   in
   let fundefs =
      List.fold_left (fun fundefs (f, loc, gflag, ty_vars, ty, vars, e) ->
            if SymbolSet.mem live f then
               let ty_args, ty_res = dest_fun_type genv pos ty in
               let ty_args = filter_vars live pos vars ty_args in
               let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
               let vars = List.filter (SymbolSet.mem live) vars in
               let e = dead_exp genv funs live e in
               let def = f, loc, gflag, ty_vars, ty_fun, vars, e in
                  def :: fundefs
            else
               fundefs) [] fundefs
   in
   let e = dead_exp genv funs live e in
      if fundefs = [] then
         e
      else
         make_exp loc (LetFuns (List.rev fundefs, e))

(*
 * Only keep the subscript if the field range is live.
 *)
and dead_set_subscript_exp genv funs live pos loc a1 a2 a3 ty e =
   let pos = string_pos "dead_set_subscript_exp" pos in
   let e = dead_exp genv funs live e in
      if is_live_atom live a1 then
         match a2 with
            AtomFrameLabel (frame, field)
          | AtomRecordLabel (frame, field)
          | AtomModuleLabel (frame, field) ->
               if SymbolSet.mem live field then
                  make_exp loc (SetSubscript (a1, a2, a3, ty, e))
               else
                  e
          | _ ->
               make_exp loc (SetSubscript (a1, a2, a3, ty, e))
      else
         e

(*
 * Deadcode elim for functions eliminates dead params
 * and their types.
 *)
let dead_funs genv live funs =
   let dead_fun table f def =
      if SymbolSet.mem live f then
         let pos = string_pos "dead_funs" (fundef_pos f def) in
         let loc, info, ty_vars, ty, vars, e = def in
         let ty_args, ty_res = dest_fun_type genv pos ty in
         let ty_args = filter_vars live pos vars ty_args in
         let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
         let vars = List.filter (SymbolSet.mem live) vars in
         let e = dead_exp genv funs live e in
            SymbolTable.add table f (loc, info, ty_vars, ty_fun, vars, e)
      else
         table
   in
      SymbolTable.fold dead_fun SymbolTable.empty funs

(*
 * Remove dead fields from expressions.
 *)
let dead_fields live fields =
   SymbolTable.fold (fun fields field ty ->
         if SymbolSet.mem live field then
            SymbolTable.add fields field ty
         else
            fields) SymbolTable.empty fields

let dead_frames live frames =
   SymbolTable.fold (fun frames frame (vars, fields) ->
         if SymbolSet.mem live frame then
            SymbolTable.add frames frame (vars, dead_fields live fields)
         else
            frames) SymbolTable.empty frames

(*
 * Generic table entry removal.
 *)
let dead_table live table =
   SymbolTable.fold (fun table v x ->
         if SymbolSet.mem live v then
            SymbolTable.add table v x
         else
            table) SymbolTable.empty table

(************************************** global function *)

let dead_prog preserve prog =
   let { prog_import = import;
         prog_export = export;
         prog_types = types;
         prog_globals = globals;
         prog_tags = tags;
         prog_funs = funs
       } = prog
   in
   let _ =
      if debug debug_dead then
         debug_prog "dead: before" prog
   in
   let genv    = genv_of_prog prog in
   let live    = live_prog preserve prog in
   let types   = dead_table live types in
   let funs    = dead_funs genv live funs in
   let import  = dead_table live import in
   let export  = dead_table live export in
   let globals = dead_table live globals in
   let tags    = dead_table live tags in
   let prog =
      { prog with prog_import = import;
                  prog_export = export;
                  prog_types = types;
                  prog_globals = globals;
                  prog_tags = tags;
                  prog_funs = funs
      }
   in
   let _ =
      if debug debug_dead then
         debug_prog "dead: after" prog
   in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
