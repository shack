(*
 * Translate a program into FIR.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Debug
open Symbol
open Location
open Field_table

open Fir_state

open Aml_set

open Aml_ir
open Aml_ir_ds
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_env
open Aml_ir_type
open Aml_ir_tydef
open Aml_ir_print
open Aml_ir_tast_type
open Aml_ir_tast_builtin
open Aml_ir_standardize

module Pos = MakePos (struct let name = "Aml_ir_tast_exp" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Partial-match information.
 * Based on the paper "ML pattern match compilation and partial evaluation",
 * by Peter Sestoft, in Dagstuhl Seminar on Partial Evaluation, LNCS, Springer.
 *
 * There is positive an negative information.
 * Positive:
 *    PosInt i: the term is number i
 *    PosChar c: the term is character c
 *    PosString s: the term is string s
 *    PosTuple pterms: the term is a tuple
 *    PosConst (i, pterms): the term has constructor i and subterms pterms
 *    PosDTuple (const_var, pterms): the term has tag const_var, and subterms pterms
 *
 * Negative:
 *    NegInt il: the term is not the numbers in il
 *    NegChar cl: the term is not the chars in cl
 *    NegString sl: the term is not the strings in sl
 *    NegConst (span, il): the term does not have any of the listed constructors
 *    NegDTuple const_vars: the term is a dtuple, and the tag is _not_ one of the const_vars
 *)
type termd_core =
   PosInt of int_set
 | PosChar of char_set
 | PosString of string_set
 | PosConst of const_span * termd list
 | PosDTuple of const_var * termd list
 | NegConst of const_span * const_set
 | NegDTuple of SymbolSet.t
 | NegAbstract

and const_span = int

and const_set =
   NegSet of int list
 | PosSet of int

and termd = atom * termd_core

(*
 * Infinite span is used for exceptions.
 *)
let infinite_span = max_int

(*
 * We'll need this is some places where termd is ignored.
 *)
let termd_empty = (AtomInt 0x12345678, NegAbstract)

(*
 * Success takes two environments.
 *)
type vars = (var * ty * atom) list
type cont_patt = genv -> tenv -> venv -> vars -> termd -> genv * exp * ty
type fail_patt = genv -> termd -> genv * exp * ty

(*
 * Continuation for an expression.
 *)
type fenv = (ty_var list * ty option) SymbolTable.t

type cont_simp  = genv -> tenv -> venv -> genv * exp * ty
type cont_exp   = genv -> tenv -> venv -> atom -> genv * exp * ty
type cont_field = genv -> fenv -> tenv -> venv -> genv * exp * ty

(*
 * Type fields.
 *)
let fenv_add = SymbolTable.add
let fenv_empty = SymbolTable.empty

(************************************************************************
 * CONSTANTS
 ************************************************************************)

(*
 * Indices into name records.
 *)
let in_index = 0
let out_index = 1
let open_index = 2

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Project the variables,
 * and add them to the variable environment.
 *)
let rec wrap_vars genv tenv venv loc (vars : vars) (cont : cont_simp) : genv * exp * ty =
   match vars with
      (v, ty, a) :: vars ->
         let venv = venv_add_var venv v ty in
         let genv, e, ty_exp = wrap_vars genv tenv venv loc vars cont in
         let e = make_exp loc (LetAtom (v, ty, a, e)) in
            genv, e, ty_exp
    | [] ->
         cont genv tenv venv

(*
 * The outermost continuation returns the atom.
 *)
let cont_return pos loc genv tenv venv a =
   genv, make_exp loc (Return a), type_of_atom genv venv pos loc a

(*
 * Duplicate the types in a list.
 *)
let dup_array_types length ty =
   let rec dup types length =
      if length = 0 then
         types
      else
         dup (ty :: types) (pred length)
   in
      dup [] length

(*
 * Collect the variables that are bound in the pattern.
 *)
let rec vars_of_patts vars pl =
   List.fold_left vars_of_patt vars pl

and vars_of_record_patts vars fields =
   List.fold_left (fun vars (_, p) ->
         vars_of_patt vars p) vars fields

and vars_of_patt vars p =
   let pos = string_pos "vars_of_patt" (tast_patt_pos p) in
   let loc = Aml_tast_ds.loc_of_patt p in
   let p = Aml_tast_ds.dest_patt_core p in
      match p with
         Aml_tast.PattInt _
       | Aml_tast.PattChar _
       | Aml_tast.PattString _
       | Aml_tast.PattWild _
       | Aml_tast.PattExpr _
       | Aml_tast.PattRange _ ->
            vars

       | Aml_tast.PattVar (v, ty) ->
            (v, ty) :: vars

       | Aml_tast.PattTuple (pl, _)
       | Aml_tast.PattArray (pl, _)
       | Aml_tast.PattConst (_, _, pl, _) ->
            vars_of_patts vars pl

       | Aml_tast.PattRecord (fields, _) ->
            vars_of_record_patts vars fields

       | Aml_tast.PattChoice (p, _, _)
       | Aml_tast.PattWhen (p, _, _) ->
            vars_of_patt vars p

       | Aml_tast.PattAs (p1, p2, _) ->
            vars_of_patt (vars_of_patt vars p1) p2

       | Aml_tast.PattConstrain (p, _) ->
            vars_of_patt vars p

let vars_of_patt p =
   vars_of_patt [] p

(************************************************************************
 * PATTERNS                                                             *
 ************************************************************************)

(*
 * Default negative information, constructed from a type.
 *)
let rec neg_termd genv pos ty =
   match expand_type_core genv pos ty with
      TyVoid ->
         raise (IRException (pos, StringTypeError ("illegal void type", ty)))
    | TyInt ->
         PosInt IntSet.max_set
    | TyChar ->
         PosChar CharSet.max_set
    | TyString ->
         PosString StringSet.max_set

    | TyTuple _
    | TyFrame _
    | TyRecord _
    | TyModule _
    | TyArray _ ->
         NegConst (1, NegSet [])

    | TyUnion (ty_var, tyl, index) ->
         let fields = apply_union_type genv pos ty_var tyl in
         let span = FieldTable.cardinal fields in
         let indexes =
            match index with
               AnyIndex ->
                  NegSet []
             | NegIndex labels ->
                  let set =
                     SymbolSet.fold (fun set const_var ->
                           let _, i = lookup_union_field fields pos const_var in
                              i :: set) [] labels
                  in
                     NegSet set
             | PosIndex const_var ->
                  PosSet (snd (lookup_union_field fields pos const_var))
         in
            NegConst (span, indexes)

    | TyDTuple _ ->
         NegDTuple SymbolSet.empty

    | TyAll (_, ty)
    | TyExists (_, ty) ->
         neg_termd genv pos ty
(*
    | TyProject (name, tyl) ->
         let ty' = apply_type genv pos ty name tyl in
            if ty' == ty then
               NegAbstract
            else
               neg_termd genv pos ty'
 *)
    | TyProject _

    | TyVar _
    | TyExternal _
    | TyFormat _
    | TyFloat
    | TyFun _
    | TyTag _ ->
         NegAbstract

    | TyApply _ ->
         raise (IRException (pos, InternalError))

(*
 * Expand a tuple.
 *)
let rec build_expand_tuple_patt genv tenv venv pos loc i a0 tyl (termds : termd list) f =
   match tyl with
      ty :: tyl ->
         let v = new_symbol_string "tuple" in
         let venv = venv_add_var venv v ty in
         let termd = AtomVar v, neg_termd genv pos ty in
         let genv, e, ty_exp = build_expand_tuple_patt genv tenv venv pos loc (succ i) a0 tyl (termd :: termds) f in
         let e = LetSubscript (v, ty, a0, AtomInt i, e) in
            genv, make_exp loc e, ty_exp
    | [] ->
         f genv tenv venv (List.rev termds)

let build_expand_tuple_patt genv tenv venv pos loc a0 tyl f =
   build_expand_tuple_patt genv tenv venv pos loc 0 a0 tyl [] f

(*
 * The pattern builder:
 *    genv: the environment.
 *    vars: the variables that are bound in the pattern
 *    termd: the info about the term being matched
 *    patt: the current pattern
 *    fail: the function that generates code for the fail case
 *    cont: the function that generates code the the success case
 *)
let rec build_patt genv tenv venv (vars : vars) (termd : termd) (patt : Aml_tast.patt) (fail : fail_patt) (cont : cont_patt) : genv * exp * ty =
   let pos = string_pos "build_patt" (tast_patt_pos patt) in
   let loc = Aml_tast_ds.loc_of_patt patt in
   let patt = Aml_tast_ds.dest_patt_core patt in
      match patt with
         Aml_tast.PattInt (i, _) ->
            build_int_patt genv tenv venv pos loc vars termd i fail cont
       | Aml_tast.PattChar (c, _) ->
            build_char_patt genv tenv venv pos loc vars termd c fail cont
       | Aml_tast.PattString (s, _) ->
            build_string_patt genv tenv venv pos loc vars termd s fail cont
       | Aml_tast.PattWild _ ->
            cont genv tenv venv vars termd
       | Aml_tast.PattVar (v, ty) ->
            let genv, ty = build_type genv tenv pos ty in
               cont genv tenv venv ((v, ty, fst termd) :: vars) termd
       | Aml_tast.PattTuple (pl, ty) ->
            build_tuple_patt genv tenv venv pos loc vars termd pl ty fail cont
       | Aml_tast.PattRecord (fields, ty) ->
            build_record_patt genv tenv venv pos loc vars termd fields ty fail cont
       | Aml_tast.PattArray (pl, ty) ->
            build_array_patt genv tenv venv pos loc vars termd pl ty fail cont
       | Aml_tast.PattConst (ty_var, const_var, patts, ty) ->
            build_const_patt genv tenv venv pos loc vars termd ty_var const_var patts ty fail cont
       | Aml_tast.PattChoice (p1, p2, _) ->
            build_choice_patt genv tenv venv pos loc vars termd p1 p2 fail cont
       | Aml_tast.PattAs (p1, p2, _) ->
            build_as_patt genv tenv venv pos loc vars termd p1 p2 fail cont

       | Aml_tast.PattExpr (e, _) ->
            build_expr_patt genv tenv venv pos loc vars termd e fail cont
       | Aml_tast.PattRange (e1, e2, _) ->
            build_range_patt genv tenv venv pos loc vars termd e1 e2 fail cont
       | Aml_tast.PattWhen (p, e, _) ->
            build_when_patt genv tenv venv pos loc vars termd p e fail cont
       | Aml_tast.PattConstrain (p, _) ->
            build_patt genv tenv venv vars termd p fail cont

(*
 * Expr.
 *)
and build_expr_patt genv tenv venv pos loc vars ((a1, termd') as termd) e fail cont =
   let pos = string_pos "build_expr_patt" pos in
   let ty_bool = genv_ty_bool genv loc in
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
      wrap_vars genv tenv venv loc vars (fun genv tenv venv ->
      build_exp genv tenv venv e (fun genv tenv venv a2 ->
            let genv, true_exp, ty_exp = cont genv tenv venv vars termd in
            let genv, false_exp, _ = fail genv termd in
            let e = Match (AtomBinop (EqIntOp, a2, a1), [true_set, true_exp; false_set, false_exp]) in
               genv, make_exp loc e, ty_exp))

and build_range_patt genv tenv venv pos loc vars ((a0, termd') as termd) e1 e2 fail cont =
   let pos = string_pos "build_range_patt" pos in
   let t1 = Aml_tast_util.type_of_exp e1 in
   let t2 = Aml_tast_util.type_of_exp e2 in
   let ty_bool = genv_ty_bool genv loc in
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
      wrap_vars genv tenv venv loc vars (fun genv tenv venv ->
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
            let genv, true_exp, ty_exp = cont genv tenv venv vars termd in
            let genv, false_exp, _ = fail genv termd in
            let e =
               Match (AtomBinop (AndIntOp, AtomBinop (GeIntOp, a0, a1), AtomBinop (LeIntOp, a0, a2)), (**)
                         [true_set, true_exp; false_set, false_exp])
            in
               genv, make_exp loc e, ty_exp)))

and build_when_patt genv tenv venv pos loc vars termd p e fail cont =
   let pos = string_pos "build_when_patt" pos in
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
      build_patt genv tenv venv vars termd p fail (fun genv tenv venv vars termd ->
      wrap_vars genv tenv venv loc vars (fun genv tenv venv ->
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let genv, true_exp, ty_exp = cont genv tenv venv vars termd in
            let genv, false_exp, _ = fail genv termd in
            let e = Match (a, [true_set, true_exp; false_set, false_exp]) in
               genv, make_exp loc e, ty_exp)))

(*
 * Check constants.
 *)
and build_int_patt genv tenv venv pos loc vars ((a0, termd') as termd) i fail cont =
   let pos = string_pos "build_int_patt" pos in
      match termd' with
         PosInt s ->
            if IntSet.is_singleton s && IntSet.dest_singleton s = i then
               cont genv tenv venv vars termd
            else if IntSet.mem_point i s then
               let genv, true_exp, ty_exp = cont genv tenv venv vars (a0, PosInt (IntSet.add_point s i)) in
               let genv, false_exp, _ = fail genv (a0, PosInt (IntSet.subtract_point s i)) in
               let e =
                  Match (a0, [IntSet (IntSet.of_point i), true_exp;
                              IntSet IntSet.max_set, false_exp])
               in
                  genv, make_exp loc e, ty_exp
            else
               fail genv termd
       | _ ->
            raise (Invalid_argument "Aml_tast_type.build_int_patt")

and build_char_patt genv tenv venv pos loc vars ((a0, termd') as termd) i fail cont =
   let pos = string_pos "build_char_patt" pos in
      match termd' with
         PosChar s ->
            if CharSet.is_singleton s && CharSet.dest_singleton s = i then
               cont genv tenv venv vars termd
            else if CharSet.mem_point i s then
               let genv, true_exp, ty_exp = cont genv tenv venv vars (a0, PosChar (CharSet.add_point s i)) in
               let genv, false_exp, _ = fail genv (a0, PosChar (CharSet.subtract_point s i)) in
               let e =
                  Match (a0, [CharSet (CharSet.of_point i), true_exp;
                              CharSet CharSet.max_set, false_exp])
               in
                  genv, make_exp loc e, ty_exp
         else
            fail genv termd
    | _ ->
         raise (Invalid_argument "Aml_tast_type.build_char_patt")

and build_string_patt genv tenv venv pos loc vars ((a0, termd') as termd) i fail cont =
   let pos = string_pos "build_string_patt" pos in
      match termd' with
         PosString s ->
            if StringSet.is_singleton s && StringSet.dest_singleton s = i then
               cont genv tenv venv vars termd
            else if StringSet.mem_point i s then
               let genv, true_exp, ty_exp = cont genv tenv venv vars (a0, PosString (StringSet.add_point s i)) in
               let genv, false_exp, _ = fail genv (a0, PosString (StringSet.subtract_point s i)) in

               (* Call the strcmp function *)
               let v = new_symbol_string "streq" in
               let ty_bool = genv_ty_bool genv loc in
               let true_set = genv_true_set genv in
               let false_set = genv_false_set genv in
               let genv, v_string = genv_add_string genv loc i in
               let e = make_exp loc (Match (AtomVar v, [true_set, true_exp; false_set, false_exp])) in
               let ty_string = make_type loc TyString in
               let ty_strcmp = make_type loc (TyFun ([ty_string; ty_string], ty_bool)) in
               let e = make_exp loc (LetExt (v, ty_bool, "string_equal", ty_strcmp, [], [a0; AtomVar v_string], e)) in
                  genv, e, ty_exp
         else
            fail genv termd
    | _ ->
         raise (Invalid_argument "Aml_tast_type.build_string_patt")

(*
 * Translate a tuple.
 *)
and build_tuple_patt genv tenv venv pos loc vars (a0, termd) patts ty (fail : fail_patt) (cont : cont_patt) =
   let pos = string_pos "build_tuple_patt" pos in

   (*
    * This is the function that generates the code for
    * the pattern matching.
    *)
   let rec compile genv tenv venv vars matched (termds : termd list) patts =
      match termds, patts with
         termd :: termds, patt :: patts ->
            let cont' genv tenv venv vars termd =
               compile genv tenv venv vars (termd :: matched) termds patts
            in
            let fail' genv termd =
               (* Reconstruct the termd for the tuple *)
               let termds = List.rev matched @ (termd :: termds) in
               let termd = a0, PosConst (0, termds) in
                  fail genv termd
            in
               build_patt genv tenv venv vars termd patt fail' cont'

       | [], [] ->
            let termds = List.rev matched in
            let termd = a0, PosConst (0, termds) in
               cont genv tenv venv vars termd

       | _ ->
            raise (IRException (pos, InternalError))
   in

   (*
    * Project all the fields in the tuple.
    *)
   let genv, ty = build_type genv tenv pos ty in
   let tyl = dest_tuple_type genv pos ty in
      match termd with
         PosConst (_, termds) ->
            compile genv tenv venv vars [] termds patts
       | NegConst (_, NegSet _) ->
            build_expand_tuple_patt genv tenv venv pos loc a0 tyl (fun genv tenv venv termds ->
                  compile genv tenv venv vars [] termds patts)
       | _ ->
            raise (IRException (pos, InternalError))

(*
 * Translate a record.
 *)
and build_record_patt genv tenv venv pos loc vars (v, termd) fields ty fail cont =
   let pos = string_pos "build_record_patt" pos in

   (*
    * This is the function that generates the code for
    * the pattern matching.  Similar to tuples, but the ordering
    * may be different.
    *)
   let genv, ty = build_type genv tenv pos ty in
   let _, rfields = dest_record_type genv pos ty in
   let rec compile genv tenv venv vars termds fields =
      match fields with
         (label, patt) :: fields ->
            let _, index = lookup_record_field rfields pos label in
            let cont' genv tenv venv vars termd =
               let termds = Mc_list_util.replace_nth index termd termds in
                  compile genv tenv venv vars termds fields
            in
            let fail' genv termd =
               let termds = Mc_list_util.replace_nth index termd termds in
                  fail genv (v, PosConst (0, termds))
            in
            let termd = List.nth termds index in
               build_patt genv tenv venv vars termd patt fail' cont'

       | [] ->
            cont genv tenv venv vars (v, PosConst (0, termds))
   in

   (*
    * Project all the fields in the record.
    *)
   let rfields = FieldTable.to_list rfields in
   let tyl = List.map (fun (_, (_, ty)) -> ty) rfields in
      match termd with
         PosConst (0, termds) ->
            compile genv tenv venv vars termds fields
       | NegConst (1, NegSet []) ->
            build_expand_tuple_patt genv tenv venv pos loc v tyl (fun genv tenv venv termds ->
                  compile genv tenv venv vars termds fields)
       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_record_patt")

(*
 * Translate an array.
 *)
and build_array_patt genv tenv venv pos loc vars (a0, termd) patts ty fail cont =
   (*
    * This is the function that generates the code for
    * the pattern matching.
    *)
   let length = List.length patts in
   let rec compile genv tenv venv vars matched termds patts =
      match termds, patts with
         termd :: termds, patt :: patts ->
            let cont' genv tenv venv vars termd =
               compile genv tenv venv vars (termd :: matched) termds patts
            in
            let fail' genv termd =
               (* Reconstruct the termd for the tuple *)
               let termds = List.rev matched @ (termd :: termds) in
               let termd = a0, PosConst (length, termds) in
                  fail genv termd
            in
               build_patt genv tenv venv vars termd patt fail' cont'

       | [], [] ->
            let termds = List.rev matched in
            let termd = a0, PosConst (length, termds) in
               cont genv tenv venv vars termd

       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_match_array_patt")
   in
      (*
       * Project all the fields in the tuple.
       *)
      match termd with
         PosConst (length', termds) ->
            if length' = length then
               compile genv tenv venv vars [] termds patts
            else
               fail genv (a0, termd)
       | NegConst (span, NegSet il) ->
            if List.mem length il then
               fail genv (a0, termd)
            else
               let genv, ty = build_type genv tenv pos ty in
               let ty = dest_array_type genv pos ty in
               let tyl = dup_array_types length ty in
               let genv, true_exp, ty_exp =
                  build_expand_tuple_patt genv tenv venv pos loc a0 tyl (fun genv tenv venv termds ->
                        compile genv tenv venv vars [] termds patts)
               in
               let genv, false_exp, _ =
                  fail genv (a0, NegConst (span, NegSet (length :: il)))
               in
               let e =
                  Match (AtomUnop (LengthOfBlockOp ty, a0), (**)
                            [IntSet (IntSet.of_point length), true_exp;
                             IntSet IntSet.max_set, false_exp])
               in
                  genv, make_exp loc e, ty_exp
       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_array_patt")

(*
 * Match a constructor.
 *)
and build_const_patt genv tenv venv pos loc vars termd ty_var const_var patts ty fail cont =
   let pos = string_exp_pos "build_const_patt" in
   let genv, ty = build_type genv tenv pos ty in
   let tydef = tenv_lookup_tydef tenv pos ty_var in
      match Aml_tast_ds.dest_tydef_inner_core tydef.Aml_tast.tydef_inner with
         Aml_tast.TyDefUnion (Aml_tast.NormalUnion, _) ->
            build_const_union_patt genv tenv venv pos loc vars termd ty_var const_var patts ty fail cont
       | Aml_tast.TyDefUnion (Aml_tast.ExnUnion, _) ->
            build_const_exn_patt genv tenv venv pos loc vars termd ty_var const_var patts ty fail cont
       | _ ->
            raise (IRException (pos, StringError "not a union type"))

(*
 * For a normal constructor,
 * we check if we know already which way the match will go.
 *)
and build_const_union_patt genv tenv venv pos loc vars termd ty_var const_var patts ty fail cont =
   let pos = string_pos "build_const_union_patt" pos in
   let a0, termd' = termd in

   (* Get the field types *)
   let _, ufields, _ = dest_union_type genv pos ty in
   let tyl, index = lookup_union_field ufields pos const_var in

   (*
    * This is the function that generates the code for
    * the pattern matching.
    *)
   let rec compile genv tenv venv vars matched termds patts =
      match termds, patts with
         termd :: termds, patt :: patts ->
            let cont' genv tenv venv vars termd =
               compile genv tenv venv vars (termd :: matched) termds patts
            in
            let fail' genv termd =
               (* Reconstruct the termd for the tuple *)
               let termds = List.rev matched @ (termd :: termds) in
               let termd = a0, PosConst (index, termds) in
                  fail genv termd
            in
               build_patt genv tenv venv vars termd patt fail' cont'

       | [], [] ->
            let termds = List.rev matched in
            let termd = a0, PosConst (index, termds) in
               cont genv tenv venv vars termd

       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_const_union_patt.compile")
   in
      match termd' with
         PosConst (index', termds) ->
            if index = index' then
               compile genv tenv venv vars [] termds patts
            else
               fail genv termd
       | NegConst (span, PosSet index') ->
            if index = index' then
               build_expand_tuple_patt genv tenv venv pos loc a0 tyl (fun genv tenv venv termds ->
                     compile genv tenv venv vars [] termds patts)
            else
               fail genv termd
       | NegConst (span, NegSet il) ->
            if List.mem index il then
               fail genv termd
            else if List.length il = pred span then
               build_expand_tuple_patt genv tenv venv pos loc a0 tyl (fun genv tenv venv termds ->
                     compile genv tenv venv vars [] termds patts)
            else
               let genv, true_case, ty_exp =
                  build_expand_tuple_patt genv tenv venv pos loc a0 tyl (fun genv tenv venv termds ->
                        compile genv tenv venv vars [] termds patts)
               in
               let genv, false_case, _ = fail genv (a0, NegConst (span, NegSet (index :: il))) in
               let e =
                  Match (a0, [IntSet (IntSet.of_point index), true_case;
                              IntSet IntSet.max_set, false_case])
               in
                  genv, make_exp loc e, ty_exp
       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_const_union_patt")

(*
 * For exceptions, we use the DTuple cases.
 *)
and build_const_exn_patt genv tenv venv pos loc vars termd ty_var const_var patts ty fail cont =
   let pos = string_pos "build_const_exn_patt" pos in
   let a0, termd' = termd in

   (* Get the field types *)
   let ty_tag = venv_lookup_var venv pos const_var in
   let _, tyl = dest_tag_type genv pos ty_tag in

   (*
    * This is the function that generates the code for
    * the pattern matching.
    *)
   let rec compile genv tenv venv vars matched termds patts =
      match termds, patts with
         termd :: termds, patt :: patts ->
            let cont' genv tenv venv vars termd =
               compile genv tenv venv vars (termd :: matched) termds patts
            in
            let fail' genv termd =
               (* Reconstruct the termd for the tuple *)
               let termds = List.rev matched @ (termd :: termds) in
               let termd = a0, PosDTuple (const_var, termds) in
                  fail genv termd
            in
               build_patt genv tenv venv vars termd patt fail' cont'

       | [], [] ->
            let termds = List.rev matched in
            let termd = a0, PosDTuple (const_var, termds) in
               cont genv tenv venv vars termd

       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_const_union_patt.compile")
   in
      match termd' with
         PosDTuple (const_var', termds) ->
            if Symbol.eq const_var' const_var then
               compile genv tenv venv vars [] termds patts
            else
               fail genv termd
       | NegDTuple const_vars ->
            if SymbolSet.mem const_vars const_var then
               fail genv termd
            else
               let genv, true_case, ty_exp =
                  build_expand_tuple_patt genv tenv venv pos loc a0 tyl (fun genv tenv venv termds ->
                        compile genv tenv venv vars [] termds patts)
               in
               let genv, false_case, _ = fail genv (a0, NegDTuple (SymbolSet.add const_vars const_var)) in
               let e =
                  MatchDTuple (a0, [Some (AtomVar const_var), true_case;
                                    None, false_case])
               in
                  genv, make_exp loc e, ty_exp
       | _ ->
            raise (Invalid_argument "Aml_ir_trans.build_const_union_patt")

(*
 * Choice allows either pattern to match.
 *)
and build_choice_patt genv tenv venv pos loc vars termd p1 p2 fail cont =
   build_patt genv tenv venv vars termd p1 (fun genv termd ->
         build_patt genv tenv venv vars termd p2 fail cont) cont

(*
 * As forces both patterns to match.
 *)
and build_as_patt genv tenv venv pos loc vars termd p1 p2 fail cont =
   build_patt genv tenv venv vars termd p1 fail (fun genv tenv venv vars termd ->
         build_patt genv tenv venv vars termd p2 fail cont)

(*
 * We have to be careful about compiling guarded commands
 * to avoid repetition of the body.  Instead, we compute the
 * variables that are bound by the pattern, and place the body
 * of the command in a function.
 *
 * This function performs the pattern match.
 * It takes the pattern/cont list for the pattern cases, where
 * cont produces a function call to the function containing the body.
 *
 * It detect an incomplete match (if the pattern falls off the
 * end), but it does not check for duplicate matches.
 *)
and build_guarded_body genv tenv venv loc termd ty cases =
   match cases with
      (patt, cont) :: patts ->
         build_patt genv tenv venv [] termd patt (fun genv termd ->
               build_guarded_body genv tenv venv loc termd ty patts) cont
    | [] ->
         let _ = string_warning loc "incomplete match" in
         let file, sline, schar, _, _ = dest_loc loc in
         let genv, v_file = genv_add_string genv loc (Symbol.to_string file) in
         let v_tuple = new_symbol_string "match_failure_tuple" in

         (* Type of the exception *)
         let exn_var = genv_exn_var genv in
         let match_failurecomplete_match_var = genv_match_failure_var genv in
         let ty =
            make_type loc (TyDTuple (exn_var, Some [make_type loc TyString;
                                                    make_type loc TyInt;
                                                    make_type loc TyInt]))
         in

         (* Args *)
         let args = [AtomVar v_file; AtomInt sline; AtomInt schar] in
         let alloc_op = AllocDTuple (ty, exn_var, AtomVar match_failure_var, args) in
         let e = make_exp loc (Raise (AtomVar v_tuple, ty)) in
         let e = make_exp loc (LetAlloc (v_tuple, alloc_op, e)) in
            genv, e, ty

(*
 * Build the function that defines the body of the
 * case.  The vars are the list of variables bound in
 * the pattern.
 *)
and build_guarded_function genv tenv venv pos loc vars e cont =
   let pos = string_pos "build_guarded_body" pos in

   (* Build the function type *)
   let vars, tyl = List.split vars in
   let genv, tyl = build_types genv tenv pos tyl in
   let genv, ty_exp = build_type genv tenv pos (Aml_tast_util.type_of_exp e) in
   let ty_fun = make_type loc (TyFun (tyl, ty_exp)) in

   (* Build the body *)
   let venv = List.fold_left2 venv_add_var venv vars tyl in
   let genv, e, _ = build_exp genv tenv venv e cont in

   (* Create a new function for the body *)
   let g = new_symbol_string "match_body" in
   let fundef = g, loc, FunLocalClass, [], ty_fun, vars, e in

   (* Build the function call to call the body *)
   let apply = make_exp loc (TailCall (AtomVar g, List.map (fun v -> AtomVar v) vars)) in
   let cont genv tenv venv vars _ =
      wrap_vars genv tenv venv loc vars (fun genv tenv venv -> genv, apply, ty_exp)
   in
      genv, fundef, cont

(*
 * Build the code for a guarded command.
 *)
and build_guarded genv tenv venv cases cont =
   let rec collect genv funs cases' cases =
      match cases with
         (p, e) :: cases ->
            let loc = Aml_tast_ds.loc_of_patt p in
            let pos = string_pos "build_guarded_guarded" (tast_patt_pos p) in
            let vars = vars_of_patt p in
            let genv, fundef, cont = build_guarded_function genv tenv venv pos loc vars e cont in
               collect genv (fundef :: funs) ((p, cont) :: cases') cases
       | [] ->
            genv, List.rev funs, List.rev cases'
   in
      collect genv [] [] cases

(*
 * This is similar code for a lambda pattern matching sequence.
 * This first part builds the inner function that is the
 * body of the match.
 *)
and build_lambda_match genv tenv venv pos loc patts e cont =
   let pos = string_pos "build_guarded_guarded" pos in
   let vars = vars_of_patts [] patts in
      build_guarded_function genv tenv venv pos loc vars e cont

(*
 * This builds the pattern matching code.
 * It is very much like a tuple match.
 *)
and build_lambda_body genv tenv venv pos loc (termds : termd list) patts cont =
   let pos = string_pos "build_lambda_body" pos in
   let rec compile genv tenv venv vars (termds : termd list) patts =
      match termds, patts with
         termd :: termds, patt :: patts ->
            let cont' genv tenv venv vars termd =
               compile genv tenv venv vars termds patts
            in
            let fail' genv termd =
               raise (IRException (pos, StringError "incomplete match"))
            in
               build_patt genv tenv venv vars termd patt fail' cont'

       | [], [] ->
            cont genv tenv venv vars termd_empty

       | _ ->
            raise (IRException (pos, InternalError))
   in
      compile genv tenv venv [] termds patts

(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)

(*
 * Translate a variable reference.
 * If the variable is not local, project it
 * from the outer modules.
 *)
and build_var_exp genv tenv venv pos loc var_name (cont : cont_exp) =
   let pos = string_pos "build_var_exp" pos in
      match var_name with
         Aml_tast.NameEnv v ->
            cont genv tenv venv (AtomVar v)

       | Aml_tast.NamePath (me_name, ext_var) ->
            build_path_exp genv tenv venv pos loc me_name ext_var cont

and build_me_name_exp genv tenv venv pos loc me_name (cont : cont_exp) =
   let pos = string_pos "build_me_exp" pos in
      match me_name with
         Aml_tast.ModNameEnv v ->
            cont genv tenv venv (AtomVar v)

       | Aml_tast.ModNameProj (me_name, ext_var) ->
            build_path_exp genv tenv venv pos loc me_name ext_var cont

       | Aml_tast.ModNameApply (me_name1, me_name2) ->
            raise (IRException (pos, NotImplemented "module application"))

and build_path_exp genv tenv venv pos loc me_name ext_var cont =
   let pos = string_pos "build_path_exp" pos in

   (*
    * No module applications are allowed.
    *)
   let rec expand_path me_name ext_vars =
      match me_name with
         Aml_tast.ModNameEnv v ->
            v, ext_vars
       | Aml_tast.ModNameProj (me_name, ext_var) ->
            expand_path me_name (ext_var :: ext_vars)
       | Aml_tast.ModNameApply _ ->
            raise (IRException (pos, StringVarError ("illegal variable", ext_var)))
   in
   let v, ext_vars = expand_path me_name [] in

   (*
    * Descend the type hierarchy,
    * and relocate the variable's type.
    *)
   let rec unwrap genv tenv venv v path ty_module ext_vars =
      match ext_vars with
         ext_var :: ext_vars ->
            (* Descend one more module *)
            let path = ext_var :: path in
            let ty_var, names, mt = dest_module_type genv pos ty_module in
            let ty = lookup_module_field names mt pos ext_var in
            let v' = new_symbol v in
            let venv = venv_add_var venv v' ty in
            let genv, e, ty_exp = unwrap genv tenv venv v' path ty ext_vars in
            let e = LetSubscript (v', ty, AtomVar v, AtomModuleLabel (ty_var, ext_var), e) in
               genv, make_exp loc e, ty_exp

       | [] ->
            let ty_var, names, mt = dest_module_type genv pos ty_module in
            let ty = lookup_module_field names mt pos ext_var in
            let v' = new_symbol v in
            let venv = venv_add_var venv v' ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v') in
            let e = LetSubscript (v', ty, AtomVar v, AtomModuleLabel (ty_var, ext_var), e) in
               genv, make_exp loc e, ty_exp
   in
   let ty_module = venv_lookup_var venv pos v in
      unwrap genv tenv venv v [] ty_module ext_vars

(*
 * When an expression is translated,
 * the continuation takes an atom and produces
 * the expression corresonding to the "rest"
 * of the computation.
 *)
and build_exp genv tenv venv e (cont : cont_exp) : genv * exp * ty =
   let pos = string_pos "build_exp" (tast_exp_pos e) in
   let loc = Aml_tast_ds.loc_of_exp e in
      match Aml_tast_ds.dest_exp_core e with
         Aml_tast.Number (i, _) ->
            cont genv tenv venv (AtomInt i)
       | Aml_tast.Char (c, _) ->
            cont genv tenv venv (AtomChar c)
       | Aml_tast.String (s, _) ->
            let genv, v = genv_add_string genv loc s in
            let venv = venv_add_var venv v (make_type loc TyString) in
               cont genv tenv venv (AtomVar v)
       | Aml_tast.Var (var_name, ty) ->
            build_var_exp genv tenv venv pos loc var_name cont
       | Aml_tast.Const (ty_var, const_var, args, ty, _) ->
            build_const_exp genv tenv venv pos loc ty_var const_var args ty cont
       | Aml_tast.Apply (e, ty_fun, args, ty) ->
            build_apply_exp genv tenv venv pos loc e ty_fun args ty cont
       | Aml_tast.Lambda (ty_vars, pl, e, ty) ->
            build_lambda_exp genv tenv venv pos loc ty_vars pl e ty cont
       | Aml_tast.Function (ty_vars, guarded, ty) ->
            build_function_exp genv tenv venv pos loc ty_vars guarded ty cont
       | Aml_tast.Let (lets, e, _) ->
            build_lets_exp genv tenv venv pos loc lets e cont
       | Aml_tast.LetRec (letrecs, e, _) ->
            build_letrecs_exp genv tenv venv pos loc letrecs e cont
       | Aml_tast.If (e1, e2, e3, ty) ->
            build_if_exp genv tenv venv pos loc e1 e2 e3 ty cont
       | Aml_tast.For (v, e1, to_flag, e2, e3) ->
            build_for_exp genv tenv venv pos loc v e1 to_flag e2 e3 cont
       | Aml_tast.While (e1, e2) ->
            build_while_exp genv tenv venv pos loc e1 e2 cont

       | Aml_tast.ApplyType (e, ty_args, ty) ->
            build_apply_type_exp genv tenv venv pos loc e ty_args ty cont

       | Aml_tast.Tuple (el, ty) ->
            build_tuple_exp genv tenv venv pos loc el ty cont
       | Aml_tast.Array (el, ty) ->
            build_array_exp genv tenv venv pos loc el ty cont
       | Aml_tast.ArraySubscript (e1, e2, ty) ->
            build_array_subscript_exp genv tenv venv pos loc e1 e2 ty cont
       | Aml_tast.ArraySetSubscript (e1, e2, e3) ->
            build_array_set_subscript_exp genv tenv venv pos loc e1 e2 e3 cont
       | Aml_tast.StringSubscript (e1, e2) ->
            build_string_subscript_exp genv tenv venv pos loc e1 e2 cont
       | Aml_tast.StringSetSubscript (e1, e2, e3) ->
            build_string_set_subscript_exp genv tenv venv pos loc e1 e2 e3 cont
       | Aml_tast.Record (fields, ty) ->
            build_record_exp genv tenv venv pos loc fields ty cont
       | Aml_tast.RecordUpdate (e, fields, ty) ->
            build_record_update_exp genv tenv venv pos loc e fields ty cont
       | Aml_tast.RecordProj (e, ty1, label, ty2) ->
            build_record_project_exp genv tenv venv pos loc e ty1 label ty2 cont
       | Aml_tast.RecordSetProj (e1, ty, label, e2) ->
            build_record_set_project_exp genv tenv venv pos loc e1 ty label e2 cont

       | Aml_tast.Sequence (e1, e2, _) ->
            build_sequence_exp genv tenv venv pos loc e1 e2 cont

       | Aml_tast.Match (e, guarded, ty) ->
            build_match_exp genv tenv venv pos loc e guarded ty cont
       | Aml_tast.Try (e, guarded, ty) ->
            build_try_exp genv tenv venv pos loc e guarded ty cont
       | Aml_tast.Raise (e, ty) ->
            build_raise_exp genv tenv venv pos loc e ty cont

       | Aml_tast.Constrain (e, _) ->
            build_exp genv tenv venv e cont

       | Aml_tast.In (me_name, ty) ->
            build_msg_exp in_index genv tenv venv pos loc me_name ty cont
       | Aml_tast.Out (me_name, ty) ->
            build_msg_exp out_index genv tenv venv pos loc me_name ty cont
       | Aml_tast.Open (me_name, ty) ->
            build_msg_exp open_index genv tenv venv pos loc me_name ty cont
       | Aml_tast.Action (el, ty) ->
            build_action_exp genv tenv venv pos loc el ty cont
       | Aml_tast.Restrict (v, mt_var, e, _) ->
            build_restrict_exp genv tenv venv pos loc v mt_var e cont
       | Aml_tast.Ambient (v, me, _) ->
            build_ambient_exp genv tenv venv pos loc v me cont
       | Aml_tast.Thread (e) ->
            build_thread_exp genv tenv venv pos loc e cont
       | Aml_tast.Migrate (msgs, e, ty) ->
            build_migrate_exp genv tenv venv pos loc msgs e ty cont
       | Aml_tast.LetModule (v, me, e, ty) ->
            build_let_module_exp genv tenv venv pos loc v me e ty cont

(*
 * Build a list of expressions.
 *)
and build_exps genv tenv venv el cont =
   let rec collect genv tenv venv atoms el =
      match el with
         e :: el ->
            build_exp genv tenv venv e (fun genv tenv venv a ->
                  collect genv tenv venv (a :: atoms) el)
       | [] ->
            cont genv tenv venv (List.rev atoms)
   in
      collect genv tenv venv [] el

and build_record_exps genv tenv venv fields cont =
   let rec collect genv tenv venv fields' fields =
      match fields with
         (label, e) :: fields ->
            build_exp genv tenv venv e (fun genv tenv venv a ->
                  collect genv tenv venv ((label, a) :: fields') fields)
       | [] ->
            cont genv tenv venv (List.rev fields')
   in
      collect genv tenv venv [] fields

(*
 * Translate a constructor application.
 * Be careful to catch exception types.
 *)
and build_const_exp genv tenv venv pos loc ty_var const_var args ty (cont : cont_exp) =
   (* Eval the args, then build the tuple, then apply the atoms *)
   let pos = string_pos "build_const_exp" pos in
   let tydef = tenv_lookup_tydef tenv pos ty_var in
      match Aml_tast_ds.dest_tydef_inner_core tydef.Aml_tast.tydef_inner with
         Aml_tast.TyDefUnion (Aml_tast.NormalUnion, _) ->
            build_const_union_exp genv tenv venv pos loc ty_var const_var args ty cont
       | Aml_tast.TyDefUnion (Aml_tast.ExnUnion, _) ->
            build_const_exn_exp genv tenv venv pos loc ty_var const_var args ty cont
       | _ ->
            raise (IRException (pos, StringError "not a union type"))

(*
 * For a normal union, build all the args,
 * and produce an AllocUnion.
 *)
and build_const_union_exp genv tenv venv pos loc ty_var const_var args ty (cont : cont_exp) =
   let pos = string_pos "build_const_union_exp" pos in
      build_exps genv tenv venv args (fun genv tenv venv args ->
            (* Get the type *)
            let genv, ty = build_type genv tenv pos ty in
            let ty_vars, ty_union = dest_all_type genv pos ty in
            let ty_var, fields, _ = dest_union_type genv pos ty_union in
            let tyl, _ = lookup_union_field fields pos const_var in

            (* Get tag and size for the allocated block *)
            let len1 = List.length tyl in
            let len2 = List.length args in
            let _ =
               if len1 <> len2 then
                  raise (IRException (int_pos 2 pos, TypeArityMismatch (ty, len1, len2)))
            in

            (* Assign the fields *)
            let v = new_symbol const_var in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetAlloc (v, AllocUnion (ty_vars, ty_union, ty_var, const_var, args), e) in
               genv, make_exp loc e, ty_exp)

(*
 * For an exception, the const_var is a tag,
 * and the value is built with AllocDTuple.
 *)
and build_const_exn_exp genv tenv venv pos loc ty_var const_var args ty (cont : cont_exp) =
   let pos = string_pos "build_const_exn_exp" pos in
      build_exps genv tenv venv args (fun genv tenv venv args ->
            (* Get the type *)
            let genv, ty = build_type genv tenv (int_pos 1 pos) ty in
            let ty_var, _ = dest_dtuple_type genv (int_pos 2 pos) ty in
            let ty_fields = List.map (type_of_atom genv venv pos loc) args in
            let ty = make_type loc (TyDTuple (ty_var, Some ty_fields)) in

            (* Assign the fields *)
            let v = new_symbol const_var in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetAlloc (v, AllocDTuple (ty, ty_var, AtomVar const_var, args), e) in
               genv, make_exp loc e, ty_exp)

(*
 * Boolean negation.
 *)
and build_not_exp genv tenv venv pos loc e cont =
   let pos = string_pos "build_not_exp" pos in

   (* Boolean values *)
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
   let atom_true = genv_true_atom genv loc in
   let atom_false = genv_false_atom genv loc in
   let ty_bool = genv_ty_bool genv loc in

   (* Place the rest in a function *)
   let v = new_symbol_string "not" in
   let venv = venv_add_var venv v ty_bool in
   let genv, body, ty_exp = cont genv tenv venv (AtomVar v) in
   let f = new_symbol_string "not_cont" in
   let ty_fun = make_type loc (TyFun ([ty_bool], ty_exp)) in

   (* Perform the match *)
   let true_exp = make_exp loc (TailCall (AtomVar f, [atom_false])) in
   let false_exp = make_exp loc (TailCall (AtomVar f, [atom_true])) in
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let e = make_exp loc (Match (a, [true_set, true_exp; false_set, false_exp])) in
            let e = make_exp loc (LetFuns ([f, loc, FunLocalClass, [], ty_fun, [v], body], e)) in
               genv, e, ty_exp)

(*
 * Short-circuit Boolean operations.
 * If bool_flag is set, this compiles for &&,
 * otherwise it compiles for ||.
 *)
and build_bool_exp bool_flag genv tenv venv pos loc e1 e2 cont =
   let pos = string_pos "build_bool_exp" pos in

   (* Boolean values *)
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
   let atom_true = genv_true_atom genv loc in
   let atom_false = genv_false_atom genv loc in
   let ty_bool = genv_ty_bool genv loc in

   (* Place the rest in a function *)
   let v = new_symbol_string "flag" in
   let venv = venv_add_var venv v ty_bool in
   let genv, body, ty_exp = cont genv tenv venv (AtomVar v) in
   let f = new_symbol_string "and_cont" in
   let ty_fun = make_type loc (TyFun ([ty_bool], ty_exp)) in
   let true_exp = make_exp loc (TailCall (AtomVar f, [atom_true])) in
   let false_exp = make_exp loc (TailCall (AtomVar f, [atom_false])) in

   (* Translate the code *)
   let genv, e2, _ =
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
            let e = make_exp loc (TailCall (AtomVar f, [a2])) in
               genv, e, ty_exp)
   in
   let genv, e1, _ =
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
            let true_exp, false_exp =
               if bool_flag then
                  let false_exp = make_exp loc (TailCall (AtomVar f, [atom_false])) in
                     e2, false_exp
               else
                  let true_exp = make_exp loc (TailCall (AtomVar f, [atom_true])) in
                     true_exp, false_exp
            in
            let e = make_exp loc (Match (a1, [true_set, true_exp; false_set, false_exp])) in
               genv, e, ty_exp)
   in

   (* Build the fun *)
   let e = make_exp loc (LetFuns ([f, loc, FunLocalClass, [], ty_fun, [v], body], e1)) in
      genv, e, ty_exp

(*
 * Function application.
 * Check for external calls, and inline the code if possible.
 *)
and build_apply_exp genv tenv venv pos loc e ty_fun args ty_res (cont : cont_exp) =
   let pos = string_pos "build_apply_exp" pos in
   let genv, ty_res = build_type genv tenv pos ty_res in
   let genv, ty_fun = build_type genv tenv pos ty_fun in
      (* Eval the arg then the fun, then apply the atoms *)
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let a = AtomTyConstrain (a, ty_fun) in
               match expand_type_core genv pos ty_fun with
                  TyExternal (ty_fun, s) ->
                     (* External call *)
                     (match s, args with
                         "%not", [e] ->
                            build_not_exp genv tenv venv pos loc e cont
                       | "%and", [e1; e2] ->
                            build_bool_exp true genv tenv venv pos loc e1 e2 cont
                       | "%or", [e1; e2] ->
                            build_bool_exp false genv tenv venv pos loc e1 e2 cont
                       | _ ->
                            build_exps genv tenv venv args (fun genv tenv venv args ->
                                  build_builtin_exp genv tenv venv pos loc ty_res s a ty_fun args cont))
                | _ ->
                     (* This is a normal function call *)
                     build_exps genv tenv venv args (fun genv tenv venv args ->
                           let f = new_symbol_string "fun" in
                           let v = new_symbol_string "apply" in
                           let venv = venv_add_var venv f ty_fun in
                           let venv = venv_add_var venv v ty_res in
                           let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
                           let e = make_exp loc (LetApply (v, ty_res, a, args, e)) in
                              genv, e, ty_exp))

(*
 * Translate a type application.
 *)
and build_apply_type_exp genv tenv venv pos loc e ty_args ty (cont : cont_exp) =
   let pos = string_pos "build_apply_type_exp" pos in
      (* Eval the fun, then apply the atoms *)
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let genv, ty = build_type genv tenv pos ty in
            let genv, ty_args = build_types genv tenv pos ty_args in
               cont genv tenv venv (AtomTyApply (a, ty, ty_args)))

(*
 * Translate a function.
 *)
and build_lambda genv tenv venv pos loc f param_vars pl body ty_mono =
   let pos = string_pos "build_lambda" pos in

   (* Build an inner unquantified lambda *)
   let vars = List.map (fun _ -> new_symbol_string "arg") pl in
   let ty_vars, ty_res = dest_fun_type genv (int_pos 3 pos) ty_mono in
   let venv = venv_add_vars venv (int_pos 4 pos) vars ty_vars in

   (* Chain together the pattern match *)
   let genv, fundef1, cont = build_lambda_match genv tenv venv (int_pos 5 pos) loc pl body (cont_return (int_pos 6 pos) loc) in
   let termds = List.map2 (fun v ty -> AtomVar v, neg_termd genv (int_pos 7 pos) ty) vars ty_vars in
   let genv, body, ty_exp = build_lambda_body genv tenv venv (int_pos 8 pos) loc termds pl cont in
   let body = make_exp loc (LetFuns ([fundef1], body)) in
   let fundef2 = f, loc, FunGlobalClass, param_vars, ty_mono, vars, body in
      genv, fundef2

and build_lambda_exp genv tenv venv pos loc ty_vars pl body ty (cont : cont_exp) =
   let pos = string_pos "build_lambda_exp" pos in

   (* We'll add it as a named function *)
   let f = new_symbol_string "lambda" in
   let genv, ty = build_type genv tenv (int_pos 1 pos) ty in
   let ty_fun = make_type loc (TyAll (ty_vars, ty)) in
   let venv = venv_add_var venv f ty_fun in
   let genv, e, ty_exp = cont genv tenv venv (AtomVar f) in

   (* Build the lambda function *)
   let genv, fundef2 = build_lambda genv tenv venv pos loc f ty_vars pl body ty in
   let e = make_exp loc (LetFuns ([fundef2], e)) in
      genv, e, ty_exp

(*
 * Translate a guarded function.
 * Basically, we treat this just a like a function
 * containing a match statement.
 *)
and build_function genv tenv venv pos loc f param_vars guarded ty =
   let pos = string_pos "build_function" pos in

   (* Translate the continuation *)
   let ty_vars, ty_res = dest_fun_type genv pos ty in
      match ty_vars with
         [ty_var] ->
            (* Translate the guarded command *)
            let genv, funs, cases = build_guarded genv tenv venv guarded (cont_return pos loc) in
            let v = new_symbol_string "arg" in
            let termd = neg_termd genv pos ty_var in
            let genv, body, ty_body = build_guarded_body genv tenv venv loc (AtomVar v, termd) ty_res cases in
            let body = make_exp loc (LetFuns (funs, body)) in
            let ty_fun = make_type loc (TyFun (ty_vars, ty_res)) in
            let fundef = f, loc, FunLocalClass, param_vars, ty_fun, [v], body in
               genv, fundef
       | _ ->
            raise (IRException (pos, StringIntError ("function has the wrong number of arguments", List.length ty_vars)))

and build_function_exp genv tenv venv pos loc param_vars guarded ty (cont : cont_exp) =
   let pos = string_pos "build_function_exp" pos in
   let genv, ty = build_type genv tenv pos ty in

   (* Translate the continuation *)
   let f = new_symbol_string "function" in
   let genv, fundef = build_function genv tenv venv pos loc f param_vars guarded ty in
   let ty_fun = make_type loc (TyAll (param_vars, ty)) in
   let venv = venv_add_var venv f ty in
   let genv, e, ty_exp = cont genv tenv venv (AtomVar f) in
   let e = make_exp loc (LetFuns ([fundef], e)) in
      genv, e, ty_exp

(*
 * Translate a let binding.
 * First evaluate all the expressions, then do the pattern matching.
 *)
and build_lets genv tenv venv pos loc lets (cont : cont_simp) =
   let pos = string_pos "build_lets" pos in
   let rec match_patts genv tenv venv vars lets =
      match lets with
         (p, a, ty) :: lets ->
            let cont genv tenv venv vars termd =
               match_patts genv tenv venv vars lets
            in
            let loc = Aml_tast_ds.loc_of_patt p in
            let genv, ty = build_type genv tenv pos ty in
            let termd = a, neg_termd genv pos ty in
               build_guarded_body genv tenv venv loc termd ty [p, cont]
       | [] ->
            wrap_vars genv tenv venv loc vars cont
   in
   let rec eval_args genv tenv venv cases lets =
      match lets with
         (p, e, ty) :: lets ->
            build_exp genv tenv venv e (fun genv tenv venv a ->
                  eval_args genv tenv venv ((p, a, ty) :: cases) lets)
    | [] ->
         match_patts genv tenv venv [] (List.rev cases)
   in
      eval_args genv tenv venv [] lets

and build_lets_exp genv tenv venv pos loc lets e (cont : cont_exp) =
   let pos = string_pos "build_lets_exp" pos in
      build_lets genv tenv venv pos loc lets (fun genv tenv venv ->
            build_exp genv tenv venv e cont)

(*
 * Translate some function definitions.
 * Note: we probably want to redo recursive definitions.
 *
 * Here's the idea.  Some of the definitions are functions.
 * Those are special.  The other _must_ be allocation expressions.
 * We have to separate the functions from the allocation values,
 * then form a recursive alloc definition...
 *)
and build_letrecs genv tenv venv pos loc fields (cont : cont_simp) =
   let pos = string_pos "build_letrecs" pos in

   (* Add the types to the variable environment *)
   let venv, fields =
      List.fold_left (fun (venv, fields) (v, e, ty) ->
            let genv, ty = build_type genv tenv pos ty in
            let venv = venv_add_var venv v ty in
            let fields = (v, e, ty) :: fields in
               venv, fields) (venv, []) fields
   in

   (* Sort them into functions and allocations *)
   let genv, funs, allocs =
      List.fold_left (fun (genv, funs, allocs) (v, e, ty) ->
            match Aml_tast_ds.dest_exp_core e with
               Aml_tast.Lambda (pl, e, _) ->
                  let genv, fundef = build_lambda genv tenv venv pos loc v pl e ty in
                  let funs = fundef :: funs in
                     genv, funs, allocs
             | Aml_tast.Function (guarded, _) ->
                  let genv, fundef = build_function genv tenv venv pos loc v guarded ty in
                  let funs = fundef :: funs in
                     genv, funs, allocs
             | Aml_tast.Tuple _
             | Aml_tast.Array _
             | Aml_tast.Const _ ->
                  (* We should build the allocops, but wait until we really implement letrec *)
                  let allocs = (v, e, ty) :: allocs in
                     genv, funs, allocs
             | _ ->
                  raise (IRException (pos, StringError "illegal right-hand-side of recursive definition"))) (**)
         (genv, [], []) fields
   in

   (*
    * BUG: This part must be fixed.  For now, we allow only functions.
    * Raise an error if there are allocations.
    *)
   let _ =
      if allocs <> [] then
         raise (IRException (pos, NotImplemented "recursive allocations"))
   in

   (* Now we have a list of recursive function definitions *)
   let genv, e, ty_exp = cont genv tenv venv in
   let e = make_exp loc (LetFuns (funs, e)) in
      genv, e, ty_exp

and build_letrecs_exp genv tenv venv pos loc letrecs e (cont : cont_exp) =
   build_letrecs genv tenv venv pos loc letrecs (fun genv tenv venv ->
         build_exp genv tenv venv e cont)

(*
 * If-then-else.
 *
 * The test gets compiled into a function of its own.
 * The remainder calls this function.  It looks like this.
 *
 * [[ if e1 then e2 else e3; cont ]] =
 *    let f v =
 *       cont v
 *    in
 *    let a1 = [[e1]] in
 *        match a1 with
 *           true -> let a2 = [[e2]] in f a2
 *         | false -> let a3 = [[e3]] in f a3
 *)
and build_if_exp genv tenv venv pos loc e1 e2 e3 ty (cont : cont_exp) =
   let pos = string_pos "build_if_exp" pos in
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
   let ty_bool = genv_ty_bool genv (Aml_tast_ds.loc_of_exp e1) in

   (* Put the continuation in a function *)
   let f = new_symbol_string "if_cont" in
   let v = new_symbol_string "if_arg" in
   let genv, ty_arg = build_type genv tenv pos ty in
   let venv = venv_add_var venv v ty_arg in
   let genv, e, ty_res = cont genv tenv venv (AtomVar v) in
   let ty_fun = make_type loc (TyFun ([ty_arg], ty_res)) in
   let fundef = f, loc, FunLocalClass, [], ty_fun, [v], e in

   (* Call the continuation *)
   let tail_exp genv tenv venv a =
      let e = make_exp loc (TailCall (AtomVar f, [a])) in
         genv, e, ty_res
   in
      (* Now build the match *)
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
            (* Compile the branches *)
            let genv, true_exp, _  = build_exp genv tenv venv e2 tail_exp in
            let genv, false_exp, _ = build_exp genv tenv venv e3 tail_exp in

            (* Perform the match *)
            let e = make_exp loc (Match (a1, [true_set, true_exp; false_set, false_exp])) in
            let e = make_exp loc (LetFuns ([fundef], e)) in
               genv, e, ty_res)

(*
 * The for-loop is just an integer loop.
 * We compile it into a recursive function.
 *
 * [[for v = e1 to_flag e2; do e3 done ]]; cont =
 *    let a1 = [[e1]] in
 *    let a2 = [[e2]] in
 *    let f v =
 *       match v with
 *          a2 -> cont ()
 *        | _ -> [[e3]]
 *    in
 *       f a1
 *)
and build_for_exp genv tenv venv pos loc v e1 to_flag e2 e3 (cont : cont_exp) =
   let pos = string_pos "build_for_exp" pos in
   let ty_unit = genv_ty_unit genv loc in
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
   let ty_int = make_type loc TyInt in

   (* Name of the function *)
   let f = new_symbol_string "for_fun" in

   (* Build the body of the loop *)
   let genv, true_exp, _ =
      build_exp genv tenv venv e3 (fun genv tenv venv a ->
            let e = make_exp loc (TailCall (AtomVar f, [AtomVar v])) in
            let e = make_exp loc (LetAtom (v, ty_int, AtomBinop (PlusIntOp, AtomVar v, AtomInt 1), e)) in
               genv, e, ty_unit)
   in

   (* Continue *)
   let genv, false_exp, ty_exp = cont genv tenv venv (genv_unit_atom genv loc) in
   let ty_fun = make_type loc (TyFun ([ty_int], ty_exp)) in
      (* Evaluate the start and finish *)
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
            (* Add the pattern match *)
            let body =
               make_exp loc (Match (AtomBinop (EqIntOp, AtomVar v, a2), (**)
                                        [true_set, true_exp; false_set, false_exp]))
            in

            (* The initial call to the function *)
            let e = make_exp loc (TailCall (AtomVar f, [a1])) in

            (* Build the function and call it *)
            let e = make_exp loc (LetFuns ([f, loc, FunLocalClass, [], ty_fun, [v], body], e)) in
               genv, e, ty_unit))

(*
 * The while loop is somewhat simpler than the for loop.
 *
 * [[while e1 do e2 done]] =
 *
 * let loop () =
 *     if e1 then
 *        (e2; loop ())
 *     else
 *        cont ()
 * in
 *    loop ()
 *)
and build_while_exp genv tenv venv pos loc e1 e2 cont =
   let pos = string_pos "build_while_exp" pos in
   let ty_unit = genv_ty_unit genv loc in
   let true_set = genv_true_set genv in
   let false_set = genv_false_set genv in
   let atom_unit = genv_unit_atom genv loc in

   (* Name of the function; it has no arguments *)
   let f = new_symbol_string "while" in
   let v = new_symbol_string "unit" in
   let call_exp = make_exp loc (TailCall (AtomVar f, [])) in

   (* Get the rest of the code *)
   let genv, false_exp, ty_exp = cont genv tenv venv atom_unit in

   (* While body *)
   let genv, true_exp, _ =
      build_exp genv tenv venv e2 (fun genv tenv venv _ ->
            genv, call_exp, ty_exp)
   in

   (* Loop body *)
   let genv, body, _ =
      build_exp genv tenv venv e1 (fun genv tenv venv a ->
            let e = Match (a, [true_set, true_exp; false_set, false_exp]) in
               genv, make_exp loc e, ty_exp)
   in

   (* Function *)
   let ty_fun = make_type loc (TyFun ([], ty_exp)) in
   let e = LetFuns ([f, loc, FunLocalClass, [], ty_fun, [], body], call_exp) in
      genv, make_exp loc e, ty_exp

(*
 * Tupling.
 *)
and build_tuple_exp genv tenv venv pos loc el ty cont =
   let pos = string_pos "build_tuple_exp" pos in
   let genv, ty = build_type genv tenv pos ty in
      build_exps genv tenv venv el (fun genv tenv venv args ->
            let v = new_symbol_string "tuple" in
            let venv = venv_add_var venv v ty in
            let ty_vars, ty = dest_all_type genv pos ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetAlloc (v, AllocTuple (ty_vars, ty, args), e) in
               genv, make_exp loc e, ty_exp)

(*
 * An initialized array.
 *)
and build_array_exp genv tenv venv pos loc el ty cont =
   let pos = string_pos "build_array_exp" pos in
   let genv, ty = build_type genv tenv pos ty in
      build_exps genv tenv venv el (fun genv tenv venv args ->
            let v = new_symbol_string "array" in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetAlloc (v, AllocArray (ty, args), e) in
               genv, make_exp loc e, ty_exp)

(*
 * Array subscript.
 *)
and build_array_subscript_exp genv tenv venv pos loc e1 e2 ty cont =
   let pos = string_pos "build_array_subscript_exp" pos in
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
            let v = new_symbol_string "elem" in
            let genv, ty = build_type genv tenv pos ty in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetSubscript (v, ty, a1, a2, e) in
               genv, make_exp loc e, ty_exp))

and build_array_set_subscript_exp genv tenv venv pos loc e1 e2 e3 cont =
   let pos = string_pos "build_array_set_subscript_exp" pos in
   let genv, ty = build_type genv tenv pos (Aml_tast_util.type_of_exp e3) in
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
      build_exp genv tenv venv e3 (fun genv tenv venv a3 ->
            let genv, e, ty_exp = cont genv tenv venv (genv_unit_atom genv loc) in
            let e = SetSubscript (a1, a2, a3, ty, e) in
               genv, make_exp loc e, ty_exp)))

(*
 * String subscript.
 *)
and build_string_subscript_exp genv tenv venv pos loc e1 e2 cont =
   let pos = string_pos "build_string_subscript_exp" pos in
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
            let v = new_symbol_string "c" in
            let ty_char = make_type loc TyChar in
            let venv = venv_add_var venv v ty_char in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetSubscript (v, ty_char, a1, a2, e) in
               genv, make_exp loc e, ty_exp))

and build_string_set_subscript_exp genv tenv venv pos loc e1 e2 e3 cont =
   let pos = string_pos "build_string_set_subscript_exp" pos in
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
      build_exp genv tenv venv e3 (fun genv tenv venv a3 ->
            let genv, e, ty_exp = cont genv tenv venv (genv_unit_atom genv loc) in
            let ty_char = make_type loc TyChar in
            let e = SetSubscript (a1, a2, a3, ty_char, e) in
               genv, make_exp loc e, ty_exp)))

(*
 * Record allocation.
 *)
and build_record_exp genv tenv venv pos loc fields ty cont =
   let pos = string_pos "build_record_exp" pos in
      build_record_exps genv tenv venv fields (fun genv tenv venv fields ->
            let genv, ty = build_type genv tenv pos ty in
            let ty_vars, ty_record = dest_all_type genv pos ty in
            let ty_var, ty_fields = dest_record_type genv pos ty_record in
            let v = new_symbol ty_var in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetAlloc (v, AllocRecord (ty_vars, ty_record, ty_var, fields), e) in
               genv, make_exp loc e, ty_exp)

(*
 * Record update.
 *)
and build_record_update_exp genv tenv venv pos loc e1 fields ty cont =
   let pos = string_pos "build_record_update_exp" pos in
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_record_exps genv tenv venv fields (fun genv tenv venv fields ->
            let genv, ty = build_type genv tenv pos ty in
            let ty_vars, ty_record = dest_all_type genv pos ty in

            (* Get the set of all the fields *)
            let ty_var, ty_fields = dest_record_type genv pos ty_record in
            let ty_fields = FieldTable.to_list ty_fields in

            (* Collect the set of names mentioned *)
            let names = List.fold_left (fun names (label, _) -> SymbolSet.add names label) SymbolSet.empty fields in

            (* Collect the fields not mentioned *)
            let rec collect genv tenv venv names fields ty_fields =
               match ty_fields with
                  (label, (_, ty)) :: ty_fields ->
                     if SymbolSet.mem names label then
                        (* Already have a value for this field *)
                        collect genv tenv venv names fields ty_fields
                     else
                        (* Project the field *)
                        let v = new_symbol label in
                        let venv = venv_add_var venv v ty in
                        let names = SymbolSet.add names label in
                        let fields = (label, AtomVar v) :: fields in
                        let genv, e, ty_exp = collect genv tenv venv names fields ty_fields in
                        let e = LetSubscript (v, ty, a1, AtomRecordLabel (ty_var, label), e) in
                           genv, make_exp loc e, ty_exp
                | _ ->
                     (* Now allocate the new record *)
                     let v = new_symbol ty_var in
                     let venv = venv_add_var venv v ty in
                     let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
                     let e = LetAlloc (v, AllocRecord (ty_vars, ty_record, ty_var, fields), e) in
                        genv, make_exp loc e, ty_exp
            in
               collect genv tenv venv names fields ty_fields))


(*
 * Record projection.
 *)
and build_record_project_exp genv tenv venv pos loc e ty1 label ty2 cont =
   let pos = string_pos "build_record_project_exp" pos in
   let genv, ty1 = build_type genv tenv pos ty1 in
   let genv, ty2 = build_type genv tenv pos ty2 in
   let ty_var, ty_fields = dest_record_type genv pos ty1 in
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let v = new_symbol label in
            let venv = venv_add_var venv v ty2 in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = LetSubscript (v, ty2, a, AtomRecordLabel (ty_var, label), e) in
               genv, make_exp loc e, ty_exp)

and build_record_set_project_exp genv tenv venv pos loc e1 ty1 label e2 cont =
   let pos = string_pos "build_record_set_project_exp" pos in
   let genv, ty1 = build_type genv tenv pos ty1 in
   let ty_var, ty_fields = dest_record_type genv pos ty1 in
   let genv, ty2 = build_type genv tenv pos (Aml_tast_util.type_of_exp e2) in
   let genv, e, ty_exp = cont genv tenv venv (genv_unit_atom genv loc) in
      build_exp genv tenv venv e1 (fun genv tenv venv a1 ->
      build_exp genv tenv venv e2 (fun genv tenv venv a2 ->
            let e = SetSubscript (a1, AtomRecordLabel (ty_var, label), a2, ty2, e) in
               genv, make_exp loc e, ty_exp))

(*
 * Sequential execution.
 *)
and build_sequence_exp genv tenv venv pos loc e1 e2 cont =
   let pos = string_pos "build_sequence_exp" pos in
      build_exp genv tenv venv e1 (fun genv tenv venv _ ->
      build_exp genv tenv venv e2 (fun genv tenv venv a ->
            cont genv tenv venv a))

(*
 * Pattern matching.
 *
 * [[match e1 with cases]] =
 *
 * let f v =
 *    cont v
 * in
 *    match [[e]] with [[cases]]
 *)
and build_match_exp genv tenv venv pos loc e guarded ty cont =
   let pos = string_pos "build_match_exp" pos in
   let genv, ty_arg = build_type genv tenv pos (Aml_tast_util.type_of_exp e) in
   let genv, ty_res = build_type genv tenv pos ty in
   let ty_fun = make_type loc (TyFun ([ty_arg], ty_res)) in

   (* Translate the rest an put it in a function *)
   let v = new_symbol_string "match_arg" in
   let venv_inner = venv_add_var venv v ty_res in
   let genv, inner, ty_exp = cont genv tenv venv_inner (AtomVar v) in
   let f = new_symbol_string "match_cont" in
   let ty_fun = make_type loc (TyFun ([ty_arg], ty_res)) in

      (* Translate the argument *)
      build_exp genv tenv venv e (fun genv tenv venv a ->
            (* Build the match code *)
            let genv, funs, cases =
               build_guarded genv tenv venv guarded (fun genv tenv venv a ->
                     let e = make_exp loc (TailCall (AtomVar f, [a])) in
                        genv, e, ty_exp)
            in
            let termd = neg_termd genv pos ty_arg in
            let genv, body, ty_body = build_guarded_body genv tenv venv loc (a, termd) ty_res cases in
            let body = make_exp loc (LetFuns (funs, body)) in
            let body = make_exp loc (LetFuns ([f, loc, FunLocalClass, [], ty_fun, [v], inner], body)) in
               genv, body, ty_exp)

(*
 * A try is a lot like a match.
 *
 * [[try e with guarded]] =
 *
 * let f () =
 *    try [[e]] with v -> [[guarded]]
 * in
 * let v = v () in
 *    cont v
 *)
and build_try_exp genv tenv venv pos loc e guarded ty cont =
   let pos = string_pos "build_try_exp" pos in
   let genv, ty_arg = build_type genv tenv pos (Aml_tast_util.type_of_exp e) in
   let genv, ty_res = build_type genv tenv pos ty in
   let ty_exn = genv_ty_exn genv loc in
   let atom_unit = genv_unit_atom genv loc in

   (* Translate the rest an put it in a function *)
   let v = new_symbol_string "arg" in
   let venv_inner = venv_add_var venv v ty_res in
   let genv, inner, ty_exp = cont genv tenv venv_inner (AtomVar v) in
   let f = new_symbol_string "try_cont" in
   let ty_f = make_type loc (TyFun ([ty_arg], ty_res)) in

   (* Build the match code and put it in a function *)
   let genv, funs, cases =
      build_guarded genv tenv venv guarded (fun genv tenv venv a ->
            let e = make_exp loc (TailCall (AtomVar f, [a])) in
               genv, e, ty_exp)
   in
   let termd = neg_termd genv pos ty_exn in
   let genv, body, ty_body = build_guarded_body genv tenv venv loc (AtomVar v, termd) ty_res cases in
   let body = make_exp loc (LetFuns (funs, body)) in
   let g = new_symbol_string "try_handle" in
   let ty_g = make_type loc (TyFun ([ty_exn], ty_res)) in

   (* Build the try function body *)
   let genv, e, ty_exp =
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let e = make_exp loc (TailCall (AtomVar f, [a])) in
               genv, e, ty_exp)
   in

   (* Now put it all together *)
   let handle_exp = make_exp loc (TailCall (AtomVar g, [AtomVar v])) in
   let e = make_exp loc (Try (e, v, handle_exp)) in
   let e = make_exp loc (LetFuns ([f, loc, FunLocalClass, [], ty_f, [v], inner;
                                   g, loc, FunLocalClass, [], ty_f, [v], body],
                                  e))
   in
      genv, e, ty_exp

(*
 * Raising is simple.
 *)
and build_raise_exp genv tenv venv pos loc e ty cont =
   let pos = string_pos "build_raise_exp" pos in
   let genv, ty = build_type genv tenv pos ty in
      build_exp genv tenv venv e (fun genv tenv venv a ->
            let e = make_exp loc (Raise (a, ty)) in
               genv, e, ty)

(*
 * Actions are a lot like variable references.
 *)
and build_msg_exp index genv tenv venv pos loc me_name ty cont =
   let pos = string_pos "build_msg" pos in
   let me_name =
      match me_name with
         Aml_tast.ModNameEnv me_var ->
            Aml_tast.NameEnv me_var
       | Aml_tast.ModNameProj (mod_name, ext_var) ->
            Aml_tast.NamePath (mod_name, ext_var)
       | Aml_tast.ModNameApply _ ->
            raise (IRException (pos, StringTAstModNameError ("illegal name", me_name)))
   in
      build_var_exp genv tenv venv pos loc me_name (fun genv tenv venv a ->
            (* BUG: supposed to project the name here *)
            cont genv tenv venv a)

(*
 * An action is a tuple containing the sub-actions.
 *)
and build_action_exp genv tenv venv pos loc el ty cont =
   let pos = string_pos "build_action_exp" pos in
      raise (IRException (pos, NotImplemented "actions"))

(*
 * A restriction creates a new name.
 *)
and build_restrict_exp genv tenv venv pos loc v mt_var e cont =
   let pos = string_pos "build_restrict_exp" pos in
      raise (IRException (pos, NotImplemented "restriction"))

(*
 * Ambient creation.
 *)
and build_ambient_exp genv tenv venv pos loc v me cont =
   let pos = string_pos "build_ambient_exp" pos in
      raise (IRException (pos, NotImplemented "ambients"))

and build_let_module_exp genv tenv venv pos loc v me e ty cont =
   let pos = string_pos "build_let_module_exp" pos in
      build_module_exp genv tenv venv me (fun genv tenv venv a ->
            let genv, ty = build_type genv tenv pos ty in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = build_exp genv tenv venv e cont in
            let e = make_exp loc (LetAtom (v, ty, a, e)) in
               genv, e, ty_exp)

(*
 * Create a thread.
 *)
and build_thread_exp genv tenv venv pos loc e cont =
   let pos = string_pos "build_thread_exp" pos in
      raise (IRException (pos, NotImplemented "threads"))

(*
 * Migrate the current folder.
 *)
and build_migrate_exp genv tenv venv pos loc e1 e2 ty cont =
   let pos = string_pos "build_migrate_exp" pos in
      raise (IRException (pos, NotImplemented "migration"))

(************************************************************************
 * MODULE EXPRESSIONS                                                   *
 ************************************************************************)

(*
 * Translate the items that make up a module.
 *)
and build_module_exp genv tenv venv me (cont : cont_exp) =
   let pos = string_pos "build_module_exp" (tast_me_pos me) in
   let loc = Aml_tast_ds.loc_of_module_exp me in
   let me = Aml_tast_ds.dest_module_exp_core me in
      match me with
         Aml_tast.ModuleExpStruct (fields, names, ty) ->
            build_module_fields genv tenv venv pos loc fields names ty cont
       | Aml_tast.ModuleExpVar (me_name, ty) ->
            build_module_var genv tenv venv pos loc me_name ty cont
       | Aml_tast.ModuleExpConstrain (me, ty) ->
            build_module_constrain genv tenv venv pos loc me ty cont
       | Aml_tast.ModuleExpFunctor (me_var, mt1, me, mt2) ->
            build_module_functor genv tenv venv pos loc me_var mt1 me mt2 cont
       | Aml_tast.ModuleExpApply (me, mn, mt) ->
            build_module_apply genv tenv venv pos loc me mn mt cont

(*
 * Translate a module reference.
 *)
and build_module_var genv tenv venv pos loc me_name ty cont =
   let pos = string_pos "build_module_var" pos in
      build_me_name_exp genv tenv venv pos loc me_name (fun genv tenv venv a ->
            coerce_module_exp genv tenv venv pos loc a ty cont)

(*
 * Coerce a module expression.
 *)
and build_module_constrain genv tenv venv pos loc me ty cont =
   let pos = string_pos "build_module_constrain" pos in
      build_module_exp genv tenv venv me (fun genv tenv venv a ->
            coerce_module_exp genv tenv venv pos loc a ty cont)

(*
 * Build a functor.
 * This become a function that takes the module as an argument.
 *)
and build_module_functor genv tenv venv pos loc me_var ty_arg me ty_res cont =
   let pos = string_pos "build_module_functor" pos in
   let genv, ty_arg = build_type genv tenv pos ty_arg in
   let genv, ty_res = build_type genv tenv pos ty_res in

   (* Build the function body *)
   let venv_body = venv_add_var venv me_var ty_arg in
   let genv, body, _ = build_module_exp genv tenv venv me (cont_return pos loc) in

   (* Build the function *)
   let f = new_symbol_string "functor" in
   let ty_fun = make_type loc (TyFun ([ty_arg], ty_res)) in
   let venv = venv_add_var venv f ty_fun in
   let genv, e, ty_exp = cont genv tenv venv (AtomVar f) in
   let e = make_exp loc (LetFuns ([f, loc, FunLocalClass, [], ty_fun, [me_var], body], e)) in
      genv, e, ty_exp

(*
 * Apply a functor.
 *)
and build_module_apply genv tenv venv pos loc me me_name ty cont =
   let pos = string_pos "build_module_apply" pos in
   let genv, ty = build_type genv tenv pos ty in
      build_module_exp genv tenv venv me (fun genv tenv venv a1 ->
      build_me_name_exp genv tenv venv pos loc me_name (fun genv tenv venv a2 ->
            let v = new_symbol_string "mod_apply" in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
            let e = make_exp loc (LetApply (v, ty, a1, [a2], e)) in
               genv, e, ty_exp))

(************************************************************************
 * MODULE FIELDS
 ************************************************************************)

(*
 * Translate the structure.  This produces a record of all the
 * fields listed in the module_type.
 *)
and build_module_fields genv tenv venv pos loc fields names ty (cont : cont_exp) =
   let names = build_module_names names in
   let tenv = tenv_add_names tenv names in
   let rec collect genv fenv tenv venv fields =
      match fields with
         field :: fields ->
            build_field genv fenv tenv venv field (fun genv fenv tenv venv ->
                  collect genv fenv tenv venv fields)
       | [] ->
            build_module_record genv fenv tenv venv pos loc names ty cont
   in
      collect genv fenv_empty tenv venv fields

(*
 * Treat the module fields as if they were expressions.
 *)
and build_field genv fenv tenv venv field (cont : cont_field) =
   let pos = string_pos "build_module_field" (tast_field_pos field) in
   let loc = Aml_tast_ds.loc_of_field field in
      match Aml_tast_ds.dest_field_core field with
         Aml_tast.FieldLet lets ->
            build_lets genv tenv venv pos loc lets (fun genv tenv venv -> cont genv fenv tenv venv)
       | Aml_tast.FieldLetRec letrecs ->
            build_letrecs genv tenv venv pos loc letrecs (fun genv tenv venv -> cont genv fenv tenv venv)
       | Aml_tast.FieldType types ->
            build_type_field genv fenv tenv venv pos loc types cont
       | Aml_tast.FieldExpr e ->
            build_exp_field genv fenv tenv venv pos loc e cont
       | Aml_tast.FieldRestrict (me_var, mt) ->
            build_restrict_field genv fenv tenv venv pos loc me_var mt cont
       | Aml_tast.FieldModule (me_var, me, mt) ->
            build_module_field genv fenv tenv venv pos loc me_var me mt cont
       | Aml_tast.FieldModuleType (mt_var, mt) ->
            build_module_type_field genv fenv tenv venv pos loc mt_var mt cont
       | Aml_tast.FieldException (tag_var, ty_var, tyl) ->
            build_exception_field genv fenv tenv venv pos loc tag_var ty_var tyl cont
       | Aml_tast.FieldExternal (v, ty, s) ->
            build_external_field genv fenv tenv venv pos loc v ty s cont

(*
 * Compile all the types in the module.
 *)
and build_type_field genv fenv tenv venv pos loc types cont =
   let pos = string_pos "build_type_field" pos in

   (* First, add all the types as delayed *)
   let tenv =
      List.fold_left (fun tenv (ty_var, ty_vars, ty_opt) ->
            tenv_add_so_var tenv ty_var ty_vars ty_opt) tenv types
   in
   let genv, fenv =
      List.fold_left (fun (genv, fenv) (ty_var, ty_vars, ty_opt) ->
            let genv, ty_opt = build_type_opt genv tenv pos ty_opt in
            let fenv = fenv_add fenv ty_var (ty_vars, ty_opt) in
               genv, fenv) (genv, fenv) types
   in
      cont genv fenv tenv venv

(*
 * Evaluate the expression for its effect.
 *)
and build_exp_field genv fenv tenv venv pos loc e cont =
   let pos = string_pos "build_exp_field" pos in
   let v = new_symbol_string "build_module_expr" in
      build_exp genv tenv venv e (fun genv tenv venv _ -> cont genv fenv tenv venv)

(*
 * Name restriction.
 *)
and build_restrict_field genv fenv tenv venv pos loc me_var ty cont =
   let pos = string_pos "build_restrict_field" pos in
      raise (IRException (pos, NotImplemented "name restriction"))

(*
 * A nested module.
 *)
and build_module_field genv fenv tenv venv pos loc me_var me ty cont =
   let pos = string_pos "build_module_field" pos in
   let genv, ty = build_type genv tenv pos ty in
      build_module_exp genv tenv venv me (fun genv tenv venv a ->
            let venv = venv_add_var venv me_var ty in
            let genv, e, ty_exp = cont genv fenv tenv venv in
            let e = make_exp loc (LetAtom (me_var, ty, a, e)) in
               genv, e, ty_exp)

(*
 * Module type.
 *)
and build_module_type_field genv fenv tenv venv pos loc mt_var ty cont =
   let pos = string_pos "build_module_type_field" pos in
   let tenv = tenv_add_so_var tenv mt_var [] (Some ty) in
   let genv, ty = build_type genv tenv pos ty in
   let fenv = fenv_add fenv mt_var ([], Some ty) in
      cont genv fenv tenv venv

(*
 * An exception.
 *)
and build_exception_field genv fenv tenv venv pos loc tag_var ty_var tyl cont =
   let pos = string_pos "build_exception_field" pos in
   let genv, tyl = build_types genv tenv pos tyl in
   let genv = genv_add_tag genv loc tag_var ty_var tyl in
   let ty = make_type loc (TyTag (ty_var, tyl)) in
   let venv = venv_add_var venv tag_var ty in
      cont genv fenv tenv venv

(*
 * External function.
 * We build a function to be called in case the value
 * can't be inlined.
 *)
and build_external_field genv fenv tenv venv pos loc v ty s cont =
   let pos = string_pos "build_external_field" pos in

   (* Build the rest of the program *)
   let genv, ty = build_type genv tenv pos ty in
   let venv = venv_add_var venv v ty in
   let genv, e, ty_exp = cont genv fenv tenv venv in
      if is_fun_type genv pos ty then
         (* If this is a function type, so build a function *)
         let ty_vars, ty_args, ty_res = dest_all_fun_type genv pos ty in
         let ty_mono = make_type loc (TyFun (ty_args, ty_res)) in
         let len = List.length ty_args in
         let vars = List.map (fun _ -> new_symbol_string "v") ty_args in
         let venv = List.fold_left2 venv_add_var venv vars ty_args in
         let args = List.map (fun v -> AtomVar v) vars in
         let genv, body, _ =
            build_builtin_ext genv tenv venv pos loc ty_res s ty args (fun genv tenv venv a ->
                  genv, make_exp loc (Return a), ty_res)
         in
         let e = make_exp loc (LetFuns ([v, loc, FunGlobalClass, ty_vars, ty_mono, vars, body], e)) in
            genv, e, ty_exp
      else
         (* This is not a function type, so we should know about it *)
         let a = build_builtin_atom genv pos s in
         let e = make_exp loc (LetAtom (v, ty, a, e)) in
            genv, e, ty_exp

(*
 * Build the record that defines the module.
 *)
and build_module_record genv fenv tenv venv pos loc names ty (cont : cont_exp) =
   let pos = string_pos "build_module_record" pos in
   let genv, ty = build_type genv tenv pos ty in

   (* Initial with all the vars *)
   let atoms =
      FieldTable.fold (fun atoms v v' ->
            SymbolTable.add atoms v (AtomVar v')) SymbolTable.empty names.names_var
   in

   (* Build the rest of the code *)
   let v = new_symbol_string "module" in
   let venv = venv_add_var venv v ty in
   let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in

   (* Allocate the module *)
   let ty_var, _, _ = dest_module_type genv pos ty in
   let e = make_exp loc (LetAlloc (v, AllocModule (ty, ty_var, atoms), e)) in
      genv, e, ty_exp

(*
 * Coerce a module.
 * This is an active coercion:
 * the new module may have the fields in a different order.
 *
 * Project all the elements from the first module,
 * and allocate a new record.
 *)
and coerce_module_exp genv tenv venv pos loc a ty2 (cont : cont_exp) =
   let pos = string_pos "coerce_module_exp" pos in

   (* Get the argument type *)
   let ty1 = type_of_atom genv venv pos loc a in
   let ty_var, names1, vars1 = dest_module_type genv pos ty1 in

   (* Get the result type *)
   let genv, ty2 = build_type genv tenv pos ty2 in
   let ty_var2, names2, vars2 = dest_module_type genv pos ty2 in

   (* Add a new variable and get the continuation *)
   let v = new_symbol_string "coerce_module" in
   let venv = venv_add_var venv v ty2 in
   let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in

   (* Generate new names for all the variables to be used *)
   let labels =
      FieldTable.fold (fun labels ext_var label2 ->
            let label1 = new_symbol ext_var in
               SymbolTable.add labels label2 (ext_var, label1)) SymbolTable.empty names2
   in

   (* Allocate the module *)
   let fields = SymbolTable.map (fun (_, label1) -> AtomVar label1) labels in
   let e = make_exp loc (LetAlloc (v, AllocModule (ty2, ty_var2, fields), e)) in

   (* Now project all the elements *)
   let e =
      SymbolTable.fold (fun e label2 (ext_var, v) ->
            let label1 =
               try FieldTable.find names1 ext_var with
                  Not_found ->
                     raise (IRException (pos, UnboundLabel ext_var))
            in
            let ty =
               try SymbolTable.find vars1 label1 with
                  Not_found ->
                     raise (IRException (pos, UnboundLabel label1))
            in
               make_exp loc (LetSubscript (v, ty, a, AtomModuleLabel (ty_var, label1), e))) e labels
   in
      genv, e, ty_exp

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
