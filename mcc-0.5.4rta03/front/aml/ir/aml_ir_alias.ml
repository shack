(*
 * We perform two basic operations in this file.
 * It is a kind of simplified alias analysis.
 *
 * First: function arguments are really pairs.
 * We expand the argument pairs.  Whenever a function is
 * stored, we allocate a closure pair.  When we load it
 * from memory, we project the parts.
 *
 * Second, we can do the same for the other aggregates
 * like tuples, records, frames, and modules.  They can
 * be passed as multiple arguments.  However, we don't do
 * it now because abstract types make this hard.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_type

module Pos = MakePos (struct let name = "Aml_ir_alias" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Curry a type.  In this "escaping" version,
 * all types remain single types.  Function types
 * turn into existentials.
 *)
let rec curry_type_esc ty =
   let pos = string_pos "curry_type_esc" (type_exp_pos ty) in
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyVoid
       | TyInt
       | TyChar
       | TyString
       | TyFloat
       | TyVar _
       | TyProject _ ->
            ty

       | TyFun (ty_args, ty_res) ->
            curry_fun_type pos loc ty_args ty_res

       | TyTuple tyl ->
            make_type loc (TyTuple (curry_type_esc_list tyl))
       | TyArray ty ->
            make_type loc (TyArray (curry_type_esc ty))

       | TyAll (vars, ty) ->
            make_type loc (TyAll (vars, curry_type_esc ty))
       | TyExists (vars, ty) ->
            make_type loc (TyExists (vars, curry_type_esc ty))

       | TyApply (ty_var, tyl) ->
            make_type loc (TyApply (ty_var, curry_type_esc_list tyl))
       | TyUnion (ty_var, tyl, s) ->
            make_type loc (TyUnion (ty_var, curry_type_esc_list tyl, s))
       | TyRecord (ty_var, tyl) ->
            make_type loc (TyRecord (ty_var, curry_type_esc_list tyl))
       | TyFrame (ty_var, tyl) ->
            make_type loc (TyFrame (ty_var, curry_type_esc_list tyl))
       | TyModule (ty_var, tyl) ->
            make_type loc (TyModule (ty_var, curry_type_esc_list tyl))
       | TyTag (ty_var, tyl) ->
            make_type loc (TyTag (ty_var, curry_type_esc_list tyl))
       | TyDTuple (ty_var, tyl) ->
            make_type loc (TyDTuple (ty_var, curry_type_esc_list_opt tyl))

       | TyExternal (ty, s) ->
            curry_type_esc ty
       | TyFormat (ty1, ty2, ty3) ->
            make_type loc (TyFormat (curry_type_esc ty1, curry_type_esc ty2, curry_type_esc ty3))

(*
 * In the argument version, a function argument may expand to multiple
 * function arguments.
 *)
and curry_type_arg new_ty_vars new_ty_args ty =
   let pos = string_pos "curry_type_arg" (type_exp_pos ty) in
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyVoid
       | TyInt
       | TyChar
       | TyString
       | TyFloat
       | TyVar _
       | TyProject _ ->
            new_ty_vars, ty :: new_ty_args

       | TyFun (ty_args, ty_res) ->
            let ty_fun = curry_fun_type pos loc ty_args ty_res in
               new_ty_vars, ty_fun :: new_ty_args

       | TyTuple tyl ->
            let ty = make_type loc (TyTuple (curry_type_esc_list tyl)) in
               new_ty_vars, ty :: new_ty_args
       | TyArray ty ->
            let ty = make_type loc (TyArray (curry_type_esc ty)) in
               new_ty_vars, ty :: new_ty_args

       | TyAll (vars, ty) ->
            let ty = make_type loc (TyAll (vars, curry_type_esc ty)) in
               new_ty_vars, ty :: new_ty_args
       | TyExists (vars, ty) ->
            let new_ty_vars = List.rev_append vars new_ty_vars in
               curry_type_arg new_ty_vars new_ty_args ty

       | TyApply (ty_var, tyl) ->
            let ty = make_type loc (TyApply (ty_var, curry_type_esc_list tyl)) in
               new_ty_vars, ty :: new_ty_args
       | TyUnion (ty_var, tyl, s) ->
            let ty = make_type loc (TyUnion (ty_var, curry_type_esc_list tyl, s)) in
               new_ty_vars, ty :: new_ty_args
       | TyRecord (ty_var, tyl) ->
            let ty = make_type loc (TyRecord (ty_var, curry_type_esc_list tyl)) in
               new_ty_vars, ty :: new_ty_args
       | TyFrame (ty_var, tyl) ->
            let ty = make_type loc (TyFrame (ty_var, curry_type_esc_list tyl)) in
               new_ty_vars, ty :: new_ty_args
       | TyModule (ty_var, tyl) ->
            let ty = make_type loc (TyModule (ty_var, curry_type_esc_list tyl)) in
               new_ty_vars, ty :: new_ty_args
       | TyTag (ty_var, tyl) ->
            let ty = make_type loc (TyTag (ty_var, curry_type_esc_list tyl)) in
               new_ty_vars, ty :: new_ty_args
       | TyDTuple (ty_var, tyl) ->
            let ty = make_type loc (TyDTuple (ty_var, curry_type_esc_list_opt tyl)) in
               new_ty_vars, ty :: new_ty_args

       | TyExternal (ty, s) ->
            curry_type_arg new_ty_vars new_ty_args ty
       | TyFormat (ty1, ty2, ty3) ->
            let ty = make_type loc (TyFormat (curry_type_esc ty1, curry_type_esc ty2, curry_type_esc ty3)) in
               new_ty_vars, ty :: new_ty_args

(*
 * This is where most of the work is done.
 * The arguments in a function type are expanded.
 *)
and curry_fun_type pos loc ty_args ty_res =
   let pos = string_pos "curry_fun_type" pos in
   let ty_res = curry_type_esc ty_res in
   let ty_vars, ty_args =
      List.fold_left (fun (ty_vars, ty_args) ty ->
            curry_type_arg ty_vars ty_args ty) ([], []) ty_args
   in
   let ty_vars = List.rev ty_vars in
   let ty_args = List.rev ty_args in
   let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
      make_type loc (TyAll (ty_vars, ty_fun))

(*
 * List versions.
 *)
and curry_type_esc_list tyl =
   List.map curry_type_esc tyl

and curry_type_esc_list_opt tyl_opt =
   match tyl_opt with
      Some tyl -> Some (curry_type_esc_list tyl)
    | None -> None

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Curry a typedef.
 *)
let curry_record_tydef fields =
   FieldTable.map (fun (mflag, ty) ->
         mflag, curry_type_esc ty) fields

let curry_union_tydef fields =
   FieldTable.map curry_type_esc_list fields

let curry_module_tydef names fields =
   let fields = SymbolTable.map curry_type_esc fields in
      names, fields

let curry_tydef tydef =
   let { tydef_inner = inner } = tydef in
   let loc = loc_of_tydef_inner inner in
   let inner =
      match dest_tydef_inner_core inner with
         TyDefLambda ty ->
            TyDefLambda (curry_type_esc ty)
       | TyDefUnion fields ->
            TyDefUnion (curry_union_tydef fields)
       | TyDefRecord fields ->
            TyDefRecord (curry_record_tydef fields)
       | TyDefFrame fields ->
            TyDefFrame (curry_record_tydef fields)
       | TyDefModule (names, fields) ->
            let names, fields = curry_module_tydef names fields in
               TyDefModule (names, fields)
       | TyDefDTuple v ->
            TyDefDTuple v
   in
      { tydef with tydef_inner = make_tydef_inner loc inner }

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Build an escaping atom.
 * This just converts all the types, and leaves
 * the rest of the atom unchanged.
 *)
let rec build_atom_esc a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomVar _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomTyUnpack _ ->
         a
    | AtomNil ty ->
         AtomNil (curry_type_esc ty)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (build_atom_esc a, curry_type_esc ty, curry_type_esc_list tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, curry_type_esc ty, curry_type_esc_list tyl)
    | AtomConst (ty, ty_var, const_var) ->
         AtomConst (curry_type_esc ty, ty_var, const_var)
    | AtomUnop (op, a) ->
         AtomUnop (op, build_atom_esc a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, build_atom_esc a1, build_atom_esc a2)

(*
 * Build an argument atom.
 * In this case, when we know that the atom is an aggregate,
 * we expand it to multiple arguments.
 *)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
