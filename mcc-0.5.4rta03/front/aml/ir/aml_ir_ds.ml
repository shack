(*
 * Delayed substitution.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Attribute
open Field_table

open Aml_ir
open Aml_ir_exn

(************************************************************************
 * FREE VARS                                                            *
 ************************************************************************)

(*
 * Empty free vars.
 *)
let free_vars_empty =
   { fv_vars    = SymbolSet.empty;
     fv_fo_vars = SymbolSet.empty
   }

(*
 * Union.
 *)
let free_vars_union fv1 fv2 =
   let { fv_vars      = vars1;
         fv_fo_vars   = params1
       } = fv1
   in
   let { fv_vars    = vars2;
         fv_fo_vars = params2
       } = fv2
   in
      { fv_vars    = SymbolSet.union vars1 vars2;
        fv_fo_vars = SymbolSet.union params1 params2
      }

(*
 * Equality on free vars.
 *)
let free_vars_equal fv1 fv2 =
   let { fv_vars      = vars1;
         fv_fo_vars   = params1
       } = fv1
   in
   let { fv_vars      = vars2;
         fv_fo_vars   = params2
       } = fv2
   in
      SymbolSet.equal vars1 vars2
      && SymbolSet.equal params1 params2

(*
 * Adding variables.
 *)
let free_vars_add_param fv v =
   { fv with fv_fo_vars = SymbolSet.add fv.fv_fo_vars v }

let free_vars_add_params fv vars =
   let rec add subst = function
      v :: vars ->
         add (SymbolSet.add subst v) vars
    | [] ->
         subst
   in
      { fv with fv_fo_vars = add fv.fv_fo_vars vars }

let free_vars_add_var fv v =
   { fv with fv_vars = SymbolSet.add fv.fv_vars v }

(*
 * Removing variables.
 *)
let free_vars_subtract_param fv v =
   { fv with fv_fo_vars = SymbolSet.remove fv.fv_fo_vars v }

let free_vars_subtract_params fv vars =
   { fv with fv_fo_vars = SymbolSet.subtract_list fv.fv_fo_vars vars }

let free_vars_subtract_var fv v =
   { fv with fv_vars = SymbolSet.remove fv.fv_vars v }

let free_vars_subtract_vars fv vars =
   { fv with fv_vars = SymbolSet.subtract_list fv.fv_vars vars }

(*
 * Subtract the domain of the subst,
 * and add the free vars of the range.
 *)
let rec free_vars_subst subst fv =
   let { sub_fo_vars   = sub_fo_vars } = subst in

   (* Add the substituted values until fixpoint *)
   let step fv =
      let fv =
         SymbolTable.fold (fun fv v ty ->
               if SymbolSet.mem fv.fv_fo_vars v then
                  free_vars_type fv ty
               else
                  fv) fv sub_fo_vars
      in
         fv
   in
   let rec fixpoint fv1 =
      let fv2 = step fv1 in
         if free_vars_equal fv2 fv1 then
            fv1
         else
            fixpoint fv2
   in
   let fv = fixpoint fv in

   (* Now subtract the domain of the substitution *)
   let { fv_vars      = vars;
         fv_fo_vars   = params
       } = fv
   in

   (* Subtract the domain *)
   let params = SymbolTable.fold (fun params v _ -> SymbolSet.remove params v) params sub_fo_vars in
      { fv_vars      = vars;
        fv_fo_vars   = params
      }

(************************************************
 * TYPES
 *)
and free_vars_type fv1 ds =
   let fv2 = dest_up free_vars_type_core free_vars_subst ds in
      free_vars_union fv1 fv2

and free_vars_type_core core =
   free_vars_type_aux free_vars_empty core

and free_vars_type_aux fv ty =
   match ty with
      TyVoid
    | TyInt
    | TyChar
    | TyString
    | TyFloat ->
         fv

    | TyFun (tyl, ty) ->
         free_vars_types (free_vars_type fv ty) tyl
    | TyTuple tyl ->
         free_vars_types fv tyl
    | TyArray ty ->
         free_vars_type fv ty

    | TyAll (vars, ty)
    | TyExists (vars, ty) ->
         free_vars_subtract_params (free_vars_type fv ty) vars
    | TyFormat (ty1, ty2, ty3) ->
         free_vars_type (free_vars_type (free_vars_type fv ty3) ty2) ty1
    | TyExternal (ty, _) ->
         free_vars_type fv ty

    | TyVar v ->
         free_vars_add_param fv v
    | TyProject (v, _) ->
         free_vars_add_var fv v

    | TyFrame  (v, tyl)
    | TyApply  (v, tyl)
    | TyRecord (v, tyl)
    | TyUnion  (v, tyl, _)
    | TyModule (v, tyl)
    | TyTag    (v, tyl) ->
         free_vars_types fv tyl
    | TyDTuple (v, tyl_opt) ->
         free_vars_types_opt fv tyl_opt

(*
    | TyOpen fields ->
         free_vars_ty_open fv fields
*)

and free_vars_types fv tyl =
   List.fold_left free_vars_type fv tyl

and free_vars_types_opt fv tyl_opt =
   match tyl_opt with
      Some tyl -> free_vars_types fv tyl
    | None -> fv

and free_vars_ty_open fv fields =
   List.fold_left (fun fv (v, ty) ->
         let fv = free_vars_add_var fv v in
         let fv = free_vars_type fv ty in
            fv) fv fields

(************************************************
 * Type definitions.
 *)

(*
 * Free variables in a record definition.
 *)
let free_vars_ty_record fv fields =
   FieldTable.fold (fun fv _ (_, ty) ->
         free_vars_type fv ty) fv fields

(*
 * Free variables in a union.
 *)
let free_vars_ty_union fv fields =
   FieldTable.fold (fun fv _ tyl ->
         free_vars_types fv tyl) fv fields

(*
 * Free vars of a type definition.
 *)
let free_vars_tydef_inner_aux fv tydef =
   match tydef with
      TyDefFrame fields
    | TyDefRecord fields ->
         free_vars_ty_record fv fields
    | TyDefUnion fields ->
         free_vars_ty_union fv fields
    | TyDefDTuple ty_var ->
         fv
    | TyDefLambda ty ->
         free_vars_type fv ty
    | TyDefModule (_, fields) ->
         SymbolTable.fold (fun fv _ ty -> free_vars_type fv ty) fv fields

let free_vars_tydef_inner_core tydef =
   free_vars_tydef_inner_aux free_vars_empty tydef

let free_vars_tydef_inner fv1 ds =
   let fv2 = dest_up free_vars_tydef_inner_core free_vars_subst ds in
      free_vars_union fv1 fv2

let free_vars_tydef fv tydef =
   let { tydef_fo_vars = params;
         tydef_inner = tydef
       } = tydef
   in
   let fv = free_vars_tydef_inner fv tydef in
   let fv = free_vars_subtract_params fv params in
      fv

(************************************************
 * EXPRESSIONS
 *)

(*
 * Atoms.
 *)
let rec free_vars_atom fv atom =
   match atom with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _ ->
         fv
    | AtomVar v
    | AtomTyUnpack v ->
         free_vars_add_var fv v
    | AtomNil ty ->
         free_vars_type fv ty
    | AtomTyConstrain (a, ty) ->
         free_vars_atom (free_vars_type fv ty) a
    | AtomTyApply (a, ty, tyl) ->
         free_vars_atom (free_vars_type (free_vars_types fv tyl) ty) a
    | AtomTyPack (v, ty, tyl) ->
         free_vars_add_var (free_vars_type (free_vars_types fv tyl) ty) v
    | AtomConst (ty, ty_var, const_var) ->
         free_vars_type fv ty
    | AtomUnop (_, a) ->
         free_vars_atom fv a
    | AtomBinop (_, a1, a2) ->
         free_vars_atom (free_vars_atom fv a1) a2

let free_vars_atom_opt fv a_opt =
   match a_opt with
      Some a -> free_vars_atom fv a
    | None -> fv

let free_vars_atoms fv atoms =
   List.fold_left free_vars_atom fv atoms

(*
 * Expression free vars.
 *)
let rec free_vars_exp fv1 ds =
   let fv2 = dest_up free_vars_exp_core free_vars_subst ds in
      free_vars_union fv1 fv2

and free_vars_exp_core e =
   free_vars_exp_aux free_vars_empty e

and free_vars_exp_aux fv e =
   match e with
      LetFuns (funs, e) ->
         free_vars_funs (free_vars_exp fv e) funs
    | LetRec (letrecs, e) ->
         free_vars_letrecs (free_vars_exp fv e) letrecs
    | LetExt (v, ty1, _, ty2, ty_args, args, e) ->
         free_vars_types (free_vars_atoms (free_vars_type (free_vars_type (free_vars_subtract_var (free_vars_exp fv e) v) ty1) ty2) args) ty_args
    | LetAtom (v, ty, a, e) ->
         free_vars_type (free_vars_atom (free_vars_subtract_var (free_vars_exp fv e) v) a) ty
    | LetApply (v, ty, f, args, e) ->
         free_vars_type (free_vars_atom (free_vars_atoms (free_vars_subtract_var (free_vars_exp fv e) v) args) f) ty
    | LetClosure (v, f, ty_args, args, e) ->
         free_vars_atom (free_vars_types (free_vars_atoms (free_vars_subtract_var (free_vars_exp fv e) v) args) ty_args) f
    | TailCall (f, args) ->
         free_vars_atom (free_vars_atoms fv args) f
    | Return a ->
         free_vars_atom fv a

    | Match (a, cases) ->
         List.fold_left (fun fv (_, e) ->
               free_vars_exp fv e) (free_vars_atom fv a) cases

    | MatchDTuple (a, cases) ->
         List.fold_left (fun fv (a_opt, e) ->
               free_vars_atom_opt (free_vars_exp fv e) a_opt) (free_vars_atom fv a) cases

    | Try (e1, v, e2) ->
         free_vars_exp (free_vars_subtract_var (free_vars_exp fv e2) v) e1
    | Raise (v, ty) ->
         free_vars_atom (free_vars_type fv ty) v

    | LetAlloc (v, op, e) ->
         free_vars_alloc_op (free_vars_subtract_var (free_vars_exp fv e) v) op

    | SetSubscript (v, a1, a2, ty, e) ->
         free_vars_atom (free_vars_atom (free_vars_atom (free_vars_type (free_vars_exp fv e) ty) a2) a1) v
    | LetSubscript (v1, ty, a2, a3, e) ->
         free_vars_type (free_vars_atom (free_vars_atom (free_vars_subtract_var (free_vars_exp fv e) v1) a2) a3) ty

    | SetGlobal (v, ty, a, e) ->
         free_vars_add_var (free_vars_type (free_vars_atom (free_vars_exp fv e) a) ty) v

(*
    | LetName (v, ty, e) ->
         free_vars_type (free_vars_subtract_var (free_vars_exp fv e) v) ty
    | Ambient (v1, v2, e) ->
         free_vars_add_var (free_vars_add_var (free_vars_exp fv e) v2) v1
    | Thread (v, e) ->
         free_vars_add_var (free_vars_exp fv e) v
    | Migrate (v, a, e) ->
         free_vars_add_var (free_vars_atom (free_vars_exp fv e) a) v
*)

and free_vars_exps fv el =
   List.fold_left free_vars_exp fv el

(*
 * Free variables in an allocation operator.
 *)
and free_vars_alloc_op fv op =
   match op with
      AllocTuple (ty_vars, ty, args) ->
         free_vars_subtract_params (free_vars_type (free_vars_atoms fv args) ty) ty_vars
    | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
         free_vars_subtract_params (free_vars_type (free_vars_atoms fv args) ty) ty_vars
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         free_vars_subtract_params (free_vars_type (free_vars_record_atoms fv fields) ty) ty_vars
    | AllocDTuple (ty, ty_var, a, args) ->
         free_vars_type (free_vars_atom (free_vars_atoms fv args) a) ty
    | AllocArray (ty, args) ->
         free_vars_type (free_vars_atoms fv args) ty
    | AllocVArray (ty, a1, a2) ->
         free_vars_type (free_vars_atom (free_vars_atom fv a2) a1) ty
    | AllocFrame (ty, ty_var, tyl) ->
         free_vars_types (free_vars_type fv ty) tyl
    | AllocModule (ty, ty_var, fields) ->
         let fv = free_vars_type fv ty in
         let fv = SymbolTable.fold (fun fv _ a -> free_vars_atom fv a) fv fields in
            fv

and free_vars_record_atom fv (_, a) =
   free_vars_atom fv a

and free_vars_record_atoms fv fields =
   List.fold_left free_vars_record_atom fv fields

(*
 * Function definitions.
 *)
and free_vars_funs fv funs =
   let collect1 fv (_, _, _, ty_vars, ty, vars, e) =
      free_vars_subtract_params (free_vars_type (free_vars_subtract_vars (free_vars_exp fv e) vars) ty) ty_vars
   in
   let collect2 fv (v, _, _, _, _, _, _) =
      free_vars_subtract_var fv v
   in
      List.fold_left collect2 (List.fold_left collect1 fv funs) funs

(*
 * Let rec definitions.
 *)
and free_vars_letrecs fv fields =
   let collect1 fv (_, e, ty) =
      free_vars_type (free_vars_exp fv e) ty
   in
   let rec collect2 fv (v, _, _) =
      free_vars_subtract_var fv v
   in
      List.fold_left collect2 (List.fold_left collect1 fv fields) fields

(************************************************************************
 * SUBSTITUTION                                                         *
 ************************************************************************)

(*
 * Empty subst.
 *)
let empty_subst =
   { sub_fo_vars = SymbolTable.empty;
     sub_loc     = None
   }

let subst_add_loc subst loc =
   { subst with sub_loc = Some loc }

let subst_add_fo_var subst v ty =
   { subst with sub_fo_vars = SymbolTable.add subst.sub_fo_vars v ty }

(*
 * Combine two substitions.
 *)
let subst_union sub1 sub2 =
   let { sub_fo_vars   = sub_params1;
         sub_loc       = loc
       } = sub1
   in
   let { sub_fo_vars   = sub_params2 } = sub2 in
      { sub_fo_vars   = SymbolTable.union sub_params1 sub_params2;
        sub_loc       = loc
      }

(************************************************
 * NAMES
 *)
let subst_subtract_param subst v =
   { subst with sub_fo_vars = SymbolTable.remove subst.sub_fo_vars v }

let subst_subtract_params subst vars =
   let rec subtract subst = function
      v :: vars ->
         subtract (SymbolTable.remove subst v) vars
    | [] ->
         subst
   in
      { subst with sub_fo_vars = subtract subst.sub_fo_vars vars }

(************************************************
 * TYPES
 *)
let rec dest_type_core ds =
   dest_core subst_type_core ds

and subst_type subst ds =
   apply subst_union subst ds

and subst_types subst tyl =
   List.map (subst_type subst) tyl

and subst_types_opt subst tyl_opt =
   match tyl_opt with
      Some tyl -> Some (subst_types subst tyl)
    | None -> None

and subst_type_core subst ty =
   match ty with
      TyVoid
    | TyInt
    | TyChar
    | TyString
    | TyFloat
    | TyProject _ ->
         ty

    | TyFun (tyl, ty) ->
         TyFun (subst_types subst tyl, subst_type subst ty)
    | TyTuple tyl ->
         TyTuple (subst_types subst tyl)
    | TyArray ty ->
         TyArray (subst_type subst ty)
    | TyAll (vars, ty) ->
         let subst = subst_subtract_params subst vars in
            TyAll (vars, subst_type subst ty)
    | TyExists (vars, ty) ->
         let subst = subst_subtract_params subst vars in
            TyExists (vars, subst_type subst ty)

    | TyVar v ->
         (try dest_type_core (SymbolTable.find subst.sub_fo_vars v) with
             Not_found ->
                ty)
    | TyFrame (v, tyl) ->
         TyFrame (v, subst_types subst tyl)
    | TyApply (v, tyl) ->
         TyApply (v, subst_types subst tyl)
    | TyRecord (v, tyl) ->
         TyRecord (v, subst_types subst tyl)
    | TyUnion (v, tyl, set) ->
         TyUnion (v, subst_types subst tyl, set)
    | TyModule (v, tyl) ->
         TyModule (v, subst_types subst tyl)
    | TyTag (v, tyl) ->
         TyTag (v, subst_types subst tyl)
    | TyDTuple (v, tyl_opt) ->
         TyDTuple (v, subst_types_opt subst tyl_opt)

    | TyFormat (t1, t2, t3) ->
         TyFormat (subst_type subst t1, subst_type subst t2, subst_type subst t3)
    | TyExternal (ty, s) ->
         TyExternal (subst_type subst ty, s)
(*
    | TyOpen fields ->
         TyOpen (subst_type_open subst fields)

and subst_type_open subst fields =
   List.map (fun (v, ty) ->
         v, subst_type subst ty) fields
*)

(************************************************
 * TYPE DEFINITIONS
 *)

(*
 * Substitute into a record type.
 *)
let subst_type_record subst fields =
   FieldTable.map (fun (mflag, ty) ->
         mflag, subst_type subst ty) fields

(*
 * Subst into a union type.
 *)
let subst_type_union subst fields =
  FieldTable.map (subst_types subst) fields

(*
 * Substitute into a typ definition.
 *)
let subst_tydef_inner_core subst tydef =
   match tydef with
      TyDefFrame fields ->
         TyDefFrame (subst_type_record subst fields)
    | TyDefRecord fields ->
         TyDefRecord (subst_type_record subst fields)
    | TyDefUnion fields ->
         TyDefUnion (subst_type_union subst fields)
    | TyDefDTuple ty_var ->
         TyDefDTuple ty_var
    | TyDefLambda ty ->
         TyDefLambda (subst_type subst ty)
    | TyDefModule (names, fields) ->
         TyDefModule (names, SymbolTable.map (subst_type subst) fields)

let subst_tydef_inner subst (ds : tydef_inner) =
   apply subst_union subst ds

let dest_tydef_inner_core (ds : tydef_inner) =
   dest_core subst_tydef_inner_core ds

let subst_tydef subst tydef =
   let { tydef_fo_vars = params;
         tydef_inner = tydef
       } = tydef
   in
   let subst = subst_subtract_params subst params in
   let tydef = subst_tydef_inner subst tydef in
      { tydef_fo_vars = params;
        tydef_inner = tydef
      }

(************************************************
 * EXPRESSIONS
 *)
let lookup_var subst v =
   v

let rec subst_atom subst a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _ ->
         a
    | AtomVar v ->
         AtomVar (lookup_var subst v)
    | AtomNil ty ->
         AtomNil (subst_type subst ty)
    | AtomTyConstrain (a, ty) ->
         AtomTyConstrain (subst_atom subst a, subst_type subst ty)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (subst_atom subst a, subst_type subst ty, subst_types subst tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (lookup_var subst v, subst_type subst ty, subst_types subst tyl)
    | AtomTyUnpack v ->
         AtomTyUnpack (lookup_var subst v)
    | AtomConst (ty, ty_var, const_var) ->
         AtomConst (subst_type subst ty, ty_var, const_var)
    | AtomUnop (op, a) ->
         AtomUnop (op, subst_atom subst a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, subst_atom subst a1, subst_atom subst a2)

let subst_atom_opt subst a_opt =
   match a_opt with
      Some a -> Some (subst_atom subst a)
    | None -> None

let subst_atoms subst al =
   List.map (subst_atom subst) al

(*
 * Expressions.
 *)
let rec dest_exp_core ds =
   dest_core subst_exp_core ds

and subst_exp subst ds =
   apply subst_union subst ds

and subst_exp_core subst exp =
   match exp with
      LetFuns (funs, e) ->
         subst_funs subst funs e
    | LetRec (letrecs, e) ->
         subst_letrecs subst letrecs e
    | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
         LetExt (v, subst_type subst ty1, s, subst_type subst ty2, subst_types subst ty_args, subst_atoms subst args, subst_exp subst e)
    | LetAtom (v, ty, a, e) ->
         LetAtom (v, subst_type subst ty, subst_atom subst a, subst_exp subst e)
    | LetApply (v, ty, f, args, e) ->
         LetApply (v, subst_type subst ty, subst_atom subst f, subst_atoms subst args, subst_exp subst e)
    | LetClosure (v, f, ty_args, args, e) ->
         LetClosure (v, subst_atom subst f, subst_types subst ty_args, subst_atoms subst args, subst_exp subst e)
    | TailCall (f, args) ->
         TailCall (lookup_var subst f, subst_atoms subst args)
    | Return a ->
         Return (subst_atom subst a)

    | Match (a, cases) ->
         let cases = List.map (fun (s, e) -> s, subst_exp subst e) cases in
            Match (subst_atom subst a, cases)

    | MatchDTuple (a, cases) ->
         let cases = List.map (fun (a_opt, e) -> subst_atom_opt subst a_opt, subst_exp subst e) cases in
            MatchDTuple (subst_atom subst a, cases)

    | Try (e1, v, e2) ->
         Try (subst_exp subst e1,  lookup_var subst v, subst_exp subst e2)
    | Raise (a, ty) ->
         Raise (subst_atom subst a, subst_type subst ty)

    | LetAlloc (v, op, e) ->
         LetAlloc (v, subst_alloc_op subst op, subst_exp subst e)

    | SetSubscript (a1, a2, a3, ty, e) ->
         SetSubscript (subst_atom subst a1, subst_atom subst a2, subst_atom subst a3, subst_type subst ty, subst_exp subst e)
    | LetSubscript (v1, ty, a2, a3, e) ->
         LetSubscript (v1, subst_type subst ty, subst_atom subst a2, subst_atom subst a3, subst_exp subst e)

    | SetGlobal (v, ty, a, e) ->
         SetGlobal (lookup_var subst v, subst_type subst ty, subst_atom subst a, subst_exp subst e)

(*
    | LetName (v, ty, e) ->
         let subst' = subst_subtract_var subst v in
            LetName (v, subst_type subst ty, subst_exp subst' e)
    | Ambient (v1, v2, e) ->
         Ambient (lookup_var subst v1, lookup_var subst v2, subst_exp subst e)
    | Thread (v, e) ->
         Thread (lookup_var subst v, subst_exp subst e)
    | Migrate (v, a, e) ->
         Migrate (lookup_var subst v, subst_atom subst a, subst_exp subst e)
*)

and subst_funs subst funs e =
   let funs =
      List.map (fun (v, loc, gflag, ty_vars, ty, vars, e) ->
            let subst = subst_subtract_params subst ty_vars in
               v, loc, gflag, ty_vars, subst_type subst ty, vars, subst_exp subst e) funs
   in
      LetFuns (funs, subst_exp subst e)

and subst_letrecs subst fields e =
   let fields =
      List.map (fun (v, e, ty) -> v, subst_exp subst e, subst_type subst ty) fields
   in
      LetRec (fields, subst_exp subst e)

and subst_alloc_op subst op =
   match op with
      AllocTuple (ty_vars, ty, args) ->
         let subst = subst_subtract_params subst ty_vars in
            AllocTuple (ty_vars, subst_type subst ty, subst_atoms subst args)
    | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
         let subst = subst_subtract_params subst ty_vars in
            AllocUnion (ty_vars, subst_type subst ty, ty_var, const_var, subst_atoms subst args)
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         let subst = subst_subtract_params subst ty_vars in
            AllocRecord (ty_vars, subst_type subst ty, ty_var, subst_record_atoms subst fields)
    | AllocDTuple (ty, ty_var, a, args) ->
         AllocDTuple (subst_type subst ty, ty_var, subst_atom subst a, subst_atoms subst args)
    | AllocArray (ty, args) ->
         AllocArray (subst_type subst ty, subst_atoms subst args)
    | AllocVArray (ty, a1, a2) ->
         AllocVArray (subst_type subst ty, subst_atom subst a1, subst_atom subst a2)
    | AllocFrame (ty, ty_var, tyl) ->
         AllocFrame (subst_type subst ty, ty_var, subst_types subst tyl)
    | AllocModule (ty, ty_var, fields) ->
         let ty = subst_type subst ty in
         let fields = SymbolTable.map (subst_atom subst) fields in
            AllocModule (ty, ty_var, fields)

and subst_record_atom subst (label, a) =
   label, subst_atom subst a

and subst_record_atoms subst fields =
   List.map (subst_record_atom subst) fields

(*
 * Substitution for an entire program.
 *)
let subst_prog subst prog =
   let { prog_types = types;
         prog_import = import;
         prog_export = export;
         prog_globals = globals
       } = prog
   in
   let types = SymbolTable.map (subst_tydef subst) types in
   let globals =
      SymbolTable.map (fun (ty, init) ->
            subst_type subst ty, init) globals
   in
   let import =
      SymbolTable.map (fun { import_info = info; import_type = ty } ->
            { import_info = info; import_type = subst_type subst ty }) import
   in
   let export =
      SymbolTable.map (fun export ->
            let { export_type = ty } = export in
               { export with export_type = subst_type subst ty }) export
   in
      { prog with prog_types = types;
                  prog_import = import;
                  prog_export = export;
                  prog_globals = globals
      }

(************************************************************************
 * GLOBAL FUNCTIONS                                                     *
 ************************************************************************)

let free_vars_type =
   free_vars_type free_vars_empty

let free_vars_exp =
   free_vars_exp free_vars_empty

(*
 * Wrap a core.
 *)
let make_type = wrap_core
let make_tydef_inner = wrap_core
let make_exp = wrap_core

let loc_of_type = label_of_term
let loc_of_tydef_inner = label_of_term
let loc_of_exp = label_of_term

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
