(*
 * Delayed substitution operations.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Location

open Aml_ir

(*
 * Free vars.
 *)
val free_vars_type       : ty  -> free_vars
val free_vars_exp        : exp -> free_vars

(*
 * Substitution.
 *)
val empty_subst          : subst

val subst_add_loc        : subst -> loc -> subst
val subst_add_fo_var     : subst -> ty_var -> ty -> subst

val subst_type           : subst -> ty -> ty
val subst_tydef_inner    : subst -> tydef_inner -> tydef_inner
val subst_exp            : subst -> exp -> exp
val subst_prog           : subst -> prog -> prog

(*
 * Destruction.
 *)
val dest_type_core        : ty -> ty_core
val dest_tydef_inner_core : tydef_inner -> tydef_inner_core
val dest_exp_core         : exp -> exp_core

(*
 * Wrap the core.
 *)
val loc_of_type          : ty -> loc
val loc_of_tydef_inner   : tydef_inner -> loc
val loc_of_exp           : exp -> loc

val make_type            : loc -> ty_core -> ty
val make_tydef_inner     : loc -> tydef_inner_core -> tydef_inner
val make_exp             : loc -> exp_core -> exp

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
