(*
 * Compile the file.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Fir_state

open Aml_ir_print

(*
 * Debugging.
 *)
let debug_prog s prog =
   if debug debug_print_ir then
      debug_prog s prog

(*
 * Compile the program.
 *)
let compile prog =
   let prog = Aml_ir_tast_prog.build_prog prog in
   let _ = debug_prog "Aml_ir_tast" prog in

   let prog = Aml_ir_partial.partial_prog prog in
   let _ = debug_prog "Aml_ir_partial" prog in

   let prog = Aml_ir_dead.dead_prog Aml_ir_dead.PreserveNone prog in
   let _ = debug_prog "Aml_ir_dead (before_cps,before_inline)" prog in

   let prog = Aml_ir_inline.inline_prog prog in
   let _ = debug_prog "Aml_ir_inline (before_cps)" prog in

   let prog = Aml_ir_dead.dead_prog Aml_ir_dead.PreserveNone prog in
   let _ = debug_prog "Aml_ir_dead (before_cps,after_inline)" prog in

   let prog = Aml_ir_cps.cps_prog prog in
   let _ = debug_prog "Aml_ir_cps" prog in

   let prog = Aml_ir_closure.close_prog prog in
   let _ = debug_prog "Aml_ir_closure" prog in

   let prog = Aml_ir_prog.prog_prog prog in
   let _ = debug_prog "Aml_ir_prog" prog in

   let prog = Aml_ir_fun.fun_prog prog in
   let _ = debug_prog "Aml_ir_fun" prog in

   let prog = Aml_ir_dead.dead_prog Aml_ir_dead.PreserveNone prog in
   let _ = debug_prog "Aml_ir_dead (after_cps,before_inline)" prog in

   let prog = Aml_ir_inline.inline_prog prog in
   let _ = debug_prog "Aml_ir_inline (after_cps)" prog in

   let prog = Aml_ir_dead.dead_prog Aml_ir_dead.PreserveNone prog in
   let _ = debug_prog "Aml_ir_dead (after_cps,after_inline)" prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
