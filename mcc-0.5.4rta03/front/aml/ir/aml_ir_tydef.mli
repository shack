(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_ir
open Aml_ir_pos
open Aml_ir_env_type

(*
 * Utilities.
 *)
val wrap_fun_type : loc -> ty list -> ty -> ty

(*
 * Type application.
 *)
val apply_type        : genv -> pos -> ty_var -> ty list -> tydef_inner

val apply_apply_type  : genv -> pos -> ty_var -> ty list -> ty
val apply_union_type  : genv -> pos -> ty_var -> ty list -> union_fields
val apply_record_type : genv -> pos -> ty_var -> ty list -> record_fields
val apply_module_type : genv -> pos -> ty_var -> ty list -> module_names * module_fields

(*
 * Type destructors.
 *)
val dest_apply_type  : genv -> pos -> ty -> ty_var * ty
val dest_union_type  : genv -> pos -> ty -> ty_var * union_fields * union_index
val dest_record_type : genv -> pos -> ty -> ty_var * record_fields
val dest_module_type : genv -> pos -> ty -> ty_var * module_names * module_fields

(*
 * Utilities.
 *)
val lookup_union_field  : union_fields -> pos -> const_var -> union_field * int
val lookup_record_field : record_fields -> pos -> label -> record_field * int
val lookup_module_field : module_names -> module_fields -> pos -> ext_var -> ty

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
