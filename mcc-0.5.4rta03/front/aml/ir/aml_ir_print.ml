(*
 * Print the FIR.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol
open Field_table
open Interval_set
open Attribute

open Aml_set

open Aml_ir
open Aml_ir_ds
open Aml_ir_env_type

(*
 * Default tabstop.
 *)
let tabstop = 3

(************************************************************************
 * TYPES                                                                *
 ************************************************************************)

(*
 * Print a list of ints.
 *)
let pp_print_ints buf il =
   List.iter (fun i ->
         pp_print_string buf " ";
         pp_print_int buf i) il

(*
 * Print a list of vars
 *)
let pp_print_param buf v =
   pp_print_string buf " ";
   pp_print_symbol buf v

let pp_print_params buf vars =
   List.iter (pp_print_param buf) vars

let rec pp_print_param_args first buf = function
   p :: params ->
      if not first then
         pp_print_string buf ", ";
      pp_print_string buf "'";
      pp_print_symbol buf p;
      pp_print_param_args false buf params
 | [] ->
      ()

(*
 * Print the global flag.
 *)
let string_of_gflag gflag =
   match gflag with
      FunGlobalClass -> "$global"
    | FunTopLevelClass -> "$toplevel"
    | FunPartialClass -> "$partial"
    | FunContClass -> "$cont"
    | FunLocalClass -> "$local"

let pp_print_gflag buf gflag =
   pp_print_string buf (string_of_gflag gflag)

(*
 * Print a type.
 *)
let pp_print_bound buf fold f l =
   pp_print_string buf "=";
   pp_open_hvbox buf 0;
   ignore (fold (fun first left right ->
                 if not first then
                    begin
                       pp_print_string buf ",";
                       pp_print_space buf ()
                    end;
                 (match left with
                     Interval_set.Infinity ->
                        pp_print_string buf "[min"
                   | Interval_set.Open x ->
                        pp_print_string buf "(";
                        f buf x
                   | Interval_set.Closed x ->
                        pp_print_string buf "[";
                        f buf x);
                 pp_print_string buf "..";
                 (match right with
                     Interval_set.Infinity ->
                        pp_print_string buf "max]"
                   | Interval_set.Open x ->
                        f buf x;
                        pp_print_string buf ")"
                   | Interval_set.Closed x ->
                        f buf x;
                        pp_print_string buf "]");
                 false) true l);
   pp_print_string buf "";
   pp_close_box buf ()

let rec pp_print_type buf ty =
   pp_print_type_core buf (dest_type_core ty)

and pp_print_type_core buf = function
   TyVoid ->
      pp_print_string buf "$void"
 | TyInt ->
      pp_print_string buf "$int"
 | TyChar ->
      pp_print_string buf "$char"
 | TyString ->
      pp_print_string buf "$string"
 | TyFloat ->
      pp_print_string buf "$float"
 | TyFormat (t1, t2, t3) ->
      pp_print_string buf "(";
      pp_open_hvbox buf 0;
      pp_print_type buf t1;
      pp_print_string buf ",";
      pp_print_space buf ();
      pp_print_type buf t2;
      pp_print_string buf ",";
      pp_print_space buf ();
      pp_print_type buf t3;
      pp_print_string buf ") format";
      pp_close_box buf ()
 | TyFun (tyl, ty) ->
      fprintf buf "@[<hv 1>((%a)@ -> %a)@]" (**)
         (pp_print_type_params true) tyl
         pp_print_type ty
 | TyTuple tl ->
      pp_print_string buf "(";
      pp_print_type_prod true buf tl;
      pp_print_string buf ")"

 | TyArray ty ->
      pp_print_string buf "(";
      pp_print_type buf ty;
      pp_print_string buf " array)"

 | TyVar v ->
      pp_print_string buf "'";
      pp_print_symbol buf v

 | TyAll (vars, ty) ->
      fprintf buf "@[<hv 3>(all (%a).@ %a)@]" (**)
         (pp_print_type_vars true) vars
         pp_print_type ty

 | TyExists (vars, ty) ->
      fprintf buf "@[<hv 3>(exists (%a).@ %a)@]" (**)
         (pp_print_type_vars true) vars
         pp_print_type ty

 | TyProject (v, i) ->
      fprintf buf "%a.%d" pp_print_symbol v i

 | TyApply (v, tyl) ->
      fprintf buf "@[<hv 3>apply:%a@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl

 | TyUnion (v, tyl, set) ->
      fprintf buf "@[<hv 3>union:%a@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl

 | TyRecord (v, tyl) ->
      fprintf buf "@[<hv 3>record:%a@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl

 | TyFrame (v, tyl) ->
      fprintf buf "@[<hv 3>frame:%a@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl

 | TyModule (v, tyl) ->
      fprintf buf "@[<hv 3>module:%a@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl

 | TyTag (v, tyl) ->
      fprintf buf "@[<hv 3>tag[%a]@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl
 | TyDTuple (v, tyl_opt) ->
      fprintf buf "@[<hv 3>dtuple:%a" pp_print_symbol v;
      (match tyl_opt with
          Some tyl ->
             fprintf buf "@,[@[<hv 0>%a@]]" (pp_print_type_params true) tyl
        | None ->
             ());
      fprintf buf "@]"

(*
 | TyOpen fields ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf 2;
      pp_print_string buf "{ ";
      pp_print_type_open true buf fields;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}";
      pp_close_box buf ()
*)

 | TyExternal (ty, s) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf ("(external \"" ^ String.escaped s ^ "\" :");
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ()

(*
 * Print the variables in a universal binding.
 *)
and pp_print_type_vars first buf = function
   v :: vars ->
      if not first then
         pp_print_string buf ", ";
      pp_print_string buf "'";
      pp_print_symbol buf v;
      pp_print_type_vars false buf vars
 | [] ->
      ()

(*
 * Print the variables in a universal binding.
 *)
and pp_print_type_args first buf = function
   ty :: args ->
      if not first then
         pp_print_string buf "; ";
      pp_print_type buf ty;
      pp_print_type_args false buf args
 | [] ->
      ()

(*
 * Type parameters.
 *)
and pp_print_type_params first buf = function
   ty :: tl ->
      if not first then
         begin
            pp_print_string buf ",";
            pp_print_space buf ()
         end;
      pp_print_type buf ty;
      pp_print_type_params false buf tl
 | [] ->
      ()

(*
 * Print a product type.
 *)
and pp_print_type_prod first buf = function
   ty :: tyl ->
      if not first then
         begin
            pp_print_space buf ();
            pp_print_string buf "* "
         end;
      pp_print_type buf ty;
      pp_print_type_prod false buf tyl

 | [] ->
      ()

(*
 * Print the open name.
 *)
and pp_print_type_open first buf = function
   (label, ty) :: fields ->
      if not first then
         begin
            pp_print_string buf ";";
            pp_print_space buf ()
         end;
      pp_open_hvbox buf tabstop;
      pp_print_symbol buf label;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ();
      pp_print_type_open false buf fields

 | [] ->
      ()

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Print a record.
 *)
let pp_print_type_record buf fields =
   let _ =
      FieldTable.fold (fun first l (mflag, ty) ->
            if not first then
               begin
                  pp_print_string buf ";";
                  pp_print_space buf ()
               end;
            pp_open_hvbox buf tabstop;
            if mflag then
               pp_print_string buf "mutable ";
            pp_print_symbol buf l;
            pp_print_string buf " :";
            pp_print_space buf ();
            pp_print_type buf ty;
            pp_close_box buf ();
            false) true fields
   in
      ()

(*
 * Print the union fields.
 *)
let pp_print_type_union buf fields =
   let _ =
      FieldTable.fold (fun first v tyl ->
            if not first then
               begin
                  pp_print_space buf ();
                  pp_print_string buf "| "
               end;
            pp_open_hvbox buf tabstop;
            pp_print_symbol buf v;
            if tyl <> [] then
               begin
                  pp_print_string buf " of ";
                  pp_print_type_prod true buf tyl
               end;
            pp_close_box buf ();
            false) true fields
   in
      ()

(*
 * Print a type definition.
 *)
let pp_print_tydef_inner_core buf tydef =
   match tydef with
      TyDefFrame rty ->
         pp_open_hvbox buf 0;
         pp_print_string buf "{< ";
         pp_open_hvbox buf 0;
         pp_print_type_record buf rty;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf ">}";
         pp_close_box buf ()

    | TyDefRecord rty ->
         pp_open_hvbox buf 0;
         pp_print_string buf "{ ";
         pp_open_hvbox buf 0;
         pp_print_type_record buf rty;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "}";
         pp_close_box buf ()

    | TyDefModule (names, fields) ->
         fprintf buf "@[<hv 0>@[<hv 3>struct";
         SymbolTable.iter (fun v ty ->
               fprintf buf "@ @[<hv 3>val %a@ : %a@]" (**)
                  pp_print_symbol v
                  pp_print_type ty) fields;
         FieldTable.iter (fun ext_var v ->
               fprintf buf "@ let %a = %a" (**)
                  pp_print_symbol ext_var
                  pp_print_symbol v) names;
         fprintf buf "@]@ end@]"

    | TyDefUnion fields ->
         fprintf buf "@[<hv 0>union %a@]" (**)
            pp_print_type_union fields

    | TyDefDTuple ty_var ->
         fprintf buf "dtuple[%a]" pp_print_symbol ty_var

    | TyDefLambda ty ->
         pp_print_type buf ty

let pp_print_tydef_inner buf tydef =
   pp_print_tydef_inner_core buf (dest_tydef_inner_core tydef)

let pp_print_tydef buf tydef =
   let { tydef_fo_vars = params;
         tydef_inner = tydef
       } = tydef
   in
      fprintf buf "@[<hv 3>Lambda [%a].@ %a@]" (**)
         (pp_print_param_args true) params
         pp_print_tydef_inner tydef

(************************************************************************
 * SETS
 ************************************************************************)

(*
 * Print the sets.
 *)
let pp_print_char_set buf set =
   if CharSet.is_singleton set then
      fprintf buf "{ '%s' }" (Char.escaped (CharSet.dest_singleton set))
   else
      CharSet.iter (fun left right ->
            (match left with
                Infinity ->
                   fprintf buf "($inf"
              | Open i ->
                   fprintf buf "('%s'" (Char.escaped i)
              | Closed i ->
                   fprintf buf "['%s'" (Char.escaped i));
            fprintf buf ",";
            (match right with
                Infinity ->
                   fprintf buf "$inf)"
              | Open i ->
                   fprintf buf "'%s')" (Char.escaped i)
              | Closed i ->
                   fprintf buf "'%s']" (Char.escaped i))) set

let pp_print_int_set buf set =
   if IntSet.is_singleton set then
      fprintf buf "{ %d }" (IntSet.dest_singleton set)
   else
      IntSet.iter (fun left right ->
            (match left with
                Infinity ->
                   fprintf buf "($inf"
              | Open i ->
                   fprintf buf "(%d" i
              | Closed i ->
                   fprintf buf "[%d" i);
            fprintf buf ",";
            (match right with
                Infinity ->
                   fprintf buf "$inf)"
              | Open i ->
                   fprintf buf "%d)" i
              | Closed i ->
                   fprintf buf "%d]" i)) set

let pp_print_const_set buf ty_var set =
      fprintf buf "const-set[%a]{" pp_print_symbol ty_var;
      ignore (SymbolSet.fold (fun first v ->
                    if first then
                       fprintf buf " %a" pp_print_symbol v
                    else
                       fprintf buf ", %a" pp_print_symbol v;
                    false) true set);
      pp_print_string buf " }"

(*
 * Print an arbitrary set.
 *)
let pp_print_set buf s =
   match s with
      CharSet set ->
         pp_print_char_set buf set
    | IntSet set ->
         pp_print_int_set buf set
    | ConstSet (ty_var, set) ->
         pp_print_const_set buf ty_var set

(************************************************************************
 * OPERATORS
 ************************************************************************)

(*
 * Operator with type argument.
 *)
let string_of_type_op s ty =
   Format.fprintf Format.str_formatter "%s[%a]" s pp_print_type ty;
   Format.flush_str_formatter ()

(*
 * String for a binary op.
 *)
let string_of_unop = function
   NotBoolOp          -> "$not"

 | UMinusIntOp        -> "-"
 | NotIntOp           -> "$lnot"
 | AbsIntOp           -> "$abs"

 | UMinusFloatOp       -> "-."
 | AbsFloatOp          -> "$fabs"
 | SqrtFloatOp         -> "$fsqrt"
 | ExpFloatOp          -> "$fexp"
 | LogFloatOp          -> "$flog"
 | Log10FloatOp        -> "$flog10"
 | CosFloatOp          -> "$fcos"
 | SinFloatOp          -> "$fsin"
 | TanFloatOp          -> "$ftan"
 | ACosFloatOp         -> "$facos"
 | ASinFloatOp         -> "$fasin"
 | ATanFloatOp         -> "$fatan"
 | CosHFloatOp         -> "$fcosh"
 | SinHFloatOp         -> "$fsinh"
 | TanHFloatOp         -> "$ftanh"
 | CeilFloatOp         -> "$fceil"
 | FloorFloatOp        -> "$ffloor"

 | IntOfFloatOp       -> "$coerce{$int,$float}"
 | FloatOfIntOp       -> "$coerce{$float,$int}"
 | CharOfIntOp        -> "$coerce{$char,$int}"
 | IntOfCharOp        -> "$coerce{$int,$char}"

 | LengthOfStringOp -> "$string_length"
 | LengthOfBlockOp ty -> string_of_type_op "$array_length" ty

(*
 * String for a binary op.
 *)
let string_of_binop = function
   OrBoolOp   -> "||"
 | AndBoolOp  -> "&&"

 | PlusIntOp  -> "+"
 | MinusIntOp -> "-"
 | MulIntOp   -> "*"
 | DivIntOp   -> "/"
 | RemIntOp   -> "rem"

 | AndIntOp -> "land"
 | OrIntOp  -> "lor"
 | LslIntOp -> "lsl"
 | LsrIntOp -> "lsr"
 | AsrIntOp -> "asr"
 | XorIntOp -> "xor"

 | EqIntOp   -> "="
 | NeqIntOp  -> "<>"
 | LeIntOp   -> "<="
 | LtIntOp   -> "<"
 | GtIntOp   -> ">"
 | GeIntOp   -> ">="
 | CmpIntOp  -> "<=>"

 | EqCharOp   -> "=c"
 | NeqCharOp  -> "<>c"
 | LeCharOp   -> "<=c"
 | LtCharOp   -> "<c"
 | GtCharOp   -> ">c"
 | GeCharOp   -> ">=c"
 | CmpCharOp  -> "<=>c"

 | MaxIntOp  -> "max"
 | MinIntOp  -> "min"

 | PlusFloatOp  -> "+."
 | MinusFloatOp -> "-."
 | MulFloatOp   -> "*."
 | DivFloatOp   -> "/."
 | RemFloatOp   -> "rem."
 | LdExpFloatIntOp -> "$fldexp"
 | ATan2FloatOp -> "$fatan2"
 | PowerFloatOp -> "$fpow"

 | EqFloatOp   -> "=."
 | NeqFloatOp  -> "<>."
 | LeFloatOp   -> "<=."
 | LtFloatOp   -> "<."
 | GtFloatOp   -> ">."
 | GeFloatOp   -> ">=."
 | CmpFloatOp  -> "<=>."

 | MaxFloatOp  -> "max."
 | MinFloatOp  -> "min."

 | NeqEqOp ty -> string_of_type_op "!=*" ty
 | EqEqOp ty -> string_of_type_op "==*" ty

(************************************************************************
 * ATOMS                                                                *
 ************************************************************************)

(*
 * Print an atom.
 *)
let rec pp_print_atom buf = function
   AtomInt i ->
      pp_print_int buf i
 | AtomChar c ->
      let s = String.escaped (String.make 1 c) in
         pp_print_string buf ("'" ^ s ^ "'")
 | AtomFloat x ->
      pp_print_float buf x
 | AtomVar v ->
      pp_print_symbol buf v
 | AtomNil ty ->
      fprintf buf "@[<hv 3>nil[%a]@]" pp_print_type ty
 | AtomTyConstrain (a, ty) ->
      fprintf buf "@[<hv 3>ty_constrain(%a@ : %a)@]" (**)
         pp_print_atom a
         pp_print_type ty
 | AtomTyApply (a, ty, tyl) ->
      fprintf buf "@[<hv 3>ty_apply(%a[%a]@ : %a)@]" (**)
         pp_print_atom a
         (pp_print_type_args true) tyl
         pp_print_type ty
 | AtomTyPack (v, ty, tyl) ->
      fprintf buf "@[<hv 3>ty_pack(%a[%a]@ : %a)@]" (**)
         pp_print_symbol v
         (pp_print_type_args true) tyl
         pp_print_type ty
 | AtomTyUnpack v ->
      fprintf buf "ty_unpack(%a)" pp_print_symbol v
 | AtomConst (ty, ty_var, const_var) ->
      Format.fprintf buf "const[%a]{%a, %a}" (**)
         pp_print_symbol const_var
         pp_print_type ty
         pp_print_symbol ty_var
 | AtomFrameLabel (ty_var, label) ->
      fprintf buf "frame:%a.%a" pp_print_symbol ty_var pp_print_symbol label
 | AtomRecordLabel (ty_var, label) ->
      fprintf buf "record:%a.%a" pp_print_symbol ty_var pp_print_symbol label
 | AtomModuleLabel (ty_var, label) ->
      fprintf buf "module:%a.%a" pp_print_symbol ty_var pp_print_symbol label
 | AtomUnop (op, a) ->
      fprintf buf "(@[<hv 3>%s@ %a@])" (**)
         (string_of_unop op)
         pp_print_atom a
 | AtomBinop (op, a1, a2) ->
      fprintf buf "(@[<hv 3>%a@ %s %a@])" (**)
         pp_print_atom a1
         (string_of_binop op)
         pp_print_atom a2

let pp_print_atom_opt buf a_opt =
   match a_opt with
      Some a -> pp_print_atom buf a
    | None -> pp_print_string buf "_"

let rec pp_print_args first buf = function
   arg :: args ->
      if not first then
         pp_print_string buf ", ";
      pp_print_atom buf arg;
      pp_print_args false buf args
 | [] ->
      ()

let rec pp_print_vars first buf = function
   v :: vars ->
      if not first then
         pp_print_string buf ", ";
      pp_print_symbol buf v;
      pp_print_vars false buf vars
 | [] ->
      ()

(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)

(*
 * Print an expression.
 *)
let rec pp_print_exp inlet buf e =
   pp_print_exp_core inlet buf (dest_exp_core e)

and pp_print_exp_core inlet buf e =
   if inlet then
      pp_print_space buf ();
   match e with
      LetFuns (funs, e) ->
         pp_open_hvbox buf 0;
         pp_print_funs true buf funs;
         pp_print_space buf ();
         pp_print_string buf "in";
         pp_print_exp true buf e;
         pp_close_box buf ()

    | LetRec (letrecs, e) ->
         pp_open_hvbox buf 0;
         pp_print_letrecs true buf letrecs;
         pp_print_space buf ();
         pp_print_string buf "in";
         pp_print_exp true buf e;
         pp_close_box buf ()

    | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
         fprintf buf "@[<hv 3>let @[<hv 3>%a :@ %a@] = (\"%s\" : %a)[%a](%a) in@]" (**)
            pp_print_symbol v
            pp_print_type ty1
            (String.escaped s)
            pp_print_type ty2
            (pp_print_type_args true) ty_args
            (pp_print_args true) args;
         pp_print_exp true buf e

    | LetAtom (v, ty, a, e) ->
         fprintf buf "@[<hv 3>let %a@ : %a@ = %a in@]" (**)
            pp_print_symbol v
            pp_print_type ty
            pp_print_atom a;
         pp_print_exp true buf e

    | LetApply (v, ty, f, args, e) ->
         fprintf buf "@[<hv 3>let %a :@ %a = %a(%a) in@]" (**)
            pp_print_symbol v
            pp_print_type ty
            pp_print_atom f
            (pp_print_args true) args;
         pp_print_exp true buf e

    | LetClosure (v, f, ty_args, args, e) ->
         fprintf buf "@[<hv 3>let closure %a = %a[%a](%a) in@]" (**)
            pp_print_symbol v
            pp_print_atom f
            (pp_print_type_args true) ty_args
            (pp_print_args true) args;
         pp_print_exp true buf e

    | TailCall (f, args) ->
         pp_print_atom buf f;
         pp_print_string buf "(";
         pp_print_args true buf args;
         pp_print_string buf ")"

    | Return a ->
         pp_print_string buf "return ";
         pp_print_atom buf a

    | Match (a, cases) ->
         fprintf buf "@[<hv 3>($match %a $with" pp_print_atom a;
         List.iter (fun (set, e) ->
               Format.fprintf buf "@ @[<hv 3>| %a ->%a@]" (**)
                  pp_print_set set
                  (pp_print_exp true) e) cases;
         fprintf buf ")@]"

    | MatchDTuple (a, cases) ->
         fprintf buf "@[<hv 3>($match %a $with" pp_print_atom a;
         List.iter (fun (a_opt, e) ->
               Format.fprintf buf "@ @[<hv 3>| %a ->%a@]" (**)
                  pp_print_atom_opt a_opt
                  (pp_print_exp true) e) cases;
         fprintf buf ")@]"

    | Try (e1, v, e2) ->
         fprintf buf "@[<hv 0>@[<hv 3>try %a@]@ @[<hv 3>with %a ->@ %a@]@]" (**)
            (pp_print_exp true) e1
            pp_print_symbol v
            (pp_print_exp true) e2

    | Raise (a, ty) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "((raise ";
         pp_print_atom buf a;
         pp_print_string buf ") :";
         pp_print_space buf ();
         pp_print_type buf ty;
         pp_print_string buf ")";
         pp_close_box buf ()

    | LetAlloc (v, op, e) ->
         fprintf buf "@[<hv 0>@[<hv 3>let %a =@ %a@]@ in@]" (**)
            pp_print_symbol v
            pp_print_alloc_op op;
         pp_print_exp true buf e

    | SetSubscript (v, i, x, ty, e) ->
         pp_open_hvbox buf tabstop;
         pp_print_atom buf v;
         pp_print_string buf ".(";
         pp_print_atom buf i;
         pp_print_string buf ") <-";
         pp_print_space buf ();
         pp_print_string buf "(";
         pp_open_hvbox buf 0;
         pp_print_atom buf x;
         pp_print_space buf ();
         pp_print_string buf ": ";
         pp_print_type buf ty;
         pp_print_string buf ");";
         pp_close_box buf ();
         pp_close_box buf ();
         pp_print_exp true buf e

    | LetSubscript (v, ty, a, i, e) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "let ";
         pp_print_symbol buf v;
         pp_print_space buf ();
         pp_print_string buf ": ";
         pp_print_type buf ty;
         pp_print_string buf " = ";
         pp_print_atom buf a;
         pp_print_string buf ".(";
         pp_print_atom buf i;
         pp_print_string buf ") in";
         pp_close_box buf ();
         pp_print_exp true buf e

    | SetGlobal (v, ty, a, e) ->
         fprintf buf "@[<hv 3>%a :@ %a <- %a;@]" (**)
            pp_print_symbol v
            pp_print_type ty
            pp_print_atom a;
         pp_print_exp true buf e

(*
    | LetName (v, ty, e) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "let ";
         pp_print_symbol buf v;
         pp_print_space buf ();
         pp_print_string buf ": ";
         pp_print_type buf ty;
         pp_print_string buf " = name() in";
         pp_close_box buf ();
         pp_print_exp true buf e

    | Ambient (v, f, e) ->
         pp_print_string buf "ambient ";
         pp_print_symbol buf v;
         pp_print_string buf " (";
         pp_print_symbol buf f;
         pp_print_string buf ");";
         pp_print_exp true buf e

    | Thread (f, e) ->
         pp_print_string buf "thread(";
         pp_print_symbol buf f;
         pp_print_string buf ");";
         pp_print_exp true buf e

    | Migrate (v_self, a, e) ->
         pp_print_string buf "migrate(";
         pp_print_symbol buf v_self;
         pp_print_string buf ", ";
         pp_print_atom buf a;
         pp_print_string buf ");";
         pp_print_exp true buf e
*)

and pp_print_funs first buf = function
   (f, _, gflag, ty_vars, ty, vars, e) :: rest ->
      if first then
         pp_print_string buf "let "
      else
         begin
            pp_print_space buf ();
            pp_print_string buf "and "
         end;
      pp_open_hvbox buf 0;
      pp_print_gflag buf gflag;
      pp_print_string buf " (";
      pp_print_symbol buf f;
      pp_print_space buf ();
      pp_print_string buf ": ";
      pp_print_type buf ty;
      pp_print_string buf ")[";
      pp_print_param_args true buf ty_vars;
      pp_print_string buf "](";
      pp_print_vars true buf vars;
      pp_print_string buf ") =";
      pp_print_exp true buf e;
      pp_close_box buf ();
      pp_print_funs false buf rest

 | [] ->
      ()

and pp_print_letrecs first buf = function
   (f, e, ty) :: rest ->
      if first then
         pp_print_string buf "let "
      else
         begin
            pp_print_space buf ();
            pp_print_string buf "and "
         end;
      pp_open_hvbox buf 0;
      pp_print_string buf "(";
      pp_print_symbol buf f;
      pp_print_space buf ();
      pp_print_string buf ": ";
      pp_print_type buf ty;
      pp_print_string buf ") =";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_close_box buf ();
      pp_print_letrecs false buf rest

 | [] ->
      ()

(*
 * Allocation.
 *)
and pp_print_alloc_op buf op =
   match op with
      AllocTuple (ty_vars, ty, args) ->
         Format.fprintf buf "@[<hv 3>$alloc_tuple[%a](%a)@ : %a@]" (**)
            (pp_print_type_vars true) ty_vars
            (pp_print_args true) args
            pp_print_type ty
    | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
         Format.fprintf buf "@[<hv 3>$alloc_union[%a](tu:%a=%a,@ tag:%a,@ args={%a})@]" (**)
            (pp_print_type_vars true) ty_vars
            pp_print_type ty
            pp_print_symbol ty_var
            pp_print_symbol const_var
            (pp_print_args true) args
    | AllocDTuple (ty, ty_var, a, args) ->
         fprintf buf "@[<hv 0>$alloc_dtuple[%a.%a](%a)@ : %a@]" (**)
            pp_print_symbol ty_var
            pp_print_atom a
            (pp_print_args true) args
            pp_print_type ty
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         Format.fprintf buf "@[<hv 0>@[<hv 3>$alloc_record[%a][%a : %a] {" (**)
            (pp_print_type_vars true) ty_vars
            pp_print_symbol ty_var
            pp_print_type ty;
         List.iter (fun (label, a) ->
               fprintf buf "@ %a = %a;" pp_print_symbol label pp_print_atom a) fields;
         Format.fprintf buf "@]@ }@]"
    | AllocArray (ty, args) ->
         Format.fprintf buf "@[<hv 3>$alloc_array(%a@ : %a)@]" (**)
            (pp_print_args true) args
            pp_print_type ty
    | AllocVArray (ty, a1, a2) ->
         Format.fprintf buf "$alloc_varray(%a, %a : %a)" (**)
            pp_print_atom a1
            pp_print_atom a2
            pp_print_type ty
    | AllocFrame (ty, ty_var, tyl) ->
         Format.fprintf buf "@[<hv 3>$alloc_frame[%a[%a] : %a]@]" (**)
            pp_print_symbol ty_var
            (pp_print_type_args true) tyl
            pp_print_type ty
    | AllocModule (ty, ty_var, fields) ->
         fprintf buf "@[<hv 0>@[<hv 3>$alloc_module[%a] : %a = struct" (**)
            pp_print_symbol ty_var
            pp_print_type ty;
         SymbolTable.iter (fun v a ->
               fprintf buf "@ let %a = %a" (**)
                  pp_print_symbol v
                  pp_print_atom a) fields;
         fprintf buf "@]@ end@]"

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Print a global.
 *)
let pp_print_init buf init =
   match init with
      InitString s ->
         fprintf buf "\"%s\"" (String.escaped s)
    | InitAtom a ->
         pp_print_atom buf a
    | InitAlloc op ->
         pp_print_alloc_op buf op

(*
 * Print the boot record.
 *)
let pp_print_boot buf boot =
   let { boot_ty_unit     = ty_unit;
         boot_ty_unit_var = ty_unit_var;
         boot_unit_var    = unit_var;

         boot_ty_bool     = ty_bool;
         boot_ty_bool_var = ty_bool_var;
         boot_true_var    = true_var;
         boot_false_var   = false_var;
         boot_true_set    = true_set;
         boot_false_set   = false_set;

         boot_ty_exn      = ty_exn;
         boot_ty_exn_var  = ty_exn_var
       } = boot
   in
      fprintf buf "@ ty_unit = %a" pp_print_type_core ty_unit;
      fprintf buf "@ ty_unit_var = %a" pp_print_symbol ty_unit_var;
      fprintf buf "@ unit_var = %a" pp_print_symbol unit_var;

      fprintf buf "@ ty_bool = %a" pp_print_type_core ty_bool;
      fprintf buf "@ ty_bool_var = %a" pp_print_symbol ty_bool_var;
      fprintf buf "@ true_var = %a" pp_print_symbol true_var;
      fprintf buf "@ false_var = %a" pp_print_symbol false_var;

      fprintf buf "@ ty_exn = %a" pp_print_type_core ty_exn;
      fprintf buf "@ ty_exn_var = %a" pp_print_symbol ty_exn_var

(*
 * Print the program.
 *)
let pp_print_prog buf prog =
   let { prog_name    = name;
         prog_names   = names;
         prog_types   = types;
         prog_globals = globals;
         prog_tags    = tags;
         prog_import  = import;
         prog_export  = export;
         prog_funs    = funs;
         prog_boot    = boot
       } = prog
   in
      fprintf buf "@[<v 0>AML.IR Program:";

      fprintf buf "@ @[<hv 3>Boot:%a@]" pp_print_boot boot;

      fprintf buf "@ @[<hv 3>Types:";
      SymbolTable.iter (fun v tydef ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_tydef tydef) types;

      fprintf buf "@]@ @[<hv 3>Import:";
      SymbolTable.iter (fun me_var { import_type = ty } ->
            fprintf buf "@ @[<hv 3>import %a :@ %a@]" (**)
               pp_print_symbol me_var
               pp_print_type ty) import;

      fprintf buf "@]@ @[<hv 3>Export:";
      SymbolTable.iter (fun me_var { export_name = name; export_type = ty } ->
            fprintf buf "@ @[<hv 3>export %a = %a :@ %a@]" (**)
               pp_print_symbol name
               pp_print_symbol me_var
               pp_print_type ty) export;

      fprintf buf "@]@ @[<hv 3>Globals:";
      SymbolTable.iter (fun v (ty, init) ->
            fprintf buf "@ @[<hv 3>%a :@ %a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_type ty
               pp_print_init init) globals;

      fprintf buf "@]@ @[<hv 3>Tags:";
      SymbolTable.iter (fun v (ty_var, tyl) ->
            fprintf buf "@[<hv 3>%a : tag[%a]@,[@[<hv 0>%a@]]@]" (**)
               pp_print_symbol v
               pp_print_symbol ty_var
               (pp_print_type_params true) tyl) tags;

      fprintf buf "@]@ @[<hv 3>Functions:";
      SymbolTable.iter (fun f (_, gflag, ty_vars, ty, vars, e) ->
            fprintf buf "@ @[<hv 3>%a @[<hv 3>(%a :@ %a)@]@,@[<hv 3>[%a]@]@,@[<hv 3>(%a)@] =@ %a@]" (**)
               pp_print_gflag gflag
               pp_print_symbol f
               pp_print_type ty
               (pp_print_vars true) ty_vars
               (pp_print_vars true) vars
               (pp_print_exp true) e) funs;

      fprintf buf "@]@]"

(************************************************************************
 * VAR TYPES
 ************************************************************************)

(*
 * Print the name record.
 *)
let pp_print_module_names buf names =
   let { names_ty = ty_names;
         names_var = var_names
       } = names
   in
   let print_field_names s names =
      FieldTable.iter (fun ext_var v ->
            fprintf buf "@ export %s %a = %a" (**)
               s
               pp_print_symbol ext_var
               pp_print_symbol v) names
   in
   let print_var_names s names =
      SymbolTable.iter (fun ext_var v ->
            fprintf buf "@ export %s %a = %a" (**)
               s
               pp_print_symbol ext_var
               pp_print_symbol v) names
   in
      print_var_names "type" ty_names;
      print_field_names "val" var_names

(*
 * Print a module.
 *)
let rec pp_print_module_type buf mt =
   let { mt_types = types;
         mt_vars = vars;
         mt_names = names
       } = mt
   in
      fprintf buf "@[<hv 0>@[<hv 3>sig";
      SymbolTable.iter (fun v (vars, ty) ->
            fprintf buf "@ @[<hv 3>type %a[%a] =@ %a@]" (**)
               pp_print_symbol v
               (pp_print_type_vars true) vars
               Aml_tast_print.pp_print_type_opt ty) types;
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>val %a :@ %a@]" (**)
               pp_print_symbol v
               pp_print_var_type ty) vars;
      fprintf buf "%a@]@ end@]" pp_print_module_names names

(*
 * Print a var_type.
 *)
and pp_print_var_type buf ty =
   match ty with
      VarTypeNormal ty ->
         pp_print_type buf ty
    | VarTypeDelayed ty ->
         fprintf buf "delayed(%a)" Aml_tast_print.pp_print_type ty
    | VarTypeFun (vars, ty) ->
         fprintf buf "@[<hv 3>fun (%a) ->@ %a@]" (**)
            (pp_print_type_vars true) vars
            Aml_tast_print.pp_print_type_opt ty
    | VarTypeModule (ty, mt) ->
         fprintf buf "@[<hv 3>module :@ %a =@ %a@]" (**)
            pp_print_type ty
            pp_print_module_type mt
    | VarTypeFunctor (ty, v, mt1, mt2) ->
         fprintf buf "@[<hv 3>functor :@ %a@ (@[<hv 0>%a :@ %a@]) ->@ %a@]" (**)
            pp_print_type ty
            pp_print_symbol v
            pp_print_var_type mt1
            Aml_tast_print.pp_print_type mt2

(************************************************************************
 * CORE DEBUGGING
 ************************************************************************)

(*
 * Print a set of symbols.
 *)
let pp_print_symbol_set label buf s =
   if not (SymbolSet.is_empty s) then
      begin
         fprintf buf "@ @[<b 3>%s =" label;
         SymbolSet.iter (fun v ->
               fprintf buf "@ %a" pp_print_symbol v) s;
         fprintf buf "@]"
      end

(*
 * Print a substitution.
 *)
let rec pp_print_subst buf subst =
   let { sub_fo_vars = fo_vars;
         sub_loc     = loc
       } = subst
   in
      fprintf buf "@[<hv 0>@[<hv 3>subst {";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_type ty) fo_vars;
      fprintf buf "@]@ }@]"

(*
 * Print the free variables.
 *)
let pp_print_free_vars buf fv =
   let { fv_fo_vars = fo_vars;
         fv_vars = vars
       } = fv
   in
      fprintf buf "@[<hv 0>@[<hv 3>fv = {";
      pp_print_symbol_set "fo_vars" buf fo_vars;
      pp_print_symbol_set "vars" buf vars;
      fprintf buf "@]@ }@]"

(*
 * Debug printer for types.
 *)
let pp_print_type_ds =
   pp_print_term pp_print_free_vars pp_print_subst pp_print_type_core

(************************************************************************
 * GLOBALS
 ************************************************************************)

(*
 * Redirect the output.
 *)
let pp_print_type buf ty =
   pp_open_hvbox buf 0;
   pp_print_type buf ty;
   pp_close_box buf ()

let pp_print_exp buf e =
   pp_open_hvbox buf 0;
   pp_print_exp false buf e;
   pp_close_box buf ()

let pp_print_funs buf funs =
   pp_open_hvbox buf 0;
   pp_print_funs true buf funs;
   pp_close_box buf ()

let pp_print_fundef buf f (loc, gflag, ty, ty_vars, vars, e) =
   pp_print_funs buf [f, loc, gflag, ty, ty_vars, vars, e]

(*
 * Debugging.
 *)
let debug s f x =
   eprintf "@[<v 0>*** AML: IR: %s@ %a@]@." s f x

let debug_prog s prog =
   debug s pp_print_prog prog

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
