(*
 * Basic environment functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_env_type

module Pos = MakePos (struct let name = "Aml_ir_env_base" end)
open Pos

(************************************************************************
 * GENV
 ************************************************************************)

(*
 * Build an environment from a program.
 *)
let genv_of_prog prog =
   let { prog_types = types;
         prog_globals = globals;
         prog_tags = tags;
         prog_boot = boot
       } = prog
   in
      { genv_types = types;
        genv_ty_apply = Aml_tast_type.TyApplyTable.empty;
        genv_globals = globals;
        genv_tags = tags;
        genv_boot = boot
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
