(*
 * Basic inline expansion.
 * We inline functions only if they
 * are called exactly once.  We do some simple
 * intra-procedural constant folding.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Trace

open Aml_set

open Aml_ir
open Aml_ir_ds
open Aml_ir_env
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_type
open Aml_ir_print
open Aml_ir_standardize

module Pos = MakePos (struct let name = "Aml_ir_inline" end)
open Pos

let tabstop = 3

module IntCompare =
struct
   type t = int
   let compare = Pervasives.compare
end

module IntTable = Mc_map.McMake (IntCompare)

(************************************************************************
 * AVAILABLE EXPRESSIONS
 ************************************************************************)

type ('a, 'b) option2 =
   Some2 of 'a
 | Some1 of 'b
 | None0

(*
 * Simple expressions that a variable van refer to.
 *)
type avail =
   AvailVar of var * ty
 | AvailAtom of atom
 | AvailClosure of var * ty list * atom list
 | AvailFunction of var * var list * exp
 | AvailTuple of var * avail list
 | AvailArray of var * avail IntTable.t * avail option
 | AvailFrame of var * avail SymbolTable.t
 | AvailRecord of var * avail SymbolTable.t

type aenv = avail SymbolTable.t

let aenv_empty = SymbolTable.empty

let rec aenv_lookup aenv v =
   try
      match SymbolTable.find aenv v with
         AvailAtom (AtomVar v)
       | AvailVar (v, _) ->
            (* eprintf "aenv_lookup: %s%t" (string_of_symbol v) eflush; *)
            aenv_lookup aenv v
       | a ->
            a
   with
      Not_found ->
         AvailAtom (AtomVar v)

let rec aenv_lookup_atom aenv = function
   AtomVar v ->
      aenv_lookup aenv v
 | a ->
      AvailAtom a

let aenv_add aenv v a =
   SymbolTable.add aenv v a

let atom_of_avail = function
   AvailVar (v, _)
 | AvailFunction (v, _, _)
 | AvailClosure (v, _, _)
 | AvailTuple (v, _)
 | AvailArray (v, _, _)
 | AvailFrame (v, _)
 | AvailRecord (v, _) ->
      AtomVar v
 | AvailAtom a ->
      a

let rec type_of_var aenv pos v =
   try
      match SymbolTable.find aenv v with
         AvailAtom (AtomVar v) ->
            type_of_var aenv pos v
       | AvailVar (_, ty) ->
            ty
       | _ ->
            raise Not_found
   with
      Not_found ->
         raise (IRException (pos, StringVarError ("unknown type", v)))

let aenv_field_atom fields label =
   match List.assoc label fields with
      AvailAtom a -> a
    | _ -> raise Not_found

(************************************************************************
 * FUNCTION SIZING
 ************************************************************************)

(*
 * Measure the "size" of a function.
 *)
let is_small_max = 25

let rec size_exp fenv i e =
   if i >= is_small_max then
      i
   else
      match dest_exp_core e with
         LetFuns (funs, e) ->
            let i =
               List.fold_left (fun i (f, _, _, _, _, _, e) ->
                     let _, degree, _ = SymbolTable.find fenv f in
                        if degree > 1 then
                           is_small_max
                        else
                           size_exp fenv i e) i funs
            in
               size_exp fenv i e
       | LetRec (fields, e) ->
            List.fold_left (fun i (_, e, _) ->
                  size_exp fenv i e) (size_exp fenv i e) fields
       | LetClosure (_, _, _, _, e)
       | LetAtom (_, _, _, e)
       | LetAlloc (_, _, e) ->
            size_exp fenv (succ i) e
       | TailCall _
       | Raise _
       | Return _ ->
            succ i
       | Match (_, cases) ->
            List.fold_left (fun i (_, e) -> size_exp fenv (succ i) e) i cases
       | MatchDTuple (_, cases) ->
            List.fold_left (fun i (_, e) -> size_exp fenv (succ i) e) i cases
       | Try (e1, _, e2) ->
            size_exp fenv (size_exp fenv (succ i) e1) e2
       | LetExt (_, _, _, _, _, _, e)
       | LetApply (_, _, _, _, e) ->
            size_exp fenv (succ i) e
       | LetSubscript (_, _, _, _, e)
       | SetSubscript (_, _, _, _, e)
       | SetGlobal (_, _, _, e) ->
            size_exp fenv (i + 2) e

let is_small_exp fenv e =
   size_exp fenv 0 e < is_small_max

(************************************************************************
 * LOOP-NEST TREE
 ************************************************************************)

(*
 * This is the node we pass to the loop-nest algorithm.
 *)
type node =
   { node_name : var;
     node_succ : var list
   }

(*
 * Count number of uses of each var.
 *)
let count_empty = SymbolTable.empty

let count_lookup count v =
   try SymbolTable.find count v with
      Not_found ->
         0

let count_var count v =
   SymbolTable.add count v (succ (count_lookup count v))

let count_atom count a =
   match a with
      AtomVar v -> count_var count v
    | _ -> count

let count_atom_opt count a_opt =
   match a_opt with
      Some a -> count_atom count a
    | None -> count

let count_atoms count args =
   List.fold_left count_atom count args

(*
 * Allocation operator.
 *)
let count_alloc_op count op =
   match op with
      AllocTuple (_, _, args)
    | AllocUnion (_, _, _, _, args)
    | AllocArray (_, args) ->
         count_atoms count args
    | AllocVArray (_, a1, a2) ->
         count_atom (count_atom count a1) a2
    | AllocRecord (_, _, _, fields) ->
         List.fold_left (fun count (_, a) ->
               count_atom count a) count fields
    | AllocModule (_, _, fields) ->
         SymbolTable.fold (fun count _ a ->
               count_atom count a) count fields
    | AllocFrame _
    | AllocDTuple _ ->
         count

(*
 * Live variable set.
 *)
let live_empty = SymbolSet.empty

(*
 * Add a function occurrence.
 *)
let live_var live f =
   SymbolSet.add live f

let live_atom live = function
   AtomVar v -> live_var live v
 | _ -> live

let live_atom_opt live = function
   Some a -> live_atom live a
 | None -> live

let live_atoms live args =
   List.fold_left live_atom live args

let live_alloc_op live op =
   match op with
      AllocTuple (_, _, args)
    | AllocUnion (_, _, _, _, args)
    | AllocArray (_, args) ->
         live_atoms live args
    | AllocVArray (_, a1, a2) ->
         live_atom (live_atom live a1) a2
    | AllocRecord (_, _, _, fields) ->
         List.fold_left (fun live (_, a) ->
               live_atom live a) live fields
    | AllocModule (_, _, fields) ->
         SymbolTable.fold (fun live _ a ->
               live_atom live a) live fields
    | AllocFrame _
    | AllocDTuple _ ->
         live

(*
 * Function table keeps live set for each function
 *)
let fenv_empty = SymbolTable.empty

let fenv_add fenv f vars =
   SymbolTable.add fenv f vars

let fenv_mem fenv f =
   SymbolTable.mem fenv f

(*
 * Check if a node1 is an ancestor for node2.
 *)
let rec is_ancestor fenv pos name1 name2 =
   let pos = string_pos "is_ancestor" pos in
      if Symbol.eq name1 name2 then
         true
      else
         let name3, _, _ = SymbolTable.find fenv name2 in
            if Symbol.eq name3 name2 then
               false
            else
               is_ancestor fenv pos name1 name3

(*
 * Print ancestor list.
 *)
let rec pp_print_ancestors buf fenv (name, degree, loop) =
   pp_print_space buf ();
   pp_print_string buf ": ";
   pp_print_int buf degree;
   pp_print_string buf " ";
   pp_print_symbol buf name;
   if loop then
      pp_print_string buf " <loop-header>";
   let name', degree, loop = SymbolTable.find fenv name in
      if not (Symbol.eq name' name) then
         pp_print_ancestors buf fenv (name', degree, loop)

(*
 * Collect the number of uses of each variable.
 *)
let rec call_exp fenv count live e =
   match dest_exp_core e with
      LetFuns (funs, e) ->
         let fenv, count =
            List.fold_left (fun (fenv, count) (f, _, _, _, _, _, e) ->
                  let fenv, count, live = call_exp fenv count live_empty e in
                  let fenv = fenv_add fenv f live in
                     fenv, count) (fenv, count) funs
         in
            call_exp fenv count live e
    | LetRec (letrecs, e) ->
         let fenv, count, live =
            List.fold_left (fun (fenv, count, live) (_, e, _) ->
                  call_exp fenv count live e) (fenv, count, live) letrecs
         in
            call_exp fenv count live e
    | LetClosure (_, f, _, args, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atoms (count_atom count f) args in
         let live = live_atoms (live_atom live f) args in
            fenv, count, live
    | TailCall (f, args) ->
         let count = count_atoms (count_atom count f) args in
         let live = live_atoms (live_atom live f) args in
            fenv, count, live
    | LetAtom (_, _, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom count a in
         let live = live_atom live a in
            fenv, count, live
    | LetApply (_, _, f, args, e) ->
         let count = count_atoms (count_atom count f) args in
         let live = live_atoms (live_atom live f) args in
            call_exp fenv count live e
    | LetExt (_, _, _, _, _, args, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atoms count args in
         let live = live_atoms live args in
            fenv, count, live
    | LetAlloc (_, op, e) ->
         let count = count_alloc_op count op in
         let live = live_alloc_op live op in
            call_exp fenv count live e
    | LetSubscript (_, _, a1, a2, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom count a1) a2 in
         let live = live_atom (live_atom live a1) a2 in
            fenv, count, live
    | SetSubscript (a1, a2, a3, _, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_atom (count_atom count a1) a2) a3 in
         let live = live_atom (live_atom (live_atom live a1) a2) a3 in
            fenv, count, live
    | SetGlobal (v, _, a, e) ->
         let fenv, count, live = call_exp fenv count live e in
         let count = count_atom (count_var count v) a in
         let live = live_atom (live_var live v) a in
            fenv, count, live
    | Match (a, cases) ->
         let fenv, count, live =
            List.fold_left (fun (fenv, count, live) (_, e) ->
                  call_exp fenv count live e) (fenv, count, live) cases
         in
         let count = count_atom count a in
         let live = live_atom live a in
            fenv, count, live
    | MatchDTuple (a, cases) ->
         let fenv, count, live =
            List.fold_left (fun (fenv, count, live) (a_opt, e) ->
                  let count = count_atom_opt count a_opt in
                  let live = live_atom_opt live a_opt in
                     call_exp fenv count live e) (fenv, count, live) cases
         in
         let count = count_atom count a in
         let live = live_atom live a in
            fenv, count, live
    | Try (e1, _, e2) ->
         let fenv, count, live = call_exp fenv count live e1 in
            call_exp fenv count live e2
    | Raise (a, _)
    | Return a ->
         let count = count_atom count a in
         let live = live_atom live a in
            fenv, count, live

(*
 * Filter the live set so that it contains only the
 * function names.
 *)
let filter_live fenv live =
   SymbolSet.fold (fun live v ->
         if fenv_mem fenv v then
            v :: live
         else
            live) [] live

(*
 * Construct the call-graph.
 *)
let root_sym = new_symbol_string "root"

let call_prog prog =
   let { prog_funs = funs } = prog in

   (* Toplevel functions *)
   let fenv, count =
      SymbolTable.fold (fun (fenv, count) f (_, _, _, _, _, e) ->
            let fenv, count, live = call_exp fenv count live_empty e in
            let fenv = fenv_add fenv f live in
               fenv, count) (fenv_empty, count_empty) funs
   in

   (* Add successor nodes to all global functions *)
   let live =
      SymbolTable.fold (fun live f (_, gflag, _, _, _, _) ->
         match gflag with
            FunGlobalClass
          | FunTopLevelClass
          | FunPartialClass ->
               live_var live f
          | FunLocalClass
          | FunContClass ->
               live) live_empty funs
   in

   (* Create successor table by filtering out live vars that are functions *)
   let nodes =
      SymbolTable.fold (fun nodes f live ->
            let node = f, filter_live fenv live in
               node :: nodes) [] fenv
   in
   let root_node = root_sym, filter_live fenv live in

   (* Debugging *)
   let _ =
      if debug Fir_state.debug_print_ir then
         let buf = err_formatter in
            pp_open_vbox buf tabstop;
            pp_print_string buf "*** Call graph:";
            List.iter (fun (f, succ) ->
                  pp_print_space buf ();
                  pp_print_symbol buf f;
                  pp_print_string buf ":";
                  List.iter (fun v ->
                        pp_print_string buf " ";
                        pp_print_symbol buf v) succ) (root_node :: nodes);
            pp_close_box buf ();
            pp_print_newline buf ()
   in

   (* Build the dominator tree *)
   let loop = Loop.create "Fc_ir_inline" fst snd root_node nodes in
   let fenv = Loop.dominators loop fst in
   let nest = Loop.loop_nest loop fst in
   let head =
      List.fold_left (fun head (f, _) ->
            SymbolSet.add head f) SymbolSet.empty (Trace.special_nodes nest)
   in
   let fenv =
      SymbolTable.mapi (fun f v ->
            v, count_lookup count f, SymbolSet.mem head f) fenv
   in
   let _ =
      if debug Fir_state.debug_print_ir then
         let buf = err_formatter in
            pp_open_vbox buf tabstop;
            pp_print_string buf "*** Dominator indexes:";
            SymbolTable.iter (fun f name ->
                  pp_print_space buf ();
                  pp_print_symbol buf f;
                  pp_print_string buf ": ";
                  pp_print_ancestors buf fenv name) fenv;
            pp_close_box buf ();
            pp_print_newline buf ()
   in
      fenv

(*
 * Construct the initial available list for the functions.
 *)
let aenv_function fenv aenv f gflag vars e =
   let _, degree, loop = SymbolTable.find fenv f in
      if not loop && (degree = 1 || is_small_exp fenv e) then
         aenv_add aenv f (AvailFunction (f, vars, e))
      else
         begin
            if debug Fir_state.debug_print_ir then
               eprintf "aenv_function: rejected function %s because it is too big: %d, loop=%b@." (**)
                  (string_of_symbol f) (size_exp fenv 0 e) loop;
            aenv
         end

let avail_prog prog =
   let { prog_funs = funs;
         prog_globals = globals;
       } = prog
   in
   let fenv = call_prog prog in
   let aenv =
      SymbolTable.fold (fun aenv f (_, _, gflag, _, vars, e) ->
            aenv_function fenv aenv f gflag vars e) aenv_empty funs
   in
   let aenv =
      SymbolTable.fold (fun aenv v (_, init) ->
            match init with
               InitAtom a ->
                  aenv_add aenv v (AvailAtom a)
             | InitAlloc _
             | InitString _ ->
                  aenv) aenv globals
   in
      fenv, aenv

let aenv_bind_vars aenv pos vars args =
   let pos = string_pos "aenv_bind_vars" pos in
   let rec collect aenv vars' args' =
      match vars', args' with
         v :: vars', a :: args' ->
            collect (aenv_add aenv v (AvailAtom a)) vars' args'
       | [], [] ->
            aenv
       | _ ->
            raise (IRException (pos, ArityMismatch (List.length vars, List.length args)))
   in
      collect aenv vars args

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * We really just want to inline the vars in TyProject.
 *)
let rec inline_type aenv ty =
   let loc = loc_of_type ty in
   let ty = inline_type_core aenv (dest_type_core ty) in
      make_type loc ty

and inline_type_core aenv ty =
   match ty with
      TyVoid
    | TyInt
    | TyChar
    | TyString
    | TyFloat
    | TyVar _ ->
         ty
    | TyFun (ty_args, ty_res) ->
         TyFun (inline_types aenv ty_args, inline_type aenv ty_res)
    | TyTuple tyl ->
         TyTuple (inline_types aenv tyl)
    | TyArray ty ->
         TyArray (inline_type aenv ty)
    | TyAll (vars, ty) ->
         TyAll (vars, inline_type aenv ty)
    | TyExists (vars, ty) ->
         TyExists (vars, inline_type aenv ty)
    | TyProject (v, i) ->
         TyProject (inline_var aenv v, i)
    | TyApply (ty_var, tyl) ->
         TyApply (ty_var, inline_types aenv tyl)
    | TyUnion (ty_var, tyl, s) ->
         TyUnion (ty_var, inline_types aenv tyl, s)
    | TyRecord (ty_var, tyl) ->
         TyRecord (ty_var, inline_types aenv tyl)
    | TyFrame (ty_var, tyl) ->
         TyFrame (ty_var, inline_types aenv tyl)
    | TyModule (ty_var, tyl) ->
         TyModule (ty_var, inline_types aenv tyl)
    | TyTag (ty_var, tyl) ->
         TyTag (ty_var, inline_types aenv tyl)
    | TyDTuple (ty_var, tyl) ->
         TyDTuple (ty_var, inline_types_opt aenv tyl)
    | TyExternal (ty, s) ->
         TyExternal (inline_type aenv ty, s)
    | TyFormat (t1, t2, t3) ->
         TyFormat (inline_type aenv t1, inline_type aenv t2, inline_type aenv t3)

and inline_types aenv tyl =
   List.map (inline_type aenv) tyl

and inline_types_opt aenv tyl_opt =
   match tyl_opt with
      Some tyl -> Some (inline_types aenv tyl)
    | None -> None

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Make a boolean value.
 *)
and make_bool_atom genv loc b =
   if b then
      genv_true_atom genv loc
   else
      genv_false_atom genv loc

(*
 * Inline a var.
 *)
and inline_var aenv v =
   match aenv_lookup aenv v with
      AvailAtom (AtomVar v)
    | AvailVar (v, _)
    | AvailFunction (v, _, _)
    | AvailClosure (v, _, _)
    | AvailTuple (v, _)
    | AvailArray (v, _, _)
    | AvailFrame (v, _)
    | AvailRecord (v, _) ->
         v
    | AvailAtom _ ->
         v

(*
 * Inline an atom.
 *)
let rec inline_atom genv aenv pos loc a =
   let pos = string_pos "inline_atom" pos in
      match a with
         AtomInt _
       | AtomChar _
       | AtomFloat _
       | AtomNil _
       | AtomFrameLabel _
       | AtomRecordLabel _
       | AtomModuleLabel _
       | AtomConst _ ->
            a
       | AtomTyConstrain (a, ty) ->
            AtomTyConstrain (inline_atom genv aenv pos loc a, ty)
       | AtomTyApply (a, ty, tyl) ->
            AtomTyApply (inline_atom genv aenv pos loc a, ty, tyl)
       | AtomTyPack (v, ty, tyl) ->
            AtomTyPack (inline_var aenv v, ty, tyl)
       | AtomTyUnpack v ->
            AtomTyUnpack (inline_var aenv v)
       | AtomVar v ->
            (match aenv_lookup aenv v with
                AvailAtom a ->
                   a
              | AvailVar (v, _)
              | AvailFunction (v, _, _)
              | AvailClosure (v, _, _)
              | AvailRecord (v, _)
              | AvailTuple (v, _)
              | AvailArray (v, _, _)
              | AvailFrame (v, _) ->
                   AtomVar v)
       | AtomUnop (op, a) ->
            inline_unop_atom genv aenv pos loc op a
       | AtomBinop (op, a1, a2) ->
            inline_binop_atom genv aenv pos loc op a1 a2

(*
 * Inline unary operation.
 *)
and inline_unop_atom genv aenv pos loc op a =
   let a = inline_atom genv aenv pos loc a in
      match op, a with
         UMinusIntOp, AtomInt i ->
            AtomInt (-i)
       | NotIntOp, AtomInt i ->
            AtomInt (lnot i)
       | AbsIntOp, AtomInt i ->
            AtomInt (abs i)

       | UMinusFloatOp, AtomFloat x ->
            AtomFloat (-. x)
       | AbsFloatOp, AtomFloat x ->
            AtomFloat (abs_float x)
       | SqrtFloatOp, AtomFloat x ->
            AtomFloat (sqrt x)
       | ExpFloatOp, AtomFloat x ->
            AtomFloat (exp x)
       | LogFloatOp, AtomFloat x ->
            AtomFloat (log x)
       | Log10FloatOp, AtomFloat x ->
            AtomFloat (log10 x)
       | CosFloatOp, AtomFloat x ->
            AtomFloat (cos x)
       | SinFloatOp, AtomFloat x ->
            AtomFloat (sin x)
       | TanFloatOp, AtomFloat x ->
            AtomFloat (tan x)
       | ACosFloatOp, AtomFloat x ->
            AtomFloat (acos x)
       | ASinFloatOp, AtomFloat x ->
            AtomFloat (asin x)
       | ATanFloatOp, AtomFloat x ->
            AtomFloat (atan x)
       | CosHFloatOp, AtomFloat x ->
            AtomFloat (cosh x)
       | SinHFloatOp, AtomFloat x ->
            AtomFloat (sinh x)
       | TanHFloatOp, AtomFloat x ->
            AtomFloat (tanh x)

       | CharOfIntOp, AtomInt i ->
            AtomChar (Char.chr i)
       | IntOfCharOp, AtomChar c ->
            AtomInt (Char.code c)
       | IntOfFloatOp, AtomFloat x ->
            AtomInt (int_of_float x)
       | FloatOfIntOp, AtomInt i ->
            AtomFloat (float_of_int i)

       | _ ->
            (* None of the others get inlined *)
            AtomUnop (op, a)

(*
 * Binary operations.
 *)
and inline_binop_atom genv aenv pos loc op a1 a2 =
   let a1 = inline_atom genv aenv pos loc a1 in
   let a2 = inline_atom genv aenv pos loc a2 in
      match op, a1, a2 with
         EqCharOp, AtomChar c1, AtomChar c2 ->
            make_bool_atom genv loc (c1 = c2)
       | NeqCharOp, AtomChar c1, AtomChar c2 ->
            make_bool_atom genv loc (c1 <> c2)
       | LtCharOp, AtomChar c1, AtomChar c2 ->
            make_bool_atom genv loc (c1 < c2)
       | LeCharOp, AtomChar c1, AtomChar c2 ->
            make_bool_atom genv loc (c1 <= c2)
       | GtCharOp, AtomChar c1, AtomChar c2 ->
            make_bool_atom genv loc (c1 > c2)
       | GeCharOp, AtomChar c1, AtomChar c2 ->
            make_bool_atom genv loc (c1 >= c2)
       | CmpCharOp, AtomChar c1, AtomChar c2 ->
            AtomInt (Pervasives.compare c1 c2)

       | PlusIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 + i2)
       | MinusIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 - i2)
       | MulIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 * i2)
       | DivIntOp, AtomInt i1, AtomInt i2 when i2 <> 0 ->
            AtomInt (i1 / i2)
       | RemIntOp, AtomInt i1, AtomInt i2 when i2 <> 0 ->
            AtomInt (i1 mod i2)
       | LslIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lsl i2)
       | LsrIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lsr i2)
       | AsrIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 asr i2)
       | AndIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 land i2)
       | OrIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lor i2)
       | XorIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lxor i2)
       | MaxIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (max i1 i2)
       | MinIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (min i1 i2)

       | EqIntOp, AtomInt i1, AtomInt i2 ->
            make_bool_atom genv loc (i1 = i2)
       | NeqIntOp, AtomInt i1, AtomInt i2 ->
            make_bool_atom genv loc (i1 <> i2)
       | LtIntOp, AtomInt i1, AtomInt i2 ->
            make_bool_atom genv loc (i1 < i2)
       | LeIntOp, AtomInt i1, AtomInt i2 ->
            make_bool_atom genv loc (i1 <= i2)
       | GtIntOp, AtomInt i1, AtomInt i2 ->
            make_bool_atom genv loc (i1 > i2)
       | GeIntOp, AtomInt i1, AtomInt i2 ->
            make_bool_atom genv loc (i1 >= i2)
       | CmpIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (Pervasives.compare i1 i2)

       | PlusFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 +. x2)
       | MinusFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 -. x2)
       | MulFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 *. x2)
       | DivFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 /. x2)
       | RemFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (mod_float x1 x2)
       | MaxFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (max x1 x2)
       | MinFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (min x1 x2)
       | ATan2FloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (atan2 x1 x2)
       | LdExpFloatIntOp, AtomFloat x, AtomInt i ->
            AtomFloat (ldexp x i)

       | EqFloatOp, AtomFloat i1, AtomFloat i2 ->
            make_bool_atom genv loc (i1 = i2)
       | NeqFloatOp, AtomFloat i1, AtomFloat i2 ->
            make_bool_atom genv loc (i1 <> i2)
       | LtFloatOp, AtomFloat i1, AtomFloat i2 ->
            make_bool_atom genv loc (i1 < i2)
       | LeFloatOp, AtomFloat i1, AtomFloat i2 ->
            make_bool_atom genv loc (i1 <= i2)
       | GtFloatOp, AtomFloat i1, AtomFloat i2 ->
            make_bool_atom genv loc (i1 > i2)
       | GeFloatOp, AtomFloat i1, AtomFloat i2 ->
            make_bool_atom genv loc (i1 >= i2)
       | CmpFloatOp, AtomFloat i1, AtomFloat i2 ->
            AtomInt (Pervasives.compare i1 i2)

       | _ ->
            AtomBinop (op, a1, a2)

(*
 * Options and  lists.
 *)
let inline_atom_opt genv aenv pos loc a_opt =
   match a_opt with
      Some a -> Some (inline_atom genv aenv pos loc a)
    | None -> None

let inline_atoms genv aenv pos loc args =
   List.map (inline_atom genv aenv pos loc) args

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Perform inlining.
 *)
let rec inline_exp genv fenv aenv name e =
   let pos = string_pos "inline_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            inline_funs_exp genv fenv aenv name pos loc funs e
       | LetRec (fields, e) ->
            inline_letrec_exp genv fenv aenv name pos loc fields e
       | LetAtom (v, ty, a, e) ->
            inline_atom_exp genv fenv aenv name pos loc v ty a e
       | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
            inline_ext_exp genv fenv aenv name pos loc v ty1 s ty2 ty_args args e
       | LetClosure (v, f, ty_args, args, e) ->
            inline_closure_exp genv fenv aenv name pos loc v f ty_args args e
       | TailCall (f, args) ->
            inline_tailcall_exp genv fenv aenv name pos loc f args
       | LetAlloc (v, op, e) ->
            inline_alloc_exp genv fenv aenv name pos loc v op e
       | LetSubscript (v, ty, v2, a3, e) ->
            inline_subscript_exp genv fenv aenv name pos loc v ty v2 a3 e
       | SetSubscript (v1, a2, ty, a3, e) ->
            inline_set_subscript_exp genv fenv aenv name pos loc v1 a2 ty a3 e
       | SetGlobal (v, ty, a, e) ->
            inline_set_global_exp genv fenv aenv name pos loc v ty a e
       | Match (a, cases) ->
            inline_match_exp genv fenv aenv name pos loc a cases
       | MatchDTuple (a, cases) ->
            inline_match_dtuple_exp genv fenv aenv name pos loc a cases
       | LetApply (v, ty, f, args, e) ->
            inline_apply_exp genv fenv aenv name pos loc v ty f args e
       | Return a ->
            make_exp loc (Return (inline_atom genv aenv pos loc a))
       | Try (e1, v, e2) ->
            let e1 = inline_exp genv fenv aenv name e1 in
            let e2 = inline_exp genv fenv aenv name e2 in
               make_exp loc (Try (e1, v, e2))
       | Raise (a, ty) ->
            make_exp loc (Raise (inline_atom genv aenv pos loc a, ty))

(*
 * Add the function definitions.
 * Add the functions to the inline environment.
 *)
and inline_funs_exp genv fenv aenv name pos loc funs e =
   let pos = string_pos "inline_funs_exp" pos in
   let funs =
      List.fold_left (fun funs fund ->
            let f, loc, gflag, tvars, ty, vars, e = fund in
            let ty = inline_type aenv ty in
            let e = inline_exp genv fenv aenv f e in
            let fund = f, loc, gflag, tvars, ty, vars, e in
               fund :: funs) [] funs
   in
   let aenv =
      List.fold_left (fun aenv (f, _, gflag, _, _, vars, e) ->
            aenv_function fenv aenv f gflag vars e) aenv funs
   in
   let e = inline_exp genv fenv aenv name e in
      make_exp loc (LetFuns (List.rev funs, e))

(*
 * Recursive definitions.
 *)
and inline_letrec_exp genv fenv aenv name pos loc fields e =
   let pos = string_pos "inline_letrec_exp" pos in
   let fields =
      List.fold_left (fun fields (v, e, ty) ->
            let ty = inline_type aenv ty in
            let e = inline_exp genv fenv aenv name e in
               (v, e, ty) :: fields) [] fields
   in
   let e = inline_exp genv fenv aenv name e in
      make_exp loc (LetRec (List.rev fields, e))

(*
 * Operators.
 * Try to perform constant folding.
 *)
and inline_atom_exp genv fenv aenv name pos loc v ty a e =
   let pos = string_pos "inline_atom_exp" pos in
   let a = inline_atom genv aenv pos loc a in
   let aenv = aenv_add aenv v (AvailAtom a) in
   let e = inline_exp genv fenv aenv name e in
   let ty = inline_type aenv ty in
      make_exp loc (LetAtom (v, ty, a, e))

(*
 * Conditionals.
 * If the test can be matched, select that branch.
 *)
and inline_match_exp genv fenv aenv name pos loc a cases =
   let pos = string_pos "inline_match_exp" pos in
   let a = inline_atom genv aenv pos loc a in
      match a with
         AtomChar c ->
            let rec search cases =
               match cases with
                  (CharSet s, e) :: cases ->
                     if CharSet.mem_point c s then
                        inline_exp genv fenv aenv name e
                     else
                        search cases
                | _ ->
                     raise (IRException (pos, StringError "match is invalid"))
            in
               search cases
       | AtomInt i ->
            let rec search cases =
               match cases with
                  (IntSet s, e) :: cases ->
                     if IntSet.mem_point i s then
                        inline_exp genv fenv aenv name e
                     else
                        search cases
                | _ ->
                     raise (IRException (pos, StringError "match is invalid"))
            in
               search cases
       | _ ->
            let cases = List.map (fun (s, e) -> s, inline_exp genv fenv aenv name e) cases in
               make_exp loc (Match (a, cases))

(*
 * Inline a tuple match.
 * We can't ever inline the match.
 *)
and inline_match_dtuple_exp genv fenv aenv name pos loc a cases =
   let pos = string_pos "inline_match_exp" pos in
   let a = inline_atom genv aenv pos loc a in
   let cases =
      List.map (fun (a_opt, e) ->
          let a_opt = inline_atom_opt genv aenv pos loc a_opt in
          let e = inline_exp genv fenv aenv name e in
             a_opt, e) cases
   in
      make_exp loc (MatchDTuple (a, cases))

(*
 * External call.
 * Never inlined.
 *)
and inline_ext_exp genv fenv aenv name pos loc v ty1 s ty2 ty_args args e =
   let pos = string_pos "inline_ext_exp" pos in
   let args = inline_atoms genv aenv pos loc args in
   let v' = new_symbol v in
   let aenv = aenv_add aenv v (AvailVar (v', ty1)) in
   let e = inline_exp genv fenv aenv name e in
   let ty1 = inline_type aenv ty1 in
   let ty2 = inline_type aenv ty2 in
   let ty_args = inline_types aenv ty_args in
      make_exp loc (LetExt (v', ty1, s, ty2, ty_args, args, e))

(*
 * Inline the closure.
 * Add it to the available expressions.
 *)
and inline_closure_exp genv fenv aenv name pos loc v f ty_args args e =
   let pos = string_pos "inline_closure_exp" pos in
   let f = inline_atom genv aenv pos loc f in
   let args = inline_atoms genv aenv pos loc args in
   let aenv = aenv_add aenv v (AvailClosure (var_of_atom pos f, ty_args, args)) in
   let e = inline_exp genv fenv aenv name e in
      make_exp loc (LetClosure (v, f, ty_args, args, e))

(*
 * Inline the tailcall.
 * If the function is available, substitute and recurse.
 *)
and inline_tailcall_exp genv fenv aenv name pos loc f args =
   let pos = string_pos "inline_tailcall_normal" pos in
   let f = inline_atom genv aenv pos loc f in
   let args = inline_atoms genv aenv pos loc args in
   let f_var = var_of_atom pos f in
      if Symbol.eq f_var name || is_ancestor fenv pos f_var name then
         make_exp loc (TailCall (f, args))
      else
         match aenv_lookup aenv f_var with
            AvailFunction (f, vars, e) as avail ->
               if debug Fir_state.debug_print_ir then
                  eprintf "Inlining %s from %s@." (string_of_symbol f) (string_of_symbol name);
               let aenv = aenv_bind_vars aenv pos vars args in
                  inline_exp genv fenv aenv f e
          | AvailClosure (f, ty_args, args') ->
               inline_tailcall_exp genv fenv aenv name pos loc (AtomVar f) (args' @ args)
          | AvailAtom (AtomVar v) when not (Symbol.eq v f_var) ->
               inline_tailcall_exp genv fenv aenv name pos loc (AtomVar v) args
          | _ ->
               make_exp loc (TailCall (f, args))

(*
 * Normal function call.
 *)
and inline_apply_exp genv fenv aenv name pos loc v ty f args e =
   let pos = string_pos "inline_apply_exp" pos in
   let f = inline_atom genv aenv pos loc f in
   let args = inline_atoms genv aenv pos loc args in
   let v' = new_symbol v in
   let aenv = aenv_add aenv v (AvailVar (v', ty)) in
   let e = inline_exp genv fenv aenv name e in
   let ty = inline_type aenv ty in
      make_exp loc (LetApply (v', ty, f, args, e))

(*
 * Cancel addr-of and subscripting.
 *)
and inline_subscript_exp genv fenv aenv name pos loc v ty a2 a3 e =
   let pos = string_pos "inline_subscript_exp" pos in
   let a2 = inline_atom genv aenv pos loc a2 in
   let a3 = inline_atom genv aenv pos loc a3 in
   let ty = inline_type aenv ty in
   let e =
      match aenv_lookup_atom aenv a2, a3 with
         AvailTuple (v', fields), AtomInt i ->
            let avail = List.nth fields i in
            let v' = new_symbol v in
            let aenv = aenv_add aenv v avail in
            let a = atom_of_avail avail in
               LetAtom (v', ty, a, inline_exp genv fenv aenv name e)
       | AvailArray (v', fields, def), AtomInt i ->
            (try
                let avail = IntTable.find fields i in
                let v' = new_symbol v in
                let aenv = aenv_add aenv v avail in
                let a = atom_of_avail avail in
                   LetAtom (v', ty, a, inline_exp genv fenv aenv name e)
             with
                Not_found ->
                   match def with
                      Some avail ->
                         let v' = new_symbol v in
                         let aenv = aenv_add aenv v avail in
                         let a = atom_of_avail avail in
                            LetAtom (v', ty, a, inline_exp genv fenv aenv name e)
                    | None ->
                         let v' = new_symbol v in
                         let aenv = aenv_add aenv v (AvailVar (v', ty)) in
                            LetSubscript (v', ty, a2, a3, inline_exp genv fenv aenv name e))
       | AvailRecord (v', fields), AtomRecordLabel (_, label) ->
            let avail = SymbolTable.find fields label in
            let v' = new_symbol v in
            let aenv = aenv_add aenv v avail in
            let a = atom_of_avail avail in
               LetAtom (v', ty, a, inline_exp genv fenv aenv name e)
       | AvailFrame (v', fields), AtomFrameLabel (_, label) ->
            (try
                let avail = SymbolTable.find fields label in
                let v' = new_symbol v in
                let aenv = aenv_add aenv v avail in
                let a = atom_of_avail avail in
                   LetAtom (v', ty, a, inline_exp genv fenv aenv name e)
             with
                Not_found ->
                   let v' = new_symbol v in
                   let aenv = aenv_add aenv v (AvailVar (v', ty)) in
                      LetSubscript (v', ty, a2, a3, inline_exp genv fenv aenv name e))
       | _ ->
            let v' = new_symbol v in
            let aenv = aenv_add aenv v (AvailVar (v', ty)) in
               LetSubscript (v', ty, a2, a3, inline_exp genv fenv aenv name e)
   in
      make_exp loc e

and inline_set_subscript_exp genv fenv aenv name pos loc a1 a2 a3 ty e =
   let pos = string_pos "inline_set_subscript_exp" pos in
   let a1 = inline_atom genv aenv pos loc a1 in
   let a2 = inline_atom genv aenv pos loc a2 in
   let avail3 = aenv_lookup_atom aenv a3 in
   let a3 = inline_atom genv aenv pos loc a3 in
   let v1 = var_of_atom pos a1 in
   let ty = inline_type aenv ty in
   let aenv =
      match aenv_lookup aenv v1, a2 with
         AvailTuple (v', fields), AtomInt i ->
            let fields = Mc_list_util.replace_nth i avail3 fields in
               aenv_add aenv v1 (AvailTuple (v', fields))
       | AvailRecord (v', fields), AtomRecordLabel (_, label) ->
            let fields = SymbolTable.add fields label avail3 in
               aenv_add aenv v1 (AvailRecord (v', fields))
       | AvailFrame (v', fields), AtomFrameLabel (_, label) ->
            let fields = SymbolTable.add fields label avail3 in
               aenv_add aenv v1 (AvailFrame (v', fields))
       | AvailArray (v', fields, def), AtomInt i ->
            let fields = IntTable.add fields i avail3 in
               aenv_add aenv v1 (AvailArray (v', fields, def))
       | AvailArray (v', _, _), _ ->
            (* If we don't know the index, kill all the entries *)
            aenv_add aenv v1 (AvailArray (v', IntTable.empty, None))
       | _ ->
            aenv
   in
      make_exp loc (SetSubscript (a1, a2, a3, ty, inline_exp genv fenv aenv name e))

and inline_set_global_exp genv fenv aenv name pos loc v ty a e =
   let pos = string_pos "inline_set_global_exp" pos in
   let avail = aenv_lookup_atom aenv a in
   let a = inline_atom genv aenv pos loc a in
   let aenv = aenv_add aenv v avail in
   let ty = inline_type aenv ty in
      make_exp loc (SetGlobal (v, ty, a, inline_exp genv fenv aenv name e))

(*
 * Allocation.
 *)
and inline_alloc_exp genv fenv aenv name pos loc v op e =
   let pos = string_pos "inline_alloc_exp" pos in
   let v' = new_symbol v in
   let avail, op =
      match op with
         AllocTuple (ty_vars, ty, args) ->
            let ty = inline_type aenv ty in
            let avails = List.map (aenv_lookup_atom aenv) args in
            let args = List.map (inline_atom genv aenv pos loc) args in
               AvailTuple (v', avails), AllocTuple (ty_vars, ty, args)
       | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
            let ty = inline_type aenv ty in
            let args = List.map (inline_atom genv aenv pos loc) args in
               AvailVar (v', ty), AllocUnion (ty_vars, ty, ty_var, const_var, args)
       | AllocDTuple (ty, ty_var, tag_var, args) ->
            let ty = inline_type aenv ty in
            let args = List.map (inline_atom genv aenv pos loc) args in
               AvailVar (v', ty), AllocDTuple (ty, ty_var, tag_var, args)
       | AllocArray (ty, args) ->
            let ty = inline_type aenv ty in
            let avails, _ =
               List.fold_left (fun (avails, i) a ->
                     let avail = aenv_lookup_atom aenv a in
                     let avails = IntTable.add avails i avail in
                        avails, succ i) (IntTable.empty, 0) args
            in
            let args = List.map (inline_atom genv aenv pos loc) args in
               AvailArray (v', avails, None), AllocArray (ty, args)
       | AllocVArray (ty, a1, a2) ->
            let ty = inline_type aenv ty in
            let a1 = inline_atom genv aenv pos loc a1 in
            let avail2 = aenv_lookup_atom aenv a2 in
            let a2 = inline_atom genv aenv pos loc a2 in
               AvailArray (v', IntTable.empty, Some avail2), AllocVArray (ty, a1, a2)
       | AllocRecord (ty_vars, ty, ty_var, fields) ->
            let ty = inline_type aenv ty in
            let avails =
               List.fold_left (fun avails (label, a) ->
                     SymbolTable.add avails label (aenv_lookup_atom aenv a)) SymbolTable.empty fields
            in
            let fields = List.map (fun (label, a) -> label, inline_atom genv aenv pos loc a) fields in
               AvailRecord (v', avails), AllocRecord (ty_vars, ty, ty_var, fields)
       | AllocModule (ty, ty_var, fields) ->
            let ty = inline_type aenv ty in
            let fields = SymbolTable.map (inline_atom genv aenv pos loc) fields in
               AvailVar (v', ty), AllocModule (ty, ty_var, fields)
       | AllocFrame _ ->
            AvailFrame (v', SymbolTable.empty), op
   in
   let aenv = aenv_add aenv v avail in
      make_exp loc (LetAlloc (v', op, inline_exp genv fenv aenv name e))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Inline the entire program.
 *)
let inline_prog prog =
   let { prog_types = tenv;
         prog_funs = funs
       } = prog
   in
   let genv = genv_of_prog prog in
   let fenv, aenv = avail_prog prog in

   (*
    * Inline each of the funs.
    *)
   let funs =
      SymbolTable.mapi (fun f (loc, gflag, ty, tvars, vars, e) ->
            loc, gflag, ty, tvars, vars, inline_exp genv fenv aenv f e) funs
   in
      (* Replace functions *)
      { prog with prog_funs = funs }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
