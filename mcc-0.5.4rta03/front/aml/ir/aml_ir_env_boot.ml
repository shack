(*
 * Add some "boot" types to the program,
 * including unit, Boolean.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_env_type

module Pos = MakePos (struct let name = "Aml_ir_env_boot" end)
open Pos

(*
 * Get the unit and bool types.
 *)
let genv_ty_unit genv loc =
   make_type loc genv.genv_boot.boot_ty_unit

let genv_unit_atom genv loc =
   let ty_unit = genv_ty_unit genv loc in
      AtomConst (ty_unit, genv.genv_boot.boot_ty_unit_var, genv.genv_boot.boot_unit_var)

let genv_ty_bool genv loc =
   make_type loc genv.genv_boot.boot_ty_bool

let genv_true_set genv =
   genv.genv_boot.boot_true_set

let genv_false_set genv =
   genv.genv_boot.boot_false_set

let genv_true_atom genv loc =
   let ty_bool = genv_ty_bool genv loc in
      AtomConst (ty_bool, genv.genv_boot.boot_ty_bool_var, genv.genv_boot.boot_true_var)

let genv_false_atom genv loc =
   let ty_bool = genv_ty_bool genv loc in
      AtomConst (ty_bool, genv.genv_boot.boot_ty_bool_var, genv.genv_boot.boot_false_var)

let genv_ty_exn genv loc =
   make_type loc genv.genv_boot.boot_ty_exn

let genv_exn_var genv =
   genv.genv_boot.boot_ty_exn_var

let genv_match_failure_var genv =
   genv.genv_boot.boot_match_failure_var

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
