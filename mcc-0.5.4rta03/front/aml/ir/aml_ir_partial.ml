(*
 * Partial-application elimination.
 *
 * The input to this stage is an IR program where the function arities
 * are completely uncorrelated.  For example, ((int, int, int) -> int) =
 * ((int) -> ((int) -> ((int) -> int))).  What we want is a program
 * where function arities are exact: if a function type has n arguments,
 * the value of that type is a real function expecting exactly n
 * arguments.
 *
 * To do this, we basically punt, and each escaping function is
 * curried: it takes one argument at a time.  We convert all the types
 * in the program so that higher-order function types are curried;
 * we convert applications so that they apply the right number of
 * arguments; and we build curry functions to convert normal
 * functions into curried ones.
 *
 * Note, we could be smarter than this.  One method would be to
 * look at the function body and see how functions are applied.
 * However, this gets harder to do when we import some of the
 * functions.  All of those will have to be curried anyway.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_type

module Pos = MakePos (struct let name = "Aml_ir_partial" end)
open Pos

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * The environment keeps track of the names of the curried functions.
 *)
type cenv = var SymbolTable.t

(*
 * Empty environment.
 *)
let cenv_empty = SymbolTable.empty

(*
 * Lookup a curry function based on its index.
 *)
let cenv_add_fun = SymbolTable.add

(*
 * Test for membership.
 *)
let cenv_mem_fun = SymbolTable.mem

(*
 * Lookup a fun, don't trap Not_found.
 *)
let cenv_lookup_fun = SymbolTable.find

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Curry a type.
 *)
let rec curry_type_esc ty =
   let pos = string_pos "curry_type_esc" (type_exp_pos ty) in
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyVoid
       | TyInt
       | TyChar
       | TyString
       | TyFloat
       | TyVar _
       | TyProject _ ->
            ty

       | TyFun ([], ty) ->
            make_type loc (TyFun ([], curry_type_esc ty))
       | TyFun (ty_args, ty_res) ->
            curry_fun_type loc ty_args ty_res

       | TyTuple tyl ->
            make_type loc (TyTuple (curry_types_esc tyl))
       | TyArray ty ->
            make_type loc (TyArray (curry_type_esc ty))

       | TyAll (vars, ty) ->
            make_type loc (TyAll (vars, curry_type_esc ty))
       | TyExists (vars, ty) ->
            make_type loc (TyExists (vars, curry_type_esc ty))

       | TyApply (ty_var, tyl) ->
            make_type loc (TyApply (ty_var, curry_types_esc tyl))
       | TyUnion (ty_var, tyl, s) ->
            make_type loc (TyUnion (ty_var, curry_types_esc tyl, s))
       | TyRecord (ty_var, tyl) ->
            make_type loc (TyRecord (ty_var, curry_types_esc tyl))
       | TyFrame (ty_var, tyl) ->
            make_type loc (TyFrame (ty_var, curry_types_esc tyl))
       | TyModule (ty_var, tyl) ->
            make_type loc (TyModule (ty_var, curry_types_esc tyl))
       | TyTag (ty_var, tyl) ->
            make_type loc (TyTag (ty_var, curry_types_esc tyl))
       | TyDTuple (ty_var, tyl_opt) ->
            make_type loc (TyDTuple (ty_var, curry_types_opt_esc tyl_opt))

       | TyExternal (ty, s) ->
            make_type loc (TyExternal (curry_type_esc ty, s))
       | TyFormat (ty1, ty2, ty3) ->
            make_type loc (TyFormat (curry_type_esc ty1, curry_type_esc ty2, curry_type_esc ty3))

and curry_fun_type loc ty_args ty_res =
   let ty_res = curry_type_esc ty_res in
      List.fold_right (fun ty_arg ty_res ->
            make_type loc (TyFun ([curry_type_esc ty_arg], ty_res))) ty_args ty_res

and curry_types_esc tyl =
   List.map curry_type_esc tyl

and curry_types_opt_esc tyl_opt =
   match tyl_opt with
      Some tyl -> Some (curry_types_esc tyl)
    | None -> None

(*
 * For a non-escaping function, do not curry the outermost
 * function type.
 *)
let rec curry_type ty =
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyAll (vars, ty) ->
            make_type loc (TyAll (vars, curry_type ty))
       | TyFun (ty_args, ty_res) ->
            make_type loc (TyFun (curry_types_esc ty_args, curry_type_esc ty_res))
       | TyExternal (ty, _) ->
            curry_type ty
       | _ ->
            curry_type_esc ty

let curry_types tyl =
   List.map curry_type tyl

(*
 * Curry a typedef.
 *)
let curry_record_tydef fields =
   FieldTable.map (fun (mflag, ty) ->
         mflag, curry_type_esc ty) fields

let curry_union_tydef fields =
   FieldTable.map curry_types_esc fields

let curry_module_tydef names fields =
   let fields = SymbolTable.map curry_type_esc fields in
      names, fields

let curry_tydef tydef =
   let { tydef_inner = inner } = tydef in
   let loc = loc_of_tydef_inner inner in
   let inner =
      match dest_tydef_inner_core inner with
         TyDefLambda ty ->
            TyDefLambda (curry_type_esc ty)
       | TyDefUnion fields ->
            TyDefUnion (curry_union_tydef fields)
       | TyDefRecord fields ->
            TyDefRecord (curry_record_tydef fields)
       | TyDefFrame fields ->
            TyDefFrame (curry_record_tydef fields)
       | TyDefModule (names, fields) ->
            let names, fields = curry_module_tydef names fields in
               TyDefModule (names, fields)
       | TyDefDTuple ty_var ->
            TyDefDTuple ty_var
   in
      { tydef with tydef_inner = make_tydef_inner loc inner }

(************************************************************************
 * FUNCTION CURRYING
 ************************************************************************)

(*
 * Build a curry function.
 * This is what the function looks like for n=3:
 *
 * let f_curry3_1 arg1 =
 *    let f_curry3_2 arg2 =
 *       let f_curry3_3 arg3 =
 *          let v = f(arg1, arg2, arg3) in
 *             return v
 *       in
 *          return f_curry3_3
 *    in
 *       return f_curry3_2
 *)
let build_curry_fun genv venv cenv pos loc f ty_vars ty_fun vars =
   let pos = string_pos "build_curry_fun" pos in
   let ty_args, ty_res = dest_fun_type genv pos ty_fun in
   let s = Symbol.to_string f in
   let f_curry = cenv_lookup_fun cenv f in
   let len1 = List.length ty_args in
   let len2 = List.length vars in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, TypeArityMismatch (ty_fun, len1, len2)))
   in

   (* Build a curried function type *)
   let build_fun_type ty_args ty_res =
         List.fold_right (fun ty_arg ty_res ->
               make_type loc (TyFun ([ty_arg], ty_res))) ty_args ty_res
   in

   (* Build the expression *)
   let rec build vars_list ty_args_list i =
         match vars_list, ty_args_list with
            v :: vars, ty_arg :: ty_args ->
               (* Build the function that takes the next argument *)
               let ty_fun = build_fun_type ty_args_list ty_res in
               let body = build vars ty_args (succ i) in
               let f_curry_var = new_symbol_string (Printf.sprintf "%s.curry%d" s i) in
               let f_atom = AtomVar f_curry_var in
                  make_exp loc (LetFuns ([f_curry_var, loc, FunGlobalClass, [], ty_fun, [v], body],
                  make_exp loc (Return f_atom)))

          | [], [] ->
               (* The final block just applies the function *)
               let v = new_symbol_string "res" in
               let args = List.map (fun v -> AtomVar v) vars in
               let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
                  make_exp loc (LetApply (v, ty_res, AtomVar f, args,
                  make_exp loc (Return (AtomVar v))))
          | _ ->
               raise (Invalid_argument "Aml_ir_partial.build_curry_fun.build")
   in
   let ty_fun = build_fun_type ty_args ty_res in
      match vars, ty_args with
         v :: vars, ty_arg :: ty_args ->
            (* Build the function that takes the next argument *)
            let f_curry_var = cenv_lookup_fun cenv f in
            let body = build vars ty_args 2 in
               ((f_curry_var, loc, FunGlobalClass, ty_vars, ty_fun, [v], body) : fundef)
       | _ ->
            raise (Invalid_argument "Aml_ir_partial.build_curry_fun")

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * This variable is about to escape.
 * If it is a function, use the curried version.
 *)
let coerce_var_esc cenv v =
   try cenv_lookup_fun cenv v with
      Not_found ->
         v

(*
 * Check if an atom is escaped.
 *)
let rec is_atom_esc cenv a =
   match a with
      AtomVar v ->
         cenv_mem_fun cenv v
    | AtomTyConstrain (a, _)
    | AtomTyApply (a, _, _)
    | AtomUnop (_, a) ->
         is_atom_esc cenv a
    | _ ->
         false

(*
 * Coerce an atom that is about to escape.
 * All we really care about are functions.
 *)
let rec coerce_atom_esc cenv a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _ ->
         a
    | AtomVar v ->
         AtomVar (coerce_var_esc cenv v)
    | AtomNil ty ->
         AtomNil (curry_type_esc ty)
    | AtomTyConstrain (a, ty) ->
         AtomTyConstrain (coerce_atom_esc cenv a, curry_type_esc ty)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (coerce_atom_esc cenv a, curry_type_esc ty, curry_types_esc tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (coerce_var_esc cenv v, curry_type_esc ty, curry_types_esc tyl)
    | AtomTyUnpack v ->
         AtomTyUnpack (coerce_var_esc cenv v)
    | AtomConst (ty, ty_var, const_var) ->
         AtomConst (curry_type_esc ty, ty_var, const_var)
    | AtomUnop (op, a) ->
         AtomUnop (op, coerce_atom_esc cenv a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, coerce_atom_esc cenv a1, coerce_atom_esc cenv a2)

let coerce_atom_esc_list cenv atoms =
   List.map (coerce_atom_esc cenv) atoms

(*
 * Coerce an atom this is not escaping.
 * Type arguments are always curried.
 *)
let rec coerce_atom cenv a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomVar _
    | AtomTyUnpack _ ->
         a
    | AtomNil ty ->
         AtomNil (curry_type ty)
    | AtomTyConstrain (a, ty) ->
         AtomTyConstrain (coerce_atom cenv a, curry_type ty)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (coerce_atom cenv a, curry_type ty, curry_types_esc tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, curry_type ty, curry_types_esc tyl)
    | AtomConst (ty, ty_var, const_var) ->
         AtomConst (curry_type_esc ty, ty_var, const_var)
    | AtomUnop (op, a) ->
         AtomUnop (op, coerce_atom_esc cenv a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, coerce_atom_esc cenv a1, coerce_atom_esc cenv a2)

let coerce_atom_opt cenv a_opt =
   match a_opt with
      Some a -> Some (coerce_atom cenv a)
    | None -> None

let coerce_atom_list cenv atoms =
   List.map (coerce_atom cenv) atoms

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Coerce an allocation.
 * All the vars in the block escape.
 *)
let coerce_alloc_op cenv op =
   match op with
      AllocTuple (ty_vars, ty, args) ->
         AllocTuple (ty_vars, curry_type_esc ty, coerce_atom_esc_list cenv args)
    | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
         AllocUnion (ty_vars, curry_type_esc ty, ty_var, const_var, coerce_atom_esc_list cenv args)
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         let fields = List.map (fun (label, a) -> label, coerce_atom_esc cenv a) fields in
            AllocRecord (ty_vars, curry_type_esc ty, ty_var, fields)
    | AllocDTuple (ty, ty_var, tag_var, args) ->
         AllocDTuple (curry_type_esc ty, ty_var, tag_var, coerce_atom_esc_list cenv args)
    | AllocArray (ty, args) ->
         AllocArray (curry_type_esc ty, coerce_atom_esc_list cenv args)
    | AllocVArray (ty, a1, a2) ->
         AllocVArray (curry_type_esc ty, coerce_atom_esc cenv a1, coerce_atom_esc cenv a2)
    | AllocModule (ty, ty_var, fields) ->
         let fields = SymbolTable.map (coerce_atom_esc cenv) fields in
            AllocModule (curry_type_esc ty, ty_var, fields)
    | AllocFrame (ty, ty_var, tyl) ->
         AllocFrame (curry_type_esc ty, ty_var, curry_types_esc tyl)

(*
 * Now coerce an expression.
 *)
let rec coerce_exp genv venv cenv e =
   let pos = string_pos "coerce_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            let e =
               if is_atom_esc cenv a then
                  (* Add the escaped version *)
                  let ty = curry_type_esc ty in
                  let a = coerce_atom_esc cenv a in
                  let v' = new_symbol_string (Symbol.to_string v ^ "_curry") in
                  let venv = venv_add_var venv v' ty in
                  let cenv = cenv_add_fun cenv v v' in
                  let e = coerce_exp genv venv cenv e in
                     make_exp loc (LetAtom (v', ty, a, e))
               else
                  coerce_exp genv venv cenv e
            in
            let ty = curry_type ty in
            let a = coerce_atom cenv a in
            let venv = venv_add_var venv v ty in
               make_exp loc (LetAtom (v, ty, a, e))
       | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
            let ty1 = curry_type_esc ty1 in
            let ty2 = curry_type ty2 in
            let ty_args = curry_types_esc ty_args in
            let args = coerce_atom_esc_list cenv args in
            let venv = venv_add_var venv v ty1 in
            let e = coerce_exp genv venv cenv e in
               make_exp loc (LetExt (v, ty1, s, ty2, ty_args, args, e))
       | TailCall (a, args) ->
            let a = coerce_atom cenv a in
            let args = coerce_atom_list cenv args in
               make_exp loc (TailCall (a, args))
       | Match (a, cases) ->
            let a = coerce_atom cenv a in
            let cases = List.map (fun (s, e) -> s, coerce_exp genv venv cenv e) cases in
               make_exp loc (Match (a, cases))
       | MatchDTuple (a, cases) ->
            let a = coerce_atom cenv a in
            let cases = List.map (fun (a_opt, e) -> coerce_atom_opt cenv a_opt, coerce_exp genv venv cenv e) cases in
               make_exp loc (MatchDTuple (a, cases))
       | LetAlloc (v, op, e) ->
            let op = coerce_alloc_op cenv op in
            let venv = venv_add_var venv v (type_of_alloc_op op) in
            let e = coerce_exp genv venv cenv e in
               make_exp loc (LetAlloc (v, op, e))
       | SetSubscript (a1, a2, a3, ty, e) ->
            (* This value escapes *)
            let a1 = coerce_atom cenv a1 in
            let a2 = coerce_atom cenv a2 in
            let a3 = coerce_atom_esc cenv a3 in
            let ty = curry_type_esc ty in
            let e = coerce_exp genv venv cenv e in
               make_exp loc (SetSubscript (a1, a2, a3, ty, e))
       | LetSubscript (v, ty, a1, a2, e) ->
            (* This value escapes *)
            let ty = curry_type_esc ty in
            let a1 = coerce_atom cenv a1 in
            let a2 = coerce_atom cenv a2 in
            let venv = venv_add_var venv v ty in
            let e = coerce_exp genv venv cenv e in
               make_exp loc (LetSubscript (v, ty, a1, a2, e))
       | SetGlobal (v, ty, a, e) ->
            (* This value escapes *)
            let ty = curry_type_esc ty in
            let a = coerce_atom_esc cenv a in
            let e = coerce_exp genv venv cenv e in
               make_exp loc (SetGlobal (v, ty, a, e))
       | LetRec (fields, e) ->
            let fields = List.map (fun (v, e, ty) -> v, coerce_exp genv venv cenv e, curry_type ty) fields in
            let e = coerce_exp genv venv cenv e in
               make_exp loc (LetRec (fields, e))
       | LetFuns (funs, e) ->
            (* Add curried functions to the list *)
            coerce_funs_exp genv venv cenv pos loc funs e
       | LetApply (v, ty, a, args, e) ->
            (* Partial applications use the curried form *)
            coerce_apply_exp genv venv cenv pos loc v ty a args e
       | Return a ->
            (* This value escapes *)
            let a = coerce_atom_esc cenv a in
               make_exp loc (Return a)
       | Try (e1, v, e2) ->
            let e1 = coerce_exp genv venv cenv e1 in
            let e2 = coerce_exp genv venv cenv e2 in
               make_exp loc (Try (e1, v, e2))
       | Raise (a, ty) ->
            (* This value escapes *)
            let a = coerce_atom_esc cenv a in
            let ty = curry_type_esc ty in
               make_exp loc (Raise (a, ty))
       | LetClosure _ ->
            raise (IRException (pos, InternalError))

(*
 * For each global function, build a curried form
 * and add the name to the curry environment.
 *)
and coerce_funs_exp genv venv cenv pos loc funs e =
   let pos = string_pos "coerce_funs_exp" pos in

   (* Build the new cenv *)
   let cenv =
      List.fold_left (fun cenv (f, _, gflag, _, _, _, _) ->
            match gflag with
               FunGlobalClass
             | FunTopLevelClass
             | FunPartialClass ->
                  let f_curry = new_symbol_string (Symbol.to_string f ^ "_curry") in
                     cenv_add_fun cenv f f_curry
             | FunLocalClass
             | FunContClass ->
                  cenv) cenv funs
   in

   (* Convert all the function types *)
   let funs =
      List.map (fun (f, loc, gflag, ty_vars, ty_fun, vars, e) ->
            let ty_fun = curry_type ty_fun in
               f, loc, gflag, ty_vars, ty_fun, vars, e) funs
   in

   (* Add to the variable environment *)
   let venv =
      List.fold_left (fun venv (f, loc, _, ty_vars, ty, _, _) ->
            venv_add_var venv f (make_type loc (TyAll (ty_vars, ty)))) venv funs
   in

   (* Convert all the funs *)
   let funs =
      List.map (fun (f, loc, gflag, ty_vars, ty_fun, vars, e) ->
            let e = coerce_exp genv venv cenv e in
               f, loc, gflag, ty_vars, ty_fun, vars, e) funs
   in

   (* Add all the curried versions *)
   let cfuns =
      List.fold_left (fun cfuns (f, loc, gflag, ty_vars, ty_fun, vars, _) ->
            match gflag with
               FunGlobalClass
             | FunTopLevelClass
             | FunPartialClass ->
                  let fundef = build_curry_fun genv venv cenv pos loc f ty_vars ty_fun vars in
                     fundef :: cfuns
             | FunLocalClass
             | FunContClass ->
                  cfuns) [] funs
   in

   (* Convert the body *)
   let e = coerce_exp genv venv cenv e in
   let e =
      if cfuns <> [] then
         make_exp loc (LetFuns (cfuns, e))
      else
         e
   in
   let e = make_exp loc (LetFuns (funs, e)) in
      e

(*
 * Apply a function.  There are several cases to consider.
 *
 * If this is a partial application, use the curried function,
 * and apply one argument at a time.
 *
 * If this application has too many arguments, apply only as
 * many as the function will take.
 *)
and coerce_apply_exp genv venv cenv pos loc v ty f args e =
   let pos = string_pos "coerce_apply_exp" pos in
   let ty = curry_type_esc ty in
   let venv = venv_add_var venv v ty in
   let e = coerce_exp genv venv cenv e in

   (* All the arguments escape *)
   let args = coerce_atom_esc_list cenv args in

   (* The function doesn't escape *)
   let f = coerce_atom cenv f in

   (* See how many arguments the function wants *)
   let ty_fun = type_of_atom genv venv pos loc f in
   let ty_args, ty_res = dest_fun_type genv pos ty_fun in
   let len1 = List.length ty_args in
   let len2 = List.length args in
      if len1 = len2 then
         make_exp loc (LetApply (v, ty, f, args, e))
      else if len1 < len2 then
         coerce_apply_too_many genv venv cenv pos loc v ty f len1 ty_args ty_res args e
      else
         coerce_apply_partial genv venv cenv pos loc v ty f len1 ty_args ty_res args e

(*
 * We have too many arguments.
 * Apply as many as possible at once, then recurse.
 *)
and coerce_apply_too_many genv venv cenv pos loc v ty f len ty_args ty_res args e =
   let pos = string_pos "coerce_apply_too_many" pos in
   let args1, args2 = Mc_list_util.split len args in
   let ty_args1, ty_args2 = Mc_list_util.split len ty_args in
   let ty_fun = make_type loc (TyFun (ty_args1, make_type loc (TyFun (ty_args2, ty_res)))) in

   (* Get a name for the new function *)
   let f' = new_symbol_string "partial" in
   let venv = venv_add_var venv f' ty_fun in

   (* Build the expression *)
   let e = coerce_apply_exp genv venv cenv pos loc v ty (AtomVar f') args2 e in
      make_exp loc (LetApply (f', ty_fun, f, args1, e))

(*
 * We have too few arguments.
 * Define v as a function that will take the rest of the
 * arguments and apply f to them.
 *)
and coerce_apply_partial genv venv cenv pos loc v ty f len ty_args ty_res args1 e =
   let pos = string_pos "coerce_apply_partial" pos in
   let ty_args2 = Mc_list_util.nth_tl len ty_args in
   let vars2 = List.map (fun _ -> new_symbol_string "arg") ty_args2 in
   let args2 = List.map (fun v -> AtomVar v) vars2 in

   (* Define v as a new function *)
   let body =
      make_exp loc (LetApply (v, ty_res, f, args1 @ args2,
      make_exp loc (Return (AtomVar v))))
   in
      make_exp loc (LetFuns ([v, loc, FunGlobalClass, [], ty, vars2, body], e))

(************************************************************************
 * CONVERT THE PROGRAM
 ************************************************************************)

(*
 * Initializer.
 *)
let coerce_init cenv init =
   match init with
      InitString _ ->
         init
    | InitAtom a ->
         InitAtom (coerce_atom cenv a)
    | InitAlloc op ->
         InitAlloc (coerce_alloc_op cenv op)

(*
 * Convert partial applications in the program.
 *)
let partial_prog prog =
   let { prog_types = types;
         prog_globals = globals;
         prog_tags = tags;
         prog_import = import;
         prog_export = export;
         prog_funs = funs
       } = prog
   in

   (* Convert the outermost types *)
   let types = SymbolTable.map curry_tydef types in
   let globals =
      SymbolTable.map (fun (ty, init) ->
            curry_type_esc ty, init) globals
   in
   let tags =
      SymbolTable.map (fun (ty_var, tyl) ->
            ty_var, curry_types_esc tyl) tags
   in
   let import =
      SymbolTable.map (fun import ->
            { import with import_type = curry_type_esc import.import_type }) import
   in
   let export =
      SymbolTable.map (fun export ->
            { export with export_type = curry_type_esc export.export_type }) export
   in
   let funs =
      SymbolTable.map (fun (loc, gflag, ty_vars, ty, vars, e) ->
            loc, gflag, ty_vars, curry_type ty, vars, e) funs
   in

   (* Rebuild the prog *)
   let prog =
      { prog with prog_types = types;
                  prog_globals = globals;
                  prog_tags = tags;
                  prog_import = import;
                  prog_export = export;
                  prog_funs = funs
      }
   in

   (* Build environments *)
   let genv = genv_of_prog prog in
   let venv = venv_of_prog prog in
   let cenv = SymbolTable.empty in

   (* Convert the function bodies *)
   let funs =
      SymbolTable.map (fun (loc, gflag, ty, ty_vars, vars, e) ->
            loc, gflag, ty, ty_vars, vars, coerce_exp genv venv cenv e) funs
   in

   (* Now convert the parts *)
   let globals =
      SymbolTable.mapi (fun v (ty, init) ->
            let init = coerce_init cenv init in
               ty, init) globals
   in

   (* Rebuild the prog *)
   let prog =
      { prog with prog_globals = globals;
                  prog_funs = funs
      }
   in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
