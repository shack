(*
 * Add to the environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_ir
open Aml_ir_pos
open Aml_ir_env_type

(*
 * Add values to the variable environment.
 *)
val venv_add_var    : venv -> var -> ty -> venv
val venv_add_vars   : venv -> pos -> var list -> ty list -> venv

(*
 * Add values to the type environments.
 *)
val tenv_add_var    : tenv -> var -> var_type -> tenv
val tenv_add_fo_var : tenv -> ty_var -> var_type -> tenv
val tenv_add_so_var : tenv -> ty_var -> ty_var list -> Aml_tast.ty option -> tenv

(*
 * Add value lists to the type environment.
 *)
val tenv_add_vars    : tenv -> pos -> var list -> var_type list -> tenv
val tenv_add_fo_vars : tenv -> pos -> ty_var list -> var_type list -> tenv
val tenv_add_so_vars : tenv -> pos -> ty_var list -> tyfun list -> tenv

(*
 * Add the names record.
 *)
val tenv_add_names   : tenv -> mt_names -> tenv

(*
 * Add values to the global environment.
 *)
val genv_add_tydef  : genv -> ty_var -> tydef -> genv
val genv_add_string : genv -> loc -> string -> genv * var
val genv_add_tag    : genv -> loc -> tag_var -> ty_var -> ty list -> genv

val genv_add_ty_apply : genv -> Aml_tast.ty_var -> Aml_tast.ty_kind list -> (loc -> ty list -> ty) -> genv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
