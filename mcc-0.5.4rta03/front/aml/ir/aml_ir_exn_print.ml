(*
 * Exception printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fir_state

open Aml_ir
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_print

module Pos = MakePos (struct let name = "Aml_ir_exn_print" end)
open Pos

(************************************************************************
 * PRINTING
 ************************************************************************)

let tabstop = 3

(*
 * Format the possible FIR exceptions.
 *)
let pp_print_error buf exn =
   match exn with
      UnboundVar v ->
         fprintf buf "unbound variable: %a" pp_print_symbol v

    | UnboundType v ->
         fprintf buf "unbound type: %a" pp_print_symbol v

    | UnboundLabel label ->
         fprintf buf "unbound label: %a" pp_print_symbol label

    | UnboundConst label ->
         fprintf buf "unbound constructor: %a" pp_print_symbol label

    | UnboundVal (ty_var, ext_var) ->
         fprintf buf "unbound module field: %a.%a" pp_print_symbol ty_var pp_print_symbol ext_var

    | ArityMismatch (arity1, arity2) ->
         fprintf buf "arity mismatch (expected %d, found %d)" arity1 arity2

    | VarArityMismatch (v, arity1, arity2) ->
         fprintf buf "arity mismatch for variable %a (expected %d, found %d)" pp_print_symbol v arity1 arity2

    | TypeArityMismatch (ty, arity1, arity2) ->
         fprintf buf "type arity mismatch for %a (expected %d, found %d)" pp_print_type ty arity1 arity2

    | InvalidFormatString s ->
         fprintf buf "invalid format string: \"%s\"" (String.escaped s)

    | StringError s ->
         pp_print_string buf s

    | StringIntError (s, i) ->
         fprintf buf "%s: %d" s i

    | StringAtomError (s, a) ->
         fprintf buf "%s: %a" s pp_print_atom a

    | StringTypeError (s, ty) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_type ty

    | StringTydefError (s, tydef) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_tydef tydef

    | StringTydefInnerError (s, inner) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_tydef_inner inner

    | StringVarError (s, v) ->
         fprintf buf "%s: %a" s pp_print_symbol v

    | StringVarTypeError (s, ty) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_var_type ty

    | StringTAstTydefInnerError (s, ty) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s Aml_tast_print.pp_print_tydef_inner ty

    | StringTAstModNameError (s, name) ->
         fprintf buf "%s: %a" s Aml_tast_print.pp_print_mod_name name

    | NotImplemented s ->
         fprintf buf "not implemented: %s" s

    | InternalError ->
         pp_print_string buf "internal error"

(*
 * Exception printer.
 *)
let pp_print_exn buf e =
   match e with
      IRException (pos, exn) ->
         fprintf buf "@[<v 0>%a@ @[<hv 3>*** AML IR Error: %a@]@]@." (**)
            pp_print_pos pos
            pp_print_error exn
    | exn ->
         Aml_tast_exn_print.pp_print_exn buf exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      IRException _
    | Aml_tast_pos.TAstException _
    | Aml_ast.SyntaxError _ as exn ->
         pp_print_exn err_formatter exn;
         exit 2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
