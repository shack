(*
 * This is the first part of closure conversion.
 * Convert functions so they have no free variables.
 *
 * There are several passes here.
 *    1. First, collect _all_ the definitions in each global function.
 *       This will be a superset of the function's frame.
 *    2. Next, construct the dataflow equations for each function.
 *    3. Solve the dataflow equations.  Global variables are never
 *       free in a function, and global functions never let any
 *       of their definitions (regardless of scope) to escape.
 *    4. Construct the set of escaping variables.  A variable escapes
 *       if it is free in a continuation or a global function.  Build
 *       a table that maps escaping vars to the frames in which they
 *       were defined.
 *    5. Close the program.  All references to escaping variables
 *       are converted to frame operations or operations on the
 *       globals.  Note that globals are _always_ pointers, so we
 *       have to insert subscript operations to access them.
 *
 *       Each global function gets a frame, and the frames are
 *       linked in a list in the nesting order.  All escaping functions
 *       get a frame.  All closures, fetch all the frames they can access.
 *
 *       Local functions are treated as if they don't have frames.
 *       Free variables are passed as extra arguments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location
open Field_table

open Fir_state

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_type
open Aml_ir_tydef
open Aml_ir_print
open Aml_ir_standardize

module Pos = MakePos (struct let name = "Aml_ir_closure" end)
open Pos

(************************************************************************
 * ENVIRONMENTS
 ************************************************************************)

(*
 * In the first pass, we collect all the function names.
 *)
module DefSet    = SymbolSet
module UseSet    = SymbolSet
module EscSet    = SymbolSet
module TyVarSet  = SymbolSet
module CallTable = SymbolTable
module DefTable  = SymbolTable
module FunTable  = SymbolTable

(*
 * This is the info that is recorded for each function.
 *    entry_nest: the nesting stack
 *    entry_defs: the types of all the defined variables
 *    entry_frame: the name of the function's frame
 *)
type entry =
   { entry_nest    : var list;
     entry_defs    : ty DefTable.t;
     entry_ty_defs : TyVarSet.t;
     entry_frame   : var;
     entry_loc     : loc;
     entry_ty      : ty
   }

(*
 * These are synthesized attributes.
 *    dflow_class: function class
 *    dflow_entry: the information about the function
 *    dflow_calls: the calls (with defs) for each function call
 *    dflow_uses: the uses (free vars) of the function
 *    dflow_top: the uses, minus globals
 *    dflow_ty_uses: the type vars used
 *    dflow_defs: the definitions that have been assigned to the frame
 *    dflow_free: the types of the variables not assigned to the frame, but free
 *)
type dflow =
   { dflow_class   : fun_class;
     dflow_entry   : entry;
     dflow_calls   : DefSet.t CallTable.t;
     dflow_uses    : UseSet.t;
     dflow_top     : UseSet.t;
     dflow_free    : ty SymbolTable.t;
     dflow_ty_uses : TyVarSet.t;
   }

(*
 * After analysis, this is the in info we keep for each function.
 *   fun_nest: list of nested functions, hd is parent
 *   fun_uses: free variables
 *   fun_info: extra info
 *      GlobalInfo (frame, free):
 *          frame: name of the frame
 *          free: the type of each free variable
 *      LocalInfo:
 *          the function is not a global function
 *)
type fun_info =
   GlobalInfo of var * ty SymbolTable.t
 | LocalInfo

type fun_desc =
   { fun_nest    : var list;
     fun_uses    : var list;
     fun_info    : fun_info;
     fun_gflag   : fun_class;
     fun_ty_uses : ty_var list;
     fun_loc     : loc;
     fun_ty      : ty
   }

(*
 * Frame info.
 *    frame_vars: free type vars in the frame
 *    frame_fields: fields in the frame (without the parent)
 *    frame_parent: optional frame name of the parent
 *)
type frame =
   { frame_vars   : ty_var list option;
     frame_fields : (bool * ty) FieldTable.t;
     frame_parent : ty_var option;
     frame_loc    : loc
   }

(*
 * This is the info we carry around during conversion.
 *    close_esc: table of escaping vars, and the frame they belong to
 *    close_funs: info for fun conversion
 *)
type close_var =
   GlobalVar
 | ExternVar
 | FrameVar of var

type close_info =
   { close_esc : (close_var * ty) SymbolTable.t;
     close_funs : fun_desc FunTable.t
   }

let cenv_lookup_fun { close_funs = funs } pos f =
   try SymbolTable.find funs f with
      Not_found ->
         raise (IRException (pos, UnboundVar f))

let fun_table_lookup fenv pos f =
   try FunTable.find fenv f with
      Not_found ->
         raise (IRException (pos, UnboundVar f))

let table_lookup venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (IRException (pos, UnboundVar v))

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Utilities.
 *)
let hd pos = function
   h :: _ -> h
 | _ -> raise (IRException (pos, StringError "hd on empty list"))

(*
 * Fold left with arity checking.
 *)
let fold_left2 pos f x l1 l2 =
   let pos = string_pos "fold_left2" pos in
   let len1 = List.length l1 in
   let len2 = List.length l2 in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, ArityMismatch (len1, len2)))
   in
      List.fold_left2 f x l1 l2

(*
 * Get the type of a frame from its name.
 *)
let frame_type frames pos loc v =
   let pos = string_pos "frame_type" pos in
   let { frame_vars = vars } =
      try SymbolTable.find frames v with
         Not_found ->
            raise (IRException (pos, UnboundVar v))
   in
      match vars with
         Some vars ->
            let args = List.map (fun v -> make_type loc (TyVar v)) vars in
               args, make_type loc (TyFrame (v, args))
       | None ->
            raise (IRException (pos, InternalError))

(************************************************************************
 * INITIAL DEFS
 ************************************************************************)

(*
 * Add a variable definition to the defs.
 *)
type defs =
   { defs_vars : ty DefTable.t;
     defs_ty_vars : TyVarSet.t
   }

let defs_empty =
   { defs_vars = DefTable.empty;
     defs_ty_vars = TyVarSet.empty
   }

let defs_var defs v ty =
   { defs with defs_vars = DefTable.add defs.defs_vars v ty }

let defs_ty_var defs v =
   { defs with defs_ty_vars = TyVarSet.add defs.defs_ty_vars v }

(*
 * Collect the definition info for the functions.
 *)
let rec defs_exp genv fenv nest defs e =
   let pos = string_pos "defs_exp" (exp_pos e) in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            defs_funs_exp genv fenv nest defs pos funs e
       | LetRec (letrecs, e) ->
            defs_letrecs_exp genv fenv nest defs pos letrecs e
       | LetAtom (v, ty, _, e)
       | LetExt (v, ty, _, _, _, _, e)
       | LetSubscript (v, ty, _, _, e)
       | SetGlobal (v, ty, _, e) ->
            let defs = defs_var defs v ty in
               defs_exp genv fenv nest defs e
       | LetAlloc (v, op, e) ->
            let ty, defs = defs_alloc_op defs op in
            let defs = defs_var defs v ty in
               defs_exp genv fenv nest defs e
       | SetSubscript (_, _, _, _, e) ->
            defs_exp genv fenv nest defs e
       | Match (_, cases) ->
            List.fold_left (fun (fenv, defs) (_, e) ->
                  defs_exp genv fenv nest defs e) (fenv, defs) cases
       | MatchDTuple (_, cases) ->
            List.fold_left (fun (fenv, defs) (_, e) ->
                  defs_exp genv fenv nest defs e) (fenv, defs) cases
       | TailCall _ ->
            fenv, defs
       | LetClosure _
       | LetApply _
       | Return _
       | Try _
       | Raise _ ->
            raise (IRException (pos, InternalError))

(*
 * Recursive definitions.
 *)
and defs_letrecs_exp genv fenv nest defs pos letrecs e =
   let pos = string_pos "defs_letrecs_exp" pos in
   let fenv, defs =
      List.fold_left (fun (fenv, defs) (v, e, ty) ->
            let defs = defs_var defs v ty in
               defs_exp genv fenv nest defs e) (fenv, defs) letrecs
   in
      defs_exp genv fenv nest defs e

(*
 * Collect the definitions in the functions.
 *)
and defs_funs_exp genv fenv nest defs pos funs e =
   let pos = string_pos "defs_funs_exp" pos in
   let fenv, defs =
      List.fold_left (fun (fenv, defs) (f, loc, gflag, ty, ty_vars, vars, e) ->
            defs_fun genv fenv nest defs f (loc, gflag, ty, ty_vars, vars, e)) (fenv, defs) funs
   in
      defs_exp genv fenv nest defs e

and defs_fun genv fenv nest defs f def =
   let pos = string_pos "defs_fun" (fundef_pos f def) in
   let loc, gflag, ty_vars, ty, vars, e = def in
      match gflag with
         FunGlobalClass
       | FunTopLevelClass
       | FunPartialClass ->
            let ty_args, _ = dest_fun_type genv pos ty in
            let defs' = defs_empty in
            let defs' = List.fold_left defs_ty_var defs' ty_vars in
            let defs' = fold_left2 pos defs_var defs' vars ty_args in
            let frame = new_symbol f in
            let fenv, defs' = defs_exp genv fenv (frame :: nest) defs' e in
            let entry =
               { entry_nest = nest;
                 entry_defs = defs'.defs_vars;
                 entry_ty_defs = defs'.defs_ty_vars;
                 entry_frame = frame;
                 entry_loc = loc;
                 entry_ty = ty
               }
            in
            let fenv = FunTable.add fenv f entry in
               fenv, defs
       | FunContClass
       | FunLocalClass ->
            (* Add a dummy entry so the function will be detected *)
            let entry =
               { entry_nest = nest;
                 entry_defs = DefTable.empty;
                 entry_ty_defs = TyVarSet.empty;
                 entry_frame = f;
                 entry_loc = loc;
                 entry_ty = ty
               }
            in
            eprintf "defs_fun: %a@." pp_print_symbol f;

            (* Add the defs to parent's frame *)
            let ty_args, _ = dest_fun_type genv pos ty in
            let defs = fold_left2 pos defs_var defs vars ty_args in
            let defs = List.fold_left defs_ty_var defs ty_vars in
            let fenv = FunTable.add fenv f entry in
               defs_exp genv fenv nest defs e

(*
 * Definitions in an allocation.
 * The ty_vars are counted as type definitions.
 *)
and defs_alloc_op defs op =
   match op with
      AllocTuple (ty_vars, ty, _)
    | AllocUnion (ty_vars, ty, _, _, _)
    | AllocRecord (ty_vars, ty, _, _) ->
         ty, List.fold_left defs_ty_var defs ty_vars
    | AllocDTuple (ty, _, _, _)
    | AllocArray (ty, _)
    | AllocVArray (ty, _, _)
    | AllocModule (ty, _, _)
    | AllocFrame (ty, _, _) ->
         ty, defs

(*
 * Collect definitions in a function table.
 *)
let defs_funs genv fenv nest defs funs =
   SymbolTable.fold (fun (fenv, defs) f def ->
         defs_fun genv fenv nest defs f def) (fenv, defs) funs

(************************************************************************
 * DATAFLOW GRAPH
 ************************************************************************)

(*
 * Use a var, this adds to the uses.
 * If this is a function, add it to the call table.
 * Otherwise, add the use.
 *)
let dflow_use_var fenv defs calls uses ty_uses v =
   if FunTable.mem fenv v then
      let calls = CallTable.add calls v defs in
         calls, uses, ty_uses
   else
      let uses = UseSet.add uses v in
         calls, uses, ty_uses

(*
 * Use a type.
 * Add all the free vars in the type.
 *)
let dflow_use_type fenv defs calls uses ty_uses ty =
   let fv = free_vars_type ty in
   let { fv_fo_vars = fo_vars;
         fv_vars = vars
       } = fv
   in
   let ty_uses = SymbolSet.union ty_uses fo_vars in
      SymbolSet.fold (fun (calls, uses, ty_uses) v ->
            dflow_use_var fenv defs calls uses ty_uses v) (calls, uses, ty_uses) vars

let dflow_use_types fenv defs calls uses ty_uses tyl =
   List.fold_left (fun (calls, uses, ty_uses) ty ->
         dflow_use_type fenv defs calls uses ty_uses ty) (calls, uses, ty_uses) tyl

(*
 * Operator uses.
 *)
let dflow_use_unop fenv defs calls uses ty_uses op =
   match op with
      NotBoolOp

      (* Arithmetic *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Floats *)
    | UMinusFloatOp
    | AbsFloatOp
    | SqrtFloatOp
    | ExpFloatOp
    | LogFloatOp
    | Log10FloatOp
    | CosFloatOp
    | SinFloatOp
    | TanFloatOp
    | ACosFloatOp
    | ASinFloatOp
    | ATanFloatOp
    | CosHFloatOp
    | SinHFloatOp
    | TanHFloatOp
    | CeilFloatOp
    | FloorFloatOp

      (* Coercions *)
    | CharOfIntOp
    | IntOfCharOp
    | IntOfFloatOp
    | FloatOfIntOp

      (* Block operations *)
    | LengthOfStringOp ->
         calls, uses, ty_uses

    | LengthOfBlockOp ty ->
         dflow_use_type fenv defs calls uses ty_uses ty

let dflow_use_binop fenv defs calls uses ty_uses op =
   match op with
      (* Non-short-circuit Booleans *)
      OrBoolOp
    | AndBoolOp

      (* Comparisons on ML ints *)
    | EqCharOp
    | NeqCharOp
    | LtCharOp
    | LeCharOp
    | GtCharOp
    | GeCharOp
    | CmpCharOp                (* Similar to ML's ``compare'' function. *)

      (* Standard binary operations on ML ints *)
    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp

      (* Comparisons on ML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp
    | CmpIntOp                (* Similar to ML's ``compare'' function. *)

      (* Standard binary operations on floats *)
    | PlusFloatOp
    | MinusFloatOp
    | MulFloatOp
    | DivFloatOp
    | RemFloatOp
    | MaxFloatOp
    | MinFloatOp
    | PowerFloatOp
    | ATan2FloatOp
    | LdExpFloatIntOp

      (* Comparisons on floats *)
    | EqFloatOp
    | NeqFloatOp
    | LtFloatOp
    | LeFloatOp
    | GtFloatOp
    | GeFloatOp
    | CmpFloatOp ->
         calls, uses, ty_uses

      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty
    | NeqEqOp ty ->
         dflow_use_type fenv defs calls uses ty_uses ty

(*
 * Atom uses.
 *)
let rec dflow_use_atom fenv defs calls uses ty_uses a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _ ->
         calls, uses, ty_uses
    | AtomConst (ty, _, _)
    | AtomNil ty ->
         dflow_use_type fenv defs calls uses ty_uses ty
    | AtomVar v
    | AtomTyUnpack v ->
         dflow_use_var fenv defs calls uses ty_uses v
    | AtomTyPack (v, ty, tyl) ->
         let calls, uses, ty_uses = dflow_use_var fenv defs calls uses ty_uses v in
            dflow_use_types fenv defs calls uses ty_uses (ty :: tyl)
    | AtomTyConstrain (a, ty) ->
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a in
            dflow_use_type fenv defs calls uses ty_uses ty
    | AtomTyApply (a, ty, tyl) ->
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a in
            dflow_use_types fenv defs calls uses ty_uses (ty :: tyl)
    | AtomUnop (op, a) ->
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a in
            dflow_use_unop fenv defs calls uses ty_uses op
    | AtomBinop (op, a1, a2) ->
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a1 in
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a2 in
            dflow_use_binop fenv defs calls uses ty_uses op

let dflow_use_atom_opt fenv defs calls uses ty_uses a_opt =
   match a_opt with
      Some a -> dflow_use_atom fenv defs calls uses ty_uses a
    | None -> calls, uses, ty_uses

let dflow_use_atoms fenv defs calls uses ty_uses atoms =
   List.fold_left (fun (calls, uses, ty_uses) a ->
         dflow_use_atom fenv defs calls uses ty_uses a) (calls, uses, ty_uses) atoms

(*
 * Allocation operation.
 *)
let dflow_use_alloc_op fenv defs calls uses ty_uses op =
   match op with
      AllocTuple (_, ty, args)
    | AllocUnion (_, ty, _, _, args)
    | AllocArray (ty, args)
    | AllocDTuple (ty, _, _, args) ->
         let calls, uses, ty_uses = dflow_use_atoms fenv defs calls uses ty_uses args in
            dflow_use_type fenv defs calls uses ty_uses ty
    | AllocVArray (ty, a1, a2) ->
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a1 in
         let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a2 in
            dflow_use_type fenv defs calls uses ty_uses ty
    | AllocRecord (_, ty, _, fields) ->
         let calls, uses, ty_uses =
            List.fold_left (fun (calls, uses, ty_uses) (_, a)->
                  dflow_use_atom fenv defs calls uses ty_uses a) (calls, uses, ty_uses) fields
         in
            dflow_use_type fenv defs calls uses ty_uses ty
    | AllocModule (ty, _, fields) ->
         let calls, uses, ty_uses =
            SymbolTable.fold (fun (calls, uses, ty_uses) _ a ->
                  dflow_use_atom fenv defs calls uses ty_uses a) (calls, uses, ty_uses) fields
         in
            dflow_use_type fenv defs calls uses ty_uses ty
    | AllocFrame (ty, _, _) ->
         dflow_use_type fenv defs calls uses ty_uses ty

(*
 * Transform an expression.
 *)
let rec dflow_exp fenv nest dflow defs calls uses ty_uses e =
   let pos = string_pos "dflow_exp" (exp_pos e) in
      match dest_exp_core e with
         LetFuns (funs, e) ->
            dflow_funs_exp fenv nest dflow defs calls uses ty_uses pos funs e
       | LetRec (letrecs, e) ->
            dflow_letrecs_exp fenv nest dflow defs calls uses ty_uses pos letrecs e
       | LetAtom (v, ty, a, e)
       | SetGlobal (v, ty, a, e) ->
            dflow_def_arg_exp fenv nest dflow defs calls uses ty_uses pos v ty a e
       | LetExt (v, ty1, _, ty2, ty_args, args, e) ->
            dflow_ext_exp fenv nest dflow defs calls uses ty_uses pos v ty1 ty2 ty_args args e
       | TailCall (f, args) ->
            dflow_tailcall_exp fenv nest dflow defs calls uses ty_uses pos f args
       | LetAlloc (v, op, e) ->
            dflow_alloc_exp fenv nest dflow defs calls uses ty_uses pos v op e
       | Match (a, cases) ->
            dflow_match_exp fenv nest dflow defs calls uses ty_uses pos a cases
       | MatchDTuple (a, cases) ->
            dflow_match_dtuple_exp fenv nest dflow defs calls uses ty_uses pos a cases
       | LetSubscript (v1, ty, a2, a3, e) ->
            dflow_subscript_exp fenv nest dflow defs calls uses ty_uses pos v1 ty a2 a3 e
       | SetSubscript (v1, a2, a3, ty, e) ->
            dflow_set_subscript_exp fenv nest dflow defs calls uses ty_uses pos v1 a2 a3 ty e
       | LetClosure _
       | LetApply _
       | Return _
       | Try _
       | Raise _ ->
            raise (IRException (pos, StringError "unexpected expression"))

(*
 * Dataflow calculation for functions.
 *)
and dflow_funs_exp fenv nest dflow defs calls uses ty_uses pos funs e =
   let pos = string_pos "dflow_funs_exp" pos in

   (* Build a set containing the function names *)
   let fvars =
      List.fold_left (fun fvars (f, _, _, _, _, _, _) ->
            SymbolSet.add fvars f) SymbolSet.empty funs
   in

   (* Walk through the body of each function *)
   let dflow =
      List.fold_left (fun dflow (f, _, gflag, ty_vars, ty, vars, e) ->
            dflow_fun fenv nest dflow pos fvars f gflag ty_vars ty vars e) dflow funs
   in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow defs calls uses ty_uses e in
   let uses = UseSet.diff uses fvars in
      dflow, calls, uses, ty_uses

(*
 * Global function starts a new dflow environment.
 *)
and dflow_fun fenv nest dflow pos fvars f gflag ty_vars ty vars e =
   let pos = string_pos "dflow_fun" pos in

   (* Get the function's entry *)
   let nest, entry =
      match gflag with
         FunGlobalClass
       | FunTopLevelClass
       | FunPartialClass ->
            f :: nest, fun_table_lookup fenv pos f
       | FunContClass
       | FunLocalClass ->
            let entry = fun_table_lookup fenv pos (hd pos nest) in
            let ty_defs = List.fold_left TyVarSet.add TyVarSet.empty ty_vars in
            let entry = { entry with entry_ty_defs = ty_defs } in
               nest, entry
   in

   (* Reset the defs and calls *)
   let defs = DefSet.of_list vars in
   let calls = CallTable.empty in
   let uses = UseSet.empty in
   let ty_uses = TyVarSet.empty in
   let calls, uses, ty_uses = dflow_use_type fenv defs calls uses ty_uses ty in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow defs calls uses ty_uses e in

   (* Remove the bound vars from the uses *)
   let uses = UseSet.subtract_list uses vars in
   let uses = UseSet.diff uses fvars in
   let ty_uses = TyVarSet.subtract_list ty_uses ty_vars in

   (* Add the dflow for the function to the env *)
   let entry =
      { dflow_class = gflag;
        dflow_entry = entry;
        dflow_calls = calls;
        dflow_uses = uses;
        dflow_top = uses;
        dflow_ty_uses = ty_uses;
        dflow_free = SymbolTable.empty
      }
   in
      FunTable.add dflow f entry

(*
 * Recursive definitions.
 *)
and dflow_letrecs_exp fenv nest dflow defs calls uses ty_uses pos letrecs e =
   let pos = string_pos "dflow_letrecs_exp" pos in
      raise (IRException (pos, NotImplemented "recursive definitions"))

(*
 * External call.
 *)
and dflow_ext_exp fenv nest dflow defs calls uses ty_uses pos v ty1 ty2 ty_args args e =
   let pos = string_pos "dflow_def_args_exp" pos in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses ty_uses e in
   let uses = UseSet.remove uses v in
   let calls, uses, ty_uses = dflow_use_type fenv defs calls uses ty_uses ty1 in
   let calls, uses, ty_uses = dflow_use_type fenv defs calls uses ty_uses ty2 in
   let calls, uses, ty_uses = dflow_use_types fenv defs calls uses ty_uses ty_args in
   let calls, uses, ty_uses = dflow_use_atoms fenv defs calls uses ty_uses args in
      dflow, calls, uses, ty_uses

(*
 * A let binding with an argument.
 *)
and dflow_def_arg_exp fenv nest dflow defs calls uses ty_uses pos v ty arg e =
   let pos = string_pos "dflow_def_arg_exp" pos in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses ty_uses e in
   let uses = UseSet.remove uses v in
   let calls, uses, ty_uses = dflow_use_type fenv defs calls uses ty_uses ty in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses arg in
      dflow, calls, uses, ty_uses

(*
 * A tailcall.
 *)
and dflow_tailcall_exp fenv nest dflow defs calls uses ty_uses pos f args =
   let pos = string_pos "dflow_tailcall_exp" pos in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses f in
   let calls, uses, ty_uses = dflow_use_atoms fenv defs calls uses ty_uses args in
      dflow, calls, uses, ty_uses

(*
 * A simpler one-atom operation.
 *)
and dflow_alloc_exp fenv nest dflow defs calls uses ty_uses pos v op e =
   let pos = string_pos "dflow_alloc_exp" pos in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow (DefSet.add defs v) calls uses ty_uses e in
   let uses = UseSet.remove uses v in
   let calls, uses, ty_uses = dflow_use_alloc_op fenv defs calls uses ty_uses op in
      dflow, calls, uses, ty_uses

(*
 * Multiple branches.
 *)
and dflow_match_exp fenv nest dflow defs calls uses ty_uses pos a cases =
   let pos = string_pos "dflow_match_exp" pos in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a in
      List.fold_left (fun (dflow, calls, uses, ty_uses) (_, e) ->
            dflow_exp fenv nest dflow defs calls uses ty_uses e) (dflow, calls, uses, ty_uses) cases

(*
 * Tuple testing.
 *)
and dflow_match_dtuple_exp fenv nest dflow defs calls uses ty_uses pos a cases =
   let pos = string_pos "dflow_match_dtuple_exp" pos in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a in
      List.fold_left (fun (dflow, calls, uses, ty_uses) (a_opt, e) ->
            let calls, uses, ty_uses = dflow_use_atom_opt fenv defs calls uses ty_uses a_opt in
               dflow_exp fenv nest dflow defs calls uses ty_uses e) (dflow, calls, uses, ty_uses) cases

(*
 * Subscript.
 *)
and dflow_subscript_exp fenv nest dflow defs calls uses ty_uses pos v1 ty a2 a3 e =
   let pos = string_pos "dflow_subscript_exp" pos in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow (DefSet.add defs v1) calls uses ty_uses e in
   let uses = UseSet.remove uses v1 in
   let calls, uses, ty_uses = dflow_use_type fenv defs calls uses ty_uses ty in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a2 in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a3 in
      dflow, calls, uses, ty_uses

and dflow_set_subscript_exp fenv nest dflow defs calls uses ty_uses pos a1 a2 a3 ty e =
   let pos = string_pos "dflow_set_subscript_exp" pos in
   let dflow, calls, uses, ty_uses = dflow_exp fenv nest dflow defs calls uses ty_uses e in
   let calls, uses, ty_uses = dflow_use_type fenv defs calls uses ty_uses ty in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a1 in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a2 in
   let calls, uses, ty_uses = dflow_use_atom fenv defs calls uses ty_uses a3 in
      dflow, calls, uses, ty_uses

(*
 * Dataflow calculation for functions.
 *)
let dflow_funs fenv funs =
   (* Build a set containing the function names *)
   let fvars =
      SymbolTable.fold (fun fvars f _ ->
            SymbolSet.add fvars f) SymbolSet.empty funs
   in
      (* Walk through the body of each function *)
      SymbolTable.fold (fun dflow f def ->
            let pos = string_pos "dflow_funs" (fundef_pos f def) in
            let loc, gflag, ty, ty_vars, vars, e = def in
               dflow_fun fenv [] dflow pos fvars f gflag ty ty_vars vars e) SymbolTable.empty funs

(************************************************************************
 * DATAFLOW CALCULATION
 ************************************************************************)

(*
 * Print data flow.
 *)
let pp_print_dflow buf dflow =
   fprintf buf "@[<hv 3>DFLow:";
   FunTable.iter (fun f info ->
         let { dflow_calls = calls;
               dflow_uses = uses;
               dflow_top = top;
               dflow_ty_uses = ty_uses;
               dflow_entry = entry
             } = info
         in
         let { entry_defs = defs;
               entry_ty_defs = ty_defs
             } = entry
         in
            fprintf buf "@ @[<v 3>%a" pp_print_symbol f;

            fprintf buf "@ @[<b 3>Calls:";
            CallTable.iter (fun g defs ->
                  fprintf buf "@ %a" pp_print_symbol g) calls;

            fprintf buf "@]@ @[<b 3>Uses:";
            UseSet.iter (fun v ->
                  fprintf buf "@ %a" pp_print_symbol v) uses;

            fprintf buf "@]@ @[<b 3>Top:";
            UseSet.iter (fun v ->
                  fprintf buf "@ %a" pp_print_symbol v) top;

            fprintf buf "@]@ @[<b 3>Type uses:";
            UseSet.iter (fun v ->
                  fprintf buf "@ %a" pp_print_symbol v) ty_uses;

            fprintf buf "@]@ @[<b 3>Defs:";
            DefTable.iter (fun v _ ->
                  fprintf buf "@ %a" pp_print_symbol v) defs;

            fprintf buf "@]@ @[<b 3>Type defs:";
            TyVarSet.iter (fun v ->
                  fprintf buf "@ %a" pp_print_symbol v) ty_defs;

            fprintf buf "@]@]") dflow;
   fprintf buf "@]@."

let print_dflow dflow =
   pp_print_dflow err_formatter dflow

(*
 * Solve the dataflow equations.
 * This is _just_ the variables.
 * We compute type variable after assigning variables to frames.
 *
 *    vars[n] = uses[n] + union ((p, defs) in calls[n]. uses[p] - defs)
 *
 * Solve by iteration.
 *)
let dflow_solve_vars globals dflow =
   (* Vars: one sweep of the dataflow computation *)
   let step_uses dflow =
      FunTable.fold (fun (dflow, changed) f info ->
            let { dflow_class = gflag;
                  dflow_entry = entry;
                  dflow_calls = calls;
                  dflow_uses = uses;
                  dflow_top = top
                } = info
            in
            let { entry_defs = entry_defs } = entry in

            (* Add call uses to uses *)
            let top' =
               CallTable.fold (fun top' g defs ->
                     let pos = var_exp_pos g in
                     let { dflow_uses = uses } = fun_table_lookup dflow pos g in
                     let top = DefSet.fold UseSet.remove uses defs in
                     let top = UseSet.union top' top in
                        top) top calls
            in

            (* Remove global defs *)
            (* BUG: this is too expensive, think about how to fix it *)
            let top' = UseSet.subtract_list top' globals in
            let top' =
               FunTable.fold (fun top' f _ ->
                     UseSet.remove top' f) top' dflow
            in

            (* Remove defs in global function *)
            let uses' =
               match gflag with
                  FunGlobalClass
                | FunTopLevelClass
                | FunPartialClass ->
                     DefTable.fold (fun uses v _ ->
                           UseSet.remove uses v) top' entry_defs
                | FunContClass
                | FunLocalClass ->
                     top'
            in
               (* If nothing changed, don't do anything *)
               if UseSet.equal top top' then
                  dflow, changed
               else
                  let entry =
                     { info with dflow_uses = uses';
                                 dflow_top = top'
                     }
                  in
                  let dflow = FunTable.add dflow f entry in
                     dflow, true) (dflow, false) dflow
   in

   (* Vars: calculate fixpoint *)
   let rec fixpoint_uses dflow =
      let dflow, changed = step_uses dflow in
         if changed then
            fixpoint_uses dflow
         else
            dflow
   in
   let dflow = fixpoint_uses dflow in
      if debug Fir_state.debug_print_ir then
         print_dflow dflow;
      dflow

(*
 * Build a table that associates all variables with their frame.
 *)
let frame_all genv import globals dflow =
   (* Add all the globals without a frame *)
   let frame =
      SymbolTable.fold (fun frame v ty ->
            let pos = var_exp_pos v in
            let info = GlobalVar, ty in
               SymbolTable.add frame v info) SymbolTable.empty globals
   in
   let frame =
      SymbolTable.fold (fun frame v { import_type = ty } ->
            SymbolTable.add frame v (ExternVar, ty)) frame import
   in

   (* Add all the defined vars *)
   let frame =
      FunTable.fold (fun frame f info ->
            let { dflow_class = gflag;
                  dflow_entry = entry
                } = info
            in
               match gflag with
                  FunGlobalClass
                | FunTopLevelClass
                | FunPartialClass ->
                     let { entry_frame = f';
                           entry_defs = defs
                         } = entry
                     in
                        DefTable.fold (fun frame v ty ->
                              SymbolTable.add frame v (FrameVar f', ty)) frame defs
                | FunContClass
                | FunLocalClass ->
                     frame) frame dflow
   in
      frame

(*
 * Define all the escaping variables.
 * A variable escapes if it is free in any global function
 * or continuation, or if it is in the escape set already.
 *)
let esc_uses pos esc frame uses =
   let pos = string_pos "esc_uses" pos in
      UseSet.fold (fun esc v -> SymbolTable.add esc v (table_lookup frame pos v)) esc uses

let frame_esc pos frame globals dflow =
   let pos = string_pos "frame_esc" pos in
   let esc =
      SymbolTable.fold (fun esc v _ ->
            SymbolTable.add esc v (table_lookup frame pos v)) SymbolTable.empty globals
   in
   let pos = string_pos "frame_esc_fun" pos in
   let esc =
      FunTable.fold (fun esc f info ->
            let { dflow_class = gflag;
                  dflow_uses = uses
                } = info
            in
               match gflag with
                  FunGlobalClass
                | FunTopLevelClass
                | FunPartialClass ->
                     esc_uses pos esc frame uses
                | FunContClass ->
                     esc_uses pos esc frame uses
                | FunLocalClass ->
                     esc) esc dflow
   in
      esc

(*
 * Rebuild the dataflow table.
 * Global functions get at most one use: their parent's frame.
 * Local functions get all their uses, plus all the frames in scope.
 *)
let dflow_assign_frames esc dflow =
   FunTable.fold_map (fun frames f info ->
         let { dflow_class = gflag;
               dflow_entry = entry;
               dflow_uses = uses;
               dflow_top = top;
               dflow_ty_uses = ty_uses
             } = info
         in
         let { entry_nest = nest;
               entry_defs = defs;
               entry_frame = frame;
               entry_loc = loc;
               entry_ty = ty
             } = entry
         in
         let pos = var_exp_pos f in
         let nest, uses, fields, free, frames =
            match gflag with
               FunGlobalClass
             | FunTopLevelClass
             | FunPartialClass ->
                  (* Collect only those fields that have escaped *)
                  let free, fields =
                     SymbolTable.fold (fun (free, fields) v ty ->
                           try
                              let gflag, _ = SymbolTable.find esc v in
                              let fields =
                                 match gflag with
                                    FrameVar _ ->
                                       FieldTable.add fields v (true, ty)
                                  | GlobalVar
                                  | ExternVar ->
                                       fields
                              in
                                 free, fields
                           with
                              Not_found ->
                                 let free =
                                    if UseSet.mem top v then
                                       SymbolTable.add free v ty
                                    else
                                       free
                                 in
                                    free, fields) (SymbolTable.empty, FieldTable.empty) defs
                  in

                  (* Add the parent's frame if there is one *)
                  let uses, parent =
                     match nest with
                        g :: _ ->
                           SymbolSet.singleton g, Some g
                      | [] ->
                           SymbolSet.empty, None
                  in

                  (* Add the frame to the frame table *)
                  let frame_info =
                     { frame_vars = None;
                       frame_fields = fields;
                       frame_parent = parent;
                       frame_loc = loc
                     }
                  in
                  let frames = SymbolTable.add frames frame frame_info in
                     nest, uses, fields, free, frames

             | FunContClass ->
                  (* Parent's frame is the only free var *)
                  let uses = SymbolSet.singleton frame in
                     frame :: nest, uses, FieldTable.empty, SymbolTable.empty, frames

             | FunLocalClass ->
                  (* Filter the argument list to exclude vars that escaped *)
                  let uses =
                     SymbolSet.fold (fun uses v ->
                           if SymbolTable.mem esc v then
                              uses
                           else
                              SymbolSet.add uses v) SymbolSet.empty uses
                  in

                  (* Add all parent frames as uses *)
                  let uses = SymbolSet.add_list uses (frame :: nest) in
                     nest, uses, FieldTable.empty, SymbolTable.empty, frames
         in
         let entry =
            { entry with entry_nest = nest }
         in
         let info =
            { info with dflow_entry = entry;
                        dflow_uses  = uses;
                        dflow_free  = free
            }
         in
            frames, info) SymbolTable.empty dflow

(*
 * Compute the free variables in the frames.
 *)
let frame_free_vars frames =
   let rec free_vars frames v =
      let info = SymbolTable.find frames v in
      let { frame_vars = vars;
            frame_fields = fields;
            frame_parent = parent
          } = info
      in
         match vars with
            Some vars ->
               frames, vars
          | None ->
               (* Get the free vars in the fields *)
               let vars =
                  FieldTable.fold (fun vars _ (_, ty) ->
                        let { fv_fo_vars = fo_vars } = free_vars_type ty in
                        let _ =
                           eprintf "@[<hv 3>frame_free_vars: type: %a; free_vars = @[<b 3>" pp_print_type_ds ty;
                           SymbolSet.iter (fun v ->
                                 eprintf "@ %a" pp_print_symbol v) fo_vars;
                           eprintf "@]@]@."
                        in
                           SymbolSet.union vars fo_vars) SymbolSet.empty fields
               in

               (* Get the free vars in the parent *)
               let frames, vars =
                  match parent with
                     Some v ->
                        let frames, vars' = free_vars frames v in
                        let vars = SymbolSet.add_list vars vars' in
                           frames, vars
                   | None ->
                        frames, vars
               in

               (* Remember what we did *)
               let vars = SymbolSet.to_list vars in
               let info = { info with frame_vars = Some vars } in
               let frames = SymbolTable.add frames v info in
                  frames, vars
   in
      SymbolTable.fold (fun frames v _ ->
            let frames, _ = free_vars frames v in
               frames) frames frames

(*
 * Solve the dataflow equations.
 * This is _just_ the type variables.
 * We compute type variable after assigning variables to frames.
 *
 *    vars[n] = uses[n] + union ((p, defs) in calls[n]. uses[p] - defs)
 *
 * Solve by iteration.
 *)
let dflow_solve_types frames dflow =
   (* Vars: one sweep of the dataflow computation *)
   let step_uses dflow =
      FunTable.fold (fun (dflow, changed) f info ->
            let { dflow_class = gflag;
                  dflow_entry = entry;
                  dflow_calls = calls;
                  dflow_ty_uses = uses
                } = info
            in
            let { entry_ty_defs = defs;
                  entry_nest = nest
                } = entry
            in

            (* Add call uses to uses *)
            let uses' =
               CallTable.fold (fun uses' g defs ->
                     let pos = var_exp_pos g in
                     let { dflow_ty_uses = uses } = fun_table_lookup dflow pos g in
                     let uses = TyVarSet.union uses' uses in
                        uses) uses calls
            in

            (* Add frame uses *)
            let uses' =
               match nest with
                  g :: _ ->
                     let { frame_vars = vars } = SymbolTable.find frames g in
                        (match vars with
                            Some vars ->
                               SymbolSet.add_list uses' vars
                          | None ->
                               raise (Invalid_argument "Aml_ir_closure.dflow_solve_types"))
                | [] ->
                     uses'
            in

            (* Remove defs *)
            let uses' = TyVarSet.diff uses' defs in

               (* If nothing changed, don't do anything *)
               if UseSet.equal uses uses' then
                  dflow, changed
               else
                  let info = { info with dflow_ty_uses = uses' } in
                  let dflow = FunTable.add dflow f info in
                     dflow, true) (dflow, false) dflow
   in

   (* Vars: calculate fixpoint *)
   let rec fixpoint_uses dflow =
      let dflow, changed = step_uses dflow in
         if changed then
            fixpoint_uses dflow
         else
            dflow
   in
   let dflow = fixpoint_uses dflow in
      if debug Fir_state.debug_print_ir then
         print_dflow dflow;
      dflow

(*
 * Convert dflow to functions.
 *)
let funs_of_dflow dflow =
   FunTable.map (fun info ->
         let { dflow_class = gflag;
               dflow_entry = entry;
               dflow_uses = uses;
               dflow_free = free;
               dflow_ty_uses = ty_uses
             } = info
         in
         let { entry_nest = nest;
               entry_frame = frame;
               entry_loc = loc;
               entry_ty = ty
             } = entry
         in
         let fun_info =
            match gflag with
               FunGlobalClass
             | FunPartialClass
             | FunTopLevelClass ->
                  GlobalInfo (frame, free)
             | FunContClass
             | FunLocalClass ->
                  LocalInfo
         in
            { fun_nest = nest;
              fun_uses = UseSet.to_list uses;
              fun_info = fun_info;
              fun_gflag = gflag;
              fun_ty_uses = TyVarSet.to_list ty_uses;
              fun_loc = loc;
              fun_ty = ty
            }) dflow

(************************************************************************
 * CLOSURE CONVERSION
 ************************************************************************)

(*
 * Defaults for various types.
 *)
let default_atom genv pos ty =
   let pos = string_pos "default_atom" pos in
      match expand_type_core genv pos ty with
         TyInt ->
            AtomInt 0
       | TyChar ->
            AtomChar '\000'
       | TyFloat ->
            AtomFloat 0.0
       | _ ->
            AtomNil ty

(*
 * Var reference.
 * If the var escapes, change this to a projection.
 *)
let close_var cenv loc v (cont : var -> exp) =
   try
      let frame, ty = SymbolTable.find cenv.close_esc v in
         match frame with
            GlobalVar ->
               cont v
          | ExternVar ->
               cont v
          | FrameVar frame ->
               (* This is in one of the frames *)
               let v' = new_symbol v in
               let e = cont v' in
                  make_exp loc (LetSubscript (v', ty, AtomVar frame, AtomFrameLabel (frame, v), e))
   with
      Not_found ->
         (* May be a function variable *)
         try
            let { fun_uses = uses;
                  fun_ty_uses = ty_uses;
                  fun_ty = ty
                } = SymbolTable.find cenv.close_funs v
            in
               if uses = [] then
                  (* If this is a top-level function, it has no environment *)
                  cont v
               else
                  (* Otherwise, we add the free vars (there should be exactly 1 *)
                  let v' = new_symbol v in
                  let ty_args = List.map (fun v -> make_type loc (TyVar v)) ty_uses in
                  let args = List.map (fun v -> AtomVar v) uses in
                     make_exp loc (LetClosure (v', AtomVar v, ty_args, args, cont v'))
         with
            Not_found ->
               (* Just a normal variable *)
               cont v

(*
 * Set a var reference.
 *)
let close_set_var cenv pos loc v ty v' e =
   let e =
      try
         let frame, ty = SymbolTable.find cenv.close_esc v in
            match frame with
               GlobalVar
             | ExternVar ->
                  SetGlobal (v, ty, AtomVar v', e)
             | FrameVar frame ->
                  SetSubscript (AtomVar frame, AtomFrameLabel (frame, v), AtomVar v', ty, e)
      with
         Not_found ->
            LetAtom (v, ty, AtomVar v', e)
   in
      make_exp loc e

let rec close_set_vars cenv pos loc vars e =
   let rec set e = function
      v :: vars ->
         let e =
            try
               let e =
                  let frame, ty = SymbolTable.find cenv.close_esc v in
                     match frame with
                        GlobalVar
                      | ExternVar ->
                           SetGlobal (v, ty, AtomVar v, e)
                      | FrameVar frame ->
                           SetSubscript (AtomVar frame, AtomFrameLabel (frame, v), AtomVar v, ty, e)
               in
                  make_exp loc e
            with
               Not_found ->
                  e
         in
            set e vars
    | [] ->
         e
   in
      set e vars

(*
 * Atom reference.
 *)
let rec close_atom cenv loc a cont =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomConst _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomNil _ ->
         cont a
    | AtomVar v ->
         close_var cenv loc v (fun v -> cont (AtomVar v))
    | AtomTyConstrain (a, ty) ->
         close_atom cenv loc a (fun a ->
               cont (AtomTyConstrain (a, ty)))
    | AtomTyApply (a, ty, tyl) ->
         close_atom cenv loc a (fun a ->
               cont (AtomTyApply (a, ty, tyl)))
    | AtomTyPack (v, ty, tyl) ->
         close_var cenv loc v (fun v ->
               cont (AtomTyPack (v, ty, tyl)))
    | AtomTyUnpack v ->
         close_var cenv loc v (fun v ->
               cont (AtomTyUnpack v))
    | AtomUnop (op, a) ->
         close_atom cenv loc a (fun a ->
               cont (AtomUnop (op, a)))
    | AtomBinop (op, a1, a2) ->
         close_atom cenv loc a1 (fun a1 ->
         close_atom cenv loc a2 (fun a2 ->
               cont (AtomBinop (op, a1, a2))))

let close_atoms cenv loc atoms cont =
   let rec collect atoms' = function
      a :: atoms ->
         close_atom cenv loc a (fun a ->
               collect (a :: atoms') atoms)
    | [] ->
         cont (List.rev atoms')
   in
      collect [] atoms

(*
 * Allocation operations.
 *)
let close_field_atoms cenv loc fields cont =
   let rec collect fields' = function
      (label, a) :: fields ->
         close_atom cenv loc a (fun a ->
               collect ((label, a) :: fields') fields)
    | [] ->
         cont (List.rev fields')
   in
      collect [] fields

let close_table_atoms cenv loc fields cont =
   let fields = SymbolTable.fold (fun fields v a -> (v, a) :: fields) [] fields in
      close_field_atoms cenv loc fields (fun fields ->
            let fields =
               List.fold_left (fun fields (v, a) ->
                     SymbolTable.add fields v a) SymbolTable.empty fields
            in
               cont fields)

let close_alloc_op cenv loc op cont =
   match op with
      AllocTuple (ty, ty_vars, args) ->
         close_atoms cenv loc args (fun args ->
               cont (AllocTuple (ty, ty_vars, args)))
    | AllocArray (ty, args) ->
         close_atoms cenv loc args (fun args ->
               cont (AllocArray (ty, args)))
    | AllocUnion (ty, ty_vars, ty_var, const_var, args) ->
         close_atoms cenv loc args (fun args ->
               cont (AllocUnion (ty, ty_vars, ty_var, const_var, args)))
    | AllocDTuple (ty, ty_var, tag_var, args) ->
         close_atoms cenv loc args (fun args ->
               cont (AllocDTuple (ty, ty_var, tag_var, args)))
    | AllocVArray (ty, a1, a2) ->
         close_atom cenv loc a1 (fun a1 ->
         close_atom cenv loc a2 (fun a2 ->
               cont (AllocVArray (ty, a1, a2))))
    | AllocRecord (ty, ty_vars, ty_var, fields) ->
         close_field_atoms cenv loc fields (fun fields ->
               cont (AllocRecord (ty, ty_vars, ty_var, fields)))
    | AllocFrame _ ->
         cont op
    | AllocModule (ty, ty_var, fields) ->
         close_table_atoms cenv loc fields (fun fields ->
               cont (AllocModule (ty, ty_var, fields)))

(*
 * Project all the nested frames.
 *)
let rec close_project_frames frames frame pos loc nest e =
   match nest with
      frame' :: nest ->
         let _, ty_frame = frame_type frames pos loc frame' in
         let e = close_project_frames frames frame' pos loc nest e in
            make_exp loc (LetSubscript (frame', ty_frame, AtomVar frame, AtomFrameLabel (frame, frame'), e))
    | [] ->
         e

(*
 * Close an expression.
 *)
let rec close_exp genv venv cenv frames e =
   let pos = string_pos "close_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      close_exp_core genv venv cenv frames pos loc (dest_exp_core e)

and close_exp_core genv venv cenv frames pos loc e : exp =
   match e with
      LetFuns (funs, e) ->
         close_funs_exp genv venv cenv frames pos loc funs e
    | LetAtom (v, ty, a, e) ->
         close_atom_exp genv venv cenv frames pos loc v ty a e
    | LetExt (v, ty, s, ty', ty_args, args, e) ->
         close_extcall_exp genv venv cenv frames pos loc v ty s ty' ty_args args e
    | TailCall (f, args) ->
         close_tailcall_exp genv venv cenv frames pos loc f args
    | LetAlloc (v, op, e) ->
         close_alloc_exp genv venv cenv frames pos loc v op e
    | Match (a, cases) ->
         close_match_exp genv venv cenv frames pos loc a cases
    | MatchDTuple (a, cases) ->
         close_match_dtuple_exp genv venv cenv frames pos loc a cases
    | LetSubscript (v1, ty, v2, a3, e) ->
         close_subscript_exp genv venv cenv frames pos loc v1 ty v2 a3 e
    | SetSubscript (a1, a2, a3, ty, e) ->
         close_set_subscript_exp genv venv cenv frames pos loc a1 a2 ty a3 e
    | SetGlobal (v, ty, a, e) ->
         close_set_global_exp genv venv cenv frames pos loc v ty a e
    | LetApply _
    | LetClosure _
    | Return _
    | Try _
    | Raise _
    | LetRec _ ->
         raise (IRException (pos, StringError "unexpected expression"))

(*
 * Close the function definitions.
 *)
and close_funs_exp genv venv cenv frames pos loc funs e =
   let pos = string_pos "close_funs_exp" pos in
   let funs =
      List.map (fun (f, loc, gflag, ty, tvars, vars, e) ->
            let loc, gflag, ty, tvars, vars, e =
               close_fun genv venv cenv frames f (loc, gflag, ty, tvars, vars, e)
            in
               f, loc, gflag, ty, tvars, vars, e) funs
   in
   let e = close_exp genv venv cenv frames e in
      make_exp loc (LetFuns (funs, e))

and close_fun genv venv cenv frames f def =
   let pos = string_pos "close_fun" (fundef_pos f def) in
   let loc, gflag, tvars, ty, vars, e = def in
   let info = cenv_lookup_fun cenv pos f in
   let { fun_nest = nest;
         fun_uses = uses;
         fun_info = info;
         fun_ty_uses = ty_uses
       } = info
   in

   (* Get the argument types, parameterized by tvars *)
   let ty_vars, ty_res = dest_fun_type genv pos ty in

   (* Get the additional arguments *)
   let ty_vars' = List.map (venv_lookup_var venv pos) uses in
   let ty_vars, vars =
      match gflag with
         FunLocalClass ->
            (*
             * Local functions can include functions that are target of a
             * SpecialCall; they must APPEND the uses to the normal args.
             *)
            let ty_vars = ty_vars @ ty_vars' in
            let vars = vars @ uses in
               ty_vars, vars
       | FunGlobalClass
       | FunTopLevelClass
       | FunPartialClass
       | FunContClass ->
            (*
             * Other functions can escape and in particular may turn up in
             * a LetClosure with their uses given as arguments; they must
             * PREPEND the uses to the normal args.
             *)
            let ty_vars = ty_vars' @ ty_vars in
            let vars = uses @ vars in
               ty_vars, vars
   in
   let gflag =
      if uses = [] then
         match gflag with
            FunGlobalClass
          | FunTopLevelClass
          | FunPartialClass ->
               FunTopLevelClass
          | FunContClass
          | FunLocalClass ->
               gflag
      else
         gflag
   in
   let venv = fold_left2 pos venv_add_var venv vars ty_vars in
   let ty = make_type loc (TyFun (ty_vars, ty_res)) in

   (* Add a function header to the body *)
   let e =
      match gflag, info with
         FunGlobalClass,   GlobalInfo (frame, free)
       | FunTopLevelClass, GlobalInfo (frame, free)
       | FunPartialClass,  GlobalInfo (frame, free) ->
            close_global_fun genv venv cenv frames pos loc nest frame vars ty_vars free e
       | FunContClass, _ ->
            close_cont_fun genv venv cenv frames pos loc nest vars ty_vars e
       | FunLocalClass, _ ->
            close_local_fun genv venv cenv frames pos loc nest e
       | _ ->
            raise (IRException (pos, InternalError))
   in
      loc, gflag, ty_uses @ tvars, ty, vars, e

(*
 * Global functions have to allocate a frame.
 *    1. Allocate the frame
 *    2. Store the parent frame
 *    3. Project all the frames
 *)
and close_global_fun genv venv cenv frames pos loc nest frame vars ty_vars free e =
   let pos = string_pos "close_global_fun" pos in

   (* Add the frame to the environment *)
   let ty_frame_args, ty_frame = frame_type frames pos loc frame in
   let venv = venv_add_var venv frame ty_frame in

   (* Add all the free vars to the environment *)
   let venv = SymbolTable.fold venv_add_var venv free in

   (* Convert the body *)
   let e = close_exp genv venv cenv frames e in

   (* Add all the free vars *)
   let e =
      SymbolTable.fold (fun e v ty ->
            make_exp loc (LetAtom (v, ty, default_atom genv pos ty, e))) e free
   in

   (* Project all the frames *)
   let e = close_project_frames frames frame pos loc nest e in

   (* Store all the arguments *)
   let e =
      fold_left2 pos (fun e v ty ->
            if SymbolTable.mem cenv.close_esc v then
               make_exp loc (SetSubscript (AtomVar frame, AtomFrameLabel (frame, v), AtomVar v, ty, e))
            else
               e) e vars ty_vars
   in

   (* Store the parent frame *)
   let e =
      match nest with
         parent :: _ ->
            let _, ty = frame_type frames pos loc parent in
               make_exp loc (SetSubscript (AtomVar frame, AtomFrameLabel (frame, parent), AtomVar parent, ty, e))
       | [] ->
            e
   in
      (* Allocate the frame *)
      make_exp loc (LetAlloc (frame, AllocFrame (ty_frame, frame, ty_frame_args), e))

(*
 * Continuation projects all the frames.
 *)
and close_cont_fun genv venv cenv frames pos loc nest vars ty_vars e =
   let pos = string_pos "close_cont_fun" pos in

   (* Convert the body *)
   let e = close_exp genv venv cenv frames e in

   (* Project all the frames *)
   let e =
      match nest with
         frame :: nest ->
            close_project_frames frames frame pos loc nest e
       | [] ->
            raise (IRException (pos, InternalError))
   in

   (* Store all the arguments *)
   let e = close_set_vars cenv pos loc vars e in
      e

(*
 * Local function needs no function header.
 *)
and close_local_fun genv venv cenv frames pos loc nest e =
   close_exp genv venv cenv frames e

(*
 * Operator.
 *)
and close_atom_exp genv venv cenv frames pos loc v ty a e =
   let pos = string_pos "close_unop_exp" pos in
   let venv = venv_add_var venv v ty in
      close_atom cenv loc a (fun a ->
            let e = close_exp genv venv cenv frames e in
            let v' = new_symbol v in
            let e = close_set_var cenv pos loc v ty v' e in
            let e = LetAtom (v', ty, a, e) in
               make_exp loc e)

and close_extcall_exp genv venv cenv frames pos loc v ty s ty' ty_args args e =
   let pos = string_pos "close_op_exp" pos in
   let venv = venv_add_var venv v ty in
      close_atoms cenv loc args (fun args ->
            let e = close_exp genv venv cenv frames e in
            let v' = new_symbol v in
            let e = close_set_var cenv pos loc v ty v' e in
            let e = LetExt (v', ty, s, ty', ty_args, args, e) in
               make_exp loc e)

(*
 * Tailcall.
 *)
and close_tailcall_exp genv venv cenv frames pos loc f args =
   let pos = string_pos "close_tailcall" pos in
   let f_var = var_of_atom pos f in
      close_atoms cenv loc args (fun args ->
         try
            let { fun_gflag = gflag;
                  fun_uses = uses
                } = SymbolTable.find cenv.close_funs f_var
            in
            let e =
               if uses = [] then
                  TailCall (f, args)
               else
                  let f' = new_symbol f_var in
                  let vars = List.map (fun v -> AtomVar v) uses in
                  let args =
                     (* See comments in close_fun, above, for details. *)
                     match gflag with
                        FunLocalClass ->
                           args @ vars
                      | FunGlobalClass
                      | FunTopLevelClass
                      | FunPartialClass
                      | FunContClass ->
                           vars @ args
                  in
                     TailCall (f, args)
            in
               make_exp loc e
         with
            Not_found ->
               close_atom cenv loc f (fun f ->
                     make_exp loc (TailCall (f, args))))

(*
 * Malloc.
 *)
and close_alloc_exp genv venv cenv frames pos loc v op e =
   let pos = string_pos "close_alloc_exp" pos in
      close_alloc_op cenv loc op (fun op ->
            make_exp loc (LetAlloc (v, op, close_exp genv venv cenv frames e)))

(*
 * Conditional.
 *)
and close_match_exp genv venv cenv frames pos loc a cases =
   let pos = string_pos "close_match_exp" pos in
      close_atom cenv loc a (fun a ->
            let cases = List.map (fun (s, e) -> s, close_exp genv venv cenv frames e) cases in
               make_exp loc (Match (a, cases)))

(*
 * Tuple matching.
 *)
and close_match_dtuple_exp genv venv cenv frames pos loc a cases =
   let pos = string_pos "close_match_dtuple_exp" pos in
      close_atom cenv loc a (fun a ->
            let cases = List.map (fun (a_opt, e) -> a_opt, close_exp genv venv cenv frames e) cases in
               make_exp loc (MatchDTuple (a, cases)))

(*
 * Subscripting.
 *)
and close_subscript_exp genv venv cenv frames pos loc v1 ty v2 a3 e =
   let pos = string_pos "close_subscript_exp" pos in
   let venv = venv_add_var venv v1 ty in
      close_atom cenv loc v2 (fun v2 ->
      close_atom cenv loc a3 (fun a3 ->
            let e = close_exp genv venv cenv frames e in
            let v1' = new_symbol v1 in
            let e = close_set_var cenv pos loc v1 ty v1' e in
            let e = LetSubscript (v1', ty, v2, a3, e) in
               make_exp loc e))

and close_set_subscript_exp genv venv cenv frames pos loc v1 a2 ty a3 e =
   let pos = string_pos "close_set_subscript_exp" pos in
      close_atom cenv loc v1 (fun v1 ->
      close_atom cenv loc a2 (fun a2 ->
      close_atom cenv loc a3 (fun a3 ->
            let e = close_exp genv venv cenv frames e in
            let e = SetSubscript (v1, a2, a3, ty, e) in
               make_exp loc e)))

and close_set_global_exp genv venv cenv frames pos loc v ty a e =
   let pos = string_pos "close_set_global_exp" pos in
      close_atom cenv loc a (fun a ->
            let e = close_exp genv venv cenv frames e in
            let e = SetGlobal (v, ty, a, e) in
               make_exp loc e)

(*
 * Close the functions in the table.
 *)
let close_funs genv venv cenv frames funs =
   SymbolTable.mapi (close_fun genv venv cenv frames) funs

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Closure conversion.
 *)
let close_prog prog =
   (* Close the parts *)
   let { prog_name = name;
         prog_funs = funs;
         prog_types = types;
         prog_globals = globals';
         prog_tags = tags;
         prog_export = export;
         prog_import = import
       } = prog
   in
   let loc = create_loc name 0 0 0 0 in
   let genv = genv_of_prog prog in

   (* Get all the defs in the program *)
   let fenv, { defs_vars = globals } = defs_funs genv FunTable.empty [] defs_empty funs in

   (* Add the globals already defined *)
   let globals =
      SymbolTable.fold (fun globals v (ty, _) ->
            DefTable.add globals v ty) globals globals'
   in
   let globals =
      SymbolTable.fold (fun globals v { import_type = ty } ->
            DefTable.add globals v ty) globals import
   in
   let globals =
      SymbolTable.fold (fun globals v (ty_var, tyl) ->
            let ty = make_type loc (TyTag (ty_var, tyl)) in
               DefTable.add globals v ty) globals tags
   in

   (* Build the dataflow graph *)
   let globals_list = SymbolTable.fold (fun l v _ -> v :: l) [] globals in
   let globals_list = SymbolTable.fold (fun l v _ -> v :: l) globals_list import in
   let dflow = dflow_funs fenv funs in
   let dflow = dflow_solve_vars globals_list dflow in
   let frame = frame_all genv import globals dflow in
   let pos = string_pos "Frame" (var_exp_pos (Symbol.add "frame")) in
   let esc = frame_esc pos frame globals dflow in
   let frames, dflow = dflow_assign_frames esc dflow in
   let frames = frame_free_vars frames in
   let dflow = dflow_solve_types frames dflow in
   let info = funs_of_dflow dflow in

   (* Add the frames to the type definitions *)
   let types =
      FunTable.fold (fun types frame info ->
            let { frame_vars = vars;
                  frame_fields = fields;
                  frame_parent = parent;
                  frame_loc = loc
                } = info
            in

            (* Parameters *)
            let vars =
               match vars with
                  Some vars ->
                     vars
                | None ->
                     raise (IRException (pos, InternalError))
            in

            (* Add the parent field *)
            let fields =
               match parent with
                  None ->
                     fields
                | Some parent ->
                     let _, ty_frame = frame_type frames pos loc parent in
                        FieldTable.add fields parent (true, ty_frame)
            in
            let tydef = make_tydef_inner loc (TyDefFrame fields) in
            let tydef =
               { tydef_fo_vars = vars;
                 tydef_inner = tydef
               }
            in
               SymbolTable.add types frame tydef) types frames
   in

   (* Close the expressions *)
   let cenv = { close_esc = esc; close_funs = info } in
   let funs = close_funs genv globals cenv frames funs in

   (* Rebuild the program *)
   let prog =
      { prog with prog_types = types;
                  prog_globals = globals';
                  prog_funs = funs
      }
   in
      if debug debug_print_ir then
         debug_prog "Aml_ir_closure (before standardize)" prog;
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
