(*
 * This pass converts function types to existentials.
 * For efficiency, we pass functions as two arguments:
 * a "real" function, and its environment.  When
 * a function is stored in memory, we allocate a
 * pair.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_type

module Pos = MakePos (struct let name = "Aml_ir_fun" end)
open Pos

(*
 * Null environment is used for top-level functions.
 *)
let null_env_var = new_symbol_string "null_env"

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Fenv provides environment names and types for function names.
 *)
type fenv = (atom * ty * atom * ty) SymbolTable.t

let fenv_empty = SymbolTable.empty

let fenv_add_fun = SymbolTable.add

let fenv_lookup_fun_exn = SymbolTable.find

let rec fenv_lookup_atom_exn fenv a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomNil _
    | AtomTyPack _
    | AtomTyUnpack _
    | AtomConst _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomUnop _
    | AtomBinop _ ->
         raise Not_found
    | AtomVar v ->
         fenv_lookup_fun_exn fenv v
    | AtomTyConstrain (a, _)
    | AtomTyApply (a, _, _) ->
         fenv_lookup_atom_exn fenv a

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Convert all function types to existentials.
 *)
let rec build_type_esc ty =
   let pos = string_pos "build_type_esc" (type_exp_pos ty) in
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyVoid
       | TyInt
       | TyChar
       | TyString
       | TyFloat
       | TyVar _
       | TyProject _ ->
            ty

       | TyTuple tyl ->
            make_type loc (TyTuple (build_type_list_esc tyl))
       | TyArray ty ->
            make_type loc (TyArray (build_type_esc ty))

       | TyAll ([], ty)
       | TyExists ([], ty) ->
            build_type_esc ty

       | TyAll (vars, ty) ->
            build_all_type_esc loc vars ty
       | TyFun (ty_args, ty_res) ->
            build_fun_type_esc loc [] ty_args ty_res

       | TyExists (vars, ty) ->
            make_type loc (TyExists (vars, build_type_esc ty))

       | TyApply (ty_var, tyl) ->
            make_type loc (TyApply (ty_var, build_type_list_esc tyl))
       | TyUnion (ty_var, tyl, s) ->
            make_type loc (TyUnion (ty_var, build_type_list_esc tyl, s))
       | TyRecord (ty_var, tyl) ->
            make_type loc (TyRecord (ty_var, build_type_list_esc tyl))
       | TyFrame (ty_var, tyl) ->
            make_type loc (TyFrame (ty_var, build_type_list_esc tyl))
       | TyModule (ty_var, tyl) ->
            make_type loc (TyModule (ty_var, build_type_list_esc tyl))
       | TyTag (ty_var, tyl) ->
            make_type loc (TyTag (ty_var, build_type_list_esc tyl))
       | TyDTuple (ty_var, tyl) ->
            make_type loc (TyDTuple (ty_var, build_type_list_opt_esc tyl))

       | TyExternal (ty, s) ->
            make_type loc (TyExternal (build_type_esc ty, s))
       | TyFormat (ty1, ty2, ty3) ->
            make_type loc (TyFormat (build_type_esc ty1, build_type_esc ty2, build_type_esc ty3))

(*
 * Usually Tyall quantifies a function type.
 * So expand the type until a non-all type is reached.
 *)
and build_all_type_esc loc ty_vars ty =
   match dest_type_core ty with
      TyAll (ty_vars', ty) ->
         build_all_type_esc loc (ty_vars @ ty_vars') ty
    | TyFun (ty_args, ty_res) ->
         build_fun_type_esc loc ty_vars ty_args ty_res
    | _ ->
         make_type loc (TyAll (ty_vars, build_type_esc ty))

(*
 * Build the function type.
 * Recursively convert the types, and wrap in an existential type.
 *)
and build_fun_type_esc loc ty_vars ty_args ty_res =
   let ty_res = build_type_esc ty_res in
   let ty_vars', ty_args = build_type_list_arg ty_args in
   let v_env = new_symbol_string "env" in
   let ty_env = make_type loc (TyVar v_env) in
   let ty_fun = make_type loc (TyFun (ty_env :: ty_args, ty_res)) in
   let ty_fun = make_all_type loc (ty_vars @ ty_vars') ty_fun in
   let ty_prod = make_type loc (TyTuple [ty_fun; ty_env]) in
      make_type loc (TyExists ([v_env], ty_prod))

(*
 * For function arguments, pass the function as multiple arguments.
 *)
and build_type_arg ty_vars ty_args ty =
   let pos = string_pos "build_type_arg" (type_exp_pos ty) in
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyVoid
       | TyInt
       | TyChar
       | TyString
       | TyFloat
       | TyVar _
       | TyProject _ ->
            ty_vars, ty :: ty_args

       | TyTuple tyl ->
            ty_vars, make_type loc (TyTuple (build_type_list_esc tyl)) :: ty_args
       | TyArray ty ->
            ty_vars, make_type loc (TyArray (build_type_esc ty)) :: ty_args

       | TyAll ([], ty)
       | TyExists ([], ty)
       | TyExternal (ty, _) ->
            build_type_arg ty_vars ty_args ty

       | TyAll (vars, ty) ->
            ty_vars, make_type loc (TyAll (vars, build_type_esc ty)) :: ty_args
       | TyFun (ty_args', ty_res) ->
            build_fun_type_arg ty_vars ty_args loc ty_args' ty_res

       | TyExists (vars, ty) ->
            ty_vars, make_type loc (TyExists (vars, build_type_esc ty)) :: ty_args

       | TyApply (ty_var, tyl) ->
            ty_vars, make_type loc (TyApply (ty_var, build_type_list_esc tyl)) :: ty_args
       | TyUnion (ty_var, tyl, s) ->
            ty_vars, make_type loc (TyUnion (ty_var, build_type_list_esc tyl, s)) :: ty_args
       | TyRecord (ty_var, tyl) ->
            ty_vars, make_type loc (TyRecord (ty_var, build_type_list_esc tyl)) :: ty_args
       | TyFrame (ty_var, tyl) ->
            ty_vars, make_type loc (TyFrame (ty_var, build_type_list_esc tyl)) :: ty_args
       | TyModule (ty_var, tyl) ->
            ty_vars, make_type loc (TyModule (ty_var, build_type_list_esc tyl)) :: ty_args
       | TyTag (ty_var, tyl) ->
            ty_vars, make_type loc (TyTag (ty_var, build_type_list_esc tyl)) :: ty_args
       | TyDTuple (ty_var, tyl) ->
            ty_vars, make_type loc (TyDTuple (ty_var, build_type_list_opt_esc tyl)) :: ty_args

       | TyFormat (ty1, ty2, ty3) ->
            ty_vars, make_type loc (TyFormat (build_type_esc ty1, build_type_esc ty2, build_type_esc ty3)) :: ty_args

(*
 * In this final version, we expect the arguments to be named.
 *)
and build_type_param (fenv : fenv) ty_vars ty_args v ty =
   let pos = string_pos "build_type_arg" (type_exp_pos ty) in
   let loc = loc_of_type ty in
      match dest_type_core ty with
         TyVoid
       | TyInt
       | TyChar
       | TyString
       | TyFloat
       | TyVar _
       | TyProject _ ->
            fenv, ty_vars, (v, ty) :: ty_args

       | TyTuple tyl ->
            fenv, ty_vars, (v, make_type loc (TyTuple (build_type_list_esc tyl))) :: ty_args
       | TyArray ty ->
            fenv, ty_vars, (v, make_type loc (TyArray (build_type_esc ty))) :: ty_args

       | TyAll ([], ty)
       | TyExists ([], ty)
       | TyExternal (ty, _) ->
            build_type_param fenv ty_vars ty_args v ty

       | TyAll (vars, ty) ->
            fenv, ty_vars, (v, make_type loc (TyAll (vars, build_type_esc ty))) :: ty_args
       | TyFun (ty_args', ty_res) ->
            build_fun_type_param fenv ty_vars ty_args loc v ty_args' ty_res

       | TyExists (vars, ty) ->
            fenv, ty_vars, (v, make_type loc (TyExists (vars, build_type_esc ty))) :: ty_args

       | TyApply (ty_var, tyl) ->
            fenv, ty_vars, (v, make_type loc (TyApply (ty_var, build_type_list_esc tyl))) :: ty_args
       | TyUnion (ty_var, tyl, s) ->
            fenv, ty_vars, (v, make_type loc (TyUnion (ty_var, build_type_list_esc tyl, s))) :: ty_args
       | TyRecord (ty_var, tyl) ->
            fenv, ty_vars, (v, make_type loc (TyRecord (ty_var, build_type_list_esc tyl))) :: ty_args
       | TyFrame (ty_var, tyl) ->
            fenv, ty_vars, (v, make_type loc (TyFrame (ty_var, build_type_list_esc tyl))) :: ty_args
       | TyModule (ty_var, tyl) ->
            fenv, ty_vars, (v, make_type loc (TyModule (ty_var, build_type_list_esc tyl))) :: ty_args
       | TyTag (ty_var, tyl) ->
            fenv, ty_vars, (v, make_type loc (TyTag (ty_var, build_type_list_esc tyl))) :: ty_args
       | TyDTuple (ty_var, tyl) ->
            fenv, ty_vars, (v, make_type loc (TyDTuple (ty_var, build_type_list_opt_esc tyl))) :: ty_args

       | TyFormat (ty1, ty2, ty3) ->
            fenv, ty_vars, (v, make_type loc (TyFormat (build_type_esc ty1, build_type_esc ty2, build_type_esc ty3))) :: ty_args

(*
 * Build the function type as an argument.
 * In this case, split the argument list in two.
 *)
and build_fun_type_arg ty_vars ty_args loc ty_args' ty_res =
   let ty_res = build_type_esc ty_res in
   let ty_vars', ty_args' = build_type_list_arg ty_args' in
   let v_env = new_symbol_string "env" in
   let ty_env = make_type loc (TyVar v_env) in
   let ty_fun = make_type loc (TyFun (ty_env :: ty_args', ty_res)) in
   let ty_fun = make_all_type loc ty_vars' ty_fun in
   let ty_vars = v_env :: ty_vars in
   let ty_args = ty_env :: ty_fun :: ty_args in
      ty_vars, ty_args

(*
 * Build the function type as a name parameter.
 * In this case, split the argument list in two.
 *)
and build_fun_type_param fenv ty_vars ty_args loc v ty_args' ty_res =
   let ty_res = build_type_esc ty_res in
   let ty_vars', ty_args' = build_type_list_arg ty_args' in
   let v_env = new_symbol_string (Symbol.to_string v ^ "_env") in
   let ty_env = make_type loc (TyVar v_env) in
   let ty_fun = make_type loc (TyFun (ty_env :: ty_args', ty_res)) in
   let ty_fun = make_all_type loc ty_vars' ty_fun in
   let ty_vars = v_env :: ty_vars in
   let ty_args = (v_env, ty_env) :: (v, ty_fun) :: ty_args in
   let fenv = fenv_add_fun fenv v (AtomVar v, ty_fun, AtomVar v_env, ty_env) in
      fenv, ty_vars, ty_args

(*
 * Build types.
 *)
and build_type_list_esc tyl =
   List.map build_type_esc tyl

and build_type_list_opt_esc tyl_opt =
   match tyl_opt with
      Some tyl -> Some (build_type_list_esc tyl)
    | None -> None

and build_type_list_arg tyl =
   let ty_vars, ty_args =
      List.fold_left (fun (ty_vars, ty_args) ty ->
            build_type_arg ty_vars ty_args ty) ([], []) tyl
   in
   let ty_vars = List.rev ty_vars in
   let ty_args = List.rev ty_args in
      ty_vars, ty_args

and build_type_list_param venv fenv pos vars tyl =
   let pos = string_pos "build_type_list_param" pos in
   let len1 = List.length tyl in
   let len2 = List.length vars in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, ArityMismatch (len1, len2)))
   in
   let fenv, ty_vars, ty_args =
      List.fold_left2 (fun (fenv, ty_vars, ty_args) v ty ->
            build_type_param fenv ty_vars ty_args v ty) (fenv, [], []) vars tyl
   in
   let ty_vars = List.rev ty_vars in
   let ty_args = List.rev ty_args in
   let vars, ty_args = List.split ty_args in
   let venv = List.fold_left2 venv_add_var venv vars ty_args in
      venv, fenv, ty_vars, vars, ty_args

(*
 * Outermost function type.
 * In this case, the environment is already the first argument.
 * For each function argument, we add the extra environment parameter.
 *)
let build_type_fun genv venv fenv pos loc ty_vars vars ty =
   let pos = string_pos "build_type_fun" pos in
   let ty_args, ty_res = dest_fun_type genv pos ty in
   let ty_res = build_type_esc ty_res in
   let venv, fenv, ty_vars', vars, ty_args = build_type_list_param venv fenv pos vars ty_args in
   let ty_vars = ty_vars @ ty_vars' in
   let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
      venv, fenv, ty_vars, vars, ty_fun

(*
 * This is also an outermost function type.
 *)
let build_type_closure genv venv fenv pos loc ty =
   let pos = string_pos "build_type_closure" pos in
   let ty_vars, ty_args, ty_res = dest_all_fun_type genv pos ty in
   let ty_vars', ty_args = build_type_list_arg ty_args in
   let ty_res = build_type_esc ty_res in
   let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
   let ty_fun = make_all_type loc (ty_vars @ ty_vars') ty_fun in
      ty_fun

(*
 * Build the expanded type.
 *)
let build_closure_fun_type genv venv fenv pos loc v ty =
   let pos = string_pos "build_closure_fun_type" pos in
   let ty_vars, ty_args, ty_res = dest_all_fun_type genv pos ty in
   let ty_res = build_type_esc ty_res in
   let ty_vars', ty_args = build_type_list_arg ty_args in
   let ty_env = make_type loc (TyProject (v, 0)) in
   let ty_fun = make_type loc (TyFun (ty_env :: ty_args, ty_res)) in
   let ty_fun = make_all_type loc (ty_vars @ ty_vars') ty_fun in
      ty_fun

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Record type.
 *)
let build_record_tydef fields =
   FieldTable.map (fun (mflag, ty) ->
         mflag, build_type_esc ty) fields

(*
 * Union type.
 *)
let build_union_tydef fields =
   FieldTable.map build_type_list_esc fields

(*
 * Module type.
 *)
let build_module_tydef names fields =
   let fields = SymbolTable.map build_type_esc fields in
      names, fields

(*
 * General type definition.
 *)
let build_tydef tydef =
   let { tydef_inner = inner } = tydef in
   let loc = loc_of_tydef_inner inner in
   let inner =
      match dest_tydef_inner_core inner with
         TyDefLambda ty ->
            TyDefLambda (build_type_esc ty)
       | TyDefUnion fields ->
            TyDefUnion (build_union_tydef fields)
       | TyDefRecord fields ->
            TyDefRecord (build_record_tydef fields)
       | TyDefFrame fields ->
            TyDefFrame (build_record_tydef fields)
       | TyDefModule (names, fields) ->
            let names, fields = build_module_tydef names fields in
               TyDefModule (names, fields)
       | TyDefDTuple v ->
            TyDefDTuple v
   in
      { tydef with tydef_inner = make_tydef_inner loc inner }

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Coerce an atom that is escaping.
 * If the atom is a function, a closure is allocated.
 *)
let build_var_esc (fenv : fenv) loc v cont =
   try
      let a_fun, ty_fun, a_env, ty_env = fenv_lookup_fun_exn fenv v in
      let v_closure = new_symbol_string (Symbol.to_string v ^ "_closure") in
      let ty_closure = make_type loc (TyTuple [ty_fun; ty_env]) in
      let e = cont v_closure in
         make_exp loc (LetAlloc (v_closure, AllocTuple ([], ty_closure, [a_fun; a_env]), e))
   with
      Not_found ->
         cont v

let rec build_atom_esc fenv loc a cont =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomTyUnpack _ ->
         cont a
    | AtomVar v ->
         build_var_esc fenv loc v (fun v ->
               cont (AtomVar v))
    | AtomNil ty ->
         cont (AtomNil (build_type_esc ty))
    | AtomTyConstrain (a, ty) ->
         build_atom_esc fenv loc a (fun a ->
               cont (AtomTyConstrain (a, build_type_esc ty)))
    | AtomTyApply (a, ty, tyl) ->
         build_atom_esc fenv loc a (fun a ->
               cont (AtomTyApply (a, build_type_esc ty, build_type_list_esc tyl)))
    | AtomTyPack (v, ty, tyl) ->
         cont (AtomTyPack (v, build_type_esc ty, build_type_list_esc tyl))
    | AtomConst (ty, ty_var, const_var) ->
         cont (AtomConst (build_type_esc ty, ty_var, const_var))
    | AtomUnop (op, a) ->
         build_atom_esc fenv loc a (fun a ->
               cont (AtomUnop (op, a)))
    | AtomBinop (op, a1, a2) ->
         build_atom_esc fenv loc a1 (fun a1 ->
         build_atom_esc fenv loc a2 (fun a2 ->
               cont (AtomBinop (op, a1, a2))))

let build_atom_list_esc fenv loc atoms cont =
   let rec collect atoms' atoms =
      match atoms with
         a :: atoms ->
            build_atom_esc fenv loc a (fun a ->
                  collect (a :: atoms') atoms)
       | [] ->
            cont (List.rev atoms')
   in
      collect [] atoms

(*
 * In the arg version, we expand the atom arguments.
 *)
let build_var_arg args (fenv : fenv) loc v cont =
   try
      let a_fun, _, a_env, _ = fenv_lookup_fun_exn fenv v in
         cont (a_env :: a_fun :: args)
   with
      Not_found ->
         cont (AtomVar v :: args)

(*
 * Type application.
 *)
let build_ty_apply_arg args fenv loc v ty tyl cont =
   let tyl = build_type_list_esc tyl in
   let ty_vars, ty_args = build_type_arg [] [] ty in
   let ty_vars = List.rev ty_vars in
   let ty_args = List.rev ty_args in
   let ty = List.hd ty_args in
      try
         let a_fun, _, a_env, _ = fenv_lookup_fun_exn fenv v in
         let a_fun = AtomTyApply (a_fun, ty, tyl) in
            cont (a_env :: a_fun :: args)
      with
         Not_found ->
            cont (AtomTyApply (AtomVar v, ty, tyl) :: args)

let rec build_atom_arg args fenv loc a cont =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _
    | AtomFrameLabel _
    | AtomRecordLabel _
    | AtomModuleLabel _
    | AtomTyUnpack _ ->
         cont (a :: args)
    | AtomVar v ->
         build_var_arg args fenv loc v cont
    | AtomNil ty ->
         cont (AtomNil (build_type_esc ty) :: args)
    | AtomTyApply (AtomVar v, ty, tyl) ->
         build_ty_apply_arg args fenv loc v ty tyl cont
    | AtomTyConstrain (a, _)
    | AtomTyApply (a, _, _) ->
         build_atom_arg args fenv loc a cont
    | AtomTyPack (v, ty, tyl) ->
         cont (AtomTyPack (v, build_type_esc ty, build_type_list_esc tyl) :: args)
    | AtomConst (ty, ty_var, const_var) ->
         cont (AtomConst (build_type_esc ty, ty_var, const_var) :: args)
    | AtomUnop (op, a) ->
         build_atom_esc fenv loc a (fun a ->
               cont (AtomUnop (op, a) :: args))
    | AtomBinop (op, a1, a2) ->
         build_atom_esc fenv loc a1 (fun a1 ->
         build_atom_esc fenv loc a2 (fun a2 ->
               cont (AtomBinop (op, a1, a2) :: args)))

let build_atom_list_arg fenv loc atoms cont =
   let rec collect args atoms =
      match atoms with
         a :: atoms ->
            build_atom_arg args fenv loc a (fun args ->
                  collect args atoms)
       | [] ->
            cont (List.rev args)
   in
      collect [] atoms

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Record fields.
 *)
let build_record_fields fenv loc fields cont =
   let rec collect fields' fields =
      match fields with
         (label, a) :: fields ->
            build_atom_esc fenv loc a (fun a ->
                  collect ((label, a) :: fields') fields)
       | [] ->
            cont (List.rev fields')
   in
      collect [] fields

(*
 * Module fields are a little harder because the SymbolTable
 * doesn't support the kind of recursion.
 *)
let build_module_fields fenv loc fields cont =
   let rec collect fields' fields =
      match fields with
         (v, a) :: fields ->
            build_atom_esc fenv loc a (fun a ->
                  collect (SymbolTable.add fields' v a) fields)
       | [] ->
            cont fields'
   in
   let fields = SymbolTable.fold (fun fields v a -> (v, a) :: fields) [] fields in
      collect SymbolTable.empty fields

(*
 * Coerce an allocation.
 * All the vars in the block escape.
 *)
let build_alloc_op fenv loc op cont =
   match op with
      AllocTuple (ty_vars, ty, args) ->
         let ty = build_type_esc ty in
            build_atom_list_esc fenv loc args (fun args ->
                  cont (AllocTuple (ty_vars, ty, args)))
    | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
         let ty = build_type_esc ty in
            build_atom_list_esc fenv loc args (fun args ->
                  cont (AllocUnion (ty_vars, ty, ty_var, const_var, args)))
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         let ty = build_type_esc ty in
            build_record_fields fenv loc fields (fun fields ->
                  cont (AllocRecord (ty_vars, ty, ty_var, fields)))
    | AllocDTuple (ty, ty_var, tag_var, args) ->
         let ty = build_type_esc ty in
            build_atom_list_esc fenv loc args (fun args ->
                  cont (AllocDTuple (ty, ty_var, tag_var, args)))
    | AllocArray (ty, args) ->
         let ty = build_type_esc ty in
            build_atom_list_esc fenv loc args (fun args ->
                  cont (AllocArray (ty, args)))
    | AllocVArray (ty, a1, a2) ->
         let ty = build_type_esc ty in
            build_atom_esc fenv loc a1 (fun a1 ->
            build_atom_esc fenv loc a2 (fun a2 ->
                  cont (AllocVArray (ty, a1, a2))))
    | AllocModule (ty, ty_var, fields) ->
         let ty = build_type_esc ty in
            build_module_fields fenv loc fields (fun fields ->
                  cont (AllocModule (ty, ty_var, fields)))
    | AllocFrame (ty, ty_var, tyl) ->
         cont (AllocFrame (build_type_esc ty, ty_var, List.map build_type_esc tyl))

(*
 * Now coerce an expression.
 *)
let rec build_exp (genv : genv) (venv : venv) (fenv : fenv) e =
   let pos = string_pos "build_exp genv venv" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, a, e) ->
            build_atom_exp genv venv fenv pos loc v ty a e
       | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
            build_ext_exp genv venv fenv pos loc v ty1 s ty2 ty_args args e
       | TailCall (a, args) ->
            build_tailcall_exp genv venv fenv pos loc a args
       | Match (a, cases) ->
            build_match_exp genv venv fenv pos loc a cases
       | MatchDTuple (a, cases) ->
            build_match_dtuple_exp genv venv fenv pos loc a cases
       | LetAlloc (v, op, e) ->
            build_alloc_exp genv venv fenv pos loc v op e
       | SetSubscript (a1, a2, a3, ty, e) ->
            build_set_subscript_exp genv venv fenv pos loc a1 a2 a3 ty e
       | LetSubscript (v, ty, a1, a2, e) ->
            build_subscript_exp genv venv fenv pos loc v ty a1 a2 e
       | SetGlobal (v, ty, a, e) ->
            build_set_global_exp genv venv fenv pos loc v ty a e
       | LetRec (fields, e) ->
            build_letrec_exp genv venv fenv pos loc fields e
       | LetClosure (v, f, ty_args, args, e) ->
            build_closure_exp genv venv fenv pos loc v f ty_args args e
       | LetFuns _
       | LetApply _
       | Return _
       | Try _
       | Raise _ ->
            raise (IRException (pos, InternalError))

(*
 * Atoms.
 *)
and build_atom_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc v ty a e =
   let pos = string_pos "build_atom_exp" pos in
   let ty = build_type_esc ty in
      try
         (* If this is a function, remember the environment *)
         let a_fun, ty_fun, a_env, ty_env = fenv_lookup_atom_exn fenv a in
         let fenv = fenv_add_fun fenv v (a_fun, ty_fun, a_env, ty_env) in
         let e = build_exp genv venv fenv e in
            make_exp loc (LetAtom (v, ty_fun, a_fun, e))
      with
         Not_found ->
            build_atom_esc fenv loc a (fun a ->
                  let e = build_exp genv venv fenv e in
                     make_exp loc (LetAtom (v, ty, a, e)))

(*
 * External call.
 *)
and build_ext_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc v ty1 s ty2 ty_args args e =
   let pos = string_pos "build_ext_exp" pos in
   let ty1 = build_type_esc ty1 in
   let ty2 = build_type_closure genv venv fenv pos loc ty2 in
   let ty_args = build_type_list_esc ty_args in
      build_atom_list_esc fenv loc args (fun args ->
            let e = build_exp genv venv fenv e in
               make_exp loc (LetExt (v, ty1, s, ty2, ty_args, args, e)))

(*
 * Tailcall.
 *)
and build_tailcall_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc a args =
   let pos = string_pos "build_tailcall_exp" pos in
      build_atom_list_arg fenv loc args (fun args ->
            try
               let a_fun, _, a_env, ty_env = fenv_lookup_atom_exn fenv a in
               let args = a_env :: args in
                  make_exp loc (TailCall (a_fun, args))
            with
               Not_found ->
                  make_exp loc (TailCall (a, args)))

(*
 * Match.
 *)
and build_match_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc a cases =
   let pos = string_pos "build_match_exp" pos in
      build_atom_esc fenv loc a (fun a ->
            let cases = List.map (fun (s, e) -> s, build_exp genv venv fenv e) cases in
               make_exp loc (Match (a, cases)))

(*
 * Tuple matching.
 *)
and build_match_dtuple_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc a cases =
   let pos = string_pos "build_match_dtuple_exp" pos in
      build_atom_esc fenv loc a (fun a ->
            let cases = List.map (fun (a_opt, e) -> a_opt, build_exp genv venv fenv e) cases in
               make_exp loc (MatchDTuple (a, cases)))

(*
 * Allocation.
 *)
and build_alloc_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc v op e =
   let pos = string_pos "build_alloc_exp" pos in
      build_alloc_op fenv loc op (fun op ->
            let e = build_exp genv venv fenv e in
               make_exp loc (LetAlloc (v, op, e)))

(*
 * Subscripting.
 *)
and build_set_subscript_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc a1 a2 a3 ty e =
   let pos = string_pos "build_set_subscript_exp" pos in
      build_atom_esc fenv loc a1 (fun a1 ->
      build_atom_esc fenv loc a2 (fun a2 ->
      build_atom_esc fenv loc a3 (fun a3 ->
            let ty = build_type_esc ty in
            let e = build_exp genv venv fenv e in
               make_exp loc (SetSubscript (a1, a2, a3, ty, e)))))

and build_subscript_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc v ty_orig a1 a2 e =
   let pos = string_pos "build_subscript_exp" pos in
   let ty = build_type_esc ty_orig in
      build_atom_esc fenv loc a1 (fun a1 ->
      build_atom_esc fenv loc a2 (fun a2 ->
            if is_fun_type genv pos ty_orig then
               let v_closure = new_symbol_string "closure" in
               let v_env = new_symbol_string (Symbol.to_string v ^ "_env") in

               (* Build the unwrapped types *)
               let ty_env = make_type loc (TyProject (v_closure, 0)) in
               let ty_fun = build_closure_fun_type genv venv fenv pos loc v_closure ty_orig in

               (* Add to the fenv *)
               let fenv = fenv_add_fun fenv v (AtomVar v, ty_fun, AtomVar v_env, ty_env) in

               (* Build the rest, and project *)
               let e = build_exp genv venv fenv e in
               let e = make_exp loc (LetSubscript (v_env, ty_env, AtomVar v_closure, AtomInt 1, e)) in
               let e = make_exp loc (LetSubscript (v, ty_fun, AtomVar v_closure, AtomInt 0, e)) in
               let e = make_exp loc (LetSubscript (v_closure, ty, a1, a2, e)) in
                  e
            else
               let e = build_exp genv venv fenv e in
                  make_exp loc (LetSubscript (v, ty, a1, a2, e))))

(*
 * Global assignment.
 *)
and build_set_global_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc v ty a e =
   let pos = string_pos "build_global_exp" pos in
   let ty = build_type_esc ty in
      build_atom_esc fenv loc a (fun a ->
            let e = build_exp genv venv fenv e in
               make_exp loc (SetGlobal (v, ty, a, e)))

(*
 * LetRec definitions.
 *)
and build_letrec_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc fields e =
   let pos = string_pos "build_letrec_exp" pos in
   let fields = List.map (fun (v, e, ty) -> v, build_exp genv venv fenv e, build_type_esc ty) fields in
   let e = build_exp genv venv fenv e in
      make_exp loc (LetRec (fields, e))

(*
 * Closure creation is really a tuple allocation
 * of the environment and the function.
 *)
and build_closure_exp (genv : genv) (venv : venv) (fenv : fenv) pos loc v f ty_args args e =
   let pos = string_pos "build_closure_exp" pos in
      match args with
         [a] ->
            (* Don't do anything, just add the closure *)
            let ty_fun = type_of_atom genv venv pos loc f in
            let ty_arg = type_of_atom genv venv pos loc a in
            let ty_fun = build_type_closure genv venv fenv pos loc ty_fun in
            let ty_arg = build_type_esc ty_arg in
            let fenv = fenv_add_fun fenv v (f, ty_fun, a, ty_arg) in
            let e = build_exp genv venv fenv e in
               make_exp loc (LetAtom (v, ty_fun, f, e))

       | _ ->
            raise (IRException (pos, StringIntError ("closure does not have exactly one argument", List.length args)))

(************************************************************************
 * CONVERT THE PROGRAM
 ************************************************************************)

(*
 * Initializer.
 *)
let build_init init =
   match init with
      InitString _ ->
         init
    | InitAtom a ->
         InitAtom a
    | InitAlloc op ->
         InitAlloc op

(*
 * Convert partial applications in the program.
 *)
let fun_prog prog =
   let { prog_name = name;
         prog_types = types;
         prog_globals = globals;
         prog_tags = tags;
         prog_import = import;
         prog_export = export;
         prog_funs = funs
       } = prog
   in
   let file_loc = create_loc name 0 0 0 0 in

   (* Convert the outermost types *)
   let types = SymbolTable.map build_tydef types in
   let globals =
      SymbolTable.map (fun (ty, init) ->
            build_type_esc ty, init) globals
   in
   let import =
      SymbolTable.map (fun import ->
            { import with import_type = build_type_esc import.import_type }) import
   in

   (* Build environments *)
   let genv = genv_of_prog prog in
   let venv = venv_all_of_prog genv prog in
   let fenv = fenv_empty in

   (* Convert the normal functions *)
   let funs =
      SymbolTable.mapi (fun f def ->
            let pos = string_pos "fun_prog.build_fun" (fundef_pos f def) in
            let loc, gflag, ty_vars, ty, vars, e = def in
            let venv, fenv, ty_vars, vars, ty = build_type_fun genv venv fenv pos loc ty_vars vars ty in
            let e = build_exp genv venv fenv e in
            let def = loc, gflag, ty_vars, ty, vars, e in
               def) funs
   in

   (* Convert the globals *)
   let globals =
      SymbolTable.mapi (fun v (ty, init) ->
            let init = build_init init in
               ty, init) globals
   in

   (* Convert the tags *)
   let tags =
      SymbolTable.map (fun (ty_var, tyl) ->
            let tyl = build_type_list_esc tyl in
               ty_var, tyl) tags
   in

   (* We export closures, not the original functions *)
   let export =
      SymbolTable.fold (fun export v { export_type = ty; export_name = name } ->
            let pos = string_pos "fun_prog.build_export" (var_exp_pos v) in
            let ty =
               if is_fun_type genv pos ty then
                  build_type_closure genv venv fenv pos file_loc ty
               else
                  build_type_esc ty
            in
            let info = { export_type = ty; export_name = name } in
               SymbolTable.add export v info) SymbolTable.empty export
   in

   (* Add the empty environment *)
   let ty_null_env = make_type file_loc (TyTuple []) in
   let globals = SymbolTable.add globals null_env_var (ty_null_env, InitAlloc (AllocTuple ([], ty_null_env, []))) in

   (* Rebuild the prog *)
   let prog =
      { prog with prog_types = types;
                  prog_globals = globals;
                  prog_tags = tags;
                  prog_import = import;
                  prog_export = export;
                  prog_funs = funs
      }
   in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
