(*
 * This file defines utilities on types.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_tydef

module Pos = MakePos (struct let name = "Aml_ir_type" end)
open Pos

(************************************************************************
 * TYPE EXPANSION
 ************************************************************************)

(*
 * Expand a type, removing the outermost TyProject if possible.
 *)
let rec expand_type genv pos ty =
   let pos = string_pos "expand_type" pos in
      match dest_type_core ty with
         TyApply (ty_var, tyl) ->
            apply_apply_type genv pos ty_var tyl
       | TyAll ([], ty) ->
            expand_type genv pos ty
       | _ ->
            ty

let expand_type_core genv pos ty =
   dest_type_core (expand_type genv pos ty)

(************************************************************************
 * PREDICATES
 ************************************************************************)

(*
 * Tell if something is a function type.
 *)
let rec is_fun_type genv pos ty =
   let pos = string_pos "is_fun_type" pos in
      match expand_type_core genv pos ty with
         TyAll (_, ty)
       | TyExternal (ty, _) ->
            is_fun_type genv pos ty
       | TyFun _ ->
            true
       | _ ->
            false

(*
 * Tell if something is a frame type.
 *)
let rec is_frame_type genv pos ty =
   match expand_type_core genv pos ty with
      TyAll (_, ty) ->
         is_frame_type genv pos ty
    | TyFrame _ ->
         true
    | _ ->
         false

(************************************************************************
 * TYPE DESTRUCTORS
 ************************************************************************)

(*
 * Plain type destructors.
 *)
let rec dest_all_type genv pos ty =
   match expand_type_core genv pos ty with
      TyAll (vars1, ty) ->
         let vars2, ty = dest_all_type genv pos ty in
            vars1 @ vars2, ty
    | _ ->
         [], ty

let rec dest_exists_type genv pos ty =
   match expand_type_core genv pos ty with
      TyExists (vars1, ty) ->
         let vars2, ty = dest_exists_type genv pos ty in
            vars1 @ vars2, ty
    | _ ->
         [], ty

let rec dest_all_fun_type genv pos ty =
   let pos = string_pos "dest_all_fun_type" pos in
      match expand_type_core genv pos ty with
         TyAll (vars1, ty) ->
            let vars2, tyl1, ty2 = dest_all_fun_type genv pos ty in
               vars1 @ vars2, tyl1, ty2
       | TyFun (tyl1, ty2) ->
            [], tyl1, ty2
       | TyExternal (ty, _) ->
            dest_all_fun_type genv pos ty
       | _ ->
            raise (IRException (pos, StringTypeError ("not a function type", ty)))

let rec dest_fun_type genv pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match expand_type_core genv pos ty with
         TyFun (tyl1, ty2) ->
            tyl1, ty2
       | TyExternal (ty, _) ->
            dest_fun_type genv pos ty
       | _ ->
            raise (IRException (pos, StringTypeError ("not a function type", ty)))

let dest_array_type genv pos ty =
   let pos = string_pos "dest_array_type" pos in
      match expand_type_core genv pos ty with
         TyArray ty ->
            ty
       | _ ->
            raise (IRException (pos, StringTypeError ("not an array type", ty)))

let dest_tuple_type genv pos ty =
   let pos = string_pos "dest_tuple_type" pos in
      match expand_type_core genv pos ty with
         TyTuple tyl ->
            tyl
       | _ ->
            raise (IRException (pos, StringTypeError ("not an tuple type", ty)))

let dest_dtuple_type genv pos ty =
   let pos = string_pos "dest_dtuple_type" pos in
      match expand_type_core genv pos ty with
         TyDTuple (ty_var, tyl_opt) ->
            ty_var, tyl_opt
       | _ ->
            raise (IRException (pos, StringTypeError ("not an dtuple type", ty)))

let dest_tag_type genv pos ty =
   let pos = string_pos "dest_tag_type" pos in
      match expand_type_core genv pos ty with
         TyTag (ty_var, tyl) ->
            ty_var, tyl
       | _ ->
            raise (IRException (pos, StringTypeError ("not a tag type", ty)))

let dest_pair_type genv pos ty =
   let pos = string_pos "dest_pair_type" pos in
      match dest_tuple_type genv pos ty with
         [ty1; ty2] ->
            ty1, ty2
       | _ ->
            raise (IRException (pos, StringTypeError ("not an pair type", ty)))

(*
 * Rename the quantified vars in the universal type.
 *)
let rename_all_vars genv pos ty ty_vars =
   let loc = loc_of_type ty in
   let ty_params, ty = dest_all_type genv pos ty in
   let len1 = List.length ty_params in
   let len2 = List.length ty_vars in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, TypeArityMismatch (ty, len1, len2)))
   in
   let subst =
      List.fold_left2 (fun subst v1 v2 ->
            subst_add_fo_var subst v1 (make_type loc (TyVar v2))) empty_subst ty_params ty_vars
   in
      make_type loc (TyAll (ty_vars, subst_type subst ty))

(************************************************************************
 * CONSTRUCTORS
 ************************************************************************)

(*
 * These are just handy utilites.
 *)
let make_all_type loc vars ty =
   match vars with
      [] ->
         ty
    | _ ->
         make_type loc (TyAll (vars, ty))

let make_ty_apply_atom a ty tyl =
   match tyl with
      [] ->
         a
    | _ ->
         AtomTyApply (a, ty, tyl)

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Type of unary operations.
 *)
let type_of_unop genv loc op =
   match op with
      (* Boolean *)
      NotBoolOp ->
         let ty_bool = genv_ty_bool genv loc in
            ty_bool, ty_bool

      (* Arithmetic *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp ->
         let ty_int = make_type loc TyInt in
            ty_int, ty_int

      (* Floats *)
    | UMinusFloatOp
    | AbsFloatOp
    | SqrtFloatOp
    | ExpFloatOp
    | LogFloatOp
    | Log10FloatOp
    | CosFloatOp
    | SinFloatOp
    | TanFloatOp
    | ACosFloatOp
    | ASinFloatOp
    | ATanFloatOp
    | CosHFloatOp
    | SinHFloatOp
    | TanHFloatOp
    | CeilFloatOp
    | FloorFloatOp ->
         let ty_float = make_type loc TyFloat in
            ty_float, ty_float

      (* Coercions *)
    | IntOfCharOp ->
         let ty_int = make_type loc TyInt in
         let ty_char = make_type loc TyChar in
            ty_int, ty_char
    | CharOfIntOp ->
         let ty_int = make_type loc TyInt in
         let ty_char = make_type loc TyChar in
            ty_char, ty_int
    | IntOfFloatOp ->
         let ty_int = make_type loc TyInt in
         let ty_float = make_type loc TyFloat in
            ty_int, ty_float
    | FloatOfIntOp ->
         let ty_int = make_type loc TyInt in
         let ty_float = make_type loc TyFloat in
            ty_float, ty_int

      (* Block operations *)
    | LengthOfStringOp ->
         let ty_string = make_type loc TyString in
         let ty_int = make_type loc TyInt in
            ty_int, ty_string

    | LengthOfBlockOp ty ->
         let ty_int = make_type loc TyInt in
            ty_int, ty

(*
 * Type of binary operations.
 *)
let type_of_binop genv loc op =
   match op with
      (* Non short-circuit Booleans *)
      OrBoolOp
    | AndBoolOp ->
         let ty_bool = genv_ty_bool genv loc in
            ty_bool, ty_bool, ty_bool

      (* Comparisons on ML ints *)
    | EqCharOp
    | NeqCharOp
    | LtCharOp
    | LeCharOp
    | GtCharOp
    | GeCharOp ->
         let ty_char = make_type loc TyChar in
         let ty_bool = genv_ty_bool genv loc in
            ty_bool, ty_char, ty_char

    | CmpCharOp ->
         let ty_char = make_type loc TyChar in
         let ty_int = make_type loc TyInt in
            ty_int, ty_char, ty_char

    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp
    | CmpIntOp ->
         let ty_int = make_type loc TyInt in
            ty_int, ty_int, ty_int

      (* Comparisons on ML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp ->
         let ty_int = make_type loc TyInt in
         let ty_bool = genv_ty_bool genv loc in
            ty_bool, ty_int, ty_int

      (* Standard binary operations on floats *)
    | PlusFloatOp
    | MinusFloatOp
    | MulFloatOp
    | DivFloatOp
    | RemFloatOp
    | MaxFloatOp
    | MinFloatOp
    | ATan2FloatOp
    | PowerFloatOp ->
         let ty_float = make_type loc TyFloat in
            ty_float, ty_float, ty_float

    | LdExpFloatIntOp ->
         let ty_int = make_type loc TyInt in
         let ty_float = make_type loc TyFloat in
            ty_float, ty_float, ty_int

      (* Comparisons on floats *)
    | EqFloatOp
    | NeqFloatOp
    | LtFloatOp
    | LeFloatOp
    | GtFloatOp
    | GeFloatOp ->
         let ty_float = make_type loc TyFloat in
         let ty_bool = genv_ty_bool genv loc in
            ty_bool, ty_float, ty_float

    | CmpFloatOp ->
         let ty_int = make_type loc TyInt in
         let ty_float = make_type loc TyFloat in
            ty_int, ty_float, ty_float

      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty
    | NeqEqOp ty ->
         let ty_bool = genv_ty_bool genv loc in
            ty_bool, ty, ty

(*
 * Get the unpacked type.
 *)
let unpack_type genv pos v ty =
   let pos = string_pos "unpack_type" pos in
   let loc = loc_of_type ty in
   let vars, ty = dest_exists_type genv pos ty in
   let subst, _ =
      List.fold_left (fun (subst, i) v ->
            let ty = make_type loc (TyProject (v, i)) in
            let subst = subst_add_fo_var subst v ty in
               subst, succ i) (empty_subst, 0) vars
   in
      subst_type subst ty

(*
 * Get the type of an atom.
 *)
let type_of_atom genv venv pos loc a =
   let pos = string_pos "type_of_atom" pos in
      match a with
         AtomInt _
       | AtomFrameLabel _
       | AtomRecordLabel _
       | AtomModuleLabel _ ->
            make_type loc TyInt
       | AtomChar _ ->
            make_type loc TyChar
       | AtomFloat _ ->
            make_type loc TyFloat
       | AtomVar v ->
            venv_lookup_var venv pos v
       | AtomTyUnpack v ->
            let ty = venv_lookup_var venv pos v in
               unpack_type genv pos v ty
       | AtomNil ty
       | AtomTyConstrain (_, ty)
       | AtomTyApply (_, ty, _)
       | AtomTyPack (_, ty, _)
       | AtomConst (ty, _, _) ->
            ty
       | AtomUnop (op, _) ->
            let ty, _ = type_of_unop genv loc op in
               ty
       | AtomBinop (op, _, _) ->
            let ty, _, _ = type_of_binop genv loc op in
               ty

(*
 * Atom should be a variable.
 *)
let rec var_of_atom pos a =
   let pos = string_pos "var_of_atom" pos in
      match a with
         AtomInt _
       | AtomChar _
       | AtomFloat _
       | AtomFrameLabel _
       | AtomRecordLabel _
       | AtomModuleLabel _
       | AtomConst _
       | AtomUnop _
       | AtomBinop _
       | AtomNil _ ->
            raise (IRException (pos, StringAtomError ("not a variable", a)))
       | AtomVar v
       | AtomTyPack (v, _, _)
       | AtomTyUnpack v ->
            v
       | AtomTyConstrain (a, _)
       | AtomTyApply (a, _, _) ->
            var_of_atom pos a

(*
 * Type of an allocation operations.
 *)
let type_of_alloc_op op =
   match op with
      AllocTuple (ty_vars, ty, _)
    | AllocUnion (ty_vars, ty, _, _, _)
    | AllocRecord (ty_vars, ty, _, _) ->
         let loc = loc_of_type ty in
            make_type loc (TyAll (ty_vars, ty))
    | AllocDTuple (ty, _, _, _)
    | AllocArray (ty, _)
    | AllocVArray (ty, _, _)
    | AllocFrame (ty, _, _)
    | AllocModule (ty, _, _) ->
         ty

(************************************************************************
 * VARIABLE ENV
 ************************************************************************)

(*
 * Define the types of all the outermost vars.
 *)
let venv_of_prog prog =
   let { prog_globals = globals;
         prog_import = import;
         prog_funs = funs
       } = prog
   in
   let venv = SymbolTable.empty in
   let venv =
      SymbolTable.fold (fun venv v (ty, _) ->
            venv_add_var venv v ty) venv globals
   in
   let venv =
      SymbolTable.fold (fun venv v { import_type = ty } ->
            venv_add_var venv v ty) venv import
   in
   let venv =
      SymbolTable.fold (fun venv v (_, _, ty_vars, ty, _, _) ->
            let loc = loc_of_type ty in
            let ty = make_type loc (TyAll (ty_vars, ty)) in
               venv_add_var venv v ty) venv funs
   in
      venv

(*
 * Get the types of _all_ the variables in the program.
 *)
let rec venv_of_exp genv venv e =
   let pos = string_pos "venv_of_exp" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ty, _, e)
       | LetExt (v, ty, _, _, _, _, e)
       | LetSubscript (v, ty, _, _, e)
       | LetApply (v, ty, _, _, e) ->
            venv_of_exp genv (venv_add_var venv v ty) e
       | TailCall _
       | Return _
       | Raise _ ->
            venv
       | Match (_, cases) ->
            List.fold_left (fun venv (_, e) -> venv_of_exp genv venv e) venv cases
       | MatchDTuple (_, cases) ->
            List.fold_left (fun venv (_, e) -> venv_of_exp genv venv e) venv cases
       | LetAlloc (v, op, e) ->
            venv_of_exp genv (venv_add_var venv v (type_of_alloc_op op)) e
       | SetSubscript (_, _, _, _, e)
       | SetGlobal (_, _, _, e) ->
            venv_of_exp genv venv e
       | LetRec (fields, e) ->
            List.fold_left (fun venv (v, e, ty) ->
                  venv_of_exp genv (venv_add_var venv v ty) e) (venv_of_exp genv venv e) fields
       | LetFuns (funs, e) ->
            List.fold_left (fun venv (f, _, _, ty_vars, ty, vars, e) ->
                  let ty_args, _ = dest_fun_type genv pos ty in
                  let venv = List.fold_left2 venv_add_var venv vars ty_args in
                     venv_of_exp genv venv e) (venv_of_exp genv venv e) funs
       | Try (e1, v, e2) ->
            let venv = venv_add_var venv v (genv_ty_exn genv loc) in
               venv_of_exp genv (venv_of_exp genv venv e1) e2
       | LetClosure (v, f, params, args, e) ->
            let ty_fun = type_of_atom genv venv pos loc f in
            let ty_params, ty_args, ty_res = dest_all_fun_type genv pos ty_fun in

            (* Apply the types *)
            let len1 = List.length params in
            let len2 = List.length ty_params in
            let _ =
               if len1 > len2 then
                  raise (IRException (pos, ArityMismatch (len2, len1)))
            in
            let ty_params1, ty_params2 = Mc_list_util.split len1 ty_params in
            let subst = List.fold_left2 subst_add_fo_var empty_subst ty_params1 params in

            (* Remove the arguments *)
            let len1 = List.length args in
            let len2 = List.length ty_args in
            let _ =
               if len1 >= len2 then
                  raise (IRException (pos, StringError "too many arguments"))
            in
            let ty_args = Mc_list_util.nth_tl len1 ty_args in

            (* The resulting type is whatever remains *)
            let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
            let ty_fun = subst_type subst ty_fun in
            let ty_fun = make_type loc (TyAll (ty_params2, ty_fun)) in
               venv_add_var venv v ty_fun

let venv_all_of_prog genv prog =
   let { prog_funs = funs } = prog in
   let venv = venv_of_prog prog in
   let venv =
      SymbolTable.fold (fun venv f (_, _, ty_vars, ty, vars, e) ->
            let pos = string_pos "venv_all_of_prog" (var_exp_pos f) in
            let ty_args, _ = dest_fun_type genv pos ty in
            let venv = List.fold_left2 venv_add_var venv vars ty_args in
               venv_of_exp genv venv e) venv funs
   in
      venv

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
