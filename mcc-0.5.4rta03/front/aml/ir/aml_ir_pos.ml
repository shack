(*
 * Position information for debugging.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open Fir_state

open Location

open Aml_ir
open Aml_ir_ds
open Aml_ir_exn
open Aml_ir_print

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * The pos type is used to provide info for debugging
 * and error messages.
 *)
type item =
   Exp             of exp
 | String          of string
 | Symbol          of symbol
 | Type            of ty
 | Fundef          of var * gfundef

 | TAstExp         of Aml_tast.exp
 | TAstType        of Aml_tast.ty
 | TAstPatt        of Aml_tast.patt
 | TAstModuleExp   of Aml_tast.module_exp
 | TAstModuleField of Aml_tast.module_field

type pos = item Position.pos

(*
 * General exception includes debugging info.
 *)
exception IRException of pos * ir_error

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the source location for an exception.
 *)
let var_loc = bogus_loc "<Aml_ir_pos>"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_exp e
    | Type ty ->
         loc_of_type ty
    | Symbol _
    | String _ ->
         var_loc
    | Fundef (_, (loc, _, _, _, _, _)) ->
         loc
    | TAstExp e ->
         Aml_tast_ds.loc_of_exp e
    | TAstType ty ->
         Aml_tast_ds.loc_of_type ty
    | TAstPatt p ->
         Aml_tast_ds.loc_of_patt p
    | TAstModuleExp me ->
         Aml_tast_ds.loc_of_module_exp me
    | TAstModuleField field ->
         Aml_tast_ds.loc_of_field field

(*
 * Print debugging info.
 *)
let rec pp_print_value buf x =
   match x with
      Exp e ->
         pp_print_exp buf e

    | Type t ->
         pp_print_type buf t

    | Symbol v ->
         pp_print_symbol buf v

    | String s ->
         pp_print_string buf s

    | Fundef (f, fundef) ->
         pp_print_fundef buf f fundef

    | TAstPatt p ->
         Aml_tast_print.pp_print_patt buf p

    | TAstExp e ->
         Aml_tast_print.pp_print_exp buf e

    | TAstType ty ->
         Aml_tast_print.pp_print_type buf ty

    | TAstModuleExp me ->
         Aml_tast_print.pp_print_module_exp buf me

    | TAstModuleField field ->
         Aml_tast_print.pp_print_module_field buf field

(************************************************************************
 * CONSTRUCTION
 ************************************************************************)

module type PosSig =
sig
   val loc_pos : loc -> pos

   val exp_pos : exp -> pos
   val var_exp_pos : var -> pos
   val type_exp_pos : ty -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos
   val type_pos : ty -> pos -> pos
   val fundef_pos : var -> gfundef -> pos

   val tast_exp_pos   : Aml_tast.exp -> pos
   val tast_type_pos  : Aml_tast.ty -> pos
   val tast_patt_pos  : Aml_tast.patt -> pos
   val tast_me_pos    : Aml_tast.module_exp -> pos
   val tast_field_pos : Aml_tast.module_field -> pos

   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let exp_pos e = base_pos (Exp e)
   let var_exp_pos v = base_pos (Symbol v)
   let type_exp_pos ty = base_pos (Type ty)
   let string_exp_pos s = base_pos (String s)
   let var_pos = symbol_pos
   let type_pos ty pos = cons_pos (Type ty) pos
   let fundef_pos v def = base_pos (Fundef (v, def))

   let tast_exp_pos p = base_pos (TAstExp p)
   let tast_patt_pos p = base_pos (TAstPatt p)
   let tast_type_pos p = base_pos (TAstType p)
   let tast_me_pos me = base_pos (TAstModuleExp me)
   let tast_field_pos field = base_pos (TAstModuleField field)
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
