(*
 * After closure conversion, all functions are closed.
 * Lift them to the top level.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env
open Aml_ir_cps

module Pos = MakePos (struct let name = "Aml_ir_closure" end)
open Pos

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Lift all functions.
 *)
let rec prog_exp funs e =
   let loc = loc_of_exp e in
   let pos = string_pos "prog_exp" (exp_pos e) in
      prog_exp_core funs pos loc (dest_exp_core e)

and prog_exp_core funs pos loc e =
   match e with
      LetAtom (v, ty, a, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (LetAtom (v, ty, a, e))
    | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (LetExt (v, ty1, s, ty2, ty_args, args, e))
    | TailCall _ ->
         funs, make_exp loc e
    | Match (a, cases) ->
         let funs, cases =
            List.fold_left (fun (funs, cases) (s, e) ->
                  let funs, e = prog_exp funs e in
                     funs, (s, e) :: cases) (funs, []) cases
         in
            funs, make_exp loc (Match (a, List.rev cases))
    | MatchDTuple (a, cases) ->
         let funs, cases =
            List.fold_left (fun (funs, cases) (a_opt, e) ->
                  let funs, e = prog_exp funs e in
                     funs, (a_opt, e) :: cases) (funs, []) cases
         in
            funs, make_exp loc (MatchDTuple (a, List.rev cases))
    | LetAlloc (v, op, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (LetAlloc (v, op, e))
    | SetSubscript (a1, a2, a3, ty, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (SetSubscript (a1, a2, a3, ty, e))
    | LetSubscript (v, ty, a1, a2, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (LetSubscript (v, ty, a1, a2, e))
    | SetGlobal (v, ty, a, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (SetGlobal (v, ty, a, e))
    | LetRec (letrecs, e) ->
         let funs, e = prog_exp funs e in
         let funs, letrecs =
            List.fold_left (fun (funs, letrecs) (v, e, ty) ->
                  let funs, e = prog_exp funs e in
                  let letrecs = (v, e, ty) :: letrecs in
                     funs, letrecs) (funs, []) letrecs
         in
         let e = LetRec (List.rev letrecs, e) in
            funs, make_exp loc e
    | LetFuns (fundefs, e) ->
         let funs =
            List.fold_left (fun funs (f, loc, gflag, ty, tvars, vars, e) ->
                  let funs, e = prog_exp funs e in
                     SymbolTable.add funs f (loc, gflag, ty, tvars, vars, e)) funs fundefs
         in
            prog_exp funs e
    | LetClosure (v, f, ty_args, args, e) ->
         let funs, e = prog_exp funs e in
            funs, make_exp loc (LetClosure (v, f, ty_args, args, e))
    | LetApply _
    | Return _
    | Try _
    | Raise _ ->
         raise (IRException (pos, StringError "unexpected expression"))

(*
 * Lift out all nested functions.
 *)
let prog_funs funs =
   SymbolTable.fold (fun funs f (loc, gflag, ty, tvars, vars, e) ->
         let funs, e = prog_exp funs e in
            SymbolTable.add funs f (loc, gflag, ty, tvars, vars, e)) SymbolTable.empty funs

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Lift all functions to the top level.
 *)
let prog_prog prog =
   let { prog_funs = funs } = prog in
   let funs = prog_funs funs in
      { prog with prog_funs = funs }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
