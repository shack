(*
 * Substitution and free variables on the FIR.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn

module Pos = MakePos (struct let name = "Aml_ir_standardize" end)
open Pos

(*
 * Labels are symbol pairs.
 *)
module SymbolPairCompare =
struct
   type t = ty_var * var

   let compare (v11, v12) (v21, v22) =
      let cmp = Symbol.compare v11 v21 in
         if cmp = 0 then
            Symbol.compare v12 v22
         else
            cmp
end

module SymbolPairTable = Mc_map.McMake (SymbolPairCompare)

(************************************************************************
 * SUBSTITUTION
 ************************************************************************)

(*
 * Substitutions for all the different kinds of variables.
 *)
type subst =
   { subst_types : ty_var SymbolTable.t;
     subst_vars : var SymbolTable.t;
     subst_fields : var SymbolPairTable.t
   }

let empty_subst =
   { subst_types = SymbolTable.empty;
     subst_vars = SymbolTable.empty;
     subst_fields = SymbolPairTable.empty
   }

(*
 * Adding to subst.
 *)
let subst_add_type_var subst v1 v2 =
   { subst with subst_types = SymbolTable.add subst.subst_types v1 v2 }

let subst_add_var subst v1 v2 =
   { subst with subst_vars = SymbolTable.add subst.subst_vars v1 v2 }

let subst_add_field subst ty_var label1 label2 =
   { subst with subst_fields = SymbolPairTable.add subst.subst_fields (ty_var, label1) label2 }

(*
 * Fetching from subst.
 *)
let subst_type_var subst v =
   try SymbolTable.find subst.subst_types v with
      Not_found ->
         v

let subst_var subst v =
   try SymbolTable.find subst.subst_vars v with
      Not_found ->
         v

let subst_field subst ty_var v =
   try SymbolPairTable.find subst.subst_fields (ty_var, v) with
      Not_found ->
         v

(*
 * Make up new variables.
 *)
let subst_new_type_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_type_var subst v v' in
      v', subst

let subst_new_type_vars subst vars =
   let vars' = List.map new_symbol vars in
   let subst = List.fold_left2 subst_add_type_var subst vars vars' in
      vars', subst

let subst_new_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_var subst v v' in
      v', subst

let subst_new_vars subst vars =
   let vars' = List.map new_symbol vars in
   let subst = List.fold_left2 subst_add_var subst vars vars' in
      vars', subst

(************************************************************************
 * SUBST INITIALIZATION
 ************************************************************************)

(*
 * Rename all the constructor names in the union.
 *)
let subst_of_union subst ty_var fields =
   FieldTable.fold (fun subst const_var _ ->
         let const_var' = new_symbol const_var in
            subst_add_field subst ty_var const_var const_var') subst fields

(*
 * Rename all the labels in the record.
 *)
let subst_of_record subst ty_var fields =
   FieldTable.fold (fun subst label _ ->
         let label' = new_symbol label in
            subst_add_field subst ty_var label label') subst fields

(*
 * Rename all the fields in the module.
 *)
let subst_of_module subst ty_var fields =
   SymbolTable.fold (fun subst label _ ->
         let label' = new_symbol label in
            subst_add_field subst ty_var label label') subst fields

(*
 * Build a new substitution by going through
 * all the type definitions and creating all the
 * new names.
 *)
let subst_of_prog prog =
   let { prog_types = types;
         prog_globals = globals;
         prog_import = import;
         prog_export = export;
         prog_funs = funs
       } = prog
   in
   let subst = empty_subst in
   let subst =
      SymbolTable.fold (fun subst ty_var tydef ->
            let subst =
               match dest_tydef_inner_core tydef.tydef_inner with
                  TyDefLambda _
                | TyDefDTuple _ ->
                     subst
                | TyDefUnion fields ->
                     subst_of_union subst ty_var fields
                | TyDefFrame fields
                | TyDefRecord fields ->
                     subst_of_record subst ty_var fields
                | TyDefModule (_, fields) ->
                     subst_of_module subst ty_var fields
            in
               subst_add_type_var subst ty_var (new_symbol ty_var)) subst types
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            subst_add_var subst v (new_symbol v)) subst globals
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            subst_add_var subst v (new_symbol v)) subst import
   in
   let subst =
      SymbolTable.fold (fun subst f _ ->
            subst_add_var subst f (new_symbol f)) subst funs
   in
      subst

(************************************************************************
 * ALPHA RENAMING                                                       *
 ************************************************************************)

(*
 * Rename type variables and parameters.
 *)
let rec standardize_type subst ty =
   let loc = loc_of_type ty in
   let ty = dest_type_core ty in
   let ty = standardize_type_core subst ty in
      make_type loc ty

and standardize_type_core subst ty =
   match ty with
      TyVoid
    | TyInt
    | TyChar
    | TyString
    | TyFloat ->
         ty

    | TyFun (t1, t2) ->
         TyFun (standardize_types subst t1, standardize_type subst t2)
    | TyTuple tyl ->
         TyTuple (standardize_types subst tyl)
    | TyArray ty ->
         TyArray (standardize_type subst ty)
    | TyAll (vars, ty) ->
         let vars, subst = subst_new_type_vars subst vars in
            TyAll (vars, standardize_type subst ty)
    | TyExists (vars, ty) ->
         let vars, subst = subst_new_type_vars subst vars in
            TyExists (vars, standardize_type subst ty)

    | TyExternal (ty, s) ->
         TyExternal (standardize_type subst ty, s)
(*
    | TyOpen fields ->
         TyOpen (List.map (fun (label, ty) -> reintern label, standardize_type subst ty) fields)
*)
    | TyFormat (t1, t2, t3) ->
         TyFormat (standardize_type subst t1, standardize_type subst t2, standardize_type subst t3)

    | TyVar v ->
         TyVar (subst_type_var subst v)
    | TyProject (v, i) ->
         TyProject (subst_var subst v, i)

    | TyApply (v, tyl) ->
         TyApply (subst_type_var subst v, standardize_types subst tyl)
    | TyUnion (v, tyl, set) ->
         TyUnion (subst_type_var subst v, standardize_types subst tyl, set)
    | TyFrame (v, tyl) ->
         TyFrame (subst_type_var subst v, standardize_types subst tyl)
    | TyRecord (v, tyl) ->
         TyRecord (subst_type_var subst v, standardize_types subst tyl)
    | TyModule (v, tyl) ->
         TyModule (subst_type_var subst v, standardize_types subst tyl)
    | TyTag (v, tyl) ->
         TyTag (subst_type_var subst v, standardize_types subst tyl)
    | TyDTuple (v, tyl_opt) ->
         TyDTuple (subst_type_var subst v, standardize_types_opt subst tyl_opt)

(*
 * Map over a list.
 *)
and standardize_types subst tyl =
   List.map (standardize_type subst) tyl

and standardize_types_opt subst tyl_opt =
   match tyl_opt with
      Some tyl -> Some (standardize_types subst tyl)
    | None -> None

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Standardize a record type.
 *)
let standardize_record_type subst ty_var fields =
   let fields =
      FieldTable.fold_index (fun fields label (mflag, ty) i ->
            let label = subst_field subst ty_var label in
            let ty = standardize_type subst ty in
               FieldTable.add_index fields label (mflag, ty) i) FieldTable.empty fields
   in
      fields

(*
 * Standardize a union type.
 *)
let standardize_union_type subst ty_var fields =
   FieldTable.fold_index (fun fields label tyl i ->
         let label = subst_field subst ty_var label in
         let tyl = standardize_types subst tyl in
            FieldTable.add_index fields label tyl i) FieldTable.empty fields

(*
 * Standardize a module definition.
 *)
let standardize_module_type subst ty_var names fields =
   let names = FieldTable.map (subst_field subst ty_var) names in
   let fields =
      SymbolTable.fold (fun fields v ty ->
            let v = subst_field subst ty_var v in
               SymbolTable.add fields v (standardize_type subst ty)) SymbolTable.empty fields
   in
      names, fields

(*
 * Standardize a type definition.
 *)
let standardize_tydef_inner_core subst ty_var tydef =
   match tydef with
      TyDefLambda ty ->
         TyDefLambda (standardize_type subst ty)
    | TyDefDTuple ty_var ->
         TyDefDTuple (subst_type_var subst ty_var)
    | TyDefFrame fields ->
         TyDefFrame (standardize_record_type subst ty_var fields)
    | TyDefRecord fields ->
         TyDefRecord (standardize_record_type subst ty_var fields)
    | TyDefUnion fields ->
         TyDefUnion (standardize_union_type subst ty_var fields)
    | TyDefModule (names, fields) ->
         let names, fields = standardize_module_type subst ty_var names fields in
            TyDefModule (names, fields)

(*
 * Standardize a type definition.
 *)
let standardize_tydef_inner subst ty_var inner =
   let loc = loc_of_tydef_inner inner in
   let inner = dest_tydef_inner_core inner in
      make_tydef_inner loc (standardize_tydef_inner_core subst ty_var inner)

(*
 * Standardize a type definition core.
 * We have to rename both sets of parameters.
 *)
let standardize_tydef subst ty_var core =
   let { tydef_fo_vars = params;
         tydef_inner = inner
       } = core
   in
   let params', subst = subst_new_type_vars subst params in
   let inner = standardize_tydef_inner subst ty_var inner in
      { tydef_fo_vars = params';
        tydef_inner = inner
      }

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Type of unary operations.
 *)
let standardize_unop subst op =
   match op with
      (* Boolean *)
      NotBoolOp

      (* Arithmetic *)
    | UMinusIntOp
    | NotIntOp
    | AbsIntOp

      (* Floats *)
    | UMinusFloatOp
    | AbsFloatOp
    | SqrtFloatOp
    | ExpFloatOp
    | LogFloatOp
    | Log10FloatOp
    | CosFloatOp
    | SinFloatOp
    | TanFloatOp
    | ACosFloatOp
    | ASinFloatOp
    | ATanFloatOp
    | CosHFloatOp
    | SinHFloatOp
    | TanHFloatOp

      (* Coercions *)
    | IntOfCharOp
    | CharOfIntOp
    | CeilFloatOp
    | FloorFloatOp
    | IntOfFloatOp
    | FloatOfIntOp

      (* Block operations *)
    | LengthOfStringOp ->
         op

    | LengthOfBlockOp ty ->
         LengthOfBlockOp (standardize_type subst ty)

(*
 * Type of binary operations.
 *)
let standardize_binop subst op =
   match op with
      (* Non short-circuit Booleans *)
      OrBoolOp
    | AndBoolOp

      (* Comparisons on ML ints *)
    | EqCharOp
    | NeqCharOp
    | LtCharOp
    | LeCharOp
    | GtCharOp
    | GeCharOp

    | CmpCharOp

    | PlusIntOp
    | MinusIntOp
    | MulIntOp
    | DivIntOp
    | RemIntOp
    | LslIntOp
    | LsrIntOp
    | AsrIntOp
    | AndIntOp
    | OrIntOp
    | XorIntOp
    | MaxIntOp
    | MinIntOp
    | CmpIntOp

      (* Comparisons on ML ints *)
    | EqIntOp
    | NeqIntOp
    | LtIntOp
    | LeIntOp
    | GtIntOp
    | GeIntOp

      (* Standard binary operations on floats *)
    | PlusFloatOp
    | MinusFloatOp
    | MulFloatOp
    | DivFloatOp
    | RemFloatOp
    | MaxFloatOp
    | MinFloatOp
    | ATan2FloatOp
    | PowerFloatOp

    | LdExpFloatIntOp

      (* Comparisons on floats *)
    | EqFloatOp
    | NeqFloatOp
    | LtFloatOp
    | LeFloatOp
    | GtFloatOp
    | GeFloatOp

    | CmpFloatOp ->
         op

      (* Pointer (in)equality.  Arguments must have the given type *)
    | EqEqOp ty ->
         EqEqOp (standardize_type subst ty)
    | NeqEqOp ty ->
         NeqEqOp (standardize_type subst ty)

(*
 * Rename var atoms.
 *)
let rec standardize_atom subst a =
   match a with
      AtomInt _
    | AtomChar _
    | AtomFloat _ ->
         a
    | AtomVar v ->
         AtomVar (subst_var subst v)
    | AtomNil ty ->
         AtomNil (standardize_type subst ty)
    | AtomTyConstrain (a, ty) ->
         AtomTyConstrain (standardize_atom subst a, standardize_type subst ty)
    | AtomTyApply (a, ty, tyl) ->
         AtomTyApply (standardize_atom subst a, standardize_type subst ty, standardize_types subst tyl)
    | AtomTyPack (v, ty, tyl) ->
         AtomTyPack (subst_var subst v, standardize_type subst ty, standardize_types subst tyl)
    | AtomTyUnpack v ->
         AtomTyUnpack (subst_var subst v)
    | AtomConst (ty, ty_var, const_var) ->
         AtomConst (standardize_type subst ty, subst_type_var subst ty_var, subst_field subst ty_var const_var)
    | AtomFrameLabel (ty_var, label) ->
         AtomFrameLabel (subst_type_var subst ty_var, subst_field subst ty_var label)
    | AtomRecordLabel (ty_var, label) ->
         AtomRecordLabel (subst_type_var subst ty_var, subst_field subst ty_var label)
    | AtomModuleLabel (ty_var, ext_var) ->
         AtomModuleLabel (subst_type_var subst ty_var, ext_var)
    | AtomUnop (op, a) ->
         AtomUnop (standardize_unop subst op, standardize_atom subst a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (standardize_binop subst op, standardize_atom subst a1, standardize_atom subst a2)

let standardize_atom_opt subst a_opt =
   match a_opt with
      Some a -> Some (standardize_atom subst a)
    | None -> None

let standardize_atoms subst args =
   List.map (standardize_atom subst) args

(*
 * Allocation values.
 *)
let standardize_alloc_op subst op =
   match op with
      AllocTuple (ty_vars, ty, args) ->
         let ty_vars, subst = subst_new_type_vars subst ty_vars in
         let ty = standardize_type subst ty in
         let args = standardize_atoms subst args in
            AllocTuple (ty_vars, ty, args)
    | AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
         let ty_vars, subst = subst_new_type_vars subst ty_vars in
         let ty = standardize_type subst ty in
         let const_var = subst_field subst ty_var const_var in
         let ty_var = subst_type_var subst ty_var in
         let args = standardize_atoms subst args in
            AllocUnion (ty_vars, ty, ty_var, const_var, args)
    | AllocRecord (ty_vars, ty, ty_var, fields) ->
         let ty_vars, subst = subst_new_type_vars subst ty_vars in
         let ty = standardize_type subst ty in
         let ty_var' = subst_type_var subst ty_var in
         let fields = List.map (fun (label, a) -> subst_field subst ty_var label, standardize_atom subst a) fields in
            AllocRecord (ty_vars, ty, ty_var', fields)
    | AllocDTuple (ty, ty_var, a, args) ->
         let ty = standardize_type subst ty in
         let ty_var = subst_type_var subst ty_var in
         let a = standardize_atom subst a in
         let args = standardize_atoms subst args in
            AllocDTuple (ty, ty_var, a, args)
    | AllocArray (ty, args) ->
         AllocArray (standardize_type subst ty, standardize_atoms subst args)
    | AllocVArray (ty, a1, a2) ->
         AllocVArray (standardize_type subst ty, standardize_atom subst a1, standardize_atom subst a2)
    | AllocFrame (ty, ty_var, tyl) ->
         let ty = standardize_type subst ty in
         let ty_var = subst_type_var subst ty_var in
         let tyl = standardize_types subst tyl in
            AllocFrame (ty, ty_var, tyl)
    | AllocModule (ty, ty_var, fields) ->
         let ty = standardize_type subst ty in
         let ty_var = subst_type_var subst ty_var in
         let fields = SymbolTable.map (standardize_atom subst) fields in
            AllocModule (ty, ty_var, fields)

(*
 * Match set.
 *)
let standardize_set subst s =
   match s with
      CharSet _
    | IntSet _ ->
         s
    | ConstSet (ty_var, s) ->
         let s =
            SymbolSet.fold (fun s v ->
                  SymbolSet.add s (subst_field subst ty_var v)) SymbolSet.empty s
         in
            ConstSet (subst_type_var subst ty_var, s)

(*
 * Standardize an expression.
 *)
let rec standardize_exp subst exp =
   let loc = loc_of_exp exp in
   let exp = dest_exp_core exp in
   let exp = standardize_exp_core subst exp in
      make_exp loc exp

and standardize_exp_core subst exp =
   match exp with
      LetFuns (funs, e) ->
         standardize_funs_exp subst funs e

    | LetRec (letrecs, e) ->
         standardize_letrecs_exp subst letrecs e

    | LetExt (v, ty1, s, ty2, ty_args, args, e) ->
         let ty1 = standardize_type subst ty1 in
         let ty2 = standardize_type subst ty2 in
         let ty_args = standardize_types subst ty_args in
         let args = standardize_atoms subst args in
         let v, subst = subst_new_var subst v in
         let e = standardize_exp subst e in
            LetExt (v, ty1, s, ty2, ty_args, args, e)

    | LetAtom (v, ty, a, e) ->
         let ty = standardize_type subst ty in
         let a = standardize_atom subst a in
         let v, subst = subst_new_var subst v in
         let e = standardize_exp subst e in
            LetAtom (v, ty, a, e)

    | LetApply (v, ty, f, args, e) ->
         let ty = standardize_type subst ty in
         let f = standardize_atom subst f in
         let args = standardize_atoms subst args in
         let v, subst = subst_new_var subst v in
         let e = standardize_exp subst e in
            LetApply (v, ty, f, args, e)

    | LetClosure (v, f, ty_args, args, e) ->
         let f = standardize_atom subst f in
         let ty_args = standardize_types subst ty_args in
         let args = standardize_atoms subst args in
         let v, subst = subst_new_var subst v in
         let e = standardize_exp subst e in
            LetClosure (v, f, ty_args, args, e)

    | TailCall (f, args) ->
         let f = standardize_atom subst f in
         let args = standardize_atoms subst args in
            TailCall (f, args)
    | Return a ->
         Return (standardize_atom subst a)

    | Match (a, cases) ->
         let cases = List.map (fun (s, e) -> standardize_set subst s, standardize_exp subst e) cases in
            Match (standardize_atom subst a, cases)

    | MatchDTuple (a, cases) ->
         let cases = List.map (fun (a_opt, e) -> standardize_atom_opt subst a_opt, standardize_exp subst e) cases in
            MatchDTuple (standardize_atom subst a, cases)

    | Try (e1, v, e2) ->
         let e1 = standardize_exp subst e1 in
         let v, subst = subst_new_var subst v in
         let e2 = standardize_exp subst e2 in
            Try (e1, v, e2)
    | Raise (a, ty) ->
         Raise (standardize_atom subst a, standardize_type subst ty)
    | LetAlloc (v, op, e) ->
         let op = standardize_alloc_op subst op in
         let v, subst = subst_new_var subst v in
         let e = standardize_exp subst e in
            LetAlloc (v, op, e)
    | SetSubscript (a1, a2, a3, ty, e) ->
         let a1 = standardize_atom subst a1 in
         let a2 = standardize_atom subst a2 in
         let a3 = standardize_atom subst a3 in
         let ty = standardize_type subst ty in
         let e = standardize_exp subst e in
            SetSubscript (a1, a2, a3, ty, e)
    | LetSubscript (v, ty, a1, a2, e) ->
         let a1 = standardize_atom subst a1 in
         let a2 = standardize_atom subst a2 in
         let ty = standardize_type subst ty in
         let v, subst = subst_new_var subst v in
         let e = standardize_exp subst e in
            LetSubscript (v, ty, a1, a2, e)
    | SetGlobal (v, ty, a, e) ->
         let v = subst_var subst v in
         let ty = standardize_type subst ty in
         let a = standardize_atom subst a in
         let e = standardize_exp subst e in
            SetGlobal (v, ty, a, e)

(*
 * Rename all the arguments in the functions.
 *)
and standardize_funs_exp subst funs e =
   let subst =
      List.fold_left (fun subst (f, _, _, _, _, _, _) ->
            let _, subst = subst_new_var subst f in
               subst) subst funs
   in
   let funs =
      List.map (fun (f, loc, gflag, ty_vars, ty, vars, e) ->
            let f = subst_var subst f in
            let ty_vars, subst = subst_new_type_vars subst ty_vars in
            let vars, subst = subst_new_vars subst vars in
            let ty = standardize_type subst ty in
            let e = standardize_exp subst e in
               f, loc, gflag, ty_vars, ty, vars, e) funs
   in
   let e = standardize_exp subst e in
      LetFuns (funs, e)

(*
 * Rename all the arguments in the recursive definitions.
 *)
and standardize_letrecs_exp subst letrecs e =
   let subst =
      List.fold_left (fun subst (v, _, _) ->
            let _, subst = subst_new_var subst v in
               subst) subst letrecs
   in
   let letrecs =
      List.map (fun (v, e, ty) ->
            let v = subst_var subst v in
            let ty = standardize_type subst ty in
            let e = standardize_exp subst e in
               v, e, ty) letrecs
   in
   let e = standardize_exp subst e in
      LetRec (letrecs, e)

(*
 * Initializer.
 *)
let standardize_init subst init =
   match init with
      InitString _ ->
         init
    | InitAtom a ->
         InitAtom (standardize_atom subst a)
    | InitAlloc op ->
         InitAlloc (standardize_alloc_op subst op)

(*
 * Boot record.
 *)
let standardize_boot subst boot =
   let { boot_ty_unit     = ty_unit;
         boot_ty_unit_var = ty_unit_var;
         boot_unit_var    = unit_var;

         boot_ty_bool     = ty_bool;
         boot_ty_bool_var = ty_bool_var;
         boot_true_var    = true_var;
         boot_false_var   = false_var;
         boot_true_set    = true_set;
         boot_false_set   = false_set;

         boot_ty_exn      = ty_exn;
         boot_ty_exn_var  = ty_exn_var;
         boot_match_failure_var = match_failure_var
       } = boot
   in
      { boot_ty_unit      = standardize_type_core subst ty_unit;
        boot_ty_unit_var  = subst_type_var subst ty_unit_var;
        boot_unit_var     = subst_field subst ty_unit_var unit_var;

        boot_ty_bool      = standardize_type_core subst ty_bool;
        boot_ty_bool_var  = subst_type_var subst ty_bool_var;
        boot_true_var     = subst_field subst ty_bool_var true_var;
        boot_false_var    = subst_field subst ty_bool_var false_var;
        boot_true_set     = standardize_set subst true_set;
        boot_false_set    = standardize_set subst false_set;

        boot_ty_exn       = standardize_type_core subst ty_exn;
        boot_ty_exn_var   = subst_type_var subst ty_exn_var;
        boot_match_failure_var = subst_var subst match_failure_var
      }

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Standardize the program.
 *)
let standardize_prog prog =
   let { prog_name    = name;
         prog_names   = names;
         prog_types   = types;
         prog_import  = import;
         prog_export  = export;
         prog_globals = globals;
         prog_tags    = tags;
         prog_funs    = funs;
         prog_boot    = boot
       } = prog
   in
   let subst = subst_of_prog prog in
   let name = subst_var subst name in
   let names =
      SymbolTable.fold (fun names v ext_var ->
            let v = subst_var subst v in
            let ext_var = reintern ext_var in
               SymbolTable.add names v ext_var) SymbolTable.empty names
   in
   let types =
      SymbolTable.fold (fun types ty_var tydef ->
            let v = subst_type_var subst ty_var in
            let tydef = standardize_tydef subst ty_var tydef in
               SymbolTable.add types v tydef) SymbolTable.empty types
   in
   let import =
      SymbolTable.fold (fun import me_var { import_info = info; import_type = ty } ->
            let me_var = subst_var subst me_var in
            let ty = standardize_type subst ty in
            let info = { import_info = info; import_type = ty } in
               SymbolTable.add import me_var info) SymbolTable.empty import
   in
   let export =
      SymbolTable.fold (fun exports me_var export ->
            let { export_type = ty } = export in
            let me_var = subst_var subst me_var in
            let ty = standardize_type subst ty in
            let export = { export with export_type = ty } in
               SymbolTable.add exports me_var export) SymbolTable.empty export
   in
   let globals =
      SymbolTable.fold (fun globals v (ty, init) ->
            let v = subst_var subst v in
            let ty = standardize_type subst ty in
            let init = standardize_init subst init in
               SymbolTable.add globals v (ty, init)) SymbolTable.empty globals
   in
   let tags =
      SymbolTable.fold (fun tags v (ty_var, tyl) ->
            let v = subst_var subst v in
            let ty_var = subst_type_var subst ty_var in
            let tyl = standardize_types subst tyl in
               SymbolTable.add tags v (ty_var, tyl)) SymbolTable.empty tags
   in
   let funs =
      SymbolTable.fold (fun funs f (gflag, loc, ty_vars, ty, vars, e) ->
            let f = subst_var subst f in
            let ty_vars, subst = subst_new_vars subst ty_vars in
            let vars, subst = subst_new_vars subst vars in
            let ty = standardize_type subst ty in
            let e = standardize_exp subst e in
               SymbolTable.add funs f (gflag, loc, ty_vars, ty, vars, e)) SymbolTable.empty funs
   in
   let boot = standardize_boot subst boot in
      { prog_name    = name;
        prog_names   = names;
        prog_types   = types;
        prog_import  = import;
        prog_export  = export;
        prog_globals = globals;
        prog_tags    = tags;
        prog_funs    = funs;
        prog_boot    = boot
      }

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
