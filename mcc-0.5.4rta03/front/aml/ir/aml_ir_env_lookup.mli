(*
 * Look up values in the environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_ir
open Aml_ir_pos
open Aml_ir_env_type

(*
 * Get values from the variable environment.
 *)
val venv_lookup_var        : venv -> pos -> var -> ty

(*
 * Get values from the type environment.
 *)
val tenv_lookup_var        : tenv -> pos -> var -> var_type
val tenv_lookup_fo_var_exn : tenv -> ty_var -> var_type
val tenv_lookup_fo_var     : tenv -> pos -> ty_var -> var_type
val tenv_lookup_so_var     : tenv -> pos -> ty_var -> tyfun
val tenv_lookup_so_var_ext : tenv -> pos -> ext_var -> ty_var * tyfun
val tenv_lookup_tydef      : tenv -> pos -> ty_var -> Aml_tast.tydef

(*
 * These versions raise Not_found if the variable is not found.
 *)
val tenv_lookup_fo_var_exn : tenv -> ty_var -> var_type

(*
 * Type application.
 *)
val genv_lookup_ty_apply_exn : genv -> Aml_tast.ty_var -> Aml_tast.ty_kind list -> (loc -> ty list -> ty)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
