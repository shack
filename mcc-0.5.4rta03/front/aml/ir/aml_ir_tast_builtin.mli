(*
 * Compile bultin functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_ir
open Aml_ir_pos
open Aml_ir_env_type

type result = genv * exp * ty
type cont_exp = genv -> tenv -> venv -> atom -> result

val build_builtin_atom : genv -> pos -> string -> atom

(*
 * Expression form results in a LetApply if the op is not found.
 *)
val build_builtin_exp :
   genv -> tenv -> venv -> pos -> loc ->   (* Usual environment *)
   ty ->                           (* Type of the result *)
   string ->                       (* String name of the external function *)
   atom ->                         (* Value of the external function *)
   ty ->                           (* Type of the external function *)
   atom list ->                    (* Arguments *)
   cont_exp ->                     (* Rest of the code *)
   result

(*
 * Ext form results in a LetExt if the op is not found.
 *)
val build_builtin_ext :
   genv -> tenv -> venv -> pos -> loc ->   (* Usual environment *)
   ty ->                           (* Type of the result *)
   string ->                       (* String name of the external function *)
   ty ->                           (* Type of the external function *)
   atom list ->                    (* Arguments *)
   cont_exp ->                     (* Rest of the code *)
   result

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
