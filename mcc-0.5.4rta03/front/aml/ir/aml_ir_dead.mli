(*
 * AML IR dead-expression elimination.
 * The code first computes the set of all live symbols using fixpoint analysis
 * across function boundaries.  Then it uses the set to perform dead code elimination.
 * Note that this removes all dead arguments to any function always called directly.
 * The callgraph is used to speed up the fixpoint analysis
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_ir

(*
 * Flags used to determine what arguments to preserve.
 *   PreserveNone: preserve only the args that are not dead
 *   PreserveFrames: preserve args that have type TyFrame
 *   PreserveAll: preserve all args
 *)
type preserve =
   PreserveNone
 | PreserveFrames
 | PreserveAll

(*
 * Eliminate dead code.
 *)
val dead_prog : preserve -> prog -> prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
