(*
 * This file defines utilities on types.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Field_table

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_exn
open Aml_ir_env_type

module Pos = MakePos (struct let name = "Aml_ir_type" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Build a function type from arguments and result type.
 *)
let rec wrap_fun_type loc ty_args ty_res =
   match ty_args with
      [] ->
         ty_res
    | ty :: ty_args ->
         make_type loc (TyFun ([ty], wrap_fun_type loc ty_args ty_res))

(************************************************************************
 * TYPE DEF EXPANSION
 ************************************************************************)

(*
 * Apply a type definition.
 *)
let apply_type genv pos ty_var tyl =
   let pos = string_pos "apply_type" pos in
   let tydef =
      try SymbolTable.find genv.genv_types ty_var with
         Not_found ->
            raise (IRException (pos, UnboundType ty_var))
   in
   let { tydef_fo_vars = params;
         tydef_inner = inner
       } = tydef
   in

   (* Params *)
   let len1 = List.length params in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, VarArityMismatch (ty_var, len1, len2)))
   in
   let subst = List.fold_left2 subst_add_fo_var empty_subst params tyl in
      subst_tydef_inner subst inner

(*
 * Various applications.
 *)
let apply_apply_type genv pos ty_var tyl =
   let pos = string_pos "apply_apply_type" pos in
   let inner = apply_type genv pos ty_var tyl in
      match dest_tydef_inner_core inner with
         TyDefLambda ty ->
            ty
       | _ ->
            raise (IRException (pos, StringTydefInnerError ("not a type application", inner)))

let apply_union_type genv pos ty_var tyl =
   let pos = string_pos "apply_union_type" pos in
   let inner = apply_type genv pos ty_var tyl in
      match dest_tydef_inner_core inner with
         TyDefUnion fields ->
            fields
       | _ ->
            raise (IRException (pos, StringTydefInnerError ("not a union type", inner)))

let apply_record_type genv pos ty_var tyl =
   let pos = string_pos "apply_record_type" pos in
   let inner = apply_type genv pos ty_var tyl in
      match dest_tydef_inner_core inner with
         TyDefRecord fields ->
            fields
       | _ ->
            raise (IRException (pos, StringTydefInnerError ("not a record type", inner)))

let apply_module_type genv pos ty_var tyl =
   let pos = string_pos "apply_module_type" pos in
   let inner = apply_type genv pos ty_var tyl in
      match dest_tydef_inner_core inner with
         TyDefModule (names, fields) ->
            names, fields
       | _ ->
            raise (IRException (pos, StringTydefInnerError ("not a module type", inner)))

(************************************************************************
 * TYPE DESTRUCTORS
 ************************************************************************)

(*
 * Type definition destructors.
 *)
let dest_apply_type genv pos ty =
   match dest_type_core ty with
      TyApply (ty_var, tyl) ->
         ty_var, apply_apply_type genv pos ty_var tyl
    | _ ->
         raise (IRException (pos, StringTypeError ("not a type application", ty)))

let dest_union_type genv pos ty =
   match dest_type_core ty with
      TyUnion (ty_var, tyl, set) ->
         let fields = apply_union_type genv pos ty_var tyl in
            ty_var, fields, set
    | _ ->
         raise (IRException (pos, StringTypeError ("not a union type", ty)))

let dest_record_type genv pos ty =
   match dest_type_core ty with
      TyRecord (ty_var, tyl) ->
         ty_var, apply_record_type genv pos ty_var tyl
    | _ ->
         raise (IRException (pos, StringTypeError ("not a record type", ty)))

let dest_module_type genv pos ty =
   match dest_type_core ty with
      TyModule (ty_var, tyl) ->
         let names, fields = apply_module_type genv pos ty_var tyl in
            ty_var, names, fields
    | _ ->
         raise (IRException (pos, StringTypeError ("not a module type", ty)))

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the index of a union field.
 *)
let lookup_record_field fields pos label =
   let pos = string_pos "lookup_of_record_field" pos in
      try FieldTable.find_with_index fields label with
         Not_found ->
            raise (IRException (pos, UnboundLabel label))

let lookup_union_field fields pos const_var =
   let pos = string_pos "lookup_union_field" pos in
      try FieldTable.find_with_index fields const_var with
         Not_found ->
            raise (IRException (pos, UnboundConst const_var))

let lookup_module_field names fields pos ext_var =
   let pos = string_pos "lookup_module_field" pos in
   let v =
      try FieldTable.find names ext_var with
         Not_found ->
            raise (IRException (pos, UnboundVar ext_var))
   in
      try SymbolTable.find fields v with
         Not_found ->
            raise (IRException (pos, UnboundVar v))

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
