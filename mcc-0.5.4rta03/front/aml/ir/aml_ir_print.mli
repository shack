(*
 * Print the FIR.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Aml_ir
open Aml_ir_env_type

(*
 * String conversions.
 *)
val string_of_unop     : unop -> string
val string_of_binop    : binop -> string

(*
 * Printers.
 *)
val pp_print_type        : formatter -> ty -> unit
val pp_print_atom        : formatter -> atom -> unit
val pp_print_exp         : formatter -> exp -> unit
val pp_print_funs        : formatter -> fundef list -> unit
val pp_print_fundef      : formatter -> var -> gfundef -> unit
val pp_print_tydef       : formatter -> tydef -> unit
val pp_print_tydef_inner : formatter -> tydef_inner -> unit
val pp_print_prog        : formatter -> prog -> unit
val pp_print_var_type    : formatter -> var_type -> unit

(*
 * Debugging versions.
 *)
val pp_print_type_ds     : formatter -> ty -> unit

val debug_prog    : string -> prog -> unit

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
