(*
 * Lookup values from the env.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Aml_ir
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_env_type
open Aml_ir_env_base

module Pos = MakePos (struct let name = "Aml_ir_env_lookup" end)
open Pos

(************************************************************************
 * VARIABLE ENV
 ************************************************************************)

(*
 * Get the type of a variable.
 *)
let venv_lookup_var venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (IRException (string_pos "venv_lookup_var" pos, UnboundVar v))

(************************************************************************
 * TYPE ENV
 ************************************************************************)

(*
 * Get the type of a variable.
 *)
let tenv_lookup_var tenv pos v =
   try SymbolTable.find tenv.tenv_vars v with
      Not_found ->
         raise (IRException (string_pos "tenv_lookup_var" pos, UnboundVar v))

(*
 * Type variables.
 *)
let tenv_lookup_fo_var_exn tenv ty_var =
   SymbolTable.find tenv.tenv_fo_vars ty_var

let tenv_lookup_fo_var tenv pos ty_var =
   try tenv_lookup_fo_var_exn tenv ty_var with
      Not_found ->
         raise (IRException (string_pos "tenv_lookup_fo_var" pos, UnboundType ty_var))

let tenv_lookup_so_var tenv pos ty_var =
   let pos = string_pos "tenv_lookup_so_var" pos in
      try SymbolTable.find tenv.tenv_so_vars ty_var with
         Not_found ->
            raise (IRException (string_pos "tenv_lookup_so_var" pos, UnboundType ty_var))

let tenv_lookup_so_var_ext tenv pos v =
   let pos = string_pos "tenv_lookup_so_var_ext" pos in
   let v =
      try SymbolTable.find tenv.tenv_names.names_ty v with
         Not_found ->
            raise (IRException (pos, UnboundType v))
   in
      v, tenv_lookup_so_var tenv pos v

(*
 * Look up a type definition.
 *)
let tenv_lookup_tydef tenv pos ty_var =
   try SymbolTable.find tenv.tenv_tydefs ty_var with
      Not_found ->
         raise (IRException (pos, UnboundType ty_var))

(************************************************************************
 * GLOBAL ENV
 ************************************************************************)

(*
 * Look up a type application.
 *)
let genv_lookup_ty_apply_exn genv ty_var ty_kinds =
   Aml_tast_type.TyApplyTable.find genv.genv_ty_apply (ty_var, ty_kinds)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
