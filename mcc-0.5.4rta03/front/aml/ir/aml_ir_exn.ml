(*
 * Exception definitions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Aml_ir
open Aml_ir_env_type

(*
 * Exceptions.
 *)
type ir_error =
   (* Unbound value errors *)
   UnboundVar                   of var
 | UnboundLabel                 of label
 | UnboundConst                 of const_var
 | UnboundType                  of ext_var
 | UnboundVal                   of ty_var * ext_var

   (* Type errors *)
 | ArityMismatch                of int * int
 | VarArityMismatch             of var * int * int
 | TypeArityMismatch            of ty * int * int

 | InvalidFormatString          of string

   (* Generic errors *)
 | StringError                  of string
 | StringIntError               of string * int
 | StringAtomError              of string * atom
 | StringTypeError              of string * ty
 | StringTydefError             of string * tydef
 | StringTydefInnerError        of string * tydef_inner
 | StringVarError               of string * var
 | StringVarTypeError           of string * var_type
 | StringTAstTydefInnerError    of string * Aml_tast.tydef_inner
 | StringTAstModNameError       of string * Aml_tast.me_name

 | NotImplemented               of string
 | InternalError

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
