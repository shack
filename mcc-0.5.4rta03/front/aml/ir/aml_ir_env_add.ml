(*
 * Add values to the environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Aml_ir
open Aml_ir_ds
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_env_type

module Pos = MakePos (struct let name = "Aml_ir_env_lookup" end)
open Pos

(************************************************************************
 * VARIABLE ENVIRONMENT
 ************************************************************************)

(*
 * Add a variable to the variable environment.
 *)
let venv_add_var = SymbolTable.add

let venv_add_vars venv pos vars tyl =
   let len1 = List.length vars in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (IRException (string_pos "venv_add_vars" pos, ArityMismatch (len1, len2)))
   in
      List.fold_left2 venv_add_var venv vars tyl

(************************************************************************
 * TYPE ENV
 ************************************************************************)

(*
 * Add a type to the type environment.
 *)
let tenv_add_var tenv v ty =
   { tenv with tenv_vars = SymbolTable.add tenv.tenv_vars v ty }

(*
 * Add a list of vars.
 *)
let tenv_add_vars tenv pos vl tl =
   let pos = string_pos "tenv_add_vars" pos in
   let len1 = List.length vl in
   let len2 = List.length tl in
      if len1 <> len2 then
         raise (IRException (pos, ArityMismatch (len1, len2)));
      List.fold_left2 tenv_add_var tenv vl tl

(*
 * Add type types.
 *)
let tenv_add_fo_var tenv v ty =
   { tenv with tenv_fo_vars = SymbolTable.add tenv.tenv_fo_vars v ty }

let tenv_add_fo_vars tenv pos vars tyl =
   let pos = string_pos "tenv_add_fo_vars" pos in
   let len1 = List.length vars in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, ArityMismatch (len1, len2)))
   in
   let fo_vars = List.fold_left2 SymbolTable.add tenv.tenv_fo_vars vars tyl in
      { tenv with tenv_fo_vars = fo_vars }

let tenv_add_so_var tenv v ty_vars ty =
   { tenv with tenv_so_vars = SymbolTable.add tenv.tenv_so_vars v (ty_vars, ty) }

let tenv_add_so_vars tenv pos vars tyl =
   let pos = string_pos "tenv_add_so_vars" pos in
   let len1 = List.length vars in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (IRException (pos, ArityMismatch (len1, len2)))
   in
   let so_vars = List.fold_left2 SymbolTable.add tenv.tenv_so_vars vars tyl in
      { tenv with tenv_so_vars = so_vars }

(*
 * Add the names.
 *)
let tenv_add_names tenv names =
   { tenv with tenv_names = names }

(************************************************************************
 * GLOBAL ENV
 ************************************************************************)

(*
 * Add a type definition.
 *)
let genv_add_tydef genv ty_var tydef =
   { genv with genv_types = SymbolTable.add genv.genv_types ty_var tydef }

(*
 * Add a string.
 * Try to give it a name like the string.
 *)
let name_len = 12

let buf = Buffer.create 20

let rec buffer_add_escape buf s i len =
   if len <> 0 then
      let c = s.[i] in
      let () =
         match c with
            'a'..'z'
          | 'A'..'Z'
          | '0'..'9'
          | '_'
          | '.' ->
               Buffer.add_char buf c
          | _ ->
               Buffer.add_char buf '&';
               Buffer.add_string buf (string_of_int (Char.code c))
      in
         buffer_add_escape buf s (succ i) (pred len)

let escaped_string_of_string s =
   let len = String.length s in
   let () =
      Buffer.clear buf;
      buffer_add_escape buf s 0 len
   in
      Buffer.contents buf

let genv_add_string genv loc s =
   let len = String.length s in
   let name =
      if len < name_len then
         s
      else
         String.sub s 0 name_len
   in
   let name = escaped_string_of_string name in
   let v = new_symbol_string name in
   let ty = make_type loc TyString in
   let init = InitString s in
   let genv = { genv with genv_globals = SymbolTable.add genv.genv_globals v (ty, init) } in
      genv, v

let genv_add_tag genv loc tag_var ty_var tyl =
   { genv with genv_tags = SymbolTable.add genv.genv_tags tag_var (ty_var, tyl) }

(*
 * Add a TyApply entry.
 *)
let genv_add_ty_apply genv ty_var ty_kinds ty_var' =
   { genv with genv_ty_apply = Aml_tast_type.TyApplyTable.add genv.genv_ty_apply (ty_var, ty_kinds) ty_var' }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
