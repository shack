(*
 * Builtin operators.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mc_string_util
open Format

open Symbol
open Location

open Aml_ir
open Aml_ir_ds
open Aml_ir_pos
open Aml_ir_env
open Aml_ir_exn
open Aml_ir_type
open Aml_ir_print

module Pos = MakePos (struct let name = "Aml_ir_tast_builtin" end)
open Pos

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * These are the kinds of builtin values.
 *)
type result = genv * exp * ty
type cont_exp = genv -> tenv -> venv -> atom -> result
type unop_fun = genv -> tenv -> venv -> pos -> loc -> atom -> cont_exp -> result
type unop_ty_fun = genv -> tenv -> venv -> pos -> loc -> ty -> atom -> cont_exp -> result
type binop_fun = genv -> tenv -> venv -> pos -> loc -> atom -> atom -> cont_exp -> result

(*
 * Information about builtin operators.
 *)
type builtin =
   BuiltinUnop of unop_fun
 | BuiltinBinop of binop_fun
 | BuiltinUnopType of unop_ty_fun
 | BuiltinExternal

(*
 * Equality.
 *)
type equal_ops =
   { equal_int_op   : binop;
     equal_char_op  : binop;
     equal_float_op : binop;
     equal_type_res : (genv -> loc -> ty);
     equal_type_fun : (genv -> loc -> ty);
     equal_fun      : string
   }

type eq_ops =
   { eq_int_op   : binop;
     eq_char_op  : binop;
     eq_float_op : binop;
     eq_eq_op    : (ty -> binop)
   }

(************************************************************************
 * EQUALITY BUILTINS
 ************************************************************************)

(*
 * More types.
 *)
let genv_ty_bool_fun genv loc =
   let ty_bool = genv_ty_bool genv loc in
   let v = new_symbol_string "a" in
   let ty = make_type loc (TyVar v) in
   let ty = make_type loc (TyFun ([ty; ty], ty_bool)) in
      make_type loc (TyAll ([v], ty))

let genv_ty_int_fun genv loc =
   let ty_int = make_type loc TyInt in
   let v = new_symbol_string "a" in
   let ty = make_type loc (TyVar v) in
   let ty = make_type loc (TyFun ([ty; ty], ty_int)) in
      make_type loc (TyAll ([v], ty))

(*
 * Equality operations.
 *)
let equal_ops =
   { equal_int_op   = EqIntOp;
     equal_char_op  = EqCharOp;
     equal_float_op = EqFloatOp;
     equal_type_res = genv_ty_bool;
     equal_type_fun = genv_ty_bool_fun;
     equal_fun      = "equal"
   }

let nequal_ops =
   { equal_int_op   = NeqIntOp;
     equal_char_op  = NeqCharOp;
     equal_float_op = NeqFloatOp;
     equal_type_res = genv_ty_bool;
     equal_type_fun = genv_ty_bool_fun;
     equal_fun      = "nequal"
   }

let lt_ops =
   { equal_int_op   = LtIntOp;
     equal_char_op  = LtCharOp;
     equal_float_op = LtFloatOp;
     equal_type_res = genv_ty_bool;
     equal_type_fun = genv_ty_bool_fun;
     equal_fun      = "lt"
   }

let le_ops =
   { equal_int_op   = LeIntOp;
     equal_char_op  = LeCharOp;
     equal_float_op = LeFloatOp;
     equal_type_res = genv_ty_bool;
     equal_type_fun = genv_ty_bool_fun;
     equal_fun      = "le"
   }

let gt_ops =
   { equal_int_op   = GtIntOp;
     equal_char_op  = GtCharOp;
     equal_float_op = GtFloatOp;
     equal_type_res = genv_ty_bool;
     equal_type_fun = genv_ty_bool_fun;
     equal_fun      = "gt"
   }

let ge_ops =
   { equal_int_op   = GeIntOp;
     equal_char_op  = GeCharOp;
     equal_float_op = GeFloatOp;
     equal_type_res = genv_ty_bool;
     equal_type_fun = genv_ty_bool_fun;
     equal_fun      = "ge"
   }

let cmp_ops =
   { equal_int_op   = CmpIntOp;
     equal_char_op  = CmpCharOp;
     equal_float_op = CmpFloatOp;
     equal_type_res = (fun genv loc -> make_type loc TyInt);
     equal_type_fun = genv_ty_int_fun;
     equal_fun      = "compare"
   }

(*
 * Pointer equality operations.
 *)
let eq_eq_ops =
   { eq_int_op     = EqIntOp;
     eq_char_op    = EqCharOp;
     eq_float_op   = EqFloatOp;
     eq_eq_op      = (fun ty -> EqEqOp ty)
   }

let neq_eq_ops =
   { eq_int_op     = NeqIntOp;
     eq_char_op    = NeqCharOp;
     eq_float_op   = NeqFloatOp;
     eq_eq_op      = (fun ty -> NeqEqOp ty)
   }

(************************************************************************
 * BUILTIN CONSTANTS
 ************************************************************************)

(*
 * There aren't very many constants, so don't bother with a table.
 *)
let build_builtin_atom genv pos s =
   let pos = string_pos "build_builtin_atom" pos in
      match s with
         "%max_int" -> AtomInt max_int
       | "%min_int" -> AtomInt min_int
       | _ ->
            raise (IRException (pos, StringError ("unknown builtin constant: \"" ^ s ^ "\"")))

(************************************************************************
 * BUILTIN OPERATORS
 ************************************************************************)

(*
 * Functions to construct builtins.
 *)
let wrap_unop op =
   BuiltinUnop (fun genv tenv venv pos loc a cont ->
         cont genv tenv venv (AtomUnop (op, a)))

let wrap_unop_binop op a2 =
   BuiltinUnop (fun genv tenv venv pos loc a1 cont ->
         cont genv tenv venv (AtomBinop (op, a1, a2)))

let wrap_binop op =
   BuiltinBinop (fun genv tenv venv pos loc a1 a2 cont ->
         cont genv tenv venv (AtomBinop (op, a1, a2)))

(*
 * Length of an array.
 *)
let alength_builtin =
   let build genv tenv venv pos loc a cont =
      let ty = type_of_atom genv venv pos loc a in
         cont genv tenv venv (AtomUnop (LengthOfBlockOp ty, a))
   in
      BuiltinUnop build

(*
 * Raise an exception.
 *)
let raise_builtin =
   let build genv tenv venv pos loc ty a cont =
      (* We build the rest just to find out the return type *)
      let v = new_symbol_string "raise" in
      let venv = venv_add_var venv v ty in
      let genv, _, ty_exp = cont genv tenv venv (AtomVar v) in

      (* Drop the rest, and raise the exception *)
      let e = make_exp loc (Raise (a, ty_exp)) in
         genv, e, ty_exp
   in
      BuiltinUnopType build

(*
 * Equality is special.
 * We catch integer, char cases.
 *)
let equal_builtin ops =
   let build genv tenv venv pos loc a1 a2 cont =
      let ty = type_of_atom genv venv pos loc a1 in
         match expand_type_core genv pos ty with
            TyInt ->
               cont genv tenv venv (AtomBinop (ops.equal_int_op, a1, a2))
          | TyChar ->
               cont genv tenv venv (AtomBinop (ops.equal_char_op, a1, a2))
          | TyFloat ->
               cont genv tenv venv (AtomBinop (ops.equal_float_op, a1, a2))
          | _ ->
               let ty_res = ops.equal_type_res genv loc in
               let ty_fun = ops.equal_type_fun genv loc in
               let v = new_symbol_string "equal" in
               let venv = venv_add_var venv v ty_res in
               let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
               let e = make_exp loc (LetExt (v, ty_res, ops.equal_fun, ty_fun, [ty], [a1; a2], e)) in
                  genv, e, ty_exp
   in
      BuiltinBinop build


(* Pointer equality *)
let eq_builtin ops =
   let build genv tenv venv pos loc a1 a2 cont =
      let ty = type_of_atom genv venv pos loc a1 in
         match expand_type_core genv pos ty with
            TyInt ->
               cont genv tenv venv (AtomBinop (ops.eq_int_op, a1, a2))
          | TyChar ->
               cont genv tenv venv (AtomBinop (ops.eq_char_op, a1, a2))
          | TyFloat ->
               cont genv tenv venv (AtomBinop (ops.eq_float_op, a1, a2))
          | _ ->
               cont genv tenv venv (AtomBinop (ops.eq_eq_op ty, a1, a2))
   in
      BuiltinBinop build

(*
 * Builtin table.
 *)
let builtins =
   ["%uminus",       wrap_unop       UMinusIntOp;
    "%succ",         wrap_unop_binop PlusIntOp (AtomInt 1);
    "%pred",         wrap_unop_binop MinusIntOp (AtomInt 1);
    "%abs",          wrap_unop       AbsIntOp;
    "%lnot",         wrap_unop       NotIntOp;

    "%add",          wrap_binop      PlusIntOp;
    "%sub",          wrap_binop      MinusIntOp;
    "%mul",          wrap_binop      MulIntOp;
    "%div",          wrap_binop      DivIntOp;
    "%mod",          wrap_binop      RemIntOp;
    "%land",         wrap_binop      AndIntOp;
    "%lor",          wrap_binop      OrIntOp;
    "%lsl",          wrap_binop      LslIntOp;
    "%lsr",          wrap_binop      LsrIntOp;
    "%asl",          wrap_binop      LslIntOp;
    "%asr",          wrap_binop      AsrIntOp;
    "%lxor",         wrap_binop      MinIntOp;

    "%equal",        equal_builtin   equal_ops;
    "%nequal",       equal_builtin   nequal_ops;
    "%eq",           eq_builtin      eq_eq_ops;
    "%neq",          eq_builtin      neq_eq_ops;
    "%le",           equal_builtin   le_ops;
    "%lt",           equal_builtin   lt_ops;
    "%gt",           equal_builtin   gt_ops;
    "%ge",           equal_builtin   ge_ops;
    "%compare",      equal_builtin   cmp_ops;

    "%not",          wrap_unop         NotBoolOp;
    "%or",           wrap_binop        OrBoolOp;
    "%and",          wrap_binop        AndBoolOp;

    "%fuminus",      wrap_unop         UMinusFloatOp;
    "%fsqrt",        wrap_unop         SqrtFloatOp;
    "%fexp",         wrap_unop         ExpFloatOp;
    "%flog",         wrap_unop         LogFloatOp;
    "%flog10",       wrap_unop         Log10FloatOp;
    "%fcos",         wrap_unop         CosFloatOp;
    "%fsin",         wrap_unop         SinFloatOp;
    "%ftan",         wrap_unop         TanFloatOp;
    "%facos",        wrap_unop         ACosFloatOp;
    "%fasin",        wrap_unop         ASinFloatOp;
    "%fatan",        wrap_unop         ATanFloatOp;
    "%fatan2",       wrap_binop        ATan2FloatOp;
    "%fcosh",        wrap_unop         CosHFloatOp;
    "%fsinh",        wrap_unop         SinHFloatOp;
    "%ftanh",        wrap_unop         TanHFloatOp;
    "%fabs",         wrap_unop         AbsFloatOp;

    "%fadd",         wrap_binop        PlusFloatOp;
    "%fsub",         wrap_binop        MinusFloatOp;
    "%fmul",         wrap_binop        MulFloatOp;
    "%fdiv",         wrap_binop        DivFloatOp;
    "%fpower",       wrap_binop        PowerFloatOp;
    "%fmod",         wrap_binop        RemFloatOp;

    "%fldexp",       wrap_binop        LdExpFloatIntOp;

    "%fceil",        wrap_unop         CeilFloatOp;
    "%ffloor",       wrap_unop         FloorFloatOp;
    "%ftrunc",       wrap_unop         IntOfFloatOp;
    "%float_of_int", wrap_unop         FloatOfIntOp;

    "%slength",      wrap_unop         LengthOfStringOp;

    "%int_of_char",  wrap_unop     IntOfCharOp;
    "%char_of_int",  wrap_unop     CharOfIntOp;

    "%alength",      alength_builtin;

    "%raise",        raise_builtin]

(* Build a symboltable from the table *)
let table =
   List.fold_left (fun table (s, op) ->
         StringTable.add table s op) StringTable.empty builtins

let lookup_builtin s =
   try StringTable.find table s with
      Not_found ->
         BuiltinExternal

(*
 * Build code for a builtin if we know something about it.
 *)
let build_builtin genv tenv venv pos loc ty s args cont fail =
   let pos = string_pos "build_builtin" pos in
      match lookup_builtin s, args with
         BuiltinUnop unop, [e] ->
            unop genv tenv venv pos loc e cont
       | BuiltinUnopType unop, [e] ->
            unop genv tenv venv pos loc ty e cont
       | BuiltinBinop binop, [e1; e2] ->
            binop genv tenv venv pos loc e1 e2 cont
       | _ ->
            (* Sanity check to make sure this is supposed to be external *)
            let _ =
               if String.length s = 0 || s.[0] = '%' then
                  raise (IRException (pos, StringError (Printf.sprintf "unknown external function: \"%s\"" (String.escaped s))))
            in

            (* Can't figure out what to do, so call the original function *)
            let v = new_symbol_string s in
            let venv = venv_add_var venv v ty in
            let genv, e, ty_exp = cont genv tenv venv (AtomVar v) in
               fail genv tenv venv v ty_exp e

let build_builtin_exp genv tenv venv pos loc ty s a_fun ty_fun args cont =
   let pos = string_pos "build_builtin_exp" pos in
      build_builtin genv tenv venv pos loc ty s args cont (fun genv tenv venv v ty_exp e ->
            (* Can't figure out what to do, so call the original function *)
            let e = make_exp loc (LetApply (v, ty, a_fun, args, e)) in
               genv, e, ty_exp)

(*
 * Build code for a builtin if we know something about it.
 *)
let build_builtin_ext genv tenv venv pos loc ty s ty_fun args cont =
   let pos = string_pos "build_builtin_ext" pos in
      build_builtin genv tenv venv pos loc ty s args cont (fun genv tenv venv v ty_exp e ->
            let ty_vars, _ = dest_all_type genv pos ty_fun in
            let ty_args = List.map (fun v -> make_type loc (TyVar v)) ty_vars in
            let e = make_exp loc (LetExt (v, ty, s, ty_fun, ty_args, args, e)) in
               genv, e, ty_exp)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
