(*
 * Translate a TAST program to an IR program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol
open Location
open Field_table

open Fir_state

open Aml_ir
open Aml_ir_ds
open Aml_ir_exn
open Aml_ir_pos
open Aml_ir_env
open Aml_ir_type
open Aml_ir_print
open Aml_ir_tast_exp
open Aml_ir_tast_type
open Aml_ir_standardize

module Pos = MakePos (struct let name = "Aml_ir_tast_prog" end)
open Pos

(*
 * Build the boot record.
 *)
let match_failure_var = Symbol.add "Incomplete_match"

let build_boot boot =
   let { Aml_tast.boot_ty_unit_var = ty_unit_var;
         Aml_tast.boot_unit_var    = unit_var;

         Aml_tast.boot_ty_bool_var = ty_bool_var;
         Aml_tast.boot_true_var    = true_var;
         Aml_tast.boot_false_var   = false_var;

         Aml_tast.boot_ty_exn_var  = ty_exn_var
       } = boot
   in
      { boot_ty_unit        = TyUnion (ty_unit_var, [], AnyIndex);
        boot_ty_unit_var    = ty_unit_var;
        boot_unit_var       = unit_var;

        boot_ty_bool        = TyUnion (ty_bool_var, [], AnyIndex);
        boot_ty_bool_var    = ty_bool_var;
        boot_true_var       = true_var;
        boot_false_var      = false_var;
        boot_true_set       = ConstSet (ty_bool_var, SymbolSet.singleton true_var);
        boot_false_set      = ConstSet (ty_bool_var, SymbolSet.singleton false_var);

        boot_ty_exn         = TyDTuple (ty_exn_var, None);
        boot_ty_exn_var     = ty_exn_var;
        boot_match_failure_var = match_failure_var
      }

(*
 * Add the boot types.
 *)
let build_boot_types genv tenv pos loc boot =
   let { Aml_tast.boot_ty_unit = ty_unit;
         Aml_tast.boot_ty_bool = ty_bool;
         Aml_tast.boot_ty_exn  = ty_exn;
         Aml_tast.boot_ty_exn_var  = ty_exn_var
       } = boot
   in
   let ty_unit = Aml_tast_ds.make_type loc ty_unit in
   let ty_bool = Aml_tast_ds.make_type loc ty_bool in
   let ty_exn  = Aml_tast_ds.make_type loc ty_exn in
   let genv, _ = build_type genv tenv pos ty_unit in
   let genv, _ = build_type genv tenv pos ty_bool in
   let genv, _ = build_type genv tenv pos ty_exn in

   (*
    * BUG: this is temporary until we decide how
    * implement the built-in exceptions better.
    *)
   let ty_int = make_type loc TyInt in
   let ty_string = make_type loc TyString in
   let genv = genv_add_tag genv loc match_failure_var ty_exn_var [ty_string; ty_int; ty_int] in
      genv

(*
 * Translate a module field.
 *)
let build_prog prog =
   let { Aml_tast.prog_name   = me_var;
         Aml_tast.prog_names  = names;
         Aml_tast.prog_types  = types;
         Aml_tast.prog_import = import;
         Aml_tast.prog_boot   = boot_tast;
         Aml_tast.prog_exp    = exp
       } = prog
   in
   let file_name = Symbol.reintern me_var in
   let loc  = create_loc file_name 0 0 0 0 in
   let pos = string_pos "build_prog" (loc_pos loc) in

   (* Initial environments *)
   let boot = build_boot boot_tast in
   let tenv_names =
      { names_ty       = SymbolTable.empty;
        names_var      = FieldTable.empty
      }
   in
   let tenv =
      { tenv_tydefs    = types;
        tenv_vars      = SymbolTable.empty;
        tenv_fo_vars   = SymbolTable.empty;
        tenv_so_vars   = SymbolTable.empty;
        tenv_names     = tenv_names
      }
   in
   let genv =
      { genv_types     = SymbolTable.empty;
        genv_globals   = SymbolTable.empty;
        genv_tags      = SymbolTable.empty;
        genv_ty_apply  = Aml_tast_type.TyApplyTable.empty;
        genv_boot      = boot
      }
   in

   (* Build the boot types *)
   let genv = build_boot_types genv tenv pos loc boot_tast in

   (* Compile the import list *)
   let genv, import =
      SymbolTable.fold_map (fun genv _ (hash, me_var, ty) ->
            let genv, ty = build_type genv tenv pos ty in
            let import =
               { import_type = ty;
                 import_info = Fir.ImportGlobal
               }
            in
               genv, import) genv import
   in

   (* Compile the items *)
   let genv, e, ty =
      match exp with
         Aml_tast.Implementation (_, fields, names, ty) ->
            let genv, e, _ =
               build_module_fields genv tenv SymbolTable.empty pos loc fields names ty (fun genv tenv venv a ->
                     let ty = type_of_atom genv venv pos loc a in
                     let a_unit = genv_unit_atom genv loc in
                     let ty_unit = genv_ty_unit genv loc in
                     let e = make_exp loc (Return a_unit) in
                     let e = make_exp loc (SetGlobal (me_var, ty, a, e)) in
                        genv, e, ty_unit)
            in
            let genv, ty = build_type genv tenv pos ty in
               genv, e, ty

       | Aml_tast.Interface _ ->
            raise (Failure "trying to compile an interface")
   in

   (* Wrap it all inside a function called "main" *)
   let main_var = Symbol.add "main" in
   let ty_unit = genv_ty_unit genv loc in
   let ty_main = make_type loc (TyFun ([], ty_unit)) in
   let fundef = loc, FunGlobalClass, [], ty_main, [], e in
   let funs = SymbolTable.add SymbolTable.empty main_var fundef in

   (* Add the module global *)
   let globals = SymbolTable.add genv.genv_globals me_var (ty, InitAtom (AtomNil ty)) in

   (* Export the module and main *)
   let export = SymbolTable.empty in
   let export = SymbolTable.add export me_var { export_name = Symbol.reintern me_var; export_type = ty } in
   let export = SymbolTable.add export main_var { export_name = main_var; export_type = ty_main } in

   (* Build the prog *)
   let prog =
      { prog_name     = me_var;
        prog_names    = names;
        prog_types    = genv.genv_types;
        prog_tags     = genv.genv_tags;
        prog_globals  = globals;
        prog_import   = import;
        prog_export   = export;
        prog_funs     = funs;
        prog_boot     = boot
      }
   in
      if debug debug_print_ir then
         debug_prog "Aml_ir_tast_prog (before standardize)" prog;
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
