(*
 * Translate TAST types to IR types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_ir
open Aml_ir_pos
open Aml_ir_env

val build_module_names : Aml_tast.module_names -> mt_names

val eval_type : genv -> tenv -> Aml_tast.ty -> genv * var_type
val type_of_var_type : genv -> tenv -> pos -> var_type -> genv * ty

val build_type : genv -> tenv -> pos -> Aml_tast.ty -> genv * ty
val build_type_opt : genv -> tenv -> pos -> Aml_tast.ty option -> genv * ty option
val build_types : genv -> tenv -> pos -> Aml_tast.ty list -> genv * ty list

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
