(*
 * Sets of values.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Interval_set

(*
 * Characters.
 *)
module CharElement =
struct
   type t = char
   let compare =
      Pervasives.compare
   let pred c =
      Char.chr (pred (Char.code c))
   let succ c =
      Char.chr (succ (Char.code c))
   let min = '\000'
   let max = '\255'
end

module CharSet = CountableIntervalSet (CharElement)

(*
 * Normal integers.
 *)
module IntElement =
struct
   type t = int
   let compare = Pervasives.compare
   let pred = Pervasives.pred
   let succ = Pervasives.succ
   let min = Pervasives.min_int
   let max = Pervasives.max_int
end

module IntSet = CountableIntervalSet (IntElement)

(*
 * Strings.
 *)
module StringElement =
struct
   type t = string
   let compare = Pervasives.compare
end

module StringSet = DenseIntervalSet (StringElement)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
