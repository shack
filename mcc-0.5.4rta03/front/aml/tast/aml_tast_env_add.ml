(*
 * Adding new values to the environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_mt_util
open Aml_tast_env_type

module Pos = MakePos (struct let name = "Aml_tast_env_add" end)
open Pos

(*
 * Add a name translation.
 *)
let genv_add_name genv ext_var v =
   { genv with genv_names = SymbolTable.add genv.genv_names v ext_var }

(*
 * Set the names in the current module.
 *)
let genv_set_current_names genv names =
   let current = genv.genv_current in
   let core = { current.genv_core with mt_names = names } in
   let current = { current with genv_core = core } in
      { genv with genv_current = current }

(*
 * Add a type definition.
 *)
let genv_add_tydef genv ty_var ty_def =
  { genv with genv_types = SymbolTable.add genv.genv_types ty_var ty_def }

(*
 * Add a new module type.  The type is already added as to the top-level
 * types, so we just need
 *    1. the external name
 *    2. the internal name
 *    3. the top-level name
 *    4. the type arguments
 *)
let genv_add_module_type genv ext_var mt_var v ty ty_args =
   let genv = genv_add_name genv ext_var mt_var in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with mt_opens = SymbolTable.remove opens.mt_opens ext_var } in
   let core =
      { core with mt_mt_types = SymbolTable.add core.mt_mt_types mt_var ty;
                  mt_names = nenv_add_module_type core.mt_names ext_var mt_var ty_args
      }
   in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

(*
 * Add a new module declaration.
 * We get:
 *    1. the external name
 *    2. the internal name
 *    3. the module type
 *)
let genv_add_module genv ext_var me_var ty =
   let genv = genv_add_name genv ext_var me_var in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with me_opens = SymbolTable.remove opens.me_opens ext_var } in
   let core =
      { core with mt_me_vars = SymbolTable.add core.mt_me_vars me_var ty;
                  mt_names = nenv_add_module core.mt_names ext_var me_var
      }
   in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

(*
 * Add a type definition.
 * Remove it from the opens.
 *)
let genv_add_type genv ext_var ty_var ty_vars ty =
   let genv = genv_add_name genv ext_var ty_var in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with me_opens = SymbolTable.remove opens.ty_opens ext_var } in
   let core =
      { core with mt_types = SymbolTable.add core.mt_types ty_var (ty_vars, ty);
                  mt_names = nenv_add_type core.mt_names ext_var ty_var
      }
   in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

(*
 * This version just adds the type _name_.
 * The type itself remains undefined, and will be filled in later.
 *)
let genv_add_type_name genv ext_var ty_var =
   let genv = genv_add_name genv ext_var ty_var in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with me_opens = SymbolTable.remove opens.ty_opens ext_var } in
   let core = { core with mt_names = nenv_add_type core.mt_names ext_var ty_var } in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

(*
 * Add a new module declaration.
 * We get:
 *    1. the external name
 *    2. the internal name
 *    3. the module type
 *)
let genv_add_var genv ext_var v ty =
   let genv = genv_add_name genv ext_var v in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with var_opens = SymbolTable.remove opens.var_opens ext_var } in
   let core =
      { core with mt_vars = SymbolTable.add core.mt_vars v ty;
                  mt_names = nenv_add_var core.mt_names ext_var v
      }
   in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

(*
 * Add the name of a label.
 *)
let genv_add_label genv ext_var label ty_var arity kinds =
   let genv = genv_add_name genv ext_var label in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with label_opens = SymbolTable.remove opens.label_opens ext_var } in
   let core =
      { core with mt_names = nenv_add_label core.mt_names ext_var label;
                  mt_labels = SymbolTable.add core.mt_labels label (ty_var, arity, kinds)
      }
   in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

(*
 * Add the name of a constructor.
 * This is just a scoping issue.
 * We need the type name for the union,
 * and the internal name for the constructor.
 *)
let genv_add_const_aux genv ext_var const_var ty_var arity field =
   let genv = genv_add_name genv ext_var const_var in
   let level = genv.genv_current in
   let { genv_core = core; genv_opens = opens } = level in
   let opens = { opens with const_opens = SymbolTable.remove opens.const_opens ext_var } in
   let core =
      { core with mt_names = nenv_add_const core.mt_names ext_var const_var;
                  mt_consts = SymbolTable.add core.mt_consts const_var (ty_var, arity, field)
      }
   in
   let level = { level with genv_core = core; genv_opens = opens } in
      { genv with genv_current = level }

let genv_add_const genv ext_var const_var ty_var arity kinds =
   genv_add_const_aux genv ext_var const_var ty_var arity (ConstNormal kinds)

let genv_add_exn genv ext_var const_var ty_var tyl =
   genv_add_const_aux genv ext_var const_var ty_var 0 (ConstException tyl)

(*
 * Extend a union type with the given case.
 *)
let genv_add_union_case genv pos ty_var const_var ty_args =
   let pos = string_pos "genv_add_union_case" pos in
   let tydef =
      try SymbolTable.find genv.genv_types ty_var with
         Not_found ->
            raise (TAstException (pos, UnboundType ty_var))
   in
   let { tydef_inner = inner } = tydef in
   let loc = loc_of_tydef_inner inner in
   let inner =
      match dest_tydef_inner_core inner with
         TyDefUnion (ExnUnion, fields) ->
            (* Add the new field *)
            let fields = FieldTable.add fields const_var ty_args in
               TyDefUnion (ExnUnion, fields)
       | TyDefUnion _ ->
            raise (TAstException (pos, StringVarError ("union can't be extended", ty_var)))
       | _ ->
            raise (TAstException (pos, StringVarError ("not a union type", ty_var)))
   in
   let inner = make_tydef_inner loc inner in
   let tydef = { tydef with tydef_inner = inner } in
      { genv with genv_types = SymbolTable.add genv.genv_types ty_var tydef }

(************************************************************************
 * INTERNAL FORMS
 ************************************************************************)

(*
 * These do not expect an external name.
 *)
let genv_add_module_type_int genv v ty =
   let level = genv.genv_current in
   let core = level.genv_core in
   let core = { core with mt_mt_types = SymbolTable.add core.mt_mt_types v ty } in
   let level = { level with genv_core = core } in
      { genv with genv_current = level }

let genv_add_module_int genv v ty =
   let level = genv.genv_current in
   let core = level.genv_core in
   let core = { core with mt_me_vars = SymbolTable.add core.mt_me_vars v ty } in
   let level = { level with genv_core = core } in
      { genv with genv_current = level }

let genv_add_type_int genv v ty_vars ty =
   let level = genv.genv_current in
   let core = level.genv_core in
   let core = { core with mt_types = SymbolTable.add core.mt_types v (ty_vars, ty) } in
   let level = { level with genv_core = core } in
      { genv with genv_current = level }

let genv_add_var_int genv v ty =
   let level = genv.genv_current in
   let core = level.genv_core in
   let core = { core with mt_vars = SymbolTable.add core.mt_vars v ty } in
   let level = { level with genv_core = core } in
      { genv with genv_current = level }

let genv_add_exn_int genv v ty_var tyl =
   let level = genv.genv_current in
   let core = level.genv_core in
   let core = { core with mt_consts = SymbolTable.add core.mt_consts v (ty_var, 0, ConstException tyl) } in
   let level = { level with genv_core = core } in
      { genv with genv_current = level }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
