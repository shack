(*
 * Some utilities on the names record.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast

(*
 * Name environment.
 *)
val nenv_empty : module_names

val nenv_add_module_type : module_names -> ext_var -> mt_var -> ty list -> module_names
val nenv_add_module      : module_names -> ext_var -> me_var -> module_names
val nenv_add_type        : module_names -> ext_var -> ty_var -> module_names
val nenv_add_var         : module_names -> ext_var -> var -> module_names
val nenv_add_const       : module_names -> ext_var -> const_var -> module_names
val nenv_add_label       : module_names -> ext_var -> label -> module_names

val nenv_lookup_module_type : module_names -> ext_var -> mt_var * ty list
val nenv_lookup_module      : module_names -> ext_var -> me_var
val nenv_lookup_type        : module_names -> ext_var -> ty_var
val nenv_lookup_var         : module_names -> ext_var -> var
val nenv_lookup_const       : module_names -> ext_var -> const_var
val nenv_lookup_label       : module_names -> ext_var -> label

val nenv_mem_module_type : module_names -> ext_var -> bool
val nenv_mem_module      : module_names -> ext_var -> bool
val nenv_mem_type        : module_names -> ext_var -> bool
val nenv_mem_var         : module_names -> ext_var -> bool
val nenv_mem_const       : module_names -> ext_var -> bool
val nenv_mem_label       : module_names -> ext_var -> bool

(*
 * Module utilities.
 *)
val mt_mem_module_type : module_type -> mt_var -> bool
val mt_mem_module      : module_type -> me_var -> bool
val mt_mem_type        : module_type -> ty_var -> bool
val mt_mem_var         : module_type -> var -> bool
val mt_mem_const       : module_type -> const_var -> bool
val mt_mem_label       : module_type -> label -> bool

val mt_mem_module_type_ext : module_type -> ext_var -> bool
val mt_mem_module_ext      : module_type -> ext_var -> bool
val mt_mem_type_ext        : module_type -> ext_var -> bool
val mt_mem_var_ext         : module_type -> ext_var -> bool
val mt_mem_const_ext       : module_type -> ext_var -> bool
val mt_mem_label_ext       : module_type -> ext_var -> bool

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
