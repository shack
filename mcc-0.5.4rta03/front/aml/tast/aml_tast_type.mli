(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_tast
open Aml_tast_pos
open Aml_tast_env_type

(*
 * Eliminate outermost TyProject.
 *)
val expand_type : genv -> pos -> ty -> ty

(*
 * Type destructors.
 *)
val dest_all_type        : genv -> pos -> ty -> ty_var list * ty
val dest_all_rename_type : genv -> pos -> ty -> ty_var list * ty

val dest_fun_type_all : genv -> pos -> ty -> ty_var list * ty list * ty
val dest_array_type   : genv -> pos -> ty -> ty

(*
 * Unwrap the function type an inline all the arguments.
 *)
val flatten_fun_type : genv -> pos -> loc -> ty -> ty

(*
 * A Type table.
 *)
module TypeSet2 : Mc_set.McSet with type elt = ty * ty
module TypeKindSet2 : Mc_set.McSet with type elt = ty_kind * ty_kind

module TypeTable : Mc_map.McMap with type key = ty
module TypeTable2 : Mc_map.McMap with type key = ty * ty

module TypeNameTable : Mc_map.McMap with type key = ty_name

(*
 * A type application table.
 *)
module TyApplyTable : Mc_map.McMap with type key = ty_var * ty_kind list

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
