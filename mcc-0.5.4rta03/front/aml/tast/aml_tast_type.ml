(*
 * This file defines utilities on types.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env
open Aml_tast_print

module Pos = MakePos (struct let name = "Aml_tast_type" end)
open Pos

(************************************************************************
 * TYPE EXPANSION
 ************************************************************************)

(*
 * Type application.
 *)
let apply_type pos ty_vars ty tyl =
   let pos = string_pos "apply_type" pos in
   let len1 = List.length ty_vars in
   let len2 = List.length tyl in
   let _ =
      if len1 <> len2 then
         raise (TAstException (pos, ArityMismatch (len1, len2)))
   in
   let subst = List.fold_left2 subst_add_fo_var empty_subst ty_vars tyl in
      subst_type subst ty

(*
 * Expand a type, removing the outermost TyProject if possible.
 *)
let rec expand_type genv pos ty =
   let pos = string_pos "expand_type" pos in
      match dest_type_core ty with
         TyProject (ty_name, tyl) ->
            (match genv_lookup_type genv pos ty_name with
                ty_vars, Some ty ->
                   apply_type pos ty_vars ty tyl
              | _, None ->
                   ty)
       | TyAll ([], ty) ->
            expand_type genv pos ty
       | _ ->
            ty

let expand_type_core genv pos ty =
   dest_type_core (expand_type genv pos ty)

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Destruct a forall type.
 *)
let rec dest_all_type genv pos ty =
   let pos = string_pos "dest_all_type" pos in
      match expand_type_core genv pos ty with
         TyAll (vars1, ty) ->
            let vars2, ty = dest_all_type genv pos ty in
               vars1 @ vars2, ty
       | _ ->
            [], ty

let dest_all_rename_type genv pos ty =
   let pos = string_pos "dest_all_rename_type" pos in
   let loc = loc_of_type ty in
   let ty_vars, ty = dest_all_type genv pos ty in
      if ty_vars = [] then
         ty_vars, ty
      else
         let ty_vars' = List.map new_symbol ty_vars in
         let subst =
            List.fold_left2 (fun subst v1 v2 ->
                  subst_add_fo_var subst v1 (make_type loc (TyVar v2))) empty_subst ty_vars ty_vars'
         in
            ty_vars', subst_type subst ty

(*
 * "Flatten"a function type: all the arguments are collected at once.
 *)
let rec dest_fun_type_all genv pos ty =
   let pos = string_pos "dest_fun_type_all" pos in
      match expand_type_core genv pos ty with
         TyAll (ty_vars, ty) ->
            let ty_vars', tyl', ty = dest_fun_type_all genv pos ty in
               ty_vars @ ty_vars', tyl', ty
       | TyFun (tyl, ty) ->
            let ty_vars', tyl', ty = dest_fun_type_all genv pos ty in
               ty_vars', tyl @ tyl', ty
       | _ ->
            [], [], ty

let flatten_fun_type genv pos loc ty =
   let pos = string_pos "flatten_fun_type" pos in
   let ty_vars, tyl, ty = dest_fun_type_all genv pos ty in
   let ty_fun =
      match tyl with
         [] ->
            ty
       | _ ->
            make_type loc (TyFun (tyl, ty))
   in
      make_type loc (TyAll (ty_vars, ty_fun))

(************************************************************************
 * TYPE DESTRUCTORS
 ************************************************************************)

(*
 * Plain type destructors.
 *)
let dest_array_type genv pos ty =
   match expand_type_core genv pos ty with
      TyArray ty ->
         ty
    | _ ->
         raise (TAstException (pos, StringTypeError ("not an array type", ty)))

(************************************************************************
 * TYPE TABLES
 ************************************************************************)

(*
 * We're cheating here.
 * Make sure the tags are different.
 *)
let compare_tags (pp_print : formatter -> 'a -> unit) s (obj1 : 'a) (obj2 : 'a) =
   let tag_of_object x =
      let x = Obj.repr x in
         if Obj.is_int x then
            Obj.magic x
         else
            Obj.tag x + 1000
   in
   let tag1 = tag_of_object obj1 in
   let tag2 = tag_of_object obj2 in
      if tag1 = tag2 then
         begin
            eprintf "@[<hv 3>compare_tags: tags are identical: %s: %d@ Obj1: %a@ Obj2: %a@]@." (**)
               s tag1
               pp_print obj1
               pp_print obj2;
            exit 5
         end;
      tag1 - tag2

(*
 * Compare two types.
 * This is a fairly literal comparison,
 * although we try do do a little with alpha equality.
 *)
let rec compare_types eq_ty_vars eq_vars ty1 ty2 =
   match dest_type_core ty1, dest_type_core ty2 with
      TyInt, TyInt
    | TyChar, TyChar
    | TyString, TyString
    | TyFloat, TyFloat ->
         0
    | TyFun (tyl1, ty1), TyFun (tyl2, ty2) ->
         let cmp = compare_types eq_ty_vars eq_vars ty1 ty2 in
            if cmp = 0 then
               compare_type_lists eq_vars eq_ty_vars tyl1 tyl2
            else
               cmp
    | TyFunctor (v1, ty_arg1, ty_res1), TyFunctor (v2, ty_arg2, ty_res2) ->
         let eq_vars = (v1, v2) :: eq_ty_vars in
         let cmp = compare_types eq_ty_vars eq_vars ty_arg1 ty_arg2 in
            if cmp = 0 then
               compare_types eq_ty_vars eq_vars ty_res1 ty_res2
            else
               cmp
    | TyProd tyl1, TyProd tyl2 ->
         compare_type_lists eq_ty_vars eq_vars tyl1 tyl2
    | TyArray ty1, TyArray ty2 ->
         compare_types eq_ty_vars eq_vars ty1 ty2
    | TyAll (vars1, ty1), TyAll (vars2, ty2) ->
         let len1 = List.length vars1 in
         let len2 = List.length vars2 in
         let cmp = len1 - len2 in
            if cmp = 0 then
               let eq_ty_vars = List.fold_left2 (fun eq_ty_vars v1 v2 -> (v1, v2) :: eq_ty_vars) eq_ty_vars vars1 vars2 in
                  compare_types eq_ty_vars eq_vars ty1 ty2
            else
               cmp
    | TyVar v1, TyVar v2 ->
         if List.mem (v1, v2) eq_ty_vars then
            0
         else
            Symbol.compare v1 v2
    | TySOVar (v1, tyl1), TySOVar (v2, tyl2) ->
         let cmp = Symbol.compare v1 v2 in
            if cmp = 0 then
               compare_type_lists eq_ty_vars eq_vars tyl1 tyl2
            else
               cmp
    | TyProject (ty_name1, tyl1), TyProject (ty_name2, tyl2) ->
         let cmp = compare_type_names eq_vars ty_name1 ty_name2 in
            if cmp = 0 then
               compare_type_lists eq_ty_vars eq_vars tyl1 tyl2
            else
               cmp
    | TyApply (ty_var1, tyl1, kindl1),  TyApply (ty_var2, tyl2, kindl2)
    | TyUnion (ty_var1, tyl1, kindl1),  TyUnion (ty_var2, tyl2, kindl2)
    | TyRecord (ty_var1, tyl1, kindl1), TyRecord (ty_var2, tyl2, kindl2)
    | TyModule (ty_var1, tyl1, kindl1), TyModule (ty_var2, tyl2, kindl2) ->
         let cmp = Symbol.compare ty_var1 ty_var2 in
            if cmp = 0 then
               let cmp = compare_type_lists eq_ty_vars eq_vars tyl1 tyl2 in
                  if cmp = 0 then
                     compare_type_kind_lists eq_vars kindl1 kindl2
                  else
                     cmp
            else
               cmp
    | TyFormat (ty_a1, ty_b1, ty_c1), TyFormat (ty_a2, ty_b2, ty_c2) ->
         let cmp = compare_types eq_ty_vars eq_vars ty_a1 ty_a2 in
            if cmp = 0 then
               let cmp = compare_types eq_ty_vars eq_vars ty_b1 ty_b2 in
                  if cmp = 0 then
                     compare_types eq_ty_vars eq_vars ty_c1 ty_c2
                  else
                     cmp
            else
               cmp
    | TyExternal (ty1, s1), TyExternal (ty2, s2) ->
         let cmp = Pervasives.compare s1 s2 in
            if cmp = 0 then
               compare_types eq_ty_vars eq_vars ty1 ty2
            else
               cmp
    | TyOpen (ty_opens1, ty_var1), TyOpen (ty_opens2, ty_var2) ->
         let cmp = compare_type_opens eq_ty_vars eq_vars ty_opens1 ty_opens2 in
            if cmp = 0 then
               Symbol.compare ty_var1 ty_var2
            else
               cmp
    | ty1, ty2 ->
         compare_tags pp_print_type_core "compare_types" ty1 ty2

(*
 * Compare two lists of types.
 *)
and compare_type_lists eq_ty_vars eq_vars tyl1 tyl2 =
   match tyl1, tyl2 with
      ty1 :: tyl1, ty2 :: tyl2 ->
         let cmp = compare_types eq_ty_vars eq_vars ty1 ty2 in
            if cmp = 0 then
               compare_type_lists eq_ty_vars eq_vars tyl1 tyl2
            else
               cmp
    | [], [] ->
         0
    | _, [] ->
         1
    | [], _ ->
         -1

(*
 * Compare two type names.
 *)
and compare_type_names eq_vars ty_name1 ty_name2 =
   match ty_name1, ty_name2 with
      TyNameSoVar v1, TyNameSoVar v2 ->
         if List.mem (v1, v2) eq_vars then
            0
         else
            Symbol.compare v1 v2
    | TyNameSelf (me_var1, v1), TyNameSelf (me_var2, v2) ->
         let cmp = Symbol.compare me_var1 me_var2 in
            if cmp = 0 then
               Symbol.compare v1 v2
            else
               cmp
    | TyNamePath (me_name1, v1), TyNamePath (me_name2, v2) ->
         let cmp = compare_mod_names eq_vars me_name1 me_name2 in
            if cmp = 0 then
               Symbol.compare v1 v2
            else
               cmp
    | _ ->
         compare_tags pp_print_type_name "compare_type_names" ty_name1 ty_name2

(*
 * Compare two module names.
 *)
and compare_mod_names eq_vars me_name1 me_name2 =
   match me_name1, me_name2 with
      ModNameEnv v1, ModNameEnv v2 ->
         if List.mem (v1, v2) eq_vars then
            0
         else
            Symbol.compare v1 v2
    | ModNameProj (me_name1, l1), ModNameProj (me_name2, l2) ->
         let cmp = compare_mod_names eq_vars me_name1 me_name2 in
            if cmp = 0 then
               Symbol.compare l1 l2
            else
               cmp
    | ModNameApply (me_name_fun1, me_name_arg1), ModNameApply (me_name_fun2, me_name_arg2) ->
         let cmp = compare_mod_names eq_vars me_name_fun1 me_name_fun2 in
            if cmp = 0 then
               compare_mod_names eq_vars me_name_arg1 me_name_arg2
            else
               cmp
    | _ ->
         compare_tags pp_print_mod_name "compare_mod_names" me_name1 me_name2

(*
 * Compare type kinds.
 *)
and compare_type_kinds eq_vars kind1 kind2 =
   let { kind_arity = arity1; kind_value = name1 } = kind1 in
   let { kind_arity = arity2; kind_value = name2 } = kind2 in
   let cmp = arity1 - arity2 in
      if cmp = 0 then
         compare_type_names eq_vars name1 name2
      else
         cmp

and compare_type_kind_lists eq_vars kinds1 kinds2 =
   match kinds1, kinds2 with
      kind1 :: kinds1, kind2 :: kinds2 ->
         let cmp = compare_type_kinds eq_vars kind1 kind2 in
            if cmp = 0 then
               compare_type_kind_lists eq_vars kinds1 kinds2
            else
               cmp
    | [], [] ->
         0
    | [], _ ->
         -1
    | _, [] ->
         1

(*
 * Compare two open lists.
 *)
and compare_type_opens eq_ty_vars eq_vars opens1 opens2 =
   match opens1, opens2 with
      (v1, ty1) :: opens1, (v2, ty2) :: opens2 ->
         let cmp = Symbol.compare v1 v2 in
            if cmp = 0 then
               let cmp = compare_types eq_ty_vars eq_vars ty1 ty2 in
                  if cmp = 0 then
                     compare_type_opens eq_ty_vars eq_vars opens1 opens2
                  else
                     cmp
            else
               cmp
    | [], [] ->
         0
    | _, [] ->
         1
    | [], _ ->
         -1

(*
 * Build the table.
 *)
module TypeCompare =
struct
   type t = ty
   let compare = compare_types [] []
end

module TypeCompare2 =
struct
   type t = ty * ty
   let compare (t11, t12) (t21, t22) =
      let cmp = TypeCompare.compare t11 t21 in
         if cmp = 0 then
            TypeCompare.compare t12 t22
         else
            cmp
end

module TypeNameCompare =
struct
   type t = ty_name
   let compare = compare_type_names []
end

module TypeKindCompare2 =
struct
   type t = ty_kind * ty_kind
   let compare (k11, k12) (k21, k22) =
      let cmp = compare_type_kinds [] k11 k21 in
         if cmp = 0 then
            compare_type_kinds [] k12 k22
         else
            cmp
end

module TypeTable = Mc_map.McMake (TypeCompare)
module TypeTable2 = Mc_map.McMake (TypeCompare2)

module TypeNameTable = Mc_map.McMake (TypeNameCompare)

module TypeSet2 = Mc_set.McMake (TypeCompare2)
module TypeKindSet2 = Mc_set.McMake (TypeKindCompare2)

(************************************************************************
 * TYPE APPLICATION TABLE
 ************************************************************************)

(*
 * This is used to remember values associated with TyApply types.
 *)
module TyApplyCompare =
struct
   type t = ty_var * ty_kind list

   (*
    * Compare two keys.
    *)
   let compare (ty_var1, ty_kinds1) (ty_var2, ty_kinds2) =
      let cmp = Symbol.compare ty_var1 ty_var2 in
         if cmp = 0 then
            compare_type_kind_lists [] ty_kinds1 ty_kinds2
         else
            cmp
end

module TyApplyTable = Mc_map.McMake (TyApplyCompare)


(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
