(*
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Aml_tast

(*
 * Exceptions.
 *)
type tast_error =
   UnboundVar of var
 | UnboundConst of const_var
 | UnboundLabel of label
 | UnboundType of ext_var
 | UnboundTyVar of ty_var
 | UnboundModule of me_var
 | UnboundModuleName of me_name
 | UnboundModuleType of mt_var
 | UnboundBuiltin of string

 | MissingVar        of ext_var
 | MissingConst      of ext_var
 | MissingLabels     of ext_var list
 | MissingType       of ext_var
 | MissingModule     of ext_var
 | MissingModuleType of ext_var
 | MissingParam      of ext_var

 | ExtMultipleBinding of ext_var

 | TypeMismatch of ty * ty
 | TypeMismatch4 of ty * ty * ty * ty
 | ConstMismatch of ext_var * ext_var
 | VarTypeMismatch of ext_var * ty * ty
 | TydefInnerCoreMismatch of tydef_inner_core * tydef_inner_core

 | DuplicateConst of const_var * ty

 | ArityMismatch of int * int
 | VarArityMismatch of ty_var * int * int
 | TypeArityMismatch of ty * int * int
 | ConstArityMismatch of const_var * int * int

 | RecordLabelMismatch of label * ty
 | ConstLabelMismatch of const_var * ty
 | UnionTypeMismatch of union_type * union_type

 | NotImplemented of string
 | InternalError

 | StringError of string
 | StringTypeError of string * ty
 | StringTydefError of string * tydef
 | StringTydefInnerError of string * tydef_inner
 | StringVarError of string * var
 | StringPattError of string * patt

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
