(*
 * We define the variables abstractly.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
module Var           = Symbol.Symbol
module LabelVar      = Symbol.Symbol
module ConstVar      = Symbol.Symbol
module TypeVar       = Symbol.Symbol
module ModuleVar     = Symbol.Symbol
module ModuleTypeVar = Symbol.Symbol
module ParamVar      = Symbol.Symbol

module VarTable        = Var.SymbolTable
module LabelTable      = LabelVar.SymbolTable
module ConstTable      = ConstVar.SymbolTable
module TypeTable       = TypeVar.SymbolTable
module ModuleTable     = ModuleVar.SymbolTable
module ModuleTypeTable = ModuleTypeVar.SymbolTable
module ParamTable      = ParamVar.SymbolTable

module VarSet        = Var.SymbolSet
module LabelSet      = LabelVar.SymbolSet
module ConstSet      = ConstVar.SymbolSet
module TypeSet       = TypeVar.SymbolSet
module ModuleSet     = ModuleVar.SymbolSet
module ModuleTypeSet = ModuleTypeVar.SymbolSet
module ParamSet      = ParamVar.SymbolSet

let new_symbol_ast_var   = Var.new_symbol
let new_symbol_ast_label = LabelVar.new_symbol
let new_symbol_ast_const = ConstVar.new_symbol
let new_symbol_ast_type  = TypeVar.new_symbol
let new_symbol_ast_me    = ModuleVar.new_symbol
let new_symbol_ast_mt    = ModuleTypeVar.new_symbol
let new_symbol_ast_param = ParamVar.new_symbol

let var_of_label_var v = v
let var_of_const_var v = v
let var_of_ty_var v = v
let var_of_me_var v = v
let var_of_mt_var v = v
let var_of_param_var v = v

let label_var_of_var v = v
let label_var_of_me_var v = v
let label_var_of_ty_var v = v

let ty_var_of_var v = v
let ty_var_of_param_var v = v
let ty_var_of_me_var v = v
let ty_var_of_mt_var v = v
let ty_var_of_const_var v = v
let mt_var_of_ty_var v = v

let me_var_of_var v = v
let var_of_me_var v = v
let me_var_of_mt_var v = v
let mt_var_of_me_var v = v

let ext_var_of_var v = v
let ext_var_of_me_var v = v

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
