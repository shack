(*
 * Type inference.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Debug
open Symbol
open Location
open Field_table

open Fir_state

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env
open Aml_tast_util
open Aml_tast_type
open Aml_tast_tydef
open Aml_tast_unify
open Aml_tast_quant
open Aml_tast_print
open Aml_tast_standardize

let debug_type =
   create_debug (**)
      { debug_name = "type";
        debug_description = "print results of type inference";
        debug_value = false
      }

let debug_infer =
   create_debug (**)
      { debug_name = "infer";
        debug_description = "print internal operations during type inference";
        debug_value = false
      }

module Pos = MakePos (struct let name = "Aml_tast_infer" end)
open Pos

(************************************************************************
 * VARIABLE ENVIRONMENT
 ************************************************************************)

(*
 * The variable environment is just a symbol table.
 *)
type venv = ty SymbolTable.t

let venv_empty = SymbolTable.empty

let venv_add_var = SymbolTable.add

let venv_lookup_var = SymbolTable.find

(*
 * Two stage lookup: it should be either in venv or genv.
 *)
let gvenv_lookup_var genv venv pos id =
   let pos = string_pos "gvenv_lookup_var" pos in
      match id with
         NameEnv v ->
            (try venv_lookup_var venv v with
                Not_found ->
                   genv_lookup_var genv pos id)
       | _ ->
            genv_lookup_var genv pos id

(************************************************************************
 * GENERIC VARS ENVIRONMENT
 ************************************************************************)

(*
 * "Generic" vars are type variables that are defined outside of
 * a polymorphic value.  The value can't be quantified over these
 * vars.  The gvars is just a set.
 *)
type gvars = SymbolSet.t

let gvars_empty = SymbolSet.empty

let gvars_mem = SymbolSet.mem

let gvars_add_var = SymbolSet.add

let gvars_add_vars gvars vars =
   List.fold_left gvars_add_var gvars vars

let gvars_to_set gvars =
   gvars

(*
 * Add the type variables in the type.
 *)
let rec gvars_add_type gvars ty =
   match dest_type_core ty with
      TyInt
    | TyChar
    | TyString
    | TyFloat ->
         gvars
    | TyFun (ty_args, ty_res) ->
         gvars_add_types (gvars_add_type gvars ty_res) ty_args
    | TyFunctor (v, ty_arg, ty_res) ->
         gvars_add_var (gvars_add_type (gvars_add_type gvars ty_res) ty_arg) v
    | TyProd tyl
    | TyProject (_, tyl) ->
         gvars_add_types gvars tyl
    | TyArray ty ->
         gvars_add_type gvars ty
    | TyAll (vars, ty) ->
         gvars_add_vars (gvars_add_type gvars ty) vars
    | TyVar v ->
         gvars_add_var gvars v
    | TySOVar (v, tyl) ->
         gvars_add_var (gvars_add_types gvars tyl) v
    | TyApply (v, tyl, kinds)
    | TyUnion (v, tyl, kinds)
    | TyRecord (v, tyl, kinds)
    | TyModule (v, tyl, kinds) ->
         gvars_add_var (gvars_add_types (gvars_add_kinds gvars kinds) tyl) v
    | TyFormat (ty1, ty2, ty3) ->
         gvars_add_type (gvars_add_type (gvars_add_type gvars ty1) ty2) ty3
    | TyExternal (ty, _) ->
         gvars_add_type gvars ty
    | TyOpen (fields, v) ->
         raise (Failure "Aml_tast_unify.gvars_add_type: TyOpen not implemented")

and gvars_add_types gvars tyl =
   List.fold_left gvars_add_type gvars tyl

and gvars_add_type_name gvars ty_name =
   match ty_name with
      TyNameSoVar v ->
         gvars_add_var gvars v
    | TyNameSelf _
    | TyNamePath _ ->
         gvars

and gvars_add_kind gvars { kind_value = ty_name } =
   gvars_add_type_name gvars ty_name

and gvars_add_kinds gvars kinds =
   List.fold_left gvars_add_kind gvars kinds

(*
 * Quantify the type's free vars, minus any generic vars.
 *)
let pp_print_symbol_set buf s =
   fprintf buf "@[<b 0>";
   SymbolSet.iter (fun v -> fprintf buf "@ %a" pp_print_symbol v) s;
   fprintf buf "@]"

let wrap_all_type loc gvars subst ty =
   let ty = subst_type subst ty in
   let { fv_fo_vars = fv } = free_vars_type ty in
   let _ =
      if debug debug_infer then
         eprintf "@[<hv 3>wrap_all_type:@ type = %a;@ fv = %a;@ gvars = %a@]@." (**)
            pp_print_type ty
            pp_print_symbol_set fv
            pp_print_symbol_set gvars
   in
   let vars = SymbolSet.diff fv gvars in
   let vars = SymbolSet.to_list vars in
      make_type loc (TyAll (vars, ty))

(************************************************************************
 * PATTERNS
 ************************************************************************)

(*
 * When inferring a pattern, we add the new variables
 * to the variable environment.
 *)
let rec infer_patt genv venv bvars gvars subst patt =
   let pos = string_pos "infer_patt" (patt_exp_pos patt) in
      match dest_patt_core patt with
         PattInt (i, ty) ->
            let gvars = gvars_add_type gvars ty in
            let subst = unify_type_int genv bvars pos subst ty in
               gvars, subst, venv, ty
       | PattChar (c, ty) ->
            let gvars = gvars_add_type gvars ty in
            let subst = unify_type_char genv bvars pos subst ty in
               gvars, subst, venv, ty
       | PattString (_, ty) ->
            let gvars = gvars_add_type gvars ty in
            let subst = unify_type_string genv bvars pos subst ty in
               gvars, subst, venv, ty
       | PattWild ty ->
            let gvars = gvars_add_type gvars ty in
               gvars, subst, venv, ty
       | PattVar (v, ty) ->
            let gvars = gvars_add_type gvars ty in
            let venv = venv_add_var venv v ty in
               gvars, subst, venv, ty
       | PattTuple (pl, ty) ->
            infer_tuple_patt genv venv bvars gvars pos subst pl ty
       | PattRecord (fields, ty) ->
            infer_record_patt genv venv bvars gvars pos subst fields ty
       | PattArray (pl, ty) ->
            infer_array_patt genv venv bvars gvars pos subst pl ty
       | PattConst (ty_var, const_var, pl, ty) ->
            infer_const_patt genv venv bvars gvars pos subst ty_var const_var pl ty
       | PattChoice (p1, p2, ty) ->
            infer_choice_patt genv venv bvars gvars pos subst p1 p2 ty
       | PattAs (p1, p2, ty) ->
            infer_as_patt genv venv bvars gvars pos subst p1 p2 ty
       | PattExpr (e, ty) ->
            infer_exp_patt genv venv bvars gvars pos subst e ty
       | PattRange (e1, e2, ty) ->
            infer_range_patt genv venv bvars gvars pos subst e1 e2 ty
       | PattWhen (p, e, ty) ->
            infer_when_patt genv venv bvars gvars pos subst p e ty
       | PattConstrain (p, ty) ->
            infer_constrain_patt genv venv bvars gvars pos subst p ty

and infer_patts genv venv bvars gvars subst pl =
   let gvars, subst, venv, tyl =
      List.fold_left (fun (gvars, subst, venv, tyl) p ->
            let gvars, subst, venv, ty = infer_patt genv venv bvars gvars subst p in
               gvars, subst, venv, ty :: tyl) (gvars, subst, venv, []) pl
   in
      gvars, subst, venv, List.rev tyl

(*
 * Tuple type.
 * Get the type of the elements,
 * then unify with the type.
 *)
and infer_tuple_patt genv venv bvars gvars pos subst pl ty =
   let pos = string_pos "infer_tuple_patt" pos in
   let gvars, subst, venv, tyl = infer_patts genv venv bvars gvars subst pl in
   let loc = loc_of_type ty in
   let ty' = make_type loc (TyProd tyl) in
   let subst = unify_types genv bvars pos subst ty ty' in
      gvars, subst, venv, ty

(*
 * Array type.
 * The element types must all unify with the array type.
 *)
and infer_array_patt genv venv bvars gvars pos subst pl ty =
   let pos = string_pos "infer_array_patt" pos in
   let gvars = gvars_add_type gvars ty in
   let ty_elem = dest_array_type genv pos ty in
   let gvars, subst, venv =
      List.fold_left (fun (gvars, subst, venv) p ->
            let gvars, subst, venv, ty_elem' = infer_patt genv venv bvars gvars subst p in
            let subst = unify_types genv bvars pos subst ty_elem ty_elem' in
               gvars, subst, venv) (gvars, subst, venv) pl
   in
      gvars, subst, venv, ty

(*
 * Record pattern.
 *
 * This is harder because the record pattern may be partial.
 * Infer the types of the fields.
 *)
and infer_record_patt genv venv bvars gvars pos subst fields ty =
   let pos = string_pos "infer_record_patt" pos in
   let gvars = gvars_add_type gvars ty in

   (* The type _must_ be a record type *)
   let ty_fields = dest_record_type genv pos ty in

   (* Unify with the fields *)
   let gvars, subst, venv =
      List.fold_left (fun (gvars, subst, venv) (label, p) ->
            let _, ty1 =
               try FieldTable.find ty_fields label with
                  Not_found ->
                     raise (TAstException (pos, RecordLabelMismatch (label, ty)))
            in
            let gvars, subst, venv, ty2 = infer_patt genv venv bvars gvars subst p in
            let subst = unify_types genv bvars pos subst ty1 ty2 in
               gvars, subst, venv) (gvars, subst, venv) fields
   in
      gvars, subst, venv, ty

(*
 * Constructor patterm
 *)
and infer_const_patt genv venv bvars gvars pos subst ty_var const_var pl ty =
   let pos = string_pos "infer_const_patt" pos in
   let gvars = gvars_add_type gvars ty in

   (* The type _must_ be a union type, and it may be universally quantified *)
   let _, ty = dest_all_rename_type genv pos ty in
   let kind, ty_fields = dest_union_type genv pos ty in

   (* Find the named field *)
   let ty_fields =
      match kind with
         NormalUnion ->
            (* The named field should be present *)
            (try FieldTable.find ty_fields const_var with
                Not_found ->
                   raise (TAstException (pos, ConstLabelMismatch (const_var, ty))))
       | ExnUnion ->
            (* The named field will not be present, so use the fields as declared *)
            genv_lookup_exn genv pos const_var
   in

   (* Unify with the pattern *)
   let gvars, subst, venv, ty_fields' = infer_patts genv venv bvars gvars subst pl in
   let len1 = List.length ty_fields in
   let len2 = List.length ty_fields' in
   let _ =
      if len1 <> len2 then
         raise (TAstException (pos, VarArityMismatch (const_var, len1, len2)))
   in
   let subst = unify_type_lists genv bvars pos subst ty_fields ty_fields' in
      gvars, subst, venv, ty

(*
 * Choice pattern.
 * BUG: this is wrong for the moment because we have to
 * check that the binding variables have all the same types.
 *)
and infer_choice_patt genv venv bvars gvars pos subst p1 p2 ty =
   let pos = string_pos "infer_choice_patt" pos in
   let gvars, subst, venv, ty1 = infer_patt genv venv bvars gvars subst p1 in
   let gvars, subst, venv, ty2 = infer_patt genv venv bvars gvars subst p2 in
   let subst = unify_types genv bvars pos subst ty ty1 in
   let subst = unify_types genv bvars pos subst ty ty2 in
      gvars, subst, venv, ty

(*
 * As pattern.
 *)
and infer_as_patt genv venv bvars gvars pos subst p1 p2 ty =
   let pos = string_pos "infer_as_patt" pos in
   let gvars, subst, venv, ty1 = infer_patt genv venv bvars gvars subst p1 in
   let gvars, subst, venv, ty2 = infer_patt genv venv bvars gvars subst p2 in
   let subst = unify_types genv bvars pos subst ty ty1 in
   let subst = unify_types genv bvars pos subst ty ty2 in
      gvars, subst, venv, ty

(*
 * This is an expression pattern.
 * The test is by equality.
 *)
and infer_exp_patt genv venv bvars gvars pos subst e ty =
   let pos = string_pos "infer_exp_patt" pos in
   let subst, ty' = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_types genv bvars pos subst ty ty' in
      gvars, subst, venv, ty

(*
 * Range pattern.
 *)
and infer_range_patt genv venv bvars gvars pos subst e1 e2 ty =
   let pos = string_pos "infer_range_patt" pos in
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in
   let subst = unify_types genv bvars pos subst ty ty1 in
   let subst = unify_types genv bvars pos subst ty ty2 in
      gvars, subst, venv, ty

(*
 * Pattern when a Boolean condition is true.
 *)
and infer_when_patt genv venv bvars gvars pos subst p e ty =
   let pos = string_pos "infer_when_patt" pos in

   (* Unify the pattern *)
   let gvars, subst, venv, ty' = infer_patt genv venv bvars gvars subst p in
   let subst = unify_types genv bvars pos subst ty ty' in

   (* Expression must be Bool *)
   let subst, ty_exp = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_type_bool genv bvars pos subst ty_exp in
      gvars, subst, venv, ty

(*
 * Type constraint.
 *)
and infer_constrain_patt genv venv bvars gvars pos subst p ty =
   let pos = string_pos "infer_constrain_patt" pos in

   (* Unify the pattern *)
   let gvars, subst, venv, ty' = infer_patt genv venv bvars gvars subst p in
   let subst = unify_types genv bvars pos subst ty ty' in
      gvars, subst, venv, ty

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

and infer_exp genv venv (bvars : bvars) (gvars : gvars) pos subst e =
   let pos = string_pos "infer_exp" (pos_pos pos (exp_pos e)) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         Number (_, ty) ->
            let gvars = gvars_add_type gvars ty in
            let subst = unify_type_int genv bvars pos subst ty in
               subst, ty
       | Char (_, ty) ->
            let gvars = gvars_add_type gvars ty in
            let subst = unify_type_char genv bvars pos subst ty in
               subst, ty
       | String (_, ty) ->
            let gvars = gvars_add_type gvars ty in
            let subst = unify_type_string genv bvars pos subst ty in
               subst, ty
       | Var (v, ty) ->
            infer_var_exp genv venv bvars gvars pos subst v ty
       | Const (ty_var, const_var, el, ty1, ty2) ->
            infer_const_exp genv venv bvars gvars pos loc subst ty_var const_var el ty1 ty2
       | Apply (e, ty_fun, el, ty_res) ->
            infer_apply_exp genv venv bvars gvars pos subst e ty_fun el ty_res
       | Lambda (_, pl, e, ty) ->
            infer_lambda_exp genv venv bvars gvars pos subst pl e ty
       | Function (_, cases, ty) ->
            infer_function_exp genv venv bvars gvars pos subst cases ty
       | Let (lets, e, ty) ->
            infer_lets_exp genv venv bvars gvars pos subst lets e ty
       | LetRec (letrecs, e, ty) ->
            infer_letrecs_exp genv venv bvars gvars pos subst letrecs e ty
       | If (e1, e2, e3, ty) ->
            infer_if_exp genv venv bvars gvars pos subst e1 e2 e3 ty
       | For (v, e1, b, e2, e3) ->
            infer_for_exp genv venv bvars gvars pos loc subst v e1 b e2 e3
       | While (e1, e2) ->
            infer_while_exp genv venv bvars gvars pos loc subst e1 e2
       | Constrain (e, ty) ->
            infer_constrain_exp genv venv bvars gvars pos subst e ty
       | Tuple (el, ty) ->
            infer_tuple_exp genv venv bvars gvars pos subst el ty
       | Array (el, ty) ->
            infer_array_exp genv venv bvars gvars pos subst el ty
       | ArraySubscript (e1, e2, ty) ->
            infer_array_subscript_exp genv venv bvars gvars pos subst e1 e2 ty
       | ArraySetSubscript (e1, e2, e3) ->
            infer_array_set_subscript_exp genv venv bvars gvars pos subst e1 e2 e3
       | StringSubscript (e1, e2) ->
            infer_string_subscript_exp genv venv bvars gvars pos subst e1 e2
       | StringSetSubscript (e1, e2, e3) ->
            infer_string_set_subscript_exp genv venv bvars gvars pos loc subst e1 e2 e3
       | Record (fields, ty) ->
            infer_record_exp genv venv bvars gvars pos subst fields ty
       | RecordProj (e, ty1, label, ty2) ->
            infer_record_proj_exp genv venv bvars gvars pos subst e ty1 label ty2
       | RecordSetProj (e1, ty, label, e2) ->
            infer_record_set_proj_exp genv venv bvars gvars pos loc subst e1 ty label e2
       | RecordUpdate (e, fields, ty) ->
            infer_record_update_exp genv venv bvars gvars pos subst e fields ty
       | Sequence (e1, e2, ty) ->
            infer_sequence_exp genv venv bvars gvars pos subst e1 e2 ty
       | Match (e, cases, ty) ->
            infer_match_exp genv venv bvars gvars pos loc subst e cases ty
       | Try (e, cases, ty) ->
            infer_try_exp genv venv bvars gvars pos loc subst e cases ty
       | Raise (e, ty) ->
            infer_raise_exp genv venv bvars gvars pos subst e ty
       | ApplyType _
       | In _
       | Out _
       | Open _
       | Action _
       | Restrict _
       | Ambient _
       | Thread _
       | Migrate _
       | LetModule _ ->
            raise (TAstException (pos, NotImplemented "inter_exp"))

and infer_exps genv venv bvars gvars pos subst el =
   let subst, tyl =
      List.fold_left (fun (subst, tyl) e ->
            let subst, ty = infer_exp genv venv bvars gvars pos subst e in
               subst, ty :: tyl) (subst, []) el
   in
      subst, List.rev tyl

(*
 * Variables.
 *)
and infer_var_exp genv venv bvars gvars pos subst id ty =
   let pos = string_pos "infer_var_exp" pos in
   let ty' = gvenv_lookup_var genv venv pos id in
   let subst = unify_types genv bvars pos subst ty ty' in
      subst, ty

(*
 * Constructor application.
 *
 * The value is polymorphic in all the types _not_ mentioned in
 * the case being constructed.  For example, consider a type
 *
 *    type ('a, 'b1, 'b2, 'c) t =
 *       A of 'a
 *     | B of 'b1 * 'b2
 *     | C of 'c
 *
 * Here are some types:
 *
 *       A 1: all ('b1, 'b2, 'c). t[int; 'b1; 'b2; 'c]
 *       B (2, 3.0): all ('a, 'c). t['a; int; float; 'c]
 *
 * Suppose None has the type (all 'd. option['d]).
 * We have the following types.
 *
 *     A None: all ('d, 'b1, 'b2, 'c). t['d option; 'b1; 'b2; 'c]
 *
 * The basic idea is this.  First, figure out the type of the tuple
 * argument (see infer_tuple_exp below).  In general, this will have
 * some universal type (all (ty_vars). t1 * ... * tn).  Unify the
 * monomorphic tuple type with the union type.
 *)
and infer_const_exp genv venv bvars gvars pos loc subst ty_var const_var el ty1 ty2 =
   let pos = string_pos "infer_const_exp" pos in

   (* The type _must_ be a union type, and it may be universally quantified *)
   let ty_vars, ty = dest_all_rename_type genv pos ty2 in
   let kind, ty_fields = dest_union_type genv pos ty in

   (* Find the named field *)
   let ty_fields =
      match kind with
         NormalUnion ->
            (* The named field should be present *)
            (try FieldTable.find ty_fields const_var with
                Not_found ->
                   raise (TAstException (pos, ConstLabelMismatch (const_var, ty))))
       | ExnUnion ->
            (* The named field will not be present, so use the fields as declared *)
            genv_lookup_exn genv pos const_var
   in

   (* Infer the expression types, and lift universal quantification *)
   let subst, ty_fields' = infer_exps genv venv bvars gvars pos subst el in
   let ty_vars, ty_fields' =
      List.fold_left (fun (ty_vars, tyl) ty ->
            let ty_vars', ty = dest_all_rename_type genv pos ty in
            let ty_vars = List.rev_append ty_vars' ty_vars in
            let tyl = ty :: tyl in
               ty_vars, tyl) (ty_vars, []) ty_fields'
   in
   let ty_fields' = List.rev ty_fields' in

   (* Unify the element types with the union type *)
   let len1 = List.length ty_fields in
   let len2 = List.length ty_fields' in
   let _ =
      if len1 <> len2 then
         raise (TAstException (pos, VarArityMismatch (const_var, len1, len2)));
      if debug debug_infer then
         begin
            List.iter (fun ty ->
                  eprintf "infer_union1: %a@." pp_print_type ty) ty_fields;
            List.iter (fun ty ->
                  eprintf "infer_union2: %a@." pp_print_type ty) ty_fields'
         end
   in
   let subst = unify_type_lists genv bvars pos subst ty_fields ty_fields' in

   (* Remove universal vars that are not free *)
   let ty_vars =
      List.fold_left (fun ty_vars v ->
            if subst_mem_fo_var subst v then
               ty_vars
            else
               v :: ty_vars) [] ty_vars
   in

   (* Build the type *)
   let ty_union = make_type loc (TyAll (List.rev ty_vars, ty)) in
   let subst = unify_types genv bvars pos subst ty1 ty_union in
      subst, ty1

(*
 * Function application.
 *)
and infer_apply_exp genv venv bvars gvars pos subst e ty_fun1 el ty_res =
   let pos = string_pos "infer_apply_exp" pos in
   let loc = loc_of_exp e in

   (* Get the types *)
   let subst, ty_fun0 = infer_exp genv venv bvars gvars pos subst e in
   let subst, ty_args = infer_exps genv venv bvars gvars pos subst el in

   (* Unify with the function type *)
   let ty_fun2 = make_type loc (TyFun (ty_args, ty_res)) in
   let subst = unify_types genv bvars pos subst ty_fun2 ty_fun0 in
   let subst = unify_types genv bvars pos subst ty_fun1 ty_fun2 in
      subst, ty_res

(*
 * Lambda abstraction.
 *)
and infer_lambda_exp genv venv bvars gvars pos subst pl e ty =
   let pos = string_pos "infer_lambda_exp" pos in
   let loc = loc_of_type ty in

   (* Type of the arguments *)
   let gvars_exp, subst, venv, ty_args = infer_patts genv venv bvars gvars subst pl in
   let subst, ty_res = infer_exp genv venv bvars gvars_exp pos subst e in
   let ty_fun = make_type loc (TyFun (ty_args, ty_res)) in
   let ty_fun = wrap_all_type loc gvars subst ty_fun in

   (* Unify the types *)
   let subst = unify_types genv bvars (int_pos 1 pos) subst ty ty_fun in
      subst, ty

(*
 * Match pattern.
 *)
and infer_function_exp genv venv bvars gvars pos subst cases ty =
   let pos = string_pos "infer_function_exp" pos in
   let loc = loc_of_type ty in

   (* Get the body type *)
   let subst, ty_arg, ty_res = infer_guarded genv venv bvars gvars pos loc subst cases in
   let ty_fun = wrap_fun_type loc [ty_arg] ty_res in
   let ty_fun = wrap_all_type loc gvars subst ty_fun in

   (* Unify *)
   let subst = unify_types genv bvars pos subst ty ty_fun in
      subst, ty

(*
 * Infer the guarded commands.
 * All the patterns and bodies must have the same type.
 *)
and infer_guarded genv venv bvars gvars pos loc subst cases =
   let pos = string_pos "infer_guarded" pos in
   let ty_arg = make_type loc (TyVar (new_symbol_string "ty_arg")) in
   let ty_res = make_type loc (TyVar (new_symbol_string "ty_res")) in
   let subst =
      List.fold_left (fun subst (p, e) ->
            let gvars, subst, venv, ty_arg' = infer_patt genv venv bvars gvars subst p in
            let subst, ty_res' = infer_exp genv venv bvars gvars pos subst e in
            let subst = unify_types genv bvars pos subst ty_arg ty_arg' in
            let subst = unify_types genv bvars pos subst ty_res ty_res' in
               subst) subst cases
   in
      subst, ty_arg, ty_res

(*
 * Let definitions.
 * Note that the variable in the patterns are _not_ bound in
 * the definitions.
 *)
and infer_lets genv venv_orig bvars gvars pos subst lets =
   let pos = string_pos "infer_lets" pos in
      List.fold_left (fun (subst, venv) (p, e, ty) ->
            let gvars, subst, venv, ty_patt = infer_patt genv venv bvars gvars subst p in
            let subst, ty_exp = infer_exp genv venv_orig bvars gvars pos subst e in
            let subst = unify_types genv bvars pos subst ty ty_patt in
            let subst = unify_types genv bvars pos subst ty ty_exp in
               subst, venv) (subst, venv_orig) lets

and infer_lets_exp genv venv bvars gvars pos subst lets e ty =
   let pos = string_pos "infer_lets_exp" pos in

   (* Infer the definitions *)
   let subst, venv = infer_lets genv venv bvars gvars pos subst lets in

   (* Infer the body *)
   let subst, ty' = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_types genv bvars pos subst ty ty' in
      subst, ty

(*
 * Recursive let definitions.
 * First, we add each of the variables to the variable environment,
 * then we check the bodies.
 *)
and infer_letrecs genv venv_orig bvars gvars pos subst letrecs =
   let pos = string_pos "infer_letrecs" pos in

   (* Add the variables with the new types *)
   let venv_int =
      List.fold_left (fun venv (v, _, ty_int, _) ->
            venv_add_var venv v ty_int) venv_orig letrecs
   in

   (* Check the bodies *)
   let subst =
      List.fold_left (fun subst (v, e, ty_int, ty_ext) ->
            let subst, ty_exp = infer_exp genv venv_int bvars gvars (int_pos 1 pos) subst e in
            let _ =
               if !debug_infer then
                  eprintf "@[<hv 3>letrec2:@ Ty_ext: %a@ Ty_exp: %a@ Ty_int: %a@]@." (**)
                     pp_print_type (subst_type subst ty_ext)
                     pp_print_type (subst_type subst ty_exp)
                     pp_print_type (subst_type subst ty_int)
            in
            let subst = unify_types genv bvars (int_pos 2 pos) subst ty_ext ty_exp in
            let subst = unify_types genv bvars (int_pos 3 pos) subst ty_int ty_exp in
               subst) subst letrecs
   in

   (* Add the external types for the rest *)
   let venv_ext =
      List.fold_left (fun venv (v, _, _, ty_ext) ->
            venv_add_var venv v ty_ext) venv_orig letrecs
   in
      subst, venv_ext

and infer_letrecs_exp genv venv bvars gvars pos subst letrecs e ty =
   let pos = string_pos "infer_letrecs_exp" pos in

   (* Add the variables *)
   let subst, venv = infer_letrecs genv venv bvars gvars pos subst letrecs in

   (* Check the let body *)
   let subst, ty' = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_types genv bvars pos subst ty ty' in
      subst, ty

(*
 * Conditional.
 *)
and infer_if_exp genv venv bvars gvars pos subst e1 e2 e3 ty =
   let pos = string_pos "infer_if_exp" pos in

   (* Get the expression types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in
   let subst, ty3 = infer_exp genv venv bvars gvars pos subst e3 in

   (* Unify *)
   let subst = unify_type_bool genv bvars pos subst ty1 in
   let subst = unify_types genv bvars pos subst ty ty2 in
   let subst = unify_types genv bvars pos subst ty ty3 in
      subst, ty

(*
 * For loop.
 *)
and infer_for_exp genv venv bvars gvars pos loc subst v e1 _ e2 e3 =
   let pos = string_pos "infer_for_exp" pos in

   (* The args must be integers *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in
   let subst = unify_type_int genv bvars pos subst ty1 in
   let subst = unify_type_int genv bvars pos subst ty2 in

   (* The var is bound in the body *)
   let venv = venv_add_var venv v (make_type loc TyInt) in
   let subst, ty3 = infer_exp genv venv bvars gvars pos subst e3 in
   let subst = unify_type_unit genv bvars pos subst ty3 in
      subst, ty_unit genv loc

(*
 * While loop.
 *)
and infer_while_exp genv venv bvars gvars pos loc subst e1 e2 =
   let pos = string_pos "infer_while_exp" pos in

   (* Get the types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in

   (* Unify *)
   let subst = unify_type_bool genv bvars pos subst ty1 in
   let subst = unify_type_unit genv bvars pos subst ty2 in
      subst, ty_unit genv loc

(*
 * Type constraint.
 *)
and infer_constrain_exp genv venv bvars gvars pos subst e ty =
   let pos = string_pos "infer_constrain_exp" pos in
   let subst, ty' = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_types genv bvars pos subst ty ty' in
      subst, ty

(*
 * Tuple allocation.
 *
 * The result may be polymorphic, but it can't contain polymorphic
 * elements (this is the ML convention).  To deal with polymorphism,
 * we lift out the polymorphic quantifiers.  For example, suppose
 * we build a pair with elements of type (all 'a. t1) * (all ('b, 'c). t2).
 * Then the tuple will have type (all ('a, 'b, 'c). (t1 * t2)).
 *)
and infer_tuple_exp genv venv bvars gvars pos subst el ty =
   let pos = string_pos "infer_tuple_exp" pos in
   let loc = loc_of_type ty in
   let subst, tyl = infer_exps genv venv bvars gvars pos subst el in
   let ty_vars, tyl =
      List.fold_left (fun (ty_vars, tyl) ty ->
            let ty = subst_type subst ty in
            eprintf "@[<hv 3>infer_tuple_exp:@ %a@]@." pp_print_type ty;
            let ty_vars', ty = dest_all_rename_type genv pos ty in
            let ty_vars = List.rev_append ty_vars' ty_vars in
            let tyl = ty :: tyl in
               ty_vars, tyl) ([], []) tyl
   in
   let ty_prod = make_type loc (TyProd (List.rev tyl)) in
   let ty_prod = make_type loc (TyAll (List.rev ty_vars, ty_prod)) in
   let subst = unify_types genv bvars pos subst ty ty_prod in
      subst, ty

(*
 * Array allocation.
 *)
and infer_array_exp genv venv bvars gvars pos subst el ty =
   let pos = string_pos "infer_array_exp" pos in
   let loc = loc_of_type ty in
   let ty_array =
      (* The zero-sized array is a special case *)
      if el = [] then
         let v = new_symbol_string "ty_array_elem" in
         let ty = make_type loc (TyVar v) in
         let ty = make_type loc (TyArray ty) in
         let ty = make_type loc (TyAll ([v], ty)) in
            ty
      else
         (* Unify the types of all the elements *)
         let ty_elem = make_type loc (TyVar (new_symbol_string "elem")) in
         let subst =
            List.fold_left (fun subst e ->
                  let subst, ty_elem' = infer_exp genv venv bvars gvars pos subst e in
                  let subst = unify_types genv bvars pos subst ty_elem ty_elem' in
                     subst) subst el
         in
            make_type loc (TyArray ty_elem)
   in

   (* Unify with the result type *)
   let subst = unify_types genv bvars pos subst ty ty_array in
      subst, ty

(*
 * Array subscripting.
 *)
and infer_array_subscript_exp genv venv bvars gvars pos subst e1 e2 ty =
   let pos = string_pos "infer_array_subscript_exp" pos in

   (* Get the expression types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in

   (* Index must be an int *)
   let subst = unify_type_int genv bvars pos subst ty2 in

   (* Array type *)
   let loc = loc_of_type ty in
   let ty_array = make_type loc (TyArray ty) in
   let subst = unify_types genv bvars pos subst ty_array ty1 in
      subst, ty

(*
 * Store a value.
 *)
and infer_array_set_subscript_exp genv venv bvars gvars pos subst e1 e2 e3 =
   let pos = string_pos "infer_array_set_subscript_exp" pos in

   (* Get the expression types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in
   let subst, ty3 = infer_exp genv venv bvars gvars pos subst e3 in

   (* Index must be an int *)
   let subst = unify_type_int genv bvars pos subst ty2 in

   (* Array type *)
   let loc = loc_of_exp e1 in
   let ty_array = make_type loc (TyArray ty3) in
   let subst = unify_types genv bvars pos subst ty_array ty1 in
      subst, ty_unit genv loc

(*
 * Array subscripting.
 *)
and infer_string_subscript_exp genv venv bvars gvars pos subst e1 e2 =
   let pos = string_pos "infer_string_subscript_exp" pos in

   (* Get the expression types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in

   (* Index must be an int *)
   let subst = unify_type_int genv bvars pos subst ty2 in

   (* Array type *)
   let loc = loc_of_exp e1 in
   let subst = unify_type_string genv bvars pos subst ty1 in
      subst, make_type loc TyChar

(*
 * Store a value.
 *)
and infer_string_set_subscript_exp genv venv bvars gvars pos loc subst e1 e2 e3 =
   let pos = string_pos "infer_string_set_subscript_exp" pos in

   (* Get the expression types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in
   let subst, ty3 = infer_exp genv venv bvars gvars pos subst e3 in

   (* Index must be an int *)
   let subst = unify_type_int genv bvars pos subst ty2 in

   (* Array type *)
   let subst = unify_type_string genv bvars pos subst ty1 in

   (* Element type *)
   let subst = unify_type_char genv bvars pos subst ty3 in
      subst, ty_unit genv loc

(*
 * Record construction.
 *)
and infer_record_exp genv venv bvars gvars pos subst fields ty =
   let pos = string_pos "infer_record_exp" pos in

   (* The type _must_ be a record type *)
   let ty_fields = dest_record_type genv pos ty in
   let ty_table =
      FieldTable.fold (fun ty_table label (_, ty) ->
            SymbolTable.add ty_table label ty) SymbolTable.empty ty_fields
   in

   (* Unify with the fields *)
   let subst, ty_table =
      List.fold_left (fun (subst, ty_table) (label, e) ->
            let ty1 =
               try SymbolTable.find ty_table label with
                  Not_found ->
                     raise (TAstException (pos, RecordLabelMismatch (label, ty)))
            in
            let subst, ty2 = infer_exp genv venv bvars gvars pos subst e in
            let subst = unify_types genv bvars pos subst ty1 ty2 in
            let ty_table = SymbolTable.remove ty_table label in
               subst, ty_table) (subst, ty_table) fields
   in

   (* Make sure all the fields are defined *)
   let labels =
      SymbolTable.fold (fun labels label _ ->
            label :: labels) [] ty_table
   in
   let _ =
      if labels <> [] then
         raise (TAstException (pos, MissingLabels labels))
   in
      subst, ty

(*
 * Record projection.
 *)
and infer_record_proj_exp genv venv bvars gvars pos subst e ty_exp label_var ty =
   let pos = string_pos "infer_record_proj_exp" pos in

   (* The expression must have a record type *)
   let subst, ty_exp' = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_types genv bvars pos subst ty_exp ty_exp' in
   let ty_fields = dest_record_type genv pos ty_exp in

   (* Get the field type *)
   let _, ty_field =
      try FieldTable.find ty_fields label_var with
         Not_found ->
            raise (TAstException (pos, RecordLabelMismatch (label_var, ty_exp)))
   in
   let subst = unify_types genv bvars pos subst ty ty_field in
      subst, ty

(*
 * Record assignment.
 * Make sure the field is mutable.
 *)
and infer_record_set_proj_exp genv venv bvars gvars pos loc subst e1 ty_exp label_var e2 =
   let pos = string_pos "infer_record_set_proj_exp" pos in

   (* The expression must have a record type *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in
   let subst = unify_types genv bvars pos subst ty_exp ty1 in

   (* Get the field type *)
   let ty_fields = dest_record_type genv pos ty_exp in
   let mutablep, ty_field =
      try FieldTable.find ty_fields label_var with
         Not_found ->
            raise (TAstException (pos, RecordLabelMismatch (label_var, ty_exp)))
   in
   let _ =
      if not mutablep then
         raise (TAstException (pos, StringVarError ("record field is not mutable", label_var)))
   in
   let subst = unify_types genv bvars pos subst ty_field ty2 in
      subst, ty_unit genv loc

(*
 * Record update.
 *)
and infer_record_update_exp genv venv bvars gvars pos subst e fields ty =
   let pos = string_pos "infer_record_update_exp" pos in

   (* The type _must_ be a record type *)
   let subst, ty_exp = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_types genv bvars pos subst ty ty_exp in
   let ty_fields = dest_record_type genv pos ty in

   (* Unify with the fields *)
   let subst =
      List.fold_left (fun subst (label, e) ->
            let _, ty1 =
               try FieldTable.find ty_fields label with
                  Not_found ->
                     raise (TAstException (pos, RecordLabelMismatch (label, ty_exp)))
            in
            let subst, ty2 = infer_exp genv venv bvars gvars pos subst e in
            let subst = unify_types genv bvars pos subst ty1 ty2 in
               subst) subst fields
   in
      subst, ty

(*
 * Sequence.
 * Don't check the return type of the first expression.
 * We'll check it later.
 *)
and infer_sequence_exp genv venv bvars gvars pos subst e1 e2 ty =
   let pos = string_pos "infer_sequence_exp" pos in

   (* Get the types *)
   let subst, ty1 = infer_exp genv venv bvars gvars pos subst e1 in
   let subst, ty2 = infer_exp genv venv bvars gvars pos subst e2 in

   (* Unify *)
   let subst = unify_types genv bvars pos subst ty ty1 in
      subst, ty

(*
 * Match statement.
 *)
and infer_match_exp genv venv bvars gvars pos loc subst e cases ty =
   let pos = string_pos "infer_match_exp" pos in

   (* Type of argument *)
   let subst, ty_exp = infer_exp genv venv bvars gvars pos subst e in
   let subst, ty_arg, ty_res = infer_guarded genv venv bvars gvars pos loc subst cases in

   (* Unify *)
   let subst = unify_types genv bvars pos subst ty_arg ty_exp in
   let subst = unify_types genv bvars pos subst ty ty_res in
      subst, ty

(*
 * Try.
 *)
and infer_try_exp genv venv bvars gvars pos loc subst e cases ty =
   let pos = string_pos "infer_try_exp" pos in

   (* Type of argument *)
   let subst, ty_exp = infer_exp genv venv bvars gvars pos subst e in
   let subst, ty_arg, ty_res = infer_guarded genv venv bvars gvars pos loc subst cases in

   (* Unify *)
   let subst = unify_type_exn genv bvars pos subst ty_arg in
   let subst = unify_types genv bvars pos subst ty ty_exp in
   let subst = unify_types genv bvars pos subst ty ty_res in
      subst, ty

(*
 * Raise an exception.
 * The expression must be an exception.
 * Any return type is allowed.
 *)
and infer_raise_exp genv venv bvars gvars pos subst e ty =
   let pos = string_pos "infer_raise_exp" pos in

   (* Argument type *)
   let subst, ty_exp = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_type_exn genv bvars pos subst ty_exp in
      subst, ty

(************************************************************************
 * MODULES
 ************************************************************************)

(*
 * Module expression.
 *)
and infer_me genv venv bvars gvars subst me =
   let pos = loc_pos (loc_of_module_exp me) in
   let pos = del_exp_pos (fun buf -> pp_print_genv buf genv) pos in
   let pos = string_pos "infer_module_exp" pos in
      match dest_module_exp_core me with
         ModuleExpStruct (me_var, fields, names, ty) ->
            infer_struct_me genv venv bvars gvars pos subst me_var fields names ty
       | ModuleExpVar (me_name, ty) ->
            infer_var_me genv venv bvars gvars pos subst me_name ty
       | ModuleExpConstrain (me, ty) ->
            infer_constrain_me genv venv bvars gvars pos subst me ty
       | ModuleExpFunctor (me_var, ty1, me, ty2) ->
            infer_functor_me genv venv bvars gvars pos subst me_var ty1 me ty2
       | ModuleExpApply (me, me_name, ty) ->
            infer_apply_me genv venv bvars gvars pos subst me me_name ty

(*
 * Struct.
 *)
and infer_struct_me genv venv bvars gvars pos subst me_var fields names ty =
   let pos = string_pos "infer_struct_me" pos in
   let genv = genv_push genv me_var in
   let genv = genv_set_current_names genv names in
   let subst, genv = infer_fields genv venv bvars gvars pos subst fields in
   let genv, mt2 = genv_pop genv in
   let mt1 = dest_module_type genv pos ty in
   let subst = unify_module_types genv bvars pos subst mt1 mt2 in
      subst, genv, ty

(*
 * A named module.
 *)
and infer_var_me genv venv bvars gvars pos subst me_name mt1 =
   let pos = string_pos "infer_var_me" pos in
   let mt2 = genv_lookup_module genv pos me_name in
   let subst = unify_types genv bvars pos subst mt1 mt2 in
      subst, genv, mt1

(*
 * A module coercion.
 *)
and infer_constrain_me genv venv bvars gvars pos subst me mt1 =
   let pos = string_pos "infer_constrain_me" pos in
   let subst, genv, mt2 = infer_me genv venv bvars gvars subst me in
   let subst = unify_types genv bvars pos subst mt1 mt2 in
      subst, genv, mt1

(*
 * A functor.
 *)
and infer_functor_me genv venv bvars gvars pos subst me_var ty1 me ty2 =
   let pos = string_pos "infer_functor_me" pos in
   let genv = genv_add_module_int genv me_var ty1 in
   let subst, genv, mt2 = infer_me genv venv bvars gvars subst me in
   let loc = loc_of_type ty2 in
   let ty = make_type loc (TyFunctor (me_var, ty1, mt2)) in
   let subst = unify_types genv bvars pos subst ty2 ty in
      subst, genv, ty

(*
 * A module application.
 *)
and infer_apply_me genv venv bvars gvars pos subst me me_name ty =
   let pos = string_pos "infer_apply_me" pos in
   let subst, genv, mt1 = infer_me genv venv bvars gvars subst me in
   let mt2 = genv_lookup_module genv pos me_name in
   let loc = loc_of_type ty in
   let ty_functor = make_type loc (TyFunctor (new_symbol_string "???", mt1, ty)) in
   let subst = unify_types genv bvars pos subst ty_functor mt1 in
      subst, genv, ty

(************************************************************************
 * FIELDS
 ************************************************************************)

(*
 * Infer a struct field.
 *)
and infer_field genv venv (bvars : bvars) (gvars : gvars) pos subst field =
   let loc = loc_of_field field in
   let pos = string_pos "infer_field" pos in
      match dest_field_core field with
         FieldLet lets ->
            infer_lets_field genv venv bvars gvars pos subst lets
       | FieldLetRec letrecs ->
            infer_letrecs_field genv venv bvars gvars pos subst letrecs
       | FieldType types ->
            infer_types_field genv venv bvars gvars pos subst types
       | FieldExpr e ->
            infer_exp_field genv venv bvars gvars pos subst e
       | FieldRestrict (v, ty) ->
            infer_restrict_field genv venv bvars gvars pos subst v ty
       | FieldModule (me_var, me, ty) ->
            infer_module_field genv venv bvars gvars pos subst me_var me ty
       | FieldModuleType (mt_var, ty) ->
            infer_module_type_field genv venv bvars gvars pos subst mt_var ty
       | FieldException (tag_var, ty_var, tyl) ->
            infer_exception_field genv venv bvars gvars pos subst tag_var ty_var tyl
       | FieldExternal (v, ty, s) ->
            infer_external_field genv venv bvars gvars pos subst v ty s

and infer_fields genv venv bvars gvars pos subst fields =
   List.fold_left (fun (subst, genv) field ->
         infer_field genv venv bvars gvars pos subst field) (subst, genv) fields

(*
 * Infer a let definition.
 *)
and infer_lets_field genv venv bvars gvars pos subst lets =
   let pos = string_pos "infer_lets_field" pos in

   (* Add the variables *)
   let subst, vars = infer_lets genv venv_empty bvars gvars pos subst lets in
   let genv = SymbolTable.fold genv_add_var_int genv vars in
      subst, genv

(*
 * Recursive let definition.
 *)
and infer_letrecs_field genv venv bvars gvars pos subst letrecs =
   let pos = string_pos "infer_letrecs_field" pos in
   let subst, vars = infer_letrecs genv venv_empty bvars gvars pos subst letrecs in
   let genv = SymbolTable.fold genv_add_var_int genv vars in
      subst, genv

(*
 * Add some type definitions.
 *)
and infer_types_field genv venv bvars gvars pos subst types =
   let pos = string_pos "infer_types_field" pos in
   let genv =
      List.fold_left (fun genv (ty_var, ty_vars, ty) ->
            genv_add_type_int genv ty_var ty_vars ty) genv types
   in
      subst, genv

(*
 * An unbound expression.
 *)
and infer_exp_field genv venv bvars gvars pos subst e =
   let pos = string_pos "infer_exp_field" pos in
   let subst, ty = infer_exp genv venv bvars gvars pos subst e in
   let subst = unify_type_unit genv bvars pos subst ty in
      subst, genv

(*
 * A Module name restriction.
 *)
and infer_restrict_field genv venv bvars gvars pos subst v ty =
   let pos = string_pos "infer_restrict_field" pos in
      raise (TAstException (pos, NotImplemented "infer_restrict_exp"))

(*
 * A module expression.
 *)
and infer_module_field genv venv bvars gvars pos subst me_var me ty =
   let pos = string_pos "infer_module_field" pos in
   let subst, genv, ty' = infer_me genv venv bvars gvars subst me in
   let subst = unify_types genv bvars pos subst ty ty' in
   let genv = genv_add_module_int genv me_var ty in
      subst, genv

(*
 * A module type.
 *)
and infer_module_type_field genv venv bvars gvars pos subst mt_var ty =
   let pos = string_pos "infer_module_type_field" pos in
   let genv = genv_add_module_type_int genv mt_var ty in
      subst, genv

(*
 * Exceptions.
 *)
and infer_exception_field genv venv bvars gvars pos subst const_var ty_var tyl =
   let pos = string_pos "infer_exception_field" pos in
   let genv = genv_add_exn_int genv const_var ty_var tyl in
      subst, genv

(*
 * External values.
 * We accept any type (in the worst
 * case, it will be caught in the FIR).
 *)
and infer_external_field genv venv bvars gvars pos subst v ty s =
   let pos = string_pos "infer_external_field" pos in
   let genv = genv_add_var_int genv v ty in
      subst, genv

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Type inference for the program.
 *)
let infer_prog prog =
   let { prog_name = name;
         prog_exp = exp
       } = prog
   in
   let loc = create_loc name 0 0 0 0 in
   let pos = string_pos "infer_prog" (loc_pos loc) in
      match exp with
         Interface _ ->
            prog
       | Implementation (me_var, fields, names, ty) ->
            let _ =
               if !debug_infer then
                  Format.eprintf "@[<v 3>*** AML Inference (before):@ %a@]@." (**)
                     pp_print_prog prog;
            in
            let genv = genv_of_prog prog in
            let subst, _, ty = infer_struct_me genv venv_empty bvars_empty gvars_empty pos empty_subst me_var fields names ty in
            let prog = subst_prog subst prog in

            (* Print it out *)
            let _ =
               if !debug_infer then
                  Format.eprintf "@[<v 3>*** AML Inference (before type quantification):@ %a@ @[<hv 3>Subst:@ %a@]@]@." (**)
                     pp_print_prog prog
                     pp_print_subst subst
            in
            let prog = quantify_prog prog in

            (* Print it out *)
            let _ =
               if !debug_infer then
                  Format.eprintf "@[<v 3>*** AML Inference (before free vars):@ %a@ @[<hv 3>Subst:@ %a@]@]@." (**)
                     pp_print_prog prog
                     pp_print_subst subst
            in

            (* Replace free type vars with unit *)
            let ty_unit = ty_unit genv loc in
            let fv = free_vars_prog prog in
            let prog =
               if fv = [] then
                  prog
               else
                  let subst =
                     List.fold_left (fun subst v ->
                           eprintf "@[<hv 3>warning: replacing free type variable %a with unit@]@." pp_print_symbol v;
                           subst_add_fo_var subst v ty_unit) empty_subst fv
                  in
                     subst_prog subst prog
            in

            (* Print it out *)
            let _ =
               if !debug_infer then
                  Format.eprintf "@[<v 3>*** AML Inference (before standardization):@ %a@ @[<hv 3>Subst:@ %a@]@]@." (**)
                     pp_print_prog prog
                     pp_print_subst subst
            in
               standardize_prog prog

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
