(*
 * Add some "boot" types to the program,
 * including unit, Boolean.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Field_table

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_mt_util
open Aml_tast_env_type

module Pos = MakePos (struct let name = "Aml_tast_env_boot" end)
open Pos

(*
 * Location for boot.
 *)
let loc = bogus_loc "<Aml_tast_env_boot>"

(*
 * Empty open set.
 *)
let empty_opens =
   { var_opens   = SymbolTable.empty;
     const_opens = SymbolTable.empty;
     label_opens = SymbolTable.empty;
     ty_opens    = SymbolTable.empty;
     me_opens    = SymbolTable.empty;
     mt_opens    = SymbolTable.empty
   }

(*
 * Empty module type.
 *)
let empty_module_type =
   { mt_self         = new_symbol_string "self";
     mt_mt_types     = SymbolTable.empty;
     mt_me_vars      = SymbolTable.empty;
     mt_types        = SymbolTable.empty;
     mt_vars         = SymbolTable.empty;
     mt_consts       = SymbolTable.empty;
     mt_labels       = SymbolTable.empty;
     mt_names        = nenv_empty
   }

(*
 * Initial environment.
 *)
let genv_empty name =
   let names      = SymbolTable.empty in
   let types      = SymbolTable.empty in

   (*
    * Add unit type.
    *)
   let unit_var   = Symbol.add "unit" in
   let it_var     = Symbol.add "()" in
   let unit_label = new_symbol unit_var in
   let it_label   = new_symbol it_var in

   let tydef_unit_inner =
      make_tydef_inner loc (TyDefUnion (NormalUnion, (FieldTable.add FieldTable.empty it_label [])))
   in
   let tydef_unit =
      { tydef_fo_vars = [];
        tydef_so_vars = [];
        tydef_inner = tydef_unit_inner
      }
   in
   let names = SymbolTable.add names unit_label unit_var in
   let names = SymbolTable.add names it_label it_var in
   let types = SymbolTable.add types unit_label tydef_unit in

   (*
    * Add bool type.
    *)
   let bool_var   = Symbol.add "bool" in
   let true_var   = Symbol.add "true" in
   let false_var  = Symbol.add "false" in

   let bool_label  = new_symbol bool_var in
   let true_label  = new_symbol true_var in
   let false_label = new_symbol false_var in

   let tydef_bool_fields = FieldTable.empty in
   let tydef_bool_fields = FieldTable.add tydef_bool_fields false_label [] in
   let tydef_bool_fields = FieldTable.add tydef_bool_fields true_label [] in
   let tydef_bool_inner =
      make_tydef_inner loc (TyDefUnion (NormalUnion, tydef_bool_fields))
   in
   let tydef_bool =
      { tydef_fo_vars = [];
        tydef_so_vars = [];
        tydef_inner = tydef_bool_inner
      }
   in
   let names = SymbolTable.add names bool_label bool_var in
   let names = SymbolTable.add names true_label true_var in
   let names = SymbolTable.add names false_label false_var in
   let types = SymbolTable.add types bool_label tydef_bool in

   (*
    * Add exception type.
    *)
   let exn_var     = Symbol.add "exn" in
   let exn_label   = new_symbol exn_var in
   let tydef_exn_inner =
      make_tydef_inner loc (TyDefUnion (ExnUnion, FieldTable.empty))
   in
   let tydef_exn =
      { tydef_fo_vars = [];
        tydef_so_vars = [];
        tydef_inner = tydef_exn_inner
      }
   in
   let names = SymbolTable.add names exn_label exn_var in
   let types = SymbolTable.add types exn_label tydef_exn in

   (*
    * Now build the empty genv.
    *)
   let current =
      { genv_core = empty_module_type;
        genv_opens = empty_opens
      }
   in
   let boot =
      { boot_ty_unit     = TyUnion (unit_label, [], []);
        boot_ty_unit_var = unit_label;
        boot_unit_var    = it_label;

        boot_ty_bool     = TyUnion (bool_label, [], []);
        boot_ty_bool_var = bool_label;
        boot_true_var    = true_label;
        boot_false_var   = false_label;

        boot_ty_exn      = TyUnion (exn_label, [], []);
        boot_ty_exn_var  = exn_label
      }
   in
      { genv_name    = name;
        genv_names   = names;
        genv_types   = types;
        genv_import  = SymbolTable.empty;
        genv_current = current;
        genv_next    = [];
        genv_boot    = boot
      }

(*
 * Get the unit and bool types.
 *)
let ty_unit genv loc =
   make_type loc genv.genv_boot.boot_ty_unit

let ty_bool genv loc =
   make_type loc genv.genv_boot.boot_ty_bool

let ty_exn genv loc =
   make_type loc genv.genv_boot.boot_ty_exn

let ty_exn_var genv =
   genv.genv_boot.boot_ty_exn_var

(*
 * Array should have 1 var.
 *)
let ty_array genv pos loc vars =
   let pos = string_pos "ty_array" pos in
      match vars with
         [v1] ->
            let ty1 = make_type loc (TyVar v1) in
               make_type loc (TyArray ty1)
       | _ ->
            raise (TAstException (pos, StringError "array type requires 1 type argument"))

(*
 * Format should have 3 vars.
 *)
let ty_format genv pos loc vars =
   let pos = string_pos "ty_format" pos in
      match vars with
         [v1; v2; v3] ->
            let ty1 = make_type loc (TyVar v1) in
            let ty2 = make_type loc (TyVar v2) in
            let ty3 = make_type loc (TyVar v3) in
               make_type loc (TyFormat (ty1, ty2, ty3))
       | _ ->
            raise (TAstException (pos, StringError "format type requires 3 type arguments"))

(*
 * Get external types.
 *)
let type_of_external genv pos vars s loc =
   let pos = string_pos "type_of_external" pos in
      match s with
         "%int"    -> make_type loc TyInt
       | "%char"   -> make_type loc TyChar
       | "%string" -> make_type loc TyString
       | "%float"  -> make_type loc TyFloat
       | "%unit"   -> ty_unit genv loc
       | "%bool"   -> ty_bool genv loc
       | "%exn"    -> ty_exn genv loc
       | "%array"  -> ty_array genv pos loc vars
       | "%format" -> ty_format genv pos loc vars
       | _ ->
            raise (TAstException (pos, StringError ("unbound extern type: \"" ^ String.escaped s ^ "\"")))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
