(*
 * Utilities on explicity-typed programs.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Aml_tast
open Aml_tast_ds

(*
 * Unit type.
 *)
let ty_unit loc =
   make_type loc TyInt

(*
 * Get the type of an expression.
 *)
let type_of_exp exp =
   let loc = loc_of_exp exp in
      match dest_exp_core exp with
         Number (_, ty)
       | Char (_, ty)
       | String (_, ty)
       | Var (_, ty)
       | Const (_, _, _, ty, _)
       | Apply (_, _, _, ty)
       | Lambda (_, _, _, ty)
       | Function (_, _, ty)
       | Let (_, _, ty)
       | LetRec (_, _, ty)
       | If (_, _, _, ty)
       | ApplyType (_, _, ty)
       | Tuple (_, ty)
       | Array (_, ty)
       | ArraySubscript (_, _, ty)
       | Record (_, ty)
       | RecordUpdate (_, _, ty)
       | RecordProj (_, _, _, ty)
       | Sequence (_, _, ty)
       | Match (_, _, ty)
       | Try (_, _, ty)
       | Raise (_, ty)
       | In (_, ty)
       | Out (_, ty)
       | Open (_, ty)
       | Action (_, ty)
       | Restrict (_, _, _, ty)
       | Migrate (_, _, ty)
       | LetModule (_, _, _, ty)
       | Constrain (_, ty) ->
            ty

       | For _
       | While _
       | ArraySetSubscript _
       | StringSetSubscript _
       | RecordSetProj _
       | Ambient _
       | Thread _ ->
            ty_unit loc

       | StringSubscript _ ->
            make_type loc TyChar

(*
 * Get the type of a pattern.
 *)
let type_of_pattern p =
   match dest_patt_core p with
      PattInt (_, ty)
    | PattChar (_, ty)
    | PattString (_, ty)
    | PattWild (ty)
    | PattVar (_, ty)
    | PattRecord (_, ty)
    | PattArray (_, ty)
    | PattConst (_, _, _, ty)
    | PattChoice (_, _, ty)
    | PattAs (_, _, ty)
    | PattTuple (_, ty)
    | PattExpr (_, ty)
    | PattRange (_, _, ty)
    | PattWhen (_, _, ty)
    | PattConstrain (_, ty) ->
         ty

(*
 * Type of a module expression.
 *)
let type_of_module me =
   match dest_module_exp_core me with
      ModuleExpStruct (_, _, _, mt)
    | ModuleExpVar (_, mt)
    | ModuleExpConstrain (_, mt)
    | ModuleExpFunctor (_, _, _, mt)
    | ModuleExpApply (_, _, mt) ->
         mt

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
