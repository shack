(*
 * Compile the item.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug

open Fir_state

open Aml_tast
open Aml_tast_ds
open Aml_tast_env
open Aml_tast_print
open Aml_tast_mt_util
open Aml_tast_standardize

let debug_prog s prog =
   if debug debug_print_tast then
      debug_prog s prog

let compile prog =
   (* Convert to TAST *)
   let prog = Aml_tast_ast.build_prog prog in
   let _ = debug_prog "Aml_tast_ast" prog in

   (* Run type inference *)
   let prog = Aml_tast_infer.infer_prog prog in
   let _ = debug_prog "Aml_tast_infer" prog in

      (* If it is an interface, write the description and quit *)
      match prog.prog_exp with
         Interface _ ->
            save_interface prog;
            exit 0
       | Implementation _ ->
            save_implementation prog;
            prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
