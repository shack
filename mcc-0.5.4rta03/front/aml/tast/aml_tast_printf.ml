(*
 * Printf utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env
open Aml_tast_tydef

module Pos = MakePos (struct let name = "Aml_tast_printf" end)
open Pos

(*
 * Format types.
 *)
let skip_flags s i =
   let len = String.length s in
   let rec search i =
      if i = len then
         i, '%'
      else
         match s.[i] with
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '.' | '-' ->
               search (succ i)
          | c ->
               i, c
   in
      search i

let format_string_type genv pos s t2 t3 loc =
   let pos = string_pos "format_string_type" pos in
   let len = String.length s in
   let rec loop tyl i =
      if i = len then
         tyl
      else if s.[i] = '%' && i < pred len then
         let i, c = skip_flags s (succ i) in
            if c = '%' then
               loop tyl i
            else
               let ty =
                  match c with
                     'd' | 'i' | 'o' | 'u' | 'x' | 'X' ->
                        make_type loc TyInt
                   | 'e' | 'E' | 'f' | 'F' | 'g' | 'G' ->
                        make_type loc TyFloat
                   | 'c' | 'C' ->
                        make_type loc TyChar
                   | 's' | 'S' ->
                        make_type loc TyString
                   | 'b' | 'B' ->
                        ty_bool genv loc
                   | 'a' ->
                        let v = new_symbol_string "a" in
                           make_type loc (TyFun ([t2], make_type loc (TyFun ([make_type loc (TyVar v)], t3))))
                   | 't' ->
                        make_type loc (TyFun ([t2], t3))
                   | _ ->
                        raise (TAstException (pos, StringError ("invalid format string: \"" ^ String.escaped s ^ "\"")))
               in
                  loop (ty :: tyl) i
      else
         loop tyl (succ i)
   in
   let ty_args = List.rev (loop [] 0) in
      wrap_fun_type loc ty_args t3

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
