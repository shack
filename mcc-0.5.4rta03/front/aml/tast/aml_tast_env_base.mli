(*
 * Basic operations on genv.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast
open Aml_tast_exn
open Aml_tast_env_type

(*
 * Convert to level list.
 *)
val levels_of_genv : genv -> genv_level list
val mt_of_genv : genv -> module_type

(*
 * Push an pop scopes.
 *)
val genv_push      : genv -> me_var -> genv
val genv_push_core : genv -> module_type -> genv
val genv_pop       : genv -> genv * module_type

(*
 * Build the program from the environment.
 *)
val prog_of_genv : genv -> prog_exp -> prog
val genv_of_prog : prog -> genv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
