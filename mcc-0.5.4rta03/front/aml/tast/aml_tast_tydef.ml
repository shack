(*
 * This file defines utilities on types.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env_type

module Pos = MakePos (struct let name = "Aml_tast_type" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Build a function type from arguments and result type.
 * The type takes one argument at a time.
 *)
let rec wrap_fun_type loc ty_args ty_res =
   match ty_args with
      [] ->
         ty_res
    | ty :: ty_args ->
         make_type loc (TyFun ([ty], wrap_fun_type loc ty_args ty_res))

(************************************************************************
 * TYPE DEF EXPANSION
 ************************************************************************)

(*
 * Apply a type definition.
 *)
let apply_type genv pos ty_var tyl1 tyl2 =
   let pos = string_pos "apply_type" pos in
   let tydef =
      try SymbolTable.find genv.genv_types ty_var with
         Not_found ->
            raise (TAstException (pos, UnboundType ty_var))
   in
   let { tydef_fo_vars = params;
         tydef_so_vars = names;
         tydef_inner = inner
       } = tydef
   in

   (* Params *)
   let len1 = List.length params in
   let len2 = List.length tyl1 in
   let _ =
      if len1 <> len2 then
         let pos = string_pos "apply_type_fo_vars" pos in
            raise (TAstException (pos, VarArityMismatch (ty_var, len1, len2)))
   in
   let subst = List.fold_left2 subst_add_fo_var empty_subst params tyl1 in

   (* Names *)
   let len1 = List.length names in
   let len2 = List.length tyl2 in
   let _ =
      if len1 <> len2 then
         let pos = string_pos "apply_type_so_vars" pos in
            raise (TAstException (pos, VarArityMismatch (ty_var, len1, len2)))
   in
   let names = List.map (fun { kind_value = name } -> name) names in
   let subst = List.fold_left2 subst_add_so_var subst names tyl2 in

      (* Now do the subst *)
      subst_tydef_inner subst inner

(*
 * Various applications.
 *)
let apply_union_type genv pos ty_var tyl1 tyl2 =
   let pos = string_pos "apply_union_type" pos in
   let inner = apply_type genv pos ty_var tyl1 tyl2 in
      match dest_tydef_inner_core inner with
         TyDefUnion (kind, fields) ->
            kind, fields
       | _ ->
            raise (TAstException (pos, StringTydefInnerError ("not a union type", inner)))

let apply_record_type genv pos ty_var tyl1 tyl2 =
   let pos = string_pos "apply_record_type" pos in
   let inner = apply_type genv pos ty_var tyl1 tyl2 in
      match dest_tydef_inner_core inner with
         TyDefRecord fields ->
            fields
       | _ ->
            raise (TAstException (pos, StringTydefInnerError ("not a record type", inner)))

let apply_module_type genv pos ty_var tyl1 tyl2 =
   let pos = string_pos "apply_module_type" pos in
   let inner = apply_type genv pos ty_var tyl1 tyl2 in
      match dest_tydef_inner_core inner with
         TyDefModule mt ->
            mt
       | _ ->
            raise (TAstException (pos, StringTydefInnerError ("not a module type", inner)))

(************************************************************************
 * TYPE DESTRUCTORS
 ************************************************************************)

(*
 * Type definition destructors.
 *)
let rec dest_union_type genv pos ty =
   let pos = string_pos "dest_union_type" pos in
      match dest_type_core ty with
         TyUnion (ty_var, tyl1, tyl2) ->
            apply_union_type genv pos ty_var tyl1 tyl2
       | TyAll ([], ty) ->
            dest_union_type genv pos ty
       | _ ->
            raise (TAstException (pos, StringTypeError ("not a union type", ty)))

let rec dest_record_type genv pos ty =
   let pos = string_pos "dest_record_type" pos in
      match dest_type_core ty with
         TyRecord (ty_var, tyl1, tyl2) ->
            apply_record_type genv pos ty_var tyl1 tyl2
       | TyAll ([], ty) ->
            dest_record_type genv pos ty
       | _ ->
            raise (TAstException (pos, StringTypeError ("not a record type", ty)))

let rec dest_module_type genv pos ty =
   let pos = string_pos "dest_module_type" pos in
      match dest_type_core ty with
         TyModule (ty_var, tyl1, tyl2) ->
            apply_module_type genv pos ty_var tyl1 tyl2
       | TyAll ([], ty) ->
            dest_module_type genv pos ty
       | _ ->
            raise (TAstException (pos, StringTypeError ("not a module type", ty)))

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
