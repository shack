(*
 * After type inference, the functions have explicit universal types.
 * Move the quantifiers to the function expression itself.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env
open Aml_tast_type

module Pos = MakePos (struct let name = "Aml_tast_quant" end)
open Pos

(*
 * Quantify all the functions in the expression.
 *)
let rec quantify_exp genv e =
   let loc = loc_of_exp e in
   let pos = string_pos "quantify_exp" (exp_pos e) in
   let e = quantify_exp_core genv pos (dest_exp_core e) in
      make_exp loc e

and quantify_exp_list genv el =
   List.map (quantify_exp genv) el

and quantify_exp_core genv pos e =
   match e with
      Number _
    | Char _
    | String _
    | Var _ ->
         e
    | Const (ty_var, const_var, el, ty1, ty2) ->
         let el = quantify_exp_list genv el in
            Const (ty_var, const_var, el, ty1, ty2)
    | Apply (e, ty_fun, el, ty_res) ->
         let e = quantify_exp genv e in
         let el = quantify_exp_list genv el in
            Apply (e, ty_fun, el, ty_res)
    | Lambda (ty_vars1, pl, e, ty) ->
         let ty_vars2, ty = dest_all_type genv pos ty in
         let e = quantify_exp genv e in
            Lambda (ty_vars1 @ ty_vars2, pl, e, ty)
    | Function (ty_vars1, cases, ty) ->
         let ty_vars2, ty = dest_all_type genv pos ty in
         let cases = List.map (fun (p, e) -> p, quantify_exp genv e) cases in
            Function (ty_vars1 @ ty_vars2, cases, ty)
    | Let (lets, e, ty) ->
         let lets = List.map (fun (p, e, ty) -> p, quantify_exp genv e, ty) lets in
         let e = quantify_exp genv e in
            Let (lets, e, ty)
    | LetRec (letrecs, e, ty) ->
         let letrecs = List.map (fun (v, e, ty_int, ty_ext) -> v, quantify_exp genv e, ty_int, ty_ext) letrecs in
         let e = quantify_exp genv e in
            LetRec (letrecs, e, ty)
    | If (e1, e2, e3, ty) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
         let e3 = quantify_exp genv e3 in
            If (e1, e2, e3, ty)
    | For (v, e1, b, e2, e3) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
         let e3 = quantify_exp genv e3 in
            For (v, e1, b, e2, e3)
    | While (e1, e2) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
            While (e1, e2)
    | Constrain (e, ty) ->
         let e = quantify_exp genv e in
            Constrain (e, ty)
    | Tuple (el, ty) ->
         let el = quantify_exp_list genv el in
            Tuple (el, ty)
    | Array (el, ty) ->
         let el = quantify_exp_list genv el in
            Array (el, ty)
    | ArraySubscript (e1, e2, ty) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
            ArraySubscript (e1, e2, ty)
    | ArraySetSubscript (e1, e2, e3) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
         let e3 = quantify_exp genv e3 in
            ArraySetSubscript (e1, e2, e3)
    | StringSubscript (e1, e2) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
            StringSubscript (e1, e2)
    | StringSetSubscript (e1, e2, e3) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
         let e3 = quantify_exp genv e3 in
            StringSetSubscript (e1, e2, e3)
    | Record (fields, ty) ->
         let fields = List.map (fun (label, e) -> label, quantify_exp genv e) fields in
            Record (fields, ty)
    | RecordProj (e, ty1, label, ty2) ->
         let e = quantify_exp genv e in
            RecordProj (e, ty1, label, ty2)
    | RecordSetProj (e1, ty, label, e2) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
            RecordSetProj (e1, ty, label, e2)
    | RecordUpdate (e, fields, ty) ->
         let e = quantify_exp genv e in
         let fields = List.map (fun (label, e) -> label, quantify_exp genv e) fields in
            RecordUpdate (e, fields, ty)
    | Sequence (e1, e2, ty) ->
         let e1 = quantify_exp genv e1 in
         let e2 = quantify_exp genv e2 in
            Sequence (e1, e2, ty)
    | Match (e, cases, ty) ->
         let e = quantify_exp genv e in
         let cases = List.map (fun (p, e) -> p, quantify_exp genv e) cases in
            Match (e, cases, ty)
    | Try (e, cases, ty) ->
         let e = quantify_exp genv e in
         let cases = List.map (fun (p, e) -> p, quantify_exp genv e) cases in
            Try (e, cases, ty)
    | Raise (e, ty) ->
         let e = quantify_exp genv e in
            Raise (e, ty)
    | ApplyType _
    | In _
    | Out _
    | Open _
    | Action _
    | Restrict _
    | Ambient _
    | Thread _
    | Migrate _
    | LetModule _ ->
         raise (TAstException (pos, NotImplemented "inter_exp"))

(************************************************************************
 * MODULES
 ************************************************************************)

(*
 * Module expression.
 *)
and quantify_me genv me =
   let loc = loc_of_module_exp me in
   let me = dest_module_exp_core me in
   let me =
      match me with
         ModuleExpStruct (me_var, fields, names, ty) ->
            let fields = List.map (quantify_field genv) fields in
               ModuleExpStruct (me_var, fields, names, ty)
       | ModuleExpVar _ ->
            me
       | ModuleExpConstrain (me, ty) ->
            let me = quantify_me genv me in
               ModuleExpConstrain (me, ty)
       | ModuleExpFunctor (me_var, ty1, me, ty2) ->
            let me = quantify_me genv me in
               ModuleExpFunctor (me_var, ty1, me, ty2)
       | ModuleExpApply (me, me_name, ty) ->
            let me = quantify_me genv me in
               ModuleExpApply (me, me_name, ty)
   in
      make_module_exp loc me

(*
 * Infer a struct field.
 *)
and quantify_field genv field =
   let loc = loc_of_field field in
   let field = dest_field_core field in
   let field =
      match field with
         FieldLet lets ->
            let lets = List.map (fun (p, e, ty) -> p, quantify_exp genv e, ty) lets in
               FieldLet lets
       | FieldLetRec letrecs ->
            let letrecs = List.map (fun (v, e, ty_int, ty_ext) -> v, quantify_exp genv e, ty_int, ty_ext) letrecs in
               FieldLetRec letrecs
       | FieldExpr e ->
            let e = quantify_exp genv e in
               FieldExpr e
       | FieldModule (me_var, me, ty) ->
            let me = quantify_me genv me in
               FieldModule (me_var, me, ty)
       | FieldType _
       | FieldRestrict _
       | FieldModuleType _
       | FieldException _
       | FieldExternal _ ->
            field
   in
      make_field loc field

(************************************************************************
 * GLOBAL FUNCTION
 ************************************************************************)

(*
 * Quantify all the fields.
 *)
let quantify_prog prog =
   let genv = genv_of_prog prog in
   let { prog_exp = exp } = prog in
   let exp =
      match exp with
         Interface _ ->
            exp
       | Implementation (me_var, fields, names, ty) ->
            let fields = List.map (quantify_field genv) fields in
               Implementation (me_var, fields, names, ty)
   in
      { prog with prog_exp = exp }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
