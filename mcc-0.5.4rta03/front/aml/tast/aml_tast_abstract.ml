(*
 * Convert to "abstract" types.
 * An "abstract" type is a type that contains no module names.
 * The only type that contains a module name is TyProject,
 * so we eliminate all occurrences of TyProject.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_tast
open Aml_tast_ds
open Aml_tast_env
open Aml_tast_type
open Aml_tast_print

(*
 * Empty name environment.
 *)
let nenv_empty = TypeNameTable.empty

(*
 * Add a type name to the environment.
 *)
let nenv_add nenv name_kind =
   let { kind_value = name } = name_kind in
   try nenv, (TypeNameTable.find nenv name).kind_value with
      Not_found ->
         let v = new_symbol_string "name" in
         let var_kind = { name_kind with kind_value = v } in
         let nenv = TypeNameTable.add nenv name var_kind in
            nenv, v

(*
 * An "abstract" type is a type that contains no module names.
 * The only type that contains a module name is TyProject,
 * so we eliminate all occurrences of TyProject.
 *
 * is a set of vars that are ok to use.
 * vars are the set of abstract type vars.
 * args are the replacement types.
 *)
let rec build_abstract_type genv mt nenv ty =
   let loc = loc_of_type ty in
   let nenv, ty = build_abstract_type_core genv mt nenv (dest_type_core ty) in
   let ty = make_type loc ty in
      nenv, ty

and build_abstract_type_core genv mt nenv ty =
   match ty with
      TyInt
    | TyChar
    | TyString
    | TyFloat
    | TyVar _ ->
         nenv, ty
    | TyFun (tyl, ty) ->
         let nenv, tyl = build_abstract_types genv mt nenv tyl in
         let nenv, ty = build_abstract_type genv mt nenv ty in
            nenv, TyFun (tyl, ty)
    | TyFunctor (v, ty1, ty2) ->
         let nenv, ty1 = build_abstract_type genv mt nenv ty1 in
         let nenv, ty2 = build_abstract_type genv mt nenv ty2 in
            nenv, TyFunctor (v, ty1, ty2)
    | TyProd tyl ->
         let nenv, tyl = build_abstract_types genv mt nenv tyl in
            nenv, TyProd tyl
    | TyArray ty ->
         let nenv, ty = build_abstract_type genv mt nenv ty in
            nenv, TyArray ty
    | TyAll (ty_vars, ty) ->
         let nenv, ty = build_abstract_type genv mt nenv ty in
            nenv, TyAll (ty_vars, ty)
    | TySOVar (v, tyl) ->
         let nenv, tyl = build_abstract_types genv mt nenv tyl in
            nenv, TySOVar (v, tyl)
    | TyProject (name, tyl) ->
         let nenv, tyl = build_abstract_types genv mt nenv tyl in
            if genv_ty_name_is_abstract genv mt name then
               let ty = TyProject (name, tyl) in
                  nenv, ty
            else
               let len = List.length tyl in
               let kind = { kind_arity = len; kind_value = name } in
               let nenv, v = nenv_add nenv kind in
                  nenv, TySOVar (v, tyl)
    | TyApply (v, tyl1, tyl2) ->
         let nenv, tyl1 = build_abstract_types genv mt nenv tyl1 in
         let nenv, tyl2 = build_abstract_type_kinds genv mt nenv tyl2 in
            nenv, TyApply (v, tyl1, tyl2)
    | TyUnion (v, tyl1, tyl2) ->
         let nenv, tyl1 = build_abstract_types genv mt nenv tyl1 in
         let nenv, tyl2 = build_abstract_type_kinds genv mt nenv tyl2 in
            nenv, TyUnion (v, tyl1, tyl2)
    | TyRecord (v, tyl1, tyl2) ->
         let nenv, tyl1 = build_abstract_types genv mt nenv tyl1 in
         let nenv, tyl2 = build_abstract_type_kinds genv mt nenv tyl2 in
            nenv, TyRecord (v, tyl1, tyl2)
    | TyModule (v, tyl1, tyl2) ->
         let nenv, tyl1 = build_abstract_types genv mt nenv tyl1 in
         let nenv, tyl2 = build_abstract_type_kinds genv mt nenv tyl2 in
            nenv, TyModule (v, tyl1, tyl2)
    | TyFormat (ty1, ty2, ty3) ->
         let nenv, ty1 = build_abstract_type genv mt nenv ty1 in
         let nenv, ty2 = build_abstract_type genv mt nenv ty2 in
         let nenv, ty3 = build_abstract_type genv mt nenv ty3 in
            nenv, TyFormat (ty1, ty2, ty3)
    | TyExternal (ty, s) ->
         let nenv, ty = build_abstract_type genv mt nenv ty in
            nenv, TyExternal (ty, s)
    | TyOpen (names, v) ->
         raise (Failure "build_abstract_type: TyOpen not implemented")

and build_abstract_types genv mt nenv tyl =
   let nenv, tyl =
      List.fold_left (fun (nenv, tyl) ty ->
            let nenv, ty = build_abstract_type genv mt nenv ty in
               nenv, ty :: tyl) (nenv, []) tyl
   in
      nenv, List.rev tyl

(*
 * Type variable names.
 * If it is just a variable, leave it be.
 * Otherwise, replace it with a variable.
 *)
and build_abstract_type_kind genv mt nenv ty_kind =
   match ty_kind.kind_value with
      TyNameSoVar _
    | TyNameSelf _ ->
         nenv, ty_kind
    | TyNamePath (_, label) ->
         let nenv, v = nenv_add nenv ty_kind in
         let name = { ty_kind with kind_value = TyNameSoVar v } in
            nenv, name

and build_abstract_type_kinds genv mt nenv names =
   let nenv, names =
      List.fold_left (fun (nenv, names) name ->
            let nenv, name = build_abstract_type_kind genv mt nenv name in
               nenv, name :: names) (nenv, []) names
   in
      nenv, List.rev names

(*
 * Union fields.
 *)
let build_abstract_union_type genv mt nenv fields =
   let (nenv), fields =
      FieldTable.fold_map (fun (nenv) v tyl ->
            let nenv, tyl = build_abstract_types genv mt nenv tyl in
               (nenv), tyl) (nenv) fields
   in
      nenv, fields

(*
 * Record fields.
 *)
let build_abstract_record_type genv mt nenv fields =
   let (nenv), fields =
      FieldTable.fold_map (fun (nenv) v (b, ty) ->
            let nenv, ty = build_abstract_type genv mt nenv ty in
               (nenv), (b, ty)) (nenv) fields
   in
      nenv, fields

(*
 * Module type.
 *)
let build_abstract_symbol_table genv mt nenv table =
   let (nenv), table =
      SymbolTable.fold_map (fun (nenv) _ ty ->
            let nenv, ty = build_abstract_type genv mt nenv ty in
               (nenv), ty) (nenv) table
   in
      nenv, table

let build_abstract_symbol_table_opt genv mt nenv table =
   let (nenv), table =
      SymbolTable.fold_map (fun (nenv) _ (ty_vars, ty) ->
            match ty with
               Some ty ->
                  let nenv, ty = build_abstract_type genv mt nenv ty in
                     (nenv), (ty_vars, Some ty)
             | None ->
                  (nenv), (ty_vars, None)) (nenv) table
   in
      nenv, table

let build_abstract_module_type genv mt nenv =
   let { mt_mt_types = mt_mt_types;
         mt_me_vars  = mt_me_vars;
         mt_types    = mt_types;
         mt_vars     = mt_vars
       } = mt
   in
   let nenv, mt_mt_types = build_abstract_symbol_table genv mt nenv mt_mt_types in
   let nenv, mt_me_vars  = build_abstract_symbol_table genv mt nenv mt_me_vars in
   let nenv, mt_types    = build_abstract_symbol_table_opt genv mt nenv mt_types in
   let nenv, mt_vars     = build_abstract_symbol_table genv mt nenv mt_vars in
   let mt =
      { mt with mt_mt_types = mt_mt_types;
                mt_me_vars  = mt_me_vars;
                mt_types    = mt_types;
                mt_vars     = mt_vars
      }
   in
      nenv, mt

(*
 * Abstract type definitions.
 *)
let build_abstract_tydef_inner_core_aux genv tydef =
   match tydef with
      TyDefLambda ty ->
         let nenv, ty = build_abstract_type genv empty_module_type nenv_empty ty in
            nenv, TyDefLambda ty
    | TyDefUnion (kind, fields) ->
         let nenv, fields = build_abstract_union_type genv empty_module_type nenv_empty fields in
            nenv, TyDefUnion (kind, fields)
    | TyDefRecord fields ->
         let nenv, fields = build_abstract_record_type genv empty_module_type nenv_empty fields in
            nenv, TyDefRecord fields
    | TyDefModule mt ->
         let nenv, mt = build_abstract_module_type genv mt nenv_empty in
            nenv, TyDefModule mt

(*
 * Split the absracted names into new vars and new args.
 *)
let build_abstract_tydef_inner_core genv tydef =
   let nenv, tydef = build_abstract_tydef_inner_core_aux genv tydef in
   let vars, args =
      TypeNameTable.fold (fun (vars, args) name var_kind ->
            let args = { var_kind with kind_value = name } :: args in
            let vars = var_kind :: vars in
               vars, args) ([], []) nenv
   in
      vars, args, tydef

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
