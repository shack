(*
 * Initial environment.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

open Aml_type
open Aml_type_type

val pervasive_tenv : tenv
val pervasive_me : module_field
val pervasive_me_var : me_var

val ty_unit       : loc -> ty
val ty_bool       : loc -> ty
val ty_exn        : loc -> ty
val ty_array      : ty -> loc -> ty
val ty_list       : ty -> loc -> ty
val ty_ref        : ty -> loc -> ty
val ty_signal     : loc -> ty
val ty_sig_handle : loc -> ty
val ty_format     : ty -> ty -> ty -> loc -> ty

val unit_name : ty_name
val exn_name : ty_name
val const_match_failure : const_name

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
