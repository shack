(*
 * Lookup values from the environment.
 * For the most part, this is just name translation.
 *
 * However, the tricky part is that when we get to
 * a module name that is undefined, we need to look
 * for a file with that name.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Field_table

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_print
open Aml_tast_mt_util
open Aml_tast_env_type
open Aml_tast_env_file
open Aml_tast_env_base
open Aml_tast_tydef

module Pos = MakePos (struct let name = "Aml_tast_env_lookup" end)
open Pos

(************************************************************************
 * TYPEDEF LOOKUP
 ************************************************************************)

(*
 * External names.
 *)
let genv_lookup_name genv pos v =
   let pos = string_pos "genv_lookup_name" pos in
      try SymbolTable.find genv.genv_names v with
         Not_found ->
            raise (TAstException (pos, UnboundVar v))

(*
 * Get the type definition.
 *)
let genv_lookup_tydef genv pos v =
   try SymbolTable.find genv.genv_types v with
      Not_found ->
         raise (TAstException (pos, UnboundType v))

(*
 * Look up an import.
 *)
let genv_lookup_import genv pos me_var =
   let pos = string_pos "genv_lookup_import" pos in
   let ext_var = Symbol.reintern me_var in
      try SymbolTable.find genv.genv_import ext_var with
         Not_found ->
            raise (TAstException (pos, UnboundVar me_var))

(************************************************************************
 * INTERNAL LOOKUP
 ************************************************************************)

(*
 * Get the current module with the given self name.
 *)
let rec self_module levels me_var pos =
   match levels with
      { genv_core = core } :: next ->
         if Symbol.eq core.mt_self me_var then
            core
         else
            self_module next me_var pos
    | [] ->
         let pos = string_pos "self_module" pos in
            raise (TAstException (pos, UnboundModule me_var))

(*
 * These functions lookup values in the current module based on
 * _internal_ names.  This only looks at the modules in scope.
 *)
let rec internal_module_type levels mt_var pos =
   match levels with
      { genv_core = core } :: next ->
         (try SymbolTable.find core.mt_mt_types mt_var with
             Not_found ->
                internal_module_type next mt_var pos)
    | [] ->
         let pos = string_pos "internal_module_type" pos in
            raise (TAstException (pos, UnboundModuleType mt_var))

(*
 * For modules, if all else fails, then we try to look
 * up the value from the filesystem import list.
 *)
let rec internal_module genv levels me_var pos =
   match levels with
      { genv_core = core } :: next ->
         (try SymbolTable.find core.mt_me_vars me_var with
             Not_found ->
                internal_module genv next me_var pos)
    | [] ->
         (* Get the export name *)
         let _, _, ty = genv_lookup_import genv pos me_var in
            ty

let rec internal_type levels v pos =
   match levels with
      { genv_core = core } :: next ->
         (try SymbolTable.find core.mt_types v with
             Not_found ->
                internal_type next v pos)
    | [] ->
         let pos = string_pos "internal_type" pos in
            raise (TAstException (pos, UnboundType v))

let rec internal_var levels v pos =
   match levels with
      { genv_core = core } :: next ->
         (try SymbolTable.find core.mt_vars v with
             Not_found ->
                internal_var next v pos)
    | [] ->
         let pos = string_pos "internal_var" pos in
            raise (TAstException (pos, UnboundVar v))

let rec internal_const levels const_var pos =
   match levels with
      { genv_core = core } :: next ->
         (try SymbolTable.find core.mt_consts const_var with
             Not_found ->
                internal_const next const_var pos)
    | [] ->
         let pos = string_pos "internal_const" pos in
            raise (TAstException (pos, UnboundConst const_var))

let rec internal_label levels label pos =
   match levels with
      { genv_core = core } :: next ->
         (try SymbolTable.find core.mt_labels label with
             Not_found ->
                internal_label next label pos)
    | [] ->
         let pos = string_pos "internal_label" pos in
            raise (TAstException (pos, UnboundLabel label))

(************************************************************************
 * EXTERNAL LOOKUP
 ************************************************************************)

(*
 * Select a module from the core.
 *)
let select_module_type core ext_var pos =
   let pos = string_pos "select_module_type" pos in
   let ty_opt =
      try
         (* BUG: what to do with the type list *)
         let mt_var, _ = SymbolTable.find core.mt_names.names_mt ext_var in
            SymbolTable.find core.mt_types mt_var
      with
         Not_found ->
            raise (TAstException (pos, UnboundModuleType ext_var))
   in
      match ty_opt with
         [], Some ty ->
            ty
       | _, Some _ ->
            raise (TAstException (pos, StringError "polymorphic module type"))
       | _, None ->
            raise (TAstException (pos, UnboundModuleType ext_var))

let select_module core ext_var pos =
   let pos = string_pos "select_module" pos in
      try
         let me_var = FieldTable.find core.mt_names.names_me ext_var in
            SymbolTable.find core.mt_vars me_var
      with
         Not_found ->
            raise (TAstException (pos, UnboundModule ext_var))

let select_type core ext_var pos =
   let pos = string_pos "select_type" pos in
      try
         let ty_var = SymbolTable.find core.mt_names.names_ty ext_var in
            SymbolTable.find core.mt_types ty_var
      with
         Not_found ->
            raise (TAstException (pos, UnboundType ext_var))

let select_var core ext_var pos =
   let pos = string_pos "select_var" pos in
      try
         let v = FieldTable.find core.mt_names.names_var ext_var in
            SymbolTable.find core.mt_vars v
      with
         Not_found ->
            raise (TAstException (pos, UnboundType ext_var))

let select_const core ext_var pos =
   let pos = string_pos "select_const" pos in
      try
         let const_var = SymbolTable.find core.mt_names.names_const ext_var in
         let ty_var, arity, field = SymbolTable.find core.mt_consts const_var in
            ty_var, const_var, arity, field
      with
         Not_found ->
            raise (TAstException (pos, UnboundConst ext_var))

let select_label core ext_var pos =
   let pos = string_pos "select_label" pos in
      try
         let label = SymbolTable.find core.mt_names.names_label ext_var in
         let ty_var, arity, kinds = SymbolTable.find core.mt_labels label in
            ty_var, label, arity, kinds
      with
         Not_found ->
            raise (TAstException (pos, UnboundLabel ext_var))

(************************************************************************
 * GENERAL LOOKUP
 ************************************************************************)

(*
 * Get the type referred to by NamePath (me_var, path, ext_ty_var).
 * All the types in the result are renamed so that they are referenced
 * from me_var as the root.
 *
 * The unwrapping function extracts the type without modifying
 * the module type.  It is fairly complicated, due to the need to resolve
 * the names in mod_type.
 *)
let unwrap_module genv mod_name pos =
   let pos = string_pos "unwrap_module" pos in
   let levels = levels_of_genv genv in

   (*
    * Descend the module name tree.
    *)
   let rec unwrap_mod_name mod_name args ext_vars =
      match mod_name with
         ModNameEnv me_var ->
            unwrap_module_type (internal_module genv levels me_var pos) empty_subst mod_name args ext_vars
       | ModNameSelf (me_var, ext_var) ->
            let core = self_module levels me_var pos in
            let module_type = select_module core ext_var pos in
            let path = ModNameProj (mod_name, ext_var) in
               unwrap_module_type module_type empty_subst path args ext_vars
       | ModNameProj (mod_name, ext_var) ->
            unwrap_mod_name mod_name args (ext_var :: ext_vars)
       | ModNameApply (mod_name1, mod_name2) ->
            unwrap_mod_name mod_name1 (mod_name2 :: args) ext_vars

   (*
    * Expand from a module type.
    *)
   and unwrap_module_type module_type subst path args ext_vars =
      match dest_type_core module_type with
         TyFunctor (me_var, module_type1, module_type2) ->
            (match args with
                arg :: args ->
                   let subst = subst_add_me_var subst me_var arg in
                      unwrap_module_type module_type2 subst path args ext_vars
              | [] ->
                   if ext_vars = [] then
                      subst, module_type
                   else
                      raise (TAstException (pos, StringError "module arity mismatch")))

       | TyModule (ty_var, tyl1, tyl2) ->
            let core = apply_module_type genv pos ty_var tyl1 tyl2 in
            let subst = subst_add_me_var subst core.mt_self path in
               unwrap_core module_type core subst path args ext_vars

       | _ ->
            raise (TAstException (pos, StringTypeError ("not a module type", module_type)))

   (*
    * Walk down the list of ext_vars.
    *)
   and unwrap_core module_type core subst path args ext_vars =
      match ext_vars with
         ext_var :: ext_vars ->
            (* Descend one more module *)
            let module_type = select_module core ext_var pos in
            let path = ModNameProj (path, ext_var) in
               unwrap_module_type module_type subst path args ext_vars

       | [] ->
            subst, module_type
   in
      unwrap_mod_name mod_name [] []

(*
 * Module lookup.
 *)
let genv_lookup_module_type genv pos name =
   let pos = string_pos "genv_lookup_module_type" pos in
      match name with
         TyNameSoVar mt_var ->
            raise (TAstException (pos, StringError "illegal TyNameSoVar"))
       | TyNameSelf (me_var, ext_var) ->
            let core = genv.genv_current.genv_core in
               select_module_type core ext_var pos
       | TyNamePath (me_name, ext_var) ->
            let subst, module_type = unwrap_module genv me_name pos in
            let core = dest_module_type genv pos module_type in
            let module_type = select_module_type core ext_var pos in
               subst_type subst module_type

let genv_lookup_module genv pos me_name =
   let pos = string_pos "genv_lookup_module" pos in
   let subst, module_type = unwrap_module genv me_name pos in
      subst_type subst module_type

(*
 * Resolve a type.
 *
 * If the module name is "self", then lookup the type in
 * the current module.
 *)
let genv_lookup_type genv pos name =
   let pos = string_pos "genv_lookup_type" pos in
      match name with
         TyNameSoVar ty_var ->
            raise (TAstException (pos, StringError "illegal TyNameSoVar"))
       | TyNameSelf (me_var, ext_var) ->
            let core = genv.genv_current.genv_core in
               select_type core ext_var pos
       | TyNamePath (me_name, ext_var) ->
            let subst, module_type = unwrap_module genv me_name pos in
            let core = dest_module_type genv pos module_type in
            let ty_vars, ty_opt = select_type core ext_var pos in
            let ty_opt =
               match ty_opt with
                   Some ty ->
                      Some (subst_type subst ty)
                 | None ->
                      None
            in
               ty_vars, ty_opt

(*
 * Resolve a variable.
 *)
let genv_lookup_var genv pos name =
   let pos = string_pos "genv_lookup_var" pos in
      match name with
         NameEnv v ->
            internal_var (levels_of_genv genv) v pos
       | NamePath (me_name, ext_var) ->
            let subst, module_type = unwrap_module genv me_name pos in
            let core = dest_module_type genv pos module_type in
            let ty = select_var core ext_var pos in
               subst_type subst ty

(*
 * Resolve a constructor.
 *)
let genv_lookup_const genv pos name =
   let pos = string_pos "genv_lookup_const" pos in
      match name with
         NameEnv const_var ->
            let ty_var, arity, field = internal_const (levels_of_genv genv) const_var pos in
               ty_var, const_var, arity, field
       | NamePath (me_name, ext_var) ->
            let subst, module_type = unwrap_module genv me_name pos in
            let core = dest_module_type genv pos module_type in
            let ty_var, const_var, arity, field = select_const core ext_var pos in
            let field = subst_const_field subst field in
               ty_var, const_var, arity, field

(*
 * An exception constructor.
 *)
let genv_lookup_exn genv pos const_var =
   let pos = string_pos "genv_lookup_exn" pos in
      match internal_const (levels_of_genv genv) const_var pos with
         _, 0, ConstException tyl ->
            tyl
       | _ ->
            raise (TAstException (pos, StringVarError ("not an exception constructor", const_var)))

(*
 * Resolve a constructor.
 *)
let genv_lookup_label genv pos name =
   let pos = string_pos "genv_lookup_label" pos in
      match name with
         NameEnv label ->
            let ty_var, arity, kinds = internal_label (levels_of_genv genv) label pos in
               ty_var, label, arity, kinds
       | NamePath (me_name, ext_var) ->
            let subst, module_type = unwrap_module genv me_name pos in
            let core = dest_module_type genv pos module_type in
            let ty_var, label, arity, kinds = select_label core ext_var pos in
            let kinds = subst_type_kinds subst kinds in
               ty_var, label, arity, kinds

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
