(*
 * General substitution operations.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Debug
open Symbol

open Location

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn

let debug_subst =
   create_debug (**)
      { debug_name = "subst";
        debug_description = "Print substitution operations";
        debug_value = false
      }

module Pos = MakePos (struct let name = "Aml_tast_subst" end)
open Pos

(************************************************************************
 * ADDITION
 ************************************************************************)

(*
 * Define one var as another.
 *)
let subst_equate_fo_vars subst v1 v2 loc =
   subst_add_fo_var subst v1 (make_type loc (TyVar v2))

(*
 * Relocate a type.
 *)
let relocate_type loc ty =
   subst_type (subst_add_loc empty_subst loc) ty

(************************************************************************
 * LOOKUP
 ************************************************************************)

(*
 * Look up the type for a fo_var.
 *)
let subst_lookup_fo_var subst pos v =
   try SymbolTable.find subst.sub_fo_vars v with
      Not_found ->
         raise (TAstException (pos, UnboundTyVar v))

(*
 * Lookup the type of the parameter.
 *)
let subst_lookup_fo_var_exn subst v =
   SymbolTable.find subst.sub_fo_vars v

let subst_lookup_fo_var_loc subst v loc =
   try subst_lookup_fo_var_exn subst v with
      Not_found ->
         let pos = string_pos "subst_lookup_fo_var_exn" (loc_pos loc) in
            raise (TAstException (pos, UnboundTyVar v))

(*
 * Relocate the types of expression2,
 * and allow exceptions.
 *)
let subst_lookup_fo_var_relocate_exn subst v loc =
   let ty = SymbolTable.find subst.sub_fo_vars v in
      subst_type (subst_add_loc empty_subst loc) ty

(*
 * Relocate the types of expression2.
 *)
let subst_lookup_fo_var_relocate subst v loc =
   try subst_lookup_fo_var_relocate_exn subst v loc with
      Not_found ->
         let pos = string_pos "lookup_loc" (loc_pos loc) in
            raise (TAstException (pos, UnboundTyVar v))

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
