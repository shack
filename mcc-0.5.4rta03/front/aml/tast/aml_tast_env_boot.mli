(*
 * Base environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_tast
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env_type

(*
 * Empty open record.
 *)
val empty_opens : open_info

(*
 * Empty values.
 *)
val empty_module_type : module_type

(*
 * Empty environment.
 * The var is the name of the module being compiled.
 *)
val genv_empty : var -> genv

(*
 * Get the pervasive types.
 *)
val ty_unit    : genv -> loc -> ty
val ty_bool    : genv -> loc -> ty
val ty_exn     : genv -> loc -> ty
val ty_exn_var : genv -> ty_var

val type_of_external : genv -> pos -> ty_var list -> string -> loc -> ty

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
