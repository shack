(*
 * Lookup values from the environment.
 * For the most part, this is just name translation.
 *
 * However, the tricky part is that when we get to
 * a module name that is undefined, we need to look
 * for a file with that name.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_tast
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_tydef
open Aml_tast_env_type
open Aml_tast_env_base
open Aml_tast_env_file

module Pos = MakePos (struct let name = "Aml_tast_env_intern" end)
open Pos

(*
 * The AST represents module projections with lists.
 * We convert them to TAST data structures.
 *)
let rec wrap_mod_name pos mod_name = function
   ext_var :: ext_vars ->
      wrap_mod_name pos (ModNameProj (mod_name, ext_var)) ext_vars
 | [] ->
      mod_name

(*
 * Convert external module names to internal names.
 *
 * There are three cases:
 *    1. The name is a single variable v:
 *       a. If the variable is bound in me_opens, use that value
 *       b. If not, look up the variable if me_intern
 *    2. The name is a path (uid :: name)
 *       In this case, the uid refers to a module, and the name is a
 *       projection.  Lookup the module by its uid, and append the path.
 *       a. If the uid is bound in me_opens, concatenate the paths.
 *       b. If not, lookup uid in me_intern.
 *)
let rec resolve_module genv levels uid path pos =
   match levels with
      { genv_core = core; genv_opens = opens } :: next ->
         (try
             let mod_name = SymbolTable.find opens.me_opens uid in
             let mod_name = wrap_mod_name pos mod_name path in
                genv, mod_name
          with
             Not_found ->
                try
                   let me_var = FieldTable.find core.mt_names.names_me uid in
                   let mod_name = wrap_mod_name pos (ModNameEnv me_var) path in
                      genv, mod_name
                with
                   Not_found ->
                      resolve_module genv next uid path pos)
    | [] ->
         let genv, me_var, _ = load_interface genv pos uid in
         let mod_name = wrap_mod_name pos (ModNameEnv me_var) path in
            genv, mod_name

(*
 * Get a module by name.
 * This case is similar to resolve_module, but the module
 * is just a single variable.
 *)
let rec find_module genv levels v pos =
   match levels with
      { genv_core = core; genv_opens = opens } :: next ->
         (try
             let mod_name = SymbolTable.find opens.me_opens v in
             let mod_name = ModNameProj (mod_name, v) in
                genv, mod_name
          with
             Not_found ->
                try
                   let me_var = FieldTable.find core.mt_names.names_me v in
                   let mod_name = ModNameEnv me_var in
                      genv, mod_name
                with
                   Not_found ->
                      find_module genv next v pos)
    | [] ->
         let genv, me_var, _ = load_interface genv pos v in
         let mod_name = ModNameEnv me_var in
            genv, mod_name

(*
 * General module form.
 *)
let genv_intern_module genv pos name =
   let pos = string_pos "genv_intern_module" pos in
   let levels = levels_of_genv genv in
      match name with
         [v] ->
            find_module genv levels v pos
       | uid :: name ->
            resolve_module genv levels uid name pos
       | [] ->
            raise (TAstException (pos, InternalError))

(*
 * Convert module type names.
 * This is similar to module resolution,
 * but we just resolve the module in the name's prefix.
 *)
let rec find_module_type genv levels v pos =
   match levels with
      { genv_core = core; genv_opens = opens } :: next ->
         (try
             let mod_name = SymbolTable.find opens.mt_opens v in
                genv, TyNamePath (mod_name, v)
          with
             Not_found ->
                try
                   let _ = SymbolTable.find core.mt_names.names_mt v in
                      genv, TyNameSelf (core.mt_self, v)
                with
                   Not_found ->
                      find_module_type genv next v pos)
    | [] ->
         let genv, me_var, _ = load_interface genv pos v in
            genv, TyNamePath (ModNameEnv me_var, v)

let genv_intern_module_type genv pos name =
   let pos = string_pos "genv_intern_module_type" pos in
   let levels = levels_of_genv genv in
      match name with
         [v] ->
            find_module_type genv levels v pos
       | uid :: name ->
            let name, ext_var = Mc_list_util.split_last name in
            let genv, name = resolve_module genv levels uid name pos in
               genv, TyNamePath (name, ext_var)
       | [] ->
            raise (TAstException (pos, InternalError))

(*
 * Convert type names.
 *)
let rec find_type levels v pos =
   let pos = string_pos "find_type" pos in
      match levels with
         { genv_core = core; genv_opens = opens } :: next ->
            (try
                let mod_name = SymbolTable.find opens.ty_opens v in
                   TyNamePath (mod_name, v)
             with
                Not_found ->
                   try
                      let _ = SymbolTable.find core.mt_names.names_ty v in
                         TyNameSelf (core.mt_self, v)
                   with
                      Not_found ->
                         find_type next v pos)
       | [] ->
            raise (TAstException (pos, UnboundType v))

let genv_intern_type genv pos name =
   let pos = string_pos "genv_intern_type" pos in
   let levels = levels_of_genv genv in
      match name with
         [v] ->
            genv, find_type levels v pos
       | uid :: name ->
            let name, ext_var = Mc_list_util.split_last name in
            let genv, name = resolve_module genv levels uid name pos in
               genv, TyNamePath (name, ext_var)
       | [] ->
            raise (TAstException (pos, InternalError))

(*
 * Convert variable names.
 *)
let rec find_var levels v pos =
   match levels with
      { genv_core = core; genv_opens = opens } :: next ->
         (try
             let mod_name = SymbolTable.find opens.var_opens v in
                NamePath (mod_name, v)
          with
             Not_found ->
                try
                   let v = FieldTable.find core.mt_names.names_var v in
                      NameEnv v
                with
                   Not_found ->
                      find_var next v pos)
    | [] ->
         raise (TAstException (pos, UnboundVar v))

let genv_intern_var genv pos name =
   let pos = string_pos "genv_intern_var" pos in
   let levels = levels_of_genv genv in
      match name with
         [v] ->
            genv, find_var levels v pos
       | uid :: name ->
            let name, ext_var = Mc_list_util.split_last name in
            let genv, name = resolve_module genv levels uid name pos in
               genv, NamePath (name, ext_var)
       | [] ->
            raise (TAstException (pos, InternalError))

(*
 * Convert constructor names.
 *)
let rec find_const levels v pos =
   match levels with
      { genv_core = core; genv_opens = opens } :: next ->
         (try
             let mod_name = SymbolTable.find opens.const_opens v in
                NamePath (mod_name, v)
          with
             Not_found ->
                try NameEnv (SymbolTable.find core.mt_names.names_const v) with
                   Not_found ->
                      find_const next v pos)
    | [] ->
         raise (TAstException (pos, UnboundConst v))

let genv_intern_const genv pos name =
   let pos = string_pos "genv_intern_const" pos in
   let levels = levels_of_genv genv in
      match name with
         [v] ->
            genv, find_const levels v pos
       | uid :: name ->
            let name, ext_var = Mc_list_util.split_last name in
            let genv, name = resolve_module genv levels uid name pos in
               genv, NamePath (name, ext_var)
       | [] ->
            raise (TAstException (pos, InternalError))

(*
 * Convert label names.
 *)
let rec find_label levels v pos =
   match levels with
      { genv_core = core; genv_opens = opens } :: next ->
         (try
             let mod_name = SymbolTable.find opens.label_opens v in
                NamePath (mod_name, v)
          with
             Not_found ->
                try NameEnv (SymbolTable.find core.mt_names.names_label v) with
                   Not_found ->
                      find_label next v pos)
    | [] ->
         raise (TAstException (pos, UnboundLabel v))

let genv_intern_label genv pos name =
   let pos = string_pos "genv_intern_label" pos in
   let levels = levels_of_genv genv in
      match name with
         [v] ->
            genv, find_label levels v pos
       | uid :: name ->
            let name, ext_var = Mc_list_util.split_last name in
            let genv, name = resolve_module genv levels uid name pos in
               genv, NamePath (name, ext_var)
       | [] ->
            raise (TAstException (pos, InternalError))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
