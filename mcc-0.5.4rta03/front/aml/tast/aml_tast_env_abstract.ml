(*
 * Some helper functions for type abstraction.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Aml_tast
open Aml_tast_tydef
open Aml_tast_mt_util
open Aml_tast_env_type

(*
 * Check if all the modules in the module name
 * are imported modules.
 *)
let rec me_name_is_global genv me_name =
   match me_name with
      ModNameEnv me_var
    | ModNameSelf (me_var, _) ->
         (*
          * BUG: this is an _internal_ variable, so this doesn't work.
          * We probably want to keep track of the internal names of the
          * imports.
          *)
         SymbolTable.mem genv.genv_import me_var
    | ModNameProj (me_name, _) ->
         me_name_is_global genv me_name
    | ModNameApply (me_name1, me_name2) ->
         me_name_is_global genv me_name1 && me_name_is_global genv me_name2

(*
 * This predicate determines if a type name should
 * be abstracted.
 *
 * A type name is _not_ abstracted if either of the following are true:
 *    1. it is defined in the current module
 *    2. it is projected from an imported module
 *
 * All other names must be abstracted.
 *)
let genv_ty_name_is_abstract genv mt ty_name =
   match ty_name with
      TyNameSoVar v ->
         (* Always abstract *)
         true

    | TyNameSelf (_, v) ->
         (* Check if the external var is defined *)
         mt_mem_type_ext mt v

    | TyNamePath (me_name, _) ->
         (* Check if the module is defined as an import *)
         me_name_is_global genv me_name

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
