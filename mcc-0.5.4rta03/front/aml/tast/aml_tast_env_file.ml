(*
 * File IO for interfaces.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env_type
open Aml_tast_env_base
open Aml_tast_env_boot
open Aml_tast_env_add
open Aml_tast_standardize

module Pos = MakePos (struct let name = "Aml_tast_env_file" end)
open Pos

let debug_load =
   create_debug (**)
      { debug_name = "load";
        debug_description = "print file loading information";
        debug_value = false
      }

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * The program is saved with a hash code.
 *    ext_hash: the hash code
 *    ext_imp: whether this program was produced by compiling a .ml file
 *    ext_prog: the program itself
 *)
type ext =
   { ext_hash : hash;
     ext_imp  : bool;
     ext_prog : prog
   }

(************************************************************************
 * GLOBAL TABLE
 ************************************************************************)

(*
 * Magic number for this version of the compiler.
 *)
let magic_number = Hashtbl.hash "$Id: aml_tast_env_file.ml,v 1.6 2002/09/16 21:40:40 jyh Exp $"

(*
 * Suffixes.
 *)
let interface_src_suffix = ".mli"
let interface_bin_suffix = ".ami"

(*
 * Safer marshaling.
 *)
let input_ext_value inx =
   (input_value inx : ext)

let output_ext_value outx (ext : ext) =
   output_value outx ext

(*
 * This is the raw program loader.
 * It opens the file, checks the magic number,
 * and unmarshals the program.
 *)
let load_file genv filename =
   let inx = open_in_bin filename in
   let ext =
      try
         (* Check the magic number *)
         let magic = input_binary_int inx in
         let _ =
            if magic <> magic_number then
               raise (Sys_error "file was compiled with another compiler version")
         in

         (* Get the program *)
         let ext = input_ext_value inx in
            close_in inx;
            ext
      with
         Sys_error _ as exn ->
            close_in inx;
            raise exn
       | End_of_file ->
            close_in inx;
            raise (Sys_error "truncated file")
       | _ ->
            close_in inx;
            raise (Failure "unexpected exception while loading a file")
   in
   let exn_var = ty_exn_var genv in
      { ext with ext_prog = standardize_interface exn_var ext.ext_prog }

(*
 * This is the raw program loader.
 * It searches the include path for an occurence of the file.
 *)
let search_file genv name =
   let s = Symbol.to_string name in

   (* Format the name of the module *)
   let filename = String.uncapitalize s in
   let filename = filename ^ interface_bin_suffix in

   (* Walk through the search path *)
   let rec search dirs =
      match dirs with
         d :: dirs ->
            (try load_file genv filename with
                Sys_error _ ->
                   search dirs)
       | [] ->
            raise Not_found
   in
      search (List.rev !Fir_state.rev_include)

(*
 * Merge a program into the current environment.
 *)
let merge_file genv pos name hash prog me_var =
   let { prog_names = names2;
         prog_types = types2;
         prog_exp   = exp
       } = prog
   in

   (* Figure out the name of the toplevel module *)
   let ty =
      match exp with
         Interface ty
       | Implementation (_, _, _, ty) ->
            ty
   in
   let genv = genv_add_module genv name me_var ty in

   (* Add all the current types *)
   let { genv_names = names1;
         genv_types = types1;
         genv_import = import
       } = genv
   in
   let names = SymbolTable.union names1 names2 in
   let types = SymbolTable.union types1 types2 in
      { genv with genv_names = names;
                  genv_import = SymbolTable.add import name (hash, me_var, ty);
                  genv_types = types
      }

(*
 * Get the module with the given external name.  This
 * works by side-effect: the module is loaded into
 * the cache.
 *
 * First, see if it is already in the cache.  If the
 * hash codes match up, then we don't need to do anything.
 *
 * Otherwise, if it doesn't exist, try to load it from
 * a file.
 *)
let rec load_file interface_onlyp genv pos name hash me_var =
   try
      let hash', _, _ = SymbolTable.find genv.genv_import name in
         if hash <> 0 && hash' <> hash then
            raise (TAstException (pos, StringVarError ("bogus hash", name)));
         genv
   with
      Not_found ->
         let ext = search_file genv name in
         let { ext_hash = hash';
               ext_imp  = imp;
               ext_prog = prog
             } = ext
         in
         let _ =
            if hash <> 0 && hash' <> hash then
               raise (TAstException (pos, StringVarError ("bogus hash", name)))
         in
         let _ =
            if interface_onlyp && imp then
               raise Not_found
         in

         (*
          * Merge the loaded program with the current environment.
          * This will save the file in the genv file cache,
          * which also means that the file becomes a dependency.
          *)
         let genv = merge_file genv pos name hash prog me_var in

            (* Recursively load all the included modules *)
            SymbolTable.fold (fun genv name (hash, me_var, _) ->
                  load_file false genv pos (reintern name) hash me_var) genv prog.prog_import

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * Load an interface.
 * Allow the Not_found exception.
 *)
let load_interface_exn interface_onlyp genv pos name =
   let me_var = new_symbol name in
   let genv = load_file interface_onlyp genv pos name 0 me_var in
   let _, me_var, ty = SymbolTable.find genv.genv_import name in
      genv, me_var, ty

(*
 * Load an interface.
 *)
let load_interface genv pos name =
   try load_interface_exn false genv pos name with
      Not_found ->
         raise (TAstException (pos, UnboundModule name))

(*
 * Load the program interface.
 * The ty is the type to use if the interface is not found.
 *)
let load_interface_default genv pos name ty =
   try
      let genv, _, ty = load_interface_exn true genv pos name in
         genv, ty
   with
      Not_found ->
         genv, ty

(*
 * Name of the interface file.
 *)
let filename_of_prog prog =
   let { prog_name = name } = prog in
   let filename = Symbol.to_string name in
   let filename = String.uncapitalize filename in
      filename

let srcfile_of_prog prog =
   filename_of_prog prog ^ interface_src_suffix

let binfile_of_prog prog =
   filename_of_prog prog ^ interface_bin_suffix

(*
 * Save a file.
 *)
let save_interface prog =
   let { prog_name = name } = prog in
   let loc = create_loc name 0 0 0 0 in
   let pos = string_pos "save_interface" (loc_pos loc) in

   (* This is the value we save *)
   let imp, prog =
      match prog.prog_exp with
         Interface _ ->
            false, prog
       | Implementation (_, _, _, ty) ->
            true, { prog with prog_exp = Interface ty }
   in
   let hash = Hashtbl.hash prog in
   let ext =
      { ext_hash = hash;
        ext_imp  = imp;
        ext_prog = prog
      }
   in

   (* This is the file we save into *)
   let filename = binfile_of_prog prog in
      try
         let outx = open_out_bin filename in
            try
               output_binary_int outx magic_number;
               output_ext_value outx ext;
               close_out outx
            with
               _ ->
                  close_out outx;
                  raise (TAstException (pos, StringVarError ("unexpected exception while saving file", name)))
      with
         Sys_error s ->
            raise (TAstException (pos, StringError ("can't open file: " ^ s)))

(*
 * Save an interface for the implementation if
 * there isn't one already.
 *)
let save_implementation prog =
   let filename = srcfile_of_prog prog in
      if not (Sys.file_exists filename) then
         save_interface prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
