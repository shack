(*
 * Some utilities on the names record.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_tast

(*
 * No names.
 *)
let nenv_empty =
   { names_mt     = SymbolTable.empty;
     names_me     = FieldTable.empty;
     names_ty     = SymbolTable.empty;
     names_var    = FieldTable.empty;
     names_const  = SymbolTable.empty;
     names_label  = SymbolTable.empty
   }

(*
 * Addition.
 *)
let nenv_add_module_type names mt_var mt_var' ty_args =
   { names with names_mt = SymbolTable.add names.names_mt mt_var (mt_var', ty_args) }

let nenv_add_module names me_var me_var' =
   { names with names_me = FieldTable.add names.names_me me_var me_var' }

let nenv_add_type names ty_var ty_var' =
   { names with names_ty = SymbolTable.add names.names_ty ty_var ty_var' }

let nenv_add_var names v v' =
   { names with names_var = FieldTable.add names.names_var v v' }

let nenv_add_const names const_var const_var' =
   { names with names_const = SymbolTable.add names.names_const const_var const_var' }

let nenv_add_label names label_var label_var' =
   { names with names_label = SymbolTable.add names.names_label label_var label_var' }

(*
 * Lookup.
 *)
let nenv_lookup_module_type names ext_var =
   SymbolTable.find names.names_mt ext_var

let nenv_lookup_module names ext_var =
   FieldTable.find names.names_me ext_var

let nenv_lookup_type names ext_var =
   SymbolTable.find names.names_ty ext_var

let nenv_lookup_var names ext_var =
   FieldTable.find names.names_var ext_var

let nenv_lookup_const names ext_var =
   SymbolTable.find names.names_const ext_var

let nenv_lookup_label names ext_var =
   SymbolTable.find names.names_label ext_var

(*
 * Membership.
 *)
let nenv_mem_module_type names ext_var =
   SymbolTable.mem names.names_mt ext_var

let nenv_mem_module names ext_var =
   FieldTable.mem names.names_me ext_var

let nenv_mem_type names ext_var =
   SymbolTable.mem names.names_ty ext_var

let nenv_mem_var names ext_var =
   FieldTable.mem names.names_var ext_var

let nenv_mem_const names ext_var =
   SymbolTable.mem names.names_const ext_var

let nenv_mem_label names ext_var =
   SymbolTable.mem names.names_label ext_var

(*
 * Internal testing.
 *)
let mt_mem_module_type mt v =
   SymbolTable.mem mt.mt_mt_types v

let mt_mem_module mt v =
   SymbolTable.mem mt.mt_me_vars v

let mt_mem_type mt v =
   SymbolTable.mem mt.mt_types v

let mt_mem_var mt v =
   SymbolTable.mem mt.mt_vars v

let mt_mem_const mt v =
   SymbolTable.mem mt.mt_consts v

let mt_mem_label mt v =
   SymbolTable.mem mt.mt_labels v

(*
 * External testing.
 *)
let mt_mem_module_type_ext mt v =
   nenv_mem_module_type mt.mt_names v

let mt_mem_module_ext mt v =
   nenv_mem_module mt.mt_names v

let mt_mem_type_ext mt v =
   nenv_mem_type mt.mt_names v

let mt_mem_var_ext mt v =
   nenv_mem_var mt.mt_names v

let mt_mem_const_ext mt v =
   nenv_mem_const mt.mt_names v

let mt_mem_label_ext mt v =
   nenv_mem_label mt.mt_names v

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
