(*
 * Delayed substition operations.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Field_table

open Location
open Attribute

open Aml_tast
open Aml_tast_exn

(************************************************************************
 * FREE VARS                                                            *
 ************************************************************************)

(*
 * Empty free vars.
 *)
let free_vars_empty =
   { fv_vars      = SymbolSet.empty;
     fv_fo_vars   = SymbolSet.empty;
     fv_so_vars   = SymbolSet.empty;
     fv_me_vars   = SymbolSet.empty
   }

(*
 * Union of two free vars sets.
 *)
let free_vars_union fv1 fv2 =
   let { fv_vars      = vars1;
         fv_fo_vars   = params1;
         fv_so_vars   = names1;
         fv_me_vars   = me1
       } = fv1
   in
   let { fv_vars      = vars2;
         fv_fo_vars   = params2;
         fv_so_vars   = names2;
         fv_me_vars   = me2
       } = fv2
   in
      { fv_vars      = SymbolSet.union vars1 vars2;
        fv_fo_vars   = SymbolSet.union params1 params2;
        fv_so_vars   = SymbolSet.union names1 names2;
        fv_me_vars   = SymbolSet.union me1 me2
      }

(*
 * Equality on free vars.
 *)
let free_vars_equal fv1 fv2 =
   let { fv_vars      = vars1;
         fv_fo_vars   = params1;
         fv_so_vars   = names1;
         fv_me_vars   = me1
       } = fv1
   in
   let { fv_vars      = vars2;
         fv_fo_vars   = params2;
         fv_so_vars   = names2;
         fv_me_vars   = me2
       } = fv2
   in
      SymbolSet.equal vars1 vars2
      && SymbolSet.equal params1 params2
      && SymbolSet.equal names1 names2
      && SymbolSet.equal me1 me2

(*
 * Adding variables.
 *)
let free_vars_add_fo_var fv v =
   { fv with fv_fo_vars = SymbolSet.add fv.fv_fo_vars v }

let free_vars_add_fo_vars fv vars =
   let rec add subst = function
      v :: vars ->
         add (SymbolSet.add subst v) vars
    | [] ->
         subst
   in
      { fv with fv_fo_vars = add fv.fv_fo_vars vars }

let free_vars_add_so_var fv v =
   { fv with fv_so_vars = SymbolSet.add fv.fv_so_vars v }

let free_vars_add_var fv v =
   { fv with fv_vars = SymbolSet.add fv.fv_vars v }

let free_vars_add_me_var fv me_var =
   { fv with fv_me_vars = SymbolSet.add fv.fv_me_vars me_var }

(*
 * Removing variables.
 *)
let free_vars_subtract_fo_var fv v =
   { fv with fv_fo_vars = SymbolSet.remove fv.fv_fo_vars v }

let free_vars_subtract_fo_vars fv vars =
   { fv with fv_fo_vars = SymbolSet.subtract_list fv.fv_fo_vars vars }

let free_vars_subtract_so_var fv v =
   { fv with fv_so_vars = SymbolSet.remove fv.fv_so_vars v }

let free_vars_subtract_so_vars fv vars =
   { fv with fv_so_vars = SymbolSet.subtract_list fv.fv_so_vars vars }

let free_vars_subtract_so_vars_kind fv vars =
   let fv_so_vars =
      List.fold_left (fun fv_so_vars { kind_value = v } ->
            SymbolSet.remove fv_so_vars v) fv.fv_so_vars vars
   in
      { fv with fv_so_vars = fv_so_vars }

let free_vars_subtract_var fv v =
   { fv with fv_vars = SymbolSet.remove fv.fv_vars v }

let free_vars_subtract_me_var fv me_var =
   { fv with fv_me_vars = SymbolSet.remove fv.fv_me_vars me_var }

(*
 * Subtract the domain of the subst,
 * and add the free vars of the range.
 *)
let rec free_vars_subst subst fv =
   let { sub_vars      = sub_var;
         sub_me_vars   = sub_me;
         sub_fo_vars   = sub_fo_vars;
         sub_so_vars   = sub_so_vars
       } = subst
   in

   (* Add the substituted values until fixpoint *)
   let step fv =
      let fv =
         SymbolTable.fold (fun fv v (ty, _) ->
               if SymbolSet.mem fv.fv_vars v then
                  free_vars_mod_name fv ty
               else
                  fv) fv sub_var
      in
      let fv =
         SymbolTable.fold (fun fv v ty ->
               if SymbolSet.mem fv.fv_fo_vars v then
                  free_vars_type fv ty
               else
                  fv) fv sub_fo_vars
      in
      let fv =
         SymbolTable.fold (fun fv v ty_name ->
               if SymbolSet.mem fv.fv_so_vars v then
                  free_vars_type_kind fv ty_name
               else
                  fv) fv sub_so_vars
      in
      let fv =
         SymbolTable.fold (fun fv v mod_name ->
               if SymbolSet.mem fv.fv_me_vars v then
                  free_vars_mod_name fv mod_name
               else
                  fv) fv sub_me
      in
         fv
   in
   let rec fixpoint fv1 =
      let fv2 = step fv1 in
         if free_vars_equal fv2 fv1 then
            fv1
         else
            fixpoint fv2
   in
   let fv = fixpoint fv in

   (* Now subtract the domain of the substitution *)
   let { fv_vars      = vars;
         fv_me_vars   = me;
         fv_fo_vars   = params;
         fv_so_vars   = names
       } = fv
   in

   (* Subtract the domain *)
   let vars   = SymbolTable.fold (fun vars   v _ -> SymbolSet.remove vars v) vars sub_var in
   let params = SymbolTable.fold (fun params v _ -> SymbolSet.remove params v) params sub_fo_vars in
   let names  = SymbolTable.fold (fun names  v _ -> SymbolSet.remove names v) names sub_so_vars in
   let me     = SymbolTable.fold (fun me     v _ -> SymbolSet.remove me v) me sub_me in
      { fv_vars      = vars;
        fv_me_vars   = me;
        fv_fo_vars   = params;
        fv_so_vars   = names
      }

(*
 * Module name free vars.
 *)
and free_vars_mod_name fv me_name =
   match me_name with
      ModNameEnv me_var
    | ModNameSelf (me_var, _) ->
         free_vars_add_me_var fv me_var
    | ModNameProj (mod_name, _) ->
         free_vars_mod_name fv mod_name
    | ModNameApply (mod_name1, mod_name2) ->
         free_vars_mod_name (free_vars_mod_name fv mod_name1) mod_name2

(*
 * Name free vars.
 *)
and free_vars_var_name fv name =
   match name with
      NameEnv v ->
         free_vars_add_fo_var fv v
    | NamePath (mod_name, _) ->
         free_vars_mod_name fv mod_name

and free_vars_type_name fv name =
   match name with
      TyNameSoVar v ->
         free_vars_add_so_var fv v
    | TyNameSelf (me_var, _) ->
         free_vars_add_me_var fv me_var
    | TyNamePath (mod_name, _) ->
         free_vars_mod_name fv mod_name

and free_vars_type_kind fv { kind_value = name } =
   free_vars_type_name fv name

and free_vars_type_kinds fv ty_kinds =
   List.fold_left free_vars_type_kind fv ty_kinds

and free_vars_me_name fv me_name =
   free_vars_mod_name fv me_name

and free_vars_mt_name fv (mod_name, _) =
   free_vars_mod_name fv mod_name

(************************************************
 * TYPES
 *)
and free_vars_type fv1 ds =
   let fv2 = dest_up free_vars_type_core free_vars_subst ds in
      free_vars_union fv1 fv2

and free_vars_type_core core =
   free_vars_type_aux free_vars_empty core

and free_vars_type_aux fv ty =
   match ty with
      TyInt
    | TyChar
    | TyString
    | TyFloat ->
         fv

    | TyFun (tyl, ty) ->
         free_vars_types (free_vars_type fv ty) tyl
    | TyFunctor (v, t1, t2) ->
         free_vars_type (free_vars_subtract_fo_var (free_vars_type fv t2) v) t1
    | TyProd tyl ->
         free_vars_types fv tyl
    | TyArray ty ->
         free_vars_type fv ty

    | TyAll (vars, ty) ->
         free_vars_subtract_fo_vars (free_vars_type fv ty) vars
    | TyFormat (ty1, ty2, ty3) ->
         free_vars_type (free_vars_type (free_vars_type fv ty3) ty2) ty1
    | TyExternal (ty, _) ->
         free_vars_type fv ty

    | TyVar v ->
         free_vars_add_fo_var fv v
    | TySOVar (v, tyl) ->
         free_vars_add_so_var (free_vars_types fv tyl) v
    | TyProject (ty_name, tyl) ->
         free_vars_type_name (free_vars_types fv tyl) ty_name

    | TyApply  (v, tyl1, tyl2)
    | TyRecord (v, tyl1, tyl2)
    | TyUnion  (v, tyl1, tyl2)
    | TyModule (v, tyl1, tyl2) ->
         free_vars_type_kinds (free_vars_types fv tyl1) tyl2

    | TyOpen (fields, v) ->
         free_vars_add_fo_var (free_vars_ty_open fv fields) v

and free_vars_types fv tyl =
   List.fold_left free_vars_type fv tyl

and free_vars_ty_open fv fields =
   List.fold_left (fun fv (v, ty) ->
         let fv = free_vars_add_var fv v in
         let fv = free_vars_type fv ty in
            fv) fv fields

let free_vars_type_list tyl =
   free_vars_types free_vars_empty tyl

(************************************************
 * TYDEFs
 *)

(*
 * Free variables in a record definition.
 *)
let free_vars_ty_record fv fields =
   FieldTable.fold (fun fv _ (_, ty) ->
         free_vars_type fv ty) fv fields

(*
 * Free variables in a union.
 *)
let free_vars_ty_union fv fields =
   FieldTable.fold (fun fv _ tyl ->
         free_vars_types fv tyl) fv fields

(*
 * Free variables in a module type.
 *)
let free_vars_module_type fv mt =
   let { mt_self     = me_var;
         mt_mt_types = mt_types;
         mt_me_vars  = me_vars;
         mt_types    = types;
         mt_vars     = vars;
         mt_consts   = consts;
         mt_labels   = labels
       } = mt
   in
   let fv =
      SymbolTable.fold (fun fv _ ty ->
            free_vars_type fv ty) fv mt_types
   in
   let fv =
      SymbolTable.fold (fun fv _ ty ->
            free_vars_type fv ty) fv me_vars
   in
   let fv =
      SymbolTable.fold (fun fv _ (ty_vars, ty_opt) ->
            let fv =
               match ty_opt with
                  Some ty ->
                     free_vars_type fv ty
                | None ->
                     fv
            in
               free_vars_subtract_fo_vars fv ty_vars) fv types
   in
   let fv =
      SymbolTable.fold (fun fv _ ty ->
            free_vars_type fv ty) fv vars
   in
   let fv =
      SymbolTable.fold (fun fv _ (_, _, field) ->
            match field with
               ConstNormal ty_kinds ->
                  free_vars_type_kinds fv ty_kinds
             | ConstException tyl ->
                  free_vars_types fv tyl) fv consts
   in
   let fv =
      SymbolTable.fold (fun fv _ (_, _, ty_kinds) ->
            free_vars_type_kinds fv ty_kinds) fv labels
   in
      free_vars_subtract_me_var fv me_var

(*
 * Free vars of a type definition.
 *)
let free_vars_tydef_inner_aux fv tydef =
   match tydef with
      TyDefRecord fields ->
         free_vars_ty_record fv fields
    | TyDefUnion (kind, fields) ->
         free_vars_ty_union fv fields
    | TyDefLambda ty ->
         free_vars_type fv ty
    | TyDefModule mt ->
         free_vars_module_type fv mt

let free_vars_tydef_inner_core tydef =
   free_vars_tydef_inner_aux free_vars_empty tydef

let free_vars_tydef_inner fv1 ds =
   let fv2 = dest_up free_vars_tydef_inner_core free_vars_subst ds in
      free_vars_union fv1 fv2

let free_vars_tydef fv tydef =
   let { tydef_fo_vars = fo_vars;
         tydef_so_vars = so_vars;
         tydef_inner = tydef
       } = tydef
   in
   let fv = free_vars_tydef_inner fv tydef in
   let fv = free_vars_subtract_fo_vars fv fo_vars in
   let fv = free_vars_subtract_so_vars_kind fv so_vars in
      fv

(************************************************
 * EXPRESSIONS
 *)

(*
 * Patterns.
 *)
let rec free_vars_patt fv p =
   match dest_simple_core p with
      PattInt (_, ty) ->
         free_vars_type fv ty
    | PattChar (_, ty) ->
         free_vars_type fv ty
    | PattString (_, ty) ->
         free_vars_type fv ty
    | PattWild ty ->
         free_vars_type fv ty
    | PattVar (v, ty) ->
         free_vars_type (free_vars_subtract_var fv v) ty
    | PattTuple (pl, ty) ->
         free_vars_type (free_vars_patts fv pl) ty
    | PattRecord (fields, ty) ->
         free_vars_type (free_vars_patt_record fv fields) ty
    | PattArray (pl, ty) ->
         free_vars_type (free_vars_patts fv pl) ty
    | PattConst (_, _, pl, ty) ->
         free_vars_type (free_vars_patts fv pl) ty
    | PattChoice (p1, p2, ty) ->
         free_vars_type (free_vars_patt (free_vars_patt fv p2) p1) ty
    | PattAs (p1, p2, ty) ->
         free_vars_type (free_vars_patt (free_vars_patt fv p2) p1) ty
    | PattExpr (e, ty) ->
         free_vars_type (free_vars_exp fv e) ty
    | PattRange (e1, e2, ty) ->
         free_vars_type (free_vars_exp (free_vars_exp fv e2) e1) ty
    | PattWhen (p, e, ty) ->
         free_vars_type (free_vars_patt (free_vars_exp fv e) p) ty
    | PattConstrain (p, ty) ->
         free_vars_type (free_vars_patt fv p) ty

and free_vars_patts fv pl =
   List.fold_left free_vars_patt fv pl

and free_vars_patt_record fv fields =
   List.fold_left (fun fv (_, p) ->
         free_vars_patt fv p) fv fields

(*
 * Expression free vars.
 *)
and free_vars_exp fv1 ds =
   let fv2 = dest_up free_vars_exp_core free_vars_subst ds in
      free_vars_union fv1 fv2

and free_vars_exp_core e =
   free_vars_exp_aux free_vars_empty e

and free_vars_exp_aux fv e =
   match e with
      Number (_, ty) ->
         free_vars_type fv ty
    | Char (_, ty) ->
         free_vars_type fv ty
    | String (_, ty) ->
         free_vars_type fv ty
    | Var (name, ty) ->
         free_vars_var_name (free_vars_type fv ty) name
    | Const (ty_var, const_var, el, ty1, ty2) ->
         free_vars_exps (free_vars_type (free_vars_type fv ty1) ty2) el
    | Apply (e, ty_fun, el, ty_res) ->
         free_vars_exp (free_vars_type (free_vars_exps (free_vars_type fv ty_res) el) ty_fun) e
    | Lambda (ty_vars, patts, e, ty) ->
         free_vars_subtract_fo_vars (free_vars_type (free_vars_patts (free_vars_exp fv e) patts) ty) ty_vars
    | Function (ty_vars, guarded, ty) ->
         free_vars_subtract_fo_vars (free_vars_type (free_vars_guarded fv guarded) ty) ty_vars
    | Let (fields, e, ty) ->
         free_vars_type (free_vars_lets (free_vars_exp fv e) fields) ty
    | LetRec (fields, e, ty) ->
         free_vars_type (free_vars_letrecs (free_vars_exp fv e) fields) ty
    | If (e1, e2, e3, ty) ->
         free_vars_type (free_vars_exp (free_vars_exp (free_vars_exp fv e1) e2) e3) ty
    | For (v, e1, _, e2, e) ->
         free_vars_exp (free_vars_exp (free_vars_subtract_var (free_vars_exp fv e) v) e2) e1
    | While (e1, e2) ->
         free_vars_exp (free_vars_exp fv e2) e1
    | Constrain (e, ty) ->
         free_vars_exp (free_vars_type fv ty) e
    | ApplyType (e, tyl, ty) ->
         free_vars_type (free_vars_types (free_vars_exp fv e) tyl) ty
    | Tuple (el, ty) ->
         free_vars_type (free_vars_exps fv el) ty
    | Array (el, ty) ->
         free_vars_type (free_vars_exps fv el) ty
    | ArraySubscript (e1, e2, ty) ->
         free_vars_type (free_vars_exp (free_vars_exp fv e2) e1) ty
    | ArraySetSubscript (e1, e2, e3) ->
         free_vars_exp (free_vars_exp (free_vars_exp fv e3) e2) e1
    | StringSubscript (e1, e2) ->
         free_vars_exp (free_vars_exp fv e2) e1
    | StringSetSubscript (e1, e2, e3) ->
         free_vars_exp (free_vars_exp (free_vars_exp fv e3) e2) e1
    | Record (fields, ty) ->
         free_vars_type (free_vars_record fv fields) ty
    | RecordProj (e, ty1, label_var, ty2) ->
         free_vars_type (free_vars_type (free_vars_exp fv e) ty1) ty2
    | RecordSetProj (e1, ty, label_var, e2) ->
         free_vars_type (free_vars_exp (free_vars_exp fv e2) e1) ty
    | RecordUpdate (e, fields, ty) ->
         free_vars_type (free_vars_record (free_vars_exp fv e) fields) ty
    | Sequence (e1, e2, ty) ->
         free_vars_type (free_vars_exp (free_vars_exp fv e2) e1) ty
    | Match (e, guarded, ty) ->
         free_vars_type (free_vars_guarded (free_vars_exp fv e) guarded) ty
    | Try (e, guarded, ty) ->
         free_vars_type (free_vars_guarded (free_vars_exp fv e) guarded) ty
    | Raise (e, ty) ->
         free_vars_type (free_vars_exp fv e) ty
    | In (me_name, ty) ->
         free_vars_type (free_vars_me_name fv me_name) ty
    | Out (me_name, ty) ->
         free_vars_type (free_vars_me_name fv me_name) ty
    | Open (me_name, ty) ->
         free_vars_type (free_vars_me_name fv me_name) ty
    | Action (el, ty) ->
         free_vars_type (free_vars_exps fv el) ty
    | Restrict (me_var, mt, e, ty) ->
         free_vars_type (free_vars_type (free_vars_subtract_me_var (free_vars_exp fv e) me_var) mt) ty
    | Ambient (me_var, me, mt) ->
         free_vars_type (free_vars_add_me_var (free_vars_module_exp fv me) me_var) mt
    | Thread (e) ->
         free_vars_exp fv e
    | Migrate (e1, e2, ty) ->
         free_vars_type (free_vars_exp (free_vars_exp fv e2) e1) ty
    | LetModule (me_var, me, e, ty) ->
         free_vars_type (free_vars_subtract_me_var (free_vars_module_exp (free_vars_exp fv e) me) me_var) ty

and free_vars_exps fv el =
   List.fold_left free_vars_exp fv el

(*
 * Guarded command.
 *)
and free_vars_guarded fv cases =
   List.fold_left (fun fv (p, e) ->
         free_vars_patt (free_vars_exp fv e) p) fv cases

(*
 * Record.
 *)
and free_vars_record fv fields =
   List.fold_left (fun fv (_, e) ->
         free_vars_exp fv e) fv fields

(*
 * Let definitions.
 *)
and free_vars_lets fv lets =
   List.fold_left (fun fv (p, e, ty) ->
         free_vars_type (free_vars_exp (free_vars_patt fv p) e) ty) fv lets

(*
 * Let rec definitions.
 *)
and free_vars_letrecs fv fields =
   let fv =
      List.fold_left (fun fv (_, e, _, _) ->
            free_vars_exp fv e) fv fields
   in
   let fv =
      List.fold_left (fun fv (v, _, ty1, ty2) ->
            free_vars_type (free_vars_type (free_vars_subtract_var fv v) ty1) ty2) fv fields
   in
      fv

(*
 * Type definitions.
 *)
and free_vars_type_decls fv fields =
   List.fold_left (fun fv (_, ty_vars, ty_opt) ->
         match ty_opt with
            Some ty ->
               let fv = free_vars_type fv ty in
                  free_vars_subtract_fo_vars fv ty_vars
          | None ->
               fv) fv fields

(************************************************
 * MODULES
 *)
and free_vars_field fv field =
   match dest_simple_core field with
      FieldLet lets ->
         free_vars_lets fv lets
    | FieldLetRec letrecs ->
         free_vars_letrecs fv letrecs
    | FieldType types ->
         free_vars_type_decls fv types
    | FieldExpr e ->
         free_vars_exp fv e
    | FieldRestrict (me_var, mt) ->
         free_vars_type (free_vars_subtract_me_var fv me_var) mt
    | FieldModule (me_var, me, mt) ->
         free_vars_type (free_vars_module_exp (free_vars_subtract_me_var fv me_var) me) mt
    | FieldModuleType (_, mt) ->
         free_vars_type fv mt
    | FieldException (_, _, tyl) ->
         free_vars_types fv tyl
    | FieldExternal (_, ty, _) ->
         free_vars_type fv ty

and free_vars_fields fv fields =
   List.fold_left free_vars_field fv fields

and free_vars_module_exp fv1 ds =
   let fv2 = dest_up free_vars_module_exp_core free_vars_subst ds in
      free_vars_union fv1 fv2

and free_vars_module_exp_core me =
   free_vars_module_exp_aux free_vars_empty me

and free_vars_module_exp_aux fv me =
   match me with
      ModuleExpStruct (me_var, fields, _, mt) ->
         free_vars_subtract_me_var (free_vars_type (free_vars_fields fv fields) mt) me_var
    | ModuleExpVar (me_name, mt) ->
         free_vars_type (free_vars_me_name fv me_name) mt
    | ModuleExpConstrain (me, mt) ->
         free_vars_type (free_vars_module_exp fv me) mt
    | ModuleExpFunctor (me_var, mt1, me, mt2) ->
         free_vars_type (free_vars_type (free_vars_subtract_me_var (free_vars_module_exp fv me) me_var) mt1) mt2
    | ModuleExpApply (me, me_name, mt) ->
         free_vars_type (free_vars_module_exp (free_vars_me_name fv me_name) me) mt

(************************************************
 * PROGRAM
 *)

(*
 * This calculates only the free fo type variables.
 *)
let free_vars_prog prog =
   let { prog_types = types;
         prog_import = import;
         prog_exp = exp
       } = prog
   in
   let fv = free_vars_empty in
   let fv =
      SymbolTable.fold (fun fv _ tydef ->
            free_vars_tydef fv tydef) fv types
   in
   let fv =
      SymbolTable.fold (fun fv _ (_, _, ty) ->
            free_vars_type fv ty) fv import
   in
   let fv =
      match exp with
         Interface ty ->
            free_vars_type fv ty
       | Implementation (me_var, fields, names, ty) ->
            free_vars_subtract_me_var (free_vars_type (free_vars_fields fv fields) ty) me_var
   in
      SymbolSet.to_list fv.fv_fo_vars

(************************************************************************
 * SUBSTITUTION                                                         *
 ************************************************************************)

(*
 * Empty subst.
 *)
let empty_subst =
   { sub_vars    = SymbolTable.empty;
     sub_fo_vars = SymbolTable.empty;
     sub_so_vars = SymbolTable.empty;
     sub_me_vars = SymbolTable.empty;
     sub_loc     = None
   }

let subst_add_loc subst loc =
   { subst with sub_loc = Some loc }

let subst_add_fo_var subst v ty =
   { subst with sub_fo_vars = SymbolTable.add subst.sub_fo_vars v ty }

let subst_add_so_var subst v ty_kind =
   { subst with sub_so_vars = SymbolTable.add subst.sub_so_vars v ty_kind }

let subst_add_me_var subst me_var mod_name =
   { subst with sub_me_vars = SymbolTable.add subst.sub_me_vars me_var mod_name }

let subst_mem_fo_var subst v =
   SymbolTable.mem subst.sub_fo_vars v

(*
 * Combine two substitions.
 *)
let subst_union sub1 sub2 =
   let { sub_vars      = sub_var1;
         sub_fo_vars   = sub_params1;
         sub_so_vars   = sub_names1;
         sub_me_vars   = sub_me_vars1;
         sub_loc       = loc
       } = sub1
   in
   let { sub_vars      = sub_var2;
         sub_fo_vars   = sub_params2;
         sub_so_vars   = sub_names2;
         sub_me_vars   = sub_me_vars2
       } = sub2
   in
      { sub_vars      = SymbolTable.union sub_var1 sub_var2;
        sub_fo_vars   = SymbolTable.union sub_params1 sub_params2;
        sub_so_vars   = SymbolTable.union sub_names1 sub_names2;
        sub_me_vars   = SymbolTable.union sub_me_vars1 sub_me_vars2;
        sub_loc       = loc
      }

(************************************************
 * NAMES
 *)
let rec subst_mod_name subst name =
   match name with
      ModNameEnv me_var ->
         (try SymbolTable.find subst.sub_me_vars me_var with
             Not_found ->
                name)
    | ModNameSelf (me_var, ext_var) ->
         (try ModNameProj (SymbolTable.find subst.sub_me_vars me_var, ext_var) with
             Not_found ->
                name)
    | ModNameProj (mod_name, ext_var) ->
         ModNameProj (subst_mod_name subst mod_name, ext_var)
    | ModNameApply (mod_name1, mod_name2) ->
         let mod_name1 = subst_mod_name subst mod_name1 in
         let mod_name2 = subst_mod_name subst mod_name2 in
            ModNameApply (mod_name1, mod_name2)

let subst_var_name subst name =
   match name with
      NameEnv v ->
         (try
             let mod_name, ext_var = SymbolTable.find subst.sub_vars v in
                NamePath (mod_name, ext_var)
          with
             Not_found ->
                name)
    | NamePath (mod_name, ext_var) ->
         NamePath (subst_mod_name subst mod_name, ext_var)

let subst_name subst name =
   match name with
      NameEnv v ->
         name
    | NamePath (mod_name, ext_var) ->
         NamePath (subst_mod_name subst mod_name, ext_var)

let subst_const_name = subst_name
let subst_label_name = subst_name
let subst_me_name = subst_mod_name

let rec subst_type_name subst name =
   match name with
      TyNameSoVar v ->
         (try
             let ty_name = SymbolTable.find subst.sub_so_vars v in
                subst_type_name subst ty_name.kind_value
          with
             Not_found ->
                name)
    | TyNameSelf (me_var, ext_var) ->
         name
    | TyNamePath (mod_name, ext_var) ->
         TyNamePath (subst_mod_name subst mod_name, ext_var)

let rec subst_type_kind subst kind =
   match kind.kind_value with
      TyNameSoVar v ->
         (try
             let ty_kind = SymbolTable.find subst.sub_so_vars v in
                subst_type_kind subst ty_kind
          with
             Not_found ->
                kind)
    | TyNameSelf (me_var, ext_var) ->
         (try { kind with kind_value = TyNamePath (SymbolTable.find subst.sub_me_vars me_var, ext_var) } with
             Not_found ->
                kind)
    | TyNamePath (mod_name, ext_var) ->
         { kind with kind_value = TyNamePath (subst_mod_name subst mod_name, ext_var) }

let subst_type_kinds subst ty_kinds =
   List.map (subst_type_kind subst) ty_kinds

let subst_mt_name subst (mod_name, ext_var) =
   subst_mod_name subst mod_name, ext_var

let subst_subtract_fo_var subst v =
   { subst with sub_fo_vars = SymbolTable.remove subst.sub_fo_vars v }

let subst_subtract_fo_vars subst vars =
   let rec subtract subst = function
      v :: vars ->
         subtract (SymbolTable.remove subst v) vars
    | [] ->
         subst
   in
      { subst with sub_fo_vars = subtract subst.sub_fo_vars vars }

let subst_subtract_so_vars subst vars =
   let rec subtract subst = function
      { kind_value = v } :: vars ->
         subtract (SymbolTable.remove subst v) vars
    | [] ->
         subst
   in
      { subst with sub_so_vars = subtract subst.sub_so_vars vars }

let subst_subtract_me_var subst v =
   { subst with sub_me_vars = SymbolTable.remove subst.sub_me_vars v }

let subst_subtract_var subst v =
   { subst with sub_vars = SymbolTable.remove subst.sub_vars v }

(************************************************
 * TYPES
 *)
let rec dest_type_core ds =
   dest_core subst_type_core ds

and subst_type subst ds =
   apply subst_union subst ds

and subst_types subst tyl =
   List.map (subst_type subst) tyl

and subst_type_core subst ty =
   match ty with
      TyInt
    | TyChar
    | TyString
    | TyFloat ->
         ty

    | TyFun (tyl, ty) ->
         TyFun (subst_types subst tyl, subst_type subst ty)
    | TyFunctor (v, t1, t2) ->
         let t1 = subst_type subst t1 in
         let subst = subst_subtract_fo_var subst v in
         let t2 = subst_type subst t2 in
            TyFunctor (v, t1, t2)
    | TyProd tyl ->
         TyProd (subst_types subst tyl)
    | TyArray ty ->
         TyArray (subst_type subst ty)
    | TyAll (vars, ty) ->
         let subst = subst_subtract_fo_vars subst vars in
            TyAll (vars, subst_type subst ty)

    | TyVar v ->
         (try dest_type_core (subst_type (subst_subtract_fo_var subst v) (SymbolTable.find subst.sub_fo_vars v)) with
             Not_found ->
                ty)
    | TySOVar (v, tyl) ->
         let tyl = subst_types subst tyl in
            (try
                let { kind_value = name } = SymbolTable.find subst.sub_so_vars v in
                   TyProject (name, tyl)
             with
                Not_found ->
                   TySOVar (v, tyl))

    | TyProject (ty_name, tyl) ->
         TyProject (subst_type_name subst ty_name, subst_types subst tyl)

    | TyApply (v, tyl1, tyl2) ->
         TyApply (v, subst_types subst tyl1, subst_type_kinds subst tyl2)
    | TyRecord (v, tyl1, tyl2) ->
         TyRecord (v, subst_types subst tyl1, subst_type_kinds subst tyl2)
    | TyUnion (v, tyl1, tyl2) ->
         TyUnion (v, subst_types subst tyl1, subst_type_kinds subst tyl2)
    | TyModule (v, tyl1, tyl2) ->
         TyModule (v, subst_types subst tyl1, subst_type_kinds subst tyl2)

    | TyFormat (t1, t2, t3) ->
         TyFormat (subst_type subst t1, subst_type subst t2, subst_type subst t3)
    | TyExternal (ty, s) ->
         TyExternal (subst_type subst ty, s)
    | TyOpen (fields, v) ->
         TyOpen (subst_type_open subst fields, v)

and subst_type_open subst = function
   (v, ty) :: fields ->
      (v, subst_type subst ty) :: subst_type_open subst fields
 | [] ->
      []

(************************************************
 * TYPE DEFINITIONS
 *)

(*
 * Substitute into a record type.
 *)
let subst_type_record subst fields =
   FieldTable.map (fun (mflag, ty) ->
         mflag, subst_type subst ty) fields

(*
 * Subst into a union type.
 *)
let subst_type_union subst fields =
   FieldTable.map (subst_types subst) fields

(*
 * Constructor field.
 *)
let subst_const_field subst field =
   match field with
      ConstNormal ty_kinds ->
         ConstNormal (subst_type_kinds subst ty_kinds)
    | ConstException tyl ->
         ConstException (subst_types subst tyl)

(*
 * Substitute into a module type.
 *)
let subst_module_type subst mt =
   let { mt_self = me_var;
         mt_mt_types = mt_types;
         mt_me_vars = me_vars;
         mt_types = types;
         mt_vars = vars;
         mt_consts = consts;
         mt_labels = labels
       } = mt
   in
   let subst = subst_subtract_me_var subst me_var in
   let mt_types = SymbolTable.map (subst_type subst) mt_types in
   let me_vars = SymbolTable.map (subst_type subst) me_vars in
   let types =
      SymbolTable.map (fun (ty_vars, ty_opt) ->
            let subst = subst_subtract_fo_vars subst ty_vars in
            let ty_opt =
               match ty_opt with
                  Some ty ->
                     Some (subst_type subst ty)
                | None ->
                     None
            in
               ty_vars, ty_opt) types
   in
   let vars = SymbolTable.map (subst_type subst) vars in
   let consts =
      SymbolTable.map (fun (ty_var, arity, field) ->
            let field = subst_const_field subst field in
               ty_var, arity, field) consts
   in
   let labels =
      SymbolTable.map (fun (ty_var, arity, ty_kinds) ->
            let ty_kinds = subst_type_kinds subst ty_kinds in
               ty_var, arity, ty_kinds) labels
   in
      { mt with mt_mt_types = mt_types;
                mt_me_vars = me_vars;
                mt_types = types;
                mt_vars = vars;
                mt_consts = consts;
                mt_labels = labels
      }

(*
 * Substitute into a typ definition.
 *)
let subst_tydef_inner_core subst tydef =
   match tydef with
      TyDefRecord fields ->
         TyDefRecord (subst_type_record subst fields)
    | TyDefUnion (kind, fields) ->
         TyDefUnion (kind, subst_type_union subst fields)
    | TyDefLambda ty ->
         TyDefLambda (subst_type subst ty)
    | TyDefModule mt ->
         TyDefModule (subst_module_type subst mt)

let subst_tydef_inner subst (ds : tydef_inner) =
   apply subst_union subst ds

let dest_tydef_inner_core (ds : tydef_inner) =
   dest_core subst_tydef_inner_core ds

let subst_tydef subst tydef =
   let { tydef_fo_vars = params;
         tydef_so_vars = names;
         tydef_inner = tydef
       } = tydef
   in
   let subst = subst_subtract_fo_vars subst params in
   let subst = subst_subtract_so_vars subst names in
   let tydef = subst_tydef_inner subst tydef in
      { tydef_fo_vars = params;
        tydef_so_vars = names;
        tydef_inner = tydef
      }

(************************************************
 * EXPRESSIONS
 *)
let rec dest_exp_core ds =
   dest_core subst_exp_core ds

and subst_exp subst ds =
   apply subst_union subst ds

and subst_exp_core subst exp =
   match exp with
      Number (i, ty) ->
         Number (i, subst_type subst ty)
    | Char (c, ty) ->
         Char (c, subst_type subst ty)
    | String (s, ty) ->
         String (s, subst_type subst ty)
    | Var (var_name, ty) ->
         Var (subst_var_name subst var_name, subst_type subst ty)
    | Const (ty_var, const_var, el, ty1, ty2) ->
         Const (ty_var, const_var, List.map (subst_exp subst) el, subst_type subst ty1, subst_type subst ty2)
    | Apply (e, ty_fun, el, ty_res) ->
         Apply (subst_exp subst e, subst_type subst ty_fun, List.map (subst_exp subst) el, subst_type subst ty_res)
    | Lambda (ty_vars, pl, e, ty) ->
         let subst = subst_subtract_fo_vars subst ty_vars in
         let pl, subst' = subst_patts subst pl in
            Lambda (ty_vars, pl, subst_exp subst' e, subst_type subst ty)
    | Function (ty_vars, guarded, ty) ->
         let subst = subst_subtract_fo_vars subst ty_vars in
            Function (ty_vars, subst_guarded subst guarded, subst_type subst ty)
    | Let (lets, e, ty) ->
         subst_lets subst lets e ty
    | LetRec (letrecs, e, ty) ->
         subst_letrecs subst letrecs e ty
    | If (e1, e2, e3, ty) ->
         If (subst_exp subst e1, subst_exp subst e2, subst_exp subst e3, subst_type subst ty)
    | For (v, e1, flag, e2, e3) ->
         let sub' = subst_subtract_var subst v in
            For (v, subst_exp subst e1, flag, subst_exp subst e2, subst_exp sub' e3)
    | While (e1, e2) ->
         While (subst_exp subst e1, subst_exp subst e2)
    | Constrain (e, ty) ->
         Constrain (subst_exp subst e, subst_type subst ty)
    | ApplyType (e, tyl, ty) ->
         ApplyType (subst_exp subst e, List.map (subst_type subst) tyl, subst_type subst ty)
    | Tuple (el, ty) ->
         Tuple (List.map (subst_exp subst) el, subst_type subst ty)
    | Array (el, ty) ->
         Array (List.map (subst_exp subst) el, subst_type subst ty)
    | ArraySubscript (e1, e2, ty) ->
         ArraySubscript (subst_exp subst e1, subst_exp subst e2, subst_type subst ty)
    | ArraySetSubscript (e1, e2, e3) ->
         ArraySetSubscript (subst_exp subst e1, subst_exp subst e2, subst_exp subst e3)
    | StringSubscript (e1, e2) ->
         StringSubscript (subst_exp subst e1, subst_exp subst e2)
    | StringSetSubscript (e1, e2, e3) ->
         StringSetSubscript (subst_exp subst e1, subst_exp subst e2, subst_exp subst e3)
    | Record (fields, ty) ->
         Record (subst_record subst fields, subst_type subst ty)
    | RecordProj (e, ty1, label_var, ty2) ->
         RecordProj (subst_exp subst e, subst_type subst ty1, label_var, subst_type subst ty2)
    | RecordSetProj (e1, ty, label_var, e2) ->
         RecordSetProj (subst_exp subst e1, subst_type subst ty, label_var, subst_exp subst e2)
    | RecordUpdate (e, fields, ty) ->
         RecordUpdate (subst_exp subst e, subst_record subst fields, subst_type subst ty)
    | Sequence (e1, e2, ty) ->
         Sequence (subst_exp subst e1, subst_exp subst e2, subst_type subst ty)
    | Match (e, guarded, ty) ->
         Match (subst_exp subst e, subst_guarded subst guarded, subst_type subst ty)
    | Try (e, guarded, ty) ->
         Try (subst_exp subst e, subst_guarded subst guarded, subst_type subst ty)
    | Raise (e, ty) ->
         Raise (subst_exp subst e, subst_type subst ty)
    | In (me_name, ty) ->
         In (subst_me_name subst me_name, subst_type subst ty)
    | Out (me_name, ty) ->
         Out (subst_me_name subst me_name, subst_type subst ty)
    | Open (me_name, ty) ->
         Open (subst_me_name subst me_name, subst_type subst ty)
    | Action (el, ty) ->
         Action (List.map (subst_exp subst) el, subst_type subst ty)
    | Restrict (me_var, trans, e, ty) ->
         let sub' = subst_subtract_me_var subst me_var in
            Restrict (me_var, subst_type subst trans, subst_exp subst e, subst_type subst ty)
    | Ambient (me_var, me, trans) ->
         Ambient (me_var, subst_module_exp subst me, subst_type subst trans)
    | Thread (e) ->
         Thread (subst_exp subst e)
    | Migrate (e1, e2, ty) ->
         Migrate (subst_exp subst e1, subst_exp subst e2, subst_type subst ty)
    | LetModule (me_var, me, e, ty) ->
         let sub' = subst_subtract_me_var subst me_var in
            LetModule (me_var, subst_module_exp subst me, subst_exp sub' e, subst_type subst ty)

and subst_record subst fields =
   List.map (fun (label, e) -> label, subst_exp subst e) fields

and subst_lets' subst sub' = function
   (p, e, ty) :: fields ->
      let p, sub' = subst_patt sub' p in
      let fields, sub' = subst_lets' subst sub' fields in
         (p, subst_exp subst e, subst_type subst ty) :: fields, sub'
 | [] ->
      [], sub'

and subst_lets subst lets e ty =
   let lets, subst' = subst_lets' subst subst lets in
      Let (lets, subst_exp subst' e, subst_type subst ty)

and subst_letrecs' subst fields =
   let subst =
      List.fold_left (fun subst (v, _, _, _) -> subst_subtract_var subst v) subst fields
   in
   let fields =
      List.map (fun (v, e, ty1, ty2) -> v, subst_exp subst e, subst_type subst ty1, subst_type subst ty2) fields
   in
      fields, subst

and subst_letrecs subst letrecs e ty =
   let letrecs, sub' = subst_letrecs' subst letrecs in
      LetRec (letrecs, subst_exp sub' e, subst_type subst ty)

and subst_types subst types =
   let types =
      List.map (fun (ext_var, ty_vars, ty_opt) ->
            let ty_opt =
               match ty_opt with
                  Some ty ->
                     let subst = subst_subtract_fo_vars subst ty_vars in
                        Some (subst_type subst ty)
                | None ->
                     None
            in
               ext_var, ty_vars, ty_opt) types
   in
      types, subst

and subst_guarded subst guarded =
   List.map (fun (p, e) ->
         let p, sub' = subst_patt subst p in
            p, subst_exp sub' e) guarded

(*
 * Patterns.
 *)
and dest_patt_core patt =
   dest_simple_core patt

and subst_patt subst patt =
   let loc = label_of_simple_term patt in
   let patt = dest_patt_core patt in
   let patt, subst = subst_patt_core subst patt in
   let patt = wrap_simple_core loc patt in
      patt, subst

and subst_patt_core subst patt =
   match patt with
      PattInt (i, ty) ->
         PattInt (i, subst_type subst ty), subst
    | PattChar (c, ty) ->
         PattChar (c, subst_type subst ty), subst
    | PattString (s, ty) ->
         PattString (s, subst_type subst ty), subst
    | PattWild ty ->
         PattWild (subst_type subst ty), subst
    | PattVar (v, ty) ->
         let sub' = subst_subtract_var subst v in
            PattVar (v, subst_type subst ty), sub'
    | PattTuple (pl, ty) ->
         let pl, sub' = subst_patts subst pl in
            PattTuple (pl, subst_type subst ty), sub'
    | PattRecord (fields, ty) ->
         let fields, sub' = subst_patt_record subst fields in
            PattRecord (fields, subst_type subst ty), sub'
    | PattArray (pl, ty) ->
         let pl, sub' = subst_patts subst pl in
            PattArray (pl, subst_type subst ty), sub'
    | PattConst (const_name, const_var, pl, ty) ->
         let pl, sub' = subst_patts subst pl in
            PattConst (const_name, const_var, pl, subst_type subst ty), sub'
    | PattChoice (p1, p2, ty) ->
         let p1, sub' = subst_patt subst p1 in
         let p2, sub' = subst_patt sub' p2 in
            PattChoice (p1, p2, subst_type subst ty), sub'
    | PattAs (p1, p2, ty) ->
         let p1, sub' = subst_patt subst p1 in
         let p2, sub' = subst_patt sub' p2 in
            PattAs (p1, p2, subst_type subst ty), sub'
    | PattExpr (e, ty) ->
         PattExpr (subst_exp subst e, subst_type subst ty), subst
    | PattRange (e1, e2, ty) ->
         PattRange (subst_exp subst e1, subst_exp subst e2, subst_type subst ty), subst
    | PattWhen (p, e, ty) ->
         let p, sub' = subst_patt subst p in
            PattWhen (p, subst_exp sub' e, subst_type subst ty), sub'
    | PattConstrain (p, ty) ->
         let p, sub' = subst_patt subst p in
            PattConstrain (p, subst_type subst ty), sub'

and subst_patts subst = function
   patt :: patts ->
      let patt, subst = subst_patt subst patt in
      let patts, subst = subst_patts subst patts in
         patt :: patts, subst
 | [] ->
      [], subst

and subst_patt_record subst = function
   (label, patt) :: fields ->
      let patt, sub' = subst_patt subst patt in
      let fields, sub' = subst_patt_record subst fields in
         (label, patt) :: fields, sub'
 | [] ->
      [], subst

(************************************************
 * MODULE EXPRESSIONS
 *)
and dest_module_exp_core ds =
   dest_core subst_module_exp_core ds

and subst_module_exp subst ds =
   apply subst_union subst ds

and subst_module_exp_core subst me =
   match me with
      ModuleExpStruct (me_var, fields, names, trans) ->
         let subst' = subst_subtract_me_var subst me_var in
         let fields, _ = subst_fields subst' fields in
            ModuleExpStruct (me_var, fields, names, subst_type subst trans)
    | ModuleExpVar (me_name, trans) ->
         ModuleExpVar (subst_me_name subst me_name, subst_type subst trans)
    | ModuleExpConstrain (me, mt) ->
         ModuleExpConstrain (subst_module_exp subst me, subst_type subst mt)
    | ModuleExpFunctor (me_var, trans1, me, trans2) ->
         let sub' = subst_subtract_me_var subst me_var in
            ModuleExpFunctor (me_var, subst_type subst trans1, subst_module_exp sub' me, subst_type subst trans2)
    | ModuleExpApply (me, me_name, trans_type) ->
         ModuleExpApply (subst_module_exp subst me, subst_me_name subst me_name, subst_type subst trans_type)

and dest_field_core ds =
   dest_simple_core ds

and subst_fields subst = function
   field :: fields ->
      let field, subst = subst_field subst field in
      let fields, subst = subst_fields subst fields in
         field :: fields, subst
 | [] ->
      [], subst

and subst_field subst field =
   let loc = label_of_simple_term field in
   let field = dest_field_core field in
   let field, subst = subst_field_core subst field in
   let field = wrap_simple_core loc field in
      field, subst

and subst_field_core subst field =
   match field with
      FieldLet lets ->
         let lets, subst = subst_lets' subst subst lets in
            FieldLet lets, subst
    | FieldLetRec letrecs ->
         let letrecs, subst = subst_letrecs' subst letrecs in
            FieldLetRec letrecs, subst
    | FieldType types ->
         let types, subst = subst_types subst types in
            FieldType types, subst
    | FieldExpr e ->
         FieldExpr (subst_exp subst e), subst
    | FieldRestrict (me_var, trans) ->
         let sub' = subst_subtract_me_var subst me_var in
            FieldRestrict (me_var, subst_type subst trans), sub'
    | FieldModule (me_var, me, trans) ->
         FieldModule (me_var, subst_module_exp subst me, subst_type subst trans), subst
    | FieldModuleType (v, trans) ->
         FieldModuleType (v, subst_type subst trans), subst
    | FieldException (const_var, ty_var, tyl) ->
         FieldException (const_var, ty_var, List.map (subst_type subst) tyl), subst
    | FieldExternal (v, ty, s) ->
         FieldExternal (v, subst_type subst ty, s), subst

(*
 * Substitute in the program exp.
 *)
let subst_prog_exp subst exp =
   match exp with
      Interface ty ->
         Interface (subst_type subst ty)
    | Implementation (me_var, fields, names, ty) ->
         let ty = subst_type subst ty in
         let subst = subst_subtract_me_var subst me_var in
         let fields, _ = subst_fields subst fields in
            Implementation (me_var, fields, names, ty)

(*
 * Substitution for an entire program.
 *)
let subst_prog subst prog =
   let { prog_types  = types;
         prog_import = import;
         prog_exp    = exp
       } = prog
   in
   let types = SymbolTable.map (subst_tydef subst) types in
   let import =
      SymbolTable.map (fun (hash, me_var, ty) ->
            hash, me_var, subst_type subst ty) import
   in
   let exp = subst_prog_exp subst exp in
      { prog with prog_types  = types;
                  prog_import = import;
                  prog_exp    = exp
      }

(************************************************************************
 * GLOBAL FUNCTIONS                                                     *
 ************************************************************************)

let free_vars_type =
   free_vars_type free_vars_empty

let free_type_vars ty =
   let { fv_fo_vars = fo_vars } = free_vars_type ty in
      SymbolSet.to_list fo_vars

let free_vars_tydef_inner =
   free_vars_tydef_inner free_vars_empty

let free_vars_tydef =
   free_vars_tydef free_vars_empty

let free_vars_exp =
   free_vars_exp free_vars_empty

let free_vars_module_exp =
   free_vars_module_exp free_vars_empty

(*
 * Wrap the core.
 *)
let make_type            = wrap_core
let make_tydef_inner     = wrap_core
let make_exp             = wrap_core
let make_patt            = wrap_simple_core
let make_field           = wrap_simple_core
let make_module_exp      = wrap_core

(*
 * Locations.
 *)
let loc_of_type          = label_of_term
let loc_of_tydef_inner   = label_of_term
let loc_of_exp           = label_of_term
let loc_of_patt          = label_of_simple_term
let loc_of_field         = label_of_simple_term
let loc_of_module_exp    = label_of_term

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
