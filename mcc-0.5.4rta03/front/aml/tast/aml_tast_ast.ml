(*
 * Convert the AST code to TAST code.
 *
 * This _doesn't_ do type inference.  In many cases an expression
 * is just given a type variable.
 *
 * The main issues here:
 *    1. Convert AST to TAST
 *
 *       The languages are similar, so there is not much in the way
 *       of structural changes that have to be performed.
 *
 *    2. Convert AST names to internal names.
 *
 *       Essentially, every name is translated as we process
 *       the file.  The hard part is including the signatures
 *       of other files.  The names have to be translated when
 *       the interface file is loaded.
 *
 *    3. Lift type definitions to top-level.
 *
 *       All module types, union types, etc. are moved to
 *       top-level definitions.  The types have to be abstracted
 *       when this happens: the top-level definitions are not allowed
 *       to contain variables, so for instance ModuleName.t is
 *       not allowed in a top level definition.  To solve this
 *       we create the type as a Lambda type with type variables
 *       in place of the free type names (esentially closure
 *       conversion on the types).
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Field_table
open Interval_set
open Location

open Fir_state

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env
open Aml_tast_util
open Aml_tast_type
open Aml_tast_print
open Aml_tast_abstract
open Aml_tast_standardize

let debug_tast =
   create_debug (**)
      { debug_name = "tast";
        debug_description = "print results of typed AST";
        debug_value = false
      }

module Pos = MakePos (struct let name = "Aml_tast_ast" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Build new type arguments from the var list.
 *)
let new_fo_type_vars arity =
   let rec collect vars i =
      if i = 0 then
         vars
      else
         collect (new_symbol_string "a" :: vars) (pred i)
   in
      collect [] arity

let new_fo_type_args loc arity =
   let rec collect args i =
      if i = 0 then
         args
      else
         collect (make_type loc (TyVar (new_symbol_string "arg")) :: args) (pred i)
   in
      collect [] arity

(*
 * Define a null-record type.
 *)
let null_record_loc = bogus_loc "<null_record>"

let null_record_var = new_symbol_string "null_record"

let genv_add_null_record genv =
   let inner = make_tydef_inner null_record_loc (TyDefRecord FieldTable.empty) in
   let tydef =
      { tydef_fo_vars = [];
        tydef_so_vars = [];
        tydef_inner = inner
      }
   in
      genv_add_tydef genv null_record_var tydef

(*
 * Get the fields of a tuple.
 *)
let rec dest_tuple arg =
   match arg with
      Aml_ast.Wrap (arg, _) ->
         dest_tuple arg
    | Aml_ast.Tuple (args, _) ->
         args
    | _ ->
         [arg]

(*
 * Make up a new function type from the argument list.
 *)
let type_of_lambda args e loc =
   let tyl = List.map type_of_pattern args in
   let ty = type_of_exp e in
      make_type loc (TyFun (tyl, ty))

(*
 * Convert the pattern to an expression.
 *)
let rec exp_of_patt pos p =
   let loc = loc_of_patt p in
      match dest_patt_core p with
         PattInt (i, ty) ->
            make_exp loc (Number (i, ty))
       | PattChar (c, ty) ->
            make_exp loc (Char (c, ty))
       | PattString (s, ty) ->
            make_exp loc (String (s, ty))
       | PattVar (v, ty) ->
            make_exp loc (Var (NameEnv v, ty))
       | PattTuple (pl, ty) ->
            make_exp loc (Tuple (List.map (exp_of_patt pos) pl, ty))
       | PattRecord (fields, ty) ->
            make_exp loc (Record (List.map (fun (label, p) -> label, exp_of_patt pos p) fields, ty))
       | PattArray (pl, ty) ->
            make_exp loc (Array (List.map (exp_of_patt pos) pl, ty))
       | PattConst (ty_var, const_var, pl, ty) ->
            make_exp loc (Const (ty_var, const_var, List.map (exp_of_patt pos) pl, ty, ty))
       | PattExpr (e, ty) ->
            e
       | PattConstrain (p, _) ->
            exp_of_patt pos p
       | PattWild _
       | PattChoice _
       | PattAs _
       | PattRange _
       | PattWhen _ ->
            raise (TAstException (pos, StringPattError ("not an expression", p)))

(************************************************************************
 * ABSTRACT TYPES
 ************************************************************************)

(*
 * Convert a type definition to abstract form.
 *)
let genv_add_abstract_tydef genv ty_var vars tydef loc =
   let so_vars, so_args, inner = build_abstract_tydef_inner_core genv tydef in
   let inner = make_tydef_inner loc inner in
   let tydef =
      { tydef_fo_vars = vars;
        tydef_so_vars = so_vars;
        tydef_inner   = inner
      }
   in
   let fo_args = [] in
   let genv = genv_add_tydef genv ty_var tydef in
      genv, fo_args, so_args, tydef

(************************************************************************
 * VARIABLE ENVIRONMENT
 ************************************************************************)

type venv = (var * ty) SymbolTable.t

let venv_empty = SymbolTable.empty

let venv_add_var venv v v' ty =
   SymbolTable.add venv v (v', ty)

let venv_lookup_var = SymbolTable.find

let venv_union = SymbolTable.union

(************************************************************************
 * TYPES                                                                *
 ************************************************************************)

(*
 * Convert a type definition.
 *)
let rec build_type genv venv ty =
   let pos = string_pos "build_type" (ast_type_pos ty) in
      match ty with
         Aml_ast.TyFun (t1, t2, loc) ->
            let genv, t1 = build_type genv venv t1 in
            let genv, t2 = build_type genv venv t2 in
            let ty = make_type loc (TyFun ([t1], t2)) in
               genv, ty
       | Aml_ast.TyProd (tyl, loc) ->
            let genv, tyl = build_types genv venv pos tyl in
            let ty = make_type loc (TyProd tyl) in
               genv, ty
       | Aml_ast.TyRecord (fields, loc) ->
            (* Nested records are not polymorphic *)
            let ty_var = new_symbol_string "record" in
               build_record_type genv venv pos ty_var [] fields loc
       | Aml_ast.TyUnion (fields, loc) ->
            (* Nested unions are not polymorphic *)
            let ty_var = new_symbol_string "union" in
               build_union_type genv venv pos ty_var [] fields loc
       | Aml_ast.TyArray (ty, loc) ->
            let genv, ty = build_type genv venv ty in
            let ty = make_type loc (TyArray ty) in
               genv, ty
       | Aml_ast.TyApply (name, args, loc) ->
            build_project_type genv venv pos name args loc
       | Aml_ast.TyVar (v, loc) ->
            let ty = make_type loc (TyVar v) in
               genv, ty
       | Aml_ast.TyAbstract (v, loc) ->
            raise (Failure "Abstract type")

(*
 * List of types.
 *)
and build_types genv venv pos tyl =
   let genv, tyl =
      List.fold_left (fun (genv, tyl) ty ->
            let genv, ty = build_type genv venv ty in
               genv, ty :: tyl) (genv, []) tyl
   in
      genv, List.rev tyl

(*
 * Check arity of application.
 *)
and build_project_type genv venv pos name ty_args loc =
   let pos = string_pos "build_project_type" pos in
   let genv, ty_args = build_types genv venv pos ty_args in
   let genv, ty_name = genv_intern_type genv pos name in
   let ty = make_type loc (TyProject (ty_name, ty_args)) in
      genv, ty

(*
 * Record type.
 * Add the actual record type as a type definition,
 * the return a TyRecord that references the definition.
 *)
and build_record_type genv venv pos ty_var fo_vars fields loc =
   let pos = string_pos "build_record_type" pos in

   (* Build the type *)
   let fields, lenv, genv =
     List.fold_left (fun (fields, lenv, genv) (mflag, label, ty) ->
            let genv, ty = build_type genv venv ty in
            let label' = new_symbol label in
            let lenv = (label, label') :: lenv in
            let fields = FieldTable.add fields label' (mflag, ty) in
               fields, lenv, genv) (FieldTable.empty, [], genv) fields
   in

   (* Add the definition *)
   let tydef = TyDefRecord fields in
   let genv, fo_args1, so_args, tydef = genv_add_abstract_tydef genv ty_var fo_vars tydef loc in
   let fo_args2 = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let fo_args = fo_args1 @ fo_args2 in
   let ty = make_type loc (TyRecord (ty_var, fo_args, so_args)) in

   (* Add the labels *)
   let arity = List.length fo_args in
   let genv =
      List.fold_left (fun genv (label, label') ->
            genv_add_label genv label label' ty_var arity so_args) genv lenv
   in
      genv, ty

(*
 * Union type.
 *)
and build_union_type genv venv pos ty_var vars fields loc =
   let pos = string_pos "build_union_type" pos in

   (* Build the fields *)
   let fields, lenv, genv =
      List.fold_left (fun (fields, lenv, genv) (label, tyl) ->
            let genv, tyl = build_types genv venv pos tyl in
            let label' = new_symbol label in
            let lenv = (label, label') :: lenv in
            let fields = FieldTable.add fields label' tyl in
               fields, lenv, genv) (FieldTable.empty, [], genv) fields
   in

   (* Build the type definition *)
   let tydef = TyDefUnion (NormalUnion, fields) in
   let genv, fo_args1, so_args, tydef = genv_add_abstract_tydef genv ty_var vars tydef loc in
   let fo_args2 = List.map (fun v -> make_type loc (TyVar v)) vars in
   let fo_args = fo_args1 @ fo_args2 in
   let ty = make_type loc (TyUnion (ty_var, fo_args, so_args)) in

   (* Add the labels *)
   let arity = List.length fo_args in
   let genv =
      List.fold_left (fun genv (label, label') ->
            genv_add_const genv label label' ty_var arity so_args) genv lenv
   in
      genv, ty

(*
 * Lambda type is just an abstracted type definition.
 *)
let build_lambda_type genv venv pos ty_var vars ty loc =
   let pos = string_pos "build_lambda_type" pos in
   let genv, ty = build_type genv venv ty in
   let tydef = TyDefLambda ty in
   let genv, fo_args1, so_args, tydef = genv_add_abstract_tydef genv ty_var vars tydef loc in
   let fo_args2 = List.map (fun v -> make_type loc (TyVar v)) vars in
   let ty = make_type loc (TyApply (ty_var, fo_args1 @ fo_args2, so_args)) in
      genv, ty

(*
 * For type definitions, we do the following.
 * For each type that is defined, add a new abstracted
 * type definition to the environment, and add a new type
 * definition to the module.
 *
 * build_top_type creates the new type definition.
 * We add a top-level abstracted type definition.
 * Occurences of this type will use the application form.
 *)
let build_top_type genv venv pos vars ty_var ty_var' ty loc =
   let genv, ty =
      match ty with
         Aml_ast.TyRecord (fields, loc) ->
            build_record_type genv venv pos ty_var' vars fields loc
       | Aml_ast.TyUnion (fields, loc) ->
            build_union_type genv venv pos ty_var' vars fields loc
       | _ ->
            build_lambda_type genv venv pos ty_var' vars ty loc
   in
   let genv = genv_add_type genv ty_var ty_var' vars (Some ty) in
      genv, ty

(*
 * Check the type declaration.
 *)
let build_top_types genv venv types =
   (* Add the type names to allow for recursive calls *)
   let genv, types =
      List.fold_left (fun (genv, types) (v, vars, ty, loc) ->
            let v' = new_symbol v in
            let types = (v, v', vars, ty, loc) :: types in
            let genv = genv_add_type_name genv v v' in
               genv, types) (genv, []) types
   in
   let types = List.rev types in

   (* Now actually compile the types *)
   let genv, types =
      List.fold_left (fun (genv, types) (v, v', vars, ty, loc) ->
            let pos = string_pos "build_top_types" (var_pos v (ast_type_pos ty)) in
            let genv, ty = build_top_type genv venv pos vars v v' ty loc in
               genv, (v', vars, Some ty) :: types) (genv, []) types
   in
   let types = List.rev types in
      genv, types

(************************************************************************
 * PATTERNS                                                             *
 ************************************************************************)

(*
 * Convert a pattern.
 *)
let rec build_pattern genv venv patt =
   let pos = string_pos "build_pattern" (ast_patt_pos patt) in
      match patt with
         Aml_ast.PattInt (i, loc) ->
            let ty = make_type loc TyInt in
            let patt = make_patt loc (PattInt (i, ty)) in
               genv, venv, patt
       | Aml_ast.PattChar (c, loc) ->
            let ty = make_type loc TyChar in
            let patt = make_patt loc (PattChar (c, ty)) in
               genv, venv, patt
       | Aml_ast.PattString (s, loc) ->
            let ty = make_type loc TyString in
            let patt = make_patt loc (PattString (s, ty)) in
               genv, venv, patt
       | Aml_ast.PattWild loc ->
            let v = new_symbol_string "_" in
            let ty = make_type loc (TyVar v) in
            let patt = make_patt loc (PattWild ty) in
               genv, venv, patt
       | Aml_ast.PattVar (v, loc) ->
            build_patt_var genv venv pos v loc
       | Aml_ast.PattTuple (pl, loc) ->
            build_patt_tuple genv venv pos pl loc
       | Aml_ast.PattRecord (fields, loc) ->
            build_patt_record genv venv pos fields loc
       | Aml_ast.PattArray (pl, loc) ->
            build_patt_array genv venv pos pl loc
       | Aml_ast.PattConst (v, pl, loc) ->
            build_patt_const genv venv pos v pl loc
       | Aml_ast.PattChoice (p1, p2, loc) ->
            build_patt_choice genv venv pos p1 p2 loc
       | Aml_ast.PattAs (p1, p2, loc) ->
            build_patt_as genv venv pos p1 p2 loc
       | Aml_ast.PattConstrain (p, ty, loc) ->
            build_patt_constrain genv venv p ty loc
       | Aml_ast.PattWrap (p, loc) ->
            build_pattern genv venv p

       | Aml_ast.PattExpr (e, loc) ->
            build_patt_exp genv venv pos e loc
       | Aml_ast.PattRange (p1, p2, loc) ->
            build_patt_range genv venv pos p1 p2 loc
       | Aml_ast.PattWhen (p, e, loc) ->
            build_patt_when genv venv pos p e loc

(*
 * Infer a list of patterns.
 *)
and build_patterns genv venv pos patts =
   let genv, venv, patts =
      List.fold_left (fun (genv, venv, patts) p ->
            let genv, venv, p = build_pattern genv venv p in
            let patts = p :: patts in
               genv, venv, patts) (genv, venv, []) patts
   in
      genv, venv, List.rev patts

(*
 * Variable gets a new type.
 * Variables are only allowed to occur once in a pattern.
 *)
and build_patt_var genv venv pos v loc =
   let ty_v = new_symbol v in
   let ty = make_type loc (TyVar ty_v) in
   let v' = new_symbol v in
   let venv = venv_add_var venv v v' ty in
   let patt = make_patt loc (PattVar (v', ty)) in
      genv, venv, patt

(*
 * Tuple.
 *)
and build_patt_tuple genv venv pos pl loc =
   let genv, venv, pl = build_patterns genv venv pos pl in
   let ty = make_type loc (TyProd (List.map type_of_pattern pl)) in
   let patt = make_patt loc (PattTuple (pl, ty)) in
      genv, venv, patt

(*
 * In an array, all the fields must have the same type.
 *)
and build_patt_array genv venv pos pl loc =
   let genv, venv, pl = build_patterns genv venv pos pl in
   let ty_v = new_symbol_string "array" in
   let ty = make_type loc (TyVar ty_v) in
   let ty = make_type loc (TyArray ty) in
   let patt = make_patt loc (PattArray (pl, ty)) in
      genv, venv, patt

(*
 * Infer a record pattern.
 *)
and build_patt_record genv venv pos fields loc =
   (* Walk through the record, checking the fields *)
   let genv, venv, ty_var, arity, so_args, fields =
      List.fold_left (fun (genv, venv, ty_var, arity, so_args, fields) (label, p) ->
            let genv, label_name = genv_intern_label genv pos label in
            let ty_var, label_var, arity, so_args = genv_lookup_label genv pos label_name in
            let genv, venv, p = build_pattern genv venv p in
            let fields = (label_var, p) :: fields in
               genv, venv, ty_var, arity, so_args, fields) (genv, venv, null_record_var, 0, [], []) fields
   in
   let fo_args = new_fo_type_args loc arity in
   let ty = make_type loc (TyRecord (ty_var, fo_args, so_args)) in
   let patt = make_patt loc (PattRecord (List.rev fields, ty)) in
      genv, venv, patt

(*
 * Disjoint union.
 *)
and build_patt_const genv venv pos v pl loc =
   let genv, const_name = genv_intern_const genv pos v in
   let ty_var, const_var, arity, field = genv_lookup_const genv pos const_name in
   let fo_args = new_fo_type_args loc arity in
   let so_args =
      match field with
         ConstNormal so_args ->
            so_args
       | ConstException _ ->
            []
   in
   let ty = make_type loc (TyUnion (ty_var, fo_args, so_args)) in
   let genv, venv, pl = build_patterns genv venv pos pl in
   let patt = make_patt loc (PattConst (ty_var, const_var, pl, ty)) in
      genv, venv, patt

(*
 * No variables allowed in a choice.
 *)
and build_patt_choice genv venv pos p1 p2 loc =
   let genv, venv, p1 = build_pattern genv venv p1 in
   let genv, venv, p2 = build_pattern genv venv p2 in
   let ty = type_of_pattern p1 in
   let patt = make_patt loc (PattChoice (p1, p2, ty)) in
      genv, venv, patt

(*
 * As patterns allow bindings.
 *)
and build_patt_as genv venv pos p1 p2 loc =
   let genv, venv, p1 = build_pattern genv venv p1 in
   let genv, venv, p2 = build_pattern genv venv p2 in
   let ty = type_of_pattern p1 in
   let patt = make_patt loc (PattAs (p1, p2, ty)) in
      genv, venv, patt

(*
 * Expression pattern.
 *)
and build_patt_exp genv venv pos e loc =
   let genv, e = build_exp genv venv e in
   let ty = type_of_exp e in
   let patt = make_patt loc (PattExpr (e, ty)) in
      genv, venv, patt

(*
 * Range pattern.
 *)
and build_patt_range genv venv pos p1 p2 loc =
   let genv, venv, p1 = build_pattern genv venv p1 in
   let genv, venv, p2 = build_pattern genv venv p2 in
   let ty = type_of_pattern p1 in
   let e1 = exp_of_patt pos p1 in
   let e2 = exp_of_patt pos p2 in
   let patt = make_patt loc (PattRange (e1, e2, ty)) in
      genv, venv, patt

(*
 * Conditional pattern.
 *)
and build_patt_when genv venv pos p e loc =
   let genv, venv, p = build_pattern genv venv p in
   let genv, e = build_exp genv venv e in
   let patt = make_patt loc (PattWhen (p, e, type_of_exp e)) in
      genv, venv, patt

(*
 * Type constraint.
 *)
and build_patt_constrain genv venv p ty loc =
   let genv, venv, p = build_pattern genv venv p in
   let genv, ty = build_type genv venv ty in
   let patt = make_patt loc (PattConstrain (p, ty)) in
      genv, venv, patt

(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)

(*
 * Infer a type for an expression.
 * Returns:
 *   1. The translated expression
 *   2. a flag that is true iff the expression is a value
 *)
and build_exp genv venv e =
   let pos = string_pos "build_exp" (ast_exp_pos e) in
      match e with
         Aml_ast.Number (i, loc) ->
            let ty = make_type loc TyInt in
            let e = make_exp loc (Number (i, ty)) in
               genv, e
       | Aml_ast.Char (c, loc) ->
            let ty = make_type loc TyChar in
            let e = make_exp loc (Char (c, ty)) in
               genv, e
       | Aml_ast.String (s, loc) ->
            let ty = make_type loc TyString in
            let e = make_exp loc (String (s, ty)) in
               genv, e
       | Aml_ast.Var (id, loc) ->
            build_var_exp genv venv pos id loc
       | Aml_ast.Const (id, loc) ->
            build_const_exp genv venv pos id None loc
       | Aml_ast.Apply (Aml_ast.Const (id, loc1), [arg], loc2) ->
            build_const_exp genv venv pos id (Some arg) (union_loc loc1 loc2)
       | Aml_ast.Apply (f, args, loc) ->
            build_apply_exp genv venv pos f args loc
       | Aml_ast.Lambda (args, body, loc) ->
            build_lambda_exp genv venv pos args body loc
       | Aml_ast.Function (guarded, loc) ->
            build_function_exp genv venv pos guarded loc
       | Aml_ast.Let (lets, e, loc) ->
            build_lets_exp genv venv pos lets e loc
       | Aml_ast.LetRec (letrecs, e, loc) ->
            build_letrecs_exp genv venv pos letrecs e loc
       | Aml_ast.If (e1, e2, e3, loc) ->
            build_if_exp genv venv pos e1 e2 e3 loc
       | Aml_ast.For (v, e1, to_flag, e2, e3, loc) ->
            build_for_exp genv venv pos v e1 to_flag e2 e3 loc
       | Aml_ast.While (e1, e2, loc) ->
            build_while_exp genv venv pos e1 e2 loc
       | Aml_ast.Constrain (e, ty, loc) ->
            build_constrain_exp genv venv pos e ty loc
       | Aml_ast.Wrap (e, loc) ->
            build_exp genv venv e

       | Aml_ast.Tuple (el, loc) ->
            build_tuple_exp genv venv pos el loc
       | Aml_ast.Array (el, loc) ->
            build_array_exp genv venv pos el loc
       | Aml_ast.ArraySubscript (e1, e2, loc) ->
            build_array_subscript_exp genv venv pos e1 e2 loc
       | Aml_ast.ArraySetSubscript (e1, e2, e3, loc) ->
            build_array_set_subscript_exp genv venv pos e1 e2 e3 loc
       | Aml_ast.StringSubscript (e1, e2, loc) ->
            build_string_subscript_exp genv venv pos e1 e2 loc
       | Aml_ast.StringSetSubscript (e1, e2, e3, loc) ->
            build_string_set_subscript_exp genv venv pos e1 e2 e3 loc
       | Aml_ast.Record (fields, loc) ->
            build_record_exp genv venv pos fields loc
       | Aml_ast.RecordUpdate (e, fields, loc) ->
            build_record_update_exp genv venv pos e fields loc
       | Aml_ast.RecordProj (e, v, loc) ->
            build_record_proj_exp genv venv pos e v loc
       | Aml_ast.RecordSetProj (e1, v, e2, loc) ->
            build_record_set_proj_exp genv venv pos e1 v e2 loc

       | Aml_ast.Sequence (e1, e2, loc) ->
            build_sequence_exp genv venv pos e1 e2 loc

       | Aml_ast.Match (e, guarded, loc) ->
            build_match_exp genv venv pos e guarded loc
       | Aml_ast.Try (e, guarded, loc) ->
            build_try_exp genv venv pos e guarded loc
       | Aml_ast.Raise (e, loc) ->
            build_raise_exp genv venv pos e loc

       | Aml_ast.In (v, loc) ->
            build_in_exp genv venv pos v loc
       | Aml_ast.Out (v, loc) ->
            build_out_exp genv venv pos v loc
       | Aml_ast.Open (v, loc) ->
            build_open_exp genv venv pos v loc
       | Aml_ast.Action (msgs, loc) ->
            build_action_exp genv venv pos msgs loc
       | Aml_ast.Restrict (v, genv, e, loc) ->
            build_restrict_exp genv venv pos v genv venv e loc
       | Aml_ast.Ambient (v, me, loc) ->
            build_ambient_exp genv venv pos v me loc
       | Aml_ast.Thread (e, loc) ->
            build_thread_exp genv venv pos e loc
       | Aml_ast.Migrate (e1, e2, loc) ->
            build_migrate_exp genv venv pos e1 e2 loc
       | Aml_ast.LetModule (mn, me, e, loc) ->
            build_let_module_exp genv venv pos mn me e loc

and build_exps genv venv el =
   let genv, el =
      List.fold_left (fun (genv, el) e ->
            let genv, e = build_exp genv venv e in
               genv, e :: el) (genv, []) el
   in
      genv, List.rev el

(*
 * Variables.
 *)
and build_var_exp genv venv pos id loc =
   let pos = string_pos "build_var_exp" pos in
      try
         match id with
            [v] ->
               let v', ty = venv_lookup_var venv v in
               let e = make_exp loc (Var (NameEnv v', ty)) in
                  genv, e
          | _ ->
               raise Not_found
      with
         Not_found ->
            let genv, id = genv_intern_var genv pos id in
            let ty = genv_lookup_var genv pos id in
            let e = make_exp loc (Var (id, ty)) in
               genv, e

(*
 * Infer a type for the constructor application.
 * The result is a value if all the args are values.
 *)
and build_const_exp genv venv pos id arg_opt loc =
   let pos = string_pos "build_const_exp" pos in
   let genv, const_name = genv_intern_const genv pos id in

   (* Get the type *)
   let ty_var, const_var, arity, field = genv_lookup_const genv pos const_name in
   let fo_vars = new_fo_type_vars arity in
   let fo_args = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let so_args =
      match field with
         ConstNormal so_args ->
            so_args
       | ConstException _ ->
            []
   in
   let ty2 = make_type loc (TyUnion (ty_var, fo_args, so_args)) in
   let ty2 = make_type loc (TyAll (fo_vars, ty2)) in

   (* Get the arguments *)
   let args =
      match arg_opt with
         Some arg ->
            dest_tuple arg
       | None ->
            []
   in
   let genv, args = build_exps genv venv args in
   let ty1 = make_type loc (TyVar (new_symbol_string "union")) in
   let e = make_exp loc (Const (ty_var, const_var, args, ty1, ty2)) in
      genv, e

(*
 * Function application.
 * We don't decide on the return type yet, nor do we
 * perform any function type applications.
 *)
and build_apply_exp genv venv pos f args loc =
   let pos = string_pos "build_apply_exp" pos in

   (* Expression *)
   let genv, f = build_exp genv venv f in
   let genv, args = build_exps genv venv args in

   (* Just make up a random new type *)
   let fun_var = new_symbol_string "fun" in
   let res_var = new_symbol_string "res" in
   let ty_fun = make_type loc (TyVar fun_var) in
   let ty_res = make_type loc (TyVar res_var) in
   let e = make_exp loc (Apply (f, ty_fun, args, ty_res)) in
      genv, e

(*
 * Lambda.  We just give it an arbitrary type for
 * now.
 *)
and build_lambda_exp genv venv pos args e loc =
   let pos = string_pos "build_lambda_exp" pos in
   let genv, venv, args = build_patterns genv venv pos args in
   let genv, e = build_exp genv venv e in
   let ty_v = new_symbol_string "lambda" in
   let ty = make_type loc (TyVar ty_v) in
   let e = make_exp loc (Lambda ([], args, e, ty)) in
      genv, e

(*
 * Function has a guarded command.
 * It is like a lambda, but it has multiple matches.
 *)
and build_function_exp genv venv pos guarded loc =
   let pos = string_pos "build_function_exp" pos in
   let genv, guarded = build_guarded genv venv pos guarded in
   let ty1 = make_type loc (TyVar (new_symbol_string "arg")) in
   let ty2 = make_type loc (TyVar (new_symbol_string "res")) in
   let ty = make_type loc (TyFun ([ty1], ty2)) in
   let e = make_exp loc (Function ([], guarded, ty)) in
      genv, e

(*
 * Let definitions.
 *
 * These are _nonrecursive_ definitions.
 * We collect the declarations in each of the parts
 * and merge them.
 *)
and build_lets genv venv pos lets loc =
   let pos = string_pos "build_lets" pos in
   let genv, venv_new, lets =
      List.fold_left (fun (genv, venv_new, lets) (p, e) ->
            let genv, venv_new, p = build_pattern genv venv_new p in
            let genv, e = build_exp genv venv e in
            let ty = make_type loc (TyVar (new_symbol_string "let")) in
            let lets = (p, e, ty) :: lets in
               genv, venv_new, lets) (genv, venv_empty, []) lets
   in
   let lets = List.rev lets in
      genv, venv_new, lets

and build_lets_exp genv venv pos lets e loc =
   let pos = string_pos "build_lets_exp" pos in
   let genv, venv_new, lets = build_lets genv venv pos lets loc in
   let venv = venv_union venv venv_new in
   let genv, e = build_exp genv venv e in
   let ty = type_of_exp e in
   let e = make_exp loc (Let (lets, e, ty)) in
      genv, e

(*
 * Recursive function definitions.
 * First the functions are given arbitrary types.
 *)
and build_letrecs genv venv pos letrecs loc =
   let pos = string_pos "build_letrecs" pos in

   (* Add the vars with new types *)
   let venv_int, venv_ext, letrecs =
      List.fold_left (fun (venv_int, venv_ext, letrecs) (f, e) ->
            let ty_int = make_type loc (TyVar (new_symbol f)) in
            let ty_ext = make_type loc (TyVar (new_symbol f)) in
            let f' = new_symbol f in
            let venv_int = venv_add_var venv_int f f' ty_int in
            let venv_ext = venv_add_var venv_ext f f' ty_ext in
            let letrecs = (f', e, ty_int, ty_ext) :: letrecs in
               venv_int, venv_ext, letrecs) (venv_empty, venv_empty, []) letrecs
   in
   let venv = venv_union venv venv_int in
   let genv, letrecs =
      List.fold_left (fun (genv, letrecs) (f, e, ty_int, ty_ext) ->
            let genv, e = build_exp genv venv e in
            let letrecs = (f, e, ty_int, ty_ext) :: letrecs in
               genv, letrecs) (genv, []) letrecs
   in
      genv, venv_ext, letrecs

and build_letrecs_exp genv venv pos letrecs e loc =
   let pos = string_pos "build_letrecs_exp" pos in
   let genv, venv_ext, letrecs = build_letrecs genv venv pos letrecs loc in
   let venv = venv_union venv venv_ext in
   let genv, e = build_exp genv venv e in
   let ty = type_of_exp e in
   let e = make_exp loc (LetRec (letrecs, e, ty)) in
      genv, e

(*
 * Conditional expression.
 *)
and build_if_exp genv venv pos e1 e2 e3 loc =
   let pos = string_pos "build_if_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let genv, e3 = build_exp genv venv e3 in
   let ty = type_of_exp e2 in
   let e = make_exp loc (If (e1, e2, e3, ty)) in
      genv, e

and build_for_exp genv venv pos v e1 to_flag e2 e3 loc =
   let pos = string_pos "build_for_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let genv, e3 = build_exp genv venv e3 in
   let e = make_exp loc (For (v, e1, to_flag, e2, e3)) in
      genv, e

and build_while_exp genv venv pos e1 e2 loc =
   let pos = string_pos "build_while_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let e = make_exp loc (While (e1, e2)) in
      genv, e

(*
 * Type constraint.
 *)
and build_constrain_exp genv venv pos e ty loc =
   let pos = string_pos "build_constrain_exp" pos in
   let genv, e = build_exp genv venv e in
   let genv, ty = build_type genv venv ty in
   let e = make_exp loc (Constrain (e, ty)) in
      genv, e

(*
 * Tuple.
 *)
and build_tuple_exp genv venv pos el loc =
   let pos = string_pos "build_tuple_exp" pos in
   let genv, el = build_exps genv venv el in
   let ty = make_type loc (TyVar (new_symbol_string "tuple")) in
   let e = make_exp loc (Tuple (el, ty)) in
      genv, e

(*
 * Array.
 *)
and build_array_exp genv venv pos el loc =
   let pos = string_pos "build_array_exp" pos in
   let genv, el = build_exps genv venv el in
   let ty = make_type loc (TyVar (new_symbol_string "ty_array")) in
   let e = make_exp loc (Array (el, ty)) in
      genv, e

(*
 * Array subscript.
 *)
and build_array_subscript_exp genv venv pos e1 e2 loc =
   let pos = string_pos "build_array_subscript_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let ty = make_type loc (TyVar (new_symbol_string "sub")) in
   let e = make_exp loc (ArraySubscript (e1, e2, ty)) in
      genv, e

(*
 * Array assignment.
 *)
and build_array_set_subscript_exp genv venv pos e1 e2 e3 loc =
   let pos = string_pos "build_array_set_subscript_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let genv, e3 = build_exp genv venv e3 in
   let e = make_exp loc (ArraySetSubscript (e1, e2, e3)) in
      genv, e

(*
 * String subscript.
 *)
and build_string_subscript_exp genv venv pos e1 e2 loc =
   let pos = string_pos "build_string_subscript_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let e = make_exp loc (StringSubscript (e1, e2)) in
      genv, e

(*
 * String assignment.
 *)
and build_string_set_subscript_exp genv venv pos e1 e2 e3 loc =
   let pos = string_pos "build_string_set_subscript_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let genv, e3 = build_exp genv venv e3 in
   let e = make_exp loc (StringSetSubscript (e1, e2, e3)) in
      genv, e

(*
 * Record.
 * All the labels have to point to the same record.
 *)
and build_record_exp genv venv pos fields loc =
   let pos = string_pos "build_record_exp" pos in
   let genv, ty_var, arity, so_args, fields =
      List.fold_left (fun (genv, ty_var, arity, so_args, fields) (label, e, loc) ->
            let genv, label_name = genv_intern_label genv pos label in
            let ty_var, label_var, arity, so_args = genv_lookup_label genv pos label_name in
            let genv, e = build_exp genv venv e in
            let fields = (label_var, e) :: fields in
               genv, ty_var, arity, so_args, fields) (genv, null_record_var, 0, [], []) fields
   in
   let fo_vars = new_fo_type_vars arity in
   let fo_args = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_type loc (TyRecord (ty_var, fo_args, so_args)) in
   let ty = make_type loc (TyAll (fo_vars, ty)) in
   let e = make_exp loc (Record (fields, ty)) in
      genv, e

(*
 * Record.
 * All the labels have to point to the same record.
 *)
and build_record_update_exp genv venv pos e_arg fields loc =
   let pos = string_pos "build_record_update_exp" pos in
   let genv, ty_var, arity, so_args, fields =
      List.fold_left (fun (genv, ty_var, arity, so_args, fields) (label, e, loc) ->
            let genv, label_name = genv_intern_label genv pos label in
            let ty_var, label_var, arity, so_args = genv_lookup_label genv pos label_name in
            let genv, e = build_exp genv venv e in
            let fields = (label_var, e) :: fields in
               genv, ty_var, arity, so_args, fields) (genv, null_record_var, 0, [], []) fields
   in
   let fo_vars = new_fo_type_vars arity in
   let fo_args = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_type loc (TyRecord (ty_var, fo_args, so_args)) in
   let ty = make_type loc (TyAll (fo_vars, ty)) in
   let genv, e_arg = build_exp genv venv e_arg in
   let e = make_exp loc (RecordUpdate (e_arg, fields, ty)) in
      genv, e

(*
 * Infer a type for a record projection.
 *)
and build_record_proj_exp genv venv pos e v loc =
   let pos = string_pos "build_record_proj" pos in
   let genv, e = build_exp genv venv e in
   let genv, label_name = genv_intern_label genv pos v in
   let ty_var, label_var, arity, so_args = genv_lookup_label genv pos label_name in
   let fo_vars = new_fo_type_vars arity in
   let fo_args = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_type loc (TyRecord (ty_var, fo_args, so_args)) in
   let ty = make_type loc (TyAll (fo_vars, ty)) in
   let e = make_exp loc (RecordProj (e, ty, label_var, ty)) in
      genv, e

(*
 * Infer a type for a record assignment.
 *)
and build_record_set_proj_exp genv venv pos e1 v e2 loc =
   let pos = string_pos "build_record_set_proj_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let genv, label_name = genv_intern_label genv pos v in
   let ty_var, label_var, arity, so_args = genv_lookup_label genv pos label_name in
   let fo_vars = new_fo_type_vars arity in
   let fo_args = List.map (fun v -> make_type loc (TyVar v)) fo_vars in
   let ty = make_type loc (TyRecord (ty_var, fo_args, so_args)) in
   let ty = make_type loc (TyAll (fo_vars, ty)) in
   let e = make_exp loc (RecordSetProj (e1, ty, label_var, e2)) in
      genv, e

(*
 * Sequential execution.
 *)
and build_sequence_exp genv venv pos e1 e2 loc =
   let pos = string_pos "build_match_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let ty = type_of_exp e2 in
   let e = make_exp loc (Sequence (e1, e2, type_of_exp e2)) in
      genv, e

(*
 * Control flow.
 *)
and build_match_exp genv venv pos e guarded loc =
   let pos = string_pos "build_match_exp" pos in
   let genv, e = build_exp genv venv e in
   let genv, guarded = build_guarded genv venv pos guarded in
   let ty = make_type loc (TyVar (new_symbol_string "match")) in
   let e = make_exp loc (Match (e, guarded, ty)) in
      genv, e

and build_try_exp genv venv pos e guarded loc =
   let pos = string_pos "build_try_exp" pos in
   let genv, e = build_exp genv venv e in
   let genv, guarded = build_guarded genv venv pos guarded in
   let ty = type_of_exp e in
   let e = make_exp loc (Try (e, guarded, ty)) in
      genv, e

and build_raise_exp genv venv pos e loc =
   let pos = string_pos "build_raise_exp" pos in
   let genv, e = build_exp genv venv e in
   let ty = make_type loc (TyVar (new_symbol_string "raise")) in
   let e = make_exp loc (Raise (e, ty)) in
      genv, e

(*
 * Ambients.
 *)
and build_in_exp genv venv pos v loc =
   let pos = string_pos "build_in_exp" pos in
   let genv, me_name = genv_intern_module genv pos v in
   let ty = make_type loc (TyOpen ([], new_symbol_string "in")) in
   let e = make_exp loc (In (me_name, ty)) in
      genv, e

and build_out_exp genv venv pos v loc =
   let pos = string_pos "build_out_exp" pos in
   let genv, me_name = genv_intern_module genv pos v in
   let ty = make_type loc (TyOpen ([], new_symbol_string "out")) in
   let e = make_exp loc (Out (me_name, ty)) in
      genv, e

and build_open_exp genv venv pos v loc =
   let pos = string_pos "build_open_exp" pos in
   let genv, me_name = genv_intern_module genv pos v in
   let ty = make_type loc (TyOpen ([], new_symbol_string "open")) in
   let e = make_exp loc (Open (me_name, ty)) in
      genv, e

and build_action_exp genv venv pos el loc =
   let pos = string_pos "build_action_exp" pos in
   let genv, el = build_exps genv venv el in
   let ty = make_type loc (TyOpen ([], new_symbol_string "action")) in
   let e = make_exp loc (Action (el, ty)) in
      genv, e

and build_restrict_exp genv venv pos v genv venv e loc =
   raise (Failure "Type_infer.build_restrict: not implemented")

and build_ambient_exp genv venv pos mn me loc =
   raise (Failure "Type_infer.build_ambient: not implemented")

and build_thread_exp genv venv pos e loc =
   let pos = string_pos "build_thread_exp" pos in
   let genv, e = build_exp genv venv e in
   let e = make_exp loc (Thread e) in
      genv, e

and build_migrate_exp genv venv pos e1 e2 loc =
   let pos = string_pos "build_migrate_exp" pos in
   let genv, e1 = build_exp genv venv e1 in
   let genv, e2 = build_exp genv venv e2 in
   let ty = type_of_exp e2 in
   let e = make_exp loc (Migrate (e1, e2, ty)) in
      genv, e

and build_let_module_exp genv venv pos mn me e loc =
   raise (Failure "Type_infer.build_let_module_exp: not implemented")

(*
 * Guarded command combines pattern inference.
 *)
and build_guarded genv venv pos cases =
   let pos = string_pos "build_guarded" pos in
   let genv, cases =
      List.fold_left (fun (genv, cases) (p, e) ->
            let genv, venv, p = build_pattern genv venv p in
            let genv, e = build_exp genv venv e in
            let cases = (p, e) :: cases in
               genv, cases) (genv, []) cases
   in
      genv, List.rev cases

(************************************************************************
 * MODULE TYPE                                                          *
 ************************************************************************)

(*
 * Convert the module type.
 * All the sharing constraints are collected into
 * a separate module type, which is then unified with
 * the first module type.
 *)
and build_module_type genv venv mt : genv * ty =
   let pos = string_pos "build_module_type" (ast_mt_pos mt) in
      match mt with
         Aml_ast.ModuleTypeVar (name, loc) ->
            build_module_type_var genv venv pos name loc

       | Aml_ast.ModuleTypeSig (items, loc) ->
            build_module_type_sig genv venv pos items loc

       | Aml_ast.ModuleTypeWithType (tenv, v, params, ty, loc) ->
            build_module_type_with_type genv venv pos tenv v params ty loc

       | Aml_ast.ModuleTypeWithModule (tenv, mn, me, loc) ->
            build_module_type_with_module genv venv pos tenv mn me loc

       | Aml_ast.ModuleTypeFunctor (v, mt1, mt2, loc) ->
            build_module_type_functor genv venv pos v mt1 mt2 loc

(*
 * Lookup the module type for the variable.
 *)
and build_module_type_var genv venv pos id loc =
   let pos = string_pos "build_module_type_var" pos in
   let genv, mt_name = genv_intern_module_type genv pos id in
   let mt = genv_lookup_module_type genv pos mt_name in
      genv, mt

(*
 * Build a signature from the item list.
 *)
and build_module_type_sig genv venv pos items loc =
   let pos = string_pos "build_module_type_items" pos in
   let me_var = new_symbol_string "mt" in
   let genv = genv_push genv me_var in
   let genv, venv =
      List.fold_left (fun (genv, venv) item ->
            let genv = build_module_type_item pos genv venv item in
               genv, venv) (genv, venv) items
   in
   let genv, core = genv_pop genv in
   let tydef = TyDefModule core in
   let ty_var = new_symbol_string "mt" in
   let genv, fo_args, so_args, tydef = genv_add_abstract_tydef genv ty_var [] tydef loc in
   let ty = make_type loc (TyModule (ty_var, fo_args, so_args)) in
      genv, ty

and build_module_type_item pos genv venv item : genv =
   let pos = string_pos "build_module_type_item" pos in
      match item with
         Aml_ast.SigItemType (types, loc) ->
            build_module_type_types genv venv pos types loc
       | Aml_ast.SigItemVal (v, ty, loc) ->
            build_module_type_val genv venv pos v ty loc
       | Aml_ast.SigItemModuleType (v, tenv, loc) ->
            build_module_type_module_type genv venv pos v tenv loc
       | Aml_ast.SigItemModule (v, tenv, loc) ->
            build_module_type_module genv venv pos v tenv loc
       | Aml_ast.SigItemException (v, tyl, loc) ->
            build_module_type_exn genv venv pos v tyl loc
       | Aml_ast.SigItemOpen (id, loc) ->
            build_module_type_open genv venv pos id loc
       | Aml_ast.SigItemExternal (v, ty, s, loc) ->
            build_module_type_external genv venv pos v ty s loc
       | Aml_ast.SigItemExternalType (v, vars, s, loc) ->
            build_module_type_external_type genv venv pos v vars s loc

(*
 * Expose the variables in a sub-module.
 *)
and build_module_type_open genv venv pos id loc =
   let pos = string_pos "build_module_type_open" pos in
   let genv, me_name = genv_intern_module genv pos id in
      genv_open genv pos me_name

(*
 * Add a type definition to the module type.
 *)
and build_module_type_types genv venv pos types loc =
   let pos = string_pos "build_module_type_types" pos in
   let genv, _ = build_top_types genv venv types in
      genv

(*
 * Add an external type definition.
 *)
and build_module_type_external_type genv venv pos v vars s loc =
   let pos = string_pos "build_module_type_external_type" pos in
   let ty = type_of_external genv pos vars s loc in
   let v' = new_symbol v in
   let genv = genv_add_type genv v v' vars (Some ty) in
      genv

(*
 * Add a value declaration to the module type.
 *)
and build_module_type_val genv venv pos v ty loc =
   let pos = string_pos "build_module_type_val" pos in
   let genv, ty = build_type genv venv ty in
   let vars = Aml_tast_ds.free_type_vars ty in
   let ty = make_type loc (TyAll (vars, ty)) in
   let v' = new_symbol v in
      genv_add_var genv v v' ty

(*
 * Add an external function.
 *)
and build_module_type_external genv venv pos v ty s loc =
   let pos = string_pos "genv_module_type_external" pos in
   let vars = Aml_ast_util.free_type_vars ty in
   let genv, ty = build_type genv venv ty in
   let ty = flatten_fun_type genv pos loc ty in
   let ty = make_type loc (TyExternal (ty, s)) in
   let ty = make_type loc (TyAll (vars, ty)) in
   let v' = new_symbol v in
      genv_add_var genv v v' ty

(*
 * Exception declaration.
 *)
and build_module_type_exn genv venv pos ext_var tyl loc =
   let pos = string_pos "genv_module_type_exn" pos in
   let const_var = new_symbol ext_var in
   let genv, tyl = build_types genv venv pos tyl in
   let ty_var = ty_exn_var genv in
   let genv = genv_add_exn genv ext_var const_var ty_var tyl in
      genv

(*
 * Add a module type declaration to the module type.
 *)
and build_module_type_module_type genv venv pos v mt loc =
   (* Convert the module type *)
   let genv, mt = build_module_type genv venv mt in
   let v' = new_symbol v in
      (* BUG: this is wrong, but temporary *)
      genv_add_module_type genv v v' v' mt []

(*
 * Add a module declaration to the module type.
 *)
and build_module_type_module genv venv pos v mt loc =
   (* Convert the module type *)
   let genv, mt = build_module_type genv venv mt in
   let me_var = new_symbol v in
      genv_add_module genv v me_var mt

(*
 * A functor.
 *)
and build_module_type_functor genv venv pos v mt1 mt2 loc =
   let pos = string_pos "build_module_type_functor" pos in
   let genv, mt1 = build_module_type genv venv mt1 in
   let me_var = new_symbol v in
   let genv = genv_add_module genv v me_var mt1 in
   let genv, mt2 = build_module_type genv venv mt2 in
   let mt = make_type loc (TyFunctor (me_var, mt1, mt2)) in
   (* let genv = genv_remove_module genv venv v loc in *)
      genv, mt

(*
 * Add type definitions to the with_mt module.
 *)
and build_module_type_with_type genv venv pos mt id vars ty loc =
   let pos = string_pos "build_module_type_with_type" pos in
      raise (TAstException (pos, NotImplemented "build_module_type_with_type"))

(*
   (* Resolve the type *)
   let vars = List.map (fun v -> v, new_symbol v) vars in
   let ty = build_type genv venv ty in

   (* Inner module type *)
   let genv, mt = build_module_type genv venv mt in

   (* Constrain the field *)
   let ty_name = trans_intern_type mt id loc in
   let ty' = trans_lookup_type mt ty_name loc in
   let _ = match_type "build_module_type" genv venv sub ty' ty in
   let mt = trans_set_type mt ty_name ty loc in
      mt, genv
*)

and build_module_type_with_module genv venv pos mt id mn loc =
   let pos = string_pos "build_module_type_with_module" pos in
      raise (TAstException (pos, NotImplemented "build_module_type_with_module"))
(*
   (* Resolve the module *)
   let me_name = genv_intern_module genv mn loc in
   let mt' = genv_lookup_module genv venv me_name loc in

   (* Inner module type *)
   let mt, genv = build_module_type genv venv penv sub mt in

   (* Constrain the field *)
   let me_name = trans_intern_module mt id loc in
   let _ = match_trans_type "build_module_type_with_module" genv venv sub mt mt' loc in
   let _, mt' = core_of_trans_type mt' loc in
   let mt = trans_set_module mt me_name mt' loc in
      mt, genv
 *)

(************************************************************************
 * MODULE EXPRESSION                                                    *
 ************************************************************************)

(*
 * Convert the module expression.
 *)
and build_module_exp genv venv me =
   let pos = string_pos "build_module_exp" (ast_me_pos me) in
      match me with
         Aml_ast.ModuleExpStruct (items, loc) ->
            build_module_exp_items genv venv pos items loc
       | Aml_ast.ModuleExpVar (id, loc) ->
            build_module_exp_var genv venv pos id loc
       | Aml_ast.ModuleExpConstrain (me, tenv, loc) ->
            build_module_exp_constrain genv venv pos me tenv loc
       | Aml_ast.ModuleExpFunctor (v, tenv, me, loc) ->
            build_module_exp_functor genv venv pos v tenv me loc
       | Aml_ast.ModuleExpApply (me, mn, loc) ->
            build_module_exp_apply genv venv pos me mn loc

(*
 * Infer the struct items.
 *)
and build_module_exp_items genv venv pos items loc =
   let pos = string_pos "build_module_exp_items" pos in
   let me_var = new_symbol_string "struct" in
   let genv, fields, names, ty = build_module_exp_struct genv venv pos me_var items loc in
   let me = make_module_exp loc (ModuleExpStruct (me_var, fields, names, ty)) in
      genv, me

and build_module_exp_struct genv venv pos me_var items loc =
   let pos = string_pos "build_module_exp_struct" pos in
   let genv = genv_push genv me_var in
   let genv, fields =
      List.fold_left (fun (genv, fields) item ->
            let genv, items = build_module_exp_item genv venv pos item in
            let fields = items @ fields in
               genv, fields) (genv, []) items
   in
   let genv, core = genv_pop genv in
   let tydef = TyDefModule core in
   let ty_var = new_symbol_string "mt" in
   let genv, fo_args, so_args, tydef = genv_add_abstract_tydef genv ty_var [] tydef loc in
   let ty = make_type loc (TyModule (ty_var, fo_args, so_args)) in
      genv, List.rev fields, core.mt_names, ty

and build_module_exp_item genv venv pos item =
   let pos = string_pos "build_module_exp_item" pos in
      match item with
         Aml_ast.StrItemLet (lets, loc) ->
            build_module_exp_lets genv venv pos lets loc
       | Aml_ast.StrItemExternal (v, ty, s, loc) ->
            build_module_exp_external genv venv pos v ty s loc
       | Aml_ast.StrItemExternalType (v, vars, s, loc) ->
            build_module_exp_external_type genv venv pos v vars s loc
       | Aml_ast.StrItemLetRec (letrecs, loc) ->
            build_module_exp_letrecs genv venv pos letrecs loc
       | Aml_ast.StrItemExpr (e, loc) ->
            build_module_exp_expr genv venv pos e loc
       | Aml_ast.StrItemRestrict (v, ty, loc) ->
            build_module_exp_restrict genv venv pos v ty loc
       | Aml_ast.StrItemType (types, loc) ->
            build_module_exp_types genv venv pos types loc
       | Aml_ast.StrItemModule (v, me, loc) ->
            build_module_exp_module genv venv pos v me loc
       | Aml_ast.StrItemModuleType (v, ty, loc) ->
            build_module_exp_module_type genv venv pos v ty loc
       | Aml_ast.StrItemException (v, tyl, loc) ->
            build_module_exp_exn genv venv pos v tyl loc
       | Aml_ast.StrItemOpen (id, loc) ->
            build_module_exp_open genv venv pos id loc

(*
 * External value.
 *)
and build_module_exp_external genv venv pos v ty s loc =
   let pos = string_pos "build_module_exp_external" pos in
   let vars = Aml_ast_util.free_type_vars ty in
   let genv, ty = build_type genv venv ty in
   let ty = flatten_fun_type genv pos loc ty in
   let ty = make_type loc (TyExternal (ty, s)) in
   let ty = make_type loc (TyAll (vars, ty)) in
   let v' = new_symbol v in
   let genv = genv_add_var genv v v' ty in
      genv, [make_field loc (FieldExternal (v', ty, s))]

(*
 * Open a module.
 *)
and build_module_exp_open genv venv pos id loc =
   let pos = string_pos "build_module_exp_open" pos in
   let genv, me_name = genv_intern_module genv pos id in
   let genv = genv_open genv pos me_name in
      genv, []

(*
 * Inference for type declaration.
 *)
and build_module_exp_types genv venv pos types loc =
   let pos = string_pos "build_module_exp_types" pos in
   let genv, tyl = build_top_types genv venv types in
      genv, [make_field loc (FieldType tyl)]

(*
 * Add an external type definition.
 *)
and build_module_exp_external_type genv venv pos v vars s loc =
   let pos = string_pos "build_module_exp_external_type" pos in
   let ty = type_of_external genv pos vars s loc in
   let v' = new_symbol v in
   let genv = genv_add_type genv v v' vars (Some ty) in
      genv, [make_field loc (FieldType [v', vars, Some ty])]

(*
 * Inference for let declarations.
 *)
and build_module_exp_lets genv venv pos lets loc =
   let pos = string_pos "build_module_exp_lets" pos in
   let genv, venv_new, lets = build_lets genv venv pos lets loc in
   let genv =
      SymbolTable.fold (fun genv v (v', ty) ->
            if debug debug_tast then
               Format.eprintf "@[<hv 3>build_module_exp_lets: adding variable:@ %a = @[<hv 3>%a,@ %a@]@]@." (**)
                  pp_print_symbol v
                  pp_print_symbol v'
                  pp_print_type ty;
            genv_add_var genv v v' ty) genv venv_new
   in
      genv, [make_field loc (FieldLet lets)]

(*
 * Inference for recursive definitions.
 *)
and build_module_exp_letrecs genv venv pos letrecs loc =
   let pos = string_pos "build_module_exp_letrecs" pos in
   let genv, venv_new, letrecs = build_letrecs genv venv pos letrecs loc in
   let genv =
      SymbolTable.fold (fun genv v (v', ty) ->
            if debug debug_tast then
               Format.eprintf "@[<hv 3>build_module_exp_lets: adding variable:@ %a = @[<hv 3>%a,@ %a@]@]@." (**)
                  pp_print_symbol v
                  pp_print_symbol v'
                  pp_print_type ty;
            genv_add_var genv v v' ty) genv venv_new
   in
      genv, [make_field loc (FieldLetRec letrecs)]

(*
 * Inference for exception declarations.
 *)
and build_module_exp_exn genv venv pos ext_var tyl loc =
   let pos = string_pos "build_module_exp_exn" pos in
   let const_var = new_symbol ext_var in
   let genv, tyl = build_types genv venv pos tyl in
   let ty_var = ty_exn_var genv in
   let genv = genv_add_exn genv ext_var const_var ty_var tyl in
   let items = [make_field loc (FieldException (const_var, ty_var, tyl))] in
      genv, items

(*
 * Inference for expression.
 *)
and build_module_exp_expr genv venv pos e loc =
   let pos = string_pos "build_module_exp_expr" pos in
   let genv, e = build_exp genv venv e in
      genv, [make_field loc (FieldExpr e)]

(*
 * Inference for restriction.
 *)
and build_module_exp_restrict genv venv pos v mt loc =
   let pos = string_pos "build_module_exp_restrict" pos in
   let genv, mt = build_module_type genv venv mt in
   let me_var = new_symbol v in
   let genv = genv_add_module genv v me_var mt in
      genv, [make_field loc (FieldRestrict (me_var, mt))]

(*
 * Inference for a module declaration.
 *)
and build_module_exp_module genv venv pos ext_var me loc =
   let pos = string_pos "build_module_exp_module" pos in
   let genv, me = build_module_exp genv venv me in
   let mt = type_of_module me in
   let me_var = new_symbol ext_var in
   let genv = genv_add_module genv ext_var me_var mt in
      genv, [make_field loc (FieldModule (me_var, me, mt))]

(*
 * Inference for a module type declaration.
 *)
and build_module_exp_module_type genv venv pos v mt loc =
   let pos = string_pos "build_module_exp_module" pos in
   let genv, mt = build_module_type genv venv mt in
      raise (TAstException (pos, NotImplemented "module_type"))
(*
   let genv = genv_add_module_type genv v mt in
      genv, [FieldModuleType (v, mt)]
*)

(*
 * Get the module expression for a var.
 *)
and build_module_exp_var genv venv pos id loc =
   let pos = string_pos "build_module_exp_var" pos in
   let genv, me_name = genv_intern_module genv pos id in
   let mt = genv_lookup_module genv pos me_name in
   let me = make_module_exp loc (ModuleExpVar (me_name, mt)) in
      genv, me

(*
 * Infer a functor.
 *)
and build_module_exp_functor genv venv pos v mt me loc =
   let pos = string_pos "build_module_exp_functor" pos in
   let genv, mt = build_module_type genv venv mt in
   let me_var = new_symbol v in
   let genv = genv_add_module genv v me_var mt in
   let genv, me = build_module_exp genv venv me in
   let ty = make_type loc (TyFunctor (me_var, mt, type_of_module me)) in
   let me = make_module_exp loc (ModuleExpFunctor (me_var, mt, me, ty)) in
   (* let genv = genv_remove_module genv venv v loc in *)
      genv, me

(*
 * Infer a module application.
 *)
and build_module_exp_apply genv venv pos me id loc =
   let pos = string_pos "build_module_exp_apply" pos in
   let genv, me = build_module_exp genv venv me in
   let genv, me_name = genv_intern_module genv pos id in
   let mt_fun = type_of_module me in
   let mt_src = genv_lookup_module genv pos me_name in
      raise (TAstException (pos, NotImplemented "build_module_exp_apply"))

(*
 * Constrain the module expression.
 *)
and build_module_exp_constrain genv venv pos me mt loc =
   let pos = string_pos "build_module_exp_constrain" pos in
   let genv, me = build_module_exp genv venv me in
   let genv, mt = build_module_type genv venv mt in
      raise (TAstException (pos, NotImplemented "build_module_exp_constrain"))

(************************************************************************
 * GLOBAL FUNCTIONS                                                     *
 ************************************************************************)

(*
 * Expand the substitituion.
 *)
let build_prog prog =
   let { Aml_ast.prog_name = name;
         Aml_ast.prog_exp = exp
       } = prog
   in
   let loc = create_loc name 0 0 0 0 in
   let pos = string_pos "build_prog" (loc_pos loc) in

   (* Compile the contents *)
   let genv = genv_empty name in
   let genv, exp =
      match exp with
         Aml_ast.Implementation items ->
            let me_var = new_symbol name in
            let genv, items, names, ty = build_module_exp_struct genv venv_empty pos me_var items loc in
            let genv, ty = load_interface_default genv pos name ty in
            let genv = genv_remove_import genv name in
               genv, Implementation (me_var, items, names, ty)
       | Aml_ast.Interface items ->
            let genv, ty = build_module_type_sig genv venv_empty pos items loc in
               genv, Interface ty
   in
   let prog = prog_of_genv genv exp in
      if debug debug_print_tast then
         debug_prog "Aml_tast_ast (before standardize)" prog;
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
