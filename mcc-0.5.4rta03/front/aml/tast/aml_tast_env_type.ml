(*
 * Program environments.
 *
 * We have to keep track of lots of info.
 * The most important is keeping track of the current
 * scope, which in general is a multi-level nesting of modules.
 *
 * Also, to support "open" we keep track of an open
 * environment that maps names to their original module
 * names.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Aml_tast

(*
 * The type environment is a nested list of modules.
 * Also include "open" info, which maps variables to modules
 * in which they occur.
 *)
type open_info =
   { mt_opens        : me_name SymbolTable.t;
     me_opens        : me_name SymbolTable.t;
     ty_opens        : me_name SymbolTable.t;
     var_opens       : me_name SymbolTable.t;
     const_opens     : me_name SymbolTable.t;
     label_opens     : me_name SymbolTable.t
   }

(*
 * A genv "level" is one level of module nesting.
 * It includes the name of the module being defined,
 * the module_type (under construction), and the
 * open_info for name maps.
 *)
type genv_level =
   { genv_core   : module_type;
     genv_opens  : open_info
   }

(*
 * An environment is a list of levels.
 *
 * genv_types:
 *    These are the toplevel type definitions for records, unions, modules, etc.
 *
 * genv_import:
 *    These are the types of interfaces that have been loaded from
 *    files.
 *
 * genv_names:
 *    A map from internal to external names.
 *
 * genv_current, genv_next:
 *    The current (and nested) module scopes
 *
 * genv_ty_unit, genv_ty_bool:
 *    The names of the unit and bool types
 *)
type genv =
   { genv_name       : me_var;
     genv_names      : ext_var SymbolTable.t;
     genv_types      : tydef SymbolTable.t;
     genv_import     : (hash * me_var * ty) SymbolTable.t;
     genv_current    : genv_level;
     genv_next       : genv_level list;
     genv_boot       : boot
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
