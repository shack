(*
 * Bring all the environment operations together into one file.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast_env_boot
open Aml_tast_env_base
open Aml_tast_env_add
open Aml_tast_env_remove
open Aml_tast_env_file
open Aml_tast_env_intern
open Aml_tast_env_lookup
open Aml_tast_env_open
open Aml_tast_env_abstract
open Aml_tast_env_print

type genv = Aml_tast_env_type.genv

include Aml_tast_env_boot
include Aml_tast_env_base
include Aml_tast_env_add
include Aml_tast_env_remove
include Aml_tast_env_file
include Aml_tast_env_intern
include Aml_tast_env_lookup
include Aml_tast_env_open
include Aml_tast_env_abstract
include Aml_tast_env_print

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
