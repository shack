(*
 * All the files in this directory are designed to convert the
 * parsed abstract syntax tree  (defined in ast.mlz) to a similar ast
 * that is strongly-typed.  Type inference is performed in the
 * Aml_tast_infer module, which relies on unification (defined in
 * Aml_tast_unify) and substitution (defined in Aml_tast_ds).
 *
 * Variable naming convention:
 *    During translation, all source variable names are converted to
 *    "internal" names where every definition is given a different
 *    variable name.  The "external" names are preserved for linking
 *    with other separately-compiled modules.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Field_table

open Location
open Attribute

(*
 * Give names to all the different kinds of symbols.
 * The symbols are really all the same.
 *)
type var        = symbol
type me_var     = symbol
type mt_var     = symbol
type ty_var     = symbol
type const_var  = symbol
type ext_var    = symbol
type label      = symbol
type self_var   = symbol
type tag_var    = symbol

(************************************************************************
 * NAMES                                                                *
 ************************************************************************)

(*
 * Module projection and variables.
 *)
type ext_var_name   = Aml_ast.var_name
type ext_ty_name    = Aml_ast.ty_name
type ext_const_name = Aml_ast.const_name
type ext_label_name = Aml_ast.label_name
type ext_mod_name   = Aml_ast.mod_name

(*
 * Module names are either local modules,
 * projections from modules, or applications of
 * functors to named arguments.
 *)
type me_name =
   ModNameEnv of me_var
 | ModNameSelf of me_var * ext_var
 | ModNameProj of me_name * ext_var
 | ModNameApply of me_name * me_name

(*
 * A variable name is either local,
 * or a projection from a module.
 *)
type 'a name =
   NameEnv of 'a
 | NamePath of me_name * ext_var

(*
 * A type name is like a regular name,
 * but we allow references to the current module.
 *)
type type_name =
   TyNameSoVar of ty_var
 | TyNameSelf of me_var * ext_var
 | TyNamePath of me_name * ext_var

(*
 * A kind classifies second-order types.
 * All type functions take some number of types as arguments,
 * and returns a type as the result.  The only thing we keep
 * track of is the arity of the function.
 *)
type 'a kind =
   { kind_arity : int;
     kind_value : 'a
   }

type var_name    = var name
type const_name  = const_var name
type label_name  = label name
type mt_name     = type_name
type ty_name     = type_name

type ty_kind     = ty_name kind

(************************************************************************
 * SUBSTITUTION                                                         *
 ************************************************************************)

(*
 * Free vars.
 *)
type free_vars =
   { fv_fo_vars   : SymbolSet.t;
     fv_so_vars   : SymbolSet.t;
     fv_vars      : SymbolSet.t;
     fv_me_vars   : SymbolSet.t
   }

(*
 * Substitution.
 *)
type subst =
   { (*
      * Type variables get replaced with actual types.
      *)
     sub_fo_vars  : ty SymbolTable.t;

     (*
      * Replace a type variable with a type name.
      *)
     sub_so_vars  : ty_kind SymbolTable.t;

     (*
      * Variables get replaced with module expressions.
      *)
     sub_vars     : (me_name * label) SymbolTable.t;

     (*
      * Module variables get replaced with module expressions.
      *)
     sub_me_vars  : me_name SymbolTable.t;

     (*
      * If this is set, we're supposed to replace every
      * location in the term with this one.
      *)
     sub_loc      : loc option
   }

(*
 * A delayed substitution.
 *)
and 'core delayed_subst = (loc, free_vars, subst, 'core) term
and 'core simple_subst = (loc, 'core) simple_term

(************************************************************************
 * TYPES                                                                *
 ************************************************************************)

(*
 * The type uses a delayed subst.
 *)
and ty = ty_core delayed_subst

(*
 * Core types.
 *)
and ty_core =
   (*
    * Basis numeric and string types.
    *)
   TyInt
 | TyChar
 | TyString
 | TyFloat

   (*
    * Function types.
    *
    * The TyFun is a normal function type.
    * For (TyFun (t1, t2)) the function takes an argument
    * of type t1, and returns a value of type t2.
    *
    * Functor types are dependent function types.
    * For (TyFunctor (v, t1, t2)), the types t1 and t2
    * are always module types.  The variable v is bound in
    * t2, and represents a value of type t1.
    *)
 | TyFun of ty list * ty
 | TyFunctor of ty_var * ty * ty

   (*
    * Basic unnamed aggregates, including tuples and arrays.
    *)
 | TyProd of ty list
 | TyArray of ty

   (*
    * TyParam is a type variable.
    *
    * TyAll is universal quantification,
    * used for function types.
    *)
 | TyAll of ty_var list * ty

   (*
    * The TyVar is a simple polymorphic type variable.
    *)
 | TyVar of ty_var

   (*
    * The TySOVar is a higher-order type application.
    * The variable is a type variable that represent
    * a type name.
    *)
 | TySOVar of ty_var * ty list

   (*
    * The TyProject is used to refer to a type in
    * another module.  The ty_name projects the type
    * from the module.
    *)
 | TyProject of ty_name * ty list

   (*
    * The other type applications are used to
    * refer to top-level type definitions.  They
    * have _two_ sets of arguments.  See the comment
    * in tydef_core below.
    *)
 | TyApply  of ty_var * ty list * ty_kind list
 | TyUnion  of ty_var * ty list * ty_kind list
 | TyRecord of ty_var * ty list * ty_kind list
 | TyModule of ty_var * ty list * ty_kind list

   (*
    * Printf has a special type all it's own.
    * This is a hack to make sure that printf
    * format strings are well-typed.
    *)
 | TyFormat of ty * ty * ty

   (* External functions *)
 | TyExternal of ty * string

   (*
    * Ambient types.
    * These are undocumented for the time being,
    * until we decide exactly what to do with ambients.
    *)
 | TyOpen of (var * ty) list * ty_var

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * A union is a "normal" union: all the fields are known
 * and ordered, or it is an "exception" or "extensible" union
 * where new fields can be added dynamically.
 *)
type union_type =
   NormalUnion                  (* Exact field count and ordering *)
 | ExnUnion                     (* Extensible union *)

(*
 * A union has a set of tagged tuples.
 *)
type union_field  = ty list
type union_fields = union_field FieldTable.t

(*
 * A record field has a name, a "mutable" flag, and a type.
 *)
type record_field  = bool * ty
type record_fields = record_field FieldTable.t

(*
 * The "names" record maps external (AST) names to
 * internal names.  This is just like standardization:
 * for each original variable in the environment, there
 * is a corresponding internal name, and <em>all</em>
 * the internal names are different.
 *)
type module_names =
   { names_mt     : (mt_var * ty list)   SymbolTable.t;
     names_me     : me_var               FieldTable.t;
     names_ty     : ty_var               SymbolTable.t;
     names_var    : var                  FieldTable.t;
     names_const  : const_var            SymbolTable.t;
     names_label  : label                SymbolTable.t
   }

(*
 * Constructor names are either for exceptions or
 * definitions in the current module.  If the constructor
 * is an exception, we keep track of the types of the fields
 * because they are not listed in the exception type.
 *)
type const_field =
   ConstNormal of ty_kind list
 | ConstException of ty list

(*
 * Translucent sums represent module types.
 * They are just like records, but they have type fields.
 * A type definition may be None, in which case the type
 * is abstract.
 *
 *     mt_self_var:
 *         The name for the module in its own definition.
 *         The name is used in TyNameSelf.
 *
 *     mt_mt_types:
 *         module type definitions
 *
 *     me_me_vars:
 *         module declarations
 *
 *     mt_types (fo_vars, ty_opt):
 *         type definitions
 *             fo_vars are the type parameters to the definition.
 *             If ty_opt is None, the type is abstract.
 *             If ty_opt is (Some ty), the type is defined as (all fo_vars. ty)
 *
 *     mt_vars: variable declarataions
 *
 *     mt_consts (ty_var, arity, const_field):
 *        We keep track of the type constructor names for each of the
 *        union types that are defined in the module.  Ty ty_var is
 *        the name of the top-level type definition.  The arity is
 *        the number of type perameters to the type def.  The const_field
 *        is either the name of the internal type, or the types of
 *        the fields (for exceptions).
 *
 *     mt_labels (ty_var, arity, ty_kinds):
 *        We also keep track of the record types for each of the
 *        record labels.  ty_var is the name of the top-level
 *        definition.  The arity is the number of type parameters.
 *        The ty_kinds are the second-order type arguments.
 *
 * Next, the tables use internal names.  We have
 * to keep track of their original (external) names so
 * that we can translate labels from the AST.
 *)
type module_type =
   { mt_self         : me_var;
     mt_mt_types     : ty                                SymbolTable.t;
     mt_me_vars      : ty                                SymbolTable.t;
     mt_types        : (ty_var list * ty option)         SymbolTable.t;
     mt_vars         : ty                                SymbolTable.t;
     mt_consts       : (ty_var * int * const_field)      SymbolTable.t;
     mt_labels       : (ty_var * int * ty_kind list)     SymbolTable.t;
     mt_names        : module_names
   }

(*
 * Type definitions.
 *
 * These few types are lifted to top-level, including
 * any parameterized types, unions, records, and module types.
 * These _are not_ types, but there are types that correspond
 * to them:
 *
 *    TyDefLambda     <-->    TyApply
 *    TyDefUnion      <-->    TyUnion
 *    TyDefRecord     <-->    TyRecord
 *    TyDefModule     <-->    TyModule
 *
 * The reason why these types, and not others, are lifted is
 * that these types contain names, and it is easier to deal
 * with the naming issue when the types are all in one place.
 *
 * Note that top-level types can't contain module names, because
 * they must be closed (see the comment about tydef_core below).
 *)
type tydef_inner_core =
   TyDefLambda  of ty
 | TyDefUnion   of union_type * union_fields
 | TyDefRecord  of record_fields
 | TyDefModule  of module_type

type tydef_inner = tydef_inner_core delayed_subst

(*
 * The core type definitions have two sets of parameters.
 * The tydef_fo_vars are normal type parameters (kind = 0), and
 * tydef_so_vars are higher-order type constructors.
 *
 * However, the tydef_so_vars are generally used by type
 * closure conversion (implemented in Aml_tast_ast) to replace
 * module names in the type.  Top-level type definitions must be
 * closed; they cannot contain module or variable names.  Each
 * form of type application supplies two lists of type arguments,
 * one for the normal parameters, and one for the names.
 * The main reason to split them is that it is easier to
 * keep track of the parameters this way.
 *)
type tydef =
   { tydef_fo_vars : ty_var list;
     tydef_so_vars : ty_var kind list;
     tydef_inner   : tydef_inner
   }

(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)

(*
 * Patterns.
 *)
type patt = patt_core simple_subst

and patt_core =
   (* Simple patterns *)
   PattInt of int * ty
 | PattChar of char * ty
 | PattString of string * ty
 | PattWild of ty
 | PattVar of var * ty

   (* Aggregate patterns *)
 | PattTuple of patt list * ty
 | PattRecord of (label * patt) list * ty
 | PattArray of patt list * ty
 | PattConst of ty_var * const_var * patt list * ty
 | PattChoice of patt * patt * ty
 | PattAs of patt * patt * ty

 | PattExpr of exp * ty
 | PattRange of exp * exp * ty
 | PattWhen of patt * exp * ty

   (* Type constraint *)
 | PattConstrain of patt * ty

(*
 * Strongly-typed expressions.
 *)
and exp = exp_core delayed_subst

and exp_core =
   (* Lambda calculus *)
   Number of int * ty
 | Char of char * ty
 | String of string * ty

 | Var of var_name * ty
 | Const of ty_var * const_var * exp list * ty * ty

 | Apply of exp * ty * exp list * ty
 | Lambda of ty_var list * patt list * exp * ty
 | Function of ty_var list * guarded * ty
 | Let of lets * exp * ty
 | LetRec of letrecs * exp * ty
 | If of exp * exp * exp * ty
 | For of var * exp * bool * exp * exp
 | While of exp * exp
 | Constrain of exp * ty

   (* Type application *)
 | ApplyType of exp * ty list * ty

   (* Aggregates *)
 | Tuple of exp list * ty
 | Array of exp list * ty
 | ArraySubscript of exp * exp * ty
 | ArraySetSubscript of exp * exp * exp
 | StringSubscript of exp * exp
 | StringSetSubscript of exp * exp * exp
 | Record of record_item list * ty
 | RecordProj of exp * ty * label * ty
 | RecordSetProj of exp * ty * label * exp
 | RecordUpdate of exp * record_item list * ty

   (* Statement composition *)
 | Sequence of exp * exp * ty

   (* Control flow *)
 | Match of exp * guarded * ty
 | Try of exp * guarded * ty
 | Raise of exp * ty

   (* Ambient calculus *)
 | In of me_name * ty
 | Out of me_name * ty
 | Open of me_name * ty
 | Action of exp list * ty
 | Restrict of me_var * ty * exp * ty
 | Ambient of me_var * module_exp * ty
 | Thread of exp
 | Migrate of exp * exp * ty
 | LetModule of me_var * module_exp * exp * ty

(* Info for a record creation *)
and record_item = label * exp

(* Variable definitions *)
and lets = (patt * exp * ty) list

(*
 * Function definitions.
 * Functions take type arguments as well as pattern arguments.
 * The first type is the internal monomorphic type.
 * The second type is the quantified universal type.
 *)
and letrecs = (var * exp * ty * ty) list

(*
 * Type definitions.
 *)
and types = (var * ty_var list * ty option) list

(*
 * Guarded commands are used in pattern matching,
 * (like a match e with
 *            p1 -> e1
 *            ...
 *)
and guarded = (patt * exp) list

(************************************************************************
 * MODULES                                                              *
 ************************************************************************)

(*
 * In a module expression, each item has an internal and
 * an external name.  For lets, funs, and types, the map
 * between internal and external is given through a substitution.
 * For the others, both variable names are given.
 *)
and module_field_core =
   FieldLet          of lets
 | FieldLetRec       of letrecs
 | FieldType         of types
 | FieldExpr         of exp
 | FieldRestrict     of me_var * ty
 | FieldModule       of me_var * module_exp * ty
 | FieldModuleType   of me_var * ty
 | FieldException    of const_var * ty_var * ty list
 | FieldExternal     of var * ty * string

and module_field = module_field_core simple_subst

and module_exp = module_exp_core delayed_subst

and module_exp_core =
   ModuleExpStruct    of me_var * module_field list * module_names * ty
 | ModuleExpVar       of me_name * ty
 | ModuleExpConstrain of module_exp * ty
 | ModuleExpFunctor   of me_var * ty * module_exp * ty
 | ModuleExpApply     of module_exp * me_name * ty

(************************************************************************
 * PROGRAMS
 ************************************************************************)

(*
 * Hash codes top label programs when they
 * are stored in files.
 *)
type hash = int

(*
 * Boot types.
 * We have to know the definitons of unit, bool, and exn,
 * since we use them extensively.
 *)
type boot =
   { boot_ty_unit     : ty_core;
     boot_ty_unit_var : ty_var;
     boot_unit_var    : const_var;

     boot_ty_bool     : ty_core;
     boot_ty_bool_var : ty_var;
     boot_true_var    : const_var;
     boot_false_var   : const_var;

     boot_ty_exn      : ty_core;
     boot_ty_exn_var  : ty_var
   }

(*
 * A program is either an interface or implementation.
 *)
type prog_exp =
   Interface of ty
 | Implementation of me_var * module_field list * module_names * ty

(*
 * Program:
 *    prog_names: map from internal to external names
 *    prog_types: top-level type definitions
 *
 *    prog_import: external modules
 *       the hash is a version number
 *       the me_var is the internal name of the module
 *       the ty is the type of the module
 *
 *    prog_items: the top-level struct definitions in the program
 *
 *    prog_ty_unit
 *    prog_ty_bool
 *    prog_ty_exn: names of pervasive types
 *)
type prog =
   { prog_name    : me_var;
     prog_names   : ext_var SymbolTable.t;
     prog_types   : tydef SymbolTable.t;
     prog_import  : (hash * me_var * ty) SymbolTable.t;
     prog_boot    : boot;
     prog_exp     : prog_exp
   }

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
