(*
 * File IO for interfaces.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast
open Aml_tast_pos
open Aml_tast_env_type

(*
 * Load a file.
 * The return type is the type of the module that was
 * defined by the interface.
 *)
val load_interface : genv -> pos -> ext_var -> genv * me_var * ty

(*
 * Load the interface for a program.
 * The ty is the defalt type of the program;
 * use it if there is no interface.
 *)
val load_interface_default : genv -> pos -> ext_var -> ty -> genv * ty

(*
 * Save files.
 *)
val save_interface : prog -> unit
val save_implementation : prog -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
