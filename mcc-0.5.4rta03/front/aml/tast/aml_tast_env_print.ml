(*
 * Print the environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open Aml_tast
open Aml_tast_print
open Aml_tast_env_type

let pp_print_opens buf info =
   let { mt_opens = mt_opens;
         me_opens = me_opens;
         ty_opens = ty_opens;
         var_opens = var_opens;
         const_opens = const_opens;
         label_opens = label_opens
       } = info
   in
   let print_opens s opens =
      SymbolTable.iter (fun v me_name ->
            fprintf buf "@ open %s %a = %a" (**)
               s
               pp_print_symbol v
               pp_print_mod_name me_name) opens
   in
      print_opens "module type" mt_opens;
      print_opens "module" me_opens;
      print_opens "type" ty_opens;
      print_opens "val" var_opens;
      print_opens "const" const_opens;
      print_opens "label" label_opens

let pp_print_level buf level =
   let { genv_core = core;
         genv_opens = opens
       } = level
   in
      fprintf buf "@[<hv 3>Level: %a@ %a@]" (**)
         pp_print_opens opens
         pp_print_module_type core

let pp_print_genv buf genv =
   let { genv_types = types;
         genv_import = import;
         genv_current = current;
         genv_next = next
       } = genv
   in
      fprintf buf "@[<v 3>genv:";

      fprintf buf "@ @[<hv 3>Types:";
      SymbolTable.iter (fun v tydef ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_tydef tydef) types;

      fprintf buf "@]@ @[<hv 3>Import:";
      SymbolTable.iter (fun v (hash, me_var, ty) ->
            fprintf buf "@ @[<hv 0>@[<hv 3>%a = {@ hash = 0x%08x;@ me_var = %a;@ type = %a@]@ }@]" (**)
               pp_print_symbol v
               hash
               pp_print_symbol me_var
               pp_print_type ty) import;

      fprintf buf "@]@ @[<hv 3>Current:@ %a@]" pp_print_level current;
      List.iter (fun level ->
            fprintf buf "@ @[<hv 3>Next:@ %a@]" pp_print_level level) next;

      fprintf buf "@]"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
