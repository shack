(*
 * Rename all the variables in the program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Field_table

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn

module Pos = MakePos (struct let name = "Aml_tast_standardize" end)
open Pos

(************************************************************************
 * SUBSTITUTION
 ************************************************************************)

(*
 * Substitutions for all the different kinds of variables.
 *)
type subst =
   { subst_normal_vars : var SymbolTable.t;
     subst_type_vars : ty_var SymbolTable.t;
     subst_field_vars : ty_var SymbolTable.t;
     subst_fo_vars : ty_var SymbolTable.t;
     subst_so_vars : ty_var SymbolTable.t;
     subst_const_vars : const_var SymbolTable.t;
     subst_label_vars : label SymbolTable.t;
     subst_tag_vars : tag_var SymbolTable.t
   }

let empty_subst =
   { subst_normal_vars = SymbolTable.empty;
     subst_type_vars = SymbolTable.empty;
     subst_field_vars = SymbolTable.empty;
     subst_fo_vars = SymbolTable.empty;
     subst_so_vars = SymbolTable.empty;
     subst_const_vars = SymbolTable.empty;
     subst_label_vars = SymbolTable.empty;
     subst_tag_vars = SymbolTable.empty
   }

let subst_any_var subst pos v =
   try SymbolTable.find subst.subst_normal_vars v with
      Not_found ->
         try SymbolTable.find subst.subst_type_vars v with
            Not_found ->
               try SymbolTable.find subst.subst_field_vars v with
                  Not_found ->
                     try SymbolTable.find subst.subst_const_vars v with
                        Not_found ->
                           try SymbolTable.find subst.subst_label_vars v with
                              Not_found ->
                                 let pos = string_pos "subst_any_var" pos in
                                    raise (TAstException (pos, UnboundVar v))

let subst_add_normal_var subst v v' =
   { subst with subst_normal_vars = SymbolTable.add subst.subst_normal_vars v v' }

let subst_normal_var subst v =
   try SymbolTable.find subst.subst_normal_vars v with
      Not_found ->
         v

let subst_add_type_var subst v v' =
   { subst with subst_type_vars = SymbolTable.add subst.subst_type_vars v v' }

let subst_type_var subst v =
   try SymbolTable.find subst.subst_type_vars v with
      Not_found ->
         v

let subst_add_field_var subst v v' =
   { subst with subst_field_vars = SymbolTable.add subst.subst_field_vars v v' }

let subst_field_var subst v =
   try SymbolTable.find subst.subst_field_vars v with
      Not_found ->
         v

let subst_add_const_var subst v v' =
   { subst with subst_const_vars = SymbolTable.add subst.subst_const_vars v v' }

let subst_const_var subst v =
   try SymbolTable.find subst.subst_const_vars v with
      Not_found ->
         v

let subst_mem_const_var subst v =
   SymbolTable.mem subst.subst_const_vars v

let subst_add_label_var subst v v' =
   { subst with subst_label_vars = SymbolTable.add subst.subst_label_vars v v' }

let subst_label_var subst v =
   try SymbolTable.find subst.subst_label_vars v with
      Not_found ->
         v

let subst_add_tag_var subst v v' =
   { subst with subst_tag_vars = SymbolTable.add subst.subst_tag_vars v v' }

let subst_tag_var subst v =
   try SymbolTable.find subst.subst_tag_vars v with
      Not_found ->
         v

let subst_add_fo_var subst v v' =
   { subst with subst_fo_vars = SymbolTable.add subst.subst_fo_vars v v' }

let subst_fo_var subst v =
   try SymbolTable.find subst.subst_fo_vars v with
      Not_found ->
         v

let subst_add_so_var subst v v' =
   { subst with subst_so_vars = SymbolTable.add subst.subst_so_vars v v' }

let subst_so_var subst v =
   try SymbolTable.find subst.subst_so_vars v with
      Not_found ->
         v

(*
 * Make up new variables.
 *)
let subst_new_normal_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_normal_var subst v v' in
      v', subst

let subst_new_normal_vars subst vars =
   let vars' = List.map new_symbol vars in
   let subst = List.fold_left2 subst_add_normal_var subst vars vars' in
      vars', subst

let subst_new_type_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_type_var subst v v' in
      v', subst

let subst_new_field_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_field_var subst v v' in
      v', subst

let subst_new_const_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_const_var subst v v' in
      v', subst

let subst_new_fo_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_fo_var subst v v' in
      v', subst

let subst_new_fo_vars subst vars =
   let vars' = List.map new_symbol vars in
   let subst = List.fold_left2 subst_add_fo_var subst vars vars' in
      vars', subst

let subst_new_so_vars subst vars =
   let vars' =
      List.map (fun kind ->
            { kind with kind_value = new_symbol kind.kind_value }) vars
   in
   let subst =
      List.fold_left2 (fun subst { kind_value = v1 } { kind_value = v2 } ->
            subst_add_so_var subst v1 v2) subst vars vars'
   in
      vars', subst

let subst_new_tag_var subst v =
   let v' = new_symbol v in
   let subst = subst_add_tag_var subst v v' in
      v', subst

(************************************************************************
 * SUBST INITIALIZATION
 ************************************************************************)

(*
 * Rename all the constructor names in the union.
 *)
let subst_of_union subst fields =
   FieldTable.fold (fun subst const_var _ ->
         let const_var' = new_symbol const_var in
            subst_add_const_var subst const_var const_var') subst fields

(*
 * Rename all the labels in the record.
 *)
let subst_of_record subst fields =
   FieldTable.fold (fun subst label _ ->
         let label' = new_symbol label in
            subst_add_label_var subst label label') subst fields

(*
 * Rename all the fields in a module type.
 *)
let subst_of_module_type subst mt =
   let { mt_mt_types = mt_types;
         mt_me_vars = me_vars;
         mt_types = types;
         mt_vars = vars;
         mt_consts = consts
       } = mt
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            subst_add_field_var subst v (new_symbol v)) subst mt_types
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            subst_add_field_var subst v (new_symbol v)) subst types
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            subst_add_normal_var subst v (new_symbol v)) subst me_vars
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            subst_add_normal_var subst v (new_symbol v)) subst vars
   in
   let subst =
      SymbolTable.fold (fun subst v _ ->
            if subst_mem_const_var subst v then
               subst
            else
               subst_add_const_var subst v (new_symbol v)) subst consts
   in
      subst

(*
 * Build a new substitution by going through
 * all the type definitions and creating all the
 * new names.
 *)
let subst_of_prog prog =
   let { prog_types = types;
         prog_boot = boot
       } = prog
   in
   let subst = empty_subst in

   (* Rename all the top-level names *)
   let subst =
      SymbolTable.fold (fun subst v tydef ->
            let subst = subst_add_type_var subst v (new_symbol v) in
               match dest_tydef_inner_core tydef.tydef_inner with
                  TyDefLambda _ ->
                     subst
                | TyDefUnion (_, fields) ->
                     subst_of_union subst fields
                | TyDefRecord fields ->
                     subst_of_record subst fields
                | TyDefModule mt ->
                     subst_of_module_type subst mt) subst types
   in
      (* Make sure that exceptions are named with a common name *)
      subst

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Rename the name.
 *)
let rec standardize_mod_name subst name =
   match name with
      ModNameEnv me_var ->
         ModNameEnv (subst_normal_var subst me_var)
    | ModNameSelf (me_var, ext_var) ->
         ModNameSelf (subst_normal_var subst me_var, ext_var)
    | ModNameProj (mod_name, ext_var) ->
         ModNameProj (standardize_mod_name subst mod_name, ext_var)
    | ModNameApply (mod_name1, mod_name2) ->
         ModNameApply (standardize_mod_name subst mod_name1, standardize_mod_name subst mod_name2)

let standardize_var_name_aux subst_var subst name =
   match name with
      NameEnv v ->
         NameEnv (subst_var subst v)
    | NamePath (me_name, label) ->
         NamePath (standardize_mod_name subst me_name, label)

let standardize_normal_var_name = standardize_var_name_aux subst_normal_var
let standardize_const_var_name = standardize_var_name_aux subst_const_var
let standardize_label_var_name = standardize_var_name_aux subst_label_var

let rec standardize_type_name subst name =
   match name with
      TyNameSoVar v ->
         TyNameSoVar (subst_field_var subst v)
    | TyNameSelf (me_var, ext_var) ->
         TyNameSelf (subst_normal_var subst me_var, ext_var)
    | TyNamePath (me_name, label) ->
         TyNamePath (standardize_mod_name subst me_name, label)

let standardize_type_kind subst kind =
   { kind with kind_value = standardize_type_name subst kind.kind_value }

let standardize_type_kinds subst ty_names =
   List.map (standardize_type_kind subst) ty_names

(*
 * Standardize a type.
 *)
let rec standardize_type subst ty =
   let loc = loc_of_type ty in
   let ty = dest_type_core ty in
      make_type loc (standardize_type_core subst ty)

and standardize_type_core subst ty =
   match ty with
      TyInt
    | TyChar
    | TyString
    | TyFloat ->
         ty

    | TyFun (tyl, ty) ->
         TyFun (standardize_types subst tyl, standardize_type subst ty)
    | TyFunctor (v, t1, t2) ->
         let t1 = standardize_type subst t1 in
         let v', subst = subst_new_normal_var subst v in
         let t2 = standardize_type subst t2 in
            TyFunctor (v', t1, t2)
    | TyProd tl ->
         TyProd (standardize_types subst tl)
    | TyArray ty ->
         TyArray (standardize_type subst ty)
    | TyAll (vars, ty) ->
         let vars', subst = subst_new_fo_vars subst vars in
            TyAll (vars', standardize_type subst ty)
    | TyExternal (ty, s) ->
         TyExternal (standardize_type subst ty, s)
    | TyOpen (fields, v) ->
         TyOpen (List.map (fun (v, ty) -> v, standardize_type subst ty) fields, subst_normal_var subst v)
    | TyFormat (t1, t2, t3) ->
         TyFormat (standardize_type subst t1, standardize_type subst t2, standardize_type subst t3)

    | TyVar v ->
         TyVar (subst_fo_var subst v)
    | TySOVar (v, tyl) ->
         TySOVar (subst_so_var subst v, standardize_types subst tyl)
    | TyProject (ty_name, tyl) ->
         TyProject (standardize_type_name subst ty_name, standardize_types subst tyl)

    | TyApply (v, tyl1, tyl2) ->
         TyApply (subst_type_var subst v, standardize_types subst tyl1, standardize_type_kinds subst tyl2)
    | TyUnion (v, tyl1, tyl2) ->
         TyUnion (subst_type_var subst v, standardize_types subst tyl1, standardize_type_kinds subst tyl2)
    | TyRecord (v, tyl1, tyl2) ->
         TyRecord (subst_type_var subst v, standardize_types subst tyl1, standardize_type_kinds subst tyl2)
    | TyModule (v, tyl1, tyl2) ->
         TyModule (subst_type_var subst v, standardize_types subst tyl1, standardize_type_kinds subst tyl2)

(*
 * Map over a list.
 *)
and standardize_types subst tyl =
   List.map (standardize_type subst) tyl

(*
 * Optional type.
 *)
let standardize_type_opt subst ty_opt =
   match ty_opt with
      Some ty ->
         Some (standardize_type subst ty)
    | None ->
         None

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Standardize a record type.
 *)
let standardize_record_type subst fields =
   let fields =
      FieldTable.fold_index (fun fields label (mflag, ty) i ->
            let label = subst_label_var subst label in
            let ty = standardize_type subst ty in
               FieldTable.add_index fields label (mflag, ty) i) FieldTable.empty fields
   in
      TyDefRecord fields

(*
 * Standardize a union type.
 *)
let standardize_union_type subst kind fields =
   let fields =
      FieldTable.fold_index (fun fields label tyl i->
            let label = subst_const_var subst label in
            let tyl = standardize_types subst tyl in
               FieldTable.add_index fields label tyl i) FieldTable.empty fields
   in
      TyDefUnion (kind, fields)

(*
 * standardize the names.
 *)
let standardize_names subst names =
   let { names_mt    = mt_names;
         names_me    = me_names;
         names_ty    = ty_names;
         names_var   = var_names;
         names_const = const_names;
         names_label = label_names
       } = names
   in
   let mt_names =
      SymbolTable.fold (fun mt_names mt_var (mt_var', tyl) ->
            let mt_var = reintern mt_var in
            let mt_var' = subst_field_var subst mt_var' in
            let tyl = standardize_types subst tyl in
               SymbolTable.add mt_names mt_var (mt_var', tyl)) SymbolTable.empty mt_names
   in
   let me_names =
      FieldTable.fold_index (fun me_names v v' i ->
            let v = reintern v in
            let v' = subst_normal_var subst v' in
               FieldTable.add_index me_names v v' i) FieldTable.empty me_names
   in
   let ty_names =
      SymbolTable.fold (fun ty_names v v' ->
            let v = reintern v in
            let v' = subst_field_var subst v' in
               SymbolTable.add ty_names v v') SymbolTable.empty ty_names
   in
   let var_names =
      FieldTable.fold_index (fun var_names v v' i->
            let v = reintern v in
            let v' = subst_normal_var subst v' in
               FieldTable.add_index var_names v v' i) FieldTable.empty var_names
   in
   let const_names =
      SymbolTable.fold (fun const_names v v' ->
            let v = reintern v in
            let v' = subst_const_var subst v' in
               SymbolTable.add const_names v v') SymbolTable.empty const_names
   in
   let label_names =
      SymbolTable.fold (fun label_names v v' ->
            let v = reintern v in
            let v' = subst_label_var subst v' in
               SymbolTable.add label_names v v') SymbolTable.empty label_names
   in
      { names_mt = mt_names;
        names_me = me_names;
        names_ty = ty_names;
        names_var = var_names;
        names_const = const_names;
        names_label = label_names
      }

(*
 * Constructor field.
 *)
let standardize_const_field subst field =
   match field with
      ConstNormal tyl ->
         ConstNormal (standardize_type_kinds subst tyl)
    | ConstException tyl ->
         ConstException (standardize_types subst tyl)

(*
 * Standardize a module type.
 * The tables have to be remapped.
 *)
let standardize_module_type subst mt =
   let { mt_self = me_var;
         mt_mt_types = mt_types;
         mt_me_vars = me_vars;
         mt_types = types;
         mt_vars = vars;
         mt_consts = consts;
         mt_labels = labels;
         mt_names = names
       } = mt
   in
   let me_var' = new_symbol me_var in
   let subst = subst_add_normal_var subst me_var me_var' in
   let mt_types =
      SymbolTable.fold (fun mt_types v ty ->
            let v' = subst_field_var subst v in
            let ty = standardize_type subst ty in
               SymbolTable.add mt_types v' ty) SymbolTable.empty mt_types
   in
   let me_vars =
      SymbolTable.fold (fun me_vars v ty ->
            let v' = subst_normal_var subst v in
            let ty = standardize_type subst ty in
               SymbolTable.add me_vars v' ty) SymbolTable.empty me_vars
   in
   let types =
      SymbolTable.fold (fun types v (ty_vars, ty_opt) ->
            let v' = subst_field_var subst v in
            let ty_vars, subst = subst_new_fo_vars subst ty_vars in
            let ty_opt = standardize_type_opt subst ty_opt in
               SymbolTable.add types v' (ty_vars, ty_opt)) SymbolTable.empty types
   in
   let vars =
      SymbolTable.fold (fun vars v ty ->
            let v' = subst_normal_var subst v in
            let ty = standardize_type subst ty in
               SymbolTable.add vars v' ty) SymbolTable.empty vars
   in
   let consts =
      SymbolTable.fold (fun consts const_var (ty_var, arity, field) ->
            let const_var = subst_const_var subst const_var in
            let ty_var = subst_type_var subst ty_var in
            let field = standardize_const_field subst field in
               SymbolTable.add consts const_var (ty_var, arity, field)) SymbolTable.empty consts
   in
   let labels =
      SymbolTable.fold (fun labels v (ty_var, arity, tyl) ->
            let v = subst_label_var subst v in
            let ty_var = subst_type_var subst ty_var in
            let tyl = standardize_type_kinds subst tyl in
               SymbolTable.add labels v (ty_var, arity, tyl)) SymbolTable.empty labels
   in
   let names = standardize_names subst names in
   let mt =
      { mt_self = me_var';
        mt_mt_types = mt_types;
        mt_me_vars = me_vars;
        mt_types = types;
        mt_vars = vars;
        mt_consts = consts;
        mt_labels = labels;
        mt_names = names
      }
   in
      TyDefModule mt

(*
 * Standardize a type definition.
 *)
let standardize_tydef_inner_core subst tydef =
   match tydef with
      TyDefLambda ty ->
         TyDefLambda (standardize_type subst ty)
    | TyDefRecord fields ->
         standardize_record_type subst fields
    | TyDefUnion (kind, fields) ->
         standardize_union_type subst kind fields
    | TyDefModule mt ->
         standardize_module_type subst mt

(*
 * Standardize a type definition.
 *)
let standardize_tydef_inner subst inner =
   let loc = loc_of_tydef_inner inner in
   let inner = dest_tydef_inner_core inner in
      make_tydef_inner loc (standardize_tydef_inner_core subst inner)

(*
 * Standardize a type definition core.
 * We have to rename both sets of parameters.
 *)
let standardize_tydef subst core =
   let { tydef_fo_vars = fo_vars;
         tydef_so_vars = so_vars;
         tydef_inner = inner
       } = core
   in
   let fo_vars, subst = subst_new_fo_vars subst fo_vars in
   let so_vars, subst = subst_new_so_vars subst so_vars in
   let inner = standardize_tydef_inner subst inner in
      { tydef_fo_vars = fo_vars;
        tydef_so_vars = so_vars;
        tydef_inner = inner
      }

(************************************************************************
 * PATTERNS
 ************************************************************************)

(*
 * Standardize a pattern.
 *)
let rec standardize_patt subst patt =
   let loc = loc_of_patt patt in
   let subst, patt = standardize_patt_core subst (dest_patt_core patt) in
   let patt = make_patt loc patt in
      subst, patt

and standardize_patt_core subst patt =
   match patt with
      PattInt (i, ty) ->
         subst, PattInt (i, standardize_type subst ty)
    | PattChar (c, ty) ->
         subst, PattChar (c, standardize_type subst ty)
    | PattString (s, ty) ->
         subst, PattString (s, standardize_type subst ty)
    | PattWild ty ->
         subst, PattWild (standardize_type subst ty)
    | PattVar (v, ty) ->
         let v', subst = subst_new_normal_var subst v in
         let ty = standardize_type subst ty in
            subst, PattVar (v', ty)
    | PattTuple (pl, ty) ->
         let subst, pl = standardize_patts subst pl in
         let ty = standardize_type subst ty in
            subst, PattTuple (pl, ty)
    | PattRecord (fields, ty) ->
         let subst, fields =
            List.fold_left (fun (subst, fields) (label, p) ->
                  let label = subst_label_var subst label in
                  let subst, p = standardize_patt subst p in
                     subst, (label, p) :: fields) (subst, []) fields
         in
         let ty = standardize_type subst ty in
            subst, PattRecord (List.rev fields, ty)
    | PattArray (pl, ty) ->
         let subst, pl = standardize_patts subst pl in
         let ty = standardize_type subst ty in
            subst, PattArray (pl, ty)
    | PattConst (ty_var, const_var, pl, ty) ->
         let ty_var = subst_type_var subst ty_var in
         let const_var = subst_const_var subst const_var in
         let subst, pl = standardize_patts subst pl in
         let ty = standardize_type subst ty in
            subst, PattConst (ty_var, const_var, pl, ty)
    | PattChoice (p1, p2, ty) ->
         let subst, p1 = standardize_patt subst p1 in
         let subst, p2 = standardize_patt subst p2 in
         let ty = standardize_type subst ty in
            subst, PattChoice (p1, p2, ty)
    | PattAs (p1, p2, ty) ->
         let subst, p1 = standardize_patt subst p1 in
         let subst, p2 = standardize_patt subst p2 in
         let ty = standardize_type subst ty in
            subst, PattAs (p1, p2, ty)
    | PattExpr (e, ty) ->
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            subst, PattExpr (e, ty)
    | PattRange (e1, e2, ty) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let ty = standardize_type subst ty in
            subst, PattRange (e1, e2, ty)
    | PattWhen (p, e, ty) ->
         let subst, p = standardize_patt subst p in
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            subst, PattWhen (p, e, ty)
    | PattConstrain (p, ty) ->
         let subst, p = standardize_patt subst p in
         let ty = standardize_type subst ty in
            subst, PattConstrain (p, ty)

and standardize_patts subst pl =
   let subst, pl =
      List.fold_left (fun (subst, pl) p ->
            let subst, p = standardize_patt subst p in
               subst, p :: pl) (subst, []) pl
   in
      subst, List.rev pl

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

and standardize_exp subst e =
   let loc = loc_of_exp e in
   let e = dest_exp_core e in
      make_exp loc (standardize_exp_core subst e)

and standardize_exp_core subst e =
   match e with
      Number (i, ty) ->
         Number (i, standardize_type subst ty)
    | Char (c, ty) ->
         Char (c, standardize_type subst ty)
    | String (s, ty) ->
         String (s, standardize_type subst ty)
    | Var (v, ty) ->
         Var (standardize_normal_var_name subst v, standardize_type subst ty)
    | Const (ty_var, const_var, el, ty1, ty2) ->
         let ty_var = subst_type_var subst ty_var in
         let const_var = subst_const_var subst const_var in
         let el = standardize_exps subst el in
         let ty1 = standardize_type subst ty1 in
         let ty2 = standardize_type subst ty2 in
            Const (ty_var, const_var, el, ty1, ty2)
    | Apply (e, ty_fun, el, ty) ->
         let e = standardize_exp subst e in
         let ty_fun = standardize_type subst ty_fun in
         let el = standardize_exps subst el in
         let ty = standardize_type subst ty in
            Apply (e, ty_fun, el, ty)
    | Lambda (ty_vars, pl, e, ty) ->
         let ty_vars, subst = subst_new_fo_vars subst ty_vars in
         let subst, pl = standardize_patts subst pl in
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            Lambda (ty_vars, pl, e, ty)
    | Function (ty_vars, guarded, ty) ->
         let ty_vars, subst = subst_new_fo_vars subst ty_vars in
         let guarded = standardize_guarded subst guarded in
         let ty = standardize_type subst ty in
            Function (ty_vars, guarded, ty)
    | Let (lets, e, ty) ->
         let subst, lets = standardize_lets subst lets in
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            Let (lets, e, ty)
    | LetRec (letrecs, e, ty) ->
         let subst, letrecs = standardize_letrecs subst letrecs in
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            LetRec (letrecs, e, ty)
    | If (e1, e2, e3, ty) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let e3 = standardize_exp subst e3 in
         let ty = standardize_type subst ty in
            If (e1, e2, e3, ty)
    | For (v, e1, b, e2, e3) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let v', subst = subst_new_normal_var subst v in
         let e3 = standardize_exp subst e3 in
            For (v', e1, b, e2, e3)
    | While (e1, e2) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
            While (e1, e2)
    | Constrain (e, ty) ->
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            Constrain (e, ty)
    | ApplyType (e, tyl, ty) ->
         let e = standardize_exp subst e in
         let tyl = standardize_types subst tyl in
         let ty = standardize_type subst ty in
            ApplyType (e, tyl, ty)
    | Tuple (el, ty) ->
         let el = standardize_exps subst el in
         let ty = standardize_type subst ty in
            Tuple (el, ty)
    | Array (el, ty) ->
         let el = standardize_exps subst el in
         let ty = standardize_type subst ty in
            Array (el, ty)
    | ArraySubscript (e1, e2, ty) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let ty = standardize_type subst ty in
            ArraySubscript (e1, e2, ty)
    | ArraySetSubscript (e1, e2, e3) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let e3 = standardize_exp subst e3 in
            ArraySetSubscript (e1, e2, e3)
    | StringSubscript (e1, e2) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
            StringSubscript (e1, e2)
    | StringSetSubscript (e1, e2, e3) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let e3 = standardize_exp subst e3 in
            StringSetSubscript (e1, e2, e3)
    | Record (items, ty) ->
         let items =
            List.map (fun (label, e) ->
                  subst_label_var subst label, standardize_exp subst e) items
         in
         let ty = standardize_type subst ty in
            Record (items, ty)
    | RecordProj (e, ty1, l, ty2) ->
         let e = standardize_exp subst e in
         let ty1 = standardize_type subst ty1 in
         let l = subst_label_var subst l in
         let ty2 = standardize_type subst ty2 in
            RecordProj (e, ty1, l, ty2)
    | RecordSetProj (e1, ty, l, e2) ->
         let e1 = standardize_exp subst e1 in
         let ty = standardize_type subst ty in
         let l = subst_label_var subst l in
         let e2 = standardize_exp subst e2 in
            RecordSetProj (e1, ty, l, e2)
    | RecordUpdate (e, items, ty) ->
         let e = standardize_exp subst e in
         let items =
            List.map (fun (label, e) ->
                  subst_label_var subst label, standardize_exp subst e) items
         in
         let ty = standardize_type subst ty in
            RecordUpdate (e, items, ty)
    | Sequence (e1, e2, ty) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let ty = standardize_type subst ty in
            Sequence (e1, e2, ty)
    | Match (e, guarded, ty) ->
         let e = standardize_exp subst e in
         let guarded = standardize_guarded subst guarded in
         let ty = standardize_type subst ty in
            Match (e, guarded, ty)
    | Try (e, guarded, ty) ->
         let e = standardize_exp subst e in
         let guarded = standardize_guarded subst guarded in
         let ty = standardize_type subst ty in
            Try (e, guarded, ty)
    | Raise (e, ty) ->
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            Raise (e, ty)
    | In (name, ty) ->
         let name = standardize_mod_name subst name in
         let ty = standardize_type subst ty in
            In (name, ty)
    | Out (name, ty) ->
         let name = standardize_mod_name subst name in
         let ty = standardize_type subst ty in
            Out (name, ty)
    | Open (name, ty) ->
         let name = standardize_mod_name subst name in
         let ty = standardize_type subst ty in
            Open (name, ty)
    | Action (el, ty) ->
         let el = standardize_exps subst el in
         let ty = standardize_type subst ty in
            Action (el, ty)
    | Restrict (me_var, ty1, e, ty2) ->
         let me_var = subst_normal_var subst me_var in
         let ty1 = standardize_type subst ty1 in
         let ty2 = standardize_type subst ty2 in
         let e = standardize_exp subst e in
            Restrict (me_var, ty1, e, ty2)
    | Ambient (me_var, me, ty) ->
         let me_var = subst_normal_var subst me_var in
         let me = standardize_me subst me in
         let ty = standardize_type subst ty in
            Ambient (me_var, me, ty)
    | Thread e ->
         let e = standardize_exp subst e in
            Thread e
    | Migrate (e1, e2, ty) ->
         let e1 = standardize_exp subst e1 in
         let e2 = standardize_exp subst e2 in
         let ty = standardize_type subst ty in
            Migrate (e1, e2, ty)
    | LetModule (me_var, me, e, ty) ->
         let me_var', subst = subst_new_normal_var subst me_var in
         let me = standardize_me subst me in
         let e = standardize_exp subst e in
         let ty = standardize_type subst ty in
            LetModule (me_var', me, e, ty)

(*
 * Expression lists.
 *)
and standardize_exps subst el =
   List.map (standardize_exp subst) el

(*
 * Let definitions.
 *)
and standardize_lets subst lets =
   let subst, fields =
      List.fold_left (fun (subst, fields) (p, e, ty) ->
            let subst, p = standardize_patt subst p in
            let e = standardize_exp subst e in
            let ty = standardize_type subst ty in
            let fields = (p, e, ty) :: fields in
               subst, fields) (subst, []) lets
   in
      subst, List.rev fields

(*
 * Letrec definitions.
 * Do this in two phases: first rename the vars, then rename the values.
 *)
and standardize_letrecs subst fields =
   let subst =
      List.fold_left (fun subst (v, _, _, _) ->
            subst_add_normal_var subst v (new_symbol v)) subst fields
   in
   let fields =
      List.map (fun (v, e, ty1, ty2) ->
            let v = subst_normal_var subst v in
            let e = standardize_exp subst e in
            let ty1 = standardize_type subst ty1 in
            let ty2 = standardize_type subst ty2 in
               v, e, ty1, ty2) fields
   in
      subst, fields

(*
 * Type definitions.
 *)
and standardize_typedefs subst fields =
   let subst =
      List.fold_left (fun subst (v, _, _) ->
            subst_add_field_var subst v (new_symbol v)) subst fields
   in
   let fields =
      List.map (fun (v, ty_vars, ty_opt) ->
            let v = subst_field_var subst v in
            let ty_vars, subst = subst_new_fo_vars subst ty_vars in
            let ty_opt = standardize_type_opt subst ty_opt in
               v, ty_vars, ty_opt) fields
   in
      subst, fields

(*
 * Guarded commands.
 *)
and standardize_guarded subst guarded =
   List.map (fun (p, e) ->
         let subst, p = standardize_patt subst p in
         let e = standardize_exp subst e in
            p, e) guarded

(************************************************************************
 * MODULES
 ************************************************************************)

(*
 * Module expressions.
 *)
and standardize_me subst me =
   let loc = loc_of_module_exp me in
   let me = dest_module_exp_core me in
      make_module_exp loc (standardize_me_core subst me)

and standardize_me_core subst me =
   match me with
      ModuleExpStruct (me_var, fields, names, ty) ->
         let ty = standardize_type subst ty in
         let me_var' = new_symbol me_var in
         let subst = subst_add_normal_var subst me_var me_var' in
         let subst, fields = standardize_fields subst fields in
         let names = standardize_names subst names in
            ModuleExpStruct (me_var', fields, names, ty)
    | ModuleExpVar (me_name, ty) ->
         let me_name = standardize_mod_name subst me_name in
         let ty = standardize_type subst ty in
            ModuleExpVar (me_name, ty)
    | ModuleExpConstrain (me, ty) ->
         let me = standardize_me subst me in
         let ty = standardize_type subst ty in
            ModuleExpConstrain (me, ty)
    | ModuleExpFunctor (me_var, ty1, me, ty2) ->
         let me_var', subst = subst_new_normal_var subst me_var in
         let ty1 = standardize_type subst ty1 in
         let me = standardize_me subst me in
         let ty2 = standardize_type subst ty2 in
            ModuleExpFunctor (me_var', ty1, me, ty2)
    | ModuleExpApply (me, me_name, ty) ->
         let me = standardize_me subst me in
         let me_name = standardize_mod_name subst me_name in
         let ty = standardize_type subst ty in
            ModuleExpApply (me, me_name, ty)

(*
 * Module fields.
 *)
and standardize_field subst field =
   let loc = loc_of_field field in
   let field = dest_field_core field in
   let subst, field = standardize_field_core subst field in
   let field = make_field loc field in
      subst, field

and standardize_field_core subst field =
   match field with
      FieldLet lets ->
         let subst, lets = standardize_lets subst lets in
            subst, FieldLet lets
    | FieldLetRec letrecs ->
         let subst, letrecs = standardize_letrecs subst letrecs in
            subst, FieldLetRec letrecs
    | FieldType types ->
         let subst, types = standardize_typedefs subst types in
            subst, FieldType types
    | FieldExpr e ->
         let e = standardize_exp subst e in
            subst, FieldExpr e
    | FieldRestrict (me_var, ty) ->
         let me_var', subst = subst_new_normal_var subst me_var in
         let ty = standardize_type subst ty in
            subst, FieldRestrict (me_var', ty)
    | FieldModule (me_var, me, ty) ->
         let me_var', subst = subst_new_normal_var subst me_var in
         let me = standardize_me subst me in
         let ty = standardize_type subst ty in
            subst, FieldModule (me_var', me, ty)
    | FieldModuleType (mt_var, ty) ->
         let mt_var, subst = subst_new_field_var subst mt_var in
         let ty = standardize_type subst ty in
            subst, FieldModuleType (mt_var, ty)
    | FieldException (const_var, ty_var, tyl) ->
         let const_var, subst = subst_new_const_var subst const_var in
         let ty_var = subst_type_var subst ty_var in
         let tyl = standardize_types subst tyl in
            subst, FieldException (const_var, ty_var, tyl)
    | FieldExternal (v, ty, s) ->
         let v, subst = subst_new_normal_var subst v in
         let ty = standardize_type subst ty in
            subst, FieldExternal (v, ty, s)

and standardize_fields subst fields =
   let subst, fields =
      List.fold_left (fun (subst, fields) field ->
            let subst, field = standardize_field subst field in
               subst, field :: fields) (subst, []) fields
   in
      subst, List.rev fields

(*
 * Boot record.
 *)
let standardize_boot subst boot =
   let { boot_ty_unit     = ty_unit;
         boot_ty_unit_var = ty_unit_var;
         boot_unit_var    = unit_var;

         boot_ty_bool     = ty_bool;
         boot_ty_bool_var = ty_bool_var;
         boot_true_var    = true_var;
         boot_false_var   = false_var;

         boot_ty_exn      = ty_exn;
         boot_ty_exn_var  = ty_exn_var
       } = boot
   in
      { boot_ty_unit      = standardize_type_core subst ty_unit;
        boot_ty_unit_var  = subst_type_var subst ty_unit_var;
        boot_unit_var     = subst_const_var subst unit_var;

        boot_ty_bool      = standardize_type_core subst ty_bool;
        boot_ty_bool_var  = subst_type_var subst ty_bool_var;
        boot_true_var     = subst_const_var subst true_var;
        boot_false_var    = subst_const_var subst false_var;

        boot_ty_exn       = standardize_type_core subst ty_exn;
        boot_ty_exn_var   = subst_type_var subst ty_exn_var
      }

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Standardize the program exp.
 *)
let standardize_prog_exp subst exp =
   match exp with
      Interface ty ->
         Interface (standardize_type subst ty)
    | Implementation (me_var, fields, names, ty) ->
         let ty = standardize_type subst ty in
         let me_var' = new_symbol me_var in
         let subst = subst_add_normal_var subst me_var me_var' in
         let subst, fields = standardize_fields subst fields in
         let names = standardize_names subst names in
            Implementation (me_var', fields, names, ty)

(*
 * Standardize the program.
 *)
let standardize_prog_exn exn_var prog =
   let { prog_name    = name;
         prog_names   = names;
         prog_types   = types;
         prog_import  = import;
         prog_boot    = boot;
         prog_exp     = exp
       } = prog
   in
   let loc = create_loc name 0 0 0 0 in
   let pos = string_pos "standardize_prog" (loc_pos loc) in

   (* Build subst.  If exception var is specified, use it *)
   let subst = subst_of_prog prog in
   let subst =
      match exn_var with
         Some v ->
            let { boot_ty_exn_var = exn_var } = boot in
               subst_add_type_var subst exn_var v
       | None ->
            subst
   in

   (* Now standardize the prog *)
   let name = subst_normal_var subst name in
   let names =
      SymbolTable.fold (fun names v ext_var ->
            let v = subst_any_var subst pos v in
            let ext_var = reintern ext_var in
               SymbolTable.add names v ext_var) SymbolTable.empty names
   in
   let types =
      SymbolTable.fold (fun types v tydef ->
            let v = subst_type_var subst v in
            let tydef = standardize_tydef subst tydef in
               SymbolTable.add types v tydef) SymbolTable.empty types
   in
   let import =
      SymbolTable.fold (fun import v (hash, me_var, ty) ->
            let v = reintern v in
            let me_var = subst_normal_var subst me_var in
            let ty = standardize_type subst ty in
               SymbolTable.add import v (hash, me_var, ty)) SymbolTable.empty import
   in
   let boot = standardize_boot subst boot in
   let exp = standardize_prog_exp subst exp in
      { prog_name    = name;
        prog_names   = names;
        prog_types   = types;
        prog_import  = import;
        prog_boot    = boot;
        prog_exp     = exp
      }

let standardize_interface exn_var prog =
   standardize_prog_exn (Some exn_var) prog

let standardize_prog prog =
   standardize_prog_exn None prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
