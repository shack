(*
 * Delayed substitution operations.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Location

open Aml_tast
open Aml_tast_exn

(*
 * Free vars.
 *)
val free_vars_type        : ty          -> free_vars
val free_vars_type_list   : ty list     -> free_vars
val free_vars_tydef       : tydef       -> free_vars
val free_vars_tydef_inner : tydef_inner -> free_vars
val free_vars_exp         : exp         -> free_vars
val free_vars_module_exp  : module_exp  -> free_vars

val free_type_vars        : ty          -> ty_var list
val free_vars_prog        : prog        -> ty_var list

(*
 * Substitution.
 *)
val empty_subst          : subst

val subst_add_loc        : subst -> loc -> subst
val subst_add_fo_var     : subst -> ty_var -> ty -> subst
val subst_add_so_var     : subst -> ty_var -> ty_kind -> subst
val subst_add_me_var     : subst -> me_var -> me_name -> subst

val subst_type           : subst -> ty -> ty
val subst_type_kind      : subst -> ty_kind -> ty_kind
val subst_type_kinds     : subst -> ty_kind list -> ty_kind list
val subst_tydef          : subst -> tydef -> tydef
val subst_tydef_inner    : subst -> tydef_inner -> tydef_inner
val subst_exp            : subst -> exp -> exp
val subst_const_field    : subst -> const_field -> const_field
val subst_module_exp     : subst -> module_exp -> module_exp
val subst_field          : subst -> module_field -> module_field * subst
val subst_prog           : subst -> prog -> prog

val subst_mem_fo_var     : subst -> ty_var -> bool

(*
 * Destruction.
 *)
val dest_type_core        : ty -> ty_core
val dest_tydef_inner_core : tydef_inner -> tydef_inner_core
val dest_exp_core         : exp -> exp_core
val dest_patt_core        : patt -> patt_core
val dest_field_core       : module_field -> module_field_core
val dest_module_exp_core  : module_exp -> module_exp_core

(*
 * Wrap the core.
 *)
val make_type            : loc -> ty_core -> ty
val make_tydef_inner     : loc -> tydef_inner_core -> tydef_inner
val make_exp             : loc -> exp_core -> exp
val make_patt            : loc -> patt_core -> patt
val make_field           : loc -> module_field_core -> module_field
val make_module_exp      : loc -> module_exp_core -> module_exp

(*
 * Locations.
 *)
val loc_of_type          : ty -> loc
val loc_of_tydef_inner   : tydef_inner -> loc
val loc_of_exp           : exp -> loc
val loc_of_patt          : patt -> loc
val loc_of_field         : module_field -> loc
val loc_of_module_exp    : module_exp -> loc

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
