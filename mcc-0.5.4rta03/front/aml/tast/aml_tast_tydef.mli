(*
 * Type utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Location

open Aml_tast
open Aml_tast_pos
open Aml_tast_env_type

(*
 * Utilities.
 *)
val wrap_fun_type : loc -> ty list -> ty -> ty

(*
 * Type application.
 *)
val apply_type : genv -> pos -> ty_var -> ty list -> ty_kind list -> tydef_inner

val apply_union_type  : genv -> pos -> ty_var -> ty list -> ty_kind list -> union_type * union_fields
val apply_record_type : genv -> pos -> ty_var -> ty list -> ty_kind list -> record_fields
val apply_module_type : genv -> pos -> ty_var -> ty list -> ty_kind list -> module_type

(*
 * Type destructors.
 *)
val dest_union_type  : genv -> pos -> ty -> union_type * union_fields
val dest_record_type : genv -> pos -> ty -> record_fields
val dest_module_type : genv -> pos -> ty -> module_type

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
