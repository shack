(*
 * Type unification.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Location

open Aml_tast
open Aml_tast_pos

(*
 * Adding to subst.
 *)
val subst_equate_fo_vars             : subst -> ty_var -> ty_var -> loc -> subst

(*
 * Relocation.
 *)
val relocate_type                    : loc -> ty -> ty

(*
 * Lookup.
 *)
val subst_lookup_fo_var              : subst -> pos -> ty_var -> ty

val subst_lookup_fo_var_exn          : subst -> ty_var -> ty
val subst_lookup_fo_var_loc          : subst -> ty_var -> loc -> ty
val subst_lookup_fo_var_relocate     : subst -> ty_var -> loc -> ty
val subst_lookup_fo_var_relocate_exn : subst -> ty_var -> loc -> ty

(*
(*
 * Unification.
 *)
type sub

(*
 * Sets of parameters.
 *)
type penv

(*
 * Allocating parameters.
 *)
val empty_penv : penv
val penv_new_symbol_string : penv -> string -> ty_var * penv
val penv_new_symbol_ast : penv -> ext_var -> ty_var * penv
val penv_add_vars : penv -> ty_var list -> penv
val free_params : penv -> sub -> ty -> ty_var list

(*
 * Substitutions of type (ty_var -> ty).
 *)
val empty_subst : sub
val add_subst : sub -> ty_var -> ty -> sub
val add_expand : sub -> ty_var -> ty_var list -> sub
val equate_vars : sub -> ty_var -> ty_var -> loc -> sub

val lookup_notfound : sub -> ty_var -> ty
val lookup : string -> sub -> ty_var -> loc -> ty

(*
 * Substitution.
 *)
val expand_type : bool -> sub -> ty -> ty
val expand_field : bool -> sub -> module_field -> module_field

val locate_type : loc -> ty -> ty

(*
 * Type substitutions.
 *)
val standardize_type : ty -> ty_var list * ty list * ty
val standardize_apply_type : ty -> ty list * ty

(*
 * Bound unification.
 *)
val lookup_int           : sub -> ty_var -> int_set bound
val lookup_char          : sub -> ty_var -> char_set bound
val lookup_string        : sub -> ty_var -> string_set bound

val add_int_bound        : sub -> ty_var -> int -> sub
val add_char_bound       : sub -> ty_var -> char -> sub
val add_char_range_bound : sub -> ty_var -> char -> char -> sub
val add_string_bound     : sub -> ty_var -> string -> sub

exception Unify_error

val unify_int_bound      : sub -> int_set bound -> int_set bound -> sub
val unify_char_bound     : sub -> char_set bound -> char_set bound -> sub
val unify_string_bound   : sub -> string_set bound -> string_set bound -> sub
 *)

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
