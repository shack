(*
 * We define the variables abstractly.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol

module Var           : SymbolSig
module LabelVar      : SymbolSig
module ConstVar      : SymbolSig
module ModuleVar     : SymbolSig
module ParamVar      : SymbolSig

module VarTable      : Mc_map.McMap with type key = Var.symbol
module LabelTable    : Mc_map.McMap with type key = LabelVar.symbol
module ConstTable    : Mc_map.McMap with type key = ConstVar.symbol
module ModuleTable   : Mc_map.McMap with type key = ModuleVar.symbol
module ParamTable    : Mc_map.McMap with type key = ParamVar.symbol

module VarSet        : Mc_set.McSet with type elt = Var.symbol
module LabelSet      : Mc_set.McSet with type elt = LabelVar.symbol
module ConstSet      : Mc_set.McSet with type elt = ConstVar.symbol
module ModuleSet     : Mc_set.McSet with type elt = ModuleVar.symbol
module ParamSet      : Mc_set.McSet with type elt = ParamVar.symbol

(*
 * New symbols.
 *)
val new_symbol_ast_var   : Aml_ast.var -> Var.symbol
val new_symbol_ast_label : Aml_ast.var -> LabelVar.symbol
val new_symbol_ast_const : Aml_ast.var -> ConstVar.symbol
val new_symbol_ast_me    : Aml_ast.var -> ModuleVar.symbol
val new_symbol_ast_param : Aml_ast.var -> ParamVar.symbol

(*
 * Translate between var types.
 * THESE SHOULD NOT BE USED IN THE ambient/type DIRECTORY!
 *)
val var_of_label_var    : LabelVar.symbol -> Var.symbol
val var_of_const_var    : ConstVar.symbol -> Var.symbol
val var_of_me_var       : ModuleVar.symbol -> Var.symbol
val var_of_param_var    : ParamVar.symbol -> Var.symbol

val label_var_of_var    : Var.symbol -> LabelVar.symbol
val label_var_of_me_var : ModuleVar.symbol -> LabelVar.symbol

val me_var_of_var       : Var.symbol -> ModuleVar.symbol
val var_of_me_var       : ModuleVar.symbol -> Var.symbol
val ext_var_of_me_var   : ModuleVar.symbol -> Symbol.symbol

val ext_var_of_var      : Var.symbol -> Symbol.symbol

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
