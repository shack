(*
 * Initial environment.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Debug
open Interval_set

open Aml_type
open Aml_type_ds
open Aml_type_set
open Aml_type_type
open Aml_type_print
open Aml_type_symbol

let debug_type =
   create_debug (**)
      { debug_name = "type";
        debug_description = "print results of type inference";
        debug_value = false
      }

(*
 * Default location.
 *)
let zero_loc = "<pervasive>", 0, 0, 0, 0

(*
 * Base types.
 *)
let v1 = ParamVar.new_symbol_string "a"
let v2 = ParamVar.new_symbol_string "b"
let v3 = ParamVar.new_symbol_string "c"

let base_types me_var =
   ["int", TyInt (BoundSet IntSet.max_set, zero_loc);
    "char", TyChar (BoundSet CharSet.max_set, zero_loc);
    "string", TyString (BoundSet StringSet.max_set, zero_loc);
    "float", TyFloat zero_loc;
    "array", TyAll ([v1], wrap_core (TyArray (wrap_core (TyParam (v1, zero_loc)), zero_loc)), zero_loc);
    "sig_handle", TyAbstract ((ModNameEnv me_var, Symbol.Symbol.add "sig_handle"), zero_loc);
    "format", TyAll ([v1; v2; v3],
                     wrap_core (TyFormat (wrap_core (TyParam (v1, zero_loc)),
                                          wrap_core (TyParam (v2, zero_loc)),
                                          wrap_core (TyParam (v3, zero_loc)),
                                          zero_loc)), zero_loc)]

let base_union_types =
   ["unit", NormalUnion,   [],    ["Unit", []];
    "bool", NormalUnion,   [],    ["True", []; "False", []];
    "list", NormalUnion,   ["a"], ["Nil", []; "Cons", [["a"]; ["a"; "list"]]];
    "exn",  ExnUnion None, [],    ["Match_failure", [["string"]; ["int"]; ["int"]]];
    "signal", NormalUnion, [],    ["Signal_default", []; "Signal_ignore", []; "Signal_handle", [["sig_handle"]]]]

let base_record_types =
   ["ref", ["a"], [true, "contents", ["a"]]]

(*
 * Plain basic type.
 *)
let add_base_type tenv _ (name, ty) =
   let name = Symbol.Symbol.add name in
   let ty = wrap_core ty in
   let tenv = tenv_add_type tenv name ty in
      tenv, (name, ty)

(*
 * Build the type from the string list.
 *)
let rec build_type tenv me_var name ty_name vars args = function
   [v] ->
      if List.mem_assoc v vars && args = [] then
         TyParam (List.assoc v vars, zero_loc)
      else if v = name then
         TyApply ((ModNameEnv me_var, ty_name), List.rev args, zero_loc)
      else
         let ext_name = Symbol.Symbol.add v in
         let ty_name = tenv_intern_type tenv [ext_name] zero_loc in
            TyApply (ty_name, List.rev args, zero_loc)

 | v :: tl ->
      (try build_type tenv me_var name ty_name vars (wrap_core (TyParam (List.assoc v vars, zero_loc)) :: args) tl with
         Not_found ->
            raise (Invalid_argument ("Aml_type_pervasive.build_type2: " ^ v)))

 | [] ->
      raise (Invalid_argument "Aml_type_pervasive.build_type")

let build_type tenv me_var name ty_name vars ty =
   wrap_core (build_type tenv me_var name ty_name vars [] ty)

(*
 * Union type.
 *)
let add_union_type tenv me_var (name, kind, params, cases) =
   let ext_var = Symbol.Symbol.add name in
   let vars = List.map (fun v -> v, ParamVar.new_symbol_string v) params in

   (*
    * Add the constructor for the case,
    * and build the type.
    *)
   let add_case tenv (name', tyl) =
      let v_name = Symbol.Symbol.add name' in
      let c_name = new_symbol_ast_const v_name in
      let tenv = tenv_add_const tenv v_name c_name ext_var in
      let tyl = List.map (build_type tenv me_var name ext_var vars) tyl in
         tenv, (v_name, c_name, tyl)
   in
   let rec add_cases tenv = function
      hd :: tl ->
         let tenv, hd = add_case tenv hd in
         let tenv, tl = add_cases tenv tl in
            tenv, hd :: tl
    | [] ->
         tenv, []
   in
   let tenv, fields = add_cases tenv cases in
   let ty = TyUnion (kind, fields, zero_loc) in
   let ty =
      if vars = [] then
         ty
      else
         TyAll (List.map snd vars, wrap_core ty, zero_loc)
   in
   let ty = wrap_core ty in
   let tenv = tenv_add_type tenv ext_var ty in
      tenv, (ext_var, ty)

(*
 * Add a record type.
 *)
let add_record_type tenv me_var (name, params, fields) =
   let ext_var = Symbol.Symbol.add name in
   let vars = List.map (fun v -> v, ParamVar.new_symbol_string v) params in

   let add_field tenv (mflag, name', ty) =
      let v_name = Symbol.Symbol.add name' in
      let l_name = new_symbol_ast_label v_name in
      let tenv = tenv_add_label tenv v_name l_name ext_var in
      let ty = build_type tenv me_var name ext_var vars ty in
         tenv, (mflag, v_name, l_name, ty)
   in
   let rec add_fields tenv = function
      hd :: tl ->
         let tenv, hd = add_field tenv hd in
         let tenv, tl = add_fields tenv tl in
            tenv, hd :: tl
    | [] ->
         tenv, []
   in
   let tenv, fields = add_fields tenv fields in
   let ty = TyRecord (fields, zero_loc) in
   let ty =
      if vars = [] then
         ty
      else
         TyAll (List.map snd vars, wrap_core ty, zero_loc)
   in
   let ty = wrap_core ty in
   let tenv = tenv_add_type tenv ext_var ty in
      tenv, (ext_var, ty)

(*
 * Add the base types.
 *)
let rec fold_type f types tenv me_var = function
   h :: t ->
      let tenv, ty = f tenv me_var h in
         fold_type f (ty :: types) tenv me_var t
 | [] ->
      tenv, types

let add_base_types tenv me_var =
   let tenv, types = fold_type add_base_type [] tenv me_var (base_types me_var) in
   let tenv, types = fold_type add_union_type types tenv me_var base_union_types in
   let tenv, types = fold_type add_record_type types tenv me_var base_record_types in
      tenv, List.rev types

(*
 * Pervasives.
 *)
let pervasive_me_ext, pervasive_me_var, pervasive_tenv, pervasive_me =
   (* Construct the module type *)
   let me_var, tenv = tenv_push empty_tenv in
   let tenv, types = add_base_types tenv me_var in
   let _, mt_mod, tenv = tenv_pop tenv in
   let mt_mod_type = wrap_core (TransModule (me_var, mt_mod, zero_loc)) in

   (* Add the definitions *)
   let me_var = ModuleVar.new_symbol_string "Pervasives_boot" in
   let ext_var = Symbol.Symbol.add "Pervasives_boot" in
   let tenv = tenv_add_module empty_tenv ext_var me_var mt_mod_type in
   let _ = add_pervasive me_var mt_mod in

   (* Define the initial module *)
   let field' = FieldType (types, zero_loc) in
   let exp = wrap_core (ModuleExpStruct ([field'], mt_mod_type, zero_loc)) in
   let field = FieldModule (ext_var, me_var, exp, mt_mod_type, zero_loc) in
      if !debug_type then
         pp_debug_module_field Format.err_formatter "TYPE_pervasives" field;
      ext_var, me_var, tenv, field'

(*
 * Units and booleans.
 *)
let path_name s =
   [pervasive_me_ext; Symbol.Symbol.add s]

let unit_name =
   tenv_intern_type pervasive_tenv (path_name "unit") zero_loc

let bool_name =
   tenv_intern_type pervasive_tenv (path_name "bool") zero_loc

let exn_name =
   tenv_intern_type pervasive_tenv (path_name "exn") zero_loc

let array_name =
   tenv_intern_type pervasive_tenv (path_name "array") zero_loc

let list_name =
   tenv_intern_type pervasive_tenv (path_name "list") zero_loc

let ref_name =
   tenv_intern_type pervasive_tenv (path_name "ref") zero_loc

let signal_name =
   tenv_intern_type pervasive_tenv (path_name "signal") zero_loc

let sig_handle_name =
   tenv_intern_type pervasive_tenv (path_name "sig_handle") zero_loc

let format_name =
   tenv_intern_type pervasive_tenv (path_name "format") zero_loc

let ty_unit loc =
   wrap_core (TyApply (unit_name, [], loc))

let ty_bool loc =
   wrap_core (TyApply (bool_name, [], loc))

let ty_exn loc =
   wrap_core (TyApply (exn_name, [], loc))

let ty_array ty loc =
   wrap_core (TyApply (array_name, [ty], loc))

let ty_list ty loc =
   wrap_core (TyApply (list_name, [ty], loc))

let ty_ref ty loc =
   wrap_core (TyApply (ref_name, [ty], loc))

let ty_signal loc =
   wrap_core (TyApply (signal_name, [], loc))

let ty_sig_handle loc =
   wrap_core (TyApply (sig_handle_name, [], loc))

let ty_format t1 t2 t3 loc =
   wrap_core (TyApply (format_name, [t1; t2; t3], loc))

let const_match_failure =
   tenv_intern_const pervasive_tenv (path_name "Match_failure") zero_loc

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
