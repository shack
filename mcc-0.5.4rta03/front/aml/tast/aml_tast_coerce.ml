(*
 * Module coercions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

(************************************************************************
 * MODULE-LEVEL MATCHING                                                *
 ************************************************************************)

(*
 * Instantiate a polymorphic variable.
 *)
let coerce_var subst genv var_name t1 t2 loc =
   let t1 = subst_type subst t1 in
   let t2 = subst_type subst t2 in
      if !debug_unify then
         Format.eprintf "@[<hv 3>Coerce_var:@ @[<hv 3>Type1:@ %a@]@ @[<hv 3>Type2:@ %a@]@]@." (**)
            pp_print_type t1
            pp_print_type t2;
      match dest_type_core t1, dest_type_core t2 with
         TyAll (vars1, ty1, _), TyAll (vars2, ty2, loc2) ->
            let subst = unify_type "coerce_var" genv subst ty2 ty1 in
            let ty2 = subst_type subst ty2 in
            let ty = wrap_core (TyAll (vars1, ty2, loc2)) in
            let ty_args =
               List.map (fun v -> subst_type subst (wrap_core (TyVar (v)))) vars2
            in
            let exp =
               wrap_core (TyApply (wrap_core (Var (var_name, t1)), ty_args, ty2))
            in
               wrap_core (TLambda (vars1, exp, ty)), ty
       | _, TyAll (vars2, t2, loc2) ->
            let subst = unify_type "coerce_var" genv subst t1 t2 in
            let ty = subst_type subst t2 in
            let ty_args =
               List.map (fun v -> subst_type subst (wrap_core (TyVar (v)))) vars2
            in
            let exp =
               wrap_core (TyApply (wrap_core (Var (var_name, t2)), ty_args, ty))
            in
               exp, ty
       | _ ->
            match_type "coerce_var" genv subst t1 t2;
            wrap_core (Var (var_name, t2)), t2

(*
 * Recursively match all module types.
 *)
let match_trans_type debug arg_genv subst arg_mt1 arg_mt2 loc =
   let rec match_module_cores genv me_var core1 core2 =
      let { trans_module_types = { mt_table = mt_table1 };
            trans_types        = { ty_table = ty_table1 };
            trans_modules      = { me_table = me_table1; me_intern = me_intern1 };
            trans_vars         = { var_table = var_table1; var_intern = var_intern1 };
            trans_consts       = { const_table = const_table1; const_intern = const_intern1 }
          } = core1
      in
      let { trans_module_types = { mt_table = mt_table2 };
            trans_types        = { ty_table = ty_table2 };
            trans_modules      = { me_table = me_table2; me_intern = me_intern2 };
            trans_vars         = { var_table = var_table2; var_intern = var_intern2 };
            trans_consts       = { const_table = const_table2; const_intern = const_intern2 }
          } = core2
      in

      (*
       * Add the parts of both modules to the environment.
       *)
      let genv =
         let core =
            { trans_module_types = { mt_table = mt_table2 };
              trans_modules      = { me_table = me_table2; me_intern = ExtTable.empty };
              trans_types        = { ty_table = ty_table2 };
              trans_vars         = { var_table = VarTable.empty; var_intern = ExtTable.empty };
              trans_consts       = { const_table = ConstTable.empty; const_intern = ExtTable.empty };
              trans_labels       = { label_table = LabelTable.empty; label_intern = ExtTable.empty }
            }
         in
            genv_push_core genv me_var core
      in
         (*
          * All module types in mt1 must exist in mt2, and their types must match.
          *)
         ExtTable.iter (fun ext_var mt1 ->
               let mt2 =
                  try ExtTable.find mt_table2 ext_var with
                     Not_found ->
                        raise (TAstException (MissingModuleType (ext_var)))
               in
                  match_trans_types genv mt1 mt2) mt_table1;

         (*
          * All modules in mt1 must exist in mt2, and their types must match.
          *)
         ExtTable.iter (fun ext_var me_var1 ->
               let mt1 = ModuleTable.find me_table1 me_var1 in
               let mt2 =
                  try
                     let me_var2 = ExtTable.find me_intern2 ext_var in
                        ModuleTable.find me_table2 me_var2
                  with
                     Not_found ->
                        raise (TAstException (MissingModule (ext_var)))
               in
                  match_trans_types genv mt1 mt2) me_intern1;

         (*
          * All types in mt1 must exist in mt2, and their values must match.
          *)
         ExtTable.iter (fun ext_var ty1 ->
               let ty2 =
                  try ExtTable.find ty_table2 ext_var with
                     Not_found ->
                        raise (TAstException (MissingType (ext_var)))
               in
                  match_type debug genv subst ty1 ty2) ty_table1;

         (*
          * All values in mt1 must exist in mt2, and their types must match.
          *)
         ExtTable.iter (fun ext_var v1 ->
               let ty1 = VarTable.find var_table1 v1 in
               let ty2 =
                  try
                     let v2 = ExtTable.find var_intern2 ext_var in
                        VarTable.find var_table2 v2
                  with
                     Not_found ->
                        raise (TAstException (MissingVar (ext_var)))
               in
                  match_type debug genv subst ty1 ty2 ) var_intern1;

         (*
          * All constructors in mt1 must exist in mt2, and their types must match.
          *)
         ExtTable.iter (fun ext_var const_var1 ->
               let _, ty_var1 = ConstTable.find const_table1 const_var1 in
               let ty_var2 =
                  try
                     let const_var2 = ExtTable.find const_intern2 ext_var in
                     let _, ty_var2 = ConstTable.find const_table2 const_var2 in
                        ty_var2
                  with
                     Not_found ->
                        raise (TAstException (MissingConst (ext_var)))
               in
                  if ty_var1 <> ty_var2 then
                     raise (TAstException (ConstMismatch (ty_var1, ty_var2)))) const_intern1

   and match_trans_types genv mt1 mt2 =
      match dest_trans_type_core mt1, dest_trans_type_core mt2 with
         TransModule (me_var1, core1, _), TransModule (me_var2, core2, _) ->
            let subst = subst_add_me_var Aml_tast_ds.empty_subst me_var1 (ModNameEnv me_var2) in
            let core1 = subst_trans_core subst core1 in
               match_module_cores genv me_var2 core1 core2

       | TransFun (me_var1, mt1a, mt1b, _), TransFun (me_var2, mt2a, mt2b, _) ->
            match_trans_types genv mt1a mt2a;
            let genv = genv_add_internal_module genv me_var1 mt1a in
               match_trans_types genv mt1b mt2b

       | _ ->
            raise (TAstException (ModuleArityMismatch loc))
   in
      match_trans_types arg_genv arg_mt1 arg_mt2

(*
 * Recursively coerce all the variables and modules.
 * This may cause polymorphic instantiation.
 *)
let coerce_trans_type debug arg_genv subst arg_mt1 arg_me arg_mt2 loc =
   (*
    * Match the two cores.
    * The ext_vars is the path to core2.
    *)
   let rec coerce_module_cores genv me me_var1 core1 me_var2 core2 cont =
      let { trans_module_types = { mt_table = mt_table1 };
            trans_types        = { ty_table = ty_table1 };
            trans_modules      = { me_table = me_table1; me_intern = me_intern1 };
            trans_vars         = { var_table = var_table1; var_intern = var_intern1 };
            trans_consts       = { const_table = const_table1; const_intern = const_intern1 }
          } = core1
      in
      let { trans_module_types = { mt_table = mt_table2 };
            trans_types        = { ty_table = ty_table2 };
            trans_modules      = { me_table = me_table2; me_intern = me_intern2 };
            trans_vars         = { var_table = var_table2; var_intern = var_intern2 };
            trans_consts       = { const_table = const_table2; const_intern = const_intern2 }
          } = core2
      in

      (*
       * Wrap in a constraint.
       *)
      let me_var = ModuleVar.new_symbol_string "me" in
      let mod_name = ModNameEnv me_var in

      (*
       * Add the parts of both modules to the environment.
       *)
      let genv =
         let core =
            { trans_module_types = { mt_table = mt_table2 };
              trans_modules      = { me_table = me_table2; me_intern = ExtTable.empty };
              trans_types        = { ty_table = ty_table2 };
              trans_vars         = { var_table = VarTable.empty; var_intern = ExtTable.empty };
              trans_consts       = { const_table = ConstTable.empty; const_intern = ExtTable.empty };
              trans_labels       = { label_table = LabelTable.empty; label_intern = ExtTable.empty }
            }
         in
            genv_push_core genv me_var core
      in

      (*
       * All module types in mt1 must exist in mt2, and their types must match.
       * All the module types from core2 are added as fields.
       *)
      let mt_fields, mt_table3 =
         ExtTable.fold (fun (fields, mt_table3) ext_var trans_type1 ->
               let trans_type2 =
                  try ExtTable.find mt_table2 ext_var with
                     Not_found ->
                        raise (TAstException (MissingModuleType (ext_var)))
               in
               let _ = match_trans_type debug genv subst trans_type1 trans_type2 in
               let mt_table3 = ExtTable.add mt_table3 ext_var trans_type1 in
               let field = FieldModuleType (ext_var, trans_type1) in
                  field :: fields, mt_table3) (**)
            ([], ExtTable.empty) mt_table1
      in

      (*
       * All types in mt1 must exist in mt2, and their values must match.
       *)
      let ty_fields, ty_table3 =
         ExtTable.fold (fun (fields, ty_table3) ext_var ty1 ->
               let ty2 =
                  try ExtTable.find ty_table2 ext_var with
                     Not_found ->
                        raise (TAstException (MissingType (ext_var)))
               in
               let _ =
                  try match_type debug genv subst ty1 ty2 with
                     TAstException (TypeMismatch _) ->
                        raise (TAstException (ExtTypeMismatch (ext_var, ty1, ty2)))
               in
               let ty_table3 = ExtTable.add ty_table3 ext_var ty1 in
               let field = FieldType ([ext_var, ty1]) in
                  field :: fields, ty_table3) (**)
            ([], ExtTable.empty) ty_table1
      in

      (*
       * All modules in mt1 must exist in mt2, and recursively
       * coerce them.
       *)
      let me_fields, me_intern3, me_table3 =
         ExtTable.fold (fun (fields, me_intern3, me_table3) ext_var me_var1 ->
               let mt1 = ModuleTable.find me_table1 me_var1 in
               let me_var2, mt2 =
                  try
                     let me_var2 = ExtTable.find me_intern2 ext_var in
                        me_var2, ModuleTable.find me_table2 me_var2
                  with
                     Not_found ->
                        raise (TAstException (MissingModule (ext_var)))
               in
               let me = wrap_core (ModuleExpVar (ModNameProj (mod_name, ext_var), mt2)) in
               let me, mt = coerce_trans_types genv me mt1 mt2 (fun _ me mt -> me, mt) in
               let me_intern3 = ExtTable.add me_intern3 ext_var me_var2 in
               let me_table3 = ModuleTable.add me_table3 me_var2 mt in
               let field = FieldModule (ext_var, me_var2, me, mt1) in
                  field :: fields, me_intern3, me_table3) (**)
            ([], ExtTable.empty, ModuleTable.empty) me_intern1
      in

      (*
       * All values in mt1 must exist in mt2, but they may undergo polymorphic
       * instantiation.
       *)
      let var_fields, var_intern3, var_table3 =
         ExtTable.fold (fun (fields, var_intern3, var_table3) ext_var v1 ->
               let ty1 = VarTable.find var_table1 v1 in
               let v2, ty2 =
                  try
                     let v2 = ExtTable.find var_intern2 ext_var in
                        v2, VarTable.find var_table2 v2
                  with
                     Not_found ->
                        raise (TAstException (MissingVar (ext_var)))
               in
               let exp, ty =
                  try coerce_var subst genv (NamePath (mod_name, ext_var)) ty1 ty2 loc with
                     TAstException (TypeMismatch _) ->
                        raise (TAstException (ExtTypeMismatch (ext_var, ty1, ty2)))
               in
               let lets = [PattVar (v2, ext_var, ty), exp, ty] in
               let field = FieldLet (lets) in
               let var_intern3 = ExtTable.add var_intern3 ext_var v2 in
               let var_table3 = VarTable.add var_table3 v2 ty in
                  field :: fields, var_intern3, var_table3) (**)
            ([], ExtTable.empty, VarTable.empty) var_intern1
      in

      (*
       * All constructors in mt1 must exist in mt2, and their types must match.
       *)
      let const_intern3, const_table3 =
         ExtTable.fold (fun (const_intern3, const_table3) ext_var const_var1 ->
               let _, ty_var1 = ConstTable.find const_table1 const_var1 in
               let const_var2, ty_var2 =
                  try
                     let const_var2 = ExtTable.find const_intern2 ext_var in
                     let _, ty_var2 = ConstTable.find const_table2 const_var2 in
                        const_var2, ty_var2
                  with
                     Not_found ->
                        raise (TAstException (MissingConst (ext_var)))
               in
               let ty1 = ExtTable.find ty_table1 ty_var1 in
               let ty2 = ExtTable.find ty_table2 ty_var2 in
               let _ = match_type debug genv subst ty1 ty2 in
               let const_intern3 = ExtTable.add const_intern3 ext_var const_var2 in
               let const_table3 = ConstTable.add const_table3 const_var2 (ext_var, ty_var2) in
                  const_intern3, const_table3) (**)
            (ExtTable.empty, ConstTable.empty) const_intern1
      in

      (*
       * Build the module type.
       *)
      let fields = (List.rev mt_fields) @ (List.rev me_fields) @ (List.rev ty_fields) @ (List.rev var_fields) in
      let core3 =
         { core2 with trans_module_types = { mt_table = mt_table3 };
                      trans_modules = { me_intern = me_intern3; me_table = me_table3 };
                      trans_types = { ty_table = ty_table3 };
                      trans_vars = { var_intern = var_intern3; var_table = var_table3 };
                      trans_consts = { const_intern = const_intern3; const_table = const_table3 }
         }
      in
      let mt3 = wrap_core (TransModule (me_var2, core3)) in
      let mt1 = wrap_core (TransModule (me_var1, core1)) in
      let me', mt' = cont genv (wrap_core (ModuleExpStruct (fields, mt3))) mt1 in
         wrap_core (ModuleExpConstrain (me_var, me, me', mt')), mt1

   (*
    * Coerce me_var of type mt2 to type mt1.
    *)
   and coerce_trans_types genv me mt1 mt2 cont =
      match dest_trans_type_core mt1, dest_trans_type_core mt2 with
         TransFun (v1, mt1a, mt1b, _), TransFun (v2, mt2a, mt2b, _) ->
            let genv = genv_add_internal_module genv v1 mt1a in
            let me1, _ =
               coerce_trans_types genv (wrap_core (ModuleExpVar (ModNameEnv v1, mt1a))) mt2a mt1a (fun mt0 (me3 : module_exp) (mt3 : trans_type) ->
                     let genv = genv_add_internal_module genv v2 mt2a in
                     let me3', _ = coerce_trans_types genv (wrap_core (ModuleExpApply (me, ModNameEnv v2, mt2b))) mt1b mt2b cont in
                        wrap_core (ModuleExpConstrain (v2, me3, me3', mt1b)), mt1b)
            in
               wrap_core (ModuleExpFunctor (v1, mt1a, me1, mt1)), mt1
       | TransModule (me_var1, core1, _), TransModule (me_var2, core2, _) ->
            coerce_module_cores genv me me_var1 core1 me_var2 core2 cont
       | _ ->
            raise (TAstException (ModuleArityMismatch loc))
   in
   let me, _ = coerce_trans_types arg_genv arg_me arg_mt1 arg_mt2 (fun _ me mt -> me, mt) in
      me

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
