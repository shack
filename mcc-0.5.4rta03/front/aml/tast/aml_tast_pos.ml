(*
 * Position information for debugging.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fir_state

open Location

open Aml_tast
open Aml_tast_ds
open Aml_tast_exn
open Aml_tast_print

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * The pos type is used to provide info for debugging
 * and error messages.
 *)
type item =
   Exp           of exp
 | String        of string
 | Type          of ty
 | Patt          of patt

 | AstType       of Aml_ast.ty
 | AstPatt       of Aml_ast.patt
 | AstExp        of Aml_ast.exp
 | AstModuleType of Aml_ast.module_type
 | AstModule     of Aml_ast.module_exp
 | AstStrItem    of Aml_ast.str_item

type pos = item Position.pos

(*
 * General exception includes debugging info.
 *)
exception TAstException of pos * tast_error

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the source location for an exception.
 *)
let var_loc = bogus_loc "<Aml_tast_pos>"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_exp e
    | Type ty ->
         loc_of_type ty
    | Patt p ->
         loc_of_patt p
    | String _ ->
         var_loc
    | AstType ty ->
         Aml_ast_loc.loc_of_type ty
    | AstPatt p ->
         Aml_ast_loc.loc_of_patt p
    | AstExp e ->
         Aml_ast_loc.loc_of_exp e
    | AstModuleType mt ->
         Aml_ast_loc.loc_of_module_type mt
    | AstModule me ->
         Aml_ast_loc.loc_of_module_exp me
    | AstStrItem item ->
         Aml_ast_loc.loc_of_str_item item

(*
 * Print debugging info.
 *)
let rec pp_print_value buf x =
   match x with
      Exp e ->
         pp_print_exp buf e

    | Type t ->
         pp_print_type buf t

    | Patt p ->
         pp_print_patt buf p

    | String s ->
         pp_print_string buf s

    | AstType x ->
         Aml_ast_print.pp_print_type buf x

    | AstPatt x ->
         Aml_ast_print.pp_print_patt buf x

    | AstExp x ->
         Aml_ast_print.pp_print_exp buf x

    | AstModuleType x ->
         Aml_ast_print.pp_print_module_type buf x

    | AstModule x ->
         Aml_ast_print.pp_print_module_exp buf x

    | AstStrItem x ->
         Aml_ast_print.pp_print_str_item buf x

(************************************************************************
 * CONSTRUCTION
 ************************************************************************)

module type PosSig =
sig
   val loc_pos : loc -> pos
   val exp_pos : exp -> pos
   val patt_exp_pos : patt -> pos
   val type_exp_pos : ty -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos
   val type_pos : ty -> pos -> pos
   val ast_type_pos : Aml_ast.ty -> pos
   val ast_exp_pos : Aml_ast.exp -> pos
   val ast_patt_pos : Aml_ast.patt -> pos
   val ast_mt_pos : Aml_ast.module_type -> pos
   val ast_me_pos : Aml_ast.module_exp -> pos
   val ast_str_item_pos : Aml_ast.str_item -> pos
   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let exp_pos e = base_pos (Exp e)
   let patt_exp_pos p = base_pos (Patt p)
   let type_exp_pos ty = base_pos (Type ty)
   let string_exp_pos s = base_pos (String s)
   let var_pos = symbol_pos
   let type_pos ty pos = cons_pos (Type ty) pos
   let ast_type_pos ty = base_pos (AstType ty)
   let ast_exp_pos e = base_pos (AstExp e)
   let ast_patt_pos p = base_pos (AstPatt p)
   let ast_mt_pos mt = base_pos (AstModuleType mt)
   let ast_me_pos me = base_pos (AstModule me)
   let ast_str_item_pos item = base_pos (AstStrItem item)
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
