(*
 * Lookup values from the environment.
 * For the most part, this is just name translation.
 *
 * However, the tricky part is that when we get to
 * a module name that is undefined, we need to look
 * for a file with that name.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast
open Aml_tast_pos
open Aml_tast_env_type

(* Names *)
val genv_lookup_name        : genv -> pos -> var -> ext_var

(* Looking up types *)
val genv_lookup_tydef       : genv -> pos -> ty_var -> tydef

(* Lookup values int the current module *)
val genv_lookup_module_type : genv -> pos -> mt_name -> ty
val genv_lookup_module      : genv -> pos -> me_name -> ty
val genv_lookup_type        : genv -> pos -> ty_name -> ty_var list * ty option
val genv_lookup_var         : genv -> pos -> var_name -> ty
val genv_lookup_const       : genv -> pos -> const_name -> ty_var * const_var * int * const_field
val genv_lookup_label       : genv -> pos -> label_name -> ty_var * label * int * ty_kind list

(* Internal lookup *)
val genv_lookup_exn         : genv -> pos -> const_var -> ty list

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
