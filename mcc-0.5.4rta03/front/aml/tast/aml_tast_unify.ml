(*
 * Unification.
 *
 * We use the concept of "bound type variable".  If a variable is bound,
 * then it acts like a normal, but unknown, type.  It does not
 * get added to the substitution.  Two bound variables never
 * unify.
 *
 * When unifying TyAll (vars1, ty1) and TyAll (vars2, ty2),
 * the vars are marked as bound, and vars2 are replaced with
 * new vars in ty2, and ty1 and ty2 are unified.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Debug
open Symbol
open Field_table

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_env
open Aml_tast_util
open Aml_tast_type
open Aml_tast_tydef
open Aml_tast_print
open Aml_tast_subst

let debug_unify =
   create_debug (**)
      { debug_name = "unify";
        debug_description = "show trace of unification";
        debug_value = false
      }

module Pos = MakePos (struct let name = "Aml_tast_unify" end)
open Pos

(************************************************************************
 * BOUND VARS
 ************************************************************************)

type bvars = SymbolSet.t

let bvars_empty = SymbolSet.empty

let bvars_mem = SymbolSet.mem

let bvars_add_var = SymbolSet.add

let bvars_add_vars bvars vars =
   List.fold_left bvars_add_var bvars vars

let bvars_to_set bvars =
   bvars

(************************************************************************
 * EQUAL TYPES AND KINDS
 ************************************************************************)

(*
 * Equal.
 *)
type eq =
   { eq_types : TypeSet2.t;
     eq_names : TypeKindSet2.t
   }

let eq_empty =
   { eq_types = TypeSet2.empty;
     eq_names = TypeKindSet2.empty
   }

let eq_add_types eq ty1 ty2 =
   { eq with eq_types = TypeSet2.add eq.eq_types (ty1, ty2) }

let eq_mem_types eq ty1 ty2 =
   TypeSet2.mem eq.eq_types (ty1, ty2)

let eq_add_kinds eq k1 k2 =
   { eq with eq_names = TypeKindSet2.add eq.eq_names (k1, k2) }

let eq_mem_kinds eq k1 k2 =
   TypeKindSet2.mem eq.eq_names (k1, k2)

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Rename the vars in the type.
 *)
let rename_type_vars vars ty =
   let loc = loc_of_type ty in
   let vars' = List.map new_symbol vars in
   let subst =
      List.fold_left2 (fun subst v1 v2 ->
            subst_add_fo_var subst v1 (make_type loc (TyVar v2))) empty_subst vars vars'
   in
      subst_type subst ty

let rename_type_vars_opt vars ty =
   match ty with
      Some ty ->
         Some (rename_type_vars vars ty)
    | None ->
         None

(*
 * Rename the type vars from one list to another.
 *)
let rename_type_vars2 pos vars1 vars2 ty =
   let pos = string_pos "rename_type_vars2" pos in
   let loc = loc_of_type ty in
   let len1 = List.length vars1 in
   let len2 = List.length vars2 in
   let _ =
      if len1 <> len2 then
         raise (TAstException (pos, TypeArityMismatch (ty, len1, len2)))
   in
   let subst =
      List.fold_left2 (fun subst v1 v2 ->
            subst_add_fo_var subst v1 (make_type loc (TyVar v2))) empty_subst vars1 vars2
   in
      subst_type subst ty

let rename_type_vars2_opt pos vars1 vars2 ty_opt =
   match ty_opt with
      Some ty ->
         Some (rename_type_vars2 pos vars1 vars2 ty)
    | None ->
         None

(*
 * Check two symbol lists for equality.
 *)
let rec eq_var_list2 vars1 vars2 =
   match vars1, vars2 with
      v1 :: vars1, v2 :: vars2 ->
         Symbol.eq v1 v2 && eq_var_list2 vars1 vars2
    | [], [] ->
         true
    | _ ->
         false

(************************************************************************
 * UNIFICATION
 ************************************************************************)

(*
 * Maximum recursion.
 * This is really just to correct a problem with infinite loops
 * on recursive types.  It is only used when debug_unify is set.
 *)
let max_depth = 10

(*
 * Type unification.
 *)
let rec unify_types genv1 genv2 depth eq bvars pos subst t1' t2' =
   (*
    * Raise a TypeMismatch error.
    *)
   let type_error pos t1 t2 =
      raise (TAstException (pos, TypeMismatch4 (subst_type subst t1',
                                                subst_type subst t2',
                                                subst_type subst t1,
                                                subst_type subst t2)))
   in

   (*
    * Use an occurs-check.
    * This is not too expensive, because of
    * delayed substitution.
    *)
   let subst_add_fo_var t1 t2 subst pos v ty =
      let pos = string_pos "subst_add_fo_var" pos in
      let ty' = subst_type subst ty in
      let fv = (free_vars_type ty').fv_fo_vars in
      let _ =
         if SymbolSet.mem fv v then
            let pos =
               del_exp_pos (fun buf ->
                     fprintf buf "@[<hv 3>Occurs-check error:@ Var: %a@ @[<hv 3>Type:@ %a@]@ @[<b 3>Free vars:%t@]@]" (**)
                        pp_print_symbol v
                        pp_print_type ty'
                        (fun buf ->
                              SymbolSet.iter (fun v ->
                                    fprintf buf "@ %a" pp_print_symbol v) fv)) pos
            in
               type_error pos t1 t2
      in
         subst_add_fo_var subst v ty
   in

   (*
    * This is the main unification function.
    *)
   let rec unify_types pos depth (eq : eq) (bvars : bvars) (subst : subst) t1 t2 =
      let loc1 = loc_of_type t1 in
      let loc2 = loc_of_type t2 in
      let pos =
         if !debug_unify then
            del_exp_pos (fun buf ->
                  Format.fprintf buf "@[<hv 3>Type_unify.unify_type:@ @[<hv 3>Type1:@ %a@]@ @[<hv 3>Type2:@ %a@]@ @[<hv 3>Type1 subst:@ %a@]@ @[<hv 3>Type2 subst:@ %a@]@]" (**)
                     pp_print_type t1
                     pp_print_type t2
                     pp_print_type (subst_type subst t1)
                     pp_print_type (subst_type subst t2)) pos
         else
            pos
      in
         match dest_type_core t1, dest_type_core t2 with
            (*
             * Strip off leading useless quantifiers.
             *)
            TyAll ([], t1), _ ->
               unify_types pos depth eq bvars subst t1 t2
          | _, TyAll ([], t2) ->
               unify_types pos depth eq bvars subst t1 t2

            (*
             * If the parameters match in the bvars context,
             * then we do nothing.
             *)
          | TyVar v1, TyVar v2 ->
               let pos = string_pos "ty_var2" pos in
                  (if Symbol.eq v1 v2 then
                      subst
                   else
                      (* Aggressive application of substitution *)
                      try unify_types (int_pos 2 pos) depth eq bvars subst t1 (subst_lookup_fo_var_relocate_exn subst v2 loc2) with
                         Not_found ->
                            try unify_types (int_pos 4 pos) depth eq bvars subst (subst_lookup_fo_var_relocate_exn subst v1 loc1) t2 with
                               Not_found ->
                                  (* Be careful not to add substitution for bound variables *)
                                  if bvars_mem bvars v1 then
                                     if bvars_mem bvars v2 then
                                        type_error (int_pos 1 pos) t1 t2
                                     else
                                        subst_add_fo_var t1 t2 subst (int_pos 3 pos) v2 (relocate_type loc2 t1)
                                  else
                                     subst_add_fo_var t1 t2 subst (int_pos 5 pos) v1 t2)

            (*
             * If one is not a variable, the other should not be a binding
             * in the bvars context.
             *)
          | TyVar v1, _ ->
               if bvars_mem bvars v1 then
                  let pos = string_pos "ty_var_left_bvar" pos in
                     type_error pos t1 t2
               else
                  let pos = string_pos "ty_var_left_nobvar" pos in
                     (try unify_types pos depth eq bvars subst (subst_lookup_fo_var_relocate_exn subst v1 loc1) t2 with
                         Not_found ->
                            subst_add_fo_var t1 t2 subst pos v1 t2)

          | _, TyVar v2 ->
               if bvars_mem bvars v2 then
                  let pos = string_pos "ty_var_right_bvar" pos in
                     type_error pos t1 t2
               else
                  let pos = string_pos "ty_var_right_nobvar" pos in
                     (try unify_types pos depth eq bvars subst t1 (subst_lookup_fo_var_relocate_exn subst v2 loc2) with
                         Not_found ->
                            subst_add_fo_var t1 t2 subst pos v2 (relocate_type loc2 t1))

            (*
             * Variables are expanded only when absolutely necessary.
             *)
          | TyProject (name1, tyl1), TyProject (name2, tyl2) ->
               let pos = string_pos "ty_project2" pos in
                  if name1 = name2 then
                     unify_type_lists t1 t2 depth eq bvars pos subst tyl1 tyl2
                  else
                     let t1' = expand_type genv1 pos t1 in
                        if t1' = t1 then
                           let t2' = expand_type genv2 pos t2 in
                              if t2' = t2 then
                                 type_error pos t1 t2
                              else
                                 unify_types pos depth eq bvars subst t1 (relocate_type loc2 t2')
                        else
                           unify_types pos depth eq bvars subst (relocate_type loc1 t1') t2
          | TyProject _, _ ->
               let pos = string_pos "ty_project_left" pos in
               let t1' = expand_type genv1 pos t1 in
                  if t1' = t1 then
                     type_error pos t1 t2;
                  unify_types pos depth eq bvars subst (relocate_type loc1 t1') t2
          | _, TyProject (name2, tl2) ->
               let pos = string_pos "ty_project_right" pos in
               let t2' = expand_type genv2 pos t2 in
                  if t2' = t2 then
                     type_error pos t1 t2;
                  unify_types pos depth eq bvars subst t1 (relocate_type loc2 t2')

            (*
             * Type applications.
             *)
          | TyApply  (ty_var1, fo_args1, so_args1), TyApply  (ty_var2, fo_args2, so_args2)
          | TyRecord (ty_var1, fo_args1, so_args1), TyRecord (ty_var2, fo_args2, so_args2)
          | TyUnion  (ty_var1, fo_args1, so_args1), TyUnion  (ty_var2, fo_args2, so_args2)
          | TyModule (ty_var1, fo_args1, so_args1), TyModule (ty_var2, fo_args2, so_args2) ->
               let pos = string_pos "ty_apply" pos in
                  (* Check to see if we think they are already equal *)
                  if eq_mem_types eq t1 t2 then
                     subst
                  else
                     (* Add it _now_ for recursive types *)
                     let eq = eq_add_types eq t1 t2 in
                     let depth = succ depth in
                        if depth = max_depth && !debug_unify then
                           let pos = string_pos "max depth exceeded" pos in
                              type_error pos t1 t2
                        else if Symbol.eq ty_var1 ty_var2 then
                           (* If they have the same name, unify the arguments *)
                           let subst = unify_type_lists t1 t2 depth eq bvars pos subst fo_args1 fo_args2 in
                              unify_kind_lists t1 t2 depth eq bvars pos subst so_args1 so_args2
                        else
                           (* Otherwise, unfold them and continue *)
                           let tydef1 = apply_type genv1 pos ty_var1 fo_args1 so_args1 in
                           let tydef2 = apply_type genv2 pos ty_var2 fo_args2 so_args2 in
                              unify_tydefs genv1 genv2 depth eq bvars pos subst tydef1 tydef2

            (*
             * Base types should be exactly the same.
             *)
          | TyInt, TyInt
          | TyChar, TyChar
          | TyString, TyString
          | TyFloat, TyFloat ->
               subst

            (*
             * Format strings.
             *)
          | TyFormat (t1, t2, t3), TyFormat (t1', t2', t3') ->
               let pos = string_pos "ty_format" pos in
               let subst = unify_types pos depth eq bvars subst t1 t1' in
               let subst = unify_types pos depth eq bvars subst t2 t2' in
               let subst = unify_types pos depth eq bvars subst t3 t3' in
                  subst

            (*
             * Function types are special because we currently want
             * to ignore currying.
             *)
          | TyFun (tyl1, ty1), TyFun (tyl2, ty2) ->
               let pos = string_pos "ty_fun" pos in
               let len1 = List.length tyl1 in
               let len2 = List.length tyl2 in
                  if len1 = len2 then
                     let subst = unify_type_lists t1 t2 depth eq bvars (int_pos 1 pos) subst tyl2 tyl1 in
                        unify_types (int_pos 2 pos) depth eq bvars subst ty1 ty2
                  else if len1 < len2 then
                     let tyl2, tyl2' = Mc_list_util.split len1 tyl2 in
                     let subst = unify_type_lists t1 t2 depth eq bvars (int_pos 3 pos) subst tyl2 tyl1 in
                        unify_types (int_pos 4 pos) depth eq bvars subst ty1 (make_type loc2 (TyFun (tyl2', ty2)))
                  else (* len1 > len2 *)
                     let tyl1, tyl1' = Mc_list_util.split len2 tyl1 in
                     let subst = unify_type_lists t1 t2 depth eq bvars (int_pos 5 pos) subst tyl2 tyl1 in
                        unify_types (int_pos 6 pos) depth eq bvars subst (make_type loc1 (TyFun (tyl1', ty1))) ty2

            (*
             * Parts must unify.
             *)
          | TyProd tl1, TyProd tl2 ->
               let pos = string_pos "ty_prod" pos in
                  unify_type_lists t1 t2 depth eq bvars pos subst tl1 tl2
          | TyArray t1, TyArray t2 ->
               let pos = string_pos "ty_array" pos in
                  unify_types pos depth eq bvars subst t1 t2

            (*
             * A value has a universal type if it has the
             * type when the vars are bound.
             *
             * A universal type can be instantiated at a
             * particular type is we rename all the vars
             * and it unifies.
             *)
          | TyAll (vars1, t1), TyAll (vars2, t2) when eq_var_list2 vars1 vars2 ->
               let pos = string_pos "ty_all2" pos in
               let bvars = bvars_add_vars bvars vars1 in
                  unify_types pos depth eq bvars subst t1 t2
          | TyAll (vars1, t1), _ ->
               let pos = string_pos "ty_all_left" pos in
               let bvars = bvars_add_vars bvars vars1 in
                  unify_types pos depth eq bvars subst t1 t2
          | _, TyAll (vars2, t2) ->
               let pos = string_pos "ty_all_right" pos in
               let t2 = rename_type_vars vars2 t2 in
                  unify_types pos depth eq bvars subst t1 t2

            (*
             * External values.
             *)
          | TyExternal (t1', s1), TyExternal (t2', s2) ->
               let pos = string_pos "ty_external2" pos in
                  if s1 <> s2 then
                     type_error pos t1 t2;
                  unify_types pos depth eq bvars subst t1' t2'
          | _, TyExternal (t2, _) ->
               let pos = string_pos "ty_external_right" pos in
                  unify_types pos depth eq bvars subst t1 t2

            (*
             * In all other cases, the types do not match.
             *)
          | _ ->
               type_error pos t1 t2

   (*
    * Unify a list of types.
    *)
   and unify_type_lists t1 t2 depth eq bvars pos subst tl1 tl2 =
      let pos = string_pos "unify_type_lists" pos in
      let len1 = List.length tl1 in
      let len2 = List.length tl2 in
         if len1 <> len2 then
            type_error pos t1 t2;
         List.fold_left2 (unify_types pos depth eq bvars) subst tl1 tl2

   (*
    * Unify a list of kind names.
    *)
   and unify_kind_lists t1 t2 depth eq bvars pos subst kl1 kl2 =
      let pos = string_pos "unify_kins_lists" pos in
      let len1 = List.length kl1 in
      let len2 = List.length kl2 in
      let _ =
         if len1 <> len2 then
            type_error pos t1 t2
      in
         List.fold_left2 (fun subst k1 k2 ->
               if eq_mem_kinds eq k1 k2 then
                  subst
               else
                  let eq = eq_add_kinds eq k1 k2 in
                  let { kind_arity = arity1; kind_value = name1 } = k1 in
                  let { kind_arity = arity2; kind_value = name2 } = k2 in
                  let ty_vars1, ty1 = genv_lookup_type genv1 pos name1 in
                  let ty_vars2, ty2 = genv_lookup_type genv2 pos name2 in
                  let len1 = List.length ty_vars1 in
                  let len2 = List.length ty_vars2 in
                  let _ =
                     if len1 <> arity1 || len2 <> arity2 || arity1 <> arity2 then
                        type_error pos t1 t2
                  in
                  let bvars = bvars_add_vars bvars ty_vars1 in
                  let ty2 =
                     if eq_var_list2 ty_vars1 ty_vars2 then
                        ty2
                     else
                        rename_type_vars_opt ty_vars2 ty2
                  in
                     match ty1, ty2 with
                        Some ty1, Some ty2 ->
                           unify_types pos depth eq bvars subst ty1 ty2
                      | Some _, None ->
                           type_error pos t1 t2
                      | None, _ ->
                           subst) subst kl1 kl2
   in
      unify_types pos depth eq bvars subst t1' t2'

and unify_type_lists genv1 genv2 depth eq bvars pos subst tl1 tl2 =
   let pos = string_pos "unify_type_lists" pos in
   let len1 = List.length tl1 in
   let len2 = List.length tl2 in
      if len1 <> len2 then
         raise (TAstException (pos, ArityMismatch (len1, len2)));
      List.fold_left2 (unify_types genv1 genv2 depth eq bvars pos) subst tl1 tl2

(*
 * Record unification.
 * The fields must have the same names in the same order.
 *)
and unify_record_types genv1 genv2 depth eq bvars pos subst fields1 fields2 =
   let pos = string_pos "unify_record_types" pos in

   (* Check the number of fields *)
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
   let len1 = List.length fields1 in
   let len2 = List.length fields2 in
   let _ =
      if len1 <> len2 then
         raise (TAstException (pos, ArityMismatch (len1, len2)))
   in

   (* Unify a field *)
   let unify_record_field subst (label1, (mutablep1, ty1)) (label2, (mutablep2, ty2)) =
      (* Make sure they have the same name *)
      let ext_var1 = genv_lookup_name genv1 pos label1 in
      let ext_var2 = genv_lookup_name genv2 pos label2 in
      let _ =
         if not (Symbol.eq ext_var1 ext_var2) || mutablep1 <> mutablep2 then
            raise (TAstException (pos, ConstMismatch (ext_var1, ext_var2)))
      in
         unify_types genv1 genv2 depth eq bvars pos subst ty1 ty2
   in
      (* Unify them in the same order *)
      List.fold_left2 unify_record_field subst fields1 fields2

(*
 * Union unification.
 *)
and unify_union_kind genv1 genv2 depth eq bvars pos subst kind1 kind2 =
   let pos = string_pos "unify_union_kind" pos in
      match kind1, kind2 with
         NormalUnion, NormalUnion
       | ExnUnion, ExnUnion ->
            subst
       | _ ->
            raise (TAstException (pos, UnionTypeMismatch (kind1, kind2)))

and unify_union_types genv1 genv2 depth eq bvars pos subst kind1 fields1 kind2 fields2 =
   let pos = string_pos "unify_union_types" pos in

   (* Check the kinds *)
   let subst = unify_union_kind genv1 genv2 depth eq bvars pos subst kind1 kind2 in

   (* Check the number of fields *)
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
   let len1 = List.length fields1 in
   let len2 = List.length fields2 in
   let _ =
      if len1 <> len2 then
         raise (TAstException (pos, ArityMismatch (len1, len2)))
   in

   (* Unify a field *)
   let unify_union_field subst (const_var1, tyl1) (const_var2, tyl2) =
      (* Make sure they have the same name *)
      let ext_var1 = genv_lookup_name genv1 pos const_var1 in
      let ext_var2 = genv_lookup_name genv2 pos const_var2 in
      let _ =
         if not (Symbol.eq ext_var1 ext_var2) then
            raise (TAstException (pos, ConstMismatch (ext_var1, ext_var2)))
      in
         unify_type_lists genv1 genv2 depth eq bvars pos subst tyl1 tyl2
   in
      (* Unify them all in the same order *)
      List.fold_left2 unify_union_field subst fields1 fields2

(*
 * Unify two module types.
 * The second is allowed to be a subtype of the first
 * (it may have more external fields).
 *)
and unify_module_types genv1 genv2 depth eq bvars pos subst mt1 mt2 =
   let pos =
      del_exp_pos (fun buf ->
            fprintf buf "@[<hv 3>module type1:@ %a@]" pp_print_module_type mt1) pos
   in
   let pos =
      del_exp_pos (fun buf ->
            fprintf buf "@[<hv 3>module type2:@ %a@]" pp_print_module_type mt2) pos
   in
   let pos = string_pos "unify_module_types" pos in
   let { mt_mt_types = mt_types1;
         mt_me_vars  = me_vars1;
         mt_types    = types1;
         mt_vars     = vars1;
         mt_labels   = labels1;
         mt_names    = names1
       } = mt1
   in
   let { mt_mt_types = mt_types2;
         mt_me_vars  = me_vars2;
         mt_types    = types2;
         mt_vars     = vars2;
         mt_labels   = labels2;
         mt_names    = names2
       } = mt2
   in
   let { names_mt = mt_names1;
         names_me = me_names1;
         names_ty = ty_names1;
         names_var = var_names1
       } = names1
   in
   let { names_mt = mt_names2;
         names_me = me_names2;
         names_ty = ty_names2;
         names_var = var_names2
       } = names2
   in

   (* Add the modules to the context *)
   let genv1 = genv_push_core genv1 mt1 in
   let genv2 = genv_push_core genv2 mt2 in

   (* Compare module types *)
   let subst =
      SymbolTable.fold (fun subst ext_var (mt_var1, _) ->
            let ty1, ty2 =
               try
                  let ty1 = SymbolTable.find mt_types1 mt_var1 in
                  let mt_var2, _ = SymbolTable.find mt_names2 ext_var in
                  let ty2 = SymbolTable.find mt_types2 mt_var2 in
                     ty1, ty2
               with
                  Not_found ->
                     raise (TAstException (pos, StringVarError ("missing module type", ext_var)))
            in
               unify_types genv1 genv2 depth eq bvars pos subst ty1 ty2) subst mt_names1
   in

   (* Compare modules *)
   let subst =
      FieldTable.fold (fun subst ext_var me_var1 ->
            let ty1, ty2 =
               try
                  let ty1 = SymbolTable.find me_vars1 me_var1 in
                  let me_var2 = FieldTable.find me_names2 ext_var in
                  let ty2 = SymbolTable.find me_vars2 me_var2 in
                     ty1, ty2
               with
                  Not_found ->
                     raise (TAstException (pos, StringVarError ("missing module", ext_var)))
            in
               unify_types genv1 genv2 depth eq bvars pos subst ty1 ty2) subst me_names1
   in

   (* Compare types *)
   let subst =
      SymbolTable.fold (fun subst ext_var ty_var1 ->
            let ty_vars1, ty1, ty_vars2, ty2 =
               try
                  let ty_vars1, ty1 = SymbolTable.find types1 ty_var1 in
                  let ty_var2 = SymbolTable.find ty_names2 ext_var in
                  let ty_vars2, ty2 = SymbolTable.find types2 ty_var2 in
                     ty_vars1, ty1, ty_vars2, ty2
               with
                  Not_found ->
                     raise (TAstException (pos, StringVarError ("missing type", ext_var)))
            in
            let bvars = bvars_add_vars bvars ty_vars1 in
            let ty2 = rename_type_vars2_opt pos ty_vars2 ty_vars1 ty2 in
               match ty1, ty2 with
                  Some ty1, Some ty2 ->
                     unify_types genv1 genv2 depth eq bvars pos subst ty1 ty2
                | Some _, None ->
                     raise (TAstException (pos, StringVarError ("type is not defined", ext_var)))
                | None, _ ->
                     subst) subst ty_names1
   in

   (* Compare variables *)
   let subst =
      FieldTable.fold (fun subst ext_var v1 ->
            let ty1, ty2 =
               try
                  let ty1 = SymbolTable.find vars1 v1 in
                  let v2 = FieldTable.find var_names2 ext_var in
                  let ty2 = SymbolTable.find vars2 v2 in
                     ty1, ty2
               with
                  Not_found ->
                     raise (TAstException (pos, StringVarError ("missing type", ext_var)))
            in
               unify_types genv1 genv2 depth eq bvars pos subst ty1 ty2) subst var_names1
   in
      subst

(*
 * Unify two module types.
 *)
and unify_tydefs genv1 genv2 depth eq bvars pos subst t1 t2 =
   let pos = string_pos "unify_tydefs" pos in
      match dest_tydef_inner_core t1, dest_tydef_inner_core t2 with
         TyDefLambda t1, TyDefLambda t2 ->
            unify_types genv1 genv2 depth eq bvars pos subst t1 t2
       | TyDefUnion (kind1, fields1), TyDefUnion (kind2, fields2) ->
            unify_union_types genv1 genv2 depth eq bvars pos subst kind1 fields1 kind2 fields2
       | TyDefRecord fields1, TyDefRecord fields2 ->
            unify_record_types genv1 genv2 depth eq bvars pos subst fields1 fields2
       | TyDefModule mt1, TyDefModule mt2 ->
            unify_module_types genv1 genv2 depth eq bvars pos subst mt1 mt2
       | t1, t2 ->
            raise (TAstException (pos, TydefInnerCoreMismatch (t1, t2)))

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * By default, we assume the environments are the same.
 *)
let unify_types genv bvars pos subst t1 t2 =
   unify_types genv genv 0 eq_empty bvars pos subst t1 t2

let unify_type_lists genv bvars pos subst tyl1 tyl2 =
   unify_type_lists genv genv 0 eq_empty bvars pos subst tyl1 tyl2

let unify_record_types genv bvars pos subst fields1 fields2 =
   unify_record_types genv genv 0 eq_empty bvars pos subst fields1 fields2

let unify_union_types genv bvars pos subst kind1 fields1 kind2 fields2 =
   unify_union_types genv genv 0 eq_empty bvars pos subst kind1 fields1 kind2 fields2

let unify_module_types genv bvars pos subst mt1 mt2 =
   unify_module_types genv genv 0 eq_empty bvars pos subst mt1 mt2

(*
 * Some special cases.
 *)
let unify_type_int genv bvars pos subst ty =
   let loc = loc_of_type ty in
      unify_types genv bvars pos subst (make_type loc TyInt) ty

let unify_type_bool genv bvars pos subst ty =
   let loc = loc_of_type ty in
      unify_types genv bvars pos subst (ty_bool genv loc) ty

let unify_type_unit genv bvars pos subst ty =
   let loc = loc_of_type ty in
      unify_types genv bvars pos subst (ty_unit genv loc) ty

let unify_type_exn genv bvars pos subst ty =
   let loc = loc_of_type ty in
      unify_types genv bvars pos subst (ty_exn genv loc) ty

let unify_type_string genv bvars pos subst ty =
   let loc = loc_of_type ty in
      unify_types genv bvars pos subst (make_type loc TyString) ty

let unify_type_char genv bvars pos subst ty =
   let loc = loc_of_type ty in
      unify_types genv bvars pos subst (make_type loc TyChar) ty

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
