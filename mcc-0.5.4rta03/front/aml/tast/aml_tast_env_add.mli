(*
 * Adding new values to the environment.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Aml_tast
open Aml_tast_pos
open Aml_tast_env_type

(*
 * Add a name translation.
 *)
val genv_add_name : genv -> ext_var -> var -> genv

(*
 * Set the name translation for the current module.
 *)
val genv_set_current_names : genv -> module_names -> genv

(*
 * Add a type definition.
 *)
val genv_add_tydef : genv -> ty_var -> tydef -> genv

(*
 * Add a new module type.  The type is already added as to the top-level
 * types, so we just need
 *    1. the external name
 *    2. the internal name
 *    3. the top-level name
 *    4. the type arguments
 *)
val genv_add_module_type : genv -> mt_var -> mt_var -> var -> ty -> ty list -> genv

(*
 * Add a new module declaration.
 * We get:
 *    1. the external name
 *    2. the internal name
 *    3. the module type
 *)
val genv_add_module : genv -> me_var -> me_var -> ty -> genv

(*
 * Add a type definition.
 * Remove it from the opens.
 *)
val genv_add_type : genv -> ty_var -> ty_var -> ty_var list -> ty option -> genv

(*
 * Add the _name_ of the type definition.
 * The type definition itself is added later.
 *)
val genv_add_type_name : genv -> ty_var -> ty_var -> genv

(*
 * Add a new module declaration.
 * We get:
 *    1. the external name
 *    2. the internal name
 *    3. the module type
 *)
val genv_add_var : genv -> var -> var -> ty -> genv

(*
 * Add the name of a constructor.
 * This is just a scoping issue.
 * We need the type name for the union,
 * and the internal name for the constructor.
 *)
val genv_add_const : genv -> ext_var -> const_var -> ty_var -> int -> ty_kind list -> genv
val genv_add_exn : genv -> ext_var -> const_var -> ty_var -> ty list -> genv

val genv_add_union_case : genv -> pos -> ty_var -> const_var -> ty list -> genv

(*
 * Add the name of a label.
 *)
val genv_add_label : genv -> label -> label -> ty_var -> int -> ty_kind list -> genv

(*
 * Internal versions don't use an external variable.
 *)
val genv_add_module_type_int : genv -> mt_var -> ty -> genv
val genv_add_module_int : genv -> me_var -> ty -> genv
val genv_add_type_int : genv -> ty_var -> ty_var list -> ty option -> genv
val genv_add_var_int : genv -> var -> ty -> genv
val genv_add_exn_int : genv -> const_var -> ty_var -> ty list -> genv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
