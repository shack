(*
 * Type unification.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol

open Aml_tast
open Aml_tast_pos
open Aml_tast_env_type

(*
 * Bound variable sets.
 *)
type bvars

val bvars_empty     : bvars
val bvars_add_var   : bvars -> var -> bvars
val bvars_add_vars  : bvars -> var list -> bvars

(*
 * Unification.
 *)
val unify_types      : genv -> bvars -> pos -> subst -> ty -> ty -> subst
val unify_type_lists : genv -> bvars -> pos -> subst -> ty list -> ty list -> subst

(*
 * Standard types.
 *)
val unify_type_int      : genv -> bvars -> pos -> subst -> ty -> subst
val unify_type_bool     : genv -> bvars -> pos -> subst -> ty -> subst
val unify_type_unit     : genv -> bvars -> pos -> subst -> ty -> subst
val unify_type_exn      : genv -> bvars -> pos -> subst -> ty -> subst
val unify_type_string   : genv -> bvars -> pos -> subst -> ty -> subst
val unify_type_char     : genv -> bvars -> pos -> subst -> ty -> subst

(*
 * Type definitions.
 *)
val unify_record_types  : genv -> bvars -> pos -> subst -> record_fields -> record_fields -> subst
val unify_union_types   : genv -> bvars -> pos -> subst -> union_type -> union_fields -> union_type -> union_fields -> subst
val unify_module_types  : genv -> bvars -> pos -> subst -> module_type -> module_type -> subst

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
