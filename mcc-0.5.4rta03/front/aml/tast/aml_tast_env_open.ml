(*
 * Implement the "open" operation on a module.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Aml_tast
open Aml_tast_exn
open Aml_tast_tydef
open Aml_tast_env_type
open Aml_tast_env_base
open Aml_tast_env_lookup

(*
 * Add all the names to the open table.
 *)
let open_core opens me_name core =
   let { names_mt = mt_names;
         names_me = me_names;
         names_ty = ty_names;
         names_var = var_names;
         names_const = const_names;
         names_label = label_names
       } = core.mt_names
   in
   let { mt_opens    = mt_opens;
         me_opens    = me_opens;
         ty_opens    = ty_opens;
         label_opens = label_opens;
         const_opens = const_opens;
         var_opens   = var_opens
       } = opens
   in
   let add_opens opens intern =
      SymbolTable.fold (fun opens ext_var _ ->
            SymbolTable.add opens ext_var me_name) opens intern
   in
   let add_field_opens opens intern =
      FieldTable.fold (fun opens ext_var _ ->
            SymbolTable.add opens ext_var me_name) opens intern
   in
   let mt_opens    = add_opens mt_opens mt_names in
   let me_opens    = add_field_opens me_opens me_names in
   let ty_opens    = add_opens ty_opens ty_names in
   let var_opens   = add_field_opens var_opens var_names in
   let const_opens = add_opens const_opens const_names in
   let label_opens = add_opens label_opens label_names in
      { mt_opens    = mt_opens;
        me_opens    = me_opens;
        ty_opens    = ty_opens;
        label_opens = label_opens;
        const_opens = const_opens;
        var_opens   = var_opens
      }

(*
 * Open a module.
 *)
let genv_open genv pos me_name =
   let ty = genv_lookup_module genv pos me_name in
   let core' = dest_module_type genv pos ty in
   let current = genv.genv_current in
   let current = { current with genv_opens = open_core current.genv_opens me_name core' } in
      { genv with genv_current = current }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
