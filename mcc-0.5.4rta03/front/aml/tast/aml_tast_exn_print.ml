(*
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Debug
open Format
open Symbol
open Location

open Fir_state

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_print

module Pos = MakePos (struct let name = "Aml_tast_exn_print" end)
open Pos

(************************************************************************
 * PRINTING
 ************************************************************************)

let tabstop = 3

(*
 * Format the possible FIR exceptions.
 *)
let pp_print_error buf exn =
   match exn with
      UnboundVar v ->
         fprintf buf "unbound variable: %a" pp_print_symbol v
    | UnboundConst v ->
         fprintf buf "unbound constructor: %a" pp_print_symbol v
    | UnboundLabel v ->
         fprintf buf "unbound label: %a" pp_print_symbol v
    | UnboundTyVar v ->
         fprintf buf "unbound type parameter: '%a" pp_print_symbol v
    | UnboundType v ->
         fprintf buf "unbound type variable: %a" pp_print_symbol v
    | UnboundModule v ->
         fprintf buf "unbound module variable: %a" pp_print_symbol v
    | UnboundModuleName me_name ->
         fprintf buf "unbound module: %a" pp_print_mod_name me_name
    | UnboundModuleType mt_var ->
         fprintf buf "unbound module type: %a" pp_print_symbol mt_var
    | UnboundBuiltin s ->
         fprintf buf "unbound builtin function: %s" s

    | MissingVar v ->
         fprintf buf "missing variable: %a" pp_print_symbol v
    | MissingConst v ->
         fprintf buf "missing constructor: %a" pp_print_symbol v
    | MissingLabels vars ->
         fprintf buf "@[<hv 3>missing labels:";
         List.iter (fun v ->
               fprintf buf "@ %a" pp_print_symbol v) vars;
         fprintf buf "@]"
    | MissingType v ->
         fprintf buf "missing type variable: %a" pp_print_symbol v
    | MissingModule v ->
         fprintf buf "missing module variable: %a" pp_print_symbol v
    | MissingModuleType mt_var ->
         fprintf buf "missing module type: %a" pp_print_symbol mt_var
    | MissingParam v ->
         fprintf buf "missing type parameter: '%a" pp_print_symbol v

    | ExtMultipleBinding v ->
         fprintf buf "multiple binding for the variable: %a" pp_print_symbol v

    | ConstMismatch (v1, v2) ->
         fprintf buf "constructor definition mismatch: %a, %a" (**)
            pp_print_symbol v1
            pp_print_symbol v2

    | TypeMismatch (ty1, ty2) ->
         fprintf buf "@[<hv 3>type error:";
         fprintf buf "@ @[<hv 3>this value has type@ %a@]" pp_print_type ty2;
         fprintf buf "@ @[<hv 3>but it is used with type@ %a@]" pp_print_type ty1;
         fprintf buf "@]"
    | TydefInnerCoreMismatch (ty1, ty2) ->
         fprintf buf "@[<hv 3>type error:";
         fprintf buf "@ @[<hv 3>this value has type@ %a@]" pp_print_tydef_inner_core ty2;
         fprintf buf "@ @[<hv 3>but it is used with type@ %a@]" pp_print_tydef_inner_core ty1;
         fprintf buf "@]"
    | TypeMismatch4 (ty1', ty2', ty1, ty2) ->
         fprintf buf "@[<hv 3>type error:";
         fprintf buf "@ @[<hv 3>this value has type@ %a@]" pp_print_type ty2';
         fprintf buf "@ @[<hv 3>but it is used with type@ %a@]" pp_print_type ty1';
         fprintf buf "@ @[<hv 3>this value has type@ %a@]" pp_print_type ty2;
         fprintf buf "@ @[<hv 3>but it is used with type@ %a@]" pp_print_type ty1;
         fprintf buf "@]"
    | VarTypeMismatch (v, ty1, ty2) ->
         fprintf buf "@[<hv 3>type error: %a" pp_print_symbol v;
         fprintf buf "@ @[<hv 3>this value has type@ %a@]" pp_print_type ty2;
         fprintf buf "@ @[<hv 3>but it is used with type@ %a@]" pp_print_type ty1;
         fprintf buf "@]"

    | DuplicateConst (v, ty) ->
         fprintf buf "@[<hv 3>duplicate constructor: %a@ %a@]" (**)
            pp_print_symbol v
            pp_print_type ty

    | ArityMismatch (i1, i2) ->
         fprintf buf "@[<hv 3>arity mismatch";
         fprintf buf "@ wanted %d arguments, got %d@]" i1 i2

    | VarArityMismatch (v, i1, i2) ->
         fprintf buf "@[<hv 3>variable arity mismatch: %a" pp_print_symbol v;
         fprintf buf "@ wanted %d arguments, got %d@]" i1 i2

    | TypeArityMismatch (ty, i1, i2) ->
         fprintf buf "@[<hv 3>type arity mismatch: %a" pp_print_type ty;
         fprintf buf "@ wanted %d arguments, got %d@]" i1 i2

    | ConstArityMismatch (v, i1, i2) ->
         fprintf buf "@[<hv 3>constructor mismatch: %a" pp_print_symbol v;
         fprintf buf "@ wanted %d arguments, got %d@]" i1 i2

    | RecordLabelMismatch (label, ty) ->
         fprintf buf "@[<hv 3>record label mismatch:@ label=%a@ @[<hv 3>this record has type:@ %a@]@]" (**)
            pp_print_symbol label
            pp_print_type ty

    | ConstLabelMismatch (label, ty) ->
         fprintf buf "@[<hv 3>constructor label mismatch:@ label=%a@ @[<hv 3>this union has type:@ %a@]@]" (**)
            pp_print_symbol label
            pp_print_type ty

    | UnionTypeMismatch (kind1, kind2) ->
         fprintf buf "@[<hv 3>unions have different types:@ this union has type %a@ but is used with type %a@]" (**)
            pp_print_union_kind kind1
            pp_print_union_kind kind2

    | NotImplemented s ->
         fprintf buf "NotImplemented: %s" s

    | InternalError ->
         fprintf buf "internal error"

    | StringError s ->
         pp_print_string buf s

    | StringTypeError (s, ty) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_type ty

    | StringTydefError (s, tydef) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_tydef tydef

    | StringTydefInnerError (s, inner) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_tydef_inner inner

    | StringVarError (s, v) ->
         fprintf buf "%s: %a" s pp_print_symbol v

    | StringPattError (s, p) ->
         fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_patt p

(*
 * Exception printer.
 *)
let pp_print_exn buf e =
   match e with
      TAstException (pos, exn) ->
         fprintf buf "@[<v 0>%a@ @[<hv 3>AML IR Error:@ %a@]@]@." (**)
            pp_print_pos pos
            pp_print_error exn
    | Aml_ast.SyntaxError (loc, s) ->
         fprintf buf "@[<v 0>%a@ Syntax error: %s@]@." (**)
            pp_print_location loc
            s
    | exn ->
         let s = Printexc.to_string exn in
            fprintf buf "AML TAST: uncaught exception:@ %s@." s;
            raise exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      TAstException _
    | Aml_ast.SyntaxError _ as exn ->
         pp_print_exn err_formatter exn;
         exit 2

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
