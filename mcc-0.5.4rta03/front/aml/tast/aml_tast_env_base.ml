(*
 * Basic operations on genv.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location

open Aml_tast
open Aml_tast_ds
open Aml_tast_pos
open Aml_tast_exn
open Aml_tast_mt_util
open Aml_tast_env_type
open Aml_tast_env_boot

module Pos = MakePos (struct let name = "aml_tast_env_base" end)
open Pos

(************************************************************************
 * EMPTY                                                                *
 ************************************************************************)

(*
 * Get a list of all the levels in the genv
 *)
let levels_of_genv genv =
   let { genv_current = current;
         genv_next = next
       } = genv
   in
      current :: next

(*
 * Get the current module type.
 *)
let mt_of_genv genv =
   genv.genv_current.genv_core

(*
 * Enter a new module.
 *)
let genv_push genv me_var =
   let mt = { empty_module_type with mt_self = me_var } in
   let level =
      { genv_core = mt;
        genv_opens = empty_opens
      }
   in
   let { genv_current = current; genv_next = next } = genv in
   let genv = { genv with genv_current = level; genv_next = current :: next } in
      genv

let genv_push_core genv core =
   let level =
      { genv_core = core;
        genv_opens = empty_opens
      }
   in
   let { genv_current = current; genv_next = next } = genv in
      { genv with genv_current = level; genv_next = current :: next }

(*
 * Recover the module type.
 *)
let genv_pop genv =
   let { genv_current = current;
         genv_next = next
       } = genv
   in
   let { genv_core = core } = current in
   let genv =
      match next with
         level :: next ->
            { genv with genv_current = level; genv_next = next }
       | [] ->
            raise (Invalid_argument "Type_type.pop_module_type")
   in
      genv, core

(*
 * Convert the environment to a program.
 *)
let zero_loc = bogus_loc "<Aml_tast_env_base>"

let prog_of_genv genv exp =
   let { genv_name    = name;
         genv_names   = names;
         genv_types   = types;
         genv_import  = import;
         genv_current = level;
         genv_boot    = boot
       } = genv
   in
   let { genv_core = mt } = level in
   let inner = TyDefModule mt in
   let loc = zero_loc in
   let inner = make_tydef_inner loc inner in
   let tydef =
      { tydef_fo_vars = [];
        tydef_so_vars = [];
        tydef_inner = inner
      }
   in
   let v = new_symbol name in
   let types = SymbolTable.add types v tydef in
      { prog_name    = name;
        prog_names   = names;
        prog_types   = types;
        prog_import  = import;
        prog_boot    = boot;
        prog_exp     = exp
      }

(*
 * Get an environment from the program.
 *)
let genv_of_prog prog =
   let { prog_name    = name;
         prog_names   = names;
         prog_types   = types;
         prog_import  = import;
         prog_boot    = boot
       } = prog
   in
   let current =
      { genv_core  = empty_module_type;
        genv_opens = empty_opens
      }
   in
      { genv_name    = name;
        genv_names   = names;
        genv_types   = types;
        genv_import  = import;
        genv_current = current;
        genv_next    = [];
        genv_boot    = boot
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
