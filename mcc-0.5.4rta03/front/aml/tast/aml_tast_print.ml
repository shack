(*
 * Print explicitly-typed expressions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format
open Debug
open Symbol
open Location
open Attribute
open Field_table

open Aml_tast
open Aml_tast_ds

let debug_alpha =
   create_debug (**)
      { debug_name = "alpha";
        debug_description = "show alpha indices of variables";
        debug_value = false
      }

(************************************************************************
 * UTILITIES                                                            *
 ************************************************************************)

(*
 * Default tabstop.
 *)
let tabstop = 3

(*
 * Name printing.
 *)
let rec pp_print_mod_name buf me_name =
   match me_name with
      ModNameEnv me_var ->
         pp_print_symbol buf me_var
    | ModNameSelf (me_var, ext_var) ->
         fprintf buf "self[%a.%a]" (**)
            pp_print_symbol me_var
            pp_print_symbol ext_var
    | ModNameProj (mod_name, ext_var) ->
         fprintf buf "%a.%a" (**)
            pp_print_mod_name mod_name
            pp_print_symbol ext_var
    | ModNameApply (mod_name1, mod_name2) ->
         fprintf buf "%a(%a)" (**)
            pp_print_mod_name mod_name1
            pp_print_mod_name mod_name2

let pp_print_var_name buf name =
   match name with
      NameEnv v ->
         pp_print_symbol buf v
    | NamePath (mod_name, ext_var) ->
         pp_print_mod_name buf mod_name;
         pp_print_string buf ".";
         pp_print_symbol buf ext_var

let pp_print_type_name buf name =
   match name with
      TyNameSoVar v ->
         pp_print_symbol buf v
    | TyNameSelf (me_var, ext_var) ->
         fprintf buf "$self[%a.%a]" pp_print_symbol me_var pp_print_symbol ext_var
    | TyNamePath (mod_name, ext_var) ->
         pp_print_mod_name buf mod_name;
         pp_print_string buf ".";
         pp_print_symbol buf ext_var

let pp_print_type_kind buf { kind_arity = k; kind_value = name } =
   fprintf buf "%a[%d]" pp_print_type_name name k

(*
 * Type parameter string.
 *)
let param_string_of_index i =
   let letter = i mod 26 in
   let index = i / 26 in
   let c = Char.chr (Char.code 'a' + letter) in
      if index <> 0 then
         Printf.sprintf "%c%d" c index
      else
         String.make 1 c

(*
 * Print a list of vars
 *)
let rec pp_print_params first buf = function
   v :: vars ->
      if not first then
         pp_print_string buf ", ";
      pp_print_string buf "'";
      pp_print_symbol buf v;
      pp_print_params false buf vars
 | [] ->
      ()

let pp_print_params = pp_print_params true

(*
 * Print the names.
 *)
let rec pp_print_names first buf = function
   { kind_value = v; kind_arity = k } :: vars ->
      if not first then
         pp_print_string buf ", ";
      fprintf buf "'%a[%d]" pp_print_symbol v k;
      pp_print_names false buf vars
 | [] ->
      ()

let pp_print_names = pp_print_names true

(************************************************************************
 * FREE VARS
 ************************************************************************)

(*
 * Print the free variable set.
 *)
let pp_print_free_vars buf fv =
   let { fv_fo_vars = fo_vars;
         fv_so_vars = so_vars;
         fv_vars    = vars;
         fv_me_vars = me_vars
       } = fv
   in
   let print_set s vars =
      fprintf buf "@ @[<hv 0>@[<b 3>free %s {" s;
      SymbolSet.iter (fun v ->
            fprintf buf "@ %a" pp_print_symbol v) vars;
      fprintf buf "@]@ }@]"
   in
      fprintf buf "@[<hv 0>@[<hv 3>Free vars {";
      print_set "fo_vars" fo_vars;
      print_set "so_vars" so_vars;
      print_set "me_vars" me_vars;
      print_set "vars"    vars;
      fprintf buf "@]@ }@]"

(*
 * Print a substitution.
 *)
let rec pp_print_subst buf subst =
   let { sub_fo_vars = fo_vars;
         sub_so_vars = so_vars;
         sub_me_vars = me_vars;
         sub_vars    = vars;
         sub_loc     = loc
       } = subst
   in
      fprintf buf "@[<hv 0>@[<hv 3>subst {@ @[<hv 0>@[<hv 3>fo_vars = {";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" pp_print_symbol v pp_print_type ty) fo_vars;
      fprintf buf "@]@ }@]@ @[<hv 0>@[<hv 3>so_vars = {";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" pp_print_symbol v pp_print_type_kind ty) so_vars;
      fprintf buf "@]@ }@]@ @[<hv 0>@[<hv 3>me_vars = {";
      SymbolTable.iter (fun v me_name ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_mod_name me_name) me_vars;
      fprintf buf "@]@ }@]@ @[<hv 0>@[<hv 3>vars = {";
      SymbolTable.iter (fun v (me_name, label) ->
            fprintf buf "@ @[<hv 3>%a =@ %a.%a@]" (**)
               pp_print_symbol v
               pp_print_mod_name me_name
               pp_print_symbol label) vars;
      fprintf buf "@]@ }@]";
      (match loc with
          Some loc ->
             fprintf buf "@ loc = %a" pp_print_location loc
        | None ->
             ());
      fprintf buf "@]@ }@]"

(************************************************************************
 * TYPES                                                                *
 ************************************************************************)

(*
 * Types.
 *)
and pp_print_type buf ty =
   pp_open_hvbox buf 0;
   pp_print_type_core buf (dest_type_core ty);
   pp_close_box buf ()

and pp_print_type_core buf = function
   TyInt ->
      pp_print_string buf "$int"
 | TyChar ->
      pp_print_string buf "$char"
 | TyString ->
      pp_print_string buf "$string"
 | TyFloat ->
      pp_print_string buf "$float"

 | TyFun (tyl, ty) ->
      fprintf buf "@[<hv 1>(%a)@ -> %a@]" (**)
         (pp_print_type_params true) tyl
         pp_print_type ty

 | TyFunctor (v, t1, t2) ->
      fprintf buf "@[<hv 3>functor (%a :@ %a) -> %a@]" (**)
         pp_print_symbol v
         pp_print_type t1
         pp_print_type t2

 | TyProd types ->
      pp_print_string buf "(";
      pp_open_hvbox buf 0;
      pp_print_type_prod true buf types;
      pp_print_string buf ")";
      pp_close_box buf ()

 | TyArray ty ->
      pp_print_type buf ty;
      pp_print_string buf " $array"

 | TyVar v ->
      pp_print_string buf "'";
      pp_print_symbol buf v

 | TySOVar (v, tyl) ->
      fprintf buf "@[<hv 3>'%a[%a]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl

 | TyProject (name, params) ->
      fprintf buf "@[<hv 3>project:%a@,[%a]@]" (**)
         pp_print_type_name name
         (pp_print_type_params true) params

 | TyApply (v, tyl1, tyl2) ->
      fprintf buf "@[<hv 3>apply:%a@,[@[<hv 0>%a@]]@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl1
         (pp_print_type_kinds true) tyl2

 | TyUnion (v, tyl1, tyl2) ->
      fprintf buf "@[<hv 3>union:%a@,[@[<hv 0>%a@]]@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl1
         (pp_print_type_kinds true) tyl2

 | TyRecord (v, tyl1, tyl2) ->
      fprintf buf "@[<hv 3>record:%a@,[@[<hv 0>%a@]]@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl1
         (pp_print_type_kinds true) tyl2

 | TyModule (v, tyl1, tyl2) ->
      fprintf buf "@[<hv 3>module:%a@,[@[<hv 0>%a@]]@,[@[<hv 0>%a@]]@]" (**)
         pp_print_symbol v
         (pp_print_type_params true) tyl1
         (pp_print_type_kinds true) tyl2

 | TyAll (vars, ty) ->
      pp_print_string buf "all (";
      pp_print_params buf vars;
      pp_print_string buf "). ";
      pp_print_type buf ty

 | TyExternal (ty, s) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(external ";
      pp_print_string buf ("\"" ^ String.escaped s ^ "\"");
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ()
 | TyFormat (t1, t2, t3) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "((";
      pp_print_type buf t1;
      pp_print_string buf ",";
      pp_print_space buf ();
      pp_print_type buf t2;
      pp_print_string buf ",";
      pp_print_space buf ();
      pp_print_type buf t3;
      pp_print_string buf ") $format)";
      pp_close_box buf ()

 | TyOpen (vars, v) ->
      pp_open_hvbox buf 3;
      pp_print_string buf "open ";
      pp_print_symbol buf v;
      pp_print_string buf " {";
      pp_print_type_vars true buf vars;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "}"

and pp_print_type_vars first buf = function
   (l, ty) :: tl ->
      if not first then
         begin
            pp_print_string buf ";";
            pp_print_space buf ()
         end;
      pp_open_hvbox buf tabstop;
      pp_print_symbol buf l;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ();
      pp_print_type_vars false buf tl

 | [] ->
      ()

and pp_print_type_prod first buf = function
   ty :: tl ->
      if not first then
         begin
            pp_print_space buf ();
            pp_print_string buf "* "
         end;
      pp_print_type buf ty;
      pp_print_type_prod false buf tl
 | [] ->
      ()

and pp_print_type_params first buf = function
   ty :: tl ->
      if not first then
         begin
            pp_print_string buf ",";
            pp_print_space buf ()
         end;
      pp_print_type buf ty;
      pp_print_type_params false buf tl
 | [] ->
      ()

and pp_print_type_kinds first buf = function
   ty_name :: tl ->
      if not first then
         begin
            pp_print_string buf ",";
            pp_print_space buf ()
         end;
      pp_print_type_kind buf ty_name;
      pp_print_type_kinds false buf tl
 | [] ->
      ()

let rec pp_print_type_vars first buf = function
   v :: tl ->
      if not first then
         pp_print_string buf ", ";
      fprintf buf "'%a" pp_print_symbol v;
      pp_print_type_vars false buf tl
 | [] ->
      ()

let pp_print_type_vars buf = function
   [] ->
      ()
 | [v] ->
      fprintf buf "'%a " pp_print_symbol v
 | vars ->
      fprintf buf "(%a) " (pp_print_type_vars true) vars

let rec pp_print_type_args first buf = function
   ty :: args ->
      if not first then
         pp_print_string buf ", ";
      pp_print_type buf ty;
      pp_print_type_args false buf args
 | [] ->
      ()

(*
 * Abstract type.
 *)
let pp_print_type_opt buf ty_opt =
   match ty_opt with
      Some ty ->
         pp_print_type buf ty
    | None ->
         pp_print_string buf "<abstr>"

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Print a record.
 *)
let rec pp_print_type_record first buf fields =
   let _ =
      FieldTable.fold (fun first l (mflag, ty) ->
            if not first then
               begin
                  pp_print_string buf ";";
                  pp_print_space buf ()
               end;
            pp_open_hvbox buf tabstop;
            if mflag then
               pp_print_string buf "mutable ";
            pp_print_symbol buf l;
            pp_print_string buf " :";
            pp_print_space buf ();
            pp_print_type buf ty;
            pp_close_box buf ();
            false) true fields
   in
      ()

(*
 * Print the union fields.
 *)
let rec pp_print_type_union first buf fields =
   let _ =
      FieldTable.fold (fun first v tyl ->
            if not first then
               begin
                  pp_print_space buf ();
                  pp_print_string buf "| "
               end;
            pp_open_hvbox buf tabstop;
            pp_print_symbol buf v;
            if tyl <> [] then
               begin
                  pp_print_string buf " of ";
                  pp_print_type_prod true buf tyl
               end;
            pp_close_box buf ();
            false) true fields
   in
      ()

(*
 * Print the name record.
 *)
let pp_print_module_names buf names =
   let { names_mt = mt_names;
         names_me = me_names;
         names_ty = ty_names;
         names_var = var_names;
         names_const = const_names;
         names_label = label_names
       } = names
   in
   let print_any_names fst s names =
      SymbolTable.iter (fun ext_var v ->
            fprintf buf "@ export %s %a = %a" (**)
               s
               pp_print_symbol ext_var
               pp_print_symbol (fst v)) names
   in
   let print_table_names s names =
      FieldTable.iter (fun ext_var v ->
            fprintf buf "@ export %s %a = %a" (**)
               s
               pp_print_symbol ext_var
               pp_print_symbol v) names
   in
   let print_mt_names = print_any_names fst in
   let print_var_names = print_any_names (fun v -> v) in
      print_mt_names "module type" mt_names;
      print_table_names "module" me_names;
      print_var_names "type" ty_names;
      print_table_names "val" var_names;
      print_var_names "const" const_names;
      print_var_names "label" label_names

(*
 * Print a constructor field.
 *)
let pp_print_const_field buf field =
   match field with
      ConstNormal kinds ->
         fprintf buf "normal (%a)" (pp_print_type_kinds true) kinds
    | ConstException tyl ->
         fprintf buf "exception (%a)" (pp_print_type_prod true) tyl

(*
 * Print a module.
 *)
let pp_print_module_type buf mt =
   let { mt_self = me_var;
         mt_mt_types = mt_types;
         mt_me_vars = me_vars;
         mt_types = types;
         mt_vars = vars;
         mt_consts = consts;
         mt_labels = labels;
         mt_names = names
       } = mt
   in
      fprintf buf "@[<hv 0>@[<hv 3>sig[%a]" pp_print_symbol me_var;
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>module type %a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_type ty) mt_types;
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>module %a :@ %a@]" (**)
               pp_print_symbol v
               pp_print_type ty) me_vars;
      SymbolTable.iter (fun v (ty_vars, ty_opt) ->
            fprintf buf "@ @[<hv 3>type %a%a =@ %a@]" (**)
               pp_print_type_vars ty_vars
               pp_print_symbol v
               pp_print_type_opt ty_opt) types;
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>val %a :@ %a@]" (**)
               pp_print_symbol v
               pp_print_type ty) vars;
      SymbolTable.iter (fun v (ty_var, arity, field) ->
            fprintf buf "@ @[<hv 3>const %a = %a[%d][%a]@]" (**)
               pp_print_symbol v
               pp_print_symbol ty_var
               arity
               pp_print_const_field field) consts;
      SymbolTable.iter (fun v (ty_var, arity, kinds) ->
            fprintf buf "@ @[<hv 3>label %a = %a[%d][%a]@]" (**)
               pp_print_symbol v
               pp_print_symbol ty_var
               arity
               (pp_print_type_kinds true) kinds) labels;
      fprintf buf "%a@]@ end@]" pp_print_module_names names

(*
 * Print a type definition.
 *)
let pp_print_union_kind buf kind =
   let s =
      match kind with
         NormalUnion -> "normal"
       | ExnUnion -> "exn"
   in
      pp_print_string buf s

let pp_print_tydef_inner_core buf tydef =
   match tydef with
      TyDefRecord rty ->
         pp_open_hvbox buf 0;
         pp_print_string buf "{ ";
         pp_open_hvbox buf 0;
         pp_print_type_record true buf rty;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "}";
         pp_close_box buf ()

    | TyDefUnion (kind, fields) ->
         fprintf buf "@[<hv 0>union[%a] %a@]" (**)
            pp_print_union_kind kind
            (pp_print_type_union true) fields

    | TyDefLambda ty ->
         pp_print_type buf ty

    | TyDefModule (mt) ->
         pp_print_module_type buf mt

let pp_print_tydef_inner buf tydef =
   pp_print_tydef_inner_core buf (dest_tydef_inner_core tydef)

let pp_print_tydef buf tydef =
   let { tydef_fo_vars = params;
         tydef_so_vars = names;
         tydef_inner = tydef
       } = tydef
   in
      fprintf buf "@[<hv 3>Lambda [%a][%a].@ %a@]" (**)
         pp_print_params params
         pp_print_names names
         pp_print_tydef_inner tydef

(************************************************************************
 * PATTERNS                                                             *
 ************************************************************************)

(*
 * Print patterns.
 *)
let rec pp_print_patt buf patt =
   pp_open_hvbox buf 0;
   pp_print_patt_core buf (dest_patt_core patt);
   pp_close_box buf ()

and pp_print_patt_core buf = function
   PattInt (i, _) ->
      fprintf buf "$patt_int[%d]" i
 | PattChar (c, _) ->
      fprintf buf "$patt_char['%s']" (Char.escaped c)
 | PattString (s, _) ->
      fprintf buf "$patt_string[\"%s\"]" (String.escaped s)
 | PattWild ty ->
      fprintf buf "@[<hv 3>($patt_wild@ : %a)@]" pp_print_type ty

 | PattVar (v, ty) ->
      fprintf buf "@[<hv 3>($patt_var[%a]@ : %a)@]" (**)
         pp_print_symbol v
         pp_print_type ty

 | PattRecord (fields, ty) ->
      fprintf buf "@[<hv 0>@[<hv 3>$patt_record {";
      pp_print_patt_fields true buf fields;
      fprintf buf "@]@ } : %a@]" pp_print_type ty

 | PattArray (pl, ty) ->
      fprintf buf "@[<hv 3>([|";
      pp_print_patt_array true buf pl;
      fprintf buf "|]@ : %a)" pp_print_type ty

 | PattConst (ty_var, const_var, pl, ty) ->
      fprintf buf "@[<hv 3>$patt_const(%a.%a (%a) :@ %a)@]" (**)
         pp_print_symbol ty_var
         pp_print_symbol const_var
         (pp_print_patt_tuple true) pl
         pp_print_type ty

 | PattChoice (p1, p2, ty) ->
      fprintf buf "@[<hv 1>(($patt_choice@ %a@ | %a)@ : %a)@]" (**)
         pp_print_patt p1
         pp_print_patt p2
         pp_print_type ty

 | PattAs (p1, p2, ty) ->
      fprintf buf "@[<hv 1>(($patt_as@ %a@ as %a)@ : %a)@]" (**)
         pp_print_patt p1
         pp_print_patt p2
         pp_print_type ty

 | PattTuple (pl, ty) ->
      fprintf buf "@[<hv 1>(($patt_tuple";
      pp_print_patt_tuple true buf pl;
      fprintf buf ")@ : %a)" pp_print_type ty

 | PattExpr (e, ty) ->
      fprintf buf "@[<hv 1>(($patt_expr %a)@ : %a)@]" (**)
         (pp_print_exp false) e
         pp_print_type ty

 | PattRange (e1, e2, ty) ->
      fprintf buf "@[<hv 1>(($patt_range %a..%a)@ : %a)@]" (**)
         (pp_print_exp false) e1
         (pp_print_exp false) e2
         pp_print_type ty

 | PattWhen (p, e, ty) ->
      fprintf buf "@[<hv 3>(($patt_when %a@ when %a)@ : %a)@]" (**)
         pp_print_patt p
         (pp_print_exp false) e
         pp_print_type ty

 | PattConstrain (p, ty) ->
      fprintf buf "@[<hv 3>($patt_constrain %a@ : %a)@]" (**)
         pp_print_patt p
         pp_print_type ty

(*
 * Print the array arguments.
 *)
and pp_print_patt_sep sep first buf = function
   p :: pl ->
      if not first then
         begin
            pp_print_string buf sep;
            pp_print_space buf ()
         end;
      pp_print_patt buf p;
      pp_print_patt_sep sep false buf pl
 | [] ->
      ()

and pp_print_patt_array first buf pl =
   pp_print_patt_sep ";" first buf pl

and pp_print_patt_tuple first buf pl =
   pp_print_patt_sep "," first buf pl

(*
 * Record.
 *)
and pp_print_patt_fields first buf = function
   (v, p) :: pl ->
      if not first then
         begin
            pp_print_string buf ";";
            pp_print_space buf ()
         end;
      pp_open_hvbox buf tabstop;
      pp_print_symbol buf v;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_patt buf p;
      pp_close_box buf ();
      pp_print_patt_fields false buf pl
 | [] ->
      ()

(*
 * Print a sequence of pattern arguments.
 *)
and pp_print_patts buf patts =
   List.iter (fun p ->
         pp_print_string buf " ";
         pp_print_patt buf p) patts

(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)

(*
 * Print a first-level piece of code.
 *)
and pp_print_exp inlet buf exp =
   pp_open_hvbox buf 0;
   pp_print_exp_core inlet buf (dest_exp_core exp);
   pp_close_box buf ()

and pp_print_exp_core inlet buf = function
   Number (i, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "(";
      pp_print_int buf i;
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf ")"
 | Char (c, _) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_char buf '\'';
      pp_print_string buf (String.escaped (String.make 1 c));
      print_char '\''
 | String (s, _) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf ("\"" ^ String.escaped s ^ "\"")

 | Var (v, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "(";
      pp_print_var_name buf v;
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | Const (ty_var, const_var, args, ty1, ty2) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "@[<hv 3>(%a.%a %a :@ [%a =@ %a])@]" (**)
         pp_print_symbol ty_var
         pp_print_symbol const_var
         pp_print_args args
         pp_print_type ty1
         pp_print_type ty2

 | Apply (e1, ty_fun, args, ty_res) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "(@[<hv 0>@[<hv 3>(%a@ : %a)@](%a)@ : %a@])" (**)
         (pp_print_exp false) e1
         pp_print_type ty_fun
         pp_print_args args
         pp_print_type ty_res

 | Lambda (ty_vars, vars, e, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "@[<hv 3>((fun [%a](%a)@ -> %a)@ : %a)@]" (**)
         pp_print_params ty_vars
         pp_print_patts vars
         (pp_print_exp false) e
         pp_print_type ty

 | Function (ty_vars, gc, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "@[<hv 3>((function [%a]%a)@ : %a)@]" (**)
         pp_print_params ty_vars
         (pp_print_commands true) gc
         pp_print_type ty

 | Let (lets, e2, ty) ->
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_print_lets buf lets;
      pp_print_string buf " in";
      pp_print_space buf ();
      pp_print_exp true buf e2;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ();

 | LetRec (letrecs, e2, ty) ->
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_print_letrecs buf letrecs;
      pp_print_string buf " in";
      pp_print_space buf ();
      pp_print_exp true buf e2;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | If (e1, e2, e3, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "if ";
      pp_print_exp false buf e1;
      pp_print_string buf " then";
      pp_print_space buf ();
      pp_print_exp false buf e2;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "else";
      pp_print_space buf ();
      pp_print_exp false buf e3;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ();
      pp_close_box buf ()

 | For (v, e1, to_flag, e2, e3) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "for ";
      pp_print_symbol buf v;
      pp_print_string buf " = ";
      pp_print_exp false buf e1;
      pp_print_string buf (if to_flag then " to " else " downto ");
      pp_print_exp false buf e2;
      pp_print_string buf " do";
      pp_print_space buf ();
      pp_print_exp false buf e3;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "done";
      pp_close_box buf ()

 | While (e1, e2) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "while ";
      pp_print_exp false buf e1;
      pp_print_string buf " do";
      pp_print_space buf ();
      pp_print_exp false buf e2;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "done";
      pp_close_box buf ()

 | Constrain (e, ty) ->
      fprintf buf "@[<hv 3>(%a@ : %a)@]" (**)
         (pp_print_exp true) e
         pp_print_type ty

 | ApplyType (e, args, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_print_exp false buf e;
      pp_print_space buf ();
      pp_print_string buf "[";
      pp_print_type_args true buf args;
      pp_print_string buf "]) :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | Tuple (el, ty) ->
      pp_print_string buf "(";
      pp_print_tuple buf inlet el "(" "," ")";
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | Array (el, ty) ->
      pp_print_string buf "(";
      pp_print_tuple buf inlet el "[|" ";" "|]";
      pp_print_string buf " : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | ArraySubscript (e1, e2, ty) ->
      pp_print_string buf "((";
      pp_print_subscript buf inlet e1 e2 ".(" ")";
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")"
 | StringSubscript (e1, e2) ->
      pp_print_subscript buf inlet e1 e2 ".[" "]"
 | ArraySetSubscript (e1, e2, e3) ->
      pp_print_assign buf inlet e1 e2 e3 ".(" ")"
 | StringSetSubscript (e1, e2, e3) ->
      pp_print_assign buf inlet e1 e2 e3 ".[" "]"

 | Record (items, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_open_hvbox buf 0;
      pp_print_string buf "({ ";
      pp_open_hvbox buf 0;
      pp_print_record_items true buf items;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "} : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | RecordUpdate (e, items, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "@[<hv 0>({@[<hv 0>%a with@ %a@]@ } :@ %a)@]" (**)
         (pp_print_exp false) e
         (pp_print_record_items true) items
         pp_print_type ty

 | RecordProj (e, ty1, v, ty2) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "@[<hv 3>((%a : %a).%a :@ %a)@]" (**)
         (pp_print_exp false) e
         pp_print_type ty1
         pp_print_symbol v
         pp_print_type ty2

 | RecordSetProj (e1, ty, v, e2) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      fprintf buf "@[<hv 3>((%a : %a).%a <-@ %a)@]" (**)
         (pp_print_exp false) e1
         pp_print_type ty
         pp_print_symbol v
         (pp_print_exp false) e2

 | Sequence (e1, e2, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_print_exp false buf e1;
      pp_print_string buf ";";
      pp_print_space buf ();
      pp_print_exp false buf e2;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | Match (e, gc, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "match ";
      pp_print_exp false buf e;
      pp_print_string buf " with";
      pp_close_box buf ();
      pp_print_commands true buf gc;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | Try (e, gc, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "try ";
      pp_print_exp false buf e;
      pp_print_string buf " with";
      pp_close_box buf ();
      pp_print_commands true buf gc;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | Raise (e, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf tabstop;
      pp_print_string buf "raise";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_print_string buf ") :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | In (n, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_print_string buf "in ";
      pp_print_mod_name buf n;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | Out (n, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_print_string buf "out ";
      pp_print_mod_name buf n;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | Open (n, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_print_string buf "open ";
      pp_print_mod_name buf n;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")"

 | Action (args, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf tabstop;
      pp_print_string buf "action ";
      List.iter (pp_print_arg buf) args;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | Restrict (v, mt, e, ty) ->
      pp_print_string buf "((";
      pp_open_hvbox buf tabstop;
      pp_print_string buf "restrict ";
      pp_print_symbol buf v;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_type buf mt;
      pp_print_string buf " ->";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | Ambient (v, me, mt) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_open_hvbox buf 0;
      pp_print_string buf "ambient ";
      pp_print_symbol buf v;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_type buf mt;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_open_vbox buf 0;
      pp_print_module_exp buf me;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "end";
      pp_close_box buf ()

 | Thread e ->
      if inlet then
         pp_print_break buf 1 tabstop
      else
         pp_print_string buf "(";
      pp_open_hvbox buf tabstop;
      pp_print_string buf "migrant";
      pp_print_space buf ();
      pp_print_exp false buf e;
      if not inlet then
         pp_print_string buf ")";
      pp_close_box buf ()

 | Migrate (msg, e, ty) ->
      if inlet then
         pp_print_break buf 1 tabstop;
      pp_print_string buf "((";
      pp_open_hvbox buf tabstop;
      pp_print_string buf "migrate ";
      pp_print_exp false buf msg;
      pp_print_string buf " ->";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

 | LetModule (v, me, e, ty) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "((let module ";
      pp_print_symbol buf v;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_module_exp buf me;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "in";
      pp_print_exp true buf e;
      pp_print_string buf ") : ";
      pp_print_type buf ty;
      pp_print_string buf ")";
      pp_close_box buf ()

(*
 * Print a list of exp with a separator.
 *)
and pp_print_exps first buf sep = function
   e :: el ->
      if not first then
         begin
            pp_print_string buf sep;
            pp_print_space buf ()
         end;
      pp_print_exp false buf e;
      pp_print_exps false buf sep el
 | [] ->
      ()

(*
 * Tupling.
 *)
and pp_print_tuple buf inlet el start middle finish =
   if inlet then
      pp_print_break buf 1 tabstop;
   pp_print_string buf start;
   pp_open_hvbox buf 0;
   pp_print_exps true buf middle el;
   pp_print_string buf finish;
   pp_close_box buf ()

(*
 * Print a subscript operation.
 *)
and pp_print_subscript buf inlet e1 e2 str1 str2 =
   if inlet then
      pp_print_break buf 1 tabstop;
   pp_open_hvbox buf tabstop;
   pp_print_exp false buf e1;
   pp_print_string buf str1;
   pp_print_exp false buf e2;
   pp_print_string buf str2;
   pp_close_box buf ()

and pp_print_assign buf inlet e1 e2 e3 str1 str2 =
   if inlet then
      pp_print_break buf 1 tabstop;
   pp_open_hvbox buf tabstop;
   pp_print_exp false buf e1;
   pp_print_string buf str1;
   pp_print_exp false buf e2;
   pp_print_string buf str2;
   pp_print_string buf " <-";
   pp_print_space buf ();
   pp_print_exp false buf e3;
   pp_close_box buf ()

(*
 * Print some record items.
 *)
and pp_print_record_items first buf = function
   (v, e) :: items ->
      if not first then
         begin
            pp_print_string buf ";";
            pp_print_space buf ()
         end;
      pp_open_hvbox buf tabstop;
      pp_print_symbol buf v;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_close_box buf ();
      pp_print_record_items false buf items
 | [] ->
      ()

(*
 * Print the arguments.
 *)
and pp_print_arg buf e =
   pp_print_space buf ();
   pp_print_exp false buf e

and pp_print_args buf args =
   ignore (List.fold_left (fun first e ->
                 if not first then
                    fprintf buf "@ ,";
                 pp_print_exp false buf e;
                 false) true args)

(*
 * Print the let bindings.
 *)
and pp_print_lets buf lets =
   let rec pp_print_lets first buf = function
      (p, e, ty) :: lets ->
         if first then
            begin
               pp_open_hvbox buf tabstop;
               pp_print_string buf "let ";
            end
         else
            begin
               pp_print_space buf ();
               pp_open_hvbox buf tabstop;
               pp_print_string buf "and "
            end;
         pp_print_patt buf p;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_string buf "(";
         pp_print_exp false buf e;
         pp_print_string buf " : ";
         pp_print_type buf ty;
         pp_print_string buf ")";
         pp_close_box buf ();
         pp_print_lets false buf lets
    | [] ->
         ()
   in
      pp_print_lets true buf lets

(*
 * Print some function bindings.
 *)
and pp_print_letrecs buf letrecs =
   let rec pp_print_letrecs flag buf = function
      (f, e, ty1, ty2) :: letrecs ->
         if flag then
            begin
               pp_open_hvbox buf tabstop;
               pp_print_string buf "let rec "
            end
         else
            begin
               pp_print_space buf ();
               pp_open_hvbox buf tabstop;
               pp_print_string buf "and "
            end;
         pp_print_string buf "(";
         pp_print_symbol buf f;
         pp_print_string buf " : ";
         pp_print_type buf ty1;
         pp_print_string buf " = ";
         pp_print_type buf ty2;
         pp_print_string buf ")";
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_exp false buf e;
         pp_close_box buf ();
         pp_print_letrecs false buf letrecs
    | [] ->
         ()
   in
      pp_print_letrecs true buf letrecs

(*
 * Guarded commands.
 *)
and pp_print_commands first buf = function
   (p, e) :: pel ->
      pp_print_space buf ();
      if not first then
         pp_print_string buf "| ";
      pp_open_hvbox buf tabstop;
      pp_print_patt buf p;
      pp_print_string buf " ->";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_close_box buf ();
      pp_print_commands false buf pel
 | [] ->
      ()

(*
 * Print the type declarations.
 *)
and pp_print_types first buf = function
   (v, ty) :: types ->
      if first then
         begin
            pp_open_hvbox buf tabstop;
            pp_print_string buf "type "
         end
      else
         begin
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "and "
         end;
      pp_print_symbol buf v;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ();
      pp_print_types false buf types
 | [] ->
      ()

(************************************************************************
 * MODULES                                                              *
 ************************************************************************)

(*
 * Structure items.
 *)
and pp_print_module_field buf field =
   match dest_field_core field with
      FieldLet lets ->
         pp_print_lets buf lets

    | FieldLetRec letrecs ->
         pp_print_letrecs buf letrecs

    | FieldType subst ->
         pp_print_type_subst buf subst

    | FieldExpr e ->
         pp_print_exp false buf e

    | FieldRestrict (v, mt) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "restrict ";
         pp_print_symbol buf v;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf mt;
         pp_close_box buf ()

    | FieldModule (v, me, mt) ->
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "module ";
         pp_print_symbol buf v;
         pp_print_space buf ();
         pp_print_string buf ": (";
         pp_open_hvbox buf 0;
         pp_print_type buf mt;
         pp_print_string buf ") =";
         pp_close_box buf ();
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_module_exp buf me;
         pp_close_box buf ()

    | FieldModuleType (v, mt) ->
         pp_open_hvbox buf 0;
         pp_print_string buf "module type ";
         pp_print_symbol buf v;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_type buf mt;
         pp_close_box buf ()

    | FieldException (const_var, ty_var, tyl) ->
         fprintf buf "@[<hv 3>exception[%a] %a" (**)
            pp_print_symbol ty_var
            pp_print_symbol const_var;
         if tyl <> [] then
            fprintf buf "@ of %a" (pp_print_type_prod true) tyl;
         fprintf buf "@]"

    | FieldExternal (v, ty, s) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "external ";
         pp_print_symbol buf v;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf ty;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"");
         pp_close_box buf ()

and pp_print_module_fields buf = function
   item :: items ->
      pp_print_space buf ();
      pp_print_module_field buf item;
      pp_print_module_fields buf items
 | [] ->
      ()

and pp_print_type_subst buf subst =
   ignore (List.fold_left (fun first (v, ty_vars, ty_opt) ->
                 if first then
                    fprintf buf "@[<hv 3>type "
                 else
                    fprintf buf "@ @[<hv 3>and ";
                 fprintf buf "%a%a =@ %a@]" (**)
                    pp_print_type_vars ty_vars
                    pp_print_symbol v
                    pp_print_type_opt ty_opt;
                 false) true subst)

(*
 * Module expression.
 *)
and pp_print_module_exp buf me =
   pp_print_module_exp_core buf (dest_module_exp_core me)

and pp_print_module_exp_core buf = function
   ModuleExpStruct (me_var, items, names, mt) ->
      fprintf buf "@[<hv 0>@[<hv 3>struct[%a]%a%a@]@ end :@ %a@]" (**)
         pp_print_symbol me_var
         pp_print_module_fields items
         pp_print_module_names names
         pp_print_type mt

 | ModuleExpVar (id, mt) ->
      pp_print_string buf "(";
      pp_print_mod_name buf id;
      pp_print_string buf " : ";
      pp_print_type buf mt;
      pp_print_string buf ")"

 | ModuleExpConstrain (me, mt) ->
      fprintf buf "@[<hv 0>%a@ : %a@]" (**)
         pp_print_module_exp me
         pp_print_type mt

 | ModuleExpFunctor (me_var, mt1, me, mt2) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "((functor ";
      pp_print_symbol buf me_var;
      pp_print_string buf " : (";
      pp_print_type buf mt1;
      pp_print_string buf ") ->";
      pp_print_space buf ();
      pp_print_module_exp buf me;
      pp_print_string buf ") :";
      pp_print_space buf ();
      pp_print_type buf mt2;
      pp_print_string buf ")";
      pp_close_box buf ()

 | ModuleExpApply (me, mn, mt) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "((";
      pp_print_module_exp buf me;
      pp_print_string buf ")";
      pp_print_space buf ();
      pp_print_string buf "(";
      pp_print_mod_name buf mn;
      pp_print_string buf ") :";
      pp_print_space buf ();
      pp_print_type buf mt;
      pp_print_string buf ")";
      pp_close_box buf ()

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Print the boot record.
 *)
let pp_print_boot buf boot =
   let { boot_ty_unit     = ty_unit;
         boot_ty_unit_var = ty_unit_var;
         boot_unit_var    = unit_var;

         boot_ty_bool     = ty_bool;
         boot_ty_bool_var = ty_bool_var;
         boot_true_var    = true_var;
         boot_false_var   = false_var;

         boot_ty_exn      = ty_exn;
         boot_ty_exn_var  = ty_exn_var
       } = boot
   in
      fprintf buf "@ ty_unit = %a" pp_print_type_core ty_unit;
      fprintf buf "@ ty_unit_var = %a" pp_print_symbol ty_unit_var;
      fprintf buf "@ unit_var = %a" pp_print_symbol unit_var;

      fprintf buf "@ ty_bool = %a" pp_print_type_core ty_bool;
      fprintf buf "@ ty_bool_var = %a" pp_print_symbol ty_bool_var;
      fprintf buf "@ true_var = %a" pp_print_symbol true_var;
      fprintf buf "@ false_var = %a" pp_print_symbol false_var;

      fprintf buf "@ ty_exn = %a" pp_print_type_core ty_exn;
      fprintf buf "@ ty_exn_var = %a" pp_print_symbol ty_exn_var

(*
 * Print the expression.
 *)
let pp_print_prog_exp buf exp =
   match exp with
      Interface ty ->
         fprintf buf "@[<hv 0>@[<hv 3>sig@ %a@]@ end@]" pp_print_type ty
    | Implementation (me_var, items, names, mt) ->
         fprintf buf "@[<hv 0>@[<hv 3>struct[%a]%a%a@]@ end :@ %a@]" (**)
            pp_print_symbol me_var
            pp_print_module_fields items
            pp_print_module_names names
            pp_print_type mt

(*
 * Print the program.
 *)
let pp_print_prog buf prog =
   let { prog_names  = names;
         prog_types  = types;
         prog_import = import;
         prog_exp    = exp;
         prog_boot   = boot
       } = prog
   in
      fprintf buf "@[<v 0>AML.TAST Program:";

      fprintf buf "@ @[<hv 3>Boot:%a@]" pp_print_boot boot;

      fprintf buf "@ @[<hv 3>Types:";
      SymbolTable.iter (fun v tydef ->
            fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
               pp_print_symbol v
               pp_print_tydef tydef) types;

      fprintf buf "@]@ @[<hv 3>Import:";
      SymbolTable.iter (fun v (hash, me_var, ty) ->
            fprintf buf "@ @[<hv 0>@[<hv 3>%a = {@ hash = 0x%08x;@ me_var = %a;@ type = %a@]@ }@]" (**)
               pp_print_symbol v
               hash
               pp_print_symbol me_var
               pp_print_type ty) import;

      fprintf buf "@]@ @[<hv 3>Names:";
      SymbolTable.iter (fun v ext_var ->
            fprintf buf "@ export %a = %a" pp_print_symbol ext_var pp_print_symbol v) names;

      fprintf buf "@]@ @[<hv 3>Exp:@ %a@]@]" pp_print_prog_exp exp

(************************************************************************
 * EXTERNAL                                                             *
 ************************************************************************)

(*
 * Global functions.
 *)
let pp_print_exp buf e =
   pp_open_hvbox buf 0;
   pp_print_exp false buf e;
   pp_close_box buf ()

let pp_print_type buf ty =
   pp_open_hvbox buf 0;
   pp_print_type buf ty;
   pp_close_box buf ()

let pp_print_tydef buf tydef =
   pp_open_hvbox buf 0;
   pp_print_tydef buf tydef;
   pp_close_box buf ()

let pp_print_patt buf patt =
   pp_open_hvbox buf 0;
   pp_print_patt buf patt;
   pp_close_box buf ()

let pp_print_module_field buf item =
   pp_open_hvbox buf 0;
   pp_print_module_field buf item;
   pp_close_box buf ()

let pp_print_module_exp buf me =
   pp_open_vbox buf 0;
   pp_print_module_exp buf me;
   pp_close_box buf ()

(*
 * For debugging.
 *)
let pp_print_type_ds =
   pp_print_term pp_print_free_vars pp_print_subst pp_print_type_core

let debug str f x =
   eprintf "@[<v 0>*** AML: TAST: %s:@ %a@]@." str f x

let debug_exp str exp =
   debug str pp_print_exp exp

let debug_type str ty =
   debug str pp_print_type ty

let debug_patt str patt =
   debug str pp_print_patt patt

let debug_module_field str item =
   debug str pp_print_module_field item

let debug_module_exp str me =
   debug str pp_print_module_exp me

let debug_tydef str tydef =
   debug str pp_print_tydef tydef

let debug_prog str prog =
   debug str pp_print_prog prog

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
