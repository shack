(*
 * Operations on locations.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Location

open Aml_ast

(*
 * Get locations.
 *)
let loc_of_exp = function
   Number (_, loc)
 | Char (_, loc)
 | String (_, loc)
 | Const (_, loc)
 | Var (_, loc)
 | Apply (_, _, loc)
 | Lambda (_, _, loc)
 | Function (_, loc)
 | Let (_, _, loc)
 | LetRec (_, _, loc)
 | If (_, _, _, loc)
 | For (_, _, _, _, _, loc)
 | While (_, _, loc)
 | Constrain (_, _, loc)
 | Wrap (_, loc)

 | Tuple (_, loc)
 | Array (_, loc)
 | ArraySubscript (_, _, loc)
 | StringSubscript (_, _, loc)
 | ArraySetSubscript (_, _, _, loc)
 | StringSetSubscript (_, _, _, loc)
 | Record (_, loc)
 | RecordProj (_, _, loc)
 | RecordSetProj (_, _, _, loc)
 | RecordUpdate (_, _, loc)

 | Sequence (_, _, loc)

 | Match (_, _, loc)
 | Try (_, _, loc)
 | Raise (_, loc)

 | In (_, loc)
 | Out (_, loc)
 | Open (_, loc)
 | Action (_, loc)
 | Restrict (_, _, _, loc)
 | Ambient (_, _, loc)
 | Thread (_, loc)
 | Migrate (_, _, loc)
 | LetModule (_, _, _, loc) ->
      loc

let loc_of_patt = function
   PattInt (_, loc)
 | PattChar (_, loc)
 | PattString (_, loc)
 | PattWild loc
 | PattVar (_, loc)
 | PattTuple (_, loc)
 | PattRecord (_, loc)
 | PattArray (_, loc)
 | PattConst (_, _, loc)
 | PattChoice (_, _, loc)
 | PattAs (_, _, loc)
 | PattConstrain (_, _, loc)
 | PattWrap (_, loc)
 | PattExpr (_, loc)
 | PattRange (_, _, loc)
 | PattWhen (_, _, loc) ->
      loc

let loc_of_str_item = function
   StrItemLet (_, loc)
 | StrItemLetRec (_, loc)
 | StrItemExpr (_, loc)
 | StrItemRestrict (_, _, loc)
 | StrItemType (_, loc)
 | StrItemModule (_, _, loc)
 | StrItemModuleType (_, _, loc)
 | StrItemException (_, _, loc)
 | StrItemOpen (_, loc)
 | StrItemExternal (_, _, _, loc)
 | StrItemExternalType (_, _, _, loc) ->
      loc

let rec loc_of_str_items loc = function
   item :: items ->
      loc_of_str_items (union_loc loc (loc_of_str_item item)) items
 | [] ->
      loc

let loc_of_sig_item = function
   SigItemType (_, loc)
 | SigItemVal (_, _, loc)
 | SigItemModuleType (_, _, loc)
 | SigItemModule (_, _, loc)
 | SigItemException (_, _, loc)
 | SigItemOpen (_, loc)
 | SigItemExternal (_, _, _, loc)
 | SigItemExternalType (_, _, _, loc) ->
      loc

let rec loc_of_sig_items loc = function
   item :: items ->
      loc_of_sig_items (union_loc loc (loc_of_sig_item item)) items
 | [] ->
      loc

let loc_of_type = function
   TyFun (_, _, loc)
 | TyProd (_, loc)
 | TyRecord (_, loc)
 | TyUnion (_, loc)
 | TyArray (_, loc)
 | TyApply (_, _, loc)
 | TyVar (_, loc)
 | TyAbstract (_, loc) ->
      loc

let loc_of_module_exp = function
   ModuleExpStruct (_, loc)
 | ModuleExpVar (_, loc)
 | ModuleExpConstrain (_, _, loc)
 | ModuleExpFunctor (_, _, _, loc)
 | ModuleExpApply (_, _, loc) ->
      loc

let loc_of_module_type = function
   ModuleTypeSig (_, loc)
 | ModuleTypeVar (_, loc)
 | ModuleTypeWithType (_, _, _, _, loc)
 | ModuleTypeWithModule (_, _, _, loc)
 | ModuleTypeFunctor (_, _, _, loc) ->
      loc

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
