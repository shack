(*
 * Utilities on the ast.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

open Aml_ast

(*
 * Compute the free vars in the type definition.
 * the variabes are collected in postfix order.
 *)
let rec free_type_vars vars = function
   TyAbstract _ ->
      vars

 | TyFun (t1, t2, _) ->
      free_type_vars (free_type_vars vars t1) t2
 | TyProd (tyl, _) ->
      free_type_vars_list vars tyl
 | TyRecord (fields, _) ->
      free_type_vars_record vars fields
 | TyUnion (fields, _) ->
      free_type_vars_union vars fields
 | TyArray (ty, _) ->
      free_type_vars vars ty
 | TyApply (_, tyl, _) ->
      free_type_vars_list vars tyl
 | TyVar (v, loc) ->
      if List.mem v vars then
         vars
      else
         v :: vars

and free_type_vars_list vars = function
   t :: tl ->
      free_type_vars_list (free_type_vars vars t) tl
 | [] ->
      vars

and free_type_vars_record vars = function
   (_, _, ty) :: fields ->
      free_type_vars_record (free_type_vars vars ty) fields
 | [] ->
      vars

and free_type_vars_union vars = function
   (_, tyl) :: fields ->
      free_type_vars_union (free_type_vars_list vars tyl) fields
 | [] ->
      vars

(*
 * External function starts with empty list.
 * The variables should be converted to prefix order.
 *)
let free_type_vars ty =
   List.rev (free_type_vars [] ty)

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
