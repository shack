(*
 * Basic lexical definitions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

val set_file_name : string -> int -> Location.loc

val parse_str_item  : Lexing.lexbuf -> Aml_ast.str_item option
val parse_str_items : Lexing.lexbuf -> Aml_ast.str_item list
val parse_sig_items : Lexing.lexbuf -> Aml_ast.sig_item list

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
