(*
 * Print an expression.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol
open Aml_ast

(*
 * Default tabstop.
 *)
let tabstop = 3

(*
 * Print an identifier.
 *)
let rec pp_print_id first buf l =
   match l with
   id :: tl ->
      if not first then
         pp_print_string buf ".";
      pp_print_symbol buf id;
      pp_print_id false buf tl
 | [] ->
      ()

let pp_print_id =
   pp_print_id true

(*
 * Print a list of vars
 *)
let pp_print_var buf v =
   fprintf buf " %a" pp_print_symbol v

let pp_print_vars buf vars =
   List.iter (pp_print_var buf) vars

(*
 * Print a list of restrictions.
 *)
let rec pp_print_restrict first buf l =
   match l with
      (v, _) :: vars ->
         if not first then
            pp_print_space buf ();
         fprintf buf "restrict %a%a" pp_print_symbol v (pp_print_restrict false) vars
    | [] ->
         ()

let pp_print_restrict =
   pp_print_restrict true

(*
 * Types.
 *)
let rec pp_print_type buf t =
   match t with
      TyFun (ty1, ty2, _) ->
         fprintf buf "(@[<hv 0>%a ->@ %a)@]" pp_print_type ty1 pp_print_type ty2

    | TyProd (types, _) ->
         fprintf buf "(@[<hv 0>%a)@]" (pp_print_type_prod true) types

    | TyRecord (rty, _) ->
         fprintf buf "@[<hv 0>{@[<hv 0>%a@]@ }@]" (pp_print_type_record true) rty

    | TyUnion (uty, _) ->
         fprintf buf "@[<hv 0>%a@]" (pp_print_type_union true) uty

    | TyArray (ty, _) ->
         fprintf buf "%a array" pp_print_type ty

    | TyApply (id, params, _) ->
         pp_open_hvbox buf tabstop;
         if params <> [] then
            fprintf buf "(@[<hv 0>%a)@]@ " (pp_print_type_params true) params;
         pp_print_id buf id;
         pp_close_box buf ()

    | TyVar (v, _) ->
         fprintf buf "'%a" pp_print_symbol v
    | TyAbstract (v, _) ->
         fprintf buf "<abstr=%a>" pp_print_symbol v

and pp_print_type_record first buf l =
   match l with
      (mflag, l, ty) :: tl ->
         if not first then
            fprintf buf ";@ ";
         if mflag then
            pp_print_string buf "mutable ";
         fprintf buf "%a : @[<hv 0>%a@]%a" (**)
            pp_print_symbol l
            pp_print_type ty
            (pp_print_type_record true) tl

    | [] ->
         ()

and pp_print_type_prod first buf l =
   match l with
      ty :: tl ->
         if not first then
            fprintf buf "@ * ";
         fprintf buf "%a%a" pp_print_type ty (pp_print_type_prod false) tl
    | [] ->
         ()

and pp_print_type_union first buf l =
   match l with
      (v, tyl) :: tl ->
         if not first then
            fprintf buf "@ | ";
         fprintf buf "@[<hv 3>%a" pp_print_symbol v;
         if tyl <> [] then
            fprintf buf " of %a" (pp_print_type_prod true) tyl;
         pp_close_box buf ();
         pp_print_type_union false buf tl
    | [] ->
         ()

and pp_print_type_params first buf l =
   match l with
      ty :: tl ->
         if not first then
            fprintf buf ",@ ";
         pp_print_type buf ty;
         pp_print_type_params false buf tl
    | [] ->
         ()

let rec pp_print_type_vars first buf l =
   match l with
      v :: tl ->
         if not first then
            pp_print_string buf ", ";
         fprintf buf "'%a" pp_print_symbol v;
         pp_print_type_vars false buf tl
    | [] ->
         ()

let pp_print_type_vars buf l =
   match l with
      [] ->
         ()
    | vars ->
         fprintf buf "(@[<hv 0>%a) @]" (pp_print_type_vars true) vars

(*
 * Print patterns.
 *)
let rec pp_print_patt buf p =
   match p with
      PattInt (i, _) ->
         pp_print_int buf i
    | PattChar (c, _) ->
         pp_print_string buf ("'" ^ Char.escaped c ^ "'")
    | PattString (s, _) ->
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"")
    | PattWild _ ->
         pp_print_string buf "_"
    | PattVar (v, _) ->
         pp_print_symbol buf v

    | PattRecord (fields, _) ->
         fprintf buf "@[<hv 0>{@[<hv 0>%a@]@ }@]" (pp_print_patt_fields true) fields

    | PattArray (pl, _) ->
         fprintf buf "[|@[<hv 0>%a|]@]" (pp_print_patt_array true) pl

    | PattConst (id, pl, _) ->
         pp_print_id buf id;
         if pl <> [] then
            fprintf buf " (@[<hv 0>%a)@]" (pp_print_patt_tuple true) pl

    | PattChoice (p1, p2, _) ->
         fprintf buf "(@[<hv 0>%a@ | @[<hv 0>%a)@]@]" pp_print_patt p1 pp_print_patt p2

    | PattAs (p1, p2, _) ->
         fprintf buf "@[<hv 0>%a@ as @[<hv 0>%a@]@]" pp_print_patt p1 pp_print_patt p2

    | PattConstrain (p, t, _) ->
         fprintf buf "(@[<hv 0>%a@ : @[<hv 0>%a)@]@]" pp_print_patt p pp_print_type t

    | PattTuple (pl, _) ->
         fprintf buf "(@[<hv 0>%a)@]" (pp_print_patt_tuple true) pl

    | PattWrap (p, _) ->
         pp_print_patt buf p

    | PattRange (p1, p2, _) ->
         fprintf buf "%a@ .. %a" pp_print_patt p1 pp_print_patt p2

    | PattWhen (p, e, _) ->
         fprintf buf "%a@ when %a" pp_print_patt p (pp_print_exp false) e

    | PattExpr (e, _) ->
         fprintf buf "@%a" (pp_print_exp false) e

(*
 * Print the array arguments.
 *)
and pp_print_patt_sep sep first buf l =
   match l with
      p :: pl ->
         if not first then
            fprintf buf "%s@ " sep;
         pp_print_patt buf p;
         pp_print_patt_sep sep false buf pl
    | [] ->
         ()

and pp_print_patt_array first buf pl =
   pp_print_patt_sep ";" first buf pl

and pp_print_patt_tuple first buf pl =
   pp_print_patt_sep "," first buf pl

(*
 * Record.
 *)
and pp_print_patt_fields first buf l =
   match l with
      (id, p) :: pl ->
         if not first then
            fprintf buf ";@ ";
         fprintf buf "@[<hv 3>%a =@ %a@]" pp_print_id id pp_print_patt p;
         pp_print_patt_fields false buf pl
    | [] ->
         ()

(*
 * Print a sequence of pattern arguments.
 *)
and pp_print_patts buf patts =
   List.iter (fun p ->
         pp_print_string buf " ";
         pp_print_patt buf p) patts

(*
 * Print a first-level piece of code.
 *)
and pp_print_exp inlet buf e =
   match e with
      Number (i, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_int buf i
    | Char (c, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_char buf '\'';
         pp_print_string buf (String.escaped (String.make 1 c));
         pp_print_char buf '\''
    | String (s, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"")
    | Var (v, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_id buf v
    | Const (c, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_id buf c

    | Apply (e1, args, _) ->
         if inlet then
            pp_print_break buf 1 tabstop
         else
            pp_print_string buf "(";
         pp_open_hvbox buf 0;
         pp_print_exp false buf e1;
         List.iter (pp_print_arg buf) args;
         if not inlet then
            pp_print_string buf ")";
         pp_close_box buf ()

    | Lambda (vars, e, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "fun";
         pp_print_patts buf vars;
         pp_print_string buf " ->";
         pp_print_space buf ();
         pp_print_exp false buf e;
         pp_print_string buf ")";
         pp_close_box buf ()

    | Function (gc, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "function";
         pp_print_commands true buf gc;
         pp_print_string buf ")";
         pp_close_box buf ()

    | Let (lets, e2, _) ->
         pp_open_hvbox buf 0;
         pp_print_lets buf lets;
         pp_print_string buf " in";
         pp_print_space buf ();
         pp_print_exp true buf e2;
         pp_close_box buf ();

    | LetRec (letrecs, e2, _) ->
         pp_open_hvbox buf 0;
         pp_print_letrecs buf letrecs;
         pp_print_string buf " in";
         pp_print_space buf ();
         pp_print_exp true buf e2;
         pp_close_box buf ()

    | If (e1, e2, e3, _) ->
         if inlet then
            pp_print_break buf 1 tabstop
         else
            pp_print_string buf "(";
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "if ";
         pp_print_exp false buf e1;
         pp_print_string buf " then";
         pp_print_space buf ();
         pp_print_exp false buf e2;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_open_hvbox buf tabstop;
         pp_print_string buf "else";
         pp_print_space buf ();
         pp_print_exp false buf e3;
         if not inlet then
            pp_print_string buf ")";
         pp_close_box buf ();
         pp_close_box buf ()

    | For (v, e1, to_flag, e2, e3, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "for ";
         pp_print_symbol buf v;
         pp_print_string buf " = ";
         pp_print_exp false buf e1;
         pp_print_string buf (if to_flag then " to " else " downto ");
         pp_print_exp false buf e2;
         pp_print_string buf " do";
         pp_print_space buf ();
         pp_print_exp false buf e3;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "done";
         pp_close_box buf ()

    | While (e1, e2, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "while ";
         pp_print_exp false buf e1;
         pp_print_string buf " do";
         pp_print_space buf ();
         pp_print_exp false buf e2;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "done";
         pp_close_box buf ()

    | Constrain (e, ty, _) ->
         if inlet then
            pp_print_break buf 1 tabstop
         else
            pp_print_string buf "(";
         pp_open_hvbox buf 0;
         pp_print_exp false buf e;
         pp_print_string buf " : ";
         pp_open_hvbox buf 0;
         pp_print_type buf ty;
         pp_print_string buf ")";
         pp_close_box buf ();
         pp_close_box buf ()

    | Wrap (e, _) ->
         pp_print_exp inlet buf e

    | Tuple (el, _) ->
         pp_print_tuple inlet buf el "(" "," ")"
    | Array (el, _) ->
         pp_print_tuple inlet buf el "[|" ";" "|]"

    | ArraySubscript (e1, e2, _) ->
         pp_print_subscript buf inlet e1 e2 ".(" ")"
    | StringSubscript (e1, e2, _) ->
         pp_print_subscript buf inlet e1 e2 ".[" "]"
    | ArraySetSubscript (e1, e2, e3, _) ->
         pp_print_assign buf inlet e1 e2 e3 ".(" ")"
    | StringSetSubscript (e1, e2, e3, _) ->
         pp_print_assign buf inlet e1 e2 e3 ".[" "]"

    | Record (items, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_print_string buf "{ ";
         pp_open_hvbox buf 0;
         pp_print_record_items buf true items;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "}"
    | RecordProj (e, v, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_exp false buf e;
         pp_print_string buf ".";
         pp_print_id buf v
    | RecordSetProj (e1, v, e2, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "(";
         pp_print_exp false buf e1;
         pp_print_string buf ").";
         pp_print_id buf v;
         pp_print_string buf " <-";
         pp_print_space buf ();
         pp_print_exp false buf e2;
         pp_close_box buf ()
    | RecordUpdate (e, items, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_print_string buf "{ ";
         pp_open_hvbox buf 0;
         pp_print_exp false buf e;
         pp_print_string buf " with";
         pp_print_space buf ();
         pp_print_record_items buf true items;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "}"

    | Sequence (e1, e2, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_print_exp false buf e1;
         pp_print_string buf ";";
         pp_print_space buf ();
         pp_print_exp false buf e2;
         pp_close_box buf ()

    | Match (e, gc, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "match ";
         pp_print_exp false buf e;
         pp_print_string buf " with";
         pp_close_box buf ();
         pp_print_commands true buf gc;
         pp_close_box buf ()

    | Try (e, gc, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "try ";
         pp_print_exp false buf e;
         pp_print_string buf " with";
         pp_close_box buf ();
         pp_print_commands true buf gc;
         pp_close_box buf ()

    | Raise (e, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "$raise ";
         pp_print_exp false buf e

    | In (n, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "(";
         pp_print_string buf "in ";
         pp_print_id buf n;
         pp_print_string buf ")"

    | Out (n, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "(";
         pp_print_string buf "out ";
         pp_print_id buf n;
         pp_print_string buf ")"

    | Open (n, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "(";
         pp_print_string buf "open ";
         pp_print_id buf n;
         pp_print_string buf ")"

    | Action (args, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "action ";
         List.iter (pp_print_arg buf) args;
         pp_print_string buf ")";
         pp_close_box buf ()

    | Restrict (v, mt, e, _) ->
         if not inlet then
            pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "restrict ";
         pp_print_symbol buf v;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_module_type buf mt;
         pp_print_string buf " ->";
         pp_print_space buf ();
         pp_print_exp false buf e;
         if not inlet then
            pp_print_string buf ")";
         pp_close_box buf ()

    | Ambient (v, me, _) ->
         if inlet then
            pp_print_break buf 1 tabstop;
         pp_open_hvbox buf 0;
         pp_print_string buf "ambient ";
         pp_print_symbol buf v;
         pp_print_string buf " =";
         pp_print_space buf ();
         open_vbox 0;
         pp_print_module_exp buf me;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "end";
         pp_close_box buf ()

    | Thread (e, _) ->
         if inlet then
            pp_print_break buf 1 tabstop
         else
            pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "migrant";
         pp_print_space buf ();
         pp_print_exp false buf e;
         if not inlet then
            pp_print_string buf ")";
         pp_close_box buf ()

    | Migrate (msg, e, _) ->
         if inlet then
            pp_print_break buf 1 tabstop
         else
            pp_print_string buf "(";
         pp_open_hvbox buf tabstop;
         pp_print_string buf "migrate ";
         pp_print_exp false buf msg;
         pp_print_string buf " ->";
         pp_print_space buf ();
         pp_print_exp false buf e;
         if not inlet then
            pp_print_string buf ")";
         pp_close_box buf ()

    | LetModule (mn, me, e, _) ->
         pp_open_hvbox buf 0;
         pp_open_hvbox buf tabstop;
         pp_print_string buf "let module ";
         pp_print_symbol buf mn;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_module_exp buf me;
         pp_close_box buf ();
         pp_print_space buf ();
         pp_print_string buf "in";
         pp_print_exp true buf e;
         pp_close_box buf ()

(*
 * Print a list of exp with a separator.
 *)
and pp_print_exps first sep buf = function
   e :: el ->
      if not first then
         begin
            pp_print_string buf sep;
            pp_print_space buf ()
         end;
      pp_print_exp false buf e;
      pp_print_exps false sep buf el
 | [] ->
      ()

(*
 * Tupling.
 *)
and pp_print_tuple inlet buf el start middle finish =
   if inlet then
      pp_print_break buf 1 tabstop;
   pp_print_string buf start;
   pp_open_hvbox buf 0;
   pp_print_exps true middle buf el;
   pp_print_string buf finish;
   pp_close_box buf ()

(*
 * Print a subscript operation.
 *)
and pp_print_subscript buf inlet e1 e2 str1 str2 =
   if inlet then
      pp_print_break buf 1 tabstop;
   pp_open_hvbox buf tabstop;
   pp_print_exp false buf e1;
   pp_print_string buf str1;
   pp_print_exp false buf e2;
   pp_print_string buf str2;
   pp_close_box buf ()

and pp_print_assign buf inlet e1 e2 e3 str1 str2 =
   if inlet then
      pp_print_break buf 1 tabstop;
   pp_open_hvbox buf tabstop;
   pp_print_exp false buf e1;
   pp_print_string buf str1;
   pp_print_exp false buf e2;
   pp_print_string buf str2;
   pp_print_string buf " <-";
   pp_print_space buf ();
   pp_print_exp false buf e3;
   pp_close_box buf ()

(*
 * Print some record items.
 *)
and pp_print_record_items buf first = function
   (v, e, _) :: items ->
      if not first then
         begin
            pp_print_string buf ";";
            pp_print_space buf ()
         end;
      pp_open_hvbox buf tabstop;
      pp_print_id buf v;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_close_box buf ();
      pp_print_record_items buf false items
 | [] ->
      ()

(*
 * Print the arguments.
 *)
and pp_print_arg buf e =
   pp_print_space buf ();
   pp_print_exp false buf e

(*
 * Print the let bindings.
 *)
and pp_print_lets buf lets =
   let rec pp_print_lets buf flag = function
      (p, e) :: lets ->
         if flag then
            begin
               pp_open_hvbox buf tabstop;
               pp_print_string buf "let "
            end
         else
            begin
               pp_print_space buf ();
               pp_open_hvbox buf tabstop;
               pp_print_string buf "and "
            end;
         pp_print_patt buf p;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_exp false buf e;
         pp_close_box buf ();
         pp_print_lets buf false lets
    | [] ->
         ()
   in
      pp_print_lets buf true lets

(*
 * Print some function bindings.
 *)
and pp_print_letrecs buf letrecs =
   let rec pp_print_letrecs buf flag = function
      (f, e) :: letrecs ->
         if flag then
            begin
               pp_open_hvbox buf tabstop;
               pp_print_string buf "let "
            end
         else
            begin
               pp_print_space buf ();
               pp_open_hvbox buf tabstop;
               pp_print_string buf "and "
            end;
         pp_print_symbol buf f;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_exp false buf e;
         pp_close_box buf ();
         pp_print_letrecs buf false letrecs
    | [] ->
         ()
   in
      pp_print_letrecs buf true letrecs

(*
 * Guarded commands.
 *)
and pp_print_commands first buf = function
   (p, e) :: pel ->
      pp_print_space buf ();
      if not first then
         pp_print_string buf "| ";
      pp_open_hvbox buf tabstop;
      pp_print_patt buf p;
      pp_print_string buf " ->";
      pp_print_space buf ();
      pp_print_exp false buf e;
      pp_close_box buf ();
      pp_print_commands false buf pel
 | [] ->
      ()

(*
 * Print the type declarations.
 *)
and pp_print_types first buf = function
   (v, vars, ty, _) :: types ->
      if first then
         begin
            pp_open_hvbox buf tabstop;
            pp_print_string buf "type "
         end
      else
         begin
            pp_print_space buf ();
            pp_open_hvbox buf tabstop;
            pp_print_string buf "and "
         end;
      pp_print_type_vars buf vars;
      pp_print_symbol buf v;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ();
      pp_print_types false buf types
 | [] ->
      ()

(*
 * Structure items.
 *)
and pp_print_str_item buf item =
   match item with
      StrItemLet (lets, _) ->
         pp_print_lets buf lets

    | StrItemLetRec (letrecs, _) ->
         pp_print_letrecs buf letrecs

    | StrItemExpr (e, _) ->
         pp_print_exp false buf e

    | StrItemRestrict (v, mt, _) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "restrict ";
         pp_print_symbol buf v;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_module_type buf mt;
         pp_close_box buf ()

    | StrItemType (types, _) ->
         pp_open_hvbox buf 0;
         pp_print_types true buf types;
         pp_close_box buf ()

    | StrItemModule (mn, me, _) ->
         pp_open_hvbox buf 0;
         pp_print_string buf "module ";
         pp_print_symbol buf mn;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_module_exp buf me;
         pp_close_box buf ()

    | StrItemModuleType (id, mt, _) ->
         pp_open_hvbox buf 0;
         pp_print_string buf "module type ";
         pp_print_symbol buf id;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_module_type buf mt;
         pp_close_box buf ()

    | StrItemException (id, tyl, _) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "exception ";
         pp_print_symbol buf id;
         if tyl <> [] then
            begin
               pp_print_string buf " of ";
               pp_print_type_prod true buf tyl
            end;
         pp_close_box buf ()

    | StrItemOpen (id, _) ->
         pp_print_string buf "open ";
         pp_print_id buf id

    | StrItemExternal (v, ty, s, _) ->
         pp_open_hvbox buf tabstop;
         pp_print_string buf "external ";
         pp_print_symbol buf v;
         pp_print_string buf " :";
         pp_print_space buf ();
         pp_print_type buf ty;
         pp_print_string buf " =";
         pp_print_space buf ();
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"");
         pp_close_box buf ()

    | StrItemExternalType (v, params, s, _) ->
         fprintf buf "external type %a%a = \"%s\"" (**)
            pp_print_type_vars params
            pp_print_symbol v
            (String.escaped s)

and pp_print_str_items first buf = function
   item :: items ->
      if not first then
         pp_print_space buf ();
      pp_print_str_item buf item;
      pp_print_str_items false buf items
 | [] ->
      ()

(*
 * Sig items.
 *)
and pp_print_sig_item buf = function
   SigItemType (types, _) ->
      pp_open_hvbox buf 0;
      pp_print_types true buf types;
      pp_close_box buf ()

 | SigItemVal (id, ty, _) ->
      pp_print_string buf "val ";
      pp_print_symbol buf id;
      pp_print_string buf " : ";
      pp_print_type buf ty

 | SigItemModuleType (id, mt, _) ->
      pp_open_hvbox buf 0;
      pp_print_string buf "module type ";
      pp_print_symbol buf id;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_module_type buf mt;
      pp_close_box buf ()

 | SigItemModule (v, mt, _) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "module ";
      pp_print_symbol buf v;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_module_type buf mt;
      pp_close_box buf ()

 | SigItemException (id, tyl, _) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "exception ";
      pp_print_symbol buf id;
      if tyl <> [] then
         begin
            pp_print_string buf " of ";
            pp_print_type_prod true buf tyl
         end;
      pp_close_box buf ()

 | SigItemOpen (id, _) ->
      pp_print_string buf "open ";
      pp_print_id buf id

 | SigItemExternal (v, ty, s, _) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "external ";
      pp_print_symbol buf v;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_string buf ("\"" ^ String.escaped s ^ "\"");
      pp_close_box buf ()

 | SigItemExternalType (v, params, s, _) ->
      fprintf buf "external type %a%a = \"%s\"" (**)
         pp_print_type_vars params
         pp_print_symbol v
         (String.escaped s)

and pp_print_sig_items first buf = function
   item :: items ->
      if not first then
         pp_print_space buf ();
      pp_print_sig_item buf item;
      pp_print_sig_items false buf items
 | [] ->
      ()

(*
 * Module expression.
 *)
and pp_print_module_exp buf = function
   ModuleExpStruct (items, _) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "struct";
      pp_print_space buf ();
      pp_print_str_items true buf items;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "end";
      pp_close_box buf ()

 | ModuleExpVar (id, _) ->
      pp_print_id buf id

 | ModuleExpConstrain (me, mt, _) ->
      pp_print_string buf "(";
      pp_open_hvbox buf 0;
      pp_print_module_exp buf me;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_module_type buf mt;
      pp_print_string buf ")";
      pp_close_box buf ()

 | ModuleExpFunctor (v, mt, me, _) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(functor (";
      pp_print_symbol buf v;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_module_type buf mt;
      pp_print_string buf ") ->";
      pp_print_space buf ();
      pp_print_module_exp buf me;
      pp_print_string buf ")";
      pp_close_box buf ()

 | ModuleExpApply (me, mn, _) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(";
      pp_open_hvbox buf 0;
      pp_print_module_exp buf me;
      pp_print_string buf ")";
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "(";
      pp_print_id buf mn;
      pp_print_string buf ")";
      pp_close_box buf ()

and pp_print_module_type buf = function
   ModuleTypeSig (items, _) ->
      pp_open_hvbox buf 0;
      pp_open_hvbox buf tabstop;
      pp_print_string buf "sig";
      pp_print_space buf ();
      pp_print_sig_items true buf items;
      pp_close_box buf ();
      pp_print_space buf ();
      pp_print_string buf "end";
      pp_close_box buf ()

 | ModuleTypeVar (id, _) ->
      pp_print_id buf id

 | ModuleTypeWithType (mt, id, params, ty, _) ->
      pp_open_hvbox buf 0;
      pp_print_module_type buf mt;
      pp_print_space buf ();
      pp_print_string buf "with type ";
      pp_print_type_vars buf params;
      pp_print_id buf id;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_type buf ty;
      pp_close_box buf ()

 | ModuleTypeWithModule (mt, mn, me, _) ->
      pp_open_hvbox buf 0;
      pp_print_module_type buf mt;
      pp_print_space buf ();
      pp_open_hvbox buf tabstop;
      pp_print_string buf "with module ";
      pp_print_id buf mn;
      pp_print_string buf " =";
      pp_print_space buf ();
      pp_print_id buf me;
      pp_close_box buf ();
      pp_close_box buf ()

 | ModuleTypeFunctor (v, mt1, mt2, _) ->
      pp_open_hvbox buf tabstop;
      pp_print_string buf "(functor (";
      pp_print_symbol buf v;
      pp_print_string buf " :";
      pp_print_space buf ();
      pp_print_module_type buf mt1;
      pp_print_string buf ") ->";
      pp_print_space buf ();
      pp_print_module_type buf mt2;
      pp_print_string buf ")";
      pp_close_box buf ()

(*
 * Print a program.
 *)
let pp_print_prog buf prog =
   let { prog_name = name;
         prog_exp = exp
       } = prog
   in
      match exp with
         Interface items ->
            fprintf buf "@[<hv 0>module type %a =@ @[<hv 3>sig" (**)
               pp_print_symbol name;
            List.iter (fun item ->
                  fprintf buf "@ %a" pp_print_sig_item item) items;
            fprintf buf "@]@ end@]"
       | Implementation items ->
            fprintf buf "@[<hv 0>module %a =@ @[<hv 3>struct" (**)
               pp_print_symbol name;
            List.iter (fun item ->
                  fprintf buf "@ %a" pp_print_str_item item) items;
            fprintf buf "@]@ end@]"

(*
 * Global functions.
 *)
let pp_print_exp buf e =
   pp_open_hvbox buf 0;
   pp_print_exp false buf e;
   pp_close_box buf ()

let pp_print_type buf ty =
   pp_open_hvbox buf 0;
   pp_print_type buf ty;
   pp_close_box buf ()

let pp_print_patt buf patt =
   pp_open_hvbox buf 0;
   pp_print_patt buf patt;
   pp_close_box buf ()

let pp_print_str_item buf item =
   pp_open_hvbox buf 0;
   pp_print_str_item buf item;
   pp_close_box buf ()

let pp_print_sig_item buf item =
   pp_open_hvbox buf 0;
   pp_print_sig_item buf item;
   pp_close_box buf ()

let pp_print_module_exp buf me =
   pp_open_hvbox buf 0;
   pp_print_module_exp buf me;
   pp_close_box buf ()

let pp_print_module_type buf mt =
   pp_open_hvbox buf 0;
   pp_print_module_type buf mt;
   pp_close_box buf ()

let debug str f x =
   eprintf "@[<v 0>*** AML: AST: %s@ %a@]@." str f x

let debug_exp str exp =
   debug str pp_print_exp exp

let debug_type str ty =
   debug str pp_print_type ty

let debug_patt str patt =
   debug str pp_print_patt patt

let debug_str_item str item =
   debug str pp_print_str_item item

let debug_sig_item str item =
   debug str pp_print_sig_item item

let debug_module_exp str me =
   debug str pp_print_module_exp me

let debug_module_type str mt =
   debug str pp_print_module_type mt

let debug_prog str prog =
   debug str pp_print_prog prog

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
