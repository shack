(*
 * Main compilation unit.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Symbol

open Fir_state

open Aml_state

open Aml_ast
open Aml_ast_print

let debug_ast =
   create_debug (**)
      { debug_name = "ast";
        debug_description = "print AST";
        debug_value = false
      }

let debug_compile =
   create_debug (**)
      { debug_name = "compile";
        debug_description = "print outermost compiling operations";
        debug_value = false
      }

(*
 * Flag for loading pervasives.
 *)
let sym_pervasives = Symbol.add "Pervasives"

(*
 * File suffixes.
 *)
let ml_suffix  = ".ml"
let mli_suffix = ".mli"

(*
 * Read an implementation.
 *)
let compile_interface filename =
   (* Read the file *)
   let inx = open_in filename in
   let items =
      try
         (* Parse the input file *)
         let lexbuf = Lexing.from_channel inx in
         let loc = Aml_ast_lex.set_file_name filename 1 in
         let items = Aml_ast_lex.parse_sig_items lexbuf in
            Interface items
      with
         exn ->
            close_in inx;
            raise exn
   in
   let _ = close_in inx in
      items

(*
 * Read an implementation.
 *)
let compile_implementation filename =
   (* Read the file *)
   let inx = open_in filename in
   let items =
      try
         (* Parse the input file *)
         let lexbuf = Lexing.from_channel inx in
         let loc = Aml_ast_lex.set_file_name filename 1 in
         let items = Aml_ast_lex.parse_str_items lexbuf in
            Implementation items
      with
         exn ->
            close_in inx;
            raise exn
   in
   let _ = close_in inx in
      items

(*
 * Compile the file, check type based on suffix.
 *)
let compile filename =
   let name, exp =
      if Filename.check_suffix filename ml_suffix then
         let name = Filename.chop_suffix filename ml_suffix in
         let exp = compile_implementation filename in
            name, exp
      else if Filename.check_suffix filename mli_suffix then
         let name = Filename.chop_suffix filename mli_suffix in
         let exp = compile_interface filename in
            name, exp
      else
         raise (Invalid_argument filename)
   in
   let name = String.capitalize name in
   let name = Symbol.add name in
      { prog_name = name;
        prog_exp  = exp
      }

(*
 * Print the item.
 *)
let debug_prog s prog =
   if debug debug_print_ast then
      Format.eprintf "@[<hv 3>AML AST: %s@ %a@]@." s pp_print_prog prog

let compile s =
   let prog = compile s in
   let _ = debug_prog "Aml_ast_compile" prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
