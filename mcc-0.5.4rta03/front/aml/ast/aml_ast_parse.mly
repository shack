/*
 * Grammar:
 *
 * prog: epsilon | prog si ;; | prog exp ;;
 *
 * si:    ambient <id> = struct si list end
 *      | restrict <id>
 *      | let <id> args = exp
 *      | migrant exp
 *
 * args: epsilon | args <id>
 *
 * exp:   <const>
 *      | <string>
 *      | <id>
 *      | exp <binop> exp
 *      | exp exp                  (* application *)
 *      | fun <id> args -> exp     (* function *)
 *      | fix exp                  (* fixpoint *)
 *      | if exp then exp else exp
 *      | let <id> args = exp in exp
 *      | ambient <id> = struct si list end
 *      | restrict <id> in exp
 *      | migrate cap -> e
 *
 * cap:   <id>
 *      | in <id>
 *      | out <id>
 *      | open <id>
 *      | cap . cap
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *
 */
 
%{
open Location

open Aml_ast
open Aml_ast_loc
open Aml_ast_pervasive

(*
 * Module name allows contraint.
 *)
type module_name =
   ModuleNameVar of var * loc
 | ModuleNameConstrain of module_name * module_type * loc

(*
 * Default location.
 *)
let zero_loc = bogus_loc "<null>"

(*
 * Unary minus.
 *)
let sym_uminus = Symbol.add "~-"

(************************************************************************
 * TYPE									*
 ************************************************************************)

(*
 * Variable.
 *)
let type_lid (loc, id) =
   loc, TyApply (id, [], loc)

(*
 * Union type.
 *)
let split_prod_type (loc, ty) =
   match ty with
      TyProd (tyl, _) ->
         loc, tyl
    | _ ->
         loc, [ty]

let make_union_header (loc1, id) (loc2, tyl) =
   let loc = union_loc loc1 loc2 in
      loc, [id, tyl]

let append_union_header (loc1, types) (loc2, id) (loc3, tyl) =
   let loc2 = union_loc loc2 loc3 in
   let loc = union_loc loc1 loc2 in
      loc, types @ [id, tyl]

let type_union (loc, types) =
   loc, TyUnion (types, loc)

(*
 * Make a record type.
 *)
let type_record_field mflag (loc1, id) (loc2, ty) =
   let loc = union_loc loc1 loc2 in
      loc, [mflag, id, ty]

let append_type_record_field (loc1, types) mflag (_, id) (loc2, ty) =
   let loc = union_loc loc1 loc2 in
      loc, types @ [mflag, id, ty]

let type_record loc1 (_, types) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, TyRecord (types, loc)

(*
 * Type param application.
 *)
let type_apply (loc1, ty) (loc2, id) =
   let loc = union_loc loc1 loc2 in
      loc, TyApply (id, [ty], loc)

let type_args_apply loc1 (_, ty) (_, params) (loc2, id) =
   let loc = union_loc loc1 loc2 in
      loc, TyApply (id, ty :: params, loc)

(*
 * Products.
 *)
let type_product (loc1, t1) (loc2, t2) =
   let loc = union_loc loc1 loc2 in
   let ty =
      match t1 with
         TyProd (tyl, _) ->
            TyProd (tyl @ [t2], loc)
       | _ ->
            TyProd ([t1; t2], loc)
   in
      loc, ty

(*
 * Functions.
 *)
let type_function (loc1, t1) (loc2, t2) =
   let loc = union_loc loc1 loc2 in
      loc, TyFun (t1, t2, loc)

(*
 * Wrap a type.
 *)
let type_wrap loc1 (_, ty) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, ty

(************************************************************************
 * PATTERNS								*
 ************************************************************************)

(*
 * Patterns.
 *)
let pattern_number (loc, i) =
   loc, PattInt (i, loc)

let pattern_char (loc, c) =
   loc, PattChar (c, loc)

let pattern_string (loc, s) =
   loc, PattString (s, loc)

let pattern_wild loc =
   loc, PattWild loc

let pattern_var (loc, v) =
   loc, PattVar (v, loc)

let pattern_unit loc1 loc2 =
   let loc = union_loc loc1 loc2 in
      loc, PattConst (unit_const, [], loc)

let pattern_nil loc =
   loc, PattConst (nil_const, [], loc)

let pattern_cons (loc1, p1) (loc2, p2) =
   let loc = union_loc loc1 loc2 in
      loc, PattConst (cons_const, [p1; p2], loc)

let pattern_list loc1 (_, pl) loc2 =
   let loc = union_loc loc1 loc2 in
   let rec wrap_cons = function
      (loc, p) :: pl ->
         PattConst (cons_const, [p; wrap_cons pl], union_loc loc loc2)
    | [] ->
         PattConst (nil_const, [], loc)
   in
      loc, wrap_cons pl

let pattern_record_header (loc1, lid) (loc2, p) =
   let loc = union_loc loc1 loc2 in
      loc, [lid, p]

let append_record_pattern_header (loc1, fields) (_, lid) (loc2, p) =
   let loc = union_loc loc1 loc2 in
      loc, fields @ [lid, p]

let pattern_record loc1 (_, fields) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, PattRecord (fields, loc)

let pattern_array loc1 (_, pl) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, PattArray (List.map snd pl, loc)

let pattern_const (loc1, id) (loc2, p) =
   let pl =
      match p with
         None ->
            []
       | Some p ->
            match p with
               PattWrap (PattTuple (pl, _), _) ->
                  pl
             | _ ->
                  [p]
   in
   let loc = union_loc loc1 loc2 in
      loc, PattConst (id, pl, loc)

let pattern_choice (loc1, p1) (loc2, p2) =
   let loc = union_loc loc1 loc2 in
      loc, PattChoice (p1, p2, loc)

let pattern_as (loc1, p1) (loc2, p2) =
   let loc = union_loc loc1 loc2 in
      loc, PattAs (p1, p2, loc)
   
let pattern_constrain loc1 (_, p) (_, ty) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, PattConstrain (p, ty, loc)

let pattern_tuple (loc1, p1) (loc2, p2) =
   let pl1 =
      match p1 with
         PattTuple (pl1, _) -> pl1
       | _ -> [p1]
   in
   let pl2 =
      match p2 with
         PattTuple (pl2, _) -> pl2
       | _ -> [p2]
   in
   let loc = union_loc loc1 loc2 in
      loc, PattTuple (pl1 @ pl2, loc)

let pattern_wrap loc1 (_, p) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, PattWrap (p, loc)

let pattern_range (loc1, p1) (loc2, p2) =
   let loc = union_loc loc1 loc2 in
      loc, PattRange (p1, p2, loc)

let pattern_range_left (loc1, p1) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, PattRange (p1, PattWild loc, loc)

let pattern_range_right loc1 (loc2, p2) =
   let loc = union_loc loc1 loc2 in
      loc, PattRange (PattWild loc, p2, loc)

let pattern_when (loc1, p) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, PattWhen (p, e, loc)

let pattern_expr loc1 (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, PattExpr (e, loc)

(************************************************************************
 * EXPR									*
 ************************************************************************)

(*
 * Unary expressions.
 *)
let expr_unop (loc1, v) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Apply (Var ([v], loc1), [e], loc)

(*
 * Binary expressions.
 *)
let expr_binop (loc1, e1) (_, v) (loc2, e2) =
   let loc = union_loc loc1 loc2 in
      loc, Apply (Var ([v], loc1), [e1; e2], loc)

(*
 * Variables and projection.
 *)
let expr_var (loc, id) =
   loc, Var (id, loc)

let expr_const (loc, id) =
   loc, Const (id, loc)

(*
 * Other expressions.
 *)
let expr_if loc1 (_, e1) (loc2, e2) =
   let loc = union_loc loc1 loc2 in
   let e3 = Const (unit_const, loc) in
      loc, If (e1, e2, e3, loc)

let expr_if_else loc1 (_, e1) (_, e2) (loc3, e3) =
   let loc = union_loc loc1 loc3 in
      loc, If (e1, e2, e3, loc)

let expr_for loc1 (_, v) (_, e1) to_flag (_, e2) (_, e3) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, For (v, e1, to_flag, e2, e3, loc)

let expr_while loc1 (_, e1) (_, e2) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, While (e1, e2, loc)

let expr_let (loc1, lets) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Let (lets, e, loc)

let expr_letrecs (loc1, letrecs) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, LetRec (letrecs, e, loc)

let expr_fun loc1 (_, id) (_, vars) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Lambda (id :: vars, e, loc)

let expr_function loc1 (loc2, pel) =
   let loc = union_loc loc1 loc2 in
      loc, Function (pel, loc)

(*
 * Guarded commands.
 *)
let expr_guarded_command (loc1, p) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, [p, e]

let append_guarded_command (loc1, guards) (loc2, p) (loc3, e) =
   let loc2 = union_loc loc2 loc3 in
   let loc = union_loc loc1 loc2 in
      loc, guards @ [p, e]

let expr_match loc1 (_, e) (loc2, pel) =
   let loc = union_loc loc1 loc2 in
      loc, Match (e, pel, loc)

let expr_try loc1 (_, e) (loc2, pel) =
   let loc = union_loc loc1 loc2 in
      loc, Try (e, pel, loc)

let expr_restrict (loc1, (name, mt)) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Restrict (name, mt, e, loc)

let expr_migrate loc1 (_, msgs) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Migrate (msgs, e, loc)

let expr_thread loc1 (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Thread (e, loc)

let expr_action loc1 (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Action (e, loc)

let expr_out loc1 (loc2, name) =
   union_loc loc1 loc2, Out (name, loc2)

let expr_in loc1 (loc2, name) =
   union_loc loc1 loc2, In (name, loc2)

let expr_open loc1 (loc2, name) =
   union_loc loc1 loc2, Open (name, loc2)

let expr_apply (loc, el) =
   match el with
      [] ->
       raise (Invalid_argument "Ast_parse.expr_apply")
    | [e] ->
       loc, e
    | e :: args ->
       loc, Apply (e, args, loc)

let expr_module_expr loc1 (_, (name, e1)) (loc2, e2) =
   let loc = union_loc loc1 loc2 in
      loc, LetModule (name, e1, e2, loc)

let expr_ambient loc1 (_, name) (loc2, me) =
   let loc = union_loc loc1 loc2 in
      loc, Ambient (name, me, loc)

(*
 * Sequential execution.
 *)
let expr_sequence (loc1, e1) (loc2, e2) =
   let loc = union_loc loc1 loc2 in
      loc, Sequence (e1, e2, loc)

(*
 * Tuples.
 *)
let expr_tuple (loc1, e1) (loc2, e2) =
   let el1 =
      match e1 with
         Tuple (el1, _) -> el1
       | _ -> [e1]
   in
   let el2 =
      match e2 with
         Tuple (el2, _) -> el2
       | _ -> [e2]
   in
   let loc = union_loc loc1 loc2 in
      loc, Tuple (el1 @ el2, loc)

(*
 * Arrays.
 *)
let rec split_sequence = function
   Sequence (e1, e2, _) ->
      e1 :: split_sequence e2
 | e ->
      [e]

let make_array = function
   None ->
      []
 | Some (_, e) ->
      split_sequence e

let expr_array loc1 el loc2 =
   let loc = union_loc loc1 loc2 in
      loc, Array (make_array el, loc)

let expr_array_subscript (loc1, e1) (_, e2) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, ArraySubscript (e1, e2, loc)

let expr_array_assign (loc1, e1) (_, e2) (loc2, e3) =
   let loc = union_loc loc1 loc2 in
      loc, ArraySetSubscript (e1, e2, e3, loc)

(*
 * Strings.
 *)
let expr_string_subscript (loc1, e1) (_, e2) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, StringSubscript (e1, e2, loc)

let expr_string_assign (loc1, e1) (_, e2) (loc2, e3) =
   let loc = union_loc loc1 loc2 in
      loc, StringSetSubscript (e1, e2, e3, loc)

(*
 * Lists.
 *)
let expr_nil loc =
   loc, Const (nil_const, loc)

let expr_cons (loc1, e1) (loc2, e2) =
   let loc = union_loc loc1 loc2 in
      loc, Apply (Const (cons_const, loc), [Tuple ([e1; e2], loc)], loc)

let rec split_list = function
   Sequence (e1, e2, loc) ->
      Apply (Const (cons_const, loc), [Tuple ([e1; split_list e2], loc)], loc)
 | e ->
      let loc = loc_of_exp e in
         Apply (Const (cons_const, loc), [Tuple ([e; Const (nil_const, loc)], loc)], loc)

let make_list loc = function
   None ->
      Const (nil_const, loc)
 | Some (_, e) ->
      split_list e

let expr_list loc1 el loc2 =
   let loc = union_loc loc1 loc2 in
      loc, make_list loc el

(*
 * Records.
 *)
let expr_record loc1 (_, el) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, Record (el, loc)

let expr_record_update loc1 (_, e) (_, el) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, RecordUpdate (e, el, loc)

let expr_record_arg (loc1, id) (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, [id, e, loc]

let append_record_arg (loc1, args) (loc2, id) (loc3, e) =
   let loc4 = union_loc loc2 loc3 in
   let loc = union_loc loc1 loc4 in
      loc, args @ [id, e, loc4]

let expr_proj (loc1, e) (loc2, id) =
   let loc = union_loc loc1 loc2 in
      loc, RecordProj (e, id, loc)

let expr_assign (loc1, e1) (_, id) (loc2, e2) =
   let loc = union_loc loc1 loc2 in
      loc, RecordSetProj (e1, id, e2, loc)

(*
 * Type constraints.
 *)
let expr_constraint loc1 (_, e) (_, ty) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, Constrain (e, ty, loc)

(*
 * Built-ins.
 *)
let expr_raise loc1 (loc2, e) =
   let loc = union_loc loc1 loc2 in
      loc, Raise (e, loc)

(*
 * Just a placeholder.
 *)
let expr_wrap loc1 (_, e) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, Wrap (e, loc)

(************************************************************************
 * STR_ITEM								*
 ************************************************************************)

(*
 * Type definitions.
 *)
let make_type_header loc1 (_, params) (_, id) (loc2, ty) =
   let loc = union_loc loc1 loc2 in
      loc, [id, params, ty, loc]

let make_abstract_type_header loc1 (_, params) (loc2, id) =
   let loc = union_loc loc1 loc2 in
      loc, [id, params, TyAbstract (id, loc2), loc]

let append_type_header (loc1, types) loc2 (_, params) (_, id) (loc3, ty) =
   let loc2 = union_loc loc2 loc3 in
   let loc = union_loc loc1 loc2 in
      loc, types @ [id, params, ty, loc2]

let append_abstract_type_header (loc1, types) loc2 (_, params) (loc3, id) =
   let loc2 = union_loc loc2 loc3 in
   let loc = union_loc loc1 loc2 in
      loc, types @ [id, params, TyAbstract (id, loc3), loc2]

let str_item_type (loc, types) =
   StrItemType (types, loc)

(*
 * Variable declarations.
 *)
let make_var_decl_header2 locl (loc1, id) (locv, vars) (loce, e) =
    union_loc locl loce, [PattVar (id, loc1), Lambda (vars, e, union_loc locv loce)]

let make_var_decl_header1 locl (_, id) (loce, e) =
    union_loc locl loce, [id, e]

let append_var_decl_header (locl, lets) (_, id) (locv, vars) (loce, e) =
   match vars with
      [] ->
         union_loc locl loce, lets @ [id, e]
    | _ ->
         union_loc locl loce, lets @ [id, Lambda (vars, e, union_loc locv loce)]

let str_item_let (loc, lets) =
   StrItemLet (lets, loc)

(*
 * Function declarations.
 *)
let wrap_letrec loc vars e =
   if vars = [] then
      e
   else
      Lambda (vars, e, loc)

let expr_fun_decl_header locl (_, id) (_, vars) (loce, e) =
   let loc = union_loc locl loce in
      loc, [id, wrap_letrec loc vars e]

let append_fun_decl_header (locl, letrecs) (_, id) (_, vars) (loce, e) =
   let loc = union_loc locl loce in
      loc, letrecs @ [id, wrap_letrec loc vars e]

let str_item_letrecs (loc, letrecs) =
   StrItemLetRec (letrecs, loc)

(*
 * Expression items.
 *)
let str_item_expr (loc, e) =
   StrItemExpr (e, loc)

(*
 * Restriction.
 *)
let restrict_header loc1 (_, name) (loc2, mt) =
   union_loc loc1 loc2, (name, mt)

let str_item_restrict (loc, (name, mt)) =
   StrItemRestrict (name, mt, loc)

(*
 * Ambient items.
 *)
let rec wrap_functor loc args mn me =
   match args with
      [] ->
         (match mn with
             ModuleNameVar (v, _) ->
                v, me
           | ModuleNameConstrain (ModuleNameVar (v, loc), mt, _) ->
                v, ModuleExpConstrain (me, mt, loc)
           | _ ->
                raise (SyntaxError (loc, "bad module name")))
    | (v', mt) :: args ->
         let v, me = wrap_functor loc args mn me in
            v, ModuleExpFunctor (v', mt, me, loc)
   
let rec make_module_header loc1 (loc2, mn) (loc3, args) (loc4, me) =
   let v, me = wrap_functor (union_loc loc3 loc4) args mn me in
   let loc = union_loc loc1 loc2 in
      loc, (v, me)

let str_item_module (loc, (id, me)) =
   StrItemModule (id, me, loc)

(*
 * Module type.
 *)
let make_module_type_header loc1 (_, id) (loc2, me) =
   let loc = union_loc loc1 loc2 in
      loc, (id, me)

let str_item_module_type (loc, (id, mt)) =
   StrItemModuleType (id, mt, loc)

let rec wrap_sig_functor loc args mt =
   match args with
      [] ->
         mt
    | (v, mt') :: args ->
         ModuleTypeFunctor (v, mt', wrap_sig_functor loc args mt, loc)

let make_module_val_header loc1 (loc2, id) (loc3, args) (loc4, mt) =
   let loc = union_loc loc1 loc4 in
      loc, (id, wrap_sig_functor loc args mt)

(*
 * Adding items to a struct.
 *)
let append_item (loc1, items) (loc2, item) f =
   union_loc loc1 loc2, (items @ [f loc2 item])

(************************************************************************
 * SIG ITEM								*
 ************************************************************************)

(*
 * Value declaration.
 *)
let make_module_append (loc1, items) (loc2, item) =
   let loc = union_loc loc1 loc2 in
      loc, items @ [item]

let sig_item_val loc1 (_, id) (loc2, ty) =
   let loc = union_loc loc1 loc2 in
      loc, SigItemVal (id, ty, loc)

(************************************************************************
 * MODULE NAME								*
 ************************************************************************)

let module_name_var (loc, id) =
   loc, ModuleNameVar (id, loc)

let module_name_constrain (loc1, id) (loc2, mt) =
   let loc = union_loc loc1 loc2 in
      loc, ModuleNameConstrain (id, mt, loc)

let module_name_wrap loc1 (_, mn) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, mn

(************************************************************************
 * MODULE EXPR								*
 ************************************************************************)

(*
 * Module expressions.
 *)
let module_expr_var (loc, id) =
   loc, ModuleExpVar (id, loc)

let module_expr_struct loc1 (_, items) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, ModuleExpStruct (items, loc)

let module_expr_wrap loc1 (_, me) loc2 =
   union_loc loc1 loc2, me

let module_expr_constrain (loc1, me) (loc2, mt) =
   let loc = union_loc loc1 loc2 in
      loc, ModuleExpConstrain (me, mt, loc)

let module_expr_functor loc1 (loc2, (v, mt)) (loc3, me) =
   let loc = union_loc loc1 loc3 in
      loc, ModuleExpFunctor (v, mt, me, loc)

let module_expr_apply (loc1, me) (loc2, mn) =
   let loc = union_loc loc1 loc2 in
      loc, ModuleExpApply (me, mn, loc)

(************************************************************************
 * MODULE TYPE								*
 ************************************************************************)

(*
 * Module types.
 *)
let module_type_var (loc, id) =
   loc, ModuleTypeVar (id, loc)

let module_type_sig loc1 (_, items) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, ModuleTypeSig (items, loc)

let module_type_with_type (loc1, mt) (_, params) (_, id) (loc2, ty) =
   let loc = union_loc loc1 loc2 in
      loc, ModuleTypeWithType (mt, id, params, ty, loc)

let module_type_with_module (loc1, mt) (_, mn) (loc2, me) =
   let loc = union_loc loc1 loc2 in
      loc, ModuleTypeWithModule (mt, mn, me, loc)

let module_type_wrap loc1 (_, mt) loc2 =
   let loc = union_loc loc1 loc2 in
      loc, mt

let module_type_functor loc1 (loc2, (v, mt1)) (loc3, mt2) =
   let loc = union_loc loc1 loc3 in
      loc, ModuleTypeFunctor (v, mt1, mt2, loc)
%}

/*
 * End-of-file is a token.
 */
%token TokEof

/*
 * Binary operators return position.
 */
%token <Location.loc * Symbol.symbol> TokPlus
%token <Location.loc * Symbol.symbol> TokMinus
%token <Location.loc * Symbol.symbol> TokUMinus
%token <Location.loc * Symbol.symbol> TokStar
%token <Location.loc * Symbol.symbol> TokSlash
%token <Location.loc * Symbol.symbol> TokMod
%token <Location.loc * Symbol.symbol> TokLsl
%token <Location.loc * Symbol.symbol> TokLsr
%token <Location.loc * Symbol.symbol> TokAsl
%token <Location.loc * Symbol.symbol> TokAsr
%token <Location.loc * Symbol.symbol> TokXor
%token <Location.loc * Symbol.symbol> TokLAnd
%token <Location.loc * Symbol.symbol> TokLOr

%token <Location.loc * Symbol.symbol> TokEq
%token <Location.loc * Symbol.symbol> TokLt
%token <Location.loc * Symbol.symbol> TokLe
%token <Location.loc * Symbol.symbol> TokGe
%token <Location.loc * Symbol.symbol> TokGt
%token <Location.loc * Symbol.symbol> TokBAnd
%token <Location.loc * Symbol.symbol> TokBOr
%token <Location.loc * Symbol.symbol> TokExclam
%token <Location.loc * Symbol.symbol> TokColonEq
%token <Location.loc * Symbol.symbol> TokConcat

%token <Location.loc> TokDot
%token <Location.loc> TokDotDot
%token <Location.loc> TokDotDotDot
%token <Location.loc> TokComma
%token <Location.loc> TokSemi
%token <Location.loc> TokQuote
%token <Location.loc> TokArrow
%token <Location.loc> TokPipe
%token <Location.loc> TokColon
%token <Location.loc> TokLeftArrow
%token <Location.loc> TokDoubleColon
%token <Location.loc> TokDoubleSemi
%token <Location.loc> TokArrayDeref
%token <Location.loc> TokStringDeref
%token <Location.loc> TokDollar

/*
 * Keywords.
 */
%token <Location.loc> TokIf
%token <Location.loc> TokThen
%token <Location.loc> TokElse
%token <Location.loc> TokFor
%token <Location.loc> TokTo
%token <Location.loc> TokDownto
%token <Location.loc> TokDo
%token <Location.loc> TokDone
%token <Location.loc> TokWhile
%token <Location.loc> TokLet
%token <Location.loc> TokExternal
%token <Location.loc> TokVal
%token <Location.loc> TokRec
%token <Location.loc> TokAnd
%token <Location.loc> TokFun
%token <Location.loc> TokFunction
%token <Location.loc> TokCallCC
%token <Location.loc> TokMatch
%token <Location.loc> TokWith
%token <Location.loc> TokWhen
%token <Location.loc> TokTry
%token <Location.loc> TokException
%token <Location.loc> TokRestrict
%token <Location.loc> TokModule
%token <Location.loc> TokFunctor
%token <Location.loc> TokAmbient
%token <Location.loc> TokStruct
%token <Location.loc> TokSig
%token <Location.loc> TokBegin
%token <Location.loc> TokEnd
%token <Location.loc> TokThread
%token <Location.loc> TokMigrate
%token <Location.loc> TokAction
%token <Location.loc> TokIn
%token <Location.loc> TokOut
%token <Location.loc> TokOpen

%token <Location.loc> TokLeftParen
%token <Location.loc> TokRightParen
%token <Location.loc> TokLeftBrack
%token <Location.loc> TokRightBrack
%token <Location.loc> TokLeftCurly
%token <Location.loc> TokRightCurly
%token <Location.loc> TokLeftArray
%token <Location.loc> TokRightArray

%token <Location.loc> TokType
%token <Location.loc> TokOf
%token <Location.loc> TokMutable

%token <Location.loc> TokWild
%token <Location.loc> TokAs

/*
 * Terminal tokens.
 */
%token <Location.loc * Symbol.symbol> TokLid
%token <Location.loc * Symbol.symbol> TokUid
%token <Location.loc * string> TokString
%token <Location.loc * float> TokFloat
%token <Location.loc * char> TokChar
%token <Location.loc * int> TokInt

/*
 * Precedences.
 */
%left TokDoubleSemi
%left TokFunctor
%left TokLet TokIn
%left TokColon
%left TokFun TokFunction TokRestrict TokMigrate TokModule TokAmbient TokThread TokAction
%left TokPipe
%right TokDollar
%left TokWhen
%left TokSemi
%right TokIf
%nonassoc TokLeftArrow
%left TokDotDot TokDotDotDot
%left TokComma
%left TokColonEq
%left TokDoubleColon
%left TokBOr
%left TokBAnd
%left TokEq
%left TokLe TokLt TokGe TokGt
%left TokLOr
%left TokLAnd
%right TokConcat
%left TokLsl TokLsr TokAsl TokAsr TokXor
%left TokPlus TokMinus
%left TokStar TokSlash TokMod
%left prec_apply
%right TokUMinus TokExclam TokAs
%left TokDot TokArrayDeref TokStringDeref
%left TokWith TokAnd
%left TokLeftParen

/*
 * A complete program.
 */
%start parse_str_item
%start parse_str_items
%start parse_sig_items
%type <Aml_ast.str_item option> parse_str_item
%type <Aml_ast.str_item list> parse_str_items
%type <Aml_ast.sig_item list> parse_sig_items

%%
/************************************************************************
 * TOPLEVEL								*
 ************************************************************************/

parse_sig_items:
          sig_items TokEof
          { snd $1 }
        ;

sig_items:
          /* empty */
          { zero_loc, [] }
        | sig_items type_decl
          { append_item $1 $2 (fun loc item -> SigItemType (item, loc)) }
        | sig_items val_decl
          { make_module_append $1 $2 }
        | sig_items module_type_decl
          { append_item $1 $2 (fun loc (id, mt) -> SigItemModuleType (id, mt, loc)) }
        | sig_items module_val_decl
          { append_item $1 $2 (fun loc (id, mt) -> SigItemModule (id, mt, loc)) }
        | sig_items exception_decl
          { append_item $1 $2 (fun loc (id, tyl) -> SigItemException (id, tyl, loc)) }
        | sig_items open_decl
          { append_item $1 $2 (fun loc id -> SigItemOpen (id, loc)) }
        | sig_items external_decl
          { append_item $1 $2 (fun loc (id, ty, s) -> SigItemExternal (id, ty, s, loc)) }
        | sig_items external_type
          { append_item $1 $2 (fun loc (id, params, s) -> SigItemExternalType (id, params, s, loc)) }
        ;

val_decl: TokVal var TokColon ty
          { sig_item_val $1 $2 $4 }
        ;


/*
 * A program is either a declaration, or an expression.
 */
parse_str_items:
          opt_str_items TokEof
          { snd $1 }
        ;

parse_str_item:
 	  str_item TokDoubleSemi
	  { Some $1 }
        | TokEof
          { None }
	;

str_item: var_decl
          { str_item_let $1 }
        | fun_decl
          { str_item_letrecs $1 }
        | expr
          { str_item_expr $1 }
        | restr
          { str_item_restrict $1 }
        | type_decl
          { str_item_type $1 }
        | module_decl
          { str_item_module $1 }
        | module_type_decl
          { str_item_module_type $1 }
        ;

/************************************************************************
 * STRUCTURE ITEMS							*
 ************************************************************************/

opt_str_items:
          /* empty */
          { zero_loc, [] }
        | str_items
          { $1 }
        ;

str_items:
          expr
          { let loc, item = $1 in
               loc, [StrItemExpr (item, loc)]
          }
        | var_decl
	  { let loc, item = $1 in
               loc, [StrItemLet (item, loc)]
          }
        | fun_decl
	  { let loc, item = $1 in
               loc, [StrItemLetRec (item, loc)]
          }
        | restr
          { let loc, (name, mt) = $1 in
               loc, [StrItemRestrict (name, mt, loc)]
          }
        | type_decl
          { let loc, item = $1 in
               loc, [StrItemType (item, loc)]
          }
        | module_decl
          { let loc, (name, me) = $1 in
               loc, [StrItemModule (name, me, loc)]
          }
        | module_type_decl
          { let loc, (name, mt) = $1 in
               loc, [StrItemModuleType (name, mt, loc)]
          }
        | exception_decl
          { let loc, (name, tyl) = $1 in
               loc, [StrItemException (name, tyl, loc)]
          }
        | open_decl
          { let loc, id = $1 in
               loc, [StrItemOpen (id, loc)]
          }
        | external_decl
          { let loc, (id, ty, s) = $1 in
               loc, [StrItemExternal (id, ty, s, loc)]
          }
        | external_type
          { let loc, (id, params, s) = $1 in
               loc, [StrItemExternalType (id, params, s, loc)]
          }
        | str_items var_decl
	  { let loc1, items = $1 in
            let loc2, item = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemLet (item, loc)]
          }
        | str_items fun_decl
	  { let loc1, items = $1 in
            let loc2, item = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemLetRec (item, loc)]
          }
        | str_items restr
	  { let loc1, items = $1 in
            let loc2, (mn, mt) = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemRestrict (mn, mt, loc)]
          }
        | str_items type_decl
	  { let loc1, items = $1 in
            let loc2, item = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemType (item, loc)]
          }
        | str_items module_decl
          { let loc1, items = $1 in
            let loc2, (name, me) = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemModule (name, me, loc)]
          }
        | str_items module_type_decl
          { let loc1, items = $1 in
            let loc2, (name, mt) = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemModuleType (name, mt, loc)]
          }
        | str_items exception_decl
          { let loc1, items = $1 in
            let loc2, (name, tyl) = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemException (name, tyl, loc)]
          }
        | str_items open_decl
          { let loc1, items = $1 in
            let loc2, id = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemOpen (id, loc)]
          }
        | str_items external_decl
          { let loc1, items = $1 in
            let loc2, (id, ty, s) = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemExternal (id, ty, s, loc)]
          }
        | str_items external_type
          { let loc1, items = $1 in
            let loc2, (id, vars, s) = $2 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemExternalType (id, vars, s, loc)]
          }
        | str_items TokDoubleSemi
          { $1 }
        | str_items TokDoubleSemi expr
	  { let loc1, items = $1 in
            let loc2, item = $3 in
            let loc = union_loc loc1 loc2 in
               loc, items @ [StrItemExpr (item, loc)]
          }
        ;

restr:    TokRestrict TokUid TokColon module_type
          { restrict_header $1 $2 $4 }
        ;

var_decl: TokLet var patt_args TokEq expr
          { make_var_decl_header2 $1 $2 $3 $5 }
        | TokLet patt TokEq expr
          { make_var_decl_header1 $1 $2 $4 }
        | var_decl TokAnd patt_arg opt_patt_args TokEq expr
          { append_var_decl_header $1 $3 $4 $6 }
        ;

fun_decl: TokLet TokRec var opt_patt_args TokEq expr
          { expr_fun_decl_header $1 $3 $4 $6 }
        | fun_decl TokAnd var opt_patt_args TokEq expr
          { append_fun_decl_header $1 $3 $4 $6 }
        ;

open_decl: TokOpen uid2
          { let loc1 = $1 in
            let loc2, id = $2 in
               union_loc loc1 loc2, id
          }
        ;

external_decl: TokExternal var TokColon ty TokEq TokString
          { let loc1 = $1 in
            let _, id = $2 in
            let _, ty = $4 in
            let loc2, s = $6 in
            let loc = union_loc loc1 loc2 in
               loc, (id, ty, s)
          }
        ;

external_type: TokExternal TokType opt_type_params TokLid TokEq TokString
          { let loc1 = $1 in
            let _, params = $3 in
            let _, id = $4 in
            let loc2, s = $6 in
            let loc = union_loc loc1 loc2 in
               loc, (id, params, s)
          }
        ;

/************************************************************************
 * EXPRESSIONS								*
 ************************************************************************/

expr:	  apply
	  { expr_apply $1 }

          /* Unary operations */
	| TokMinus expr %prec TokUMinus
	  { let loc, _ = $1 in
               expr_unop (loc, sym_uminus) $2
          }
        | TokUMinus expr
          { expr_unop $1 $2 }

          /* Binary operations */
	| expr TokPlus expr
	  { expr_binop $1 $2 $3 }
	| expr TokMinus expr
	  { expr_binop $1 $2 $3 }
	| expr TokStar expr
	  { expr_binop $1 $2 $3 }
	| expr TokSlash expr
	  { expr_binop $1 $2 $3 }
        | expr TokMod expr
          { expr_binop $1 $2 $3 }
        | expr TokLAnd expr
          { expr_binop $1 $2 $3 }
        | expr TokLOr expr
          { expr_binop $1 $2 $3 }
        | expr TokLsl expr
          { expr_binop $1 $2 $3 }
        | expr TokLsr expr
          { expr_binop $1 $2 $3 }
        | expr TokAsl expr
          { expr_binop $1 $2 $3 }
        | expr TokAsr expr
          { expr_binop $1 $2 $3 }
        | expr TokXor expr
          { expr_binop $1 $2 $3 }
        | expr TokBAnd expr
          { expr_binop $1 $2 $3 }
        | expr TokBOr expr
          { expr_binop $1 $2 $3 }
	| expr TokEq expr
	  { expr_binop $1 $2 $3 }
	| expr TokLe expr
	  { expr_binop $1 $2 $3 }
	| expr TokLt expr
	  { expr_binop $1 $2 $3 }
	| expr TokGe expr
	  { expr_binop $1 $2 $3 }
	| expr TokGt expr
	  { expr_binop $1 $2 $3 }
        | expr TokConcat expr
          { expr_binop $1 $2 $3 }

          /* Assignment */
        | expr TokColonEq expr
           { expr_binop $1 $2 $3 }
        | expr TokSemi expr
           { expr_sequence $1 $3 }
        | simp TokDot lid TokLeftArrow expr
           { expr_assign $1 $3 $5 }

          /* Tuples */
        | expr TokComma expr
          { expr_tuple $1 $3 }

          /* Arrays and string */
	| simp TokArrayDeref expr TokRightParen TokLeftArrow expr %prec TokArrayDeref
           { expr_array_assign $1 $3 $6 }
	| simp TokStringDeref expr TokRightBrack TokLeftArrow expr %prec TokStringDeref
           { expr_string_assign $1 $3 $6 }

          /* Cons */
        | expr TokDoubleColon expr
          { expr_cons $1 $3 }

          /* Other expressions */
        | TokIf expr TokThen expr %prec TokIf
          { expr_if $1 $2 $4 }
	| TokIf expr TokThen expr TokElse expr %prec TokIf
	  { expr_if_else $1 $2 $4 $6 }
        | TokFor TokLid TokEq expr to_or_downto expr TokDo expr TokDone
          { expr_for $1 $2 $4 $5 $6 $8 $9 }
        | TokWhile expr TokDo expr TokDone
          { expr_while $1 $2 $4 $5 }
        | TokFun patt_arg opt_patt_args TokArrow expr %prec TokFun
          { expr_fun $1 $2 $3 $5 }
        | TokFunction guarded_commands %prec TokFun
          { expr_function $1 $2 }
        | TokMatch expr TokWith guarded_commands %prec TokFun
          { expr_match $1 $2 $4 }
        | TokTry expr TokWith guarded_commands %prec TokFun
          { expr_try $1 $2 $4 }

          /* Let definitions */
        | var_decl TokIn expr
          { expr_let $1 $3 }
        | fun_decl TokIn expr
          { expr_letrecs $1 $3 }
        | TokLet module_decl TokIn expr %prec TokLet
          { expr_module_expr $1 $2 $4 }

          /* Ambient calculus */
        | restr TokArrow expr %prec TokFun
          { expr_restrict $1 $3 }
        | TokMigrate expr TokArrow expr %prec TokMigrate
          { expr_migrate $1 $2 $4 }
        | TokThread expr
          { expr_thread $1 $2 }
        | TokAction apply
	  { expr_action $1 $2 }
        | TokOut uid2
          { expr_out $1 $2 }
        | TokAmbient TokUid TokEq module_expr %prec TokAmbient
          { expr_ambient $1 $2 $4 }
	;

to_or_downto:
          TokTo
          { true }
        | TokDownto
          { false }
        ;

opt_expr:
          /* empty */
          { None }
        | expr
          { Some $1 }
        ;

opt_record_args:
          /* empty */
          { zero_loc, [] }
        | record_args
          { $1 }
        | record_args TokSemi
          { $1 }
        ;

record_args:
          lid TokEq expr
          { expr_record_arg $1 $3 }
        | record_args TokSemi lid TokEq expr
          { append_record_arg $1 $3 $5 }
        ;

apply   : simp_uid
          { let loc, e = $1 in
               loc, [e]
          }
        | apply simp_uid
          { let loc1, a = $1 in
            let loc2, e = $2 in
               union_loc loc1 loc2, a @ [e]
          }
        ;

/*
 * "Simple" expressions are fully parenthesized.
 */
simp_uid: simp
          { $1 }
        | uid
          { expr_const $1 }
        ;

simp    : TokInt
	  { let loc, i = $1 in
               loc, Number (i, loc)
          }
        | TokString
          { let loc, s = $1 in
               loc, String (s, loc)
          }
        | TokChar
          { let loc, c = $1 in
               loc, Char (c, loc)
          }
        | lid_var
          { expr_var $1 }
        | TokExclam simp
	  { expr_unop $1 $2 }
	| simp TokDot lid
          { expr_proj $1 $3 }

          /* Parenthesized expressions */
        | TokBegin expr TokEnd
          { expr_wrap $1 $2 $3 }
        | TokLeftParen expr TokRightParen
          { expr_wrap $1 $2 $3 }
	| TokLeftParen TokIn uid2 TokRightParen
          { let loc2, name = $3 in
               union_loc $1 $4, In (name, loc2)
          }
        | TokLeftParen TokOpen uid2 TokRightParen
          { let loc2, name = $3 in
               union_loc $1 $4, Out (name, loc2)
          }
        | TokLeftParen TokRightParen
          { let loc = union_loc $1 $2 in
               loc, Const (unit_const, loc)
          }
	| TokLeftParen expr TokColon ty TokRightParen
          { expr_constraint $1 $2 $4 $5 }

          /* Arrays */
        | simp TokArrayDeref expr TokRightParen
          { expr_array_subscript $1 $3 $4 }
        | simp TokStringDeref expr TokRightBrack
           { expr_string_subscript $1 $3 $4 }

          /* Lists */
        | TokLeftBrack opt_expr TokRightBrack
          { expr_list $1 $2 $3 }

          /* Arrays */
        | TokLeftArray opt_expr TokRightArray
          { expr_array $1 $2 $3 }

          /* Records */
        | TokLeftCurly opt_record_args TokRightCurly
          { expr_record $1 $2 $3 }
        | TokLeftCurly simp TokWith opt_record_args TokRightCurly
          { expr_record_update $1 $2 $4 $5 }
        ;

lid     : TokLid
          { let loc, id = $1 in
               loc, [id]
          }
        | uid2 TokDot TokLid
          { let loc1, uid = $1 in
            let loc2, lid = $3 in
               union_loc loc1 loc2, uid @ [lid]
          }
        ;

uid     : uid2
          { $1 }
        ;

uid2    : TokUid
          { let loc, id = $1 in
               loc, [id]
          }
        | uid2 TokDot TokUid
          { let loc1, uid1 = $1 in
            let loc2, uid2 = $3 in
               union_loc loc1 loc2, uid1 @ [uid2]
          }
        ;

lid_var : var
          { let loc, id = $1 in
               loc, [id]
          }
        | uid2 TokDot var
          { let loc1, uid = $1 in
            let loc2, lid = $3 in
               union_loc loc1 loc2, uid @ [lid]
          }
        ;

var     : TokLid
          { $1 }
        | TokLeftParen operator TokRightParen
          { $2 }
        ;

operator: TokPlus
          { $1 }
        | TokMinus
          { $1 }
        | TokUMinus
          { $1 }
        | TokStar
          { $1 }
        | TokSlash
          { $1 }
        | TokMod
          { $1 }
        | TokLAnd
          { $1 }
        | TokLOr
          { $1 }
        | TokLsl
          { $1 }
        | TokLsr
          { $1 }
        | TokAsl
          { $1 }
        | TokAsr
          { $1 }
        | TokXor
          { $1 }
        | TokBAnd
          { $1 }
        | TokBOr
          { $1 }
        | TokEq
          { $1 }
        | TokLe
          { $1 }
        | TokLt
          { $1 }
        | TokGe
          { $1 }
        | TokGt
          { $1 }
        | TokExclam
          { $1 }
        | TokConcat
          { $1 }
        | TokColonEq
          { $1 }
        ;

/************************************************************************
 * TYPES								*
 ************************************************************************/

type_decl:
          TokType opt_type_params TokLid
          { make_abstract_type_header $1 $2 $3 }
        | TokType opt_type_params TokLid TokEq ty_top
          { make_type_header $1 $2 $3 $5 }
        | type_decl TokAnd opt_type_params TokLid
          { append_abstract_type_header $1 $2 $3 $4 }
        | type_decl TokAnd opt_type_params TokLid TokEq ty_top
          { append_type_header $1 $2 $3 $4 $6 } 
        ;

/*
 * Parameters.
 */
opt_type_params:
          /* empty */
          { zero_loc, [] }
        | type_params
          { $1 }
        ;

type_params:
          TokQuote TokLid
          { let loc, v = $2 in
               loc, [v]
          }
        | TokLeftParen opt_type_params2 TokRightParen
          { union_loc $1 $3, snd $2 }
        ;

opt_type_params2:
          /* empty */
          { zero_loc, [] }
        | type_params2
          { $1 }
        ;

type_params2:
          TokQuote TokLid
          { let loc, v = $2 in
               loc, [v]
          }
        | type_params2 TokComma TokQuote TokLid
          { let loc1, params = $1 in
            let loc2, v = $4 in
               union_loc loc1 loc2, params @ [v]
          }
        ;

/*
 * Toplevel type defintion allows
 * unions and records.
 */
ty_top  : union_type
          { type_union $1 }
        | TokLeftCurly opt_field_types TokRightCurly
          { type_record $1 $2 $3 }
        | ty
          { $1 }
        ;

ty      : ty_prod
          { $1 }
        | ty_prod TokArrow ty
          { type_function $1 $3 }
        ;

ty_prod : ty_simp
	  { $1 }
        | ty_prod TokStar ty_simp
          { type_product $1 $3 }
        ;

ty_simp : lid
          { type_lid $1 }
        | TokQuote TokLid
          { let loc2, id = $2 in
            let loc = union_loc $1 loc2 in
               loc, TyVar (id, loc)
          }
        | TokLeftParen ty TokComma type_args TokRightParen lid
          { type_args_apply $1 $2 $4 $6 }
        | TokLeftParen ty TokRightParen
          { type_wrap $1 $2 $3 }
        | ty_simp lid
          { type_apply $1 $2 }
        ;

type_args:
          ty
          { let loc, ty = $1 in
               loc, [ty]
          }
        | type_args TokComma ty
          { let loc1, types = $1 in
            let loc2, ty = $3 in
               union_loc loc1 loc2, types @ [ty]
          }
        ;

union_type:
	  union_id
          { make_union_header $1 (zero_loc, []) }
        | union_id TokOf ty
          { make_union_header $1 (split_prod_type $3) }
        | TokPipe union_id
          { make_union_header $2 (zero_loc, []) }
        | TokPipe union_id TokOf ty
          { make_union_header $2 (split_prod_type $4) }
        | union_type TokPipe union_id
          { append_union_header $1 $3 (zero_loc, []) }
        | union_type TokPipe union_id TokOf ty
          { append_union_header $1 $3 (split_prod_type $5) }
        ;


/*
 * Sorry, but we hardcode some constructors.
 */
union_id:
          TokUid
          { $1 }
        | TokLeftBrack TokRightBrack
          { union_loc $1 $2, Symbol.add "[]" }
        | TokLeftParen TokRightParen
          { union_loc $1 $2, Symbol.add "()" }
        | TokDoubleColon
          { $1, Symbol.add "::" }
        ;

opt_field_types:
          /* empty */
          { zero_loc, [] }
        | field_types
          { $1 }
        | field_types TokSemi
          { let loc, fty = $1 in
               union_loc loc $2, fty
          }
        ;

field_types:
          opt_mutable TokLid TokColon ty
          { type_record_field $1 $2 $4 }
        | field_types TokSemi opt_mutable TokLid TokColon ty
          { append_type_record_field $1 $3 $4 $6 }
        ;

opt_mutable:
          /* empty */
          { false }
        | TokMutable
          { true }
        ;

/************************************************************************
 * EXCEPTIONS								*
 ************************************************************************/

exception_decl:
          TokException TokUid
          { let loc1 = $1 in
            let loc2, id = $2 in
            let loc = union_loc loc1 loc2 in
               loc, (id, [])
          }
        | TokException TokUid TokOf ty
          { let loc1 = $1 in
            let _, id = $2 in
            let loc2, _ = $4 in
            let loc = union_loc loc1 loc2 in
               loc, (id, snd (split_prod_type $4))
          }
        ;

/************************************************************************
 * PATTERNS								*
 ************************************************************************/

guarded_commands:
          patt TokArrow expr %prec TokFun
          { expr_guarded_command $1 $3 }
        | TokPipe patt TokArrow expr %prec TokFun
          { expr_guarded_command $2 $4 }
        | guarded_commands TokPipe patt TokArrow expr %prec TokFun
          { append_guarded_command $1 $3 $5 }
        ;

opt_patt_args:
          /* empty */
          { zero_loc, [] }
        | patt_args
          { $1 }
        ;

patt_args:
          patt_arg
          { let loc, p = $1 in
               loc, [p]
          }
        | patt_args patt_arg
          { let loc1, pl = $1 in
            let loc2, p = $2 in
               union_loc loc1 loc2, pl @ [p]
          }
        ;
          
patt_arg:
          TokInt
          { pattern_number $1 }
        | TokChar
          { pattern_char $1 }
        | TokString
          { pattern_string $1 }
        | TokWild
          { pattern_wild $1 }
        | var
          { pattern_var $1 }

          /* Lists */
        | TokLeftBrack opt_array_patt TokRightBrack
          { pattern_list $1 $2 $3 }

          /* Arrays */
        | TokLeftArray opt_array_patt TokRightArray
          { pattern_array $1 $2 $3 }

          /* Records */
        | TokLeftCurly opt_record_patt TokRightCurly
          { pattern_record $1 $2 $3 }

          /* Disjoint union */
        | uid
          { pattern_const $1 (zero_loc, None) }

          /* Type constraint */
        | TokLeftParen patt TokColon ty TokRightParen
          { pattern_constrain $1 $2 $4 $5 }
        | TokLeftParen patt TokRightParen
          { pattern_wrap $1 $2 $3 }
        | TokLeftParen TokRightParen
          { pattern_unit $1 $2 }
        ;

patt:
          TokInt
          { pattern_number $1 }
        | TokChar
          { pattern_char $1 }
        | TokString
          { pattern_string $1 }
        | TokWild
          { pattern_wild $1 }
        | var
          { pattern_var $1 }

          /* Tuples */
        | patt TokComma patt
          { pattern_tuple $1 $3 }

          /* Ranges */
	| patt TokDotDot patt
          { pattern_range $1 $3 }
	| patt TokDotDotDot
          { pattern_range_left $1 $2 }
        | TokDotDotDot patt
          { pattern_range_right $1 $2 }
        | patt TokWhen expr
          { pattern_when $1 $3 }
        | TokDollar expr
          { pattern_expr $1 $2 }
        
          /* Lists */
        | patt TokDoubleColon patt
          { pattern_cons $1 $3 }
        | TokLeftBrack opt_array_patt TokRightBrack
          { pattern_list $1 $2 $3 }

          /* Arrays */
        | TokLeftArray opt_array_patt TokRightArray
          { pattern_array $1 $2 $3 }

          /* Records */
        | TokLeftCurly opt_record_patt TokRightCurly
          { pattern_record $1 $2 $3 }

          /* Disjoint union */
        | uid
          { pattern_const $1 (zero_loc, None) }
        | uid patt_arg
          { let loc, p = $2 in
               pattern_const $1 (loc, Some p)
          }
        | patt TokPipe patt
          { pattern_choice $1 $3 }

          /* Aliasing */
        | patt TokAs patt
          { pattern_as $1 $3 }

          /* Type constraint */
        | TokLeftParen patt TokColon ty TokRightParen
          { pattern_constrain $1 $2 $4 $5 }
        | TokLeftParen patt TokRightParen
          { pattern_wrap $1 $2 $3 }
        | TokLeftParen TokRightParen
          { pattern_unit $1 $2 }
        ;

opt_array_patt:
          /* empty */
          { zero_loc, [] }
        | array_patt
          { $1 }
        | array_patt TokSemi
          { $1 }
        ;

array_patt:
          patt
          { let loc, p = $1 in
               loc, [loc, p]
          }
        | array_patt TokSemi patt
          { let loc1, a = $1 in
            let loc2, p = $3 in
            let loc = union_loc loc1 loc2 in
               loc, a @ [loc2, p]
          }
        ;

opt_record_patt:
          /* empty */
          { zero_loc, [] }
        | record_patt
          { $1 }
        | record_patt TokSemi
          { $1 }
        ;

record_patt:
          lid TokEq patt
          { pattern_record_header $1 $3 }
        | record_patt TokSemi lid TokEq patt
          { append_record_pattern_header $1 $3 $5 }
        ;

/************************************************************************
 * MODULES								*
 ************************************************************************/

module_type_decl:
          TokModule TokType TokUid TokEq module_type
          { make_module_type_header $1 $3 $5 }
        ;

module_decl:
          TokModule module_name opt_module_args TokEq module_expr
          { make_module_header $1 $2 $3 $5 }
        ;

module_val_decl:
          TokModule TokUid opt_module_args TokColon module_type
          { make_module_val_header $1 $2 $3 $5 }
        ;

module_arg:
          TokLeftParen TokUid TokColon module_type TokRightParen
          { let loc = union_loc $1 $5 in
            let _, id = $2 in
            let _, mt = $4 in
               loc, (id, mt)
          }
        ;

module_args:
          module_arg
          { let loc, arg = $1 in
               loc, [arg]
          }

        | module_args module_arg
          { let loc1, args = $1 in
            let loc2, arg = $2 in
            let loc = union_loc loc1 loc2 in
               loc, args @ [arg]
          }
        ;

opt_module_args:
          /* empty */
          { zero_loc, [] }
        | module_args
          { $1 }
        ;

module_expr:
          module_expr_apply
          { $1 }
        | module_expr TokColon module_type
          { module_expr_constrain $1 $3 }
	| TokFunctor module_arg TokArrow module_expr %prec TokFunctor
          { module_expr_functor $1 $2 $4 }
        ;

module_expr_apply:
          module_expr_simp
          { $1 }
        | module_expr_apply module_expr_arg
          { module_expr_apply $1 $2 }
        ;

module_expr_arg:
          uid2
          { $1 }
        | TokLeftParen module_expr_arg TokRightParen
          { $2 }
        | TokLeftParen module_expr_arg TokColon module_type TokRightParen
          { $2 }
        ;

module_expr_simp:
          TokStruct opt_str_items TokEnd
          { module_expr_struct $1 $2 $3 }
        | uid2
          { module_expr_var $1 }
        | TokLeftParen module_expr TokRightParen
          { module_expr_wrap $1 $2 $3 }
        ;

module_type:
          TokSig sig_items TokEnd
          { module_type_sig $1 $2 $3 }
        | uid2
          { module_type_var $1 }
        | module_type with_or_and TokType opt_type_params lid TokEq ty_prod
          { module_type_with_type $1 $4 $5 $7 }
        | module_type with_or_and TokModule uid2 TokEq uid2
          { module_type_with_module $1 $4 $6 }
        | TokLeftParen module_type TokRightParen
          { module_type_wrap $1 $2 $3 }
        | TokFunctor module_arg TokArrow module_type %prec TokFunctor
          { module_type_functor $1 $2 $4 }
        ;

module_name:
          TokUid
          { module_name_var $1 }
        | TokLeftParen module_name TokColon module_type TokRightParen
          { module_name_constrain $2 $4 }
        | TokLeftParen module_name TokRightParen
          { module_name_wrap $1 $2 $3 }
        ;

with_or_and:
          TokWith
          { $1 }
        | TokAnd
          { $1 }
        ;
