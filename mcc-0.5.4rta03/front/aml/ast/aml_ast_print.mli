(*
 * Printing of expressions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format
open Symbol
open Aml_ast

(*
 * Helper functions.
 *)
val pp_print_vars : formatter -> var list -> unit

(*
 * Main printing functions.
 *)
val pp_print_exp : formatter -> exp -> unit
val pp_print_type : formatter -> ty -> unit
val pp_print_patt : formatter -> patt -> unit
val pp_print_str_item : formatter -> str_item -> unit
val pp_print_sig_item : formatter -> sig_item -> unit
val pp_print_module_exp : formatter -> module_exp -> unit
val pp_print_module_type : formatter -> module_type -> unit
val pp_print_prog : formatter -> prog -> unit

(*
 * Debugging functions.
 *)
val debug_exp : string -> exp -> unit
val debug_type : string -> ty -> unit
val debug_patt : string -> patt -> unit
val debug_str_item : string -> str_item -> unit
val debug_sig_item : string -> sig_item -> unit
val debug_module_exp : string -> module_exp -> unit
val debug_module_type : string -> module_type -> unit
val debug_prog : string -> prog -> unit

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
