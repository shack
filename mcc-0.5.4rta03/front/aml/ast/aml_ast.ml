(*
 * A functional language.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol

open Location

(*
 * Syntax errors.
 *)
exception SyntaxError of loc * string

(*
 * Variables are symbols.
 *)
type var = symbol

(*
 * Other kinds of names.
 *)
type label_name = var list
type const_name = var list
type ty_name = var list
type var_name = var list
type mod_name = var list
type mod_type_name = var list

(*
 * Types.
 *)
type ty =
   TyFun of ty * ty * loc
 | TyProd of ty list * loc
 | TyRecord of (bool * var * ty) list * loc
 | TyUnion of (var * ty list) list * loc
 | TyArray of ty * loc

   (* Variables *)
 | TyVar of var * loc
 | TyApply of ty_name * ty list * loc
 | TyAbstract of var * loc

(*
 * Patterns.
 *)
type patt =
   PattInt of int * loc
 | PattChar of char * loc
 | PattString of string * loc
 | PattWild of loc
 | PattVar of var * loc

 | PattTuple of patt list * loc
 | PattRecord of (label_name * patt) list * loc
 | PattArray of patt list * loc
 | PattConst of const_name * patt list * loc
 | PattChoice of patt * patt * loc
 | PattAs of patt * patt * loc
 | PattConstrain of patt * ty * loc
 | PattWrap of patt * loc

 | PattExpr of exp * loc
 | PattRange of patt * patt * loc
 | PattWhen of patt * exp * loc

(*
 * Static expressions.
 *)
and exp =
   (* Lambda calculus *)
   Number of int * loc
 | Char of char * loc
 | String of string * loc

 | Var of var_name * loc
 | Const of const_name * loc

 | Apply of exp * exp list * loc
 | Lambda of patt list * exp * loc
 | Function of guarded * loc
 | Let of lets * exp * loc
 | LetRec of letrecs * exp * loc
 | If of exp * exp * exp * loc
 | Constrain of exp * ty * loc
 | For of var * exp * bool * exp * exp * loc
 | While of exp * exp * loc
 | Wrap of exp * loc

   (* Aggregates *)
 | Tuple of exp list * loc
 | Array of exp list * loc
 | ArraySubscript of exp * exp * loc
 | ArraySetSubscript of exp * exp * exp * loc
 | StringSubscript of exp * exp * loc
 | StringSetSubscript of exp * exp * exp * loc
 | Record of record_item list * loc
 | RecordProj of exp * label_name * loc
 | RecordSetProj of exp * label_name * exp * loc
 | RecordUpdate of exp * record_item list * loc

   (* Sequential execution *)
 | Sequence of exp * exp * loc

   (* Control flow *)
 | Match of exp * guarded * loc
 | Try of exp * guarded * loc
 | Raise of exp * loc

   (* Ambient calculus *)
 | In of mod_name * loc
 | Out of mod_name * loc
 | Open of mod_name * loc
 | Action of exp list * loc
 | Restrict of var * module_type * exp * loc
 | Ambient of var * module_exp * loc
 | Thread of exp * loc
 | Migrate of exp * exp * loc
 | LetModule of var * module_exp * exp * loc

(* Top-level items in an ambient *)
and str_item =
   StrItemLet of lets * loc
 | StrItemLetRec of letrecs * loc
 | StrItemExpr of exp * loc
 | StrItemRestrict of var * module_type * loc
 | StrItemType of types * loc
 | StrItemModule of var * module_exp * loc
 | StrItemModuleType of var * module_type * loc
 | StrItemException of var * ty list * loc
 | StrItemOpen of mod_name * loc
 | StrItemExternal of var * ty * string * loc
 | StrItemExternalType of var * var list * string * loc

(* Info for a record creation *)
and record_item = label_name * exp * loc

(* Variable definitions *)
and lets = (patt * exp) list

(* Function definitions *)
and letrecs = (var * exp) list

(* Type definitions *)
and types = (var * var list * ty * loc) list

(* Guarded commands *)
and guarded = (patt * exp) list

(*
 * Signature item.
 *)
and sig_item =
   SigItemType of types * loc
 | SigItemVal of var * ty * loc
 | SigItemModuleType of var * module_type * loc
 | SigItemModule of var * module_type * loc
 | SigItemException of var * ty list * loc
 | SigItemOpen of mod_name * loc
 | SigItemExternal of var * ty * string * loc
 | SigItemExternalType of var * var list * string * loc

(*
 * Module expression.
 *)
and module_exp =
   ModuleExpStruct of str_item list * loc
 | ModuleExpVar of mod_name * loc
 | ModuleExpConstrain of module_exp * module_type * loc
 | ModuleExpFunctor of var * module_type * module_exp * loc
 | ModuleExpApply of module_exp * mod_name * loc

and module_type =
   ModuleTypeSig of sig_item list * loc
 | ModuleTypeVar of mod_type_name * loc
 | ModuleTypeWithType of module_type * ty_name * var list * ty * loc
 | ModuleTypeWithModule of module_type * mod_name * mod_name * loc
 | ModuleTypeFunctor of var * module_type * module_type * loc

(*
 * A program.
 *)
type prog_exp =
   Interface of sig_item list
 | Implementation of str_item list

type prog =
   { prog_name   : var;
     prog_exp    : prog_exp
   }

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
