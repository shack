(*
 * Operations on locations.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Location

open Aml_ast

(*
 * Get locations.
 *)
val loc_of_exp : exp -> loc
val loc_of_patt : patt -> loc
val loc_of_type : ty -> loc
val loc_of_str_item : str_item -> loc
val loc_of_str_items : loc -> str_item list -> loc
val loc_of_sig_item : sig_item -> loc
val loc_of_sig_items : loc -> sig_item list -> loc
val loc_of_module_type : module_type -> loc
val loc_of_module_exp : module_exp -> loc

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
