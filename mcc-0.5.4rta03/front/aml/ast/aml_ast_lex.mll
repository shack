(*
 * Lexer for the simple grammar.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

{
open Format

open Location

open Aml_ast
open Aml_ast_parse
open Symbol

(*
 * Keep track of line numbers and locations.
 *)
let current_file     = ref (Symbol.add "<stdin>")
let current_line     = ref 1
let current_schar    = ref 0
let current_location = ref (bogus_loc "<stdin>")

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
   let line = !current_line in
   let schar = Lexing.lexeme_start lexbuf - !current_schar in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let file = !current_file in
   let pos = create_loc file line schar line echar in
      current_location := pos;
      pos

(*
 * Set the name of the current file.
 *)
let set_file_name name line =
   let name = Symbol.add name in
      current_file := name;
      current_line := line;
      current_schar := 0;
      current_location := create_loc name line 0 line 0;
      !current_location

(*
 * Go to the next line.
 *)
let set_next_line lexbuf =
   incr current_line;
   current_schar := Lexing.lexeme_end lexbuf

(*
 * Set the line number from a preprocessor command.
 *)
let set_line_number lexbuf line file =
   let file = Symbol.add file in
   let _ = current_line := line in
   let _ = current_schar := Lexing.lexeme_end lexbuf in
   let pos = create_loc file line 0 line 0 in
      current_location := pos

(*
 * Read a number from the string.
 *)
let zero_code = Char.code '0'

let string_of_int_array a =
   let len = Array.length a in
   let s = String.create len in
      for i = 0 to pred len do
         s.[i] <- Char.chr (a.(i) land 0xff)
      done;
      s

let int_of_line s =
   let len = String.length s in
   let rec loop i j =
      if i = len then
         j
      else
         let c = s.[i] in
            match c with
               '0'..'9' -> loop (succ i) (j * 10 + (Char.code c - zero_code))
              | _ -> loop (succ i) j
   in
      loop 0 0

(*
 * Provide a buffer for building strings.
 *)
let string_start = ref (0, 0)
let stringbuf = ref []

let string_add_int i =
   stringbuf := i :: !stringbuf

let string_add_char c =
   string_add_int (Char.code c)

let pop_string lexbuf =
   let sline, schar = !string_start in
   let eline = !current_line in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let pos = create_loc !current_file sline schar eline echar in
   let s = Array.of_list (List.rev !stringbuf) in
      stringbuf := [];
      string_of_int_array s, pos

let set_string_start lexbuf =
   string_start := !current_line, Lexing.lexeme_start lexbuf - !current_schar

(*
 * Same for normal integers.
 *)
let int_of_radix radix s i =
   let len = String.length s in
   let add_char x c1 c2 off =
      let i = Char.code c1 - Char.code c2 + off in
         (x * radix) + i
   in
   let rec loop x i =
      if i = len then
         x
      else
         let c = s.[i] in
            match s.[i] with
               'a'..'f' -> loop (add_char x c 'a' 10) (succ i)
             | 'A'..'F' -> loop (add_char x c 'A' 10) (succ i)
             | '0'..'9' -> loop (add_char x c '0' 0)  (succ i)
             | _ -> loop x (succ i)
   in
      loop 0 i

let int_of_decimal = int_of_radix 10
let int_of_octal   = int_of_radix 8
let int_of_hex     = int_of_radix 16

(*
 * Extract the char constant.
 *)
let lex_char s =
   match s.[0] with
      '\\' ->
         (match s.[1] with
             '0'..'9' -> int_of_decimal s 1
           | 'o' | 'O' -> int_of_octal s 2
           | 'x' | 'X' -> int_of_hex s 2
           | 'n' ->  Char.code '\n'
           | 't' ->  Char.code '\t'
           | 'v' ->  Char.code '\011'
           | 'b' ->  Char.code '\b'
           | 'r' ->  Char.code '\r'
           | 'f' ->  Char.code '\012'
           | 'a' ->  Char.code '\007'
           | '\\' -> Char.code '\\'
           | '\'' -> Char.code '\''
           | '"' ->  Char.code '"'
           | c ->    Char.code c)
    | c ->
        Char.code c

(*
 * Floating-point constants.
 * the constant may be terminated by a 'f' or 'l'.
 * 'f' means float, 'l' means long double.
 *)
let lex_float s =
   let len = String.length s in
      if len = 0 then
         0.0
      else
         let s =
            match s.[pred len] with
               'f' | 'F' -> String.sub s 0 (pred len)
             | 'l' | 'L' -> String.sub s 0 (pred len)
             | _ -> s
         in
            float_of_string s
   
(*
 * Keyword table.
 *)
let true_sym  = Symbol.add "true"
let false_sym = Symbol.add "false"

let special =
   ["if",              (fun loc id -> TokIf loc);
    "then",            (fun loc id -> TokThen loc);
    "else",            (fun loc id -> TokElse loc);
    "for",             (fun loc id -> TokFor loc);
    "to",              (fun loc id -> TokTo loc);
    "downto",          (fun loc id -> TokDownto loc);
    "do",              (fun loc id -> TokDo loc);
    "done",            (fun loc id -> TokDone loc);
    "while",           (fun loc id -> TokWhile loc);
    "let",             (fun loc id -> TokLet loc);
    "external",        (fun loc id -> TokExternal loc);
    "val",             (fun loc id -> TokVal loc);
    "rec",             (fun loc id -> TokRec loc);
    "and",             (fun loc id -> TokAnd loc);
    "or",              (fun loc id -> TokBOr (loc, id));
    "fun",             (fun loc id -> TokFun loc);
    "function",        (fun loc id -> TokFunction loc);

    "restrict",        (fun loc id -> TokRestrict loc);
    "ambient",         (fun loc id -> TokAmbient loc);
    "module",          (fun loc id -> TokModule loc);
    "functor",         (fun loc id -> TokFunctor loc);
    "thread",          (fun loc id -> TokThread loc);
    "migrate",         (fun loc id -> TokMigrate loc);
    "sig",             (fun loc id -> TokSig loc);
    "begin",           (fun loc id -> TokBegin loc);
    "struct",          (fun loc id -> TokStruct loc);
    "end",             (fun loc id -> TokEnd loc);
    "action",          (fun loc id -> TokAction loc);
    "in",              (fun loc id -> TokIn loc);
    "out",             (fun loc id -> TokOut loc);
    "open",            (fun loc id -> TokOpen loc);

    "match",           (fun loc id -> TokMatch loc);
    "try",             (fun loc id -> TokTry loc);
    "exception",       (fun loc id -> TokException loc);
    "with",            (fun loc id -> TokWith loc);

    "mod",             (fun loc id -> TokMod (loc, id));
    "lsl",             (fun loc id -> TokLsl (loc, id));
    "lsr",             (fun loc id -> TokLsr (loc, id));
    "asl",             (fun loc id -> TokAsl (loc, id));
    "asr",             (fun loc id -> TokAsr (loc, id));
    "lxor",            (fun loc id -> TokXor (loc, id));
    "land",            (fun loc id -> TokLAnd (loc, id));
    "lor",             (fun loc id -> TokLOr (loc, id));

    "type",            (fun loc id -> TokType loc);
    "of",              (fun loc id -> TokOf loc);
    "mutable",         (fun loc id -> TokMutable loc);

    "as",              (fun loc id -> TokAs loc);
    "true",            (fun loc id -> TokUid (loc, true_sym));
    "false",           (fun loc id -> TokUid (loc, false_sym));
    "_",               (fun loc id -> TokWild loc)]

let symtab =
   let table =
      List.fold_left (fun table (name, f) ->
         SymbolTable.add table (Symbol.add name) f) SymbolTable.empty special
   in
      table

(*
 * Look up the string, and return a symbol if
 * the lookup fails.
 *)
let lex_symbol s pos =
   let sym = Symbol.add s in
      try (SymbolTable.find symtab sym) pos sym with
         Not_found ->
            TokLid (pos, sym)
}

(*
 * White space.
 *)
let white = [' ' '\t' '\r' '\012']+

(*
 * Identifiers and keywords.
 *)
let lid_prefix = ['_' 'a'-'z']
let uid_prefix = ['A'-'Z']
let name_suffix = ['_' 'A'-'Z' 'a'-'z' '0'-'9' ''']
let lid = lid_prefix name_suffix *
let uid = uid_prefix name_suffix *

(*
 * Numbers.
 *)
let decimal_char = ['0'-'9']
let octal_char = ['0'-'7']
let hex_char = ['0'-'9' 'a'-'f' 'A'-'F']

let decimal = ['1'-'9'] decimal_char*
let octal = '0' octal_char*
let hex = "0x" hex_char*
let number = decimal | octal
let float0 = ['0'-'9']+ '.' ['0'-'9']* (('e' | 'E') ('+' | '-')? decimal)?
let float1 = ['0'-'9']* '.' ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)?
let float2 = ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)

let float_suffix = ['f' 'F' 'l' 'L']
let float_const = float0 | float1 | float2

let int_suffix = (['l' 'L'] ['u' 'U']?) | (['u' 'U'] ['l' 'L']?)
let decimal_const = decimal int_suffix?
let octal_const   = octal int_suffix?
let hex_const     = hex int_suffix?

(*
 * Character constants.
 *)
let char_decimal = "\\"  decimal_char decimal_char? decimal_char?
let char_octal   = "\\o" octal_char octal_char? octal_char?
let char_hex     = "\\x" hex_char hex_char? hex_char?
let char_escape  = "\\" ['a'-'z' '\'' '\\' '"']
let char_normal  = [' '-'\255']
let char_cases   = char_octal | char_decimal | char_hex | char_escape | char_normal
let char_const   = "'" char_cases "'"

(*
 * String constants.
 * Be kind to DOS.
 *)
let string_break = "\\\n" | "\\\n\r"

(*
 * Main parser.
 *)
rule main =
   parse white
      { main lexbuf }
 | '\n'
      { set_next_line lexbuf; main lexbuf }

   (* Preprocessor line numbers *)
 | '#' white? number white? '"'
      { let line = int_of_line (Lexing.lexeme lexbuf) in
           set_string_start lexbuf;
           string lexbuf;
           skip_line lexbuf;
           let file, _ = pop_string lexbuf in
              set_line_number lexbuf line file;
              main lexbuf
      }

   (* Numbers *)
 | octal_const
      { let pos = set_lexeme_position lexbuf in
        let i = int_of_octal (Lexing.lexeme lexbuf) 0 in
           TokInt (pos, i)
      }
 | hex_const
      { let pos = set_lexeme_position lexbuf in
        let i = int_of_hex (Lexing.lexeme lexbuf) 0 in
           TokInt (pos, i)
      }
 | decimal_const
      { let pos = set_lexeme_position lexbuf in
        let i = int_of_decimal (Lexing.lexeme lexbuf) 0 in
           TokInt (pos, i)
      }
 | float_const
      { let pos = set_lexeme_position lexbuf in
        let x = lex_float (Lexing.lexeme lexbuf) in
           TokFloat (pos, x)
      }

   (* Names *)
 | lid
      {	let loc = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
           lex_symbol s loc
      }
 | uid
      { TokUid (set_lexeme_position lexbuf, Symbol.add (Lexing.lexeme lexbuf)) }

   (* Comment *)
 | "(*"
      { comment lexbuf; main lexbuf }
 | "("
      { TokLeftParen (set_lexeme_position lexbuf) }
 | ")"
      { TokRightParen (set_lexeme_position lexbuf) }

 | char_const
      { let pos = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
        let len = String.length s in
        let s = String.sub s 1 (len - 1) in
        let c = lex_char s in
        let c = Char.chr c in
           TokChar (pos, c)
      }

 | '"'
      { set_string_start lexbuf;
        string lexbuf;
        let s, pos = pop_string lexbuf in
           TokString (pos, s)
      }

 | ";;"
      { TokDoubleSemi (set_lexeme_position lexbuf) }
 | ".("
      { TokArrayDeref (set_lexeme_position lexbuf) }
 | ".["
      { TokStringDeref (set_lexeme_position lexbuf) }
 | "[|"
      { TokLeftArray (set_lexeme_position lexbuf) }
 | "|]"
      { TokRightArray (set_lexeme_position lexbuf) }
 | [';' ',' ''' '[' ']' '{' '}' '$']
      { let loc = set_lexeme_position lexbuf in
           match (Lexing.lexeme lexbuf).[0] with
              ';' -> TokSemi loc
            | ',' -> TokComma loc
            | '\'' -> TokQuote loc
            | '[' -> TokLeftBrack loc
            | ']' -> TokRightBrack loc
            | '{' -> TokLeftCurly loc
            | '}' -> TokRightCurly loc
            | '$' -> TokDollar loc
            | _ -> raise (SyntaxError (loc, "malformed operator"))
      }

 | [':' '+' '-' '*' '/' '=' '<' '>' '&' '|' '!' '.' '~' '^' '@']+
      { let loc = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
        let id = Symbol.add s in
        let len = String.length s in
        match s.[0] with
              '+' -> TokPlus   (loc, id)
            | '*' -> TokStar   (loc, id)
            | '/' -> TokSlash  (loc, id)
            | '-' ->
                 if len = 2 && s.[1] = '>' then
                    TokArrow loc
                 else
                    TokMinus (loc, id)
            | '~' ->
                 TokUMinus (loc, id)

            | ':' ->
                 if len = 1 then
		    TokColon loc
                 else if len = 2 && s.[1] = ':' then
	            TokDoubleColon loc
                 else
                    TokColonEq (loc, id)

  	    | '!' ->
                 if len = 2 && s.[1] = '=' then
                    TokLe (loc, id)
                 else
                    TokExclam (loc, id)

            | '=' -> TokEq (loc, id)

            | '<' ->
                if len = 1 then
                   TokLt (loc, id)
                else if len = 2 && s.[1] = '-' then
	           TokLeftArrow loc
                else
                   TokLe (loc, id)

            | '>' ->
                if len > 1 && s.[1] = '=' then
                   TokGe (loc, id)
                else
                   TokGt (loc, id)
                
            | '&' -> TokBAnd (loc, id)
            | '|' ->
                if len = 1 then
                   TokPipe loc
                else
                   TokBOr (loc, id)

            | '^'
            | '@' -> TokConcat (loc, id)

            | '.' ->
                (match s with
                   "." -> TokDot loc
                  | ".." -> TokDotDot loc
                  | "..." -> TokDotDotDot loc
                  | _ -> raise (SyntaxError (loc, sprintf "undefined token: %s" s)))

            | _ -> raise (SyntaxError (loc, sprintf "undefined token: %s" s))
      }

   (*
    * All other characters are a syntax error.
    *)
 | _
      { let pos = set_lexeme_position lexbuf in
           raise (SyntaxError (pos, Printf.sprintf "illegal char: '%s'"
              (String.escaped (Lexing.lexeme lexbuf))))
      }

 | eof
      { TokEof }

(*
 * Strings are delimited by double-quotes.
 * Chars may be escaped.
 *)
and string = parse
   '"' | eof
      { () }

   (* Escaped newlines are ignored *)
 | string_break
      { set_next_line lexbuf;
        string lexbuf
      }

   (* Not ANSI standard, but newlines are allowed inside a string *)
 | '\n'
      { set_next_line lexbuf;
        string_add_char '\n';
        string lexbuf
      }

   (* Other characters (including escape sequences) *)
 | char_cases
      { let c = lex_char (Lexing.lexeme lexbuf) in
        string_add_int c;
        string lexbuf
      }

   (* All other are literal *)
 | _
      { string_add_char (Lexing.lexeme lexbuf).[0];
        string lexbuf
      }

(*
 * Skip all chars to the eol.
 *)
and skip_line = parse
   '\n' { () }
 | eof { () }
 | _ { skip_line lexbuf }

(*
 * ML-style comments are nested.
 *)
and comment =
   parse "(*"
      { comment lexbuf; comment lexbuf }
 | "*)"
      { () }
 | eof
      { () }
 | '\n'
      { set_next_line lexbuf;
        comment lexbuf
      }
 | _
      { comment lexbuf }

{
let parse_str_item lexbuf =
   try Aml_ast_parse.parse_str_item main lexbuf with
      Parsing.Parse_error ->
         raise (SyntaxError (!current_location, "syntax error"))

let parse_str_items lexbuf =
   try Aml_ast_parse.parse_str_items main lexbuf with
      Parsing.Parse_error ->
         raise (SyntaxError (!current_location, "syntax error"))

let parse_sig_items lexbuf =
   try Aml_ast_parse.parse_sig_items main lexbuf with
      Parsing.Parse_error ->
         raise (SyntaxError (!current_location, "syntax error"))
}

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
