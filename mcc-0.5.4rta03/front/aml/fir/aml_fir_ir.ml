(*
 * Convert the IR to FIR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Field_table
open Interval_set

open Sizeof_type

open Fir
open Fir_ds
open Fir_set
open Fir_type
open Fir_state
open Fir_print
open Fir_ty_closure
open Fir_standardize

open Aml_ir_pos
open Aml_ir_exn

module Pos = MakePos (struct let name = "Aml_fir_ir" end)
open Pos

module FirPos = Fir_pos.MakePos (struct let name = "Aml_fir_ir" end)

module Symbol2Compare =
struct
   type t = var * var

   let compare (v11, v12) (v21, v22) =
      let cmp = Symbol.compare v11 v21 in
         if cmp = 0 then
            Symbol.compare v12 v22
         else
            cmp
end

module Symbol2Table = Mc_map.McMake (Symbol2Compare)

(*
 * Constants.
 *)
let pre_char = Rawint.Int8
let signed_char = false
let pre_float = Rawfloat.Double

(*
 * Generic types.
 *)
let ty_char = TyRawInt (pre_char, signed_char)
let ty_float = TyFloat pre_float

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Reorder the second list to correspond to the first.
 *)
let reorder_table pos labels table =
   let pos = string_pos "reorder_table" pos in
      List.map (fun label ->
            try SymbolTable.find table label with
               Not_found ->
                  raise (IRException (pos, UnboundVar label))) labels

let reorder_list pos labels fields =
   let pos = string_pos "reorder_list" pos in
   let table = List.fold_left (fun table (v, x) -> SymbolTable.add table v x) SymbolTable.empty fields in
      reorder_table pos labels table

(************************************************************************
 * LABEL ENVRIONMENT
 ************************************************************************)

(*
 * The label environment maps variables to numbers.
 *)
type lenv =
   { lenv_label  : int SymbolTable.t;
     lenv_val    : int Symbol2Table.t;
     lenv_union  : (bool * int * int_set) SymbolTable.t;
     lenv_record : label list SymbolTable.t;
     lenv_module : label list SymbolTable.t;
     lenv_const  : int SymbolTable.t
   }

let lenv_empty =
   { lenv_label  = SymbolTable.empty;
     lenv_val    = Symbol2Table.empty;
     lenv_union  = SymbolTable.empty;
     lenv_record = SymbolTable.empty;
     lenv_module = SymbolTable.empty;
     lenv_const  = SymbolTable.empty
   }

(*
 * Label operations.
 *)
let lenv_add_label lenv v i =
   { lenv with lenv_label = SymbolTable.add lenv.lenv_label v i }

let lenv_lookup_label lenv pos v =
   try SymbolTable.find lenv.lenv_label v with
      Not_found ->
         raise (IRException (string_pos "lenv_lookup_label" pos, UnboundVar v))

let lenv_add_record lenv v s =
   { lenv with lenv_record = SymbolTable.add lenv.lenv_record v s }

let lenv_lookup_record lenv pos v =
   try SymbolTable.find lenv.lenv_record v with
      Not_found ->
         raise (IRException (pos, UnboundVar v))

(*
 * Constructor names.
 *)
let lenv_add_const lenv v i =
   { lenv with lenv_const = SymbolTable.add lenv.lenv_const v i }

let lenv_lookup_const lenv pos v =
   try SymbolTable.find lenv.lenv_const v with
      Not_found ->
         raise (IRException (pos, UnboundVar v))

let lenv_add_union lenv v enum_flag n s =
   { lenv with lenv_union = SymbolTable.add lenv.lenv_union v (enum_flag, n, s) }

let lenv_lookup_union lenv pos v =
   try SymbolTable.find lenv.lenv_union v with
      Not_found ->
         raise (IRException (pos, UnboundVar v))

let lenv_union_is_enum lenv pos v =
   let enum_flag, _, _ = lenv_lookup_union lenv pos v in
      enum_flag

(*
 * Module names.
 *)
let lenv_add_val lenv ty_var ext_var i =
   { lenv with lenv_val = Symbol2Table.add lenv.lenv_val (ty_var, ext_var) i }

let lenv_lookup_val lenv pos ty_var ext_var =
   try Symbol2Table.find lenv.lenv_val (ty_var, ext_var) with
      Not_found ->
         raise (IRException (pos, UnboundVal (ty_var, ext_var)))

let lenv_add_module lenv v s =
   { lenv with lenv_module = SymbolTable.add lenv.lenv_module v s }

let lenv_lookup_module lenv pos v =
   try SymbolTable.find lenv.lenv_module v with
      Not_found ->
         raise (IRException (pos, UnboundVar v))

(************************************************************************
 * BUILD ENV
 ************************************************************************)

(*
 * Add union constructors to the environment.
 *)
let build_union_lenv lenv ty_var fields =
   let enum_flag =
      FieldTable.fold (fun enum_flag _ tyl ->
            enum_flag && tyl = []) true fields
   in
   let n = FieldTable.cardinal fields in
   let s = IntSet.of_interval (Closed 0) (Open n) in
   let lenv = lenv_add_union lenv ty_var enum_flag n s in
      FieldTable.fold_index (fun lenv v _ i ->
            lenv_add_const lenv v i) lenv fields

(*
 * Add record labels to the environment.
 *)
let build_record_lenv lenv ty_var fields =
   let lenv =
      FieldTable.fold_index (fun lenv v _ i ->
            lenv_add_label lenv v i) lenv fields
   in
   let fields = FieldTable.to_list fields in
   let fields = List.map (fun (v, _) -> v) fields in
      lenv_add_record lenv ty_var fields

(*
 * Add module labels to the environment.
 *)
let build_module_lenv lenv ty_var names fields =
   let lenv =
      FieldTable.fold_index (fun lenv ext_var v i ->
            eprintf "module_field: %a.%a@." pp_print_symbol ty_var pp_print_symbol ext_var;
            lenv_add_val lenv ty_var ext_var i) lenv names
   in
   let fields = FieldTable.to_list names in
   let fields = List.map (fun (v, _) -> v) fields in
      lenv_add_module lenv ty_var fields

(*
 * Create the label translations by looking at all the type defininitions.
 *)
let build_lenv prog =
   let { Aml_ir.prog_types = types } = prog in
      SymbolTable.fold (fun lenv ty_var { Aml_ir.tydef_inner = tydef } ->
            match Aml_ir_ds.dest_tydef_inner_core tydef with
               Aml_ir.TyDefLambda _
             | Aml_ir.TyDefFrame _
             | Aml_ir.TyDefDTuple _ ->
                  lenv
             | Aml_ir.TyDefUnion fields ->
                  build_union_lenv lenv ty_var fields
             | Aml_ir.TyDefRecord fields ->
                  build_record_lenv lenv ty_var fields
             | Aml_ir.TyDefModule (names, fields) ->
                  build_module_lenv lenv ty_var names fields) lenv_empty types

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Build the FIR union set.
 *)
let build_union_set lenv pos ty_var s =
   let pos = string_pos "build_union_set" pos in
   let _, _, set = lenv_lookup_union lenv pos ty_var in
      match s with
         Aml_ir.AnyIndex ->
            set
       | Aml_ir.PosIndex const_var ->
            let i = lenv_lookup_const lenv pos const_var in
               IntSet.of_point i
       | Aml_ir.NegIndex const_set ->
            (* Subtract these *)
            SymbolSet.fold (fun set const_var ->
                  let i = lenv_lookup_const lenv pos const_var in
                     IntSet.subtract_point set i) set const_set

(*
 * Convert a type.
 *)
let rec build_type lenv ty =
   let pos = string_pos "build_type" (type_exp_pos ty) in
      match Aml_ir_ds.dest_type_core ty with
         Aml_ir.TyVoid ->
            TyEnum 0
       | Aml_ir.TyInt ->
            TyInt
       | Aml_ir.TyChar ->
            ty_char
       | Aml_ir.TyString ->
            TyRawData
       | Aml_ir.TyFloat ->
            ty_float
       | Aml_ir.TyFun (tyl, ty) ->
            TyFun (build_types lenv tyl, build_type lenv ty)
       | Aml_ir.TyTuple tyl ->
            TyTuple (NormalTuple, build_immutable_types lenv tyl)
       | Aml_ir.TyDTuple (ty_var, tyl) ->
            TyDTuple (ty_var, build_immutable_types_opt lenv tyl)
       | Aml_ir.TyTag (ty_var, tyl) ->
            TyTag (ty_var, build_immutable_types lenv tyl)
       | Aml_ir.TyArray ty ->
            TyArray (build_type lenv ty)
       | Aml_ir.TyVar v ->
            TyVar v
       | Aml_ir.TyAll (vars, ty) ->
            TyAll (vars, build_type lenv ty)
       | Aml_ir.TyExists (vars, ty) ->
            TyExists (vars, build_type lenv ty)
       | Aml_ir.TyProject (v, i) ->
            TyProject (v, i)
       | Aml_ir.TyApply  (v, tyl)
       | Aml_ir.TyRecord (v, tyl)
       | Aml_ir.TyModule (v, tyl) ->
            TyApply (v, build_types lenv tyl)
       | Aml_ir.TyFrame  (v, tyl) ->
            TyFrame (v, build_types lenv tyl)
       | Aml_ir.TyUnion (v, tyl, s) ->
            let tyl = build_types lenv tyl in
            let enum_flag, n, _ = lenv_lookup_union lenv pos v in
               if enum_flag then
                  TyEnum n
               else
                  TyUnion (v, tyl, build_union_set lenv pos v s)
       | Aml_ir.TyExternal (ty, _) ->
            build_type lenv ty
       | Aml_ir.TyFormat _ ->
            raise (IRException (pos, NotImplemented "printf types"))

and build_types lenv tyl =
   List.map (build_type lenv) tyl

and build_immutable_types lenv tyl =
   List.map (fun ty -> build_type lenv ty, Immutable) tyl

and build_immutable_types_opt lenv tyl_opt =
   match tyl_opt with
      Some tyl -> Some (build_immutable_types lenv tyl)
    | None -> None

(************************************************************************
 * TYPE DEFINITIONS
 ************************************************************************)

(*
 * Build a normal quanitified type.
 *)
let build_lambda_tydef types lenv pos ty_var fo_vars ty =
   let pos = string_pos "build_lambda_tydef" pos in
   let ty = build_type lenv ty in
      SymbolTable.add types ty_var (TyDefLambda (fo_vars, ty))

(*
 * Build a union definition.
 *)
let build_union_tydef types lenv pos ty_var fo_vars fields =
   let pos = string_pos "build_union_tydef" pos in
   let fields = FieldTable.to_list fields in
   let enum_flag, n, s = lenv_lookup_union lenv pos ty_var in
      if enum_flag then
         SymbolTable.add types ty_var (TyDefLambda (fo_vars, TyEnum n))
      else
         let fields =
            List.map (fun (_, tyl) ->
                  List.map (fun ty -> ty, Immutable) (build_types lenv tyl)) fields
         in
            SymbolTable.add types ty_var (TyDefUnion (fo_vars, fields))

(*
 * Build a record definition.
 * This just turns into a tuple.
 *)
let build_record_tydef types lenv pos ty_var fo_vars fields =
   let pos = string_pos "build_record_tydef" pos in
   let fields = FieldTable.to_list fields in
   let tyl =
      List.map (fun (_, (b, ty)) ->
            let b =
               if b then
                  Mutable
               else
                  Immutable
            in
               build_type lenv ty, b) fields
   in
      SymbolTable.add types ty_var (TyDefLambda (fo_vars, TyTuple (NormalTuple, tyl)))

(*
 * Build a module definition.
 * This is also a tuple.
 *)
let build_module_tydef types lenv pos ty_var fo_vars names fields =
   let pos = string_pos "build_module_tydef" pos in
   let names = FieldTable.to_list names in
   let tyl =
      List.map (fun (_, v) ->
            let ty =
               try SymbolTable.find fields v with
                  Not_found ->
                     raise (IRException (pos, UnboundVar v))
            in
               build_type lenv ty, Immutable) names
   in
      SymbolTable.add types ty_var (TyDefLambda (fo_vars, TyTuple (NormalTuple, tyl)))

(*
 * Build a frame definition.
 * The labels are preserved.
 * The frames are separate from the types.
 *)
let build_frame_tydef frames lenv pos ty_var fo_vars fields =
   let pos = string_pos "build_frame_tydef" pos in
   let fields =
      FieldTable.fold (fun fields v (_, ty) ->
            let ty = build_type lenv ty in
               SymbolTable.add fields v [v, ty, 0]) SymbolTable.empty fields
   in
      SymbolTable.add frames ty_var (fo_vars, fields)

(*
 * Build a type definition.
 *)
let build_tydef types frames lenv ty_var tydef =
   let pos = string_pos "build_tydef" (var_exp_pos ty_var) in
   let { Aml_ir.tydef_fo_vars = fo_vars;
         Aml_ir.tydef_inner = tydef
       } = tydef
   in
      match Aml_ir_ds.dest_tydef_inner_core tydef with
         Aml_ir.TyDefLambda ty ->
            let types = build_lambda_tydef types lenv pos ty_var fo_vars ty in
               types, frames
       | Aml_ir.TyDefRecord fields ->
            let types = build_record_tydef types lenv pos ty_var fo_vars fields in
               types, frames
       | Aml_ir.TyDefFrame fields ->
            let frames = build_frame_tydef frames lenv pos ty_var fo_vars fields in
               types, frames
       | Aml_ir.TyDefUnion fields ->
            let types = build_union_tydef types lenv pos ty_var fo_vars fields in
               types, frames
       | Aml_ir.TyDefDTuple v ->
            let types = SymbolTable.add types ty_var (TyDefDTuple v) in
               types, frames
       | Aml_ir.TyDefModule (names, fields) ->
            let types = build_module_tydef types lenv pos ty_var fo_vars names fields in
               types, frames

(*
 * Build all the type definitions.
 *)
let build_tydefs lenv tydefs =
   SymbolTable.fold (fun (types, frames) ty_var tydef ->
         build_tydef types frames lenv ty_var tydef) (SymbolTable.empty, SymbolTable.empty) tydefs

(************************************************************************
 * ATOMS
 ************************************************************************)

(*
 * Subscripts.
 *)
let zero32 = Rawint.of_int Rawint.Int32 true 0

(*
 * Characters are int8.
 *)
let atom_char c =
   AtomRawInt (Rawint.of_int pre_char signed_char (Char.code c))

(*
 * Floats are float64.
 *)
let atom_float x =
   AtomFloat (Rawfloat.of_float pre_float x)

(*
 * String subop.
 *)
let string_subop =
   { sub_block = RawDataSub;
     sub_value = RawIntSub (pre_char, signed_char);
     sub_index = ByteIndex;
     sub_script = IntIndex
   }

(*
 * Other blocks use integers.
 *)
let word_subop =
   { sub_block  = BlockSub;
     sub_value  = BlockPointerSub;
     sub_index  = WordIndex;
     sub_script = IntIndex
   }

let byte_subop =
   { sub_block  = RawDataSub;
     sub_value  = BlockPointerSub;
     sub_index  = ByteIndex;
     sub_script = RawIntIndex (Rawint.Int32, true)
   }

let subop_of_subscript tenv loc ty a1 a2 =
   let pos = FirPos.string_pos "subop_of_subscript" (FirPos.loc_pos loc) in
   let subop =
      match a2 with
         AtomLabel _ ->
            byte_subop
       | _ ->
            word_subop
   in
      { subop with sub_value = sub_value_of_type_dir tenv pos ty }

(*
 * Unary operators.
 *)
let op_of_unop lenv pos op =
   let pos = string_pos "op_of_unop" pos in
      match op with
         Aml_ir.NotBoolOp     -> NotEnumOp 2

       | Aml_ir.UMinusIntOp   -> UMinusIntOp
       | Aml_ir.NotIntOp      -> NotIntOp
       | Aml_ir.AbsIntOp      -> AbsIntOp

         (* Floats *)
       | Aml_ir.UMinusFloatOp -> UMinusFloatOp pre_float
       | Aml_ir.AbsFloatOp    -> AbsFloatOp pre_float
       | Aml_ir.SqrtFloatOp   -> SqrtFloatOp pre_float
       | Aml_ir.ExpFloatOp    -> ExpFloatOp pre_float
       | Aml_ir.LogFloatOp    -> LogFloatOp pre_float
       | Aml_ir.Log10FloatOp  -> Log10FloatOp pre_float
       | Aml_ir.CosFloatOp    -> CosFloatOp pre_float
       | Aml_ir.SinFloatOp    -> SinFloatOp pre_float
       | Aml_ir.TanFloatOp    -> TanFloatOp pre_float
       | Aml_ir.ACosFloatOp   -> ACosFloatOp pre_float
       | Aml_ir.ASinFloatOp   -> ASinFloatOp pre_float
       | Aml_ir.ATanFloatOp   -> ATanFloatOp pre_float
       | Aml_ir.CosHFloatOp   -> CosHFloatOp pre_float
       | Aml_ir.SinHFloatOp   -> SinHFloatOp pre_float
       | Aml_ir.TanHFloatOp   -> TanHFloatOp pre_float
       | Aml_ir.CeilFloatOp   -> CeilFloatOp pre_float
       | Aml_ir.FloorFloatOp  -> FloorFloatOp pre_float

         (* Coercions *)
       | Aml_ir.CharOfIntOp  -> RawIntOfIntOp (pre_char, signed_char)
       | Aml_ir.IntOfCharOp  -> IntOfRawIntOp (pre_char, signed_char)
       | Aml_ir.IntOfFloatOp -> IntOfFloatOp  pre_float
       | Aml_ir.FloatOfIntOp -> FloatOfIntOp  pre_float

         (* Block operations *)
       | Aml_ir.LengthOfStringOp -> LengthOfBlockOp (string_subop, TyRawData)
       | Aml_ir.LengthOfBlockOp ty -> LengthOfBlockOp (word_subop, build_type lenv ty)

(*
 * Binary operators.
 *)
let op_of_binop lenv pos op =
   let pos = string_pos "op_of_binop" pos in
      match op with
         (* Non-short-circuit operations *)
         Aml_ir.OrBoolOp  -> OrEnumOp  2
       | Aml_ir.AndBoolOp -> AndEnumOp 2

         (* Comparisons on ML ints *)
       | Aml_ir.EqCharOp  -> EqRawIntOp  (pre_char, signed_char)
       | Aml_ir.NeqCharOp -> NeqRawIntOp (pre_char, signed_char)
       | Aml_ir.LtCharOp  -> LtRawIntOp  (pre_char, signed_char)
       | Aml_ir.LeCharOp  -> LeRawIntOp  (pre_char, signed_char)
       | Aml_ir.GtCharOp  -> GtRawIntOp  (pre_char, signed_char)
       | Aml_ir.GeCharOp  -> GeRawIntOp  (pre_char, signed_char)
       | Aml_ir.CmpCharOp -> CmpRawIntOp (pre_char, signed_char)

         (* Standard binary operations on ML ints *)
       | Aml_ir.PlusIntOp  -> PlusIntOp
       | Aml_ir.MinusIntOp -> MinusIntOp
       | Aml_ir.MulIntOp   -> MulIntOp
       | Aml_ir.DivIntOp   -> DivIntOp
       | Aml_ir.RemIntOp   -> RemIntOp
       | Aml_ir.LslIntOp   -> LslIntOp
       | Aml_ir.LsrIntOp   -> LsrIntOp
       | Aml_ir.AsrIntOp   -> AsrIntOp
       | Aml_ir.AndIntOp   -> AndIntOp
       | Aml_ir.OrIntOp    -> OrIntOp
       | Aml_ir.XorIntOp   -> XorIntOp
       | Aml_ir.MaxIntOp   -> MaxIntOp
       | Aml_ir.MinIntOp   -> MinIntOp

         (* Comparisons on ML ints *)
       | Aml_ir.EqIntOp  -> EqIntOp
       | Aml_ir.NeqIntOp -> NeqIntOp
       | Aml_ir.LtIntOp  -> LtIntOp
       | Aml_ir.LeIntOp  -> LeIntOp
       | Aml_ir.GtIntOp  -> GtIntOp
       | Aml_ir.GeIntOp  -> GeIntOp
       | Aml_ir.CmpIntOp -> CmpIntOp

         (* Standard binary operations on floats *)
       | Aml_ir.PlusFloatOp      -> PlusFloatOp pre_float
       | Aml_ir.MinusFloatOp     -> MinusFloatOp pre_float
       | Aml_ir.MulFloatOp       -> MulFloatOp pre_float
       | Aml_ir.DivFloatOp       -> DivFloatOp pre_float
       | Aml_ir.RemFloatOp       -> RemFloatOp pre_float
       | Aml_ir.MaxFloatOp       -> MaxFloatOp pre_float
       | Aml_ir.MinFloatOp       -> MinFloatOp pre_float
       | Aml_ir.PowerFloatOp     -> PowerFloatOp pre_float
       | Aml_ir.ATan2FloatOp     -> ATan2FloatOp pre_float
       | Aml_ir.LdExpFloatIntOp  -> LdExpFloatIntOp pre_float

         (* Comparisons on floats *)
       | Aml_ir.EqFloatOp  -> EqFloatOp pre_float
       | Aml_ir.NeqFloatOp -> NeqFloatOp pre_float
       | Aml_ir.LtFloatOp  -> LtFloatOp pre_float
       | Aml_ir.LeFloatOp  -> LeFloatOp pre_float
       | Aml_ir.GtFloatOp  -> GtFloatOp pre_float
       | Aml_ir.GeFloatOp  -> GeFloatOp pre_float
       | Aml_ir.CmpFloatOp -> CmpFloatOp pre_float

         (* Pointer (in)equality.  Arguments must have the given type *)
       | Aml_ir.EqEqOp ty -> EqEqOp (build_type lenv ty)
       | Aml_ir.NeqEqOp ty -> NeqEqOp (build_type lenv ty)

(*
 * Build an atom.
 *)
let rec build_atom lenv pos a =
   match a with
      Aml_ir.AtomInt i ->
         AtomInt i
    | Aml_ir.AtomChar c ->
         atom_char c
    | Aml_ir.AtomFloat x ->
         atom_float x
    | Aml_ir.AtomVar v ->
         AtomVar v
    | Aml_ir.AtomNil ty ->
         AtomNil (build_type lenv ty)
    | Aml_ir.AtomTyConstrain (a, _) ->
         (* We ignore the constraint, type inference will re-add it *)
         build_atom lenv pos a
    | Aml_ir.AtomTyApply (a, ty, tyl) ->
         AtomTyApply (build_atom lenv pos a, build_type lenv ty, build_types lenv tyl)
    | Aml_ir.AtomTyPack (v, ty, tyl) ->
         AtomTyPack (v, build_type lenv ty, build_types lenv tyl)
    | Aml_ir.AtomTyUnpack v ->
         AtomTyUnpack v
    | Aml_ir.AtomConst (ty, ty_var, const_var) ->
         let enum_flag, n, _ = lenv_lookup_union lenv pos ty_var in
         let i = lenv_lookup_const lenv pos const_var in
            if enum_flag then
               AtomEnum (n, i)
            else
               AtomConst (build_type lenv ty, ty_var, i)
    | Aml_ir.AtomFrameLabel (ty_var, label_var) ->
         AtomLabel ((ty_var, label_var, label_var), zero32)
    | Aml_ir.AtomRecordLabel (ty_var, label_var) ->
         AtomInt (lenv_lookup_label lenv pos label_var)
    | Aml_ir.AtomModuleLabel (ty_var, ext_var) ->
         AtomInt (lenv_lookup_val lenv pos ty_var ext_var)
    | Aml_ir.AtomUnop (op, a) ->
         AtomUnop (op_of_unop lenv pos op, build_atom lenv pos a)
    | Aml_ir.AtomBinop (op, a1, a2) ->
         AtomBinop (op_of_binop lenv pos op, build_atom lenv pos a1, build_atom lenv pos a2)

let build_atom_opt lenv pos a_opt =
   match a_opt with
      Some a -> Some (build_atom lenv pos a)
    | None -> None

let build_atoms lenv pos atoms =
   List.map (build_atom lenv pos) atoms

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Allocation operators.
 *)
let build_alloc_op lenv pos op =
   let pos = string_pos "build_alloc_op" pos in
      match op with
         Aml_ir.AllocTuple (ty_vars, ty, args) ->
            AllocTuple (NormalTuple, ty_vars, build_type lenv ty, build_atoms lenv pos args)
       | Aml_ir.AllocUnion (ty_vars, ty, ty_var, const_var, args) ->
            let i = lenv_lookup_const lenv pos const_var in
               AllocUnion (ty_vars, build_type lenv ty, ty_var, i, build_atoms lenv pos args)
       | Aml_ir.AllocDTuple (ty, ty_var, a, args) ->
            let ty = build_type lenv ty in
            let a = build_atom lenv pos a in
            let args = build_atoms lenv pos args in
               AllocDTuple (ty, ty_var, a, args)
       | Aml_ir.AllocRecord (ty_vars, ty, ty_var, fields) ->
            (* Reorder the fields *)
            let labels = lenv_lookup_record lenv pos ty_var in
            let fields = reorder_list pos labels fields in
            let fields = build_atoms lenv pos fields in
               AllocTuple (NormalTuple, ty_vars, build_type lenv ty, fields)
       | Aml_ir.AllocArray (ty, args) ->
            AllocArray (build_type lenv ty, build_atoms lenv pos args)
       | Aml_ir.AllocVArray (ty, a1, a2) ->
            AllocVArray (build_type lenv ty, WordIndex, build_atom lenv pos a1, build_atom lenv pos a2)
       | Aml_ir.AllocModule (ty, ty_var, fields) ->
            (* Reorder the fields *)
            let labels = lenv_lookup_module lenv pos ty_var in
            let fields = reorder_table pos labels fields in
            let fields = build_atoms lenv pos fields in
               AllocTuple (NormalTuple, [], build_type lenv ty, fields)
       | Aml_ir.AllocFrame (_, ty_var, tyl) ->
            AllocFrame (ty_var, build_types lenv tyl)

(*
 * Build a match set.
 *)
let build_set lenv pos s =
   let pos = string_pos "build_set" pos in
      match s with
         Aml_ir.CharSet s ->
            let s =
               Aml_set.CharSet.fold (fun s left right ->
                     let coerce_char = function
                        Infinity -> Infinity
                      | Open c -> Open (Rawint.of_int pre_char signed_char (Char.code c))
                      | Closed c -> Closed (Rawint.of_int pre_char signed_char (Char.code c))
                     in
                     let left = coerce_char left in
                     let right = coerce_char right in
                     let i = RawIntSet.of_interval pre_char signed_char left right in
                        RawIntSet.union s i) (RawIntSet.empty pre_char signed_char) s
            in
               RawIntSet s
       | Aml_ir.IntSet s ->
            let s =
               Aml_set.IntSet.fold (fun s left right ->
                     let i = IntSet.of_interval left right in
                        IntSet.union s i) IntSet.empty s
            in
               IntSet s
       | Aml_ir.ConstSet (ty_var, s) ->
            let s =
               SymbolSet.fold (fun s const_var ->
                     let i = lenv_lookup_const lenv pos const_var in
                        IntSet.add_point s i) IntSet.empty s
            in
               IntSet s

(*
 * Now build the expression.
 *)
let rec build_exp tenv lenv e =
   let pos = string_pos "build_exp" (exp_pos e) in
   let loc = Aml_ir_ds.loc_of_exp e in
      match Aml_ir_ds.dest_exp_core e with
         Aml_ir.LetAtom (v, ty, a, e) ->
            let ty = build_type lenv ty in
            let a = build_atom lenv pos a in
            let e = build_exp tenv lenv e in
               make_exp loc (LetAtom (v, ty, a, e))
       | Aml_ir.LetExt (v, ty1, s, ty2, ty_args, args, e) ->
            let ty1 = build_type lenv ty1 in
            let ty2 = build_type lenv ty2 in
            let ty_args = build_types lenv ty_args in
            let args = build_atoms lenv pos args in
            let e = build_exp tenv lenv e in
            let s = "aml_" ^ s in
               make_exp loc (LetExt (v, ty1, s, false, ty2, ty_args, args, e))
       | Aml_ir.TailCall (f, args) ->
            let label = new_symbol_string "aml_tailcall" in
            let f = build_atom lenv pos f in
            let args = build_atoms lenv pos args in
               make_exp loc (TailCall (label, f, args))
       | Aml_ir.Match (a, cases) ->
            let a = build_atom lenv pos a in
            let cases =
               List.map (fun (s, e) ->
                     let label = new_symbol_string "aml_case" in
                     let s = build_set lenv pos s in
                     let e = build_exp tenv lenv e in
                        label, s, e) cases
            in
               make_exp loc (Match (a, cases))
       | Aml_ir.MatchDTuple (a, cases) ->
            let a = build_atom lenv pos a in
            let cases =
               List.map (fun (a_opt, e) ->
                     let label = new_symbol_string "aml_dtuple_case" in
                     let a_opt = build_atom_opt lenv pos a_opt in
                     let e = build_exp tenv lenv e in
                        label, a_opt, e) cases
            in
               make_exp loc (MatchDTuple (a, cases))
       | Aml_ir.LetAlloc (v, Aml_ir.AllocUnion (_, _, ty_var, const_var, _), e)
         when lenv_union_is_enum lenv pos ty_var ->
            (* Generate special code for enumerations *)
            let e = build_exp tenv lenv e in
            let _, n, _ = lenv_lookup_union lenv pos ty_var in
            let i = lenv_lookup_const lenv pos const_var in
               make_exp loc (LetAtom (v, TyEnum n, AtomEnum (n, i), e))
       | Aml_ir.LetAlloc (v, op, e) ->
            let op = build_alloc_op lenv pos op in
            let e = build_exp tenv lenv e in
               make_exp loc (LetAlloc (v, op, e))
       | Aml_ir.SetSubscript (a1, a2, a3, ty, e) ->
            let label = new_symbol_string "aml_setsub" in
            let a1 = build_atom lenv pos a1 in
            let a2 = build_atom lenv pos a2 in
            let a3 = build_atom lenv pos a3 in
            let ty = build_type lenv ty in
            let e = build_exp tenv lenv e in
            let subop = subop_of_subscript tenv loc ty a1 a2 in
               make_exp loc (SetSubscript (subop, label, a1, a2, ty, a3, e))
       | Aml_ir.LetSubscript (v, ty, a1, a2, e) ->
            let ty = build_type lenv ty in
            let a1 = build_atom lenv pos a1 in
            let a2 = build_atom lenv pos a2 in
            let e = build_exp tenv lenv e in
            let subop = subop_of_subscript tenv loc ty a1 a2 in
               make_exp loc (LetSubscript (subop, v, ty, a1, a2, e))
       | Aml_ir.SetGlobal (v, ty, a, e) ->
            let label = new_symbol_string "aml_setglob" in
            let ty = build_type lenv ty in
            let a = build_atom lenv pos a in
            let e = build_exp tenv lenv e in
            let fir_pos = FirPos.type_exp_pos loc ty in
            let op = Fir_type.sub_value_of_type_dir tenv fir_pos ty in
               make_exp loc (SetGlobal (op, label, v, ty, a, e))
       | Aml_ir.LetRec _ ->
            raise (IRException (pos, NotImplemented "recursive definitions"))
       | Aml_ir.LetFuns _
       | Aml_ir.LetClosure _
       | Aml_ir.LetApply _
       | Aml_ir.Return _
       | Aml_ir.Try _
       | Aml_ir.Raise _ ->
            raise (IRException (pos, StringError "unexpected expression"))

(*
 * Build a function definition.
 *)
let build_fundef tenv lenv f (loc, _, ty_vars, ty, vars, e) =
   let pos = string_pos "build_fundef" (var_exp_pos f) in
   let ty = build_type lenv ty in
   let e = build_exp tenv lenv e in
      (loc, ty_vars, ty, vars, e)

(*
 * Build and initializer.
 *)
let build_init lenv (ty, init) =
   let loc = Aml_ir_ds.loc_of_type ty in
   let pos = string_pos "build_init" (loc_pos loc) in
   let ty = build_type lenv ty in
   let init =
      match init with
         Aml_ir.InitString s ->
            let len = String.length s in
            let a = Array.make len 0 in
               for i = 0 to pred len do
                  a.(i) <- Char.code s.[i]
               done;
               InitRawData (pre_char, a)
       | Aml_ir.InitAtom a ->
            InitAtom (build_atom lenv pos a)
       | Aml_ir.InitAlloc op ->
            InitAlloc (build_alloc_op lenv pos op)
   in
      ty, init

(*
 * Now convert the program.
 *)
let build_prog prog =
   let { Aml_ir.prog_name = name;
         Aml_ir.prog_types = types;
         Aml_ir.prog_globals = globals;
         Aml_ir.prog_tags = tags;
         Aml_ir.prog_import = import;
         Aml_ir.prog_export = export;
         Aml_ir.prog_funs = funs
       } = prog
   in

   (* Compute the environment *)
   let lenv = build_lenv prog in

   (* File name info *)
   let file_info =
      { file_dir = ".";
        file_name = Symbol.to_string name;
        file_class = FileAml
      }
   in

   (* Build the imports *)
   let import =
      SymbolTable.mapi (fun v { Aml_ir.import_type = ty; Aml_ir.import_info = info } ->
            { import_name = Symbol.to_string v;
              import_type = build_type lenv ty;
              import_info = info
            }) import
   in

   (* Build the exports *)
   let export =
      SymbolTable.map (fun { Aml_ir.export_name = name;
                             Aml_ir.export_type = ty
                           } ->
            { export_name = Symbol.to_string name;
              export_type = build_type lenv ty
            }) export
   in

   (* Build the type definitions *)
   let types, frames = build_tydefs lenv types in

   (* Build the globals *)
   let globals = SymbolTable.map (build_init lenv) globals in

   (* Allocate tags *)
   let globals =
      SymbolTable.fold (fun globals v (ty_var, tyl) ->
            let tyl = build_immutable_types lenv tyl in
            let ty = TyTag (ty_var, tyl) in
               SymbolTable.add globals v (ty, InitTag (ty_var, tyl))) globals tags
   in

   (* Build the functions *)
   let funs = SymbolTable.mapi (build_fundef types lenv) funs in

   (* Redefine the frame field sizes *)
   let frames =
      SymbolTable.mapi (fun v (vars, fields) ->
            let pos = FirPos.string_pos "sizeof_type" (FirPos.var_exp_pos v) in
            let fields =
               SymbolTable.map (fun subfields ->
                     List.map (fun (v, ty, _) ->
                           let size = sizeof_type_dir types pos 0 ty in
                              v, ty, size) subfields) fields
            in
               vars, fields) frames
   in

   (* Put it all together and standardize *)
   let prog =
      { prog_file    = file_info;
        prog_import  = import;
        prog_export  = export;
        prog_types   = types;
        prog_frames  = frames;
        prog_names   = SymbolTable.empty;
        prog_globals = globals;
        prog_funs    = funs
      }
   in
   let _ =
      if !debug_print_fir > 0 then
         debug_prog "AML final" prog
   in
      standardize_prog prog

(*!
 * @Docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
