(*
 * Compile mc files to FC parse trees.
 * ----------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *
 *)
open Format

open Debug
open Location

open Fc_parse_type
open Fc_parse_print
open Fc_frontends

let compile name =
   (* Parse the file *)
   let inx = open_in name in
   let pos = create_loc (Symbol.add name) 0 0 0 0 in
   let prog =
      Fc_parse_state.set_current_position pos;
      try
         let lex = Lexing.from_channel inx in
         let prog = Mc_parser.program Mc_lexer.main lex in
            close_in inx;
            SeqExpr (pos, prog)
      with
         exn ->
            close_in inx;
            raise exn
   in
   let prog = Pasqual_pascal_compat.compat_expr prog false in
   let _ =
      if debug Fir_state.debug_print_ast then
         pp_print_debug err_formatter "parse" prog;
   in
   let prog = Fc_parse_standardize.standardize_expr prog in
      if debug Fir_state.debug_print_ast then
         pp_print_debug err_formatter "parse standard" prog;
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
