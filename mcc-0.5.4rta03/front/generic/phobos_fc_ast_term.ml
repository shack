(*
 * The FC AST Termset.
 * Convert MetaPRL terms to FC AST and FIR
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

open Phobos_constants
open Phobos_parse_state
open Phobos_exn
open Refiner.Refiner.TermType
open Refiner.Refiner.Term
open Refiner.Refiner.Rewrite
open Opname
open Mp_num
open Format

let fc_module_name = "Fc_ast"
let string_param = String ""
let int_param = Number (num_of_int 0)
let funcall_sym = Symbol.add "()"
let subscript_sym = Symbol.add "[]"

let string_of_opname opname =
   let s = string_of_opname opname in
   let last_parent, rest =
      try
         let index = String.index s '!' in
            String.sub s 0 index, String.sub s (index+1) (String.length s - index - 1)
      with
         Not_found ->
            s, s
   in
   if last_parent = fc_module_name then
      rest
   else
      s

let rec int_array_of_string l s =
   if s <> "" then
      Char.code s.[0] :: int_array_of_string l (String.sub s 1 (String.length s-1))
   else
      l

let int_array_of_string s =
   Array.of_list (int_array_of_string [] s)

let check_param loc term param param' =
   let loc = loc_of_string loc term "check_param" in
   match param, param' with
      Number _, Number _
    | String _, String _
    | Var _, Var _ ->
         ()
    | _ ->
         raise (ConvertException (loc, ParamError2 (param, param', "parameter type mismatch")))

let string_of_param loc = function
   String s ->
      s
 | param ->
      raise (ConvertException (loc, ParamError (param, "string parameter expected")))

let integer_of_param loc = function
   Number num as param ->
      if not (is_integer_num num) then
         raise (ConvertException (loc, ParamError (param, "not an integer param")));
      int_of_num num
 | param ->
      raise (ConvertException (loc, ParamError (param, "number parameter expected")))

let check_params loc opname bterm params params' =
   let loc = loc_of_string loc bterm "check_params" in
   let len1 = List.length params' in
   let len2 = List.length params in
   if len1 <> len2 then
      raise (ConvertException (loc, TermError (bterm, opname, (**)
         Printf.sprintf "parameter arity mismatch, should be %d (not %d)" len1 len2)));
   List.iter2 (check_param loc bterm) params params';
   params

let check_subterms_arity loc opname bterm subterms num =
   let loc = loc_of_string loc bterm "check_subterms_arity" in
   if List.length subterms <> num then
      raise (ConvertException (loc, TermError (bterm, opname, (**)
         Printf.sprintf "subterm arity mismatch, should be %d (not %d)" num (List.length subterms))));
   subterms

let rec build_list loc elt_builder term =
   let loc = loc_of_string loc term "build_list" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "list" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
               build_list_aux loc elt_builder (List.nth subterms 0)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a list")))

and build_option loc elt_builder term =
   let loc = loc_of_string loc term "build_option" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "option" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
               build_option_aux loc elt_builder (List.nth subterms 0)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an option")))

and build_tuple2 loc elt_builder1 elt_builder2 term =
   let loc = loc_of_string loc term "build_tuple2" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "option" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let t1 = elt_builder1 loc (List.nth subterms 0) in
            let t2 = elt_builder2 loc (List.nth subterms 1) in
               t1, t2
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a 2-tuple")))

and build_list_aux loc elt_builder term =
   let loc = loc_of_string loc term "build_list_aux" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "list_create" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let expr = elt_builder loc (List.nth subterms 0) in
               [expr]
       | "list_create2" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let expr1 = elt_builder loc (List.nth subterms 0) in
            let expr2 = elt_builder loc (List.nth subterms 1) in
               expr1 :: [expr2]
       | "list_create3" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let expr1 = elt_builder loc (List.nth subterms 0) in
            let expr2 = elt_builder loc (List.nth subterms 1) in
	    let expr3 = elt_builder loc (List.nth subterms 2) in
               expr1 :: expr2 :: [expr3]
       | "list_create4" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 4 in
            let expr1 = elt_builder loc (List.nth subterms 0) in
            let expr2 = elt_builder loc (List.nth subterms 1) in
            let expr3 = elt_builder loc (List.nth subterms 2) in
	    let expr4 = elt_builder loc (List.nth subterms 3) in
               expr1 :: expr2 :: expr3 :: [expr4]
       | "list_create5" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 5 in
            let expr1 = elt_builder loc (List.nth subterms 0) in
            let expr2 = elt_builder loc (List.nth subterms 1) in
            let expr3 = elt_builder loc (List.nth subterms 2) in
	    let expr4 = elt_builder loc (List.nth subterms 3) in
            let expr5 = elt_builder loc (List.nth subterms 4) in
               expr1 :: expr2 :: expr3 :: expr4 :: [expr5]
       | "list_append" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let lst = build_list loc elt_builder (List.nth subterms 0) in
            let expr = elt_builder loc (List.nth subterms 1) in
               lst @ [expr]
       | "list_insert" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let lst = build_list loc elt_builder (List.nth subterms 0) in
            let expr = elt_builder loc (List.nth subterms 1) in
               expr :: lst
       | "list_merge" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let lst1 = build_list loc elt_builder (List.nth subterms 0) in
            let lst2 = build_list loc elt_builder (List.nth subterms 1) in
               lst1 @ lst2
       | "list_reverse" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let lst = build_list loc elt_builder (List.nth subterms 0) in
               List.rev lst 
       | "empty_list" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               [] 
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a list kind")))

and build_option_aux loc elt_builder term =
   let loc = loc_of_string loc term "build_option_aux" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "Some" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let expr = elt_builder loc (List.nth subterms 0) in
               Some expr
       | "None" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               None
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an option kind")))

let rec build_pos loc term =
   let loc = loc_of_string loc term "build_pos" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "__pos__" ->
            let params = check_params loc opname term params [string_param; int_param; int_param; int_param; int_param] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
            let filename = string_of_param loc (List.nth params 0) in
            let lpos1 = integer_of_param loc (List.nth params 1) in
            let lpos2 = integer_of_param loc (List.nth params 2) in
            let rpos1 = integer_of_param loc (List.nth params 3) in
            let rpos2 = integer_of_param loc (List.nth params 4) in
               Location.create_loc (Symbol.add filename) lpos1 lpos2 rpos1 rpos2
       | "union_pos" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let pos1 = build_pos loc (List.nth subterms 0) in
            let pos2 = build_pos loc (List.nth subterms 1) in
               Location.union_loc pos1 pos2
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a position")))

and build_error term =
   let loc = loc_start (mk_bterm [] term) "build_error" in
   let opname, params, subterms = breakup_term term in
   let opname = string_of_opname opname in
   let term = mk_bterm [] term in
      match opname with
         "error" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let s = string_of_param loc (List.nth params 0) in
            let pos = build_pos loc (List.nth subterms 0) in
               s, pos
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an error term")))

and build_op_class loc term =
   let loc = loc_of_string loc term "build_op_class" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "PreOp" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.PreOp
       | "PostOp" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.PreOp
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an op-class")))

and build_boolean loc term =
   let loc = loc_of_string loc term "build_boolean" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "true" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               true
       | "false" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               false
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a boolean")))

and build_int_precision loc term =
   let loc = loc_of_string loc term "build_int_precision" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "Int8" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawint.Int8
       | "Int16" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawint.Int16
       | "Int32" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawint.Int32
       | "Int64" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawint.Int64
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an int_precision")))

and build_float_precision loc term =
   let loc = loc_of_string loc term "build_int_precision" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "Single" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawfloat.Single
       | "Double" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawfloat.Double
       | "LongDouble" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Rawfloat.LongDouble
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a float_precision")))

and build_int loc term =
   let loc = loc_of_string loc term "build_int" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "int" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               int_of_string (string_of_param loc (List.nth params 0))
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an integer")))

and build_float loc term =
   let loc = loc_of_string loc term "build_float" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "float" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               float_of_string (string_of_param loc (List.nth params 0))
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a float")))

and build_rawint loc term =
   let loc = loc_of_string loc term "build_rawint" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "rawint" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let ri_prec = build_int_precision loc (List.nth subterms 0) in
            let ri_signed = build_boolean loc (List.nth subterms 1) in
            let i = build_int loc (List.nth subterms 2) in
               Rawint.of_int ri_prec ri_signed i
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a rawint")))

and build_rawfloat loc term =
   let loc = loc_of_string loc term "build_rawfloat" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "rawfloat" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let rf_prec = build_float_precision loc (List.nth subterms 0) in
            let f = build_float loc (List.nth subterms 1) in
               Rawfloat.of_float rf_prec f
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a rawfloat")))

and build_enum_decl loc term =
   let loc = loc_of_string loc term "build_enum_decl" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "enum_decl" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let pos = build_pos loc (List.nth subterms 0) in
            let sym, sympos = build_symbol loc (List.nth subterms 1) in
            let opt_expr = build_expr_option loc (List.nth subterms 2) in
               pos, sym, opt_expr
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an enum_decl")))

and build_union_enum_decl loc term =
   let loc = loc_of_string loc term "build_union_enum_decl" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "union_enum_decl" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let pos = build_pos loc (List.nth subterms 0) in
            let sym, sympos = build_symbol loc (List.nth subterms 1) in
            let opt_ty = build_type_option loc (List.nth subterms 2) in
               pos, sym, opt_ty
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a union_enum_decl")))

and build_field_decl loc term =
   let loc = loc_of_string loc term "build_field_decl" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "field_decl" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 4 in
            let pos = build_pos loc (List.nth subterms 0) in
            let sym, sympos = build_symbol loc (List.nth subterms 1) in
            let ty = build_type loc (List.nth subterms 2) in
            let opt_expr = build_expr_option loc (List.nth subterms 3) in
               pos, sym, ty, opt_expr
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a field_decl")))

and build_patt_decl loc term =
   let loc = loc_of_string loc term "build_patt_decl" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "patt_decl" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let pos = build_pos loc (List.nth subterms 0) in
            let patt = build_pattern loc (List.nth subterms 1) in
            let ty = build_type loc (List.nth subterms 2) in
               pos, (*Fc_parse_type.VarPattern (sympos, sym, sym, None)*)patt, ty
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a patt_decl")))

and build_var_decl loc term =
   let loc = loc_of_string loc term "build_var_decl" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "var_decl" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 5 in
            let pos = build_pos loc (List.nth subterms 0) in
            let storage_class = build_storage_class loc (List.nth subterms 1) in
            let patt = build_pattern loc (List.nth subterms 2) in
            let ty = build_type loc (List.nth subterms 3) in
            let init_expr = build_init_expr loc (List.nth subterms 4) in
               pos, storage_class, (*Fc_parse_type.VarPattern (sympos, sym, sym, None)*)patt, ty, init_expr
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a var_decl")))

and build_type_status loc term =
   let loc = loc_of_string loc term "build_type_status" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "StatusConst" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StatusConst
       | "StatusVolatile" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StatusVolatile
       | "StatusNormal" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StatusNormal
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a type_status")))

and build_storage_class loc term =
   let loc = loc_of_string loc term "build_storage_class" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "StoreAuto" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StoreAuto
       | "StoreRegister" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StoreRegister
       | "StoreStatic" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StoreStatic
       | "StoreExtern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_config.StoreExtern
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a storage class")))

and build_init_expr loc term =
   let loc = loc_of_string loc term "build_init_expr" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "InitNone" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.InitNone
       | "InitExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let pos = build_pos loc (List.nth subterms 0) in
            let expr = build_expr loc (List.nth subterms 1) in
               Fc_parse_type.InitExpr (pos, expr)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an init_expr")))
(*
and build_try_type loc term =
   let loc = loc_of_string loc term "build_try_type" in
   let opname, params, subterms = breakup_bterm term in
      match opname with
         "TryNormal" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.TryNormal
       | "TryAtomic" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.TryAtomic
       | _ ->
            raise (ConvertException (loc, TermError (term, "not a try_type")))
*)
and build_ty loc pos term =
   let loc = loc_of_string loc term "build_ty" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "TypeUnit" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let i = build_int loc (List.nth subterms 1) in
               Fc_parse_type.TypeUnit (pos, ty_status, i)
       | "TypePoly" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
               Fc_parse_type.TypePoly (pos, ty_status)
       | "TypeChar" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let int_prec = build_int_precision loc (List.nth subterms 1) in
            let b = build_boolean loc (List.nth subterms 2) in
               Fc_parse_type.TypeChar (pos, ty_status, int_prec, b)
       | "TypeInt" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let int_prec = build_int_precision loc (List.nth subterms 1) in
            let b = build_boolean loc (List.nth subterms 2) in
               Fc_parse_type.TypeInt (pos, ty_status, int_prec, b)
       | "TypeFloat" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let float_prec = build_float_precision loc (List.nth subterms 1) in
               Fc_parse_type.TypeFloat (pos, ty_status, float_prec)
       | "TypeArray" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 4 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty = build_type loc (List.nth subterms 1) in
            let e1 = build_expr loc (List.nth subterms 2) in
            let e2 = build_expr loc (List.nth subterms 3) in
               Fc_parse_type.TypeArray (pos, ty_status, ty, e1, e2)
       | "TypeConfArray" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 5 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty1 = build_type loc (List.nth subterms 1) in
            let sym1, _ = build_symbol loc (List.nth subterms 2) in
            let sym2, _ = build_symbol loc (List.nth subterms 3) in
            let ty2 = build_type loc (List.nth subterms 4) in
               Fc_parse_type.TypeConfArray (pos, ty_status, ty1, sym1, sym2, ty2)
       | "TypePointer" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty = build_type loc (List.nth subterms 1) in
               Fc_parse_type.TypePointer (pos, ty_status, ty)
       | "TypeRef" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty = build_type loc (List.nth subterms 1) in
               Fc_parse_type.TypeRef (pos, ty_status, ty)
       | "TypeProduct" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty_list = build_type_list loc (List.nth subterms 1) in
               Fc_parse_type.TypeProduct (pos, ty_status, ty_list)
      (* MISSING: TypeEnum, TypeUEnum, TypeStruct, TypeUnion *)
       | "TypeFun" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty_args = build_type_list loc (List.nth subterms 1) in
            let ty_res = build_type loc (List.nth subterms 2) in
               Fc_parse_type.TypeFun (pos, ty_status, ty_args, ty_res)
       | "TypeVar" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let ty_var, _ = build_symbol loc (List.nth subterms 1) in
               Fc_parse_type.TypeVar (pos, ty_status, ty_var)
       | "TypeLambda" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let sym_list = build_symbol_list loc (List.nth subterms 1) in
            let sym_list = List.map (fun (sym, _) -> sym) sym_list in
            let ty = build_type loc (List.nth subterms 2) in
               Fc_parse_type.TypeLambda (pos, ty_status, sym_list, ty)
       | "TypeApply" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let ty_status = build_type_status loc (List.nth subterms 0) in
            let sym, _ = build_symbol loc (List.nth subterms 1) in
            let ty_list = build_type_list loc (List.nth subterms 2) in
               Fc_parse_type.TypeApply (pos, ty_status, sym, ty_list)
       | "TypeElide" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.TypeElide pos
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a ty kind")))

and build_type loc term =
   let loc = loc_of_string loc term "build_type" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "ty" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let pos = build_pos loc (List.nth subterms 1) in
               build_ty loc pos (List.nth subterms 0)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a ty")))

and build_pattern_aux loc pos term =
   let loc = loc_of_string loc term "build_pattern_aux" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "CharPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let ri = build_rawint loc (List.nth subterms 0) in
               Fc_parse_type.CharPattern (pos, ri)
       | "IntPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let ri = build_rawint loc (List.nth subterms 0) in
               Fc_parse_type.IntPattern (pos, ri)
       | "FloatPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let rf = build_rawfloat loc (List.nth subterms 0) in
               Fc_parse_type.FloatPattern (pos, rf)
       | "StringPattern" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
            let s = string_of_param loc (List.nth params 0) in
               Fc_parse_type.StringPattern (pos, Fc_config.NormalPrecision, int_array_of_string s)
       | "VarPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let sym1, _ = build_symbol loc (List.nth subterms 0) in
            let sym2, _ = build_symbol loc (List.nth subterms 1) in
            let opt_ty = build_type_option loc (List.nth subterms 2) in
               Fc_parse_type.VarPattern (pos, sym1, sym2, opt_ty)
       | "StructPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let fields = build_sym_opt_pattern_list loc (List.nth subterms 0) in
               Fc_parse_type.StructPattern (pos, fields)
       | "EnumPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let sym, _ = build_symbol loc (List.nth subterms 0) in
            let patt = build_pattern loc (List.nth subterms 1) in
               Fc_parse_type.EnumPattern (pos, sym, patt)
       | "AsPattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let patt1 = build_pattern loc (List.nth subterms 0) in
            let patt2 = build_pattern loc (List.nth subterms 1) in
               Fc_parse_type.AsPattern (pos, patt1, patt2)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a pattern kind")))

and build_pattern loc term =
   let loc = loc_of_string loc term "build_pattern" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "pattern" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let pos = build_pos loc (List.nth subterms 1) in
               build_pattern_aux loc pos (List.nth subterms 0)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a pattern")))

and build_symbol loc term =
   let loc = loc_of_string loc term "build_symbol" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "symbol" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let s = string_of_param loc (List.nth params 0) in
            let pos = build_pos loc (List.nth subterms 0) in
               Symbol.add s, pos
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not a symbol")))

and build_expr loc term =
   let loc = loc_of_string loc term "build_expr" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "expr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let pos = build_pos loc (List.nth subterms 1) in
               build_expr_aux loc pos (List.nth subterms 0)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an expr")))

and build_expr_aux loc pos term =
   let loc = loc_of_string loc term "build_expr_aux" in
   let opname, params, subterms = breakup_bterm term in
   let opname = string_of_opname opname in
      match opname with
         "UnitExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let i1 = build_int loc (List.nth subterms 0) in
            let i2 = build_int loc (List.nth subterms 1) in
               Fc_parse_type.UnitExpr (pos, i1, i2)
       | "CharExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let ri = build_rawint loc (List.nth subterms 0) in
               Fc_parse_type.CharExpr (pos, ri)
       | "IntExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let ri = build_rawint loc (List.nth subterms 0) in
               Fc_parse_type.IntExpr (pos, ri)
       | "FloatExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let rf = build_rawfloat loc (List.nth subterms 0) in
               Fc_parse_type.FloatExpr (pos, rf)
       | "StringExpr" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
            let s = string_of_param loc (List.nth params 0) in
            let ia = int_array_of_string s in
               Fc_parse_type.StringExpr (pos, Fc_config.NormalPrecision, ia)
       | "VarExpr" ->
            let params = check_params loc opname term params [string_param] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
            let sym = Symbol.add (string_of_param loc (List.nth params 0)) in
               Fc_parse_type.VarExpr (pos, sym, sym)
       | "OpExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 4 in
            let op_class = build_op_class loc (List.nth subterms 0) in
            let sym1, _ = build_symbol loc (List.nth subterms 1) in
            let sym2, _ = build_symbol loc (List.nth subterms 2) in
            let expr_list = build_expr_list loc (List.nth subterms 3) in
               Fc_parse_type.OpExpr (pos, op_class, sym1, sym2, expr_list)
       | "FunCallExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let fun_expr = build_expr loc (List.nth subterms 0) in
            let expr_list = build_expr_list loc (List.nth subterms 1) in
               Fc_parse_type.OpExpr (pos, Fc_parse_type.PreOp, funcall_sym, funcall_sym, fun_expr :: fun_expr :: expr_list)
       | "SubscriptExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let expr1 = build_expr loc (List.nth subterms 0) in
            let expr2 = build_expr loc (List.nth subterms 1) in
               Fc_parse_type.OpExpr (pos, Fc_parse_type.PreOp, subscript_sym, subscript_sym,  expr1 :: [expr2])
       | "ProjectExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let expr = build_expr loc (List.nth subterms 0) in
            let sym, _ = build_symbol loc (List.nth subterms 1) in
               Fc_parse_type.ProjectExpr (pos, expr, sym)
       | "SizeofExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let expr = build_expr loc (List.nth subterms 0) in
               Fc_parse_type.SizeofExpr (pos, expr)
       | "SizeofType" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let ty = build_type loc (List.nth subterms 0) in
               Fc_parse_type.SizeofType (pos, ty)
       | "CastExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let ty = build_type loc (List.nth subterms 0) in
            let expr = build_expr loc (List.nth subterms 1) in
               Fc_parse_type.CastExpr (pos, ty, expr)
       | "IfExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
            let expr1 = build_expr loc (List.nth subterms 0) in
            let expr2 = build_expr loc (List.nth subterms 1) in
            let opt_expr = build_expr_option loc (List.nth subterms 2) in
               Fc_parse_type.IfExpr (pos, expr1, expr2, opt_expr)
       | "ForExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 4 in
            let expr1 = build_expr loc (List.nth subterms 0) in
            let expr2 = build_expr loc (List.nth subterms 1) in
            let expr3 = build_expr loc (List.nth subterms 2) in
            let expr4 = build_expr loc (List.nth subterms 3) in
               Fc_parse_type.ForExpr (pos, expr1, expr2, expr3, expr4)
       | "WhileExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let expr1 = build_expr loc (List.nth subterms 0) in
            let expr2 = build_expr loc (List.nth subterms 1) in
               Fc_parse_type.WhileExpr (pos, expr1, expr2)
       | "DoExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let expr1 = build_expr loc (List.nth subterms 0) in
            let expr2 = build_expr loc (List.nth subterms 1) in
               Fc_parse_type.DoExpr (pos, expr1, expr2)
       | "TryExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 3 in
(*            let tty = build_try_type loc (List.nth subterms 0) in*)
            let expr = build_expr loc (List.nth subterms 0) in
            let matches = build_match_list loc (List.nth subterms 1) in
            let opt_expr = build_expr_option loc (List.nth subterms 2) in
               Fc_parse_type.TryExpr (pos, expr, matches, opt_expr)
       | "SwitchExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 2 in
            let expr = build_expr loc (List.nth subterms 0) in
            let matches = build_match_list loc (List.nth subterms 1) in
               Fc_parse_type.SwitchExpr (pos, expr, matches)
       | "LabelExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let sym, _ = build_symbol loc (List.nth subterms 0) in
               Fc_parse_type.LabelExpr (pos, sym)
       | "CaseExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let patt = build_pattern loc (List.nth subterms 0) in
               Fc_parse_type.CaseExpr (pos, patt)
       | "DefaultExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.DefaultExpr pos
       | "ReturnExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let expr = build_expr loc (List.nth subterms 0) in
               Fc_parse_type.ReturnExpr (pos, expr)
       | "RaiseExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let expr = build_expr loc (List.nth subterms 0) in
               Fc_parse_type.RaiseExpr (pos, expr)
       | "BreakExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.BreakExpr pos
       | "ContinueExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 0 in
               Fc_parse_type.ContinueExpr pos
       | "GotoExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let sym, _ = build_symbol loc (List.nth subterms 0) in
               Fc_parse_type.GotoExpr (pos, sym)
       | "SeqExpr" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let expr_lst = build_expr_list loc (List.nth subterms 0) in
               Fc_parse_type.SeqExpr (pos, expr_lst)
       | "VarDefs" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 1 in
            let vars = build_var_decl_list loc (List.nth subterms 0) in
               Fc_parse_type.VarDefs (pos, vars)
       | "FunDef" ->
            let params = check_params loc opname term params [] in
            let subterms = check_subterms_arity loc opname term subterms 5 in
            let storage_class = build_storage_class loc (List.nth subterms 0) in
            let sym, _ = build_symbol loc (List.nth subterms 1) in
            let params = build_patt_decl_list loc (List.nth subterms 2) in
            let ty = build_type loc (List.nth subterms 3) in
            let body = build_expr loc (List.nth subterms 4) in
               Fc_parse_type.FunDef (pos, storage_class, sym, sym, params, ty, body)
       | s ->
            raise (ConvertException (loc, TermError (term, s, "not an expr kind")))

(* Special *)
and build_sym loc term = fst (build_symbol loc term)

(* Option constructors *)
and build_expr_option loc term = build_option loc build_expr term
and build_type_option loc term = build_option loc build_type term
and build_symbol_option loc term = build_option loc build_sym term

(* Tuples *)
and build_sym_opt_pattern_tuple loc term = build_tuple2 loc build_symbol_option build_pattern term
and build_pattern_expr_list_tuple loc term = build_tuple2 loc build_pattern build_expr_list term

(* List constructors *)
and build_symbol_list loc term = build_list loc build_symbol term
and build_type_list loc term = build_list loc build_type term
and build_expr_list loc term = build_list loc build_expr term
and build_patt_decl_list loc term = build_list loc build_patt_decl term
and build_var_decl_list loc term = build_list loc build_var_decl term
and build_sym_opt_pattern_list loc term = build_list loc build_sym_opt_pattern_tuple term
and build_match_list loc term = build_list loc build_pattern_expr_list_tuple term

let build_ast_of_term term =
   let loc = loc_start (mk_bterm [] term) "build_ast_of_term" in
   let opname, params, subterms = breakup_term term in
   let opname = string_of_opname opname in
   let term = mk_bterm [] term in
      match opname with
         "expr" ->
            build_expr loc term
       | _ ->
            raise (ConvertException (loc, TermError (term, opname, "must supply an Fc_ast!expr term")))

let compile_aux name term =
   let expr = build_ast_of_term term in
   if !Fir_state.debug_print_parse then
      Fc_parse_print.pp_print_debug err_formatter "After PhoAST" expr;
   let expr = Fc_parse_standardize.standardize_expr expr in
   if !Fir_state.debug_print_parse then
      Fc_parse_print.pp_print_debug err_formatter "After standardization" expr;
   let expr = Fc_ast_compile.compile expr in
   let prog = Fc_air_compile.compile name expr in
   let prog = Fc_ir_compile.compile prog in
   let prog = Fc_fir_ir.compile prog in
      prog

let compile name term =
(*   Fc_ir_exn.catch compile_aux name term *)
    compile_aux name term


