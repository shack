(*
 * Process Phobos files.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

open Phobos_type
open Phobos_constants
open Phobos_builtin
open Phobos_parse_state
open Phobos_grammar
open Phobos_util
open Phobos_exn
open Phobos_tokenizer
open Phobos_main
open Phobos_print
open Phobos_debug
open Phobos_marshal
open Phobos_rewrite

(*
 * Parse Phobos file.
 *)
let parse_phobos_file s =
   if s = "" then
      raise (Invalid_argument "Must specify a language description file");
   (* Open source file and parse it *)
   let inx = open_in s in
   let pos = s, 0, 0, 0, 0 in
   set_current_position pos;
   let lex = Lexing.from_channel inx in
      Phobos_parser.main Phobos_lexer.main lex

(*
 * Compile a source program with a source grammar.
 *)
let compile_with_source_grammar paths name =
   let parser_struct = parse_phobos_file !Fir_state.grammar_filename in
   let gst, module_name, lenv, parsable = Phobos_grammar.compile paths parser_struct in
   let clenv = create_clenv lenv in
   if parsable then begin
      let penv = Phobos_main.create_penv module_name gst in
      let ptable = Phobos_main.create_parsing_table name gst penv in
      if !Fir_state.save_grammar then
         save_grammar gst lenv penv ptable (filename_of_compiled_grammar name);
      let tokens = tokenize name clenv in
         parse_source gst clenv penv ptable tokens,
         gst.grammar_post_rewrites,
         gst.grammar_inline_forms,
         gst.grammar_local_rewrites
   end else
      raise (PhobosError "no start production is given")

(*
 * Compile a source program with a compiled grammar.
 *)
let compile_with_saved_grammar paths name =
   let fname = find_file paths !Fir_state.compiled_grammar_filename in
   let gst, lenv, penv, ptable = load_grammar fname in
   if gst.grammar_start_symbol = bogus_symbol then
      raise (PhobosError "no start production is given in compiled grammar");
   let clenv = create_clenv lenv in
   let tokens = tokenize name clenv in
      Clock.clock_reset "closure";
      Clock.clock_reset "state_add_set";
      Clock.clock_reset "iterate";
      Clock.clock_reset "obtain_id";
      Clock.clock_reset "obtain_id_contain";
      Clock.clock_reset "states_union";
      Clock.clock_reset "first_n";
      Clock.clock_reset "last_n";
      parse_source gst clenv penv ptable tokens,
      gst.grammar_post_rewrites,
      gst.grammar_inline_forms,
      gst.grammar_local_rewrites

(*
 * Compile a source grammar.
 *)
let compile_source_grammar paths name =
   let parser_struct = parse_phobos_file name in
   let gst, module_name, lenv, parsable = Phobos_grammar.compile paths parser_struct in
   let penv, ptable =
      if parsable then begin
         let penv = Phobos_main.create_penv module_name gst in
         let ptable = Phobos_main.create_parsing_table name gst penv in
            penv, ptable
      end else begin
         (* Make up fake parser *)
         let penv =
            { parser_module      = "[empty-parser]";
              parser_grammar     = PSymbolMTable.empty;
              parser_prod_ids    = ProductionIdTable.empty;
              parser_prods       = ProductionTable.empty;
              parser_nullables   = PSymbolSet.empty;
              parser_first_set   = PSymbolTable.empty;
              parser_follow_set  = PSymbolTable.empty;
              parser_rewrites    = ProductionIdMTable.empty
            } in
         let ptable = ParserFA.empty in
            penv, ptable
      end
   in
   (* Save grammar *)
   save_grammar gst lenv penv ptable (filename_of_compiled_grammar name);
   (* Everything was successful, so we just exit. *)
      exit 0

let compile_aux paths name =
   let term, post_rewrites, inline_forms, local_rewrites =
      if !Fir_state.compiled_grammar_filename <> "" then
         compile_with_saved_grammar paths name
      else if !Fir_state.grammar_filename <> "" then
         compile_with_source_grammar paths name
      else
         compile_source_grammar paths name
   in
   let post_rewrites =
      List.map (fun iforms ->
         local_rewrites @ iforms) post_rewrites
   in

   (* Temporary debugging information *)
   debug_timer "" "closure";
   debug_timer "" "state_add_set";
   debug_timer "" "iterate";
   debug_timer "" "obtain_id";
   debug_timer "" "obtain_id_contain";
   debug_timer "" "states_union";
   debug_timer "" "first_n";
   debug_timer "" "last_n";

   (* Print resulting term *)
   if !Fir_state.apply_no_rewrites then
      begin
         print_string "Parsed term=\n";
         print_string (Simple_print.SimplePrint.string_of_term term);
         print_string "\n";
         exit 1
      end
   (* Or, convert to FC AST *)
   else if !Fir_state.use_fc_ast then
      let term = Mp_mc_compile.compile_phobos_ast term post_rewrites in
         Phobos_fc_ast_term.compile name term
   (* Or, pass it to MetaPRL, and hopefully get a FIR expression back *)
   else begin
      Mp_mc_compile.compile_phobos_fir term post_rewrites inline_forms;
      raise (Invalid_argument "MISSING: MetaPRL/MCC FIR conversion")
   end

(*
 * Do the job and catch exceptions.
 *)
let compile paths name =
   Fir_state.phobos_paths := paths;
   Phobos_exn.catch (compile_aux paths) name
