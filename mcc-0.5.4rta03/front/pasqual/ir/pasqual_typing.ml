(*
 * Enforce Pasqual typing rules.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *
 *)

open Format
open Symbol
open Fc_ast
open Fc_config
open Fc_ast_type
open Fc_ast_env
open Fc_ast_exn
open Fc_ast_print

type kind =
      Parameter
    | Assignment
    | Operand

let exp_pos pos = string_pos "Pasqual_typing" (exp_pos pos)

let referenced_type loc ty =
    match ty with
          TyRef(_, _, ty) ->
            ty
        (* return unit type *)
        | TyUnit(_, _, 1) ->
	    ty
        | _ ->
	    raise (AstException(loc, TypeError2(ty, ty)))

(*
 * Push vars of TyConfArray to venv.
 *)
let rec ptype_type venv ty =
    match ty with
	  TyUnit _
     | TyDelayed _
     | TyZero _
	| TyInt _
	| TyField _
	| TyFloat _
        | TyUEnum (_, _, Label _)
        | TyStruct (_, _, Label _)
        | TyUnion (_, _, Label _) ->
	    venv, ty
	| TyArray (pos, status, ty, a1, a2) ->
	    let venv, ty = ptype_type venv ty in
		venv, TyArray (pos, status, ty, a1, a2)
	| TyConfArray (pos, status, ty, v1, v2, v_ty) ->
	    let venv, v_ty = ptype_type venv v_ty in
	    let venv = venv_add venv v1 v_ty in
	    let venv = venv_add venv v2 v_ty in
	    let venv, ty = ptype_type venv ty in
		venv, TyConfArray (pos, status, ty, v1, v2, v_ty)
	| TyPointer (pos, status, ty) ->
	    let venv, ty = ptype_type venv ty in
		venv, TyPointer (pos, status, ty)
	| TyRef (pos, status, ty) ->
	    let venv, ty = ptype_type venv ty in
		venv, TyRef (pos, status, ty)
	| TyTuple (pos, status, ty_list) ->
	    let venv, ty_list = ptype_type_list venv ty_list in
		venv, TyTuple (pos, status, ty_list)
	| TyElide (pos, status, ty_list) ->
	    let venv, ty_list = ptype_type_list venv ty_list in
		venv, TyElide (pos, status, ty_list)
	| TyEnum _ ->
	    venv, ty
	| TyUEnum (pos, status, Fields (label, fields)) ->
	    let venv, fields = List.fold_left (fun (venv, fields) (pos, var, ty_opt, label) ->
		let venv, ty_opt = ptype_type_option venv ty_opt in
		    venv, (pos, var, ty_opt, label) :: fields) (venv, []) fields
	    in
		venv, TyUEnum(pos, status, Fields (label, fields))
	| TyUnion (pos, status, Fields fields) ->
	    let venv, fields = List.fold_left (fun (venv, fields) (pos, var, ty, io) ->
		let venv, ty = ptype_type venv ty in
		    venv, (pos, var, ty, io) :: fields) (venv, []) fields
	    in
		venv, TyUnion (pos, status, Fields fields)
	| TyStruct (pos, status, Fields fields) ->
	    let venv, fields = List.fold_left (fun (venv, fields) (pos, var, ty, io) ->
		let venv, ty = ptype_type venv ty in
		    venv, (pos, var, ty, io) :: fields) (venv, []) fields
	    in
		venv, TyStruct (pos, status, Fields fields)
	| TyFun (pos, status, ty_list, ty) ->
	    let venv, ty_list = ptype_type_list venv ty_list in
	    let venv, ty = ptype_type venv ty in
		venv, TyFun (pos, status, ty_list, ty)
	| TyVar _ ->
	    venv, ty
	| TyAll (pos, status, vars, ty) ->
	    let venv, ty = ptype_type venv ty in
		venv, TyAll (pos, status, vars, ty)
	| TyLambda (pos, status, vars, ty) ->
	    let venv, ty = ptype_type venv ty in
		venv, TyLambda (pos, status, vars, ty)
	| TyApply (pos, status, v, ty_list) ->
	    let venv, ty_list = ptype_type_list venv ty_list in
		venv, TyApply (pos, status, v, ty_list)

and ptype_type_option venv ty =
    match ty with
	  Some ty ->
	    let venv, ty = ptype_type venv ty in
		venv, Some ty
	| None ->
	    venv, None

and ptype_type_list venv ty_list =
    let venv, ty_list = List.fold_left (fun (venv, ty_list) ty ->
	let venv, ty = ptype_type venv ty in
	    venv, ty :: ty_list) (venv, []) ty_list
    in
	venv, ty_list

(* We are only interested in integral vs. real type
 * incompatibility. All other types are let through.
 *
 * Rules:
 * - The character type is only compatible with itself.
 * - The boolean type is only compatible with itself.
 * - Floats can not be assigned chars.
 * - Ints can not be assigned floats.
 *)
let check_type loc kind ty1 ty2 =
    match ty1, ty2, kind with
	(* Exact types are ok *)
	  TyInt(_, _, prec1, sign1), TyInt(_, _, prec2, sign2), _
	    when prec1 = prec2 && sign1 = sign2 ->
		ty1
	| TyFloat(_, _, prec1), TyFloat(_, _, prec2), _
	    when prec1 = prec2 ->
		ty1

	(* int-char, char-int *)
	| TyInt(_, _, Rawint.Int8, false), TyInt(_, _, _, _), _
	| TyInt(_, _, _, _), TyInt(_, _, Rawint.Int8, false), _ ->
	    raise (AstException(loc, TypeError2(ty1, ty2)))

	(* float-char, char-float *)
	| TyFloat(_, _, _), TyInt(_, _, Rawint.Int8, false), _
	| TyInt(_, _, Rawint.Int8, false), TyFloat(_, _, _), _ ->
	    raise (AstException(loc, TypeError2(ty1, ty2)))

	(* bool-int, int-bool *)
	| TyUnit(_, _, 2), TyInt(_, _, _, _), _
	| TyInt(_, _, _, _), TyUnit(_, _, 2), _ ->
	    raise (AstException(loc, TypeError2(ty1, ty2)))

	(***************************************************
	 * ASSIGNMENT.
	 ***************************************************)
	(* v: int <- float *)
	| TyInt(_, _, _, _), TyFloat(_, _, _), Assignment ->
	    raise (AstException(loc, TypeError2(ty1, ty2)))

	(***************************************************
	 * PARAMETERS.
	 ***************************************************)
	(* floats can't be passed as ints *)
	| TyInt(_, _, _, _), TyFloat(_, _, _), Parameter ->
	    raise (AstException(loc, TypeError2(ty1, ty2)))

	(* int parameters are compatible with each other *)
	| TyInt(_, _, _, _), TyInt(_, _, _, _), Parameter ->
	    ty1

	(* float parameters and arguments must have same precision *)
	| TyFloat(_, _, _), TyFloat(_, _, _), Parameter ->
	    raise (AstException(loc, TypeError2(ty1, ty2)))

	(***************************************************
	 * OTHER.
	 ***************************************************)
	(* Everything else is ok *)
	| _ ->
	    ty1

let check_type_list loc kind ty_list1 ty_list2 =
    List. map2 (fun ty1 ty2 ->
	check_type loc kind ty1 ty2) ty_list1 ty_list2

let rec ptype_expr tenv venv e =
    let loc = string_pos "ptype_expr" (exp_pos e) in
    match e with
	  LetTypes(pos, types, e) ->
	    ptype_add_types tenv venv pos types e
	| LetFuns(pos, funs, e) ->
	    ptype_funs_expr tenv venv pos funs e
	| LetFunDecl(pos, sc, v, l, gflag, ty, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetFunDecl(pos, sc, v, l, gflag, ty, e)
	| LetVar(pos, sc, v, l, gflag, ty, init, e) ->
	    let venv = venv_add venv v ty in
	    (match init with
		  InitAtom (_, a) ->
		    let _ = check_type loc Assignment ty (type_of_atom venv loc a) in
		    let _, _, e = ptype_expr tenv venv e in
			    tenv, venv, LetVar(pos, sc, v, l, gflag, ty, init, e)
		| InitNone ->
		    let _, _, e = ptype_expr tenv venv e in
                       tenv, venv, LetVar(pos, sc, v, l, gflag, ty, init, e)

                | InitArray _ ->
                    eprintf "warning: Pasqual_typing: aggregate initialization not checked";
		    let _, _, e = ptype_expr tenv venv e in
			tenv, venv, LetVar(pos, sc, v, l, gflag, ty, init, e)
	    )
	| LetCoerce(pos, v, ty, a, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetCoerce(pos, v, ty, a, e)
	| LetAtom(pos, v, ty, a, e) ->
	    let venv = venv_add venv v ty in
	    let _ = check_type loc Assignment ty (type_of_atom venv loc a) in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetAtom(pos, v, ty, a, e)
	| LetUnop(pos, v, ty, op, a, e) ->
	    let venv = venv_add venv v ty in
	    let _ = check_type loc Operand ty (type_of_atom venv loc a) in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetUnop(pos, v, ty, op, a, e)
	| LetBinop(pos, v, ty, op, a1, a2, e) ->
	    let venv = venv_add venv v ty in
	    let ty1 = type_of_atom venv loc a1 in
	    let ty2 = type_of_atom venv loc a2 in
	    (* Can operands be used with each other? *)
	    let _ = check_type loc Operand ty1 ty2 in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetBinop(pos, v, ty, op, a1, a2, e)
	| LetExtCall(pos, v, ty, s, ty', args, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetExtCall(pos, v, ty, s, ty', args, e)
	(* () doesn't exist anymore *)
	| LetApply(pos, v, ty, f, label, args, e) ->
	    let venv = venv_add venv v ty in
	    let f_ty = venv_lookup venv loc f in
	    let arg_ty_list = List.map (fun a -> type_of_atom venv loc a) args in
	    let _, ty_list, ty_res = dest_fun_type tenv loc f_ty in
	    (* Check whether function's value can be assigned *)
	    let _ = check_type loc Assignment ty ty_res in
	    (* Check arguments' types *)
	    let _ = check_type_list loc Parameter ty_list arg_ty_list in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetApply(pos, v, ty, f, label, args, e)
    	| LetTest(pos, v, ty, a, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetTest(pos, v, ty, a, e)
	| LetSizeof(pos, v, ty1, ty2, e) ->
	    let venv = venv_add venv v ty1 in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetSizeof(pos, v, ty1, ty2, e)

	| TailCall _
	| Return _ ->
	    tenv, venv, e

	| LetString(pos, v, ty, pre, s, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetString(pos, v, ty, pre, s, e)
	| LetAlloc(pos, v, ty, label, args, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetAlloc(pos, v, ty, label, args, e)
	| LetMalloc(pos, v, a, e) ->
	    let venv = venv_add venv v (TyPointer(pos, StatusNormal, TyInt(pos, StatusNormal, Rawint.Int8, false))) in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetMalloc(pos, v, a, e)
	| IfThenElse(pos, a, e1, e2) ->
	    let _, _, e1 = ptype_expr tenv venv e1 in
	    let _, _, e2 = ptype_expr tenv venv e2 in
		tenv, venv, IfThenElse(pos, a, e1, e2)

	| Match(pos, a, ty, cases) ->
	    tenv, venv, e
	| Try(pos, e1, v, e2) ->
	    let _, _, e1 = ptype_expr tenv venv e1 in
	    let _, _, e2 = ptype_expr tenv venv e2 in
		tenv, venv, Try(pos, e1, v, e2)
	| Raise(pos, a) ->
	    tenv, venv, e
	| LetProject(pos, v1, ty, a2, v3, e) ->
	    let venv = venv_add venv v1 ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetProject(pos, v1, ty, a2, v3, e)
	| LetSubscript(pos, v1, ty, v2, ty2, a, e) ->
	    let venv = venv_add venv v1 ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetSubscript(pos, v1, ty, v2, ty2, a, e)
	| Set(pos, a1, ty, a2, e) ->
	    let ty = referenced_type loc ty in
	    let _ = check_type loc Assignment ty (type_of_atom venv loc a2) in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, Set(pos, a1, ty, a2, e)
	| LetAddrOf(pos, v, ty, a, e) ->
	    let venv = venv_add venv v ty in
	    let _, _, e = ptype_expr tenv venv e in
		tenv, venv, LetAddrOf(pos, v, ty, a, e)
	| Debug(pos, info, e') ->
	    tenv, venv, e

(*
 * Add types to the type environment.
 *)
and ptype_add_types tenv venv pos types e =
    let tenv = List.fold_left (fun tenv (v, _, ty) ->
	tenv_add tenv v ty) tenv types
    in
	let _, _, e = ptype_expr tenv venv e in
	tenv, venv, LetTypes(pos, types, e)

(*
 * Add function names to venv,
 * process their bodies.
 *)
and ptype_funs_expr tenv venv pos funs e =
    (* Add all function names to variable environment *)
    let venv = List.fold_left (fun venv (_, f, _, _, _, ty_f, _, _) ->
	venv_add venv f ty_f) venv funs
    in

    let funs = List.map (fun (pos, f, l, dl, vc, ty_f, args, body) ->
	match ty_f with
	      TyFun(_, _, ty_list, ty_res) ->
		(* Add formal parameters to venv *)
		let venv = List.fold_left2 (fun venv v ty ->
		    let venv, ty = ptype_type venv ty in
			venv_add venv v ty) venv args ty_list
		in
		let _, _, body = ptype_expr tenv venv body in
		    pos, f, l, dl, vc, ty_f, args, body
	    | _ -> raise (Invalid_argument "ptype_funs_expr: not a function")
	) funs
    in
    let _, _, e = ptype_expr tenv venv e in
	tenv, venv, LetFuns(pos, funs, e)

let compile prog =
    ptype_expr tenv_empty venv_empty prog
