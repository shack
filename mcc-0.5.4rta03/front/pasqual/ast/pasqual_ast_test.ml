open Fc_parse_type
open Fc_parse_print
open Fc_parse_state
open Fc_parse_exn

let print_prog prog = 
    Format.open_hvbox 0;
    List.iter print_expr prog;
    Format.close_box ()

let parse_str str =
    let lexbuf = Lexing.from_string str in
    try
	let prog = Pasqual_parser.program Pasqual_lexer.main lexbuf in
	let prog = Fc_parse_standardize.standardize_prog prog in
	print_prog prog;
	Format.print_string "\n";
    with Parsing.Parse_error ->
	Format.print_string "Parse error."; 
	Format.print_string "\n";
	flush stdout

let parse_stdin () =
    let lexbuf = Lexing.from_channel stdin in
    try
	let prog = Pasqual_parser.console Pasqual_lexer.main lexbuf in
	(match prog with
	      Some prog ->
		let prog = Fc_parse_standardize.standardize_prog prog in
		print_endline ">>>";
		print_prog prog;
		Format.print_string "\n";
		flush stdout
	    | None ->
		Format.print_string "nothing to parse."
	)
    with Parsing.Parse_error ->
	Format.print_string "Parse error."; 
	Format.print_string "\n";
	flush stdout

let parse_file file =
    let inc = open_in file in
    let lexbuf = Lexing.from_channel inc in
    try
	let prog = Pasqual_parser.program Pasqual_lexer.main lexbuf in
	let prog = Fc_parse_standardize.standardize_prog prog in     
	print_endline ">>>";
	print_prog prog;
	Format.print_string "\n";
	flush stdout;
	close_in inc	 
    with Parsing.Parse_error ->
	Format.print_string "Parse error in "; 
	print_pos (current_position()); 
	Format.print_string "\n";
	flush stdout; 
	close_in inc; 
	exit 1

let _ =
    if Array.length Sys.argv < 2 then
	parse_stdin ()
    else 
	parse_file Sys.argv.(1)

