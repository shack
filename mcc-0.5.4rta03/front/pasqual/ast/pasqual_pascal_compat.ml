(*
 * Maintain compatibility with Pascal.
 *
 * Replace "variables" with function-calls if a function with
 * that name exists in scope and it is the last declaration for that
 * identifier.
 *
 * Insert Return's in functions and procedures.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz, granicz@cs.caltech.edu
 *
 *)

open Symbol
open Fc_parse_type
open Fc_config

(************************************************************************
 * FUNCTIONS
 ************************************************************************)

let apply_sym = Symbol.add "()"
let bogus_label = Symbol.add "bogus_var_label"

(*
 * Keep track of functions in scope.
 *)
type fenv = symbol SymbolTable.t

let fenv_empty = SymbolTable.empty

let fenv_add = SymbolTable.add

let is_function fenv f =
    try
	let _ = SymbolTable.find fenv f in
	    true
    with
	Not_found ->
	    false

(*
 * Keep track of functions we are inside of.
 *)
let fbenv_empty = SymbolTable.empty

let fbenv_add = SymbolTable.add

let inside_function fbenv f =
    try
	let _ = SymbolTable.find fbenv f in
	    true
    with
	Not_found ->
	    false

let return_var fbenv f =
    try SymbolTable.find fbenv f with
	Not_found ->
	    raise (Invalid_argument ("return_var: Can not find function name [" ^ Symbol.to_string f ^ "]"))

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Convert an expression.
 *)
let rec pascal_expr fenv fbenv is_pascal e =
    match e with
          WithExpr _ ->
                raise (Invalid_argument "pascal_expr: WithExpr: Not implemented")
	| UnitExpr _
	| CharExpr _
	| IntExpr _
	| FloatExpr _
	| StringExpr _ ->
    	    fenv, fbenv, e
	| VarExpr (pos, v, label) ->
	    if is_pascal = false then
		fenv, fbenv, e
	    else
    	    if is_function fenv v then
		fenv, fbenv, OpExpr (pos, PreOp, apply_sym, apply_sym, VarExpr (pos, v, label) :: [VarExpr (pos, label, label)])
	    else
		fenv, fbenv, e
	| OpExpr (pos, op_class, v, label, elist) ->
	    if is_pascal = true then
	    (* Be careful with assignments and function applications *)
	    (match v, List.hd elist with
		  sym, VarExpr (vpos, vv, _)
		    when sym = Symbol.add "=" ->
		    let elist_hd = List.hd elist in
		    let fenv, fbenv, elist_tail = pascal_expr_list fenv fbenv is_pascal (List.tl elist) in
		    let elist_hd =
			if inside_function fbenv vv then begin
			    let f_ret_var = return_var fbenv vv in
				VarExpr (vpos, f_ret_var, bogus_label)
			end else
			    elist_hd
		    in
		    let elist = elist_hd :: elist_tail in
			fenv, fbenv, OpExpr (pos, op_class, v, label, elist)
		| sym, VarExpr (vpos, vv, _)
		    when sym = Symbol.add "()" ->
			let elist_hd = List.hd elist in
			let elist_hd2 = List.hd (List.tl elist) in
			let fenv, fbenv, elist_tail = pascal_expr_list fenv fbenv is_pascal (List.tl (List.tl elist)) in
			let elist = elist_hd :: elist_hd2 :: elist_tail in
			    fenv, fbenv, OpExpr (pos, op_class, v, label, elist)
		| _ ->
		    let fenv, fbenv, elist = pascal_expr_list fenv fbenv is_pascal elist in
			fenv, fbenv, OpExpr (pos, op_class, v, label, elist)
	    )
	    else
		fenv, fbenv, e
	| IfExpr (pos, e1, e2, e3_opt) ->
	    let fenv, fbenv, e1 = pascal_expr fenv fbenv is_pascal e1 in
	    let fenv, fbenv, e2 = pascal_expr fenv fbenv is_pascal e2 in
	    let fenv, fbenv, e3_opt = pascal_expr_option fenv fbenv is_pascal e3_opt in
		fenv, fbenv, IfExpr (pos, e1, e2, e3_opt)
	| ForExpr (pos, e1, e2, e3, e4) ->
	    let fenv, fbenv, e1 = pascal_expr fenv fbenv is_pascal e1 in
	    let fenv, fbenv, e2 = pascal_expr fenv fbenv is_pascal e2 in
	    let fenv, fbenv, e3 = pascal_expr fenv fbenv is_pascal e3 in
	    let fenv, fbenv, e4 = pascal_expr fenv fbenv is_pascal e4 in
		fenv, fbenv, ForExpr (pos, e1, e2, e3, e4)
	| WhileExpr (pos, e1, e2) ->
	    let fenv, fbenv, e1 = pascal_expr fenv fbenv is_pascal e1 in
	    let fenv, fbenv, e2 = pascal_expr fenv fbenv is_pascal e2 in
		fenv, fbenv, WhileExpr (pos, e1, e2)
	| DoExpr (pos, e1, e2) ->
	    let fenv, fbenv, e1 = pascal_expr fenv fbenv is_pascal e1 in
	    let fenv, fbenv, e2 = pascal_expr fenv fbenv is_pascal e2 in
		fenv, fbenv, DoExpr (pos, e1, e2)
	| BreakExpr _
	| ContinueExpr _ ->
	    fenv, fbenv, e
	| ReturnExpr (pos, e) ->
	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
		fenv, fbenv, ReturnExpr (pos, e)
	| RaiseExpr (pos, e) ->
	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
		fenv, fbenv, RaiseExpr (pos, e)
	| SwitchExpr (pos, e, cases) ->
    	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
	    let cases = List.map (fun (patt, elist) ->
		let fenv, fbenv, elist = pascal_expr_list fenv fbenv is_pascal elist in
		    patt, elist) cases
	    in
		fenv, fbenv, SwitchExpr (pos, e, cases)
	| TryExpr (pos, e1, cases, e_opt) ->
    	    let fenv, fbenv, e1 = pascal_expr fenv fbenv is_pascal e1 in
	    let cases = List.map (fun (patt, elist) ->
		let fenv, fbenv, elist = pascal_expr_list fenv fbenv is_pascal elist in
		    patt, elist) cases
	    in
	    let fenv, fbenv, e_opt = pascal_expr_option fenv fbenv is_pascal e_opt in
		fenv, fbenv, TryExpr (pos, e1, cases, e_opt)
	| SeqExpr (pos, el) ->
            let fenv, fbenv, el = pascal_expr_list fenv fbenv is_pascal el in
		fenv, fbenv, SeqExpr (pos, el)
	| PascalExpr (pos, el) ->
            let fenv, fbenv, el = pascal_expr_list fenv fbenv true el in
		fenv, fbenv, SeqExpr (pos, el)
	| PasqualExpr (pos, el) ->
            let fenv, fbenv, el = pascal_expr_list fenv fbenv false el in
		fenv, fbenv, SeqExpr (pos, el)
	| CaseExpr _
	| DefaultExpr _
	| GotoExpr _
	| LabelExpr _ ->
	    fenv, fbenv, e
	| ProjectExpr (pos, e, v) ->
    	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
		fenv, fbenv, ProjectExpr (pos, e, v)
	| SizeofExpr (pos, e) ->
	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
		fenv, fbenv, SizeofExpr (pos, e)
	| SizeofType _ ->
	    fenv, fbenv, e
	| CastExpr (pos, ty, e) ->
	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
		fenv, fbenv, CastExpr (pos, ty, e)
	| VarDefs (pos, defs) ->
	    let fenv' = List.fold_left (fun fenv (pos, sc, patt, ty, init_exp) ->
		match ty, patt with
		      TypeFun (_, _, ty_vars, ty_res), VarPattern (_, f, _, _) ->
		        fenv_add fenv f (ty_vars, ty_res)
		    | _ ->
			fenv) fenv defs
	    in
		fenv', fbenv, VarDefs (pos, defs)
	| TypeDefs _ ->
	    fenv, fbenv, e
	| FunDef (pos, sc, f, label, ty_vars, ty_res, e) ->
	    let ty_vars' = List.map (fun (_, _, ty) -> ty) ty_vars in
	    let fenv' = fenv_add fenv f (ty_vars', ty_res) in
	    let ret_var = new_symbol_string "ret_var" in
	    let fbenv' =
		match ty_res with
		  TypeUnit (_, _, 1) ->
		    fbenv
		| _ ->
		    fbenv_add fbenv f ret_var
	    in
	    let fenv, fbenv, e = pascal_expr fenv' fbenv' is_pascal e in
	    let expr =
		match ty_res, is_pascal with
		  TypeUnit (_, _, 1), true ->
		    let e_return = ReturnExpr (pos, UnitExpr (pos, 1, 0)) in
			FunDef (pos, sc, f, label, ty_vars, ty_res, SeqExpr (pos, [e] @ [e_return]))
		| _, true ->
		    let e_var_decl = VarDefs (pos, [pos, StoreAuto, VarPattern (pos, ret_var, ret_var, None), ty_res, InitNone]) in
		    let e_return = ReturnExpr (pos, VarExpr (pos, ret_var, bogus_label)) in
			FunDef (pos, sc, f, label, ty_vars, ty_res, SeqExpr (pos, [e_var_decl] @ [e] @ [e_return]))
		| _, false ->
		    FunDef (pos, sc, f, label, ty_vars, ty_res, e)
	    in
		fenv', fbenv, expr
(* This match case is unused -- jyh 1/11/02
	| WithExpr (pos, e1, e2) ->
	    let fenv, fbenv, e1 = pascal_expr fenv fbenv is_pascal e1 in
	    let fenv, fbenv, e2 = pascal_expr fenv fbenv is_pascal e2 in
		fenv, fbenv, WithExpr (pos, e1, e2)
*)

and pascal_expr_option fenv fbenv is_pascal e =
    match e with
	  Some e ->
	    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
		fenv, fbenv, Some e
	| None ->
	    fenv, fbenv, None

and pascal_expr_list fenv fbenv is_pascal el =
    let fenv, fbenv, el = List.fold_left (fun (fenv, fbenv, el) e ->
	let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
	    fenv, fbenv, e :: el) (fenv, fbenv, []) el
    in
	fenv, fbenv, List.rev el

(*
 * Finally...
 *)
let compat_expr e is_pascal =
    (* All the functions in scope *)
    let fenv = fenv_empty in
    (* All the functions we are inside of, with their return variables *)
    let fbenv = fbenv_empty in
    let fenv, fbenv, e = pascal_expr fenv fbenv is_pascal e in
	e
