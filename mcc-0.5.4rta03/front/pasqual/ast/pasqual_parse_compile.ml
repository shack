(*
 * Compile Pasqual to parse trees.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)
open Format

open Debug

open Fc_parse_type
open Fc_parse_print
open Pasqual_boot
open Fc_frontends

let print_debug prog =
   pp_print_debug err_formatter prog

let compile name =
    (* Set parameter copying *)
    (* Uncomment when AllocMalloc bug is fixed. *)
    FrontEnd.set_parameter_copying CopyAggregates;

    (* Parse the file *)
    let inx = open_in name in
    let pos = Location.create_loc (Symbol.add name) 0 0 0 0 in
    let prog =
	Fc_parse_state.set_current_position pos;
	try
	    let lex = Lexing.from_channel inx in
	    let prog = Pasqual_parser.program Pasqual_lexer.main lex in
	    close_in inx;
	    prog
	with
	    exn ->
		close_in inx;
		raise exn
    in
    let prog =
	if debug Fir_state.pasqual_add_boot then
	    SeqExpr (pos, Pasqual_boot.boot_ast @ prog)
	else
	    SeqExpr (pos, prog)
    in
    if debug Fir_state.debug_print_parse then
       print_debug "parse_first" prog;
    let prog = Fc_parse_standardize.standardize_expr prog in
    if debug Fir_state.debug_print_parse then
	print_debug "parse_after_standardization" prog;
    if not (FrontEnd.pasqual_program ()) then begin
	let prog = Pasqual_pascal_compat.compat_expr prog true in
	if debug Fir_state.debug_print_parse then
	    print_debug "parse_after_pascal_compat" prog;
	prog
    end else begin
	if debug Fir_state.debug_print_parse then
	    print_debug "parse" prog;
	prog
    end

let compile_pasqual name =
    FrontEnd.set_pasqual_parsing ();
    compile name

let compile_pascal name =
    FrontEnd.set_pascal_parsing ();
    compile name
