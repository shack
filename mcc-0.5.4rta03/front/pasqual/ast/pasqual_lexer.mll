(*
 * Lexer for the Pasqual grammar.
 *
 * ----------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

{
open Location

open Pasqual_parser
open Fc_parse_state
open Fc_parse_type
open Fc_config

(*
 * File position.
 *)
let current_line = ref 1
let current_schar = ref 0

(*
 * Advance a line.
 *)
let set_next_line lexbuf =
   incr current_line;
   current_schar := Lexing.lexeme_end lexbuf

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
   let line = !current_line in
   let schar = Lexing.lexeme_start lexbuf - !current_schar in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let file = current_file () in
   let pos = create_loc file line schar line echar in
      set_current_position pos;
      pos

(*
 * Provide a buffer for building strings.
 *)
let stringbuf = ref []
let string_start = ref (0, 0)

let string_add_int i =
    stringbuf := i :: !stringbuf

let string_add_char c =
    string_add_int (Char.code c)

let pop_string lexbuf =
    let sline, schar = !string_start in
    let eline = !current_line in
    let echar = Lexing.lexeme_end lexbuf - !current_schar in
    let pos = create_loc (current_file ()) sline schar eline echar in
    let s = Array.of_list (List.rev !stringbuf) in
	stringbuf := [];
	s, pos

let set_string_start lexbuf =
   string_start := !current_line, Lexing.lexeme_start lexbuf - !current_schar

(*
 * Extract the char constant.
 *)
let zero_code = Char.code '0'

(*
 * Floating-point constants.
 * the constant may be terminated by a 'f' or 'l'.
 * 'f' means float, 'l' means long double.
 *)
let lex_float s =
    let pre, x =
	let len = String.length s in
	    if len = 0 then
		NormalPrecision, Float80.zero
	    else
		let pre, s =
		    match s.[pred len] with
			  'f' | 'F' -> ShortPrecision, String.sub s 0 (pred len)
			| 'l' | 'L' -> LongPrecision, String.sub s 0 (pred len)
			| _ -> NormalPrecision, s
		    in
			pre, Float80.of_string s
    in
    let pre = FCParam.float_precision pre in
	Rawfloat.of_float80 pre x

(*
 * Integer conversion functions.
 * All lexing integers are computed in Int64.
 *)
let int64_of_radix radix s =
    let len = String.length s in
    let add_char x c1 c2 off =
	let i = Char.code c1 - Char.code c2 + off in
	    Int64.add (Int64.mul x radix) (Int64.of_int i)
    in
    let rec loop pre signed x i =
	if i = len then
	    let pre = FCParam.int_precision pre in
		Rawint.of_int64 pre signed x
	else
	    let c = s.[i] in
		match s.[i] with
		      'l' | 'L' -> loop LongPrecision signed x (succ i)
		    | 'u' | 'U' -> loop pre false x (succ i)
		    | 'a'..'f' -> loop pre signed (add_char x c 'a' 10) (succ i)
		    | 'A'..'F' -> loop pre signed (add_char x c 'A' 10) (succ i)
		    | '0'..'9' -> loop pre signed (add_char x c '0' 0)  (succ i)
		    | _ -> loop pre signed x (succ i)
    in
	loop NormalPrecision true Int64.zero 0

let int64_of_decimal = int64_of_radix (Int64.of_int 10)
let int64_of_octal   = int64_of_radix (Int64.of_int 8)
let int64_of_hex     = int64_of_radix (Int64.of_int 16)

(*
 * Same for normal integers.
 *)
let int_of_radix radix s i =
    let len = String.length s in
    let add_char x c1 c2 off =
	let i = Char.code c1 - Char.code c2 + off in
	    (x * radix) + i
    in
    let rec loop x i =
	if i = len then
	    x
	else
	    let c = s.[i] in
		match s.[i] with
		      'a'..'f' -> loop (add_char x c 'a' 10) (succ i)
		    | 'A'..'F' -> loop (add_char x c 'A' 10) (succ i)
		    | '0'..'9' -> loop (add_char x c '0' 0)  (succ i)
		    | _ -> loop x (succ i)
    in
	loop 0 i

let int_of_decimal = int_of_radix 10
let int_of_octal   = int_of_radix 8
let int_of_hex     = int_of_radix 16

(*
 * Extract the char constant.
 *)
let lex_char s =
    match s.[0] with
	  '\\' ->
	    (match s.[1] with
		  '0'..'7' -> Char.chr (int_of_octal s 2)
		| 'x' | 'X' -> Char.chr (int_of_hex s 2)
		| 'n' ->  '\n'
		| 't' ->  '\t'
		| 'v' ->  '\011'
		| 'b' ->  '\b'
		| 'r' ->  '\r'
		| 'f' ->  '\012'
		| 'a' ->  '\007'
		| '\\' -> '\\'
		| '\'' -> '\''
		| '"' ->  '"'
		| c -> c)
	| c ->
	    c

}

(*
 * Regular expressions.
 *)
let name_prefix = ['_' 'A'-'Z' 'a'-'z']
let name_suffix = ['_' 'A'-'Z' 'a'-'z' '0'-'9']
let name 	= name_prefix name_suffix*

let decimal_char= ['0'-'9']
let octal_char 	= ['0'-'7']
let hex_char 	= ['0'-'9' 'a'-'f' 'A'-'F']

let decimal	= ['1'-'9'] decimal_char*
let octal	= '0' octal_char*
let hex 	= "0x" hex_char*

let float0 	= ['0'-'9']+ '.' ['0'-'9']* (('e' | 'E') ('+' | '-')? decimal)?
let float1 	= ['0'-'9']* '.' ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)?
let float2 	= ['0'-'9']+ (('e' | 'E') ('+' | '-')? decimal)

let float_suffix = ['f' 'F' 'l' 'L']
let float_const = float0 | float1 | float2

let int_suffix 	= (['l' 'L'] ['u' 'U']?) | (['u' 'U'] ['l' 'L']?)
let decimal_const = decimal int_suffix?
let octal_const = octal int_suffix?
let hex_const   = hex int_suffix?

(*
 * Character constants.
 *)
let char_octal  = "\\" octal_char octal_char? octal_char?
let char_hex    = "\\x" hex_char hex_char? hex_char?
let char_escape = "\\" ['a'-'z' '\'' '\\' '"']
let char_normal = [' '-'\255']
let char_cases  = char_octal | char_hex | char_escape | char_normal
let char_const  = "#" char_cases

(*
 * String constants.
 * Be kind to DOS.
 *)
let string_break = "\\\n" | "\\\n\r"

(*
 * Main lexer.
 *)
rule main = parse 
      [' ' '\t']+ { main lexbuf }
    | '\n' { set_next_line lexbuf; main lexbuf }
    | octal_const
      { let pos = set_lexeme_position lexbuf in
        let i = int64_of_octal (Lexing.lexeme lexbuf) in
           TokInt (i, pos)
      }
    | hex_const
      { let pos = set_lexeme_position lexbuf in
        let i = int64_of_hex (Lexing.lexeme lexbuf) in
           TokInt (i, pos)
      }
    | decimal_const
      { let pos = set_lexeme_position lexbuf in
        let i = int64_of_decimal (Lexing.lexeme lexbuf) in
           TokInt (i, pos)
      }
    | float_const
      { let pos = set_lexeme_position lexbuf in
        let x = lex_float (Lexing.lexeme lexbuf) in
           TokFloat (x, pos)
      }
    | name
      { let pos = set_lexeme_position lexbuf in
        let old_id = Lexing.lexeme lexbuf in
        let id = String.lowercase old_id in
	   match id with 
	      "nil" 		-> 	TokNil pos
	    | "true" 		-> 	TokTrue pos
	    | "false"		-> 	TokFalse pos
	    | "begin" 		-> 	TokBegin pos
	    | "end" 		-> 	TokEnd pos
	    | "break" 		-> 	TokBreak pos
	    | "continue" 	-> 	TokContinue pos
	    | "goto" 		-> 	TokGoto pos
	    | "label"		-> 	TokLabel pos
	    | "sizeof" 		-> 	TokSizeOf pos

	    | "try" 		-> 	TokTry pos
	    | "except" 		-> 	TokExcept pos
	    | "finally"		-> 	TokFinally pos
	    | "raise" 		-> 	TokRaise pos
	    | "exception" 	-> 	TokException pos
	    | "case" 		-> 	TokCase pos
	    | "if" 		-> 	TokIf pos
	    | "then" 		-> 	TokThen pos
	    | "else" 		-> 	TokElse pos
	    | "for" 		-> 	TokFor pos
	    | "while" 		-> 	TokWhile pos
	    | "repeat" 		-> 	TokRepeat pos
	    | "until" 		-> 	TokUntil pos
	    | "with" 		-> 	TokWith pos
	    | "do" 		-> 	TokDo pos
	    | "and" 		-> 	TokAnd pos
	    | "or" 		-> 	TokOr pos
	    | "xor" 		-> 	TokXor pos
	    | "mod" 		-> 	TokMod pos
	    | "div" 		-> 	TokDiv pos
	    | "shr" 		-> 	TokShr pos
	    | "shl" 		-> 	TokShl pos
	    | "not" 		-> 	TokNot pos
	    | "var" 		-> 	TokVar pos
	    | "let" 		-> 	TokLet pos
	    | "const" 		-> 	TokConst pos
	    | "type" 		-> 	TokType pos
	    | "function" 	-> 	TokFunction pos
	    | "procedure" 	-> 	TokProcedure pos
	    | "program" 	-> 	TokProgram pos
	    | "uses" 		-> 	TokUses pos
	    | "interface" 	-> 	TokInterface pos
	    | "implementation" 	-> 	TokImplementation pos
	    | "unit" 		-> 	TokUnit pos
	    | "array" 		-> 	TokArray pos
	    | "record" 		-> 	TokRecord pos
	    | "tuple" 		-> 	TokTuple pos
	    | "of" 		-> 	TokOf pos
	    | "to" 		-> 	TokTo pos
	    | "downto" 		-> 	TokDownTo pos
	    | "forward" 	-> 	TokForward pos
	    | "external" 	-> 	TokExternal pos
	    | "operator" 	-> 	TokOperator pos

	    | "byte" 		-> 	TokTypeByte pos
	    | "char" 		-> 	TokTypeChar pos
	    | "short" 		-> 	TokTypeShort pos
	    | "word" 		-> 	TokTypeWord pos	    
	    | "integer" 	-> 	TokTypeInteger pos
	    | "dword" 		-> 	TokTypeDWord pos
	    | "int64" 		-> 	TokTypeInt64 pos
	    | "uint64" 		-> 	TokTypeUInt64 pos

	    | "real" 		-> 	TokTypeReal pos
	    | "double" 		-> 	TokTypeDouble pos
	    | "extended" 	-> 	TokTypeLongDouble pos

	    | "c_string" 	-> 	TokTypeCString pos
	    | "boolean" 	-> 	TokTypeBoolean pos
	    | "poly" 		-> 	TokPoly pos

	    | "class" 		-> 	TokClass pos
	    | "private" 	-> 	TokPrivate pos
	    | "public" 		-> 	TokPublic pos
	    | "return" 		-> 	TokReturn pos
	    | "mutable" 	-> 	TokMutable pos
	    | "ref" 		-> 	TokRef pos
	    | "deref" 		-> 	TokDeref pos
	    | _ 		-> 	TokId (Symbol.add old_id, pos)
      }

    (*
     * Comments. {..} can not be nested, (*..*) can be.
     *)
    | "//" [^ '\n']* '\n'
      { set_next_line lexbuf; main lexbuf }
    | "(*"
      { comment lexbuf; main lexbuf }
    | "{" [^ '}']* "}"
      { main lexbuf }

   (*
    * Strings and chars.
    *)
    | "L\'"
      { set_string_start lexbuf;
        string lexbuf;
	let s, pos = pop_string lexbuf in
	    TokString(LongPrecision, s, pos)
      }
    | '\''
      { set_string_start lexbuf;
        string lexbuf;
	let s, pos = pop_string lexbuf in
	    TokString(NormalPrecision, s, pos)
      }
    | char_const
      { let pos = set_lexeme_position lexbuf in
        let s = Lexing.lexeme lexbuf in
	let len = String.length s in
	let s = String.sub s 1 (len -1) in
	let c = lex_char s in
	    TokChar(c, pos)
      }
   (*
    * Special chars.
    *)
    | "("  { let pos = set_lexeme_position lexbuf in TokLeftParen pos }
    | ")"  { let pos = set_lexeme_position lexbuf in TokRightParen pos }
    | "["  { let pos = set_lexeme_position lexbuf in TokLeftBrack pos }
    | "]"  { let pos = set_lexeme_position lexbuf in TokRightBrack pos }
    | "{"  { let pos = set_lexeme_position lexbuf in TokLeftBrace pos }
    | "}"  { let pos = set_lexeme_position lexbuf in TokRightBrace pos }
    | ";"  { let pos = set_lexeme_position lexbuf in TokSemi pos }
    | ","  { let pos = set_lexeme_position lexbuf in TokComma pos }
    | ".." { let pos = set_lexeme_position lexbuf in TokTwoDots pos }
    | "."  { let pos = set_lexeme_position lexbuf in TokDot pos }
    | ":=" { let pos = set_lexeme_position lexbuf in TokAssignEq pos }
    | ":"  { let pos = set_lexeme_position lexbuf in TokColon pos }
    | "="  { let pos = set_lexeme_position lexbuf in TokEq pos }
    | "+"  { let pos = set_lexeme_position lexbuf in TokPlus pos }
    | "-"  { let pos = set_lexeme_position lexbuf in TokMinus pos }
    | "*"  { let pos = set_lexeme_position lexbuf in TokStar pos }
    | "/"  { let pos = set_lexeme_position lexbuf in TokSlash pos }
    | "<"  { let pos = set_lexeme_position lexbuf in TokLt pos }
    | ">"  { let pos = set_lexeme_position lexbuf in TokGt pos }
    | "&"  { let pos = set_lexeme_position lexbuf in TokAmp pos }
    | "@"  { let pos = set_lexeme_position lexbuf in TokAt pos }
    | "^"  { let pos = set_lexeme_position lexbuf in TokHat pos }
    | "<=" { let pos = set_lexeme_position lexbuf in TokLe pos }
    | ">=" { let pos = set_lexeme_position lexbuf in TokGe pos }
    | "<>" { let pos = set_lexeme_position lexbuf in TokNotEq pos }
    | "->" { let pos = set_lexeme_position lexbuf in TokArrow pos }
    | "<|" { let pos = set_lexeme_position lexbuf in TokLeftTuple pos }
    | "|>" { let pos = set_lexeme_position lexbuf in TokRightTuple pos }
    | "[]" { let pos = set_lexeme_position lexbuf in TokBrackets pos }

   (*
    * All other characters are a syntax error.
    *)
    | eof
      { TokEof }
    | _
      { let pos = set_lexeme_position lexbuf in
           raise (ParseError (pos, Printf.sprintf "illegal char: '%s'"
              (String.escaped (Lexing.lexeme lexbuf))))
      }


(*
 * Strings are delimited by single-quotes.
 * Chars may be escaped.
 *)
and string = parse 
      '\'' | eof		{ () }
    | string_break		{ set_next_line lexbuf;	string lexbuf }
    | '\n'			{ set_next_line lexbuf; string_add_char '\n'; string lexbuf }
    | char_cases		{ let c = lex_char (Lexing.lexeme lexbuf) in
				  string_add_char c;
				  string lexbuf
				}
    | _				{ string_add_char (Lexing.lexeme lexbuf).[0];
				  string lexbuf
				}

(*
 * Comments between (* *) can be nested.
 *)
and comment = parse 
      "(*" 			{ comment lexbuf; comment lexbuf }
    | "*)"			{ () }
    | eof			{ () }
    | '\n'			{ set_next_line lexbuf;
    				  comment lexbuf
				}
    | _				{ comment lexbuf }

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
