(*
 * Print the FC AST as an Ocaml expression.
 * Adam Granicz, Caltech
 * granicz@cs.caltech.edu
 *)
open Fc_parse_type
open Fc_config
open Rawint
open Rawfloat
open Location

let rec ast_string_list_prim printer = function
      []		->	""
    | [head] 		->	printer head
    | head :: tail 	->	(printer head) ^ "; \n" ^ (ast_string_list_prim printer tail)

let rec ast_string_list_prim_no_nl printer = function
      []		->	""
    | [head] 		->	printer head
    | head :: tail 	->	(printer head) ^ "; " ^ (ast_string_list_prim_no_nl printer tail)

let ast_string_list printer l =
    "[" ^ (ast_string_list_prim printer l) ^ "]"

let rec ast_string_init_expr = function
      InitNone ->
        "InitNone"
    | InitExpr(pos, expr) ->
	"InitExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr expr ^ ")"
    | InitArray(pos, init_list) ->
	"InitArray(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_list (fun (sym_opt, init_expr) ->
	    ")" ^
	    ast_string_symbol_option sym_opt ^ ", " ^
	    ast_string_init_expr init_expr ^
	    ")") init_list ^ ")"

and ast_string_symbol sym = "(Symbol.add \"" ^ Symbol.to_string sym ^ "\")"

and ast_string_symbol_option = function
      None ->
        "None"
    | Some sym ->
	"(Some " ^ ast_string_symbol sym ^ ")"

and ast_string_symbol_list sym_list =
    ast_string_list (fun sym -> ast_string_symbol sym) sym_list

and ast_string_iprec = function
      Int8	->	"Int8"
    | Int16	->	"Int16"
    | Int32	->	"Int32"
    | Int64	->	"Int64"

and ast_string_fprec = function
      Single	->	"Single"
    | Double	->	"Double"
    | LongDouble->	"LongDouble"

and ast_string_isign = function
      true	->	"true"
    | false	->	"false"

and ast_string_rawfloat rf =
    "(Rawfloat.of_string " ^
    ast_string_fprec (Rawfloat.precision rf) ^ " \"" ^ Rawfloat.to_string rf ^ "\")"

and ast_string_rawint ri =
    "(Rawint.of_string " ^
    ast_string_iprec (Rawint.precision ri) ^ " " ^
    ast_string_isign (Rawint.signed ri) ^ " \"" ^ (Rawint.to_string ri) ^ "\")"

and ast_string_int_array ia =
    let int_list = Array.to_list ia in
    "[| " ^
    ast_string_list_prim_no_nl (fun i -> string_of_int i) int_list ^
    "|]"

and ast_string_strprec = function
      ShortPrecision 	->	"ShortPrecision"
    | NormalPrecision	->	"NormalPrecision"
    | LongPrecision	->	"LongPrecision"

and ast_string_op_class = function
      PreOp		->	"PreOp"
    | PostOp		->	"PostOp"

and ast_string_storage_class = function
      StoreAuto		->	"StoreAuto"
    | StoreRegister	->	"StoreRegister"
    | StoreStatic	->	"StoreStatic"
    | StoreExtern	->	"StoreExtern"

and ast_string_pattern = function
      CharPattern(pos, rawint) ->
        "CharPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_rawint rawint ^ ")"
    | IntPattern(pos, rawint) ->
	"IntPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_rawint rawint ^ ")"
    | FloatPattern(pos, rawfloat) ->
	"FloatPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_rawfloat rawfloat ^ ")"
    | StringPattern(pos, prec, ia) ->
	"StringPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_strprec prec ^ ", " ^
	ast_string_int_array ia ^ ")"
    | VarPattern(pos, sym, label, ty_opt) ->
	"VarPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ", " ^
        ast_string_symbol label ^ ", " ^
        ast_string_type_opt ty_opt ^ ")"
    | StructPattern(pos, patterns) ->
	"StructPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_list (fun (so, pattern) ->
	    "(" ^
	    ast_string_symbol_option so ^ ", " ^
	    ast_string_pattern pattern ^
	    ")") patterns ^
	")"
    | EnumPattern(pos, sym, pattern) ->
	"EnumPattern(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_pattern pattern ^ ")"
    | AsPattern (pos, p1, p2) ->
        "AsPattern(" ^
        ast_string_pattern p1 ^ ", " ^
        ast_string_pattern p2 ^ ")"


and ast_string_pattern_e_l pattern_elist =
    ast_string_list (fun (pattern, e_list) ->
	"(" ^
	ast_string_pattern pattern ^ ", " ^
	ast_string_expr_list e_list ^
	")") pattern_elist

and ast_string_status = function
      StatusConst	->	"StatusConst"
    | StatusVolatile	->	"StatusVolatile"
    | StatusNormal	->	"StatusNormal"

and ast_string_vardecl_list vd_list =
    ast_string_list (fun (pos, s_class, p, ty, init_expr) ->
	"(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_storage_class s_class ^ ", " ^
	ast_string_pattern p ^ ", " ^
	ast_string_type ty ^ ", " ^
	ast_string_init_expr init_expr ^
	")") vd_list

and ast_string_typedecl_list vd_list =
    ast_string_list (fun (pos, sym, label, ty) ->
	"(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_symbol label ^ ", " ^
	ast_string_type ty ^
	")") vd_list

and ast_string_pattdecl_list vd_list =
    ast_string_list (fun (pos, sym, ty) ->
	"(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_pattern sym ^ ", " ^
	ast_string_type ty ^
	")") vd_list

and ast_string_enum_list enum_list =
    ast_string_list (fun (pos, sym, expr_opt) ->
	"(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_expr_option expr_opt ^
	")") enum_list

and ast_string_uenum_list enum_list =
    ast_string_list (fun (pos, sym, ty_opt) ->
	"(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_type_option ty_opt ^
	")") enum_list

and ast_string_field_list field_list =
    ast_string_list (fun (pos, sym, ty, expr_opt) ->
	"(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_type ty ^ ", " ^
	ast_string_expr_option expr_opt ^
	")") field_list

and ast_string_expr = function
      UnitExpr(pos, i1, i2) ->
        "UnitExpr(" ^
	ast_string_pos pos ^ ", " ^
	(string_of_int i1) ^ ", " ^
	(string_of_int i2) ^
	")"
    | CharExpr(pos, ri) ->
	"CharExpr('" ^
	ast_string_pos pos ^ ", " ^
	ast_string_rawint ri ^ ")"
    | IntExpr(pos, ri) ->
	"IntExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_rawint ri ^ ")"
    | FloatExpr(pos, rf) ->
	"FloatExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_rawfloat rf ^ ")"
    | StringExpr(pos, prec, ia) ->
	"StringExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_strprec prec ^ ", " ^
	ast_string_int_array ia ^ ")"
    | VarExpr(pos, v, label) ->
	"VarExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol v ^ "," ^
	ast_string_symbol label ^ ")"

    | OpExpr(pos, op_class, v, label, el) ->
	"OpExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_op_class op_class ^ ", " ^
	ast_string_symbol v ^ ", " ^
	ast_string_symbol label ^ ", " ^
	ast_string_expr_list el ^ ")"
    | ProjectExpr(pos, expr, sym) ->
	"ProjectExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr expr ^ ", " ^
	ast_string_symbol sym ^ ")"
    | SizeofExpr(pos, expr) ->
	"SizeofExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr expr ^ ")"
    | SizeofType(pos, ty) ->
	"SizeofExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_type ty ^ ")"
    | CastExpr(pos, ty, expr) ->
	"CastExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_type ty ^ ", " ^
	ast_string_expr expr ^ ")"

    | IfExpr(pos, e1, e2, e3_option) ->
	"IfExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_expr e2 ^ ", " ^
	ast_string_expr_option e3_option ^ ")"
    | ForExpr(pos, e1, e2, e3, e4) ->
	"ForExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_expr e2 ^ ", " ^
	ast_string_expr e3 ^ ", " ^
	ast_string_expr e4 ^ ")"
    | WhileExpr(pos, e1, e2) ->
	"WhileExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_expr e2 ^ ")"
    | DoExpr(pos, e1, e2) ->
	"DoExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_expr e2 ^ ")"
    | TryExpr(pos, e1, patterns, e_option) ->
	"TryExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_pattern_e_l patterns ^ ", " ^
	ast_string_expr_option e_option ^ ")"
    | SwitchExpr(pos, e1, patterns) ->
	"SwitchExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_pattern_e_l patterns ^ ")"
    | LabelExpr(pos, sym) ->
	"LabelExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^")"
    | CaseExpr(pos, pattern) ->
	"CaseExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_pattern pattern ^ ")"
    | DefaultExpr pos ->
	"DefaultExpr(" ^
	ast_string_pos pos ^ ")"
    | ReturnExpr(pos, expr) ->
	"ReturnExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr expr ^ ")"
    | RaiseExpr(pos, expr) ->
	"RaiseExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr expr ^ ")"
    | BreakExpr pos ->
	"BreakExpr(" ^
	ast_string_pos pos ^ ")"
    | ContinueExpr pos ->
	"ContinueExpr(" ^
	ast_string_pos pos ^ ")"
    | GotoExpr(pos, sym) ->
	"GotoExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_symbol sym ^ ")"

    | SeqExpr(pos, exprs) ->
	"SeqExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr_list exprs ^ ")"

    | PascalExpr(pos, exprs) ->
	"PascalExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr_list exprs ^ ")"

    | PasqualExpr(pos, exprs) ->
	"PasqualExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr_list exprs ^ ")"

    | VarDefs(pos, defs) ->
	"VarDefs(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_vardecl_list defs ^ ")"
    | FunDef(pos, s_class, sym, label, args, ty, body) ->
	"FunDef(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_storage_class s_class ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_symbol label ^ ", " ^
	ast_string_pattdecl_list args ^ ", " ^
	ast_string_type ty ^ ", " ^
	ast_string_expr body ^ ")"
    | TypeDefs(pos, decls) ->
	 "TypeDefs(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_typedecl_list decls ^ ")"
    | WithExpr(pos, e1, e2) ->
	"WithExpr(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_expr e2 ^ ")"

and ast_string_expr_list elist =
    ast_string_list (fun expr -> ast_string_expr expr) elist

and ast_string_expr_option e_option =
    (match e_option with
	  Some expr 	->	"Some (" ^ ast_string_expr expr ^ ")"
	| None 		-> 	"None"
    )
(*
 * Print a type.
 *)
and ast_string_type ty =
    match ty with
      TypeUnit(pos, status, i) ->
        "TypeUnit(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	(string_of_int i) ^
	")"
    | TypePoly(pos, status) ->
        "TypePoly(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ")"
    | TypeChar(pos, status, iprec, isign) ->
	"TypeChar(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_iprec iprec ^ ", " ^
	ast_string_isign isign ^ ")"
    | TypeInt(pos, status, iprec, isign) ->
	"TypeInt(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_iprec iprec ^ ", " ^
	ast_string_isign isign ^ ")"
    | TypeFloat(pos, status, fprec) ->
	"TypeFloat(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_fprec fprec ^ ")"
    | TypeArray(pos, status, ty, e1, e2) ->
	"TypeArray(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_type ty ^ ", " ^
	ast_string_expr e1 ^ ", " ^
	ast_string_expr e2 ^ ")"
    | TypeConfArray(pos, status, ty, v1, v2, v_ty) ->
	"TypeConfArray(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_type ty ^ ", " ^
	ast_string_symbol v1 ^ ", " ^
	ast_string_symbol v2 ^ ", " ^
	ast_string_type v_ty ^ ")"
    | TypePointer(pos, status, ty) ->
	"TypePointer(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_type ty ^ ")"
    | TypeRef(pos, status, ty) ->
	"TypeRef(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_type ty ^ ")"
    | TypeProduct(pos, status, ty_list) ->
	"TypeRef(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_type_list ty_list ^ ")"
    | TypeEnum(pos, status, Fields enums) ->
	"TypeEnum(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_enum_list enums ^ ")"
    | TypeUEnum(pos, status, Fields (label, enums)) ->
	"TypeUEnum(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
        ast_string_symbol label ^ ", " ^
	ast_string_uenum_list enums ^ ")"
    | TypeStruct(pos, status, Fields fields) ->
	"TypeStruct(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_field_list fields ^ ")"
    | TypeUnion(pos, status, Fields fields) ->
	"TypeUnion(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_field_list fields ^ ")"
    | TypeFun(pos, status, ty_list, ty) ->
	"TypeFun(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_type_list ty_list ^ ", " ^
	ast_string_type ty ^ ")"
    | TypeVar(pos, status, sym) ->
	"TypeVar(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_symbol sym ^ ")"
    | TypeLambda(pos, status, sym_list, ty) ->
	"TypeLambda(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_symbol_list sym_list ^ ", " ^
	ast_string_type ty ^ ")"
    | TypeApply(pos, status, sym, ty_list) ->
	"TypeApply(" ^
	ast_string_pos pos ^ ", " ^
	ast_string_status status ^ ", " ^
	ast_string_symbol sym ^ ", " ^
	ast_string_type_list ty_list ^ ")"
    | _ ->
        raise (Invalid_argument "Adam has to add stuff to pasqual_ast_string")

and ast_string_type_opt = function
      Some ty ->
         "Some (" ^ ast_string_type ty ^ ")"
    | None ->
         "None"

and ast_string_type_option = function
      Some ty ->
        ast_string_type ty
    | None ->
	"None"

and ast_string_type_list types =
    ast_string_list (fun (ty) ->
	ast_string_type ty) types

and ast_string_pos loc =
   let (name, sline, schar, eline, echar) = dest_loc loc in
   let name = Symbol.to_string name in
    "(Location.create_loc (Symbol.add \"" ^ name ^ "\") " ^
    string_of_int sline ^ " " ^
    string_of_int schar ^ " " ^
    string_of_int eline ^ " " ^
    string_of_int echar ^ ")"
