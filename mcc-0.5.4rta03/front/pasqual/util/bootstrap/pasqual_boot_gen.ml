open Fc_parse_type
open Fc_parse_state
open Xstr_search
open Fc_parse_exn
open Format
open Location

let parse_file bootstrap file =
    let inc = open_in file in
    let lexbuf = Lexing.from_channel inc in
    try
	let expr =
	(if bootstrap = true then
	    Pasqual_parser.bootstrap Pasqual_lexer.main lexbuf
	else
	    Pasqual_parser.program Pasqual_lexer.main lexbuf
	) in
	close_in inc; expr
    with
	  exn ->
	    Fc_parse_exn.pp_print_exn err_formatter exn;
	    exit 1

let direct = ref ""

let _ =
    if Array.length (Sys.argv) > 1 then
	direct := Sys.argv.(1);

    let name = !direct ^ "bootstrap.PSQ" in
    let _ = set_current_position (create_loc (Symbol.add name) 1 0 1 0) in
    let boot_expr = parse_file true name in
    let boot_string = Pasqual_ast_string.ast_string_expr_list boot_expr in

    let tem_file = open_in (!direct ^ "pasqual_boot.template") in
    let size = in_channel_length tem_file in
    let buf = String.create size in
    really_input tem_file buf 0 size;
    close_in tem_file;

    let replaced_string = replace_substring buf ["%%ENTRY%%"] (fun _ _ -> boot_string) in
    let out_file = open_out (!direct ^ "pasqual_boot.ml") in
    output_string out_file replaced_string;
    close_out out_file

