(*
 * Pasqual FIR test.
 *)

open Debug
open Pasqual_compile

(*
 * Arguments.
 *)
let debug_eval_ir = ref false

let usage =
   Printf.sprintf "usage: compile [options] [files...] [-- <program arguments>]"

(*
 * Program arguments.
 *)
let debug_eval = ref false
let rev_argv = ref []
let prog = ref None

(*
 * Compile and save the program.
 *)
let compile1 name =
   prog := Some (Pasqual_ir_exn.catch compile name)

let compile2 () =
   if debug debug_eval_ir then
      match !prog with
         Some prog ->
            let argv = List.rev !rev_argv in
               eprintf "evaluating: argc=%d argv={ " (List.length argv);
               List.iter (eprintf "%s; ") argv;
               eprintf "}%t" eflush;
               Fir_eval.eval_main prog argv

       | None ->
            ()

(*
 * Call the main program.
 *)
let spec =
   ["-o", Arg.String (fun s -> output_fir := Some s), "output binary fir code";
    "-g", Arg.Unit (fun () -> Fir_state.debug_stab := true), "add debugging info";
    "-print_ast", Arg.Unit (fun () -> debug_print_ast := true), "print expressions";
    "-print_ir", Arg.Unit (fun () -> debug_print_ir := true), "print intermediate code";
    "-check_ir", Arg.Unit (fun () -> debug_check_ir := true), "typecheck intermediate code";
    "-eval", Arg.Unit (fun () -> debug_eval_ir := true), "evaluate intermediate code";
    "-debug_parith", Arg.Unit (fun () -> Fc_ir_state.debug_parith := true), "trace pointer arithmetic";
    "-debug_pos", Arg.Unit (fun () -> Fir_state.debug_pos := true), "print position information";
    "-trace_pos", Arg.Unit (fun () -> Fir_state.debug_trace_pos := true), "trace function calls";
    "-debug_unify", Arg.Unit (fun () -> Fc_ir_state.debug_unify := true), "trace unification";
    "-debug_closure", Arg.Unit (fun () -> Fc_ir_state.debug_closure := true), "print closure equations";
    "--", Arg.Rest (fun s -> rev_argv := s :: !rev_argv), "program arguments"]

let _ =
   Arg.parse spec compile1 usage;
   Pasqual_ir_exn.catch compile2 ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
