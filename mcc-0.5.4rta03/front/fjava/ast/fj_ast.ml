(*
 * Abstract syntax for the CS237 language.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

open Symbol

(*
 * Position contains:
 *   1. filename
 *   2,3. starting line, starting char
 *   4,5. ending line, ending char
 *)
type pos = string * int * int * int * int

(*
 * Boolean operators.
 *)
type boolop =
   LAndOp
 | LOrOp

(*
 * Binary operators.
 *)
type binop =
   PlusOp
 | MinusOp
 | TimesOp
 | DivideOp
 | ModOp
 | BAndOp
 | BOrOp
 | BXorOp
 | LslOp        (* Left shift *)
 | LsrOp        (* Right shift, logical *)
 | AsrOp        (* Right shift, arithmetic *)
 | EqOp
 | NeqOp
 | LeOp
 | LtOp
 | GeOp
 | GtOp

(*
 * Unary operators.
 *)
type uarithop =
   PreIncrOp
 | PreDecrOp
 | PostIncrOp
 | PostDecrOp

type unop =
   UMinusOp
 | UBNotOp
 | UNotOp

(* Type modifiers *)
type ty_mod =
(* By default access is allowed to all classes in same package. Default access
   specifier if class/method/etc is not explicitly public, protected, or 
   private
 *)
   ModPublic of pos
 | ModProtected of pos
 | ModPrivate of pos

 | ModAbstract of pos
 | ModStatic of pos
 | ModFinal of pos         (* var/field is constant (one assignment only)
                              class can't be subclassed, method can't be
                              overridden *)
   (* These are the tougher ones to implement.  For now they're dropped at
      the IR stage *)
 | ModVolatile of pos      (* For threading *)
 | ModSynchronized of pos  (* Turns a method into a monitor *)
 | ModNative of pos        (* method comes from another language *)
 | ModStrictfp of pos      (* Probably won't ever implement this *)
 | ModTransient of pos     (* Indicates that field should not be considered
                              part of the persistent state of an object.  For
                              serialization, persistence, etc... *)

(*
 * Types are either identified by names,
 * or they are array or function types.
 *)
type ty =
   TypeVoid of pos
 | TypeBool of pos
 | TypeChar of pos
 | TypeInt of pos
 | TypeFloat of pos
 | TypeId of symbol * pos
 | TypeArray of ty * pos
 | TypeFun of ty list * ty * pos
   (* Interface: name * extends * var_decls * pos *)
 | TypeInterface of symbol * symbol list * var_decl list * pos

(*
 * Expressions.
 *)
and expr =
   NilExpr of pos
 | BoolExpr of bool * pos
 | CharExpr of char * pos
 | IntExpr of int * pos
 | FloatExpr of float * pos
 | StringExpr of string * pos
 | VarExpr of symbol * pos
 | UArithExpr of uarithop * expr * pos
 | UnOpExpr of unop * expr * pos
 | BinOpExpr of binop * expr * expr * pos
 | BoolOpExpr of boolop * expr * expr * pos
 | SubscriptExpr of expr * expr * pos
 | ProjectExpr of expr * symbol * pos
 | ApplyExpr of expr * expr list * pos
 | AssignExpr of binop option * expr * expr * pos
 | IfExpr of expr * expr * expr option * pos
 | ForExpr of expr * expr * expr * expr * pos
 | WhileExpr of expr * expr * pos
 | DoExpr of expr * expr * pos
 | SeqExpr of expr list * pos
 | ReturnExpr of expr * pos
 | BreakExpr of symbol option * pos
 | ContinueExpr of symbol option * pos
 | NewConstExpr of symbol * expr list * pos
 | NewArrayExpr of symbol * expr list * pos
 | InstanceofExpr of expr * symbol * pos
 | TryExpr of expr * (symbol * symbol * expr) list * expr option * pos
 | DefExpr of def * pos
 | CastExpr of ty * expr * pos
 | ThrowExpr of expr * pos
 | LabeledExpr of symbol * expr * pos

(*
 * Definitions.
 *)
and def =
   VarDefs of ty_mod list * var_def list * pos
 | FunDefs of (ty_mod list * symbol * var_decl list * ty * expr * pos) list * pos
 | ConstDef of ty_mod list * symbol * var_decl list * expr * pos
 | ClassDef of ty_mod list * symbol * symbol * symbol list * def list * pos
 | TypeDef of ty_mod list * symbol * ty * pos

(*
 * A variable declaration.
 *)
and var_def = symbol * ty * expr option * pos

(*
 * Formal parameter for a function,
 * a variable declaration, or a type
 * definition.
 *)
and var_decl = symbol * ty * pos

(* 
 * An import statement
 * java.util.Vector -> ([java; util], Some Vector)
 * foo.bar.* -> ([foo; bar], None)
 *)
type import = symbol list *
              symbol option *
              pos
              
type package = symbol list * pos

(*
 * A program is an optional package, a list of imports, and a list of 
 * definitions including only TypeDef and ClassDef.
 *)
type prog = {
   prog_package : package option;
   prog_imports : import list;
   prog_defs    : def list
}

(*
 * Parse-time exception.
 *)
type ast_exn =
   UnboundVar of symbol
 | UnboundType of symbol
 | StringError of string
 | StringVarError of string * symbol
 | StringIntError of string * int
 | ArityMismatch of int * int

exception AstException of pos * ast_exn

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
