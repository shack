(*
 * Evaluator.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Symbol

open Fj_ast
open Fj_ast_exn
open Fj_ast_util
open Fj_ast_print

(************************************************************************
 * TYPES AND EXCEPTIONS
 ************************************************************************)

(*
 * This is the value of a variable.
 * It is mutable so that we can perform assignment.
 *)
type evar =
   { mutable var_value : evalue }

(*
 * A type environment gives the definitions of type vars.
 *)
and tenv = ty SymbolTable.t

(*
 * A variable environment gives the values for vars.
 *)
and venv = evar SymbolTable.t

(*
 * An environment contains both.
 *)
and env =
   { tenv : tenv;
     venv : venv
   }

(*
 * Values.
 *
 * Note: we should probably be using an int for ValChar (since
 * chars are supposed to be 16-bit unicode), but its not worth
 * it for this simple evaluator.
 *)
and evalue =
   ValUnit
 | ValBool of bool
 | ValChar of char
 | ValString of string
 | ValInt of int
 | ValFloat of float
 | ValFun of (env -> pos -> evalue list -> evalue)
 | ValNil
 | ValArray of evalue array
 | ValObject of symbol * env

(*
 * "Addresses".  These are used to figure out how to do assignment.
 * We only have to worry about vars and arrays since projections
 * resolve to vars.
 *)
type lvalue =
   LValVar of evar

   (*
    * This is a reference to an array.
    * Invariant: the index is always within bounds (it
    * is checked when the LValArray is created.
    *)
 | LValArray of evalue array * int

(*
 * Extra exceptions.
 *)
exception Throw          of evalue * pos
exception Return         of evalue * pos
exception Break          of symbol option * pos
exception Continue       of symbol option * pos

exception EvalException  of pos * env * ast_exn

let this_sym = Symbol.add "this"

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print a value.
 *)
let rec pp_print_value depth buf v =
   if depth > 3 then
      pp_print_string buf "..."
   else
      match v with
         ValUnit ->
            pp_print_string buf "()"
       | ValBool b ->
            pp_print_bool buf b
       | ValChar c ->
            pp_print_string buf ("'" ^ Char.escaped c ^ "'")
       | ValString s ->
            pp_print_string buf ("\"" ^ String.escaped s ^ "\"")
       | ValInt i ->
            pp_print_int buf i
       | ValFloat x ->
            pp_print_float buf x
       | ValFun _ ->
            pp_print_string buf "<fun>"
       | ValNil ->
            pp_print_string buf "nil"
       | ValArray a ->
            fprintf buf "@[<hv 2>[|";
            Array.iter (fun v ->
                  fprintf buf "@ %a;" (pp_print_value (succ depth)) v) a;
            fprintf buf "@]|]"
       | ValObject (name, env) ->
            fprintf buf "@[<hv 0>@[<hv 3>object[%a] {" pp_print_symbol name;
            SymbolTable.iter (fun v { var_value = e } ->
                  if Symbol.eq v this_sym then
                     fprintf buf "@ this;"
                  else
                     fprintf buf "@ @[<hv 3>%a =@ %a;@]" pp_print_symbol v (pp_print_value (succ depth)) e) env.venv;
            fprintf buf "@]@ }@]"

let pp_print_value = pp_print_value 0

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Generic string concatenation.
 *)
let strcat = (^)

(*
 * Standard name for main.
 *)
let main_sym = Symbol.add "main"
let this_sym = Symbol.add "this"

(*
 * Lookup values.
 *)
let env_lookup_type env pos v =
   try SymbolTable.find env.tenv v with
      Not_found ->
         raise (EvalException (pos, env, UnboundType v))

let env_lookup_var env pos v =
   try SymbolTable.find env.venv v with
      Not_found ->
         raise (EvalException (pos, env, UnboundVar v))

(*
 * Adding values.
 *)
let env_add_type env v ty =
   { env with tenv = SymbolTable.add env.tenv v ty }

let env_add_var env pos v evalue =
   { env with venv = SymbolTable.add env.venv v { var_value = evalue } }

let env_set_var env pos v evalue =
   let var = env_lookup_var env pos v in
      var.var_value <- evalue;
      env

let env_copy_vars env =
   let venv =
      SymbolTable.map (fun { var_value = v } ->
            { var_value = v }) env.venv
   in
      { env with venv = venv }

(*
 * The initial environment contains definitions
 * for the builting types.
 *)
let initial_pos = "<builtin>", 1, 0, 1, 0

let initial_types =
   ["void",   TypeVoid initial_pos;
    "bool",   TypeBool initial_pos;
    "char",   TypeChar initial_pos;
    "int",    TypeInt initial_pos;
    "float",  TypeFloat initial_pos]

(*
 * Initial environment.
 *)
let tenv_empty =
   List.fold_left (fun tenv (s, ty) ->
         SymbolTable.add tenv (Symbol.add s) ty) SymbolTable.empty initial_types

(*
 * Add the PrintStream class so we can print strings.
 *)
let env_empty =
   (* String printer *)
   let pos = "<eval>", 0, 0, 0, 0 in
   let println_fun env pos args =
      match args with
         [ValString s] ->
            printf "%s@." s;
            ValUnit
       | _ ->
            raise (EvalException (pos, env, StringError "println: not a string"))
   in

   (* Integer parser *)
   let parse_int_fun env pos args =
      match args with
         [ValString s] ->
            let i =
               try int_of_string s with
                  Failure _ ->
                     raise (AstException (pos, StringError "parseInt: not a number"))
            in
               ValInt i
       | _ ->
            raise (EvalException (pos, env, StringError "parseInt: not a number"))
   in

   (* Default empty env *)
   let env = { tenv = tenv_empty; venv = SymbolTable.empty } in

   (* Define the FjObject class *)
   let fj_obj_env = env_add_var env initial_pos (Symbol.add "println") (ValFun println_fun) in
   let fj_obj_env = env_add_var fj_obj_env initial_pos (Symbol.add "parseInt") (ValFun parse_int_fun) in
   let fj_obj = ValObject (new_symbol_string "FjObject", fj_obj_env) in

   (* Add System to the env *)
   let env = env_add_var env initial_pos (Symbol.add "FjObject") fj_obj in
      env

(*
 * Print the environment.
 *)
let pp_print_tenv buf tenv =
   SymbolTable.iter (fun v ty ->
         fprintf buf "@ @[<hv 3>%a :@ %a@]" (**)
            pp_print_symbol v
            pp_print_type ty) tenv

let pp_print_venv buf venv =
   SymbolTable.iter (fun v { var_value = a } ->
         fprintf buf "@ @[<hv 3>%a =@ %a@]" (**)
            pp_print_symbol v
            pp_print_value a) venv

let pp_print_env buf env =
   fprintf buf "@[<v 0>*** State:@ *** Types:%a@ *** Variables:%a@]" (**)
      pp_print_tenv env.tenv
      pp_print_venv env.venv

(************************************************************************
 * COERCIONS
 ************************************************************************)

(*
 * Coerce to a bool type.
 * No real coercions are allowed (unlike C).
 *)
let coerce_bool env pos = function
   ValBool b -> b
 | ValChar _
 | ValInt _
 | ValFloat _
 | ValNil
 | ValString _
 | ValArray _
 | ValUnit
 | ValFun _
 | ValObject _ ->
      raise (EvalException (pos, env, StringError "not a bool"))

(*
 * Coerce to a char type.
 * Any scalar type is allowed.
 *)
let coerce_char env pos = function
   ValChar c -> c
 | ValInt i -> Char.chr (i mod 256)
 | ValFloat x -> Char.chr ((int_of_float x) mod 256)
 | ValUnit
 | ValBool _
 | ValFun _
 | ValNil
 | ValString _
 | ValArray _
 | ValObject _ ->
      raise (EvalException (pos, env, StringError "not a char"))

(*
 * Coerce to a int type.
 *)
let coerce_int env pos = function
   ValChar c -> Char.code c
 | ValInt i -> i
 | ValFloat x -> int_of_float x
 | ValUnit
 | ValBool _
 | ValFun _
 | ValNil
 | ValString _
 | ValArray _
 | ValObject _ ->
      raise (EvalException (pos, env, StringError "not an int"))

(*
 * Coerce to a float type.
 *)
let coerce_float env pos = function
   ValChar c -> float_of_int (Char.code c)
 | ValInt i -> float_of_int i
 | ValFloat x -> x
 | ValUnit
 | ValBool _
 | ValFun _
 | ValNil
 | ValString _
 | ValArray _
 | ValObject _ ->
      raise (EvalException (pos, env, StringError "not a float"))

(*
 * Coerce to a string type.
 *)
let coerce_string env pos = function
   ValChar c -> String.make 1 c
 | ValInt i -> string_of_int i
 | ValFloat x -> string_of_float x
 | ValUnit
 | ValBool _
 | ValFun _
 | ValNil
 | ValString _
 | ValArray _
 | ValObject _ ->
      raise (EvalException (pos, env, StringError "not a string"))

(*
 * Coerce to the type of another value.
 *)
let coerce_to env pos v_to v_from =
   match v_to with
      ValChar _ ->
         ValChar (coerce_char env pos v_from)
    | ValInt _ ->
         ValInt (coerce_int env pos v_from)
    | ValFloat _ ->
         ValFloat (coerce_float env pos v_from)
    | ValString _ ->
         ValString (coerce_string env pos v_from)
    | _ ->
         raise (EvalException (pos, env, StringError "coerce_to: illegal coercion"))

(*
 * Simple coercions.
 *)
let coerce_array env pos = function
   ValArray a -> a
 | _ -> raise (EvalException (pos, env, StringError "not an array"))

let coerce_fun env pos = function
   ValFun f -> f
 | _ -> raise (EvalException (pos, env, StringError "not a function"))

let coerce_object env pos = function
   ValObject (_, env) -> env
 | _ -> raise (EvalException (pos, env, StringError "not an object"))

let coerce_object_name env pos = function
   ValObject (name, _) -> name
 | _ -> raise (EvalException (pos, env, StringError "not an object"))

(*
 * Coerce two values so they have the same type.
 *)
let coerce_equal env pos v1 v2 =
   let index_of_value = function
      ValBool _   -> 1
    | ValChar _   -> 2
    | ValInt _    -> 3
    | ValFloat _  -> 4
    | ValString _ -> 5
    | ValFun _    -> -1
    | ValNil      -> -2
    | ValArray _  -> -3
    | ValUnit     -> -4
    | ValObject _ -> -5
   in
   let i1 = index_of_value v1 in
   let i2 = index_of_value v2 in
      if i1 = i2 then
         v1, v2
      else if i1 < 0 || i2 < 0 then
         raise (EvalException (pos, env, StringError "type mismatch"))
      else if i1 > i2 then
         let v2 = coerce_to env pos v1 v2 in
            v1, v2
      else
         let v1 = coerce_to env pos v2 v1 in
            v1, v2

(************************************************************************
 * EVALUATION
 ************************************************************************)

(*
 * Working with lvalues.
 *)
let val_of_lval = function
   LValVar v -> v.var_value
 | LValArray (a, i) -> a.(i)

let set_lval lval v =
   match lval with
      LValVar v' ->
         v'.var_value <- v
    | LValArray (a, i) ->
         a.(i) <- v

(*
 * Default value for a declaration with the given type.
 *)
let rec default_value env pos = function
   TypeBool _ ->
      ValBool false
 | TypeChar _ ->
      ValChar '\000'
 | TypeInt _ ->
      ValInt 0
 | TypeFloat _ ->
      ValFloat 0.0
 | TypeId (v, pos) ->
      default_value env pos (env_lookup_type env pos v)
 | TypeArray _
 | TypeFun _
 | TypeInterface _ ->
      ValNil
 | TypeVoid _ ->
      raise (EvalException (pos, env, StringError "can't create a value of type void"))

(*
 * The evaluation functions all return a pair of an environment
 * and a value.  The environment is changed only if the expression
 * is a definition.
 *)

(*
 * Lookup a variable.
 *)
let eval_var env pos v =
   (env_lookup_var env pos v).var_value

(*
 * Subscript operation.
 *)
let eval_subscript env pos v1 v2 =
   let a = coerce_array env pos v1 in
   let i = coerce_int env pos v2 in
      if i < 0 || i >= Array.length a then
         raise (EvalException (pos, env, StringIntError ("array subscript out of bounds", i)));
      a.(i)

(*
 * Struct projection.
 *)
let eval_proj env pos v label =
   let env = coerce_object env pos v in
      eval_var env pos label

(*
 * Arithmetic negation.
 *)
let eval_uminus env pos v =
   match v with
      ValInt i ->
         ValInt (-i)
    | ValFloat x ->
         ValFloat (-.x)
    | _ ->
         raise (EvalException (pos, env, StringError "uminus"))

(*
 * Boolean negation.
 *)
let eval_unot env pos v =
   ValBool (not (coerce_bool env pos v))

(*
 * Bitwise negation.
 *)
let eval_bnot env pos v =
   ValInt (lnot (coerce_int env pos v))

(*
 * Arithmetic on ints or floats.
 *)
let eval_arith op_int op_float name env pos v1 v2 =
   match coerce_equal env pos v1 v2 with
      ValInt i1, ValInt i2 ->
         ValInt (op_int i1 i2)
    | ValFloat x1, ValFloat x2 ->
         ValFloat (op_float x1 x2)
    | _ ->
         raise (EvalException (pos, env, StringError name))

(*
 * Arithmetic on ints, floats, or strings.
 *)
let eval_string_arith op_string op_int op_float name env pos v1 v2 =
   match coerce_equal env pos v1 v2 with
      ValInt i1, ValInt i2 ->
         ValInt (op_int i1 i2)
    | ValFloat x1, ValFloat x2 ->
         ValFloat (op_float x1 x2)
    | ValString s1, ValString s2 ->
         ValString (op_string s1 s2)
    | _ ->
         raise (EvalException (pos, env, StringError name))

(*
 * Integer-only arith.
 *)
let eval_int_arith op name env pos v1 v2 =
   let i1 = coerce_int env pos v1 in
   let i2 = coerce_int env pos v2 in
      ValInt (op i1 i2)

(*
 * Comparsion arith.
 *)
let eval_bool_arith op_int op_float name env pos v1 v2 =
   let b =
      match coerce_equal env pos v1 v2 with
         ValInt i1, ValInt i2 ->
            op_int i1 i2
       | ValFloat x1, ValFloat x2 ->
            op_float x1 x2
       | _ ->
            raise (EvalException (pos, env, StringError "arith"))
   in
      ValBool b

(*
 * Equality.
 *)
let eval_equal_aux env pos v1 v2 =
   match coerce_equal env pos v1 v2 with
      ValBool b1, ValBool b2 ->
         b1 = b2
    | ValChar c1, ValChar c2 ->
         c1 = c2
    | ValInt i1, ValInt i2 ->
         i1 = i2
    | ValFloat i1, ValFloat i2 ->
         i1 = i2
    | ValNil, ValNil ->
         true
    | ValNil, ValArray _
    | ValArray _, ValNil ->
         false
    | ValArray a1, ValArray a2 ->
         a1 == a2
    | _ ->
         raise (EvalException (pos, env, StringError "equal: incomparable types"))

let eval_equal env pos v1 v2 =
   ValBool (eval_equal_aux env pos v1 v2)

let eval_not_equal env pos v1 v2 =
   ValBool (not (eval_equal_aux env pos v1 v2))

(*
 * General unary arithmetic.
 *)
let eval_succ env pos = function
   ValInt i ->
      ValInt (i + 1)
 | ValFloat x ->
      ValFloat (x +. 1.0)
 | _ ->
      raise (EvalException (pos, env, StringError "not a scalar"))

let eval_pred env pos = function
   ValInt i ->
      ValInt (i - 1)
 | ValFloat x ->
      ValFloat (x -. 1.0)
 | _ ->
      raise (EvalException (pos, env, StringError "not a scalar"))

(*
 * Lvalues are the left-hand sides of assignment
 * expressions.  We can assign variables,
 * projections, and array elements.
 *)
let rec eval_lval env pos e =
   match e with
      VarExpr (v, pos) ->
         LValVar (env_lookup_var env pos v)
    | SubscriptExpr (e1, e2, pos) ->
         let _, v1 = eval_exp env e1 in
         let _, v2 = eval_exp env e2 in
         let a = coerce_array env pos v1 in
         let i = coerce_int env pos v2 in
            if i < 0 || i >= Array.length a then
               raise (EvalException (pos, env, StringIntError ("array index out of bounds", i)));
            LValArray (a, i)
    | ProjectExpr (e, l, pos) ->
         let _, obj = eval_exp env e in
         let obj_env = coerce_object env pos obj in
            LValVar (env_lookup_var obj_env pos l)
    | _ ->
         raise (EvalException (pos, env, StringError "illegal lvalue"))

(*
 * Uarith operates on pointers.
 *)
and eval_uarith env pos op e =
   let lval = eval_lval env pos e in
   let pre_v = val_of_lval lval in
      match op with
         PreIncrOp ->
            let v = eval_succ env pos pre_v in
               set_lval lval v;
               v
       | PreDecrOp ->
            let v = eval_pred env pos pre_v in
               set_lval lval v;
               v
       | PostIncrOp ->
            set_lval lval (eval_succ env pos pre_v);
            pre_v
       | PostDecrOp ->
            set_lval lval (eval_pred env pos pre_v);
            pre_v

(*
 * Logical and.
 *)
and eval_land env pos v1 e2 =
   if coerce_bool env pos v1 then
      snd (eval_exp env e2)
   else
      v1

and eval_lor env pos v1 e2 =
   if coerce_bool env pos v1 then
      v1
   else
      snd (eval_exp env e2)

and eval_boolop env pos op v1 e2 =
   match op with
      LAndOp ->
         eval_land env pos v1 e2
    | LOrOp ->
         eval_lor env pos v1 e2

(*
 * General unary operations.
 *)
and eval_unop env pos op v =
   match op with
      UMinusOp ->
         eval_uminus env pos v
    | UBNotOp ->
         eval_bnot env pos v
    | UNotOp ->
         eval_unot env pos v

(*
 * General binary operations.
 *)
and eval_binop env pos op v1 v2 =
   match op with
      PlusOp ->
         eval_string_arith (strcat) (+) (+.) "+" env pos v1 v2
    | MinusOp ->
         eval_arith (-) (-.) "-" env pos v1 v2
    | TimesOp ->
         eval_arith ( * ) ( *. ) "*" env pos v1 v2
    | DivideOp ->
         eval_arith (/) (/.) "/" env pos v1 v2
    | ModOp ->
         eval_int_arith (mod) "%" env pos v1 v2
    | BAndOp ->
         eval_int_arith (land) "&" env pos v1 v2
    | BOrOp ->
         eval_int_arith (lor) "|" env pos v1 v2
    | BXorOp ->
         eval_int_arith (lxor) "^" env pos v1 v2
    | LslOp ->
         eval_int_arith (lsl) "<<" env pos v1 v2
    | LsrOp ->
         eval_int_arith (lsr) ">>" env pos v1 v2
    | AsrOp ->
         eval_int_arith (asr) ">>>" env pos v1 v2
    | EqOp ->
         eval_equal env pos v1 v2
    | NeqOp ->
         eval_not_equal env pos v1 v2
    | LeOp ->
         eval_bool_arith (<=) (<=) "<=" env pos v1 v2
    | LtOp ->
         eval_bool_arith (<) (<) "<" env pos v1 v2
    | GeOp ->
         eval_bool_arith (>=) (>=) ">=" env pos v1 v2
    | GtOp ->
         eval_bool_arith (>) (>) ">" env pos v1 v2

(*
 * Assignment.
 *)
and eval_assign env pos op e1 e2 =
   let lval = eval_lval env pos e1 in
   let _, v2 = eval_exp env e2 in
      match op with
         Some op ->
            let v1 = val_of_lval lval in
            let v2 = eval_binop env pos op v1 v2 in
               set_lval lval v2;
               v2
       | None ->
            set_lval lval v2;
            v2

(*
 * Conditional statement.
 *)
and eval_if env pos v1 e2 e3 =
   if coerce_bool env pos v1 then
      snd (eval_exp env e2)
   else
      match e3 with
         Some e3 ->
            snd (eval_exp env e3)
       | None ->
            ValUnit

(*
 * For loop.
 *)
and eval_for_inner env pos e_test e_iterate e_body =
   let v2 = snd (eval_exp env e_test) in
      if coerce_bool env pos v2 then
         let _ = 
            try snd (eval_exp env e_body) with 
               Continue (None, pos) ->
                  ValUnit
         in
         let _ = eval_exp env e_iterate in
            eval_for_inner env pos e_test e_iterate e_body
      else
         ValUnit

and eval_for env pos e2 e3 e4 =
   try eval_for_inner env pos e2 e3 e4 with
      Break (None, pos) ->
         ValUnit

(*
 * While loop.
 *)
and eval_while_inner env pos e1 e2 =
   let _, v1 = eval_exp env e1 in
      if coerce_bool env pos v1 then
         let _ = 
            try snd (eval_exp env e2) with 
               Continue (None, pos) ->
                  ValUnit
         in
            eval_while_inner env pos e1 e2
      else
         ValUnit

and eval_while env pos e1 e2 =
   try eval_while_inner env pos e1 e2 with
      Break (None, pos) ->
         ValUnit
(* 
 * Do loop
 *)

and eval_do env pos e_body e_test =
   try 
      ignore (eval_exp env e_body);
      eval_while env pos e_test e_body
   with
      Break (None, pos) ->
         ValUnit
    | Continue (None, pos) ->
         eval_while env pos e_test e_body

(*
 * Break statement returns control to the nearest
 * enclosing loop.
 *)
and eval_break venv label pos =
   raise (Break (label, pos))

(*
 * Continue statement returns control to the nearest
 * enclosing loop.
 *)
and eval_continue env label pos =
   raise (Continue (label, pos))
   
(*
 * Sequential block.
 * Drop the nested environment.
 *)
and eval_seq env exprs =
   ignore (List.fold_left (fun env e ->
                 fst (eval_exp env e)) env exprs)

(*
 * Try evaluating.
 *)
and eval_try env pos e cases finally =
   (* Evaluate the finally case *)
   let eval_exp_opt env = function
      Some e ->
         ignore (eval_exp env e)
    | None ->
         ()
   in

   (* Evaluate the try block, but catch all Throws *)
   let _ =
      try ignore (eval_exp env e) with
         Throw (ValObject (name1, _) as obj1, pos) ->
            (* Search for a matching case *)
            let rec search = function
               (name2, v2, e2) :: cases ->
                  let obj2 = eval_var env pos name2 in
                  let name2 = coerce_object_name env pos obj2 in
                     if Symbol.eq name1 name2 then
                        let env = env_add_var env pos v2 obj1 in
                           (* Make sure another throw does not abort finally *)
                           try ignore (eval_exp env e2) with
                              Throw _ as exn ->
                                 eval_exp_opt env finally;
                                 raise exn
                     else
                        search cases
             | [] ->
                  eval_exp_opt env finally
            in
               search cases
       | Return _ as exn ->
            eval_exp_opt env finally;
            raise exn
   in
      ValUnit

and eval_throw env pos v =
   raise (Throw (v, pos))

(*
 * Instanceof.
 *)
and eval_instanceof env pos obj1 id =
   let b =
      match obj1 with
         ValNil -> false
       | ValObject (name, _) ->
            Symbol.eq name id
       | _ ->
            raise (EvalException (pos, env, StringError "not an object"))
   in
      ValBool b

(*
 * Variable declaration.
 *)
and eval_var_defs add_var env = function
   (v, ty, e, pos) :: decls ->
      let env, v1 =
         match e with
            None ->
               env, default_value env pos ty
          | Some e ->
               eval_exp env e
      in
      let env = add_var env pos v v1 in
         eval_var_defs add_var env decls
 | [] ->
      env

(*
 * Function declaration.
 * Capture the environment (lexical scoping).
 * Also catch return values.
 *)
and add_args env pos vars args =
   let len1 = List.length vars in
   let len2 = List.length args in
      if len1 <> len2 then
         raise (EvalException (pos, env, ArityMismatch (len1, len2)));
      List.fold_left2 (fun env (v, _, _) a ->
            env_add_var env pos v a) env vars args

and eval_fun_def env pos f vars ty body =
   let ty = TypeFun (List.map (fun (_, ty, _) -> ty) vars, ty, pos) in
   let f' env pos args =
      (* Add arguments to environment *)
      let env = add_args env pos vars args in

         (* Evaluate and return the body *)
         try snd (eval_exp env body) with
            Return (v, _) ->
               v
          | Break (None, pos) ->
               raise (EvalException (pos, env, StringError "uncaught break"))
          | Break (Some l, pos) ->
               raise (EvalException (pos, env, StringVarError
                           ("uncaught labeled break", l)))
          | Continue (None, pos) ->
               raise (EvalException (pos, env, StringError "uncaught continue"))
          | Continue (Some l, pos) ->
               raise (EvalException (pos, env, StringVarError
                           ("uncaught labeled continue", l)))
   in
      env_set_var env pos f (ValFun f')

and eval_fun_defs add_var env funs =
   let env =
      List.fold_left (fun env (_, f, _, _, _, pos) ->
            add_var env pos f ValNil) env funs
   in
      List.fold_left (fun env (_, f, vars, ty, body, pos) ->
            eval_fun_def env pos f vars ty body) env funs

(*
 * Return statement throws control to the
 * nearest enclosing function.
 *)
and eval_return pos v =
   raise (Return (v, pos))

(*
 * Apply a function.
 * If the function is a projection, this is a method call
 * to another class.  Otherwise, the method must be in the current
 * class.
 *)
and eval_apply env pos f args =
   match f with
      ProjectExpr (obj, label, pos) ->
         let _, obj = eval_exp env obj in
         let obj_env = coerce_object env pos obj in
         let f = eval_var obj_env pos label in
         let f = coerce_fun env pos f in
            f obj_env pos args
    | _ ->
         let _, f = eval_exp env f in
         let f = coerce_fun env pos f in
            f env pos args

(*
 * Type definition.
 *)
and eval_type_def env pos v ty =
   env_add_type env v ty

(*
 * A class evaluates to an object.
 * We make up a new name for the class,
 * inherit the parent object,
 * ignore the interfaces,
 * and evaluate all the defs in the body.
 *)
and eval_class_def env pos v extends implements defs =
   (* Add the parent's fields to the environment *)
   let env_obj =
      let obj = eval_var env pos extends in
      let env' = coerce_object env pos obj in
      let venv = SymbolTable.fold SymbolTable.add env.venv env'.venv in
         { env with venv = venv }
   in

   (* Add default definitions for all vars and funs *)
   let env_obj =
      List.fold_left (fun env def ->
            match def with
               VarDefs (_, defs, _) ->
                  List.fold_left (fun env (v, _, _, _) ->
                        env_add_var env pos v ValNil) env defs
             | FunDefs (funs, _) ->
                  List.fold_left (fun env (_, f, _, _, _, _) ->
                        env_add_var env pos f ValNil) env funs
             | ConstDef (_, f, _, _, _) ->
                  env_add_var env pos f ValNil
             | TypeDef (_, _, _, pos)
             | ClassDef (_, _, _, _, _, pos) ->
                  raise (EvalException (pos, env, 
                        StringError "nested class definition"))) env_obj defs
   in

   (* Now evaluate all the defs *)
   let env_obj = eval_defs env_set_var env_obj defs in
      env_add_var env pos v (ValObject (v, env_obj))

(*
 * A constructor definition is like a fun,
 * but it copies the environment and returns that.
 *)
and eval_const_def add_var env pos f vars body =
   let f' env pos args =
      (* Copy the environment *)
      let env = env_copy_vars env in

      (* Add the self *)
      let env = env_add_var env pos this_sym ValNil in
      let obj = ValObject (f, env) in
      let env = env_set_var env pos this_sym obj in

      (* Add arguments to environment and evaluate the body *)
      let body_env = add_args env pos vars args in
      let _ = eval_exp body_env body in

         (* Return the copied environment *)
         obj
   in
      add_var env pos f (ValFun f')

(*
 * Create a new object by calling the class's constructor.
 *)
and eval_new_const env pos id args =
   let obj = eval_var env pos id in
   let obj_env = coerce_object env pos obj in
   let const = eval_var obj_env pos id in
   let const = coerce_fun env pos const in
      const obj_env pos args

(*
 * Create a new array.
 * The type may be a class or it may be builtin.
 *)
and eval_new_array env pos id args =
   (* Default value *)
   let v =
      try default_value env pos (SymbolTable.find env.tenv id) with
         Not_found ->
            ValNil
   in

   (* Allocate the array *)
   let rec alloc = function
      [] ->
         v
    | arg :: args ->
         let size = coerce_int env pos arg in
         let a = Array.init size (fun _ -> alloc args) in
            ValArray a
   in
      alloc args

(*
 * Evaluate a definition.
 *)
and eval_def add_var env def =
   match def with
      VarDefs (_, defs, _) ->
         eval_var_defs add_var env defs
    | FunDefs (funs, _) ->
         eval_fun_defs add_var env funs
    | ConstDef (_, f, vars, body, pos) ->
         eval_const_def add_var env pos f vars body
    | TypeDef (_, v, ty, pos) ->
         eval_type_def env pos v ty
    | ClassDef (_, v, extends, implements, body, pos) ->
         eval_class_def env pos v extends implements body

(*
 * Evaluate a list of definitions.
 *)
and eval_defs add_var env defs =
   List.fold_left (eval_def add_var) env defs

(*
 * Evaluate a labeled expression
 *)
and eval_labeled_exp env label expr =
   try
      eval_exp env expr
   with (Break (Some l, _)) when l=label ->
      env, ValUnit
 
(*
 * Evaluate an expression.
 *)
and eval_exp env = function
   NilExpr _ ->
      env, ValNil
 | BoolExpr (b, _) ->
      env, ValBool b
 | IntExpr (i, _) ->
      env, ValInt i
 | FloatExpr (x, _) ->
      env, ValFloat x
 | CharExpr (c, _) ->
      env, ValChar c
 | StringExpr (s, _) ->
      env, ValString s
 | VarExpr (v, pos) ->
      env, eval_var env pos v
 | UArithExpr (op, e, pos) ->
      env, eval_uarith env pos op e
 | UnOpExpr (op, e, pos) ->
      let _, v = eval_exp env e in
         env, eval_unop env pos op v
 | BoolOpExpr (op, e1, e2, pos) ->
      let _, v1 = eval_exp  env e1 in
         env, eval_boolop env pos op v1 e2
 | BinOpExpr (op, e1, e2, pos) ->
      let _, v1 = eval_exp  env e1 in
      let _, v2 = eval_exp  env e2 in
         env, eval_binop env pos op v1 v2
 | SubscriptExpr (e1, e2, pos) ->
      let _, v1 = eval_exp  env e1 in
      let _, v2 = eval_exp  env e2 in
         env, eval_subscript env pos v1 v2
 | ProjectExpr (e, v, pos) ->
      let _, v1 = eval_exp  env e in
         env, eval_proj env pos v1 v
 | ApplyExpr (f, args, pos) ->
      let args = eval_args env args in
         env, eval_apply env pos f args
 | AssignExpr (op, e1, e2, pos) ->
      env, eval_assign env pos op e1 e2
 | IfExpr (e1, e2, e3, pos) ->
      let _, v1 = eval_exp  env e1 in
         env, eval_if env pos v1 e2 e3
 | ForExpr (e1, e2, e3, e4, pos) ->
      let _ = eval_exp  env e1 in
         env, eval_for env pos e2 e3 e4
 | WhileExpr (e1, e2, pos) ->
      env, eval_while env pos e1 e2
 | DoExpr (e1, e2, pos) ->
      env, eval_do env pos e1 e2
 | SeqExpr (el, _) ->
      eval_seq env el;
      env, ValUnit
 | ReturnExpr (e, pos) ->
      let _, v = eval_exp env e in
         eval_return pos v
 | BreakExpr (label, pos) ->
      eval_break env label pos
 | ContinueExpr (label, pos) ->
      eval_continue env label pos
 | TryExpr (e, cases, finally, pos) ->
      env, eval_try env pos e cases finally
 | DefExpr (def, _) ->
      eval_def env_add_var env def, ValUnit
 | NewConstExpr (id, args, pos) ->
      let args = eval_args env args in
         env, eval_new_const env pos id args
 | NewArrayExpr (id, args, pos) ->
      let args = eval_args env args in
         env, eval_new_array env pos id args
 | InstanceofExpr (e, id, pos) ->
      let _, v = eval_exp env e in
         env, eval_instanceof env pos v id
 | CastExpr (_, e, _) ->
      eval_exp env e
 | ThrowExpr (e, pos) ->
      let _, v = eval_exp env e in
         eval_throw env pos v
 | LabeledExpr (l, e, _) ->
      eval_labeled_exp env l e

(*
 * Evaluate a list of arguments.
 *)
and eval_args env args =
   List.map (fun a -> snd (eval_exp env a)) args

(*
 * Print eval errors.
 *)
let pp_print_exn buf exn =
   match exn with
      Throw (_, pos) ->
         pp_print_exn buf (AstException (pos, StringError "uncaught throw"))
    | Return (_, pos) ->
         pp_print_exn buf (AstException (pos, StringError "uncaught return"))
    | Break (None, pos) ->
         pp_print_exn buf (AstException (pos, StringError "uncaught break"))
    | Break (Some l, pos) ->
         pp_print_exn buf (AstException (pos, 
                     StringVarError ("uncaught labeled break", l)))
    | Continue (None, pos) ->
         pp_print_exn buf (AstException (pos, StringError "uncaught continue"))
    | Continue (Some l, pos) ->
         pp_print_exn buf (AstException (pos, 
                     StringVarError ("uncaught labeled continue", l)))
    | EvalException (pos, env, exn) ->
         fprintf buf "@[<v 0>%a@ %a@]" (**)
            pp_print_exn (AstException (pos, exn))
            pp_print_env env
    | exn ->
         pp_print_exn buf exn

let catch f x =
   try f x with
      Throw _
    | Return _
    | Break _
    | Continue _
    | EvalException _
    | AstException _
    | Parsing.Parse_error as exn ->
         pp_print_exn err_formatter exn;
         pp_print_newline err_formatter ();
         exit 1

(*
 * Program evaluation.
 *)
let eval_prog prog name argv =
   let pos = initial_pos in
   prerr_string "WARNING:  Evaluator ignores package and import statements\n";
   let env = eval_defs env_add_var env_empty prog.prog_defs in
   let argv = Array.of_list argv in
   let argv = Array.map (fun s -> ValString s) argv in
   let name = Symbol.add name in
      match env_lookup_var env pos name with
         { var_value = ValObject (_, env) } ->
            (match env_lookup_var env pos main_sym with
                { var_value = ValFun f } ->
                   ignore (catch (f env pos) [ValArray argv])
              | _ ->
                   raise (EvalException (pos, env, StringError "main is not a function")))
       | _ ->
            raise (EvalException (pos, env, StringVarError ("not a class", name)))

(*
 * Constant expressions.
 *)
let eval_int expr =
   let _, i = eval_exp env_empty expr in
      match i with
         ValInt j -> j
       | _ -> raise (AstException (pos_of_expr expr, StringError "not an int"))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
