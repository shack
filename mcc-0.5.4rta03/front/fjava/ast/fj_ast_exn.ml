(*
 * Define the parser errors and print functions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

open Format

open Symbol
open Fj_ast
open Fj_ast_state
open Fj_ast_print

(*
 * Print an AST error.
 *)
let pp_print_exn buf exn =
   match exn with
      UnboundVar v ->
         fprintf buf "unbound symbol: %a" pp_print_symbol v
    | UnboundType v ->
         fprintf buf "unbound type: %a" pp_print_symbol v
    | StringError s ->
         pp_print_string buf s
    | StringIntError (s, i) ->
         fprintf buf "%s: %d" s i
    | StringVarError (s, v) ->
         fprintf buf "%s: %a" s pp_print_symbol v
    | ArityMismatch (i1, i2) ->
         fprintf buf "arity mismatch: expected %d args, got %d args" i1 i2

(*
 * Print and catch exceptions.
 * The string argument is the filename.
 *)
let pp_print_exn buf exn =
   match exn with
      AstException (pos, exn) ->
         fprintf buf "%a: %a" pp_print_pos pos pp_print_exn exn
    | Parsing.Parse_error ->
         fprintf buf "%a:@ Syntax error" pp_print_pos (current_position ())
    | exn ->
         fprintf buf "%a:@ uncaught exception@ %s" (**)
            pp_print_pos (current_position ())
            (Printexc.to_string exn)

let print_exn out exn =
   let buf = formatter_of_out_channel out in
      pp_print_exn buf exn;
      pp_print_newline buf ()

(*
 * Only catch those errors that we care about.
 * This allows the backtrace to find the others.
 *)
let catch f x =
   try f x with
      Parsing.Parse_error
    | AstException _ as exn ->
         print_exn stderr exn;
         exit 1

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
