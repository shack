(*
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Fj_ast
open Fj_ast_print

(*
 * Program printing.
 *)
let print_prog s prog =
   if !Fj_ast_state.print_ast then
      fprintf err_formatter "@[<v 0>*** AST: %s@ %a@]@." s pp_print_prog prog

(*
 * Parse a program.
 *)
let compile name =
   (* Open the input channel and set the position *)
   let inx = open_in name in
   let _ = Fj_ast_state.set_current_position (name, 1, 0, 1, 0) in

   (*
    * Get the program.
    * Make sure to close the channel if an error happens.
    *)
   let prog =
      try
         let lexbuf = Lexing.from_channel inx in
         let prog = Fj_ast_parse.prog Fj_ast_lex.main lexbuf in
            close_in inx;
            prog
      with
         exn ->
            close_in inx;
            raise exn
   in

   (* Print the AST if desired *)
   let _ = print_prog "Build" prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
