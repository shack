(*
 * Utilities over the syntax.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 1999 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol
open Fj_ast

(************************************************************************
 * POSITIONS
 ************************************************************************)

(*
 * Combine two positions.
 *)
let union_pos
    (file1, sline1, schar1, eline1, echar1)
    (file2, sline2, schar2, eline2, echar2) =
   if file1 <> file2 then
      raise (Invalid_argument (**)
                (Printf.sprintf "union_pos: file mistmatch: \"%s\":\"%s\"" (**)
                    (String.escaped file1) (String.escaped file2)));
   let sline, schar =
      if sline1 < sline2 then
         sline1, schar1
      else if sline1 > sline2 then
         sline2, schar2
      else
         sline1, min schar1 schar2
   in
   let eline, echar =
      if eline1 > eline2 then
         eline1, echar1
      else if eline1 < eline2 then
         eline2, echar2
      else
         eline1, max echar1 echar2
   in
      file1, sline, schar, eline, echar

(*
 * Get the position info.
 *)
let pos_of_def = function
   VarDefs (_, _, pos)
 | FunDefs (_, pos)
 | ConstDef (_, _, _, _, pos)
 | ClassDef (_, _, _, _, _, pos)
 | TypeDef (_, _, _, pos) ->
      pos

let pos_of_expr = function
   NilExpr pos
 | BoolExpr (_, pos)
 | IntExpr (_, pos)
 | FloatExpr (_, pos)
 | CharExpr (_, pos)
 | StringExpr (_, pos)
 | VarExpr (_, pos)
 | UArithExpr (_, _, pos)
 | UnOpExpr (_, _, pos)
 | BinOpExpr (_, _, _, pos)
 | BoolOpExpr (_, _, _, pos)
 | SubscriptExpr (_, _, pos)
 | ProjectExpr (_, _, pos)
 | ApplyExpr (_, _, pos)
 | AssignExpr (_, _, _, pos)
 | IfExpr (_, _, _, pos)
 | ForExpr (_, _, _, _, pos)
 | WhileExpr (_, _, pos)
 | DoExpr (_, _, pos)
 | SeqExpr (_, pos)
 | ReturnExpr (_, pos)
 | BreakExpr (_, pos)
 | ContinueExpr (_, pos)
 | TryExpr (_, _, _, pos)
 | DefExpr (_, pos)
 | NewConstExpr (_, _, pos)
 | NewArrayExpr (_, _, pos)
 | InstanceofExpr (_, _, pos)
 | CastExpr (_, _, pos)
 | ThrowExpr (_, pos)
 | LabeledExpr (_, _, pos) ->
      pos

let pos_of_type = function
   TypeVoid pos
 | TypeBool pos
 | TypeChar pos
 | TypeInt pos
 | TypeFloat pos
 | TypeId (_, pos)
 | TypeArray (_, pos)
 | TypeFun (_, _, pos)
 | TypeInterface (_, _, _, pos) ->
      pos

let pos_of_mod = function
   ModPublic pos
 | ModProtected pos
 | ModPrivate pos
 | ModAbstract pos
 | ModStatic pos
 | ModFinal pos
 | ModStrictfp pos
 | ModTransient pos
 | ModVolatile pos
 | ModSynchronized pos
 | ModNative pos ->
      pos

let pos_of_mod_list = function
   [] ->
      raise (Invalid_argument "Can't find position of empty type modifier list")
 | h::t ->
      List.fold_left (fun pos a_mod -> union_pos pos (pos_of_mod a_mod))
            (pos_of_mod h) t

(*
 * -*-
 * Local Variables:
 * Caml-master: "set"
 * End:
 * -*-
 *)
