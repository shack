(*
 * Expression printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Symbol

open Fj_ast

(*
 * Blocks indents by this many spaces.
 *)
let tabstop = 3

(*
 * Print the position in a file.
 *)
let pp_print_pos buf (name, sline, schar, eline, echar) =
   if sline = eline then
      fprintf buf "%s:%d:chars (%d, %d)" name sline schar echar
   else
      fprintf buf "%s:%d,%d-%d,%d" name sline schar eline echar

(*
 * Operators.
 *)
let pp_print_boolop buf op =
   let s =
      match op with
         LAndOp -> "&&"
       | LOrOp -> "||"
   in
      pp_print_string buf s

let pp_print_binop buf op =
   let s =
      match op with
         PlusOp -> "+"
       | MinusOp -> "-"
       | TimesOp -> "*"
       | DivideOp -> "/"
       | ModOp -> "%"
       | BAndOp -> "&"
       | BOrOp -> "|"
       | BXorOp -> "^"
       | LslOp -> "<<"
       | AsrOp -> ">>"
       | LsrOp -> ">>>"
       | EqOp -> "=="
       | NeqOp -> "!="
       | LeOp -> "<="
       | LtOp -> "<"
       | GeOp -> ">="
       | GtOp -> ">"
   in
      pp_print_string buf s

(* 
 * Print Type Modifier
 *)
let pp_print_ty_mod buf tymod =
   let s =
      match tymod with
         ModPublic _ -> "public"
       | ModProtected _ -> "protected"
       | ModPrivate _ -> "private"
       | ModAbstract _ -> "abstract"
       | ModStatic _ -> "static"
       | ModFinal _ -> "final"
       | ModStrictfp _ -> "strictfp"
       | ModTransient _ -> "transient"
       | ModVolatile _ -> "volatile"
       | ModSynchronized _ -> "synchronized"
       | ModNative _ -> "native"
   in
      pp_print_string buf s

(*
 * Print a list of items with a separator.
 *)
let rec pp_print_sep_list sep printer buf l =
   ignore (List.fold_left (fun i x ->
                 if i <> 0 then
                    fprintf buf "%s" sep;
                 printer buf x;
                 succ i) 0 l)

let pp_print_sep_list sep printer buf l =
   fprintf buf "@[<hv 0>%a@]" (pp_print_sep_list sep printer) l

(*
 * Print a list of items with a terminator.
 *)
let rec pp_print_term_list term printer buf l =
   ignore (List.fold_left (fun i x ->
                 if i <> 0 then
                    pp_print_space buf ();
                 fprintf buf "%a%s" printer x term;
                 succ i) 0 l)

let rec pp_print_pre_term_list term printer buf l =
   List.iter (fun x -> fprintf buf "@ %a%s" printer x term) l

(*
 * Unary expression.
 *)
let rec pp_print_unop buf (op, e) =
   let s =
      match op with
         UMinusOp -> "-"
       | UBNotOp -> "~"
       | UNotOp -> "!"
   in
      fprintf buf "%s%a" s pp_print_expr e

and pp_print_uarith buf (op, e) =
   match op with
      PreIncrOp ->
         fprintf buf "++%a" pp_print_expr e
    | PreDecrOp ->
         fprintf buf "--%a" pp_print_expr e
    | PostIncrOp ->
         fprintf buf "%a++" pp_print_expr e
    | PostDecrOp ->
         fprintf buf "%a--" pp_print_expr e

(*
 * A variable declaration.
 *)
and pp_print_var_decl buf (sym, ty, _) =
   fprintf buf "@[<hv 3>%a@ %a@]" (**)
      pp_print_type ty
      pp_print_symbol sym

and pp_print_var_decls buf decls =
   List.iter (fun decl ->
         fprintf buf "@ %a" pp_print_var_decl decl) decls

(*
 * A variable definition.
 *)
and pp_print_var_def buf (v, ty, e, _) =
   fprintf buf "@[<hv 3>%a@ %a" (**)
      pp_print_type ty
      pp_print_symbol v;
   (match e with
       None ->
          fprintf buf ";@]"
     | Some e ->
          fprintf buf " =@ %a@]" pp_print_expr e)

(*
 * Print definitions.
 *)
and pp_print_def buf def =
   match def with
      VarDefs (ty_mods, defs, _) ->
         pp_print_sep_list " " pp_print_ty_mod buf ty_mods;
         fprintf buf " ";
         pp_print_term_list "" pp_print_var_def buf defs
    | FunDefs (funs, _) ->
         List.iter (fun (ty_mods, sym, args, ty, body, _) ->
               fprintf buf "@[<hv 0>";
               pp_print_sep_list " " pp_print_ty_mod buf ty_mods;
               fprintf buf "@ %a %a(%a)@ %a@]" (**)
                  pp_print_type ty
                  pp_print_symbol sym
                  pp_print_formals args
                  pp_print_expr body) funs
    | ConstDef (ty_mods, sym, args, body, _) ->
         fprintf buf "@[<hv 0>";
         pp_print_sep_list " " pp_print_ty_mod buf ty_mods;
         fprintf buf "@ %a(%a)@ %a" (**)
            pp_print_symbol sym
            pp_print_formals args
            pp_print_expr body
    | TypeDef (ty_mods, v, ty, _) ->
         fprintf buf "@[<hv 3>";
         pp_print_sep_list " " pp_print_ty_mod buf ty_mods;
         fprintf buf "@ type %a =@ %a@]" (**)
            pp_print_symbol v
            pp_print_type ty
    | ClassDef (ty_mods, v, extends, implements, body, _) ->
         fprintf buf "@[<v 0>";
         pp_print_sep_list " " pp_print_ty_mod buf ty_mods;
         fprintf buf "@ class %a@ extends %a" (**)
            pp_print_symbol v
            pp_print_symbol extends;
         (match implements with
             [] ->
                ()
           | _ ->
                fprintf buf "@ implements %a" (**)
                   (pp_print_sep_list ", " pp_print_symbol) implements);
         fprintf buf "@ @[<hv 3>{%a@]@ }@]" (**)
            (pp_print_pre_term_list "" pp_print_def) body

(*
 * Print expressions.
 *)
and pp_print_expr buf e =
   match e with
      NilExpr _ ->
         pp_print_string buf "nil"
    | BoolExpr (b, _) ->
         pp_print_bool buf b
    | IntExpr (i, _) ->
         pp_print_int buf i
    | FloatExpr (x, _) ->
         pp_print_float buf x
    | CharExpr (c, _) ->
         pp_print_string buf ("'" ^ Char.escaped c ^ "'")
    | StringExpr (s, _) ->
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"")
    | VarExpr (v, _) ->
         pp_print_symbol buf v
    | UArithExpr (op, e, _) ->
         fprintf buf "(%a)" pp_print_uarith (op, e)
    | UnOpExpr (op, e, _) ->
         fprintf buf "(%a)" pp_print_unop (op, e)
    | BinOpExpr (op, e1, e2, _) ->
         fprintf buf "(@[%a@ %a %a@])" (**)
            pp_print_expr e1
            pp_print_binop op
            pp_print_expr e2
    | BoolOpExpr (op, e1, e2, _) ->
         fprintf buf "(@[%a@ %a %a@])" (**)
            pp_print_expr e1
            pp_print_boolop op
            pp_print_expr e2
    | SubscriptExpr (e1, e2, _) ->
         fprintf buf "(%a)[%a]" (**)
            pp_print_expr e1
            pp_print_expr e2
    | ProjectExpr (e, label, _) ->
         fprintf buf "(%a).%a" (**)
            pp_print_expr e
            pp_print_symbol label
    | ApplyExpr (e, args, _) ->
         fprintf buf "(%a)(@[%a@])" (**)
            pp_print_expr e
            (pp_print_sep_list ", " pp_print_expr) args
    | AssignExpr (None, e1, e2, _) ->
         fprintf buf "@[<hv 3>(%a) <- %a@]" pp_print_expr e1 pp_print_expr e2
    | AssignExpr (Some op, e1, e2, _) ->
         fprintf buf "@[<hv 3>(%a) %a= %a@]" (**)
            pp_print_expr e1
            pp_print_binop op
            pp_print_expr e2
    | IfExpr (e1, e2, None, _) ->
         fprintf buf "@[<hv 3>if(%a)@ %a@]" (**)
            pp_print_expr e1
            pp_print_expr e2
    | IfExpr (e1, e2, Some e3, _) ->
         fprintf buf "@[<hv 0>@[<hv 3>if(%a)@ %a@]@ @[<hv 3>else@ %a@]@]" (**)
            pp_print_expr e1
            pp_print_expr e2
            pp_print_expr e3
    | ForExpr (e1, e2, e3, e4, _) ->
         fprintf buf "@[<hv 0>@[<hv 3>for(%a; %a; %a) {@ %a@]@ }@]" (**)
            pp_print_expr e1
            pp_print_expr e2
            pp_print_expr e3
            pp_print_expr e4
    | WhileExpr (e1, e2, _) ->
         fprintf buf "@[<hv 0>@[<hv 3>while(%a) {@ %a@]@ }@]" (**)
            pp_print_expr e1
            pp_print_expr e2
    | DoExpr (e1, e2, _) ->
         fprintf buf "@[<hv 0>@[<hv 3>do {@ %a@]@ } while(%a)@]" (**)
            pp_print_expr e1
            pp_print_expr e2
    | SeqExpr (el, _) ->
         fprintf buf "@[<v 0>@[<v 3>{%a@]@ }@]" (**)
            (pp_print_pre_term_list ";" pp_print_expr) el
    | ReturnExpr (e, _) ->
         fprintf buf "return(%a)" pp_print_expr e
    | BreakExpr (None, _) ->
         pp_print_string buf "break"
    | BreakExpr (Some l, _) ->
         fprintf buf "break %a" pp_print_symbol l
    | ContinueExpr (None, _) ->
         pp_print_string buf "continue"
    | ContinueExpr (Some l, _) ->
         fprintf buf "continue %a" pp_print_symbol l
    | TryExpr (e, cases, finally, _) ->
         fprintf buf "@[<hv 0>@[<hv 3>try {@ %a@]@ }" pp_print_expr e;
         List.iter (fun (label, v, e) ->
               fprintf buf "@ @[<hv 3>catch(%a %a) {@ %a@]@ }" (**)
                  pp_print_symbol label
                  pp_print_symbol v
                  pp_print_expr e) cases;
         (match finally with
             Some finally ->
                fprintf buf "@ @[<hv 3>finally {@ %a@]@ }@]" pp_print_expr finally
           | None ->
                pp_close_box buf ())
    | DefExpr (def, _) ->
         pp_print_def buf def
    | NewConstExpr (id, args, _) ->
         fprintf buf "new const %a(%a)" (**)
            pp_print_symbol id
            (pp_print_sep_list ", " pp_print_expr) args
    | NewArrayExpr (id, args, _) ->
         fprintf buf "new array %a" pp_print_symbol id;
         List.iter (fun e ->
               fprintf buf "[%a]" pp_print_expr e) args
    | InstanceofExpr (e, cname, pos) ->
         fprintf buf "%a instanceof %a" (**)
            pp_print_expr e
            pp_print_symbol cname
    | CastExpr (ty, e, _) ->
         fprintf buf "(%a) (%a)" (**)
            pp_print_type ty
            pp_print_expr e
    | ThrowExpr (e, _) ->
         fprintf buf "throw %a" pp_print_expr e
    | LabeledExpr (l, e, _) ->
         fprintf buf "@[<v 0>@[<v 3>%a: %a@]@]" 
            pp_print_symbol l
            pp_print_expr e

(*
 * Print the formal parameters.
 *)
and pp_print_formals buf args =
   pp_print_sep_list ", " (fun buf (v, ty, _) ->
         fprintf buf "%a %a" pp_print_type ty pp_print_symbol v) buf args

(*
 * Print a type.
 *)
and pp_print_type buf ty =
   match ty with
      TypeVoid _ ->
         pp_print_string buf "void"
    | TypeBool _ ->
         pp_print_string buf "bool"
    | TypeChar _ ->
         pp_print_string buf "char"
    | TypeInt _ ->
         pp_print_string buf "int"
    | TypeFloat _ ->
         pp_print_string buf "float"
    | TypeId (sym, _) ->
         pp_print_symbol buf sym
    | TypeArray (ty, _) ->
         fprintf buf "%a[]" pp_print_type ty
    | TypeFun (ty_args, ty_res, _) ->
         fprintf buf "((%a) -> %a)" (**)
            (pp_print_sep_list ", " pp_print_type) ty_args
            pp_print_type ty_res
    | TypeInterface (name, extends, fields, _) ->
         fprintf buf "@[<hv 0>@[<hv 3>interface %a"
            pp_print_symbol name;
         (match extends with
             [] ->
                ()
           | _ ->
                fprintf buf "@ extends %a" (**)
                   (pp_print_sep_list ", " pp_print_symbol) extends);
         fprintf buf " {%a@]@ }@]" (**)
            pp_print_var_decls fields

(*
 * Finally, we can print programs.
 *)
let pp_print_prog buf prog =
   (* Print any package stmt *)
   (match prog.prog_package with
      None -> ()
    | Some (pkg, _) ->
         fprintf buf "package %a;\n"
            (pp_print_sep_list "." pp_print_symbol) pkg);
   (* Now print any import stmts *)
   List.iter (fun (pkg, cls, _) ->
               fprintf buf "import %a;\n" 
                     (pp_print_sep_list "." pp_print_symbol) pkg;
               let str = match cls with
                  None ->
                     "*"
                | Some sym ->
                     string_of_symbol sym
               in
                  fprintf buf ".%a" pp_print_string str
            ) prog.prog_imports;
   fprintf buf "@[<hv 0>%a@]" 
      (pp_print_term_list "" pp_print_def) prog.prog_defs

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
