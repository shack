(*
 * Convert FJava FIR to FIR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location
open Field_table

open Fir
open Fir_ds
open Fir_set

open Fj_fir_exn
open Fj_fir_env
open Fj_fir_type
open Fj_fir_check

let vexp_pos pos = string_pos "Fj_fir_fir" (vexp_pos pos)
let fir_exp_pos e = string_pos "Fj_fir_fir" (fir_exp_pos e)

let loc = bogus_loc "<Fj_fir_ir>"

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Tuple classes.
 *)
let tuple_class_of_record_class rclass =
   match rclass with
      Fj_fir.RecordFrame
    | Fj_fir.RecordFields ->
         RawTuple
    | Fj_fir.RecordClass
    | Fj_fir.RecordMethods
    | Fj_fir.RecordVObject
    | Fj_fir.RecordClosure ->
         NormalTuple

let tuple_sub_of_record_class rclass =
   match rclass with
      Fj_fir.RecordFrame
    | Fj_fir.RecordFields ->
         RawTupleSub
    | Fj_fir.RecordClass
    | Fj_fir.RecordMethods
    | Fj_fir.RecordVObject
    | Fj_fir.RecordClosure ->
         TupleSub

let rec tuple_sub_of_type env pos ty =
   let pos = string_pos "tuple_sub_of_type" pos in
      match expand_type env pos ty with
         Fj_fir.TyVoid
       | Fj_fir.TyUnit
       | Fj_fir.TyBool
       | Fj_fir.TyChar
       | Fj_fir.TyInt ->
            RawIntSub (Rawint.Int32, true)
       | Fj_fir.TyFloat ->
            RawFloatSub Rawfloat.Double
       | Fj_fir.TyString
       | Fj_fir.TyArray _
       | Fj_fir.TyRecord _
       | Fj_fir.TyNames _
       | Fj_fir.TyNil
       | Fj_fir.TyVar _
       | Fj_fir.TyProject _
       | Fj_fir.TyObject _ ->
            BlockPointerSub
       | Fj_fir.TyFun _
       | Fj_fir.TyMethod _ ->
            FunctionSub
       | Fj_fir.TyAll (_, ty)
       | Fj_fir.TyExists (_, ty)
       | Fj_fir.TyLambda (_, ty) ->
            tuple_sub_of_type env pos ty
       | Fj_fir.TyApply _ ->
            raise (FirException (pos, Fj_fir.StringError "internal error"))

let sub_script = RawIntIndex (Rawint.Int32, true)

let subop_of_record_op env pos rclass ty =
   let sblock = tuple_sub_of_record_class rclass in
   let svalue = tuple_sub_of_type env pos ty in
      { sub_block = sblock;
        sub_value = svalue;
        sub_index = WordIndex;
        sub_script = sub_script
      }

(*
 * Convert a type.
 *)
let rec build_type pos ty =
   let pos = string_pos "build_type" pos in
      match ty with
         Fj_fir.TyVoid ->
            TyEnum 0
       | Fj_fir.TyUnit ->
            TyEnum 1
       | Fj_fir.TyBool ->
            TyEnum 2
       | Fj_fir.TyChar ->
            TyEnum 256
       | Fj_fir.TyInt ->
            TyRawInt (Rawint.Int32, true)
       | Fj_fir.TyString ->
            TyRawData
       | Fj_fir.TyFloat ->
            TyFloat Rawfloat.Double
       | Fj_fir.TyArray ty ->
            TyArray (build_type pos ty)
       | Fj_fir.TyFun (ty_vars, ty_res) ->
            TyFun (List.map (build_type pos) ty_vars, build_type pos ty_res)
       | Fj_fir.TyMethod (ty_this, ty_vars, ty_res) ->
            TyFun (List.map (build_type pos) (ty_this :: ty_vars), build_type pos ty_res)
       | Fj_fir.TyRecord (rclass, ty_fields) ->
            build_record_type pos rclass ty_fields
       | Fj_fir.TyNames (v, names) ->
            build_names_type pos v names
       | Fj_fir.TyApply (v, tyl) ->
            TyApply (v, List.map (build_type pos) tyl)
       | Fj_fir.TyNil ->
            raise (FirException (pos, Fj_fir.StringError "illegal nil type"))
       | Fj_fir.TyVar v ->
            TyVar v
       | Fj_fir.TyProject (v, i) ->
            TyProject (v, i)
       | Fj_fir.TyAll (vars, ty) ->
            TyAll (vars, build_type pos ty)
       | Fj_fir.TyExists (vars, ty) ->
            TyExists (vars, build_type pos ty)
       | Fj_fir.TyLambda (vars, ty) ->
            raise (FirException (pos, Fj_fir.StringTypeError
                     ("illegal lambda type", ty)))
       | Fj_fir.TyObject (v, ty) ->
            TyObject (v, build_type pos ty)

(*
 * A record get converted to a tuple.
 *)
and build_record_type pos rclass ty_fields =
   let pos = string_pos "build_record_type" pos in
   let fields = FieldTable.to_list ty_fields in
   let fields = List.map (fun (_, ty) -> build_type pos ty, Mutable) fields in
   let tclass = tuple_class_of_record_class rclass in
      TyTuple (tclass, fields)

(*
 * A names type.
 *)
and build_names_type pos v names =
   TyCase (TyApply (v, []))

(*
 * Build a typedef.
 *)
let build_tydef v ty =
   let pos = string_pos "build_tydef" (vexp_pos v) in
      match ty with
         Fj_fir.TyLambda (vars, ty) ->
            TyDefLambda (vars, build_type pos ty)
       | ty ->
            TyDefLambda ([], build_type pos ty)

(************************************************************************
 * EXPRESSIONS
 ************************************************************************)

(*
 * Boolean sets.
 *)
let false_set = IntSet (IntSet.of_point 0)
let true_set = IntSet (IntSet.of_point 1)

(*
 * Convert an atom.
 *)
let build_atom a =
   match a with
      Fj_fir.AtomUnit ->
         AtomEnum (1, 0)
    | Fj_fir.AtomNil ->
         AtomNil TyDelayed
    | Fj_fir.AtomBool b ->
         AtomEnum (2, (if b then 1 else 0))
    | Fj_fir.AtomChar c ->
         AtomEnum (256, Char.code c)
    | Fj_fir.AtomInt i ->
         AtomRawInt (Rawint.of_int Rawint.Int32 true i)
    | Fj_fir.AtomFloat x ->
         AtomFloat (Rawfloat.of_float Rawfloat.Double x)
    | Fj_fir.AtomVar v ->
         AtomVar v

(*
 * Convert the expression.
 *)
let rec build_exp env e =
   let pos = string_pos "build_exp" (fir_exp_pos e) in
      match e with
         Fj_fir.LetAtom (v, ty, a, e) ->
            build_atom_exp env pos v ty a e
       | Fj_fir.LetUnop (v, ty, op, a, e) ->
            build_unop_exp env pos v ty op a e
       | Fj_fir.LetBinop (v, ty, op, a1, a2, e) ->
            build_binop_exp env pos v ty op a1 a2 e
       | Fj_fir.LetExt (v, ty1, s, ty2, args, e) ->
            build_ext_exp env pos v ty1 s ty2 args e
       | Fj_fir.TailCall (v, args) ->
            let label = new_symbol v in
               make_exp loc (TailCall (label, AtomVar v, List.map build_atom args))
       | Fj_fir.MethodCall (v, a, args) ->
            let label = new_symbol v in
               make_exp loc (TailCall (label, AtomVar v, build_atom a :: List.map build_atom args))
       | Fj_fir.IfThenElse (a, e1, e2) ->
            build_if_exp env pos a e1 e2
       | Fj_fir.IfType (a, name, v, e1, e2) ->
            build_iftype_exp env pos a name v e1 e2
       | Fj_fir.LetArray (v, ty, args, a, e) ->
            build_array_exp env pos v ty args a e
       | Fj_fir.LetSubscript (v, ty, a1, a2, e) ->
            build_subscript_exp env pos v ty a1 a2 e
       | Fj_fir.SetSubscript (a1, a2, ty, a3, e) ->
            build_set_subscript_exp env pos a1 a2 ty a3 e
       | Fj_fir.LetRecord (v, ty, rclass, fields, e) ->
            build_record_exp env pos v ty rclass fields e
       | Fj_fir.LetProject (v, ty, a, label, e) ->
            build_project_exp env pos v ty a label e
       | Fj_fir.SetProject (a1, label, ty, a2, e) ->
            build_set_project_exp env pos a1 label ty a2 e
       | Fj_fir.LetVar _
       | Fj_fir.SetVar _
       | Fj_fir.LetFuns _ ->
            raise (FirException (pos, Fj_fir.FirLevel2))

(*
 * Atom is an ID unary.
 *)
and build_atom_exp env pos v ty a e =
   let pos = string_pos "build_atom_exp" pos in
   let env = env_add_var env v ty in
   let ty = build_type pos ty in
   let a = build_atom a in
   let e = build_exp env e in
      make_exp loc (LetAtom (v, ty, a, e))

(*
 * Unary operation.
 *)
and build_unop_exp env pos v ty op a e =
   let pos = string_pos "build_unop_exp" pos in
   let env = env_add_var env v ty in
   let ty = build_type pos ty in
   let a = build_atom a in
   let e = build_exp env e in
   let e =
      match op with
         Fj_fir.UMinusIntOp ->
            LetAtom (v, ty, AtomUnop (UMinusRawIntOp (Rawint.Int32, true), a), e)
       | Fj_fir.UMinusFloatOp ->
            LetAtom (v, ty, AtomUnop (UMinusFloatOp Rawfloat.Double, a), e)
       | Fj_fir.UNotIntOp ->
            LetAtom (v, ty, AtomUnop (NotRawIntOp (Rawint.Int32, true), a), e)
       | Fj_fir.UNotBoolOp ->
            LetAtom (v, ty, AtomBinop (XorEnumOp 2, a, AtomEnum (2, 1)), e)
       | Fj_fir.UIntOfFloat ->
            LetAtom (v, ty, AtomUnop (RawIntOfFloatOp (Rawint.Int32, true, Rawfloat.Double), a), e)
       | Fj_fir.UFloatOfInt ->
            LetAtom (v, ty, AtomUnop (FloatOfRawIntOp (Rawfloat.Double, Rawint.Int32, true), a), e)
   in
      make_exp loc e

(*
 * Binary operation.
 *)
and build_binop_exp env pos v ty op a1 a2 e =
   let pos = string_pos "build_binop_exp" pos in
   let env = env_add_var env v ty in
   let ty = build_type pos ty in
   let a1 = build_atom a1 in
   let a2 = build_atom a2 in
   let e = build_exp env e in
   let op =
      match op with
         Fj_fir.AddIntOp -> PlusRawIntOp (Rawint.Int32, true)
       | Fj_fir.SubIntOp -> MinusRawIntOp (Rawint.Int32, true)
       | Fj_fir.MulIntOp -> MulRawIntOp (Rawint.Int32, true)
       | Fj_fir.DivIntOp -> DivRawIntOp (Rawint.Int32, true)
       | Fj_fir.RemIntOp -> RemRawIntOp (Rawint.Int32, true)
       | Fj_fir.AndIntOp -> AndRawIntOp (Rawint.Int32, true)
       | Fj_fir.OrIntOp  -> OrRawIntOp (Rawint.Int32, true)
       | Fj_fir.XorIntOp -> XorRawIntOp (Rawint.Int32, true)
       | Fj_fir.LslIntOp -> SlRawIntOp (Rawint.Int32, true)
       | Fj_fir.LsrIntOp -> SrRawIntOp (Rawint.Int32, false)
       | Fj_fir.AsrIntOp -> SrRawIntOp (Rawint.Int32, true)
       | Fj_fir.EqIntOp  -> EqRawIntOp (Rawint.Int32, true)
       | Fj_fir.NeqIntOp -> NeqRawIntOp (Rawint.Int32, true)
       | Fj_fir.LeIntOp  -> LeRawIntOp (Rawint.Int32, true)
       | Fj_fir.LtIntOp  -> LtRawIntOp (Rawint.Int32, true)
       | Fj_fir.GeIntOp  -> GeRawIntOp (Rawint.Int32, true)
       | Fj_fir.GtIntOp  -> GtRawIntOp (Rawint.Int32, true)

       | Fj_fir.AddFloatOp -> PlusFloatOp Rawfloat.Double
       | Fj_fir.SubFloatOp -> MinusFloatOp Rawfloat.Double
       | Fj_fir.MulFloatOp -> MulFloatOp Rawfloat.Double
       | Fj_fir.DivFloatOp -> DivFloatOp Rawfloat.Double
       | Fj_fir.RemFloatOp -> RemFloatOp Rawfloat.Double
       | Fj_fir.EqFloatOp  -> EqFloatOp Rawfloat.Double
       | Fj_fir.NeqFloatOp -> NeqFloatOp Rawfloat.Double
       | Fj_fir.LeFloatOp  -> LeFloatOp Rawfloat.Double
       | Fj_fir.LtFloatOp  -> LtFloatOp Rawfloat.Double
       | Fj_fir.GtFloatOp  -> GtFloatOp Rawfloat.Double
       | Fj_fir.GeFloatOp  -> GeFloatOp Rawfloat.Double

       | Fj_fir.EqPtrOp    -> EqEqOp TyDelayed
       | Fj_fir.NeqPtrOp   -> NeqEqOp TyDelayed
   in
      make_exp loc (LetAtom (v, ty, AtomBinop (op, a1, a2), e))

(*
 * External call.
 *)
and build_ext_exp env pos v ty1 s ty2 args e =
   let pos = string_pos "build_ext_exp" pos in
   let env = env_add_var env v ty1 in
   let ty1 = build_type pos ty1 in
   let ty2 = build_type pos ty2 in
   let args = List.map build_atom args in
   let e = build_exp env e in
      make_exp loc (LetExt (v, ty1, "java_" ^ s, false, ty2, [], args, e))

(*
 * Conditional becomes a match.
 *)
and build_if_exp env pos a e1 e2 =
   let pos = string_pos "build_if_exp" pos in
   let false_label = new_symbol_string "false" in
   let true_label = new_symbol_string "true" in
   let a = build_atom a in
   let e1 = build_exp env e1 in
   let e2 = build_exp env e2 in
      make_exp loc (Match (a, [true_label, true_set, e1; false_label, false_set, e2]))

and build_iftype_exp env pos a name v e1 e2 =
   let pos = string_pos "build_iftype_exp" pos in
   let a = build_atom a in
   let e2 = build_exp env e2 in
   let ty_name = env_lookup_var env pos name in
   let env = env_add_var env v ty_name in
   let e1 = build_exp env e1 in
      make_exp loc (TypeCase (a, a, name, v, e1, e2))

(*
 * Allocate a multi-dimensional array.
 *)
and build_array_exp env pos v ty dimens a e =
   let pos = string_pos "build_array_exp" pos in
   let env = env_add_var env v ty in
   let ty = build_type pos ty in
      match dimens with
         [dimen] ->
            let dimen = build_atom dimen in
            let a = build_atom a in
            let e = build_exp env e in
               make_exp loc (LetAlloc (v, AllocVArray (ty, WordIndex, dimen, a), e))
       | _ ->
            raise (FirException (pos, Fj_fir.StringError "multi-dimensional array not implemented"))

and build_subscript_exp env pos v ty a1 a2 e =
   let pos = string_pos "build_subscript_exp" pos in
   let env = env_add_var env v ty in
   let subop = tuple_sub_of_type env pos ty in
   let ty = build_type pos ty in
   let v1 = var_of_atom pos a1 in
   let a2 = build_atom a2 in
   let e = build_exp env e in
   let subop =
      { sub_block = TupleSub;
        sub_value = subop;
        sub_index = WordIndex;
        sub_script = sub_script
      }
   in
      make_exp loc (LetSubscript (subop, v, ty, AtomVar v1, a2, e))

and build_set_subscript_exp env pos a1 a2 ty a3 e =
   let pos = string_pos "build_set_subscript_exp" pos in
   let subop = tuple_sub_of_type env pos ty in
   let v1 = var_of_atom pos a1 in
   let a2 = build_atom a2 in
   let ty = build_type pos ty in
   let a3 = build_atom a3 in
   let e = build_exp env e in
   let subop =
      { sub_block = TupleSub;
        sub_value = subop;
        sub_index = WordIndex;
        sub_script = sub_script
      }
   in
   let label = new_symbol_string "set_subscript" in
      make_exp loc (SetSubscript (subop, label, AtomVar v1, a2, ty, a3, e))

(*
 * Records.
 *)
and build_record_exp env pos v ty rclass fields e =
   let pos = string_pos "build_record_exp" pos in
   let env = env_add_var env v ty in
   let ty = build_type pos ty in
   let tclass = tuple_class_of_record_class rclass in
   let fields = FieldTable.to_list fields in
   let fields = List.map (fun (_, a) -> build_atom a) fields in
   let e = build_exp env e in
      make_exp loc (LetAlloc (v, AllocTuple (tclass, [], ty, fields), e))

and build_project_exp env pos v ty a label e =
   let pos = string_pos "build_project_exp" pos in
   let env = env_add_var env v ty in
   let ty' = build_type pos ty in
   let v1 = var_of_atom pos a in
   let e = build_exp env e in

   (* Get the index *)
   let ty_record = env_lookup_var env pos v1 in
   let ty_record = unfold_exists_type env pos v1 ty_record in
   let ty_record = unfold_object_type env pos ty_record in
   let rclass, ty_fields = dest_record_type env pos ty_record in
   let subop = subop_of_record_op env pos rclass ty in
   let index = index_of_field env pos ty_fields label in
   let index = AtomRawInt (Rawint.of_int Rawint.Int32 true index) in
      make_exp loc (LetSubscript (subop, v, ty', AtomVar v1, index, e))

and build_set_project_exp env pos a1 label ty a2 e =
   let pos = string_pos "build_project_exp" pos in
   let v1 = var_of_atom pos a1 in
   let ty' = build_type pos ty in
   let a2 = build_atom a2 in
   let e = build_exp env e in

   (* Get the index *)
   let ty_record = env_lookup_var env pos v1 in
   let ty_record = unfold_exists_type env pos v1 ty_record in
   let ty_record = unfold_object_type env pos ty_record in
   let rclass, ty_fields = dest_record_type env pos ty_record in
   let subop = subop_of_record_op env pos rclass ty in
   let index = index_of_field env pos ty_fields label in
   let index = AtomRawInt (Rawint.of_int Rawint.Int32 true index) in
   let label = new_symbol_string "set_project" in
      make_exp loc (SetSubscript (subop, label, AtomVar v1, index, ty', a2, e))

(*
 * Build a function.
 *)
let build_fun name env f (_, ty, vars, body) =
   let pos = string_pos "build_fun" (vexp_pos f) in
   let _, ty_vars, _ = dest_fun_or_method_type env pos ty in
   let env = List.fold_left2 env_add_var env vars ty_vars in
   let ty = build_type pos ty in
   let body = build_exp env body in
      loc, [], ty, vars, body

(************************************************************************
 * GLOBALS
 ************************************************************************)

(*
 * Build a global initializer.
 *)
let build_global v (ty, init) =
   let pos = string_pos "build_global" (vexp_pos v) in
   let ty = build_type pos ty in
   let init =
      match init with
         Fj_fir.InitString s ->
            let len = String.length s in
            let a = Array.init len (fun i -> Char.code s.[i]) in
               InitRawData (Rawint.Int8, a)
       | Fj_fir.InitRecord (rclass, args) ->
            let tclass = tuple_class_of_record_class rclass in
            let args = FieldTable.to_list args in
            let args = List.map (fun (_, a) -> build_atom a) args in
            let init = InitAlloc (AllocTuple (tclass, [], ty, args)) in
               init
       | Fj_fir.InitNames (v, names) ->
            InitNames (v, names)
   in
      ty, init

(*
 * Type names.
 *)
let build_name v ty =
   let pos = string_pos "build_name" (vexp_pos v) in
      build_type pos ty

(* Imports/Exports *)
let import_args_of_ty pos ty =
   let arg_types = match ty with
      Fj_fir.TyFun (arg_types, _) ->
         arg_types
    | _ ->
         raise (FirException (pos, Fj_fir.StringTypeError
            ("Imported function doesn't have function type", ty)))
   in
   let convert = function
      Fj_fir.TyInt ->
         ArgRawInt (Rawint.Int32, true)
    | Fj_fir.TyFloat ->
         ArgRawFloat Rawfloat.Double
    | Fj_fir.TyFun _
    | Fj_fir.TyMethod _ ->
         ArgFunction
    | _ ->
         ArgPointer
   in
      List.map convert arg_types

let build_imports imports =
   let { Fj_fir.imp_types = types;
         Fj_fir.imp_tynames = tynames;
         Fj_fir.imp_funs = funs;
         Fj_fir.imp_globals = globals
       } = imports
   in
   let imp = SymbolTable.empty in

   (* No imported/exported types *)

   (* let imp = SymbolTable.fold
         (fun imp v (v_ext, ty) ->
            let pos = string_pos "build_import_types" (vexp_pos v) in
            let entry = {
               import_name = string_of_symbol v_ext;
               import_type = build_type pos ty;
               import_info = ImportGlobal }
            in
               SymbolTable.add imp v entry
         ) imp types
   in
   let imp = SymbolTable.fold
         (fun imp v (v_ext, ty) ->
            let entry = {
               import_name = string_of_symbol v_ext;
               import_type = build_name v ty;
               import_info = ImportGlobal }
            in
               SymbolTable.add imp v entry
         ) imp tynames
   in *)
   let imp = SymbolTable.fold
         (fun imp v (v_ext, init) ->
            let entry = {
               import_name = string_of_symbol v_ext;
               import_type = fst (build_global v init);
               import_info = ImportGlobal }
            in
               SymbolTable.add imp v entry
         ) imp globals
   in
   let imp = SymbolTable.fold
         (fun imp v (v_ext, (_, ty, arg_list)) ->
            let pos = string_pos "build_import_funs" (vexp_pos v) in
            let entry = {
               import_name = string_of_symbol v_ext;
               import_type = build_type pos ty;
               import_info = ImportFun (false, import_args_of_ty pos ty) }
            in
               SymbolTable.add imp v entry
         ) imp funs
   in
      imp

let build_exports exports =
   let { Fj_fir.imp_types = types;
         Fj_fir.imp_tynames = tynames;
         Fj_fir.imp_funs = funs;
         Fj_fir.imp_globals = globals
       } = exports
   in
   let exprts = SymbolTable.empty in

   (* Jason says there are no exported types/tynames *)

   (*let exprts = SymbolTable.fold
         (fun exprts v (v_ext, ty) ->
            let pos = string_pos "build_export_types" (vexp_pos v) in
            let entry = {
               export_name = string_of_symbol v_ext;
               export_type = build_type pos ty }
            in
               SymbolTable.add exprts v entry
         ) exprts types
   in*)
   (*let exprts = SymbolTable.fold
         (fun exprts v (v_ext, ty) ->
            let entry = {
               export_name = string_of_symbol v_ext;
               export_type = build_name v ty }
            in
               SymbolTable.add exprts v entry
         ) exprts tynames
   in*)
   let exprts = SymbolTable.fold
         (fun exprts v (v_ext, init) ->
            let entry = {
               export_name = string_of_symbol v_ext;
               export_type = fst (build_global v init) }
            in
               SymbolTable.add exprts v entry
         ) exprts globals
   in
   let exprts = SymbolTable.fold
         (fun exprts v (v_ext, (_, ty, arg_list)) ->
            let pos = string_pos "build_export_funs" (vexp_pos v) in
            let entry = {
               export_name = string_of_symbol v_ext;
               export_type = build_type pos ty }
            in
               SymbolTable.add exprts v entry
         ) exprts funs
   in
      exprts


(************************************************************************
 * GLOBAL FUNS
 ************************************************************************)

(*
 * Convert the program.
 *)
let build_prog prog name =
   let { Fj_fir.prog_types = types;
         Fj_fir.prog_tynames = tynames;
         Fj_fir.prog_funs = funs;
         Fj_fir.prog_main = main;
         Fj_fir.prog_globals = globals;
         Fj_fir.prog_exports = exports;
         Fj_fir.prog_imports = imports
       } = prog
   in
   let env = env_of_prog prog in

   (* Convert the types *)
   let types = SymbolTable.mapi build_tydef types in

   (* Build globals *)
   let globals = SymbolTable.mapi build_global globals in

   (* Build funs *)
   let funs = SymbolTable.mapi (build_fun name env) funs in

   (* Build names *)
   let names = SymbolTable.mapi build_name tynames in

   (* Export the global fun *)
   (* let exports =
      let pos = vexp_pos main in
      let ty = env_lookup_var env pos main in
      let ty = build_type pos ty in
      let export = { export_name = "main"; export_type = ty } in
         SymbolTable.add SymbolTable.empty main export
   in *)

   (* Convert the exports *)
   let exports = build_exports exports in

   (* Convert the imports *)
   let imports = build_imports imports in

      (* The FIR program *)
      { prog_file = { file_dir = "."; file_name = name; file_class = FileJava };
        prog_types = types;
        prog_import = imports;
        prog_export = exports;
        prog_names = names;
        prog_globals = globals;
        prog_funs = funs;
        prog_frames = SymbolTable.empty
      }

(*
 * Compilation.
 *)
let compile name =
   (* Generate the AST *)
   let prog = Fj_ast_compile.compile name in

   (* Generate the IR *)
   let prog = Fj_ir_compile.compile prog (Filename.chop_extension
               (Filename.basename name)) in

   (* Generate the FIR *)
   let prog = Fj_fir_compile.compile prog in

   (* Generate the FIR *)
   let prog = build_prog prog name in
      prog

let compile name =
   Fj_fir_exn.catch compile name

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
