(*
 * These are some useful utilities on IR types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Field_table
open Symbol

open Fj_fir
open Fj_fir_env
open Fj_fir_exn
open Fj_fir_subst

(*
 * Apply the type.
 *)
let apply_type env pos v tyl =
   match env_lookup_type env pos v, tyl with
      ty, [] ->
         ty
    | TyLambda (vars, ty), _ ->
         if vars = [] then
            ty
         else
            let tvenv =
               let len1 = List.length vars in
               let len2 = List.length tyl in
                  if len1 <> len2 then
                     raise (FirException (pos, ArityMismatch (len1, len2)));
                  List.fold_left2 subst_add subst_empty vars tyl
            in
               subst_type tvenv ty
    | ty, _ ->
         raise (FirException (pos, StringTypeError ("not a lambda", ty)))

(*
 * Expand the outermost identifiers in a type.
 *)
let rec expand_type env pos ty =
   match ty with
      TyApply (v, tyl) ->
         expand_type env pos (apply_type env pos v tyl)
    | TyAll ([], ty)
    | TyExists ([], ty)
    | TyLambda ([], ty) ->
         expand_type env pos ty
    | _ ->
         ty

(*
 * Default atom in a type.
 * We assume identifiers refer to non-base types.
 *)
let rec default_atom ty =
   match ty with
      TyUnit ->
         AtomUnit
    | TyBool ->
         AtomBool false
    | TyChar ->
         AtomChar '\000'
    | TyInt ->
         AtomInt 0
    | TyFloat ->
         AtomFloat 0.0
    | TyNil
    | TyString
    | TyArray _
    | TyFun _
    | TyMethod _
    | TyRecord _
    | TyApply _
    | TyNames _
    | TyVar _
    | TyProject _
    | TyObject _ ->
         AtomNil
    | TyAll (_, ty)
    | TyExists (_, ty)
    | TyLambda (_, ty) ->
         default_atom ty
    | TyVoid ->
         raise (Invalid_argument "Fj_fir_type.default_atom")

(*
 * Atom should be a var.
 *)
let var_of_atom pos a =
   match a with
      AtomVar v ->
         v
    | AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         raise (FirException (pos, StringAtomError ("not a variable", a)))

(*
 * Type predicates.
 *)
let is_unit_type tenv pos ty =
   match expand_type tenv pos ty with
      TyUnit ->
         true
    | _ ->
         false

let is_fun_type tenv pos ty =
   match expand_type tenv pos ty with
      TyFun _ ->
         true
    | _ ->
         false

let is_method_type tenv pos ty =
   match expand_type tenv pos ty with
      TyMethod _ ->
         true
    | _ ->
         false

let is_names_type env pos ty =
   match expand_type env pos ty with
      TyNames _ ->
         true
    | _ ->
         false

let is_record_type env pos ty =
   match expand_type env pos ty with
      TyRecord _ ->
         true
    | _ ->
         false

let is_class_type env pos ty =
   match expand_type env pos ty with
      TyRecord (RecordClass, _) ->
         true
    | _ ->
         false

let is_fields_type env pos ty =
   match expand_type env pos ty with
      TyRecord (RecordFields, _) ->
         true
    | _ ->
         false

let is_vobject_type env pos ty =
   match expand_type env pos ty with
      TyRecord (RecordVObject, _) ->
         true
    | _ ->
         false

let is_object_type env pos ty =
   match expand_type env pos ty with
      TyObject _ ->
         true
    | _ ->
         false

(*
 * Eliminate an outermost existential type.
 *)
let rec unfold_exists_type env pos v ty =
   let pos = string_pos "unfold_exists_type" pos in
      match expand_type env pos ty with
         TyExists (vars, ty) ->
            let subst, _ =
               List.fold_left (fun (subst, i) v' ->
                     let subst = subst_add subst v' (TyProject (v, i)) in
                        subst, succ i) (subst_empty, 0) vars
            in
               unfold_exists_type env pos v (subst_type subst ty)
       | _ ->
            ty

(*
 * Get the parts of a function type.
 *)
let rec dest_all_type tenv pos ty =
   let pos = string_pos "dest_all_type" pos in
      match expand_type tenv pos ty with
         TyAll (vars, ty) ->
            let vars', ty = dest_all_type tenv pos ty in
               vars @ vars', ty
       | _ ->
            [], ty

let dest_fun_type tenv pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match expand_type tenv pos ty with
         TyFun (ty_args, ty_res) ->
            ty_args, ty_res
       | ty ->
            raise (FirException (pos, StringTypeError ("not a function type", ty)))

let dest_method_type tenv pos ty =
   let pos = string_pos "dest_method_type" pos in
      match expand_type tenv pos ty with
         TyMethod (ty_this, ty_args, ty_res) ->
            ty_this, ty_args, ty_res
       | ty ->
            raise (FirException (pos, StringTypeError ("not a method type", ty)))

let rec dest_fun_or_method_type tenv pos ty =
   let pos = string_pos "dest_fun_or_method_type" pos in
      match expand_type tenv pos ty with
         TyFun (ty_args, ty_res) ->
            [], ty_args, ty_res
       | TyMethod (ty_this, ty_args, ty_res) ->
            [], ty_this :: ty_args, ty_res
       | TyAll (vars, ty) ->
            let vars', ty_vars, ty_res = dest_fun_or_method_type tenv pos ty in
               vars @ vars', ty_vars, ty_res
       | ty ->
            raise (FirException (pos, StringTypeError ("not a function or method type", ty)))

(*
 * Get the array type.
 *)
let dest_array_type env pos ty =
   let pos = string_pos "dest_array_type" pos in
      match expand_type env pos ty with
         TyArray ty ->
            ty
       | ty ->
            raise (FirException (pos, StringTypeError ("not an array type", ty)))

(*
 * Get the names in the Name type.
 *)
let dest_names_type env pos ty =
   let pos = string_pos "dest_names_type" pos in
      match expand_type env pos ty with
         TyNames (v, names) ->
            v, names
       | ty ->
            raise (FirException (pos, StringTypeError ("not a names type", ty)))

(*
 * Get the parts of a record.
 *)
let dest_record_type env pos ty =
   let pos = string_pos "dest_record_type" pos in
      match expand_type env pos ty with
         TyRecord (rclass, ty_fields) ->
            rclass, ty_fields
       | ty ->
            raise (FirException (pos, StringTypeError ("not a record type", ty)))

(*
 * Get the fields in a normal record type.
 *)
let dest_frame_type env pos ty =
   let pos = string_pos "dest_frame_type" pos in
   let rclass, ty_fields = dest_record_type env pos ty in
      if rclass <> RecordFrame then
         raise (FirException (pos, StringTypeError ("not a frame type", ty)));
      ty_fields

(*
 * Get the fields in a normal record type.
 *)
let dest_closure_type env pos ty =
   let pos = string_pos "dest_closure_type" pos in
   let rclass, ty_fields = dest_record_type env pos ty in
      if rclass <> RecordClosure then
         raise (FirException (pos, StringTypeError ("not a closure type", ty)));
      ty_fields

(*
 * Get the fields of a class type.
 *)
let dest_class_type env pos ty =
   let pos = string_pos "dest_class_type" pos in
   let rclass, ty_fields = dest_record_type env pos ty in
      if rclass <> RecordClass then
         raise (FirException (pos, StringTypeError ("not a class type", ty)));
      ty_fields

(*
 * Get the fields of a methods type.
 *)
let dest_methods_type env pos ty =
   let pos = string_pos "dest_methods_type" pos in
   let rclass, ty_fields = dest_record_type env pos ty in
      if rclass <> RecordMethods then
         raise (FirException (pos, StringTypeError ("not a methods type", ty)));
      ty_fields

(*
 * Get the fields in a Object type.
 *)
let dest_fields_type env pos ty =
   let pos = string_pos "dest_object_type" pos in
   let rclass, ty_fields = dest_record_type env pos ty in
      if rclass <> RecordFields then
         raise (FirException (pos, StringTypeError ("not an fields type", ty)));
      ty_fields

(*
 * Get the fields in a VObject type.
 *)
let dest_vobject_type env pos ty =
   let pos = string_pos "dest_vobject_type" pos in
   let rclass, ty_fields = dest_record_type env pos ty in
      if rclass <> RecordVObject then
         raise (FirException (pos, StringTypeError ("not a vobject type", ty)));
      ty_fields

(*
 * Recursively unfold the object type.
 *)
let dest_object_type env pos ty =
   let pos = string_pos "dest_object_type" pos in
      match expand_type env pos ty with
         TyObject (v, ty_fields) ->
            let subst = subst_add subst_empty v ty in
               subst_type subst ty_fields
       | ty ->
            raise (FirException (pos, StringTypeError ("not an object type", ty)))

let unfold_object_type env pos ty =
   let pos = string_pos "unfold_object_type" pos in
      match expand_type env pos ty with
         TyObject (v, ty_fields) ->
            let subst = subst_add subst_empty v ty in
               subst_type subst ty_fields
       | _ ->
            ty

(*
 * Get the type of a field in a record type.
 *)
let type_of_field env pos fields v =
   let pos = string_pos "dest_field_type" pos in
      try FieldTable.find fields v with
         Not_found ->
            raise (FirException (pos, UnboundLabel v))

let index_of_field env pos fields v =
   let pos = string_pos "dest_field_type" pos in
      try FieldTable.find_index fields v with
         Not_found ->
            raise (FirException (pos, UnboundLabel v))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
