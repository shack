(*
 * IR exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Fj_fir

(*
 * "Position" information describing an exception.
 *)
type fir_pos

(*
 * The general IR exception.
 *)
exception FirException of fir_pos * fir_exn

(*
 * Ways of making positions.
 *)
val ast_pos : Fj_ast.pos -> fir_pos
val ast_exp_pos : Fj_ast.expr -> fir_pos
val ir_exp_pos : Fj_ir.exp -> fir_pos
val fir_exp_pos : exp -> fir_pos
val string_pos : string -> fir_pos -> fir_pos
val vexp_pos : var -> fir_pos
val fir_exn_pos : fir_exn -> fir_pos -> fir_pos
val int_pos : int -> fir_pos -> fir_pos

(*
 * Exception printer.
 *)
val pp_print_exn : formatter -> exn -> unit

(*
 * Exception handler.
 *)
val catch : ('a -> 'b) -> 'a -> 'b

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
