(*
 * This is the main wrapper for compiling the IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Fj_fir_print
open Fj_fir_state
open Fj_fir_check

(*
 * Print the program if desired.
 *)
let print_prog s prog =
   if !print_fir then
      fprintf err_formatter "*** FJFIR: %s@.%a@." s pp_print_prog prog;
   if !check_fir then
      check_prog prog

(*
 * The argument is an AST program.
 *)
let compile prog =
   (* Build the FIR *)
   let prog = Fj_fir_ir.build_prog prog in
   let _ = print_prog "Build" prog in

   (* Closure conversion *)
   let prog = Fj_fir_closure.close_prog prog in
   let _ = print_prog "Closure" prog in

   (* Lift function definitions *)
   let prog = Fj_fir_prog.lift_prog prog in
   let _ = print_prog "Prog" prog in

   (* Deadcode elimination *)
   let prog = Fj_fir_dead.dead_prog prog in
   let _ = print_prog "Dead" prog in

   (* Function inlining and constant folding *)
   let prog = Fj_fir_inline.inline_prog prog in
   let _ = print_prog "Inline" prog in

   (* Deadcode elimination *)
   let prog = Fj_fir_dead.dead_prog prog in
   let _ = print_prog "Dead" prog in
      prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
