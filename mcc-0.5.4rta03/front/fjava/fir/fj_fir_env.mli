(*
 * Type and variable environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fj_fir
open Fj_fir_exn

type env

(*
 * Empty env.
 *)
val env_empty : env

(*
 * Get the initial environment from the prog.
 *)
val env_of_prog : prog -> env

(*
 * The Object type.
 *)
val env_object_type : env -> ty

(*
 * Types.
 *)
val env_add_type : env -> var -> ty -> env
val env_lookup_type : env -> fir_pos -> var -> ty

(*
 * Vars.
 *)
val env_add_var : env -> var -> ty -> env
val env_lookup_var : env -> fir_pos -> var -> ty
val env_fold_var : ('a -> var -> ty -> 'a) -> 'a -> env -> 'a

(*
 * Type names.
 *)
val env_lookup_tyname : env -> fir_pos -> var -> ty

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
