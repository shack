(*
 * Rename all the variables in a program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Fj_fir
open Fj_fir_exn

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = var SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

let env_add_vars env vars vars' =
   List.fold_left2 env_add env vars vars'

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

let env_lookup_opt env v =
   match v with
      Some v ->
         Some (env_lookup env v)
    | None ->
         None

let env_lookup_list env l =
   List.map (env_lookup env) l

(*
 * Check if a variable is defined.
 *)
let env_mem env v =
   SymbolTable.mem env v

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv venv ty =
   match ty with
      TyVoid
    | TyUnit
    | TyNil
    | TyBool
    | TyChar
    | TyString
    | TyInt
    | TyFloat ->
         ty
    | TyArray ty ->
         TyArray (standardize_type tenv venv ty)
    | TyFun (ty_vars, ty_res) ->
         TyFun (List.map (standardize_type tenv venv) ty_vars, standardize_type tenv venv ty_res)
    | TyMethod (ty_this, ty_vars, ty_res) ->
         TyMethod (standardize_type tenv venv ty_this,
                   List.map (standardize_type tenv venv) ty_vars,
                   standardize_type tenv venv ty_res)
    | TyRecord (rclass, tyl) ->
         TyRecord (rclass, FieldTable.map (fun ty -> standardize_type tenv venv ty) tyl)
    | TyNames (v, names) ->
         let v = env_lookup tenv v in
         let names =
            List.map (fun (v, ty_opt) ->
                  env_lookup venv v, standardize_type_opt tenv venv ty_opt) names
         in
            TyNames (v, names)
    | TyApply (v, tyl) ->
         TyApply (env_lookup tenv v, List.map (standardize_type tenv venv) tyl)
    | TyVar v ->
         TyVar (env_lookup tenv v)
    | TyProject (v, i) ->
         TyProject (env_lookup venv v, i)
    | TyAll (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = env_add_vars tenv vars vars' in
         let ty = standardize_type tenv venv ty in
            TyAll (vars', ty)
    | TyExists (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = env_add_vars tenv vars vars' in
         let ty = standardize_type tenv venv ty in
            TyExists (vars', ty)
    | TyLambda (vars, ty) ->
         let vars' = List.map new_symbol vars in
         let tenv = env_add_vars tenv vars vars' in
         let ty = standardize_type tenv venv ty in
            TyLambda (vars', ty)
    | TyObject (v, ty) ->
         let v' = new_symbol v in
         let env = env_add tenv v v' in
         let ty = standardize_type tenv venv ty in
            TyObject (v', ty)

and standardize_type_opt tenv venv ty_opt =
   match ty_opt with
      Some ty ->
         Some (standardize_type tenv venv ty)
    | None ->
         None

(*
 * Rename the vars in an atom.
 *)
let standardize_atom tenv venv atom =
   match atom with
      AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         atom
    | AtomVar v ->
         AtomVar (env_lookup venv v)

let standardize_atoms tenv venv atoms =
   List.map (standardize_atom tenv venv) atoms

let standardize_fields tenv venv fields =
   FieldTable.map (fun a -> standardize_atom tenv venv a) fields

(*
 * Rename all the vars in the expr.
 *)
let rec standardize_exp tenv venv e =
   match e with
      LetFuns (funs, e) ->
         standardize_funs_exp tenv venv funs e
    | LetAtom (v, ty, a, e) ->
         standardize_atom_exp tenv venv v ty a e
    | LetVar (v, ty, a, e) ->
         standardize_var_exp tenv venv v ty a e
    | LetUnop (v, ty, op, a, e) ->
         standardize_unop_exp tenv venv v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         standardize_binop_exp tenv venv v ty op a1 a2 e
    | LetExt (v, ty1, s, ty2, args, e) ->
         standardize_ext_exp tenv venv v ty1 s ty2 args e
    | TailCall (f, args) ->
         standardize_tailcall_exp tenv venv f args
    | MethodCall (f, a, args) ->
         standardize_methodcall_exp tenv venv f a args
    | IfThenElse (a, e1, e2) ->
         standardize_if_exp tenv venv a e1 e2
    | IfType (a, cname, v, e1, e2) ->
         standardize_iftype_exp tenv venv a cname v e1 e2
    | SetVar (v, ty, a, e) ->
         standardize_set_var_exp tenv venv v ty a e
    | LetSubscript (v, ty, a1, a2, e) ->
         standardize_subscript_exp tenv venv v ty a1 a2 e
    | SetSubscript (a1, a2, ty, a3, e) ->
         standardize_set_subscript_exp tenv venv a1 a2 ty a3 e
    | LetProject (v, ty, a, label, e) ->
         standardize_project_exp tenv venv v ty a label e
    | SetProject (a1, label, ty, a2, e) ->
         standardize_set_project_exp tenv venv a1 label ty a2 e
    | LetArray (v, ty, args, a, e) ->
         standardize_array_exp tenv venv v ty args a e
    | LetRecord (v, ty, rclass, args, e) ->
         standardize_record_exp tenv venv v ty rclass args e

(*
 * Rename functions and arguments.
 *)
and standardize_fun tenv venv funs (f, (gflag, f_ty, vars, body)) =
   (* Rename the function *)
   let f' = env_lookup venv f in
   let f_ty = standardize_type tenv venv f_ty in

   (* Rename the vars *)
   let venv, vars' =
      List.fold_left (fun (venv, vars') v ->
            let v' = new_symbol v in
            let venv = env_add venv v v' in
               venv, v' :: vars') (venv, []) vars
   in
   let vars' = List.rev vars' in

   (* Rename the body *)
   let body = standardize_exp tenv venv body in
      (f', (gflag, f_ty, vars', body)) :: funs

and standardize_funs tenv venv funs =
   (* Make up new names for the funs *)
   let venv =
      List.fold_left (fun venv (f, _) ->
            let f' = new_symbol f in
               env_add venv f f') venv funs
   in

   (* Rename the function's vars and body *)
   let funs = List.fold_left (standardize_fun tenv venv) [] funs in
      venv, List.rev funs

and standardize_funs_exp tenv venv funs e =
   (* Rename the funs *)
   let venv, funs = standardize_funs tenv venv funs in

   (* Rename the rest *)
   let e = standardize_exp tenv venv e in
      LetFuns (funs, e)

(*
 * Atom declaration.
 *)
and standardize_atom_exp tenv venv v ty a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetAtom (v', ty, a, e)

(*
 * Var declaration.
 *)
and standardize_var_exp tenv venv v ty a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetVar (v', ty, a, e)

(*
 * Unary operation.
 *)
and standardize_unop_exp tenv venv v ty op a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetUnop (v', ty, op, a, e)

(*
 * Binary operation.
 *)
and standardize_binop_exp tenv venv v ty op a1 a2 e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetBinop (v', ty, op, a1, a2, e)

(*
 * External function application.
 *)
and standardize_ext_exp tenv venv v ty1 s ty2 args e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let args = standardize_atoms tenv venv args in
   let e = standardize_exp tenv venv' e in
   let ty1 = standardize_type tenv venv ty1 in
   let ty2 = standardize_type tenv venv ty2 in
      LetExt (v', ty1, s, ty2, args, e)

(*
 * Tail call.
 *)
and standardize_tailcall_exp tenv venv f args =
   let f = env_lookup venv f in
   let args = standardize_atoms tenv venv args in
      TailCall (f, args)

and standardize_methodcall_exp tenv venv f a args =
   let f = env_lookup venv f in
   let a = standardize_atom tenv venv a in
   let args = standardize_atoms tenv venv args in
      MethodCall (f, a, args)

(*
 * Conditional expression.
 *)
and standardize_if_exp tenv venv a e1 e2 =
   let a  = standardize_atom tenv venv a in
   let e1 = standardize_exp tenv venv e1 in
   let e2 = standardize_exp tenv venv e2 in
      IfThenElse (a, e1, e2)

and standardize_iftype_exp tenv venv a cname v e1 e2 =
   let a  = standardize_atom tenv venv a in
   let e2 = standardize_exp tenv venv e2 in
   let cname = env_lookup venv cname in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e1 = standardize_exp tenv venv e1 in
      IfType (a, cname, v', e1, e2)

(*
 * Set a variable.
 *)
and standardize_set_var_exp tenv venv v ty a e =
   let v = env_lookup venv v in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv e in
   let ty = standardize_type tenv venv ty in
      SetVar (v, ty, a, e)

(*
 * Array operations.
 *)
and standardize_subscript_exp tenv venv v ty a1 a2 e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetSubscript (v', ty, a1, a2, e)

and standardize_set_subscript_exp tenv venv a1 a2 ty a3 e =
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let a3 = standardize_atom tenv venv a3 in
   let e = standardize_exp tenv venv e in
   let ty = standardize_type tenv venv ty in
      SetSubscript (a1, a2, ty, a3, e)

(*
 * Record projections.
 *)
and standardize_project_exp tenv venv v ty a label e =
   let a = standardize_atom tenv venv a in
   let ty = standardize_type tenv venv ty in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetProject (v', ty, a, label, e)

and standardize_set_project_exp tenv venv a1 label ty a2 e =
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let e = standardize_exp tenv venv e in
   let ty = standardize_type tenv venv ty in
      SetProject (a1, label, ty, a2, e)

(*
 * Allocation.
 *)
and standardize_array_exp tenv venv v ty args a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let e = standardize_exp tenv venv' e in
   let args = standardize_atoms tenv venv args in
   let a = standardize_atom tenv venv a in
   let ty = standardize_type tenv venv ty in
      LetArray (v', ty, args, a, e)

and standardize_record_exp tenv venv v ty rclass args e =
   let args = standardize_fields tenv venv args in
   let ty = standardize_type tenv venv ty in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetRecord (v', ty, rclass, args, e)

(*
 * A global initializer.
 *)
let standardize_init tenv venv (ty, init) =
   let ty = standardize_type tenv venv ty in
   let init =
      match init with
         InitString _ ->
            init
       | InitRecord (rclass, args) ->
            InitRecord (rclass, standardize_fields tenv venv args)
       | InitNames (v, names) ->
            let v = env_lookup tenv v in
            let names =
               List.map (fun (v, v_opt) ->
                     let v_opt =
                        match v_opt with
                           Some v -> Some (env_lookup venv v)
                         | None -> None
                     in
                        env_lookup venv v, v_opt) names
            in
               InitNames (v, names)
   in
      ty, init

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * A program.
 *)
let standardize_prog prog =
   let { prog_types = types;
         prog_tynames = tynames;
         prog_funs = funs;
         prog_main = main;
         prog_object = v_object;
         prog_globals = globals;
         prog_imports = imports;
         prog_exports = exports;
       } = prog
   in
   let tenv = env_empty in
   let venv = env_empty in

   let stdize_symtable env table = 
      SymbolTable.fold (fun (env, table) key def ->
            let key' = new_symbol key in
            let env = env_add env key key' in
            let table = SymbolTable.add table key' def in
               env, table) (env, SymbolTable.empty) table
   in

   (* First, give a new name to all the funs *)
   let venv, funs = stdize_symtable venv funs in
   let venv, ifuns = stdize_symtable venv imports.imp_funs in

   (* Give new names to all the globals *)
   let venv, globals = stdize_symtable venv globals in
   let venv, iglobals = stdize_symtable venv imports.imp_globals in

   (* Give new names to all the type names *)
   let venv, tynames = stdize_symtable venv tynames in
   let venv, itynames = stdize_symtable venv imports.imp_tynames in

   (* Give new names to all the types *)
   let tenv, types = stdize_symtable tenv types in
   let tenv, itypes = stdize_symtable tenv imports.imp_types in

   (* Now rename all the definitions *)
   let stdize_export f table =
      SymbolTable.fold (fun table v_int (v, def) ->
         let v_int = env_lookup venv v_int in
            SymbolTable.add table v_int (v, f def)) 
                  SymbolTable.empty table
   in
   
   (* Rename the types *)
   let types = SymbolTable.map (standardize_type tenv venv) types in
   let itypes = SymbolTable.map (fun (v, ty) ->
         v, standardize_type tenv venv ty) itypes in
   let etypes = stdize_export (standardize_type tenv venv) exports.imp_types in
   
   (* Rename the funs *)
   let funs =
      SymbolTable.map (fun (gflag, ty, vars, body) ->
            let ty = standardize_type tenv venv ty in
            let vars' = List.map new_symbol vars in
            let venv = List.fold_left2 env_add venv vars vars' in
            let body = standardize_exp tenv venv body in
               gflag, ty, vars', body) funs
   in
   let ifuns = 
      SymbolTable.map (fun (v, (gflag, ty, vars)) ->
            let ty = standardize_type tenv venv ty in
            let vars' = List.map new_symbol vars in
            let venv = List.fold_left2 env_add venv vars vars' in
               v, (gflag, ty, vars')) ifuns
   in
   let efuns = 
      stdize_export (fun (gflag, ty, vars) ->
            let ty = standardize_type tenv venv ty in
            let vars' = List.map new_symbol vars in
            let venv = List.fold_left2 env_add venv vars vars' in
               gflag, ty, vars') exports.imp_funs
   in

   (* Rename the globals *)
   let globals =
      SymbolTable.map (fun init ->
            standardize_init tenv venv init) globals
   in
   let iglobals =
      SymbolTable.map (fun (v, init) ->
            v, standardize_init tenv venv init) iglobals
   in
   let eglobals =
      stdize_export (fun init ->
            standardize_init tenv venv init) exports.imp_globals
   in

   (* Rename the tynames *)
   let tynames =
      SymbolTable.map (standardize_type tenv venv) tynames
   in
   let itynames =
      SymbolTable.map (fun (v, tyname) -> 
            v, standardize_type tenv venv tyname) itynames
   in
   let etynames =
      stdize_export (standardize_type tenv venv) exports.imp_tynames
   in
   
   (* Put it all back together again *)
      { prog_types = types;
        prog_tynames = tynames;
        prog_funs = funs;
        prog_main = env_lookup venv main;
        prog_object = env_lookup tenv v_object;
        prog_globals = globals;
        prog_imports = { imp_types   = itypes;
                         imp_tynames = itynames;
                         imp_funs    = ifuns;
                         imp_globals = iglobals
                       };
        prog_exports = { imp_types   = etypes;
                         imp_tynames = etynames;
                         imp_funs    = efuns;
                         imp_globals = eglobals
                       }
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
