(*
 * Some useful utility functions on types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Field_table

open Fj_fir
open Fj_fir_exn
open Fj_fir_env

(*
 * Expand the outermost type identifier.
 *)
val apply_type : env -> fir_pos -> ty_var -> ty list -> ty
val expand_type : env -> fir_pos -> ty -> ty

(*
 * Default atom in a type.
 * Assumes that TyId refer to non-base-types.
 *)
val default_atom : ty -> atom
val var_of_atom : fir_pos -> atom -> var

(*
 * Type predicates.
 *)
val is_fun_type : env -> fir_pos -> ty -> bool
val is_method_type : env -> fir_pos -> ty -> bool
val is_names_type : env -> fir_pos -> ty -> bool
val is_record_type : env -> fir_pos -> ty -> bool
val is_class_type : env -> fir_pos -> ty -> bool
val is_fields_type : env -> fir_pos -> ty -> bool
val is_vobject_type : env -> fir_pos -> ty -> bool

val is_object_type : env -> fir_pos -> ty -> bool

(*
 * Basic type operations.
 *)
val dest_all_type : env -> fir_pos -> ty -> ty_var list * ty
val dest_fun_type : env -> fir_pos -> ty -> ty list * ty
val dest_method_type : env -> fir_pos -> ty -> ty * ty list * ty
val dest_fun_or_method_type : env -> fir_pos -> ty -> var list * ty list * ty
val dest_array_type : env -> fir_pos -> ty -> ty
val dest_names_type : env -> fir_pos -> ty -> var * (var * ty option) list

val dest_record_type : env -> fir_pos -> ty -> record_class * field_types
val dest_frame_type : env -> fir_pos -> ty -> field_types
val dest_closure_type : env -> fir_pos -> ty -> field_types
val dest_class_type : env -> fir_pos -> ty -> field_types
val dest_methods_type : env -> fir_pos -> ty -> field_types
val dest_fields_type : env -> fir_pos -> ty -> field_types
val dest_vobject_type : env -> fir_pos -> ty -> field_types

val dest_object_type : env -> fir_pos -> ty -> ty
val unfold_object_type : env -> fir_pos -> ty -> ty

(*
 * Polymorphism.
 *)
val unfold_exists_type : env -> fir_pos -> var -> ty -> ty

(*
 * Field operations.
 *)
val type_of_field : env -> fir_pos -> field_types -> var -> ty
val index_of_field : env -> fir_pos -> field_types -> var -> int

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
