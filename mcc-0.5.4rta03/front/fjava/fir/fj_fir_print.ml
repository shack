(*
 * FIR printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Symbol
open Field_table

open Fj_fir

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Default tabstop.
 *)
let tabstop = 3

(*
 * Print a quoted symbol.
 *)
let pp_print_quote_symbol buf v =
   fprintf buf "'%a" pp_print_symbol v

(*
 * Separated list of fields.
 *)
let pp_print_sep_list sep printer buf l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "%s@ " sep;
                 fprintf buf "@[<hv 3>%a@]" printer x;
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_pre_sep_list sep printer buf l =
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "@ %s " sep;
                 printer buf x;
                 false) true l)

(*
 * Prefixed list of items.
 *)
let pp_print_prefix_list sep printer buf l =
   pp_open_hvbox buf 0;
   List.iter (fun x ->
         fprintf buf "%s@ @[<hv 0>%a@]" sep printer x) l;
   pp_close_box buf ()

(*
 * Print a record class.
 *)
let pp_print_record_class buf rclass =
   let s =
      match rclass with
         RecordFrame -> "frame"
       | RecordClass -> "class"
       | RecordMethods -> "methods"
       | RecordFields -> "fields"
       | RecordVObject -> "vobject"
       | RecordClosure -> "closure"
   in
      pp_print_string buf s

(*
 * Print a type.
 * Use precedences to reduce the
 * amount of parentheses.
 *)
let prec_none   = 0
let prec_fun    = 1
let prec_tuple  = 2
let prec_array  = 3

let rec pp_print_type pre buf = function
   TyVoid ->
      pp_print_string buf "void"
 | TyUnit ->
      pp_print_string buf "unit"
 | TyNil ->
      pp_print_string buf "nil"
 | TyBool ->
      pp_print_string buf "bool"
 | TyChar ->
      pp_print_string buf "char"
 | TyString ->
      pp_print_string buf "string"
 | TyInt ->
      pp_print_string buf "int"
 | TyFloat ->
      pp_print_string buf "float"
 | TyArray ty ->
      fprintf buf "%a[]" (pp_print_type prec_array) ty
 | TyFun (ty_vars, ty_res) ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      fprintf buf "@[<hv 3>(@[%a@]) ->@ %a" (**)
         (pp_print_sep_list "," (pp_print_type prec_fun)) ty_vars
         (pp_print_type prec_fun) ty_res;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyMethod (ty_this, ty_vars, ty_res) ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      fprintf buf "@[<hv 3>method[%a] (@[%a@]) ->@ %a" (**)
         (pp_print_type prec_none) ty_this
         (pp_print_sep_list "," (pp_print_type prec_fun)) ty_vars
         (pp_print_type prec_fun) ty_res;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyRecord (rclass, fields) ->
      fprintf buf "@[<hv 0>@[<hv 3>%a {%a@]@ }@]" (**)
         pp_print_record_class rclass
         pp_print_field_types fields
 | TyNames (v, fields) ->
      fprintf buf "@[<hv 3>TyNames[%a] {%a }@]" pp_print_symbol v pp_print_names fields
 | TyApply (v, tyl) ->     (* How does FjObject become a TyApply?? *)
      fprintf buf "@[<hv 3>%a" pp_print_symbol v;
      (match tyl with
          [] -> ()
        | _ ->
             fprintf buf "[%a]" (pp_print_sep_list "," (pp_print_type prec_none)) tyl);
      pp_close_box buf ()

 | TyAll (vars, ty) ->
      fprintf buf "@[<hv 3>(all %a.@ %a)@]" (**)
         (pp_print_sep_list "," pp_print_quote_symbol) vars
         (pp_print_type prec_none) ty

 | TyExists (vars, ty) ->
      fprintf buf "@[<hv 3>(exists %a.@ %a)@]" (**)
         (pp_print_sep_list "," pp_print_quote_symbol) vars
         (pp_print_type prec_none) ty

 | TyLambda (vars, ty) ->
      fprintf buf "@[<hv 3>(lambda %a.@ %a)@]" (**)
         (pp_print_sep_list "," pp_print_quote_symbol) vars
         (pp_print_type prec_none) ty

 | TyVar v ->
      fprintf buf "'%a" pp_print_symbol v

 | TyProject (v, i) ->
      fprintf buf "%a.%d" pp_print_symbol v i

 | TyObject (v, ty) ->
      fprintf buf "@[<hv 3>(object '%a.@ %a)@]" (**)
         pp_print_symbol v
         (pp_print_type prec_none) ty

(*
 * Print the field list in a record.
 *)
and pp_print_field_types buf fields =
   FieldTable.iter (fun v ty ->
         fprintf buf "@ @[<hv 3>%a :@ %a@];" pp_print_symbol v (pp_print_type prec_none) ty) fields
(*
 * Print the name list in a class.
 *)
and pp_print_name buf (v, ty_opt) =
   match ty_opt with
      Some ty ->
         fprintf buf "@ @[<hv 3>%a :@ %a@];" pp_print_symbol v (pp_print_type prec_none) ty
    | None ->
         fprintf buf "@ %a;" pp_print_symbol v

and pp_print_names buf names =
   List.iter (pp_print_name buf) names

(*
 * Type list printing.
 *)
and pp_print_types buf tyl =
   List.iter (fun ty -> fprintf buf "@ %a;" (pp_print_type prec_none) ty) tyl

let pp_print_type_list =
   pp_print_sep_list "," (pp_print_type prec_none)

let pp_print_type = pp_print_type prec_none

(*
 * Atoms.
 *)
let pp_print_atom buf = function
   AtomUnit ->
      pp_print_string buf "()"
 | AtomNil ->
      pp_print_string buf "nil"
 | AtomBool b ->
      pp_print_bool buf b
 | AtomChar c ->
      pp_print_string buf ("\'" ^ Char.escaped c ^ "\'")
 | AtomInt i ->
      pp_print_int buf i
 | AtomFloat x ->
      pp_print_float buf x
 | AtomVar v ->
      pp_print_symbol buf v

let pp_print_atoms buf atoms =
   List.iter (pp_print_atom buf) atoms

(*
 * Fields in a record or object.
 *)
let pp_print_fields buf fields =
   FieldTable.iter (fun v a ->
         fprintf buf "@ %a = %a;" pp_print_symbol v pp_print_atom a) fields

(*
 * Array dimensions.
 *)
let pp_print_array_dimens buf atoms =
   List.iter (fun a -> fprintf buf "[%a]" pp_print_atom a) atoms

(*
 * Unary operations.
 *)
let pp_print_unop buf (op, a) =
   match op with
      UMinusIntOp ->
         fprintf buf "-[int] %a" pp_print_atom a
    | UMinusFloatOp ->
         fprintf buf "-[float] %a" pp_print_atom a
    | UNotIntOp ->
         fprintf buf "~%a" pp_print_atom a
    | UNotBoolOp ->
         fprintf buf "!%a" pp_print_atom a
    | UIntOfFloat ->
         fprintf buf "int_of_float(%a)" pp_print_atom a
    | UFloatOfInt ->
         fprintf buf "float_of_int(%a)" pp_print_atom a

(*
 * Binary operations.
 *)
let pp_print_binop buf (op, a1, a2) =
   let s =
      match op with
         AddIntOp -> "+"
       | SubIntOp -> "-"
       | MulIntOp -> "*"
       | DivIntOp -> "/"
       | RemIntOp -> "%"
       | AndIntOp -> "&"
       | OrIntOp  -> "|"
       | LslIntOp -> "<<"
       | LsrIntOp -> ">>>"
       | AsrIntOp -> ">>"
       | XorIntOp -> "^"
       | EqIntOp  -> "=="
       | NeqIntOp -> "!="
       | LeIntOp  -> "<="
       | LtIntOp  -> "<"
       | GtIntOp  -> ">"
       | GeIntOp  -> ">="

       | AddFloatOp -> "+."
       | SubFloatOp -> "-."
       | MulFloatOp -> "*."
       | DivFloatOp -> "/."
       | RemFloatOp -> "%."
       | EqFloatOp  -> "==."
       | NeqFloatOp -> "!=."
       | LeFloatOp  -> "<=."
       | LtFloatOp  -> "<."
       | GtFloatOp  -> ">."
       | GeFloatOp  -> ">=."

       | EqPtrOp    -> "==*"
       | NeqPtrOp   -> "!=*"
   in
      fprintf buf "%a %s %a" pp_print_atom a1 s pp_print_atom a2

(*
 * A "let" opener.
 *)
let pp_print_let_open buf (v, ty) =
   fprintf buf "@[<hv 3>let %a :@ %a =@ " (**)
      pp_print_symbol v
      pp_print_type ty

let rec pp_print_let_close depth buf e =
   fprintf buf " in@]@ %a" (pp_print_exp depth) e

(*
 * Print an expression.
 *)
and pp_print_exp depth buf e =
   if depth <= 0 then
      fprintf buf "..."
   else
      pp_print_exp_aux (pred depth) buf e

and pp_print_exp_aux depth buf = function
   LetFuns (funs, e) ->
      fprintf buf "@[<v 0>let %a in@]@ %a" (**)
         (pp_print_pre_sep_list "and" (pp_print_function depth)) funs
         (pp_print_exp depth) e

 | LetAtom (v, ty, a, e) ->
      pp_print_let_open buf (v, ty);
      pp_print_atom buf a;
      pp_print_let_close depth buf e

 | LetVar (v, ty, a, e) ->
      fprintf buf "@[<hv 3>let var %a :@ %a =@ %a in@]@ %a" (**)
         pp_print_symbol v
         pp_print_type ty
         pp_print_atom a
         (pp_print_exp depth) e

 | LetUnop (v, ty, op, a, e) ->
      fprintf buf "%a%a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_unop (op, a)
         (pp_print_let_close depth) e

 | LetBinop (v, ty, op, a1, a2, e) ->
      fprintf buf "%a%a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_binop (op, a1, a2)
         (pp_print_let_close depth) e

 | LetExt (v, ty1, s, ty2, args, e) ->
      fprintf buf "%aexternal (\"%s\" : %a)(%a)%a" (**)
         pp_print_let_open (v, ty1)
         (String.escaped s)
         pp_print_type ty2
         (pp_print_sep_list "," pp_print_atom) args
         (pp_print_let_close depth) e

 | TailCall (f, args) ->
      fprintf buf "tailcall %a(%a)" (**)
         pp_print_symbol f
         (pp_print_sep_list "," pp_print_atom) args

 | MethodCall (f, a, args) ->
      fprintf buf "methodcall %a[%a](%a)" (**)
         pp_print_symbol f
         pp_print_atom a
         (pp_print_sep_list "," pp_print_atom) args

 | IfThenElse (a, e1, e2) ->
      fprintf buf "@[@[<hv 3>if %a then@ %a@]@ @[<hv 3>else@ %a@]@]" (**)
         pp_print_atom a
         (pp_print_exp depth) e1
         (pp_print_exp depth) e2

 | IfType (a, cname, v, e1, e2) ->
      fprintf buf "@[<hv 0>@[<hv 3>iftype(%a)@ @[<hv 3>%a %a ->@ %a@]@]@ @[<hv 3>else@ %a@]@]" (**)
         pp_print_atom a
         pp_print_symbol cname
         pp_print_symbol v
         (pp_print_exp depth) e1
         (pp_print_exp depth) e2

 | SetVar (v, ty, a, e) ->
      fprintf buf "%a : %a <- %a;@ %a" (**)
         pp_print_symbol v
         pp_print_type ty
         pp_print_atom a
         (pp_print_exp depth) e

 | LetSubscript (v, ty, a1, a2, e) ->
      fprintf buf "%a%a[%a]%a" (**)
         pp_print_let_open (v, ty)
         pp_print_atom a1
         pp_print_atom a2
         (pp_print_let_close depth) e

 | SetSubscript (a1, a2, ty, a3, e) ->
      fprintf buf "%a[%a] : %a <- %a;@ %a" (**)
         pp_print_atom a1
         pp_print_atom a2
         pp_print_type ty
         pp_print_atom a3
         (pp_print_exp depth) e

 | LetProject (v, ty, a, l, e) ->
      fprintf buf "%a%a.%a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_atom a
         pp_print_symbol l
         (pp_print_let_close depth) e

 | SetProject (a1, l, ty, a2, e) ->
      fprintf buf "%a.%a : %a <- %a;@ %a" (**)
         pp_print_atom a1
         pp_print_symbol l
         pp_print_type ty
         pp_print_atom a2
         (pp_print_exp depth) e

 | LetArray (v, ty, args, a, e) ->
      fprintf buf "%anew array%a = %a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_array_dimens args
         pp_print_atom a
         (pp_print_let_close depth) e

 | LetRecord (v, ty, rclass, fields, e) ->
      fprintf buf "%a@[<v 0>@[<v 3>%a {%a@]@ }@]%a" (**)
         pp_print_let_open (v, ty)
         pp_print_record_class rclass
         pp_print_fields fields
         (pp_print_let_close depth) e

and pp_print_function_decl depth buf (f, (gflag, f_ty, vars)) =
   let s =
      match gflag with
         FunGlobalClass -> "$global"
       | FunContClass -> "$cont"
       | FunLocalClass -> "$local"
   in
      fprintf buf "%s %a(%a)@ : %a" (**)
         s
         pp_print_symbol f
         (pp_print_sep_list "," pp_print_symbol) vars
         pp_print_type f_ty

and pp_print_function depth buf (f, (gflag, f_ty, vars, body)) =
   fprintf buf "%a@ @[<hv 3>{@ %a@]@ }"
      (pp_print_function_decl depth) (f, (gflag, f_ty, vars))
      (pp_print_exp depth) body

(*
 * Print a name record.
 *)
let pp_print_name buf (v1, v_opt) =
   match v_opt with
      Some v2 ->
         fprintf buf "@ %a = %a;" pp_print_symbol v1 pp_print_symbol v2
    | None ->
         fprintf buf "@ %a;" pp_print_symbol v1

let pp_print_names buf names =
   List.iter (pp_print_name buf) names

(*
 * Print an initializer.
 *)
let pp_print_init buf init =
   match init with
      InitString s ->
         fprintf buf "\"%s\"" (String.escaped s)
    | InitRecord (rclass, fields) ->
         fprintf buf "@[<v 0>@[<v 3>%a {%a@]@ }@]" (**)
            pp_print_record_class rclass
            pp_print_fields fields
    | InitNames (v, names) ->
         fprintf buf "@[<hv 0>@[<hv 3>Names[%a] {%a@]@ }@]" pp_print_symbol v pp_print_names names

let pp_print_imports buf (prefix, imports) = 
   let { imp_types = tenv;
         imp_tynames = tynames;
         imp_funs = funs;
         imp_globals = globals
       } = imports
   in
   (* Print the types *)
   fprintf buf "@ @[<v 3>%s Types:" prefix;
   SymbolTable.iter (fun v (v_ext, ty) ->
         fprintf buf "@ @[<v 3>%a, %a =@ %a;@]" (**)
            pp_print_symbol v_ext
            pp_print_symbol v
            pp_print_type ty) tenv;
   pp_close_box buf ();

   (* Print the type names classes *)
   fprintf buf "@ @[<v 3>%s Type Names:" prefix;
   SymbolTable.iter (fun v (v_ext, ty) ->
         fprintf buf "@ @[<hv 3>%a, %a :@ %a@]" (**)
            pp_print_symbol v_ext
            pp_print_symbol v
            pp_print_type ty) tynames;
   pp_close_box buf ();

   (* Print the globals *)
   fprintf buf "@ @[<v 3>%s Globals:" prefix;
   SymbolTable.iter (fun v (v_ext, (ty, init)) ->
         fprintf buf "@ @[<hv 3>%a, %a :@ %a@ = %a@]" (**)
            pp_print_symbol v_ext
            pp_print_symbol v
            pp_print_type ty
            pp_print_init init) globals;
   pp_close_box buf ();

   (* Print the fun decls *)
   fprintf buf "@ @[<v 3>%s Function Declarations:" prefix;
   SymbolTable.iter (fun f (f_ext, info) ->
         fprintf buf "@ %a, %a;@ " 
            pp_print_symbol f_ext
            (pp_print_function_decl max_int) (f, info)) funs;
   pp_close_box buf ()

(*
 * Print a program.
 *)
let pp_print_prog buf prog =
   let { prog_types = tenv;
         prog_tynames = tynames;
         prog_funs = funs;
         prog_main = main;
         prog_object = v_object;
         prog_globals = globals;
         prog_imports = imports;
         prog_exports = exports;
       } = prog
   in
      (* Use a wide margin (not. -n8) *)
      pp_set_margin buf 80;

      (* Print the Main function name *)
      fprintf buf "@[<v 0>@ Main: %a;" pp_print_symbol main;

      (* Print the exported object type *)
      fprintf buf "@ Object: %a;" pp_print_symbol v_object;

      (* Print the imports *)
      fprintf buf "@ @[<v 3>Imports:%a@]" pp_print_imports ("Imported", imports);

      (* Print the exports *)
      fprintf buf "@ @[<v 3>Exports:%a@]" pp_print_imports ("Exported", exports);

      (* Print the types *)
      fprintf buf "@ @[<v 3>Types:";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<v 3>%a =@ %a;@]" (**)
               pp_print_symbol v
               pp_print_type ty) tenv;
      pp_close_box buf ();

      (* Print the type names classes *)
      fprintf buf "@ @[<v 3>Type names:";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<hv 3>%a :@ %a@]" (**)
               pp_print_symbol v
               pp_print_type ty) tynames;
      pp_close_box buf ();

      (* Print the globals *)
      fprintf buf "@ @[<v 3>Globals:";
      SymbolTable.iter (fun v (ty, init) ->
            fprintf buf "@ @[<hv 3>%a :@ %a@ = %a@]" (**)
               pp_print_symbol v
               pp_print_type ty
               pp_print_init init) globals;
      pp_close_box buf ();

      (* Print the funs *)
      fprintf buf "@ @[<v 3>Funs:";
      SymbolTable.iter (fun f info ->
            fprintf buf "@ %a" (pp_print_function max_int) (f, info)) funs;
      pp_close_box buf ()
      (* fprintf buf "@]@]" *)


(*
 * Elision control.
 *)
let pp_print_exp_elide = pp_print_exp

let pp_print_exp = pp_print_exp max_int

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
