(*
 * Simple IR evaluator.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Format
open Debug
open Symbol
open Field_table

open Fj_fir
open Fj_fir_exn
open Fj_fir_env
open Fj_fir_type
open Fj_fir_state

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Values.
 * Lazy is used only for global initializers, where
 * there is no strict ordering of initialization.
 *)
type fir_value =
   ValNil
 | ValUnit
 | ValBool of bool
 | ValChar of char
 | ValInt of int
 | ValFloat of float
 | ValString of string
 | ValArray of fir_value array
 | ValRecord of field_values
 | ValFun of (fir_value list -> fir_value)
 | ValLazy of var

(*
 * Record fields.
 *)
and field_values = fir_value ref FieldTable.t

(*
 * Variable environment.
 *)
and venv = fir_value ref SymbolTable.t

(*
 * Evaluation exception.
 *)
exception EvalException of fir_pos * venv * fir_exn
exception EvalValException of fir_pos * venv * fir_value * fir_exn

(************************************************************************
 * PRINTER
 ************************************************************************)

(*
 * Print a value.
 *)
let rec pp_print_value buf v =
   match v with
      ValNil ->
         pp_print_string buf "nil"
    | ValUnit ->
         pp_print_string buf "()"
    | ValBool b ->
         pp_print_bool buf b
    | ValChar c ->
         pp_print_string buf ("'" ^ Char.escaped c ^ "'")
    | ValString s ->
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"")
    | ValInt i ->
         pp_print_int buf i
    | ValFloat x ->
         pp_print_float buf x
    | ValFun _ ->
         pp_print_string buf "<fun>";
    | ValArray a ->
         fprintf buf "@[<hv 0>@[<hv 3>array {%a@]@ }@]" pp_print_array a
    | ValRecord a ->
         fprintf buf "@[<hv 0>@[<hv 3>record{%a@]@ }@]" pp_print_fields a
    | ValLazy v ->
         fprintf buf "lazy(%a)" pp_print_symbol v

(*
 * Print array elements.
 *)
and pp_print_array buf a =
   Array.iteri (fun i v ->
         if i <> 0 then
            fprintf buf ",@ %a" pp_print_value v
         else
            pp_print_value buf v) a

(*
 * Print the value table.
 *)
and pp_print_table buf fields =
   SymbolTable.iter (fun v { contents = a } ->
         fprintf buf "@ @[<hv 3>%a =@ %a@];" (**)
            pp_print_symbol v
            pp_print_value a) fields

(*
 * Print the fields.
 *)
and pp_print_fields buf fields =
   FieldTable.iter (fun v { contents = a } ->
         fprintf buf "@ @[<hv 3>%a =@ %a;@]" (**)
            pp_print_symbol v
            pp_print_value a) fields

(*
 * Print the variable environment.
 *)
let pp_print_venv buf venv =
   SymbolTable.iter (fun v { contents = a } ->
         fprintf buf "@ @[<hv 3>%a =@ %a;@]" (**)
            pp_print_symbol v
            pp_print_value a) venv

(************************************************************************
 * ENVIRONMENTS
 ************************************************************************)

(*
 * Normal variable environment.
 *)
let venv_empty = SymbolTable.empty

let venv_add_ref = SymbolTable.add

let venv_add venv v a =
   venv_add_ref venv v (ref a)

let venv_lookup_ref venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (EvalException (pos, venv, UnboundVar v))

let rec venv_flatten_ref venv pos p =
   match !p with
      ValLazy v ->
         let v = venv_lookup venv pos v in
            p := v;
            v
    | a ->
         a

and venv_lookup venv pos v =
   let p = venv_lookup_ref venv pos v in
      venv_flatten_ref venv pos p

let venv_set venv pos v a =
   (venv_lookup_ref venv pos v) := a

(************************************************************************
 * EVALUATOR
 ************************************************************************)

(*
 * Coercions.
 *)
let coerce_bool venv pos a =
   match a with
      ValBool b -> b
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a bool"))

let coerce_int venv pos a =
   match a with
      ValInt i -> i
    | _ -> raise (EvalValException (pos, venv, a, StringError "not an int"))

let coerce_float venv pos a =
   match a with
      ValFloat x -> x
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a float"))

let coerce_string venv pos a =
   match a with
      ValString s -> s
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a string"))

let coerce_array venv pos a =
   match a with
      ValArray a -> a
    | _ -> raise (EvalValException (pos, venv, a, StringError "not an array"))

let coerce_record venv pos a =
   match a with
      ValRecord a -> a
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a record"))

(*
 * Get a field from a tuple.
 *)
let get_array_field venv pos a i =
   let a' = coerce_array venv pos a in
      if i < 0 || i >= Array.length a' then
         raise (EvalValException (pos, venv, a, StringIntError ("subscript out of bounds", i)));
      match a'.(i) with
         ValLazy v ->
            let v = venv_lookup venv pos v in
               a'.(i) <- v;
               v
       | v ->
            v

(*
 * Get a field from a record.
 *)
let get_record_field_ref venv pos a label =
   let a = coerce_record venv pos a in
      try FieldTable.find a label with
         Not_found ->
            raise (EvalException (pos, venv, UnboundLabel label))

let get_record_field venv pos a label =
   venv_flatten_ref venv pos (get_record_field_ref venv pos a label)

(*
 * Evaluate an atom.
 *)
let eval_atom venv pos a =
   let pos = string_pos "eval_atom" pos in
      match a with
         AtomUnit ->
            ValUnit
       | AtomNil ->
            ValNil
       | AtomBool b ->
            ValBool b
       | AtomChar c ->
            ValChar c
       | AtomInt i ->
            ValInt i
       | AtomFloat x ->
            ValFloat x
       | AtomVar v ->
            venv_lookup venv pos v

(*
 * Evaluate an atom.
 *)
let eval_atom_lazy venv pos a =
   let pos = string_pos "eval_atom" pos in
      match a with
         AtomUnit ->
            ValUnit
       | AtomNil ->
            ValNil
       | AtomBool b ->
            ValBool b
       | AtomChar c ->
            ValChar c
       | AtomInt i ->
            ValInt i
       | AtomFloat x ->
            ValFloat x
       | AtomVar v ->
            ValLazy v

(*
 * General evaluator.
 *)
let rec eval_exp venv e =
   let pos = string_pos "eval_exp" (fir_exp_pos e) in
      match e with
         LetFuns (funs, e) ->
            eval_funs_exp venv pos funs e
       | LetAtom (v, _, a, e)
       | LetVar (v, _, a, e) ->
            eval_atom_exp venv pos v a e
       | LetUnop (v, _, op, a, e) ->
            eval_unop_exp venv pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            eval_binop_exp venv pos v op a1 a2 e
       | LetExt (v, _, s, _, args, e) ->
            eval_ext_exp venv pos v s args e
       | TailCall (f, args) ->
            eval_tailcall_exp venv pos f args
       | MethodCall (f, a, args) ->
            eval_tailcall_exp venv pos f (a :: args)
       | IfThenElse (a, e1, e2) ->
            eval_if_exp venv pos a e1 e2
       | IfType (a, cname, v, e1, e2) ->
            eval_iftype_exp venv pos a cname v e1 e2
       | SetVar (v, _, a, e) ->
            eval_set_var_exp venv pos v a e
       | LetSubscript (v, ty, a1, a2, e) ->
            eval_subscript_exp venv pos v ty a1 a2 e
       | SetSubscript (a1, a2, _, a3, e) ->
            eval_set_subscript_exp venv pos a1 a2 a3 e
       | LetProject (v, ty, a, label, e) ->
            eval_project_exp venv pos v ty a label e
       | SetProject (a1, label, _, a2, e) ->
            eval_set_project_exp venv pos a1 label a2 e
       | LetArray (v, _, args, a, e) ->
            eval_array_exp venv pos v args a e
       | LetRecord (v, _, _, args, e) ->
            eval_record_exp venv pos v args e

(*
 * Evaluate the function definitions.
 * Functions are recursive.  Make two passes,
 * collect function defs as refs, then set them in the
 * second pass.
 *)
and eval_fun venv pos f gflag vars body =
   let pos = string_pos "eval_funs_exp" pos in
   let len1 = List.length vars in
   let f' args =
      let pos = string_pos "funcall" (vexp_pos f) in

      (* Check arity and add the args *)
      let _ =
         let len2 = List.length args in
            if len2 <> len1 then
               raise (EvalException (pos, venv, ArityMismatch (len1, len2)))
      in
      let venv = List.fold_left2 venv_add venv vars args in
         (* Evaluate the body *)
         eval_exp venv body
   in
      venv_set venv pos f (ValFun f')

and eval_funs_exp venv pos funs e =
   let pos = string_pos "eval_funs_exp" pos in
   let venv =
      List.fold_left (fun venv (f, _) ->
            venv_add venv f ValNil) venv funs
   in
      List.iter (fun (f, (gflag, _, vars, body)) ->
            eval_fun venv pos f gflag vars body) funs;
      eval_exp venv e

(*
 * Atom.
 *)
and eval_atom_exp venv pos v a e =
   let pos = string_pos "eval_atom_exp" pos in
   let venv = venv_add venv v (eval_atom venv pos a) in
      eval_exp venv e

(*
 * Unary operation.
 *)
and eval_unop_exp venv pos v op a e =
   let pos = string_pos "eval_unop_exp" pos in
   let a = eval_atom venv pos a in
   let a =
      match op with
         UMinusIntOp ->
            ValInt (-(coerce_int venv pos a))
       | UMinusFloatOp ->
            ValFloat (-.(coerce_float venv pos a))
       | UNotIntOp ->
            ValInt (lnot (coerce_int venv pos a))
       | UNotBoolOp ->
            ValBool (not (coerce_bool venv pos a))
       | UIntOfFloat ->
            ValInt (int_of_float (coerce_float venv pos a))
       | UFloatOfInt ->
            ValFloat (float_of_int (coerce_int venv pos a))
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Binary operation.
 *)
and eval_binop_exp venv pos v op a1 a2 e =
   let pos = string_pos "eval_binop_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let a =
      match op with
         (* Integers *)
         AddIntOp -> ValInt (coerce_int venv pos a1 +    coerce_int venv pos a2)
       | SubIntOp -> ValInt (coerce_int venv pos a1 -    coerce_int venv pos a2)
       | MulIntOp -> ValInt (coerce_int venv pos a1 *    coerce_int venv pos a2)
       | DivIntOp -> ValInt (coerce_int venv pos a1 /    coerce_int venv pos a2)
       | RemIntOp -> ValInt (coerce_int venv pos a1 mod  coerce_int venv pos a2)
       | AndIntOp -> ValInt (coerce_int venv pos a1 land coerce_int venv pos a2)
       | OrIntOp  -> ValInt (coerce_int venv pos a1 lor  coerce_int venv pos a2)
       | LslIntOp -> ValInt (coerce_int venv pos a1 lsl  coerce_int venv pos a2)
       | LsrIntOp -> ValInt (coerce_int venv pos a1 lsr  coerce_int venv pos a2)
       | AsrIntOp -> ValInt (coerce_int venv pos a1 asr  coerce_int venv pos a2)
       | XorIntOp -> ValInt (coerce_int venv pos a1 lxor coerce_int venv pos a2)
       | EqIntOp  -> ValBool (coerce_int venv pos a1 =  coerce_int venv pos a2)
       | NeqIntOp -> ValBool (coerce_int venv pos a1 <> coerce_int venv pos a2)
       | LeIntOp  -> ValBool (coerce_int venv pos a1 <= coerce_int venv pos a2)
       | LtIntOp  -> ValBool (coerce_int venv pos a1 <  coerce_int venv pos a2)
       | GtIntOp  -> ValBool (coerce_int venv pos a1 >  coerce_int venv pos a2)
       | GeIntOp  -> ValBool (coerce_int venv pos a1 >= coerce_int venv pos a2)

         (* Floats *)
       | AddFloatOp -> ValFloat (coerce_float venv pos a1 +. coerce_float venv pos a2)
       | SubFloatOp -> ValFloat (coerce_float venv pos a1 -. coerce_float venv pos a2)
       | MulFloatOp -> ValFloat (coerce_float venv pos a1 *. coerce_float venv pos a2)
       | DivFloatOp -> ValFloat (coerce_float venv pos a1 /. coerce_float venv pos a2)
       | RemFloatOp -> ValFloat (mod_float (coerce_float venv pos a1) (coerce_float venv pos a2))
       | EqFloatOp  -> ValBool  (coerce_float venv pos a1 =  coerce_float venv pos a2)
       | NeqFloatOp -> ValBool  (coerce_float venv pos a1 <> coerce_float venv pos a2)
       | LeFloatOp  -> ValBool  (coerce_float venv pos a1 <= coerce_float venv pos a2)
       | LtFloatOp  -> ValBool  (coerce_float venv pos a1 <  coerce_float venv pos a2)
       | GtFloatOp  -> ValBool  (coerce_float venv pos a1 >  coerce_float venv pos a2)
       | GeFloatOp  -> ValBool  (coerce_float venv pos a1 >= coerce_float venv pos a2)

       | EqPtrOp    -> ValBool  (a1 == a2)
       | NeqPtrOp   -> ValBool  (a1 != a2)
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Function application.
 *)
and eval_tailcall_exp venv pos f args =
   let pos = string_pos "eval_tailcall_exp" pos in
   let args = List.map (eval_atom venv pos) args in
      eval_tailcall_val venv pos f args

and eval_tailcall_val venv pos f args =
   let pos = string_pos "eval_tailcall_val" pos in
   let f =
      match venv_lookup venv pos f with
         ValFun f ->
            f
       | a ->
            raise (EvalValException (pos, venv, a, StringError "eval_tailcall_exp: not a function"))
   in
      f args

(*
 * External call.
 *)
and eval_ext_exp venv pos v s args e =
   let pos = string_pos "eval_ext_exp" pos in
   let args = List.map (eval_atom venv pos) args in
   let a =
      match s, args with
         "atoi", [ValString s] ->
            let i =
               (try int_of_string s with
                   Failure _ ->
                      raise (EvalValException (pos, venv, ValString s, StringError "not a number")))
            in
               ValInt i
       | "println", [ValString s] ->
            Pervasives.print_string s;
            Pervasives.print_char '\n';
            flush stdout;
            ValUnit
       | "itoa", [ValInt i] ->
            ValString (string_of_int i)
       | "ftoa", [ValFloat x] ->
            ValString (string_of_float x)
       | "strcat", [ValString s1; ValString s2] ->
            ValString (s1 ^ s2)
       | _ ->
            raise (EvalValException (pos, venv, ValString s, StringError "unknown external function"))
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Conditional.
 *)
and eval_if_exp venv pos a e1 e2 =
   let pos = string_pos "eval_if_exp" pos in
   let e =
      match eval_atom venv pos a with
         ValBool true ->
            e1
       | ValBool false ->
            e2
       | a ->
            raise (EvalValException (pos, venv, a, StringError "not a boolean"))
   in
      eval_exp venv e

(*
 * Typecase.
 *)
and eval_iftype_exp venv pos a cname v e1 e2 =
   let pos = string_pos "eval_iftype_exp" pos in
   let a = eval_atom venv pos a in
   let vma = get_record_field venv pos a class_var in
   let pos = string_pos "eval_iftype_class" pos in
   let names = get_record_field venv pos vma names_var in
   let names = coerce_array venv pos names in
   let len = pred (Array.length names) in
   let cname = coerce_string venv pos (venv_lookup venv pos cname) in
   let rec search i =
      if i >= len then
         eval_exp venv e2
      else
         let name = coerce_string venv pos names.(i) in
            if name == cname then
               let a =
                  match names.(succ i) with
                     ValNil ->
                        a
                   | vma ->
                        let args = FieldTable.empty in
                        let args = FieldTable.add args vmethods_var (ref vma) in
                        let args = FieldTable.add args vobject_var (ref a) in
                           ValRecord args
               in
               let venv = venv_add venv v a in
                  eval_exp venv e1
            else
               search (i + 2)
   in
      search 0

(*
 * Set the value of a variable.
 *)
and eval_set_var_exp venv pos v a e =
   let pos = string_pos "eval_set_var_exp" pos in
   let a = eval_atom venv pos a in
      venv_set venv pos v a;
      eval_exp venv e

(*
 * Subscripting.
 *)
and eval_subscript_exp venv pos v ty a1 a2 e =
   let pos = string_pos "eval_subscript_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let a = coerce_array venv pos a1 in
   let i = coerce_int venv pos a2 in
   let a =
      if i < 0 || i >= Array.length a then
         raise (EvalValException (pos, venv, a1, StringIntError ("subscript out of bounds", i)));
      a.(i)
   in
   let venv = venv_add venv v a in
      eval_exp venv e

and eval_set_subscript_exp venv pos a1 a2 a3 e =
   let pos = string_pos "eval_set_subscript_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let a = coerce_array venv pos a1 in
   let i = coerce_int venv pos a2 in
   let v = eval_atom venv pos a3 in
   let _ =
      if i < 0 || i >= Array.length a then
         raise (EvalValException (pos, venv, a1, StringIntError ("subscript out of bounds", i)));
      a.(i) <- v
   in
      eval_exp venv e

(*
 * Record projection.
 *)
and eval_project_exp venv pos v ty a label e =
   let pos = string_pos "eval_project_exp" pos in
   let a = eval_atom venv pos a in
   let a = get_record_field venv pos a label in
   let venv = venv_add venv v a in
      eval_exp venv e

and eval_set_project_exp venv pos a1 label a2 e =
   let pos = string_pos "eval_set_project_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let p = get_record_field_ref venv pos a1 label in
      p := a2;
      eval_exp venv e

(*
 * Array allocation.
 *)
and eval_array_exp venv pos v args a e =
   let pos = string_pos "eval_array_exp" pos in
   let a = eval_atom venv pos a in
   let args = List.map (fun a -> coerce_int venv pos (eval_atom venv pos a)) args in
   let rec make_array args =
      match args with
         i :: args ->
            ValArray (Array.init i (fun _ -> make_array args))
       | [] ->
            a
   in
   let a = make_array args in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Record allocation.
 *)
and eval_record_exp venv pos v args e =
   let pos = string_pos "eval_record_exp" pos in
   let args = FieldTable.map (fun a -> ref (eval_atom venv pos a)) args in
   let venv = venv_add venv v (ValRecord args) in
      eval_exp venv e

(*
 * Evaluate an initializer.
 *)
let eval_init venv pos v init =
   let pos = string_pos "eval_init" pos in
   let a =
      match init with
         InitString s ->
            ValString s
       | InitRecord (_, args) ->
            let args = FieldTable.map (fun a -> ref (eval_atom_lazy venv pos a)) args in
               ValRecord args
       | InitNames (_, names) ->
            let args =
               List.fold_left (fun args (v, v_opt) ->
                     let a1 = venv_lookup venv pos v in
                     let a2 =
                        match v_opt with
                           Some v ->
                              ValLazy v
                         | None ->
                              ValNil
                     in
                        a2 :: a1 :: args) [] names
            in
               ValArray (Array.of_list (List.rev args))
   in
      venv_set venv pos v a

(************************************************************************
 * CALLING MAIN
 ************************************************************************)

(*
 * Handle exceptions.
 *)
let pp_print_exn buf exn =
   match exn with
      EvalException (pos, venv, exn) ->
         fprintf buf "@[<v 0>%a@ @[<v 3>State:%a@]@]" (**)
            Fj_fir_exn.pp_print_exn (FirException (pos, exn))
            pp_print_venv venv
    | EvalValException (pos, venv, a, exn) ->
         (* fprintf buf "@[<v 0>%a@ %a@ @[<v 3>State:%a@]@]" *) (**)
         fprintf buf "@[<v 0>%a@ %a@]" (**)
            Fj_fir_exn.pp_print_exn (FirException (pos, exn))
            pp_print_value a
            (* pp_print_venv venv *)
    | exn ->
         Fj_fir_exn.pp_print_exn buf exn

(*
 * Catch exceptions.
 *)
let catch f x =
   try f x with
      EvalException _
    | EvalValException _
    | Fj_fir_exn.FirException _
    | Fj_ir_exn.IrException _
    | Fj_ast.AstException _
    | Parsing.Parse_error as exn ->
         fprintf err_formatter "%a@." pp_print_exn exn;
         exit 1

(*
 * Evaluate a program.
 *)
let main_sym = Symbol.add "main"

let build_closure f env = 
   let fun_var = Fj_fir_closure.fun_var in
   let env_var = Fj_fir_closure.env_var in
   let fields = FieldTable.empty in
   let fields = FieldTable.add fields env_var (ref env) in
   let fields = FieldTable.add fields fun_var (ref f) in
      ValRecord fields

let eval_prog prog filename argv =
   let { prog_tynames = tynames;
         prog_funs = funs;
         prog_globals = globals;
         prog_main = main
       } = prog
   in
   let pos = string_pos "eval_prog" (ast_pos ("<eval>", 0, 0, 0, 0)) in
   let venv = venv_empty in

   (* Add all the typenames *)
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v (ValString (string_of_symbol v))) venv tynames
   in

   (* Add placeholders for all the funs *)
   let venv =
      SymbolTable.fold (fun venv f f_def -> venv_add venv f ValNil) venv funs
   in

   (* Add placeholders for all the globals *)
   let venv =
      SymbolTable.fold (fun venv v _ ->
            venv_add venv v ValNil) venv globals
   in

   (* Add all the global values *)
   let _ =
      SymbolTable.iter (fun v (_, init) ->
            eval_init venv pos v init) globals
   in

   (* Add all the function values *)
   let _ =
      SymbolTable.iter (fun f (gflag, _, vars, body) ->
            eval_fun venv pos f gflag vars body) funs;
   in

   (* Evaluate the main function *)
   let argv = List.map (fun s -> ValString s) argv in
   let argv = ValArray (Array.of_list argv) in
   
   (* These aren't right.  The continuations should be in closures.  But how
      do I get the right field names for building the records?? *)
   let cont_v = build_closure 
         (ValFun (fun _ -> ValUnit))
         ValUnit 
   in
   let exnh_v = build_closure 
         (ValFun (fun args -> raise (EvalValException 
               (pos, venv, List.hd args, StringError "uncaught exception"))  )) 
         ValUnit
   in
   
      ignore (eval_tailcall_val venv pos main [cont_v; exnh_v; argv])

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
