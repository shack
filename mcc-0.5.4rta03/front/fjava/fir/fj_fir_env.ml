(*
 * Basic type an variable environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fj_fir
open Fj_fir_exn

(*
 * Basic environment includes both a type
 * and a variable environment.
 *)
type env =
   { env_tenv     : ty SymbolTable.t;
     env_venv     : ty SymbolTable.t;
     env_tynames  : ty SymbolTable.t;
     env_object   : var
   }

(*
 * Empty environment.
 *)
let env_empty =
   { env_tenv     = SymbolTable.empty;
     env_venv     = SymbolTable.empty;
     env_tynames  = SymbolTable.empty;
     env_object   = new_symbol_string "Object_type_not_defined"
   }

(*
 * Types.
 *)
let env_add_type env v ty =
   { env with env_tenv = SymbolTable.add env.env_tenv v ty }

let env_lookup_type env pos v =
   try SymbolTable.find env.env_tenv v with
      Not_found ->
         raise (FirException (pos, UnboundType v))

(*
 * Vars.
 *)
let env_add_var env v ty =
   { env with env_venv = SymbolTable.add env.env_venv v ty }

let env_lookup_var env pos v =
   try SymbolTable.find env.env_venv v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Type names.
 *)
let env_lookup_tyname env pos v =
   try SymbolTable.find env.env_tynames v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Fold over all variable definitions.
 *)
let env_fold_var f x env =
   let { env_venv = venv;
         env_tynames = tynames
       } = env
   in
   let x = SymbolTable.fold f x venv in
      SymbolTable.fold f x tynames

(*
 * Get the Object type.
 *)
let env_object_type env =
   TyApply (env.env_object, [])

(*
 * Build the default environment from the prog.
 *)
let env_of_prog prog =
   let { prog_types = types;
         prog_funs = funs;
         prog_tynames = tynames;
         prog_globals = globals;
         prog_object = v_object;
         prog_imports = imports
       } = prog
   in

   (* Duild the variable environment *)
   let venv = SymbolTable.empty in
   let venv =
      SymbolTable.fold (fun venv f (_, ty, _, _) ->
            SymbolTable.add venv f ty) venv funs
   in
   let venv =
      SymbolTable.fold (fun venv f (_, (_, ty, _)) ->
            SymbolTable.add venv f ty) venv imports.imp_funs
   in

   (* Add the globals *)
   let venv =
      SymbolTable.fold (fun venv v (ty, _) ->
            SymbolTable.add venv v ty) venv globals
   in
   let venv =
      SymbolTable.fold (fun venv v (_, (ty, _)) ->
            SymbolTable.add venv v ty) venv imports.imp_globals
   in
   
   (* Merge the imported types and typenames in with the prog's *)
   let tenv =
      SymbolTable.fold (fun tenv v (_, ty) ->
            SymbolTable.add tenv v ty) types imports.imp_types
   in
   let tnenv =
      SymbolTable.fold (fun tnenv v (_, ty) ->
            SymbolTable.add tnenv v ty) tynames imports.imp_tynames
   in

   (* Add default info *)
   let env =
      { env_tenv = tenv;
        env_tynames = tnenv;
        env_venv = venv;
        env_object = v_object
      }
   in
      env

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
