(*
 * Inlining and constant folding.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Symbol
open Field_table

open Fj_fir
open Fj_fir_exn
open Fj_fir_print
open Fj_fir_standardize

let fir_exp_pos e = string_pos "Fj_fir_inline" (fir_exp_pos e)

(************************************************************************
 * AVAIL EXPRESSIONS
 ************************************************************************)

(*
 * Available expressions.
 *)
type avail =
   AvailVar of var
 | AvailAtom of atom
 | AvailFunction of var * bool * var list * exp
 | AvailRecord of var

type arecord = avail FieldTable.t

type aenv =
   { aenv_vars : avail SymbolTable.t;
     aenv_records : arecord SymbolTable.t
   }

(*
 * Empty table.
 *)
let aenv_empty =
   { aenv_vars = SymbolTable.empty;
     aenv_records = SymbolTable.empty
   }

(*
 * Add an entry.
 *)
let aenv_add aenv v avail =
   { aenv with aenv_vars = SymbolTable.add aenv.aenv_vars v avail }

let aenv_add_record aenv v fields =
   { aenv with aenv_records = SymbolTable.add aenv.aenv_records v fields }

(*
 * Add an atom entry.
 *)
let aenv_add_atom aenv v a =
   aenv_add aenv v (AvailAtom a)

let aenv_add_var aenv v v' =
   aenv_add aenv v (AvailAtom (AtomVar v'))

let aenv_add_id aenv v =
   aenv_add aenv v (AvailVar v)

let aenv_add_ids aenv vars =
   List.fold_left aenv_add_id aenv vars

(*
 * Adding function.
 *)
let aenv_add_fun aenv f force vars body =
   aenv_add aenv f (AvailFunction (f, force, vars, body))

(*
 * Look up a value.
 * Do this recursively if we find an AtomVar.
 *)
let aenv_lookup aenv v =
   try SymbolTable.find aenv.aenv_vars v with
      Not_found ->
         AvailVar v

let aenv_lookup_record aenv pos v =
   try SymbolTable.find aenv.aenv_records v with
      Not_found ->
         raise (FirException (pos, UnboundVar v))

(*
 * Atom expansion.
 *)
let aenv_lookup_atom aenv a =
   match a with
      AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         AvailAtom a
    | AtomVar v ->
         aenv_lookup aenv v

let aenv_lookup_atoms aenv args =
   List.map (aenv_lookup_atom aenv) args

(*
 * Add args binding.
 *)
let aenv_add_args aenv pos vars args =
   let pos = string_pos "aenv_add_args" pos in
   let len1 = List.length vars in
   let len2 = List.length args in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      List.fold_left2 aenv_add aenv vars args

(*
 * Lookup a field in a record.
 *)
let field_find fields pos label =
   try FieldTable.find fields label with
      Not_found ->
         raise (FirException (pos, UnboundLabel label))

(************************************************************************
 * PHASE 1: FUNCTION SIZES
 ************************************************************************)

(*
 * This is an arbitrary choice of the maximum function
 * size to inline.
 *)
let max_size = 20

(*
 * The function table keeps track of the number of
 * function occurrences.
 *)
type fenv =
   { fenv_funs : SymbolSet.t;
     fenv_counts : int SymbolTable.t;
     fenv_metric : (int * bool) SymbolTable.t;
     fenv_changed : bool
   }

let fenv_of_funs funs =
   let funs =
      SymbolTable.fold (fun funs f _ ->
            SymbolSet.add funs f) SymbolSet.empty funs
   in
      { fenv_funs = funs;
        fenv_counts = SymbolTable.empty;
        fenv_metric = SymbolTable.empty;
        fenv_changed = false
      }

(*
 * Add the metric.
 *)
let fenv_add_metric fenv f entry =
   { fenv with fenv_metric = SymbolTable.add fenv.fenv_metric f entry }

let fenv_lookup_metric fenv f =
   SymbolTable.find fenv.fenv_metric f

let fenv_lookup_count fenv f =
   try SymbolTable.find fenv.fenv_counts f with
      Not_found ->
         0

(*
 * Add a potential function call.
 *)
let fenv_var fenv v =
   if SymbolSet.mem fenv.fenv_funs v then
      let counts =
         SymbolTable.filter_add fenv.fenv_counts v (function
            Some i ->
               succ i
          | None ->
               1)
      in
         { fenv with fenv_counts = counts;
                     fenv_changed = true
         }
   else
      fenv

(*
 * Add an atom.
 *)
let fenv_atom fenv a =
   match a with
      AtomVar v ->
         fenv_var fenv v
    | AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         fenv

let fenv_atoms fenv args =
   List.fold_left fenv_atom fenv args

let fenv_fields fenv fields =
   FieldTable.fold (fun fenv _ a ->
         fenv_atom fenv a) fenv fields

(*
 * Compute the "size" of each function.
 * This is the size of the text, not the computational cost.
 * The branches flag indicates whether the expression has a TailCall
 * to a real function.
 *)
let rec size_exp fenv size e =
   if size >= max_size then
      fenv, size
   else
      match e with
         (* These don't cost anything *)
         LetVar (_, _, a, e)
       | LetAtom (_, _, a, e) ->
            let fenv = fenv_atom fenv a in
               size_exp fenv size e

         (* Cheap expressions *)
       | LetSubscript (_, _, _, _, e)
       | LetProject (_, _, _, _, e)
       | LetArray (_, _, _, _, e) ->
            size_exp fenv (succ size) e

       | LetUnop (_, _, _, a, e)
       | SetProject (_, _, _, a, e)
       | SetSubscript (_, _, _, a, e) ->
            let fenv = fenv_atom fenv a in
               size_exp fenv (succ size) e

       | LetBinop (_, _, _, a1, a2, e) ->
            let fenv = fenv_atom fenv a1 in
            let fenv = fenv_atom fenv a2 in
               size_exp fenv (succ size) e

       | LetExt (_, _, _, _, args, e) ->
            let fenv = fenv_atoms fenv args in
               size_exp fenv (succ size) e

       | LetRecord (_, _, _, args, e) ->
            let fenv = fenv_fields fenv args in
               size_exp fenv (succ size) e

         (* Conditional expressions *)
       | IfThenElse (_, e1, e2)
       | IfType (_, _, _, e1, e2) ->
            let fenv, size = size_exp fenv (succ size) e1 in
               size_exp fenv size e2

         (* Base case *)
       | TailCall (f, args)
       | MethodCall (f, _, args) ->
            let fenv = fenv_var fenv f in
            let fenv = fenv_atoms fenv args in
               fenv, succ size

         (* Not allowed *)
       | LetFuns _
       | SetVar _ ->
            let pos = string_pos "size_exp" (fir_exp_pos e) in
               raise (FirException (pos, FirLevel2))

let fenv_fun fenv f e =
   let fenv = { fenv with fenv_changed = false } in
   let fenv, size = size_exp fenv 0 e in
      fenv_add_metric fenv f (size, fenv.fenv_changed)

(************************************************************************
 * PHASE 2: INLINING
 ************************************************************************)

(*
 * Inline the vars in a type.
 *)
let rec inline_type aenv pos ty =
   match ty with
      TyVoid
    | TyUnit
    | TyNil
    | TyBool
    | TyChar
    | TyString
    | TyInt
    | TyFloat
    | TyVar _ ->
         ty
    | TyArray ty ->
         TyArray (inline_type aenv pos ty)
    | TyFun (ty_vars, ty_res) ->
         TyFun (List.map (inline_type aenv pos) ty_vars, inline_type aenv pos ty_res)
    | TyMethod (ty_this, ty_vars, ty_res) ->
         TyMethod (inline_type aenv pos ty_this, List.map (inline_type aenv pos) ty_vars, inline_type aenv pos ty_res)
    | TyRecord (rclass, fields) ->
         let fields = FieldTable.map (fun ty -> inline_type aenv pos ty) fields in
            TyRecord (rclass, fields)
    | TyNames (v, names) ->
         let names =
            List.map (fun (v, ty_opt) ->
                  let ty_opt =
                     match ty_opt with
                        Some ty -> Some (inline_type aenv pos ty)
                      | None -> None
                  in
                     v, ty_opt) names
         in
            TyNames (v, names)
    | TyApply (v, tyl) ->
         TyApply (v, List.map (inline_type aenv pos) tyl)
    | TyAll (vars, ty) ->
         TyAll (vars, inline_type aenv pos ty)
    | TyExists (vars, ty) ->
         TyExists (vars, inline_type aenv pos ty)
    | TyLambda (vars, ty) ->
         TyLambda (vars, inline_type aenv pos ty)
    | TyProject (v, i) ->
         let v =
            match aenv_lookup aenv v with
               AvailAtom (AtomVar v)
             | AvailVar v
             | AvailFunction (v, _, _, _)
             | AvailRecord v ->
                  v
             | AvailAtom _ ->
                  raise (FirException (pos, StringVarError ("illegal argument to TyProject", v)))
         in
            TyProject (v, i)
    | TyObject (v, ty) ->
         TyObject (v, inline_type aenv pos ty)

(*
 * Utility to get the var from an atom.
 *)
let var_of_atom pos a =
   match a with
      AtomVar v ->
         v
    | AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         raise (FirException (pos, StringError "not a var"))

(*
 * Atom of avail.
 *)
let atom_of_avail = function
   AvailAtom a ->
      a
 | AvailVar v
 | AvailFunction (v, _, _, _)
 | AvailRecord v ->
      AtomVar v

(*
 * Atom inlining.
 *)
let inline_atom aenv a =
   atom_of_avail (aenv_lookup_atom aenv a)

let inline_atoms aenv args =
   List.map (inline_atom aenv) args

let inline_fields aenv args =
   FieldTable.map (fun a ->
         aenv_lookup_atom aenv a) args

(*
 * Inline an expression.
 *)
let rec inline_exp aenv e =
   let pos = string_pos "inline_exp" (fir_exp_pos e) in
      match e with
         LetVar (v, ty, a, e)
       | LetAtom (v, ty, a, e) ->
            inline_atom_exp aenv pos v a e
       | LetUnop (v, ty, op, a, e) ->
            inline_unop_exp aenv pos v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            inline_binop_exp aenv pos v ty op a1 a2 e
       | LetExt (v, ty1, s, ty2, args, e) ->
            inline_ext_exp aenv pos v ty1 s ty2 args e
       | TailCall (f, args) ->
            inline_tailcall_exp aenv pos f args
       | MethodCall (f, a, args) ->
            inline_methodcall_exp aenv pos f a args
       | IfThenElse (a, e1, e2) ->
            inline_if_exp aenv pos a e1 e2
       | IfType (a, name, v, e1, e2) ->
            inline_iftype_exp aenv pos a name v e1 e2
       | LetSubscript (v, ty, a1, a2, e) ->
            inline_subscript_exp aenv pos v ty a1 a2 e
       | SetSubscript (a1, a2, ty, a3, e) ->
            inline_set_subscript_exp aenv pos a1 a2 ty a3 e
       | LetProject (v, ty, a, label, e) ->
            inline_project_exp aenv pos v ty a label e
       | SetProject (a1, label, ty, a2, e) ->
            inline_set_project_exp aenv pos a1 label ty a2 e
       | LetArray (v, ty, args, a, e) ->
            inline_array_exp aenv pos v ty args a e
       | LetRecord (v, ty, rclass, args, e) ->
            inline_record_exp aenv pos v ty rclass args e
       | LetFuns _
       | SetVar _ ->
            raise (FirException (pos, FirLevel2))

(*
 * LetAtom always gets inlined.
 *)
and inline_atom_exp aenv pos v a e =
   let pos = string_pos "inline_atom_exp" pos in
   let avail = aenv_lookup_atom aenv a in
   let aenv = aenv_add aenv v avail in
      inline_exp aenv e

(*
 * Unary operator.  Perform constant folding if possible.
 *)
and inline_unop_exp aenv pos v ty op a e =
   let pos = string_pos "inline_unop_exp" pos in
   let ty = inline_type aenv pos ty in
   let v' = new_symbol v in
   let a = inline_atom aenv a in
   let a' =
      match op, a with
         UMinusIntOp, AtomInt i ->
            AtomInt (-i)
       | UMinusFloatOp, AtomFloat x ->
            AtomFloat (-.x)
       | UNotBoolOp, AtomBool b ->
            AtomBool (not b)
       | UIntOfFloat, AtomFloat x ->
            AtomInt (int_of_float x)
       | UFloatOfInt, AtomInt i ->
            AtomFloat (float_of_int i)
       | _ ->
            AtomVar v'
   in
   let aenv = aenv_add_atom aenv v a' in
   let e = inline_exp aenv e in
      LetUnop (v', ty, op, a, e)

(*
 * Binary operator.  Perform constant folding if possible.
 *)
and inline_binop_exp aenv pos v ty op a1 a2 e =
   let pos = string_pos "inline_binop_exp" pos in
   let ty = inline_type aenv pos ty in
   let v' = new_symbol v in
   let a1 = inline_atom aenv a1 in
   let a2 = inline_atom aenv a2 in
   let a =
      match op, a1, a2 with
         AddIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 + i2)
       | SubIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 - i2)
       | MulIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 * i2)
       | DivIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 / i2)
       | RemIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 mod i2)
       | AndIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 land i2)
       | OrIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lor i2)
       | XorIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lxor i2)
       | LslIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lsl i2)
       | LsrIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 lsr i2)
       | AsrIntOp, AtomInt i1, AtomInt i2 ->
            AtomInt (i1 asr i2)
       | EqIntOp, AtomInt i1, AtomInt i2 ->
            AtomBool (i1 = i2)
       | NeqIntOp, AtomInt i1, AtomInt i2 ->
            AtomBool (i1 <> i2)
       | LeIntOp, AtomInt i1, AtomInt i2 ->
            AtomBool (i1 <= i2)
       | LtIntOp, AtomInt i1, AtomInt i2 ->
            AtomBool (i1 < i2)
       | GtIntOp, AtomInt i1, AtomInt i2 ->
            AtomBool (i1 > i2)
       | GeIntOp, AtomInt i1, AtomInt i2 ->
            AtomBool (i1 >= i2)

       | AddFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 +. x2)
       | SubFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 -. x2)
       | MulFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 *. x2)
       | DivFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (x1 /. x2)
       | RemFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomFloat (mod_float x1 x2)
       | EqFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomBool (x1 = x2)
       | NeqFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomBool (x1 <> x2)
       | LeFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomBool (x1 <= x2)
       | LtFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomBool (x1 < x2)
       | GtFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomBool (x1 > x2)
       | GeFloatOp, AtomFloat x1, AtomFloat x2 ->
            AtomBool (x1 >= x2)

       | _ ->
            AtomVar v'
   in
   let aenv = aenv_add_atom aenv v a in
   let e = inline_exp aenv e in
      LetBinop (v', ty, op, a1, a2, e)

(*
 * External call.
 *)
and inline_ext_exp aenv pos v ty1 s ty2 args e =
   let pos = string_pos "inline_ext_exp" pos in
   let ty1 = inline_type aenv pos ty1 in
   let ty2 = inline_type aenv pos ty2 in
   let v' = new_symbol v in
   let args = inline_atoms aenv args in
   let aenv = aenv_add_var aenv v v' in
   let e = inline_exp aenv e in
      LetExt (v', ty1, s, ty2, args, e)

(*
 * Tailcall.
 *)
and inline_tailcall_exp aenv pos f args =
   let pos = string_pos "inline_tailcall_exp" pos in
   let rec inline aenv f args =
      match aenv_lookup aenv f with
         AvailVar f ->
            TailCall (f, List.map atom_of_avail args)
       | AvailAtom a ->
            TailCall (var_of_atom pos a, List.map atom_of_avail args)
       | AvailFunction (f', force, vars, body) ->
            let aenv =
               if force then
                  aenv
               else
                  aenv_add_id aenv f'
            in
            let aenv = aenv_add_args aenv pos vars args in
               inline_exp aenv body
       | AvailRecord _ ->
            raise (FirException (pos, StringError "illegal record"))
   in
   let args = aenv_lookup_atoms aenv args in
      inline aenv f args

and inline_methodcall_exp aenv pos f a args =
   let pos = string_pos "inline_tailcall_exp" pos in
   let f = var_of_atom pos (atom_of_avail (aenv_lookup aenv f)) in
   let a = inline_atom aenv a in
   let args = inline_atoms aenv args in
      MethodCall (f, a, args)

(*
 * Conditionals.
 *)
and inline_if_exp aenv pos a e1 e2 =
   let pos = string_pos "inline_if_exp" pos in
      match inline_atom aenv a with
         AtomBool true ->
            inline_exp aenv e1
       | AtomBool false ->
            inline_exp aenv e2
       | a ->
            let e1 = inline_exp aenv e1 in
            let e2 = inline_exp aenv e2 in
               IfThenElse (a, e1, e2)

and inline_iftype_exp aenv pos a name v e1 e2 =
   let pos = string_pos "inline_iftype_exp" pos in
   let a = inline_atom aenv a in
   let e2 = inline_exp aenv e2 in
   let v' = new_symbol v in
   let aenv = aenv_add_var aenv v v' in
   let e1 = inline_exp aenv e1 in
      IfType (a, name, v', e1, e2)

(*
 * Subscripting.
 *)
and inline_subscript_exp aenv pos v ty a1 a2 e =
   let pos = string_pos "inline_subscript_exp" pos in
   let ty = inline_type aenv pos ty in
   let a1 = inline_atom aenv a1 in
   let a2 = inline_atom aenv a2 in
   let v' = new_symbol v in
   let aenv = aenv_add_var aenv v v' in
   let e = inline_exp aenv e in
      LetSubscript (v', ty, a1, a2, e)

and inline_set_subscript_exp aenv pos a1 a2 ty a3 e =
   let pos = string_pos "inline_set_subscript_exp" pos in
   let ty = inline_type aenv pos ty in
   let a1 = inline_atom aenv a1 in
   let a2 = inline_atom aenv a2 in
   let a3 = inline_atom aenv a3 in
   let e = inline_exp aenv e in
      SetSubscript (a1, a2, ty, a3, e)

(*
 * Record projection.
 *)
and inline_project_exp aenv pos v ty a label e =
   let pos = string_pos "inline_project_exp" pos in
   let ty = inline_type aenv pos ty in
      match aenv_lookup_atom aenv a with
         AvailRecord v' ->
            let fields = aenv_lookup_record aenv pos v' in
            let a = field_find fields pos label in
            let aenv = aenv_add aenv v a in
               inline_exp aenv e
       | AvailAtom _
       | AvailVar _
       | AvailFunction _ as avail ->
            let a = atom_of_avail avail in
            let v' = new_symbol v in
            let aenv = aenv_add_var aenv v v' in
            let e = inline_exp aenv e in
               LetProject (v', ty, a, label, e)

and inline_set_project_exp aenv pos a1 label ty a2 e =
   let pos = string_pos "inline_set_project_exp" pos in
   let ty = inline_type aenv pos ty in
   let v1 = var_of_atom pos a1 in
   let a1 = aenv_lookup aenv v1 in
   let a2 = aenv_lookup_atom aenv a2 in
   let aenv =
      match a1 with
         AvailRecord v' ->
            let fields = aenv_lookup_record aenv pos v' in
            let fields = FieldTable.add fields label a2 in
               aenv_add_record aenv v' fields
       | AvailVar _
       | AvailAtom _
       | AvailFunction _ ->
            aenv
   in
   let a1 = atom_of_avail a1 in
   let a2 = atom_of_avail a2 in
   let e = inline_exp aenv e in
      SetProject (a1, label, ty, a2, e)

(*
 * Allocations.
 *)
and inline_array_exp aenv pos v ty args a e =
   let pos = string_pos "inline_array_exp" pos in
   let ty = inline_type aenv pos ty in
   let args = inline_atoms aenv args in
   let a = inline_atom aenv a in
   let v' = new_symbol v in
   let aenv = aenv_add_var aenv v v' in
   let e = inline_exp aenv e in
      LetArray (v', ty, args, a, e)

and inline_record_exp aenv pos v ty rclass args e =
   let pos = string_pos "inline_record_exp" pos in
   let ty = inline_type aenv pos ty in
   let fields = inline_fields aenv args in
   let args = FieldTable.map (fun avail -> atom_of_avail avail) fields in
   let v' = new_symbol v in
   let aenv = aenv_add aenv v (AvailRecord v') in
   let aenv = aenv_add_record aenv v' fields in
   let e = inline_exp aenv e in
      LetRecord (v', ty, rclass, args, e)

(*
 * Inline the program.
 *)
let inline_prog prog =
   let { prog_funs = funs;
         prog_tynames = tynames;
         prog_globals = globals
       } = prog
   in

   (* Define identity environment for globals *)
   let aenv = aenv_empty in
   let aenv =
      SymbolTable.fold (fun aenv v _ ->
            aenv_add_id aenv v) aenv tynames
   in
   let aenv =
      SymbolTable.fold (fun aenv v _ ->
            aenv_add_id aenv v) aenv globals
   in

   (* Measure each function *)
   let fenv =
      SymbolTable.fold (fun fenv f (_, _, _, body) ->
            fenv_fun fenv f body) (fenv_of_funs funs) funs
   in

   (* Add the funs to the environment *)
   let aenv =
      SymbolTable.fold (fun aenv f (_, _, vars, body) ->
            let size, recursive = fenv_lookup_metric fenv f in
            let counts = fenv_lookup_count fenv f in
               if size < max_size || counts <= 1 then
                  aenv_add_fun aenv f (not recursive) vars body
               else
                  aenv_add_id aenv f) aenv funs
   in

   (* Inline each function body *)
   let funs =
      SymbolTable.map (fun (gflag, ty, vars, body) ->
            let aenv = aenv_add_ids aenv vars in
            let body = inline_exp aenv body in
               gflag, ty, vars, body) funs
   in
      { prog with prog_funs = funs }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
