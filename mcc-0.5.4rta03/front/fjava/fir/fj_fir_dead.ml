(*
 * Dead-code, and dead-argument elimination.
 * This is a two-stage process: first collect all the live vars,
 * then eliminate all vars that are dead.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Field_table
open Symbol

open Fj_fir
open Fj_fir_exn
open Fj_fir_env
open Fj_fir_type
open Fj_fir_state

let vexp_pos v = string_pos "Fj_fir_dead" (vexp_pos v)

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Live table is just a SymbolSet.
 *)
type live =
   { live_vars : SymbolSet.t;
     live_funs : var list SymbolTable.t
   }

let live_empty =
   { live_vars = SymbolSet.empty;
     live_funs = SymbolTable.empty
   }

(*
 * Table equality is just based on variable liveness.
 *)
let live_equal live1 live2 =
   SymbolSet.equal live1.live_vars live2.live_vars

(*
 * Variable liveness.
 *)
let live_add_var live v =
   { live with live_vars = SymbolSet.add live.live_vars v }

let live_add_vars live vars =
   List.fold_left live_add_var live vars

let live_mem live v =
   SymbolSet.mem live.live_vars v

(*
 * Liveness of function parameters.
 *)
let live_add_fun live f flags =
   { live with live_funs = SymbolTable.add live.live_funs f flags }

let live_lookup_fun live f =
   try SymbolTable.find live.live_funs f with
      Not_found ->
         []

(************************************************************************
 * LIVENESS
 ************************************************************************)

(*
 * Add a var.
 * If the var is a function, add all the vars.
 *)
let live_var live v =
   let live = live_add_var live v in
   let live = live_add_vars live (live_lookup_fun live v) in
      live

let live_var_opt live = function
   Some v ->
      live_var live v
 | None ->
      live

(*
 * Filter the argument list.
 *)
let filter_args live vars args =
   let rec rev_args args' args =
      match args' with
         [] ->
            args
       | h :: args' ->
            rev_args args' (h :: args)
   in
   let rec collect args' vars args =
      match vars, args with
         v :: vars, a :: args ->
            let args' =
               if live_mem live v then
                  a :: args'
               else
                  args'
            in
               collect args' vars args
       | _ ->
            rev_args args' args
   in
      collect [] vars args

(*
 * Liveness for a type.
 *)
let rec live_type live ty =
   match ty with
      TyVoid
    | TyUnit
    | TyNil
    | TyBool
    | TyChar
    | TyString
    | TyInt
    | TyFloat ->
         live

    | TyArray ty ->
         live_type live ty

    | TyFun (ty_vars, ty_res) ->
         live_types (live_type live ty_res) ty_vars

    | TyMethod (ty_this, ty_vars, ty_res) ->
         live_types (live_type (live_type live ty_this) ty_res) ty_vars

    | TyRecord (_, fields) ->
         live_fields_type live fields

    | TyApply (v, tyl) ->
         live_var (live_types live tyl) v

    | TyNames (v, names) ->
         live_names_type (live_var live v) names

    | TyVar v
    | TyProject (v, _) ->
         live_var live v

    | TyAll (_, ty)
    | TyExists (_, ty)
    | TyLambda (_, ty)
    | TyObject (_, ty) ->
         live_type live ty

and live_type_opt live ty_opt =
   match ty_opt with
      Some ty ->
         live_type live ty
    | None ->
         live

and live_types live tyl =
   List.fold_left live_type live tyl

(*
 * Live vars in the names.
 *)
and live_names_type live names =
   List.fold_left (fun live (v, ty_opt) ->
         live_type_opt (live_var live v) ty_opt) live names

(*
 * Live vars in the fields.
 * Include only the live fields.
 *)
and live_fields_type live fields =
   FieldTable.fold (fun live v ty ->
         if live_mem live v then
            live_type live ty
         else
            live) live fields

(*
 * Liveness for an atom.
 *)
let live_atom live a =
   match a with
      AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         live
    | AtomVar v ->
         live_var live v

let live_atoms live args =
   List.fold_left live_atom live args

let live_fields live args =
   FieldTable.fold (fun live v a ->
         if live_mem live v then
            live_atom live a
         else
            live) live args

(*
 * Add liveness for an expression.
 *)
let rec live_exp live e =
   let pos = string_pos "live_exp" (fir_exp_pos e) in
      match e with
         LetVar (v, ty, a, e)
       | LetAtom (v, ty, a, e)
       | LetUnop (v, ty, _, a, e) ->
            live_atom_exp live v ty a e
       | LetBinop (v, ty, _, a1, a2, e)
       | LetSubscript (v, ty, a1, a2, e) ->
            live_args_exp live v ty [a1; a2] e
       | LetExt (v, ty1, _, ty2, args, e) ->
            live_ext_exp live v ty1 ty2 args e
       | TailCall (f, args) ->
            live_tailcall_exp live f args
       | MethodCall (f, a, args) ->
            live_tailcall_exp live f (a :: args)
       | IfThenElse (a, e1, e2) ->
            live_if_exp live a e1 e2
       | IfType (a, name, v, e1, e2) ->
            live_iftype_exp live a name v e1 e2
       | SetSubscript (a1, a2, ty, a3, e) ->
            live_set_subscript_exp live a1 a2 ty a3 e
       | LetProject (v, ty, a, label, e) ->
            live_project_exp live v ty a label e
       | SetProject (a1, label, ty, a2, e) ->
            live_set_project_exp live a1 label ty a2 e
       | LetArray (v, ty, args, a, e) ->
            live_args_exp live v ty (a :: args) e
       | LetRecord (v, ty, _, args, e) ->
            live_record_exp live v ty args e
       | LetFuns _
       | SetVar _ ->
            raise (FirException (pos, FirLevel2))

(*
 * Atom liveness.
 *)
and live_atom_exp live v ty a e =
   let live = live_exp live e in
      if live_mem live v then
         let live = live_atom live a in
         let live = live_type live ty in
            live
      else
         live

(*
 * Liveness for a variable number of arguments.
 *)
and live_args_exp live v ty args e =
   let live = live_exp live e in
      if live_mem live v then
         let live = live_atoms live args in
         let live = live_type live ty in
            live
      else
         live

(*
 * Liveness for a field table.
 *)
and live_record_exp live v ty args e =
   let live = live_exp live e in
      if live_mem live v then
         let live = live_fields live args in
         let live = live_type live ty in
            live
      else
         live

(*
 * External calls are always live.
 *)
and live_ext_exp live v ty1 ty2 args e =
   let live = live_exp live e in
   let live = live_atoms live args in
   let live = live_type live ty2 in
   let live = live_type live ty1 in
      live

(*
 * Tailcalls are always live, but some of the arguments
 * may not be live.
 *)
and live_tailcall_exp live f args =
   let vars = live_lookup_fun live f in
   let args = filter_args live vars args in
   let live = live_atoms live args in
      live_var live f

(*
 * Conditional.
 *)
and live_if_exp live a e1 e2 =
   let live = live_exp live e1 in
   let live = live_exp live e2 in
   let live = live_atom live a in
      live

(*
 * Type test.
 *)
and live_iftype_exp live a name v e1 e2 =
   let live = live_exp live e1 in
   let live = live_exp live e2 in
   let live = live_var live name in
   let live = live_atom live a in
      live

(*
 * SetSubscript is always live.
 *)
and live_set_subscript_exp live a1 a2 ty a3 e =
   let live = live_exp live e in
   let live = live_atom live a3 in
   let live = live_type live ty in
   let live = live_atom live a2 in
   let live = live_atom live a1 in
      live

(*
 * Record projection.
 *)
and live_project_exp live v ty a label e =
   let live = live_exp live e in
      if live_mem live v then
         let live = live_type live ty in
         let live = live_atom live a in
         let live = live_var live label in
            live
      else
         live

(*
 * SetProject is live only if the label is live.
 *)
and live_set_project_exp live a1 label ty a2 e =
   let live = live_exp live e in
      if live_mem live label then
         let live = live_atom live a2 in
         let live = live_type live ty in
         let live = live_atom live a1 in
            live
      else
         live

(*
 * Liveness for an initializer.
 *)
let live_init live init =
   match init with
      InitString _ ->
         live
    | InitRecord (_, args) ->
         live_fields live args
    | InitNames (v, names) ->
         List.fold_left (fun live (v, v_opt) ->
               live_var_opt (live_var live v) v_opt) (live_var live v) names

(*
 * Liveness step for the program.
 *)
let live_prog env live prog =
   let { prog_types = types;
         prog_tynames = tynames;
         prog_funs = funs;
         prog_main = main;
         prog_object = v_object;
         prog_globals = globals;
         prog_imports = imports;
         prog_exports = exports
       } = prog
   in

   (* Main and object are always live *)
   let live = live_var live main in
   let live = live_var live v_object in

   (* Never drop an object's names *)
   let live = live_var live class_var in
   let live = live_var live names_var in

   (* Make sure no args are dropped from main *)
   let live = live_add_vars live (live_lookup_fun live main) in
   
   (* Make sure exported functions don't get dropped or have 
      their signatures changed *)
   let live =
      SymbolTable.fold (fun live v (_, (_, ty, _)) ->
            let live = live_var live v in
               live_type live ty
            ) live exports.imp_funs
   in

   (* Add types *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if live_mem live v then
               live_type live ty
            else
               live) live types
   in
   let live =
      SymbolTable.fold (fun live v (_, ty) ->
            if live_mem live v then
               live_type live ty
            else
               live) live imports.imp_types
   in

   (* Add typenames *)
   let live =
      SymbolTable.fold (fun live v ty ->
            if live_mem live v then
               live_type live ty
            else
               live) live tynames
   in
   let live =
      SymbolTable.fold (fun live v (_, ty) ->
            if live_mem live v then
               live_type live ty
            else
               live) live imports.imp_tynames
   in

   (* Functions *)
   let live =
      SymbolTable.fold (fun live f (_, ty, vars, body) ->
            if live_mem live f then
               let pos = vexp_pos f in
               let pos = string_pos "live_prog" pos in
               let _, ty_vars, ty_res = dest_fun_or_method_type env pos ty in
               let live = live_type live ty_res in
               let live = live_exp live body in
               let live =
                  List.fold_left2 (fun live v ty ->
                        if live_mem live v then
                           live_type live ty
                        else
                           live) live vars ty_vars
               in
                  live
            else
               live) live funs
   in

   (* Globals *)
   let live =
      SymbolTable.fold (fun live v (ty, init) ->
            if live_mem live v then
               let live = live_type live ty in
               let live = live_init live init in
                  live
            else
               live) live globals
   in
      live

(*
 * Compute the liveness fixpoint.
 *)
let live_prog env prog =
   (* Assume all params for each function are dead *)
   let live =
      SymbolTable.fold (fun live f (_, _, vars, _) ->
            live_add_fun live f vars) live_empty prog.prog_funs
   in

   (* Compute liveness fixpoint *)
   let rec fixpoint live =
      let live' = live_prog env live prog in
         if live_equal live' live then
            live
         else
            fixpoint live'
   in
      fixpoint live

(************************************************************************
 * DEAD-OBJECT ELIMINATION
 ************************************************************************)

(*
 * Eliminate dead fields in the types.
 *)
let rec dead_type live ty =
   match ty with
      TyVoid
    | TyUnit
    | TyNil
    | TyBool
    | TyChar
    | TyString
    | TyInt
    | TyFloat
    | TyVar _
    | TyProject _ ->
         ty
    | TyNames (v, names) ->
         TyNames (v, dead_names_type live names)
    | TyApply (v, tyl) ->
         TyApply (v, dead_types live tyl)
    | TyArray ty ->
         TyArray (dead_type live ty)
    | TyFun (ty_vars, ty_res) ->
         TyFun (dead_types live ty_vars, dead_type live ty_res)
    | TyMethod (ty_this, ty_vars, ty_res) ->
         TyMethod (dead_type live ty_this, dead_types live ty_vars, dead_type live ty_res)
    | TyRecord (rclass, fields) ->
         TyRecord (rclass, dead_fields_type live fields)
    | TyAll (vars, ty) ->
         TyAll (vars, dead_type live ty)
    | TyExists (vars, ty) ->
         TyExists (vars, dead_type live ty)
    | TyLambda (vars, ty) ->
         TyLambda (vars, dead_type live ty)
    | TyObject (v, ty) ->
         TyObject (v, dead_type live ty)

(*
 * List of types.
 *)
and dead_types live tyl =
   List.map (dead_type live) tyl

(*
 * Eliminate dead fields.
 *)
and dead_names_type live names =
   List.map (fun (v, ty_opt) ->
         v, dead_opt_type live ty_opt) names

(*
 * Optional types.
 *)
and dead_opt_type live ty_opt =
   match ty_opt with
      Some ty ->
         Some (dead_type live ty)
    | None ->
         None

(*
 * Eliminate dead type fields.
 *)
and dead_fields_type live fields =
   let fields = FieldTable.to_list fields in
      List.fold_left (fun fields (v, ty) ->
            if live_mem live v then
               FieldTable.add fields v (dead_type live ty)
            else
               fields) FieldTable.empty fields

(*
 * Eliminate dead fields.
 *)
let dead_fields live fields =
   let fields = FieldTable.to_list fields in
      List.fold_left (fun fields (v, a) ->
            if live_mem live v then
               FieldTable.add fields v a
            else
               fields) FieldTable.empty fields

(*
 * Eliminate dead exprs.
 *)
let rec dead_exp live e =
   match e with
      LetVar (v, ty, a, e) ->
         dead_var_exp live v ty a e
    | LetAtom (v, ty, a, e) ->
         dead_atom_exp live v ty a e
    | LetUnop (v, ty, op, a, e) ->
         dead_unop_exp live v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         dead_binop_exp live v ty op a1 a2 e
    | LetExt (v, ty1, s, ty2, args, e) ->
         LetExt (v, ty1, s, ty2, args, dead_exp live e)
    | TailCall (f, args) ->
         dead_tailcall_exp live f args
    | MethodCall (f, a, args) ->
         dead_methodcall_exp live f a args
    | IfThenElse (a, e1, e2) ->
         IfThenElse (a, dead_exp live e1, dead_exp live e2)
    | IfType (a, name, v, e1, e2) ->
         IfType (a, name, v, dead_exp live e1, dead_exp live e2)
    | LetSubscript (v, ty, a1, a2, e) ->
         dead_subscript_exp live v ty a1 a2 e
    | SetSubscript (a1, a2, ty, a3, e) ->
         SetSubscript (a1, a2, ty, a3, dead_exp live e)
    | LetProject (v, ty, a, label, e) ->
         dead_project_exp live v ty a label e
    | SetProject (a1, label, ty, a2, e) ->
         dead_set_project_exp live a1 label ty a2 e
    | LetArray (v, ty, args, a, e) ->
         dead_array_exp live v ty args a e
    | LetRecord (v, ty, rclass, args, e) ->
         dead_record_exp live v ty rclass args e
    | LetFuns _
    | SetVar _ ->
         let pos = string_pos "dead_exp" (fir_exp_pos e) in
            raise (FirException (pos, FirLevel2))

(*
 * A variable declaration.
 *)
and dead_var_exp live v ty a e =
   let e = dead_exp live e in
      if live_mem live v then
         LetVar (v, dead_type live ty, a, e)
      else
         e

(*
 * An atom declaration.
 *)
and dead_atom_exp live v ty a e =
   let e = dead_exp live e in
      if live_mem live v then
         LetAtom (v, dead_type live ty, a, e)
      else
         e

(*
 * Unops can be liminated.
 *)
and dead_unop_exp live v ty op a e =
   let e = dead_exp live e in
      if live_mem live v then
         LetUnop (v, dead_type live ty, op, a, e)
      else
         e

(*
 * Binops can be eliminated.
 *)
and dead_binop_exp live v ty op a1 a2 e =
   let e = dead_exp live e in
      if live_mem live v then
         LetBinop (v, dead_type live ty, op, a1, a2, e)
      else
         e

(*
 * In a tailcall, check if any of the args can be eliminated.
 *)
and dead_tailcall_exp live f args =
   let vars = live_lookup_fun live f in
   let args = filter_args live vars args in
      TailCall (f, args)

and dead_methodcall_exp live f a args =
   let vars = live_lookup_fun live f in
   let args = filter_args live vars args in
      MethodCall (f, a, args)

(*
 * Subscripts can be eliminated.
 *)
and dead_subscript_exp live v ty a1 a2 e =
   let e = dead_exp live e in
      if live_mem live v then
         LetSubscript (v, dead_type live ty, a1, a2, e)
      else
         e

(*
 * Projections can be eliminated.
 *)
and dead_project_exp live v ty a label e =
   let e = dead_exp live e in
      if live_mem live v then
         LetProject (v, dead_type live ty, a, label, e)
      else
         e

and dead_set_project_exp live a1 label ty a2 e =
   let e = dead_exp live e in
      if live_mem live label then
         SetProject (a1, label, dead_type live ty, a2, e)
      else
         e

(*
 * All allocations can be eliminated.
 *)
and dead_array_exp live v ty args a e =
   let e = dead_exp live e in
      if live_mem live v then
         LetArray (v, dead_type live ty, args, a, e)
      else
         e

and dead_record_exp live v ty rclass args e =
   let e = dead_exp live e in
      if live_mem live v then
         let ty = dead_type live ty in
         let args = dead_fields live args in
            LetRecord (v, ty, rclass, args, e)
      else
         e

(*
 * Global elimination.
 *)
let dead_global live (ty, init) =
   let ty = dead_type live ty in
   let init =
      match init with
         InitString _
       | InitNames _ ->
            init
       | InitRecord (rclass, fields) ->
            InitRecord (rclass, dead_fields live fields)
   in
      ty, init

(*
 * Generic table elimination.
 *)
let dead_table live dead_element table =
   SymbolTable.fold (fun table v x ->
         if live_mem live v then
            SymbolTable.add table v (dead_element live x)
         else
            table) SymbolTable.empty table

(*
 * Generic set elimination.
 *)
let dead_set live s =
   SymbolSet.fold (fun s v ->
         if live_mem live v then
            SymbolSet.add s v
         else
            s) SymbolSet.empty s

(*
 * Remove dead objects from the program.
 *)
let dead_prog prog =
   let env = env_of_prog prog in
   let live = live_prog env prog in

   (* Remove dead parts of the program *)
   let { prog_types = types;
         prog_tynames = tynames;
         prog_funs = funs;
         prog_main = main;
         prog_object = v_object;
         prog_globals = globals;
         prog_imports = imports;
         prog_exports = exports
       } = prog
   in

   (* Top-level tables *)
   let types = dead_table live dead_type types in
   let tynames = dead_table live dead_type tynames in
   let globals = dead_table live dead_global globals in

   (* Functions are special *)
   let funs =
      SymbolTable.fold (fun funs f (gflag, ty, vars, body) ->
               if live_mem live f then
                  let pos = vexp_pos f in
                  let pos = string_pos "dead_prog" pos in
                  let all_vars, ty = dest_all_type env pos ty in
                     if is_method_type env pos ty then
                        (* Method type *)
                        let ty_this, ty_vars, ty_res = dest_method_type env pos ty in
                        let v_this, vars =
                           match vars with
                              v_this :: vars ->
                                 v_this, vars
                            | [] ->
                                 raise (FirException (pos, StringError "method has no args"))
                        in
                        let vars, ty_vars =
                           List.fold_left2 (fun (vars, ty_vars) v ty ->
                                 if live_mem live v then
                                    v :: vars, ty :: ty_vars
                                 else
                                    vars, ty_vars) ([], []) vars ty_vars
                        in
                        let body = dead_exp live body in
                        let ty_method = TyAll (all_vars, TyMethod (ty_this, List.rev ty_vars, ty_res)) in
                        let def = gflag, ty_method, v_this :: List.rev vars, body in
                           SymbolTable.add funs f def
                     else
                        (* Function type *)
                        let ty_vars, ty_res = dest_fun_type env pos ty in
                        let vars, ty_vars =
                           List.fold_left2 (fun (vars, ty_vars) v ty ->
                                 if live_mem live v then
                                    v :: vars, ty :: ty_vars
                                 else
                                    vars, ty_vars) ([], []) vars ty_vars
                        in
                        let body = dead_exp live body in
                        let ty_fun = TyAll (all_vars, TyFun (List.rev ty_vars, ty_res)) in
                        let def = gflag, ty_fun, List.rev vars, body in
                           SymbolTable.add funs f def
               else
                  funs) SymbolTable.empty funs
   in
      (* Reconstruct the program *)
      { prog_types = types;
        prog_tynames = tynames;
        prog_funs = funs;
        prog_main = main;
        prog_object = v_object;
        prog_globals = globals;
        prog_imports = imports;
        prog_exports = exports
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
