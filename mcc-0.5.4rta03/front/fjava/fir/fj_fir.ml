(*
 * FJava intermediate representation.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Field_table

(*
 * A variable is just a symbol.
 *)
type var = symbol
type ty_var = symbol

(*
 * There are several types of records.
 * RecordFrame:
 *    a function frame
 * RecordClass:
 *    a class record has one manifest fields:
 *        names_var: the names object for the class
 *    everything else is a method
 * RecordObject:
 *    an object record should have a
 *        oclass_var: the class record
 *    everything else is a field
 * RecordVObject:
 *    a vobject record should have exactly two fields
 *        vmethods_var: the method record
 *        vobject_var: the object
 * RecordMethods:
 *    a collection of methods (used in interface definition/coercion)
 *)
type record_class =
   RecordFrame
 | RecordClass
 | RecordMethods
 | RecordFields
 | RecordVObject
 | RecordClosure

(*
 * Types.
 *)
type ty =
   (* Basic types *)
   TyVoid
 | TyUnit
 | TyNil
 | TyBool
 | TyChar
 | TyString
 | TyInt
 | TyFloat

   (* Arrays of values, all elements have the same type *)
 | TyArray of ty

   (* Functions have a list of types for the arguments, and a result type *)
 | TyFun of ty list * ty

   (* A method has a this type *)
 | TyMethod of ty * ty list * ty

   (* TyRecord is a collection of fields *)
 | TyRecord of record_class * field_types

   (* The "names" record for typecase *)
 | TyNames of ty_var * (var * ty option) list

   (* Type identifier
    *    TyApply (ty_var, type_args)
    *       Apply polymorphic type_args to the type function indicated by
    *       ty_var (which is bound in the type defs).  type_args must all be
    *       smalltypes.
    *)
 | TyVar of ty_var
 | TyApply of ty_var * ty list
 | TyAll of ty_var list * ty
 | TyExists of ty_var list * ty
 | TyLambda of ty_var list * ty
 | TyProject of ty_var * int

   (* An object type *)
 | TyObject of ty_var * ty

(*
 * The type of record fields.
 *)
and field_types = ty FieldTable.t

(*
 * Unary operators.
 *)
type unop =
   (* Arithmetic *)
   UMinusIntOp
 | UMinusFloatOp
 | UNotIntOp
 | UNotBoolOp

   (* Numeric coercions *)
 | UIntOfFloat
 | UFloatOfInt

(*
 * Binary operators.
 *)
type binop =
   (* Integers *)
   AddIntOp
 | SubIntOp
 | MulIntOp
 | DivIntOp
 | RemIntOp
 | AndIntOp
 | OrIntOp
 | LslIntOp
 | LsrIntOp
 | AsrIntOp
 | XorIntOp
 | EqIntOp
 | NeqIntOp
 | LeIntOp
 | LtIntOp
 | GtIntOp
 | GeIntOp

   (* Floats *)
 | AddFloatOp
 | SubFloatOp
 | MulFloatOp
 | DivFloatOp
 | RemFloatOp
 | EqFloatOp
 | NeqFloatOp
 | LeFloatOp
 | LtFloatOp
 | GtFloatOp
 | GeFloatOp

   (* Other comparisons *)
 | EqPtrOp
 | NeqPtrOp

(*
 * Values.
 *)
type atom =
   AtomUnit
 | AtomNil
 | AtomBool  of bool
 | AtomChar  of char
 | AtomInt   of int
 | AtomFloat of float
 | AtomVar   of var

(*
 * Atoms in a record.
 *)
type field_atoms = atom FieldTable.t

(*
 * Function classes:
 *    FunGlobalClass: user-defined function
 *    FunContClass: continuation
 *    FunLocalClass: local function
 *)
type fun_class =
   FunGlobalClass
 | FunContClass
 | FunLocalClass

(*
 * Intermediate code expressions.
 *)
type exp =
   (* Function definition *)
   LetFuns of fundef list * exp

   (* Arithmetic *)
 | LetVar of var * ty * atom * exp
 | LetAtom of var * ty * atom * exp
 | LetUnop of var * ty * unop * atom * exp
 | LetBinop of var * ty * binop * atom * atom * exp

   (* Functions *)
 | LetExt of var * ty * string * ty * atom list * exp
 | TailCall of var * atom list
 | MethodCall of var * atom * atom list

   (* Conditionals *)
 | IfThenElse of atom * exp * exp
 | IfType of atom * var * var * exp * exp

   (* Variable assignment: our objective is to eliminate this *)
 | SetVar of var * ty * atom * exp

   (* Array operations *)
 | LetArray of var * ty * atom list * atom * exp
 | LetSubscript of var * ty * atom * atom * exp
 | SetSubscript of atom * atom * ty * atom * exp

   (* Record operations *)
 | LetRecord of var * ty * record_class * field_atoms * exp
 | LetProject of var * ty * atom * var * exp
 | SetProject of atom * var * ty * atom * exp

(*
 * A function definition has:
 *    1. a function classification
 *    2. the function type
 *    3. then formal parameters
 *    4. the body
 *)
and fundef = var * fun_info

and fun_info = fun_class * ty * var list * exp

type fun_decl = fun_class * ty * var list

(*
 * Global initializers.
 *)
type init =
   InitString of string
 | InitRecord of record_class * field_atoms
 | InitNames of var * (var * var option) list

(* 
 * Info about imports/exports
 * var is the external name
 *)
type import_table = 
   { imp_types    : (var * ty) SymbolTable.t;
     imp_tynames  : (var * ty) SymbolTable.t;
     imp_funs     : (var * fun_decl) SymbolTable.t;
     imp_globals  : (var * (ty * init)) SymbolTable.t
   }

(*
 * A program has the following parts:
 *   prog_types: type definitions
 *   prog_funs: a set of function definitions
 *   prog_main: the name of the main function
 *)
type prog =
   { prog_types    : ty SymbolTable.t;
     prog_tynames  : ty SymbolTable.t;
     prog_funs     : fun_info SymbolTable.t;
     prog_main     : var;
     prog_object   : var;
     prog_globals  : (ty * init) SymbolTable.t;
     prog_imports  : import_table;
     prog_exports  : import_table
   }

(*
 * These are the exceptions that can happen during
 * the IR stage.
 *)
type fir_exn =
   UnboundVar of var
 | UnboundType of var
 | UnboundLabel of var
 | NotImplemented
 | TypeError2 of ty * ty
 | TypeError4 of ty * ty * ty * ty
 | ArityMismatch of int * int
 | StringError of string
 | StringIntError of string * int
 | StringVarError of string * var
 | StringAtomError of string * atom
 | StringTypeError of string * ty
 | StringVarVarError of string * var * var
 | FirLevel1
 | FirLevel2

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
