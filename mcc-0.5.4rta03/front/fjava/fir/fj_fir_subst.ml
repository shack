(*
 * Define a type substitution.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Field_table
open Symbol

open Fj_fir

type subst = ty SymbolTable.t

(*
 * Type substitution.
 *)
let subst_empty = SymbolTable.empty

let subst_add = SymbolTable.add

let subst_remove = SymbolTable.remove

let subst_lookup = SymbolTable.find

let rec subst_type subst ty =
   match ty with
      TyVoid
    | TyUnit
    | TyNil
    | TyBool
    | TyChar
    | TyString
    | TyInt
    | TyFloat
    | TyNames _
    | TyProject _ ->
         ty
    | TyArray ty ->
         TyArray (subst_type subst ty)
    | TyFun (ty_vars, ty_res) ->
         let ty_vars = List.map (subst_type subst) ty_vars in
         let ty_res = subst_type subst ty_res in
            TyFun (ty_vars, ty_res)
    | TyMethod (ty_this, ty_vars, ty_res) ->
         let ty_this = subst_type subst ty_this in
         let ty_vars = List.map (subst_type subst) ty_vars in
         let ty_res = subst_type subst ty_res in
            TyMethod (ty_this, ty_vars, ty_res)
    | TyRecord (rclass, fields) ->
         TyRecord (rclass, FieldTable.mapi (fun _ ty -> subst_type subst ty) fields)
    | TyVar v ->
         (try subst_type subst (subst_lookup subst v) with
             Not_found ->
                ty)
    | TyApply (v, tyl) ->
         TyApply (v, List.map (subst_type subst) tyl)
    | TyAll (vars, ty) ->
         let tvenv = List.fold_left subst_remove subst vars in
            TyAll (vars, subst_type subst ty)
    | TyExists (vars, ty) ->
         let tvenv = List.fold_left subst_remove subst vars in
            TyExists (vars, subst_type subst ty)
    | TyLambda (vars, ty) ->
         let tvenv = List.fold_left subst_remove subst vars in
            TyLambda (vars, subst_type subst ty)
    | TyObject (v, ty) ->
         let tvenv = subst_remove subst v in
            TyObject (v, subst_type subst ty)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
