(*
 * Convert from the IR to the FJ_FIR.
 * This includes CPS conversion and class elimination.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table
open Format

open Fj_fir
open Fj_fir_env
open Fj_fir_exn
open Fj_fir_type
open Fj_fir_check
open Fj_fir_state
open Fj_fir_standardize

let ir_exp_pos e = string_pos "Fj_fir_ir" (ir_exp_pos e)
let fir_exp_pos e = string_pos "Fj_fir_ir" (fir_exp_pos e)

(************************************************************************
 * INFO
 ************************************************************************)

(*
 * Default exception function name.
 *)
let f_cont = new_symbol_string "cont"
let f_exnh = new_symbol_string "exnh"

(*
 * Some type definitions.
 *)
let v_exnh = new_symbol_string "ty_exnh"
let ty_exnh = TyApply (v_exnh, [])

(*
 * For each label, we record whether it is a class method,
 * interface method, or class field.  For the methods, we also
 * record the vma type.
 *)
type label =
   ClassMethod
 | InterfaceMethod
 | ObjectField

(*
 * Program info.
 *)
type info =
   { info_globals    : (ty * init) SymbolTable.t;
     info_tynames    : ty SymbolTable.t;
     info_types      : ty SymbolTable.t;
     info_funs       : fun_info SymbolTable.t;
     info_labels     : label SymbolTable.t;
     info_classes    : field_atoms SymbolTable.t;
     info_interfaces : var SymbolTable.t SymbolTable.t;
     info_methods    : (ty * field_types) SymbolTable.t;
     info_object     : ty;
     info_imports    : info option;
     info_exports    : import_table
   }

(*
 * Empty info.
 *)
let info_empty v_object =
   (* Add the exception type *)
   let ty_object = TyApply (v_object, []) in
   let types = SymbolTable.add SymbolTable.empty v_exnh (TyFun ([ty_object], TyVoid)) in
      { info_globals = SymbolTable.empty;
        info_tynames = SymbolTable.empty;
        info_types = types;
        info_funs = SymbolTable.empty;
        info_labels = SymbolTable.empty;
        info_classes = SymbolTable.empty;
        info_interfaces = SymbolTable.empty;
        info_methods = SymbolTable.empty;
        info_object = ty_object;
        info_imports = None;
        info_exports = { imp_types   = SymbolTable.empty;
                         imp_tynames = SymbolTable.empty;
                         imp_funs    = SymbolTable.empty;
                         imp_globals = SymbolTable.empty
                       }
      }

let print_info_sizes info =
   eprintf "@[<v 0>globals: %i@ " (SymbolTable.cardinal info.info_globals);
   eprintf "tynames: %i@ " (SymbolTable.cardinal info.info_tynames);
   eprintf "types: %i@ " (SymbolTable.cardinal info.info_types);
   eprintf "funs: %i@ " (SymbolTable.cardinal info.info_funs);
   eprintf "labels: %i@ " (SymbolTable.cardinal info.info_labels);
   eprintf "classes: %i@ " (SymbolTable.cardinal info.info_classes);
   eprintf "interfaces: %i@ " (SymbolTable.cardinal info.info_interfaces);
   eprintf "methods: %i@ @]" (SymbolTable.cardinal info.info_methods)
      

(*
 * Add/export a global.
 *)
let info_add_global info v init =
   let exports = info.info_exports in
      { info with info_globals = SymbolTable.add info.info_globals v init;
                  info_exports = { exports with imp_globals =
                        SymbolTable.add exports.imp_globals v (v, init)}
      }

(*
 * Add/export a type name.
 *)
let info_add_typename info v ty =
   let exports = info.info_exports in
      { info with info_tynames = SymbolTable.add info.info_tynames v ty;
                  info_exports = { exports with imp_tynames =
                        SymbolTable.add exports.imp_tynames v (v, ty)}
      }

(*
 * Add/export a type.
 *)
let info_add_type info v ty =
   let exports = info.info_exports in
   { info with info_types = SymbolTable.add info.info_types v ty;
                  info_exports = { exports with imp_types =
                        SymbolTable.add exports.imp_types v (v, ty)}
      }

(*
 * Add/export a function.
 *)
let info_add_fun info f def =
   { info with info_funs = SymbolTable.add info.info_funs f def }
(*    let exports = info.info_exports in
   let fclass, ty, vars, _ = def in
   let decl = fclass, ty, vars in
   { info with info_funs = SymbolTable.add info.info_funs f def;
                  info_exports = { exports with imp_funs =
                        SymbolTable.add exports.imp_funs f (f, decl)}
      } *)

(* 
 * Just export a function declaration
 *)
let info_export_fun info f f_ext decl =
   let exports = info.info_exports in
      { info with info_exports = 
         { exports with imp_funs =
            SymbolTable.add exports.imp_funs f (f_ext, decl)
      }}

(*
 * Add the class info.
 *)
let info_add_class info cname args =
   { info with info_classes = SymbolTable.add info.info_classes cname args }

let rec info_lookup_class info pos cname =
   try SymbolTable.find info.info_classes cname with
      Not_found ->
         match info.info_imports with
            None ->
               raise (FirException (pos, UnboundVar cname))
          | Some info ->
               info_lookup_class info pos cname

let is_class_name info cname =
   SymbolTable.mem info.info_classes cname ||
   match info.info_imports with
      None ->
         false
    | Some info ->
         SymbolTable.mem info.info_classes cname

(*
 * Add the interface VMA records.
 *)
let info_add_interfaces info cname names =
   let names =
      List.fold_left (fun names (iname, v_opt) ->
            match v_opt with
               Some v_vma ->
                  SymbolTable.add names iname v_vma
             | None ->
                  names) SymbolTable.empty names
   in
      { info with info_interfaces = 
            SymbolTable.add info.info_interfaces cname names }

let rec info_lookup_interface info pos cname iname =
   try SymbolTable.find (SymbolTable.find info.info_interfaces cname) iname with
      Not_found ->
         match info.info_imports with
            None ->
               raise (FirException (pos, StringVarError ("interface is not implemented", iname)))
          | Some info ->
               info_lookup_interface info pos cname iname

(*
 * Add method definitions for interfaces.
 *)
let info_add_methods info iname methods =
   { info with info_methods = SymbolTable.add info.info_methods iname methods }

let rec info_lookup_methods info pos iname =
   try SymbolTable.find info.info_methods iname with
      Not_found ->
         match info.info_imports with
            None ->
               raise (FirException (pos, UnboundVar iname))
          | Some info ->
               info_lookup_methods info pos iname

(*
 * Save info about the label types.
 *)
let rec info_lookup_label info pos label =
   try SymbolTable.find info.info_labels label with
      Not_found ->
         match info.info_imports with
            None ->
               raise (FirException (pos, StringVarError ("unbound label", label)))
          | Some info ->
               info_lookup_label info pos label

let info_add_labels info linfo fields =
   let labels =
      List.fold_left (fun labels (_, v, _) ->
            SymbolTable.add labels v linfo) info.info_labels fields
   in
      { info with info_labels = labels }

(************************************************************************
 * TYPE CONVERSION
 ************************************************************************)

(*
 * Convert a type definition.
 *)
let rec build_type info ty =
   match ty with
      Fj_ir.TyUnit ->
         TyUnit
    | Fj_ir.TyNil ->
         TyNil
    | Fj_ir.TyBool ->
         TyBool
    | Fj_ir.TyChar ->
         TyChar
    | Fj_ir.TyString ->
         TyString
    | Fj_ir.TyInt ->
         TyInt
    | Fj_ir.TyFloat ->
         TyFloat
    | Fj_ir.TyArray ty ->
         let ty =
            match build_type info ty with
               TyApply _ ->
                  info.info_object
             | ty ->
                  ty
         in
            TyArray ty
    | Fj_ir.TyFun (ty_vars, ty_res) ->
         let ty_cont = TyFun ([build_type info ty_res], TyVoid) in
         let ty_vars = List.map (build_type info) ty_vars in
            TyFun (ty_cont :: ty_exnh :: ty_vars, TyVoid)
    | Fj_ir.TyCont ty_vars ->
         let ty_vars = List.map (build_type info) ty_vars in
            TyFun (ty_vars, TyVoid)
    | Fj_ir.TyMethod (ty_this, ty_vars, ty_res) ->
         let ty_cont = TyFun ([build_type info ty_res], TyVoid) in
         let ty_this = build_type info ty_this in
         let ty_vars = List.map (build_type info) ty_vars in
            TyMethod (ty_this, ty_cont :: ty_exnh :: ty_vars, TyVoid)
    | Fj_ir.TyObject id
    | Fj_ir.TyVObject id ->
         TyApply (id, [])
    | Fj_ir.TyVar v ->
         TyVar v

(*
 * This is the same, except that it does not add extra
 * cont/exnh args to the toplevel function.
 *)
let build_fun_type info ty =
   match ty with
    | Fj_ir.TyFun (ty_vars, ty_res) ->
         let ty_vars = List.map (build_type info) ty_vars in
         (* let ty_res = build_type info ty_res in *)
            TyFun (ty_vars, TyVoid)
    | _ ->
         build_type info ty

(*
 * This is the same, except that it does not add extra
 * cont/exnh args to the toplevel function and the return type
 * remains TyUnit.
 *)
let build_ext_fun_type info ty =
   match ty with
      Fj_ir.TyFun (ty_vars, ty_res) ->
         let ty_vars = List.map (build_type info) ty_vars in
         let ty_res = build_type info ty_res in
            TyFun (ty_vars, ty_res)
    | _ ->
         build_type info ty

(*
 * Convert an interface definition.
 *)
let build_interface_def info pos iname iinfo =
   let pos = string_pos "build_interface_def" pos in
   let { Fj_ir.intf_name = ext_name;
         Fj_ir.intf_this = this;
         Fj_ir.intf_methods = methods
       } = iinfo
   in

   (* Add the names *)
   let info = info_add_typename info iname (TyApply (iname, [])) in

   (* Get the method types *)
   let methods =
      try FieldMTable.to_list methods with
         Failure s ->
            raise (FirException (pos, StringError s))
   in

   (* Define the methods type *)
   let imethods =
      List.fold_left (fun imethods (_, f, ty) ->
            FieldTable.add imethods f (build_type info ty)) 
                  FieldTable.empty methods
   in
   let ty_methods = TyRecord (RecordMethods, imethods) in
   let v_methods = new_symbol_string (Symbol.to_string iname ^ "_methods") in
   let info = info_add_type info v_methods ty_methods in
   let ty_methods = TyApply (v_methods, []) in

   (* Add info about methods *)
   let info = info_add_methods info iname (ty_methods, imethods) in
   let info = info_add_labels info InterfaceMethod methods in

   (* Interface is a tuple *)
   let ty_intf = FieldTable.empty in
   let ty_intf = FieldTable.add ty_intf vmethods_var ty_methods in
   let ty_intf = FieldTable.add ty_intf vobject_var (TyApply (this, [])) in
   let ty_intf = TyRecord (RecordVObject, ty_intf) in
      info_add_type info iname ty_intf

(*
 * Build an interface coercion function.
 * We define a VMA for the interface,
 * and a function to perform the coercion.
 *)
let build_interface_coercion info pos cname iname methods =
   let pos = string_pos "build_interface_coercion" pos in
   let v_methods = new_symbol_string 
         (Printf.sprintf "%s_%s_methods" 
            (Symbol.to_string cname) (Symbol.to_string iname)) 
   in

   (* Get the methods definition *)
   let ty_methods, imethods = info_lookup_methods info pos iname in
   let imethods = FieldTable.to_list imethods in

   (* Combine the definitions *)
   let len1 = List.length methods in
   let len2 = List.length imethods in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in
   let fields =
      List.fold_left2 (fun fields (f, _) (_, g, _) ->
            FieldTable.add fields f (AtomVar g))
                  FieldTable.empty imethods methods
   in

   (* Add the global *)
   let info = info_add_global info v_methods 
         (ty_methods, InitRecord (RecordMethods, fields)) in
      info, v_methods, ty_methods

(*
 * Convert a class definition.
 *)
let build_class_def info pos cname cinfo =
   let pos = string_pos "build_class_def" pos in
   let { Fj_ir.class_this = v_this;
         Fj_ir.class_parents = parents;
         Fj_ir.class_interfaces = interfaces;
         Fj_ir.class_methods = methods;
         Fj_ir.class_fields = fields;
         Fj_ir.class_consts = constructors
         (* class_package, class_name are missing *)
       } = cinfo
   in

   (* Get the string name of the class, so we can make smart names *)
   let s = Symbol.to_string cname in

   (* Collect the coercions *)
   (* First list all of the parents with no VMA info *)
   let names = List.map (fun v -> v, None, None) (cname :: parents) in
   (* Now append the interfaces with VMA conversions *)
   let info, names =
      SymbolTable.fold (fun (info, interfaces) name methods ->
            let info, v_vma, ty_vma = 
                  build_interface_coercion info pos cname name methods in
            let interfaces = (name, Some v_vma, Some ty_vma) :: interfaces in
               info, interfaces) (info, names) interfaces
   in
   (* Build the type of the names list *)
   let ty_names = TyNames (cname, List.map (fun (v, _, ty) -> v, ty) names) in
   let names = List.map (fun (v, v_vma, _) -> v, v_vma) names in
   let v_names = new_symbol_string (s ^ "_names") in

   let v_ty_names = new_symbol_string (s ^ "_namestype") in
   let info = info_add_type info v_ty_names ty_names in
   let ty_names = TyApply (v_ty_names, []) in

   let info = 
         info_add_global info v_names (ty_names, InitNames (cname, names)) in
   let info = info_add_interfaces info cname names in

   (* Collect the fields *)
   let fields =
      try FieldMTable.to_list fields with
         Failure s ->
            raise (FirException (pos, StringError s))
   in
   let info = info_add_labels info ObjectField fields in
   let fields = 
         List.map (fun (v1, v2, ty) -> v1, v2, build_type info ty) fields in

   (* Collect the methods *)
   let methods =
      try FieldMTable.to_list methods with
         Failure s ->
            raise (FirException (pos, StringError s))
   in

   (* 
    * Define the type of the VMA for the class, which contains:
    *    type of ptr to names list
    *    types of methods
    *)
   let ty_class = FieldTable.empty in
   (* Add type of the "names" field of the VMA *)
   let ty_class = FieldTable.add ty_class names_var ty_names in
   (* Add types of all of the methods *)
   let ty_class =
      List.fold_left (fun ty_class (_, f, ty) ->
            FieldTable.add ty_class f (build_type info ty)) ty_class methods
   in
   let ty_class = TyLambda ([v_this], TyRecord (RecordClass, ty_class)) in
   let v_ty_class = new_symbol_string (s ^ "_classtype") in
   (* Save the class type *)
   let info = info_add_type info v_ty_class ty_class in
   let ty_class = TyApply (v_ty_class, []) in (* This var appears to be dead? *)

   (* Method label info *)
   let info = info_add_labels info ClassMethod methods in

   (* Define the VMA itself. *)
   let init = FieldTable.empty in
   let init = FieldTable.add init names_var (AtomVar v_names) in
   let init =
      List.fold_left (fun init (_, f, _) ->
            FieldTable.add init f (AtomVar f)) init methods
   in
   let init = InitRecord (RecordClass, init) in
   let v_class = new_symbol_string (s ^ "_class") in
   let info = info_add_global info v_class (
                  TyApply (v_ty_class, [TyApply (cname, [])]), init) in

   (* Define the type of instances of this class.  
      Type is a record with type of ptr to VMA and types of instance fields *)
   let ty_object = FieldTable.empty in
   let ty_object = FieldTable.add ty_object class_var (
                  TyApply (v_ty_class, [TyVar v_this])) in
   let ty_object =
      List.fold_left (fun ty_object (_, v, ty) ->
            FieldTable.add ty_object v ty) ty_object fields
   in
   let ty_object = TyObject (v_this, TyRecord (RecordFields, ty_object)) in
   let v_ty_object = new_symbol_string (s ^ "_objtype") in
   let info = info_add_type info v_ty_object ty_object in
   let ty_object = TyApply (v_ty_object, []) in

   (* Add the class initializer *)
   let args = FieldTable.empty in
   let args = FieldTable.add args class_var (AtomVar v_class) in
   let args =
      List.fold_left (fun args (_, v, ty) ->
            FieldTable.add args v (default_atom ty)) args fields
   in
   let info = info_add_class info cname args in

   (* Now we can add the class VMA *)
   let info = info_add_typename info cname (TyApply (cname, [])) in
   
   (* Export declarations for all of the class' constructors *)
   let info = FieldMTable.fold (fun info v_ext v_int ty -> 
               let ty = build_type info ty in
                  info_export_fun info v_int v_ext (FunGlobalClass, ty, []))
                  info constructors
   in
      info_add_type info cname ty_object

(*
 * Convert interface type definitions.
 *)
let build_interface_tydef info v tydef =
   let pos = string_pos "build_interface_tydef" (vexp_pos v) in
      match tydef with
         Fj_ir.TyDefInterface (Some iinfo) ->
            build_interface_def info pos v iinfo
       | Fj_ir.TyDefClass _
       | Fj_ir.TyDefInterface None ->
            info

let build_class_tydef info v tydef =
   let pos = string_pos "build_class_tydef" (vexp_pos v) in
      match tydef with
         Fj_ir.TyDefClass (Some cinfo) ->
            build_class_def info pos v cinfo
       | Fj_ir.TyDefClass None
       | Fj_ir.TyDefInterface _ ->
            info

(************************************************************************
 * EXPRESSION CONVERSION
 ************************************************************************)

(*
 * Convert an atom.
 *)
let build_atom a =
   match a with
      Fj_ir.AtomUnit ->
         AtomUnit
    | Fj_ir.AtomNil ->
         AtomNil
    | Fj_ir.AtomBool b ->
         AtomBool b
    | Fj_ir.AtomChar c ->
         AtomChar c
    | Fj_ir.AtomInt i ->
         AtomInt i
    | Fj_ir.AtomFloat x ->
         AtomFloat x
    | Fj_ir.AtomVar v ->
         AtomVar v

(*
 * Build an expression.
 *)
let rec build_exp env info e =
   let pos = string_pos "build_exp" (ir_exp_pos e) in
      match e with
         Fj_ir.LetFuns (funs, e) ->
            build_funs_exp env info pos funs e
       | Fj_ir.LetVar (v, ty, a, e) ->
            build_var_exp env info pos v ty a e
       | Fj_ir.LetAtom (v, ty, a, e) ->
            build_atom_exp env info pos v ty a e
       | Fj_ir.LetUnop (v, ty, op, a, e) ->
            build_unop_exp env info pos v ty op a e
       | Fj_ir.LetBinop (v, ty, op, a1, a2, e) ->
            build_binop_exp env info pos v ty op a1 a2 e
       | Fj_ir.LetApply (v, ty, f, args, e) ->
            build_apply_exp env info pos v ty f args e
       | Fj_ir.LetApplyMethod (v, ty, f, a, args, e) ->
            build_apply_method_exp env info pos v ty f a args e
       | Fj_ir.LetExt (v, ty1, s, ty2, args, e) ->
            build_ext_exp env info pos v ty1 s ty2 args e
       | Fj_ir.TailCall (f, args) ->
            build_tailcall_exp env info pos f args
       | Fj_ir.Return (f, a) ->
            build_return_exp env info pos f a
       | Fj_ir.IfThenElse (a, e1, e2) ->
            build_if_exp env info pos a e1 e2
       | Fj_ir.Try (e1, v, e2) ->
            build_try_exp env info pos e1 v e2
       | Fj_ir.Raise a ->
            build_raise_exp env info pos a
       | Fj_ir.TypeCase (a, cases, e) ->
            build_typecase_exp env info pos a cases e
       | Fj_ir.SetVar (v, ty, a, e) ->
            build_setvar_exp env info pos v ty a e
       | Fj_ir.LetSubscript (v, ty, a1, a2, e) ->
            build_subscript_exp env info pos v ty a1 a2 e
       | Fj_ir.SetSubscript (a1, a2, ty, a3, e) ->
            build_set_subscript_exp env info pos a1 a2 ty a3 e
       | Fj_ir.LetProject (v, ty, a, label, e) ->
            build_project_exp env info pos v ty a label e
       | Fj_ir.SetProject (a1, label, ty, a2, e) ->
            build_set_project_exp env info pos a1 label ty a2 e
       | Fj_ir.LetString (v, s, e) ->
            build_string_exp env info pos v s e
       | Fj_ir.LetArray (v, ty, args, a, e) ->
            build_array_exp env info pos v ty args a e
       | Fj_ir.LetObject (v, ty, cname, e) ->
            build_object_exp env info pos v ty cname e
       | Fj_ir.LetVObject (v, ty, iname, cname, a, e) ->
            build_vobject_exp env info pos v ty iname cname a e
       | Fj_ir.LetProjObject (v, ty, a, e) ->
            build_proj_object_exp env info pos v ty a e

(*
 * Build a function.
 *)
and build_fun env info pos (gflag, ty, vars, body) =
   let pos = string_pos "build_fundef" pos in
   let gflag, ty, vars =
      match gflag with
         Fj_ir.FunLocalClass ->
            (* Local functions do not get cont/exnh arguments *)
            let ty = build_fun_type info ty in
               FunLocalClass, ty, vars
       | Fj_ir.FunContClass ->
            let ty = build_fun_type info ty in
               FunContClass, ty, vars
       | Fj_ir.FunGlobalClass ->
            let ty = build_type info ty in
            let vars = f_cont :: f_exnh :: vars in
               FunGlobalClass, ty, vars
       | Fj_ir.FunMethodClass ->
            let ty = build_type info ty in
            let vars =
               match vars with
                  v_this :: vars ->
                     v_this :: f_cont :: f_exnh :: vars
                | [] ->
                     raise (FirException (pos, 
                              StringError "method has no arguments"))
            in
               FunGlobalClass, ty, vars
   in

   (* Add function vars *)
   let _, ty_vars, _ = dest_fun_or_method_type env pos ty in
   let env = List.fold_left2 env_add_var env vars ty_vars in
   let info, body = build_exp env info body in
      info, (gflag, ty, vars, body)

and build_funs_exp env info pos funs e =
   let pos = string_pos "build_funs_exp" pos in
   let info, funs =
      List.fold_left (fun (info, funs) (f, def) ->
            let info, def = build_fun env info pos def in
            let funs = (f, def) :: funs in
               info, funs) (info, []) funs
   in
   let info, e = build_exp env info e in
   let e = LetFuns (List.rev funs, e) in
      info, e

(*
 * A variable declaration.
 *)
and build_var_exp env info pos v ty a e =
   let pos = string_pos "build_var_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
      info, LetVar (v, ty, a, e)

(*
 * A variable declaration.
 *)
and build_atom_exp env info pos v ty a e =
   let pos = string_pos "build_atom_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
      info, LetAtom (v, ty, a, e)

(*
 * Unary operators.
 *)
and build_unop_exp env info pos v ty op a e =
   let pos = string_pos "build_unop_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
   let e =
      match op with
         Fj_ir.UMinusIntOp ->
            LetUnop (v, ty, UMinusIntOp, a, e)
       | Fj_ir.UMinusFloatOp ->
            LetUnop (v, ty, UMinusFloatOp, a, e)
       | Fj_ir.UNotIntOp ->
            LetUnop (v, ty, UNotIntOp, a, e)
       | Fj_ir.UNotBoolOp ->
            LetUnop (v, ty, UNotBoolOp, a, e)
       | Fj_ir.UIntOfFloat ->
            LetUnop (v, ty, UIntOfFloat, a, e)
       | Fj_ir.UFloatOfInt ->
            LetUnop (v, ty, UFloatOfInt, a, e)
   in
      info, e

(*
 * Binary operators.
 *)
and build_binop_exp env info pos v ty op a1 a2 e =
   let pos = string_pos "build_binop_exp" pos in
   let ty = build_type info ty in
   let a1 = build_atom a1 in
   let a2 = build_atom a2 in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
   let op =
      match op with
         (* Integers *)
         Fj_ir.AddIntOp -> AddIntOp
       | Fj_ir.SubIntOp -> SubIntOp
       | Fj_ir.MulIntOp -> MulIntOp
       | Fj_ir.DivIntOp -> DivIntOp
       | Fj_ir.RemIntOp -> RemIntOp
       | Fj_ir.AndIntOp -> AndIntOp
       | Fj_ir.OrIntOp -> OrIntOp
       | Fj_ir.LslIntOp -> LslIntOp
       | Fj_ir.LsrIntOp -> LsrIntOp
       | Fj_ir.AsrIntOp -> AsrIntOp
       | Fj_ir.XorIntOp -> XorIntOp
       | Fj_ir.EqIntOp -> EqIntOp
       | Fj_ir.NeqIntOp -> NeqIntOp
       | Fj_ir.LeIntOp -> LeIntOp
       | Fj_ir.LtIntOp -> LtIntOp
       | Fj_ir.GtIntOp -> GtIntOp
       | Fj_ir.GeIntOp -> GeIntOp

         (* Floats *)
       | Fj_ir.AddFloatOp -> AddFloatOp
       | Fj_ir.SubFloatOp -> SubFloatOp
       | Fj_ir.MulFloatOp -> MulFloatOp
       | Fj_ir.DivFloatOp -> DivFloatOp
       | Fj_ir.RemFloatOp -> RemFloatOp
       | Fj_ir.EqFloatOp -> EqFloatOp
       | Fj_ir.NeqFloatOp -> NeqFloatOp
       | Fj_ir.LeFloatOp -> LeFloatOp
       | Fj_ir.LtFloatOp -> LtFloatOp
       | Fj_ir.GtFloatOp -> GtFloatOp
       | Fj_ir.GeFloatOp -> GeFloatOp
   in
      info, LetBinop (v, ty, op, a1, a2, e)

(*
 * CPS convert an application.
 *)
and build_apply_exp env info pos v ty f args e =
   let pos = string_pos "build_apply_exp" pos in
   let ty = build_type info ty in
   let env = env_add_var env v ty in
   let args = List.map build_atom args in

   (* Wrap the rest in a contination function *)
   let f_cont = new_symbol_string (Symbol.to_string f ^ "_cont") in
   let info, e = build_exp env info e in
   let e =
      LetFuns ([f_cont, (FunContClass, TyFun ([ty], TyVoid), [v], e)],
      TailCall (f, AtomVar f_cont :: AtomVar f_exnh :: args))
   in
      info, e

and build_apply_method_exp env info pos v ty f a args e =
   let pos = string_pos "build_apply_exp" pos in
   let ty = build_type info ty in
   let env = env_add_var env v ty in
   let a = build_atom a in
   let args = List.map build_atom args in

   (* Wrap the rest in a contination function *)
   let f_cont = new_symbol_string (Symbol.to_string f ^ "_cont") in
   let info, e = build_exp env info e in
   let e =
      LetFuns ([f_cont, (FunContClass, TyFun ([ty], TyVoid), [v], e)],
      MethodCall (f, a, AtomVar f_cont :: AtomVar f_exnh :: args))
   in
      info, e

(*
 * External call.
 *)
and build_ext_exp env info pos v ty1 s ty2 args e =
   let pos = string_pos "build_ext_exp" pos in
   let ty1 = build_ext_fun_type info ty1 in
   let ty2 = build_ext_fun_type info ty2 in
   let env = env_add_var env v ty1 in
   let args = List.map build_atom args in
   let info, e = build_exp env info e in
      info, LetExt (v, ty1, s, ty2, args, e)

(*
 * Tailcall.
 *)
and build_tailcall_exp env info pos f args =
   let pos = string_pos "build_ext_exp" pos in
   let args = List.map build_atom args in
      info, TailCall (f, args)

(*
 * Instead of returning, call the continuation.
 *)
and build_return_exp env info pos f a =
   let pos = string_pos "build_return_exp" pos in
   let a = build_atom a in
      info, TailCall (f_cont, [a])

(*
 * Conditional.
 *)
and build_if_exp env info pos a e1 e2 =
   let pos = string_pos "build_if_exp" pos in
   let a = build_atom a in
   let info, e1 = build_exp env info e1 in
   let info, e2 = build_exp env info e2 in
      info, IfThenElse (a, e1, e2)

(*
 * Exceptions.
 *)
and build_try_exp env info pos e1 v e2 =
   let pos = string_pos "build_try_exp" pos in
   let env' = env_add_var env v info.info_object in
   let info, e1 = build_exp env info e1 in
   let info, e2 = build_exp env info e2 in

   (* Add a new exception handler *)
   let f_exnh' = new_symbol f_exnh in
   let ty_object = info.info_object in
   let e =
      LetFuns ([f_exnh', (FunContClass, TyFun ([ty_object], TyVoid), [v], e2)],
      LetFuns ([f_exnh, (FunContClass, TyFun ([ty_object], TyVoid), [v], TailCall (f_exnh', [AtomVar v]))],
      e1))
   in
      info, e

(*
 * When an exception is raised, call the exception handler with
 * the argument.
 *)
and build_raise_exp env info pos a =
   let pos = string_pos "build_raise_exp" pos in
   let a = build_atom a in
      info, TailCall (f_exnh, [a])

(*
 * SetVar is unchanged.
 *)
and build_setvar_exp env info pos v ty a e =
   let pos = string_pos "build_setvar_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let info, e = build_exp env info e in
      info, SetVar (v, ty, a, e)

(*
 * Subscripting.
 *)
and build_subscript_exp env info pos v ty a1 a2 e =
   let pos = string_pos "build_subscript_exp" pos in
   let ty = build_type info ty in
   let a1 = build_atom a1 in
   let a2 = build_atom a2 in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
      info, LetSubscript (v, ty, a1, a2, e)

and build_set_subscript_exp env info pos a1 a2 ty a3 e =
   let pos = string_pos "build_set_subscript_exp" pos in
   let ty = build_type info ty in
   let a1 = build_atom a1 in
   let a2 = build_atom a2 in
   let a3 = build_atom a3 in
   let info, e = build_exp env info e in
      info, SetSubscript (a1, a2, ty, a3, e)

(*
 * Convert projections to subscripts.
 *)
and build_project_exp env info pos v ty a label e =
   let pos = string_pos "build_project_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
   let e =
      match info_lookup_label info pos label with
         ObjectField ->
            LetProject (v, ty, a, label, e)
       | ClassMethod ->
            let v' = new_symbol v in
            let ty_object = type_of_atom env pos a in
            let ty_object = dest_object_type env pos ty_object in
            let ty_fields = dest_fields_type env pos ty_object in
            let ty_class = type_of_field env pos ty_fields class_var in
               LetProject (v', ty_class, a, class_var,
               LetProject (v, ty, AtomVar v', label,
               e))
       | InterfaceMethod ->
            let v' = new_symbol v in
            let ty_vobject = type_of_atom env pos a in
            let ty_fields = dest_vobject_type env pos ty_vobject in
            let ty_methods = type_of_field env pos ty_fields vmethods_var in
               LetProject (v', ty_methods, a, vmethods_var,
               LetProject (v, ty, AtomVar v', label,
               e))
   in
      info, e

and build_set_project_exp env info pos a1 label ty a2 e =
   let pos = string_pos "build_set_project_exp" pos in
   let ty = build_type info ty in
   let a1 = build_atom a1 in
   let a2 = build_atom a2 in
   let info, e = build_exp env info e in
   let e =
      match info_lookup_label info pos label with
         ObjectField ->
            SetProject (a1, label, ty, a2, e)
       | ClassMethod
       | InterfaceMethod ->
            raise (FirException (pos, StringVarError ("field is immutable", label)))
   in
      info, e

(*
 * String allocation.
 * Add the string to the globals.
 *)
and build_string_exp env info pos v s e =
   let pos = string_pos "build_string_exp" pos in
   let v_string = new_symbol v in
   let info = info_add_global info v_string (TyString, InitString s) in
   let env = env_add_var env v TyString in
   let info, e = build_exp env info e in
      info, LetAtom (v, TyString, AtomVar v_string, e)

(*
 * Array allocation.
 *)
and build_array_exp env info pos v ty args a e =
   let pos = string_pos "build_array_exp" pos in
   let ty = build_type info ty in
   let args = List.map build_atom args in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
      info, LetArray (v, ty, args, a, e)

(*
 * Object allocation.
 * Look up the class in the environment.
 *)
and build_object_exp env info pos v ty cname e =
   let pos = string_pos "build_object_exp" pos in
   let ty = build_type info ty in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
   let args = info_lookup_class info pos cname in
      info, LetRecord (v, ty, RecordFields, args, e)

(*
 * Interface allocation.
 *)
and build_vobject_exp env info pos v ty iname cname a e =
   let pos = string_pos "build_vobject_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
   let v_vma = info_lookup_interface info pos cname iname in
   let args = FieldTable.empty in
   let args = FieldTable.add args vmethods_var (AtomVar v_vma) in
   let args = FieldTable.add args vobject_var a in
      info, LetRecord (v, ty, RecordVObject, args, e)

(*
 * Project the object from the vobject.
 *)
and build_proj_object_exp env info pos v ty a e =
   let pos = string_pos "build_proj_object_exp" pos in
   let ty = build_type info ty in
   let a = build_atom a in
   let env = env_add_var env v ty in
   let info, e = build_exp env info e in
      info, LetProject (v, ty, a, vobject_var, e)

(*
 * Typecase.
 *)
and build_typecase_exp env info pos a cases e =
   let pos = string_pos "build_typecase_exp" pos in
   let a = build_atom a in
   let info, e = build_exp env info e in
   let rec build_cases info cases =
      match cases with
         (cname, v, body) :: cases ->
            let env = env_add_var env v (TyApply (cname, [])) in
            let info, body = build_exp env info body in
            let info, e = build_cases info cases in
               info, IfType (a, cname, v, body, e)
       | [] ->
            info, e
   in
      build_cases info cases

(*
 * Build a function.
 *)
let build_fundef env info f def =
   let pos = string_pos "build_fundef" (vexp_pos f) in
   let info, def = build_fun env info pos def in
      info_add_fun info f def

(*
 * Convert a program.
 *)
let build_prog prog =
   let { Fj_ir.prog_types = types;       (* Fj_ir.tydef SymbolTable.t *)
         Fj_ir.prog_funs = funs;         (* Fj_ir.fun_info SymbolTable.t *)
         Fj_ir.prog_main = main;         (* Fj_ir.var *)
         Fj_ir.prog_object = v_object;   (* Fj_ir.var *)
         Fj_ir.prog_imports = imports    (* Fj_ir.tydef SymbolTable.t *)
       } = prog
   in
   let i_imports = info_empty v_object in
   
   (* Convert the imported interfaces *)
   let i_imports =
      SymbolTable.fold (fun info v tydef ->
            build_interface_tydef info v tydef) i_imports imports
   in

   (* Convert the imported classes *)
   let i_imports =
      SymbolTable.fold (fun info v tydef ->
            build_class_tydef info v tydef) i_imports imports
   in
   
   let info = {(info_empty v_object) with info_imports = Some i_imports} in
   
   (* Convert the interfaces *)
   let info =
      SymbolTable.fold (fun info v tydef ->
            build_interface_tydef info v tydef) info types
   in

   (* Convert the classes *)
   let info =
      SymbolTable.fold (fun info v tydef ->
            build_class_tydef info v tydef) info types
   in

   (* Initial environment *)
   let env = match info.info_imports with
      Some info ->
         SymbolTable.fold env_add_type env_empty info.info_types
    | None ->
         failwith "shouldn't get here..."
   in
   let env = SymbolTable.fold env_add_type env info.info_types in

   (* Convert all the funs *)
   let info =
      SymbolTable.fold (fun info f def ->
            build_fundef env info f def) info funs
   in

   (* Collect the info into a program *)
   let { info_globals = globals;
         info_tynames = tynames;
         info_types = types;
         info_funs = funs;
         info_imports = imports;
         info_exports = exports
       } = info
   in
   (* The exports of the imports become the imports of the prog.  Got that? *)
   let import_table = match imports with
      Some imports_info ->
         imports_info.info_exports
    | None ->
         failwith "Internal error: info_imports not built!!..."
   in
   
   let prog =
      { prog_types = types;
        prog_tynames = tynames;
        prog_funs = funs;
        prog_main = main;
        prog_object = v_object;
        prog_globals = globals;
        prog_imports = import_table;
        prog_exports = exports
      }
   in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
