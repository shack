(*
 * Lift all functions to the top-level.
 * This assumes the closure conversion has been performed.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fj_fir

(*
 * Lift out all function definitions.
 *)
let rec lift_exp funs e =
   match e with
      LetFuns (funs', e) ->
         lift_funs_exp funs funs' e
    | LetVar (v, ty, a, e) ->
         let funs, e = lift_exp funs e in
            funs, LetVar (v, ty, a, e)
    | LetAtom (v, ty, a, e) ->
         let funs, e = lift_exp funs e in
            funs, LetAtom (v, ty, a, e)
    | LetUnop (v, ty, op, a, e) ->
         let funs, e = lift_exp funs e in
            funs, LetUnop (v, ty, op, a, e)
    | LetBinop (v, ty, op, a1, a2, e) ->
         let funs, e = lift_exp funs e in
            funs, LetBinop (v, ty, op, a1, a2, e)
    | LetExt (v, ty1, s, ty2, args, e) ->
         let funs, e = lift_exp funs e in
            funs, LetExt (v, ty1, s, ty2, args, e)
    | TailCall _
    | MethodCall _ ->
         funs, e
    | IfThenElse (a, e1, e2) ->
         let funs, e1 = lift_exp funs e1 in
         let funs, e2 = lift_exp funs e2 in
            funs, IfThenElse (a, e1, e2)
    | IfType (a, name, v, e1, e2) ->
         let funs, e1 = lift_exp funs e1 in
         let funs, e2 = lift_exp funs e2 in
            funs, IfType (a, name, v, e1, e2)
    | SetVar (v, ty, a, e) ->
         let funs, e = lift_exp funs e in
            funs, SetVar (v, ty, a, e)
    | LetSubscript (v, ty, a1, a2, e) ->
         let funs, e = lift_exp funs e in
            funs, LetSubscript (v, ty, a1, a2, e)
    | SetSubscript (a1, a2, ty, a3, e) ->
         let funs, e = lift_exp funs e in
            funs, SetSubscript (a1, a2, ty, a3, e)
    | LetProject (v, ty, a, label, e) ->
         let funs, e = lift_exp funs e in
            funs, LetProject (v, ty, a, label, e)
    | SetProject (a1, label, ty, a2, e) ->
         let funs, e = lift_exp funs e in
            funs, SetProject (a1, label, ty, a2, e)
    | LetArray (v, ty, args, a, e) ->
         let funs, e = lift_exp funs e in
            funs, LetArray (v, ty, args, a, e)
    | LetRecord (v, ty, rclass, args, e) ->
         let funs, e = lift_exp funs e in
            funs, LetRecord (v, ty, rclass, args, e)

(*
 * Recursively lift function definitions.
 *)
and lift_funs_exp funs funs' e =
   let funs =
      List.fold_left (fun funs (f, (gflag, ty, vars, body)) ->
            let funs, body = lift_exp funs body in
               SymbolTable.add funs f (gflag, ty, vars, body)) funs funs'
   in
      lift_exp funs e

(*
 * Lift all the function definitions out of the function bodies.
 *)
let lift_prog prog =
   let { prog_funs = funs } = prog in
   let funs =
      SymbolTable.fold (fun funs f (gflag, ty, vars, body) ->
            let funs, body = lift_exp funs body in
               SymbolTable.add funs f (gflag, ty, vars, body)) funs funs
   in
      { prog with prog_funs = funs }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
