(*
 * Type checking.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug
open Field_table
open Symbol

open Fj_fir
open Fj_fir_env
open Fj_fir_exn
open Fj_fir_type
open Fj_fir_state
open Fj_fir_subst

let vexp_pos v =
   string_pos "Fj_fir_check" (vexp_pos v)

let fir_exp_pos pos =
   string_pos "Fj_fir_check" (fir_exp_pos pos)

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Membership in an assoc list.
 *)
let rec all_mem_assoc v = function
   (v', v'') :: l ->
      Symbol.eq v v' || Symbol.eq v v'' || all_mem_assoc v l
 | [] ->
      false

(*
 * Equality of vars.
 *)
let rec mem_eq v1 v2 = function
   (v1', v2') :: tl ->
      (Symbol.eq v1 v1' && Symbol.eq v2 v2') || (Symbol.eq v1 v2' && Symbol.eq v2 v1') || mem_eq v1 v2 tl
 | [] ->
      false

(*
 * Add the vars to the venv (wich is a assoc list).
 *)
let fold_vars eq_vars pos vars1' vars2' =
   let rec loop eq_vars vars1 vars2 =
      match vars1, vars2 with
         v1 :: vars1, v2 :: vars2 ->
            loop ((v1, v2) :: eq_vars) vars1 vars2
       | [], [] ->
            eq_vars
       | _ ->
            raise (FirException (pos, ArityMismatch (List.length vars1', List.length vars2')))
   in
      loop eq_vars vars1' vars2'

(************************************************************************
 * TYPE EQUALITY
 ************************************************************************)

(*
 * Compare two types.
 * Returns true iff the types are equal.
 *)
let rec equal_types env vars pos ty1 ty2 =
   let pos = string_pos "equal_types" pos in
      match ty1, ty2 with
         TyVoid, TyVoid
       | TyUnit, TyUnit
       | TyNil, TyNil
       | TyBool, TyBool
       | TyChar, TyChar
       | TyString, TyString
       | TyInt, TyInt
       | TyFloat, TyFloat ->
            true

       | TyArray ty1, TyArray ty2 ->
            equal_types env vars pos ty1 ty2

       | TyFun (ty_vars1, ty_res1), TyFun (ty_vars2, ty_res2) ->
            equal_type_lists env vars pos ty_vars2 ty_vars1
            && equal_types env vars pos ty_res1 ty_res2

       | TyMethod (ty_this1, ty_vars1, ty_res1), TyMethod (ty_this2, ty_vars2, ty_res2) ->
            equal_types env vars pos ty_this1 ty_this2
            && equal_type_lists env vars pos ty_vars2 ty_vars1
            && equal_types env vars pos ty_res1 ty_res2

       | TyRecord (rclass1, tyl1), TyRecord (rclass2, tyl2) ->
            rclass1 = rclass2 && equal_field_types env vars pos tyl1 tyl2

       | TyNames (v1, names1), TyNames (v2, names2) ->
            Symbol.eq v1 v2 && equal_name_lists env vars pos names1 names2

       | TyApply (name1, tyl1), TyApply (name2, tyl2) when Symbol.eq name1 name2 ->
            equal_type_lists env vars pos tyl1 tyl2

       | TyApply (name1, tyl1), _ ->
            equal_types env vars pos (apply_type env pos name1 tyl1) ty2

       | _, TyApply (name2, tyl2) ->
            equal_types env vars pos ty1 (apply_type env pos name2 tyl2)

       | TyObject (v1, ty1), TyObject (v2, ty2) ->
            equal_types env ((v1, v2) :: vars) pos ty1 ty2

         (* Polymorphism *)
       | TyVar v1, TyVar v2 ->
            mem_eq v1 v2 vars

       | TyProject (v1, i1), TyProject (v2, i2) ->
            i1 = i2 && Symbol.eq v1 v2

       | TyAll ([], ty1), _
       | TyExists ([], ty1), _
       | TyLambda ([], ty1), _ ->
            equal_types env vars pos ty1 ty2

       | _, TyAll ([], ty2)
       | _, TyExists ([], ty2)
       | _, TyLambda ([], ty2) ->
            equal_types env vars pos ty1 ty2

       | TyAll (vars1, ty1), TyAll (vars2, ty2)
       | TyExists (vars1, ty1), TyExists (vars2, ty2)
       | TyLambda (vars1, ty1), TyLambda (vars2, ty2) ->
            let len1 = List.length vars1 in
            let len2 = List.length vars2 in
               if len1 = len2 then
                  let vars =
                     List.fold_left2 (fun vars v1 v2 ->
                           (v1, v2) :: vars) vars vars1 vars2
                  in
                     equal_types env vars pos ty1 ty2
               else
                  false
       | _ ->
            false

and equal_types_opt env vars pos ty_opt1 ty_opt2 =
   match ty_opt1, ty_opt2 with
      Some ty1, Some ty2 ->
         equal_types env vars pos ty1 ty2
    | None, None ->
         true
    | _ ->
         false

and equal_type_lists env vars pos tyl1 tyl2 =
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
      len1 = len2 && List.for_all2 (equal_types env vars pos) tyl1 tyl2

(*
 * Record types have to have all the fields in the same order.
 *)
and equal_field_type env vars pos (v1, ty1) (v2, ty2) =
   equal_types env vars pos ty1 ty2

and equal_field_types env vars pos fields1 fields2 =
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
   let len1 = List.length fields1 in
   let len2 = List.length fields2 in
      len1 = len2 && List.for_all2 (equal_field_type env vars pos) fields1 fields2

(*
 * Check if two name lists are the same.
 *)
and equal_names env vars pos (v1, ty1) (v2, ty2) =
   Symbol.eq v1 v2 && equal_types_opt env vars pos ty1 ty2

and equal_name_lists env vars pos names1 names2 =
   let len1 = List.length names1 in
   let len2 = List.length names2 in
      len1 = len2 && List.for_all2 (equal_names env vars pos) names1 names2

(*
 * Method type equality discards the self type.
 *)
let equal_method_types env eq_vars pos ty1 ty2 =
   let pos = string_pos "equal_method_types" pos in
      equal_types env eq_vars pos ty1 ty2

(************************************************************************
 * TYPE CHECKING
 ************************************************************************)

(*
 * Check that one list is a subtype of another.
 * they should have the same names on the parts where they
 * agree.
 *)
let rec check_types_subtype env eq_vars subst pos ty1' ty2' tyl1 tyl2 =
   let pos = string_pos "check_types_subtype" pos in
   let rec check subst tyl1 tyl2 =
      match tyl1, tyl2 with
         (v1, ty1) :: tyl1, (v2, ty2) :: tyl2 ->
            if not (Symbol.eq v1 v2) then
               raise (FirException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
            let subst = check_types false env eq_vars subst pos ty1' ty2' ty1 ty2 in
               check subst tyl1 tyl2
       | [], _ ->
            subst
       | _, [] ->
            raise (FirException (pos, StringError "not a subtype"))
   in
      check subst tyl1 tyl2

(*
 * Check that the first record is a subtype of the second.
 *)
and check_frame_types env eq_vars subst pos ty1' ty2' ty1 ty2 =
   let pos = string_pos "check_frame_types" pos in
   let tyl1 = dest_frame_type env pos ty1 in
   let tyl2 = dest_frame_type env pos ty2 in
      check_types_subtype env eq_vars subst pos ty1' ty2' (FieldTable.to_list tyl1) (FieldTable.to_list tyl2)

(*
 * Check that the first record is a subtype of the second.
 *)
and check_closure_types env eq_vars subst pos ty1' ty2' ty1 ty2 =
   let pos = string_pos "check_frame_types" pos in
   let tyl1 = dest_closure_type env pos ty1 in
   let tyl2 = dest_closure_type env pos ty2 in
      check_types_subtype env eq_vars subst pos ty1' ty2' (FieldTable.to_list tyl1) (FieldTable.to_list tyl2)

(*
 * Check that all the names in the first name record are also listed
 * in the second.  Note, this algorithm is quadratic, probably want to
 * use SymbolTables.
 *)
and check_names_types env eq_vars subst pos ty1' ty2' ty_names1 ty_names2 =
   let pos = string_pos "check_names_types" pos in
   let _, names1 = dest_names_type env pos ty_names1 in
   let _, names2 = dest_names_type env pos ty_names2 in
   let check_name subst (v1, ty_opt1) =
      let ty_opt2 =
         try List.assoc v1 names2 with
            Not_found ->
               raise (FirException (pos, StringVarError ("name is not implemented", v1)))
      in
         if equal_types_opt env eq_vars pos ty_opt1 ty_opt2 then
            subst
         else
            raise (FirException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty_names1, ty_names2)))
   in
      List.fold_left check_name subst names1

(*
 * Check that the first method list is shorter than
 * the second and that all the methods have the same
 * types, ignoring the first parameter.
 *)
and check_methods_subtype env eq_vars subst pos ty1' ty2' tyl1 tyl2 =
   let pos = string_pos "check_methods_subtype" pos in
   let rec check subst tyl1 tyl2 =
      match tyl1, tyl2 with
         (v1, ty1) :: tyl1, (v2, ty2) :: tyl2 ->
            let subst = check_types false env eq_vars subst pos ty1' ty2' ty1 ty2 in
               check subst tyl1 tyl2
       | [], _ ->
            subst
       | _, [] ->
            raise (FirException (pos, StringError "too many methods"))
   in
      check subst tyl1 tyl2

and check_methods_types env eq_vars subst pos ty1' ty2' ty_methods1 ty_methods2 =
   let pos = string_pos "check_methods_types" pos in
   let fields1 = dest_methods_type env pos ty_methods1 in
   let fields2 = dest_methods_type env pos ty_methods2 in
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
      check_methods_subtype env eq_vars subst pos ty1' ty2' fields1 fields2

(*
 * Check if an object from the second class can
 * be coerced to an object of the first class.
 *)
and check_class_types env eq_vars subst pos ty1' ty2' ty_class1 ty_class2 =
   let pos = string_pos "check_class_types" pos in
   let fields1 = dest_class_type env pos ty_class1 in
   let fields2 = dest_class_type env pos ty_class2 in
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
      match fields1, fields2 with
         (_, ty_names1) :: ty_methods1, (_, ty_names2) :: ty_methods2 ->
            let subst = check_names_types env eq_vars subst pos ty1' ty2' ty_names1 ty_names2 in
               check_methods_subtype env eq_vars subst pos ty1' ty2' ty_methods1 ty_methods2
       | _ ->
            raise (FirException (pos, StringError "illegal class definition"))

(*
 * Check if one object can be coerced to another.
 *)
and check_object_types env eq_vars subst pos ty1' ty2' ty_object1 ty_object2 =
   let pos = string_pos "check_object_types" pos in
   let ty_object1 = unfold_object_type env pos ty_object1 in
   let ty_object2 = unfold_object_type env pos ty_object2 in
   let fields1 = dest_fields_type env pos ty_object1 in
   let fields2 = dest_fields_type env pos ty_object2 in
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
      match fields1, fields2 with
         (_, ty_class1) :: ty_fields1, (_, ty_class2) :: ty_fields2 ->
            let subst = check_class_types env eq_vars subst pos ty1' ty2' ty_class1 ty_class2 in
               check_types_subtype env eq_vars subst pos ty1' ty2' ty_fields1 ty_fields2
       | _ ->
            raise (FirException (pos, StringError "illegal object definition"))

(*
 * Check that one type can be converted to another.
 * The coerce_flag is true iff coercions between vobject
 * and object types are allowed.
 *)
and check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2 =
   let pos =
      if debug print_fir then
         fir_exn_pos (TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)) pos
      else
         pos
   in
      match ty1, ty2 with
         TyString, TyNil
       | TyArray _, TyNil
       | TyFun _, TyNil
       | TyRecord _, TyNil ->
            subst

         (* Polymorphism *)
       | TyVar v1, TyVar v2 ->
            if Symbol.eq v1 v2 || mem_eq v1 v2 eq_vars then
               subst
            else if all_mem_assoc v1 eq_vars || all_mem_assoc v2 eq_vars then
               raise (FirException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)))
            else
               (try
                   let ty1 = subst_lookup subst v1 in
                      check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2
                with
                   Not_found ->
                      try
                         let ty2 = subst_lookup subst v2 in
                            check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2
                      with
                         Not_found ->
                            subst_add subst v1 ty2)
       | TyVar v1, _ ->
            if all_mem_assoc v1 eq_vars then
               raise (FirException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
            (try
                let ty1 = subst_lookup subst v1 in
                   check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2
             with
                Not_found ->
                   subst_add subst v1 ty2)
       | _, TyVar v2 ->
            if all_mem_assoc v2 eq_vars then
               raise (FirException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)));
            (try
                let ty2 = subst_lookup subst v2 in
                   check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2
             with
                Not_found ->
                   subst_add subst v2 ty1)

       | TyProject (v1, i1), TyProject (v2, i2) ->
            if i1 = i2 && Symbol.eq v1 v2 then
               subst
            else
               raise (FirException (pos, TypeError2 (ty1, ty2)))

       | TyAll ([], ty1), _
       | TyExists ([], ty1), _
       | TyLambda ([], ty1), _ ->
            check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2

       | _, TyAll ([], ty2)
       | _, TyExists ([], ty2)
       | _, TyLambda ([], ty2) ->
            check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyAll (vars1, ty1), TyAll (vars2, ty2)
       | TyExists (vars1, ty1), TyExists (vars2, ty2)
       | TyLambda (vars1, ty1), TyLambda (vars2, ty2) ->
            let eq_vars = fold_vars eq_vars pos vars1 vars2 in
               check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyExists (_, ty1), ty2 ->
            check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2

         (* Aggregates *)
       | TyArray ty1, TyArray ty2 ->
            check_types true env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyFun (ty_vars1, ty_res1), TyFun (ty_vars2, ty_res2) ->
            let subst = check_type_lists env eq_vars subst pos ty1' ty2' ty_vars2 ty_vars1 in
               check_types false env eq_vars subst pos ty1' ty2' ty_res1 ty_res2

       | TyMethod (ty_this1, ty_vars1, ty_res1), TyMethod (ty_this2, ty_vars2, ty_res2) ->
            let subst = check_types false env eq_vars subst pos ty1' ty2' ty_this2 ty_this1 in
            let subst = check_type_lists env eq_vars subst pos ty1' ty2' ty_vars2 ty_vars1 in
               check_types false env eq_vars subst pos ty1' ty2' ty_res1 ty_res2

         (* Names record *)
       | TyNames _, _ ->
            check_names_types env eq_vars subst pos ty1' ty2' ty1 ty2

         (* Record subtyping *)
       | TyRecord (RecordFrame, _), _ ->
            check_frame_types env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyRecord (RecordClosure, _), _ ->
            check_closure_types env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyRecord (RecordClass, _), _ ->
            check_class_types env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyRecord (RecordMethods, _), _ ->
            check_methods_types env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyRecord (RecordFields, _), _ ->
            check_object_types env eq_vars subst pos ty1' ty2' ty1 ty2

         (* Object types *)
       | TyObject (v1, ty1), TyObject (v2, ty2) ->
            let ty_id = TyApply (new_symbol_string "this", []) in
            let subst = subst_add subst v1 ty_id in
            let subst = subst_add subst v2 ty_id in
               check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2

       | TyObject (v1, ty1), _ ->
            let subst = subst_add subst v1 ty2 in
               check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 ty2

         (* Expand identifiers only when necessary *)
       | TyApply (name1, []), TyApply (name2, []) when Symbol.eq name1 name2 ->
            subst

         (* If the identifiers are not abstract, then expand them *)
       | TyApply (name1, tyl1), _ ->
            check_types coerce_flag env eq_vars subst pos ty1' ty2' (apply_type env pos name1 tyl1) ty2

       | _, TyApply (name2, tyl2) ->
            check_types coerce_flag env eq_vars subst pos ty1' ty2' ty1 (apply_type env pos name2 tyl2)

       | _ ->
            if equal_types env eq_vars pos ty1 ty2 then
               subst
            else
               raise (FirException (pos, TypeError4 (subst_type subst ty1', subst_type subst ty2', ty1, ty2)))

and check_type_lists env eq_vars subst pos ty1' ty2' tyl1 tyl2 =
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      List.fold_left2 (fun subst ty1 ty2 ->
            check_types false env eq_vars subst pos ty1' ty2' ty1 ty2) subst tyl1 tyl2

(*
 * Check the fields.
 * We don't allow subtyping here, the fields
 * have to match up exactly.
 *)
and check_field_type env eq_vars subst pos ty1' ty2' (v1, ty1) (v2, ty2) =
   if not (Symbol.eq v1 v2) then
      raise (FirException (pos, StringVarVarError ("field name mismatch", v1, v2)));
   check_types false env eq_vars subst pos ty1' ty2' ty1 ty2

and check_field_type_lists env eq_vars subst pos ty1' ty2' fields1 fields2 =
   let len1 = List.length fields1 in
   let len2 = List.length fields2 in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      List.fold_left2 (fun subst field1 field2 ->
            check_field_type env eq_vars subst pos ty1' ty2' field1 field2) subst fields1 fields2

and check_field_types env eq_vars subst pos ty1' ty2' fields1 fields2 =
   let fields1 = FieldTable.to_list fields1 in
   let fields2 = FieldTable.to_list fields2 in
      check_field_type_lists env eq_vars subst pos ty1' ty2' fields1 fields2

(*
 * By default, coercions are not allowed.
 *)
let check_types env pos ty1 ty2 =
   let pos = string_pos "check_types" pos in
      ignore (check_types false env [] subst_empty pos ty1 ty2 ty1 ty2)

(************************************************************************
 * TYPE WELL-FORMEDNESS
 ************************************************************************)

(*
 * The main objective here to to make sure that
 * object types have the right format.  We can ignore type ids
 * because we check them in a first pass over the program.
 *)
let rec check_type env pos ty =
   let pos = string_pos "check_type" pos in
      match ty with
         TyVoid
       | TyUnit
       | TyNil
       | TyBool
       | TyChar
       | TyString
       | TyInt
       | TyFloat
       | TyVar _
       | TyProject _ ->
            ()

       | TyArray ty ->
            check_type env pos ty

       | TyFun (ty_vars, ty_res) ->
            List.iter (check_type env pos) ty_vars;
            check_type env pos ty_res

       | TyMethod (ty_this, ty_vars, ty_res) ->
            check_type env pos ty_this;
            List.iter (check_type env pos) ty_vars;
            check_type env pos ty_res

       | TyRecord (RecordFields, fields) ->
            check_object_type env pos fields

       | TyRecord (_, fields1) ->
            FieldTable.iter (fun _ ty -> check_type env pos ty) fields1

       | TyNames (v, names) ->
            List.iter (check_names_type env pos v) names

       | TyApply (_, tyl) ->
            List.iter (check_type env pos) tyl

       | TyAll ([], ty)
       | TyExists ([], ty)
       | TyLambda ([], ty) ->
            check_type env pos ty

       | TyAll (_, ty)
       | TyExists (_, ty)
       | TyLambda (_, ty)
       | TyObject (_, ty) ->
            check_type env pos ty


and check_type_opt env pos ty_opt =
   match ty_opt with
      Some ty ->
         check_type env pos ty
    | None ->
         ()

(*
 * In a Names type, check that all the names are valid.
 *)
and check_names_type env pos v (name, ty_opt) =
   let pos = string_pos "check_names_type" pos in
   let _ = check_type_opt env pos ty_opt in
      match expand_type env pos (env_lookup_tyname env pos name) with
         TyObject _ ->
            (match ty_opt with
                None ->
                   ()
              | Some _ ->
                   raise (FirException (pos, StringVarError ("class name can't have initializer", name))))
       | TyRecord (RecordVObject, ty_fields) ->
            let ty1 = type_of_field env pos ty_fields vmethods_var in
               (match ty_opt with
                   Some ty2 ->
                      if not (equal_types env [] pos ty1 ty2) then
                         raise (FirException (pos, TypeError2 (ty1, ty2)))
                 | None ->
                      raise (FirException (pos, StringVarError ("interface requires an initializer", name))))
       | _ ->
            raise (FirException (pos, StringVarError ("not a class or interface", name)))

(*
 * Check that an object type refers to a class type,
 * and the class type has the right format.
 *)
and check_object_type env pos ty_fields =
   let pos = string_pos "check_object_type" pos in
   let ty_class = type_of_field env pos ty_fields class_var in
      if not (is_class_type env pos ty_class) then
         raise (FirException (pos, StringError "bad object type"))

(************************************************************************
 * EXPRESSION TYPES
 ************************************************************************)

(*
 * Get the type of an atom.
 *)
let type_of_atom env pos a =
   let pos = string_pos "type_of_atom" pos in
      match a with
         AtomUnit ->
            TyUnit
       | AtomBool _ ->
            TyBool
       | AtomChar _ ->
            TyChar
       | AtomInt _ ->
            TyInt
       | AtomFloat _ ->
            TyFloat
       | AtomNil ->
            TyNil
       | AtomVar v ->
            env_lookup_var env pos v

(*
 * General type checking.
 *)
let rec type_of_exp env code =
   let pos = string_pos "type_of_exp" (fir_exp_pos code) in
      match code with
         LetFuns (funs, e) ->
            type_of_funs_exp env pos funs e
       | LetAtom (v, ty, a, e)
       | LetVar  (v, ty, a, e) ->
            type_of_atom_exp env pos v ty a e
       | LetUnop (v, ty, op, a, e) ->
            type_of_unop_exp env pos v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            type_of_binop_exp env pos v ty op a1 a2 e
       | LetExt (v, ty1, s, ty2, args, e) ->
            type_of_ext_exp env pos v ty1 s ty2 args e
       | TailCall (f, args) ->
            type_of_tailcall_exp env pos f args
       | MethodCall (f, a, args) ->
            type_of_methodcall_exp env pos f a args
       | IfThenElse (a, e1, e2) ->
            type_of_if_exp env pos a e1 e2
       | IfType (a, cname, v, e1, e2) ->
            type_of_iftype_exp env pos a cname v e1 e2
       | SetVar (v, ty, a, e) ->
            type_of_set_var_exp env pos v ty a e
       | LetSubscript (v, ty, a1, a2, e) ->
            type_of_subscript_exp env pos v ty a1 a2 e
       | SetSubscript (a1, a2, ty, a3, e) ->
            type_of_set_subscript_exp env pos a1 a2 ty a3 e
       | LetProject (v, ty, a, l, e) ->
            type_of_project_exp env pos v ty a l e
       | SetProject (a1, l, ty, a2, e) ->
            type_of_set_project_exp env pos a1 l ty a2 e
       | LetArray (v, ty, args, a, e) ->
            type_of_array_exp env pos v ty args a e
       | LetRecord (v, ty, rclass, args, e) ->
            type_of_record_exp env pos v ty rclass args e

(*
 * Check the function declarations.
 *)
and check_fun env pos f gflag f_ty vars body =
   let pos = string_pos "check_fun" pos in

   (* Check params *)
   let _, ty_vars, ty_res = dest_fun_or_method_type env pos f_ty in
   let len1 = List.length ty_vars in
   let len2 = List.length vars in
   let _ =
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)))
   in

   (* Add the formal params to the env *)
   let env = List.fold_left2 env_add_var env vars ty_vars in

   (* Check return type *)
   let ty = type_of_exp env body in
      check_type env pos f_ty;
      check_types env pos TyVoid ty

(*
 * Check the function declarations.
 *)
and type_of_funs_exp env pos funs e =
   let pos = string_pos "type_of_funs_exp" pos in

   (* First, add all the functions to the env *)
   let env =
      List.fold_left (fun env (f, (_, f_ty, _, _)) ->
            env_add_var env f f_ty) env funs
   in
      (* Check fun definitions *)
      List.iter (fun (f, (gflag, f_ty, vars, body)) ->
            check_fun env pos f gflag f_ty vars body) funs;

      (* The type for the rest of the program *)
      type_of_exp env e

(*
 * Check a LetAtom.
 * Check that the type is the same as the argument.
 *)
and type_of_atom_exp env pos v ty a e =
   let pos = string_pos "type_of_atom_exp" pos in
      check_type env pos ty;
      check_types env pos ty (type_of_atom env pos a);
      type_of_exp (env_add_var env v ty) e

(*
 * Unary operation.
 *)
and type_of_unop_exp env pos v ty op a e =
   let pos = string_pos "type_of_unop_exp" pos in
   let ty_to, ty_from =
      match op with
         UMinusIntOp
       | UNotIntOp     -> TyInt,   TyInt
       | UMinusFloatOp -> TyFloat, TyFloat
       | UNotBoolOp    -> TyBool,  TyBool
       | UIntOfFloat   -> TyInt,   TyFloat
       | UFloatOfInt   -> TyFloat, TyInt
   in
   let ty2 = type_of_atom env pos a in
      check_type env pos ty;
      check_types env pos ty ty_to;
      check_types env pos ty_from ty2;
      type_of_exp (env_add_var env v ty) e

(*
 * Binary operation.
 *)
and type_of_binop_exp env pos v ty op a1 a2 e =
   let pos = string_pos "type_of_binop_exp" pos in
   let ty_dst, ty_src =
      match op with
         AddIntOp
       | SubIntOp
       | MulIntOp
       | DivIntOp
       | RemIntOp
       | LslIntOp
       | LsrIntOp
       | AsrIntOp
       | AndIntOp
       | OrIntOp
       | XorIntOp ->
            TyInt, TyInt

       | EqIntOp
       | NeqIntOp
       | LeIntOp
       | LtIntOp
       | GtIntOp
       | GeIntOp ->
            TyBool, TyInt

       | AddFloatOp
       | SubFloatOp
       | MulFloatOp
       | DivFloatOp
       | RemFloatOp ->
            TyFloat, TyFloat

       | EqFloatOp
       | NeqFloatOp
       | LeFloatOp
       | LtFloatOp
       | GtFloatOp
       | GeFloatOp ->
            TyBool, TyFloat

       | EqPtrOp
       | NeqPtrOp ->
            TyBool, TyNil
   in
      check_type env pos ty;
      check_types env pos ty ty_dst;
      check_types env pos ty_src (type_of_atom env pos a1);
      check_types env pos ty_src (type_of_atom env pos a2);
      type_of_exp (env_add_var env v ty) e

(*
 * Function application.
 * Check that the arguments have the right type and arity.
 *)
and type_of_tailcall_exp env pos f args =
   let pos = string_pos "type_of_tailcall_exp" pos in
   let f_ty = env_lookup_var env pos f in
      type_of_tailcall_type_exp env pos f_ty args

and type_of_tailcall_type_exp env pos f_ty args =
   let pos = string_pos "type_of_tailcall_type_exp" pos in
   let ty_vars, ty_res = dest_fun_type env pos f_ty in

   (* Check argument types *)
   let ty_args = List.map (type_of_atom env pos) args in
   let len1 = List.length ty_vars in
   let len2 = List.length ty_args in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      List.iter2 (check_types env pos) ty_vars ty_args;
      ty_res

and type_of_methodcall_exp env pos f a args =
   let pos = string_pos "type_of_methodcall_exp" pos in
   let f_ty = env_lookup_var env pos f in
   let ty_this, ty_vars, ty_res = dest_method_type env pos f_ty in

   (* Check argument types *)
   let ty_args = List.map (type_of_atom env pos) args in
   let len1 = List.length ty_vars in
   let len2 = List.length ty_args in
      if len1 <> len2 then
         raise (FirException (pos, ArityMismatch (len1, len2)));
      List.iter2 (check_types env pos) ty_vars ty_args;
      ty_res

and type_of_ext_exp env pos v ty1 s ty2 args e =
   let pos = string_pos "type_of_ext_exp" pos in
   let ty1' = type_of_tailcall_type_exp env pos ty2 args in
      check_type env pos ty1;
      check_type env pos ty2;
      check_types env pos ty1 ty1';
      type_of_exp (env_add_var env v ty1) e

(*
 * Check a conditional.
 *)
and type_of_if_exp env pos a e1 e2 =
   let pos = string_pos "type_of_if_exp" pos in
   let ty0 = type_of_atom env pos a in
   let ty1 = type_of_exp env e1 in
   let ty2 = type_of_exp env e2 in
      check_types env pos TyBool ty0;
      check_types env pos ty1 ty2;
      ty1

(*
 * Check a type test.
 *)
and type_of_iftype_exp env pos a cname v e1 e2 =
   let pos = string_pos "type_of_iftype_exp" pos in
   let ty0 = type_of_atom env pos a in
   let ty1 = type_of_exp (env_add_var env v
         (env_lookup_tyname env pos cname)) e1 in
   let ty2 = type_of_exp env e2 in
      if not (is_object_type env pos ty0) then
         raise (FirException (pos, StringTypeError ("not an object", ty0)));
      check_types env pos ty1 ty2;
      ty1

(*
 * Set a variable.
 *)
and type_of_set_var_exp env pos v ty a e =
   let pos = string_pos "type_of_set_exp" pos in
   let ty2 = type_of_atom env pos a in
   let ty1 = env_lookup_var env pos v in
      check_type env pos ty;
      check_types env pos ty ty1;
      check_types env pos ty ty2;
      type_of_exp env e

(*
 * Array elements.
 *)
and type_of_subscript_exp env pos v ty a1 a2 e =
   let pos = string_pos "type_of_subscript_exp" pos in
   let ty1 = type_of_atom env pos a1 in
   let ty2 = type_of_atom env pos a2 in
      check_type env pos ty;
      check_types env pos TyInt ty2;
      check_types env pos ty (dest_array_type env pos ty1);
      type_of_exp (env_add_var env v ty) e

and type_of_set_subscript_exp env pos a1 a2 ty a3 e =
   let pos = string_pos "type_of_set_subscript_exp" pos in
   let ty1 = type_of_atom env (int_pos 1 pos) a1 in
   let ty2 = type_of_atom env (int_pos 2 pos) a2 in
   let ty3 = type_of_atom env (int_pos 3 pos) a3 in
      check_type  env (int_pos 4 pos) ty;
      check_types env (int_pos 5 pos) (dest_array_type env pos ty1) ty;
      check_types env (int_pos 6 pos) TyInt ty2;
      check_types env (int_pos 7 pos) ty ty3;
      type_of_exp env e

(*
 * Record projection.
 *)
and type_of_project_exp env pos v ty a1 label e =
   let pos = string_pos "type_of_project_exp" pos in
   let ty1 = type_of_atom env pos a1 in
   let ty1 = unfold_exists_type env pos (var_of_atom pos a1) ty1 in
   let ty1 = unfold_object_type env pos ty1 in
   let _, ty_fields = dest_record_type env pos ty1 in
   let ty_field = type_of_field env pos ty_fields label in
      check_type env pos ty;
      check_types env pos ty ty_field;
      type_of_exp (env_add_var env v ty) e

and type_of_set_project_exp env pos a1 label ty a2 e =
   let pos = string_pos "type_of_set_project_exp" pos in
   let ty1 = type_of_atom env pos a1 in
   let ty2 = type_of_atom env pos a2 in
   let ty1 = unfold_exists_type env pos (var_of_atom pos a1) ty1 in
   let ty1 = unfold_object_type env pos ty1 in
   let _, ty_fields = dest_record_type env pos ty1 in
   let ty_field = type_of_field env pos ty_fields label in
      check_type env pos ty;
      check_types env pos ty_field ty;
      check_types env pos ty ty2;
      type_of_exp env e

(*
 * Array creation.
 *)
and type_of_array_exp env pos v ty args a e =
   let pos = string_pos "type_of_array_exp" pos in
   let rec type_of_array ty args =
      match args with
         a :: args ->
            check_types env pos TyInt (type_of_atom env pos a);
            type_of_array (TyArray ty) args
       | [] ->
            ty
   in
   let ty' = type_of_array (type_of_atom env pos a) args in
      check_type env pos ty;
      check_types env pos ty ty';
      type_of_exp (env_add_var env v ty) e

(*
 * Record creation.
 *)
and type_of_record_exp env pos v ty rclass args e =
   let pos = string_pos "type_of_record_exp" pos in
   let ty_args = FieldTable.map (fun a -> type_of_atom env pos a) args in
      check_type env pos ty;
      check_types env pos ty (TyRecord (rclass, ty_args));
      type_of_exp (env_add_var env v ty) e

(*
 * Check an initializer.
 *)
let type_of_name env pos (v, v_opt) =
   let pos = string_pos "type_of_name" pos in
   let ty_opt =
      match v_opt with
         Some v -> Some (env_lookup_var env pos v)
       | None -> None
   in
      v, ty_opt

let type_of_init env pos init =
   let pos = string_pos "type_of_init" pos in
      match init with
         InitString _ ->
            TyString
       | InitRecord (rclass, args) ->
            TyRecord (rclass, FieldTable.map (fun a -> type_of_atom env pos a) args)
       | InitNames (v, names) ->
            TyNames (v, List.map (type_of_name env pos) names)

let check_init env pos ty init =
   let pos = string_pos "check_init" pos in
   let ty' = type_of_init env pos init in
      check_type env pos ty;
      check_types env pos ty ty'

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Add the data segment.
 *)
let check_prog prog =
   let { prog_types = types;
         prog_tynames = tynames;
         prog_funs = funs;
         prog_main = main;
         prog_globals = globals
       } = prog
   in
   let env = env_of_prog prog in
      (* Check all the types *)
      SymbolTable.iter (fun v ty -> check_type env (vexp_pos v) ty) types;

      (* Check the type names *)
      SymbolTable.iter (fun v ty -> check_type env (vexp_pos v) ty) tynames;

      (* Make sure all the exports are defined *)
      if not (SymbolTable.mem funs main) then
         raise (FirException (vexp_pos main, StringVarError ("main is not defined", main)));

      (* Check globals *)
      SymbolTable.iter (fun v (ty, init) ->
            check_init env (vexp_pos v) ty init) globals;

      (* Check all the funs *)
      SymbolTable.iter (fun f (gflag, ty, vars, body) ->
            let pos = vexp_pos f in
               check_fun env pos f gflag ty vars body) funs

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
