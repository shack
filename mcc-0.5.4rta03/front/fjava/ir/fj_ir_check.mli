(*
 * Type checking.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Fj_ir
open Fj_ir_env
open Fj_ir_exn

(*
 * Type equality.
 *)
val equal_types : env -> ir_pos -> ty -> ty -> bool
val equal_method_types : env -> ir_pos -> ty -> ty -> ty -> ty -> bool

(*
 * Type of an atom.
 *)
val type_of_atom : env -> ir_pos -> atom -> ty

(*
 * Typecheck the IR.
 *)
val check_prog : prog -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
