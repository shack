(*
 * Basic type and variable environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Fj_ir
open Fj_ir_exn
open Fj_ir_state

(*
 * The environment contains type and variable environments
 * that define the types and variables in scope.  It also
 * contains the info for the class that is currently being
 * defined.
 *)
type env =
   { env_tenv      : tydef SymbolTable.t;
     env_itenv     : tydef SymbolTable.t;
     env_base      : ty SymbolTable.t;
     env_venv      : ty SymbolTable.t;
     env_fenv      : (var * ty) SymbolMTable.t;
     env_return    : (var * ty) list;
     env_class     : (var * class_info) option;
     (* breaks/continues are each:
         (target, number of finally blocks that need to be called) *)
     env_breaks    : (var * int) SymbolTable.t;
     env_continues : (var * int) SymbolTable.t;
     (* These are all the finallys, innermost first.  
         They all need to be called on return *)
     env_finally   : var list;
     env_package   : package option
   }

(* 
 * Canonical names for continue and break labels 
 *)
let break_var = Symbol.add "break"
let continue_var = Symbol.add "continue"

(*
 * Type operations.
 *)
let env_add_tydef env v ty =
   { env with env_tenv = SymbolTable.add env.env_tenv v ty }

let env_add_import_tydef env v ty =
   { env with env_itenv = SymbolTable.add env.env_itenv v ty }

let env_lookup_tydef env pos v =
   try SymbolTable.find env.env_tenv v with
      Not_found ->
         (* Is it an imported type definition? *)
         try SymbolTable.find env.env_itenv v with
            Not_found ->
               raise (IrException (pos, UnboundType v))

let env_get_tydefs env =
   env.env_tenv

let env_get_imports env =
   env.env_itenv

(*
 * Lookup a type from an identifier.
 * It is either a base type, a class, or an interface.
 *)
let env_lookup_type env pos v =
   try SymbolTable.find env.env_base v with
      Not_found ->
         match env_lookup_tydef env pos v with
            TyDefClass _ ->
               TyObject v
          | TyDefInterface _ ->
               TyVObject v

(*
 * Variable operations.
 *)
let env_add_var env v ty =
   { env with env_venv = SymbolTable.add env.env_venv v ty }

let env_mem_var env v =
   SymbolTable.mem env.env_venv v

let env_lookup_var env pos v =
   try SymbolTable.find env.env_venv v with
      Not_found ->
         raise (IrException (pos, UnboundVar v))

(*
 * Functions.
 *)
let env_mem_fun env f =
   SymbolMTable.mem env.env_fenv f

let env_add_fun env f f' ty =
   { env with env_venv = SymbolTable.add env.env_venv f' ty;
              env_fenv = SymbolMTable.add env.env_fenv f (f', ty)
   }

let env_lookup_fun env pos v =
   try SymbolMTable.find env.env_fenv v with
      Not_found ->
         raise (IrException (pos, UnboundVar v))

let env_lookup_funs env pos v =
   try SymbolMTable.find_all env.env_fenv v with
      Not_found ->
         raise (IrException (pos, UnboundVar v))

(*
 * Add the function to the return scope.
 *)
let env_add_return env v ty =
   { env with env_return = (v, ty) :: env.env_return }

let env_lookup_return env pos =
   match env.env_return with
      (v, ty) :: _ ->
         v, ty
    | [] ->
         raise (IrException (pos, StringError "not in a method"))

let env_lookup_return_type env pos f =
   try List.assoc f env.env_return with
      Not_found ->
         raise (IrException (pos, StringVarError ("return out of scope", f)))

(*
 * Add the class definition.
 *)
let env_set_current_class env v class_info =
   { env with env_class = Some (v, class_info) }

let env_get_current_class env pos =
   match env.env_class with
      Some info ->
         info
    | None ->
         raise (IrException (pos, StringError "no current class"))

(*
 * Current break label.
 *)
let env_set_break env v label =
   let label = match label with
      None ->
         break_var  (* Normal loop--use standard label *)
    | Some label ->
         label      (* Labeled stmt--use label*)
   in
      { env with env_breaks = SymbolTable.add env.env_breaks label (v, 0) }

let env_get_break env pos label =
   match label with
      None ->
         (try 
            SymbolTable.find env.env_breaks break_var 
         with
            Not_found ->
               raise (IrException (pos, StringError 
                     "break without a loop")))
    | Some label ->
         try
            SymbolTable.find env.env_breaks label
         with Not_found ->
            raise (IrException (pos, StringVarError (
                  "labeled break without matching label", label)))

(*
 * Current continue label.
 *)
let env_set_continue env v label =
   let label = match label with
      None ->
         continue_var (* Normal loop--use standard label *)
    | Some label ->
         label        (* Labeled stmt--use label*)
   in
      { env with env_continues = 
            SymbolTable.add env.env_continues label (v, 0) }

let env_get_continue env pos label =
   match label with
      None ->
         (try 
            SymbolTable.find env.env_continues continue_var
         with
            Not_found ->
               raise (IrException (pos, StringError 
                     "continue without a loop")))
    | Some label ->
         try
            SymbolTable.find env.env_continues label
         with Not_found ->
            raise (IrException (pos, StringVarError (
                  "labeled continue without matching labeled for/while/do", 
                  label)))

(* 
 * Functions for getting/setting the finally continuation
 *)
let env_add_finally env f =
   (* Increment the number of finallys that must be handled on enclosing
      breaks/continues and set the finally's label *)
   let breaks = SymbolTable.map (fun (v, x) -> (v, x+1)) 
         env.env_breaks in
   let continues = SymbolTable.map (fun (v, x) -> (v, x+1)) 
         env.env_continues in
   { env with env_finally = f::env.env_finally;
         env_breaks = breaks;
         env_continues = continues }
   
let env_get_finally_list env = env.env_finally

(* Get/Set package *)
let env_set_package env pkg = 
   (* Just need to filter out the AST pos. *)
   let pkg = match pkg with
      None ->
         None
    | Some (p,_) ->
         Some p
   in
      {env with env_package = pkg}
      
let env_get_package env = env.env_package

(*
 * Add the base types to the empty env.
 *)
let base_types =
   [Symbol.add "void", TyUnit;
    Symbol.add "boolean", TyBool;
    Symbol.add "char", TyChar;
    Symbol.add "int",  TyInt;
    Symbol.add "float", TyFloat;
    Symbol.add "String", TyString]

let env_empty =
   { env_tenv    = SymbolTable.empty;
     env_itenv   = SymbolTable.empty;
     env_base    = List.fold_left (fun base (v, ty) -> 
            SymbolTable.add base v ty) SymbolTable.empty base_types;
     env_venv    = SymbolTable.empty;
     env_fenv    = SymbolMTable.empty;
     env_return  = [];
     env_class   = None;
     env_breaks  = SymbolTable.empty;
     env_continues = SymbolTable.empty;
     env_finally = [];
     env_package = None
     
   }

(*
 * Get the environment defined by a program.
 *)
let env_of_prog prog =
   let { prog_types = types } = prog in
      { env_empty with env_tenv = types }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
