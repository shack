(*
 * Functions for resolving package/class interdependencies and imports.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Nathaniel Gray, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Nathaniel Gray
 * @email{n8gray@cs.caltech.edu}
 * @end[license]
 *)
 
open Printf
open Format
open Symbol
open Fj_ir
open Fj_ir_env
open Fj_ir_print
open Fj_ir_exn

let pkg_debug = false

(* Exception of filename, version tuple *)
exception Stale_fclass_file of string * (int * int * int)

(* 
 * The datatype that will be marshalled:
 *   class/interface name * type definitions * imports
 *
 * If you change this be sure to change fclass_vers in Fj_ir.ml
 *)
type data_ty = symbol * Fj_ast.import list * tydef SymbolTable.t

(* 
 * Load marshalled type definition from a file
 *)
let rec load_fclass_file env filename tydefs_of_prog =
   let infile = open_in_bin filename in
   try
      let file_vers : fclass_vers_ty = Marshal.from_channel infile in
      if file_vers <> fclass_vers then
         raise (Stale_fclass_file (filename, file_vers));
      let fundef : data_ty = Marshal.from_channel infile in
         close_in infile;
         let _, imports, _ = fundef in
         let env = import_classes env imports tydefs_of_prog in
            env, fundef
   with 
      exn ->
         close_in infile;
         raise exn

(* 
 * Marshal type definition to a file
 * I put type labels on fundef here too so that the load and save funcs
 * don't get out of synch.
 *)
and save_fclass_file env filename (fundef:data_ty) tydefs_of_prog =
   let outfile = open_out_bin filename in
   try
      Marshal.to_channel outfile fclass_vers [];
      Marshal.to_channel outfile fundef [];
      close_out outfile;
   with 
      exn ->
         close_out outfile;
         raise exn

(* 
 * Compile a file from filename to package data type
 *)
and data_of_file env filename class_name tydefs_of_prog =
   try
      let prog = Fj_ast_compile.compile filename in
      let imports = prog.Fj_ast.prog_imports in
      let env = import_classes env imports tydefs_of_prog in
      let tydefs = tydefs_of_prog env prog in
         if pkg_debug then
            prerr_string ("Loaded " ^ filename ^ "\n" );
         env, (class_name, imports, tydefs) (* This is data_ty *)
   with exn ->
      if pkg_debug then
         prerr_string ("*** Error while loading " ^ filename ^ "\n" );
      raise exn

(* 
 * Import a file from either a .java or .fclass file
 * The prefix should be of form:  path/to/some/file
 * where file has no extension.
 *
 * Raises Sys_error if neither .fclass nor .java file can be read
 *
 * Is there a cross-platform way to do this?
 *
 * This is dangerous right now because it assumes that any file that has
 * the correct name must correspond to the given class.  If two files in the
 * same directory both define different classes Foo then Foo.fclass from either
 * one will match during compilation of either one.  Might want to keep the
 * MD5 sum of the .o in the .fclass file if possible to prevent this sort of 
 * thing.  Problematic because the .fclass is written before the .o exists.
 *
 *)
and import_class_file env file_path class_name tydefs_of_prog =
   let str_class_name = (string_of_symbol class_name) in
   let file_prefix = file_path ^ "/" ^ str_class_name in
   try
      (* Import all of the tydefs *)
      let env, (_, imports, tydefs) =
         try
            (* First see if there's a readable .fclass file *)
            let fname = file_prefix ^ ".fclass" in
               (* Raise error if file unreadable *)
               Unix.access fname [Unix.R_OK];  
               load_fclass_file env fname tydefs_of_prog (* Returns data_ty *)
         with 
            Unix.Unix_error _ 
          | Stale_fclass_file _ ->
               (* Couldn't find/read a .fclass file.  Try a .java file *)
               (* Should write out an .fclass file here on success. *)
               let fname = file_prefix ^ ".java" in
                  data_of_file env fname class_name tydefs_of_prog
      in
      (* Get the tydef *)
      let tydef = 
         try
            SymbolTable.find tydefs class_name
         with Not_found ->
            raise (Failure ("*** Can't load class " ^ str_class_name ^ 
                            " from its own .fclass or .java file"))
      in
      (* Print some nice messages *)
      let _ = 
         if pkg_debug then begin
            fprintf err_formatter "Imported:\n";
            fprintf err_formatter "%a:%a@." 
                  pp_print_symbol class_name pp_print_tydef tydef;
         end
      in
         (* Add the tydef to the imports -- Do we need to add all tydefs?? *)
         env_add_import_tydef env class_name tydef
         
   with
      exn ->
         if pkg_debug then
            prerr_string ("*** Can't load " ^ file_prefix ^ ".{fclass,java}\n");
         raise exn
         
(* 
 * Import a class.  
 * returns env with the imported tydef added to env_types.
 *
 * What about imports from imported files??  A list of imports needs to 
 * be part of the .fclass file.  We can follow the chain of imports (discarding
 * duplicates) and compile them all to .fclass files if necessary.
 * 
 * The imports need to become the prog_import in the fir.
 *
 *)
and import_class env (pkg_list, opt_cls, pos) tydefs_of_prog =
   let fcpath = 
      try
         Sys.getenv "FCLASSPATH"
      with Not_found ->
         "."
   in
   let fcpath = Mc_string_util.split fcpath ":" in
   let pkg_list = List.map string_of_symbol pkg_list in
   let rel_path = String.concat "/" pkg_list in
   match opt_cls with
      Some class_name ->
         (* Has this class already been imported? *)
         (try
            ignore (env_lookup_tydef env (ast_pos pos) class_name);
            env
         with IrException (_, UnboundType _) ->
            (* Put a marker in to stop recursion in circular imports *)
            let env = env_add_import_tydef env class_name (TyDefClass None) in
            let rec import_from_path = function
               ""::rest ->
                  (* Skip empty path entries *)
                  import_from_path rest
             | root::rest ->
                  (try
                     (* raises Sys_error on failure *)
                     import_class_file env (root ^ rel_path) class_name 
                                       tydefs_of_prog 
                  with (Sys_error str) ->
                     import_from_path rest)
             | [] ->
                  raise Not_found
            in
               try
                  import_from_path fcpath
               with Not_found ->
                  raise (IrException (ast_pos pos, StringError (
                        "Can't import class " ^ string_of_symbol class_name)))
         )
    | None ->
         raise (IrException(ast_pos pos, StringError 
               "import * not implemented"))
         
(* 
 * It's really ugly to pass tydefs_of_prog as an argument, but it solves
 * a circular dependency. 
 *)
and import_classes env ast_imports tydefs_of_prog =
   List.fold_left (fun env ast_import -> 
            import_class env ast_import tydefs_of_prog
         ) env ast_imports

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
