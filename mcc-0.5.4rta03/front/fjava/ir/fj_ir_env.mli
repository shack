(*
 * Type and variable environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Fj_ir
open Fj_ir_exn

(*
 * General environment contains
 * a type environment, a variable environment,
 * and class information.
 *)
type env

val env_empty : env

(*
 * Types.
 *)
val env_add_tydef : env -> var -> tydef -> env
val env_add_import_tydef : env -> var -> tydef -> env
val env_lookup_tydef : env -> ir_pos -> var -> tydef
val env_lookup_type : env -> ir_pos -> var -> ty
val env_get_tydefs : env -> tydef SymbolTable.t
val env_get_imports : env -> tydef SymbolTable.t

(*
 * Variables.
 *)
val env_mem_var : env -> var -> bool
val env_add_var : env -> var -> ty -> env
val env_lookup_var : env -> ir_pos -> var -> ty

val env_mem_fun : env -> var -> bool
val env_add_fun : env -> var -> var -> ty -> env
val env_lookup_fun : env -> ir_pos -> var -> var * ty
val env_lookup_funs : env -> ir_pos -> var -> (var * ty) list

(*
 * The current class definition.
 *)
val env_set_current_class : env -> var -> class_info -> env
val env_get_current_class : env -> ir_pos -> var * class_info

(*
 * Add the return type for the current function.
 *)
val env_add_return : env -> var -> ty -> env
val env_lookup_return : env -> ir_pos -> var * ty
val env_lookup_return_type : env -> ir_pos -> var -> ty

(*
 * The current break var, or labeled break var if optional label is supplied
 *)
val env_set_break : env -> var -> var option -> env
val env_get_break : env -> ir_pos -> var option -> (var * int)

(*
 * The current continue var, or labeled cont. var if optional label is supplied
 *)
val env_set_continue : env -> var -> var option -> env
val env_get_continue : env -> ir_pos -> var option -> (var * int)

(* Get/set finally blocks *)
val env_add_finally : env -> var -> env
val env_get_finally_list : env -> var list

(* Get/set package *)
val env_set_package : env -> Fj_ast.package option -> env
val env_get_package : env -> Fj_ir.package option

(*
 * Environment defined by a program.
 *)
val env_of_prog : prog -> env

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
