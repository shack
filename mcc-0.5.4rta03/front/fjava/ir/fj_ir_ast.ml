(*
 * Translate the AST to IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Symbol
open Field_table

open Fj_ir
open Fj_ir_env
open Fj_ir_exn
open Fj_ir_exp
open Fj_ir_type
open Fj_ir_check
open Fj_ir_state
open Fj_ir_standardize
open Fj_ir_print

(*
 * Wrappers to identify this file.
 *)
let ast_pos pos = string_pos "Fj_ir_ast" (ast_pos pos)

(************************************************************************
 * OTHER TYPES
 ************************************************************************)

(*
 * This is a collection of all the fields in a class definition.
 *)
type field_info =
   { field_vars : (var * var * ty * Fj_ast.expr option * Fj_ast.pos) list;
     field_methods : 
            (var * var * ty * var list * Fj_ast.expr * Fj_ast.pos) list;
     field_consts  : (var * ty * var list * Fj_ast.expr * Fj_ast.pos) list;
     field_interfaces : (var * var * var list) list
   }

let field_empty =
   { field_vars = [];
     field_methods = [];
     field_consts = [];
     field_interfaces = []
   }

(*
 * This is the info we collect from the program.
 *)
type prog_info =
   { info_types    : tydef SymbolTable.t;
     info_funs     : (fun_class * ty * var list * exp) SymbolTable.t;
   }

(*
 * Adding to the program info.
 *)
let info_add_tydef info v ty =
   { info with info_types = SymbolTable.add info.info_types v ty }

let info_add_fun info f entry =
   { info with info_funs = SymbolTable.add info.info_funs f entry }

(************************************************************************
 * INITIAL ENVIRONMENT
 ************************************************************************)

(*
 * Add the FjObject type to the env.
 *)
let info_empty, env_empty =
   let info =
      { info_types    = SymbolTable.empty;
        info_funs     = SymbolTable.empty
      }
   in
   let v_this = new_symbol_string "object_this" in
   let ty_this = TyVar v_this in
   let ty_object = TyObject object_var in
   let methods = FieldMTable.create () in
   let fields = FieldMTable.create () in
   let consts = FieldMTable.create () in

   (* Define parseInt *)
   let ty_parse_int = TyMethod (ty_object, [TyString], TyInt) in
   let parse_int_var = Symbol.add "parseInt" in
   let parse_int_f = new_symbol parse_int_var in
   let v_string = new_symbol_string "string" in
   let v_int = new_symbol_string "int" in
   let parse_int_body =
      LetExt (v_int, TyInt, "atoi", TyFun ([TyString], TyInt), 
              [AtomVar v_string],
               Return (parse_int_f, AtomVar v_int))
   in
   let info = info_add_fun info parse_int_f 
         (FunMethodClass, ty_parse_int, [this_var; v_string], parse_int_body) 
   in
   let ty_parse_int = TyMethod (ty_this, [TyString], TyInt) in
   let methods = FieldMTable.add methods parse_int_var parse_int_f 
         ty_parse_int in

   (* Define println *)
   let ty_println = TyMethod (ty_object, [TyString], TyUnit) in
   let println_var = Symbol.add "println" in
   let println_f = new_symbol println_var in
   let v_string = new_symbol_string "string" in
   let v_unit = new_symbol_string "_" in
   let println_body =
      LetExt (v_unit, TyUnit, "println", TyFun ([TyString], TyUnit), 
                  [AtomVar v_string],
              Return (println_f, AtomUnit))
   in
   let info = info_add_fun info println_f 
         (FunMethodClass, ty_println, [this_var; v_string], println_body) in
   let ty_println = TyMethod (ty_this, [TyString], TyUnit) in
   let methods = FieldMTable.add methods println_var println_f ty_println in


   (* Define main *)
   let ty_main = TyMethod (ty_object, [TyArray TyString], TyUnit) in
   let main_var = Symbol.add "main" in
   let main_f = new_symbol main_var in
   let v_argv = new_symbol_string "argv" in
   let v_unit = new_symbol_string "_" in
   let main_body = Return (main_f, AtomUnit) in
   let info = info_add_fun info main_f
         (FunMethodClass, ty_main, [this_var; v_argv], main_body) in
   let ty_main = TyMethod (ty_this, [TyArray TyString], TyUnit) in
   let methods = FieldMTable.add methods main_var main_f ty_main in

   
   (* Define FjObject *)
   let ty_object_unit = TyFun ([ty_object], TyUnit) in
   let object_info =
      { class_name = object_var;
        class_this = v_this;
        class_parents = [];
        class_interfaces = SymbolTable.empty;
        class_consts = FieldMTable.add 
               consts object_init_var object_init_var ty_object_unit;
        class_methods = methods;
        class_fields = fields;
        class_package = None
      }
   in
   let object_def = TyDefClass (Some object_info) in

   (* Add to environment *)
   let env = env_add_tydef env_empty object_var object_def in

   (* FjObject constructor does nothing *)
   let object_body = Return (object_init_var, AtomUnit) in
   let info = info_add_fun info object_init_var 
         (FunGlobalClass, ty_object_unit, [this_var], object_body) in

   (* Add to info *)
   let info = info_add_tydef info object_var object_def in
      info, env

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Collect the parts of an interface.
 * The methods should all have function types.
 * We add the "this" parameter to each of the methods.
 *)
let build_interface_type info env name extends methods pos =
   (* Make up a name for the "this" parameter *)
   let this_sym = new_symbol_string "this" in
   let ty_this = TyObject this_sym in

   (* Extends isn't implemented yet *)
   if List.length extends > 0 then
      raise (IrException (ast_pos pos, 
               (StringError "Interface Inheritance is not yet implemented")))
   else

   (* Collect all the methods *)
   let table =
      List.fold_left (fun table (v_ext, ty, pos) ->
               let pos = string_pos "build_interface_type" (ast_pos pos) in
               let ty = build_type env pos ty in
               let ty_args, ty_res = dest_fun_type env pos ty in
               let ty = TyMethod (ty_this, ty_args, ty_res) in
               let v_int = new_symbol v_ext in
                  FieldMTable.add table v_ext v_int ty) 
            (FieldMTable.create ()) methods
   in

   (* Collect all the info *)
   let intf =
      { intf_name = name;
        intf_this = this_sym;
        intf_methods = table;
        intf_package = env_get_package env
      }
   in
   let tydef = TyDefInterface (Some intf) in
   let info = info_add_tydef info name tydef in
   let env = env_add_tydef env name tydef in
      info, env

(************************************************************************
 * CLASSES, PHASE 1
 ************************************************************************)

(*
 * Rename the this type in the method.
 *)
let rename_method_type env pos ty_this ty =
   let pos = string_pos "rename_method_type" pos in
   let _, ty_vars, ty_res = dest_method_type env pos ty in
      TyMethod (ty_this, ty_vars, ty_res)

(*
 * Rename all the method types in the fieldtable.
 *)
let rename_method_types env ty_this methods =
   FieldMTable.map (fun v_ext v_int ty ->
         let pos = vexp_pos v_int in
         let _, ty_vars, ty_res = dest_method_type env pos ty in
            v_int, TyMethod (ty_this, ty_vars, ty_res)) methods

(*
 * Override a previous definition of a method.
 *)
let merge_method env methods (f_ext, f_int, ty, pos) =
   let pos = string_pos "merge_method" (ast_pos pos) in

   (* Search for a previous definition *)
   let replace _ ty' =
      if equal_types env pos ty ty' then
         f_int, ty
      else
         (* This is not the right definition *)
         raise Not_found
   in
   (* Either replace a previous definition, or add a new one *)
   try FieldMTable.replace_ext methods f_ext replace with
      Not_found ->
         FieldMTable.add methods f_ext f_int ty

let merge_methods env v_this methods1 methods2 =
   let ty_this = TyVar v_this in
   let methods1 = rename_method_types env ty_this methods1 in
   let methods2 =
      List.map (fun (v_ext, v_int, ty, pos) ->
            let pos' = string_pos "merge_methods" (ast_pos pos) in
            let ty = rename_method_type env pos' ty_this ty in
               v_ext, v_int, ty, pos) methods2
   in
      List.fold_left (merge_method env) methods1 methods2

(*
 * The field merge just adds all the fields.
 *)
let merge_field fields (v, v', ty, _, _) =
   FieldMTable.add fields v v' ty

let merge_fields fields1 fields2 =
   List.fold_left merge_field fields1 fields2

(*
 * A variable definition.
 * We just convert the type,
 * we'll save the initializer conversion for later.
 *)
let build_var_def env fields v ty e_opt pos =
   let loc = string_pos "build_var_def" (ast_pos pos) in
   let ty = build_type env loc ty in
   let v' = new_symbol v in
      { fields with field_vars = (v, v', ty, e_opt, pos) :: fields.field_vars }

(*
 * Add a variable definition to the class fields.
 *)
let rec build_var_defs env fields defs =
   match defs with
      (v, ty, e_opt, pos) :: defs ->
         let fields = build_var_def env fields v ty e_opt pos in
            build_var_defs env fields defs
    | [] ->
         fields

(*
 * Add a method definition to the class fields.
 * Build the IR function type.  The method gets
 * an extra "this" parameter.
 *
 * discard type modifiers for now
 *)
let build_method_def env name fields (_, f, args, ty_res, body, pos) =
   (* Collect all the formal params *)
   let loc = string_pos "build_method_def" (ast_pos pos) in
   let vars, ty_vars =
      List.fold_left (fun (vars, ty_vars) (v, ty, _) ->
            let vars = v :: vars in
            let ty_vars = build_type env loc ty :: ty_vars in
               vars, ty_vars) ([], []) args
   in
   let vars = this_var :: List.rev vars in
   let ty_vars = List.rev ty_vars in

   (* Add the field with the new function type *)
   let f' = new_symbol f in
   let ty_fun = TyMethod (TyObject name, ty_vars, build_type env loc ty_res) in
      { fields with field_methods = 
            (f, f', ty_fun, vars, body, pos) :: fields.field_methods }

let build_method_defs env fields name funs =
   List.fold_left (build_method_def env name) fields funs

(*
 * Add a constructor.
 * It is like a function, but we don't add
 * the "this" parameter.
 *)
let build_const_def env fields name f args body pos =
   (* Collect all the formal params *)
   let loc = string_pos "build_const_def" (ast_pos pos) in
   let vars, ty_vars =
      List.fold_left (fun (vars, ty_vars) (v, ty, _) ->
            let vars = v :: vars in
            let ty_vars = build_type env loc ty :: ty_vars in
               vars, ty_vars) ([], []) args
   in
   let ty_fun = TyFun (TyObject name :: List.rev ty_vars, TyUnit) in
   let f' = new_symbol_string ((string_of_symbol name)^".init") in
      { fields with field_consts = 
            (f', ty_fun, this_var :: List.rev vars, body, pos) :: 
            fields.field_consts }

(*
 * Add a default constructor if there are no constructors.
 *)
let add_default_const fields name pos =
   let loc = string_pos "add_default_const" (ast_pos pos) in
      if fields.field_consts = [] then
         let f = new_symbol_string ((string_of_symbol name)^".init") in
         let ty_fun = TyFun ([TyObject name], TyUnit) in
         let body = Fj_ast.SeqExpr ([], pos) in
            { fields with field_consts = [f, ty_fun, [this_var], body, pos] }
      else
         fields

(*
 * Find the method that implements an interface.
 * Search for new definitions first.
 *)
let find_interface_method env pos methods (name, v_int, ty) =
   let pos = string_pos "find_interface_type" pos in

   (* Search for a definition *)
   let ty_this = TyObject name in
   let ty_this' = TyObject name in
   let rec search = function
      (f', ty') :: methods ->
         (* This is a potential match *)
         if equal_method_types env pos ty_this ty ty_this' ty' then
            v_int, f', ty
         else
            search methods
    | [] ->
         raise Not_found
   in
      try search (FieldMTable.find_ext methods name) with
         Not_found ->
            raise (IrException (pos, 
                  StringVarError ("interface method is not implemented", name)))

(*
 * Add an interface with a list of all the internal names and types
 * of the interface methods.  Ignore the interface if it has already
 * been defined.
 *)
let build_interface env pos cname methods1 interfaces name =
   let pos = string_pos "build_interface" pos in
      (* Ignore a duplicate definition *)
      if SymbolTable.mem interfaces name then
         interfaces
      else
         (* Find the internal method names that implement the interface *)
         let info = dest_interface_type env pos name in
         let methods2 = sort_fields pos info.intf_methods in
         let funs = List.map (find_interface_method env pos methods1) methods2 in
            SymbolTable.add interfaces name funs

let build_interfaces env pos cname methods implements =
   let pos = string_pos "build_interfaces" pos in
      List.fold_left (build_interface env pos cname methods) 
            SymbolTable.empty implements

(*
 * Build the class info.
 *)
let build_class_type env name extends implements defs pos =
   let loc = string_pos "build_class_type" (ast_pos pos) in
   let v_this = new_symbol name in

   (* Build all the types in the defs, discarding type modifiers for now *)
   let fields =
      List.fold_right (fun def fields ->
            match def with
               Fj_ast.VarDefs (_, defs, _) ->
                  build_var_defs env fields defs
             | Fj_ast.FunDefs (funs, _) ->
                  build_method_defs env fields name funs
             | Fj_ast.ConstDef (_, f, args, body, pos) ->
                  build_const_def env fields name f args body pos
             | Fj_ast.ClassDef (_, _, _, _, _, pos)
             | Fj_ast.TypeDef (_, _, _, pos) ->
                  raise (IrException (ast_pos pos, StringError 
                        "nested class definitions not implemented"))) (**)
         defs field_empty
   in

   (* Look up the parent *)
   let parent = dest_class_type env loc extends in

   (* Add a default constructor if necessary *)
   let fields = add_default_const fields name pos in

   (* Collect class vars and fields *)
   let class_methods = List.map (fun (f, f', ty, _, _, pos) -> 
         f, f', ty, pos) fields.field_methods in
   let class_methods = merge_methods env v_this parent.class_methods 
         class_methods in
   let class_fields = merge_fields parent.class_fields fields.field_vars in
   
   (* Constructors aren't inherited, so make a fresh FieldMTable *)
   let class_consts = FieldMTable.create () in
   (* XXX: External names of constructors are numbered to make them 
      unique, but predictable.  This is a hack until the linker can
      resolve symbol collisions using types. *)
   let counter = ref 0 in
   let class_consts = List.fold_left (fun st (f, ty, _, _, _) -> 
            let f_ext = Symbol.add 
                  ((string_of_symbol name) ^ ".init" ^ (string_of_int !counter)) in
            incr counter;
               FieldMTable.add st f_ext f ty)  
                     class_consts fields.field_consts
   in
   let class_interfaces = build_interfaces env loc name class_methods 
         implements in

   (* Collect all the class info *)
   let class_info =
      { class_name = name;
        class_this = v_this;
        class_parents = extends :: parent.class_parents;
        class_interfaces = class_interfaces;
        class_consts = class_consts;
        class_methods = class_methods;
        class_fields = class_fields;
        class_package = env_get_package env
      }
   in
      class_info, fields

(************************************************************************
 * CLASSES, PHASE 2
 ************************************************************************)

(*
 * Build the IR for an instance variable declaration.
 * The main result is a collection of initializers for the vars.
 *)
let rec build_field_init env init_var defs =
   match defs with
      (v, v', ty, e_opt, pos) :: defs ->
         let pos = string_pos "build_vars" (ast_pos pos) in
            (match e_opt with
                Some e ->
                   build_exp env e (fun env a ->
                         coerce_type true env pos a ty (fun env a ->
                               SetProject (this_atom, v', ty, a,
                               build_field_init env init_var defs)))
              | None ->
                   build_field_init env init_var defs)
    | [] ->
         (* Initialization works by side-effect *)
         Return (init_var, AtomUnit)

(*
 * The build_vars wrapper function defines the "alloc" function for
 * the class.  We need: the program info; the class info for
 * the current class; the name of the VMA record for the class;
 * and the fields of the class.
 *)
let build_alloc info env name init_var class_info fields pos =
   let pos = string_pos "build_alloc" (ast_pos pos) in

   (* Build the initializer expression *)
   let e = build_field_init env init_var fields.field_vars in
      (* Define the allocator as a function *)
      info_add_fun info init_var 
            (FunGlobalClass, TyFun ([TyObject name], TyUnit), [this_var], e)

(*
 * Build the IR for a method.  Add the function to the program info.
 * We need: the program info; the class info for the current class;
 * the function info.  We define one element of the VMA record for
 * the current class.
 *)
let build_method env info (f, f', ty, vars, body, pos) =
   let pos = string_pos "build_method" (ast_pos pos) in
   let ty_this, ty_vars, ty_res = dest_method_type env pos ty in

   (* Add the return label with the new function name *)
   let env = env_add_return env f' ty_res in

   (* Add all the vars to the venv *)
   let env = List.fold_left2 env_add_var env vars (ty_this :: ty_vars) in

   (* Build the body *)
   let e =
      build_exp env body (fun env a ->
            if is_unit_type env pos ty_res then
               Return (f', AtomUnit)
            else
               let v = new_symbol f in
                  LetObject (v, ty_object, object_var, Raise (AtomVar v)))
   in

   (* Add the function to the program *)
   let info = info_add_fun info f' (FunMethodClass, ty, vars, e) in
      info

(*
 * Add the code for all of the methods.
 *)
let build_methods info env fields =
   List.fold_left (build_method env) info fields.field_methods

(*
 * Build a constructor.
 * The constructor first calls the initialier,
 * then it calls any other constructors, then
 * it performs the rest of the construction.
 *)
let build_const env cname pname init_var info (f', ty, vars, body, pos) =
   let pos = string_pos "build_const" (ast_pos pos) in
   let ty_obj = TyObject cname in
   let ty_vars, _ = dest_fun_type env pos ty in

   (* Examine the body, and extract a constructor call *)
   let body, id, args, init_flag =
      match body with
         Fj_ast.SeqExpr (Fj_ast.ApplyExpr 
               (Fj_ast.VarExpr (id, _), args, _) :: rest, pos)
         when Symbol.eq id this_var || Symbol.eq id super_var ->
            let body = Fj_ast.SeqExpr (rest, pos) in
               body, id, args, false
       | _ ->
            body, super_var, [], true
   in

   (* Build the body and call the parent constructor *)
   let body =
      let name =
         if Symbol.eq id this_var then
            cname
         else
            pname
      in
      let consts = (dest_class_type env pos name).class_consts in
      let env = List.fold_left2 env_add_var env vars ty_vars in
      let env = env_add_var env this_var ty_obj in
         build_exp_list env args (fun env args ->
         apply_const env pos name (this_atom :: args) consts (fun env _ ->
         build_exp env body (fun env _ -> Return (f', AtomUnit))))
   in

   (* Call the initializer if necessary *)
   let body =
      if init_flag then
         let v = new_symbol_string "init" in
            LetApply (v, TyUnit, init_var, [AtomVar this_var], body)
      else
         body
   in

   (* Add the function to the info *)
   let info = info_add_fun info f' (FunGlobalClass, ty, vars, body) in
      info

let build_consts info env cname pname init_var fields =
   List.fold_left (build_const env cname pname init_var) 
         info fields.field_consts

(*
 * Add the method and VMA for each interface.
 *)
let build_interface env cname info (iname, method_sym, funs) =
   (* Body of the coercion method *)
   let e =
      let v = new_symbol iname in
      let ty_obj = TyVObject iname in
         LetVObject (v, ty_obj, iname, cname, this_atom, 
               Return (method_sym, AtomVar v))
   in

   (* Add the method *)
   let ty_method = TyFun ([TyObject cname], TyVObject iname) in
      info_add_fun info method_sym (FunMethodClass, ty_method, [this_var], e)

let build_interfaces info env cname fields =
   List.fold_left (build_interface env cname) info fields.field_interfaces

(*
 * Build a class definition.
 *)
let build_class_def info env name extends implements defs bodies pos =
   (* Compile the type and get the fields *)
   let class_info, fields = build_class_type env name extends implements 
         defs pos in

   (* Add the class and its type to the env *)
   let ty_class = TyDefClass (Some class_info) in
   let info = info_add_tydef info name ty_class in
   let env = env_add_tydef env name ty_class in
   let info = 
      if bodies then
         let env' = env_set_current_class env name class_info in

         (* Object allocation function *)
         let name_str = Symbol.to_string name in
         let init_var = new_symbol_string (name_str ^ ".alloc") in
         let info = build_alloc info env' name init_var class_info fields pos in

         (* Build all the methods *)
         let info = build_methods info env' fields in

         (* Constructors *)
         let info = build_consts info env' name extends init_var fields in

         (* Interfaces *)
         let info = build_interfaces info env' name fields in
            info
      else
         info
   in
      info, env

(*
 * Build the file initialization function.
 *)
let build_init info env filename =
   let pos = filename, 0, 0, 0, 0 in
   let filename = Symbol.add filename in
   let main_sym = Symbol.add "main" in
   let v_argv = new_symbol_string "argv" in
   let body =
      Fj_ast.ApplyExpr 
            (Fj_ast.ProjectExpr 
                  (Fj_ast.NewConstExpr (filename, [], pos), main_sym, pos),
                   [Fj_ast.VarExpr (v_argv, pos)],
                   pos)
   in
   let env = env_add_var env v_argv (TyArray TyString) in
   let body =
      build_exp env body (fun env _ ->
            Return (main_sym, AtomUnit))
   in
   let info = info_add_fun info main_sym (FunGlobalClass, 
               TyFun ([TyArray TyString], TyUnit), [v_argv], body) in
      info, main_sym

(************************************************************************
 * MAIN
 ************************************************************************)

(*
 * Add dummy definitions to the env.
 *)
let build_env env def =
   match def with
      Fj_ast.TypeDef (_, v, Fj_ast.TypeInterface _, _) ->
         env_add_tydef env v (TyDefInterface None)
    | Fj_ast.TypeDef (_, _, _, pos) ->
         raise (IrException (ast_pos pos, 
               StringError "illegal type definition"))
    | Fj_ast.ClassDef (_, name, _, _, _, _) ->
         env_add_tydef env name (TyDefClass None)
    | Fj_ast.VarDefs (_, _, pos) ->
         raise (IrException (ast_pos pos, 
               StringError "variable definition not allowed at top level"))
    | Fj_ast.FunDefs (_, pos)
    | Fj_ast.ConstDef (_, _, _, _, pos) ->
         raise (IrException (ast_pos pos, 
               StringError "function definition not allowed at top level"))

let build_env env defs =
   List.fold_left build_env env defs

(*
 * Compile a definition.
 * (for now we drop all type modifiers applied to top level definitions)
 * If bodies is false then we're only interested in the tydefs and don't
 * need to build the method bodies.
 *)
let build_def (info, env, bodies) def =
   match def with
      Fj_ast.TypeDef (_, v, Fj_ast.TypeInterface (_, extends, methods, _), 
            pos) ->
         let info, env = build_interface_type info env v extends methods pos in
            info, env, bodies
    | Fj_ast.TypeDef (_, _, _, pos) ->
         raise (IrException (ast_pos pos, 
               StringError "illegal type definition"))
    | Fj_ast.ClassDef (_, name, extends, implements, defs, pos) ->
         let info, env = build_class_def info env name extends implements 
                                          defs bodies pos in
            info, env, bodies
    | Fj_ast.VarDefs (_, _, pos) ->
         raise (IrException (ast_pos pos, 
               StringError "variable definition not allowed at top level"))
    | Fj_ast.FunDefs (_, pos)
    | Fj_ast.ConstDef (_, _, _, _, pos) ->
         raise (IrException (ast_pos pos, 
               StringError "function definition not allowed at top level"))

let build_defs info env defs =
   let info, env, _ = List.fold_left build_def (info, env, true) defs in
      info, env

let build_tydefs info env defs =
   let _, env, _ = List.fold_left build_def (info, env, false) defs in
      env_get_tydefs env

(* Convert an AST definition list to the tydefs that it defines. *)
let tydefs_of_prog env prog =
   let env = env_set_package env prog.Fj_ast.prog_package in
   let ast_defs = prog.Fj_ast.prog_defs in
   (* build_env adds tydef stubs to the environment *)
   let env = build_env env ast_defs in
      build_tydefs info_empty env ast_defs

(*
 * Convert the AST definition list to an IR program.
 *)
let build_prog prog filename =
   let env = env_set_package env_empty prog.Fj_ast.prog_package in
   let ast_defs = prog.Fj_ast.prog_defs in
   (* build_env adds tydef stubs to the environment *)
   let env = build_env env ast_defs in
   let env = Fj_ir_package.import_classes env prog.Fj_ast.prog_imports 
         tydefs_of_prog in
   (* build_defs adds the definitions to the tydefs *)
   let info, env = build_defs info_empty env ast_defs in
   let info, main = build_init info env filename in
   let { info_types = types;
         info_funs = funs
       } = info
   in
   let prog =
      { prog_types = types;
        prog_funs = funs;
        prog_main = main;
        prog_object = object_var;
        prog_imports = env_get_imports env
      }
   in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
