(*
 * These are some useful utilities on IR types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Fj_ir
open Fj_ir_env
open Fj_ir_exn

(*
 * Expand the outermost TyId so we get a real type.
 *)
let rec expand_tydef env pos name =
   env_lookup_tydef env pos name

(*
 * Expand the outermost identifiers in a type.
 * Currently, there is no need for type expansion.
 * However, slightly more expressive languages need this.
 *)
let rec expand_type env pos ty =
   ty

(*
 * Default value in a type.
 *)
let default_atom env pos ty =
   match expand_type env pos ty with
      TyUnit ->
         AtomUnit
    | TyBool ->
         AtomBool false
    | TyChar ->
         AtomChar '\000'
    | TyInt  ->
         AtomInt  0
    | TyFloat ->
         AtomFloat 0.0

    | TyNil
    | TyString
    | TyArray _
    | TyFun _
    | TyCont _
    | TyMethod _
    | TyObject _
    | TyVObject _
    | TyVar _ ->
         AtomNil

(*
 * Type predicates.
 *)
let is_unit_type tenv pos ty =
   match expand_type tenv pos ty with
      TyUnit ->
         true
    | _ ->
         false

let is_fun_type tenv pos ty =
   match expand_type tenv pos ty with
      TyFun _
    | TyCont _ ->
         true
    | _ ->
         false

let is_method_type tenv pos ty =
   match expand_type tenv pos ty with
      TyMethod _ ->
         true
    | _ ->
         false

(*
 * Get the parts of a function type.
 *)
let dest_fun_type tenv pos ty =
   let pos = string_pos "dest_fun_type" pos in
      match expand_type tenv pos ty with
         TyFun (ty_args, ty_res) ->
            ty_args, ty_res
       | TyCont ty_args ->
            ty_args, TyUnit
       | ty ->
            raise (IrException (pos, StringTypeError ("not a function type", ty)))

(*
 * Get the parts of a method type.
 *)
let dest_method_type tenv pos ty =
   let pos = string_pos "dest_method_type" pos in
      match expand_type tenv pos ty with
         TyMethod (ty_this, ty_args, ty_res) ->
            ty_this, ty_args, ty_res
       | ty ->
            raise (IrException (pos, StringTypeError ("not a method type", ty)))

(*
 * Get the array type.
 *)
let dest_array_type env pos ty =
   let pos = string_pos "dest_array_type" pos in
      match expand_type env pos ty with
         TyArray ty ->
            ty
       | ty ->
            raise (IrException (pos, StringTypeError ("not an array type", ty)))

(************************************************************************
 * TYPE MODIFIERS
 ************************************************************************)

let mod_is_public = function
   { mod_access = AccPublic; } -> true
 | _ -> false

let mod_is_protected = function
   { mod_access = AccProtected; } -> true
 | _ -> false

let mod_is_private = function
   { mod_access = AccPrivate; } -> true
 | _ -> false

let mod_is_default = function
   { mod_access = AccDefault; } -> true
 | _ -> false

let merge_ast_ty_mod ty_mod_out ty_mod = 
   match ty_mod with
      Fj_ast.ModPublic pos
    | Fj_ast.ModProtected pos
    | Fj_ast.ModPrivate pos ->
         (match ty_mod_out.mod_access with
            AccDefault ->
               (match ty_mod with
                  Fj_ast.ModPublic pos ->
                     {ty_mod_out with mod_access = AccPublic}
                | Fj_ast.ModProtected pos ->
                     {ty_mod_out with mod_access = AccProtected}
                | Fj_ast.ModPrivate pos ->
                     {ty_mod_out with mod_access = AccPrivate}
                | _ ->
                     failwith "build_ty_mod: should never get here"
               )
          | _ ->
               raise (IrException (ast_pos pos, StringError 
                     "Cannot use more than one of public, protected, private"))
         )
    | Fj_ast.ModAbstract pos ->
         {ty_mod_out with mod_abstract = true}
    | Fj_ast.ModStatic pos ->
         {ty_mod_out with mod_static = true}
    | Fj_ast.ModFinal pos ->
         {ty_mod_out with mod_final = true}
    (* Just drop the other modifiers *)
    | _ ->  ty_mod_out

(* 
 * Convert an AST type modifier list to an IR type modifier struct
 *)
let build_ty_mods ty_mods =
   let mods = List.fold_left merge_ast_ty_mod ty_mod_empty ty_mods in
   if mods.mod_abstract && (mods.mod_static or mods.mod_final or 
         (mod_is_private mods)) then
      raise (IrException (ast_pos (Fj_ast_util.pos_of_mod (List.hd ty_mods)),
            StringError "Illegal combination of modifiers"))
   else
      mods

(************************************************************************
 * CLASSES AND INTERFACES
 ************************************************************************)

(*
 * Check if the name is a class or interface.
 *)
let is_class_type env pos name =
   match expand_tydef env pos name with
      TyDefClass _ ->
         true
    | TyDefInterface _ ->
         false

let is_interface_type env pos name =
   match expand_tydef env pos name with
      TyDefInterface _ ->
         true
    | TyDefClass _ ->
         false

let is_class_or_interface_type env pos name =
   match expand_tydef env pos name with
      TyDefInterface _
    | TyDefClass _ ->
         true

let is_object_type env pos ty =
   match expand_type env pos ty with
      TyObject _ ->
         true
    | _ ->
         false

let is_vobject_type env pos ty =
   match expand_type env pos ty with
      TyVObject _ ->
         true
    | _ ->
         false

let is_any_object_type env pos ty =
   match expand_type env pos ty with
      TyObject _
    | TyVObject _ ->
         true
    | _ ->
         false

(*
 * Get the parts of a class type.
 *)
let dest_interface_type env pos name =
   let pos = string_pos "dest_interface_type" pos in
      match expand_tydef env pos name with
         TyDefInterface (Some info) ->
            info
       | TyDefInterface None ->
            raise (IrException (pos, StringVarError ("interface is abstract", name)))
       | TyDefClass _ ->
            raise (IrException (pos, StringVarError ("not an interface type", name)))

let dest_class_type env pos name =
   let pos = string_pos "dest_class_type" pos in
      match expand_tydef env pos name with
         TyDefClass (Some info) ->
            info
       | TyDefClass None ->
            raise (IrException (pos, StringVarError ("class is abstract", name)))
       | TyDefInterface _ ->
            raise (IrException (pos, StringVarError ("not a class type", name)))

let dest_object_type tenv pos ty =
   let pos = string_pos "dest_object_type" pos in
      match expand_type tenv pos ty with
         TyObject cname ->
            cname
       | ty ->
            raise (IrException (pos, StringTypeError ("not an object type", ty)))

let dest_vobject_type tenv pos ty =
   let pos = string_pos "dest_vobject_type" pos in
      match expand_type tenv pos ty with
         TyVObject iname ->
            iname
       | ty ->
            raise (IrException (pos, StringTypeError ("not an vobject type", ty)))

let dest_any_object_type tenv pos ty =
   let pos = string_pos "dest_any_object_type" pos in
      match expand_type tenv pos ty with
         TyObject name
       | TyVObject name ->
            name
       | ty ->
            raise (IrException (pos, StringTypeError ("not an object type", ty)))

(*
 * Check if a class implements an interface.
 *)
let class_implements_interface env pos cname iname =
   SymbolTable.mem (dest_class_type env pos cname).class_interfaces iname

(*
 * Check if the first class is a parent of the second.
 *)
let is_parent_class env pos cname1 cname2 =
   if Symbol.eq cname1 cname2 then
      true
   else
      List.mem cname1 (dest_class_type env pos cname2).class_parents

(*
 * Get the sorted fields.
 * This is a wrapper to translate Failure to an IrException.
 *)
let sort_fields pos fields =
   try FieldMTable.to_list fields with
      Failure s ->
         raise (IrException (pos, StringError s))

(*
 * Get the type of a field in an interface or a class.
 * Return a flag indicating if the field is mutable.
 *)
let type_of_interface_field env pos info label =
   let pos = string_pos "type_of_interface_field" pos in
      try
         let _, ty = FieldMTable.find_int info.intf_methods label in
            false, ty
      with
         Not_found ->
            raise (IrException (pos, UnboundLabel label))
       | Failure s ->
            raise (IrException (pos, StringError s))

let type_of_class_field env pos info label =
   let pos = string_pos "type_of_class_field" pos in
      try
         let _, ty = FieldMTable.find_int info.class_methods label in
            false, ty
      with
         Failure s ->
            raise (IrException (pos, StringError s))
       | Not_found ->
            try
               let _, ty = FieldMTable.find_int info.class_fields label in
                  true, ty
            with
               Failure s ->
                  raise (IrException (pos, StringError s))
             | Not_found ->
                  raise (IrException (pos, UnboundLabel label))

let type_of_field env pos ty label =
   let pos = string_pos "type_of_field" pos in
      match expand_type env pos ty with
         TyVObject iname ->
            let info = dest_interface_type env pos iname in
               type_of_interface_field env pos info label
       | TyObject cname ->
            let info = dest_class_type env pos cname in
               type_of_class_field env pos info label
       | ty ->
            raise (IrException (pos, StringTypeError ("not a class or interface type", ty)))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
