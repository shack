(*
 * Some useful utility functions on types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Field_table

open Fj_ir
open Fj_ir_exn
open Fj_ir_env

(*
 * Expand the outermost type identifier.
 *)
val expand_tydef : env -> ir_pos -> var -> tydef
val expand_type : env -> ir_pos -> ty -> ty

(*
 * Type predicates.
 *)
val is_unit_type : env -> ir_pos -> ty -> bool
val is_fun_type : env -> ir_pos -> ty -> bool
val is_method_type : env -> ir_pos -> ty -> bool
val is_object_type : env -> ir_pos -> ty -> bool
val is_vobject_type : env -> ir_pos -> ty -> bool
val is_any_object_type : env -> ir_pos -> ty -> bool

val is_class_type : env -> ir_pos -> var -> bool
val is_interface_type : env -> ir_pos -> var -> bool
val is_class_or_interface_type : env -> ir_pos -> var -> bool

(*
 * Type destructors.
 *)
val dest_fun_type : env -> ir_pos -> ty -> ty list * ty
val dest_method_type : env -> ir_pos -> ty -> ty * ty list * ty
val dest_array_type : env -> ir_pos -> ty -> ty
val dest_object_type : env -> ir_pos -> ty -> ty_var
val dest_vobject_type : env -> ir_pos -> ty -> ty_var
val dest_any_object_type : env -> ir_pos -> ty -> ty_var

val dest_interface_type : env -> ir_pos -> var -> interface_info
val dest_class_type : env -> ir_pos -> var -> class_info

(*
 * Get the type of a class or interface field.
 * The return flag indicates if the field is mutable.
 *)
val type_of_field : env -> ir_pos -> ty -> var -> bool * ty

(*
 * Sort the fields in a field_info.
 *)
val sort_fields : ir_pos -> ty FieldMTable.t -> (var * var * ty) list

(*
 * Is the first class a parent of the second?
 * is_parent_class env pos parent_name class_name
 *)
val is_parent_class : env -> ir_pos -> ty_var -> ty_var -> bool

(*
 * Check if a class implements the specified interface.
 * class_implements_interface env pos class_name interface_name
 *)
val class_implements_interface : env -> ir_pos -> ty_var -> ty_var -> bool

(*
 * Get the default atom for a type.
 *)
val default_atom : env -> ir_pos -> ty -> atom

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
