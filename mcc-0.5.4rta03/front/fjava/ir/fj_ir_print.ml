(*
 * Utilities for the IR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Symbol
open Field_table

open Fj_ir

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Default tabstop.
 *)
let tabstop = 3

(*
 * Separated list of fields.
 *)
let pp_print_sep_list sep printer buf l =
   pp_open_hvbox buf 0;
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "%s@ " sep;
                 fprintf buf "@[<hv 3>%a@]" printer x;
                 false) true l);
   pp_close_box buf ()

(*
 * Separated list of fields.
 *)
let pp_print_pre_sep_list sep printer buf l =
   ignore (List.fold_left (fun first x ->
                 if not first then
                    fprintf buf "@ %s " sep;
                 printer buf x;
                 false) true l)

(*
 * Prefixed list of items.
 *)
let pp_print_prefix_list sep printer buf l =
   pp_open_hvbox buf 0;
   List.iter (fun x ->
         fprintf buf "%s@ @[<hv 0>%a@]" sep printer x) l;
   pp_close_box buf ()

(* 
 * Print the list of type modifiers
 *)
let pp_print_ty_mods buf mods =
   let mlist = [] in
   let mlist = (match mods.mod_access with
         AccPublic -> "public" :: mlist
       | AccProtected -> "protected" :: mlist
       | AccPrivate -> "private" :: mlist
       | AccDefault -> mlist) in
   let mlist = if mods.mod_abstract then
      "abstract" :: mlist
   else
      mlist
   in
   let mlist = if mods.mod_static then
      "static" :: mlist
   else
      mlist
   in
   let mlist = if mods.mod_final then
      "final"  :: mlist
   else
      mlist
   in
   let mlist = List.rev mlist in
      pp_print_sep_list "" pp_print_string buf mlist

(*
 * Print a type.
 * Use precedences to reduce the
 * amount of parentheses.
 *)
let prec_none   = 0
let prec_fun    = 1
let prec_struct = 2
let prec_array  = 3

let rec pp_print_type pre buf = function
   TyUnit ->
      pp_print_string buf "unit"
 | TyNil ->
      pp_print_string buf "nil"
 | TyBool ->
      pp_print_string buf "bool"
 | TyChar ->
      pp_print_string buf "char"
 | TyString ->
      pp_print_string buf "string"
 | TyInt ->
      pp_print_string buf "int"
 | TyFloat ->
      pp_print_string buf "float"
 | TyArray ty ->
      fprintf buf "%a[]" (pp_print_type prec_array) ty
 | TyFun (ty_vars, ty_res) ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      fprintf buf "@[<hv 3>(@[%a@]) ->@ %a" (**)
         pp_print_type_list ty_vars
         (pp_print_type prec_fun) ty_res;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyCont ty_vars ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      fprintf buf "@[<hv 3>(@[%a@]) ->@ unit" (**)
         pp_print_type_list ty_vars;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyMethod (ty_this, ty_vars, ty_res) ->
      if pre >= prec_fun then
         pp_print_string buf "(";
      fprintf buf "@[<hv 3>method[%a] (@[%a@]) ->@ %a" (**)
         (pp_print_type prec_none) ty_this
         pp_print_type_list ty_vars
         (pp_print_type prec_fun) ty_res;
      if pre > prec_fun then
         pp_print_string buf ")";
      pp_close_box buf ()
 | TyObject cname ->
      fprintf buf "Obj(%a)" pp_print_symbol cname
 | TyVObject iname ->
      fprintf buf "VObj(%a)" pp_print_symbol iname
 | TyVar v ->
      fprintf buf "'%a" pp_print_symbol v

and pp_print_type_list buf tyl =
   (pp_print_sep_list "," (pp_print_type prec_none)) buf tyl

let pp_print_type = pp_print_type prec_none

(*
 * Print the constructor list.
 *)
let pp_print_decls buf consts =
   List.iter (fun (v, ty) ->
         fprintf buf "@ %a : %a" (**)
            pp_print_symbol v
            pp_print_type ty) consts

let pp_print_decls3 buf consts =
   List.iter (fun (v1, v2, ty) ->
         fprintf buf "@ %a=%a : %a" (**)
            pp_print_symbol v1
            pp_print_symbol v2
            pp_print_type ty) consts

(*
 * Print a field list.
 *)
let pp_print_fields buf table =
   try
      List.iter (fun (v_ext, v_int, ty) ->
            fprintf buf "@ @[<hv 3>%a, %a:@ %a@]" (**)
               pp_print_symbol v_ext
               pp_print_symbol v_int
               pp_print_type ty) (FieldMTable.to_list table)
   with
      Failure _ ->
         (* For some reason the fields are out of order *)
         FieldMTable.debug_iter (fun v_ext v_int ty index ->
               fprintf buf "@ *** Fields are out of order:@ @[<hv 3>%a, %a, %d:@ %a@]" (**)
                  pp_print_symbol v_ext
                  pp_print_symbol v_int index
                  pp_print_type ty) table

(*
 * Type defs.
 *)
let pp_print_tydef buf tydef =
   match tydef with
      TyDefInterface None ->
         fprintf buf "<interface>"
    | TyDefInterface (Some info) ->
         let { intf_name = name;
               intf_this = this;
               intf_methods = methods
             } = info
         in
            fprintf buf "@[<v 0>interface@ @[<v 3>{@ name = %a;@ this : %a;%a@]@ }@]" (**)
               pp_print_symbol name
               pp_print_symbol this
               pp_print_fields methods
    | TyDefClass None ->
         fprintf buf "<class>"
    | TyDefClass (Some info) ->
         let { class_name = n;
               class_this = v;
               class_parents = parents;
               class_interfaces = interfaces;
               class_consts = consts;
               class_methods = methods;
               class_fields = fields
             } = info
         in
            fprintf buf "@[<v 0>class['%a]" pp_print_symbol v;
            List.iter (fun parent ->
                  fprintf buf "@ extends %a" pp_print_symbol parent) parents;
            SymbolTable.iter (fun v funs ->
                  fprintf buf "@ implements %a @[<hv 0>{%a}@]" (**)
                     pp_print_symbol v
                     pp_print_decls3 funs) interfaces;
            fprintf buf "@ @[<v 0>{@ name = %a@ @[<v 3>constructors:%a@]@ @[<v 3>methods:%a@]@ @[<v 3>fields:%a@]@]@ }@]" (**)
               pp_print_symbol n
               pp_print_fields consts
               pp_print_fields methods
               pp_print_fields fields

(*
 * Atoms.
 *)
let pp_print_atom buf = function
   AtomUnit ->
      pp_print_string buf "()"
 | AtomNil ->
      pp_print_string buf "nil"
 | AtomBool b ->
      pp_print_bool buf b
 | AtomChar c ->
      pp_print_string buf ("\'" ^ Char.escaped c ^ "\'")
 | AtomInt i ->
      pp_print_int buf i
 | AtomFloat x ->
      pp_print_float buf x
 | AtomVar v ->
      pp_print_symbol buf v

(*
 * Array dimensions.
 *)
let pp_print_array_dimens buf atoms =
   List.iter (fun a -> fprintf buf "[%a]" pp_print_atom a) atoms

(*
 * Unary operations.
 *)
let pp_print_unop buf (op, a) =
   match op with
      UMinusIntOp ->
         fprintf buf "-[int] %a" pp_print_atom a
    | UMinusFloatOp ->
         fprintf buf "-[float] %a" pp_print_atom a
    | UNotIntOp ->
         fprintf buf "~%a" pp_print_atom a
    | UNotBoolOp ->
         fprintf buf "!%a" pp_print_atom a
    | UIntOfFloat ->
         fprintf buf "int_of_float(%a)" pp_print_atom a
    | UFloatOfInt ->
         fprintf buf "float_of_int(%a)" pp_print_atom a

(*
 * Binary operations.
 *)
let pp_print_binop buf (op, a1, a2) =
   let s =
      match op with
         AddIntOp -> "+"
       | SubIntOp -> "-"
       | MulIntOp -> "*"
       | DivIntOp -> "/"
       | RemIntOp -> "%"
       | AndIntOp -> "&"
       | OrIntOp  -> "|"
       | LslIntOp -> "<<"
       | LsrIntOp -> ">>>"
       | AsrIntOp -> ">>"
       | XorIntOp -> "^"
       | EqIntOp  -> "=="
       | NeqIntOp -> "!="
       | LeIntOp  -> "<="
       | LtIntOp  -> "<"
       | GtIntOp  -> ">"
       | GeIntOp  -> ">="

       | AddFloatOp -> "+."
       | SubFloatOp -> "-."
       | MulFloatOp -> "*."
       | DivFloatOp -> "/."
       | RemFloatOp -> "%."
       | EqFloatOp  -> "==."
       | NeqFloatOp -> "!=."
       | LeFloatOp  -> "<=."
       | LtFloatOp  -> "<."
       | GtFloatOp  -> ">."
       | GeFloatOp  -> ">=."
   in
      fprintf buf "%a %s %a" pp_print_atom a1 s pp_print_atom a2

(*
 * A "let" opener.
 *)
let pp_print_let_open buf (v, ty) =
   fprintf buf "@[<hv 3>let %a :@ %a =@ " (**)
      pp_print_symbol v
      pp_print_type ty

let rec pp_print_let_close buf e =
   fprintf buf " in@]@ %a" pp_print_exp e

(*
 * Print an expression.
 *)
and pp_print_exp buf = function
   LetFuns (funs, e) ->
      fprintf buf "@[<v 0>let %a in@]@ %a" (**)
         (pp_print_pre_sep_list "and" pp_print_function) funs
         pp_print_exp e

 | LetAtom (v, ty, a, e) ->
      pp_print_let_open buf (v, ty);
      pp_print_atom buf a;
      pp_print_let_close buf e

 | LetVar (v, ty, a, e) ->
      fprintf buf "@[<hv 3>let var %a :@ %a =@ %a in@]@ %a" (**)
         pp_print_symbol v
         pp_print_type ty
         pp_print_atom a
         pp_print_exp e

 | LetUnop (v, ty, op, a, e) ->
      fprintf buf "%a%a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_unop (op, a)
         pp_print_let_close e

 | LetBinop (v, ty, op, a1, a2, e) ->
      fprintf buf "%a%a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_binop (op, a1, a2)
         pp_print_let_close e

 | LetApply (v, ty, f, args, e) ->
      fprintf buf "%a%a(%a)%a" (**)
         pp_print_let_open (v, ty)
         pp_print_symbol f
         (pp_print_sep_list "," pp_print_atom) args
         pp_print_let_close e

 | LetApplyMethod (v, ty, f, this, args, e) ->
      fprintf buf "%a%a[%a](%a)%a" (**)
         pp_print_let_open (v, ty)
         pp_print_symbol f
         pp_print_atom this
         (pp_print_sep_list "," pp_print_atom) args
         pp_print_let_close e

 | LetExt (v, ty1, s, ty2, args, e) ->
      fprintf buf "%aexternal (\"%s\" : %a)(%a)%a" (**)
         pp_print_let_open (v, ty1)
         (String.escaped s)
         pp_print_type ty2
         (pp_print_sep_list "," pp_print_atom) args
         pp_print_let_close e

 | TailCall (f, args) ->
      fprintf buf "%a(%a)" (**)
         pp_print_symbol f
         (pp_print_sep_list "," pp_print_atom) args

 | Return (f, a) ->
      fprintf buf "return{%a}(%a)" (**)
         pp_print_symbol f
         pp_print_atom a

 | LetString (v, s, e) ->
      fprintf buf "let %a = \"%s\" in@ %a" (**)
         pp_print_symbol v
         (String.escaped s)
         pp_print_exp e

 | IfThenElse (a, e1, e2) ->
      fprintf buf "@[@[<hv 3>if %a then@ %a@]@ @[<hv 3>else@ %a@]@]" (**)
         pp_print_atom a
         pp_print_exp e1
         pp_print_exp e2

 | SetVar (v, ty, a, e) ->
      fprintf buf "%a : %a <- %a;@ %a" (**)
         pp_print_symbol v
         pp_print_type ty
         pp_print_atom a
         pp_print_exp e

 | LetSubscript (v, ty, a1, a2, e) ->
      fprintf buf "%a%a[%a]%a" (**)
         pp_print_let_open (v, ty)
         pp_print_atom a1
         pp_print_atom a2
         pp_print_let_close e

 | SetSubscript (a1, a2, ty, a3, e) ->
      fprintf buf "%a[%a] : %a <- %a;@ %a" (**)
         pp_print_atom a1
         pp_print_atom a2
         pp_print_type ty
         pp_print_atom a3
         pp_print_exp e

 | LetProject (v, ty, a, l, e) ->
      fprintf buf "%a%a.%a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_atom a
         pp_print_symbol l
         pp_print_let_close e

 | SetProject (a1, l, ty, a2, e) ->
      fprintf buf "%a.%a : %a <- %a;@ %a" (**)
         pp_print_atom a1
         pp_print_symbol l
         pp_print_type ty
         pp_print_atom a2
         pp_print_exp e

 | LetObject (v, ty, f, e) ->
      fprintf buf "%anew object %a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_symbol f
         pp_print_let_close e
 | LetVObject (v, ty, iname, cname, a, e) ->
      fprintf buf "%ainterface_of_class[%a<-%a](%a)%a" (**)
         pp_print_let_open (v, ty)
         pp_print_symbol iname
         pp_print_symbol cname
         pp_print_atom a
         pp_print_let_close e
 | LetProjObject (v, ty, a, e) ->
      fprintf buf "%aobject_of(%a)%a" (**)
         pp_print_let_open (v, ty)
         pp_print_atom a
         pp_print_let_close e
 | LetArray (v, ty, args, a, e) ->
      fprintf buf "%anew array%a = %a%a" (**)
         pp_print_let_open (v, ty)
         pp_print_array_dimens args
         pp_print_atom a
         pp_print_let_close e
 | Try (e1, v, e2) ->
      fprintf buf "@[@[<hv 3>try {@ %a@]@ }@ @[<hv 3>catch(%a) {@ %a@]@]" (**)
         pp_print_exp e1
         pp_print_symbol v
         pp_print_exp e2
 | Raise a ->
      fprintf buf "raise(%a)" pp_print_atom a
 | TypeCase (a, cases, e) ->
      fprintf buf "@[<v 3>match %a with" pp_print_atom a;
      List.iter (fun (cname, v, e) ->
            fprintf buf "@ @[<hv 3>| %a %a ->@ %a@]" (**)
               pp_print_symbol cname
               pp_print_symbol v
               pp_print_exp e) cases;
      fprintf buf "@ @[<hv 3>_ ->@ %a@]@]" pp_print_exp e

and pp_print_function buf (f, (gflag, f_ty, vars, body)) =
   let s =
      match gflag with
         FunGlobalClass -> "$global"
       | FunMethodClass -> "$method"
       | FunLocalClass -> "$local"
       | FunContClass -> "$cont"
   in
      fprintf buf "%s %a(%a)@ : %a@ @[<hv 3>{@ %a@]@ }" (**)
         s
         pp_print_symbol f
         (pp_print_sep_list "," pp_print_symbol) vars
         pp_print_type f_ty
         pp_print_exp body

(*
 * Print a program.
 *)
let pp_print_prog buf prog =
   let { prog_types = tenv;
         prog_funs = funs;
         prog_main = main;
         prog_object = v_object;
         prog_imports = imports
       } = prog
   in
      (* Use a wide margin.  Don't. n8 *)
      pp_set_margin buf 80;

      (* Print the types *)
      fprintf buf "@[<v 0>@[<v 3>Types:";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<v 3>%a =@ %a;@]" (**)
               pp_print_symbol v
               pp_print_tydef ty) tenv;

      (* Print the imported types *)
      fprintf buf "@]@ @[<v 3>Imported Types:";
      SymbolTable.iter (fun v ty ->
            fprintf buf "@ @[<v 3>%a =@ %a;@]" (**)
               pp_print_symbol v
               pp_print_tydef ty) imports;

      (* Print the exported fun *)
      fprintf buf "@]@ @[<v 3>Main function: %a;" pp_print_symbol main;

      (* Print the exported type *)
      fprintf buf "@]@ @[<v 3>Object type: %a;" pp_print_symbol v_object;

      (* Print the funs *)
      fprintf buf "@]@ @[<v 3>Funs:";
      SymbolTable.iter (fun f info ->
            fprintf buf "@ %a" pp_print_function (f, info)) funs;
      fprintf buf "@]@]"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
