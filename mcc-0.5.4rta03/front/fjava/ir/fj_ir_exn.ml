(*
 * Exceptions during the IR phase.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Symbol

open Fj_ir
open Fj_ir_print

(*
 * Exceptions use "position" info.
 * This helps make it possible to print
 * useful messages when an exception is raised.
 *)
type ir_pos =
   AstPos of Fj_ast.pos
 | AstExpPos of Fj_ast.expr
 | VarPos of var
 | IrExpPos of exp
 | StringPos of ir_pos * string

(*
 * This is the real exception type.
 *)
exception IrException of ir_pos * ir_exn

(*
 * Functions to build positions.
 *)
let ast_pos pos =
   AstPos pos

let ast_exp_pos e =
   AstExpPos e

let vexp_pos v =
   VarPos v

let ir_exp_pos e =
   IrExpPos e

let string_pos s pos =
   StringPos (pos, s)

(*
 * Printers.
 *)
let rec pp_print_pos buf = function
   StringPos (pos, s) ->
      fprintf buf "%a@ /%s" pp_print_pos pos s
 | IrExpPos e ->
      fprintf buf "@[<hv 3>%a@]" pp_print_exp e
 | VarPos v ->
      pp_print_symbol buf v
 | AstExpPos e ->
      Fj_ast_print.pp_print_expr buf e
 | AstPos pos ->
      Fj_ast_print.pp_print_pos buf pos

(*
 * Print an exception.
 *)
let pp_print_exn buf = function
   UnboundVar v ->
      fprintf buf "unbound var: %a" pp_print_symbol v
 | UnboundType v ->
      fprintf buf "unbound type: %a" pp_print_symbol v
 | UnboundClass v ->
      fprintf buf "unbound class: %a" pp_print_symbol v
 | UnboundLabel v ->
      fprintf buf "unbound label: %a" pp_print_symbol v
 | TypeError ->
      pp_print_string buf "type error"
 | TypeMismatch (ty1, ty2) ->
      fprintf buf "@[<v 3>type error:@ @[<hv 3>this expression has type:@ %a@]@ @[<hv 3>but is used with type:@ %a@]@]" (**)
         pp_print_type ty2
         pp_print_type ty1
 | InconvertibleTypes ->
      pp_print_string buf "inconvertible types"
 | NotImplemented ->
      pp_print_string buf "Not implemented"
 | StringError s ->
      pp_print_string buf s
 | StringIntError (s, i) ->
      fprintf buf "%s: %d" s i
 | StringVarError (s, v) ->
      fprintf buf "%s: %a" s pp_print_symbol v
 | StringTypeError (s, ty) ->
      fprintf buf "%s: %a" s pp_print_type ty
 | ArityMismatch (i1, i2) ->
      fprintf buf "arity mismatch: wanted %d, got %d args" i1 i2
 | StringVarVarError (s, v1, v2) ->
      fprintf buf "%s: %a, %a" s pp_print_symbol v1 pp_print_symbol v2
 | OverloadError (v, ty_args, choices) ->
      fprintf buf "@[<v 3>no matching method for %a(%a), choices are:%a@]" (**)
         pp_print_symbol v
         pp_print_type_list ty_args
         pp_print_decls choices

(*
 * General exception printer.
 *)
let pp_print_exn buf exn =
   match exn with
      IrException (pos, exn) ->
         fprintf buf "@[<v 0>*** Error: IR exception@ %a@ %a@]" pp_print_pos pos pp_print_exn exn
    | exn ->
         Fj_ast_exn.pp_print_exn buf exn

(*
 * Catch and print any exceptions.
 *)
let catch f x =
   try f x with
      IrException _
    | Fj_ast.AstException _
    | Parsing.Parse_error as exn ->
         fprintf err_formatter "%a@." pp_print_exn exn;
         exit 1

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
