(*
 * Parser tester is a toploop.
 * You can type an expression.
 * It is printed.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Debug

(*
 * Program usage.
 *)
let usage =
   "usage: compile [options] [files...] [-- program-args...]"

(*
 * Program arguments.
 *)
let rev_argv = ref []

(*
 * Evaluate the main function in the
 * class with the same name as the file.
 *)
let eval_flag = ref false

(*
 * This is the name of the file to compile.
 *)
let compile_name = ref None

let set_compile s =
   compile_name := Some s

(*
 * Compilation.
 *)
let compile name =
   (* Generate the AST *)
   let prog = Fj_ast_compile.compile name in

   (* Generate the IR *)
   let prog = Fj_ir_compile.compile prog (Filename.chop_extension 
         (Filename.basename name)) in
      prog

(*
 * Print exceptions.
 *)
let compile name =
   let prog = Fj_ir_exn.catch compile name in
      (* Program evaluation *)
      if !eval_flag then
         Fj_ir_eval.eval_prog prog (Filename.chop_extension 
               (Filename.basename name)) (List.rev !rev_argv)

(*
 * Call the main program.
 *)
let spec =
   ["-print_ast", Arg.Set Fj_ast_state.print_ast, "print the AST";
    "-print_ir",  Arg.Set Fj_ir_state.print_ir,   "print the IR";
    "-check_ir",  Arg.Set Fj_ir_state.check_ir,   "typecheck the IR";
    "-eval",      Arg.Set eval_flag,              "evaluate the main function in a class";
    "--", Arg.Rest (fun s -> rev_argv := s :: !rev_argv), "program arguments"]

let _ =
   Arg.parse spec set_compile usage;
   match !compile_name with
       Some name ->
          Fj_ir_eval.catch compile name
     | None ->
          ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
