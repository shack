(*
 * Type checking.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Symbol

open Fj_ir
open Fj_ir_env
open Fj_ir_exn
open Fj_ir_type
open Fj_ir_state

let vexp_pos v =
   string_pos "Fj_ir_check" (vexp_pos v)

let ir_exp_pos pos =
   string_pos "Fj_ir_check" (ir_exp_pos pos)

(************************************************************************
 * TYPE EQUALITY
 ************************************************************************)

(*
 * Compare two types.
 * Returns true iff the types are equal.
 *)
let rec equal_types env pos ty1 ty2 =
   let pos = string_pos "equal_types" pos in
      match ty1, ty2 with
         TyUnit, TyUnit
       | TyNil, TyNil
       | TyBool, TyBool
       | TyChar, TyChar
       | TyString, TyString
       | TyInt, TyInt
       | TyFloat, TyFloat ->
            true

       | TyArray ty1, TyArray ty2 ->
            equal_types env pos ty1 ty2

       | TyFun (ty_vars1, ty_res1), TyFun (ty_vars2, ty_res2) ->
            equal_type_lists env pos ty_vars2 ty_vars1 &&
            equal_types env pos ty_res1 ty_res2
            
       | TyMethod (ty_this1, ty_vars1, ty_res1), 
         TyMethod (ty_this2, ty_vars2, ty_res2) ->
            equal_type_lists env pos ty_vars2 ty_vars1 &&
            equal_types env pos ty_res1 ty_res2 &&
            equal_types env pos ty_this1 ty_this2
            
       | TyCont ty_vars1, TyCont ty_vars2 ->
            equal_type_lists env pos ty_vars2 ty_vars1

       | TyObject name1, TyObject name2
       | TyVObject name1, TyVObject name2
       | TyVar name1, TyVar name2 ->
            Symbol.eq name1 name2

       | _ ->
            false

(*
 * Check if two lists are equal.
 *)
and equal_type_lists env pos tyl1 tyl2 =
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
      len1 = len2 && List.for_all2 (equal_types env pos) tyl1 tyl2

(*
 * Ignore the first argument on methods.
 *)
let equal_method_types env pos ty_this1 ty1 ty_this2 ty2 =
   let pos = string_pos "equal_method_types" pos in
   let _, ty_args1, ty_res1 = dest_method_type env pos ty1 in
   let _, ty_args2, ty_res2 = dest_method_type env pos ty2 in
      equal_type_lists env pos ty_args1 ty_args2 && equal_types env pos ty_res1 ty_res2

(************************************************************************
 * TYPE CHECKING
 ************************************************************************)


(*
 * Check that one type can be converted to another.
 * The coerce_flag is true iff coercions between vobject
 * and object types are allowed.
 *)
let rec check_types coerce_flag env pos ty1 ty2 =
   let pos = string_pos "check_types" pos in
      match ty1, ty2 with
         TyUnit, TyUnit
       | TyBool, TyBool
       | TyChar, TyChar
       | TyString, TyString
       | TyInt, TyInt
       | TyFloat, TyFloat
       | TyNil, TyNil
       | TyString, TyNil
       | TyArray _, TyNil
       | TyFun _, TyNil
       | TyCont _, TyNil
       | TyObject _, TyNil
       | TyVObject _, TyNil ->
            ()

       | TyArray ty1, TyArray ty2 ->
            check_types true env pos ty1 ty2

       | TyFun (ty_vars1, ty_res1), TyFun (ty_vars2, ty_res2) ->
            check_type_lists env pos ty_vars2 ty_vars1;
            check_types false env pos ty_res1 ty_res2

       | TyCont ty_vars1, TyCont ty_vars2 ->
            check_type_lists env pos ty_vars2 ty_vars1
            
       | TyObject cname1, TyObject cname2
         when is_parent_class env pos cname1 cname2 ->
            ()

       | TyVObject iname1, TyVObject iname2
         when Symbol.eq iname1 iname2 ->
            ()

       | TyObject cname, TyVObject iname
       | TyVObject iname, TyObject cname
         when coerce_flag && class_implements_interface env pos cname iname ->
            ()

       | _ ->
            raise (IrException (pos, TypeMismatch (ty1, ty2)))

and check_type_lists env pos tyl1 tyl2 =
   let len1 = List.length tyl1 in
   let len2 = List.length tyl2 in
      if len1 <> len2 then
         raise (IrException (pos, ArityMismatch (len1, len2)));
      List.iter2 (check_types false env pos) tyl1 tyl2

(*
 * By default, coercions are not allowed.
 *)
let check_types = check_types false

(************************************************************************
 * EXPRESSION TYPES
 ************************************************************************)

(*
 * Get the type of an atom.
 *)
let type_of_atom env pos a =
   let pos = string_pos "type_of_atom" pos in
      match a with
         AtomUnit ->
            TyUnit
       | AtomBool _ ->
            TyBool
       | AtomChar _ ->
            TyChar
       | AtomInt _ ->
            TyInt
       | AtomFloat _ ->
            TyFloat
       | AtomNil ->
            TyNil
       | AtomVar v ->
            env_lookup_var env pos v

(*
 * General type checking.
 *)
let rec type_of_exp env code =
   let pos = string_pos "type_of_exp" (ir_exp_pos code) in
      match code with
         LetFuns (funs, e) ->
            type_of_funs_exp env pos funs e
       | LetAtom (v, ty, a, e)
       | LetVar  (v, ty, a, e) ->
            type_of_atom_exp env pos v ty a e
       | LetUnop (v, ty, op, a, e) ->
            type_of_unop_exp env pos v ty op a e
       | LetBinop (v, ty, op, a1, a2, e) ->
            type_of_binop_exp env pos v ty op a1 a2 e
       | LetApply (v, ty, f, args, e) ->
            type_of_apply_exp env pos v ty f args e
       | LetApplyMethod (v, ty, f, a, args, e) ->
            type_of_apply_method_exp env pos v ty f a args e
       | LetExt (v, ty1, s, ty2, args, e) ->
            type_of_ext_exp env pos v ty1 s ty2 args e
       | TailCall (f, args) ->
            type_of_tailcall_exp env pos f args
       | Return (f, a) ->
            type_of_return env pos f a
       | IfThenElse (a, e1, e2) ->
            type_of_if_exp env pos a e1 e2
       | Try (e1, v, e2) ->
            type_of_try_exp env pos e1 v e2
       | Raise a ->
            type_of_raise_exp env pos a
       | TypeCase (a, cases, e) ->
            type_of_typecase_exp env pos a cases e
       | SetVar (v, ty, a, e) ->
            type_of_set_var_exp env pos v ty a e
       | SetSubscript (a1, ty, a2, a3, e) ->
            type_of_set_subscript_exp env pos a1 ty a2 a3 e
       | LetSubscript (v, ty, a1, a2, e) ->
            type_of_subscript_exp env pos v ty a1 a2 e
       | SetProject (a1, l, ty, a2, e) ->
            type_of_set_project_exp env pos a1 l ty a2 e
       | LetProject (v, ty, a, l, e) ->
            type_of_project_exp env pos v ty a l e
       | LetString (v, s, e) ->
            type_of_string_exp env pos v s e
       | LetArray (v, ty, args, a, e) ->
            type_of_array_exp env pos v ty args a e
       | LetObject (v, ty, cname, e) ->
            type_of_object_exp env pos v ty cname e
       | LetVObject (v, ty, iname, cname, a, e) ->
            type_of_vobject_exp env pos v ty iname cname a e
       | LetProjObject (v, ty, a, e) ->
            type_of_proj_object_exp env pos v ty a e

(*
 * Check the function declarations.
 *)
and check_fun env pos f gflag f_ty vars body =
   let pos = string_pos "check_fun" pos in

   (* Check params *)
   let ty_vars, ty_res = dest_fun_type env pos f_ty in
   let len1 = List.length ty_vars in
   let len2 = List.length vars in
   let _ =
      if len1 <> len2 then
         raise (IrException (pos, ArityMismatch (len1, len2)))
   in

   (* Add the formal params to the env *)
   let env = List.fold_left2 env_add_var env vars ty_vars in

   (* Add the return type *)
   let env = env_add_return env f ty_res in

   (* Check return type *)
   let ty = type_of_exp env body in
      check_types env pos TyUnit ty

(*
 * Check the function declarations.
 *)
and type_of_funs_exp env pos funs e =
   let pos = string_pos "type_of_funs" pos in

   (* First, add all the functions to the env *)
   let env =
      List.fold_left (fun env (f, (_, f_ty, _, _)) ->
            env_add_var env f f_ty) env funs
   in
      (* Check fun definitions *)
      List.iter (fun (f, (gflag, f_ty, vars, body)) ->
            check_fun env pos f gflag f_ty vars body) funs;

      (* The type for the rest of the program *)
      type_of_exp env e

(*
 * Check a LetAtom.
 * Check that the type is the same as the argument.
 *)
and type_of_atom_exp env pos v ty a e =
   let pos = string_pos "type_of_atom_exp" pos in
      check_types env pos ty (type_of_atom env pos a);
      type_of_exp (env_add_var env v ty) e

(*
 * Unary operation.
 *)
and type_of_unop_exp env pos v ty op a e =
   let pos = string_pos "type_of_unop_exp" pos in
   let ty_to, ty_from =
      match op with
         UMinusIntOp
       | UNotIntOp     -> TyInt,   TyInt
       | UMinusFloatOp -> TyFloat, TyFloat
       | UNotBoolOp    -> TyBool,  TyBool
       | UIntOfFloat   -> TyInt,   TyFloat
       | UFloatOfInt   -> TyFloat, TyInt
   in
   let ty2 = type_of_atom env pos a in
      check_types env pos ty ty_to;
      check_types env pos ty_from ty2;
      type_of_exp (env_add_var env v ty) e

(*
 * Binary operation.
 *)
and type_of_binop_exp env pos v ty op a1 a2 e =
   let pos = string_pos "type_of_binop_exp" pos in
   let ty_dst, ty_src =
      match op with
         AddIntOp
       | SubIntOp
       | MulIntOp
       | DivIntOp
       | RemIntOp
       | LslIntOp
       | LsrIntOp
       | AsrIntOp
       | AndIntOp
       | OrIntOp
       | XorIntOp ->
            TyInt, TyInt

       | EqIntOp
       | NeqIntOp
       | LeIntOp
       | LtIntOp
       | GtIntOp
       | GeIntOp ->
            TyBool, TyInt

       | AddFloatOp
       | SubFloatOp
       | MulFloatOp
       | DivFloatOp
       | RemFloatOp ->
            TyFloat, TyFloat

       | EqFloatOp
       | NeqFloatOp
       | LeFloatOp
       | LtFloatOp
       | GtFloatOp
       | GeFloatOp ->
            TyBool, TyFloat
   in
      check_types env pos ty ty_dst;
      check_types env pos ty_src (type_of_atom env pos a1);
      check_types env pos ty_src (type_of_atom env pos a2);
      type_of_exp (env_add_var env v ty) e

(*
 * Function application.
 * Check that the arguments have the right type and arity.
 *)
and type_of_tailcall_exp env pos f args =
   let pos = string_pos "type_of_tailcall_exp" pos in
   let f_ty = env_lookup_var env pos f in
      type_of_tailcall_type_exp env pos f_ty args

and type_of_tailcall_type_exp env pos f_ty args =
   let pos = string_pos "type_of_tailcall_type_exp" pos in
   let ty_vars, ty_res = dest_fun_type env pos f_ty in

   (* Check argument types *)
   let ty_args = List.map (type_of_atom env pos) args in
   let len1 = List.length ty_vars in
   let len2 = List.length ty_args in
      if len1 <> len2 then
         raise (IrException (pos, ArityMismatch (len1, len2)));
      List.iter2 (check_types env pos) ty_vars ty_args;
      ty_res

and type_of_apply_exp env pos v ty f args e =
   let pos = string_pos "type_of_apply_exp" pos in
   let ty' = type_of_tailcall_exp env pos f args in
      check_types env pos ty ty';
      type_of_exp (env_add_var env v ty) e

and type_of_apply_method_exp env pos v ty f a args e =
   let pos = string_pos "type_of_apply_method_exp" pos in
   let f_ty = env_lookup_var env pos f in
   let ty_this, ty_vars, ty_res = dest_method_type env pos f_ty in

   (* Check argument types *)
   let ty_args = List.map (type_of_atom env pos) args in
   let len1 = List.length ty_vars in
   let len2 = List.length ty_args in
      if len1 <> len2 then
         raise (IrException (pos, ArityMismatch (len1, len2)));
      List.iter2 (check_types env pos) ty_vars ty_args;
      ty_res

and type_of_ext_exp env pos v ty1 s ty2 args e =
   let pos = string_pos "type_of_ext_exp" pos in
   let ty1' = type_of_tailcall_type_exp env pos ty2 args in
      check_types env pos ty1 ty1';
      type_of_exp (env_add_var env v ty1) e

(*
 * Check a return value.
 *)
and type_of_return env pos f a =
   let pos = string_pos "type_of_return_exp" pos in
   let ty = type_of_atom env pos a in
   let ty' = env_lookup_return_type env pos f in
      check_types env pos ty' ty;
      TyUnit

(*
 * Check a conditional.
 *)
and type_of_if_exp env pos a e1 e2 =
   let pos = string_pos "type_of_if_exp" pos in
   let ty0 = type_of_atom env pos a in
   let ty1 = type_of_exp env e1 in
   let ty2 = type_of_exp env e2 in
      check_types env pos TyBool ty0;
      check_types env pos ty1 ty2;
      ty1

(*
 * Set a variable.
 *)
and type_of_set_var_exp env pos v ty a e =
   let pos = string_pos "type_of_set_exp" pos in
   let ty2 = type_of_atom env pos a in
   let ty1 = env_lookup_var env pos v in
      check_types env pos ty ty1;
      check_types env pos ty ty2;
      type_of_exp env e

(*
 * Array elements.
 *)
and type_of_subscript_exp env pos v ty a1 a2 e =
   let pos = string_pos "type_of_subscript_exp" pos in
   let ty1 = type_of_atom env pos a1 in
   let ty2 = type_of_atom env pos a2 in
      check_types env pos ty (dest_array_type env pos ty1);
      check_types env pos TyInt ty2;
      type_of_exp (env_add_var env v ty) e

and type_of_set_subscript_exp env pos a1 a2 ty a3 e =
   let pos = string_pos "type_of_set_subscript_exp" pos in
   let ty1 = type_of_atom env pos a1 in
   let ty2 = type_of_atom env pos a2 in
   let ty3 = type_of_atom env pos a3 in
      check_types env pos (dest_array_type env pos ty1) ty;
      check_types env pos TyInt ty2;
      check_types env pos ty ty3;
      type_of_exp env e

(*
 * Field projection.
 *)
and type_of_project_exp env pos v ty a label e =
   let pos = string_pos "type_of_project_exp" pos in
   let _, ty1 = type_of_field env pos (type_of_atom env pos a) label in
      check_types env pos ty ty1;
      type_of_exp (env_add_var env v ty) e

and type_of_set_project_exp env pos a1 label ty a2 e =
   let pos = string_pos "type_of_set_project_exp" pos in
   let mflag, ty1 = type_of_field env pos (type_of_atom env pos a1) label in
   let ty2 = type_of_atom env pos a2 in
      if not mflag then
         raise (IrException (pos, StringVarError ("field is not mutable", label)));
      check_types env pos ty1 ty;
      check_types env pos ty ty2;
      type_of_exp env e

(*
 * Exceptions.
 *)
and type_of_try_exp env pos e1 v e2 =
   let pos = string_pos "type_of_try_exp" pos in
   let ty1 = type_of_exp env e1 in
   let ty2 = type_of_exp (env_add_var env v ty_object) e2 in
      check_types env pos ty1 ty2;
      ty1

and type_of_raise_exp env pos a =
   let pos = string_pos "type_of_raise_exp" pos in
   let ty1 = type_of_atom env pos a in
      if not (is_object_type env pos ty1) then
         raise (IrException (pos, StringTypeError ("not an object", ty1)));
      TyUnit

(*
 * Typecase.
 *)
and type_of_typecase_exp env pos a cases e =
   let pos = string_pos "type_of_typecase_exp" pos in

   (* Get default type *)
   let ty = type_of_exp env e in

   (* Check cases *)
   let check_case (name, v, e) =
      match expand_tydef env pos name with
         TyDefClass _ ->
            ignore (type_of_exp (env_add_var env v (TyObject name)) e)
       | TyDefInterface _ ->
            ignore (type_of_exp (env_add_var env v (TyVObject name)) e)
   in
      if not (is_object_type env pos (type_of_atom env pos a)) then
         raise (IrException (pos, StringError "not an object"));
      List.iter check_case cases;
      ty

(*
 * String allocation.
 *)
and type_of_string_exp env pos v _ e =
   let pos = string_pos "type_of_string_exp" pos in
      type_of_exp (env_add_var env v TyString) e

(*
 * Array creation.
 *)
and type_of_array_exp env pos v ty args a e =
   let pos = string_pos "type_of_array_exp" pos in
   let rec type_of_array ty args =
      match args with
         a :: args ->
            check_types env pos TyInt (type_of_atom env pos a);
            type_of_array (TyArray ty) args
       | [] ->
            ty
   in
   let ty' = type_of_array (type_of_atom env pos a) args in
      check_types env pos ty ty';
      type_of_exp (env_add_var env v ty) e

(*
 * Object allocation.
 *)
and type_of_object_exp env pos v ty name e =
   let pos = string_pos "type_of_object_exp" pos in

   (* Check the result type *)
   let ty_obj = TyObject name in
      check_types env pos ty ty_obj;
      type_of_exp (env_add_var env v ty) e

and type_of_vobject_exp env pos v ty iname cname a e =
   let pos = string_pos "type_of_vobject_exp" pos in
   let ty' = type_of_atom env pos a in
   let cname' = dest_object_type env pos ty' in
   let iname' = dest_vobject_type env pos ty in
      if not (class_implements_interface env pos cname iname) then
         raise (IrException (pos, StringVarError 
               ("interface not implemented", iname)));
      if not (Symbol.eq iname' iname) then
         raise (IrException (pos, StringVarError 
               ("interface type does not match", iname)));
      if not (Symbol.eq cname' cname) then
         raise (IrException (pos, StringVarError 
               ("class type does not match", cname)));
      type_of_exp (env_add_var env v ty) e

and type_of_proj_object_exp env pos v ty a e =
   let pos = string_pos "type_of_proj_object_exp" pos in
   let ty1 = type_of_atom env pos a in
   let cname = dest_object_type env pos ty in
      if not (is_vobject_type env pos ty1) then
         raise (IrException (pos, StringTypeError ("not a vobject", ty1)));
      type_of_exp (env_add_var env v ty) e

(************************************************************************
 * WELL-FORMEDNESS
 ************************************************************************)

(*
 * Check that the first interface list is a subset of the second.
 *)
let check_sub_interfaces pos name1 interfaces1 name2 interfaces2 =
   let pos = string_pos "check_sub_interfaces" pos in
      SymbolTable.iter (fun v _ ->
            if not (SymbolTable.mem interfaces2 v) then
               raise (IrException (pos, StringVarVarError 
                     ("interface declaration missing", name2, v)))) interfaces1

(*
 * Check the methods in a class or interface.
 * The first set of methods should be a subset of the second.
 *)
let check_sub_methods env pos name1 methods1 name2 methods2 =
   let pos = string_pos "check_sub_methods" pos in

   (* Check the fields *)
   let ty_this1 = TyObject name1 in
   let ty_this2 = TyObject name2 in
   let check_methods (v1, _, ty1) (v2, _, ty2) =
      if not (Symbol.eq v1 v2) then
         raise (IrException (pos, StringVarVarError 
               ("methods have different names", v1, v2)));
      if not (equal_method_types env pos ty_this1 ty1 ty_this2 ty2) then
         raise (IrException (pos, StringVarVarError 
               ("methods have different types", v1, v2)))
   in

   (* Get field lists *)
   let fields1 = sort_fields pos methods1 in
   let fields2 = sort_fields pos methods2 in
   let len1 = List.length fields1 in
   let len2 = List.length fields2 in
      if len1 > len2 then
         raise (IrException (pos, StringVarVarError 
               ("classes have different method counts", name1, name2)));
      Mc_list_util.short_iter2 check_methods fields1 fields2;
      len1 <> len2

let check_methods env pos name1 methods1 name2 methods2 =
   let pos = string_pos "check_methods" pos in
      if check_sub_methods env pos name1 methods1 name2 methods2 then
         raise (IrException (pos, StringVarVarError 
               ("classes have different method counts", name1, name2)))

(*
 * Check the fields in a class.
 *)
let check_sub_fields env pos name1 fields1 name2 fields2 =
   let pos = string_pos "check_sub_fields" pos in

   (* Check the fields *)
   let check_fields (v1, _, ty1) (v2, _, ty2) =
      if not (Symbol.eq v1 v2) then
         raise (IrException (pos, StringVarVarError 
               ("fields have different names", v1, v2)));
      if not (equal_types env pos ty1 ty2) then
         raise (IrException (pos, StringVarVarError 
               ("fields have different types", v1, v2)))
   in

   (* Get field lists *)
   let fields1 = sort_fields pos fields1 in
   let fields2 = sort_fields pos fields2 in
   let len1 = List.length fields1 in
   let len2 = List.length fields2 in
      if len1 > len2 then
         raise (IrException (pos, StringVarVarError 
               ("classes have different field counts", name1, name2)));
      Mc_list_util.short_iter2 check_fields fields1 fields2;
      len1 <> len2

let check_fields env pos name1 fields1 name2 fields2 =
   let pos = string_pos "check_fields" pos in
      if check_sub_fields env pos name1 fields1 name2 fields2 then
         raise (IrException (pos, StringVarVarError 
               ("classes have different field counts", name1, name2)))

(*
 * Check that a class implements an interface.
 * All the methods have to exist, and there should
 * be a method with the name of the interface.
 *)
let check_class_interface env pos cname methods1 iname methods2 =
   let pos = string_pos "check_class_interface" pos in

   (* Get the interface method info *)
   let info = dest_interface_type env pos iname in
   let { intf_this = this; intf_methods = methods3 } = info in
   let ty_class = TyObject cname in
   let ty_this = TyObject this in
   let methods3 = sort_fields pos methods3 in

   (* Check that each method is defined with the same type in the interface *)
   let len1 = List.length methods3 in
   let len2 = List.length methods2 in
      if len1 <> len2 then
         raise (IrException (pos, ArityMismatch (len1, len2)));
      List.iter2 (fun (v, v_int1, ty1) (v_int2, _, ty2) ->
            if not (equal_method_types env pos ty_this ty1 ty_class ty2 && Symbol.eq v_int1 v_int2) then
               raise (IrException (pos, StringVarError ("method type mismatch", v)))) methods3 methods2;

      (* Check that each method is defined in the class *)
      List.iter (fun (_, v, ty1) ->
            let _, ty2 = type_of_field env pos ty_class v in
               if not (equal_method_types env pos ty_class ty1 ty_class ty2) 
               then
                  raise (IrException (pos, StringVarError 
                        ("method type mismatch", v)))) methods2

(*
 * Check that all the interfaces are implemented.
 *)
let check_implements env pos cname info =
   let pos = string_pos "check_implements" pos in
   let { class_interfaces = interfaces;
         class_methods = methods
       } = info
   in
      SymbolTable.iter (check_class_interface env pos cname methods) interfaces

(*
 * Check that the parent info is preserved.
 *)
let check_parent env pos cname1 info1 =
   let pos = string_pos "check_parent" pos in
   let { class_parents = parents;
         class_interfaces = interfaces1;
         class_methods = methods1;
         class_fields = fields1
       } = info1
   in
      match parents with
         cname2 :: _ ->
            let info2 = dest_class_type env pos cname2 in
            let { class_interfaces = interfaces2;
                  class_methods = methods2;
                  class_fields = fields2
                } = info2
            in
               check_sub_interfaces pos cname2 interfaces2 cname1 interfaces1;
               ignore (check_sub_methods env pos cname2 methods2 cname1 
                     methods1);
               ignore (check_sub_fields env pos cname2 fields2 cname1 fields1)
       | [] ->
            ()

(*
 * Check that a class agrees with all its parents
 * and interfaces.
 *)
let check_class env pos cname info =
   let pos = string_pos "check_class" pos in
      check_implements env pos cname info;
      check_parent env pos cname info

(*
 * Check an interface.
 * All we care is that each method has the interface as
 * its first argument.
 *)
let check_interface env pos name info =
   let pos = string_pos "check_interface" pos in
   let { intf_this = this_var;
         intf_methods = methods
       } = info
   in
   let methods = sort_fields pos methods in
      List.iter (fun (v, _, ty) ->
            match expand_type env pos ty with
               TyMethod (TyObject this_var', _, _) 
                     when Symbol.eq this_var' this_var ->
                  ()
             | _ ->
                  raise (IrException (pos, StringVarError 
                        ("interface method has the wrong \"this\" type", v)))
            ) methods

(************************************************************************
 * PROGRAM
 ************************************************************************)

(*
 * Add the data segment.
 *)
let check_prog prog =
   let { prog_types = tydefs;
         prog_funs = funs;
         prog_main = main
       } = prog
   in

   (* Add all the tydefs to the environment *)
   let env =
      SymbolTable.fold (fun env v ty ->
            env_add_tydef env v ty) env_empty tydefs
   in

   (* Add all the funs *)
   let env =
      SymbolTable.fold (fun env f (_, ty, _, _) ->
            env_add_var env f ty) env funs
   in
      (* Check all the class definitions *)
      SymbolTable.iter (fun name tydef ->
            let pos = vexp_pos name in
               match tydef with
                  TyDefClass (Some info) ->
                     check_class env pos name info
                | TyDefInterface (Some info) ->
                     check_interface env pos name info
                | TyDefClass None
                | TyDefInterface None ->
                     ()) tydefs;

      (* Make sure the main function is defined *)
      if not (SymbolTable.mem funs main) then
         let pos = vexp_pos main in
            raise (IrException (pos, StringVarError 
                     ("main is not defined", main)));

      (* Check all the funs *)
      SymbolTable.iter (fun f (gflag, ty, vars, body) ->
            let pos = vexp_pos f in
               check_fun env pos f gflag ty vars body) funs

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
