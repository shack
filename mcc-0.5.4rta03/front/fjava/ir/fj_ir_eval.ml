(*
 * Simple IR evaluator.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Format
open Debug
open Symbol
open Field_table

open Fj_ir
open Fj_ir_exn
open Fj_ir_env
open Fj_ir_type
open Fj_ir_print
open Fj_ir_state

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Values.
 *)
type ir_value =
   ValNil
 | ValUnit
 | ValBool of bool
 | ValChar of char
 | ValInt of int
 | ValFloat of float
 | ValString of string
 | ValArray of ir_value array
 | ValFun of symbol * fun_value
 | ValClass of class_value
 | ValObject of object_value
 | ValVObject of vobject_value

(*
 * Variable environment.
 *)
and venv = ir_value ref SymbolTable.t

(*
 * A function takes a list of arguments,
 * and computes a value.
 *)
and fun_value = ir_value list -> ir_value

(*
 * A class has a set of valid class names,
 * a set of methods, and a set of fields.
 * The interfaces defines the method sets for
 * each of the implemented interfaces.
 *)
and class_value =
   { classval_name       : var;
     classval_parents    : SymbolSet.t;
     classval_interfaces : venv SymbolTable.t;
     classval_methods    : venv;
     classval_fields     : venv
   }

(*
 * An object has to belong to a class,
 * it has a copy of the fields.
 *)
and object_value =
   { object_class  : class_value;
     object_fields : venv
   }

(*
 * A vobject has and object and some methods.
 *)
and vobject_value =
   { vobject_methods : venv;
     vobject_object : object_value
   }

(*
 * Return a value from the function.
 *)
exception ReturnExn of venv * var * ir_value

(*
 * Throw an exception.
 *)
exception ThrowExn of venv * ir_value

(*
 * Evaluation exception.
 *)
exception EvalException of ir_pos * venv * ir_exn
exception EvalValException of ir_pos * venv * ir_value * ir_exn

(************************************************************************
 * PRINTER
 ************************************************************************)

(*
 * Print a value.
 *)
let rec pp_print_value buf v =
   match v with
      ValNil ->
         pp_print_string buf "nil"
    | ValUnit ->
         pp_print_string buf "()"
    | ValBool b ->
         pp_print_bool buf b
    | ValChar c ->
         pp_print_string buf ("'" ^ Char.escaped c ^ "'")
    | ValString s ->
         pp_print_string buf ("\"" ^ String.escaped s ^ "\"")
    | ValInt i ->
         pp_print_int buf i
    | ValFloat x ->
         pp_print_float buf x
    | ValFun (name, _) ->
         fprintf buf "<fun %a>" pp_print_symbol name
    | ValArray a ->
         fprintf buf "[|@[<hv 0>%a@]|]" pp_print_array a
    | ValClass info ->
         let { classval_name = name;
               classval_parents = parents;
               classval_interfaces = interfaces;
               classval_methods = methods;
               classval_fields = fields
             } = info
         in
            fprintf buf "@[<v 0>class %a@ extends %a%a@ @[<v 0>{@[<v 3> methods:%a@]@ @[<v 3>  fields:%a@]@]@ }@]" (**)
               pp_print_symbol name
               pp_print_classval_names parents
               pp_print_interfaces interfaces
               pp_print_table methods
               pp_print_table fields
    | ValObject info ->
         let { object_class = { classval_name = name };
               object_fields = fields
             } = info
         in
            fprintf buf "@[<v 0>@[<v 3>object %a {%a@]@ }@]" (**)
               pp_print_symbol name
               pp_print_table fields
    | ValVObject info ->
         let { vobject_methods = methods;
               vobject_object = obj
             } = info
         in
            fprintf buf "@[<v 0>vobject@ @[<v 3>{@ methods:%a@ object:%a@]@ }@]" (**)
               pp_print_table methods
               pp_print_value (ValObject obj)

(*
 * Print array elements.
 *)
and pp_print_array buf a =
   Array.iteri (fun i v ->
         if i <> 0 then
            pp_print_space buf ();
         fprintf buf "%a;" pp_print_value v) a

(*
 * Print class names.
 *)
and pp_print_classval_names buf names =
   ignore (SymbolSet.fold (fun i name ->
                 if i <> 0 then
                    fprintf buf ", ";
                 pp_print_symbol buf name;
                 succ i) 0 names)

(*
 * Print an interface.
 *)
and pp_print_interfaces buf interfaces =
   SymbolTable.iter (fun name methods ->
         fprintf buf "@ @[<v 3>implements %a%a@]" (**)
            pp_print_symbol name
            pp_print_table methods) interfaces

(*
 * Print the value table.
 *)
and pp_print_table buf fields =
   SymbolTable.iter (fun v { contents = a } ->
         fprintf buf "@ @[<hv 3>%a =@ %a@];" (**)
            pp_print_symbol v
            pp_print_value a) fields

(*
 * Print the variable environment.
 *)
let pp_print_venv buf venv =
   SymbolTable.iter (fun v { contents = a } ->
         fprintf buf "@ @[<hv 3>%a =@ %a;@]" (**)
            pp_print_symbol v
            pp_print_value a) venv

(************************************************************************
 * ENVIRONMENTS
 ************************************************************************)

(*
 * Normal variable environment.
 *)
let venv_empty = SymbolTable.empty

let venv_add_ref = SymbolTable.add

let venv_add venv v a =
   venv_add_ref venv v (ref a)

let venv_lookup_ref venv pos v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (EvalException (pos, venv, UnboundVar v))

let venv_lookup venv pos v =
   !(venv_lookup_ref venv pos v)

let venv_set venv pos v a =
   (venv_lookup_ref venv pos v) := a

let venv_copy venv =
   SymbolTable.map (fun vref ->
         ref !vref) venv

(*
 * Get fields from objects.
 *)
let venv_lookup_object_field_ref venv obj pos label =
   try SymbolTable.find obj.object_fields label with
      Not_found ->
         try SymbolTable.find obj.object_class.classval_methods label with
            Not_found ->
               raise (EvalValException (pos, venv, ValObject obj, UnboundLabel label))

let venv_lookup_object_field venv pos obj label =
   !(venv_lookup_object_field_ref venv obj pos label)

let venv_set_object_field venv pos obj label a =
   (venv_lookup_object_field_ref venv obj pos label) := a

(************************************************************************
 * EVALUATOR
 ************************************************************************)

(*
 * Coercions.
 *)
let coerce_bool venv pos a =
   match a with
      ValBool b -> b
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a bool"))

let coerce_int venv pos a =
   match a with
      ValInt i -> i
    | _ -> raise (EvalValException (pos, venv, a, StringError "not an int"))

let coerce_float venv pos a =
   match a with
      ValFloat x -> x
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a float"))

let coerce_string venv pos a =
   match a with
      ValString s -> s
    | _ -> raise (EvalValException (pos, venv, a, StringError "not a string"))

let coerce_array venv pos a =
   match a with
      ValArray a -> a
    | _ -> raise (EvalValException (pos, venv, a, StringError "not an array"))

let coerce_object venv pos a =
   match a with
      ValObject obj -> obj
    | _ -> raise (EvalValException (pos, venv, a, StringError "not an object"))

(*
 * Make an object from a class.
 *)
let object_of_class venv pos cname =
   match venv_lookup venv pos cname with
      ValClass cval ->
         let obj =
            { object_class = cval;
              object_fields = venv_copy cval.classval_fields
            }
         in
            ValObject obj
    | a ->
         raise (EvalValException (pos, venv, a, StringError "not a class"))

(*
 * Coerce an object to the given type.
 *)
let coerce_vobject venv pos iname obj =
   let pos = string_pos "coerce_vobject" pos in
   let { object_class = { classval_interfaces = interfaces } } = obj in
   let methods =
      try SymbolTable.find interfaces iname with
         Not_found ->
            raise (EvalValException (pos, venv, ValObject obj, StringVarError ("object does not implement interface", iname)))
   in
      ValVObject { vobject_methods = methods; vobject_object = obj }

let coerce_type venv pos ty a =
   match ty, a with
      TyVObject iname, ValObject obj ->
         coerce_vobject venv pos iname obj
    | TyObject cname, ValVObject { vobject_object = obj } ->
         ValObject obj
    | _ ->
         a

(*
 * Evaluate an atom.
 *)
let eval_atom venv pos a =
   let pos = string_pos "eval_atom" pos in
      match a with
         AtomUnit ->
            ValUnit
       | AtomNil ->
            ValNil
       | AtomBool b ->
            ValBool b
       | AtomChar c ->
            ValChar c
       | AtomInt i ->
            ValInt i
       | AtomFloat x ->
            ValFloat x
       | AtomVar v ->
            venv_lookup venv pos v

(*
 * General evaluator.
 *)
let rec eval_exp venv e =
   let pos = string_pos "eval_exp" (ir_exp_pos e) in
      if !trace_eval then
         fprintf err_formatter "@[<hv 3>eval:@ %a@]@." pp_print_exp e;
      match e with
         LetFuns (funs, e) ->
            eval_funs_exp venv pos funs e
       | LetAtom (v, _, a, e)
       | LetVar (v, _, a, e) ->
            eval_atom_exp venv pos v a e
       | LetObject (v, _, f, e) ->
            eval_object_exp venv pos v f e
       | LetVObject (v, _, iname, _, a, e) ->
            eval_vobject_exp venv pos v iname a e
       | LetProjObject (v, _, a, e) ->
            eval_proj_object_exp venv pos v a e
       | LetUnop (v, _, op, a, e) ->
            eval_unop_exp venv pos v op a e
       | LetBinop (v, _, op, a1, a2, e) ->
            eval_binop_exp venv pos v op a1 a2 e
       | LetApply (v, _, f, args, e) ->
            eval_apply_exp venv pos v f args e
       | LetApplyMethod (v, _, f, a, args, e) ->
            eval_apply_exp venv pos v f (a :: args) e
       | LetExt (v, _, s, _, args, e) ->
            eval_ext_exp venv pos v s args e
       | TailCall (f, args) ->
            eval_tailcall_exp venv pos f args
       | Return (f, a) ->
            eval_return_exp venv pos f a
       | IfThenElse (a, e1, e2) ->
            eval_if_exp venv pos a e1 e2
       | Try (e1, v, e2) ->
            eval_try_exp venv pos e1 v e2
       | Raise a ->
            eval_raise_exp venv pos a
       | TypeCase (a, cases, e) ->
            eval_typecase_exp venv pos a cases e
       | SetVar (v, _, a, e) ->
            eval_set_var_exp venv pos v a e
       | LetSubscript (v, ty, a1, a2, e) ->
            eval_subscript_exp venv pos v ty a1 a2 e
       | SetSubscript (a1, a2, _, a3, e) ->
            eval_set_subscript_exp venv pos a1 a2 a3 e
       | LetProject (v, _, a, l, e) ->
            eval_project_exp venv pos v a l e
       | SetProject (a1, l, _, a2, e) ->
            eval_set_project_exp venv pos a1 l a2 e
       | LetString (v, s, e) ->
            eval_string_exp venv pos v s e
       | LetArray (v, _, args, a, e) ->
            eval_array_exp venv pos v args a e

(*
 * Evaluate the function definitions.
 * Functions are recursive.  Make two passes,
 * collect function defs as refs, then set them in the
 * second pass.
 *)
and eval_fun venv pos f gflag vars body =
   let pos = string_pos "eval_funs_exp" pos in
   let len1 = List.length vars in
   let f' args =
      let pos = string_pos "funcall" (vexp_pos f) in

      (* Check arity and add the args *)
      let _ =
         let len2 = List.length args in
            if len2 <> len1 then
               raise (EvalException (pos, venv, ArityMismatch (len1, len2)))
      in
      let venv = List.fold_left2 venv_add venv vars args in

      (* Evaluate the body *)
      let v =
         match gflag with
            FunGlobalClass
          | FunMethodClass ->
               (try eval_exp venv body with
                   ReturnExn (_, f_ret, v) when f_ret = f ->
                      v)
          | FunLocalClass
          | FunContClass ->
               eval_exp venv body
      in
         v
   in
      venv_set venv pos f (ValFun (f, f'))

and eval_funs_exp venv pos funs e =
   let pos = string_pos "eval_funs_exp" pos in
   let venv =
      List.fold_left (fun venv (f, _) ->
            venv_add venv f ValNil) venv funs
   in
      List.iter (fun (f, (gflag, _, vars, body)) ->
            eval_fun venv pos f gflag vars body) funs;
      eval_exp venv e

(*
 * Atom.
 *)
and eval_atom_exp venv pos v a e =
   let pos = string_pos "eval_atom_exp" pos in
   let venv = venv_add venv v (eval_atom venv pos a) in
      eval_exp venv e

(*
 * Unary operation.
 *)
and eval_unop_exp venv pos v op a e =
   let pos = string_pos "eval_unop_exp" pos in
   let a = eval_atom venv pos a in
   let a =
      match op with
         UMinusIntOp ->
            ValInt (-(coerce_int venv pos a))
       | UMinusFloatOp ->
            ValFloat (-.(coerce_float venv pos a))
       | UNotIntOp ->
            ValInt (lnot (coerce_int venv pos a))
       | UNotBoolOp ->
            ValBool (not (coerce_bool venv pos a))
       | UIntOfFloat ->
            ValInt (int_of_float (coerce_float venv pos a))
       | UFloatOfInt ->
            ValFloat (float_of_int (coerce_int venv pos a))
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Binary operation.
 *)
and eval_binop_exp venv pos v op a1 a2 e =
   let pos = string_pos "eval_binop_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let a =
      match op with
         (* Integers *)
         AddIntOp -> ValInt (coerce_int venv pos a1 +    coerce_int venv pos a2)
       | SubIntOp -> ValInt (coerce_int venv pos a1 -    coerce_int venv pos a2)
       | MulIntOp -> ValInt (coerce_int venv pos a1 *    coerce_int venv pos a2)
       | DivIntOp ->
            let i1 = coerce_int venv pos a1 in
            let i2 = coerce_int venv pos a2 in
               if i2 = 0 then
                  raise (EvalException (pos, venv, StringError "divide by zero"));
               ValInt (i1 / i2)
       | RemIntOp ->
            let i1 = coerce_int venv pos a1 in
            let i2 = coerce_int venv pos a2 in
               if i2 = 0 then
                  raise (EvalException (pos, venv, StringError "divide by zero"));
               ValInt (i1 mod i2)
       | AndIntOp -> ValInt (coerce_int venv pos a1 land coerce_int venv pos a2)
       | OrIntOp  -> ValInt (coerce_int venv pos a1 lor  coerce_int venv pos a2)
       | LslIntOp -> ValInt (coerce_int venv pos a1 lsl  coerce_int venv pos a2)
       | LsrIntOp -> ValInt (coerce_int venv pos a1 lsr  coerce_int venv pos a2)
       | AsrIntOp -> ValInt (coerce_int venv pos a1 asr  coerce_int venv pos a2)
       | XorIntOp -> ValInt (coerce_int venv pos a1 lxor coerce_int venv pos a2)
       | EqIntOp  -> ValBool (coerce_int venv pos a1 =  coerce_int venv pos a2)
       | NeqIntOp -> ValBool (coerce_int venv pos a1 <> coerce_int venv pos a2)
       | LeIntOp  -> ValBool (coerce_int venv pos a1 <= coerce_int venv pos a2)
       | LtIntOp  -> ValBool (coerce_int venv pos a1 <  coerce_int venv pos a2)
       | GtIntOp  -> ValBool (coerce_int venv pos a1 >  coerce_int venv pos a2)
       | GeIntOp  -> ValBool (coerce_int venv pos a1 >= coerce_int venv pos a2)

         (* Floats *)
       | AddFloatOp -> ValFloat (coerce_float venv pos a1 +. coerce_float venv pos a2)
       | SubFloatOp -> ValFloat (coerce_float venv pos a1 -. coerce_float venv pos a2)
       | MulFloatOp -> ValFloat (coerce_float venv pos a1 *. coerce_float venv pos a2)
       | DivFloatOp -> ValFloat (coerce_float venv pos a1 /. coerce_float venv pos a2)
       | RemFloatOp -> ValFloat (mod_float (coerce_float venv pos a1) (coerce_float venv pos a2))
       | EqFloatOp  -> ValBool  (coerce_float venv pos a1 =  coerce_float venv pos a2)
       | NeqFloatOp -> ValBool  (coerce_float venv pos a1 <> coerce_float venv pos a2)
       | LeFloatOp  -> ValBool  (coerce_float venv pos a1 <= coerce_float venv pos a2)
       | LtFloatOp  -> ValBool  (coerce_float venv pos a1 <  coerce_float venv pos a2)
       | GtFloatOp  -> ValBool  (coerce_float venv pos a1 >  coerce_float venv pos a2)
       | GeFloatOp  -> ValBool  (coerce_float venv pos a1 >= coerce_float venv pos a2)
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Function application.
 *)
and eval_tailcall_exp venv pos f args =
   let pos = string_pos "eval_tailcall_exp" pos in
   let args = List.map (eval_atom venv pos) args in
      eval_tailcall_val venv pos f args

and eval_tailcall_val venv pos f args =
   let pos = string_pos "eval_tailcall_val" pos in
   let f =
      match venv_lookup venv pos f with
         ValFun (_, f) ->
            f
       | a ->
            raise (EvalValException (pos, venv, a, StringError "eval_tailcall_exp: not a function"))
   in
      f args

and eval_apply_exp venv pos v f args e =
   let pos = string_pos "eval_apply_exp" pos in
   let a = eval_tailcall_exp venv pos f args in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * External call.
 *)
and eval_ext_exp venv pos v s args e =
   let pos = string_pos "eval_ext_exp" pos in
   let args = List.map (eval_atom venv pos) args in
   let a =
      match s, args with
         "atoi", [ValString s] ->
            let i =
               (try int_of_string s with
                   Failure _ ->
                      raise (EvalValException (pos, venv, ValString s, StringError "not a number")))
            in
               ValInt i
       | "println", [ValString s] ->
            Pervasives.print_string s;
            Pervasives.print_char '\n';
            flush stdout;
            ValUnit
       | "itoa", [ValInt i] ->
            ValString (string_of_int i)
       | "ftoa", [ValFloat x] ->
            ValString (string_of_float x)
       | "strcat", [ValString s1; ValString s2] ->
            ValString (s1 ^ s2)
       | _ ->
            raise (EvalValException (pos, venv, ValString s, StringError "unknown external function"))
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Allocate an object.
 * By default.
 *)
and eval_object_exp venv pos v f e =
   let pos = string_pos "eval_object_exp" pos in
   let venv = venv_add venv v (object_of_class venv pos f) in
      eval_exp venv e

(*
 * Allocate a VObject.
 *)
and eval_vobject_exp venv pos v iname a e =
   let pos = string_pos "eval_vobject_exp" pos in
      match eval_atom venv pos a with
         ValObject obj ->
            coerce_vobject venv pos iname obj
       | a ->
            raise (EvalValException (pos, venv, a, StringError "not an object"))

(*
 * Project the object from a vobject.
 *)
and eval_proj_object_exp venv pos v a e =
   let pos = string_pos "eval_proj_object_exp" pos in
   let a =
      match eval_atom venv pos a with
         ValVObject { vobject_object = obj } ->
            ValObject obj
       | a ->
            raise (EvalValException (pos, venv, a, StringError "not a vobject"))
   in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Function return throws an exception to an
 * enclosing function.
 *)
and eval_return_exp venv pos f a =
   let pos = string_pos "eval_return_exp" pos in
      raise (ReturnExn (venv, f, eval_atom venv pos a))

(*
 * Conditional.
 *)
and eval_if_exp venv pos a e1 e2 =
   let pos = string_pos "eval_if_exp" pos in
   let e =
      match eval_atom venv pos a with
         ValBool true ->
            e1
       | ValBool false ->
            e2
       | a ->
            raise (EvalValException (pos, venv, a, StringError "not a boolean"))
   in
      eval_exp venv e

(*
 * Try evaluation.
 *)
and eval_try_exp venv pos e1 v e2 =
   let pos = string_pos "eval_try_exp" pos in
      try eval_exp venv e1 with
         ThrowExn (_, a) ->
            let venv = venv_add venv v a in
               eval_exp venv e2

(*
 * Throw a value to the exception handler.
 *)
and eval_raise_exp venv pos a =
   let pos = string_pos "eval_raise_exp" pos in
      raise (ThrowExn (venv, eval_atom venv pos a))

(*
 * Do a case analysis on an object.
 *)
and eval_typecase_exp venv pos a cases e =
   let pos = string_pos "eval_typecase_exp" pos in
   let a = eval_atom venv pos a in
   let obj = coerce_object venv pos a in
   let { classval_parents = parents;
         classval_interfaces = interfaces
       } = obj.object_class
   in
   let rec search = function
      (cname, v, e) :: cases ->
         if SymbolSet.mem parents cname then
            let venv = venv_add venv v a in
               eval_exp venv e
         else
            let methods =
               try Some (SymbolTable.find interfaces cname) with
                  Not_found ->
                     None
            in
               (match methods with
                   Some methods ->
                      let a = ValVObject { vobject_methods = methods; vobject_object = obj } in
                      let venv = venv_add venv v a in
                         eval_exp venv e
                 | None ->
                      search cases)
    | [] ->
         eval_exp venv e
   in
      search cases

(*
 * Set the value of a variable.
 *)
and eval_set_var_exp venv pos v a e =
   let pos = string_pos "eval_set_var_exp" pos in
   let a = eval_atom venv pos a in
      venv_set venv pos v a;
      eval_exp venv e

(*
 * Subscripting.
 * We have to insert an explicit coercion.
 *)
and eval_subscript_exp venv pos v ty a1 a2 e =
   let pos = string_pos "eval_subscript_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let a = coerce_array venv pos a1 in
   let i = coerce_int venv pos a2 in
   let a =
      if i < 0 || i >= Array.length a then
         raise (EvalValException (pos, venv, a1, StringIntError ("subscript out of bounds", i)));
      a.(i)
   in
   let venv = venv_add venv v a in
      eval_exp venv e

and eval_set_subscript_exp venv pos a1 a2 a3 e =
   let pos = string_pos "eval_set_subscript_exp" pos in
   let a1 = eval_atom venv pos a1 in
   let a2 = eval_atom venv pos a2 in
   let a = coerce_array venv pos a1 in
   let i = coerce_int venv pos a2 in
   let v = eval_atom venv pos a3 in
   let _ =
      if i < 0 || i >= Array.length a then
         raise (EvalValException (pos, venv, a1, StringIntError ("subscript out of bounds", i)));
      a.(i) <- v
   in
      eval_exp venv e

(*
 * Record projection.
 *)
and eval_project_exp venv pos v a label e =
   let pos = string_pos "eval_project_exp" pos in
   let a =
      match eval_atom venv pos a with
         ValObject obj ->
            venv_lookup_object_field venv pos obj label
       | ValVObject obj ->
            venv_lookup obj.vobject_methods pos label
       | a ->
            raise (EvalValException (pos, venv, a, StringError "not an object"))
   in
   let venv = venv_add venv v a in
      eval_exp venv e

and eval_set_project_exp venv pos a1 label a2 e =
   let pos = string_pos "eval_set_project_exp" pos in
   let obj = coerce_object venv pos (eval_atom venv pos a1) in
   let a2 = eval_atom venv pos a2 in
      venv_set_object_field venv pos obj label a2;
      eval_exp venv e

(*
 * String allocation.
 *)
and eval_string_exp venv pos v s e =
   let pos = string_pos "eval_string_exp" pos in
   let venv = venv_add venv v (ValString s) in
      eval_exp venv e

(*
 * Array allocation.
 *)
and eval_array_exp venv pos v args a e =
   let pos = string_pos "eval_array_exp" pos in
   let a = eval_atom venv pos a in
   let args = List.map (fun a -> coerce_int venv pos (eval_atom venv pos a)) args in
   let rec make_array args =
      match args with
         i :: args ->
            ValArray (Array.init i (fun _ -> make_array args))
       | [] ->
            a
   in
   let a = make_array args in
   let venv = venv_add venv v a in
      eval_exp venv e

(*
 * Global function returns the environment and the value.
 *)
let eval_exp venv exp =
   try venv, eval_exp venv exp with
      ReturnExn (venv, _, v) ->
         venv, v

(*
 * Get the default value for a type.
 *)
let default_value env pos ty =
   match expand_type env pos ty with
      TyUnit ->
         ValUnit
    | TyBool ->
         ValBool false
    | TyChar ->
         ValChar '\000'
    | TyInt  ->
         ValInt  0
    | TyFloat ->
         ValFloat 0.0

    | TyNil
    | TyString
    | TyArray _
    | TyFun _
    | TyCont _
    | TyMethod _
    | TyObject _
    | TyVObject _
    | TyVar _ ->
         ValNil

(*
 * Add the class initializer to the venv.
 *)
let collect_interfaces class_methods interfaces =
   SymbolTable.fold (fun interfaces iname fields ->
         let intf_methods =
            List.fold_left (fun intf_methods (v_int, v_class, _) ->
                  let pos = vexp_pos v_class in
                  let meth = venv_lookup_ref class_methods pos v_class in
                     venv_add_ref intf_methods v_int meth) venv_empty fields
         in
            SymbolTable.add interfaces iname intf_methods) SymbolTable.empty interfaces

let eval_class_def env pos venv name info =
   let { class_parents = parents;
         class_interfaces = interfaces;
         class_methods = methods;
         class_fields = fields
       } = info
   in

   (* Collect all the names *)
   let names = SymbolSet.add SymbolSet.empty name in
   let names = List.fold_left SymbolSet.add names parents in

   (* Get all the methods *)
   let methods =
      FieldMTable.fold (fun methods v_ext v_int _ ->
            let a = venv_lookup_ref venv pos v_int in
            let methods = venv_add_ref methods v_int a in
            let methods = venv_add_ref methods v_ext a in
               methods) venv_empty methods
   in

   (* Collect the interface methods *)
   let interfaces = collect_interfaces methods interfaces in

   (* Get all the fields *)
   let fields =
      FieldMTable.fold (fun fields _ v_int ty ->
            venv_add fields v_int (default_value env pos ty)) venv fields
   in

   (* Add the class *)
   let classval =
      { classval_name = name;
        classval_parents = names;
        classval_interfaces = interfaces;
        classval_methods = methods;
        classval_fields = fields
      }
   in
      venv_add venv name (ValClass classval)

(*
 * Find the exported class.
 *)
let find_global pos exports name =
   let pos = string_pos "find_global" pos in
   let name' =
      SymbolTable.fold (fun name' v s ->
            if s = name then
               Some v
            else
               name') None exports
   in
      match name' with
         Some name' ->
            name'
       | None ->
            raise (IrException (pos, StringError ("unbound class: " ^ name)))

(************************************************************************
 * CALLING MAIN
 ************************************************************************)

(*
 * Handle exceptions.
 *)
let pp_print_exn buf exn =
   match exn with
      EvalException (pos, venv, exn) ->
         fprintf buf "@[<v 0>%a@ @[<v 3>State:%a@]@]" (**)
            Fj_ir_exn.pp_print_exn (IrException (pos, exn))
            pp_print_venv venv
    | EvalValException (pos, venv, a, exn) ->
         fprintf buf "@[<v 0>%a@ %a@ @[<v 3>State:%a@]@]" (**)
            Fj_ir_exn.pp_print_exn (IrException (pos, exn))
            pp_print_value a
            pp_print_venv venv
    | exn ->
         Fj_ir_exn.pp_print_exn buf exn

(*
 * Catch exceptions.
 *)
let catch f x =
   try f x with
      EvalException _
    | EvalValException _
    | IrException _
    | Fj_ast.AstException _
    | Parsing.Parse_error as exn ->
         fprintf err_formatter "%a@." pp_print_exn exn;
         exit 1

(*
 * Evaluate a program.
 *)
let main_sym = Symbol.add "main"

let eval_prog prog filename argv =
   let { prog_funs = funs;
         prog_types = types;
         prog_main = main
       } = prog
   in
   let pos = string_pos "Fj_ir_var" (ast_pos ("<eval>", 0, 0, 0, 0)) in
   let env = env_of_prog prog in

   (* Add placeholders for all the funs *)
   let venv =
      SymbolTable.fold (fun venv f _ ->
            venv_add venv f ValNil) venv_empty funs
   in

   (* Add the class defs to the venv *)
   let venv =
      SymbolTable.fold (fun venv v tydef ->
            match tydef with
               TyDefClass (Some info) ->
                  eval_class_def env pos venv v info
             | TyDefClass None
             | TyDefInterface _ ->
                  venv) venv types
   in

   (* Now actually add the funs *)
   let _ =
      SymbolTable.iter (fun f (gflag, _, vars, body) ->
            eval_fun venv pos f gflag vars body) funs;
   in

   (* Convert the args *)
   let argv = List.map (fun s -> ValString s) argv in
   let argv = ValArray (Array.of_list argv) in
      (* Evaluate the main function *)
      ignore (eval_tailcall_val venv pos main [argv])

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
