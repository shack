(*
 * Generate IR code from AST expressions and types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fj_ir
open Fj_ir_env
open Fj_ir_exn
open Field_table

(*
 * Types.
 *)
val build_type : env -> ir_pos -> Fj_ast.ty -> ty

(*
 * Coercions.
 *)
val coerce_type : bool -> env -> ir_pos -> atom -> ty -> (env -> atom -> exp) -> exp

(*
 * Expressions.
 *)
val build_exp : env -> Fj_ast.expr -> (env -> atom -> exp) -> exp
val build_exp_list : env -> Fj_ast.expr list -> (env -> atom list -> exp) -> exp

(*
 * Select proper applications.
 *)
val apply_const : env -> ir_pos -> var -> atom list -> ty FieldMTable.t -> (env -> atom -> exp) -> exp

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
