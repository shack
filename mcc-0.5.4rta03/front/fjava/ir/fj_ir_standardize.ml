(*
 * Rename all the variables in a program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Field_table

open Fj_ir
open Fj_ir_exn

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Use symbol -> symbol map for the variables.
 *)
type env = var SymbolTable.t

(*
 * Empty env.
 *)
let env_empty = SymbolTable.empty

(*
 * Add a new variable.
 *)
let env_add = SymbolTable.add

(*
 * Lookup a variable from the environment.
 *)
let env_lookup env v =
   try SymbolTable.find env v with
      Not_found ->
         v

let env_lookup_opt env v =
   match v with
      Some v ->
         Some (env_lookup env v)
    | None ->
         None

let env_lookup_list env l =
   List.map (env_lookup env) l

(*
 * Check if a variable is defined.
 *)
let env_mem env v =
   SymbolTable.mem env v

(*
 * Special symbol that is defined while inside
 * a function body (so that we can check for nested
 * functions).
 *)
let return_sym = new_symbol_string "nested_fun"

(************************************************************************
 * STANDARDIZE
 ************************************************************************)

(*
 * Rename the vars in a type.
 *)
let rec standardize_type tenv venv ty =
   match ty with
      TyUnit
    | TyNil
    | TyBool
    | TyChar
    | TyString
    | TyInt
    | TyFloat ->
         ty
    | TyArray ty ->
         TyArray (standardize_type tenv venv ty)
    | TyFun (ty_vars, ty_res) ->
         TyFun (List.map (standardize_type tenv venv) ty_vars, 
                  standardize_type tenv venv ty_res)
    | TyCont ty_vars ->
         TyCont (List.map (standardize_type tenv venv) ty_vars)
    | TyMethod (ty_this, ty_vars, ty_res) ->
         TyMethod (standardize_type tenv venv ty_this,
                   List.map (standardize_type tenv venv) ty_vars,
                   standardize_type tenv venv ty_res)
    | TyObject cname ->
         TyObject (env_lookup tenv cname)
    | TyVObject iname ->
         TyVObject (env_lookup tenv iname)
    | TyVar v ->
         TyVar (env_lookup tenv v)

(*
 * Rename the constructors.
let standardize_decls tenv venv decls =
   List.map (fun (v, ty) ->
         let v = env_lookup venv v in
         let ty = standardize_type tenv venv ty in
            v, ty) decls
 *)

let standardize_decls3 tenv venv decls =
   List.map (fun (v1, v2, ty) ->
         let v1 = env_lookup venv v1 in
         let v2 = env_lookup venv v2 in
         let ty = standardize_type tenv venv ty in
            v1, v2, ty) decls

(*
 * Rename the vars in the interfaces.
 *)
let standardize_interfaces tenv venv interfaces =
   SymbolTable.fold (fun interfaces v funs ->
         let v = env_lookup tenv v in
         let funs = standardize_decls3 tenv venv funs in
            SymbolTable.add interfaces v funs) SymbolTable.empty interfaces

(*
 * Rename the vars in the field list.
 *)
let standardize_fields tenv venv fields =
   FieldMTable.map (fun v_ext v_int ty ->
         let v_int = env_lookup venv v_int in
         let ty = standardize_type tenv venv ty in
            v_int, ty) fields

(*
 * Rename the vars in the tydef.
 *)
let standardize_tydef tenv venv tydef =
   match tydef with
      TyDefInterface (Some info) ->
         let { intf_name = name;
               intf_this = v_this;
               intf_methods = methods;
               intf_package = pkg
             } = info
         in
         let info =
            { intf_name = name;
              intf_this = env_lookup tenv v_this;
              intf_methods = standardize_fields tenv venv methods;
              intf_package = pkg
            }
         in
            TyDefInterface (Some info)
    | TyDefClass (Some info) ->
         let { class_name = name;
               class_this = v_this;
               class_parents = parents;
               class_interfaces = interfaces;
               class_consts = consts;
               class_methods = methods;
               class_fields = fields;
               class_package = pkg
             } = info
         in
         let info =
            { class_name = name;
              class_this = v_this;
              class_parents = List.map (env_lookup tenv) parents;
              class_interfaces = standardize_interfaces tenv venv interfaces;
              class_consts = standardize_fields tenv venv consts;
              class_methods = standardize_fields tenv venv methods;
              class_fields = standardize_fields tenv venv fields;
              class_package = pkg
            }
         in
            TyDefClass (Some info)
    | TyDefInterface None
    | TyDefClass None ->
         tydef

(*
 * Rename the vars in an atom.
 *)
let standardize_atom tenv venv atom =
   match atom with
      AtomUnit
    | AtomNil
    | AtomBool _
    | AtomChar _
    | AtomInt _
    | AtomFloat _ ->
         atom
    | AtomVar v ->
         AtomVar (env_lookup venv v)

let standardize_atoms tenv venv atoms =
   List.map (standardize_atom tenv venv) atoms

(*
 * Rename all the vars in the expr.
 *)
let rec standardize_exp tenv venv e =
   match e with
      LetFuns (funs, e) ->
         standardize_funs_exp tenv venv funs e
    | LetAtom (v, ty, a, e) ->
         standardize_atom_exp tenv venv v ty a e
    | LetVar (v, ty, a, e) ->
         standardize_var_exp tenv venv v ty a e
    | LetUnop (v, ty, op, a, e) ->
         standardize_unop_exp tenv venv v ty op a e
    | LetBinop (v, ty, op, a1, a2, e) ->
         standardize_binop_exp tenv venv v ty op a1 a2 e
    | LetApply (v, ty, f, args, e) ->
         standardize_apply_exp tenv venv v ty f args e
    | LetApplyMethod (v, ty, f, a, args, e) ->
         standardize_apply_method_exp tenv venv v ty f a args e
    | LetExt (v, ty1, s, ty2, args, e) ->
         standardize_ext_exp tenv venv v ty1 s ty2 args e
    | TailCall (f, args) ->
         standardize_tailcall_exp tenv venv f args
    | Return (f, a) ->
         standardize_return_exp tenv venv f a
    | IfThenElse (a, e1, e2) ->
         standardize_if_exp tenv venv a e1 e2
    | Try (e1, v, e2) ->
         standardize_try_exp tenv venv e1 v e2
    | Raise a ->
         standardize_raise_exp tenv venv a
    | TypeCase (a, cases, e) ->
         standardize_typecase_exp tenv venv a cases e
    | SetVar (v, ty, a, e) ->
         standardize_set_var_exp tenv venv v ty a e
    | SetSubscript (a1, a2, ty, a3, e) ->
         standardize_set_subscript_exp tenv venv a1 a2 ty a3 e
    | LetSubscript (v, ty, a1, a2, e) ->
         standardize_subscript_exp tenv venv v ty a1 a2 e
    | SetProject (a1, l, ty, a2, e) ->
         standardize_set_project_exp tenv venv a1 l ty a2 e
    | LetProject (v, ty, a, l, e) ->
         standardize_project_exp tenv venv v ty a l e
    | LetString (v, s, e) ->
         standardize_string_exp tenv venv v s e
    | LetArray (v, ty, args, a, e) ->
         standardize_array_exp tenv venv v ty args a e
    | LetObject (v, ty, name, e) ->
         standardize_object_exp tenv venv v ty name e
    | LetVObject (v, ty, iname, cname, a, e) ->
         standardize_vobject_exp tenv venv v ty iname cname a e
    | LetProjObject (v, ty, a, e) ->
         standardize_proj_object_exp tenv venv v ty a e

(*
 * Rename functions and arguments.
 *)
and standardize_fun tenv venv funs (f, (gflag, f_ty, vars, body)) =
   (* Rename the function *)
   let f' = env_lookup venv f in
   let f_ty = standardize_type tenv venv f_ty in

   (* Rename the vars *)
   let venv, vars' =
      List.fold_left (fun (venv, vars') v ->
            let v' = new_symbol v in
            let venv = env_add venv v v' in
               venv, v' :: vars') (venv, []) vars
   in
   let vars' = List.rev vars' in

   (* Rename the body *)
   let body = standardize_exp tenv venv body in
      (f', (gflag, f_ty, vars', body)) :: funs

and standardize_funs tenv venv funs =
   (* Make up new names for the funs *)
   let venv =
      List.fold_left (fun venv (f, _) ->
            let f' = new_symbol f in
               env_add venv f f') venv funs
   in

   (* Rename the function's vars and body *)
   let funs = List.fold_left (standardize_fun tenv venv) [] funs in
      venv, List.rev funs

and standardize_funs_exp tenv venv funs e =
   (* Rename the funs *)
   let venv, funs = standardize_funs tenv venv funs in

   (* Rename the rest *)
   let e = standardize_exp tenv venv e in
      LetFuns (funs, e)

(*
 * Atom declaration.
 *)
and standardize_atom_exp tenv venv v ty a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetAtom (v', ty, a, e)

(*
 * Var declaration.
 *)
and standardize_var_exp tenv venv v ty a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetVar (v', ty, a, e)

(*
 * Unary operation.
 *)
and standardize_unop_exp tenv venv v ty op a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetUnop (v', ty, op, a, e)

(*
 * Binary operation.
 *)
and standardize_binop_exp tenv venv v ty op a1 a2 e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetBinop (v', ty, op, a1, a2, e)

(*
 * Function application.
 *)
and standardize_apply_exp tenv venv v ty f args e =
   let v' = new_symbol v in
   let f = env_lookup venv f in
   let args = standardize_atoms tenv venv args in
   let ty = standardize_type tenv venv ty in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetApply (v', ty, f, args, e)

and standardize_apply_method_exp tenv venv v ty f a args e =
   let v' = new_symbol v in
   let f = env_lookup venv f in
   let a = standardize_atom tenv venv a in
   let args = standardize_atoms tenv venv args in
   let ty = standardize_type tenv venv ty in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetApplyMethod (v', ty, f, a, args, e)

(*
 * External function application.
 *)
and standardize_ext_exp tenv venv v ty1 s ty2 args e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let args = standardize_atoms tenv venv args in
   let e = standardize_exp tenv venv' e in
   let ty1 = standardize_type tenv venv ty1 in
   let ty2 = standardize_type tenv venv ty2 in
      LetExt (v', ty1, s, ty2, args, e)

(*
 * Tail call.
 *)
and standardize_tailcall_exp tenv venv f args =
   let f = env_lookup venv f in
   let args = standardize_atoms tenv venv args in
      TailCall (f, args)

(*
 * Return a value.
 *)
and standardize_return_exp tenv venv f a =
   Return (env_lookup venv f, standardize_atom tenv venv a)

(*
 * Conditional expression.
 *)
and standardize_if_exp tenv venv a e1 e2 =
   let a  = standardize_atom tenv venv a in
   let e1 = standardize_exp tenv venv e1 in
   let e2 = standardize_exp tenv venv e2 in
      IfThenElse (a, e1, e2)

(*
 * Exceptions.
 *)
and standardize_try_exp tenv venv e1 v e2 =
   let e1 = standardize_exp tenv venv e1 in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e2 = standardize_exp tenv venv e2 in
      Try (e1, v', e2)

and standardize_raise_exp tenv venv a =
   Raise (standardize_atom tenv venv a)

and standardize_typecase_exp tenv venv a cases e =
   let a = standardize_atom tenv venv a in
   let cases =
      List.map (fun (cname, v, e) ->
            let cname = env_lookup tenv cname in
            let v' = new_symbol v in
            let venv = env_add venv v v' in
            let e = standardize_exp tenv venv e in
               cname, v', e) cases
   in
   let e = standardize_exp tenv venv e in
      TypeCase (a, cases, e)

(*
 * Set a variable.
 *)
and standardize_set_var_exp tenv venv v ty a e =
   let v = env_lookup venv v in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv e in
   let ty = standardize_type tenv venv ty in
      SetVar (v, ty, a, e)

(*
 * Array operations.
 *)
and standardize_subscript_exp tenv venv v ty a1 a2 e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetSubscript (v', ty, a1, a2, e)

and standardize_set_subscript_exp tenv venv a1 a2 ty a3 e =
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let a3 = standardize_atom tenv venv a3 in
   let e = standardize_exp tenv venv e in
   let ty = standardize_type tenv venv ty in
      SetSubscript (a1, a2, ty, a3, e)

(*
 * Record projection.
 *)
and standardize_project_exp tenv venv v ty a l e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let l = env_lookup venv l in
   let a = standardize_atom tenv venv a in
   let e = standardize_exp tenv venv' e in
   let ty = standardize_type tenv venv ty in
      LetProject (v', ty, a, l, e)

and standardize_set_project_exp tenv venv a1 l ty a2 e =
   let l = env_lookup venv l in
   let a1 = standardize_atom tenv venv a1 in
   let a2 = standardize_atom tenv venv a2 in
   let e = standardize_exp tenv venv e in
   let ty = standardize_type tenv venv ty in
      SetProject (a1, l, ty, a2, e)

(*
 * Allocate a string.
 *)
and standardize_string_exp tenv venv v s e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let e = standardize_exp tenv venv' e in
      LetString (v', s, e)

and standardize_array_exp tenv venv v ty args a e =
   let v' = new_symbol v in
   let venv' = env_add venv v v' in
   let e = standardize_exp tenv venv' e in
   let args = standardize_atoms tenv venv args in
   let a = standardize_atom tenv venv a in
   let ty = standardize_type tenv venv ty in
      LetArray (v', ty, args, a, e)

(*
 * Object allocation.
 *)
and standardize_object_exp tenv venv v ty name e =
   let name = env_lookup tenv name in
   let ty = standardize_type tenv venv ty in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetObject (v', ty, name, e)

and standardize_vobject_exp tenv venv v ty iname cname a e =
   let a = standardize_atom tenv venv a in
   let ty = standardize_type tenv venv ty in
   let iname = env_lookup tenv iname in
   let cname = env_lookup tenv cname in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetVObject (v', ty, iname, cname, a, e)

and standardize_proj_object_exp tenv venv v ty a e =
   let a = standardize_atom tenv venv a in
   let ty = standardize_type tenv venv ty in
   let v' = new_symbol v in
   let venv = env_add venv v v' in
   let e = standardize_exp tenv venv e in
      LetProjObject (v', ty, a, e)

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

(*
 * A program.
 *)
let standardize_prog prog =
   let { prog_types = types;
         prog_funs = funs;
         prog_main = main;
         prog_object = v_object;
         prog_imports = imports
       } = prog
   in

   (* First, give a new name to all the funs *)
   let venv, funs =
      SymbolTable.fold (fun (env, funs) f def ->
            let f' = new_symbol f in
            let env = env_add env f f' in
            let funs = SymbolTable.add funs f' def in
               env, funs) (env_empty, SymbolTable.empty) funs
   in

   (* Give new names to all the types *)
   let tenv, types =
      SymbolTable.fold (fun (tenv, types) v ty ->
            let v' = new_symbol v in
            let tenv = env_add tenv v v' in
            let types = SymbolTable.add types v' ty in
               tenv, types) (env_empty, SymbolTable.empty) types
   in

   (* Give new names to all the types *)
   let tenv, imports =
      SymbolTable.fold (fun (tenv, imports) v ty ->
            let v' = new_symbol v in
            let tenv = env_add tenv v v' in
            let imports = SymbolTable.add imports v' ty in
               tenv, imports) (tenv, SymbolTable.empty) imports
   in

   (* Now rename all the definitions *)
   let types = SymbolTable.map (standardize_tydef tenv venv) types in
   let imports = SymbolTable.map (standardize_tydef tenv venv) imports in
   let funs =
      SymbolTable.map (fun (gflag, ty, vars, body) ->
            let ty = standardize_type tenv venv ty in
            let vars' = List.map new_symbol vars in
            let venv = List.fold_left2 env_add venv vars vars' in
            let body = standardize_exp tenv venv body in
               gflag, ty, vars', body) funs
   in
      { prog_types = types;
        prog_funs = funs;
        prog_main = env_lookup venv main;
        prog_object = env_lookup tenv v_object;
        prog_imports = imports
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
