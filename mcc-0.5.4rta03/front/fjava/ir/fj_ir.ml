(*
 * FJava intermediate representation.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Field_table

(*
 * A variable is just a symbol.
 *)
type var = symbol
type ty_var = symbol

(* Note: "private protected" is a distinct access class.  shoot. *)
type access_spec =
   AccPublic
 | AccProtected
 | AccPrivate
 | AccDefault              (* Access allowed to all classes in same package.
                              Default access specifier if class/method/etc is
                              not explicitly public, protected, or private *)

(* Type modifiers *)
type ty_mod =
 { mod_access : access_spec;
   mod_abstract : bool;
   mod_static : bool;
   mod_final : bool;
 }
 
let ty_mod_empty =
 { mod_access = AccDefault;
   mod_abstract = false;
   mod_static = false;
   mod_final = false;        
 }

(*
 * Types.
 *)
type ty =
   (* Basic types *)
 | TyUnit
 | TyNil
 | TyBool
 | TyChar
 | TyString
 | TyInt
 | TyFloat

   (* Arrays of values, all elements have the same type *)
 | TyArray of ty

   (* Functions have a list of types for the arguments, and a result type *)
 | TyFun of ty list * ty

   (* A method has a "this" type, some args, and a result *)
 | TyMethod of (ty * ty list * ty)
 
   (* A continuation has arguments, but no return type *)
 | TyCont of ty list

   (* TyObject is an instance of a class *)
 | TyObject of ty_var

   (* TyVObject is an instance of an interface *)
 | TyVObject of ty_var

   (* This parameter *)
 | TyVar of ty_var


(* 
 * The parts of a package statement, e.g. [edu; caltech; mojave; util]
 *)
type package = symbol list

(*
 * A class has:
 *
 * A parent class list.  Only the built-in FjObject class
 * does not have a parent.
 *
 * A list of all interfaces that the class implements, including those 
 * implemented by parents.
 *
 * Constructors.  These are functions
 * that create instances of the class.
 *
 * The method and field lists hold _all_ of the methods
 * and fields, including those defined by parents.
 *
 * If you change this, also change the fclass version
 *)
type class_info =
   { class_name       : var;
     class_this       : ty_var;
     class_parents    : var list;
     class_interfaces : (var * var * ty) list SymbolTable.t;
     class_consts     : ty FieldMTable.t;
     class_methods    : ty FieldMTable.t;
     class_fields     : ty FieldMTable.t;
     class_package    : package option
   }

(*
 * An interface has:
 *
 * The "this" field is a type-identifier that stands for
 * the actual object type.  Each of the methods has
 * the this type for the first parameter and some type modifiers (for 
 * consistency).
 *
 * All fields in an interface are implicitly public static final
 * All methods in an interface are implicitly public abstract
 *
 * The methods have an external and internal names.
 *
 * If you change this, also change the fclass version
 *)
type interface_info =
   { intf_name    : var;
     intf_this    : ty_var;
     intf_methods : ty FieldMTable.t;
     intf_package : package option
   }
   
(* 
 * Version descriptor for .fclass files to prevent version mismatch
 *)
let fclass_vers = 0, 1, 0
type fclass_vers_ty = int * int * int

(*
 * Typedefs are used to define classes and types
 * at the program level.
 *)
type tydef =
   TyDefInterface of interface_info option
 | TyDefClass of class_info option

(*
 * Unary operators.
 *)
type unop =
   (* Arithmetic *)
   UMinusIntOp
 | UMinusFloatOp
 | UNotIntOp
 | UNotBoolOp

   (* Numeric coercions *)
 | UIntOfFloat
 | UFloatOfInt

(*
 * Binary operators.
 *)
type binop =
   (* Integers *)
   AddIntOp
 | SubIntOp
 | MulIntOp
 | DivIntOp
 | RemIntOp
 | AndIntOp
 | OrIntOp
 | LslIntOp
 | LsrIntOp
 | AsrIntOp
 | XorIntOp
 | EqIntOp
 | NeqIntOp
 | LeIntOp
 | LtIntOp
 | GtIntOp
 | GeIntOp

   (* Floats *)
 | AddFloatOp
 | SubFloatOp
 | MulFloatOp
 | DivFloatOp
 | RemFloatOp
 | EqFloatOp
 | NeqFloatOp
 | LeFloatOp
 | LtFloatOp
 | GtFloatOp
 | GeFloatOp

(*
 * Values.
 *)
type atom =
   AtomUnit
 | AtomNil
 | AtomBool  of bool
 | AtomChar  of char
 | AtomInt   of int
 | AtomFloat of float
 | AtomVar   of var

(*
 * Function classes:
 *    FunGlobalClass: user-defined function
 *    FunLocalClass: local function
 *)
type fun_class =
   FunGlobalClass
 | FunMethodClass
 | FunLocalClass
 | FunContClass

(*
 * Intermediate code expressions.
 *)
type exp =
   (* Function definition *)
   LetFuns of fundef list * exp

   (* Arithmetic *)
 | LetVar of var * ty * atom * exp
 | LetAtom of var * ty * atom * exp
 | LetUnop of var * ty * unop * atom * exp
 | LetBinop of var * ty * binop * atom * atom * exp

   (* Functions *)
 | LetApply of var * ty * var * atom list * exp
 | LetApplyMethod of var * ty * var * atom * atom list * exp
 | LetExt of var * ty * string * ty * atom list * exp
 | TailCall of var * atom list
 | Return of var * atom

   (* Conditional *)
 | IfThenElse of atom * exp * exp

   (* Exception handling *)
 | Try of exp * var * exp
 | Raise of atom
 | TypeCase of atom * (ty_var * var * exp) list * exp

   (* Variable assignment *)
 | SetVar of var * ty * atom * exp

   (* Array/pointer operations *)
 | SetSubscript of atom * atom * ty * atom * exp
 | LetSubscript of var * ty * atom * atom * exp

   (* Record projection *)
 | SetProject of atom * var * ty * atom * exp
 | LetProject of var * ty * atom * var * exp

   (* Allocation *)
 | LetString of var * string * exp
 | LetArray of var * ty * atom list * atom * exp
 | LetObject of var * ty * var * exp
 | LetVObject of var * ty * var * var * atom * exp
 | LetProjObject of var * ty * atom * exp

(*
 * A function definition has:
 *    1. a function classification
 *    2. the function type
 *    3. then formal parameters
 *    4. the body
 *)
and fundef = var * fun_info

and fun_info = fun_class * ty * var list * exp

(*
 * A program has the following parts:
 *   prog_types: type definitions
 *   prog_funs: a set of function definitions
 *   prog_main: the name of the main function
 *   prog_object: the name of the FjObject type
 *   prog_imports: types that are imported from other files
 *)
type prog =
   { prog_types    : tydef SymbolTable.t;
     prog_funs     : fun_info SymbolTable.t;
     prog_main     : var;
     prog_object   : var;
     prog_imports  : tydef SymbolTable.t
   }

(*
 * These are the exceptions that can happen during
 * the IR stage.
 *)
type ir_exn =
   UnboundVar of var
 | UnboundType of var
 | UnboundClass of var
 | UnboundLabel of var
 | NotImplemented
 | InconvertibleTypes
 | TypeError
 | TypeMismatch of ty * ty
 | ArityMismatch of int * int
 | StringError of string
 | StringIntError of string * int
 | StringVarError of string * var
 | StringTypeError of string * ty
 | StringVarVarError of string * var * var
 | OverloadError of var * ty list * (var * ty) list

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
