(*
 * Debug flags for the IR stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fj_ir

(*
 * Print the IR when it is generated.
 *)
let print_ir = Fir_state.debug_print_ir

(*
 * Typecheck the IR.
 *)
let check_ir = Fir_state.debug_check_ir

(*
 * Print an execution trace during evaluation.
 *)
let trace_eval = ref false

(*
 * These are the canonical names for "this" and "super".
 *)
let this_var   = Symbol.add "this"
let super_var  = Symbol.add "super"

let this_atom  = AtomVar this_var
let super_atom = AtomVar super_var

(*
 * This is the canonical Object type.
 *)
let object_name     = "FjObject"
let object_var      = Symbol.add object_name
let ty_object       = TyObject object_var
let object_init_var = Symbol.add "FjObject.init0"


(* 
 * The empty package name
 *)
let empty_pkg_var = Symbol.add "empty"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
