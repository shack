open Python_ast

let pos_of_expr = function
   IdExpr	(_,p)
 | LiteralExpr	(_,p)
 | TupleExpr	(_,p)
 | ListExpr	(_,p)
 | DictExpr	(_,p)
 | StrConvExpr	(_,p)
 | AttrRefExpr	(_,_,p)
 | SliceExpr	(_,_,p)
 | CallExpr	(_,_,p)
 | OpExpr	(_,_,_,p)
 | FuncExpr	(_,_,p)		-> p

let pos_of_param = function
   IdParam        (_,p)
 | SublistParam   (_,p)
 | DefParam       (_,_,p)
 | ExtraPosParam  (_,p)
 | ExtraKwParam   (_,p)		-> p

let pos_of_stmt = function
   ExprList	 (_,p)
 | AssertStmt	 (_,_,p)
 | AssignStmt	 (_,_,p)
 | AugAssignStmt (_,_,_,p)
 | PassStmt	 (p)
 | DelStmt	 (_,p)
 | PrintStmt	 (_,_,p)
 | ReturnStmt	 (_,p)
 | RaiseStmt	 (_,_,_,p)
 | BreakStmt	 (p)
 | ContStmt	 (p)
 | ImportStmt	 (_,_,p)
 | GlobalStmt	 (_,p)
 | StmtList	 (_,p)
 | IfStmt	 (_,_,_,p)
 | WhileStmt	 (_,_,_,p)
 | ForStmt	 (_,_,_,_,p)
 | TryStmt	 (_,_,_,p)
 | FuncDef	 (_,_,_,p)
 | ClassDef	 (_,_,_,p)	-> p
