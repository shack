let compile filename =
   (*
   let lexbuf = Lexing.from_channel (open_in filename) in
   *)
   try
      (*
      let ast = Python_parse.file_input Python_lex.token lexbuf in
      Python_ast_print.print_suite [ast];
      print_newline ();
      *)
      let prog = Python_ir_ast.compile filename in
      Fir_print.print_prog prog;
      print_newline ()

   with
      Parsing.Parse_error ->
         print_endline "Parse error."

    | Python_ir_ast.NotImplemented s ->
         print_string "NotImplemented exception caught: ";
         print_endline s

    | Python_ir_ast.StringError s ->
         print_string "StringError exception caught: ";
         print_endline s

let _ =
   Sys.catch_break true;
   Arg.parse [] compile "python_test [files]"
