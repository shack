type pos = string * int * int * int * int

let print_pos (file, sline, schar, eline, echar) =
   if sline == eline then begin
      Format.printf "  File \"%s\", line %d, " file sline;
      if echar = schar then
         Format.printf "character %d\n" schar
      else
         Format.printf "characters %d-%d\n" schar echar
      end
   else
      Format.printf "  File \"%s\", line %d, char %d-line %d, char %d\n"
                    file sline schar eline echar

(*
 * File position information.
 *)
let position = ref ("<null>", 1, 0, 1, 0)

(*
let set_current_position pos =
   position := pos

let current_position () =
   !position
   *)

(* File position *)
let current_file = ref "<null>"
(*
let current_line = ref 1
let current_schar = ref 0
*)

(*
let new_line () =
   current_line := !current_line + 1;
   current_schar := 0

let add_lines n =
   current_line := !current_line + n

(*
 * Get the position of the current lexeme.
 *)
let set_lexeme_position lexbuf =
    let line = !current_line in
    let schar = Lexing.lexeme_start lexbuf - !current_schar in
    let echar = Lexing.lexeme_end lexbuf - !current_schar in
    let file = !current_file in
    let pos = file, line, schar, line, echar in
    set_current_position pos;
    pos
    *)


let next_line = ref 1
let next_char = ref 1

(*
 * Set the current position to the given lexeme.  Assumes this has been called
 * on every lexeme.
 *)
let set_position lexeme =
   (* The lexeme begins wherever we left off. *)
   let sline, schar = !next_line, !next_char in

   let length = String.length lexeme in

   (* Given the line and character position of the character at index i of
    * lexeme, advance returns the line and character position of the last
    * character of lexeme, and sets next_line and next_char apropriately. *)
   let rec advance i (sline, schar) =
      try
         let newline = String.index_from lexeme i '\n' in
         if newline = length - 1 then
            begin
               (* The lexeme ends with a newline.  Set next_* to the first
                * character of the next line, and return the position of the
                * newline. *)
               next_line := sline + 1;
               next_char := 1;
               sline, schar + length - i
            end
         else
            (* The lexeme has a newline, but it's not at the end.  Recurse. *)
            advance (newline + 1) (sline + 1, 1)
      with
         Not_found ->
            (* The lexeme has no (more) newlines. *)
            next_line := sline;
            let echar = schar + (length - i) - 1 in
            next_char := echar + 1;
            sline, echar
   in

   let eline, echar = advance 0 (!next_line, !next_char) in
   let pos = !current_file, sline, schar, eline, echar in
   position := pos;
   pos
