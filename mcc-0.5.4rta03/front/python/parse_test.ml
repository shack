open Python_ast
open Python_ast_print

let command_line = ref false

(* Attempts to parse command line arguments. *)
let func str =
   command_line := true;
   let lexbuf = Lexing.from_string str in
   try
      let el = Python_parse.eval_input Python_lex.token lexbuf in
      print_expr_list el;
      print_newline ()
   with Parsing.Parse_error ->
      print_string "Parse error."; print_newline(); flush stdout

let _ = Arg.parse [] func "blah"

(* If there were no command line arguments, parse stdin. *)
let _ =
   if not !command_line
   then
      let lexbuf = Lexing.from_channel stdin in
      try
	 let result = Python_parse.file_input Python_lex.token lexbuf in
	 let rec print_stmt_list = function
	       []	-> ()
	     | [s]	-> print_stmt s
	     | s::tl	-> print_stmt s; print_newline (); print_stmt_list tl
	 in
	 print_endline ">>>";
	 print_stmt_list result;
	 print_newline ();
	 flush stdout
      with Parsing.Parse_error ->
	 print_string "Parse error."; print_newline (); flush stdout
