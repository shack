open Symbol
open Fir
open Naml_ir
open Python_ir
open Location



(*
 * The Python runtime addition function.  It inspects the type of its arguments
 * and adds apropriately.
 *)
let add_func = new_symbol_string "add"
let add_fundef : Naml_ir.fundef =
   let arg1 = new_symbol_string "add_arg" in
   let arg2 = new_symbol_string "add_arg" in
   let result = new_symbol_string "add_result" in
   let int_int_case =
      let tmp_int = new_symbol_string "tmp_int" in
      let tmp_int1 = new_symbol_string "tmp_int" in
      let tmp_int2 = new_symbol_string "tmp_int" in
      get_int tmp_int1 (AtomVar arg1) (
      get_int tmp_int2 (AtomVar arg2) (
      LetBinop (tmp_int, TyInt, PlusIntOp, AtomVar tmp_int1, AtomVar tmp_int2,
      new_int result (AtomVar tmp_int) (
      Return (AtomVar result)))))
   in
   let else_case = raise_exception () in
   let add_func_body =
      Match (AtomVar arg1,
         [int_tag_set,
            Match (AtomVar arg2,
               [int_tag_set, int_int_case;
                all_tag_set, else_case]);
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty; py_ty], py_ty) in
   add_func, (bogus_loc "add_func"), FunGlobalClass, ty, [arg1; arg2], add_func_body

let minus_func = new_symbol_string "minus"
let minus_fundef =
   let arg1 = new_symbol_string "minus_arg" in
   let arg2 = new_symbol_string "minus_arg" in
   let result = new_symbol_string "minus_result" in
   let int_int_case =
      let tmp_int = new_symbol_string "tmp_int" in
      let tmp_int1 = new_symbol_string "tmp_int" in
      let tmp_int2 = new_symbol_string "tmp_int" in
      get_int tmp_int1 (AtomVar arg1) (
      get_int tmp_int2 (AtomVar arg2) (
      LetBinop (tmp_int, TyInt, MinusIntOp, AtomVar tmp_int1, AtomVar tmp_int2,
      new_int result (AtomVar tmp_int) (
      Return (AtomVar result)))))
   in
   let else_case = raise_exception () in
   let minus_func_body =
      Match (AtomVar arg1,
         [int_tag_set,
            Match (AtomVar arg2,
               [int_tag_set, int_int_case;
                all_tag_set, else_case]);
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty; py_ty], py_ty) in
   minus_func, (bogus_loc "minus_func"), FunGlobalClass, ty, [arg1; arg2], minus_func_body

let or_func = new_symbol_string "or"
let or_fundef =
   let arg1 = new_symbol_string "or_arg" in
   let arg2 = new_symbol_string "or_arg" in
   let result = new_symbol_string "or_result" in
   let int_int_case =
      let tmp_int = new_symbol_string "tmp_int" in
      let tmp_int1 = new_symbol_string "tmp_int" in
      let tmp_int2 = new_symbol_string "tmp_int" in
      get_int tmp_int1 (AtomVar arg1) (
      get_int tmp_int2 (AtomVar arg2) (
      LetBinop (tmp_int, TyInt, OrIntOp, AtomVar tmp_int1, AtomVar tmp_int2,
      new_int result (AtomVar tmp_int) (
      Return (AtomVar result)))))
   in
   let else_case = raise_exception () in
   let or_func_body =
      Match (AtomVar arg1,
         [int_tag_set,
            Match (AtomVar arg2,
               [int_tag_set, int_int_case;
                all_tag_set, else_case]);
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty; py_ty], py_ty) in
   or_func, (bogus_loc "or_func"), FunGlobalClass, ty, [arg1; arg2], or_func_body

let coerce_bool_func = new_symbol_string "coerce_bool"
let coerce_bool_fundef =
   let arg = new_symbol_string "coerce_bool_arg" in
   let coerce_bool_body =
      let ans = new_symbol_string "coerced_bool" in
      Match (AtomVar arg,
         [int_tag_set,
            LetSubscript (block_poly_subop, ans, TyInt, arg, AtomInt 0,
            Return (AtomVar ans));
          all_tag_set, raise_exception ()])
   in
   let ty = TyFun ([py_ty], TyInt) in
   coerce_bool_func, (bogus_loc "coerce_bool_func"), FunGlobalClass, ty, [arg], coerce_bool_body

let print_func = new_symbol_string "print"
let print_fundef =
   let arg = new_symbol_string "print_arg" in
   let bogus = new_symbol_string "print_bogus" in

   (* The print_end local function ends each type case and just prints a
    * newline. *)
   let print_end_func = new_symbol_string "print_end" in
   let print_end_fundef =
      let print_end_body =
         LetExt (bogus, unit_ty, "ml_print_char", TyFun ([char_ty], unit_ty),
                 [newline_atom],
         Return unit_atom)
      in
      let ty = TyFun ([], unit_ty) in
      print_end_func, (bogus_loc "print_end_func"), FunLocalClass, ty, [], print_end_body
   in

   (* The integer case calls ml_print_int. *)
   let int_case =
      let tmp_int = new_symbol_string "tmp_int" in
      get_int tmp_int (AtomVar arg) (
      LetExt (bogus, unit_ty, "ml_print_int", TyFun ([TyInt], unit_ty),
              [AtomVar tmp_int],
      TailCall (print_end_func, [])))
   in

   (* The else case raises an exception. *)
   let else_case = raise_exception () in

   let print_body =
      LetFuns ([print_end_fundef],
      Match (AtomVar arg,
         [int_tag_set, int_case;
          all_tag_set, else_case]))
   in
   let ty = TyFun ([py_ty], unit_ty) in
   print_func, (bogus_loc "print_func"), FunGlobalClass, ty, [arg], print_body

let int_func = Symbol.add "int"
let int_fundef =
   let arg = new_symbol_string "int_arg" in
   let result = new_symbol_string "int_result" in
   let int_case = Return (AtomVar arg) in
   let string_case =
      let tmp_int = new_symbol_string "tmp_int" in
      let tmp_str = new_symbol_string "tmp_str" in
      LetSubscript (block_poly_subop, tmp_str, TyArray char_ty, arg, AtomInt 0,
      LetExt (tmp_int, TyInt, "ml_atoi", TyFun ([TyArray char_ty], TyInt), [AtomVar tmp_str],
      new_int result (AtomVar tmp_int) (
      Return (AtomVar result))))
   in
   let else_case = raise_exception () in
   let int_func_body =
      Match (AtomVar arg,
         [int_tag_set, int_case;
          string_tag_set, string_case;
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty], py_ty) in
   int_func, (bogus_loc "int_func"), FunGlobalClass, ty, [arg], int_func_body

(*let slice_func = new_symbol_string "slice"*)
(*let attrref_func = new_symbol_string "attrref"*)

let equal_func = new_symbol_string "equal"
let equal_fundef =
   let arg1 = new_symbol_string "equal_arg" in
   let arg2 = new_symbol_string "equal_arg" in
   let result = new_symbol_string "equal_result" in
   let int_int_case =
      let tmp_bool = new_symbol_string "tmp_bool" in
      let tmp_int1 = new_symbol_string "tmp_int" in
      let tmp_int2 = new_symbol_string "tmp_int" in
      get_int tmp_int1 (AtomVar arg1) (
      get_int tmp_int2 (AtomVar arg2) (
      LetBinop (tmp_bool, bool_ty, EqIntOp, AtomVar tmp_int1, AtomVar tmp_int2,
      Match (AtomVar tmp_bool,
         [bool_true_set, new_int result (AtomInt 1) (Return (AtomVar result));
          bool_false_set,
            new_int result (AtomInt 0) (Return (AtomVar result))]))))
   in
   let else_case = raise_exception () in
   let equal_func_body =
      Match (AtomVar arg1,
         [int_tag_set,
            Match (AtomVar arg2,
               [int_tag_set, int_int_case;
                all_tag_set, else_case]);
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty; py_ty], py_ty) in
   equal_func, (bogus_loc "equal_func"), FunGlobalClass, ty, [arg1; arg2], equal_func_body

let times_func = new_symbol_string "times"
let times_fundef =
   let arg1 = new_symbol_string "times_arg" in
   let arg2 = new_symbol_string "times_arg" in
   let result = new_symbol_string "times_result" in
   let int_int_case =
      let tmp_int = new_symbol_string "tmp_int" in
      let tmp_int1 = new_symbol_string "tmp_int" in
      let tmp_int2 = new_symbol_string "tmp_int" in
      get_int tmp_int1 (AtomVar arg1) (
      get_int tmp_int2 (AtomVar arg2) (
      LetBinop (tmp_int, TyInt, MulIntOp, AtomVar tmp_int1, AtomVar tmp_int2,
      new_int result (AtomVar tmp_int) (
      Return (AtomVar result)))))
   in
   let else_case = raise_exception () in
   let times_func_body =
      Match (AtomVar arg1,
         [int_tag_set,
            Match (AtomVar arg2,
               [int_tag_set, int_int_case;
                all_tag_set, else_case]);
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty; py_ty], py_ty) in
   times_func, (bogus_loc "times_func"), FunGlobalClass, ty, [arg1; arg2], times_func_body

let gt_func = new_symbol_string ">"
let gt_fundef =
   let arg1 = new_symbol_string ">_arg" in
   let arg2 = new_symbol_string ">_arg" in
   let result = new_symbol_string ">_result" in
   let int_int_case =
      let tmp_bool = new_symbol_string "tmp_bool" in
      let tmp_int1 = new_symbol_string "tmp_int" in
      let tmp_int2 = new_symbol_string "tmp_int" in
      get_int tmp_int1 (AtomVar arg1) (
      get_int tmp_int2 (AtomVar arg2) (
      LetBinop (tmp_bool, bool_ty, GtIntOp, AtomVar tmp_int1, AtomVar tmp_int2,
      Match (AtomVar tmp_bool,
         [bool_true_set,
            new_int result (AtomInt 1) (Return (AtomVar result));
          bool_false_set,
            new_int result (AtomInt 0) (Return (AtomVar result))]))))
   in
   let else_case = raise_exception () in
   let gt_func_body =
      Match (AtomVar arg1,
         [int_tag_set,
            Match (AtomVar arg2,
               [int_tag_set, int_int_case;
                all_tag_set, else_case]);
          all_tag_set, else_case])
   in
   let ty = TyFun ([py_ty; py_ty], py_ty) in
   gt_func, (bogus_loc ">_func"), FunGlobalClass, ty, [arg1; arg2], gt_func_body

let elt_func = new_symbol_string "elt"
let elt_fundef =
   let argi = new_symbol_string "elt_arg_i" in
   let argl = new_symbol_string "elt_arg_l" in
   let result = new_symbol_string "elt_result" in
   let rest = new_symbol_string "elt_rest" in
   let new_i = new_symbol_string "elt_new_i" in
   let local_func = new_symbol_string "elt_local" in
   let cons_case =
      Match (AtomVar argi,
             [zero_set,
                LetSubscript (block_poly_subop, result, py_ty, argl, AtomInt 0,
                Return (AtomVar result));
              max_set,
                LetSubscript (block_poly_subop, rest, py_list_ty, argl,
                              AtomInt 1,
                LetBinop (new_i, TyInt, MinusIntOp, AtomVar argi, AtomInt 1,
                TailCall (local_func, [AtomVar new_i; AtomVar rest])))])
   in
   let local_body =
      (* Check for the empty list. *)
      Match (AtomVar argl,
             [list_nil_set, raise_exception ();
              list_cons_set, cons_case;
              max_set, raise_exception ()
             ])
   in
   let ty = TyFun ([TyInt; py_list_ty], py_ty) in
   let body =
      LetFuns ([local_func, (bogus_loc ""), FunLocalClass, ty, [argi; argl], local_body],
      TailCall (local_func, [AtomVar argi; AtomVar argl]))
   in
   elt_func, (bogus_loc "elt"), FunGlobalClass, ty, [argi; argl], body


let runtime_funcs =
   [add_fundef;
    minus_fundef;
    or_fundef;
    coerce_bool_fundef;
    print_fundef;
    int_fundef;
    equal_fundef;
    times_fundef;
    gt_fundef;
    elt_fundef
   ]
