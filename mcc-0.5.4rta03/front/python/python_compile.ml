exception Handled

let print_prog s prog =
   if !Fir_state.debug_print_ir then begin
      Format.set_margin 78;
      Format.printf "\n*** %s:\n" s;
      Naml_ir_util.print_prog prog;
      Format.print_newline ()
   end

let compile name =
   let inx = open_in name in
   let lexbuf = Lexing.from_channel inx in
   Python_position.current_file := name;
   let stmts = Python_parse.file_input Python_lex.token lexbuf in
   if !Fir_state.debug_print_ast then
      List.iter Python_ast_print.print_stmt stmts;
   let prog = Python_ir_ast.make_prog stmts in
   print_prog "IR" prog;
   let prog = Naml_ir_cps.cps_prog prog in
   print_prog "CPS converted IR" prog;
   let prog = Naml_ir_close.close_prog prog in
   print_prog "Closed IR" prog;
   Naml_fir_ir.fir_prog prog
