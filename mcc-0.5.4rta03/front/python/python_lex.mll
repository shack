(* Lexer for Python
 * David Bustos
 *)

{
open Python_parse
open Python_position


(* Count brackets (and parenthesis and braces) for implicit line joining *)
let brackets_num = ref 0

(* Create an indentation stack *)
let indent_stack : int Stack.t = Stack.create ()
let () = Stack.push 0 indent_stack

(* Given a string, compute its length with tabs expanded. *)
let compute_indent str =
   let indent = ref 0 in
   for index = 0 to String.length str - 1 do
      match str.[index] with
	 '\t' -> indent := !indent land lnot 7 + 8
       | _ -> incr indent
   done;
   !indent

(* True when more DEDENT tokens may be necessary. *)
let dedenting = ref false
let current_indent = ref 0

(* True when at the beginning of a logical line *)
let at_bol = ref true

let unquote_string str = 
   let j = ref 0 in
      let len = String.length str in
      let i = ref 0 in
      while !i < len do
	 (match str.[!i] with
	    '\\' -> (	(* Escape sequence *)
	       incr i;
	       if !i < String.length str then
		  match str.[!i] with
		     '\n' -> ()		(* backslash-newline: ignore *)
		   | '\\' -> j := !j + 1
		   | '\'' -> str.[!j] <- '\'';	incr j
		   | '"' -> str.[!j] <- '"';	incr j
		   | 'a' -> str.[!j] <- '\007';	incr j
		   | 'b' -> str.[!j] <- '\b';	incr j
		   | 'f' -> str.[!j] <- '\014';	incr j
		   | 'n' -> str.[!j] <- '\n';	incr j
		   | 'r' -> str.[!j] <- '\r';	incr j
		   | 't' -> str.[!j] <- '\t';	incr j
		   | 'v' -> str.[!j] <- '\013';	incr j
		   | '0'..'7' ->	(* Octal escape: convert next three numbers *)
		     str.[!j] <- char_of_int (int_of_string ("0o" ^ (String.sub str !i 3)));
		     incr j;
		     i := !i + 2
		   | 'x' ->	
		     (* Hex escape: convert next two numbers.  XXX should be unlimited*)
		     str.[!j] <- char_of_int (int_of_string ("0" ^ (String.sub str !i 3)));
		     incr j;
		     i := !i + 2
		   | c -> incr j; str.[!j] <- c
	    )
	  | c -> str.[!j] <- c; incr j
	  );
	  incr i
      done;
      String.sub str 0 !j

(* Count the number of occurrences of c in str. *)
let string_count str c =
   let len = String.length str in
   let rec string_count_aux i cnt =
      if i < len then
         string_count_aux (i + 1) (cnt + (if str.[i] = c then 1 else 0))
      else
         cnt
   in
   string_count_aux 0 0


(*
 * Fatal lexer error.  Prints the given position, error string, and exits with
 * error code 1.
 *)
let fatal_error pos str =
   Python_position.print_pos pos;
   print_endline str;
   exit 1

} (**************************************************************************)

let digit = ['0'-'9']
let nonzerodigit = ['1'-'9']
let exponent = ('e'|'E') ('+'|'-')? digit+

let whitespace = [' ' '\t']*
let comment = '#' [^'\n']*

(*
 * The main parser.  This is stage one of two.  Here we deal with whitespace.
 * Then we jump to the second stage, token2.
 *)
rule token = parse
   (* First try to match whitespace at the end of the line. *)
   whitespace comment? '\n'
      {  let lexeme = Lexing.lexeme lexbuf in
         let pos = Python_position.set_position lexeme in
         if !at_bol then
            token lexbuf	(* Blank line: do nothing *)
	 else (
	    (* End of line.  Watch for implicit line joining. *)
	    at_bol := (!brackets_num = 0);
	    if !brackets_num = 0 then
               Newline pos
	    else
               token lexbuf
	 )
      }

   (* Next match whitespace at the beginning or in the middle of the line. *)
 | whitespace
      {  let lexeme = Lexing.lexeme lexbuf in
         let pos = Python_position.set_position lexeme in
         if !at_bol then
            begin
               (* handle indentation *)
               at_bol := false;
               let indent = compute_indent lexeme in
               if indent = Stack.top indent_stack then
                  (* No indentation change.  Continue. *)
                  token2 lexbuf
               else if indent > Stack.top indent_stack then
                  begin
                     (* More indentation.  Generate an INDENT token. *)
                     Stack.push indent indent_stack;
                     Indent pos
                  end
               else
                  (* indent < Stack.top indent_stack *)
                  begin
                     (* Less indentation.  Generate a DEDENT token, and set dedenting.  *)
                     dedenting := true;
                     current_indent := indent;	(* Remember the indentation level. *)
                     ignore (Stack.pop indent_stack);
                     Dedent pos
                  end
            end
         else
            (* not !at_bol *)
            begin
               if not !dedenting then
                  (* Just intraline whitespace.  Skip it and continue. *)
                  token2 lexbuf
               else
                  begin
                     (* Check to see if we've dedented enough. *)
                     if !current_indent = Stack.top indent_stack then
                        begin
                           (* Done dedenting.  Continue. *)
                           dedenting := false;
                           token2 lexbuf
                        end

                     else if !current_indent < Stack.top indent_stack then
                        begin
                           (* Keep dedenting. *)
                           ignore (Stack.pop indent_stack);
                           Dedent pos
                        end

                     else
                        (* if !current_indent > Stack.top indent_stack *)
                        (* Oops, inconsistent indentation. *)
                        raise Python_parse_util.InconsistentDedent
                  end
            end
      }

and token2 = parse

   (* Explicit line joining *)
   '\\' '\n'
      {  ignore (Python_position.set_position (Lexing.lexeme lexbuf));
         at_bol := false;
         token lexbuf
      }

   (* Keywords and identifiers *)
 | ['a'-'z' 'A'-'Z' '_']['a'-'z' 'A'-'Z' '_' '0'-'9']*
      {  at_bol := false;
	 let id = Lexing.lexeme lexbuf in
	 let pos = set_position id in
	 match id with
	  | "and" ->		And pos
	  | "assert" ->		Assert pos
	  | "break" ->		Break pos
	  | "class" ->		Class pos
	  | "continue" ->	Continue pos
	  | "def" ->		Def pos
	  | "del" ->		Del pos
	  | "elif" ->		Elif pos
	  | "else" ->		Else pos
	  | "except" ->		Except pos
	  | "exec" ->		Exec pos
	  | "finally" ->	Finally pos
	  | "for" ->		For pos
	  | "from" ->		From pos
	  | "global" ->		Global pos
	  | "if" ->		If pos
	  | "import" ->		Import pos
	  | "in" ->		In pos
	  | "is" ->		Is pos
	  | "lambda" ->		Lambda pos
	  | "not" ->		Not pos
	  | "or" ->		Or pos
	  | "pass" ->		Pass pos
	  | "print" ->		Print pos
	  | "raise" ->		Raise pos
	  | "return" ->		Return pos
	  | "try" ->		Try pos
	  | "while" ->		While pos
	  | _ ->		Identifier (Symbol.add id, pos)
      }


   (* Short strings *)
 | ('r'|'R')? '\'' ([^'\\''\n''''] | '\\' _)* '\''
 | ('r'|'R')? '"' ([^'\\''\n''"'] | '\\' _)* '"'
      {  at_bol := false;
	 let matched = Lexing.lexeme lexbuf in
	 let pos = Python_position.set_position matched in
	 let firstchar = matched.[0] in
	 if firstchar = 'r' || firstchar = 'R' then
	    (* Raw string: strip off the r and the quotes. *)
	    String (String.sub matched 2 (String.length matched - 3), pos)
	 else
	    (* Strip off the quotes. *)
	    let noquotes = String.sub matched 1 (String.length matched - 2) in
	    String ((unquote_string noquotes), pos)
      }

   (* Long (triple-quoted) strings *)
 | ('r'|'R')?			(* Raw? *)
   "'''"                        (* Tripple quote. *)
   ( [^'\\' '\'']		(* Unquoted char  /[^\\']/ *)
     | '\\' _			(* Quoted char  /\\./ *)
     | '\'' [^'\\' '\'']	(* Quote, unquoted char  /'[^\\']/ *)
     | '\'' '\\' _		(* Quote, quoted char  /'\\./ *)
     | '\'' '\'' [^'\\' '\'']	(* Quote, quote, unquoted char  /''[^\\']/ *)
     | '\'' '\'' '\\' _		(* Quote, quote, quoted char  /''\\./ *)
   )*
   "'''"                        (* Tripple quote. *)

 | ('r'|'R')?			(* Raw? *)
   "\"\"\""			(* Tripple double quote. *)
   ( [^'\\' '"']		(* Unquoted char *)
     | '\\' _			(* Quoted char *)
     | '"' [^'\\' '"']		(* Double quote, unquoted char *)
     | '"' '\\' _		(* Double quote, quoted char *)
     | '"' '"' [^'\\' '"']	(* D quote, d quote, unquoted char *)
     | '"' '"' '\\' _		(* D quote, d quote, quoted char *)
   )*
   "\"\"\""			(* Tripple double quote. *)

      {  at_bol := false;
	 let matched = Lexing.lexeme lexbuf in
	 let pos = set_position matched in
	 let firstchar = matched.[0] in
	 if firstchar = 'r' || firstchar = 'R' then
	    (* Raw string: strip off the r and the quotes *)
	    String (String.sub matched 4 (String.length matched - 7), pos)
	 else
	    (* Strip off the quotes. *)
	    let noquotes = String.sub matched 3 (String.length matched - 6) in
	    (* And then unquote the string. *)
	    String ((unquote_string noquotes), pos)
      }

   (* Integer (and long integer) literals *)
 | '0' ('l'|'L')?
 | nonzerodigit digit* ('l'|'L')?
 | '0' ('x'|'X') ['0'-'9''a'-'f''A'-'F']* ('l'|'L')?
      {  at_bol := false;
	 let matched = Lexing.lexeme lexbuf in
	 let pos = set_position matched in
	 let lastchar = matched.[String.length matched - 1] in
	 if lastchar = 'l' || lastchar = 'L' then
	    (* Strip off the L *)
	    Longinteger (String.sub matched 0 (String.length matched - 1), pos)
	 else
	    Integer (int_of_string matched, pos)
      }
 | '0' ['0'-'7']* ('l'|'L')?
      {  at_bol := false;
	 let matched = Lexing.lexeme lexbuf in
	 let pos = set_position matched in
	 let lastchar = matched.[String.length matched - 1] in
	 if lastchar = 'l' || lastchar = 'L' then
	    (* Strip off the L *)
	    Longinteger (String.sub matched 0 (String.length matched - 1), pos)
	 else
	    Integer (int_of_string ( "0o" ^ matched), pos)
      }

   (* Float literals *)
 | (nonzerodigit digit*|'0')? '.' digit+ exponent?
 | (nonzerodigit digit*|'0') '.' exponent?
 |  nonzerodigit digit* exponent
      {  at_bol := false;
         let lexeme = Lexing.lexeme lexbuf in
         let pos = set_position lexeme in
	 Float (float_of_string lexeme, pos)
      }

   (* Imaginary literals (patterns copied from floats) *)
 | (nonzerodigit digit*|'0')? '.' digit+ exponent? ('j'|'J')
 | (nonzerodigit digit*|'0') '.' exponent? ('j'|'J')
 |  nonzerodigit digit* exponent ('j'|'J')
 | (nonzerodigit digit*|'0') ('j'|'J')
      {  at_bol := false;
         let lexeme = Lexing.lexeme lexbuf in
         let pos = set_position lexeme in
	 Imaginary (float_of_string lexeme, pos)
      }

   (* Operators *)
 | '+' | '-' | '*' | "**" | '/' | '%' | "<<" | ">>" | '&' | '|' | '^' | '~'
 | '<' | '>' | "<=" | ">=" | "==" | "!=" | "<>"
      {  at_bol := false;
	 let id = Lexing.lexeme lexbuf in
	 let pos = set_position id in
	 match id with
	  | "+" ->		Plus pos
	  | "-" ->		Dash pos
	  | "*" ->		Star pos
	  | "**" ->		StarStar pos
	  | "/" ->		Slash pos
	  | "%" ->		Percent pos
	  | "<<" ->		Leftshift pos
	  | ">>" ->		Rightshift pos
	  | "&" ->		Ampersand pos
	  | "|" ->		Bar pos
	  | "^" ->		Caret pos
	  | "~" ->		Tilde pos
	  | "<" ->		Lessthan pos
	  | ">" ->		Greaterthan pos
	  | "<=" ->		LessthanEq pos
	  | ">=" ->		GreaterthanEq pos
	  | "==" ->		EqualEqual pos
	  | "!=" | "<>" ->	NotEqual pos
	  | _ ->
               let str =
                  Printf.sprintf
                     "InternalLexError: unexpected delimiter `%s'"
                     id
               in
               fatal_error pos str
      }

   (* Delimiters *)
   (* First open delimiters, so we can increase brackets_num *)
 | '(' | '[' | '{'
      {  at_bol := false;
	 incr brackets_num;
	 let id = Lexing.lexeme lexbuf in
	 let pos = set_position id in
	 match id with
	  | "(" ->	Leftparen pos
	  | "[" ->	Leftbracket pos
	  | "{" ->	Leftbrace pos
	  | _ ->
               let str =
                  Printf.sprintf
                     "InternalLexError: unexpected delimiter `%s'"
                     id
               in
               fatal_error pos str
      }

   (* Now close delimiters, so we can decrease brackets_num *)
 | ')' | ']' | '}'
      {  at_bol := false;
	 decr brackets_num;
	 let id = Lexing.lexeme lexbuf in
	 let pos = set_position id in
	 match id with
	  | ")" ->	Rightparen pos
	  | "]" ->	Rightbracket pos
	  | "}" ->	Rightbrace pos
	  | _ ->
               let str =
                  Printf.sprintf
                     "InternalLexError: unexpected delimiter `%s'"
                     id
               in
               fatal_error pos str
      }

 | ',' | ':' | '.' | '`' | '=' | ';' | "+=" | "-=" | "*=" | "/=" | "%="
 | "**=" | "&=" | "|=" | "^=" | ">>=" | "<<="
      {  at_bol := false;
	 let id = Lexing.lexeme lexbuf in
	 let pos = set_position id in
	 match id with
	  | "," ->	Comma pos
	  | ":" ->	Colon pos
	  | "." ->	Period pos
	  | "`" ->	Backquote pos
	  | "=" ->	Equal pos
	  | ";" ->	Semicolon pos
	  | "+=" ->	PlusEqual pos
	  | "-=" ->	DashEqual pos
	  | "*=" ->	StarEqual pos
	  | "/=" ->	SlashEqual pos
	  | "%=" ->	PercentEqual pos
	  | "**=" ->	StarStarEqual pos
	  | "&=" ->	AmpersandEqual pos
	  | "|=" ->	BarEqual pos
	  | "^=" ->	CaretEqual pos
	  | ">>=" ->	LeftshiftEqual pos
	  | "<<=" ->	RightshiftEqual pos
	  | _ ->
               let str =
                  Printf.sprintf "InternalLexError: unexpected delimiter `%s'" id
               in
               fatal_error pos str
      }

   (* Other stuff *)
 | "..."
      {  at_bol := false;
         let lexeme = Lexing.lexeme lexbuf in
         Ellipsis (set_position lexeme)
      }

   (* End of file *)
 | eof
      {  let pos = set_position (Lexing.lexeme lexbuf) in
         EOF pos
      }

 | _
      {  let lexeme = Lexing.lexeme lexbuf in
         let pos = set_position lexeme in
	 let escaped = String.escaped lexeme in
	 let error_str =
            Printf.sprintf "SyntaxError: illegal character '%s'" escaped
         in
         fatal_error pos error_str
      }
