(* Types ********************************************************************)

(* The Python type and the sets needed for Match's. *)
val py_ty : Fir.ty
val py_tydef_var : Fir.ty_var
val py_tydef : Fir.tydef
val python_int_tag : int
val python_float_tag : int
val python_complex_tag : int
val python_nil_tag : int
val python_cons_tag : int
val int_tag_set : Fir.set
val string_tag_set : Fir.set
val list_nil_set : Fir.set
val list_cons_set : Fir.set
val all_tag_set : Fir.set

val py_list_ty : Fir.ty

(* Miscellaneous types. *)
val bool_ty : Fir.ty
val bool_true_set : Fir.set
val bool_false_set : Fir.set

val unit_ty : Fir.ty
val char_ty : Fir.ty


val newline_atom : Fir.atom
val unit_atom : Fir.atom


val block_poly_subop : Fir.subop


(* Utility Functions ********************************************************)

val get_int : Fir.var -> Fir.atom -> Naml_ir.exp -> Naml_ir.exp
val raise_exception : unit -> Naml_ir.exp
val new_int : Fir.var -> Fir.atom -> Naml_ir.exp -> Naml_ir.exp


val zero_set : Fir.set
val max_set : Fir.set
