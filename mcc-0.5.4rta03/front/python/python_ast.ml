(* Abstract syntax for Python
 * David Bustos
 *)

open Symbol

(* Position: filename, starting {line, character}, ending {line, character} *)
type pos = string * int * int * int * int

type literal =
   StrLiteral		of string * pos
 | IntLiteral		of int * pos
 | LongIntLiteral	of string * pos
 | FloatLiteral		of float * pos
 | ImagLiteral		of float * pos

type expr =
   IdExpr		of symbol * pos
 | LiteralExpr		of literal * pos
 | TupleExpr		of expr list * pos
 | ListExpr		of listtype * pos
 | DictExpr		of (expr * expr) list * pos
 | StrConvExpr		of expr list * pos
 | AttrRefExpr		of expr * symbol * pos
 | SliceExpr		of expr * sliceitem list * pos
 | CallExpr		of expr * argument list * pos
 | OpExpr		of expr * operator * expr * pos
 | FuncExpr		of parameter list * stmt list * pos

and parameter =
   IdParam		of symbol * pos
 | SublistParam		of parameter list * pos
 | DefParam		of parameter * expr * pos
 | ExtraPosParam	of symbol * pos
 | ExtraKwParam		of symbol * pos

and operator =
   StarStar | Dash | Plus | Tilde | Star | Slash | Percent | Leftshift
 | Rightshift | Ampersand | Caret | Bar | Lessthan | Greaterthan | EqualEqual
 | GreaterthanEq | LessthanEq | NotEqual | Is | IsNot | In | NotIn | Or | And
 | Not | PlusEqual | DashEqual | StarEqual | SlashEqual | PercentEqual
 | StarStarEqual | RightshiftEqual | LeftshiftEqual | AmpersandEqual
 | CaretEqual | BarEqual

and argument =
   PosArg		of expr * pos
 | KeywordArg		of symbol * expr * pos
 | StarArg              of expr * pos
 | StarStarArg          of expr * pos

and sliceitem =
   ExprSlice		of expr
 | SimpleSlice		of expr * expr
 | ExtSlice		of expr list

and listtype =
   List			of expr list
 | ListComp		of expr * stmt

and target = expr

and stmt =
   ExprList		of expr list * pos
 | AssertStmt		of expr * expr option * pos
 | AssignStmt		of (target list) list * expr list * pos
 | AugAssignStmt	of target * operator * expr list * pos
 | PassStmt		of pos
 | DelStmt		of target list * pos
 | PrintStmt		of expr * expr list * pos
 | ReturnStmt		of expr * pos
 | RaiseStmt		of expr * expr * expr * pos
 | BreakStmt		of pos
 | ContStmt		of pos
 | ImportStmt		of symbol list * (symbol list * symbol) list * pos
 | GlobalStmt		of symbol list * pos
 | StmtList		of stmt list * pos

(* Compound statements *)
 | IfStmt		of expr * stmt list * stmt list * pos
 | WhileStmt		of expr * stmt list * stmt list * pos
 | ForStmt		of target list * expr list * stmt list * stmt list * pos
 | TryStmt		of stmt list * (expr * expr * stmt list) list * stmt list * pos
 | FuncDef		of symbol * parameter list * stmt list * pos
 | ClassDef		of symbol * expr list * stmt list * pos
