open Python_ast

exception Impossible

let current_indent = ref 0

let indent () = current_indent := !current_indent + 4
let dedent () = current_indent := !current_indent - 4
let print_indent () = print_string (String.make !current_indent ' ')

let print_symbol s = Symbol.pp_print_symbol Format.std_formatter s; Format.print_flush ()


let print_literal = function
   StrLiteral(s,_)	->
      print_char '\''; print_string s; print_char '\''

 | IntLiteral(i,_)	-> print_int i
 | LongIntLiteral(s,_)	-> print_string s
 | FloatLiteral(f,_)	-> print_float f
 | ImagLiteral(f,_)	-> print_float f; print_char 'j'


let print_operator o = print_string (match o with
   StarStar	-> "**"
 | Dash		-> "-"
 | Plus		-> "+"
 | Tilde	-> "~"
 | Star		-> "*"
 | Slash	-> "/"
 | Percent	-> "%"
 | Leftshift	-> "<<"
 | Rightshift	-> ">>"
 | Ampersand	-> "&"
 | Caret	-> "^"
 | Bar		-> "|"
 | Lessthan	-> "<"
 | Greaterthan	-> ">"
 | EqualEqual	-> "=="
 | GreaterthanEq -> ">="
 | LessthanEq	-> "<="
 | NotEqual	-> "!="
 | Is		-> "is"
 | IsNot	-> "is not"
 | In		-> "in"
 | NotIn	-> "not in"
 | Or		-> "or"
 | And		-> "and"
 | Not		-> "not"
 | PlusEqual	-> "+="
 | DashEqual	-> "-="
 | StarEqual	-> "*="
 | SlashEqual	-> "/="
 | PercentEqual	-> "%="
 | StarStarEqual -> "**="
 | RightshiftEqual -> ">>="
 | LeftshiftEqual -> "<<="
 | AmpersandEqual -> "&="
 | CaretEqual	-> "^="
 | BarEqual	-> "|="
 )


(*
let rec print_expr_list el =
   print_expr (List.hd el);
   let print_comma_expr = print_string ", "; print_expr in
   ignore (List.map print_comma_expr (List.tl el))
*)

let rec print_expr_list = function
   []	-> ()
 | [e]	-> print_expr e
 | h::t	-> print_expr h; print_string ", "; print_expr_list t

and print_expr_list_brak b1 b2 el =
   print_string b1;
   print_expr_list el;
   print_string b2

and print_expr = function
   IdExpr(s,_)		-> print_symbol s
 | LiteralExpr(l,_)	-> print_literal l
 | TupleExpr(el,_)	-> print_string "Tuple: "; print_expr_list_brak "(" ")" el
 | ListExpr(List(el),_)	-> print_expr_list_brak "[" "]" el
 | ListExpr(_,_)	-> print_string "[List comprehension]"
 | DictExpr(eel,_)	->
      print_string "{";
      let rec print_keyval_list = function
	 []	-> ()
       | [(e1,e2)]	-> print_expr e1; print_string ":"; print_expr e2
       | (e1,e2)::tl	->
	    print_expr e1;
	    print_string ":";
	    print_expr e2;
	    print_string ", ";
	    print_keyval_list tl
      in
      print_keyval_list eel;
      print_string "}"

 | StrConvExpr(el,_)	-> print_expr_list_brak "`" "`" el
 | AttrRefExpr(e,s,_)	->
      print_expr e; print_string "."; print_symbol s

 | SliceExpr(e,sil,_)	->
      print_expr e;
      print_string "[";
      let print_sliceitem = function
	 ExprSlice(e)		-> print_expr e
       | SimpleSlice(e1,e2)	->
	    print_expr e1; print_string ":"; print_expr e2
       | ExtSlice(el)		-> print_expr_list el
      in
      ignore (List.map print_sliceitem sil);
      print_string "]"

 | CallExpr(e,al,_)	->
      print_expr e;
      print_string "(";
      let print_argument = function
	 PosArg(e,_)		-> print_expr e
       | KeywordArg(s,e,_)	->
	    print_symbol s;
	    print_char '='; print_expr e
       | StarArg(e,_)           -> print_char '*'; print_expr e
       | StarStarArg(e,_)       -> print_string "**"; print_expr e
      in
      let rec print_arg_list = function
	 []	-> ()
       | [a]	-> print_argument a
       | a::tl	-> print_argument a; print_string ", "; print_arg_list tl
      in
      print_arg_list al;
      print_string ")"

 | OpExpr(l,o,r,_)	->
      print_char '(';
      print_expr l;
      print_char ' ';
      print_operator o;
      print_char ' ';
      print_expr r;
      print_char ')'

 | FuncExpr(_,_,_)	-> print_string "lambda"


let rec print_stmt = function
   ExprList(el,_)	->
      print_string "ExprList(";
      print_expr_list el;
      print_string ")"
 | AssertStmt(e,None,_)	-> print_string "assert "; print_expr e
 | AssertStmt(e1,Some e2,_) ->
      print_string "assert ";
      print_expr e1;
      print_string ", ";
      print_expr e2

 | AssignStmt(tll,el,_)	->
      let print_target_list = print_expr_list in
      let print_tl_equal tl = print_target_list tl; print_string " = " in
      ignore (List.map print_tl_equal tll);
      print_expr_list el

 | AugAssignStmt(t,o,el,_) ->
      let print_target = print_expr in
      print_expr t;
      print_string " ";
      print_operator o;
      print_string " ";
      print_expr_list el

 | PassStmt(_) ->	print_string "pass"

 | DelStmt(tl,_) ->
      print_string "del ";
      let print_target_list = print_expr_list in
      print_target_list tl

 | PrintStmt(e,el,_) ->
      print_string "print ";
      if match e with
	    IdExpr(s,_)	-> (Symbol.to_string s) <> "None"
	  | _ -> true
      then (
	 print_string ">> ";
	 print_expr e;
	 print_string ", "
      );
      print_expr_list el

 | ReturnStmt(e,_) ->	print_string "return "; print_expr e

 | RaiseStmt(e1,e2,e3,_) ->
      print_string "raise";
      if match e1 with
	    IdExpr(s,_) -> (Symbol.to_string s) <> "None"
	  | _ -> true
      then (
	 print_string " ";
	 print_expr e1;
	 if match e2 with
	       IdExpr(s,_) -> (Symbol.to_string s) <> "None"
	     | _ -> true
	 then (
	    print_string ", ";
	    print_expr e2;
	    if match e3 with
		  IdExpr(s,_) -> (Symbol.to_string s) <> "None"
		| _ -> true
	    then (
	       print_string ", ";
	       print_expr e3
	    )
	 )
      )

 | BreakStmt(_)		-> print_string "break"
 | ContStmt(_)		-> print_string "cont"

 | ImportStmt([],slsl,_) ->
      print_string "import ";
      let rec print_module_list = function
	    []		-> ()
	  | [m,s]	->
	       print_module_name m;
	       print_string " as ";
	       print_symbol s
	  | m::tl	->
	       print_module_list [m];
	       print_string ", ";
	       print_module_list tl
      in
      print_module_list slsl

 | ImportStmt(m,[],_) ->
      print_string "from ";
      print_module_name m;
      print_string " import *"

 | ImportStmt(m,syms,_) ->
      print_string "from ";
      print_module_name m;
      print_string " import ";
      let rec print_symbol_list = function
	    []	-> ()
	  | [s1,s2]	->
	       print_module_name s1;
	       print_string " as ";
	       print_symbol s2
	  | s::tl	->
	       print_symbol_list [s];
	       print_string ", ";
	       print_symbol_list tl
      in
      print_symbol_list syms

 | GlobalStmt(sl,_) ->
      print_string "global ";
      let rec print_symbol_list = function
	 []	-> ()
       | [s]	-> print_symbol s
       | s::tl	-> print_symbol_list [s]; print_string ", "; print_symbol_list tl
      in
      print_symbol_list sl

 | StmtList(sl,_) ->
      let rec print_stmt_list = function
	 []	-> ()
       | [s]	-> print_stmt s
       | s::tl	->
	    print_stmt s;
	    print_string "; ";
	    print_stmt_list tl
      in
      print_stmt_list sl

 | IfStmt(e,thensl,elsesl,_) ->
      print_string "if ";
      print_expr e;
      print_endline ":";
      indent (); print_indent ();
      print_suite thensl;
      print_newline ();
      dedent (); print_indent ();
      print_endline "else:";
      indent (); print_indent ();
      print_suite elsesl;
      dedent (); print_indent ()

 | WhileStmt(e,bodysl,elsesl,_) ->
      print_string "while ";
      print_expr e;
      print_endline ":";
      indent (); print_indent ();
      print_suite bodysl;
      print_newline ();
      dedent ();
      if List.length elsesl > 0 then begin
         print_indent ();
         print_endline "else:";
         indent (); print_indent ();
         print_suite elsesl;
         dedent (); print_indent ()
      end

 | ForStmt(tl,el,bodysl,elsesl,_) ->
      print_string "for ";
      print_expr_list tl;
      print_string " in ";
      print_expr_list el;
      print_string ":";
      print_newline (); indent (); print_indent ();
      print_suite bodysl;
      print_newline (); dedent (); print_indent ();
      print_string "else:";
      print_newline (); indent (); print_indent ();
      print_suite elsesl;
      print_newline (); dedent (); print_indent ()

 | TryStmt(sl,clauses,finally,_) ->
      print_string "try:";
      print_newline (); indent (); print_indent ();
      print_suite sl;
      print_newline (); dedent (); print_indent ();
      let print_clause = function
	    e,t,s ->
	       print_string "except ";
	       print_expr e;
	       print_string ", ";
	       print_expr t;
	       print_string ":";
	       print_newline (); indent (); print_indent ();
	       print_suite s;
	       print_newline (); dedent (); print_indent ()
      in
      ignore (List.map print_clause clauses);
      print_string "else:";
      print_newline (); indent (); print_indent ();
      print_suite finally;
      print_newline (); dedent (); print_indent ()

 | FuncDef(name,params,body,_) ->
      print_string "def ";
      print_symbol name;
      print_char '(';
      let rec print_param_list = function
	    []		-> ()
	  | [p]		-> print_parameter p
	  | p::tl	->
	       print_parameter p;
	       print_string ", ";
	       print_param_list tl
      in
      print_param_list params;
      print_string "):";
      print_newline (); indent (); print_indent ();
      print_suite body;
      print_newline (); dedent (); print_indent ()

 | ClassDef(name,bases,body,_) ->
      print_string "class ";
      print_symbol name;
      print_char '('; print_expr_list bases; print_string "):";
      print_newline (); indent (); print_indent ();
      print_suite body;
      print_newline (); dedent (); print_indent ()

and print_suite = function
      []	-> ()
    | [s]	-> print_stmt s
    | s::tl	->
	 print_stmt s;
	 print_newline ();
	 print_indent ();
	 print_suite tl

and print_parameter = function
      IdParam(sym,_)		-> print_symbol sym

    | SublistParam(pl,_)	->
	 print_char '(';
	 let rec print_param_list = function
	       []	-> ()
	     | [p]	-> print_parameter p
	     | p::tl	->
		  print_parameter p;
		  print_string ", ";
		  print_param_list tl
	 in
	 print_param_list pl;
	 print_char ')'

    | DefParam(p,e,_)		->
	 print_parameter p;
	 print_char '=';
	 print_expr e

    | ExtraPosParam(s,_)	-> print_char '*'; print_symbol s
    | ExtraKwParam(s,_)		-> print_string "**"; print_symbol s

and print_module_name = function
      []	-> ()
    | [s]	-> print_symbol s
    | s::tl	-> print_symbol s; print_char '.'; print_module_name tl
