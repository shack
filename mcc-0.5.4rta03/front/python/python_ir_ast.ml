(* Convert Python AST to IR
 *
 * David Bustos
 *)
open Format

open Python_ast
open Python_ir
open Fir
open Symbol
open Naml_ir
open Fir_set (* For IntSet *)
open Location

exception ArgumentError
exception Impossible
exception NotImplemented of string * Python_position.pos
exception StringError of string

type env =
   {  env_funcs: symbol list SymbolTable.t;
      env_imports: SymbolSet.t
   }

let env_add_func env f params =
   let new_funcs = SymbolTable.add env.env_funcs f params in
   { env with env_funcs = new_funcs }

let env_lookup_fun env f =
   try
      SymbolTable.find env.env_funcs f
   with
      Not_found ->
         raise (StringError ("Unbound function " ^ (to_string f) ^ "."))

(*
 * Get the symbol out of an AtomVar.
 *)
let sym_of_atom = function
   AtomVar s	-> s
 | _		-> raise Impossible

(*
 * Run a function on each character of a string, and return a list of the
 * return values.
 *)
let string_map f s =
   let rec string_map_helper i results =
      if i < 0 then
         results
      else
         string_map_helper (i - 1) ((f s.[i]) :: results)
   in
   string_map_helper ((String.length s) - 1) []

(*
 * Given a Python-AST operator, returns the symbol of the corresponding runtime
 * function.
 *)
let func_of_op op pos =
   match op with
      EqualEqual  -> Python_runtime.equal_func
    | Plus        -> Python_runtime.add_func
    | Dash        -> Python_runtime.minus_func
    | Or          -> Python_runtime.or_func
    | Star        -> Python_runtime.times_func
    | Greaterthan -> Python_runtime.gt_func
    | _           -> raise (NotImplemented ("func_of_op", pos))


(*
 * Gives the alloc for the box for the given literal type.
 *)
let alloc_of_literal = function
   IntLiteral (i, _) ->
      AllocUnion ([], py_ty, py_tydef_var, python_int_tag, [AtomInt i])

 | LongIntLiteral (s, p) -> raise (NotImplemented ("LongIntLiteral", p))

 | FloatLiteral (f, _) ->
      AllocUnion ([], py_ty, py_tydef_var, python_float_tag,
                  [AtomFloat (Rawfloat.of_float Rawfloat.Single f)])

 | ImagLiteral (f, _) ->
      AllocUnion ([], py_ty, py_tydef_var, python_complex_tag,
                  [AtomFloat (Rawfloat.of_int Rawfloat.Single 0);
                   AtomFloat (Rawfloat.of_float Rawfloat.Single f)])

 | StrLiteral (s, _) ->
      let strsym = new_symbol_string "string" in
      let al_no_null = string_map (fun c -> AtomEnum (256, int_of_char c)) s in
      let al = al_no_null @ [AtomEnum (256, 0)] in
         AllocArray (Naml_ir_misc.type_string, al)

let int_of_atom a c =
   match a with
      AtomInt _ -> c a

    | AtomVar _ ->
         let v = new_symbol_string "bool-of-atom" in
         LetApply (v, TyInt, Python_runtime.coerce_bool_func, [a], c (AtomVar v))

    | _ ->
         raise (NotImplemented ("int_of_atom", ("", 0, 0, 0, 0)))

(*
 * Convert a Python-AST expression to a naML-IR expression.
 *
 * \param env	The current environment.
 * \param e	The Python-AST expression.
 * \param c	The continuation.  (env -> atom -> exp)
 * \returns	The naML-IR expression.  (exp)
 *)
let rec build_exp env e c =
   match e with
      (* Identifier: Use the corresponding symbol. *)
    | IdExpr (s, _)	-> c env (AtomVar s)

    | LiteralExpr (lit, _) ->
         let litsym = new_symbol_string "literal" in
         LetAlloc (litsym, alloc_of_literal lit, c env (AtomVar litsym))

    | TupleExpr (el, _)	->
	 build_exp_list env el (fun env al ->
         let rec make_list len elt =
            if len == 0 then
               []
            else
               (elt, Mutable) :: make_list (len - 1) elt
         in
	 let tuplesym = new_symbol_string "tuple" in
         let tupletype = TyTuple (NormalTuple, make_list (List.length al) py_ty) in
         LetAlloc (tuplesym, AllocTuple (NormalTuple, [], tupletype, al),
                   c env (AtomVar tuplesym)))

    | ListExpr (List el, _) ->
         let rec make_list l list_cont =
            match l with
               [] ->
                  let listsym = new_symbol_string "list" in
                  LetAlloc (listsym, AllocUnion ([], py_ty, py_tydef_var,
                                                 python_nil_tag, []),
                  (*LetUnop (listsym, py_ty, IdOp,
                           AtomConst (py_ty, py_tydef_var,
                                      python_nil_tag),*)
                  (*list_cont (AtomConst (py_ty, py_tydef_var,
                   * python_nil_tag))*)
                  list_cont (AtomVar listsym))

             | a :: rest ->
                  make_list rest (fun l ->
                  let listsym = new_symbol_string "list" in
                  LetAlloc (listsym, AllocUnion ([], py_ty, py_tydef_var,
                                                 python_cons_tag, [a; l]),
                  list_cont (AtomVar listsym)))
         in
         build_exp_list env el (fun env al -> make_list al (fun l -> c env l))


(*    | ListExpr (ListComp (e, st), _) -> *)

(*    | DictExpr (eel, _) ->
	 if eel == []
	 then c (AtomCons (py_ty, 0))
	 else
*)

    | OpExpr (left, op, right, p) ->
	 build_exp env left (fun env left_a ->
	 build_exp env right (fun env right_a ->
	 let result = new_symbol_string "op" in
         LetApply (result, py_ty, func_of_op op p, [left_a; right_a],
         c env (AtomVar result))))

    | CallExpr (e, args, p) ->
         let f =
            match e with
               IdExpr (v, _) -> v
             | _ -> raise (NotImplemented ("CallExpr", p))
         in
	 make_arg_list env f args (fun env al ->
	 let result = new_symbol_string "apply" in
	 LetApply (result, py_ty, f, al,
         c env (AtomVar result)))

    | SliceExpr (e, sil, p) ->
         build_exp env e (fun env a ->
         let cons_case =
            match sil with
               [ExprSlice e] ->
                  build_exp env e (fun env py_i ->
                  let int_case =
                     let i = new_symbol_string "i" in
                     let elt = new_symbol_string "elt" in
                     get_int i py_i (
                     LetApply (elt, py_ty, Python_runtime.elt_func,
                               [AtomVar i; a],
                     c env (AtomVar elt)))
                  in
                  Match (py_i, [int_tag_set, int_case;
                                all_tag_set, raise_exception ()])
                  )

             | _ ->
                  raise (NotImplemented ("SliceExpr", p))
         in
         Match (a, [list_cons_set, cons_case; max_set, raise_exception ()])
         )

    | AttrRefExpr (e, s, p) ->
         if (to_string s) = "argv" then
            c env (AtomVar s)
         else
            raise (NotImplemented ("AttrRefExpr", p))
            (*
         build_exp env e (fun env a ->
         let v = new_symbol_string "attrref" in
         LetApply (v, py_ty, Python_runtime.attrref_func, [a; AtomVar s],
         c env (AtomVar v)))
         *)

    | e -> raise (NotImplemented ("build_exp", Python_ast_util.pos_of_expr e))


and build_exp_list env el c =
   match el with
      [] -> c env []
    | e::el ->
         build_exp env e
	    (fun env a -> build_exp_list env el (fun env al -> c env (a::al)))


(*
 * Given a list of atoms, create an expression which puts the arguments in the
 * right order.
 *)
and make_arg_list env func args c =
   (* Get the function's formal parameter list. *)
   let fparams = env_lookup_fun env func in
   (* Allocate an array to put the actual parameters into. *)
   let params = Array.make (List.length fparams) None in
   process_position_args env args params 0 fparams c

(*
 * Process positional arguments.
 *
 * \param env	  Environment.
 * \param args	  List of remaining arguments to process.
 * \param params  Array of actual parameters.
 * \param filled  Number of slots already filled.
 * \param fparams Formal parameters. (symbol list)
 * \param c	  Continuation: atom list -> ty
 * \returns	  A Fir expression.  (type ty)
 *)
and process_position_args env args params filled fparams c =
   match args with
      (* End of list.  Pass the atoms to the continuation and return. *)
      []	->
	 (* De-optionalize the actual parameters.  Complain if any are empty. *)
	 let rec un_option = function
	       []		-> []
	     | None :: _	-> raise ArgumentError
	     | Some(x) :: r	-> x :: (un_option r)
	 in
	 c env (un_option (Array.to_list params))

      (* Positional argument.  Put it into the array. *)
    | PosArg(e,_) :: rest	->
	 build_exp env e (fun env var ->	(* Evaluate the expression *)
	 (* Have the variable.  Put into the next params slot. *)
	 params.(filled) <- Some var;
	 (* Return the result of processing the rest of the args. *)
	 process_position_args env rest params (filled + 1) fparams c)

      (* Keyword argument.  Jump to process_keyword_args. *)
    | KeywordArg(_,_,_) :: _	->
	 process_keyword_args env args params fparams c

    | StarArg(_,_) :: rest | StarStarArg(_,_) :: rest   ->
         process_position_args env rest params filled fparams c

(*
 * Process keyword arguments.
 *
 * \param env	  Environment.
 * \param args	  List of remaining arguments.
 * \param params  Array of actual parameters.  (ty option array)
 * \param fparams Formal parameters.  (symbol list)
 * \param c	  Continuation: atom list -> ty
 * \returns	  A Fir expression.  (type ty)
 *)
and process_keyword_args env args params fparams c =
   match args with
      (* End of list.  As above. *)
      []	->
	 let rec un_option = function
	       []		-> []
	     | None :: _	-> raise ArgumentError
	     | Some(x) :: r	-> x :: (un_option r)
	 in
	 c env (un_option (Array.to_list params))

      (* Oops, can't have positional arguments after keyword arguments. *)
    | PosArg(_,_) :: _ | StarArg(_,_) :: _ | StarStarArg(_,_) :: _      ->
         raise ArgumentError

      (* Keyword argument.  Transform it and put it in the proper param slot. *)
    | KeywordArg(kw,e,_) :: rest	->
	 build_exp env e (fun env var ->
	 (* var is the variable the argument is in. *)

	 (* Run through the formal parameters and look for kw. *)
         let rec place_argument fparams pos params kw v =
            match fparams with
               (* No match.  Error! *)
               []	-> raise ArgumentError
             | fkw :: rest	->
                  if Symbol.eq fkw kw
                  (* Match!  Place the argument and return. *)
                  then (params.(pos) <- Some v; params)
                  (* No match.  Keep trying. *)
                  else place_argument rest (pos + 1) params kw v
         in
         let new_params = place_argument fparams 0 params kw var in
         process_keyword_args env rest new_params fparams c)


(*
 * Make sym available.
 *
 * \param env  Environment.
 * \param sym  Symbol to import.
 * \returns    A new environment.
 *)
let do_import env sym =
   let new_imports = SymbolSet.add env.env_imports sym in
   { env with env_imports = new_imports }


let rec build_stmt env stmt c =
   match stmt with
      ExprList (el, p)  ->
         (* An expression is just an expression.  Expressions in a
          * comma-delimited list creates a tuple, however.  Note that this
          * isn't fully correct, since a single expression with a comma should
          * create a tuple.  But that needs to be fixed in the parser, I
          * think.
          *)
         build_exp env
            (match el with
               [exp] -> exp
             | _ -> TupleExpr (el, p))
            (fun env _ -> c env)

(*    | AssertStmt (e, exception_e, p) -> *)

    | AssignStmt (tll, el, p) -> begin
         match tll with
            [[IdExpr (s, p)]] -> begin
               match el with
                  [expr] ->
                     build_exp env expr (fun env a ->
                     LetAtom (s, py_ty, a,
                     c env))

                | _ -> raise (NotImplemented ("AssignStmt 1", p))

               end

          | _ -> raise (NotImplemented ("AssignStmt 2", p))
         end

    | IfStmt (cond, con, alt, p) ->
         (* con = consequence, alt = alternative *)
         build_exp env cond (fun env a ->

         let cont = new_symbol_string "if-continuation" in
         let cont_ty = TyFun ([], TyInt) in
         let cont_body = c env in
         let cont_call = TailCall (cont, []) in

         let con = build_stmt env (StmtList (con, p)) (fun _ -> cont_call) in
         let alt = build_stmt env (StmtList (alt, p)) (fun _ -> cont_call) in

         LetFuns ([cont, (bogus_loc ""), FunLocalClass, cont_ty, [], cont_body],

         int_of_atom a (fun a ->
         Match (a, [(IntSet (IntSet.of_point 0), alt);
                    (IntSet (IntSet.max_set), con)]))))

    | ImportStmt (sl, slsl, _) -> begin
         match sl, slsl with
            [], [[sym], _] ->
               let env = do_import env sym in
               c env

          | _ -> (*raise (NotImplemented "ImportStmt")*)
               c env
         end

    | FuncDef (f, params, body, p) ->
         let params =
            List.map
               (function
                  IdParam (s, _) -> s
                | _ -> raise (NotImplemented ("FuncDef", p)))
               params
         in
         let env = env_add_func env f params in
         let body =
            build_stmt env (StmtList (body, p)) (fun _ -> Return (AtomInt 0))
         in
         let ty = TyFun (List.map (fun _ -> py_ty) params, py_ty) in
         LetFuns ([f, (bogus_loc ""), FunGlobalClass, ty, params, body],
         c env)

    | PrintStmt (file, exps, p) ->
         build_exp_list env exps (fun env al ->
         let v = new_symbol_string "print" in
         LetApply (v, TyEnum 1, Python_runtime.print_func, al,
         c env))

    | ReturnStmt (e, _) ->
         build_exp env e (fun env a ->
         Return a)

    | StmtList (stmts, p) -> begin
         match stmts with
            [] -> c env

          | stmt :: stmts ->
               build_stmt env stmt (fun env ->
               build_stmt env (StmtList (stmts, p)) c)
         end

    | WhileStmt (guard, body, else_body, p) ->
         let cont = new_symbol_string "while_continuation" in
         let cont_fundef =
            let cont_body = c env in
            let cont_ty = TyFun ([], TyInt) in
            cont, (bogus_loc ""), FunLocalClass, cont_ty, [], cont_body
         in
         let cont_call = TailCall (cont, []) in

         let while_f = new_symbol_string "while_function" in
         let while_fundef =
            let while_f_body =
               build_exp env guard (fun env a ->
               int_of_atom a (fun a ->
               Match (a, [(IntSet (IntSet.of_point 0), cont_call);
                         (IntSet (IntSet.max_set),
                          build_stmt
                             env
                             (StmtList (body, p))
                             (fun _ -> TailCall (while_f, [])))])))
            in
            let ty = TyFun ([], TyEnum 1) in
            while_f, (bogus_loc ""), FunLocalClass, ty, [], while_f_body
         in

         LetFuns ([while_fundef; cont_fundef],
         TailCall (while_f, []))

    | ClassDef (_, _, _, p)
    | TryStmt (_, _, _, p)
    | ForStmt (_, _, _, _, p)
    | GlobalStmt (_, p)
    | ContStmt p
    | BreakStmt p
    | RaiseStmt (_, _, _, p)
    | DelStmt (_, p)
    | PassStmt p
    | AugAssignStmt (_, _, _, p)
    | AssertStmt (_, _, p)      ->
         raise (NotImplemented ("build_stmt", p))


(*
 * Convert a list of Python AST statements into a naML-IR program.
 *)
let make_prog stmts =

   (* naML-IR programs have types.  We only need two here: the Python type, and
    * the naML exception handler type.
    *)
   let types =
      List.fold_left
         (fun types (id, tydef) -> SymbolTable.add types id tydef)
         SymbolTable.empty
         [(py_tydef_var, py_tydef);
          (Naml_ir_misc.exn_symbol, TyDefUnion ([], [[]]))
          ]
   in

   let initial_env =
      {  env_funcs = SymbolTable.empty;
         env_imports = SymbolSet.empty
      }
   in
   let initial_env =
      env_add_func initial_env Python_runtime.int_func [new_symbol_string "i"]
   in
   let argv = Symbol.add "argv" in
   let body =
      (* Include the runtime functions. *)
      LetFuns (Python_runtime.runtime_funcs,

(*
      (* Initialize argv to "10" *)
      let tmp_str = new_symbol_string "tmp_str" in
      LetAlloc (tmp_str,
                AllocArray (TyArray (TyEnum 256),
                            [AtomEnum (256, 48); AtomEnum (256, 49)]),
      LetAlloc (argv,
                AllocUnion (py_ty, [],
                            py_tydef_var,
                            python_string_tag,
                            [AtomVar tmp_str]),
*)

      (* Build the statements. *)
      try
         build_stmt initial_env
                    (StmtList (stmts, ("", 0, 0, 0, 0)))
                    (fun _ -> Return (AtomInt 0))
      with
         NotImplemented (str, pos) ->
            Python_position.print_pos pos;
            printf "NotImplemented: `%s'@." str;
            exit 1
      )
   in

   {  prog_types = types;
      prog_cont = new_symbol_string "cont";
      prog_body = body
   }
