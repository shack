val add_func : Symbol.symbol
val minus_func : Symbol.symbol
val equal_func : Symbol.symbol
val or_func : Symbol.symbol
val coerce_bool_func : Symbol.symbol
(*val slice_func : Symbol.symbol*)
(*val attrref_func : Symbol.symbol*)
val print_func : Symbol.symbol
val int_func : Symbol.symbol
val times_func : Symbol.symbol
val gt_func : Symbol.symbol
val elt_func : Symbol.symbol

val runtime_funcs : Naml_ir.fundef list
