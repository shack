open Symbol
open Fir
open Naml_ir
open Python_ast


let x = Symbol.add "x"

let p = "", 0, 0, 0, 0

let pyexpr_five = LiteralExpr (IntLiteral (5, p), p)

let pyexpr = AssignStmt ([[IdExpr (x,p)]], [OpExpr (pyexpr_five, Plus, pyexpr_five, p)], p)

let expr = Python_ir_ast.make_expr_from_stmt SymbolTable.empty pyexpr (fun a -> Return a)

let _ =
   Naml_ir_util.print_expr expr;
   Format.print_newline ()
