(* Intermediate Representation for Python
 *
 * Python's intermediate representation is the same as naML's IR.
 *
 * David Bustos
 *)

open Fir
open Naml_ir_misc
open Fir_set
open Symbol
open Fir_type


(* Account for name brain damage. *)
let char_ty = type_char
let float_type = type_float
let unit_ty = ty_unit


(* Set crap. *)
let int_interval i j =
   IntSet.of_interval (Interval_set.Closed i) (Interval_set.Closed j)
let zero_interval n = int_interval 0 (n - 1)
let int_set_zero_one = zero_interval 2


let py_tydef_var = new_symbol_string "python_type"


let py_ty = TyApply (py_tydef_var, [])
let py_list_ty =
   TyUnion (py_tydef_var,
            [],
            IntSet.union (IntSet.of_point 6) (IntSet.of_point 7))


let py_ty_name_const_list =
   ["int",      [TyInt, Mutable];
    "long int", [TyInt, Immutable];
    "float",    [float_type, Mutable];
    "complex",  [float_type, Mutable; float_type, Mutable];
    "string",   [TyArray char_ty, Immutable];
    "tuple",    [TyArray py_ty, Immutable];
    "nil",      [];
    "cons",     [py_ty, Mutable; py_list_ty, Mutable];
    "dict",     [];
    "func",     []
   ]

let py_ty_consts = List.map snd py_ty_name_const_list
let py_tydef = TyDefUnion ([], py_ty_consts)

let get_pyt_tag ty_name =
   let rec find_ty i rest =
      match rest with
         (name, _) :: rest ->
            if name = ty_name then
               i
            else
               find_ty (succ i) rest
       | [] ->
            raise Not_found
   in
   find_ty 0 py_ty_name_const_list

let py_ty_nil_tag_set = IntSet.of_point (get_pyt_tag "nil")
let py_ty_cons_tag_set = IntSet.of_point (get_pyt_tag "cons")

let py_nil_ty = TyUnion (py_tydef_var, [], py_ty_nil_tag_set)
let py_cons_ty = TyUnion (py_tydef_var, [], py_ty_cons_tag_set)


let python_int_tag	= get_pyt_tag "int"
let python_long_int_tag	= get_pyt_tag "long int"
let python_float_tag	= get_pyt_tag "float"
let python_complex_tag	= get_pyt_tag "complex"
let python_string_tag	= get_pyt_tag "string"
let python_tuple_tag	= get_pyt_tag "tuple"
let python_nil_tag      = get_pyt_tag "nil"
let python_cons_tag	= get_pyt_tag "cons"
let python_dict_tag	= get_pyt_tag "dict"
let python_func_tag	= get_pyt_tag "func"

type exp = Naml_ir.exp


let zero_set = IntSet (IntSet.of_point 0)
let minus_set =
   IntSet (IntSet.of_interval Interval_set.Infinity (Interval_set.Open 0))
let max_set = IntSet (IntSet.max_set)

let int_tag_set = IntSet (IntSet.of_point python_int_tag)
let string_tag_set = IntSet (IntSet.of_point python_string_tag)
let list_nil_set = IntSet (IntSet.of_point python_nil_tag)
let list_cons_set = IntSet (IntSet.of_point python_cons_tag)
(*let all_tag_set = IntSet pyt_all_tags_set*)
let all_tag_set = IntSet IntSet.max_set


let ty_unit = TyEnum 1
let unit_atom = AtomEnum (1, 0)

let bool_ty = TyEnum 2
let false_atom = AtomEnum (2, 0)
let true_atom = AtomEnum (2, 1)
let bool_false_set = IntSet (IntSet.of_point 0)
let bool_true_set = IntSet (IntSet.of_point 1)

let char_ty = TyEnum 256
let newline_atom = AtomEnum (256, 10)

let block_poly_subop =
   { sub_block = BlockSub;
     sub_value = PolySub;
     sub_index = WordIndex;
     sub_script = IntIndex
  }

(*
 * Generates naML-IR code that extracts an integer from a Python (AtomVar)
 * variable.
 *)
let get_int v a e =
   match a with
      AtomVar v' ->
         Naml_ir.LetSubscript (block_poly_subop, v, TyInt, v', AtomInt 0, e)
    | _ ->
         raise (Failure "blah3!")

(*
 * Generates naML-IR code that raises an exception.
 *)
let raise_exception () =
   let ex = new_symbol_string "add_exception" in
   Naml_ir.LetAlloc (ex, AllocUnion ([], type_exn, exn_symbol, 0, []),
   Naml_ir.Raise (AtomVar ex))

(*
 * Returns FIR code that allocates an integer with the given value.
 *)
let new_int v a e =
   Naml_ir.LetAlloc (v,
             AllocUnion ([], py_ty, py_tydef_var, python_int_tag, [a]),
             e)

