(* Unlambda front end
 * Geoffrey Irving
 * $Id: unlam_compile.ml,v 1.3 2002/08/06 03:16:21 jyh Exp $ *)
open Format

let compile name =
   let inx = open_in name in
   try
     let prog = Unlam_parse.prog inx in
     let prog = Unlam_fir_ast.fir_prog prog in
     let prog = Fir_standardize.standardize_prog prog in
       prog
   with exn ->
     Unlam_exn.output_exn err_formatter exn;
     exit 1
