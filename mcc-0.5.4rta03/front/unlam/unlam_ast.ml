(* Abstract syntax for unlambda
 * Geoffrey Irving
 * $Id: unlam_ast.ml,v 1.4 2001/08/17 04:06:15 irving Exp $ *)

type exp =
    Apply of exp * exp 
  | K | S | I | V | C | Y | D | E | At | Pipe | Slash
  | Dot of char | Ask of char
