(* Fir conversion for unlambda
 * Geoffrey Irving
 * $Id: unlam_fir_ast.ml,v 1.15 2002/08/29 17:32:34 jyh Exp $ *)

(* Notes:
 *   1. This function converts directly from ast to fir.  It does
 *      cps conversion, but no closure conversion, as unlambda code
 *      lacks variables.
 *   2. Continuations (i.e., c's passed to fir_expr) take three arguments:
 *      funs, k, and a.  funs is the function definition table, a is the
 *      atom, and k is false if it is not known whether a is d.
 *)

open Symbol
open Fir
open Unlam_exn
open Unlam_ast
open Unlam_lib

let p = new_symbol_string "p"
let y = new_symbol_string "x"
let env = new_symbol_string "env"
let cont = new_symbol_string "cont"
let atom_p = AtomVar p
let atom_y = AtomVar y
let atom_env = AtomVar env
let atom_cont = AtomVar cont
let cont_return funs _ a = funs, return cont a

let rec fir_expr funs e c =
  match e with
      K -> c funs true k_a | S -> c funs true s_a | I -> c funs true i_a
    | V -> c funs true v_a | C -> c funs true c_a | D -> c funs true d_a
    | Y -> c funs true y_a | E -> c funs true e_a | At -> c funs true at_a
    | Pipe -> c funs true pipe_a | Slash -> c funs true slash_a
    | Apply (e1, e2) ->
        fir_expr funs e1 (fun funs k a ->
          let funs, e2 = fir_expr funs e2 cont_return in
            if k then
              match a with
                  AtomVar x -> (* not d *)
                    let ct = new_symbol_string "ct" in
                    let atom_ct = AtomVar ct in
                    let funs, e = c funs false atom_y in
                    let funs = SymbolTable.add funs ct (ContFun ty_cont, [cont; y], e) in
                      funs,
                      (pair ct atom_ct atom_cont
                        (pair env a atom_ct
                          (pair cont cfp_a atom_env e2)))
                | _ -> (* d *)
                    let t = new_symbol_string "thunk" in
                    let funs = SymbolTable.add funs t (ThunkFun, [cont], e2) in
                    let funs, e = c funs true atom_p in
                      funs, pair_u p promise_a (AtomVar t) e
            else
              match a with
                  AtomVar x ->
                    let t = new_symbol_string "thunk" in
                    let funs = SymbolTable.add funs t (ThunkFun, [cont], e2) in
                    let ct = new_symbol_string "ct" in
                    let atom_ct = AtomVar ct in
                    let funs, e = c funs false atom_y in
                    let funs = SymbolTable.add funs ct (ContFun ty_cont, [cont; y], e) in
                      funs,
                      ifd x
                        (pair_u p promise_a (AtomVar t)
                          (unlam_exp (TailCall (new_symbol_string "blah", AtomVar ct, [atom_cont; atom_p]))))
                        (pair ct atom_ct atom_cont
                          (pair env a atom_ct
                            (pair cont cfp_a atom_env
                              (call_thunk t atom_cont))))
                | _ ->
                    raise (Impossible "saw weird atom in unlam_fir_ast.ml"))
    | Dot x ->
        let v = new_symbol_string "dot" in
        let funs, e = c funs true (AtomVar v) in
          funs, pair_u v dot_a (AtomInt (int_of_char x)) e
    | Ask x ->
        let v = new_symbol_string "ask" in
        let funs, e = c funs true (AtomVar v) in
          funs, pair_u v ask_a (AtomInt (int_of_char x)) e

let fir_main funs main e =
  let f = new_symbol_string "fini" in
    SymbolTable.add funs main (MainFun, [f],
      pair cont fini_a (AtomVar f) e)

let fir_funs funs =
  SymbolTable.map (fun (ft, vl, e) ->
      unlam_loc, [], type_of_fun ft, vl, e)
    funs

let fir_prog e =
  let file = { file_dir = ""; file_name = ""; file_class = FileNaml } in
  let main = new_symbol_string "main" in
  let funs, e = fir_expr funs e cont_return in
  let funs = fir_main funs main e in
  let funs = fir_funs funs in
  let export =
     { export_name = "init";
       export_type = type_of_fun MainFun
     }
  in
  let export = SymbolTable.add SymbolTable.empty main export in
    { prog_file = file;
      prog_import = SymbolTable.empty;
      prog_export = export;
      prog_types = tenv;
      prog_globals = globals;
      prog_funs = funs;
      prog_names = SymbolTable.empty;
      prog_frames = SymbolTable.empty
    }


