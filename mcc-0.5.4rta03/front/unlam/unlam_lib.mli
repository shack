(* Unlambda standard library
 * Geoffrey Irving
 * $Id: unlam_lib.mli,v 1.9 2002/08/11 19:11:16 jyh Exp $ *)

open Symbol
open Fir
open Fir_type
open Location

(******** type stuff *)

val ty_cont : ty

type fun_type =
    NormalFun of ty
  | ContFun of ty
  | ThunkFun
  | MainFun

type fundef = fun_type * var list * exp

val type_of_fun : fun_type -> ty

val unlam_loc : loc
val unlam_exp : exp_core -> exp

(******** fir macros *)

val ifd : var -> exp -> exp -> exp
val call : var -> atom list -> exp
val return : var -> atom -> exp
val call_thunk : var -> atom -> exp
val pair : var -> atom -> atom -> exp -> exp
val pair_u : var -> atom -> atom -> exp -> exp

(******** builtin functions *)

val k_a : atom
val s_a : atom
val i_a : atom
val v_a : atom
val c_a : atom
val y_a : atom
val d_a : atom
val e_a : atom
val at_a : atom
val pipe_a : atom
val slash_a : atom

val dot_a : atom
val ask_a : atom
val promise_a : atom
val cfp_a : atom
val fini_a : atom

(******** prog stuff *)

val tenv : tydef SymbolTable.t
val funs : fundef SymbolTable.t
val globals : global SymbolTable.t
