(* Unlambda standard library
 * Geoffrey Irving
 * $Id: unlam_lib.ml,v 1.33 2002/12/23 00:13:20 jyh Exp $ *)

open Symbol
open Fir
open Fir_ds
open Fir_set
open Fir_type
open Location

let unlam_loc = bogus_loc "<Unlam_lib>"
let unlam_exp e = make_exp unlam_loc e

(************************************** types *)

let d_set = IntSet.of_point 0
let f_set = IntSet.of_point 1
let df_set = IntSet.union d_set f_set
let d_set_set = IntSet d_set
let f_set_set = IntSet f_set
let char_set = IntSet.of_interval (Interval_set.Closed 0) (Interval_set.Closed 255)
let not_char_set = IntSet.negate char_set

let tu_v = new_symbol_string "tu"
let t_v = new_symbol_string "t"
let tf_v = new_symbol_string "t"
let cont_v = new_symbol_string "cont"
let su_v = new_symbol_string "state"

let ty_t = TyApply (t_v, [])
let ty_tf = TyApply (tf_v, [])
let ty_cont = TyApply (cont_v, [])
let ty_thunk = TyFun ([ty_cont], ty_void)
let ty_state = TyUnion (su_v, [], d_set)

let tenv =
  let a = new_symbol_string "a" in
  let ta = TyVar a in
  List.fold_left (fun tenv (v, td) -> SymbolTable.add tenv v td) SymbolTable.empty [
    tu_v,   TyDefUnion ([a], [[]; [TyFun ([ta; ty_t; ty_cont], ty_void), Immutable; ta, Immutable]]);
    su_v,   TyDefUnion ([], [[TyInt, Mutable]]);
    t_v,    TyDefLambda ([], TyExists ([a], TyUnion (tu_v, [ta], df_set)));
    tf_v,   TyDefLambda ([], TyExists ([a], TyUnion (tu_v, [ta], f_set)));
    cont_v, TyDefLambda ([], TyExists ([a], TyTuple (NormalTuple, [TyFun ([ta;ty_t], ty_void), Immutable; ta, Immutable])))]

type fun_type =
    NormalFun of ty
  | ContFun of ty
  | ThunkFun
  | MainFun

type fundef = fun_type * var list * exp

let ty_builtin = TyUnion (tu_v, [ty_unit], f_set)
let ty_fini = TyFun ([TyInt], ty_void)
let ty_char = TyInt

let type_of_fun = function
    NormalFun t -> TyFun ([t; ty_t; ty_cont], ty_void)
  | ContFun t -> TyFun ([t; ty_t], ty_void)
  | ThunkFun ->  TyFun ([ty_cont], ty_void)
  | MainFun -> TyFun ([ty_fini], ty_void)

(************************************** fir macros *)

let block_poly_subop =
   { sub_block = BlockSub;
     sub_value = PolySub;
     sub_index = WordIndex;
     sub_script = IntIndex
   }

let ifd f et ef =
   let true_label = new_symbol_string "true" in
   let false_label = new_symbol_string "false" in
      unlam_exp (Match (AtomVar f, [true_label, d_set_set, et; false_label, f_set_set, ef]))

let split x y p e =
  unlam_exp (LetSubscript (block_poly_subop, x, TyDelayed, AtomVar p, AtomInt 0,
  unlam_exp (LetSubscript (block_poly_subop, y, TyDelayed, AtomVar p, AtomInt 1, e))))

let call f al =
  let f_v = new_symbol f in
  let e_v = new_symbol f in
    split f_v e_v f
    (unlam_exp (TailCall (new_symbol_string "call", AtomVar f_v, AtomVar e_v :: al)))

let return c a = call c [a]

let call_thunk t c =
  unlam_exp (TailCall (new_symbol_string "thunk", AtomVar t, [c]))

let pair v a1 a2 e =
  unlam_exp (LetAlloc (v, AllocTuple (NormalTuple, [], TyDelayed, [a1; a2]), e))

let pair_u v a1 a2 e =
  unlam_exp (LetAlloc (v, AllocUnion ([], TyDelayed, tu_v, 1, [a1; a2]), e))

let triple v a1 a2 a3 e =
  unlam_exp (LetAlloc (v, AllocTuple (NormalTuple, [], TyDelayed, [a1; a2; a3]), e))

let split3 x y z t e =
  split x y t
  (unlam_exp (LetSubscript (block_poly_subop, z, TyDelayed, AtomVar t, AtomInt 2, e)))

let atom_unit = AtomEnum (1, 0)
let atom_rawzero = AtomRawInt (Rawint.of_int Rawint.Int32 true 0)
let d_a = AtomConst (TyDelayed, tu_v, 0)

let ty_print_char = TyFun ([ty_char], ty_unit)
let putc c e =
  unlam_exp (LetExt (new_symbol_string "u", ty_unit, "ml_print_char", false, ty_print_char, [], [c], e))

(************************************** setup *)

let funs = SymbolTable.empty
let define funs fl = List.fold_left (fun funs (v, d) -> SymbolTable.add funs v d) funs fl

let globals = SymbolTable.empty
let define_builtin globals bv =
  let f = new_symbol bv in
  let globals = SymbolTable.add globals f
    (ty_builtin, InitAlloc (AllocUnion ([], ty_builtin, tu_v, 1, [AtomVar bv; atom_unit]))) in
  f, globals

let state_v = new_symbol_string "state"
let globals = SymbolTable.add globals state_v (ty_state,
  InitAlloc (AllocUnion ([], ty_state, su_v, 0, [AtomInt ~-1])))

(************************************** builtin functions *)

let nss = new_symbol_string

(* variables *)
let a = nss "a"
let e = nss "e"
let f = nss "f"
let v = nss "v"
let x = nss "x"
let y = nss "y"
let z = nss "z"
let c = nss "cont"
let atom_a = AtomVar a
let atom_e = AtomVar e
let atom_f = AtomVar f
let atom_v = AtomVar v
let atom_x = AtomVar x
let atom_y = AtomVar y
let atom_z = AtomVar z
let atom_c = AtomVar c

(**** i - 'ix = x ****)

let i_v = nss "i"
let i_def = NormalFun ty_unit,
  [e; x; c],
  return c atom_x

let funs = define funs [i_v, i_def]
let i_v, globals = define_builtin globals i_v
let i_a = AtomVar i_v

(**** v - 'vx = v ****)

let v_v' = nss "v"

let v_v, globals = define_builtin globals v_v'
let v_a = AtomVar v_v

let v_def = NormalFun ty_unit,
  [e; x; c],
  return c v_a

let funs = define funs [v_v', v_def]

(**** c - well, you know what it does ****)

let ch_v = nss "ch"
let atom_ch = AtomVar ch_v
let ch_def = NormalFun ty_cont,
  [c; x; y],
  return c atom_x

let c_v = nss "c"
let c_def = NormalFun ty_unit,
  [e; x; c],
  pair_u f atom_ch atom_c
    (ifd x
      (return c atom_f)
      (call x [atom_f; atom_c]))

let funs = define funs [ch_v, ch_def; c_v, c_def]
let c_v, globals = define_builtin globals c_v
let c_a = AtomVar c_v

(**** k - ''kxy = x ****)

let k'_v = nss "k'"
let atom_k' = AtomVar k'_v
let k'_def = NormalFun ty_t,
  [x; y; c],
  return c atom_x

let k_v = nss "k"
let k_def = NormalFun ty_unit,
  [e; x; c],
  pair_u v atom_k' atom_x
    (return c atom_v)

let funs = define funs [k'_v, k'_def; k_v, k_def]
let k_v, globals = define_builtin globals k_v
let k_a = AtomVar k_v

(**** promises and special continuations ****)

let cfp_v = nss "p_cf_v"
let cfp_a = AtomVar cfp_v
let cfp_def = ContFun (TyTuple (NormalTuple, [ty_tf, Immutable; ty_cont, Immutable])),
  [e; y],
  split x c e
    (call x [atom_y; atom_c])

let cf_v = nss "cf"
let atom_cf = AtomVar cf_v
let cf_def = ContFun (TyTuple (NormalTuple, [ty_t, Immutable; ty_cont, Immutable])),
  [e; x],
  split y c e
    (ifd x
      (return c (atom_y))
      (call x [atom_y; atom_c]))

let promise_v = nss "promise"
let promise_a = AtomVar promise_v
let promise_def = NormalFun ty_thunk,
  [x; a; c],
  pair e atom_a atom_c
    (pair c atom_cf atom_e
      (call_thunk x atom_c))

let promise'_v = nss "promise'"
let atom_promise' = AtomVar promise'_v
let promise'_def = NormalFun (TyTuple (NormalTuple, [ty_tf, Immutable; ty_t, Immutable])),
  [e; a; c],
  split x y e
    (pair e atom_a atom_c
      (pair c atom_cf atom_e
        (call x [atom_y; atom_c])))

let cf'_v = nss "cf'"
let atom_cf' = AtomVar cf'_v
let cf'_def = ContFun (TyTuple (NormalTuple, [ty_tf, Immutable; ty_t, Immutable; ty_cont, Immutable])),
  [e; x],
  split3 y z c e
    (ifd x
      (pair e atom_y atom_z
        (pair_u f atom_promise' atom_e
          (return c atom_f)))
      (pair e atom_x atom_c
        (pair c cfp_a atom_e
          (call y [atom_z; atom_c]))))

let funs = define funs [cf_v, cf_def; cfp_v, cfp_def; promise_v, promise_def]
let funs = define funs [promise'_v, promise'_def; cf'_v, cf'_def]

(**** s - `sxyz = ``xz`yz ****)

let sff_v = nss "sff"
let atom_sff = AtomVar sff_v
let sff_def = NormalFun (TyTuple (NormalTuple, [ty_tf, Immutable; ty_tf, Immutable])),
  [e; z; c],
  split x y e
    (triple e atom_y atom_z atom_c
      (pair c atom_cf' atom_e
        (call x [atom_z; atom_c])))

let sfd_v = nss "sfd"
let atom_sfd = AtomVar sfd_v
let sfd_def = NormalFun ty_tf,
  [x; z; c],
  pair e atom_z atom_c
    (pair c atom_cf atom_e
      (call x [atom_z; atom_c]))

let sdf_v = nss "sdf"
let atom_sdf = AtomVar sdf_v
let sdf_def = NormalFun ty_tf,
  [y; z; c],
  ifd z
    (call y [d_a; atom_c])
    (pair e atom_z atom_c
      (pair c cfp_a atom_e
        (call y [atom_z; atom_c])))

let sdd_v = nss "sdd"
let sdd_def = NormalFun ty_unit,
  [e; z; c],
  ifd z
    (return c i_a)
    (call z [atom_z; atom_c])
let sdd_vg, globals = define_builtin globals sdd_v
let atom_sdd = AtomVar sdd_vg

let sf_v = nss "sf"
let atom_sf = AtomVar sf_v
let sf_def = NormalFun ty_tf,
  [x; y; c],
  ifd y
    (pair_u f atom_sfd atom_x
      (return c atom_f))
    (pair e atom_x atom_y
      (pair_u f atom_sff atom_e
        (return c atom_f)))

let sd_v = nss "sd"
let sd_def = NormalFun ty_unit,
  [e; y; c],
  ifd y
    (return c atom_sdd)
    (pair_u f atom_sdf atom_y
      (return c atom_f))
let sd_vg, globals = define_builtin globals sd_v
let atom_sd = AtomVar sd_vg

let s_v = nss "s"
let s_def = NormalFun ty_unit,
  [e; x; c],
  ifd x
    (return c atom_sd)
    (pair_u f atom_sf atom_x
      (return c atom_f))

let funs = define funs [sff_v, sff_def; sfd_v, sfd_def; sdf_v, sdf_def; sdd_v, sdd_def;
                        sf_v, sf_def; sd_v, sd_def; s_v, s_def]
let s_v, globals = define_builtin globals s_v
let s_a = AtomVar s_v

(**** i/o macros ****)

let ifchar c et ef =
   let true_label = new_symbol_string "true" in
   let false_label = new_symbol_string "false" in
      unlam_exp (Match (AtomVar c, [true_label, IntSet char_set, et; false_label, IntSet not_char_set, ef]))

let let_curchar c e =
  unlam_exp (LetSubscript (block_poly_subop, c, TyInt, AtomVar state_v, AtomInt 0, e))

let set_curchar c e =
   let label = new_symbol_string "set_curchar" in
      unlam_exp (SetSubscript (block_poly_subop, label, AtomVar state_v, AtomInt 0, TyInt, c, e))

let getc c e =
  unlam_exp (LetExt (c, TyInt, "unl_getchar", false, TyFun ([], TyInt), [], [], e))

let ifeq x y et ef =
   let t = new_symbol_string "t" in
   let true_label = new_symbol_string "true" in
   let false_label = new_symbol_string "false" in
      unlam_exp (LetAtom (t, ty_bool, AtomBinop (EqIntOp, AtomVar x, AtomVar y),
      unlam_exp (Match (AtomVar t, [true_label, f_set_set, et; false_label, d_set_set, ef]))))

(**** output ****)

let dot_v = nss "dot"
let dot_a = AtomVar dot_v
let dot_def = NormalFun ty_char,
  [e; x; c],
  putc atom_e
    (return c atom_x)

let pipe_v = nss "pipe"
let pipe_def = NormalFun ty_unit,
  [e; x; c],
  let_curchar y
    (ifchar y
      (pair_u f dot_a atom_y
        (ifd x
          (return c atom_f)
          (call x [atom_f; atom_c])))
      (ifd x
        (return c v_a)
        (call x [v_a; atom_c])))

let funs = define funs [dot_v, dot_def; pipe_v, pipe_def]
let pipe_v, globals = define_builtin globals pipe_v
let pipe_a = AtomVar pipe_v

(**** input ****)

let at_v = nss "at"
let at_def = NormalFun ty_unit,
  [e; x; c],
  getc y
    (set_curchar atom_y
      (ifd x
        (ifchar y
          (return c i_a)
          (return c v_a))
        (ifchar y
          (call x [i_a; atom_c])
          (call x [v_a; atom_c]))))

let ask_v = nss "ask"
let ask_a = AtomVar ask_v
let ask_def = NormalFun ty_char,
  [e; x; c],
  unlam_exp (LetExt (a, ty_int32, "flush", false, TyFun ([], ty_int32), [], [],
  let_curchar y
    (ifd x
      (ifeq e y
        (return c i_a)
        (return c v_a))
      (ifeq e y
        (call x [i_a; atom_c])
        (call x [v_a; atom_c])))))

let slash_v = nss "slash"
let slash_def = NormalFun ty_unit,
  [e; x; c],
  let_curchar y
    (ifchar y
      (pair_u f ask_a atom_y
        (ifd x
          (return c atom_f)
          (call x [atom_f; atom_c])))
      (ifd x
        (return c v_a)
        (call x [v_a; atom_c])))

let funs = define funs [at_v, at_def; ask_v, ask_def]
let at_v, globals = define_builtin globals at_v
let at_a = AtomVar at_v
let slash_v, globals = define_builtin globals slash_v
let slash_a = AtomVar slash_v

(**** y - `yx = `x`d`yx ****)

let spin_v' = nss "spin"
let spin_v, globals = define_builtin globals spin_v'
let spin_a = AtomVar spin_v

let spin_def = NormalFun ty_unit,
  [e; x; c],
  call spin_v [atom_x; atom_c]

let y_v' = nss "y"
let y_v, globals = define_builtin globals y_v'
let y_a = AtomVar y_v

let y_def = NormalFun ty_unit,
  [e; x; c],
  ifd x
    (return c spin_a)
    (pair e y_a atom_x
      (pair_u f atom_promise' atom_e
        (call x [atom_f; atom_c])))

let funs = define funs [spin_v', spin_def; y_v', y_def]

(**** fini ****)

let fini_v = nss "fini"
let fini_a = AtomVar fini_v
let fini_def = ContFun ty_fini,
  [e; x],
  unlam_exp (TailCall (new_symbol_string "final", AtomVar e, [AtomInt 0]))

let funs = define funs [fini_v, fini_def]

(**** e - exit the program ****)

let e_v = nss "e"
let e_def = NormalFun ty_unit,
  [e; x; c],
  unlam_exp (LetExt (y, ty_void, "exit", false, TyFun ([ty_int32], ty_void), [], [atom_rawzero],
  (return c atom_x)))

let funs = define funs [e_v, e_def]
let e_v, globals = define_builtin globals e_v
let e_a = AtomVar e_v
