(* Parser for unlambda
 * Geoffrey Irving
 * $Id: unlam_parse.ml,v 1.6 2002/08/30 02:33:53 justins Exp $ *)

open Unlam_ast
open Unlam_exn

let rec exp inx =
  match input_char inx with
      ' ' | '\n' | '\r' | '\012' -> exp inx 
    | '#' -> ignore (input_line inx); exp inx
    | 's' | 'S' -> S
    | 'k' | 'K' -> K
    | 'i' | 'I' -> I
    | 'v' | 'V' -> V
    | 'c' | 'C' -> C
    | 'y' | 'Y' -> Y
    | 'd' | 'D' -> D
    | 'e' | 'E' -> E
    | '@' -> At
    | '|' -> Pipe
    | '/' -> Slash
    | 'r' | 'R' -> Dot '\n'
    | '.' -> Dot (input_char inx)
    | '?' -> Ask (input_char inx)
    | '`' -> let x = exp inx in let y = exp inx in Apply (x, y) 
    | _ -> raise UserError

let rec sweep inx =
  match input_char inx with
      ' ' | '\n' | '\r' | '\012' -> sweep inx
    | '#' -> ignore (input_line inx); sweep inx
    | _ -> raise UserError

let prog inx =
  let e = exp inx in
    try sweep inx with End_of_file -> e
