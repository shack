(* Unlambda exceptions
 * Geoffrey Irving
 * $Id: unlam_exn.ml,v 1.5 2002/08/06 03:16:21 jyh Exp $ *)

open Format

exception UserError
exception Impossible of string

let output_exn out = function
    UserError ->
      fprintf out "No, you fool!"
  | Impossible s ->
      fprintf out "Impossible: %s" s
  | exn ->
      fprintf out "uncaught exception: %s" (Printexc.to_string exn)

let catch f x =
   try f x with
      exn ->
         output_exn err_formatter exn;
         exit 1
