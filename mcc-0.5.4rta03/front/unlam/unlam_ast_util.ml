(* AST utilities for unlambda
 * Geoffrey Irving
 * $Id: unlam_ast_util.ml,v 1.2 2001/08/17 04:06:15 irving Exp $ *)

open Unlam_ast

let rec output_exp out e =
  let oc = output_char out in
  let oe = output_exp out in
  match e with 
      Apply (e1, e2) -> 
        oc '`'; oe e1; oe e2 
    | K -> oc 'k'
    | S -> oc 's'
    | I -> oc 'i'
    | V -> oc 'v'
    | C -> oc 'c'
    | Y -> oc 'y'
    | D -> oc 'd'
    | E -> oc 'e'
    | At -> oc '@'
    | Pipe -> oc '|'
    | Slash -> oc '/'
    | Dot c -> oc '.'; oc c
    | Ask c -> oc '?'; oc c

let print_exp = output_exp stdout
