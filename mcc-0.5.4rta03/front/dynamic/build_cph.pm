#
# Helper functions to make building FC programs easy.
# Copyright(c) 2002 Justin David Smith (who HATES perl).
#
# NOTE:  The flags for each test profile are defined in main/test_mcc.
#        Go there if you need to modify the flags.  PLEASE DON'T MODIFY
#        THE FLAGS without asking Justin first; you will affect the
#        regression results if you modify flags, and may cause some
#        failing cases to drop off the radar!
#
package build_cph;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(cph_build);


#
# Default environment.  The standard set enables the
# usual optimizations (not experimental optimizations!),
# and sets other flags to ease debugging.
#
$env = new cons(
   CC          => 'mcc',
   CPPPATH     => '',
   CFLAGS      => '',
   LDFLAGS     => '',
   ENV  => {
      PATH     => 'main:/bin:/usr/bin:/usr/local/bin'
   },
);


#
# Build a CPH file from a PHO file.
#
sub cph_build {
   my($base) = @_;
   my $tdir = $dir::cwd->lookupfile("")->path;

   Depends $env "$base.cph", "#main/mcc";
   Command $env "$base.cph", "$base.pho", "$env->{CC} $env->{CFLAGS} -I $tdir $tdir/$base.pho";

   Depends $env "$unity_base.cph", "#main/mcc";
   Command $env "$unity_base.cph", "$unity_base.pho", "$env->{CC} $env->{CFLAGS} -I $tdir $tdir/$unity_base.pho";

   Depends $env "$c_util.cph", "#main/mcc";
   Command $env "$c_util.cph", "$c_util.pho", "$env->{CC} $env->{CFLAGS} -I $tdir $tdir/$c_util.pho";

   Depends $env "$c_util_unity.cph", "#main/mcc";
   Command $env "$c_util_unity.cph", "$c_util_unity.pho", "$env->{CC} $env->{CFLAGS} -I $tdir $tdir/$c_util_unity.pho";

   Depends $env "$unity.cph", "#main/mcc";
   Command $env "$unity.cph", "$unity.pho", "$env->{CC} $env->{CFLAGS} -I $tdir $tdir/$unity.pho";

   Depends $env "$c_of_unity.cph", "#main/mcc";
   Command $env "$c_of_unity.cph", "$c_of_unity.pho", "$env->{CC} $env->{CFLAGS} -I $tdir $tdir/$c_of_unity.pho";
}
