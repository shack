#  Run the configure script to generate the Makefile
#  Copyright(c) 2002 Justin David Smith, Caltech
#


#
#  Targets
#
include Rules.make

all: build-system

build-system: Makefile Conscript Rules.make meta-prl/Conscript main/mcc_config.ml

configure: configure.in aclocal.m4
	@echo "configure is out-of-date, rebuilding"
	autoconf

Makefile Conscript Rules.make meta-prl/Conscript main/mcc_config.ml: \
	Makefile.in Conscript.in Rules.make.in meta-prl/Conscript.in main/mcc_config.ml.in config.status
	@echo "Build system is out-of-date, rebuilding"
	./config.status
	@echo ""
	@echo "*** Makefile was rebuilt, current session is out of date ***"
	@echo "*** Please run \`\`make'' again so changes will take effect ***"
	@exit 1

config.status: configure
	@echo "config.status is out-of-date, rechecking everything"
	./config.status --recheck

dist-clean: clean
	rm -f main/mcc_config.ml configure config.cache config.log
	find . -follow -name .consign -exec rm {} \;

cvs-uptodate: build-system
	@echo "Checking that CVS is up-to-date with current working copy"
	@echo ""
	@if [ -r CVS/Tag ]; then \
		tag_name=`cat CVS/Tag`; \
		echo "*** NOTE:  Using tag $$tag_name for distribution ***"; \
	else \
		echo "*** NOTE:  Using the trunk for distribution ***"; \
	fi
	@echo ""
	@cvs -f -Q diff > /dev/null 2>&1; \
		if [ "$$?" != "0" ]; then \
			echo "*** Working copy has changes that have not been committed ***"; \
			echo "*** Please commit ALL changes and run \`\`make'' again. ***"; \
			exit 1; \
		fi

dist: build-system cvs-uptodate
	if [ -d /tmp/$(PACKAGE) ]; then \
		rm -r /tmp/$(PACKAGE); \
	fi
	$(MKDIR) /tmp/$(PACKAGE)
	cp -a CVS /tmp/$(PACKAGE)
	@cd /tmp/$(PACKAGE); \
		echo "Fetching latest data from CVS ..."; \
		cvs -f -Q update -d -P; \
		echo "Clearing CVS directories and ignore files ..."; \
		rm -r `find . -name CVS -type d`; \
		rm `find . -name .cvsignore`; \
		cd /tmp/; \
		echo "Building tarball for $(PACKAGE) ..."; \
		env GZIP=-9 tar czf /tmp/$(PACKAGE).tar.gz $(PACKAGE)
	rm -r /tmp/$(PACKAGE)
	mv /tmp/$(PACKAGE).tar.gz .

install: all
	$(MKDIR) $(bindir)
	$(INSTALL_PROGRAM) main/mcc $(bindir)
	$(INSTALL_PROGRAM) net/migrate/mcc-migrated $(bindir)
	$(INSTALL_PROGRAM) net/migrate/mcc-resurrect $(bindir)
	if [ "$(BACKEND)" = "byte" ]; then \
		$(INSTALL_PROGRAM) arch/byte/main/mvm $(bindir); \
	fi
	if [ "$(MCC_USES_RUNTIME)" = "1" ]; then \
		$(MKDIR) $(libdir)/mcc/arch/$(BACKEND)/runtime; \
		$(INSTALL_DATA) arch/$(BACKEND)/runtime/$(BACKEND)runtime.a $(libdir)/mcc/arch/$(BACKEND)/runtime; \
	fi
	if [ "$(MP_PATH)" != "" ]; then \
		$(MKDIR) $(libdir)/mcc/languages; \
		$(INSTALL_DATA) main/languages/*.cph main/languages/*.conf $(libdir)/mcc/languages; \
	fi
	for dir in mojave shadow; do \
		$(MKDIR) $(includedir)/mcc/arch/$(BACKEND)/include/$$dir; \
		$(INSTALL_DATA) arch/$(BACKEND)/include/$$dir/*.h $(includedir)/mcc/arch/$(BACKEND)/include/$$dir; \
	done
