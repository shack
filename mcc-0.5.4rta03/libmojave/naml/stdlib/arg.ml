(*
 * Parsing command line arguments.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

type spec =
   Unit of (unit -> unit)
 | Set of bool ref
 | Clear of bool ref
 | String of (string -> unit)
 | Int of (int -> unit)
 | Float of (float -> unit)
 | Rest of (string -> unit)

exception Bad of string

(*
 * Current position on the command line.
 *)
let current = ref 1
let argv = Sys.argv
let argv_len = Array.length argv

(*
 * Find the spec.
 *)
let rec find_spec name = function
   (name', spec, _) :: tl ->
      if name' = name then
         spec
      else
         find_spec name tl
 | [] ->
      raise (Bad ("illegal option: " ^ name))

(*
 * Print the usage message.
 *)
let usage spec usage_msg =
   prerr_endline usage_msg;
   List.iter (fun (opt, spec, doc) ->
         prerr_string opt;
         (match spec with
             Unit _
           | Set _
           | Clear _ ->
                ()
           | String _ ->
                prerr_string " <arg>"
           | Int _ ->
                prerr_string " <number>"
           | Float _ ->
                prerr_string " <float>"
           | Rest _ ->
                prerr_string " ...");
         prerr_string " :";
         prerr_endline doc) spec

(*
 * Add the -help option.
 *)
let help_spec spec usage_msg =
   spec @ ["-help", Unit (fun () -> usage spec usage_msg), "display help message"]

(*
 * Parse the command line.
 *)
let next_arg () =
   if !current = argv_len then
      raise (Bad "missing argument");
   let s = argv.(!current) in
      incr current;
      s

let parse spec default usage =
   while !current < argv_len do
      let opt = argv.(!current) in
         incr current;
         if String.length opt > 0 && opt.[0] = '-' then
            try
               match find_spec opt spec with
                  Unit f ->
                     f ()
                | Set x ->
                     x := true
                | Clear x ->
                     x := false
                | String f ->
                     f (next_arg ())
                | Int f ->
                     f (int_of_string (next_arg ()))
                | Float f ->
                     f (float_of_string (next_arg ()))
                | Rest f ->
                     while !current < argv_len do
                        f argv.(!current);
                        incr current
                     done
            with
               Bad s ->
                  prerr_string "Error: ";
                  prerr_endline s;
                  exit 1
             | _ ->
                  prerr_endline "Error: uncaught exception";
                  exit 1
         else
            default opt
   done

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
