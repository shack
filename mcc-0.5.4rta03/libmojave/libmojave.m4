dnl Support functions for libmojave configure script
dnl Copyright(c) 2002 Justin David Smith
dnl
dnl WARNING:
dnl   The authoritative version of this file is in the LIBMOJAVE
dnl   CVS repository.  It should be installed in the root of any
dnl   project using libmojave, but installed copies should not be
dnl   modified.
dnl


dnl MOJAVE_FIX_PATHS()
dnl   ${prefix}/${exec_prefix} references are a bad thing to have
dnl   lying around in the various paths.  Remove them.
dnl
AC_DEFUN(MOJAVE_FIX_PATHS, [
   test "x$prefix" = "xNONE" && prefix="$ac_default_prefix"
   test "x$exec_prefix" = "xNONE" && exec_prefix="$prefix"
   eval 'bindir="'"$bindir"'"'
   eval 'libdir="'"$libdir"'"'
   eval 'includedir="'"$includedir"'"'
   TOPDIR=`cd "$top_srcdir" && pwd`
   AC_SUBST(TOPDIR)
])


dnl MOJAVE_CHECK_TYPE(TYPE [, ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND
dnl                   [, INCLUDES]]])
dnl   The version of AC_CHECK_TYPE that comes with autoconf > 2.13. This
dnl   exists here because many people are still using autoconf 2.13,
dnl   which has an incompatible (and broken) version of AC_CHECK_TYPE.
dnl
AC_DEFUN(MOJAVE_CHECK_TYPE, [
   AC_MSG_CHECKING([for type $1])
   AC_TRY_COMPILE([$4], 
      [if (($1 *)0) return 0;
       if (sizeof($1)) return 0;],
      [AC_MSG_RESULT([$1])
       ifelse([$2], , :, [$2])],
      [AC_MSG_RESULT([not found])
       ifelse([$3], , :, [$3])])
])


dnl MOJAVE_CHECK_PROG(VARIABLE, PROGRAM_LIST [, ACTION_IF_FOUND
dnl                   [, ACTION_IF_NOT_FOUND]])
dnl   Check for programs in PROGRAM_LIST.  VARIABLE will be set to the
dnl   first program in PROGRAM_LIST which exists (can be found using
dnl   AC_CHECK_PROG); if no programs are found, VARIABLE is set to an
dnl   empty string and the not-found action is taken.  In either case,
dnl   the variable will be subst'able after this call.  Note that if
dnl   ACTION_IF_NOT_FOUND=abort, then an error is generated if we do
dnl   not find the requested program.
dnl
AC_DEFUN(MOJAVE_CHECK_PROG, [
   dnl Iterate over the list until a match is found.
   dnl
   [$1]=""
   for check_program in [$2]; do
      if test "x$[$1]" = "x"; then
         AC_CHECK_PROG([$1], [$check_program], [$check_program], [])
      fi
   done

   dnl Check to see if a match was found.
   dnl
   if test "x$[$1]" = "x"; then
      ifelse([$4], abort,
         [AC_MSG_ERROR([Unable to find any of: $2])],
         [ifelse([$4], , :, [$4])])
   else
      ifelse([$3], , :, [$3])
   fi

   dnl Make the variable subst'able.
   dnl
   AC_SUBST([$1])
])


dnl MOJAVE_REQUIRE_CONS()
dnl   Make sure cons is installed and is version 2.2.0.  Will fail
dnl   with an error message if cons cannot be found.  The CONS variable
dnl   will contain the name of cons if successful.
dnl
AC_DEFUN(MOJAVE_REQUIRE_CONS, [
   MOJAVE_CHECK_PROG(CONS, cons, ,
      [AC_MSG_ERROR([cons is required for libmojave builds. Acquire it from http://www.dsmit.com/cons/])])
   $CONS -V | grep -q 'Cons 2\.2\.0'
   if test "$?" != 0; then
      AC_MSG_ERROR([cons version 2.2.0 is required for libmojave builds. Acquire it from http://www.dsmit.com/cons/])
   fi
])
      

dnl MOJAVE_CHECK_READLINE([ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND]])
dnl   Check to see if GNU readline exists in the standard path.  The
dnl   variable READLINE_RESULT is set to the result of the check. This
dnl   checks for the readline and termcap libraries, as well as the
dnl   header files readline/readline.h and readline/history.h.
dnl
dnl   This exports the READLINE_PREFIX value, which contains either
dnl   "rl_", or "", depending on whether a prefix is required on the
dnl   completion_matches function call.
dnl
dnl   If readline is found, then READLINE_CP_TYPE is set to the name
dnl   of the type for the argument to (rl_)completion_matches.
dnl
dnl   If readline is found, then READLINE_CPP_TYPE is set to the name
dnl   of the type for rl_attempted_completion_function.
dnl
AC_DEFUN(MOJAVE_CHECK_READLINE, [
   dnl Check for the --with-readline flag
   dnl Allows user to specify whether to use readline
   dnl
   AC_MSG_CHECKING([for --with-readline flag])
   AC_ARG_WITH(readline, [  --with-readline         Enable readline support [auto]], , with_readline=auto)
   
   dnl Set default value for readline prefix and types
   READLINE_PREFIX=""
   READLINE_CP_TYPE=""
   READLINE_CPP_TYPE=""
   AC_SUBST(READLINE_PREFIX)
   AC_SUBST(READLINE_CP_TYPE)
   AC_SUBST(READLINE_CPP_TYPE)

   dnl Check the result.
   dnl
   if test "x$with_readline" = "xno"; then
      dnl Do not enable readline.
      AC_MSG_RESULT([readline disabled])
      READLINE_RESULT="disabled"
      ifelse([$2], , :, [$2])
   else
      dnl Attempt to find readline
      AC_MSG_RESULT([will check for readline])
      readline_okay=1
      
      dnl Check for the required libraries
      old_libs="$LIBS"
      old_ldflags="$LDFLAGS"
      AC_CHECK_LIB(termcap, tgetent, , [readline_okay=0])
      AC_CHECK_LIB(termcap, tgetstr, , [readline_okay=0])
      AC_CHECK_LIB(readline, readline, , [readline_okay=0], [-ltermcap])

      dnl Check which version of completion_matches we must call.
      AC_CHECK_LIB(readline, rl_completion_matches, 
         [READLINE_PREFIX="rl_"],
         [AC_CHECK_LIB(readline, completion_matches, 
            [READLINE_PREFIX=""], 
            [readline_okay=0], 
            [-ltermcap])],
         [-ltermcap])
      LIBS="$old_libs"
      LDFLAGS="$old_ldflags"

      dnl Check for the required headers
      old_cppflags="$CPPFLAGS"
      AC_CHECK_HEADER(termcap.h, , [readline_okay=0])
      AC_CHECK_HEADER(readline/readline.h, , [readline_okay=0])
      AC_CHECK_HEADER(readline/history.h, , [readline_okay=0])

      dnl Check for the type of completion functions
      MOJAVE_CHECK_TYPE(rl_compentry_func_t, 
         [READLINE_CP_TYPE="rl_compentry_func_t"],
         [MOJAVE_CHECK_TYPE(CPFunction,
            [READLINE_CP_TYPE="CPFunction"],
            [readline_okay=0], 
            [#include <stdio.h>
             #include <readline/readline.h>])],
         [#include <stdio.h>
          #include <readline/readline.h>])
      MOJAVE_CHECK_TYPE(rl_completion_func_t, 
         [READLINE_CPP_TYPE="rl_completion_func_t"],
         [MOJAVE_CHECK_TYPE(CPPFunction,
            [READLINE_CPP_TYPE="CPPFunction"],
            [readline_okay=0], 
            [#include <stdio.h>
             #include <readline/readline.h>])],
         [#include <stdio.h>
          #include <readline/readline.h>])
      CPPFLAGS="$old_cppflags"
      
      dnl See if we were successful
      AC_MSG_CHECKING([whether to use readline])
      if test "x$readline_okay" = "x1"; then
         AC_MSG_RESULT([using readline])
         READLINE_RESULT="enabled"
         ifelse([$1], , :, [$1])
      else
         dnl We were unable to use any version of readline
         AC_MSG_RESULT([no suitable version found])
         READLINE_RESULT="disabled"
         if test "x$with_readline" != "xauto"; then
            AC_MSG_ERROR([Readline support was requested, but I could not find a suitable installation])
         fi
         ifelse([$2], , :, [$2])
      fi
   fi
])


dnl MOJAVE_CHECK_NCURSES([ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND]])
dnl   Check to see if ncurses is installed in the standard path.  The
dnl   variable NCURSES_RESULT is set to the result of the check. This
dnl   checks for the ncurses library, as well as the standard header
dnl   file ncurses.h.
dnl
AC_DEFUN(MOJAVE_CHECK_NCURSES, [
   dnl Check for the --with-ncurses flag
   dnl Allows user to specify whether to use ncurses
   dnl
   AC_MSG_CHECKING([for --with-ncurses flag])
   AC_ARG_WITH(ncurses, [  --with-ncurses          Enable ncurses support [auto]], , with_ncurses=auto)
   
   dnl Check the result.
   dnl
   if test "x$with_ncurses" = "xno"; then
      dnl Do not enable ncurses.
      AC_MSG_RESULT([ncurses disabled])
      NCURSES_RESULT="disabled"
      ifelse([$2], , :, [$2])
   else
      dnl Attempt to find ncurses
      AC_MSG_RESULT([will check for ncurses])
      ncurses_okay=1
      
      dnl Check for the required libraries
      old_libs="$LIBS"
      old_ldflags="$LDFLAGS"
      AC_CHECK_LIB(ncurses, initscr, , [ncurses_okay=0])
      LIBS="$old_libs"
      LDFLAGS="$old_ldflags"

      dnl Check for the required headers
      old_cppflags="$CPPFLAGS"
      AC_CHECK_HEADER(ncurses.h, , [ncurses_okay=0])
      CPPFLAGS="$old_cppflags"
      
      dnl See if we were successful
      AC_MSG_CHECKING([whether to use ncurses])
      if test "x$ncurses_okay" = "x1"; then
         AC_MSG_RESULT([using ncurses])
         NCURSES_RESULT="enabled"
         ifelse([$1], , :, [$1])
      else
         dnl We were unable to use any version of ncurses
         AC_MSG_RESULT([no suitable version found])
         NCURSES_RESULT="disabled"
         if test "x$with_ncurses" != "xauto"; then
            AC_MSG_ERROR([Ncurses support was requested, but I could not find a suitable installation])
         fi
         ifelse([$2], , :, [$2])
      fi
   fi
])


dnl MOJAVE_CHECK_OCAML([ACTION_FOR_NATIVE [, ACTION_FOR_BYTECODE]])
dnl   Check for OCaml.  This will automatically setup several environment
dnl   variables, as well as check for a few optional configure options.
dnl   It will allow the user to select bytecode or native compilation, and
dnl   configure appropriately.  The following subst'able variables are
dnl   defined:
dnl      OCAML_PROGRAM           Name of compiler
dnl      OCAML_PROGRAM_BYTE      Name of bytecode compiler (may = OCAML_PROGRAM)
dnl      OCAML_DEBUG_FLAGS       Debug flags permitted for compiler
dnl      OCAML_DEBUG_FLAGS_BYTE  Debug flags for bytecode version
dnl      OCAML_LINK_FLAGS        Link flags permitted for compiler
dnl      OCAML_LINK_FLAGS_BYTE   Link flags for bytecode version
dnl      OCAML_SUFFIX_OBJ        Suffix of object files
dnl      OCAML_SUFFIX_OBJ_BYTE   Suffix of bytecode object files
dnl      OCAML_SUFFIX_LIB        Suffix of library files
dnl      OCAML_SUFFIX_LIB_BYTE   Suffix of bytecode library files
dnl      OCAML_STD_LIB           Location of standard library
dnl      OCAML_STD_INC           Location of standard C includes
dnl      OCAML_LEX               Location of ocamllex
dnl      OCAML_YACC              Location of ocamlyacc
dnl
dnl   This will fail if ocaml cannot be found.
dnl
AC_DEFUN(MOJAVE_CHECK_OCAML, [
   dnl Check for the --enable-bytecode flag (defaults to native)
   dnl
   OCAML_NATIVE="ocamlopt.opt ocamlopt"
   OCAML_BYTECODE="ocamlc.opt ocamlc"
   AC_MSG_CHECKING([for --enable-bytecode flag])
   AC_ARG_ENABLE(bytecode, [  --enable-bytecode       Build mcc as bytecode instead of native [auto]], , enable_bytecode=auto)
   if test "x$enable_bytecode" = "xauto"; then
      dnl Attempt to infer the compiler to use; default to native.
      AC_MSG_RESULT([trying native-code, will fall back to bytecode])
      MOJAVE_CHECK_PROG(OCAML_PROGRAM, [$OCAML_NATIVE], using_native=1,
         [MOJAVE_CHECK_PROG(OCAML_PROGRAM, [$OCAML_BYTECODE], using_native=0, abort)])
   elif test "x$enable_bytecode" = "xno"; then
      dnl NOT using the bytecode compiler
      AC_MSG_RESULT([using native-code compiler])
      MOJAVE_CHECK_PROG(OCAML_PROGRAM, [$OCAML_NATIVE], using_native=1, abort)
   else
      dnl Using the bytecode version of the compiler
      AC_MSG_RESULT([using bytecode compiler])
      MOJAVE_CHECK_PROG(OCAML_PROGRAM, [$OCAML_BYTECODE], using_native=0, abort)
   fi

   dnl Set extensions and default flags, depending on whether the
   dnl native-code or bytecode compiler should be used by default.
   dnl
   if test "x$using_native" = "x1"; then
      dnl Using the native code compiler
      OCAML_DEBUG_FLAGS=""
      OCAML_LINK_FLAGS=""
      OCAML_SUFFIX_OBJ=".cmx"
      OCAML_SUFFIX_LIB=".cmxa"
   else
      dnl Using the bytecode version of the compiler
      OCAML_DEBUG_FLAGS="-g"
      OCAML_LINK_FLAGS="-custom"
      OCAML_SUFFIX_OBJ=".cmo"
      OCAML_SUFFIX_LIB=".cma"
   fi

   dnl Check for ocaml bytecode compiler; OCAML_PROGRAMS tells us what
   dnl to look for.  Note the preferred compiler choice should be
   dnl listed FIRST.
   dnl
   MOJAVE_CHECK_PROG(OCAML_PROGRAM_BYTE, [$OCAML_BYTECODE], , abort)
   OCAML_DEBUG_FLAGS_BYTE="-g"
   OCAML_LINK_FLAGS_BYTE="-custom"
   OCAML_SUFFIX_OBJ_BYTE=".cmo"
   OCAML_SUFFIX_LIB_BYTE=".cma"

   dnl Search for the OCaml standard library
   dnl
   AC_MSG_CHECKING([for OCaml standard library])
   OCAML_STD_LIB=`$OCAML_PROGRAM -where`
   if test ! -d "$OCAML_STD_LIB"; then
      dnl Cannot find standard library.
      AC_MSG_RESULT([not found])
      AC_MSG_ERROR([Expected OCaml standard library at $OCAML_STD_LIB, not found])
   fi
   AC_MSG_RESULT([$OCAML_STD_LIB])

   dnl Search for the OCaml C header files
   dnl
   AC_MSG_CHECKING([for OCaml standard C headers])
   OCAML_STD_INC="$OCAML_STD_LIB"
   if test ! -r "$OCAML_STD_INC/caml/config.h"; then
      dnl Check if we need to append ocaml to the INC.
      if test -r "$OCAML_STD_INC/ocaml/caml/config.h"; then
         OCAML_STD_INC="$OCAML_STD_INC/ocaml"
      else
         dnl Last try: search /usr/include/caml and see if it's there.
         if test -r "/usr/include/caml/config.h"; then
            OCAML_STD_INC="/usr/include"
         else
            dnl Cannot find C include files.
            AC_MSG_RESULT([not found])
            AC_MSG_ERROR([Expected OCaml standard C headers at $OCAML_STD_INC, not found])
         fi
      fi
   fi
   AC_MSG_RESULT([$OCAML_STD_INC])

   dnl Check for ocaml lexer. The preferred program should be listed FIRST.
   dnl
   MOJAVE_CHECK_PROG(OCAML_LEX, [ocamllex.opt ocamllex], , abort)
   
   dnl Check for ocaml parser. The preferred program should be listed FIRST.
   dnl
   MOJAVE_CHECK_PROG(OCAML_YACC, [ocamlyacc.opt ocamlyacc], , abort)

   dnl Subst the major variables generated here.
   dnl
   AC_SUBST(OCAML_DEBUG_FLAGS)
   AC_SUBST(OCAML_LINK_FLAGS)
   AC_SUBST(OCAML_SUFFIX_OBJ)
   AC_SUBST(OCAML_SUFFIX_LIB)
   AC_SUBST(OCAML_DEBUG_FLAGS_BYTE)
   AC_SUBST(OCAML_LINK_FLAGS_BYTE)
   AC_SUBST(OCAML_SUFFIX_OBJ_BYTE)
   AC_SUBST(OCAML_SUFFIX_LIB_BYTE)
   AC_SUBST(OCAML_STD_LIB)
   AC_SUBST(OCAML_STD_INC)

   dnl Run the final action, depending on if we are native-code or
   dnl bytecode (the native-code action is listed first).
   dnl
   if test "x$using_native" = "x1"; then
      ifelse([$1], , :, [$1])
   else
      ifelse([$2], , :, [$2])
   fi
])


dnl MOJAVE_CHECK_CAMLP4([ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND]])
dnl   Check for a version of camlp4/camlp4o/camlp4r.  This will setup
dnl   several environment variables if it finds camlp4, and run the
dnl   actions specified in ACTION_IF_FOUND.  If camlp4 cannot be found
dnl   then we run the actions in ACTION_IF_NOT_FOUND.  If camlp4 is
dnl   found, then the following are set:
dnl      CAMLP4_PROGRAM       Name of camlp4
dnl      CAMLP4O_PROGRAM      Name of camlp4o
dnl      CAMLP4R_PROGRAM      Name of camlp4r
dnl      CAMLP4_STD_LIB       Location of camlp4 files
dnl
AC_DEFUN(MOJAVE_CHECK_CAMLP4, [
   dnl Check for camlp4 program.
   dnl
   found_camlp4=1
   MOJAVE_CHECK_PROG(CAMLP4_PROGRAM,  [camlp4],  , [found_camlp4=0])
   MOJAVE_CHECK_PROG(CAMLP4O_PROGRAM, [camlp4o], , [found_camlp4=0])
   MOJAVE_CHECK_PROG(CAMLP4R_PROGRAM, [camlp4r], , [found_camlp4=0])

   dnl If we actually found camlp4, then search for the standard library
   dnl for camlp4 files.
   dnl
   AC_MSG_CHECKING([for camlp4 standard library])
   if test "x$found_camlp4" = "x1"; then
      CAMLP4_STD_LIB=`$CAMLP4_PROGRAM -where`
      if test ! -d "$CAMLP4_STD_LIB"; then
         dnl Cannot find standard library.
         AC_MSG_RESULT([not found])
         AC_MSG_ERROR([Expected camlp4 standard library at $CAMLP4_STD_LIB, not found])
      fi
      AC_MSG_RESULT([$CAMLP4_STD_LIB])
   else
      dnl Without camlp4, there is no standard library
      CAMLP4_STD_LIB=""
      AC_MSG_RESULT([no camlp4, skipping])
   fi

   dnl Subst our result variables.
   dnl
   AC_SUBST(CAMLP4_STD_LIB)

   dnl Run the final action, depending on if we found camlp4.
   dnl
   if test "x$found_camlp4" = "x1"; then
      ifelse([$1], , :, [$1])
   else
      ifelse([$2], , :, [$2])
   fi
])


dnl MOJAVE_CHECK_LIBMOJAVE([ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND]])
dnl   Searches for the uninstalled source tree of libmojave.  The
dnl   variable LIBMOJAVE_RESULT is set to the result of the check.
dnl
dnl   This checks for the source files in libmojave, based on the
dnl   value of --with-libmojave, and barring that, in ../libmojave.
dnl   If libmojave is found, then LIBMOJAVE_PATH will be set to the
dnl   base directory containing libmojave.
dnl
dnl   This automatically checks for readline and ncurses support,
dnl   and exports two boolean variables USE_READLINE and USE_NCURSES.
dnl
AC_DEFUN(MOJAVE_CHECK_LIBMOJAVE, [
   dnl libmojave allows for ncurses and readline support.  If the
   dnl user is checking for libmojave, go ahead and perform the
   dnl checks for ncurses and readline as well.
   MOJAVE_CHECK_READLINE([USE_READLINE=1], [USE_READLINE=0])
   AC_SUBST(USE_READLINE)
   MOJAVE_CHECK_NCURSES([USE_NCURSES=1], [USE_NCURSES=0])
   AC_SUBST(USE_NCURSES)

   dnl Check for the --with-libmojave flag
   AC_MSG_CHECKING([for --with-libmojave flag])
   AC_ARG_WITH(libmojave, [  --with-libmojave=<path> Enable libmojave with source path [auto]], , with_libmojave=auto)
   
   dnl Set default values for libmojave
   LIBMOJAVE_RESULT=""
   LIBMOJAVE_PATH=""
   AC_SUBST(LIBMOJAVE_PATH)
   
   dnl Check to see what action the user wants.
   if test "x$with_libmojave" = "xno"; then
      AC_MSG_RESULT([disabled by user])
   elif test "x$with_libmojave" = "xyes" -o "x$with_libmojave" = "xauto"; then
      LIBMOJAVE_PATH="search"
      AC_MSG_RESULT([will search for libmojave])
   else
      LIBMOJAVE_PATH="$with_libmojave"
      AC_MSG_RESULT([using $LIBMOJAVE_PATH])
   fi

   dnl If enabled, search for libmojave source files
   libmojave_test_file="util/symbol.ml"
   if test "x$LIBMOJAVE_PATH" != "x"; then
      AC_MSG_CHECKING([for libmojave source files])
      
      dnl Do we need to search for libmojave?
      if test "x$LIBMOJAVE_PATH" = "xsearch"; then
         dnl The FIRST path listed here wins.
         for libmojave_search_path in ./libmojave ../libmojave; do
            if test "x$LIBMOJAVE_PATH" = "xsearch"; then
               if test -r "$libmojave_search_path/$libmojave_test_file"; then
                  AC_MSG_RESULT([found in $libmojave_search_path])
                  LIBMOJAVE_PATH="$libmojave_search_path"
               fi    dnl Did we find libmojave in this path?
            fi       dnl Continue searching for libmojave?
         done        dnl Loop to search through "known" paths.
         
         dnl If LIBMOJAVE_PATH remains unchanged, then we didn't find it.
         if test "x$LIBMOJAVE_PATH" = "xsearch"; then
            AC_MSG_RESULT([cannot find source files])
            LIBMOJAVE_PATH=""
         fi

      dnl Otherwise the user specified where search for libmojave.
      elif test -r "$LIBMOJAVE_PATH/util/symbol.ml"; then
         AC_MSG_RESULT([ok])
      else
         AC_MSG_RESULT([cannot find source files])
         LIBMOJAVE_PATH=""
      fi    dnl Did we succeed in finding libmojave?
   fi       dnl Did the user ask for libmojave support?
   
   dnl Determine final action
   AC_MSG_CHECKING([whether to use libmojave])
   if test "x$LIBMOJAVE_PATH" != "x"; then
      AC_MSG_RESULT([using libmojave])
      LIBMOJAVE_RESULT="enabled"
      ifelse([$1], , :, [$1])
   else
      dnl We were unable to use any version of libmojave
      AC_MSG_RESULT([no suitable version found])
      LIBMOJAVE_RESULT="disabled"
      if test "x$with_libmojave" != "xauto"; then
         AC_MSG_ERROR([Readline support was requested, but I could not find a suitable installation])
      fi
      ifelse([$2], , :, [$2])
   fi
])
