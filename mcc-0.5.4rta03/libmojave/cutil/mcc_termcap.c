/*
 * Simple Termcap Interface for ML
 * Copyright(c) 2002 Justin David Smith, Caltech
 */
#include <stdlib.h>
#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>


#ifdef READLINE


/* Headers which are readline-specific must be included here. */
#include <termcap.h>


static int loaded_terminfo = 0;

static int load_terminfo() {

   char *termname = NULL;

   /* Check to see if we already loaded the terminal data */
   if(loaded_terminfo) return(0);

   /* We haven't loaded anything yet (or we had an error). */
   termname = getenv("TERM");
   if(termname != NULL && tgetent(NULL, termname) == 1) {
      /* We were successful! */
      loaded_terminfo = 1;
      return(0);
   }

   /* Failure. */
   return(-1);

}

#endif /* READLINE support? */


/*
 * Read the indicated termcap by string.
 */
value caml_tgetstr(value id) {

   CAMLparam1(id);
   CAMLlocal1(result);
   char *termdata = NULL;

   /* Lookup the requested capability name.  Note that we only get termcap
      if we compiled with readline support; otherwise it will not be linked
      in.  */
#ifdef READLINE
   if(load_terminfo() == 0) {
      termdata = tgetstr(String_val(id), NULL);
   }
#endif
   if(termdata == NULL) {
      result = copy_string("");
   } else {
      result = copy_string(termdata);
      free(termdata);
   }

   /* Return the result */
   CAMLreturn(result);

}
