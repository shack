#  Helper rules for makefiles within the tree.
#  Copyright(c) 2002 Justin David Smith, Caltech
#


all:
	@cons -t force_bytecode=$(force_bytecode) $(TARGET) $(addsuffix $(SUFLIB),$(TARGETLIB))

clean: clean-rec
	@echo "Using cons to clear any remaining targets"
	@cons -t -r .

clean-rec: clean-local
	@for subdir_make in `ls */Makefile .??*/Makefile 2> /dev/null`; do \
		if [ "$$subdir_make" != "source/Makefile" ]; then \
			subdir=$${subdir_make%/Makefile}; \
			$(MAKE) -C $$subdir SUFLIB=$(SUFLIB) clean-rec || exit 1; \
		fi \
	done

clean-local:
	@rm -f *.o *.cmo *.cmi *.cmx *.a *.cmxa *.cma *~ .*~ *.bak .#* *.cph
	@if test "x$(TEST)" != "x"; then \
		echo "Extensive clean for test directory"; \
		rm -f *.exec *.o *.mo *.fo *.fir *.mir *.out *.s *.err *.pgm *.i a.out; \
	fi
	@if test "x$(TEXFILES)" != "x"; then \
		echo "Extensive clean for tex directory"; \
		rm -f *.aux *.dvi *.log *.idx *.ilg *.ind *.kfig *.toc *.out *.blg *.bbl *.brf *.lof; \
	fi
	@rm -f $(EXTRACLEAN)

doc: doc-rec

doc-rec: doc-local
	@for subdir_make in `ls */Makefile .??*/Makefile 2> /dev/null`; do \
		if [ "$$subdir_make" != "source/Makefile" ]; then \
			subdir=$${subdir_make%/Makefile}; \
			$(MAKE) -C $$subdir SUFLIB=$(SUFLIB) doc-rec || exit 1; \
		fi \
	done

doc-local:
	@if test "x$(TEXFILES)" != "x"; then \
		$(MAKE) $(TEXFILES); \
	fi

test:
	@if test "x$(TEST)" != "x"; then \
		cons -t -k $(TEST); \
	fi

.SUFFIXES: .tex .dvi .ps .fig .pdf .eps

.tex.dvi:
	latex $<
	[ "x$(ENABLEBIBTEX)" != "xyes" ] || bibtex $*
	latex $<
	[ ! -r $*.idx ] || makeindex $*.idx
	@echo ""
	@echo ""
	@echo "*** Final pass ***"
	@echo ""
	@echo ""
	latex $<

.dvi.ps:
	dvips $< -o $@ -t letter

.tex.pdf:
	pdflatex $<
	[ "x$(ENABLEBIBTEX)" != "xyes" ] || bibtex $*
	pdflatex $<
	[ ! -r $*.idx ] || makeindex $*.idx
	@echo ""
	@echo ""
	@echo "*** Final pass ***"
	@echo ""
	@echo ""
	pdflatex $<

.fig.eps:
	fig2dev -L eps $< $@

.fig.pdf:
	fig2dev -L pdf $< $@
