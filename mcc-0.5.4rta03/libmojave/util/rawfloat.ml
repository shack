(*
   Support for raw floating point values
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)

(* Various forms of raw floats *)
type float_precision =
   Single
 | Double
 | LongDouble

type rawfloat = float_precision * Float80.float80

(*
 * Operations.
 *)
let precision (pre, _) =
   pre

(*
 * Conversions.
 *)
let to_string (_, x) =
   Float80.to_string x

let to_float (_, x) =
   Float80.to_float x

let to_float80 (_, x) =
   x

let to_int64 (_, x) =
   Float80.to_int64 x

let to_int (_, x) =
   Float80.to_int x

let to_rawint p s (_, x) =
   Rawint.of_int64 p s (Float80.to_int64 x)

let of_int pre x =
   pre, Float80.of_int x

let of_float pre x =
   pre, Float80.of_float x

let of_float80 pre x =
   pre, x

let of_rawfloat pre (_, x) =
   pre, x

let of_rawint pre i =
   let j = Rawint.to_int64 i in
   let signed = Rawint.signed i in
   let x = Float80.of_int64 j in
   let x =
      if not signed && j < Int64.zero then
         Float80.neg x
      else
         x
   in
      pre, x

let of_string pre s =
   pre, Float80.of_string s

(*
 * Arithmetic.
 *)
let neg (pre, x) = (pre, Float80.neg x)
let abs (pre, x) = (pre, Float80.abs x)
let sin (pre, x) = (pre, Float80.sin x)
let cos (pre, x) = (pre, Float80.cos x)
let sqrt (pre, x) = (pre, Float80.sqrt x)
let add (pre, x) (_, y) = (pre, Float80.add x y)
let sub (pre, x) (_, y) = (pre, Float80.sub x y)
let mul (pre, x) (_, y) = (pre, Float80.mul x y)
let div (pre, x) (_, y) = (pre, Float80.div x y)
let rem (pre, x) (_, y) = (pre, Float80.rem x y)
let atan2 (pre, x) (_, y) = (pre, Float80.atan2 x y)
let succ (pre, x) = (pre, Float80.succ x)
let pred (pre, x) = (pre, Float80.pred x)

(*
 * Comparison.
 *)
let is_zero (_, x) =
   Float80.is_zero x

let compare (_, x) (_, y) =
   Float80.compare x y

let min x y =
   if compare x y < 0 then
      x
   else
      y

let max x y =
   if compare x y > 0 then
      x
   else
      y
