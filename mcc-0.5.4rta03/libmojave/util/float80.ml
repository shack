(*
   Support for 80-bit floating point values
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Float80 type definition *)
type float80 = string      (* TEMP: fix this so it is better *)


(* Float80 coercions *)
external to_string : float80 -> string = "string_of_float80"
external to_float  : float80 -> float  = "float_of_float80"
external to_int    : float80 -> int    = "int_of_float80"
external of_string : string -> float80 = "float80_of_string"
external of_float  : float -> float80  = "float80_of_float"
external of_int    : int -> float80    = "float80_of_int"
external format    : string -> float80 -> string = "float80_format"


(* Float80 arithmetic functions *)
external neg     : float80 -> float80 = "float80_neg"
external abs     : float80 -> float80 = "float80_abs"
external sin     : float80 -> float80 = "float80_sin"
external cos     : float80 -> float80 = "float80_cos"
external sqrt    : float80 -> float80 = "float80_sqrt"
external add     : float80 -> float80 -> float80 = "float80_add"
external sub     : float80 -> float80 -> float80 = "float80_sub"
external mul     : float80 -> float80 -> float80 = "float80_mul"
external div     : float80 -> float80 -> float80 = "float80_div"
external atan2   : float80 -> float80 -> float80 = "float80_atan2"
external compare : float80 -> float80 -> int = "float80_compare"


(* Constants *)
let zero       = of_float 0.0
let one        = of_float 1.0
let minus_one  = of_float ~-.1.0


(* Implemented functions *)
let to_int64 f = Int64.of_float (to_float f)
let of_int64 i = of_float (Int64.to_float i)
let rem x y    = raise (Failure "Not supported: Float80.rem")
let succ f     = add f one
let pred f     = sub f one
let is_zero f  = (compare f zero) = 0
let min x y    = if compare x y < 0 then x else y
let max x y    = if compare x y > 0 then x else y

(* Check buffer bounds *)
external c_blit_float32 : float80 -> string -> int -> unit = "c_blit_float32"
external c_blit_float64 : float80 -> string -> int -> unit = "c_blit_float64"
external c_blit_float80 : float80 -> string -> int -> unit = "c_blit_float80"

let blit_float32 x buf off =
   let len = String.length buf in
      if off + 4 > len then
         raise (Invalid_argument "blit_float32");
      c_blit_float32 x buf off

let blit_float64 x buf off =
   let len = String.length buf in
      if off + 8 > len then
         raise (Invalid_argument "blit_float64");
      c_blit_float64 x buf off

let blit_float80 x buf off =
   let len = String.length buf in
      if off + 10 > len then
         raise (Invalid_argument "blit_float80");
      c_blit_float80 x buf off
