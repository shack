1) All elements of tuples are assumed to be 4 bytes long.
2) Logical subscripts on tuples are calculated as a "byte offset" / 4,
   and this "byte offset" can be unaligned, i.e. not a multiple of 4.
