% Notation for upcoming pointers...
\def\ptrbase#1{\ms{base}[#1]}
\def\ptrlevel#1{\ms{level}[#1]}
\def\ptrcurrent{\ms{current}}
\def\ptrlimit{\ms{limit}}
\def\ptrptablename{\ms{ptable}}
\def\ptrptable#1{\ptrptablename[#1]}
\def\ptrfuntablename{\ms{funtable}}
\def\ptrfuntable#1{\ptrfuntablename[#1]}
\def\ptrpdiffname{\ms{pdiff}}
\def\ptrpdiff#1{\ptrpdiffname[#1]}



%
%  Backend features (safety, migration, transactions)
%
\sectionA{Runtime Implementation}
\label{section:backend}

\nocomment{JYH: The following sections are a rewrite of Justin's text.
  I'm preserving Justin's text, at Section~\ref{section:justins}, and
  we'll migrate his text into this section.}

The \fir{} is machine-independent, and the \mojave{} compiler
architecture is designed to support multiple back-ends, including both
native-code and interpreted runtimes.
\iftechreport
Currently, our main runtime
implementation is a native-code runtime for the Intel IA32
architecture.
\fi
Object code generation is performed in two stages: the FIR is
first translated to a ``Machine Intermediate Representation'' (MIR),
which introduces runtime safety checks in a machine-independent form,
and then the final object code is generated for the target architecture from the MIR
program.  We do not discuss the MIR language in detail here; the
language itself is similar to the \fir{} with a simpler type system, and the process 
of generating MIR code is a straightforward elaboration of the \fir{} code.

The runtime implementation manages several tasks, including execution
of runtime type-checks for subscript operations, garbage collection,
process migration, and atomic transactions.  To complicate matters, a
faithful C pointer semantics rules out direct use of data relocation
(which occurs when a process migrates, or during heap compaction).  To
address these matters we introduce several auxiliary data structures
and invariants.

\sectionB{Runtime data structures and invariants}

The runtime consists of the following parts and invariants.

\begin{itemize}
\setlength\itemsep{0in}
\item A \emph{heap}, containing the data for tuples, arrays, unions,
\iftechreport
   rawdata, and frames.
\else
   and rawdata.
\fi
   A data value in the heap is called a \emph{block}, and the heap
   contains multiple (possibly non-contiguous) blocks.

\item A \emph{text area}, containing the program code.  The text area is
   immutable at all times except during process migration.

\item A set of \emph{registers}.  Each variable in the
   program is assigned to a register.  At any time during program
   execution, a register may contain a value in one of several machine
   types: a pointer into the heap, a function pointer, or a numerical
   value.  The machine type is statically determined from the
   variable's type in the \fir.  Register spills have the same
   properties as registers.

   \mb{Invariant}: if a register contains a pointer, it contains the
   address of a block in the heap; if a register contains a function
   pointer, it contains the address of a function entry point in
   the text area.

\item A \emph{pointer table}, containing pointers to all valid data
   blocks in the heap.

   \mb{Invariant}: all non-empty entries in the pointer table contain
   pointers to valid blocks in the heap, and every block in the heap has an entry in the
   pointer table.

\item A \emph{function table}, containing function pointers to all
  valid higher-order functions.  The function table is immutable,
  except during process migration.

  \mb{Invariant}: all entries in the function table contain the address of
  a function entry point in the text area.   

\item A \emph{checkpoint} record, containing descriptions of all live
  program checkpoints.  Checkpoints are discussed in
  Section~\ref{section:transactions}.
\end{itemize}

\sectionC{Data blocks and the heap}

The heap represents the \fir{} store, and it contains the store values
$b$ defined in Figure~\ref{figure:store-values}, which we call
\emph{blocks}.  The runtime representation of a block contains two parts:
a header that describes the size and type of information stored in the
block, and a value area containing the contents of the block.

There are two types of data blocks in the heap.  Unsafe data
corresponds to values of type
\iftechreport
   $\tyrawdata$ and $\tyframe{\tyvar}{\cvec{t\subi}{\polybase}{\varbound}}$.
\else
   $\tyrawdata$.
\fi
Safe data is type-safe ML data, and corresponds to the
$\tytuple{\tupleclass}{\cvec{t\subi}{\polybase}{\varbound}}$, $\tyarray{t}$, and
$\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\varbound}}{\intset}$ types.

The contents of unsafe data are not explicitly typed in the \fir,
and safety checks are required to ensure the data is interpreted
properly.  Any pointer read from an unsafe block must be checked to
ensure it is a valid pointer, and the bounds must be checked any
time an unsafe block is dereferenced.  In contrast, the contents of
safe block data are typed in the \fir, and many safety checks can be
omitted\footnote{Safety checks cannot be omitted on data after a
successful migration, unless the two machines are mutually trusting.
By default, the destination machine generates safety checks on all data\iftechreport{}
unless the original program was compiled with the \tt{-unsafe} compiler option\fi.}.
The garbage collector can use explicit \fir{} types to identify pointers 
embedded in safe block data, but it must use a more conservative 
algorithm to determine which values are pointers in unsafe block data\footnote{As
a consequence, the garbage collector may consider certain blocks to be live
beyond their real live range.}.

The pointer table contains the address of each valid live block in the
heap.  A block header has three parts: it contains 1) a tag that
identifies the block type (unsafe or safe), 2) an index into the
pointer table identifying the pointer for this block, and 3) a
nonnegative number that indicates the size of the block.  The tag field
is overloaded to indicate the union case for data in a disjoint union.
The header also contains bits used by garbage collection and transactions.

\iftechreport
A null pointer is always a valid pointer to a zero-size block.  A null
pointer is allocated an entry in the pointer table and may be
manipulated like any other base pointer, but any attempt to
dereference it will result in a runtime exception.  Certain
source-level languages require a different null pointer for various
data types\footnote{For example, in Java there are different null
pointers for each object type, which are not considered equivalent.};
for these languages, a different zero-size block is allocated in
advance for each type.
\fi

\sectionC{Pointer table}
\label{section:ptbl}

The pointer table $\ptrptablename$ effectively acts as a segment table for the
blocks (segments) in the heap.  It supports several features,
including migration and transactions, but its main purpose is to allow for
relocation and safety for C data areas.  The pointer table is
implemented in software, however its design is compatible with a
hardware implementation for increased efficiency.

The pointer table contains an entry pointing to all allocated data
blocks.  Source-level C pointers are represented in the runtime as
(base + offset) pairs.  The base pointer always points to the
beginning of a data block in the heap, and the offset is a signed byte
index relative to the base.  Base pointers are never stored directly
in the heap.  Instead, the base pointer is stored as an index to an
entry in the pointer table, which contains the actual address of the
beginning of the data block.

\makefigure{figure:ptable}{Pointer table representation}{
   \begin{center}
\iftechreport
   \includegraphics{ptable2}
\else
   \includegraphics[scale=0.5]{ptable2}
\fi
   \end{center}
}

The pointer table serves several purposes.  First, it provides a simple
mechanism for identifying and validating data pointers in aggregate
blocks. When an index $i$ for a base pointer is read from the heap, the
following steps are performed:
\begin{enumerate}
\setlength\itemsep{0in}
\item $i$ is checked against the size of the pointer table to verify if it is a valid index.
\item The value $p$ is read from the $i^\ms{th}$ entry in the pointer table.
\item $p$ is checked to ensure it is not free (that it points into the
heap)%
\iftechreport
\footnote{Empty pointer table entries are always odd values, whereas valid heap pointers are always
      even values.}.
\else
.
\fi
\end{enumerate}

After these steps, $p$ is always a valid pointer to the beginning of a block.
For additional safety, we can verify that the index stored in $p$'s block header matches
index $i$.  These steps can be performed in a small number of assembly instructions,
requiring only two branch points.

The second purpose of the pointer table is to support relocation.  If
the heap is reorganized by garbage collection or process migration, the
pointer table (and registers) are updated with the new locations, but
the heap values themselves are preserved.  This level of transparency
has a cost: in addition to the execution overhead, the header of each
block in the heap contains an index.  In the IA32 runtime, if we include
the pointer table overhead, the overhead is in excess of 12 bytes per
block.

\sectionC{Function pointers}

Function pointers are managed through the function table $\ptrfuntablename$ that serves
the same purpose as the pointer table for heap data.  A function stub
must be generated for each \fir{} function that escapes (higher-order functions)%
\iftechreport
\footnote{An
escaping value is any value which is passed by reference to a function, or whose address
is taken.  An escaping function is a function whose address is taken and stored in a
function pointer.}%
\fi.
Each function stub has a function
header, and the function table contains the addresses of all the
escaping-function headers.  To ease some of the runtime safety checks,
each function stub is formatted with a block header that indicates
that the function is a data block with zero-size.  As with block
pointers, function pointers are represented in the heap as indexes
into the function table.

The function header also contains an arity tag, used to describe the
types of the arguments.  Arity tags are used when a function is called
to ensure that a function is called arguments that are compatible with
the function's signature\footnote{This must be checked at runtime
since C permits function pointers to be coerced arbitrarily.}.  The
arity tags are integer identifiers, computed at link time from the
function signatures.  The signatures themselves are generated based on
the primitive architecture types, not the high-level FIR
types\footnote{The primitive architecture types are currently
\tt{value}, \tt{pointer aggr}, \tt{pointer block}, \tt{poly}, and
\tt{function $\diamond$}.}.  When a function is called,
the arguments have the same arity tag as the function signature,
or the runtime raises an exception.

\iftechreport
Functions may be invoked in a tail-call directly by specifying the
function name, or indirectly through a function pointer.  Unlike
direct function calls, indirect calls through a function pointer use a
standardized calling convention.  The escape stub is responsible for
moving arguments from the standard registers to the registers expected
by the original function.
\fi

\sectionC{Pointer safety}

\nocomment{Emre: What happened to frames? And what are $ptable$  and $funtable$?
         Okay, so it's not to hard to actually guess what these are, but
         perhaps an explicit note is in order?}

The runtime operations for load
$\runtimeload{\genv}{\storedata{\xdata}}{i}{t}$ and store
$\runtimestore{\genv}{\storedata{\xdata}}{i}{t}{h}$ are
guaranteed to be type-safe, even for unsafe blocks.  The runtime
safety check for a load operation is performed as follows.

\begin{enumerate}
\setlength\itemsep{0in}
\item The index $i$ is compared with the bounds of block
$\storedata{\xdata}$; an exception is raised if the index is
out-of-bounds.
\item The value $h$ at location $i$ is retrieved, and a safety check is
performed.
\begin{itemize}
\item If $t$ represents a pointer, then $h$ should be an index into
the pointer table.  If $h$ is a valid pointer table index, and the
entry $\ptrptable{h}$ is a valid pointer $p$, the result of the load is
$p$.
\item If $t$ represents a function pointer, then $h$ should be an
index into the function table.  If $h$ is a value function table
index, the result of the load is $\ptrfuntable{h}$.
\item Otherwise, $h$ does not represent a pointer, and the result of
the load is $h$.
\end{itemize}
\end{enumerate}

The safety check for a store operation is somewhat simpler.  For a
store operation $\runtimestore{\genv}{\storedata{\xdata}}{i}{t}{h}$,
the runtime invariants guarantee that if $t$ represents a pointer,
then $h$ is a valid pointer to a block in the heap; and if $t$ is of
function type, then $h$ is a valid pointer to a function header.  In
these two cases (after a bounds-check on the index $i$) the index for
$h$ is stored.  If $t$ does not represent either kind of pointer, the
value $h$ is stored directly.

\sectionB{Process migration}
\label{section:migration}

\nocomment{JDS OVERVIEW:
         Discuss migration.  Mention how we get away with architecture
         independence; standardized heap (byte order, sizes and alignment),
         no pointers in heap, pointer table to migrate blocks.  Special
         constraints for migrating context.  Need to discuss how we deal with
         register values (dump to heap).}

\nocomment{JDS:  Would like to include references to some prior work on migration here;
         alas I did not bring any with me :(}

\iftechreport
To facilitate fault-tolerant computing, we would like to introduce a level of
abstraction between the processes that are running in a distributed
system, and the specific machines they are running on.  Each process
should see the distributed cluster as a single machine and a single
shared resource pool, rather than a collection of distinct nodes each
with their own set of resources.  Processes should have a lifetime greater
than that of any particular machine in the cluster, so that the process
continues running even when one or many machines it is running on fail.
\fi

In a distributed system, a process will execute on a specific
machine with a particular architecture.  Since individual nodes in a
cluster may fail at any time, a mechanism for migrating a process from
one machine to another is an essential tool for fault-tolerance.  Such
a mechanism needs to perform three operations: a \emph{pack} operation
to capture the entire state of the process, including the program
counter, all register values, heap data, and code; a \emph{transmit}
operation to transmit the state of the process to a target machine;
and an \emph{unpack} operation to reconstruct the process state on the
target machine and resume execution.  Collectively, this sequence of
operations is referred to as \emph{process migration}.

Process migration should be architecture-independent, to allow
for distributed clusters of heterogeneous nodes.  Also, process
migration should be safe; the remote machine receiving the program
should be able to verify that the program type-checks and
that heap values are used in a proper manner.  If the remote machine
can verify that a received program is safe, then we can use process
migration in environments where machines in the cluster do not trust
each other entirely, such as the wide-area computing clusters on the
Internet.

Note that since process migration requires \emph{pack} and
\emph{unpack} operations, it is fairly straightforward to extend the mechanism to
support saving the process state to a file for later execution,
and to write checkpoint files while the process is running that
contain snapshots of the full process state.  In the event of a later
failure, the process can be recovered from this file using the 
\emph{unpack} operation.

\iftechreport
\sectionC{Using process migration in the FIR}
\label{section:migration-protocols}

In the FIR, migration is expressed using the special-call mechanism. 
Special-calls are similar to tail-calls, taking a function name and argument
list, but a special-call implies some special action is performed before the
function is invoked.  Since many FIR transformations are not concerned with the 
special action, these transformations may treat a special-call exactly as a
tail-call, simplifying their implementation.

% JDS: hyphenation explicitly indicated for special-call to 
% prevent an overfull hbox.
Process migration is expressed in the \fir{} using the spe\-cial-call
$\tailsysmigrate{i}{a_\ms{ptr}}{a_\ms{off}}{f}{\cvec{a\subi}{\polybase}{\varbound}}$.  
The first three arguments indicate how the migration
should be performed, and are not passed as arguments to $f$.  The
argument $i$ is an integer, unique among all migration calls in the
program. It is used by the backend to determine where program
execution resumes after a successful migration.  The pointer
$(a_\ms{ptr}, a_\ms{off})$ refers to a null-terminated string that
determines the migration target.
The string includes information on what protocol to use.

There are three protocols that may be used for process migration.  The
first is the \tt{migrate} protocol, which will actually send the entire
state of a process to another machine, and terminate the process on the
original machine.  If migration fails for any reason, the process will
continue to execute on the original machine.  The process has no way of
directly determining what machine it is running on, or observing a successful
migration\footnote{In future implementations, we may provide an external
function which reports on the status of a prior migration attempt. 
Also, a process may indirectly observe the result of a migration since
we permit direct linking to \tt{libc} functions.}; this encourages an
abstraction between the process and the machine it is running on.  The
fact that the process does not observe the result of migration is
reflected in the operational semantics.

In order to migrate to another machine, the remote machine must run
the migration server, \tt{mcc-migrated}.  This is a version of the
compiler which will listen for incoming migration requests, recompile
any inbound processes on the new machine, and reconstruct their state
before executing them.

The other two protocols write the process state to a file for later
execution.  The \tt{suspend} protocol writes the process state to a
file, and terminates the process if it is successfully written.  In
contrast, the \tt{checkpoint} protocol will continue running
the process even when the file is successfully written.  The latter
protocol is useful for taking snapshots of a process state, as a crude
rollback mechanism or in anticipation of possible machine failure in
the near future.  The \tt{mcc-resurrect} program is used to resume
process execution from a state file.
\fi

\sectionC{Runtime support for migration}

\iftechreport
Process migration requires two key features from the runtime.  First,
to support the \emph{pack} operation, the runtime must be able to
collect the entire process state; for \emph{unpack}, it must be able
to restore the process state from a previous \emph{pack} operation.
Second, the migration should be architecture-independent.
\fi

The implementation of the \emph{pack} and \emph{unpack} operations is relatively
straightforward.  Since all heap data and function pointers in the
heap are represented indirectly as indices, the heap data is not
modified by a migration, even if the data are relocated.  The
\emph{pack} operation first performs garbage collection and then packs
the live data in the heap, the pointer table, the program text, and
the registers into a message that can be stored or transmitted.

In order to achieve architecture independence, we never
migrate the actual executable text\footnote{As a future optimization, we
may include support for migrating executable code when the
architecture is known.  Such an optimization would compromise program
safety however, since it is not trivial to verify the correctness of
assembly code.}.  Instead we migrate the \fir{} code for the program.
The location index $i$ in the migration call is used to correlate the
runtime execution point with a corresponding execution point in the
\fir.  On an \emph{unpack} operation, the \fir{} code is type-checked,
recompiled, and execution is resumed.  Note that for C programs, the
size and byte-ordering of the various data types
must conform to a uniform standard.

It is possible to migrate values stored in hardware registers across
architectures.  Note that the only live variables across migration are
the arguments $(\cvec{a\subi}{\polybase}{\varbound})$ passed to function $f$.  This
corresponds exactly to the set of register values which will be live
during migration.  To migrate these values, the backend packs them
into a newly allocated block in the heap, taking care to convert any
real pointers into index values.  This allows us to use an architecture-dependent
representation of values in the registers, and also provides safety checks
on register values automatically, when they are read out of the heap on
the target machine.

\nocomment{Emre: In the previous paragraph, ``architecture-dependent
         representation''? ``architecture-independent''?\\
         JDS:  Architecture-dependent is correct.  The registers
         must permit arch-dependent value representation, otherwise
         certain architectures would have to jump through hoops
         to emulate IA32 :)}

\iftechreport
\sectionC{Uses for process migration}

\nocomment{JDS:  BTW, at some point we should include discussion on ambient calculus,
         since that is what inspired the process migration primitive.  I think
         we might want to consider some of the example applications for ambient
         calculus.  Ugh, I forgot to pack the papers in my luggage though!}

Process migration may be applied to various higher-level concepts to simplify application
development.  Its primary benefit is in fault-tolerant computing, where each part of a distributed
computation can take periodic checkpoints which are used to recover the computation when a machine fails.
It can be used in many other applications, a few of which are described below.

Load-balancing is an important issue in a distributed system, especially if the nodes
or computation processes are heterogeneous.  A system monitor can detect nodes in the system
which are overloaded relative to other nodes, and migrate some of the processes off the
overloaded node to other nodes carrying a lighter load.  Since processes do not observe
the result of a migration, this can be done without any special support by the processes.

Migration can be used to pass ``active'' messages, or messages which include executable
code and data.  To pass a message from one process to another, the process may start a new
thread, and migrate the thread (including all necessary state) to the machine hosting the
other process.

This can be particularly useful for database queries.  When the database is hosted on a remote
machine, it is highly inefficient to run a query on the local machine that transfers effectively
the entire database over the network to extract the few records which the query is interested in.
Mainstream database systems work around this by supporting a query language used to describe the
query.  The local machine sends the text of the query, which is then run on the server.  However,
these languages are highly specialised and may not be sufficiently expressive for certain inquiries.
With process migration, we can migrate an arbitrary program to the remote machine, allowing
far greater expressiveness using a more general mechanism.

Similar to active messages, migration can be used to create active files, or files which contain
data but also code to manipulate the data, similar to existing file formats which include a macro
or scripting language.  A simple example would be a multimedia file which includes the program
required to play the media.
\fi



\sectionB{Atomic operations and transactions}
\label{section:transactions}

\nocomment{JDS OVERVIEW:
         Discuss transactions.  Well, duh.  Generational GC almost gives us
         transactions automagically; rest is just maintenance code really.
         Use of pointer table makes it easy to do COW.  Always preserve original
         block and copy data to a NEW block in current GC generation.
         
         Worth discussing the various modes of transactions --- nested (the
         type of transactions most people are used to; traditional transactions)
         and FIFO in particular (latter is nonintuitive, but useful for fault-
         tolerant computing).}

In databases, transactions play a key role in ensuring that sequences
of operations that are run simultaneously do not interfere.
Semantically, transactional execution appears atomic; that is, either
all the operations in a transaction must succeed, or none of them will
succeed.
\iftechreport
  The \fir{} provides a generalization of transactions for
  expressing rollback of a distributed computation which is more efficient 
  than using process migration alone in the event of a machine failure.
\fi

The primary obstacle in implementing atomic transactions is
restoration of the program \emph{state}\footnote{In this paper, we do not
consider rollback of I/O operations.  However, the concepts discussed here
can be extended to include I/O operations with some assistance from the
operating system.  These extensions will be discussed in a future paper.}.  
When a transaction is aborted, the entire process state, including all 
variable and heap values, must be restored to the state it had on entry 
into the transaction.

Rollback can be expressed with process migration by having a process
write a new checkpoint file each time it enters a new atomic section.
If the transaction is aborted, the previous state can be restored by
restoring the process from a checkpoint.  However, since the migration
mechanism recompiles the program, and the \emph{entire} process state
must be reconstructed, this operation can be very expensive.  Even
taking the checkpoint is expensive, since the entire state must be
written to a file, even parts of the state that have not changed since
a prior checkpoint.  By contrast, atomic transactions use a copy-on-write mechanism
to keep track of modified state that must be restored if the transaction is
rolled back.

The FIR provides three primitives for managing atomic transactions:
\emph{entry}, which enters a new atomic level; \emph{commit}, which
marks an atomic level as completed; and \emph{rollback}, which aborts
all changes made by a particular level and resumes execution at the
point where the level was previously entered.



\sectionC{Using atomic transactions in the FIR}
\label{section:transaction-levels}
\label{section:transaction-rollback}

% JDS: hyphenation explicit to avoid an overfull hbox.
\iftechreport
Like process migration, atomic transactions use the spe\-cial-call
mechanism in the FIR.
\fi
Atomic transactions may be nested; each
\emph{entry} operation enters a new atomic level nested within the
previous level.  Atomic levels are numbered from $1$ to $N$, where $1$
is the oldest atomic level entered and $N$ is the most recent.  A
process that has not entered any atomic transactions is at level $0$.
A level $l$ keeps track of all changes made to the state that have
occurred since $l$ was entered.  Atomic levels use copy-on-write
semantics; when a block in the heap is modified, the block is cloned
and the pointer table updated to point to the new copy of the block,
preserving the data in the original block.  On a \emph{commit} or
\emph{rollback} operation of $l$, exactly one of these blocks will be
discarded.

\iftechreport
%
% We said this earlier, but it is useful for the TR
%
The primitive for \emph{entry} is the
$\tailatomic{f}{c}{\cvec{a\subi}{\polybase}{\varbound}}$ special-call.  $c$ is an integer that is
passed as the first argument to $f$; on a rollback, the value of $c$
passed to $f$ may be changed to indicate that the rollback occurred.
This is the only way to carry state information across a rollback\footnote{For
technical reasons, $c$ must be an integer in the current implementation.  Future
implementations will allow $c$ to be a pointer to a block in the heap which is
preserved across a rollback operation.}.

The primitive for \emph{commit} is
$\tailatomiccommit{\ms{level}}{f}{\cvec{a\subi}{\polybase}{\varbound}}$.  This will
commit data for $\ms{level}$ by folding all changes from that level
into its previous level.  $\ms{level}$ must be in the interval
$\mkset{0\ldots N}$, otherwise a runtime exception will occur.  If
$\ms{level} = 0$, then the most recent level $N$ is committed.

The primitive for \emph{rollback} is
$\tailatomicrollback{\ms{level}}{c}$.  This will revert all changes
made by in level $\ms{level}$ and all later levels.  $\ms{level}$ must
be in the interval $\mkset{0\ldots N}$, otherwise a runtime exception
will occur.  If $\ms{level} = 0$, then the most recent level $N$ is
reverted.
\fi

Rollback resumes execution at the point where $\ms{level}$ was
entered.  No function or argument list is specified; the function that was
associated with level $\ms{level}$ is saved as part of the checkpoint,
to be called with the original atom arguments but with the new
value for $c$.  This version of the primitive is a retry primitive;
atomic level $\ms{level}$ is automatically re-entered after it (and all
later levels) have been rolled back\footnote{In effect, the state that is
captured and restored is the state immediately after level $\ms{level}$ was
entered.}.

\sectionC{Implementation of atomic transactions}
\label{section:transaction-implementation}

\nocomment{JDS:  Need to discuss copy-on-write pages and the garbage collector levels.
         Then, we should probably address the notion of levels in more detail.}

Transactions are implemented in close cooperation with the garbage
collector.
The heap layout is shown in Figure~\ref{figure:heap-layout}, which is
drawn with the base of the heap at the top of the figure, and the
limit of the heap at the bottom.

The heap has the following properties.
%
\begin{itemize}
\item Each heap generation $i$ is delimited by a base pointer
$\ptrbase{i}$, and a limit pointer $\ptrbase{i + 1}$, or, in the case
of the youngest generation, $\ptrlimit$.

\item The upper bound of atomic level $i$ is delimited by the
$\ptrlevel{i}$ pointer.

\mb{Invariant}: ({\sc Atomic Invariant}) all heap data for atomic level $i$ is between
$\ptrbase{\polybase}$ and $\ptrlevel{i}$, and it is immutable.
\end{itemize}

The generational bounds and the atomic level bounds are independent.  An
atomic level may cross a generational boundary, and often does.  The
two are related however: since the data in an atomic level is
immutable, garbage collection on an atomic level is
idempotent.

\makefigure{figure:heap-layout}{Heap data with multiple atomic levels}{%
   \begin{center}
\iftechreport
   \includegraphics{heap_layout}
\else
   \includegraphics[scale=0.5]{heap_layout}
\fi
   \end{center}
}

\sectionD{Garbage collection}

The garbage collector uses generational, mark-sweep, compacting, collection.  During
the mark phase for generation $i$, the entire live set for generation
$i$ and all younger generations is traversed, and the live blocks are
marked.  The marking algorithm uses a pointer-reversal scheme to eliminate the
need for additional storage during traversal.

During the sweep phase for generation $i$, the heap from
$\ptrbase{i}$ to $\ptrlimit$ is scanned.  If a dead block is
encountered, its entry in the pointer table is deleted.  Otherwise,
if the block is live, it is copied left to the lowest unallocated
location in the heap, and the pointer table is updated with the new
location.  Note that the heap data itself is not modified during
collection.

Block ordering is preserved by the collection, and the heap pointers
$\ptrlevel{i}$, $\ptrbase{i}$, $\ptrcurrent$, and $\ptrlimit$ are
updated after an allocation to point to their new locations.

\sectionD{Transaction support}

On atomic entry, a new generation is set up in the heap by creating a
new $\ptrlevel{i}$ pointing at $\ptrcurrent$, the end of the heap.  All
data before $\ptrcurrent$ becomes immutable.  In addition to the
$\ptrlevel{i}$ bounds, each level has its own pointer table
$\ptrpdiff{i}$, that can be used to restore the pointer table if the
transaction is aborted.  To minimize storage requirements, the
$\ptrpdiff{i}$ table is stored as a set of differences with the
current pointer table $\ms{ptable}$.

Within a transaction, the only operations requiring special support
are the assignment operations.  If a block is to be mutated, and the
block belongs to a previous generation (its address is below the
current $\ptrlevel{i}$), the block is copied into the minor heap, the
current pointer table $\ms{ptable}$ is updated with the new location,
and the previous pointer table $\ptrpdiff{i - 1}$ is updated with the
block's original location.  The original data remains unmodified.  The
garbage collector includes the $\ptrpdiffname$ tables as ``root''
pointers; the original block remains live.

When an atomic level $i$ is committed, the pointer $\ptrlevel{i}$
and the difference table $\ptrpdiff{i}$ are deleted.  In general,
this will release storage that was needed in case of rollback, and
the space is automatically reclaimed during the next garbage
collection.  On a rollback to atomic level $i$, the pointer table is
restored from the current pointer table and the $\ptrpdiff{i}$ table,
which is deleted along with the $\ptrlevel{i}$ delimiter.


\iftechreport
\sectionC{Using atomic transactions for traditional transactions}

When using traditional transactions, a transaction cannot be committed until
all nested transactions have been committed.  In this case, the \emph{commit}
operation will always be called with $\ms{level} = 0$.  This behaviour is
consistent with traditional transactions, where a nested transaction must
always be committed before an older transaction can be committed.



\sectionC{Using atomic transactions for fault-tolerant computing}

In a distributed computation, the processes involved must synchronize
periodically.  Ideally, the processes would speculatively continue computing
across a synchronization point, even if they have not received confirmation
that all other processes have reached the synchronization point.  In this
case, a process can enter a new atomic transaction as it passes the synchronization
point and continue computing.  If a process in the system fails, the remaining
processes can agree on the last known-good synchronization point using a consensus
algorithm, and then roll back to that synchronization point to continue the computation.

In this case, the process will monitor where every other process in the system 
is, and commit a transaction for a synchronization point once it knows all other
processes in the system have reached that point.  In this scenario, we will always
be committing the \emph{oldest} transaction in the system, which is opposite the
behaviour we would expect in traditional distributed transactions.  Because of
this case, it is useful to generalize the transactions so a process can commit
any transaction.  When a level is committed, it is simply folded into the previous level
so that a later rollback on the previous level will revert changes associated with the
committed level as well.

\nocomment{JDS:  Diagrams would be really helpful here, to illustrate the atomic levels.
         As much as I hate using \tt{xfig} on a touch pad, I'll give it a try.
         Worst-case, I can probably adapt the diagrams I prepared for my presentation,
         at least to illustrate how the garbage collector works.}



\sectionB{An implementation of fault-tolerant distributed computing}

\nocomment{JDS OVERVIEW:
         How to use migration/transactions to do fault-tolerant computing.
         Migration checkpoints to rescue computations on failed nodes, and
         transactions as efficient mechanism to rollback rest of computation.}

Migration and atomic transactions can be used in conjunction to implement an efficient
fault-tolerant distributed computation.  In a distributed computation, it is 
often necessary that the processes synchronize periodically and come to consensus
on the current state of the computation.  Using traditional programming techniques,
the computation on each node must be halted until all processes reach the synchronization
point and agree on the current state of the global computation.  This imposes serialization
in the system and can impact performance if the nodes are heterogeneous with respect to
computing speed.

Migration and atomic transactions can be used to support speculative computing 
each time a synchronization point is reached.  When a process reaches a
synchronization point, it sends a control message to other processes indicating it
has reached the synchronization point.  Then the process establishes a new atomic level
and continues computing, assuming that other processes will eventually reach the same
synchronization point.  Periodically, the process uses the migration system to write a
full checkpoint of the process to a shared fault-tolerant storage system.  Once the
process receives confirmation from all other processes that the synchronization point has 
been reached, it can commit the associated atomic level.  It can also delete a checkpoint
file once all other processes have reached the synchronization point and a more recent
checkpoint is written for the process.

In the event of a failure in the system (which will likely terminate at least one
process abnormally), the system can recover to the last known-valid state by rolling
back each surviving process to the appropriate atomic level.  The last checkpoint of
a terminated process can be resurrected on any surviving machine in the system, using
the migration system's resurrect program.  Atomic transactions provide an efficient
method to rollback the state of surviving processes, and migration checkpoints provide
a method to revive any process that is abnormally terminated during a failure.

Work is in progress to implement this system using the existing primitives.
\fi



