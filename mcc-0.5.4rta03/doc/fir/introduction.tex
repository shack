\sectionA{Introduction}

The design of software for distributed applications is a challenging task.  In
addition to the difficulties posed by processor and network failures,
distributed applications are often composed of several parts written in
different languages.  In this \papername, we approach these problems by using
a typed, semi-functional intermediate language that supports two key features
for distributed computing: whole-process migration, and undoable transactions.
Among other services, process migration provides location transparency and the
ability to perform load-balancing and process checkpointing.  Transactions
provide fault-isolation by limiting the scope of errors, and speculative
execution by providing the ability to rollback overly optimistic computations.
To address the multi-language issue, the intermediate language is designed to
support both type-safe source languages like ML, and unsafe languages like C.

We have implemented our approach in the \mojave{} system, which currently
provides a multi-language compiler for programs written in C, Naml (a language
based on Caml-light), and Pascal.  Programs in these languages are compiled to
a typed ``Functional Intermediate Representation'' (\fir).  Types are
maintained throughout the compilation process, which ensures that component
interactions respect type and memory safety.  The core \fir{} language is a
variant of System F~\cite{Gir71} in continuation-passing style
(CPS)~\cite{App92}.  In many respects, the \fir{} language is similar to the
typed intermediate language used in the TILT
compiler~\cite{tarditi+:til,morrisett+:til-wcsss}.  However, the \fir{} is
much more limited syntactically; the precise language is specified in
\iftechreport
   Chapter~\ref{section:syntax}.
\else
   Section~\ref{section:syntax}.
\fi

The \fir{} language has been quite robust as new source languages have been
added to the \mojave{} compiler, but there are tradeoffs, most evident when
compiling C.  First, the \fir{} requires that program execution be safe,
despite the fact that it is not always possible to maintain exact type
information for C.  This limitation is resolved using runtime safety checks.
Pointers in the runtime are represented with base/offset pairs, requiring more
storage and extra computation.  Second, the decision to use
continuation-passing style means that there is no runtime stack, and C
functions allocate storage on the heap, which can be expensive in some cases.
However, there are also many advantages.  The \fir{} has made it easy to
augment ANSI C with polymorphism and type inference, exceptions, pattern
matching, higher-order functions, and safe return of pointers to ``automatic''
variables.

% Emre: The tech report and POPL paper require different ``paper organization
% paragraphs'' since we have sections in one and chapters in the other.

\iftechreport

The \papername{} is organized as follows.  In
Section~\ref{section:related-work}, we discuss related work.
Section~\ref{section:arch} covers the system architecture and design of the
\mojave{} compiler.  Chapters~\ref{section:syntax}--\ref{section:op-semantics}
cover the \fir, including the type system and operational semantics.  In
Chapter~\ref{section:lemmas}, we present a set of basic lemmas needed to prove
the type-safety theorems of Chapter~\ref{section:type-safety}. We conclude
with a discussion of the runtime implementation in
Chapter~\ref{section:backend}.

\else

The \papername{} is organized as follows.  In Section~\ref{section:related-work},
we discuss related work.  Section~\ref{section:arch} covers the system
architecture and design of the \mojave{} compiler.
Sections~\ref{section:syntax}--\ref{section:op-semantics} cover the \fir,
including the type system and operational semantics, and
Section~\ref{section:backend} discusses the runtime implementation.  We
conclude by presenting a preliminary set of benchmarks.

\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Related work.
%

\sectionB{Related work}
\label{section:related-work}

The desire for common intermediate languages dates back to the 1950's,
significantly with the UNCOL project~\cite{uncol}.  More modern versions that
support at least part of the goals of multi-language platforms are the Java
Virtual Machine~\cite{java:jvm} and Microsoft's Common Language
Runtime~\cite{meijer:cli,syme:clr}.  The JVM has many desirable features: it
is portable, it is safe, and compiler technology can produce very efficient
executables.  However, as Meijer points out~\cite{meijer:cli}, while the JVM
was designed to be generic, it works most effectively for Java.  There is
little support for (at one extreme) unsafe languages like C, and (at another
extreme) functional languages like ML.

The Microsoft CLR addresses many of these problems, and supports a wide range
of languages including parts of C++, \ocaml, and Standard ML.  However, the CLR
does not ensure safety, although Gordon and Syme have been able to demonstrate
safety for a substantial fragment~\cite{syme:clr}.  The CLR is a fairly large
language.  In contrast, the \fir{} is quite small---and consequently more effort
is required from a language front-end to compile to \fir.

Another related area is portable code generators, of which there are several,
including MLRISC~\cite{george:mlrisc}, C-{}-~\cite{simonpj:c--,simonpj:ifl},
and \texttt{gcc}~\cite{gcc}.  As Peyton-Jones suggests, C itself should be
considered as a commonly-used assembly language.  In each of these cases, the
code generator is attacking the problem of portability and machine-code
generation, but not the problem of safety.

In the area of C program safety, the Necula et.al.~CCured
compiler~\cite{necula:popl02} and the Morrisett et.al.~Cyclone
Safe-C~\cite{jgm:cyclone} compiler both extend the C language to include extra
information needed to infer safety.  The systems distinguish between ``safe''
and ``unsafe'' pointers, where a safe pointer is always used in ways that can
be shown to be safe, and unsafe pointers include all the rest.  In both
systems, unsafe pointers are represented at runtime by a ``fat'' pointer that
includes safety information, requiring twice the storage of a safe pointer.

CCured extends the type system and uses type inference to determine safety.
Cyclone requires explicit annotation by the user: safe and unsafe pointers
have different types, and different dereference operators.  Compilation is
limited to the subset of C that can be proven safe.

In contrast with this work, the \mojave{} compiler accepts all source files that
conform to the ANSI standard, but \emph{all} pointers are represented as fat
pointers.  We use dead-code elimination coupled with alias analysis to delete
provably unnecessary safety information for variables, but this still requires
more space than the other two systems.  There is no way in \mojave{} to represent
an array of safe pointers; all pointers are 8 bytes on all platforms.

Process migration has been widely
studied~\cite{vigna:mobile-agents,Dimarzo:98:ASTMA}, yet the availability for
general-purpose languages, like C and ML, is limited.  Our approach to process
migration has been heavily influenced by Cardelli's work on the Ambient
Calculus~\cite{cardelli:mobile-ambients,cardelli:anytime-anywhere}; however,
our work with whole-process migration is only the first step toward
fine-grained mobility.

Transactions are a fundamental concept in the database community, but again,
implementations for general-purpose languages are limited.  As part of the
Venari project, Haines et.{}al.~\cite{haines:first-class-transactions}
implement a transaction mechanism as part of Standard ML.  Undoability is
implemented by extending the mutation log produced by the generational garbage
collector.  Our approach (described in
Section~\ref{section:transaction-implementation}) also uses a mutation log.
However, we combine a generational mark-sweep collector with a copy-on-write
mechanism to reduce the cost of rollback and commit operations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% System Architecture
%

\sectionB{System architecture}
\label{section:arch}

The \mojave{} system has three major parts.  The front-ends are responsible for
compiling code to \fir, and the back-ends are responsible for generating
machine code from \fir.  \fir{} type inference and optimizations form the middle
stage of the compiler.  The stages of the compiler are shown in
Figure~\ref{figure:mojave} for the C and Naml source languages; Pascal is
compiled through a preprocessor to the C front-end.  Currently, there is one
back-end, for the Intel IA32 architecture.

\makefigure{figure:mojave}{Architecture of the \mojave{} Compiler}{
\iftechreport
   \centerline{\includegraphics[scale=0.8]{mojave}}
\else
   \centerline{\includegraphics[scale=0.6]{mojave}}
\fi
}

As shown in the diagram, the front-ends perform a significant amount of
compilation to produce the \fir{} representation.  In particular, the C front-end
has to add explicit coercions and eliminate pointer arithmetic.  In addition,
all of the branches perform CPS and closure conversion.  We do not discuss the
front-ends in detail in this paper, focusing instead on the \fir{} and runtime
implementation.

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
