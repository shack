\iftechreport
\sectionA{Type safety}
\label{section:type-safety}

The proof of type-safety has two parts.  The {\sc Preservation} theorem~\ref{theorem:preservation}
shows that types are preserved during program reduction.  The {\sc Progress}
theorem~\ref{theorem:progress} shows that for well-typed programs, if the expression $e$ being
evaluated is not a value, then there is a reduction rule that can be used to evaluate the program
one more step.

\sectionB{Preservation}
\else
\sectionB{Type-safety}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRESERVATION
%
\begin{theorem}
\thmtitle\preservation{theorem:preservation}{Preservation}
%
If $\gcstate{\genv_r}{\cenv_r}{e_r}$ is a valid program and 
$\reducesto{\gcstate{\genv_r}{\cenv_r}{e_r}}{\gcstate{\genv_c}{\cenv_c}{e_c}}$, then
$\gcstate{\genv_c}{\cenv_c}{e_c}$ is a program.
\end{theorem}

\iftechreport
\begin{proof}
\newcommand\tyexp{\tyvoid}
\setgenvname{\genvorig_r}
\setcenvname{\cenvorig_r}
\setgenvtypename{\widehat{\genv}}

We prove this case analysis on the reduction.  From the \fullydefined, we can assume that the
contexts in $\cenv$ and the context $\genv$ are fully-defined.  The preservation proof amounts to
showing that types are preserved during reduction.

The proof depends on the type judgment $\withgenv{e_r : \tyexp}$.  Type judgments in general require
value \emph{declarations} of the form $\vdef{v}{t}$ rather than value \emph{definitions} of the form
$\hdef{v}{t}{b}$.  We use the \proofinduction{}
throughout this proof to infer use of specific type rules in a partially thinned context $\genvtype$
where some number of thinning rules have been applied.

%
% Atom reduction.
%
\begin{proofcase}{Variable reduction}
\settypename{t_1}
Suppose the reduction uses the rule \refmath{Red-Atom-Var}.

$$\redvar$$

By assumption $\withgenvand{\hdef{v}{\typename}{h}}{E[v] : \tyexp}$.  Since
$\withgenvand{\hdef{v}{\typename}{h}}{v : \typename}$ and $\withgenvand{\hdef{v}{\typename}{h}}{h :
\typename}$, we can conclude from the \expsubstlemma{} that
$\withgenvand{\hdef{v}{\typename}{h}}{E[h] : \tyexp}$.
\end{proofcase}

%
% Unop reduction.
%
\begin{proofcase}{Unary operations}
Suppose the reduction uses the \refmath{Red-Atom-Unop} rule.

$$\redunop$$

From the assumption $\withgenv{E[\mathop{\unop} i] : \tyexp}$ and the \exptypinglemma, we know that the
atom $\mathop{\unop} i$ has some type $t_a$.  The proof of typing must use the \refmath{Ty-AtomUnop}
rule.

\begin{block}
\setatomname{i}
$$\tyatomunop$$
\end{block}

That is, $t_a$ is $\unoptyperes{\unop}$.  Since the builtin unary operators are well-typed (by
assumption), the value $\semleft \unop \semright(i)$ also has type $\unoptyperes{\unop}$.  We can
conclude from the \expsubstlemma{} that $\withgenv E[\semleft \unop
  \semright(i)] : t$.
\end{proofcase}

%
% Binary operators
%
\begin{proofcase}{Binary operations}
Suppose the reduction uses the \refmath{Red-Atom-Binop} rule.

$$\redbinop$$

From the assumption $\withgenv E[i \mathrel{\binop} j] : \tyexp$ and the \exptypinglemma, we know that
the atom $i \mathrel{\binop} j$ has some type $t_a$.  The proof of typing must use the
\refmath{Ty-AtomBinop} rule.

\begin{block}
\setatomnameone{i}
\setatomnametwo{j}
$$\tyatombinop$$
\end{block}

That is, $t_a$ is $\binoptyperes{\binop}$.  Since the builtin binary operators are well-typed (by
assumption), the value $\semleft \binop \semright(i, j)$ also has type $\binoptyperes{\binop}$.  We can
conclude from the \expsubstlemma{} that $\withgenv E[\semleft \binop
  \semright(i, j)] : \tyexp$.
\end{proofcase}

%
% Type application
%
\begin{proofcase}{Type application}
\setfunname{v_1}
Suppose the reduction uses the \refmath{Red-Atom-Apply} rule.

$$\redatomapply$$

From the assumption $\withgenv E[\atomtyapply{u}{v_1}{\cvec{u\subi}{\polybase}{\polybound}}] :
\tyexp$, and the \exptypinglemma, we know that the atom
$\atomtyapply{u}{v_1}{\cvec{u\subi}{\polybase}{\polybound}}$ has some type $t_a$.  The proof of typing
must use the \refmath{Ty-AtomTyApply} rule.

$$\tyatomtyapply$$

That is, $t_a = \alttypename = \typename[\lvec{u\subi/\alpha\subi}{\polybase}{\polybound}]$.  By the
\typesubstlemma, we can infer that

$$
\withgenvand{\hvecdef{\alpha\subi}{\omega}{u\subi}{\polybase}{\polybound},
             \hdef{v_2}{\typename}{b}}
   {v_2 : t_a}
$$

We can conclude from the \expsubstlemma{} that $\withgenv{E[v_2] : \tyexp}$.
\end{proofcase}

%
% Pack
%
\begin{proofcase}{Type abstraction}
Suppose the reduction uses the \refmath{Red-Atom-Pack} rule.

$$\redatompack$$

From the assumption $\withgenv E[\atomtypack{u}{v_1}{\cvec{u\subi}{\polybase}{\polybound}}] :
\tyexp$, and the \exptypinglemma, we know that the atom
$\atomtypack{u}{v_1}{\cvec{u\subi}{\polybase}{\polybound}}$ has some type $t_a$.  The proof of
typing must use the \refmath{Ty-AtomTyPack} rule.

\begin{block}
\setvarname{v_1}
$$\tyatomtypack$$
\end{block}

That is, $t_a = u = \tyexists{\cvec{\alpha\subi}{\polybase}{\polybound}}{\typename}$.  Since $v_2$
also has type $t_a$, we can infer the from the \expsubstlemma{} that $\withgenv E[v_2] : \tyexp$.
\end{proofcase}

%
% Pack
%
\begin{proofcase}{Type unpacking}
Suppose the reduction uses the \refmath{Red-Atom-Unpack} rule.

$$\redatomunpack$$

From the assumption $\withgenv E[\atomtyunpack{v_1}] : t$, and the \exptypinglemma, we know that the atom
$\atomtyunpack{v_1}$ has some type $t_a$.  The proof of typing must use the
\refmath{Ty-AtomTyUnpack} rule.

\setvarname{v_1}
$$\tyatomtyunpack$$

That is, $t_a = \typename[\lvec{\typroject{\varname}{\nosubi}/\alpha\subi}{0}{\polybound}]$.  Since
$\typroject{\varname}{i} = u_i$ for each $i \in \mkset{\polybasezero\ldots\polyboundzero}$, we can infer
from the \typesubstlemma{} that $\withgenv E[v_2] : \tyexp$.
\end{proofcase}

%
% LetAtom is a pretty easy case
%
\begin{proofcase}{LetAtom}
\setatomname{\heaporig}
Suppose the reduction uses the rule \refmath{Red-LetAtom}.

\begin{block}
\settypename{t_1}
$$\redletatom$$
\end{block}

The typing proof must use the rule \refmath{Ty-LetAtom}.

\begin{block}
\settypename{\tyexp}
$$\tyletatom$$
\end{block}

Using the \refmath{Thin-Var-Def} rule and the premises,
we can infer $\withenv{\genv, \hdef{v}{t_1}{h}}{e_r : \tyexp}$.

\begin{block}
\setgenvtypename{\genv}
\setconclname{e : \tyexp}
\settypename{t_1}
\setblockname{h}
$$\thinvardef$$
\end{block}
\end{proofcase}

%
% LetExt is similar to LetAtom
%
\begin{proofcase}{LetExt}
Suppose the reduction uses the rule \refmath{Red-LetExt}.

$$\redletext$$

The typing proof must use the rule \refmath{Ty-LetExt}.

\begin{block}
\setatomname{\heaporig}
\settypename{\tyexp}
$$\tyletext$$

From the premises $\withgenvtype{\lvec{\atomname\subi : u\subi}{\varbase}{\varbound}}$ and
$\withgenvtype{\semleft \text{``s''} \semright :
\tyfun{\argvec{u\subi}{\varbase}{\varbound}}{\alttypename}}$, we can infer that $\withgenvtype{\semleft
\text{``s''} \semright \argvec{h\subi}{\varbase}{\varbound} : \alttypename}$.  Using the \refmath{Thin-Var-Def}
rule, we can infer that $\withenv{\genv, \hdef{v}{\alttypename}{\semleft
\text{``s''} \semright \argvec{h\subi}{\varbase}{\varbound}}}{e_r : \tyexp}$.
\end{block}

\begin{block}
\setgenvtypename{\genv}
\setconclname{e : \tyexp}
\settypename{t_1}
\setblockname{\semleft \text{``s''} \semright \argvec{h\subi}{\varbase}{\varbound}}
$$\thinvardef$$
\end{block}
\end{proofcase}

%
% Tailcall is a hard case because we expand the function body.
%
\begin{proofcase}{TailCall}
\settypename{\tyexp}
\setatomname{\heaporig}
\setfunname{f}

Suppose the reduction uses the rule \refmath{Red-TailCall}.

$$\redtailcall$$

The proof of typing must use the rule \refmath{Ty-TailCall}.

$$\tytailcall$$

By the \wfcontextvalueslemma{} we know that the function
is well-formed.  The proof must use the \refmath{Ty-StoreFun-Mono} rule.

$$\tystorefunmono$$

From the \refmath{Thin-Var-Def} rule, we can conclude that

$$\withgenvand{\hvecdef{v\subi}{u\subi}{h\subi}{\varbase}{\varbound}}{e : \tyexp}.$$
\end{proofcase}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIAL CALLS
%
\begin{proofcase}{SpecialCall}
\settypename{\tyexp}
\setatomname{\heaporig}
\setlevelname{i}
\setfunname{\funvarorig}
There are four cases for reducing a special-call.

\begin{itemize}
\item

Suppose the reduction uses the rule \refmath{Red-SysMigrate}.

$$\redsysmigrate$$

The proof of typing must use the rule \refmath{Ty-SysMigrate}.

$$\tysysmigrate$$

This case is similar to a tail-call.  From the premises $\withgenvtype{\lvec{\atomname\subi :
t\subi}{\varbase}{\varbound}}$ and $\withgenvtype{\funname :
\tyfun{(\cvec{t\subi}{\varbase}{\varbound})}{\tyvoid}}$, we can infer that
$\withgenv{\tailcall{\funname}{\cvec{\atomname\subi}{\varbase}{\varbound}} : \typename}$.

\item

Suppose the reduction uses the rule \refmath{Red-Atomic}.

$$\redatomic$$

The proof of typing must use the rule \refmath{Ty-Atomic}.

$$\tyatomic$$

From the premises $\withgenvtype{\atomname_\ms{const} : \tyrawint{32}{\signed}}$,
$\withgenvtype{\lvec{\atomname\subi : t\subi}{\varbase}{\varbound}}$, and
$\withgenvtype{\funname :
\tyfun{(\cvec{t\subi}{\varbase}{\varbound})}{\tyvoid}}$, we can infer that the checkpoint
$\checkpoint{\genv}{\funname}{\cvec{\heapname\subi}{\varbase}{\varbound}}$ is well-formed,
and that $\withgenv{\tailcall{\funname}{\cvec{\atomname\subi}{\varbase}{\varbound}} :
\typename}$

\item 

Suppose the reduction uses the rule \refmath{Red-Atomic-Rollback-1}.

$$\redatomicrollbackone$$

By assumption, the checkpoint context $\cenv$ is well-formed, and
$\withgenv{\tailcall{\funname}{j, \cvec{\heapname\subi}{\varbase}{\varbound}} : \tyexp}$.
Since the reduction only removes checkpoints from the context, the checkpoint context remains
well-formed.

The argument for \refmath{Red-Atomic-Rollback-2} is similar.

\item

Suppose the reduction uses the rule \refmath{Red-Atomic-Commit-1}.

$$\redatomiccommitone$$

The proof of typing must use the rule \refmath{Ty-AtomicCommit}.

$$\tyatomiccommit$$

From the premises $\withgenvtype{\lvec{\atomname\subi : t\subi}{\varbase}{\varbound}}$ and
$\withgenvtype{\funname :
\tyfun{(\cvec{t\subi}{\varbase}{\varbound})}{\tyvoid}}$, we can infer that
$\withgenv{\tailcall{\funname}{\cvec{\atomname\subi}{\varbase}{\varbound}} : \typename}$.  The
argument for \refmath{Red-Atomic-Commit-2} is similar.
\end{itemize}
\end{proofcase}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Match statements don't work currently because of the totality requirement
%
\begin{proofcase}{Match}
There are two cases, for matching against an integer, and for matching against a union.

\begin{itemize}
\item
Suppose the reduction uses the rule \refmath{Red-Match-Int}.

$$\redmatchint$$

The proof of typing must use one of \refmath{Ty-Match-Int}, \refmath{Ty-Match-Enum}, or
\refmath{Ty-Match-RawInt}.  We illustrate with the \refmath{Ty-Match-Int} rule.

\begin{block}
\setatomname{i}
\setindex{j}
\settypename{\tyexp}
$$\tymatchint$$
\end{block}

We can infer that $\withgenv{e_k : t}$ directly from the premise $\withgenv{\setindex{j} \lvec{e\subi : t}{\varbase}{\varbound}}$.

\item
Suppose the reduction uses the rule \refmath{Red-Match-Union}.

$$\redmatchunion$$

The only possible typing rule for this expression is \refmath{Ty-Match-Union}.

\begin{block}
\setatomname{v}
\setindex{j}
\settypename{\tyexp}
$$\tymatchunion$$
\end{block}

We can infer that
$\withgenvand{\hdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_k}}{\storeunion{\tyvar}{i}{\cvec{a\subi}{\varbase}{\varbound}}}}
e_k : \tyexp$ directly from the premises.
\end{itemize}
\end{proofcase}

%
% Allocation.
%
\begin{proofcase}{LetAlloc}
\setatomname{\heaporig}
\settypename{\tyexp}
\setsizename{i}

There are several allocation cases.  In each of these cases, the type judgment uses the
\refmath{Ty-Alloc} rule.

$$\tyletalloc$$

\begin{itemize}
\item
Suppose the reduction uses the rule \refmath{Red-Alloc-Tuple}.

$$\redalloctuple$$

The corresponding type judgment is \refmath{Ty-Alloc-Tuple}.

$$\tyalloctuple$$

From the premises $\withgenv \lvec{\atomname\subi : u\subi}{\varbase}{\varbound}$ we can infer that
the $u = \tytuple{\tupleclass}{\cvec{u\subi}{\varbase}{\varbound}}$ and the context $\genv,
\hdef{v}{\alttypename}{\storetuple{\cvec{h\subi}{\varbase}{\varbound}}}$ is well-formed.  From the
premise $e : \typename$ we conclude that $\withenv{\genv,
\hdef{v}{\alttypename}{\storetuple{\cvec{h\subi}{\varbase}{\varbound}}}}{e : \typename}$.

\item
Suppose the reduction uses the rule \refmath{Red-Alloc-Array}.

$$\redallocarray$$

The corresponding type judgment is \refmath{Ty-Alloc-Array}.

\begin{block}
\setalttypename{u'}
$$\tyallocarray$$
\end{block}

That is, $\alttypename = \tyarray{u'}$.  From the premise $\withgenv \atomname_\ms{init} : u'$ we can
infer that the context $\genv,
\hdef{v}{\alttypename}{\storetuple{\cvec{h\subi}{\varbase}{\varbound}}}$ is well-formed.  From the
premise $e : \typename$ we conclude that $\withenv{\genv,
\hdef{v}{\alttypename}{\storetuple{\cvec{h\subi}{\varbase}{\varbound}}}}{e : \typename}$.

\item Suppose the reduction uses the rule \refmath{Red-Alloc-Union}.

$$\redallocunion$$

The corresponding type judgment is \refmath{Ty-Alloc-Union}.

$$\tyallocunion$$

That is, $t = \tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{\mkset{j}}$.  From the premise
of the typing rule, we can infer that
$\genv, \hdef{v}{t}{\storeunion{\tyvar}{i}{\cvec{h\subi}{\varbase}{\varbound}}}$ is a well-formed
context, and from the premise $e : \typename$ we conclude that
$\withgenvand{\hdef{v}{t}{\storeunion{\tyvar}{i}{\cvec{h\subi}{\varbase}{\varbound}}}}{e :
\tyvoid}$.

\item Suppose the reduction uses the rule \refmath{Red-Alloc-Malloc}.

$$\redallocmalloc$$

The corresponding type judgment is \refmath{Ty-Alloc-Malloc}.

$$\tyallocmalloc$$

That is $t = \tyrawdata$, and we can infer that the context $\genv, \hdef{v}{t}{\storedata{\xdata}}$
is well-formed.  From the premise $e : \typename$ we conclude that
$\withgenvand{\hdef{v}{t}{\storedata{\xdata}}}{e : \tyvoid}$.

\item Suppose the reduction uses the rule \refmath{Red-Alloc-Frame}.

$$\redallocframe$$

The corresponding type judgment is \refmath{Ty-Alloc-Frame}.

$$\tyallocframe$$

The premise of the typing rule requires that $\tyapply{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}
: \Frametype$.  From the \refmath{Ty-StoreFrame} rule, we can infer that the context $\genv,
\hdef{v}{\tyframe{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}$
is well-formed.

$$\tystoreframe$$

From the premise $e : \typename$, we conclude that
$\withgenvand{\hdef{v}{\tyframe{\tyvar}{\lvec{t\subi}{\polybase}{\polybound}}}{\storeframe{\xdata}{\tyvar}{\lvec{t\subi}{\polybase}{\polybound}}}}{e
: \typename}$.
\end{itemize}
\end{proofcase}

%
% LetSubscript is actually not so hard after all.
%
\begin{proofcase}{LetSubscript}
\settypename{\tyexp}
\renewcommand\indexatom[2]{#2}

There are several subscripting cases.

\begin{itemize}
\item

\begin{block}
\setarrayname{v_2}
\setelemname{j}

Suppose the reduction uses the rule \refmath{Red-LetSub-Tuple}.

$$\redletsubtuple$$

The proof of typing must end in the rule \refmath{Ty-LetSub-Tuple}:

$$\tyletsubtuple$$

From the premise $\withgenv{v_2 : \tytuple{c}{u_0, \ldots, u_{j - 1}, \alttypename, u_{j + 1},
\ldots, u_{n-1}}}$, we can infer that $\alttypename'$ is the tuple type $\tytuple{c}{u_0, \ldots, u_{j - 1},
\alttypename, u_{j + 1}, \ldots, u_{n-1}}$.  In addition, the \wfcontextvalueslemma{} implies that
$\withgenv{h_j : \alttypename}$.  Finally, from the premise $\withenv{\genv,
\vdef{v_1}{\alttypename}}{e : \typename}$ and the
\refmath{Thin-Var-Def} rule, we can infer that $\withenv{\genv, \hdef{v_1}{\alttypename}{h_j}}{e :
\typename}$.
\end{block}

\item

\begin{block}
\setarrayname{v_2}
\setoffsetname{j}
Suppose the reduction uses the rule \refmath{Red-LetSub-Array}.

$$\redletsubarray$$

The corresponding type rule is \refmath{Ty-LetSub-Array}.

$$\tyletsubarray$$

From the premise $\withgenvtype{\arrayname : \tyarray{\alttypename}}$, we can infer that
$\alttypename' = \tyarray{\alttypename}$.  In addition, the \wfcontextvalueslemma{} implies that
$\withgenv{h_j : \alttypename}$.  Finally, from the premise $\withenv{\genv,
\vdef{v_1}{\alttypename}}{e : \typename}$ and the
\refmath{Thin-Var-Def} rule, we can infer that $\withenv{\genv, \hdef{v_1}{\alttypename}{h_j}}{e :
\typename}$.
\end{block}

\item

\begin{block}
\setarrayname{v_2}
\setoffsetname{i}
Suppose the reduction uses the rule \refmath{Red-LetSub-Union}.

$$\redletsubunion$$

The corresponding type rule is \refmath{Ty-LetSub-Union}.

$$\tyletsubunion$$

From the premise $\withgenvtype{\arrayname :
\tyunion{\tyvar}{\cvec{t'\subi}{\polybase}{\polybound}}{\mkset{j}}}$ we infer that $u' =
\tyunion{\tyvar}{\cvec{t'\subi}{\polybase}{\polybound}}{\mkset{j}}$.  In addition, the \wfcontextvalueslemma{} implies that
$\withgenv{h_k : \alttypename}$.  Finally, from the premise $\withenv{\genv,
\vdef{v_1}{\alttypename}}{e : \typename}$ and the \refmath{Thin-Var-Def} rule, we can infer that $\withenv{\genv, \hdef{v_1}{\alttypename}{h_j}}{e : \typename}$.
\end{block}

\item

Suppose the reduction uses the rule \refmath{Red-LetSub-RawData}.

$$\redletsubrawdata$$

By assumption, the runtime function $\runtimeload{\genv}{\storedata{\xdata}}{j}{\alttypename}$
establishes a derivation $\withgenv{h : \alttypename}$.  From the \refmath{Thin-Var-Def} rule, we
can infer that $\withenv{\genv, \hdef{v_1}{\alttypename}{h}}{e : \typename}$.

\item

Suppose the reduction uses the rule \refmath{Red-LetSub-Frame}.

$$\redletsubframe$$

By assumption, the runtime function
$\runtimeload{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{\alttypename}$
establishes a derivation $\withgenv{h : \alttypename}$.  From the \refmath{Thin-Var-Def} rule, we
can infer that $\withenv{\genv, \hdef{v_1}{\alttypename}{h}}{e : \typename}$.

\end{itemize}
\end{proofcase}

%
% SetSubscript is about the same.
%
\begin{proofcase}{SetSubscript}
\setarrayname{v}
\setoffsetname{j}
\setelemname{\heapname}
\settypename{\tyexp}
\renewcommand\indexatom[2]{#2}

There are several cases for subscript assignment on arrays.

\begin{itemize}
\item
Suppose the reduction uses the rule \refmath{Red-SetSub-Tuple}.

$$\redsetsubtuple$$

The proof of typing must end in the rule \refmath{Ty-SetSub-Tuple}:

$$\tysetsubtuple$$

From the premise $\arrayname : \tytuple{c}{u_0, \ldots, u_{j-1}, \alttypename, u_{j+1}, \ldots, u_{n-1}}$,
we infer that $\alttypename'$ is the array type $\tyarray{u}$.
From the premise $h : \alttypename$, we know the context
$\genv,\hdef{\arrayname}{\tyarray{u}}{\storetuple{\lvec{h\subi}{0}{i-1},h,\lvec{h\subi}{i+1}{n-1}}}$ is
well-formed.  From the premise $e : \typename$ we conclude 
$\withenv{\genv',\hdef{\arrayname}{u}{\storetuple{\lvec{h\subi}{0}{i-1},h,\lvec{h\subi}{i+1}{n-1}}}}{e
: \typename}$.

\item
Suppose the reduction uses the rule \refmath{Red-SetSub-Array}.

$$\redsetsubarray$$

The proof of typing must end in the rule \refmath{Ty-SetSub-Array}:

$$\tysetsubarray$$

From the premise $\arrayname : \tytuple{c}{u_0, \ldots, u_{j-1}, \alttypename, u_{j+1}, \ldots, u_{n-1}}$,
we can infer that $\alttypename'$ is the tuple type $\tytuple{c}{u_0, \ldots, u_{i-1}, \alttypename, u_{i+1},
\ldots, u_{n-1}}$.  From the premise $h : \alttypename$, we know the context
$\genv,\hdef{\arrayname}{u}{\storetuple{\cvec{h\subi}{0}{i-1},h,\cvec{h\subi}{i+1}{n-1}}}$ is
well-formed.  Finally, from the premise $e : \typename$ we conclude that
$\withenv{\genv',\hdef{\arrayname}{u}{\storetuple{\cvec{h\subi}{0}{i-1},h,\cvec{h\subi}{i+1}{n-1}}}}{e
: \typename}$.

\item
Suppose the reduction uses the rule \refmath{Red-SetSub-Union}.

$$\redsetsubunion$$

The proof of typing must end in the rule \refmath{Ty-SetSub-Union}:

$$\tysetsubunion$$

From the premise $\withgenvtype{\arrayname :
\tyunion{\tyvar}{\cvec{t'\subi}{\polybase}{\polybound}}{\mkset{j}}}$ we infer that
$\alttypename' = \tyunion{\tyvar}{\cvec{t'\subi}{\polybase}{\polybound}}{\mkset{j}}$.  From the
premise $h : \alttypename$, we know the context
$\genv,\hdef{\arrayname}{u}{\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbasezero}{\varboundzero}}}$ is
well-formed.  Finally, from the premise $e : \typename$ we conclude that
$\withenv{\genv',\hdef{\arrayname}{u}{\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbasezero}{\varboundzero}}}}{e
: \typename}$.

\item
Suppose the reduction uses the rule \refmath{Red-SetSub-RawData}.

$$\redsetsubrawdata$$

The corresponding type rule is \refmath{Ty-SetSub-RawData}.

$$\tysetsubrawdata$$

We can infer that $\alttypename' = \tyrawdata$.  Since $\storedata{\xdata'} : \tyrawdata$, we
conclude that $\withenv{\genv', \hdef{\arrayname}{\tyrawdata}{\storedata{\xdata'}}}{e : \typename}$.

\item
Suppose the reduction uses the rule \refmath{Red-SetSub-Frame}.

$$\redsetsubframe$$

The corresponding type rule is \refmath{Ty-SetSub-Frame}.

$$\tysetsubframe$$

We can infer that $\alttypename' = \tyframe{\tyvar}{\lvec{t\subi}{\polybase}{\polybound}}$.  Since
$\storeframe{\xdata'}{\tyvar}{\lvec{t\subi}{\polybase}{\polybound}} :
\tyframe{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}$, we
conclude that $\withenv{\genv',
\hdef{v}{\alttypename'}{\storeframe{\xdata'}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}}{e : \typename}$.

\end{itemize}
\end{proofcase}

%
% LetGlobal same.
%
\begin{proofcase}{LetGlobal}
\settypename{\tyexp}

Suppose that $e_r = \letglobal{v}{\alttypename}{\xlabel}{e}$.  The operational rule is
\refmath{Red-LetGlobal}.

$$\redletglobal$$

The proof of typing must end in the rule \refmath{Ty-LetGlobal}.

$$\tyletglobal$$

From the premise $\xlabel : \alttypename$, we know that $\alttypename' = \alttypename$.  From the
premise $\withgenvand{\vdef{v}{u}}{e : \typename}$, we conclude that $\withgenvand{\hdef{v}{u}{h}}{e : \typename}$.
\end{proofcase}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SetGlobal
%
\begin{proofcase}{SetGlobal}
\settypename{\tyexp}

Suppose that $e_r = \setglobal{\xlabel}{\alttypename}{h}{e}$.  The operational rule is
\refmath{Red-SetGlobal}.

$$\redsetglobal$$

The proof of typing must end in the rule \refmath{Ty-SetGlobal}.

$$\tysetglobal$$

From the premise $\xlabel : \alttypename$, we know that $\alttypename' = \alttypename$.  From the
premise $\withgenv{e : \typename}$, we conclude that $\withenv{\genv', \hdef{l}{u}{h}}{e : \typename}$.
\end{proofcase}

\end{proof}

%
% End of tech report mode
%
\fi


% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
