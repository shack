%
%
%

\ifpaper

\sectionA{Benchmarks}

System benchmarks are shown in Figure~\ref{figure:benchmarks} for version 0.5.0 of the \mojave{}
compiler, which was released in May 2002, about a year after the \mojave{} project started.  The
performance numbers measure total real execution time on an unloaded 700MHz Intel Pentium III.  The
\mojave{} system is freely available at \texttt{mojave.caltech.edu} under the GNU General Public License.

The \mojave{} system is currently under development, and benchmark performance varies widely.
Performance numbers are given for several compilers.  The {\tt gcc} column uses the GNU compiler
collection, version 2.96; {\tt gcc2} uses the {\tt -O2} optimization.  The {\tt mcc2} columns list performance
numbers for the \mojave{} compiler.  For comparison purposes (only), the {\tt mcc2u} column lists
performance without runtime safety checks.  In the current state of development, the {\tt mcc2}
compiler performs only minimal optimization, including dead-code elimination, function inlining, and
assembly peephole optimization.  Advanced \fir{} optimizations are fairly easy to implement, and the
{\tt mcc6u} column lists performance numbers using an optimizer under development that implements
alias analysis and partial redundancy elimination.  Naml benchmarks are similar, and include numbers
for the INRIA OCaml compiler~\cite{remy:ocaml}, version 3.04.

The specific benchmarks include the following.  The {\tt fib} program computes the $n^\ms{th}$
Fibonacci number (using the naive algorithm).  This benchmark is highly recursive, and the
performance numbers reflect the use of continuation-passing style.  The {\tt mcc} programs allocate
an exponential number of closures \emph{on the heap}, and much of the time is spent in garbage
collection.

The {\tt mandel} benchmark computes a Mandelbrot set.  This is a corner case where {\tt mcc} C
compiler, using the standard optimizations, happens to perform significantly better than {\tt gcc
-O2} (performance numbers for {\tt gcc -O3} are shown in parentheses).  In contrast, the performance
for Naml reflects the use of minimal optimization.  The {\tt mandel.ml} is implemented with
fixed-point numbers, and each arithmetic operation is a function call.  The {\tt ocamlopt} compiler
inlines the function calls, while {\tt mcc2} and {\tt ocamlc} do not.

The {\tt msort} benchmarks implement a bubble-sort algorithm, {\tt imat1} performs integer matrix
multiplication, and {\tt fmat1} tests floating-point matrix multiplication.

The {\tt migrate} benchmark measures the ``minimal'' process migration time.  The program consists
of a single migration call.  Nearly all of the time is spent in recompilation on the target machine.

The {\tt regex} algorithm is shown in Figure~\ref{figure:regex}, and represents a naive Unix-style
regular-expression matcher, using transactions to simulate functional programming.  The function
$\mb{atomic\_entry}(i)$ enters a transaction, with parameter $i$, $\mb{atomic\_commit}()$ commits
the most recent transaction, and $\mb{atomic\_rollback}(i)$ rolls-back the most recent transaction
with parameter $i$.  The time listed is for determining that the pattern
\verb+*h*e*l*l*o*w*o*r*l*d*+ does \emph{not} occur in the text of the introduction to this paper.
The benchmark enters 945341 transactions with a maximum transaction nesting depth of 6833.

\makefigure{figure:benchmarks}{Mojave benchmarks}{%
\begin{center}
\begin{tabular}{|l|lllll|}
\hline
\multicolumn{6}{|l|}{C benchmarks (time in seconds)}\\
\hline\hline
Name            & gcc  & gcc2 & mcc2 & mcc2u & mcc6u\\
\hline
fib 35          & 1.0  & 0.78 & 4.6  & 4.6  & 4.32\\
mandel          & 54.7 & 42.1 (5.5) & 7.2  & 7.3  & 6.0\\
msort1          & 3.83 & 1.15 & 5.92 & 3.01 & \\
msort4          & 5.4  & 1.15 & 8.22 & 4.13 & \\
imat1           & 37.1 & 6.27 & 27.9 & 17.3 & 7.6\\
fmat1           & 8.9  & 2.98 & 10.2 & 8.33 & 4.86\\
migrate         &      &      & 1.77 &      &\\
regex           &      &      & 2.87 &      &\\
\hline
\multicolumn{6}{c}{}\\
\hline
\multicolumn{6}{|l|}{Naml benchmarks (time in seconds)}\\
\hline\hline
Name            & ocamlc & ocamlopt & mcc2 & mcc2u &\\
\hline
fib 35          & 3.89   & 0.61     & 8.33    & 7.81 & \\
mandel          & 545    & 8.1      & 183     & 160 & \\
\hline
\end{tabular}
\end{center}}

\makefigure{figure:regex}{Unix-style pattern matching using transactions}{%
\begin{center}
\begin{code}
\mb{int} match(\mb{const char} *ppat, \mb{const char} *pbuf)\\
\{\+\\
   \mb{if}(\mb{atomic\_entry}(0) != 0) \{\+\\
      print\_string("Pattern did not match");\\
      \mb{return}(FALSE);\-\\
   \}\\
   \mb{while}(*ppat != 0) \{\+\\
      \mb{if}(*ppat == '*') \{\+\\
         if(*pbuf == 0) \{ ppat++; \}\\
        \mb{else if}(\mb{atomic\_entry}(0) == 0) \{ pbuf++; \}\\
        \mb{else} \{ \mb{atomic\_commit}(); ppat++; \}\-\\
      \} \mb{else if}(*pbuf == *ppat) \{ ppat++; pbuf++; \}\\
      else \{ \mb{atomic\_rollback}(1); \}\-\\
   \}\\
   \mb{if}(*pbuf != 0) \{ \mb{atomic\_rollback}(1); \}\\
   print\_string("Pattern matched");\\
   return(TRUE);\-\\
\}
\end{code}
\end{center}}

\ifpaper
\section{Conclusion}

The \mojave{} compiler is in an early stage of development, but we believe that it demonstrates the
feasibility of practical process migration and transactional computing.
We intend the \mojave{} compiler to be a testbed for the development of distributed
algorithms, as well as the application of domain-specific formal
methods~\cite{aydemir:mojave-metaprl,granicz:hicss36-03}.
The intermediate language has served as a solid foundation for safe multi-language compilation.
\fi

\fi   % POPL paper?

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
