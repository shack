%
% Type judgments and syntax
%

\sectionA{Judgments}
\label{section:judgments}

All judgments, including type and well-formedness judgments, are defined with
respect to an environment $\genv$, which we also call a \emph{context}.  The
environment contains both variable declarations of the form $\vdef{v}{t}$, and
variable definitions of the form $\hdef{v}{t}{b}$.  The declaration
$\vdef{v}{t}$ specifies that a variable $v$ has an unspecified value of type
$t$.  The definition $\hdef{v}{t}{b}$ specifies that variable $v$ has the
value $b$, and $b$ has type $t$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Heap and store values
%

\sectionB{Heap and store values}

The definitions in the context use values of two sorts:
heap values $h$, and store values $b$.  The \deflabel{heap-value}{heap values}
represent atoms that have been fully evaluated.
\iftechreport
   In general, an atom evaluates to a number, a label, or a variable that
   represents a store value (described below).
\fi

\makefigure{figure:heap-values}{Heap values}{%
$$
\begin{array}{rcll}
& & \hbox{Definition} & \hbox{Description}\\
\hline
h & ::= & \atomenum{i_\ms{bound}}{i_\ms{value}}
        & \hbox{Enumerated values}\\
  &  |  & \atomint{i}
        & \hbox{Boxed integer constants}\\
  &  |  & \atomrawint{\rawint}{\pre}{\sign}
        & \hbox{Raw integer constants}\\
  &  |  & \atomfloat{x}{\fpre}
        & \hbox{Floating-point constants}\\
\iftechreport
  &  |  & \atomlabel{\tyvar}{\framelabel{f}}{\framelabel{r}}{r}
        & \hbox{Offset into a frame record}\\
\fi
  &  |  & v
        & \hbox{Variables}
\end{array}
$$}

The \deflabel{store-value}{store values} are the values in a program store.
These include heap values, functions, ``packed'' values with existential type,
and data in each of the aggregate data types: tuples, arrays, elements of a
union type,
\iftechreport
   rawdata, and frames.
\else
   and rawdata.
\fi

Functions are universally quantified, with type parameters
$\cvec{\alpha\subi}{1}{m}$,\footnote{We 
allow $m=0$ here; that is, a function may or may not have any type parameters.}
and actual parameters $\cvec{v\subi}{1}{n}$.
Elements of a union type are like tagged tuples; they include the type
variable $\tyvar$ that defines the union space, an index $i$ for the \emph{union
tag}, and the elements of the tuple being tagged.  Elements of type
$\tyrawdata$ are represented abstractly using the form $\storedata{\xdata}$; the
elements in the data area are not explicitly described.
\iftechreport
   Frame data is also represented abstractly with the form
   $\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{1}{n}}$, where
   $\tyframe{\tyvar}{\cvec{t\subi}{1}{n}}$ is the type
   of the frame.
\fi

\makefigure{figure:store-values}{Store values}{%
$$
\begin{array}{rcll}
& & \hbox{Definition} & \hbox{Description}\\
\hline
b & ::= & h
        & \hbox{Heap values}\\
  &  |  & \storefunpoly{\cvec{\alpha\subi}{1}{m}}{\cvec{v\subi}{1}{n}}{e}
        & \hbox{Functions}\\
  &  |  & \atomtypack{t}{v}{\cvec{\typename\subi}{\polybase}{\polybound}}
        & \hbox{Type packing}\\
  &  |  & \storetuple{\cvec{\heapname\subi}{\varbase}{\varbound}}
        & \hbox{Tuples and arrays}\\
  &  |  & \storeunion{\tyvar}{i}{\cvec{\heapname\subi}{\varbase}{\varbound}}
        & \hbox{Unions}\\
  &  |  & \storedata{\xdata}
        & \hbox{Raw data}\\
\iftechreport
  &  |  & \storeframe{\xdata}{\tyvar}{\cvec{t\subi}{1}{n}}
        & \hbox{Frame data}
\fi
\end{array}
$$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Kinds
%

\sectionB{Kinds}

The program types are also defined/declared as part of the context $\genv$.
For presentation purposes, we classify the program types with \emph{kinds}.
The program kinds are shown in Figure~\ref{figure:kinds}.  The kind $\skind$
classifies the type definitions $\stydef$ as ``small'' types $\Smalltype$,
``large'' types $\Type$,
\iftechreport
   disjoint unions $\Uniontype{n}$, and frames $\Frametype$.
\else
   and disjoint unions $\Uniontype{n}$.
\fi

The distinction between small and large types is necessary to assist the garbage
collector.  The raw integers and floating-point values are large, and all
other types are small.  Values of small type fit into a single machine word,
and small values are tagged so the garbage collector can distinguish between
pointers and integers. In general, polymorphic functions and data structures
can be instantiated with small types, but not large types.

The general kind $k = \Typefun{m}{\skind}$ represents a parameterized type
definition $\tydef$.  The number of parameters $m$ may be any nonnegative
integer.  If $m = 0$, we often omit the type parameters.

\makefigure{figure:kinds}{Kinds}{%
$$
\begin{array}{rcll}
\iftechreport
   \skind & ::= & \Smalltype\\
          & |   & \Type\\
          & |   & \Uniontype{n}\\
          & |   & \Frametype\\
\else
   \skind & ::= & \Smalltype \vbar \Type \vbar \Uniontype{n}
\fi
\\
\kind & ::= & \Typefun{m}{\skind}
\end{array}
$$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Contexts
%

\sectionB{Contexts}

A program context $\genv$ is defined as a set of mutually-recursive
declarations and definitions, as shown in Figure~\ref{figure:contexts}.  There
are three forms of definitions.

\begin{enumerate}
   \item The type definition $\hdef{\tyvar}{k}{\tydef}$ defines a type
         named $\tyvar$, having kind $k$, and value $\tydef$.
   \item The variable definition $\hdef{v}{t}{b}$ defines a variable
         named $v$, with type $t$ and store value $b$.
   \item The global definition $\hdef{l}{t}{h}$ defines a global
         label $l$, with type $t$ and heap value $h$.
\end{enumerate}

For each definition form there is a corresponding \emph{declaration} form,
with syntax as described in Figure~\ref{figure:contexts}.

\nocomment{Emre: We need to say something like the next sentence.
         Anyone have a better way of stating it?\\
         Emre (update): According to JDS, the following sentence is just
         bogus.  In any event, we \emph{really} need to clarify
         our treatment of $\Gamma$.\\
         JDS:  We need to resolve this mess once and for all.\\
         Emre: I added a 2nd sentence to the next paragraph.  Okay?\\
         JDS:  Ok}

We assume that each variable, type variable, and global label in a context
is defined/declared at most once, and we use alpha-renaming throughout this paper to rename
variables as appropriate.

\makefigure{figure:contexts}{Program contexts}{%
$$
\begin{array}{rcll}
\gvdef & ::= & \vdef{v}{t}                 & \hbox{Variable declaration} \\
       & |   & \hdef{v}{t}{b}              & \hbox{Variable definition} \\
       & |   & \vdef{l}{t}                 & \hbox{Global declaration} \\
       & |   & \hdef{l}{t}{h}              & \hbox{Global definition} \\
       & |   & \tyvar\colon \kind          & \hbox{Type declaration} \\
       & |   & \tyvar\colon \kind = \tydef & \hbox{Type definition} \\

\\

\genv & ::= & \epsilon      & \hbox{Empty environment} \\
      & |   & \genv, \gvdef & \hbox{Adding a definition}
\end{array}
$$}

\iftechreport
   Note that we also use the meta-variable $d$ to represent
   a definition or declaration $\gvdef$.
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Judgments
%

\sectionB{Judgments}

The judgments are shown in Figure~\ref{figure:judgments}.  The judgment
$\withgenv{\diamond}$ specifies that the context $\genv$ is well-formed.
\iftechreport
   As we show in Section~\ref{section:lemmas-wf}, a
\else
   A
\fi
context is well-formed if all of its declarations and definitions are
well-formed.  For each declaration $\vdef{v}{t}$ and definition
$\hdef{v}{t}{b}$, the term $t$ must be a well-formed type, and the value $b$
must have type $t$.  Similarly, all type and global definitions in $\genv$
must be well-formed.

The judgment $\withgenv{\tyequal{\tydef_1}{\tydef_2}{\kind}}$ is a type
definition equality judgment.  When the judgment is true, $\tydef_1$ and
$\tydef_2$ have the specified kind, and they are equal.  There is no separate
membership judgment $\withgenv{\tymember{\tydef}{\kind}}$.  However, we will
often use the shorthand form $\withgenv{\tydef : \kind}$ to stand for
$\withgenv \tyequal{\tydef}{\tydef}{\kind}$.

The four judgments $\withgenv{l : t}$, $\withgenv{a : t}$, $\withgenv{b : t}$,
$\withgenv{e : t}$ express typing relations for labels, atoms, store values,
and expressions, respectively.

There are also several auxiliary judgment forms.  The
$\withgenv{\tailop : \tyspecial{t}}$ judgment specifies that $\tailop$ is
a special-call operator returning type $t$.  The
$\withgenv{\allocop : \tyalloc{t}}$ judgment specifies that $\allocop$ is
an allocation operator for a value of type $t$.
\iftechreport
   The record judgment $\withgenv{R : \Recordtype}$ specifies that $R$ is
   a valid record type that can be used as a subrecord in a frame.
\fi

\makefigure{figure:judgments}{Judgments}{%
$$
\begin{array}{ll}
\hbox{Judgment} & \hbox{Interpretation} \\
\hline

% Context well-formedness
\withgenv{\diamond} & \hbox{Context $\genv$ is well-formed} \\

% Type equality
\withgenv{\tyequal{\tydef_1}{\tydef_2}{\kind}} &
   \iftechreport
      \hbox{$\tydef_1$ and $\tydef_2$ are equal type definitions (or types)}
   \else
      \hbox{$\tydef_1$ and $\tydef_2$ are equal}
   \fi \\

% Type variable equality
\iftechreport
   \withgenv{\tyequal{\tyvar_1}{\tyvar_2}{\kind}} &
   \hbox{$\tyvar_1$ and $\tyvar_2$ are equal type definitions}\\
\fi

% Various typing judgements
\withgenv{l : t} & \hbox{Global label $l$ has type $t$}\\
\withgenv{a : t} & \hbox{Atom $a$ has type $t$}\\
\withgenv{b : t} & \hbox{Store value $b$ has type $t$}\\
\withgenv{e : t} & \hbox{Program $e$ has type $t$}\\

% Tailop typing
\withgenv{\tailop : \tyspecial{t}} &
   \iftechreport
      \hbox{$\tailop$ is a special-call of type $t$}
   \else
      \hbox{$\tailop$ is a special-call}
   \fi\\

% Alloc typing
\withgenv{\allocop : \tyalloc{t}} &
   \iftechreport
      \hbox{$\allocop$ is an allocation of a value of type $t$}
   \else
      \hbox{$\allocop$ is an allocation}
   \fi\\

% Record typing
\iftechreport
   \withgenv{R : \Recordtype} & \hbox{$R$ is a record type}
\fi
\end{array}
$$}

%
% Don't include substitution in the POPL paper
%
\iftechreport

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Free variables and substitution
%
\sectionB{Free variables and substitution}

In order to define the typing rules and operational semantics, we first need
to define type substitution.

\sectionC{Domain of context $\genv$}

The \emph{domain} $\dom{\genv}$ of a context $\genv$ is the set of type
variables, variables, and global labels that are declared and defined.
We use $\hint$ below as a placeholder for any syntactically
valid term.

$$
\begin{array}{rcl}
\dom{\epsilon} & = & \{ \}\\
\dom{\genv, \vdef{v}{\hint}} & = & \dom{\genv} \cup \{ v \} \\
\dom{\genv, \hdef{v}{\hint}{\hint}} & = & \dom{\genv} \cup \{ v \} \\
\dom{\genv, \vdef{l}{\hint}} & = & \dom{\genv} \cup \{ l \} \\
\dom{\genv, \hdef{l}{\hint}{\hint}} & = & \dom{\genv} \cup \{ l \} \\
\dom{\genv, \vdef{\tyvar}{\hint}} & = & \dom{\genv} \cup \{ \tyvar \} \\
\dom{\genv, \hdef{\tyvar}{\hint}{\hint}} & = & \dom{\genv} \cup \{ \tyvar \} \\
\end{array}
$$

\sectionC{Free variables}

The \emph{free variables} $\freevars{\genv}$ of a context are defined
inductively.  To simplify the presentation, we define the free-variables of a
term as a set of \emph{all} the variables, including type variables, normal
variables, and global labels that are free in the term.

The free variables of a type are shown in Figure~\ref{figure:free-vars-type}.

\makefigure{figure:free-vars-type}{Type free variables}{
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
t & \freevars{t}\\
\hline
\tyint & \{ \}\\
\tyenum{i} & \{ \}\\
\tyrawint{\pre}{\sign} & \{ \}\\
\tyfloat{\fpre} & \{ \}\\
\tytuple{\tupleclass}{\cvec{\typename\subi}{\polybase}{\polybound}} & \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}\\
\tyarray{t} & \freevars{t}\\
\tyunion{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}}{\intset} & \{ \tyvar \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}\\
\tyrawdata & \{ \}\\
\tyframe{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}} & \{ \tyvar \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}\\
\tyfun{(\cvec{\typename\subi}{\polybase}{\polybound})}{t} & \freevars{t} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}\\
\alpha & \{ \alpha \}\\
\tyapply{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}} & \{ \tyvar \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}\\
\tyall{\cvec{\alpha\subi}{\polybase}{\polybound}}{t} & \freevars{t} - \{ \cvec{\alpha\subi}{\polybase}{\polybound} \}\\
\tyexists{\cvec{\alpha\subi}{\polybase}{\polybound}}{t} & \freevars{t} - \{ \cvec{\alpha\subi}{\polybase}{\polybound} \}\\
\typroject{v}{i} & \{ v \}
\end{array}
$$}

The free variables of a type definition are shown in
Figure~\ref{figure:free-vars-tydef}.  The free variables of a record do not
include the frame labels.

\makefigure{figure:free-vars-tydef}{Type definition free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
t & \freevars{t}\\
\hline
\xvector{\cvec{t\subi}{\varbase}{\varbound}} & \bigcup_{i = \varbase}^{\varbound} \freevars{t_i}\\
\plusvec{\vec{t}\subi}{\varbase}{\varbound} & \bigcup_{i = \varbase}^{\varbound} \freevars{\vec{t}}\\
\tyrecord{\cvec{\framelabel{r}\subi : t\subi}{\varbase}{\varbound}} & \bigcup_{i = \varbase}^{\varbound} \freevars{t_i}\\
\tyframerecord{\cvec{\framelabel{f}\subi : \tyrecordname\subi}{\varbase}{\varbound}} & \bigcup_{i = \varbase}^{\varbound} \freevars{\tyrecordname_i}\\
\tydeflambda{\cvec{\alpha\subi}{\polybase}{\polybound}}{\stydef} & \freevars{\stydef} - \{ \cvec{\alpha\subi}{\polybase}{\polybound} \}
\end{array}
$$}

The free variables of an atom are shown in Figure~\ref{figure:free-vars-atom}.

The free variables of unary operators are defined as follows.
$$\freevars{\unop} ::=
   \begin{cases}
      \freevars{t_d}\cup\freevars{t_s} & \hbox{if $\unop = \ucoerceop{t_d}{t_s}$}\\
      \{\} & \hbox{otherwise}\\
   \end{cases}
$$
The free variables of binary operators are defined as follows.
$$\freevars{\binop} ::=
   \begin{cases}
      \freevars{t} & \hbox{if $\binop=\mathop{\eqop{t}}$ or $\binop=\mathop{\neqop{t}}$}\\
      \{\} & \hbox{otherwise}
   \end{cases}
$$


\nocomment{JDS:  Problem:  $\unop$ and $\binop$ can contain
         types, which need to be considered in the FV computation.\\
         Emre:  I've modified the paragraph before this comment. The
         binop stuff looks hideous, but I don't know what to do about it.
         I've updated the atom free vars table accordingly... ugh.
         (line 380-400 in judgments.tex)\\
         JDS: Ok}

\makefigure{figure:free-vars-atom}{Atom free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
a & \freevars{a}\\
\hline
\atomenum{i_\ms{bound}}{i_\ms{value}} & \{ \}\\
\atomint{i} & \{ \}\\
\atomrawint{\rawint}{\pre}{\sign} & \{ \}\\
\atomfloat{x}{\fpre} & \{ \}\\
\atomlabel{\tyvar}{\framelabel{f}}{\framelabel{r}}{\rawint} & \{ \tyvar \}\\
\atomsizeof{\cvec{\tyvar\subi}{\varbase}{\varbound}}{\rawint} & \{ \cvec{\tyvar\subi}{\varbase}{\varbound} \}\\
\atomconst{t}{\tyvar}{i} &
\iftechreport
   \{ \tyvar \} \cup \freevars{t}
\else
   \{ \tyvar \}
\fi\\
\atomvar{v} & \{ v \}\\
\atomtyapply{t}{a}{\cvec{\typename\subi}{\polybase}{\polybound}} & 
\iftechreport
    \freevars{t} \cup \freevars{a} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\else
    \freevars{a} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\fi\\
\atomtypack{t}{v}{\cvec{\typename\subi}{\polybase}{\polybound}} &
\iftechreport
   \freevars{t} \cup \{ v \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\else
   \{ v \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\fi\\
\atomtyunpack{v} & \{ v \}\\
\atomunop{\unop}{a} & \freevars{a} \cup \freevars{\unop}\\
\atombinop{\binop}{a_1}{a_2} & \freevars{a_1} \cup \freevars{a_2} \cup \freevars{\binop}
\end{array}
$$}

The free variables of an expression are shown in
Figure~\ref{figure:free-vars-exp}.

\makefigure{figure:free-vars-exp}{Expression free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
e & \freevars{e}\\
\hline
\letatom{v}{t}{a}{e} & \freevars{t} \cup \freevars{a} \cup (\freevars{e} - \{ v \})\\
\letext{v}{t_v}{s}{t_s}{\cvec{a\subi}{\varbase}{\varbound}}{e} &
    \freevars{t_v}
    \cup \freevars{t_s}
    \cup \left( \bigcup_{i = \varbase}^{\varbound} \freevars{a_i} \right)
    \cup \left( \freevars{e} - \{ v \} \right)\\
\tailcall{a}{\cvec{\atomname\subi}{\varbase}{\varbound}} &
    \freevars{a} \cup \bigcup_{i = \varbase}^{\varbound} \freevars{a_i}\\
\specialcall{\tailop} & \freevars{\tailop}\\
\matchstmt{a}{\cvec{\matchcase{s\subi}{e\subi}}{\varbase}{\varbound}} &
    \freevars{a} \cup \bigcup_{i = \varbase}^{\varbound} \freevars{e_i}\\
\letalloc{v}{\allocop}{e} &
    \freevars{\allocop} \cup (\freevars{e} - \{ v \})\\
\letsubscript{v}{t}{a_1}{a_2}{e} &
    \freevars{t} \cup \freevars{a_1} \cup \freevars{a_2} \cup (\freevars{e} - \{ v \})\\
\setsubscript{a_1}{a_2}{t}{a_3}{e} &
    \freevars{a_1} \cup \freevars{a_2} \cup \freevars{t} \cup \freevars{a_3} \cup \freevars{e}\\
\letglobal{v}{t}{\xlabel}{e} &
    \freevars{t} \cup \{ \xlabel \} \cup (\freevars{e} - \{ v \})\\
\setglobal{\xlabel}{t}{a}{e} &
    \{ \xlabel \} \cup \freevars{t} \cup \freevars{a} \cup \freevars{e}
\end{array}
$$}

The expression free variables use the definition of free variables for
special-calls (shown in Figure~\ref{figure:free-vars-tailop}) and allocation
operations (shown in Figure~\ref{figure:free-vars-allocop}).

\makefigure{figure:free-vars-tailop}{Allocation operator free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
\allocop & \freevars{\allocop}\\
\hline
\alloctuple{\tupleclass}{t}{\cvec{\atomname\subi}{\varbase}{\varbound}} &
   \freevars{t} \cup \bigcup_{i = \varbase}^{\varbound} \freevars{a_i}\\
\allocunion{t}{\tyvar}{\cvec{\atomname\subi}{\polybase}{\polybound}}{i} &
   \freevars{t} \cup \{ \tyvar \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{a_i}\\
\allocarray{t}{a_\ms{size}}{a_\ms{init}} &
   \freevars{t} \cup \freevars{a_{\ms{size}}} \cup \freevars{a_{\ms{init}}}\\
\allocmalloc{t}{a} &
   \freevars{t} \cup \freevars{a}\\
\allocframe{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}} &
   \{ \tyvar \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\end{array}
$$}

\makefigure{figure:free-vars-allocop}{Special-call free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
\tailop & \freevars{\tailop}\\
\hline
\tailsysmigrate{i}{a_p}{a_o}{a_\ms{fun}}{\cvec{\atomname\subi}{\varbase}{\varbound}} &
    \freevars{a_p} \cup \freevars{a_o} \cup \freevars{a_\ms{fun}} \cup \bigcup_{i = \varbase}^{\varbound}
    \freevars{a_i}\\
\tailatomic{a_\ms{fun}}{a_\ms{const}}{\cvec{\atomname\subi}{\varbase}{\varbound}} &
    \freevars{a_\ms{fun}} \cup \freevars{a_\ms{const}} \cup \bigcup_{i = \varbase}^{\varbound} \freevars{a_i}\\
\tailatomicrollback{a_\ms{level}}{a_\ms{const}} &
    \freevars{a_\ms{level}} \cup \freevars{a_\ms{const}}\\
\tailatomiccommit{a_\ms{level}}{a_\ms{fun}}{\cvec{\atomname\subi}{\varbase}{\varbound}} &
    \freevars{a_\ms{level}} \cup \freevars{a_\ms{fun}} \cup \bigcup_{i = \varbase}^{\varbound} \freevars{a_i}
\end{array}
$$}

The free variables of store values are shown in
Figure~\ref{figure:free-vars-store}.

\makefigure{figure:free-vars-store}{Store free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
b & \freevars{b}\\
\hline
h & \freevars{h}\\
\storefunpoly{\cvec{\alpha\subi}{\polybase}{\polybound}}{\cvec{v\subi}{\varbase}{\varbound}}{e} &
   \freevars{e} - \{ \cvec{\alpha\subi}{\polybase}{\polybound}, \cvec{v\subi}{\varbase}{\varbound} \}\\
\atomtypack{t}{v}{\cvec{\typename\subi}{\polybase}{\polybound}} &
\iftechreport
   \freevars{t} \cup \{ v \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\else
   \{ v \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\fi\\
\storetuple{\cvec{\heapname\subi}{\varbase}{\varbound}} &
   \bigcup_{i = \varbase}^{\varbound} \freevars{h_i}\\
\storeunion{\tyvar}{i}{\cvec{\heapname\subi}{\varbase}{\varbound}} &
   \{ \tyvar \} \cup \bigcup_{i = \varbase}^{\varbound} \freevars{h_i}\\
\storedata{\xdata} & \{ \}\\
\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}} &
   \{ \tyvar \} \cup \bigcup_{i = \polybase}^{\polybound} \freevars{t_i}
\end{array}
$$}

The free variables of context declarations and definitions are shown in
Figure~\ref{figure:free-vars-defs}.

\makefigure{figure:free-vars-defs}{Definition free variables}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
d & \freevars{d}\\
\hline

\vdef{\tyvar}{k} & \{ \}\\
\vdef{v}{t} & \freevars{t}\\
\vdef{l}{t} & \freevars{t}\\
\hdef{\tyvar}{k}{\tydef} &
    \freevars{\tydef}\\
\hdef{v}{t}{b} &
    \freevars{t} \cup \freevars{b}\\
\hdef{l}{t}{h} &
    \freevars{t} \cup \freevars{h}
\end{array}
$$}

The free variables of a context $\genv$ are the free variables of all the
definitions, excluding the variables being declared.

$$\freevars{\genv} = \left( \bigcup_{d \in \genv} \freevars{d} \right) - \dom{\genv}$$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Type substitution
%

\sectionC{Type substitution}
\label{section:type-subst}

Substitution $\tydef[u/\alpha]$ is defined over type definitions, substituting
a type $u$ for the type variable $\alpha$ in type definition $\tydef$.
Throughout this document, we assume that binding variables are renamed as
necessary to avoid capture.  Type substitution is shown in
Figure~\ref{figure:type-subst}.

\makefigure{figure:type-subst}{Type substitution}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
t & t[u/\alpha]\\
\hline
\tyint & \tyint\\
\tyenum{i} & \tyenum{i}\\
\tyrawint{\pre}{\sign} & \tyrawint{\pre}{\sign}\\
\tyfloat{\fpre} & \tyfloat{\fpre}\\
\tytuple{\tupleclass}{\cvec{\typename\subi}{\varbase}{\varbound}} &
   \tytuple{\tupleclass}{\cvec{\typename\subi[u/\alpha]}{\varbase}{\varbound}}\\
\tyarray{t} &
   \tyarray{t[u/\alpha]}\\
\tyunion{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}}{\intset} &
   \tyunion{\tyvar}{\cvec{t\subi[u/\alpha]}{\polybase}{\polybound}}{\intset}\\
\tyrawdata & \tyrawdata\\
\tyframe{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}} &
   \tyframe{\tyvar}{\cvec{t\subi[u/\alpha]}{\polybase}{\polybound}}\\
\tyfun{\argvec{\typename\subi}{\varbase}{\varbound}}{t} &
   \tyfun{\argvec{t\subi[u/\alpha]}{\varbase}{\varbound}}{t[u/\alpha]}\\
\beta &
   \begin{cases}
      u & \hbox{if $\beta = \alpha$}\cr
      \beta & \hbox{otherwise}
   \end{cases}\\
\tyapply{\tyvar}{\cvec{\typename\subi}{\polybase}{\polybound}} &
   \tyapply{\tyvar}{\cvec{t\subi[u/\alpha]}{\polybase}{\polybound}}\\
\tyall{\cvec{\beta\subi}{\polybase}{\polybound}}{t} &
   \begin{cases}
      \tyall{\cvec{\beta\subi}{\polybase}{\polybound}}{t} & \hbox{if $\alpha \in \{ \cvec{\beta\subi}{\polybase}{\polybound} \}$}\\
      \tyall{\cvec{\beta\subi}{\polybase}{\polybound}}{t[u/\alpha]} & \hbox{otherwise}
   \end{cases}\\
\tyexists{\cvec{\beta\subi}{\polybase}{\polybound}}{t} &
   \begin{cases}
      \tyexists{\cvec{\beta\subi}{\polybase}{\polybound}}{t} & \hbox{if $\alpha \in \{ \cvec{\beta\subi}{\polybase}{\polybound} \}$}\\
      \tyexists{\cvec{\beta\subi}{\polybase}{\polybound}}{t[u/\alpha]} & \hbox{otherwise}
   \end{cases}\\
\typroject{v}{i} & \typroject{v}{i}
\end{array}
$$}

Type substitution for type definitions is shown in
Figure~\ref{figure:subst-tydef}.

\makefigure{figure:subst-tydef}{Type definition substitution}{%
\renewcommand\arraystretch{1.2}
$$
\begin{array}{ll}
t & t[u/\alpha]\\
\hline
\xvector{\cvec{t\subi}{\varbase}{\varbound}} & \xvector{\cvec{t\subi[u/\alpha]}{\varbase}{\varbound}}\\
\plusvec{\vec{t}\subi}{\varbase}{\varbound} &
   \plusvec{\vec{t}\subi[u/\alpha]}{\varbase}{\varbound}\\
\tyrecord{\cvec{\framelabel{r}\subi : t\subi}{\varbase}{\varbound}} &
   \tyrecord{\cvec{\framelabel{r}\subi : t\subi[u/\alpha]}{\varbase}{\varbound}}\\
\tyframerecord{\cvec{\framelabel{f}\subi : \tyrecordname\subi}{\varbase}{\varbound}} &
   \tyframerecord{\cvec{\framelabel{f}\subi : \tyrecordname\subi[u/\alpha]}{\varbase}{\varbound}}\\
\tydeflambda{\cvec{\beta\subi}{\polybase}{\polybound}}{\stydef} &
   \begin{cases}
      \tydeflambda{\cvec{\beta\subi}{\polybase}{\polybound}}{\stydef} & \hbox{if $\alpha \in \{ \cvec{\beta\subi}{\polybase}{\polybound} \}$}\cr
      \tydeflambda{\cvec{\beta\subi}{\polybase}{\polybound}}{\stydef[u/\alpha]} & \hbox{otherwise}
   \end{cases}
\end{array}
$$}

%
% Don't include substitution in the POPL paper
%
\fi

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
