#include <stdio.h>
#include <ctype.h>

int main()
{
   int c;

   while((c = getchar()) != EOF) {
      if(isalpha(c))
          putchar(tolower(c));
   }
   return 0;
}
