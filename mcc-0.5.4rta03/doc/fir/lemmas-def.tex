%
%
%

\section{Fully-defined contexts}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Context values
%
\begin{lemma}
\thmtitle\fullydefined{lemma:fully-defined}{Fully-defined contexts}
Assume all of the following are true.
\begin{itemize}
\item $\genv_r$ is fully defined.
\item $\withenv{\genv_r} e_r : \tyvoid$
\item For each checkpoint $\checkpoint{\genv'}{f}{\cvec{a\subi}{\polybase}\varbound} \in \cenv_r$, the environment
  $\genv'$ is fully-defined, and $\withenv{\genv',
  \vdef{v_c}{\tyrawint{32}{\signed}}}{\tailcall{f}{v_c, \cvec{a\subi}{\polybase}\varbound} :
  \tyvoid}$.
\item $\gcstate{\genv_r}{\cenv_r}{e_r} \rightarrow \gcstate{\genv_c}{\cenv_c}{e_c}$
\end{itemize}

Then $\genv_c$ is fully defined, and for each $\checkpoint{\genv'}{f}{\cvec{a\subi}{\polybase}\varbound} \in
\cenv_c$, the environment $\genv'$ is fully-defined.
\end{lemma}

\begin{proof}
\setgenvname{\genvorig_r}
\setcenvname{\cenvorig_r}

The proof is by a case analysis on the reduction rule.  We will consider only those operational
rules that modify $\genv$ or $\cenv$, as the other cases are trivial.

%
% Type application
%

\begin{proofcase}{Type application}
Suppose $e_r = E[\atomtyapply{u}{f}{\cvec{u\subi}{\polybase}{\polybound}}]$.  The reduction rule is
\refmath{Red-Atom-Apply}.

$$\redatomapply$$

This rule introduces the new definitions
$\hvecdef{\alpha\subi}{\omega}{u\subi}{\polybase}{\polybound}$
and $\hdef{g}{t}{b}$.
\end{proofcase}

%
% Type abstraction
%

\begin{proofcase}{Type abstraction}
Suppose $e_r = E[\atomtypack{u}{v_1}{\cvec{u\subi}{\polybase}{\polybound}}]$.  The operational rule is
\refmath{Red-Atom-Pack}.

$$\redatompack$$

This rule introduces the new definition
$\hdef{v_2}{u}{\atomtypack{u}{v_1}{\cvec{u\subi}{\polybase}{\polybound}}}$.
\end{proofcase}

%
% Type unpacking
%

\begin{proofcase}{Type unpacking}
Suppose $e_r = E[\atomtyunpack{v_1}]$.  The operational rule is
\refmath{Red-Atom-Unpack}.

$$\redatomunpack$$

The rule introduces the new definitions $\hvecdef{\alpha\subi}{\Smalltype}{u\subi}{0}{m - 1}$ and
$\hdef{v_2}{t}{v}$.
\end{proofcase}

%
% LetAtom
%

\begin{proofcase}{LetAtom}
Suppose $e_r = \letatom{v}{\alttypename}{\heapname}{\expname}$.  The operational rule is
\refmath{Red-LetAtom}.

$$\redletatom$$

The rule introduces the new definition
$\hdef{v}{\alttypename}{\heapname}$.
\end{proofcase}

%
% LetExt
%

\begin{proofcase}{LetExt}
Suppose $e_r = \letext{v}{\typename}{s}{\tyfun{\argvec{u\subi}{\varbase}{\varbound}}{\typename}}{\cvec{h\subi}{\varbase}{\varbound}}{e}$.
The operational rule is \refmath{Red-LetExt}.

$$\redletext$$

The rule introduces the new definition
$\hdef{v}{\typename}{\semleft \text{``s''} \semright \argvec{h\subi}{\varbase}{\varbound}}$.
\end{proofcase}

%
% TailCall
%

\begin{proofcase}{TailCall}
Suppose $e_r = \tailcall{\atomfun{v}}{\cvec{h\subi}{\polybase}\varbound}$.  The operational rule is
\refmath{Red-TailCall}.

$$\redtailcall$$

The rule introduces the new definitions $\hvecdef{v\subi}{u\subi}{h\subi}{\polybase}\varbound$.
\end{proofcase}

%
% Special-calls
%

\begin{proofcase}{Special-calls}
There are several special-calls.

\begin{itemize}
\item

For $e_r = \specialcall{\tailsysmigrate{j}{\heapname_\ms{ptr}}{\heapname_\ms{off}}{f}{\cvec{\heapname\subi}{\polybase}\varbound}}$, the operational
rule is \refmath{Red-SysMigrate}.

$$\redsysmigrate$$

The contexts are not modified.

\item

For $e_r = \specialcall{\tailatomic{f}{\heapname_\xconst}{\cvec{h\subi}{\polybase}\varbound}}$, the operational rule is
\refmath{Red-Atomic}.

$$\redatomic$$

By assumption the context $\genv$ is fully-defined.

\item

For $e_r = \specialcall{\tailatomicrollback{i}{j}}$, if $i \neq 0$, the operational rule is
\refmath{Red-Atomic-Rollback-1}.

$$\redatomicrollbackone$$

This rule removes the checkpoints $\cvec{\checkpointname\subi}{m}{i + 1}$; the remaining checkpoints are
unchanged.

If $i = 0$, the operational rule is \refmath{Red-Atomic-Rollback-2}, which does not change the
checkpoint environment.

$$\redatomicrollbacktwo$$

\item

For $e_r = \specialcall{\tailatomiccommit{i}{f}{\cvec{\heapname\subi}{\polybase}\varbound}}$, the
operational rule is either \refmath{Red-Atomic-Commit-1} or \refmath{Red-Atomic-Commit-2}.

$$
\renewcommand\arraystretch{2.0}
\begin{array}{c}
\redatomiccommitone\\
\redatomiccommittwo
\end{array}
$$

In both cases, a checkpoint is removed from the checkpoint enviromnent $\cenv$, but the remaining
checkpoints are not modified.
\end{itemize}
\end{proofcase}

\begin{proofcase}{Allocation}
There are several cases here for each of the allocation types.

\begin{itemize}
\item
For $e_r = \letalloc{v}{\alloctuple{c}{\alttypename}{\cvec{h\subi}{\polybase}\varbound}}{e}$, the operational rule is
\refmath{Red-Alloc-Tuple}.

$$\redalloctuple$$

The rule introduces the new definition $\hdef{v}{\alttypename}{\storetuple{\cvec{h\subi}{\polybase}\varbound}}$.

\item
For $e_r = \letalloc{v}{\allocunion{t}{\tyvar}{\cvec{h\subi}{\polybase}\varbound}{j}}{e}$, the operational rule is
\refmath{Red-Alloc-Union}.

$$\redallocunion$$

The rule introduces the new definition $\hdef{v}{t}{\storeunion{\tyvar}{j}{\cvec{h\subi}{\polybase}\varbound}}$.

\item
For $e_r = \letalloc{v}{\allocarray{t}{i_\ms{size}}{h_\ms{init}}}{e}$, the operational rule is
\refmath{Red-Alloc-Array}.

$$\redallocarray$$

The rule introduces the new definition
$\hdef{v}{t}{\storetuple{h_\ms{init}^1,\ldots,h_\ms{init}^{i_\ms{size}}}}$.

\item 

For $e_r = \letalloc{v}{\allocmalloc{t}{i_\ms{size}}}{e}$, the operational rule is \refmath{Red-Alloc-Malloc}.

$$\redallocmalloc$$

The rule introduces the new definition $\hdef{v}{t}{\storedata{\xdata}}$.

\item

For $e_r = \letalloc{v}{\allocframe{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{e}$, the operation rule is
\refmath{Red-Alloc-Frame}.

$$\redallocframe$$

The rule introduces the new definition $\hdef{v}{\tyframe{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}$.
\end{itemize}
\end{proofcase}

\begin{proofcase}{Subscripting}
There are several subscript cases for the expression $e_r = \letsubscript{v_1}{t_1}{v_2}{i}{e}$.

\begin{itemize}
\item

The operational rule \refmath{Red-LetSub-Tuple} inroduces the new definition $\hdef{v_1}{\alttypename}{h_j}$.

$$\redletsubtuple$$

\item

The operational rule \refmath{Red-LetSub-Array} introduces the new definition
$\hdef{v_1}{\alttypename}{h_j}$.

$$\redletsubarray$$

\item

The operational rule \refmath{Red-LetSub-Union} introduces the new definition $\hdef{v_1}{\alttypename}{h_k}$.

$$\redletsubunion$$

\item

The operational rule \refmath{Red-LetSub-RawData} introduces the new definition
$\hdef{v_1}{\alttypename}{h}$.

$$\redletsubrawdata$$

\item

The operational rule \refmath{Red-LetSub-Frame} introduces the new definition $\hdef{v_1}{\alttypename}{h}$.

$$\redletsubframe$$
\end{itemize}
\end{proofcase}

\begin{proofcase}{Subscript assignment}
There are several subscript assignment cases for the expression $\setsubscript{v_1}{i}{t_2}{h}{e}$.

\begin{itemize}
\item $\refmath{Red-SetSub-Tuple}$   $$\redsetsubtuple$$
\item $\refmath{Red-SetSub-Array}$   $$\redsetsubarray$$
\item $\refmath{Red-SetSub-Union}$   $$\redsetsubunion$$
\item $\refmath{Red-SetSub-RawData}$    $$\redsetsubrawdata$$
\item $\refmath{Red-SetSub-Frame}$   $$\redsetsubframe$$
\end{itemize}

In each of the cases, the definition in the environment is modified, but remains a definition.
\end{proofcase}

\begin{proofcase}{LetGlobal}
Suppose $e_r = \letglobal{v}{\alttypename}{\xlabel}{e}$.  The operational rule is \refmath{Red-LetGlobal}.

$$\redletglobal$$

The rule adds the definition $\hdef{v}{\alttypename}{h}$ to the context.
\end{proofcase}

\begin{proofcase}{SetGlobal}
Suppose $e_r = \setglobal{\xlabel}{\alttypename}{h}{e}$.  The operational rule is \refmath{Red-SetGlobal}.

$$\redsetglobal$$

The definition $\hdef{\xlabel}{\alttypename'}{h'}$ is modified but remains a definition.
\end{proofcase}

This concludes the proof of the \fullydefined.
\end{proof}


% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
