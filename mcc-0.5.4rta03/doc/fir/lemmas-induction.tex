%
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Proof induction lemma
%
\iftechreport
\section{Proof induction}
\label{section:proof-induction}

Before continuing, we first need a technical lemma that we use frequently throughout our reasoning.
We often use induction on the length of a proof $\withgenv{\termname : t}$, where $\termname$ is an
atom $a$, expression $e$, store value $b$, or global label $\xlabel$.  These proofs require that we
enumerate the cases, relying on the proof structure to establish the use of particular rules.  For
example, if we know that there is a proof of $\withgenv{\letatom{v}{\alttypename}{a}{e} :
\typename}$, we would argue that the final rule in the proof \emph{must be} the \refmath{Ty-LetAtom}
rule, because \emph{no other rule} applies.

$$\tyletatom$$

Unfortunately, this is not exactly true.  There are seven other rules that can terminate the proof,
 shown in Figure~\ref{figure:terminating-type-rules}.

\makefigure{figure:terminating-type-rules}{Thinning and subsitution rules}{%
$$
\renewcommand\arraystretch{2.0}
\settypename{t_1}
\setalttypename{t_2}
\setconclname{\termname : \typename}
\begin{array}{cl}
\thintype      & \refmath{Thin-Type}\\
\thinvar       & \refmath{Thin-Var}\\
\thinglobal    & \refmath{Thin-Global}\\
\thintypedef   & \refmath{Thin-Type-Def}\\
\thinvardef    & \refmath{Thin-Var-Def}\\
\thinglobaldef & \refmath{Thin-Global-Def}\\
\tysubst       & \refmath{Ty-Subst}
\end{array}
$$}

In the following lemma, we show that the use of these rules does not affect the proof in any
substantial way.

\begin{lemma}
\thmtitle\proofinduction{lemma:proof-induction}{Proof induction}
\setgenvtypename{\widehat{\genv}}
Assume $\withgenv{\termname : t}$, where $\termname$ is an atom $a$, expression $e$, store value
$b$, or global label $\xlabel$, and let $R$ be the set of rules in
Figure~\ref{figure:terminating-type-rules}.  Then the proof of $\withgenv{\termname : t}$ contains
the application of a rule $r \notin R$ with conclusion $\withgenvtype{\termname : t'}$ for some
context $\genvtype$ where $\withgenvtype{\tyequal{t}{t'}{\Type}}$.  Furthermore, for each premise of
$r$ of the form $\withgenvtypeand{\Delta}{J}$, there is a derivation $\withgenvand{\Delta}{J}$.
\end{lemma}

\begin{proof}
\setconclname{\termname : \typename}

We prove this by induction on the length of the proof of $\withgenv{\termname : t}$.

\begin{proofcase}{Base case}
Suppose the final rule in the proof is the use of a rule $r \notin R$.  The result follows
trivially.
\end{proofcase}

\begin{proofcase}{Declarations}
Suppose the final rule in is the application of one of the rules \refmath{Thin-Type},
\refmath{Thin-Var}, or \refmath{Thin-Global}.  These rules are similar; we illustrate the
argument with the \refmath{Thin-Var} rule.

$$\thinvar$$

We can conclude from the induction hypothesis and the premise $\withgenv{\conclname}$ that the proof
contains the use of a rule $r \notin R$ for the abbreviated context $\widehat{\genv} = \genv$.  For
any derivation $\withenv{\widehat{\genv}, \Delta}{J}$, the context is well-formed
$\withenv{\widehat{\genv}, \Delta}{\diamond}$ by the \wfcontext,
and we can infer that $\withenv{\widehat{\genv}, \Delta}{\alttypename
: \Type}$ from the premise $\withenv{\widehat{\genv}, \alttypename : \Type}$ and application of the thinning rules.
The derivation $\withenv{\genv, \vdef{v}{\alttypename}, \Delta}{J}$ follows by re-applying the
\refmath{Thin-Var} rule.
\end{proofcase}

\begin{proofcase}{Definitions}
Suppose the final rule in the proof is the application of one of the rules \refmath{Thin-Type-Def},
\refmath{Thin-Var-Def}, or \refmath{Thin-Global-Def}.  These rules are similar; we illustrate the
argument with the \refmath{Thin-Var-Def} rule.

$$\thinvardef$$

We can conclude from the induction hypothesis and the premise $\withgenvand{\vdef{v}{t}}{\termname :
t}$ that the proof contains the use of a rule $r \notin R$ for the abbreviated context
$\widehat{\genv} = \genv, \vdef{v}{t}$.  For any derivation $\withenv{\widehat{\genv}, \Delta}{J}$,
the context is well-formed $\withenv{\widehat{\genv}, \Delta}{\diamond}$ by the \wfcontext,
and we can infer that $\withenv{\widehat{\genv}, \Delta}{b : t}$, from
the premise $\withenv{\widehat{\genv}}{b : t}$ and repeated application of the thinning rules.
Thus, for any derivation $\withenv{\widehat{\genv}, \Delta}{J}$, we can re-apply the
\refmath{Thin-Var-Def} rule to conclude that $\withgenvand{\hdef{v}{t}{b}}{J}$.
\end{proofcase}

\begin{proofcase}{Substitution}
Suppose the final rule in the proof is an application of \refmath{Ty-Subst}.

$$\tysubst$$

Since the types $\typename$ and $\alttypename$ are equal,
we can conclude that the derivation of $\withgenv{\termname : \alttypename}$ contains the
use of a rule $r \notin R$, and the context $\genv$ is unchanged.
\end{proofcase}

This concludes the \proofinduction.
\end{proof}
\fi

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
