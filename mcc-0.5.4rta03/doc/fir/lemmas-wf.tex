%
% Lemmas
%
\iftechreport
\chapter{Basic \fir{} properties}
\else
\appendix
\fi
\label{section:lemmas}

In this
\iftechreport
    chapter,
\else
    appendix,
\fi
we develop a few basic lemmas that are needed to prove the preservation and
progress (type-safety) theorems.  The brief outline is as follows.  To prove preservation, we need
to show that each rule in the operational semantics does not change the type of the program, and for
this we develop a substitution lemma (Lemma~\ref{lemma:exp-subst}) to show that atom evaluation
does not change the type of the program.  Another basic property is that each well-typed expression
has exactly one type (see
\iftechreport
   Section~\ref{section:type-uniqueness}).
\else
   Appendix~\ref{section:type-uniqueness}).
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% WELL-FORMEDNESS
%
\section{Well-formedness properties}
\label{section:lemmas-wf}

Initially, we establish that well-formed environments $\genv$ contain only well-typed definitions.
We develop this in four parts.
%
\iftechreport
\begin{enumerate}
\item The {\sc Well-formed sub-contexts} Lemma~\ref{lemma:wf-sub-context} shows that if
  a context $\genv$ is well-formed, so are its parts.
\item The {\sc Well-formed context types} Lemma~\ref{lemma:wf-context-types} shows that
  the types in the variable and global definitions in any well-formed context are well-formed types.
\item The {\sc Well-formed contexts} Lemma~\ref{lemma:wf-context} shows that the
  well-formedness judgment $\withgenv{\diamond}$ follows from any judgment $\withgenv{J}$.
\item The {\sc Well-formed context value} Lemma~\ref{lemma:wf-context-values} shows
  that the values in each definition in a well-formed context are also well-formed.
\end{enumerate}
\else
First, we show that if a context $\genv$ is well-formed, so are all of its parts.
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Context thinning
%
\begin{lemma}
\thmtitle\wfsubcontext{lemma:wf-sub-context}{Well-formed sub-contexts}
If $\withgenvand{d}{\diamond}$ for some definition or
declaration $d$, and $\dom{d} \cap \freevars{\genv} = \emptyset{}$, then $\withgenv{\diamond}$.
\end{lemma}

\iftechreport
\begin{proof}
\renewcommand\arraystretch{2.0}
\setconclname{\diamond}

We prove this by induction on the length of the proof of $\withgenvand{d}{\diamond}$.  The proof
must end with one of the thinning rules listed in Section~\ref{section:context-wf}
(not including \refmath{WF-Empty-Context}).

\begin{proofcase}{Declarations}
Suppose $d$ is a declaration $\vdef{\tyvar}{k}$, $\vdef{v}{t}$, or $\vdef{l}{t}$ and the
well-formedness proof ends with one of the following rules.

$$
\begin{array}{cl}
\thintype   & \refmath{Thin-Type}\\
\thinvar    & \refmath{Thin-Var}\\
\thinglobal & \refmath{Thin-Global}
\end{array}
$$

In each of these cases, $\withgenv{\diamond}$ follows directly from the premises of the rule.
\end{proofcase}

\begin{proofcase}{Definitions}

Suppose $d$ is a definition $\hdef{\tyvar}{k}{\tydef}$, $\hdef{v}{t}{b}$, or $\hdef{l}{t}{h}$, and the
well-formedness proof ends with one of the following rules.

$$
\begin{array}{cl}
\thintypedef   & \refmath{Thin-Type-Def}\\
\thinvardef    & \refmath{Thin-Var-Def}\\
\thinglobaldef & \refmath{Thin-Global-Def}
\end{array}
$$

The judgment $\withgenv{\diamond}$ follows by induction and the premises
$\withgenvand{\vdef{\tyvar}{k}}{\diamond}$, $\withgenvand{\vdef{v}{t}}{\diamond}$, and
$\withgenvand{\vdef{l}{t}}{\diamond}$.
\end{proofcase}

Otherwise, it must be the case that the well-formedness proof was applied to some other declaration or
definition $d' \in \genv$.  Let $\genv'$ be the environment $\genv$ without the definition $d'$.
That is, $\genv', d' = \genv$.

\begin{proofcase}{Declarations}

If $d'$ is a declaration $\vdef{\tyvar}{k}$, $\vdef{v}{t}$, or $\vdef{l}{t}$, then the
well-formedness proof ends with one of the following rules.

\begin{block}
\setgenvname{\genvorig', d}
$$
\begin{array}{cl}
\thintype   & \refmath{Thin-Type}\\
\thinvar    & \refmath{Thin-Var}\\
\thinglobal & \refmath{Thin-Global}
\end{array}
$$
\end{block}

The judgment $\withenv{\genv'}{\diamond}$ follows from the induction hypothesis, and the premises
$\withenv{\genv', d}{\diamond}$, and we can infer that $\withenv{\genv, d'}{\diamond}$ by reapplying
the well-formedness rule.
\end{proofcase}

\begin{proofcase}{Definitions}

If $d'$ is a definition $\hdef{\tyvar}{k}{t}$, $\hdef{v}{t}{b}$, or $\hdef{l}{t}{h}$, then the
well-formedness proof ends with one of the following rules.

\begin{block}
\setgenvname{\genvorig', d}
$$
\begin{array}{cl}
\thintypedef   & \refmath{Thin-Type-Def}\\
\thinvardef    & \refmath{Thin-Var-Def}\\
\thinglobaldef & \refmath{Thin-Global-Def}
\end{array}
$$
\end{block}

Let $d''$ be the declaration $\vdef{\tyvar}{k}$, $\vdef{v}{t}$, or $\vdef{l}{t}$ that corresponds to
the definition $d'$.  We can infer that $\withenv{\genv', d''}{\diamond}$ by induction and the
second premise of each rule.  In each case, the definition's value is well-typed by the first premise,
and we can reapply the rule to conclude that $\withenv{\genv', d}{\diamond}$.
\end{proofcase}

This concludes the proof of the \wfsubcontext.
\end{proof}
\else

The proof is by induction on the length of the proof of $\withgenvand{d}{\diamond}$.
For the next lemma, we show that for any well-formed context $\genv$, all of the types $t$ in
$\genv$ are also well-formed.
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Context thinning
%
\begin{lemma}
\thmtitle\wfcontexttypes{lemma:wf-context-types}{Well-formed context types}
If $\withgenv{\diamond}$ then for each declaration $\vdef{v}{t}$, $\vdef{l}{t}$, or
each definition $\hdef{v}{t}{b}$, $\hdef{l}{t}{b}$ in $\genv$, the term $t$ is a well-formed type.
\end{lemma}

\iftechreport
\begin{proof}
\renewcommand\arraystretch{2.0}
\setconclname{\diamond}
We prove this by induction on the length of the proof of $\withgenv{\diamond}$.

\begin{proofcase}{Empty context}
If $\genv$ is empty, it contains no declarations or definitions, and the result follows trivially.
\end{proofcase}

\begin{proofcase}{Type definitions}
Suppose the proof ends with a type definition rule.

$$
\begin{array}{cl}
\thintype    & \refmath{Thin-Type}\\
\thintypedef & \refmath{Thin-Type-Def}
\end{array}
$$

By induction, we can infer that each variable and global definition in $\genv$ is defined over a
well-formed type.  Since the declaration $\vdef{\tyvar}{k}$ and definition $\hdef{\tyvar}{k}{t}$ is
not a variable and global definition, the result follows directly.
\end{proofcase}

\begin{proofcase}{Variable and global declarations}
Suppose the proof ends with one of the following rules.

$$
\begin{array}{cl}
\thinvar    & \refmath{Thin-Var}\\
\thinglobal & \refmath{Thin-Global}
\end{array}
$$

Using the induction hypothesis on the premise $\withgenv{\diamond}$ we can conclude that each
variable and global definition in $\genv$ is defined over a well-formed type.  Since the second premise
requires that the term $\alttypename$ be a well-formed type, the result follows directly.
\end{proofcase}

\begin{proofcase}{Variable and global definitions}
Suppose the proof ends with one of the following rules.

$$
\begin{array}{cl}
\thinvardef    & \refmath{Thin-Var-Def}\\
\thinglobaldef & \refmath{Thin-Global-Def}
\end{array}
$$

In both of these cases, we can use the induction hypothesis on the second
premise to conclude that the type $\alttypename$ is well-formed.
\end{proofcase}

This concludes the proof of well-formed context types.
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Well-formed contexts
%
% Emre: Beware the ongoing \iftechreport ...

For the next lemma, we show that for any judgment $\withgenv{J}$, the context $\genv$ must be
well-formed.

\else

The proof is by induction on the length of the proof of $\withgenv{\diamond}$.
The next lemma shows
that for any judgment $\withgenv{J}$, the context $\genv$ must be well-formed.

\fi

\begin{lemma}
\thmtitle\wfcontext{lemma:wf-context}{Well-formed contexts}
A proof of the judgment $\withgenv{\diamond}$ follows from any judgment $\withgenv{J}$.
\end{lemma}

\iftechreport
\begin{proof}
We prove this by the length of
the proof $\withgenv{J}$.  We consider the proofs of $\withgenv{J}$ where $J \neq \diamond$,
and the proof of $\withgenv{J}$ does not end in the use of a rule where one of the premises has the form
$\withgenv{J'}$ for some $J'$ (otherwise the result follows trivially).  The rules that fit this
description are listed below.
%
\begin{block}
\renewcommand\arraystretch{2.0}
$$
\begin{array}{cr}
\thintypedef    & \refmath{Thin-Type-Def}\\
\thinvardef     & \refmath{Thin-Var-Def}\\
\thinglobaldef  & \refmath{Thin-Global-Def}\\
\wftydef        & \refmath{WF-Tydef}\\
\tystorefunmono & \refmath{Ty-StoreFun-Mono}\\
\tystorefunpoly & \refmath{Ty-StoreFun-Poly}\\
\wfsttyexists   & \refmath{WF-ST-TyExists}\\
\wfsttyfunpoly  & \refmath{WF-ST-TyFun-Poly}\\
\tymatchunion   & \refmath{Ty-Match-Union}
\end{array}
$$
\end{block}

\begin{proofcase}{Subcontexts}
If the proof ends with the rule \refmath{WF-Tydef}, \refmath{Ty-StoreFun-Poly},
\refmath{WF-ST-TyExists}, or \refmath{WF-ST-TyFun-Poly}, we can conclude by induction that
$\withgenvand{\vvecdef{\alpha\subi}{\Smalltype}{\polybase}{\polybound}}{\diamond}$.  

Similarly, if the proof ends with the rule \refmath{Ty-StoreFun-Mono}, we can conclude by induction
that $\withgenvand{\vvecdef{v\subi}{t\subi}{\polybase}{\polybound}}{\diamond}$.

The result $\withgenv{\diamond}$ follows
directly by application of the \wfsubcontext.
\end{proofcase}

\begin{proofcase}{Definitions}
From the three cases \refmath{Thin-Type-Def}, \refmath{Thin-Var-Def}, and \refmath{Thin-Global-Def}, let
the definitions and declarations be defined as in the following table.

$$
\begin{array}{ccl}
\ms{decl} & \ms{def} & \hbox{Rule}\\
\hline
\vdef{\tyvar}{k} & \hdef{\tyvar}{k}{t} & \refmath{Thin-Type-Def}\\
\vdef{v}{t}      & \hdef{v}{t}{b}      & \refmath{Thin-Var-Def}\\
\vdef{l}{t}      & \hdef{l}{t}{h}      & \refmath{Thin-Global-Def}
\end{array}
$$

We can conclude $\withgenvand{\ms{decl}}{\diamond}$ by induction, using the second premise of each
rule.  Since the first premise requires well-formedness of the definition's value, we can reapply
the rule to conclude that $\withgenvand{\ms{def}}{\diamond}$.
\end{proofcase}

\begin{proofcase}{Unions}
For \refmath{Ty-Match-Union}, we can conclude from the premise
$\lvec{\withgenvtypeand{\vdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s\subi}}}{e\subi
: t}}{\polybase}{\varbound}$, that the context $\genv,
\vdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_i}}$ is well-formed for all of the
match cases $\lvec{\matchcase{s\subi}{e\subi}}{\polybase}{\varbound}$.  The side condition requires
that there be at least one match case, and we can use the induction hypothesis on the first case to
conclude that
$\withgenvtypeand{\vdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_1}}}{\diamond}$.

From the \wfcontexttypes, the type
$\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_1}$ must be well-formed.  That is, there
is a proof
$\withgenvtype{\tymember{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_1}}{\skind}}$.
The only rule that can be used directly to prove well-formedness is the \refmath{WF-ST-TyUnion}
rule.

$$\wfsttyunion$$

The only condition on $\intset$ is $\intset \subseteq \mkset{0\ldots n - 1}$, and we can infer 
the type $\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_\ms{union}}$ is also well-formed.  We can infer
from the \wfsubcontext{} that $\withgenv{\diamond}$, and we
can conclude that
$\withgenvtypeand{\vdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_\ms{union}}}}{\diamond}$ from the
\refmath{Thin-Var} rule.

\begin{block}
\setconclname{\diamond}
\settypename{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_\ms{union}}}
$$\thinvar$$
\end{block}
\end{proofcase}

This concludes the proof of the \wfcontext.
\end{proof}

\else

The proof is relatively straightforward, by induction on the length of the proof $\withgenv{J}$.
For example, in the full set of type judgments, the rules that are not trivial are the introduction
rules for types like $\tyall{\cvec{\alpha\subi}{\polybase}{\polybound}}{t}$.  In these cases, the well-formedness proof
follows from the \wfsubcontext.
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Well-formed context values
%
\begin{lemma}
\thmtitle\wfcontextvalueslemma{lemma:wf-context-values}{Well-formed context values}
If $\withgenv{\diamond}$, then every value in each definition in $\genv$ is well-typed.  That is,
all of the following hold.

\begin{itemize}
\item For each type definition $\hdef{\tyvar}{k}{t}$ in $\genv$, 
  $\withgenv{\tymember{t}{k}}$.
\item For each variable definition $\hdef{v}{t}{b}$ in $\genv$, $\withgenv{b : t}$.
\item For each global definition $\hdef{l}{t}{h}$ in $\genv$, $\withgenv{h : t}$.
\end{itemize}
\end{lemma}

\iftechreport
\begin{proof}
\setconclname{\diamond}
We prove this by induction on the length of the proof of $\withgenv{\diamond}$.

\begin{proofcase}{Empty context}
The empty context contains no definitions, so the result follows trivially.
\end{proofcase}

\begin{proofcase}{Declarations}
Suppose the well-formedness proof ends with one of the declaration forms.

$$
\begin{array}{cl}
\rulebox{\thintype}   & \refmath{Thin-Type}\\
\rulebox{\thinvar}    & \refmath{Thin-Var}\\
\rulebox{\thinglobal} & \refmath{Thin-Global}
\end{array}
$$

We can conclude that each definition in $\genv$ is well-formed from the premise
$\withgenv{\diamond}$.
\end{proofcase}

\begin{proofcase}{Definitions}
Suppose the well-formedness proof ends with one of the definition forms.

$$
\begin{array}{cl}
\rulebox{\thintypedef}   & \refmath{Thin-Type-Def}\\
\rulebox{\thinvardef}    & \refmath{Thin-Var-Def}\\
\rulebox{\thinglobaldef} & \refmath{Thin-Global-Def}
\end{array}
$$

In each case, the first premise of the rule requires that the definition's value be well-formed.
\end{proofcase}

This concludes the proof of well-formed context values.
\end{proof}

\else

This is the final well-formedness proof.  As usual, the proof is by induction on the length of the
proof of $\withgenv{\diamond}$.

\fi

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
