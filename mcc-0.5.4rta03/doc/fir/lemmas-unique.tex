%
% Lemmas
%
\section{Type uniqueness}
\label{section:type-uniqueness}

In this section, we show that each value and each expression in a
program has a unique type.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Atom type uniqueness
%
\begin{lemma}
\thmtitle\uniqueatomtypes{lemma:unique-atom-types}{Uniqueness of atom types}
If $\withgenv{a : t_1}$ and $\withgenv{a : t_2}$ then $t_1 = t_2$.
\end{lemma}

\iftechreport

\begin{proof}
To prove this lemma, we construct a deterministic function $\typeofatom{\genv}{a}$ that produces the type of
the atom $a$, given the context $\genv$ and the atom.

$$
\begin{array}{lll}
a & \typeofatom{\genv}{a} & \hbox{Rule}\\
\hline

\atomenum{i}{j} & \tyenum{i} & \refmath{Ty-AtomEnum}\\
\atomint{i} & \tyint & \refmath{Ty-AtomInt}\\
\atomrawint{\rawint}{\pre}{\sign} & \tyrawint{\pre}{\sign} & \refmath{Ty-AtomRawInt}\\
\atomfloat{x}{\fpre} & \tyfloat{\fpre} & \refmath{Ty-AtomFloat}\\

\atomlabel{\tyvar}{\framelabel{f}}{\framelabel{sf}}{\rawint_{\ms{off}}} & \tyrawint{32}{\signed} & \refmath{Ty-AtomLabel}\\
\atomsizeof{\tyvar_1, \ldots, \tyvar_n}{\rawint_\ms{size}} & \tyrawint{32}{\signed} & \refmath{Ty-AtomSizeof}\\

\atomvar{v} & \genv(v) & \refmath{Ty-AtomVar}\\

\atomconst{t}{\tyvar}{i} & t & \refmath{Ty-AtomConst}\\

\atomtyapply{t}{a}{t_1,\ldots,t_n}
   & t
   & \refmath{Ty-AtomTyApply}\\
\atomtypack{t}{v}{t_1,\ldots,t_n}
   & t
   & \refmath{Ty-AtomTyPack}\\
\atomtyunpack{v}
   & (\genv(v))[ \lvec{\typroject{v}{i}/\alpha\subi}{\polybasezero}{\polyboundzero} ]
   & \refmath{Ty-AtomTyUnpack}\\

\atomunop{\unop}{a}           & \unoptyperes{\unop}   & \refmath{Ty-AtomUnop}\\
\atombinop{\binop}{a_1}{a_2}  & \binoptyperes{\binop} & \refmath{Ty-AtomBinop}
\end{array}
$$

The rules listed are the only typing rules for atoms.  A straightforward inspection of the rules
shows that if $\withgenv a : t$, then $t = \typeofatom{\genv}{a}$.
\end{proof}
\else

To prove this lemma, we construct a deterministic function $\typeofatom{\Gamma}{a}$ that produces the type of
the atom $a$, and show that if $\withgenv{a : t}$, then $t = \typeofatom{\genv}{a}$.

\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Expression uniqueness
%
\begin{lemma}
\thmtitle\uniqueexptypes{lemma:unique-exp-types}{Uniqueness of expression types}
If $\withgenv{e_r : t}$ then $t = \tyvoid$.
\end{lemma}

\iftechreport
\begin{proof}

% Emre: So, some of the sequents below just didn't come out
%       properly, so I hardcoded some \genv's with hats.
%       Beware if you change the next line.
\setgenvtypehat

We prove this by induction on the length of the proof of $\withgenv{e_r : t}$.  We use the
\proofinduction{} throughout.

%
% Type uniqueness for LetAtom
%
\begin{proofcase}{Let forms}
  Suppose $e_r$ is any of the \emph{let} forms, including \refmath{Ty-LetAtom}, \refmath{Ty-LetExt},
  \refmath{Ty-LetSub-Tuple}, \refmath{Ty-LetSub-Array}, \refmath{Ty-LetSub-RawData},
  \refmath{Ty-LetSub-Union}, \refmath{Ty-LetSub-Frame},
  \refmath{Ty-Alloc}, and \refmath{Ty-LetGlobal}.  These all have
  similar proofs. We illustrate with the proof for \refmath{Ty-LetAtom}.

  Suppose $e_r = \letatom{v}{\alttypename}{a}{e}$.  The proof of typing for $e_r$ must use the
  rule \refmath{Ty-LetAtom}.

\begin{block}
\settypename{t}
$$\tyletatom$$
\end{block}

From the premise $\withenv{\genvtype, \vdef{v}{\alttypename}}{e : \typename}$,
we can conclude that $t = \tyvoid$ by induction.
\end{proofcase}

%
% Type uniqueness for TailCall.
%
\begin{proofcase}{TailCall}
  Suppose $e_r = \tailcall{\funname}{\cvec{a\subi}{\varbase}{\varbound}}$.  The proof of typing for
  $e_r$ must use the rule \refmath{Ty-TailCall}.

$$\tytailcall$$

The only possible type for a tail-call is $\tyvoid$, so $t = \tyvoid$.
\end{proofcase}

%
% Type uniqueness for special-calls.
%
\begin{proofcase}{Special-calls}
Suppose $e_r = \specialcall{\tailop}$.  The proof of typing for $e_r$ must use the
\refmath{Ty-Special-Call} rule.

$$\tyspecialcall$$

There are four rules that can prove the premise
$\widehat{\genv}\vdash \tailopname : \tyspecial{\typename}$:
\refmath{Ty-SysMigrate}, \refmath{Ty-Atomic}, \refmath{Ty-AtomicCommit}, and \refmath{Ty-AtomicRollback}.
In each of these cases $t = \tyvoid$.
\end{proofcase}

%
% Type uniqueness for match expressions.
%
\begin{proofcase}{Match}
The match rules \refmath{Ty-Match-Int}, \refmath{Ty-Match-Enum}, \refmath{Ty-Match-RawInt},
\refmath{Ty-Match-Union} all have similar forms.  We illustrate the proof for these cases with
\refmath{Ty-Match-Union}.

In this case, the proof of $\withgenv{e_r : t}$ has the following form.

$$\tymatchunion$$

% Emre: The macros didn't seem to get this right...
The side-condition $\emptyset \neq s_\ms{union}$ requires there be at least one premise
$\withenv{\widehat{\genv}, \vdef{v}\tyunion{\tyvar}{t_1,\ldots,t_m}{s_i}} e_i : t$, and we can conclude that
$t = \tyvoid$ by induction.
\end{proofcase}

%
% Type uniqueness for assignments.
%
\begin{proofcase}{Subscript Assignment}
The rules for assignment operations \refmath{Ty-SetSub-Tuple}, \refmath{Ty-SetSub-RawData},
\refmath{Ty-SetSub-Union}, \refmath{Ty-SetSub-Array}, and \refmath{Ty-SetSub-Frame} have similar
forms.  We illustrate with \refmath{Ty-SetSub-Tuple}.

$$\tysetsubtuple$$

% Emre: The macros didn't seem to get this right...
The premise $\widehat{\Gamma}\vdash e : t$ requires that $t = \tyvoid$ by
induction.
\end{proofcase}

%
% Type uniqueness for assignments.
%
\begin{proofcase}{Assignment}
The type judgment for an assignment operation \refmath{Ty-SetGlobal} has the following form.

$$\tysetglobal$$

% Emre: The macros didn't seen to get this right...
The premise $\widehat{\Gamma}\vdash e : t$ requires that $t = \tyvoid$
by induction.
\end{proofcase}

This concludes the proof of type uniqueness for expressions.
\end{proof}

\else

This is proved by induction on the length of the proof of $\withgenv{e_r :
t}$.  The only expressions that do not contain sub-expressions are the
tail-call $\tailcall{\funname}{\cvec{a\subi}{\varbase}{\varbound}}$ and
special-call $\specialcall{\tailop}$ expressions, and both expressions have
type $\tyvoid$.  In all other cases, the type of an expression is the same as
the type of its sub-expression, and the result follows by induction.

\fi

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
