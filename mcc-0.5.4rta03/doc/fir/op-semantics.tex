%
% Operational semantics
%
\sectionA{Operational Semantics}
\label{section:op-semantics}

Evaluation is defined on \emph{programs}, which include three parts: the
current environment $\genv$, a checkpoint environment $\cenv$, which is an
\emph{ordered list} of checkpoints, and an expression $e$ to be evaluated.
Checkpoints are required for
\iftechreport
   transactions, which are discussed in Section~\ref{section:op-special}.
\else
   transactions.
\fi
A checkpoint $\checkpoint{\genv}{f}{\cvec{a\subi}{\polybase}{\varbound}}$
contains a context $\genv$, and a function $\tailcall{f}{\diamond,
\cvec{a\subi}{\polybase}{\varbound}}$, where $\diamond$ is a special
transaction parameter.  The function is called if evaluation is resumed from
the checkpoint.

$$
\begin{array}{rcll}
\checkpointname & ::= & \checkpoint{\genv}{f}{\cvec{a\subi}{\polybase}{\varbound}}
                      & \hbox{Single checkpoint}\\
\cenv           & ::= & \checkpointname_m; \ldots; \checkpointname_1
                      & \hbox{Checkpoint environment}
\end{array}
$$

%
% Basic definitions
%

\begin{definition}
\thmtitle\fullydefinedcontexts{def:fully-defined-contexts}{Fully-defined contexts}

   A context $\genv$ is said to be \emph{fully-defined} if every variable $v$ in
   the context is defined with the form $\hdef{v}{t}{b}$, every type variable
   $\tyvar$ is defined with the form $\hdef{\tyvar}{k}{\tydef}$, and every global
   label $\xlabel$ is defined with the form $\hdef{\xlabel}{t}{h}$.
\end{definition}

\begin{definition}
\thmtitle\program{def:program}{Programs}

   A \emph{program} is either the special term $\gerror$, or it is a triple
   $\gcstate{\genv}{\cenv}{e}$ that satisfies the following conditions.
   \begin{itemize}
      \item $\genv$ is fully-defined and $\withgenv{e : \tyvoid}$,
      \item For each $\checkpoint{\genv'}{f}{\cvec{a\subi}{\varbase}{\varbound}} \in \cenv$,
            the context $\genv'$ is fully-defined, and the judgment
            $\withenv{\genv', \vdef{v_\xconst}{\tyrawint{32}{\signed}}}
                     {\tailcall{f}{v_\xconst, \cvec{a\subi}{\varbase}{\varbound}} : \tyvoid}$
            holds.
   \end{itemize}
\end{definition}

Intuitively, the $\gerror$ term specifies a runtime error during program
evaluation (such as an out-of-bounds array access, or a runtime type error
during a subscripting operation).

%
% Talking about the evaluation relation now...
%

Evaluation is defined as a relation on programs.  The relation
   $$\reducesto{\gcstate{\genv}{\cenv}{e}}{\gcstate{\genv'}{\cenv'}{e'}}$$
specifies that the program $\gcstate{\genv}{\cenv}{e}$ evaluates in
one step to the program
   $\gcstate{\genv'}{\cenv'}{e'}$.
The relation
   $$\reducesto{\gstate{\genv}{e}}{\gerror}$$
specifies that evaluation of the program $\gstate{\genv}{e}$ results in a
runtime error in one step.

%
% Omit everything up to the special-call rules for the POPL paper.
%
\iftechreport

In this section, we specify program evaluation in two parts.  First, we
specify leftmost-innermost evaluation of the atoms in an expression.  Second,
we specify evaluation of expressions in which the atoms are heap values.  Atom
evaluation is defined using expression contexts, discussed in the following
section.

\nocomment{Emre: The following paragraph is my solution to the ``informal
         syntax'' problem that pervades this section.  Like the use
         of $f$ as a variable, $i$ when we really mean $\atomint{i}$\ldots
         
         JDS:  We need this for some of the special calls.}

To ease readability of the rules, we use informal syntax throughout this
section.  We use $f$ and $g$ as variables naming functions.  Usually, $i$ and
$j$ will stand for integer heap values, such as $\atomint{i}$ or
$\atomrawint{r}{\pre}{\sign}$.  In mathematical expressions, $i$ and $j$ are
given their obvious interpretations as integers.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evaluation contexts
%

\sectionB{Evaluation contexts}

We specify the evaluation order for the atoms in an expression using
evaluation contexts.  There are four kinds of contexts,
corresponding to atoms $A[ ]$, special-calls $S[ ]$, allocation operations
$L[ ]$, and expressions $E[ ]$.  Each context is specified as a term with a
single ``hole'' $[ ]$, in which evaluation is to be performed.  In general,
evaluation is leftmost-innermost.  The evaluation contexts are listed
in Figure~\ref{figure:evaluation-contexts}.

\makefigure{figure:evaluation-contexts}{Evaluation contexts}{%
$$
\begin{array}{rcl}
A & ::= & [ ]\\
  &  |  & \mathop{\unop} A\\
  &  |  & A \mathrel{\binop} a\\
  &  |  & h \mathrel{\binop} A\\
\\
S & ::= & \tailsysmigrate{j}{A}{a_o}{a_{\ms{fun}}}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailsysmigrate{j}{h_p}{A}{a_{\ms{fun}}}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailsysmigrate{j}{h_p}{h_o}{A}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailsysmigrate{j}{h_p}{h_o}{h_{\ms{fun}}}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}\\
  &  |  & \tailatomic{A}{a_{\ms{const}}}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailatomic{\atomfun{v}}{A}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailatomic{\atomfun{v}}{h_{\ms{const}}}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}\\
  &  |  & \tailatomicrollback{A}{a_\ms{const}}\\
  &  |  & \tailatomicrollback{h_{\ms{level}}}{A}\\
  &  |  & \tailatomiccommit{A}{a_\ms{fun}}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailatomiccommit{h_{\ms{level}}}{A}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailatomiccommit{h_{\ms{level}}}{\atomfun{v}}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}\\
\\
L & ::= & \alloctuple{\tupleclass}{t}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}\\
  &  |  & \allocunion{t}{\tyvar}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}{i}\\
  &  |  & \allocarray{t}{A}{a_\ms{init}}\\
  &  |  & \allocarray{t}{h_\ms{size}}{A}\\
  &  |  & \allocmalloc{t}{A}\\
\\
E & ::= & \letatom{v}{t}{A}{e}\\
  &  |  & \letext{v}{t}{s}{t_s}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}{e}\\
  &  |  & \tailcall{A}{\cvec{a\subi}{\varbase}{\varbound}}\\
  &  |  & \tailcall{\atomfun{v}}{\cvec{h\subi}{\polybase}{i - 1}, A, \cvec{a\subi}{i + 1}{\varbound}}\\
  &  |  & \specialcall{S}\\
  &  |  & \matchstmt{A}{\lvec{\matchcase{s\subi}{e\subi}}{\varbase}{\varbound}}\\
  &  |  & \letalloc{v}{L}{e}\\
  &  |  & \letsubscript{v}{t}{A}{a_2}{e}\\
  &  |  & \letsubscript{v}{t}{h}{A}{e}\\
  &  |  & \setsubscript{A}{a_2}{t}{a_3}{e}\\
  &  |  & \setsubscript{v}{A}{t}{a_3}{e}\\
  &  |  & \setsubscript{v}{h}{t}{A}{e}\\
  &  |  & \setglobal{\xlabel}{t}{A}{e}
\end{array}
$$
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Atom evaluation
%
\sectionB{Atom evaluation}

The non-value atoms are variables, unary and binary operators, and the
polymorphic type operators.

Unary and binary operators are specified using an interpretation $\semleft
\ms{op} \semright$ that defines an actual function for each of the operators.
We omit the interpretations in this paper, but they are the obvious ones.  For
example, the interpretation $\semleft \plusop{\tyint} \semright$ for the
$\tyint$ addition operator is actual 31 bit, signed addition.  Note that in
\refmath{Red-Atom-Unop}, we use informal syntax; $i$ and $j$ are taken to be atoma
with appropriate values for the operator being evaluated.  Similar informal
syntax is used in \refmath{Red-Atom-Binop}\footnote{It would be
straightforward to list out a reduction rule for each unary and binary
operator.}.

\mreducesrule\redunop{Red-Atom-Unop}
   {\gstate{\genv}{E[\mathop{\unop} i]}}
   {\gstate{\genv}{E[\semleft \unop \semright(i)]}}

\mreducesrule\redbinop{Red-Atom-Binop}
   {\gstate{\genv}{E[i \mathrel{\binop} j]}}
   {\gstate{\genv}{E[\semleft \binop \semright(i, j)]}}

Variables that are defined as heap values $h$ can be evaluated.  Note that the
heap value $h$ may be another variable.

\mreducesrule\redvar{Red-Atom-Var}
   {\gstate{\genv, \hdef{v}{\typename}{\heapname}}{E[v]}}
   {\gstate{\genv, \hdef{v}{\typename}{\heapname}}{E[h]}}

Type application is used to instantiate polymorphic functions.  The type
parameters of the function $f =
\tydeflambda{\cvec{\alpha\subi}{\polybase}{\polybound}}{b}$ are defined in a
new function $g$ as the actual type arguments
$\cvec{u\subi}{\polybase}{\polybound}$.  Note we are assuming the alpha
renaming convention to rename type variables as necessary.

\mreducestwolinerule\redatomapply{Red-Atom-Apply}
   {\gstate{(\genv', \hdef{f}
                          {\alttypename'}
                          {\tydeflambda{\cvec{\alpha\subi}{\polybase}{\polybound}}{b}}) \xas \genv}
           {E[\atomtyapply{\alttypename}{f}{\cvec{u\subi}{\polybase}{\polybound}}]}}
   {\gstate{\genv,
            \hvecdef{\alpha\subi}{\Smalltype}{u\subi}{\polybase}{\polybound},
            \hdef{g}{\alttypename}{b}}
           {E[g]}}

\nocomment{Emre: I also made some changes to the next rule that
         need to be checked.

         JYH: modified this some more.}

The pack operation pairs a value with type arguments to form a value in an
existential type $u = \tyexists{\cvec{\alpha\subi}{1}{m}}{t}$.  The main
effect of the rule is to add the value in the context.

\mreducestwolinerule\redatompack{Red-Atom-Pack}
   {\gstate{(\genv', \hdef{v_1}{\alttypename'}{b}) \xas \genv}
           {E[\atomtypack{u}{v_1}{\cvec{u\subi}{\polybasezero}{\polyboundzero}}]}}
   {\gstate{\genv, \hdef{v_2}{u}{\atomtypack{u}{v_1}{\cvec{u\subi}{\polybasezero}{\polyboundzero}}}}{E[v_2]}}

\nocomment{Emre: I had to hack on the next rule a bit to get rid of a
         overfull hbox.  So the rule looks a lot uglier than it did before,
         but hopefully, it still means the same thing.

	 JYH: modified this some more.
	 
	 JDS: Justin thinks Jason may have missed the point of the changes...
	 
	 JDS: Forced to use lvecs to make this rule fit on the page.}

The unpack operation is the destructor for a packed value.  The new value is
added to the context with the \emph{actual} types
$\cvec{u\subi}{\polybasezero}{\polyboundzero}$ as arguments for each of the
type parameters $\cvec{\alpha\subi}{\polybasezero}{\polyboundzero}$.

\mreducestwolinerule\redatomunpack{Red-Atom-Unpack}
   {\gstate{(\genv', \hdef{v_1}{u}{\atomtypack{\tyexists{\lvec{\alpha\subi}{\polybasezero}{\polyboundzero}}{t}}{v}{\lvec{u\subi}{\polybasezero}{\polyboundzero}}}) \xas \genv}{E[\atomtyunpack{v_1}]}}
   {\gstate{\genv, \hvecdef{\alpha\subi}{\Smalltype}{u\subi}{\polybasezero}{\polyboundzero}, \hdef{v_2}{\typename}{v}}{E[v_2]}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Basic expressions
%
\sectionB{Basic expressions}

Expression evaluation is defined on expressions in which the outermost atoms
are heap values.

Evaluation of $\letatom{v}{\alttypename}{\heapname}{\expname}$ adds the
definition $\hdef{v}{\alttypename}{\heapname}$ to the context, then evaluates the
expression $e$.

\mreducesrule\redletatom{Red-LetAtom}
   {\gstate{\genv}{\letatom{v}{\alttypename}{\heapname}{\expname}}}
   {\gstate{\genv, \hdef{v}{\alttypename}{\heapname}}{\expname}}

For external calls, we defer to a semantic interpretation function $\semleft
\text{``s''} \semright$ that specifies the interpretation of the built-in
function named $\text{``s''}$.

\mreducestwolinerule\redletext{Red-LetExt}
   {\gstate{\genv}{\letext{v}{\typename}{s}{\tyfun{\argvec{u\subi}{\varbase}{\varbound}}{\typename}}{\cvec{h\subi}{\varbase}{\varbound}}{e}}}
   {\gstate{\genv, \hdef{v}{\typename}{\semleft \text{``s''} \semright \argvec{h\subi}{\varbase}{\varbound}}}{e}}

A tail-call to function
$\left( \storefun{\cvec{v\subi}{\varbase}{\varbound}}{e} \right)$ with
arguments $\left( \cvec{h\subi}{\varbase}{\varbound} \right)$ adds the
definitions $\hvecdef{v\subi}{t\subi}{h\subi}{\varbase}{\varbound}$ to the
evaluation context, and evaluates the expression $e$.

\mreducestwolinerule\redtailcall{Red-TailCall}
   {\gstate{(\genv', \hdef{v}{\tyfun{\argvec{u\subi}{\varbase}{\varbound}}{t}}{\storefun{\cvec{v\subi}{\varbase}{\varbound}}{e}}) \xas \genv}
           {\tailcall{\atomfun{v}}{\cvec{h\subi}{\varbase}{\varbound}}}}
   {\gstate{\genv, \hvecdef{v\subi}{u\subi}{h\subi}{\varbase}{\varbound}}{e}}

%
% End of tech report mode started a little before the
% evaluation contexts section
%
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Special-calls
%
\sectionB{Special-calls}
\label{section:op-special}
\label{section:special-op-semantics}

\iftechreport
   The runtime implementation of special-calls is described in
   Sections~\ref{section:migration}--\ref{section:transactions}.
\else
   The operational semantics for special-calls are shown in
   Figure~\ref{figure:special-call-op-semantics}.
\fi
When a process migrates, the entire process (including $\genv$, $\cenv$, and
the continuation function) migrates to a new location, which is just another
runtime environment.  The migration itself is effectively invisible to the
process, although the result of external calls may reflect the new machine
environment.  Operationally, evaluation of the expression
$\tailsysmigrate{j}{\atomname_\ms{ptr}}{\atomname_\ms{off}}{f}{\cvec{a\subi}{\varbase}{\varbound}}$
ignores the location described in the string
$\atomname_\ms{ptr}[\atomname_\ms{off}]$, and acts as a tail-call
$\tailcall{f}{\cvec{a\subi}{\varbase}{\varbound}}$.

\mreducesrule\redsysmigrate{Red-SysMigrate}
   {\gstate{\genv}{\specialcall{\tailsysmigrate{j}{h_\ms{ptr}}{h_\ms{off}}{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}}
   {\gstate{\genv}{\tailcall{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}

\iftechreport
   The runtime implementation of atomic transactions is described in
   Section~\ref{section:transactions}.
\fi
Transactions are entered with the
$\tailatomic{f}{a_\ms{const}}{\cvec{a\subi}{\varbase}{\varbound}}$ special-call. The
runtime adds a process checkpoint to the checkpoint environment $\cenv$ and
evaluation proceeds with a tail-call
$\tailcall{f}{a_\ms{const},\cvec{a\subi}{\varbase}{\varbound}}$.
\iftechreport
   Operationally, we specify a checkpoint as a pair of a program state $\genv$,
   and a function $\tailcall{f}{\diamond, \cvec{a\subi}{\varbase}{\varbound}}$ to
   be called if the transaction is aborted.  The function's first argument,
   written as $\diamond$, is the transaction parameter that may be replaced on
   rollback.  Entry into the transaction adds the process checkpoint to the
   context, then calls $\tailcall{f}{a_\ms{const}, \cvec{a\subi}{\varbase}{\varbound}}$ with
   the transaction parameter $a_\ms{const}$.
\fi

\mreducestwolinerule\redatomic{Red-Atomic}
   {\gcstate{\genv}{\cenv}
      {\specialcall{\tailatomic{\funvarname}{\heapname_\xconst}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}}
   {\gcstate{\genv}{\checkpoint{\genv}{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}; \cenv}
      {\tailcall{\funvarname}{\heapname_\xconst, \cvec{\heapname\subi}{\varbase}{\varbound}}}}

When a transaction with checkpoint
$\checkpoint{\genv}{f}{\cvec{a\subi}{\varbase}{\varbound}}$ is rolled back
with the $\tailatomicrollback{i}{j}$ special-call to level $i$ with
transaction parameter $j$, evaluation proceeds as a tail-call $\tailcall{f}{j,
\cvec{a\subi}{\varbase}{\varbound}}$ using the original process context
$\genv$ and the truncated checkpoint environment $\checkpointname_i ; \ldots ;
\checkpointname_1$.  All checkpoints with level higher than $i$ are
discarded\footnote{The level that was rolled back is re-entered by this
primitive; in effect, the state that is restored is the state captured
immediately after the level was entered.
\iftechreport
   This is described in further detail in section
   \ref{section:transaction-rollback}.
\fi}.

\mreducestwolinewhenrule\redatomicrollbackone{Red-Atomic-Rollback-1}
   {\gcstate{\genv'}{\checkpointname_m; \ldots; \checkpointname_i = \checkpoint{\genv}{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}; \ldots; \checkpointname_1}
      {\specialcall{\tailatomicrollback{i}{j}}}}
   {\gcstate{\genv}{\checkpointname_i; \checkpointname_{i - 1}; \ldots; \checkpointname_1}
      {\tailcall{\funvarname}{j,\cvec{\heapname\subi}{\varbase}{\varbound}}}}
   {i \in \mkset{1\ldots m}}

\iftechreport
In most cases, rollback operates on the most recently entered level.  As a
short-hand, when $i = 0$ the runtime will roll back only the most recent
level.

\mreducestwolinerule\redatomicrollbacktwo{Red-Atomic-Rollback-2}
   {\gcstate{\genv'}{\checkpointname_m = \checkpoint{\genv}{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}; \ldots; \checkpointname_1}
      {\specialcall{\tailatomicrollback{0}{j}}}}
   {\gcstate{\genv}{\checkpointname_m; \ldots; \checkpointname_1}
      {\tailcall{\funvarname}{j,\cvec{\heapname\subi}{\varbase}{\varbound}}}}

It is an error to specify a checkpoint that does not exist.

\mreduceswhenrule\redatomicrollbackerror{Red-Atomic-Rollback-Error}
   {\gcstate{\genv}{\checkpointname_m; \ldots; \checkpointname_1}
      {\specialcall{\tailatomicrollback{i}{j}}}}
   {\gerror}
   {i \notin \mkset{0\ldots m} \vee m = 0}
\fi

When a transaction is committed with the special-call
$\tailatomiccommit{i}{f}{\cvec{a\subi}{\varbase}{\varbound}}$, the checkpoint
is deleted from the checkpoint context, and evaluation continues with a
tail-call to the function $\tailcall{f}{\cvec{a\subi}{\varbase}{\varbound}}$.

\mreducestwolinewhenrule\redatomiccommitone{Red-Atomic-Commit-1}
   {\gcstate{\genv}{\checkpointname_m; \ldots; \checkpointname_1}
      {\specialcall{\tailatomiccommit{i}{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}}
   {\gcstate{\genv}{\checkpointname_m; \ldots; \checkpointname_{i+1}; \checkpointname_{i-1}; \ldots; \checkpointname_1}
      {\tailcall{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}
   {i \in \mkset{1\ldots m}}

\iftechreport
In most cases, commit operates on the most recently entered level.  As a
short-hand, when $i = 0$ the runtime will commit the most recent level.

\mreducestwolinerule\redatomiccommittwo{Red-Atomic-Commit-2}
   {\gcstate{\genv}{\checkpointname_m; \ldots; \checkpointname_1}
      {\specialcall{\tailatomiccommit{0}{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}}
   {\gcstate{\genv}{\checkpointname_{\polybound-1}; \ldots; \checkpointname_1}
      {\tailcall{\funvarname}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}

It is an error to specify a checkpoint that does not exist.

\mreduceswhenrule\redatomiccommiterror{Red-Atomic-Commit-Error}
   {\gcstate{\genv}{\checkpointname_m; \ldots; \checkpointname_1}
      {\specialcall{\tailatomiccommit{i}{f}{\cvec{\heapname\subi}{\varbase}{\varbound}}}}}
   {\gerror}
   {i \notin \mkset{0\ldots m} \vee m = 0}
\fi

%
% If this is not a tech report, place special calls in a table.
%
\ifpaper
\makefiguretwocolumn{figure:special-call-op-semantics}{Special-call operational semantics}{%
\renewcommand\arraystretch{2}
$$
\begin{array}{c}
\redsysmigrate\\
\redatomic\\
\redatomicrollbackone\\
\redatomiccommitone
\end{array}
$$}
\fi

%
% Omit rules up to the subscripting operators from the POPL paper.
%
\iftechreport

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Allocation expressions
%
\sectionB{Allocation expressions}

There are five allocation rules, corresponding to the five aggregate types:
tuples, arrays, unions, rawdata, and frames.

When a tuple is allocated, the tuple value is added to the environment.

\mreducesrule\redalloctuple{Red-Alloc-Tuple}
   {\gstate{\genv}{\letalloc{v}{\alloctuple{c}{\alttypename}{\cvec{h\subi}{\varbase}{\varbound}}}{e}}}
   {\gstate{\genv, \hdef{v}{\alttypename}{\storetuple{\cvec{h\subi}{\varbase}{\varbound}}}}{e}}

Array allocation is like tuple allocation, but all the elements of the array
are initialized with the value $h_\ms{in}$.

\mreduceswhenrule\redallocarray{Red-Alloc-Array}
   {\gstate{\genv}{\letalloc{v}{\allocarray{\typename}{i_\ms{size}}{h_\ms{in}}}{e}}}
   {\gstate{\genv, \hdef{v}{\typename}{\storetuple{\cvec{h_\ms{in}^\nosubi}{\polybase}{i_\ms{size}}}}}{e}}
   {i_\ms{size} \ge 0}

It is an error to allocate an array of negative dimension.

\mreduceswhenrule\redallocarrayerror{Red-Alloc-Array-Error}
   {\gstate{\genv}{\letalloc{v}{\allocarray{\typename}{i_\ms{size}}{h_\ms{in}}}{e}}}
   {\gerror}
   {i_\ms{size} < 0}

Union constructor allocation adds the value to the environment.

\mreducesrule\redallocunion{Red-Alloc-Union}
   {\gstate{\genv}{\letalloc{v}{\allocunion{t}{\tyvar}{\cvec{h\subi}{\varbase}{\varbound}}{j}}{e}}}
   {\gstate{\genv, \hdef{v}{t}{\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbase}{\varbound}}}}{e}}

We do not give precise semantics of rawdata aggregates.  The heap value is
represented with the term $\storedata{\xdata}$, which represents some possible
value for the aggregate.  The runtime provides a specific representation,
which we do not state explicitly in the rules.  Note that rawdata is not
initialized explicitly at the time of allocation.

\mreduceswhenrule\redallocmalloc{Red-Alloc-Malloc}
   {\gstate{\genv}{\letalloc{v}{\allocmalloc{t}{i_\ms{size}}}{e}}}
   {\gstate{\genv, \hdef{v}{t}{\storedata{\xdata}}}{e}}
   {i_\ms{size} \ge 0}

It is an error to allocate a rawdata aggregate of negative size.

\mreduceswhenrule\redallocmallocerror{Red-Alloc-Malloc-Error}
   {\gstate{\genv}{\letalloc{v}{\allocmalloc{t}{i_\ms{size}}}{e}}}
   {\gerror}
   {i_\ms{size} < 0}
   
\nocomment{JDS: These allocation errors do not occur in the real compiler;
         the size is always treated as unsigned.  Now granted, in practice
         allocation is nearly guaranteed to fail, but that's a different
         matter...
         
         JDS: Actually, we have no safeguards in place for when an allocation
         request is simply too large (e.g. overflows the size field in the
         block header).  I'm nervous about these error conditions...  Even a
         tuple allocation can (theoretically) overflow the size header...
         (the program to do it would be massive however)}

Frame allocation is similar to rawdata allocation.  The data area is
unsafe and uninitialized.

\mreducestwolinerule\redallocframe{Red-Alloc-Frame}
   {\gstate{\genv}{\letalloc{v}{\allocframe{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{e}}}
   {\gstate{\genv, \hdef{v}{\tyframe{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}}{e}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Match statements
%
\sectionB{Match statements}

Match statements evaluate to the \emph{first} case where the match succeeds.
Operationally, there are two cases: the value to be matched is an integer, or
the value is of union type.

The integer cases are represented with a single rule.  In this case, the side condition
$$(i \in s_k) \wedge \forall j \in \{ 1, \ldots, k - 1 \}. (i \notin s_j)$$
forces the choice of the first possible match.

\mreduceswhenrule\redmatchint{Red-Match-Int}
   {\gstate{\genv}{\matchstmt{i}{\setindex{j} \lvec{\matchcase{s\subi}{e\subi}}{\varbase}{\varbound}}}}
   {\gstate{\genv}{e_k}}
   {\ (i \in s_k) \wedge \forall j \in \{ 1, \ldots, k - 1 \}. (i \notin s_j)}

Matching on values of union type is similar.  The actual value has a specific
tag $j$, and the first match case to match $j$ is chosen.  In keeping with the
type rule \refmath{Ty-Match-Union}, the type of the value being matched is
adjusted to reflect the more specific tag.

\nocomment{Emre: Much hacking on the rule to get rid of an overfull hbox.
         Hopefully, it is still okay?

         JDS:  This rule is incorrect as stated: overloaded $n$.
         RULE IS INCORRECT WARNING MARKER 2.

         Emre: Changed one $n$ to a $l$.  That should fix it.

         JDS:  Nope, there's still two distinct $n$'s running around
         in this damn thing (see the when clause).  Have we used $p$
         for anything yet?

         Emre: *sigh* I don't think the $n$ in the when clause counts
         as a collision, but how's this?}

\mreducestwolinewhenrule\redmatchunion{Red-Match-Union}
   {\gstate{\genv, \hdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s}}{\storeunion{\tyvar}{j}{\cvec{a\subi}{\varbase}{\varbound}}}}
           {\matchstmt{v}{\lvec{\matchcase{s\subi}{e\subi}}{1}{l}}}}
   {\gstate{\genv, \hdef{v}{\tyunion{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}{s_k}}{\storeunion{\tyvar}{j}{\cvec{a\subi}{\varbase}{\varbound}}}}{e_k}}
   {\ (j \in s_k) \wedge \forall p \in \{ 1, \ldots, k - 1 \}. (i \notin s_p)}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Subscripting operations
%

%
% End of tech report mode
%
\fi

\sectionB{Subscripting operations}
\label{section:subscript-operations}

The subscripting operations correspond to the aggregate values:
\iftechreport
   tuples, arrays, unions, rawdata, and frames.
   \nocomment{Emre: We should say something about $i$ and $j$
            being ``logical offsets'' or whatever\ldots
            We used the offset macro back in the type-rules.
            \ldots Oh yeah, I really don't feel like checking
            these rules\ldots
            
            JDS:  Eh, maybe.  I don't want to write that text
            though.  It's probably fine as written; the rules
            make it clear that we use logical offseting here.}
\else
   tuples, arrays, unions, and rawdata.  The operational semantics for
   tuples and rawdata are shown in Figure~\ref{figure:subscript-op-semantics}.
\fi

\iftechreport
   The projection for a tuple $\storetuple{\cvec{h\subi}{0}{n-1}}$
   and index $j$ is the element $h_j$.  There are no error cases for tuples\footnote{The
   typing rules for tuple and union subscripting ensure that the index is always within bounds.}.
\else
   % Assume the above is ``obvious'' in the POPL paper.
\fi

\mreducestwolinerule\redletsubtuple{Red-LetSub-Tuple}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storetuple{\cvec{h\subi}{\varbasezero}{\varboundzero}}}) \xas \genv}
      {\letsubscript{v_1}{\alttypename}{v_2}{j}{e}}}
   {\gstate{\genv, \hdef{v_1}{\alttypename}{h_j}}{e}}

\mreducestwolinerule\redsetsubtuple{Red-SetSub-Tuple}
   {\gstate{(\genv', \hdef{v}{\alttypename'}{\storetuple{\cvec{h\subi}{\varbasezero}{\varboundzero}}}) \xas \genv}
      {\setsubscript{v}{j}{\alttypename}{h}{e}}}
   {\gstate{\genv', \hdef{v}{\alttypename'}{\storetuple{\cvec{h\subi}{\varbasezero}{j-1},h,\cvec{h\subi}{j+1}{\varboundzero}}}}{e}}

%
% Begin tech report mode
%
\iftechreport
Arrays are similar to tuples when the array index is in bounds.

\mreducestwolinewhenrule\redletsubarray{Red-LetSub-Array}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storetuple{\cvec{h\subi}{\varbasezero}{\varboundzero}}}) \xas \genv}
      {\letsubscript{v_1}{\alttypename}{v_2}{j}{e}}}
   {\gstate{\genv, \hdef{v_1}{\alttypename}{h_j}}{e}}
   {j \in \mkset{\varbasezero\ldots\varboundzero}}

\mreducestwolinewhenrule\redsetsubarray{Red-SetSub-Array}
   {\gstate{(\genv', \hdef{v}{\alttypename'}\storetuple{\cvec{h\subi}{\varbasezero}{\varboundzero}}) \xas \genv}
      {\setsubscript{v}{j}{\alttypename}{h}{e}}}
   {\gstate{\genv', \hdef{v}{\alttypename'}\storetuple{\cvec{h\subi}{\varbasezero}{j-1},h,\cvec{h\subi}{j+1}{\varboundzero}}}{e}}
   {j \in \mkset{\varbasezero\ldots\varboundzero}}

It is an error for an array subscript to be out-of-bounds.

\nocomment{JDS: Type names are inconsistent here; is there a reason why?  (use of $t_1, t_2$ instead of $u, u'$)\\
           Emre: *shrug* Not worth changing to rule and then going though the
            entire report to make sure text stayed in sync\ldots.}

\mreducestwolinewhenrule\redletsubarrayerror{Red-LetSub-Array-Error}
   {\gstate{(\genv', \hdef{v_2}{t_2}{\storetuple{\cvec{h\subi}{\varbasezero}{\varboundzero}}}) \xas \genv}
      {\letsubscript{v_1}{t_1}{v_2}{j}{e}}}
   {\gerror}
   {j \notin \mkset{\varbasezero\ldots\varboundzero}}

\mreducestwolinewhenrule\redsetsubarrayerror{Red-SetSub-Array-Error}
   {\gstate{(\genv', \hdef{v}{t_1}\storetuple{\cvec{h\subi}{\varbasezero}{\varboundzero}}) \xas \genv}
      {\setsubscript{v}{j}{t_2}{h}{e}}}
   {\gerror}
   {j \notin \mkset{\varbasezero\ldots\varboundzero}}

Union values are like tagged tuples.  For a value
$\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbasezero}{\varboundzero}}$ and index
$k$, the element being accessed is $h_k$. There are no error cases
for unions.

\mreducestwolinerule\redletsubunion{Red-LetSub-Union}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbasezero}{\varboundzero}}}) \xas \genv}
      {\letsubscript{v_1}{\alttypename}{v_2}{k}{e}}}
   {\gstate{\genv, \hdef{v_1}{\alttypename}{h_k}}{e}}

\mreducestwolinerule\redsetsubunion{Red-SetSub-Union}
   {\gstate{\genv, \hdef{v}{\alttypename'}{\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbasezero}{\varboundzero}}}}
      {\setsubscript{v}{k}{\alttypename}{h}{e}}}
   {\gstate{\genv, \hdef{v}{\alttypename'}\storeunion{\tyvar}{j}{\cvec{h\subi}{\varbasezero}{k-1},h,\cvec{h\subi}{k+1}{\varboundzero}}}{e}}

%
% End of tech-report mode
%
\fi

For the unsafe aggregates, the subscript operations perform a runtime type
check.  Rather than specify the operations here, we rely on an operational
semantics provided by the runtime, described in
\iftechreport
   Chapter~\ref{section:backend}.
\else
   Section~\ref{section:backend}.
\fi
For aggregates of $\tyrawdata$ type, we assume the existence of a runtime
function $\runtimeload{\genv}{\storedata{\xdata}}{j}{t}$ that projects a valid
value $h$ of type $t$ from data area $\storedata{\xdata}$, or results in an
error.  We also assume the existence of a runtime function
$\runtimestore{\genv}{\storedata{\xdata}}{j}{t}{h}$ to store a value in a
rawdata aggregate, returning a new value $\storedata{\xdata'}$ or producing an
error.
%
% Begin yet another tech report mode
%
\iftechreport
$$
\runtimeload{\genv}{\storedata{\xdata}}{j}{t} =
   \begin{cases}
      h & \iftechreport\hbox{the runtime type check $\withgenv{h : t}$ succeeds}
          \else\hbox{if $\withgenv{h : t}$}
          \fi\cr
      \gerror & \hbox{otherwise}
   \end{cases}
$$
$$
\runtimestore{\genv}{\storedata{\xdata}}{j}{t}{h} =
   \begin{cases}
      \storedata{\xdata'} & \hbox{the runtime assignment succeeds}\cr
      \gerror & \hbox{otherwise}
   \end{cases}
$$

Given these runtime functions, a rawdata subscript operation returns the
value given by the runtime, or produces an error.

%
% End of yet another tech report mode
%
\fi

\mreducestwolinewhenrule\redletsubrawdata{Red-LetSub-RawData}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storedata{\xdata}}) \xas \genv}
           {\letsubscript{v_1}{\alttypename}{v_2}{j}{e}}}
   {\gstate{\genv, \hdef{v_1}{\alttypename}{h}}{e}}
   {\runtimeload{\genv}{\storedata{\xdata}}{j}{\alttypename} = h}

\mreducestwolinewhenrule\redletsubrawdataerror{Red-LetSub-RawData-Error}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storedata{\xdata}}) \xas \genv}
           {\letsubscript{v_1}{\alttypename}{v_2}{j}{e}}}
   {\gerror}
   {\runtimeload{\genv}{\storedata{\xdata}}{j}{\alttypename} = \gerror}

\mreducestwolinewhenrule\redsetsubrawdata{Red-SetSub-RawData}
   {\gstate{(\genv', \hdef{v}{\alttypename'}{\storedata{\xdata}}) \xas \genv}
           {\setsubscript{v}{j}{\alttypename}{h}{e}}}
   {\gstate{\genv', \hdef{v}{\alttypename'}{\storedata{\xdata'}}}
           {e}}
   {\runtimestore{\genv}{\storedata{\xdata}}{j}{\alttypename}{h} = \storedata{\xdata'}}

\mreducestwolinewhenrule\redsetsubrawdataerror{Red-SetSub-RawData-Error}
   {\gstate{(\genv', \hdef{v}{\alttypename'}{\storedata{\xdata}}) \xas \genv}
           {\setsubscript{v}{j}{\alttypename}{h}{e}}}
   {\gerror}
   {\runtimestore{\genv}{\storedata{\xdata}}{j}{\alttypename}{h} = \gerror}

%
% Resume rech report mode
%
\iftechreport

Frame operations are similar to rawdata operations.  The frame data is
accessed by the runtime functions
$\runtimeload{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{t}$
and
$\runtimestore{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{h}{t}$.

\mreducestwolinewhenrule\redletsubframe{Red-LetSub-Frame}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}) \xas \genv}
           {\letsubscript{v_1}{\alttypename}{v_2}{j}{e}}}
   {\gstate{\genv, \hdef{v_1}{\alttypename}{h}}{e}}
   {\runtimeload{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{\alttypename} = h}

\mreducestwolinewhenrule\redletsubframeerror{Red-LetSub-Frame-Error}
   {\gstate{(\genv', \hdef{v_2}{\alttypename'}{\storeframe{\xdata}{\tyvar}{\lvec{t\subi}{\polybase}{\polybound}}}) \xas \genv}
           {\letsubscript{v_1}{\alttypename}{v_2}{j}{e}}}
   {\gerror}
   {\runtimeload{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{\alttypename} = \gerror}

The subscript assignment function relies on the
$\runtimestore{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{h}{t}$
function.

\mreducestwolinewhenrule\redsetsubframe{Red-SetSub-Frame}
   {\gstate{(\genv', \hdef{v}{\alttypename'}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}) \xas \genv}
           {\setsubscript{v}{j}{\alttypename}{h}{e}}}
   {\gstate{\genv', \hdef{v}{\alttypename'}{\storeframe{\xdata'}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}}
           {e}}
   {\runtimestore{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{\alttypename}{h}\\
    \qquad \phantom{\mb{when}} = \storeframe{\xdata'}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}

\mreducestwolinewhenrule\redsetsubframeerror{Red-SetSub-Frame-Error}
   {\gstate{(\genv', \hdef{v}{\alttypename'}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}) \xas \genv}
           {\setsubscript{v}{j}{\alttypename}{h}{e}}}
   {\gerror}
   {\runtimestore{\genv}{\storeframe{\xdata}{\tyvar}{\cvec{t\subi}{\polybase}{\polybound}}}{j}{\alttypename}{h}
    = \gerror}

%
% Un-resume tech report mode (and return to the real world).
%
\else
\makefiguretwocolumn{figure:subscript-op-semantics}{Subscript operational semantics}{%
\renewcommand\arraystretch{2}
$$
\begin{array}{c}
\redletsubtuple\\
\redsetsubtuple\\
\redletsubrawdata\\
\redsetsubrawdata
\end{array}
$$}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Global variable expressions
%
% More ``tech report only'' stuff...
%
\iftechreport
\sectionB{Global variable expressions}

Global operations fetch and assign values to global labels.

\mreducesrule\redletglobal{Red-LetGlobal}
   {\gstate{(\genv', \hdef{\xlabel}{\alttypename}{h}) \xas \genv}{\letglobal{v}{\alttypename}{\xlabel}{e}}}
   {\gstate{\genv, \hdef{v}{\alttypename}{h}}{e}}

\mreducesrule\redsetglobal{Red-SetGlobal}
   {\gstate{(\genv', \hdef{\xlabel}{\alttypename}{h'}) \xas \genv}{\setglobal{\xlabel}{\alttypename}{h}{e}}}
   {\gstate{\genv', \hdef{\xlabel}{\alttypename}{h}}{e}}

%
% Whoops, gotta let the world see our stuff now...
%
\fi

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
