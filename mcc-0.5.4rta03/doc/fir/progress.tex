%
% Progress argument.
%
\iftechreport
\sectionB{Progress}
\label{section:progress}
\fi

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROGRESS
%
\begin{theorem}
\thmtitle\progress{theorem:progress}{Progress}
%
If $\gcstate{\genv_r}{\cenv_r}{e_r}$ is a program, and $e_r$ is not a value $h$,
then there is a program $\gcstate{\genv_c}{\cenv_c}{e_c}$ such that
$\gcstate{\genv_r}{\cenv_r}{e_r} \rightarrow \gcstate{\genv_c}{\cenv_c}{e_c}$,
or $\gcstate{\genv_r}{\cenv_r}{e_r} \rightarrow \gerror$.
\end{theorem}

\iftechreport
\begin{proof}
\setgenvtypehat
\setgenvname{\genvorig_r}
\setcenvname{\cenvorig_r}
We prove this by induction on the length of the proof $\withgenv{e_r : t}$.

Type judgments in general require value \emph{declarations} of the form $\vdef{v}{t}$ rather than
value \emph{definitions} of the form $\hdef{v}{t}{b}$.  We use the \proofinduction{} throughout this
proof to infer use of specific type rules in a partially thinned context $\genvtype$ where some
number of thinning rules have been applied.

%
% Variables
%
\begin{proofcase}{Variables}
Suppose $e_r = E[v]$ and $\genv = \left(\genv', \hdef{v}{t'}{h}\right)$.  The rule
  \refmath{Red-Atom-Var} applies.

$$\redvar$$

In the remainder of the proof, we will consider only the cases for $E[v]$ where $\genv \neq
\left(\genv', \hdef{v}{t'}{h}\right)$.
\end{proofcase}

%
% Unary operations
%
\begin{proofcase}{Unary operations}
Suppose $e_r = E[\mathop{\unop} h]$.  By the \exptypinglemma, the atom $\mathop{\unop} h$ must be
well-typed for some type $t_a$.  The only rule that applies is \refmath{Ty-AtomUnop}.

$$\tyatomunop$$

According to the unary operator syntax table in Figure~\ref{figure:fir-unops}, there are two general
cases for $\unoptypearg{\unop}$: it is either a scalar type, or the unary operator is a type coercion.

\begin{itemize}
\item
In the type coercion case, the unary reduction \refmath{Red-Atom-Unop} always applies.

\item
To illustrate the scalar case, suppose $\unoptypearg{\unop} = \tyint$.  From the
\numericvalueslemma, we can assume that the value $h$ is either a constant or a variable.  If it is
a constant, then the reduction \refmath{Red-Atom-Unop} applies.  Otherwise, if $h$ is a variable
$v$, then it must be defined in the context $\genv$ with a definition $\hdef{v}{\tyint}{b}$.  Again,
from the \numericvalueslemma, we can infer that $b$ is a constant or a variable (and so it is a heap
value).  The \refmath{Red-Atom-Var} reduction applies.
\end{itemize}

\end{proofcase}

%
% Binary operations.
%
\begin{proofcase}{Binary operations}
Binary operations are similar to the unary case.  Suppose $e_r = E[h_1 \mathrel{\binop} h_2]$.  By
the \exptypinglemma, the atom $h_1 \mathrel{\binop} h_2$ must be well-typed for some type $t_a$.  The
only rule that applies is \refmath{Ty-AtomBinop}.

$$\tyatombinop$$

According to the binary operator syntax table in Figure~\ref{figure:fir-binops}, there are two
general cases for $\binoptypearg{j}{\binop}$: it is either a scalar type, or the binary operator is
a comparison.  In the comparison case, the binary reduction \refmath{Red-Atom-Binop} always applies.
Otherwise, according to the \numericvalueslemma, the atoms $h_1$ and $h_2$ must be either constants
or variables.  One of the reductions \refmath{Red-Atom-Binop} or \refmath{Red-Atom-Var} applies.
\end{proofcase}

%
% Type application.
%
\begin{proofcase}{Type application}
\setfunname{h_f}
Suppose $e_r = E[\atomtyapply{\alttypename}{\funname}{\cvec{u\subi}{\polybase}{\varbound}}]$.  By
\exptypinglemma, the atom
$\atomtyapply{\alttypename}{\funname}{\cvec{u\subi}{\polybase}{\varbound}}$ must be well-typed for
some type $t_a$.  The only rule that applies is the \refmath{Ty-AtomTyApply} rule.

$$\tyatomtyapply$$

The only rule that can be used to prove the premise $\withgenvtype{\funname :
\tyall{\cvec{\alpha\subi}{\polybase}{\varbound}}{\typename}}$ is the \refmath{Ty-AtomVar} rule.

\begin{block}
\settypename{\tyall{\cvec{\alpha\subi}{\polybase}{\varbound}}{t}}
$$\tyatomvar$$
\end{block}

That is, the value $\funname$ must be a variable $v$, and context $\genv$ must include the
declaration $\vdef{v}{\tyall{\cvec{\alpha\subi}{\polybase}{\varbound}}{t}}$.

Since the context is fully-defined and well-formed, it contains a definition
$\hdef{v}{\tyall{\cvec{\alpha\subi}{\polybase}{\varbound}}{t_a}}{b}$, which is well-formed using the judgment
\refmath{Thin-Var-Def}.

\begin{block}
\setalttypename{\tyall{\cvec{\alpha\subi}{\polybase}{\varbound}}{t}}
\setconclname{\diamond}
$$\thinvardef$$
\end{block}

The only possible value $b$ for $v$ is specified by the \refmath{Ty-StoreFun-Poly} rule.

$$\tystorefunpoly$$

That is, $b$ must be a function
$\tydeflambda{\cvec{\alpha\subi}{\polybase}{\polybound}}{\storefun{\cvec{v\subi}{\varbase}{\varbound}}{e}}$.

This means that the context $\genv$ has the form
$$
\genv = \genv',
  \hdef{f}{\tyall{\cvec{\alpha\subi}{\polybase}{\polybound}}{\tyfun{\argvec{u\subi}{\varbase}{\varbound}}{t}}}
  \tydeflambda{\cvec{\alpha\subi}{\polybase}{\polybound}}{\storefun{\cvec{v\subi}{\varbase}{\varbound}}{e}}.
$$
The operational rule \refmath{Red-Atom-Apply} applies.

$$\redatomapply$$
\end{proofcase}

%
% Type packing
%
\begin{proofcase}{Type abstraction}
\setvarname{v_1}
Suppose $e_r = E[\atomtypack{u}{\varname}{\cvec{u\subi}{\polybase}{n-1}}]$.  By \exptypinglemma, the atom
$\atomtypack{u}{\varname}{\cvec{u\subi}{\polybase}{n-1}}$ must be well-typed for some type $t_a$.  The
only rule that applies is the \refmath{Ty-AtomTyPack} rule.

$$\tyatomtypack$$

The only rule that can be used to prove the premise $\withgenvtype{\varname :
t[\lvec{u\subi/\alpha\subi}{\polybase}{\polybound}]}$ is the \refmath{Ty-AtomVar} rule.

\begin{block}
\settypename{t[\lvec{u\subi/\alpha\subi}{\polybase}{\polybound}]}
$$\tyatomvar$$
\end{block}

That is, atom $a$ must be a variable, and context $\genv$ must include the declaration
$\vdef{v}{t[\lvec{u\subi/\alpha\subi}{\polybase}{\polybound}]}$.
Since the context is fully-defined and well-formed, it contains a definition
$\hdef{v}{t[\lvec{u\subi/\alpha\subi}{\polybase}{\varbound}]}{b}$. 
The operational rule \refmath{Red-Atom-Pack} applies.

$$\redatompack$$
\end{proofcase}

%
% Unpacking
%
\begin{proofcase}{Abstraction unpacking}
%\settypename{t_a}

Suppose $e_r = E[\atomtyunpack{v_1}]$.  By the \exptypinglemma, the atom $\atomtyunpack{v_1}$ must be
well-typed for some type $t_a$.  The only rule that can be applied is \refmath{Ty-AtomTyUnpack}.

$$\tyatomtyunpack$$

The only rule that can be used to prove the premise $\withgenvtype{\varname :
\tyexists{\cvec{\alpha\subi}{\polybase}{\polybound}}{\typename}}$ is the \refmath{Ty-AtomVar} rule.

\begin{block}
\settypename{\tyexists{\cvec{\alpha\subi}{\polybase}{\polybound}}{t}}
$$\tyatomvar$$
\end{block}

That is, the context $\genv$ must include the declaration
$\vdef{v}{\tyexists{\cvec{\alpha\subi}{\polybase}{\varbound}}{t}}$.  Since the context is
fully-defined and well-formed, it contains a definition
$\hdef{v}{\tyexists{\cvec{\alpha\subi}{\polybase}{\varbound}}{t}}{b}$, which is well-formed using
the judgment
\refmath{Thin-Var-Def}.

\begin{block}
\setconclname{\diamond}
\setalttypename{\tyexists{\cvec{\alpha\subi}{\polybase}{\polybound}}{t}}
$$\thinvardef$$
\end{block}

The only possible value $b$ for $v$ is specified by the \refmath{Ty-AtomTyPack} rule.

$$\tyatomtypack$$

That is, $b$ must be a packed expression $\atomtypack{u}{v}{\cvec{u\subi}{\polybase}{\polybound}}$.
This means that the context $\genv$ has the form $\genv',
\hdef{v}{\tyexists{\cvec{\alpha\subi}{\polybase}{\polybound}}{t}}
        {\atomtypack{u}{v}{\cvec{u\subi}{\polybase}{\polybound}}}$.
The operational rule \refmath{Red-Atom-Unpack} applies.

$$\redatomunpack$$
\end{proofcase}

%
% LetAtom.
%
For the remaining cases, we can assume that the expression $e_r$ cannot be expressed as a context
$E[a]$.  That is, the atoms in the expression have been fully evaluated.

\begin{proofcase}{LetAtom}
Suppose $e_r = \letatom{v}{t}{h}{e}$.  The reduction \refmath{Red-LetAtom} applies.

$$\redletatom$$
\end{proofcase}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tailcall
%
\begin{proofcase}{TailCall}
Suppose $e_r = \tailcall{h}{\cvec{h\subi}{\polybase}{\varbound}}$.  The type judgment must use the
\refmath{Ty-TailCall} rule.

\begin{block}
\setfunname{h}
\setatomname{h}
$$\tytailcall$$
\end{block}

By the \functionvalueslemma, the value $h$ must be a variable $v$, and the definition of $v$ must
have the form $\storefun{\setindex{j} \cvec{v\subi}{\polybase}{\varbound}}{e}$.  The
\refmath{Red-TailCall} rule applies.

$$\redtailcall$$
\end{proofcase}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Special-calls
%
\begin{proofcase}{Special Calls}
\setatomname{\heaporig}
Suppose $e_r = \specialcall{S}$ is a special call.  There are several cases.

\begin{itemize}
\item

Suppose $e_r = \tailsysmigrate{j}{h_p}{h_o}{h_{\ms{fun}}}{\cvec{h\subi}{\varbase}{\varbound}}$.
The typing rule is \refmath{Ty-SysMigrate}.

$$\tysysmigrate$$

By the \functionvalueslemma, since $\funname :
\tyfun{\argvec{\typename\subi}{\varbase}{\varbound}}{\tyvoid}$, it must be defined in the context as
a function $\storefun{\setindex{j} \cvec{v\subi}{\varbase}{\varbound}}{e}$.  The
\refmath{Red-SysMigrate} rule applies.

$$\redsysmigrate$$


\item

Suppose $e_r = \tailatomic{\funname}{h_{\ms{const}}}{\cvec{h\subi}{\varbase}{\varbound}}$.  The
corresponding typing rule is \refmath{Ty-Atomic}.

$$\tyatomic$$

By the \functionvalueslemma, since $\funname : \tyfun{(h_\ms{const},
\cvec{h\subi}{\varbase}{\varbound})}{\tyvoid}$, the value $\funname$ must be a variable $\funvarname$.
The \refmath{Red-Atomic} rule applies.

$$\redatomic$$

\item

Suppose $e_r = \tailatomicrollback{h_\ms{level}}{h_\ms{const}}$.  The correspoonding typing rule is
\refmath{Ty-AtomicRollback}.

$$\tyatomicrollback$$

By the \numericvalueslemma, the value $h_\ms{level}$ must be a variable or a constant.  In the
former case, the \refmath{Red-Atom-Var} rule applies.  Otherwise, let $i = h_\ms{level}$, and
suppose the checkpoint context $\cenv$ contains $m$ checkpoints $\cvec{C\subi}{m}{1}$.

If $1 \le i \le m$, then the \refmath{Red-Atomic-Rollback-1} rule applies.

$$\redatomicrollbackone$$

If $i = 0 \wedge m > 0$, then the \refmath{Red-Atomic-Rollback-2} rule applies.

$$\redatomicrollbacktwo$$

If $i < 0 \vee i > m$, then the \refmath{Red-Atomic-Rollback-Error} rule applies.

$$\redatomicrollbackerror$$

\item

Suppose $e_r = \tailatomiccommit{h_{\ms{level}}}{\funname}{\cvec{h\subi}{\varbase}{\varbound}}$.
The typing rule is \refmath{Ty-AtomicCommit}.

$$\tyatomiccommit$$

By the \numericvalueslemma, the value $h_\ms{level}$ must be a variable or a constant.  In the
former case, the \refmath{Red-Atom-Var} rule applies.  Otherwise, let $i = h_\ms{level}$, and
suppose the checkpoint context $\cenv$ contains $m$ checkpoints $\cvec{C\subi}{m}{1}$.

If $1 \le i \le m$, then the \refmath{Red-Atomic-Commit-1} rule applies.

$$\redatomiccommitone$$

If $i = 0 \wedge m > 0$, then the \refmath{Red-Atomic-Commit-2} rule applies.

$$\redatomiccommittwo$$

If $i < 0 \vee i > m$, then the \refmath{Red-Atomic-Commit-Error} rule applies.

$$\redatomiccommiterror$$


\end{itemize}

\end{proofcase}

%
% Match
%
\begin{proofcase}{Match}
Suppose $e_r = \matchstmt{h}{\setindex{j} \lvec{\matchcase{s\subi}{e\subi}}{\polybase}{\varbound}}$.  The type
judgment must use one of the match rules.  We illustrate with the rule \refmath{Ty-Match-Int}.

$$\tymatchint$$

Since $a$ has scalar type $\tyint$, by the \numericvalueslemma{} it must be either a variable or a
constant.  In the former case, the \refmath{Red-Atom-Var} rule applies.

In the latter case, the type judgment also requires that the sets $s$ be total, that is
$\bigcup_{j=1}^n s_j = \tyint$.  Since $h$ must belong to \emph{one} of the sets $s_k$, the
operation rule \refmath{Red-Match-Int} applies.

$$\redmatchint$$
\end{proofcase}

%
% Allocation
%
\begin{proofcase}{Allocation}
Suppose $e_r = \letalloc{v}{\allocop}{e}$.  One of the allocation reductions
\refmath{Red-Alloc-Tuple}, \refmath{Red-Alloc-Union}, or \refmath{Red-Alloc-Array} always applies.
\end{proofcase}

%
% Subscripting.
%
\begin{proofcase}{Subscripting}
\setatomname{\heaporig}

Suppose $e_r = \letsubscript{v}{t}{h_1}{h_2}{e}$.  The type judgment requires application of one of
the subscripting type rules.  We illustrate with \refmath{Ty-LetSub-Tuple}.

$$\tyletsubtuple$$

In this case, $h_1$ must be a variable $v_1$ with type $\tytuple{c}{u_0, \ldots, u_{i-1}, t_1, u_{i+1}, \ldots, u_{n-1}}$,
and $h_2$ must be a constant $\indexatom{c}{i}$.

Since $v_1$ has a tuple type, its value is determined by the \refmath{Ty-StoreTuple} rule.

$$\tystoretuple$$

That is $v_1$ must be a tuple $\hdef{v_1}{\tytuple{c}{u_0, \ldots, u_{i-1}, t_1, u_{i+1}, \ldots,
    u_{n-1}}}{\storetuple{\cvec{a\subi}{\polybase}{\varbound}}}$.

The reduction \refmath{Red-LetSub-Tuple} applies.

$$\redletsubtuple$$
\end{proofcase}

%
% Assignment
%
\begin{proofcase}{Assignment}
Suppose $e_r = \setsubscript{h_1}{h_2}{t}{h_3}{e}$.  The type judgment requires application of one
of the subscript assignment type rules.  We illustrate with \refmath{Ty-SetSub-Tuple}.

\begin{block}
\setarrayname{h_1}
\setatomname{h_3}

$$\tysetsubtuple$$
\end{block}

In this case, $h_1$ must be a variable $v_1$ with type $\tytuple{c}{u_0, \ldots, u_{i-1}, t_1, u_{i+1}, \ldots, u_{n-1}}$,
and $h_2$ must be a constant $\indexatom{c}{i}$.

Since $v_1$ has a tuple type, its value is determined by the \refmath{Ty-StoreTuple} rule.

$$\tystoretuple$$

That is $v_1$ must be a tuple $\hdef{v_1}{\tytuple{c}{u_0, \ldots, u_{i-1}, t_1, u_{i+1}, \ldots,
    u_{n-1}}}{\storetuple{\cvec{a\subi}{\polybase}{\varbound}}}$.

The reduction \refmath{Red-SetSub-Tuple} applies.

$$\redsetsubtuple$$
\end{proofcase}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Global values
%
\begin{proofcase}{LetGlobal}
Suppose $e_r = \letglobal{v}{t}{\xlabel}{e}$.  The corresponding typing rule is
\refmath{Ty-LetGlobal}.

$$\tyletglobal$$

By the \vardeclarationlemma, the label $\xlabel$ must be defined as part of the context.
The \refmath{Red-LetGlobal} rule applies.

$$\redletglobal$$
\end{proofcase}

\begin{proofcase}{SetGlobal}
Suppose $e_r = \setglobal{\xlabel}{t}{h}{e}$.  The corresponding typing rule is
\refmath{Ty-SetGlobal}.

$$\tysetglobal$$

By the \vardeclarationlemma, the label $\xlabel$ must be defined as part of the context.
The \refmath{Red-SetGlobal} rule applies.

$$\redsetglobal$$
\end{proofcase}

\end{proof}

%
% End of tech report mode
%
\else

The proof of preservation is a case analysis on the reduction operator, and the progress proof is by
induction on the length of the proof of $\withgenv{e_r : t}$.  The proofs are straightforward, but
tedious.  The complete proofs can be found in the technical report~\cite{fir:tech-report1}, and an outline of
the necessary lemmas is given in the Appendix.

\fi

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
