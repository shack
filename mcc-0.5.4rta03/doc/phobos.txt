PHOBOS DOCUMENTATION
Copyright (c) 2002 Adam Granicz, Caltech


What is Phobos?
----------------

Phobos is a generic extensible front-end to MCC. With Phobos, the
programmer can define programming languages by specifying syntax and
semantics, and add these definitions to the compiler dynamically. Syntax
is defined with lexical content and context-free grammars written in
BNF. Semantic specification occurs through term rewriting. 

Phobos provides generic lexing and parsing, and term conversion. After
parsing, resulting terms can be converted to any of the internal
representations supported by the compiler, including front-end abstract
syntax or the FIR.


How do I use Phobos?
---------------------

For now, take a look in test/phobos/*.pho. These are grammars of
languages, ranging from simple arithmetic expressions to entire
languages.

The following MCC options are useful at this point:

-grammar <file>     : Specifies a syntax/grammar description to use
-c_grammar <file>   : Specifies a saved description to use
-save_grammar       : Saves all language information
-use_fc             : Try to convert source term to FC abstract syntax
-debug_grammar      : Output grammar summary (used to resolve conflicts)

So, to compile a simple source file (in some language whose definition
is in simple.pho and the source filename called simple.tok), use

mcc -grammar simple.pho -save_grammar -use_fc -debug_grammar simple.tok

This parses simple.tok using the grammar in simple.pho, saves the parse
table and related information into a file (simple.cph), outputs a
grammar summary file (simple.output) and attempts to convert the
resulting MetaPRL term into FC abstract syntax. Note, that such
conversion will only succeed if the Phobos definition uses rewrites that
work with terms defined in the FC AST term set only. 

You can also compile language definitions by supplying only the Phobos
file to MCC:

mcc simple.pho

This creates the compiled language definition simple.cph, which can be
later used with the -cgrammar option.


Compiling Phobos
-----------------

To use Phobos, you must compile MCC with MetaPRL support. Follow the
instructions in compiler-intro.txt to enable the MetaPRL support.

 