%
%
%
\section{Data Structures}

\subsection{Memory block formats}

\subsubsection{Invariants}

\begin{itemize}
\item

Pointers during runtime refer to the first DATA word in the block.
Infix pointers can point to any offset in the data area, but the
embedded base pointer must refer to first DATA word in the block.

\item

The index is the WORD index into the pointer table.  Currently,
there are two bits below the index field; therefore, to get the
byte offset into the pointer table, we can simply mask the field
with \verb+0xfffffffc+, instead of shifting.

\item

The $\ms{size}$ field is always in WORDS.  Note that currently, there are
two bits below the size field; therefore to get the byte size, we
can simply mask the field with \verb+0xfffffffc+, instead of shifting.
\end{itemize}

\subsubsection{Definitions}

\begin{itemize}
\item

$M$ is the ``mark'' bit, used by GC.


\item

$R$ is the ``root'' bit, indicating whether this block is in the set
of root pointers.

\item

For function headers, the arity tag indicates the basic arity of
the function and the types of the arguments.  This is simply an
integer ID to a label; see \verb+arch/x86/util/x86_arity.ml+ for details.
\end{itemize}

\subsubsection{Block formats}

The block formats are shown in Figure~\ref{figure:block-formats}.

\makefigure{figure:block-formats}{Block formats}{%
\begin{center}
\begin{tabular}{cc}

\begin{tabular}[t]{|l|c|r|}
\multicolumn{3}{l}{Unsafe block format}\\
\multicolumn{2}{l}{{\sc msb}} & \multicolumn{1}{r}{{\sc lsb}}\\
\hline
\multicolumn{2}{|l|}{$\ms{size} : 30$ (words)} & 0R\\
\hline
\multicolumn{2}{|l|}{$\ms{index} : 30$ (word)} & 1M\\
\hline
\multicolumn{3}{|l|}{$\ms{data}_0$}\\
\multicolumn{3}{|l|}{$\vdots$}\\
\multicolumn{3}{|l|}{$\ms{data}_{\ms{size} - 1}$}\\
\hline
\multicolumn{3}{c}{}\\
\multicolumn{3}{l}{Function block format}\\
\multicolumn{2}{l}{{\sc msb}} & \multicolumn{1}{r}{{\sc lsb}}\\
\hline
\multicolumn{3}{|l|}{$\ms{arity\_tag} : 32$}\\
\hline
\multicolumn{2}{|l|}{$\ms{size} = 0 : 30$ (words)} & 00\\
\hline
\multicolumn{2}{|l|}{$\ms{index} : 30$ (word)} & 10\\
\hline
\multicolumn{3}{|l|}{function code}\\
\multicolumn{3}{|l|}{$\vdots$}\\
\hline
\end{tabular}

&

\begin{tabular}[t]{|l|c|r|}
\multicolumn{3}{l}{Safe block format}\\
\multicolumn{2}{l}{{\sc msb}} & \multicolumn{1}{r}{{\sc lsb}}\\
\hline
$\ms{tag} : 7$ & $\ms{size} : 23$ (words) & 0R\\
\hline
\multicolumn{2}{|l|}{$\ms{index} : 30$ (word)} & 0M\\
\hline
\multicolumn{3}{|l|}{$\ms{data}_0$}\\
\multicolumn{3}{|l|}{$\vdots$}\\
\multicolumn{3}{|l|}{$\ms{data}_{\ms{size} - 1}$}\\
\hline
\multicolumn{3}{c}{}\\
\multicolumn{3}{l}{$\atomint{i}$ format}\\
\multicolumn{2}{l}{{\sc msb}} & \multicolumn{1}{r}{{\sc lsb}}\\
\hline
\multicolumn{2}{|l|}{$i : 31$} & 1\\
\hline
\end{tabular}
\end{tabular}
\end{center}}

\subsection{Pointer table format}

The pointer table contains pointers into the heap, and free entries.
The free entries are linked into a free list, as shown in Figure~\ref{figure:pointer-table}.

\comment{JYH: this diagram needs some work.}

\makefigure{figure:pointer-table}{Pointer table}{%
\begin{center}
\begin{tabular}{|l|r|}
\multicolumn{2}{l}{Pointer table entry}\\
\multicolumn{1}{l}{MSB} & \multicolumn{1}{r}{LSB}\\
\hline
\multicolumn{2}{|l|}{$\vdots$}\\
$\ms{ptr}$ : 30 & 00\\
\multicolumn{2}{|l|}{$\vdots$}\\
$\ms{free}$ : 30 & 01\\
\multicolumn{2}{|l|}{$\vdots$}\\
\hline
\end{tabular}
\end{center}}

\subsection{Atomic levels}

Atomic levels represent the program checkpoints.  A checkpoint
$\checkpoint{\genv}{f}{\cvec{a\subi}{1}{n}}$ contains a program state $\genv$, and a function
$\tailcall{f}{\diamond, \cvec{a\subi}{1}{n}}$, where $\diamond$ is a special transaction parameter.

The atomic data struture is shown in Figure~\ref{figure:atomic-struct}.  The $\ms{fun}$ field
represents the function $f$ in the checkpoint, and $\ms{env}$ contains the arguments, collected in a
tuple.  The fields are represented as indexes into the pointer table.

The $\ms{copy\_point}$ field is the heap limit of the atomic level.  All heap data for the atomic
level is below the $\ms{copy\_point}$.

The $\ms{ptr\_entries}$ field, and the indexes $\ms{ptr\_size}$, $\ms{ptr\_next}$, and
$\ms{ptr\_scan}$ represent the inverse-difference pointer table for the level.  When a rollback is
performed for this level, the original pointer table at time of entry is reconconstructed by
replacing entries in the current table with those from $\ms{ptr\_entries}$.

\makefigure{figure:atomic}{Atomic (checkpoint) data structure}{%
\begin{center}
\begin{tabbing}
\tab\=\mb{unsigned} long ptr\_scan;XX\=\kill
\mb{typedef struct} atomic \{\+\\
    \mb{unsigned int} fun;\>       /* FUN INDEX to the atomic function */\\
    \mb{unsigned int} env;\>       /* AGGR INDEX to the environment */\\
\\
    \mb{void} *copy\_point; \>      /* Heap bounds of the atomic level */\\
\\
    \mb{unsigned} long ptr\_size;\> /* Max number of entries in the inverted table */\\
    \mb{unsigned} long ptr\_next;\> /* Current index into inverted table */\\
    \mb{unsigned} long ptr\_scan;\> /* Used for relocation during GC */\\
    \mb{void} *ptr\_entries[0];\>   /* Entries in the inverted table */\-\\
\} Atomic;
\end{tabbing}
\end{center}}

\subsection{Program context}

The program \emph{context} defines the heap areas and dtat structures for each process.  The context
has several parts.

\begin{description}
\item[Heap bounds]

The process \emph{heap} is defined with the following fields.  The $\ms{mem\_base}$ and
$\ms{mem\_limit}$ fields delimit the heap; the size of the heap is $\ms{mem\_size}$, which is always
equal to $\ms{mem\_limit} - \ms{mem\_base}$.  The $\ms{mem\_next}$ field is the next allocation
point.  We assume a compacting collector, so the free space is always contiguous.

There is always exactly one atomic block with the
MINOR flag set; this atomic block defines the minor heap.

\begin{center}
\begin{tabbing}
    \mb{void} *mem\_base;\\
    \mb{void} *mem\_next;\\
    \mb{void} *mem\_limit;\\
    \mb{unsigned} mem\_size;
\end{tabbing}
\end{center}

\item[Pointer table]

The pointer table defines a set of pointers
into the heap.  Every block in the heap has an index, and
the correspoing entry in the pointer table points to
that block.  All pointers in the heap are actually
indexes into the pointer table.

Free entries in the pointer table are linked.  The order
is random; it corresponds to the order in which the
entries were released by the GC.

The $\ms{pointer\_base}$ and $\ms{pointer\_limit}$ pointers delimit the
pointer table.  The $\ms{pointer\_next}$ indicates  the first entry
on the free list.  The $\ms{pointer\_free}$ field is the total number of
free entries, and $\ms{pointer\_size}$ is the total size of the
pointer table in \emph{bytes}; the size is $((\ms{pointer\_limit} - \ms{pointer\_base}) * sizeof(void *))$.

\begin{center}
\begin{tabbing}
    \mb{void} **pointer\_base;\\
    \mb{void} **pointer\_next;\\
    \mb{void} **pointer\_limit;\\
    \mb{unsigned} pointer\_free;\\
    \mb{unsigned} pointer\_size;
\end{tabbing}
\end{center}

\item[Floating point state]

The floatint-point state is used for floating-point temporaries.  They are a bandaid for Intel's
broken floating point arch, in particular they show up in int/float conversions.  The rest of the
compiler can ignore these two fields.

\begin{center}
\begin{tabbing}
    \mb{unsigned} float\_reg;\\
    \mb{unsigned} float\_reg\_2;
\end{tabbing}
\end{center}

\item[The minor heap]

The following fields define the minor heap.  The $\ms{minor\_base}$ and $\ms{minor\_limit}$ fields
delimit the minor heap.  The $\ms{minor\_roots}$ are the set of root pointers, and
$\ms{minor\_next}$ points to the next location for storing a root pointer.  The roots point to
blocks in the major heap that \emph{may} contain pointers into the minor heap.

\begin{center}
\begin{tabbing}
    \mb{void} *minor\_base;\\
    \mb{void} *minor\_limit;\\
    \mb{void} **minor\_roots;\\
    \mb{void} **next\_root;
\end{tabbing}
\end{center}

\item[Copy generations]

The \emph{atomic} fields are an array of pointers to all the currently-active transactions.
The array $\ms{atomic\_info}$ contains a pointer to each active transaction record.  The array has
variable size: the $\ms{atomic\_next}$ field indicates the next free entry, and the
$\ms{atomic\_size}$ field is the total number of entries.

\begin{center}
\begin{tabbing}
    \mb{Atomic} **atomic\_info;\\
    \mb{unsigned} atomic\_next;\\
    \mb{unsigned} atomic\_size;
\end{tabbing}
\end{center}

\item[Copy-on-write information]

The $\ms{copy\_point}$ field is the limit of the outermost transaction (or 0 if there is none).
The $\ms{gc\_point}$ field is $\ms{max}(\ms{minor\_base}, \ms{copy\_point})$.
There are two cases.

\begin{enumerate}
\item

If $\ms{gc\_point} = \ms{copy\_point}$, the current copy
generation is a subset of the minor heap.  In
this case, $\ms{minor\_base} \le \ms{gc\_point}$, and the minor
roots can be ignored.

\item

If $\ms{gc\_point} > \ms{copy\_point}$, then the current minor heap is a proper subset of the
current copy generation.
\end{enumerate}

The $\ms{gc\_point}$ field is used to indicate the heap area where \emph{some} action must be taken
when a block is mutated.  No action need be taken for blocks that have address larger than
$\ms{gc\_point}$.  When a block $p$ is mutated, a 
$\ms{COW}(p)$ fault is generated only if $p < gc\_point$.

\begin{enumerate}
\item

If $p \ge \ms{copy\_point}$, then the block is a reference to
a major block in the current generation.  The
block must be added to the set of minor roots.

\item

If $p < \ms{copy\_point}$, then this is a normal COW fault, and the block must be copied to the minor
heap.
\end{enumerate}

\begin{center}
\begin{tabbing}
    \mb{void} *copy\_point;\\
    \mb{void} *gc\_point;\\
    \mb{GCInfo} *gc\_info;
\end{tabbing}
\end{center}

\item[Globals]

The final part of the context is called the \emph{global} area.
The first part of the globals are pointers to global
values (like string constants, etc).  The remainder of the area includes spilled registers.

\begin{center}
\begin{tabbing}
    \mb{void} *globals[0];
\end{tabbing}
\end{center}
\end{description}


% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
