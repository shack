%
%
%

\section{Heap}

Garbage collector for x86.
This is a generational, compacting, mark/sweep collector.

The heap is shown in Figure~\ref{figure:heap-layout}.

\makefigure{figure:heap-layout}{Heap Layout}{%
   \centerline{\includegraphics{heap_layout}}}

Here are the data structures:

\begin{description}
\item[mem\_base] is the start of the heap
\item[minor\_base] is the start of the minor heap.  The major and minor heaps are contiguous.
\item[minor\_limit] is the bounds of the minor heap.  In general, the minor heap does not fill out
the entire major heap.  The minor heap will shift right as we migrate data from the minor to major
heap.
\item[mem\_limit] is the end of the heap.
\end{description}

For each copy generation $i$, there is a data structure $\ms{atomic}[i]$ that contains the
following.

\begin{itemize}
\item $\ms{copy}[i]$ is the location of the copy pointer. 
\item $\ms{ptr\_entries}[i]$ is a table of blocks that have been
copied to younger generations by the COW code.
\end{itemize}

\subsection{Invariants}

\begin{enumerate}
\item

The generational invariant is strict at $\ms{copy}[i]$ locations.  That is, there are no pointers
from any block below $\ms{copy}[i]$ to any block above $\ms{copy}[i]$.

\item

For each pointer $p$ in $\ms{ptr\_entries}[i]$, $p$ refers to a live block.  The block \emph{must}
be live, because it was referenced by the COW code, and parent generations are static.  The pointer
$p$ is always to a block at location $\ms{copy\_point}[i+1]$ or less.  Note that the block may
actually be part of an older generation.

\item

The heap may contain multiple blocks with the same index, but there is at most one block per index
per copy generation.

\item

If copy generation $i$ has been collected, so have all older generations.

\item

If there is a live block with index $i$ anywhere in the heap, then $\ms{pointer\_table}[i]$ refers
to the last of these blocks.  We need this invariant so that pointer table entries will be valid
when we perform a rollback.  See Section~\ref{section:rollback} below for more details; the problem
here is that pointer table entries can't be on the (singly-linked) free list when we roll back--how
do we unlink them?  So the default is that we never add them to the free list in the first place.

\end{enumerate}

\subsection{GC Strategy}

We use a mark-sweep compacting collector.
During the mark phase, we mark all reachable blocks, and
during the sweep phase, we shift the blocks left.

We collect one copy generation at a time.  Each proper
copy generation is collected at most once.  Further collection
will have no effect, because the heap is static for parent
generations.  We keep track of the last generation that was
collected in the $\ms{gc\_level}$ variable.

If the minor pointer is below the youngest copy pointer,
we can ignore the minor heap, or if we decide to collect the
entire youngest generation, we can ignore the minor heap.

When GC is called, we decide whether to do a major or minor
collection.  The GC is passed a set of root pointers (registers
and spills) that point to reachable blocks.

\subsection{Major Collection}

A major collection has the following phases.

\begin{itemize}
\item Mark.

Mark every block that is reachable from the root pointers.

\item Normalize.

The sweep phase will compact the heap, and all pointers will have to be adjusted to point to the new
locations.  The normalize phase converts all root pointers to indices into the pointer table.  The
pointer table will be updated with the new locations during the sweep phase, which allows us to
recover the root pointers afterwards.

\item Sweep.

In the sweep phase, we scan the heap from left to right.  The difficult part here is that the
pointers in the $\ms{ptr\_entries}$ tables have to be updated to point to their new locations.

Each block has at most one entry in the $\ms{ptr\_entries}$.  When we encounter a live block, we'll
have to find that entry.  To make this easy, we sort all the $\ms{ptr\_entries}$ tables so that
finding the pointers is fast (even though the initial cost of sorting the table is $n\ \ms{log}\
n$).

The sweep algorithm for copy generation $i$ is as follows.

\begin{enumerate}
\item[A] When we find a block that is marked:
\begin{itemize}
\item Reset the mark bit
\item Copy the block to the left
\item Set the entry in the pointer table
\item Search for an entry in the $\ms{ptr\_entries}[i]$ and above
and update the pointer if it is found
\end{itemize}

\item[B] When we find a block that is not marked,
then the block is free.

If the block is not part of the youngest generation, it is skipped.
Otherwise, if the entry in the pointer table points to the block, then thee are no other blocks with
the same index, and the pointer table entry is marked as being free.  If the entry in the pointer
table points to some other block, it must point to a block in a younger copy generation.  The entry
in the pointer table is retained, and the block is skipped.
\end{enumerate}

\item Denormalize:

In this phase, we convert all the root indices back to
normal pointers by looking up the values in the pointer
table.
\end{itemize}

\subsection{Minor Collection}

To implement the minor heap, we pretend that the minor
heap is implemented as a copy generation.  The difference is
that a COW fault does not copy the block, but it \emph{does} add
the block to the $\ms{ptr\_entries}$.

A minor collection is like a copy generation collection,
but we treat the $\ms{ptr\_entries}$ for the minor area as pointers
to blocks that may contain pointers into the minor heap.
Note: this is conservative, because a block will be in
$\ms{ptr\_entries}$ if the block is modified for $\emph{any}$ reason.

Up-marking: during a minor collection, we have to be careful
about what to do with blocks in the minor heap that have undergone
a COW fault.  If these blocks become free, we need to restore
the entry to point to the original block.  The solution we use
is to set all the pointer table entries from the parent's COW
table before the minor collection

\subsection{COW fault}

On a $\ms{COW}(p)$ fault, we have one of two cases.

The first is that the block $p$ is in the current generation, but it is in the major heap.  We
record the pointer in the minor roots (the $\ms{ptr\_entries}$ of the outermost Atomic block), and
continue.

Otherwise, this is a proper COW fault, and the block has to be copied.  The block is copied into the
minor heap, and a pointer is stored in the $\ms{ptr\_entries}$ of the second-to-outermost Atomic
block.

\subsection{Checkpoints}

When an atomic block is entered, two things have to happen.

\begin{enumerate}
\item We allocate a new atomic block.  Then $\ms{copy\_point}$ is set
to $\ms{mem\_next}$, the next memory allocation point.

\item We encapsulate the minor heap in the new copy generation.
The $\ms{minor\_base}$ becomes the base of the new copy generation.
The atomic blocks are linked so that the minor is still
the youngest copy generation.
\end{enumerate}

On the next GC, if the minor heap has shrunk significantly,
we perform a major collection.  Note, this may be expensive.
However, we expect copy generations to be long-lived, so this
may be reasonable.

\subsection{Commit}

When an atomic block finishes successfully, we delete the
youngest copy generation.  That means we delete the
second-to-outermost copy generation.

\subsection{Rollback}

When a rollback happens, the current copy generation is
garbage, and we need to restore the parent generation.  We
skip the minor generation, and restore the pointer table
from the second-to-outermost $\ms{ptr\_entries}$.  That generation
becomes the minor heap, and the current minor heap is
discarded.

% -*-
% Local Variables:
% Mode: LaTeX
% fill-column: 100
% TeX-master: "paper"
% TeX-command-default: "LaTeX/dvips Interactive"
% End:
% -*-
