MCC - HOWTO Release                                        Justin David Smith
-----------------------------------------------------------------------------


   This document contains instructions on how to prepare a new release.


HOW TO BUILD A NEW RELEASE TARBALL

   All commands should be run from the top directory of your working tree.

   1. Make sure you've committed all the changes you want in the release.
   
   2. IF RELEASING A BRANCH OFF THE TRUNK:  
         Tag the release, using ``cvs tag -b release_X_Y_Z'', where X.Y.Z 
         is the version number you specified in configure.in.  Then update 
         the working copy with ``cvs update -r release_X_Y_Z''.
      IF MAKING A TRUNK RELEASE:
         Skip to step 3.

   3. Modify configure.in, specifically:  change the variable MCC_VERSION
      near the very top of the file to reflect the new release version.

   4. Run ``make Makefile''.  This will rebuild the Makefile with correct
      version information.

   5. Commit all changes (including changes to configure and configure.in)
      to the repository, on the trunk or current branch.  This is necessary
      because the dist command will checkout the current CVS copy for the
      distribution; it will NOT use your working copy.

   6. IF MAKING A TRUNK RELEASE:
         Tag the release, using ``cvs tag -b release_X_Y_Z'', where X.Y.Z 
         is the version number you specified in configure.in.  Then update 
         the working copy with ``cvs update -r release_X_Y_Z''.
      IF RELEASING A BRANCH OFF THE TRUNK:
         You already did this ;)  Skip to step 7.

   7. Run ``make dist''.  If you forgot step #2, then make will rebuild the
      Makefile and ask you to try again.  If you forgot step #3, then make
      will stop and remind you to commit your changes before continuing. 
      The tarball is built off of the branch your current working copy is on
      --- if this is not the trunk, then you will get a message indicating
      what branch is being used to build the tarball.

   At this point, you should have a tarball named mcc-X.Y.Z.tar.gz, where
   X.Y.Z is the version number you specified in configure.in.
