\documentclass[10pt,letterpaper]{article}



%
%  Define properties specific to this specification file
%     \textitle      Title of this specification
%     \texauthor     Author of the specification
%     \texversion    Version number for this specification
%
\def\textitle{System Migration}
\def\texauthor{Justin David Smith}
\def\texversion{1.0}
\def\texdate{July, 2002}



%
%  General latex setup
%
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{latexsym}
\usepackage{fancyheadings}
\usepackage{multirow}
\usepackage{makeidx}
\usepackage{float}

\normalsize
\def\stdindent{0.25in}
\setlength{\textwidth}{6.0in}
\setlength{\marginparwidth}{0pt}
\setlength{\oddsidemargin}{0.25in}
\setlength{\headheight}{0.25in}
\setlength{\headsep}{0.25in}
\setlength{\topmargin}{0.0in}
\setlength{\textheight}{8.5in}
\setlength{\parskip}{0pt}
\setlength{\parindent}{0.25in}

\pagestyle{fancy}
\lhead{\textitle\ \texversion}
\lfoot{\texauthor}
\rfoot{\texdate}



%
%  Define utilities
%
\renewcommand\tt[1]{\texttt{#1}}
\renewcommand\ss[1]{\textsf{#1}}
\renewcommand\it[1]{\textit{#1}}

%
%  Indexing utilities
%
\newcommand\Index[1]{#1\index{#1}}
\makeindex



%
%  Migration commands
%

%  Constructors
\newcommand\SpecialCall{\ss{SpecialCall}\index{SpecialCall@\ss{SpecialCall} constructor}}
\newcommand\TailCall{\ss{TailCall}\index{TailCall@\ss{TailCall} constructor}}
\newcommand\TailSysMigrate{\ss{TailSysMigrate}\index{TailSysMigrate@\ss{TailSysMigrate} constructor}}
\newcommand\SysMigrate{\ss{SysMigrate}\index{SysMigrate@\ss{SysMigrate} constructor}}

%  Modules
\newcommand\Firmprog{\ss{Fir\_mprog}\index{Fir\_mprog@\ss{Fir\_mprog} module}}
\newcommand\Mirspecial{\ss{Mir\_special}\index{Mir\_special@\ss{Mir\_special} module}}
\newcommand\Xmir{\ss{X86\_mir}\index{X86\_mir@\ss{X86\_mir} module}}

%  Types
\newcommand\firprog{\tt{Fir.prog}\index{Fir.prog@\tt{Fir.prog} type}}
\newcommand\firmprog{\tt{Fir.mprog}\index{Fir.mprog@\tt{Fir.mprog} type}}
\newcommand\migratestruct{\tt{Frame\_type.migrate}\index{Frame\_type.migrate@\tt{Frame\_type.migrate} type}}

%  Fields
\newcommand\progglobalorder{\tt{Mir.prog\_global\_order}\index{Mir.prog\_global\_order@\tt{Mir.prog\_global\_order} field}}
\newcommand\progfunorder{\tt{Mir.prog\_fun\_order}\index{Mir.prog\_fun\_order@\tt{Mir.prog\_fun\_order} field}}
\newcommand\migkeystruct{\tt{Frame\_type.mig\_key}\index{Frame\_type.mig\_key@\tt{Frame\_type.mig\_key} field}}
\newcommand\migchannelstruct{\tt{Frame\_type.mig\_channel}\index{Frame\_type.mig\_channel@\tt{Frame\_type.mig\_channel} field}}
\newcommand\migbidirstruct{\tt{Frame\_type.mig\_bidir}\index{Frame\_type.mig\_bidir@\tt{Frame\_type.mig\_bidir} field}}
\newcommand\miginitofsstruct{\tt{Frame\_type.mig\_initofs}\index{Frame\_type.mig\_initofs@\tt{Frame\_type.mig\_initofs} field}}
\newcommand\migregistersstruct{\tt{Frame\_type.mig\_registers}\index{Frame\_type.mig\_registers@\tt{Frame\_type.mig\_registers} field}}
\newcommand\migptrsizestruct{\tt{Frame\_type.mig\_ptr\_size}\index{Frame\_type.mig\_ptr\_size@\tt{Frame\_type.mig\_ptr\_size} field}}
\newcommand\migheapsizestruct{\tt{Frame\_type.mig\_heap\_size}\index{Frame\_type.mig\_heap\_size@\tt{Frame\_type.mig\_heap\_size} field}}
\newcommand\migdebugstruct{\tt{Frame\_type.mig\_debug}\index{Frame\_type.mig\_debug@\tt{Frame\_type.mig\_debug} field}}

%  Acronyms
\newcommand\FC{\Index{FC}}
\newcommand\FIR{\Index{FIR}}
\newcommand\MIR{\Index{MIR}}

%  Other constructs
\newcommand\mdf{\ss{mdf}\index{mdf@\ss{mdf}}}
\newcommand\migrate{\tt{migrate}}
\newcommand\migrated{\tt{mcc-migrated}\index{mcc-migrated@\tt{mcc-migrated}}}
\newcommand\resurrect{\tt{mcc-resurrect}\index{mcc-resurrect@\tt{mcc-resurrect}}}
\newcommand\main{\tt{main}}




%
%  Indexing cross-references
%
\index{mdf@\textsf  {mdf}|see{migrate destination format}}
\index{mcc-migrated@\texttt  {mcc-migrated}|see{migration server}}
\index{mcc-resurrect@\texttt  {mcc-resurrect}|see{resurrection}}



%
%  Document begins here
%
\begin{document}



%
%  Generate document title and TOC
%
\pagestyle{plain}
\pagenumbering{roman}



\begin{center}
   {\Large \textitle}\linebreak\linebreak
   {\large Version \texversion}\linebreak
   {\large \texauthor}\linebreak
   {\large \texdate}\linebreak
\end{center}



\begin{abstract}
System migration is a primitive in the Mojave Compiler Collection (MCC)
designed to allow programs to migrate their entire state from one machine
to another, independent of architecture or language.  This paper describes
the basic use of system migration, including use in the source language
and use of the various migration servers.  This paper also discusses the
implementation details for system migration in MCC.
\end{abstract}



\tableofcontents
\pagebreak



%
%  Begin main paper content
%
\pagestyle{fancy}
\pagenumbering{arabic}



\section{Introduction}

   \emph{Note:}  The current version of this document is not thorough.
                  
 \subsection{Motivation}
   Since the invention of the first digital computers, the processing capability
   of computers has increased at an exponential rate rougly following Moore's law.
   The form of this increase has changed over the years; the supercomputers of past
   decades have been replaced by distributed systems composed of many low-end machines, or \emph{nodes},
   each linked by a high-speed communication network.  Computations may be performed on these
   distributed systems, however the computation must be broken up into smaller parts,
   and each part must be assigned to a specific node in the system.  A large amount of
   code in a distributed computation is devoted to maintaining synchronization, and attempting
   to recover lost computation when a particular node fails.  This tends to be very cumbersome
   for the programmer involved --- in particular, if the system is to be fault-tolerant, then
   the programmer must spend a great deal of time determining all the possible failure modes for 
   the system and how to recover from them.

   As distributed computing becomes more ubiquitous in technology, we would like to
   develop a system where each node is indistinguishable from another, and where
   processes in the system see the machine as a single entity.  Processes in such a
   system should be free to move from machine to machine, and link themselves with other
   processes to interact.  This notion of \emph{process mobility} is the central part of
   \emph{ambient calculus}.  This system should be integrated with the programming language,
   to ease the job of the programmer.
   
   We would like any mechanism we design to support fault-tolerance, such that if a
   particular node in the system fails, the mechanisms will automatically recover the
   computation without any special effort from the programmer.  We can implement \emph{rollback}
   which allows a process to return to a previous ``known-valid'' state should a later
   operation fail.  By implementing \emph{atomic} operations, we can write a block of code that
   will attempt to perform a series of operations in the block, but will rollback to the beginning 
   of the block (as if none of the operations were executed) should any one operation fail.
   
   With these higher-level primitives at the language level, we allow programmers
   to write distributed applications with much greater ease and with little direct knowledge
   of the underlying architecture of the system.  With an administrative monitor, we may 
   also improve the efficiency of the system with the load-balancing and resource localization
   techniques described above.

 \subsection{Related references}
   Migration is also discussed in the technical report caltechCSTR 2002.007, \emph{Process Migration and Transactions
   Using a Novel Intermediate Language}, by Jason Hickey, Justin D. Smith, Brian Aydemir, Nathaniel Gray, Adam Granicz and Cristian Tapus,
   July 2002.



\section{Using System Migration}
 
 \subsection{Migrate destination format and protocols}
   \label{sec:destination}
   The \Index{migrate destination format} is a string which describes how a process should
   be migrated.  It resembles standard HTTP URLs, and consists of a protocol component
   and a name component.
   \[\begin{array}{rcll}
      \mdf        & ::= &  \text{``\tt{host://}''}\ \ss{hostname}\ \left(\text{``\tt{:}''}\ \ss{integer}\right)^?
                        &  \hbox{Migrate to remote $\ss{hostname}$, port $\ss{integer}$} \\
                  &  |  &  \text{``\tt{suspend://}''}\ \ss{filename}
                        &  \hbox{Save state to $\ss{filename}$, and terminate} \\
                  &  |  &  \text{``\tt{checkpoint://}''}\ \ss{filename}
                        &  \hbox{Save state to $\ss{filename}$, and continue} \\
   \end{array}\]
   
   These protocols are discussed in more detail below:
   \begin{itemize}
   \item \tt{host://}:  The \Index{host protocol} is the standard form of system-to-system
         migration.  A hostname and optional port number are specified; a \Index{migration server}
         should be running at this location.  The process will attempt to migrate its
         entire state to the remote machine.  If the state is successfully migrated, then the
         process on the source machine will terminate, and the process will resume at the
         proper location on the destination machine.  If any errors occur, then the process
         will continue to run on the source machine, and no process will be started on the
         destination machine.
   \item \tt{suspend://}:  The \Index{suspend protocol} will save the process state to a file
         on disk, instead of sending it to a remote machine.  The name of the file to write to
         is specified as part of the format.  If the state is successfully
         written to disk, then the process will terminate; otherwise, the process will continue.
         A process state file can be resumed on any machine using the \Index{resurrection} utility.
   \item \tt{checkpoint://}:  The \Index{checkpoint protocol} is similar to the \Index{suspend protocol},
         but the process will continue executing even when the process state is successfully written
         to the disk.  This is useful for recovering a process state when a node in the network fails;
         if the process has taken periodic \Index{checkpoints}, then the process can be recovered from
         its last checkpoint.
   \end{itemize}



 \subsection{Migration utilities}
   There are two migration programs which may be used by the end user ---
   \migrated\ and \resurrect.  Both of these utilities are in the \tt{net/migrate}
   directory of MCC.

 \subsubsection{The \migrated\ program}
   \migrated\ is the \Index{migration server}, and is run on any machine willing to accept
   migrated programs.  Run \migrated\tt{~-help} for a list of supported command-line
   options.  Migration options that are shared by all programs are discussed in section
   \ref{sec:migrate-options}; the only other option of interest for the server is \tt{-port},
   which allows you to run the migration server on a nonstandard port.  It is usually safe
   to leave this set to the default.
   
   The \Index{migration server} is able to accept and run multiple processes at once; it will fork
   a new compiler for each process that is received.  After migration, processes will inherit
   the standard I/O channels used by the \Index{migration server}.  If migration fails, then an error
   will be printed to the server's \tt{stderr} channel, but other processes running on the
   server will not be interrupted.  The migration server does not terminate under normal
   circumstances (it may be killed using standard UNIX signals).

 \subsubsection{The \resurrect\ program}
   \resurrect\ is the \Index{resurrection} program, and is used to recover the process state
   file that was written by a suspend or checkpoint operation.  Run \resurrect\tt{~-help} for
   a list of supported command-line options.  Migration options that are shared by all programs
   are discussed in section \ref{sec:migrate-options}; there are no options of interest that
   are specific to \resurrect.  \resurrect\ expects one non-option argument, the name of the
   file to read the process state from.
   
   The \Index{resurrection} program will attempt to compile the \FIR{} code stored in the process
   state; if it is successful, then it will load the process heap data and run the program.
   The process will inherit the standard I/O channels used by the \Index{resurrection} program.
   Once the process terminates, the \Index{resurrection} program will also terminate.

 \subsubsection{Options shared by migration programs}
   \label{sec:migrate-options}
   Many options are shared by all migration programs; most of the shared options control
   various debugging aspects of the compiler and migration.  Note that while migration
   programs will invoke the compiler from the \FIR{} stage into the backend, you are not allowed
   to specify options to control the compilation\footnote{For example, you cannot specify new
   optimization flags to use for the migrated program.} --- the options used to compile a migrated 
   program must match the options used for the original program, therefore the options will be 
   migrated along with the remaining program state.
   
   In addition to debugging options that are imported from the compiler, several debugging
   options specific to migration are defined:
   \begin{itemize}
   \item \tt{-migrate\_emit\_fir} emits the received \FIR{} to a file.  The location of the file
         emitted will be printed on the \tt{stderr} channel.
   \item \tt{-migrate\_save\_obj} will preserve the object file(s) and executable program
         generated by the compiler.  By default, the object file(s) and executable program
         are unlinked once the migrated process terminates.  Note that the object file(s) and
         executable program have limited use in debugging; they cannot be independently run
         since they do not contain the heap data.
   \item \tt{-migrate\_no\_exec} will prevent the server from executing any received processes.
         This is generally not what you want, and is used only to debug the communication protocol
         up to the initial heap transfer.
   \item \tt{-migrate\_gdb} adds hooks to invoke \tt{gdb} on the migrated process.  Use this
         flag with caution, it is not well-documented and contains a number of developer quirks.
         You will not be able to run programs without user interaction if this switch is given.
   \end{itemize}



 \subsection{Invoking system migration from an \FC{} program}
   In \FC, migration can be invoked using the $\migrate$ call.  This call is defined in
   the standard header file \tt{fc.h}, and has the following prototype:
\begin{verbatim}
static void migrate(char *destination) = "migrate";
\end{verbatim}

   The \tt{destination} argument is a string which follows the \Index{migrate destination format}, defined in section \ref{sec:destination}.
   When this call is executed, the process will attempt to migrate its
   entire state to the location specified in \tt{destination}, and will then continue
   executing\footnote{Unless the \Index{suspend protocol} is specified in \tt{destination};
   if that is the case, the process will ``continue'' when it is revived by the \Index{resurrection} program.}.  
   Currently, there is no way for the process to detect whether migration
   was successful from this call; future revisions will provide a status function which
   the process can use to determine the result of migration.
   
   A simple example program is given below.  In order to successfully use this program,
   you must first start a copy of the migration server using \migrated.  You should see
   the program migrate to the server, and return (on the server!) with code $42$:
\begin{verbatim}
/* Trivial migration program */
#include "fc.h"

int main(int argc, char **argv) {
   migrate("host://localhost");
   return(42);
}
\end{verbatim}



 \subsection{Examples of using migration}
   The following are examples of how to use system migration.
   
 \subsubsection{Migrating from one machine to another}
   To migrate from machine \tt{source.example.com} to machine \tt{dest.example.com}, you must 
   first start a migration server on \tt{dest.example.com} as follows, using \migrated.  Assume 
   MCC is installed to \tt{/usr/local/mcc}:
\begin{verbatim}
dest.example.com# /usr/local/mcc/bin/mcc-migrated
Server listening on port 12354
\end{verbatim}

   The following \FC{} program will migrate to \tt{dest.example.com}:
\begin{verbatim}
/* Migrate to dest.example.com */
#include "fc.h"

int main(int argc, char **argv) {
   /* Place code to run on source.example.com here */
   migrate("host://dest.example.com");
   /* Place code to run on dest.example.com here */
   return(0);
}
\end{verbatim}

   Assuming this program is saved to \tt{migrate.c} on the machine \tt{source.example.com},
   run the following commands:
\begin{verbatim}
source.example.com# /usr/local/mcc/bin/mcc -O3 -o migrate migrate.c
source.example.com# ./migrate
\end{verbatim}

   At this point, you should see the program migrate from \tt{source.example.com}
   to the machine \tt{dest.example.com}.  Any code after the migration code will be run on
   the destination machine, and any output after migration will be sent to the
   console the migration server was run from.

 \subsubsection{Migrating to a nonstandard port on a machine}
   To migrate from machine \tt{source.example.com} to a nonstandard port (e.g. port $9999$) on 
   machine \tt{dest.example.com}, you must first start a migration server on \tt{dest.example.com} 
   using \migrated.  Assume MCC is installed to \tt{/usr/local/mcc}:
\begin{verbatim}
dest.example.com# /usr/local/mcc/bin/mcc-migrated -port 9999
Server listening on port 9999
\end{verbatim}

   The following \FC{} program will migrate to \tt{dest.example.com}:
\begin{verbatim}
/* Migrate to port 9999 on dest.example.com */
#include "fc.h"

int main(int argc, char **argv) {
   /* Place code to run on source.example.com here */
   migrate("host://dest.example.com:9999");
   /* Place code to run on dest.example.com here */
   return(0);
}
\end{verbatim}

   Assuming this program is saved to \tt{migrate.c} on the machine \tt{source.example.com},
   run the following commands:
\begin{verbatim}
source.example.com# /usr/local/mcc/bin/mcc -O3 -o migrate migrate.c
source.example.com# ./migrate
\end{verbatim}

   At this point, you should see the program migrate from \tt{source.example.com}
   to port $9999$ on \tt{dest.example.com}.  Any code after the migration code will 
   be run on the destination machine, and any output after migration will be sent to 
   the console the migration server was run from.

 \subsubsection{Suspending a process for later execution using \resurrect}
   Processes can use the migration mechanism to save a checkpoint for later
   execution.  To do this, processes should use the \Index{suspend protocol}
   and the \resurrect{} program.  A sample program which suspends itself for
   later execution is given below:
\begin{verbatim}
/* Suspend process for later execution */
#include "fc.h"

int main(int argc, char **argv) {
   /* Place code to run before suspending here */
   migrate("suspend://migrate.state");
   /* Place code to run after suspending here */
   return(0);
}
\end{verbatim}

   Assuming MCC is installed to \tt{/usr/local/mcc} and the source file is
   named \tt{migrate.c}, run the following commands:
\begin{verbatim}
example.com# /usr/local/mcc/bin/mcc -O3 -o migrate migrate.c
example.com# ./migrate
\end{verbatim}

   At this point, any commands before the suspend call will be run, and then
   the file \tt{migrate.state} will be written to the current directory\footnote{An
   absolute path for the state file can be specified in the suspend protocol by 
   using $3$ slashes instead of $2$; e.g. \tt{suspend:///full/path/to/migrate.state}.}.
   The process may be revived on any machine and at any later time using this file.
   To revive the process, run:
\begin{verbatim}
example.com# /usr/local/mcc/bin/mcc-resurrect migrate.state
\end{verbatim}

   At this point, any remaining code in the file will be run, and the process
   will terminate normally.
   
   Note that you can take a process checkpoint using a similar procedure.


\section{Implementation of System Migration}

 \subsection{\FIR{} implementation}
   Like all useful things in MCC, system migration requires a primitive in the
   \FIR.  System migration uses the $\SpecialCall$ mechanism in the \FIR, which is
   designed to simplify manipulation of the \FIR.  $\SpecialCall$ takes an operation
   to perform, and a function application; once a special operation is performed, 
   the function is called with its arguments.  The syntax and semantics of a
   $\SpecialCall$ resemble $\TailCall$, therefore most program manipulations can treat
   $\SpecialCall$ in the same way they treat $\TailCall$.  This helps to minimize the overhead
   of system migration in the \FIR.
   
   The exact form for system migration is given in the following ML type definitions:
   \[\begin{array}{lcll}
      \mathop{\ss{type}} \it{exp}   & ::= &  \ldots
                                          &  \hbox{\FIR expressions} \\
                                    &  |  &  \SpecialCall \mathrel{\ss{of}} \it{debug\_label} \mathrel{*} \it{tailop}
                                          &  \hbox{Special tail calls} \\
      \mathop{\ss{and}} \it{tailop} & ::= &  \ldots
                                          &  \hbox{Operations for special calls} \\
                                    &  |  &  \TailSysMigrate \mathrel{\ss{of}} \it{int} \mathrel{*} \it{atom} \mathrel{*} \it{atom} \mathrel{*}
                                          &  \hbox{System migration} \\
                                    &     &  \phantom{\TailSysMigrate \mathrel{\ss{of}}}\,\it{atom} \mathrel{*} \it{atom}~\ss{list} \\
   \end{array}\]
   
   The \it{tailop} $\TailSysMigrate\ (\it{id}, \it{dst\_base}, \it{dst\_off}, f, \it{args})$ will
   migrate to the location specified by the string in $\it{dst\_base} + \it{dst\_off}$, then call
   $\it{f}(\it{args})$ after migration\footnote{Following standard convention for \FC{} string pointers,
   the location pointer is stored in two variables, a base pointer in \it{dst\_base} and an integer offset
   in \it{dst\_off}.}.  The \it{id} argument is an integer that is unique over all $\TailSysMigrate$ calls
   present in the \FIR{} program, used to determine where execution should resume after a successful
   migration.
   
   Note that \it{args} covers all live variables of the program, therefore all data that must
   be migrated is either contained in the heap or in variables indicated in \it{args}.  This is
   useful in the implementation of system migration, since we do not have to perform a live
   variable analysis to identify variables which must be migrated.

 \subsubsection{Generation of initial special call}
   The \FIR{} performs no special manipulation of special calls; special calls are
   generally constructed by the frontend during the IR-to-\FIR{} transformation.  In
   the \FC{} frontend, the migration primitive is treated as an external function call
   with a well-known name until the IR-to-\FIR{} transformation, where it is rewritten 
   into a special call.

 \subsubsection{Marshal program data structure}
   Migration will marshal the \FIR{} program, therefore at the end of the \FIR{}
   transformations a marshallable version of the \FIR{} must be constructed.  A
   standard \FIR{} program uses the \firprog{} data structure, but marshallable
   programs use the \firmprog{} data structure.  The \firmprog{} data structure
   contains the full \FIR{} program, and it also contains two lists of ordered 
   symbols which correspond to \progfunorder{} and \progglobalorder{} in the \MIR{}
   program (described in section \ref{section:mir-impl}).
   
   Once constructed, the \firmprog{} data structure must never be modified.  The
   \firmprog{} data structure is generated by the \Firmprog module.



 \subsection{\MIR{} implementation}
   \label{section:mir-impl}
   In the \MIR, system migration is translated from the $\SpecialCall$ form into a
   native \MIR{} primitive called $\SysMigrate$.  $\SysMigrate$ resembles its $\SpecialCall$ 
   form, however the function arguments are replaced with a single environment pointer
   to simplify the migration process.  $\SysMigrate$ has the following type:
   \[\begin{array}{lcll}
      \mathop{\ss{type}} \it{exp}   & ::= &  \ldots
                                          &  \hbox{\MIR{} expressions} \\
                                    &  |  &  \SysMigrate \mathrel{\ss{of}} \it{int} \mathrel{*} \it{atom} \mathrel{*} \it{atom} \mathrel{*} \it{var} \mathrel{*} \it{atom}
                                          &  \hbox{System migration} \\
   \end{array}\]
   
   The statement $\SysMigrate\ (\it{id}, \it{dst\_base}, \it{dst\_off}, f, \it{env})$
   will migrate to the location specified in $\it{dst\_base} + \it{dst\_off}$, then call
   $\it{f}(\it{env})$.  \it{env} is a pointer to an aggregate memory block containing all live variables
   of the program (the \it{args} from $\TailSysMigrate$).  At this point, certain implementation 
   details of system migration are exposed in the language, notably the use of a single atom to
   contain the live variables.  Note that \it{env} is the only live variable in program across
   $\SysMigrate$.
   
   The translation from $\TailSysMigrate\ (\ldots, f, \it{args})$ primitive to $\SysMigrate\ (\ldots, f', \it{ienv})$ involves the following
   steps:
   \begin{itemize}
   \item Generate a \Index{stub function} $f'$ which accepts a single argument, a pointer index to an aggregate block \it{ienv}.
         Convert the index into a real aggregate pointer \it{env}, with standard safety checks.
         Then, load all corresponding to \it{vars} from the \it{env} pointer into temporary variables.
         All standard safety checks are applied to the load operations, including bounds
         checking of the \it{env} pointer.  The stub $f'$ terminates in a call to $f$ with the 
         temporary variables as arguments.
   \item Generate \MIR{} code to allocate the aggregate pointer \it{env}, and store all variables
         in \it{vars} into the \it{env} pointer.  The normal bounds checking on \it{env} can be
         omitted here, since the allocation is guaranteed to be sufficiently large to hold
         all variables.  The \it{env} pointer is converted into an pointer index \it{ienv}.
         The new \MIR{} code terminates with the statement $\SysMigrate\ (\ldots, f', \it{ienv})$.
   \end{itemize}
   
   By storing all variables into an aggregate block in the heap, we simplify several problems in system migration:
   \begin{itemize}
   \item All data is stored in the heap at the time of migration, with the exception of the \it{ienv}
         aggregate index.  This value is migrated explicitly as part of the context.
   \item Since no data is stored in variables, no data will be stored in the hardware-specific registers.
         Therefore system migration does not need to construct a map between register names across different
         architectures.
   \item Since all data is stored in the heap, all data has a standard, architecture-independent representation.
         Data in hardware registers may continue to have hardware-dependent representation without interfering
         with system migration.  No real pointers exist in the data, therefore system migration does not need
         to construct a map between pointers across different machines.
   \item All safety checks are enabled in the \Index{stub function} $f'$, therefore the data in variables will be
         validated for accuracy.  This would be difficult to perform if we attempt to migrate registers
         containing real pointers\footnote{It is for this reason that we migrate the \it{env} pointer as
         an aggregate index as well.  It would be difficult to verify \it{env} is a real pointer on the
         destination machine, and we would also have to worry about translating mapping pointers between
         two different machines.}.
   \end{itemize}

   There are a few caveats to storing data in the heap.  Notably, all registers for a process must
   be stored in the heap on each migration.  While this is reasonable when the process is actually
   migrating elsewhere, for the \Index{checkpoint protocol} it means that all register data must be 
   stored in the heap and then immediately loaded back into registers, with safety checks.  This 
   operation may be fairly expensive if the process intends to take frequent checkpoints.
   
   Also, since all pointers are stored in the heap using indexes, we must be careful that indexes
   on the destination machine \emph{exactly} match.  This is not a problem for data pointers, since
   the pointer table is migrated as-is; however, the order of \Index{program globals} and the
   \Index{function closure table} must be preserved across migration\footnote{The \Index{program globals}
   are stored in the context, and referred to by index.  The function closure table is the table of
   all valid function pointers; each function which \emph{escapes} is allocated an entry in the function
   pointer table.  If this table is reordered, then all function pointers in the heap will become invalid.}.
   Unfortunately, the sort order of symbols is not preserved when a \FIR{} program is marshalled to another
   machine.

   To resolve the issue of symbol ordering, the \MIR{} maintains two fields to keep track of symbol ordering:  \progglobalorder{}
   is an ordered list of all program globals, and \progfunorder{} is an ordered list of all functions which
   may escape.  When a program migrates to another machine, these ordered lists are preserved so the global
   and function closure tables will be emitted in the same order.  Note that the \Index{stub function}s 
   escape by definition, therefore they must be inserted into the \progfunorder{} list in a well-defined 
   order.  Typically, a \Index{stub function} $f'$ is inserted into the \progfunorder{} list immediately
   after the entry for the function $f$ that it calls.

 \subsubsection{Generation of \MIR{} primitive}
   Due to the complexity of the \FIR-to-\MIR{} transformation, the \MIR{} preserves
   the \FIR{} \SpecialCall{} primitive until an early stage in the \MIR{} transformation.
   The \Mirspecial{} module is responsible for rewriting all \SpecialCall{} forms into
   the appropriate \MIR{} primitives, such as \SysMigrate.  Notably, \Mirspecial{} generates the stub function
   and packages all arguments into the environment block.
      


 \subsection{Backend/runtime implementation}
   The implementation for the Intel IA32 backend is discussed here.  Other backends
   should use a similar mechanism for implementing system migration.

 \subsubsection{The \migratestruct{} data structure}
   Any program compiled by the IA32 backend is either a \emph{normal} program or a
   \emph{migrated} program.  Normal programs are programs compiled by the user using
   the standard compilation process; these programs begin execution at the \main{}
   function, like any standard program.  Migrated programs are programs being compiled
   by \migrated{} or \resurrect; these programs use a nonstandard entry point that
   loads the process state from a network connection or state file, and resume execution
   where the original process left off.  
   
   Migrated programs are compiled with additional information that tells the runtime 
   where to load the heap data from and where to resume execution.  This information 
   is specified in the \migratestruct{} data structure.  This data structure is not
   passed to the backend for normal programs.
   
   The \migratestruct{} data structure contains several significant fields, described
   below:
   \begin{itemize}
   \item \migkeystruct{} is the string representation of the migrate key, or \it{id},
         that determines where program execution should resume at.
   \item \migchannelstruct{}, \migbidirstruct{} and \miginitofsstruct{} describe the channel the runtime
         should use to read heap data from.  This may contain a file descriptor for
         a UNIX file or a network connection.  If the channel is bidirectional, the
         runtime can reply to the sender to verify data integrity.
   \item \migregistersstruct{} is the index for the block containing the program
         registers, or \it{env}.
   \item \migptrsizestruct{} and \migheapsizestruct{} indicate the sizes of the
         pointer table and heap, so the runtime can allocate sufficient space before
         receiving the heap data.
   \item \migdebugstruct{} contains miscellaneous debugging flags for the migrate runtime.
   \end{itemize}
   
   When the IA32 backend is given the \migratestruct{} data structure, then the compiled
   program will automatically start in the migrate runtime, instead of invoking the
   program's \main{} function.  This data structure is also written out as part of the
   code segment of a migrated program.
   
 \subsubsection{Transforming \SysMigrate\ to assembly code}
   The \MIR{} primitive $\SysMigrate\ (\it{id}, \it{dst\_base}, \it{dst\_off}, f, \it{env})$ is
   translated into assembly code in the IA32 backend by the \Xmir{} module.  Most of system
   migration is implemented in the runtime, so most of the assembly code deals with calling
   the migrate runtime and generating the ``key'', or the label that the migrate runtime will
   jump to on a successful migration.  The key is derived from \it{id}, and is the same key
   that will be stored in \migkeystruct{} when the migrated program is compiled on the destination
   machine.
   
   The pseudocode for the assembly code generated is below:
   \begin{tabbing}
      \hspace{0.25in}\=\hspace{0.25in}\=\hspace{0.25in}\=\kill\\
      \tt{migrate\_begin:}\\
         \>1.\>Push \it{dst\_off}, \it{dst\_base}, \it{env}, \it{id} onto the stack.\\
         \>2.\>Push a pointer to the section containing binary \FIR{} onto the stack.\\
         \>3.\>Call the function ``\tt{\_\_migrate}'' in the runtime.\\
         \>  \>If migration is successful, this call will not terminate.\\
         \>  \>If migration fails or \Index{checkpoint protocol} used, execution resumes.\\
         \>4.\>Clear the stack arguments from call to runtime.\\
         \>5.\>Jump to \tt{migrate\_continue}.\\
      \tt{migrate\_key:}\\
         \>1.\>Clear stack arguments from entry.\\
         \>2.\>Jump to \tt{migrate\_continue}.\\
      \tt{migrate\_continue:}\\
         \>1.\>Call $f(\it{env})$.
   \end{tabbing}
   
   Normal programs enter at \tt{migrate\_begin}.  A migrated program enters to the runtime;
   once the heap is reconstructed, the program execution resumes at \tt{migrate\_key}, which
   is a label derived from the \it{id} value.
   
 \subsubsection{Runtime support for migration}
   The runtime support for migration consists of two parts: a \emph{client} which
   is used by a program which wants to migrate, and a \emph{server} which is called
   by a \Index{migration server} after a migrated program has been compiled.  The
   migration runtime is implemented in \tt{arch/x86/runtime/x86\_migrate.c}.
   
   The migration client runtime is responsible for sending the binary \FIR{} code
   and the heap data to the destination machine, or to a state file.  The following
   actions are performed by the migration client\footnote{The steps marked with *
   are omitted when the process state is being written to a file; these steps only
   make sense when there is communication with a \Index{migration server}.}:
   \begin{enumerate}
      \setlength\itemsep{0em}
      \setlength\parskip{0em}
      \item Send binary \FIR{} representation.
      \item Send migration key, \it{id}.
      \item Send register index, \it{env}.
      \item Send information on the pointer table and heap sizes$^*$.
      \item Wait for destination to compile program.
      \item Send pointer table contents and heap contents.
      \item Send global data blocks.
      \item Verify data with server$^*$.
   \end{enumerate}
   
   The \Index{migration server} runtime has a simpler task.  By the time the runtime is
   invoked, the program has already been compiled, therefore it only needs to
   reconstruct the heap data.  Also, the migration key \it{id} and register index
   \it{env} are given as part of the \migratestruct{} data structure.
   \begin{enumerate}
      \setlength\itemsep{0em}
      \setlength\parskip{0em}
      \item Allocate initial pointer table and heap.
      \item Receive and reconstruct pointer table contents and heap contents.
      \item Receive and reconstruct global data blocks.
      \item Verify data with client$^*$.
   \end{enumerate}
   
 \subsubsection{Migration server and resurrection support}
   The core of \migrated{} and \resurrect{} is implemented in
   \tt{net/migrate/migrate.ml}.  This code is responsible for
   parsing the first part of a migration state (either from a
   state file or network connection) which contains the binary
   \FIR{} and \migratestruct{} data structure.  The migration
   server and resurrection programs perform the following steps:
   \begin{enumerate}
      \setlength\itemsep{0em}
      \setlength\parskip{0em}
      \item Read the binary \FIR{} from the channel.
      \item Read and construct parametres for the \migratestruct{} data structure.
      \item Compile the binary \FIR{} to native code, using \migratestruct.
      \item Run the program.  Because the program is compiled with an initialised
            \migratestruct, it should automatically enter the migrate server runtime.
   \end{enumerate}

   Any migrated process will automatically inherit the console of \migrated{}
   or \resurrect.
   


\section{Safety of Migrated Programs}
   Safety is not currently discussed in this paper, but will be added in a later
   revision of this paper.
   


%
%  Print the index
%
\printindex

\end{document}
