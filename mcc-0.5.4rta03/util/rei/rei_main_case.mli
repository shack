(*
   Main program for REI -- case editor loop
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Mc_string_util
open Rei_type


val current_suite       : suite ref
val last_saved_suite    : suite ref
val is_suite_modified   : unit -> bool


val current_stats       : stats option ref


val go_curses           : unit -> bool
val no_curses           : unit -> unit


val run_case_add        : string list -> unit
val run_case_edit       : case -> unit
val run_case_edit_select: case StringTable.t -> string list -> unit
