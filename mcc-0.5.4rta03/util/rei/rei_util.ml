(*
   Utilities for REI
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Rei_suite_exn
open Rei_type


(***  Utilities  ***)


(* appname
   Name of this application.  *)
let appname = "Ayanami REI"


(* use_curses ()
   Returns true if we are allowed to use curses.  To use curses, we MUST
   be in interactive mode, must have been compiled with curses support, and
   must have the curses option set. *)
let use_curses () =
   !Rei_state.interactive && !Rei_state.curses && Mcc_ncurses_display.curses_enabled


(* title_text text
   Format text to be displayed in a title, as in an xterm title.  If the
   terminal is not an xterm, then this returns an empty string.  This will
   NOT format the text to display anything to the console itself, under
   any circumstance.  This may prepend the application name to the text. *)
let title_text = title_text appname


(***  Read-Eval-Print Loop  ***)


(* read_eval_print prompt commands
   Read a single command, evaluate it, and print the results.  This
   processes at most one command for constructing a test case; call
   it from within a loop to accept multiple commands.  *)
let read_eval_print run =
   try
      read_eval_print run
   with
      SuiteException (pos, e) ->
         pp_print_exn std_formatter pos e;
         Continue


(* run_loop prompt commands cmd_quit
   Runs the loop for evaluating commands until the user quits and/or
   presses ^D.  This catches any exceptions that may have been thrown
   by the read-eval-print loop, above.  You must indicate what function
   should be called when ^D is pressed.  *)
let run_loop prompt commands cmd_quit =
   let run =
      { ri_curses       = use_curses ();
        ri_hack_title   = true;
        ri_appname      = appname;
        ri_help_prefix  = "This is Ayanami REI (Regression Evaluation Interface)";
        ri_prompt       = (fun () -> prompt);
        ri_commands     = commands;
        ri_cmd_quit     = cmd_quit;
        ri_arg_filter   = space_filter;
      }
   in
      run_loop read_eval_print run


(* yes_no_cancel prompt default
   Presents a yes/no/cancel prompt (^D maps to ``cancel'') for the user
   to respond to.  Returns None if the user cancels, otherwise returns
   Some true if the user responds yes and Some false if the user responds
   no.  *)
let yes_no_cancel prompt default =
   yes_no_cancel appname true prompt (Some default)


(* yes_no prompt default
   Similar to yes_no_cancel, but ``cancel'' gets mapped to ``no''.  *)
let yes_no prompt default =
   yes_no appname true prompt default


(* ok_to_overwrite_file filename
   If the named file exists, this puts up a prompt if it is okay to
   overwrite the named file, which defaults to yes.  No prompt is
   displayed if the file doesn't exist.  This returns true if either
   the file doesn't exist, or the user says it is okay to overwrite.  *)
let ok_to_overwrite_file filename =
   ok_to_overwrite_file appname true filename


(***  Printers for Various Data Types  ***)


(* pp_print_time_seconds buf time
   Print out a delta time (seconds only) in a semi-readable form.  *)
let pp_print_time_seconds buf time =
   let sec = Int32.of_float time in
   let usec = Int32.of_float (mod_float (time *. 1000.) 1000.) in
      fprintf buf "%ld.%03ld" sec usec


(* string_of_time_seconds time
   Returns a string containing a delta time (seconds only).  *)
let string_of_time_seconds time =
   let buf = Buffer.create 32 in
   let fmt = formatter_of_buffer buf in
      pp_print_time_seconds fmt time;
      pp_print_flush fmt ();
      Buffer.contents buf


(* pp_print_time buf time
   Print out a time (minutes:seconds) in a semi-readable form.  *)
let pp_print_time buf time =
   let sec = int_of_float time in
   let usec = (int_of_float (time *. 1000.)) mod 1000 in
   let mins = sec / 60 in
   let sec = sec mod 60 in
      fprintf buf "%d:%02d.%03d" mins sec usec


(* string_of_time time
   Returns a string containing a time (minutes:seconds).  *)
let string_of_time time =
   let buf = Buffer.create 32 in
   let fmt = formatter_of_buffer buf in
      pp_print_time fmt time;
      pp_print_flush fmt ();
      Buffer.contents buf


(* pp_print_time_short buf time
   Print out a time (minutes:seconds) in a semi-readable form.  *)
let pp_print_time_short buf time =
   let sec = int_of_float time in
   let mins = sec / 60 in
   let sec = sec mod 60 in
      fprintf buf "%d:%02d" mins sec


(* string_of_time_short time
   Returns a string containing a time (minutes:seconds).  *)
let string_of_time_short time =
   let buf = Buffer.create 32 in
   let fmt = formatter_of_buffer buf in
      pp_print_time_short fmt time;
      pp_print_flush fmt ();
      Buffer.contents buf


(* pp_print_string_op buf op
   Prints out a string operation.  *)
let pp_print_string_value buf op =
   match op with
      StrTarget ->
         fprintf buf "$target"
    | StrValue s ->
         fprintf buf "\"%s\"" (String.escaped s)

let rec pp_print_string_op buf op =
   match op with
      [v] ->
         pp_print_string_value buf v
    | v :: op ->
         pp_print_string_value buf v;
         fprintf buf " ^ ";
         pp_print_string_op buf op
    | [] ->
         fprintf buf "\"\""


(* pp_print_true_false buf b
   Prints true or false, as text.  *)
let pp_print_true_false buf b =
   if b then
      fprintf buf "true"
   else
      fprintf buf "false"
