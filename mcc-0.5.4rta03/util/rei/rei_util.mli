(*
   Utilities for REI
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Rei_type


(***  Utilities  ***)


(* use_curses ()
   Returns true if we are allowed to use curses.  To use curses, we MUST
   be in interactive mode, must have been compiled with curses support, and
   must have the curses option set. *)
val use_curses : unit -> bool


(* title_text text
   Format text to be displayed in a title, as in an xterm title.  If the
   terminal is not an xterm, then this returns an empty string.  This will
   NOT format the text to display anything to the console itself, under
   any circumstance.  This may prepend the application name to the text. *)
val title_text : string -> string


(***  Read-Eval-Print Loop  ***)


(* run_loop prompt commands cmd_quit
   Runs the loop for evaluating commands until the user quits and/or
   presses ^D.  This catches any exceptions that may have been thrown
   by the read-eval-print loop, above.  You must indicate what function
   should be called when ^D is pressed.  *)
val run_loop : string -> (string * (string list, 'result) run_command * string) list -> 
               (string list, 'result) run_command -> 'result


(***  Specialised Prompts  ***)


(* yes_no_cancel prompt default
   Presents a yes/no/cancel prompt (^D maps to ``cancel'') for the user
   to respond to.  Returns None if the user cancels, otherwise returns
   Some true if the user responds yes and Some false if the user responds
   no.  This function will check to see if the ``-yes'' flag was given to
   the program, which bypasses this prompt and always assumes a ``yes''
   response.  *)
val yes_no_cancel : string -> bool -> bool option


(* yes_no prompt default
   Similar to yes_no_cancel, but ``cancel'' gets mapped to ``no''.  *)
val yes_no : string -> bool -> bool


(* ok_to_overwrite_file filename
   If the named file exists, this puts up a prompt if it is okay to
   overwrite the named file, which defaults to yes.  No prompt is
   displayed if the file doesn't exist.  This returns true if either
   the file doesn't exist, or the user says it is okay to overwrite.  *)
val ok_to_overwrite_file : string -> bool


(***  Printers for Various Data Types  ***)


(* pp_print_time_seconds buf time
   Print out a delta time (seconds only) in a semi-readable form.  *)
val pp_print_time_seconds : formatter -> float -> unit


(* string_of_time_seconds time
   Returns a string containing a delta time (seconds only).  *)
val string_of_time_seconds : float -> string


(* pp_print_time buf time
   Print out a time (minutes:seconds) in a semi-readable form.  *)
val pp_print_time : formatter -> float -> unit


(* string_of_time time
   Returns a string containing a time (minutes:seconds).  *)
val string_of_time : float -> string


(* pp_print_time_short buf time
   Print out a time (minutes:seconds) in a semi-readable form.  *)
val pp_print_time_short : formatter -> float -> unit


(* string_of_time_short time
   Returns a string containing a time (minutes:seconds).  *)
val string_of_time_short : float -> string


(* pp_print_string_op buf op
   Prints out a string operation.  *)
val pp_print_string_op : formatter -> string_op list -> unit


(* pp_print_true_false buf b
   Prints true or false, as text.  *)
val pp_print_true_false : formatter -> bool -> unit
