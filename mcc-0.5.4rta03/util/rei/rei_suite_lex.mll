(*
   Parse a test suite file
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(***  Header  ***)


{
   open Symbol
   open Rei_suite_exn
   open Rei_suite_parse


   (* Reference cells containing the file position. *)
   let current_line = ref 1
   let current_schar = ref 0


   (* reset_pos
      Resets the current position. *)
   let reset_pos () =
      current_line := 1;
      current_schar := 0


   (* set_next_line : lexbuf -> unit
      Advance by a single line. *)
   let set_next_line lexbuf =
      incr current_line;
      current_schar := Lexing.lexeme_end lexbuf


   (* set_lexeme_position : lexbuf -> pos
      Get the position of the current lexeme. We assume the entire
      lexeme is all on one line.  Brave assumption, that.  *)
   let set_lexeme_position lexbuf =
      let line = !current_line in
      let schar = Lexing.lexeme_start lexbuf - !current_schar in
      let echar = Lexing.lexeme_end lexbuf - !current_schar in
      let file = !Rei_state.current_file in
      let pos = file, line, schar, line, echar in
         Rei_state.current_pos := pos;
         pos


   (* As we are parsing, we will build up a stack of lexers as we enter
      nested comments and OCaml block code.  A stack is maintained here
      indicating the position of the beginning of the block, as well as
      a buffer for that block designed to store temporary text.  The
      buffer can be used to obtain the full text constructed by the nested
      lexer. *)
   let lexer_stack = Stack.create ()
   let lexer_buffer = Buffer.create 256

   let push_lexer lexbuf =
      let pos = set_lexeme_position lexbuf in
         Stack.push pos lexer_stack

   let push_lexer_init_buffer lexbuf =
      push_lexer lexbuf;
      Buffer.clear lexer_buffer

   let pop_lexer () =
      Stack.pop lexer_stack

   let pop_lexer_and_buffer () =
      let pos = pop_lexer () in
      let buf = Buffer.contents lexer_buffer in
         pos, buf

   let run_lexer f lexbuf =
      push_lexer lexbuf;
      f lexbuf;
      let _ = pop_lexer () in
         ()

   let buffer_append_string s =
      Buffer.add_string lexer_buffer s

   let buffer_append_lexbuf lexbuf =
      buffer_append_string (Lexing.lexeme lexbuf)
} (* End header *)


(***  Regexp's  ***)


(* Basic structures *)
let whitespace    = ['\t' ' ']+


(* Language tokens *)
let identifier    = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*
let decimal       = '-'? ['0'-'9']+
let float         = '-'? ['0'-'9']+ '.' ['0'-'9']*


(***  Lexing Rules  ***)


(* The suite rule is invoked for normal lexing of a test suite. *)
rule suite = parse
   whitespace { suite lexbuf }
 | '\n'       { set_next_line lexbuf; suite lexbuf }
 | "/*"       { run_lexer comment_c lexbuf; suite lexbuf }
 | "(*"       { run_lexer comment_ml lexbuf; suite lexbuf }
 | identifier {
      let pos = set_lexeme_position lexbuf in
      let ident = Lexing.lexeme lexbuf in
         match ident with
            "build"     -> TokBuild pos
          | "case"      -> TokCase pos
          | "disable"   -> TokDisable pos
          | "enabled"   -> TokEnabled pos
          | "eval"      -> TokEval pos
          | "failure"   -> TokFailure pos
          | "false"     -> TokFalse pos
          | "log"       -> TokLog pos
          | "name"      -> TokName pos
          | "output"    -> TokOutput pos
          | "profile"   -> TokProfile pos
          | "return"    -> TokReturn pos
          | "start"     -> TokStart pos
          | "stats"     -> TokStats pos
          | "stop"      -> TokStop pos
          | "success"   -> TokSuccess pos
          | "suite"     -> TokSuite pos
          | "target"    -> TokTarget pos
          | "total"     -> TokTotal pos
          | "true"      -> TokTrue pos
          | _           -> TokIdent (Symbol.add (Lexing.lexeme lexbuf), pos)
   }
 | float {
      let pos = set_lexeme_position lexbuf in
      let value = float_of_string (Lexing.lexeme lexbuf) in
         TokFloat (value, pos)
   }
 | decimal {
      let pos = set_lexeme_position lexbuf in
      let value = int_of_string (Lexing.lexeme lexbuf) in
         TokInt (value, pos)
   }
 | '"' {
      push_lexer_init_buffer lexbuf;
      let pos2 = string lexbuf in
      let pos1, text = pop_lexer_and_buffer () in
      let pos = union_pos pos1 pos2 in
         TokString (text, pos)
   }
 | ['^' ':' ',' '$' '=' '[' ']' ';'] {
      let pos = set_lexeme_position lexbuf in
      let symbol = Lexing.lexeme lexbuf in
         match symbol with
            "^"         -> TokCaret pos
          | ":"         -> TokColon pos
          | ","         -> TokComma pos
          | "$"         -> TokDollar pos
          | "="         -> TokEqual pos
          | "["         -> TokLBrack pos
          | "]"         -> TokRBrack pos
          | ";"         -> TokSemi pos
          | _           -> raise (SuiteException (exp_pos pos, StringError ("illegal symbol: " ^ String.escaped symbol)))
   }
 | _ {
      let pos = set_lexeme_position lexbuf in
      let s = Lexing.lexeme lexbuf in
         raise (SuiteException (exp_pos pos, StringError ("illegal character: " ^ (String.escaped s))))
   }
 | eof {
      let pos = set_lexeme_position lexbuf in
         TokEof pos
   }


(***  Comment Lexers  ***)


and comment_c = parse
   '\n' { set_next_line lexbuf; comment_c lexbuf }
 | "/*" { run_lexer comment_c lexbuf; comment_c lexbuf }
 | "*/" { () }
 | _    { comment_c lexbuf }
 | eof  {
      let pos = pop_lexer () in
         raise (SuiteException (exp_pos pos, StringError "comment unterminated"))
   }


and comment_ml = parse
   '\n' { set_next_line lexbuf; comment_ml lexbuf }
 | "(*" { run_lexer comment_ml lexbuf; comment_ml lexbuf }
 | "*)" { () }
 | _    { comment_ml lexbuf }
 | eof  {
      let pos = pop_lexer () in
         raise (SuiteException (exp_pos pos, StringError "comment unterminated"))
   }


(***  String Lexer  ***)


and string = parse
   '\n' { buffer_append_lexbuf lexbuf; set_next_line lexbuf; string lexbuf }
 | '"'  { set_lexeme_position lexbuf }
 | '\\' ['0'-'9'] ['0'-'9'] ['0'-'9'] {
      let s = Lexing.lexeme lexbuf in
      let s = String.sub s 1 3 in
      let i = int_of_string s in
         if i < 256 then begin
            buffer_append_string (String.make 1 (char_of_int i));
            string lexbuf
         end else
            raise (SuiteException (exp_pos (pop_lexer ()), StringError "escape value out of bounds"))
   }
 | '\\' _ {
      let s = Lexing.lexeme lexbuf in
      let c =
         match s.[1] with
            'b' -> '\b'
          | 'n' -> '\n'
          | 'r' -> '\r'
          | 't' -> '\t'
          |  c  ->  c
      in
         buffer_append_string (String.make 1 c); string lexbuf }
 | [^ '"' '\n' '\\']+ {
      buffer_append_lexbuf lexbuf; string lexbuf
   }
 | eof {
      let pos = pop_lexer () in
         raise (SuiteException (exp_pos pos, StringError "string unterminated"))
   }
