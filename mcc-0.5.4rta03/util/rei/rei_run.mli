(*
   Run a test case or sequence of test cases
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Rei_type


(***  Evaluation Utilities  ***)


(* eval_string case op
   Evaluates a string op.  This pulls values from the cases structure, as
   needed.  Returns a string suitable for evaluation with system().  *)
val eval_string : case -> string_op list -> string


(***  Initial Test Case Run  ***)


(* initial_eval_case buf case
   This is an interface function that evaluates a test case, assuming the
   return coede and output have not yet been captured.  This populates the
   build code, return code, and output fields of the test case, assuming
   the test run does not end in error.  It assumes the target/eval fields
   are already set correctly.  If successful, then the boolean value true
   is also returned.

   If the build succeeds, then the eval command is run and case_return/
   case_output are set to the results of the evaluation.  If the build
   fails, then build_code is set to the result of the build, and the
   case_return/case_output fields are cleared.  Note, initial_eval_case
   considers build failures to be ``successful'', so it is important to
   check that a build failure was actually expected (initial_eval_case
   cannot distinguish test cases which are deliberate compilation errors
   used to test type checking, e.g.).

   If a fatal error occurs, a failure message is printed to the supplied
   buf, and this function returns the pair (false, case) for the case given
   as input (unaltered). Note that this call will build the test target for
   you automatically.  *)
val initial_eval_case : formatter -> case -> bool * case


(***  Run Test Cases with Expected Results  ***)


(* status_fun
   Type of the status monitor function.  Takes a format channel and a
   message to set the status to.  *)
type status_fun = formatter -> string -> unit


(* run_and_check_case buf f stats case
   This is an interface function.  This will run the indicated test case,
   and will report any errors to the buffer given.  This call will update
   the stats structure passed in, and will return the modified stats as well
   as a boolean flag indicating whether the test run was successful.  This
   function catches exceptions and prints any errors to the buffer given
   (which may be a Y-formatter).

   This call will NOT prebuild the test case.  It is expected that all test
   cases were prebuilt.  Furthermore, this function should not be called if
   you actually expected the test case to fail.  Call run_and_check_cases if
   you want build-failure test cases to be properly handled.

   f is the status-update function; if not None, then this function is
   called whenever the runner wishes to update the status line (possibly
   the title bar of the application).  Set it to None for no status update.  *)
val run_and_check_case : formatter -> status_fun option -> stats -> case -> bool * stats


(* run_and_check_cases buf f stats cases
   Run and check a list of test cases.  This function will prebuild the test
   cases automatically.  This returns a boolean value that indicates whether
   runs were successful, as well as the statistics collected for this test
   run.  This function WILL prebuild all the test cases.  The cases list can
   include cases which are expected to fail.

   f is the status-update function; if not None, then this function is
   called whenever the runner wishes to update the status line (possibly
   the title bar of the application).  Set it to None for no status update. *)
val run_and_check_cases : formatter -> status_fun option -> stats -> case list -> bool * stats
