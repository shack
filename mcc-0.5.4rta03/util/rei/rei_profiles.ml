(*
   Process test profiles
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Mc_string_util


(***  Profile Editing  ***)


(* profile_add profiles name cases
   Adds a completely new profile with the indicated list of test cases as
   the initial test profile.  Existing bindings for name will be removed. *)
let profile_add profiles name cases =
   let set = List.fold_left StringSet.add StringSet.empty cases in
      StringTable.add profiles name set


(* profile_add_cases profiles name cases
   Appends test cases to an existing profile in the set of profiles.  If
   name is not yet bound, this is equivalent to profile_add.  *)
let profile_add_cases profiles name cases =
   let set =
      try
         StringTable.find profiles name
      with
         Not_found ->
            StringSet.empty
   in
   let set = List.fold_left StringSet.add set cases in
      StringTable.add profiles name set


(* profile_delete profiles name
   Deletes the named profile.  *)
let profile_delete profiles name =
   StringTable.remove profiles name


(* profile_delete_cases profiles name cases
   Deletes the named cases from the indicated test profile.  This does NOT
   remove the profile itself; any cases in the set difference of the current
   profile binding, and cases, will remain in the test profile.  If name is
   not bound, then this has no effect.  *)
let profile_delete_cases profiles name cases =
   try
      let set = StringTable.find profiles name in
      let set = List.fold_left (fun set name ->
         try
            StringSet.remove set name
         with
            Not_found ->
               set) set cases
      in
         StringTable.add profiles name set
   with
      Not_found ->
         profiles


(***  Print Test Profiles  ***)


(* pp_print_profile buf name cases
   Print out a single test profile.  *)
let pp_print_profile buf name cases =
   fprintf buf "@[<hv 3>profile \"%s\" =@ [@[<v 0>" (String.escaped name);
   ignore (StringSet.fold (fun first name ->
      if not first then
         fprintf buf ";@ ";
      fprintf buf "\"%s\"" (String.escaped name);
      false) true cases);
   fprintf buf "@]];@]"


(* display_profiles_against_contrast buf display_profiles contrast_profiles
   Displays a summary of test profiles.  Profiles which have changed
   will be displayed in bold, if possible.  *)
let display_profiles_against_contrast buf display_profiles contrast_profiles =
   fprintf buf "@[<v 3>Current test profiles:";
   ignore (StringTable.fold (fun count name profile ->
      let changed =
         try
            profile <> StringTable.find contrast_profiles name
         with
            Not_found ->
               true
      in
      let line = sprintf "#%03d \"%s\" (%d cases)" count name (StringSet.cardinal profile) in
      let line =
         if changed then
            bold_text line
         else
            line
      in
         fprintf buf "@ %s" line;
         count + 1) Rei_state.first_case_number display_profiles);
   fprintf buf "@]@."


(* display_profiles buf profiles
   Displays a summary of test profiles.  *)
let display_profiles buf profiles =
   display_profiles_against_contrast buf profiles profiles
