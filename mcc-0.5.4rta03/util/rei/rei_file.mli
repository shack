(*
   File utilities for capturing output and simple file I/O
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(***  Clearing Temporary Files  ***)


(* remove_temporary_files list
   Remove temporary files; this catches any Unix_error that may be thrown,
   and ignores the error; since this is almost always on an exit/cleanup
   path, this is reasonable.  Be aware though that this function will fail
   silently, unless you check the return code.  The number of cases that
   were successfully deleted is returned.  *)
val remove_temporary_files : string list -> int


(***  Output Capture  ***)


(* redirect_output stdout_name stderr_name
   Redirects stdout and stderr to the named file(s).  If the filenames match
   then only one descriptor is opened for writing, and the same descriptor is
   duplicated to both stdout and stderr.  Otherwise, separate files are made
   to capture each output stream independently.  If something goes wrong,
   this will raise Unix_error.  Note that the file descriptors opened will
   not be closed (except implicitly when the process terminates).

   Note: the heuristic for determining if the filenames are the same is a
   simple string comparison;  so it will miss things like "/foo" vs. "//foo".
   If you intend to only use one descriptor, you should make sure you give
   the same string for both arguments.  *)
val redirect_output : string -> string -> unit


(***  Simple File I/O  ***)


(* read_file filename
   Read the entire contents of a file into a string buffer.  If an
   exception occurs, then this will raise a Unix error.  *)
val read_file : string -> string


(* write_file filename buffer
   Write a string buffer out to a file.  If an exception occurs, then
   this will raise a Unix error.  *)
val write_file : string -> string -> unit


(***  Filename Utilities  ***)


(* trim_extension filename
   Removes the extension component from a filename, if present.
   The returned string is the original string if the final file
   component does not contain a dot.  *)
val trim_extension : string -> string
