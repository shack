(*
   Main program for REI -- case editor loop
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Mc_string_util
open Rei_case
open Rei_file
open Rei_run
open Rei_suite
open Rei_type
open Rei_util


(***  Main Program State  ***)


(* current_case
   last_saved_case
   The contents of the test case currently being manipulated, and the
   results of its last save.  *)
let current_case    = ref (new_case "undefined")
let last_saved_case = ref !current_case


(* is_case_modified
   Returns true if current_case does not agree with last_saved_case.  *)
let is_case_modified () =
   !current_case <> !last_saved_case


(* original_case_name
   If None, then we are creating a new test case.  Otherwise, this is
   set to the real name of the test case that was used to derive this
   case.  If we save this case with a new name, then the original case
   should be deleted.  *)
let original_case_name = ref None


(* current_suite
   last_saved_suite
   Contains the contents of the current test suite being manipulated
   by REI.  This defaults to an empty test suite; use the read command
   to load a saved suite.  last_saved_suite contains the results of
   the last successful save of the test suite.  *)
let current_suite    = ref (new_suite "default")
let last_saved_suite = ref !current_suite


(* is_case_modified
   Returns true if current_suite does not agree with last_saved_suite.  *)
let is_suite_modified () =
   !current_suite <> !last_saved_suite


(* current_stats
   Contains the contents of the current run stats.  May be set to None,
   which indicates no test runs have been performed (yet).  *)
let current_stats = ref None


(***  Helper Functions  ***)


(* can_overwrite_case_in_suite
   Returns true if a case in the suite with the same name as
   current_case may be overwritten.  If no case exists in the
   suite matching (!current_case).case_name, then this function
   will return true.  *)
let can_overwrite_case_in_suite () =
   let case = !current_case in
   let name = case.case_name in
   let suite = !current_suite in
   let cases = suite.suite_cases in
      (* This should not use lookup_case, because we want to check that the
         EXACT NAME exists; we do not want any translation on the name here. *)
      if not (StringTable.mem cases name) then
         true
      else if yes_no (sprintf "A case with name \"%s\" is already defined. Overwrite" name) false then begin
         printf "Will overwrite existing case.@.";
         true
      end else begin
         printf "Will not overwrite. Use \"name\" to rename this case.@.";
         false
      end


(* go_curses ()
   Returns true if curses interfaces are enabled.  *)
let go_curses () = use_curses ()


(* no_curses ()
   Used to display a message that curses support is missing.  *)
let no_curses () =
   printf "This feature requires curses support, which is not currently enabled.@.";
   printf "(hint: did you compile with ncurses support? Have you tried \"-curses\"?)@."


(***  Case Commands  ***)


let cmd_args args =
   let args = reconstruct_arguments args in
   let args =
      if args = "" then
         ""
      else
         " " ^ args
   in
      printf "Eval on case \"%s\" set to \"$target%s\"@." (!current_case).case_name args;
      current_case := { !current_case with case_eval = [StrTarget; StrValue args] }

let cmd_args_help =
   "[<argument> ...]" ^
   "\nSets the evaluation string for the test case. The evaluation string" ^
   " is the command that will actually be run when regression is ready to" ^
   " evaluate the program. The program that is run will be the target; you" ^
   " only specify the arguments here. If you want to specify the program" ^
   " name as well, use \"eval\"."


let cmd_build = function
   [value] ->
      (try
         let value = int_of_string value in
            printf "Build code on case \"%s\" set to %d@." (!current_case).case_name value;
            current_case := { !current_case with case_build = value }
      with
         Invalid_argument _
       | Failure _ ->
            printf "Argument is not a valid integer.")
 | _ ->
      bad_arguments ()

let cmd_build_help =
   "<integer>" ^
   "\nSets the expected build code for this test."


let cmd_cancel args =
   if args <> [] then begin
      bad_arguments ();
      Continue
   end else if not (is_case_modified ()) then begin
      printf "No modifications to discard@.";
      Exit ()
   end else if yes_no (sprintf "Case \"%s\" has been modified. Cancel anyway" (!current_case).case_name) true then begin
      printf "Discarding modifications@.";
      Exit ()
   end else
      Continue

let cmd_cancel_help =
   "\nCancel unsaved modifications to the test case. Returns to main prompt."


let cmd_disable args =
   let args = reconstruct_arguments args in
      if args = "" then
         printf "Must specify a nonempty reason for disabling the test case.@."
      else begin
         printf "Disabling case \"%s\" (reason: \"%s\")@." (!current_case).case_name args;
         current_case := { !current_case with case_disable = args }
      end

let cmd_disable_help =
   "<reason> ..." ^
   "\nDisables this test case (the test case will not be included in test runs)." ^
   " You must specify a reason for disabling the case (this can be arbitrary text)."


let cmd_enable args =
   if args <> [] then
      bad_arguments ()
   else begin
      printf "Enabling case \"%s\"@." (!current_case).case_name;
      current_case := { !current_case with case_disable = "" }
   end

let cmd_enable_help =
   "\nEnables this test case (the test case will be included in test runs)."


let cmd_eval args =
   let args = reconstruct_arguments args in
      printf "Eval on case \"%s\" set to \"%s\"@." (!current_case).case_name args;
      current_case := { !current_case with case_eval = [StrValue args] }

let cmd_eval_help =
   "<program_name> [<argument> ...]" ^
   "\nSets the evaluation string for the test case. The evaluation string" ^
   " is the command that will actually be run when regression is ready to" ^
   " evaluate the program. Usually, program_name will match the target," ^
   " but not always... If you want the program name to always match the" ^
   " build target, use \"args\" instead."


let cmd_list = function
   [] ->
      display_case_against_contrast std_formatter !current_case !last_saved_case ""
 | [arg] ->
      display_case_against_contrast std_formatter !current_case !last_saved_case arg
 | _ ->
      bad_arguments ()

let cmd_list_help =
   "[-all]" ^
   "\nDisplays current information on the test case being modified. If" ^
   " the \"-all\" keyword is given, then all information (including the" ^
   " expected program output) is displayed. Otherwise, the expected" ^
   " program output will be omitted."


let cmd_name = function
   [name] ->
      printf "Case name \"%s\" changed to \"%s\"@." (!current_case).case_name name;
      current_case := { !current_case with case_name = name }
 | _ ->
      bad_arguments ()

let cmd_name_help =
   "<name>" ^
   "\nChanges the name of this test case."


let cmd_output_dump = function
   [name] ->
      if ok_to_overwrite_file name then begin
         printf "Dumping output to file \"%s\"@." name;
         write_file name (!current_case).case_output
      end
 | [] ->
      if go_curses () then
         Mcc_ncurses_display.in_display (fun () ->
            Mcc_ncurses_display.text_viewer "Output Dump" (!current_case).case_output) ()
      else
         no_curses ()
 | _ ->
      bad_arguments ()

let cmd_output_dump_help =
   "[<filename>]" ^
   "\nDumps the output for this test case to the named file. If the filename" ^
   " is omitted and curses is enabled, then the output is displayed in a viewer" ^
   " instead."


let cmd_output_load = function
   [name] ->
      printf "Loading output from file \"%s\"@." name;
      current_case := { !current_case with case_output = read_file name }
 | _ ->
      bad_arguments ()

let cmd_output_load_help =
   "<filename>" ^
   "\nReads the output for this test case in from the specified file."


let cmd_return = function
   [value] ->
      (try
         let value = int_of_string value in
            printf "Return code on case \"%s\" set to %d@." (!current_case).case_name value;
            current_case := { !current_case with case_return = value }
      with
         Invalid_argument _
       | Failure _ ->
            printf "Argument is not a valid integer.")
 | _ ->
      bad_arguments ()

let cmd_return_help =
   "<integer>" ^
   "\nSets the expected return code for this test."


let cmd_run = function
   ["-gcc"] ->
      (* Force build using GCC *)
      printf "Cannot force GCC build yet, sorry.@."
 | [] ->
      (* Traditional build using MCC *)
      let name = (!current_case).case_name in
      let () = printf "Recapturing output for test case \"%s\".@." name in
      let success, case = initial_eval_case std_formatter !current_case in
         if not success then
            printf "Test case could not be run, no changes to current results.@."
         else if case = !current_case then
            printf "No differences between expected results and evaluated results.@."
         else begin
            printf "@.This is the output I got from the test run just now:@.";
            display_case_against_contrast std_formatter case !current_case "-all";
            if yes_no "Evaluation disagrees with expected results. Replace current expected results" true then begin
               printf "Discarding current expected results and modifying test case.@.";
               current_case := case;
            end else
               printf "Discrepancy detected, but I kept the current expected results.@."
         end
 | _ ->
      bad_arguments ()

let cmd_run args =
   (* We are expected to catch ^C whenever we escape into build/eval mode.
      Since the test suite won't do it for us in this case, we have to do
      it ourself. *)
   try
      cmd_run args
   with
      Sys.Break ->
         bad_arguments ()

let cmd_run_help =
   "[-gcc]" ^
   "\nRuns and recaptures the program's exit code and output. This should" ^
   " be used to set the return code and output if the current data is not" ^
   " valid. Make sure the eval and target strings are correct before you" ^
   " run this; if the build or run fail, then the case is left unmodified." ^
   " If the \"-gcc\" option is given, then REI will force the test case be" ^
   " be built with GCC (needed when MCC produces known-wrong results)." ^
   "\n\nThis command can also be used to check the current output and return" ^
   " code saved for this test case. If the output agrees with the expected" ^
   " output, then no action is taken; otherwise, you will be asked whether" ^
   " you want to update the test case information or preserve the current" ^
   " values for the output and return code."


let cmd_save_add () =
   let case = !current_case in
   let name = case.case_name in
   let suite = !current_suite in
   let cases = suite.suite_cases in
      if can_overwrite_case_in_suite () then begin
         (* We can save the new case *)
         printf "Saving case \"%s\"@." name;
         let cases = case_add cases name case in
         let suite = { suite with suite_cases = cases } in
            current_suite := suite;
            Exit ()
      end else
         (* Not ready to terminate yet *)
         Continue


let cmd_save_edit original_name =
   let case = !current_case in
   let name = case.case_name in
   let suite = !current_suite in
   let cases = suite.suite_cases in
      if name = original_name then begin
         (* We are modifying a test case in-place. No prompt necessary. *)
         printf "Modifying case \"%s\"@." name;
         let cases = case_add cases name case in
         let suite = { suite with suite_cases = cases } in
            current_suite := suite;
            Exit ()
      end else if can_overwrite_case_in_suite () then begin
         (* The prompt was required because the test case name was changed.
            However, the user apparently accepted the changes.  We need to
            delete the original case, as well as add the nice new one.  *)
         printf "Saving case \"%s\" and deleting old case \"%s\"@." name original_name;
         let cases = case_delete cases original_name in
         let cases = case_add cases name case in
         let suite = { suite with suite_cases = cases } in
            current_suite := suite;
            Exit ()
      end else
         (* Not ready to terminate yet *)
         Continue


let cmd_save = function
   [] ->
      (match !original_case_name with
         None ->
            (* We are adding a new test case *)
            cmd_save_add ()
       | Some orig ->
            (* We are editing an existing test case *)
            cmd_save_edit orig)
 | _ ->
      bad_arguments ();
      Continue

let cmd_save_help =
   "\nSaves this test case. If adding a new test case, you will be warned" ^
   " if the test case name conflicts with another test case already in the" ^
   " suite. If modifying an existing test case, you will not receive this" ^
   " warning unless you explicitly changed the name of the test case, and" ^
   " the new name conflicts with an existing test case. The modifications" ^
   " are saved to the test suite and you are returned to the main prompt."


let cmd_target = function
   [arg] ->
      printf "Target on case \"%s\" set to \"%s\"@." (!current_case).case_name arg;
      current_case := { !current_case with case_target = arg }
 | _ ->
      bad_arguments ()

let cmd_target_help =
   "<target_name>" ^
   "\nChanges the name of the build target. This must be a build target" ^
   " that is recognized by cons. Multiple test cases may use the same" ^
   " build target (they will be distinguished by the test case name)."


let commands =
   ["args",          continue cmd_args,         cmd_args_help;
    "build",         continue cmd_build,        cmd_build_help;
    "cancel",        cmd_cancel,                cmd_cancel_help;
    "disable",       continue cmd_disable,      cmd_disable_help;
    "enable",        continue cmd_enable,       cmd_enable_help;
    "eval",          continue cmd_eval,         cmd_eval_help;
    "list",          continue cmd_list,         cmd_list_help;
    "ls",            continue cmd_list,         cmd_list_help;
    "name",          continue cmd_name,         cmd_name_help;
    "output_dump",   continue cmd_output_dump,  cmd_output_dump_help;
    "output_load",   continue cmd_output_load,  cmd_output_load_help;
    "return",        continue cmd_return,       cmd_return_help;
    "run",           continue cmd_run,          cmd_run_help;
    "save",          cmd_save,                  cmd_save_help;
    "target",        continue cmd_target,       cmd_target_help]


(* run_case_add
   Runs the loop for building a test case until the user quits and/or
   presses ^D.  This catches any exceptions that may have been thrown
   by the read-eval-print loop, above.  This should be called when
   creating a NEW test case; the argument will be parsed as an eval
   line for the test case, and will create a blank test case if it is
   empty.  *)
let run_case_add args =
   original_case_name := None;
   (match get_next_argument args with
      "", _ ->
         current_case := new_case "unnamed";
         last_saved_case := !current_case
    | target, arguments ->
         let arguments = reconstruct_arguments arguments in
         let eval =
            if arguments = "" then
               [StrTarget]
            else
               [StrTarget; StrValue (" " ^ arguments)]
         in
         let case = new_case target in
         let case =
            { case with
              case_target = target;
              case_eval   = eval;
            }
         in
            last_saved_case := case;
            try
               let _, case = initial_eval_case std_formatter case in
                  current_case := case
            with
               Sys.Break ->
                  printf "@.Initial test run interrupted.@.";
                  current_case := case);
   ignore (cmd_list ["-all"]);
   run_loop "add case> " commands cmd_cancel


(* run_case_edit
   Runs the loop for building a test case until the user quits and/or
   presses ^D.  This catches any exceptions that may have been thrown
   by the read-eval-print loop, above.  This should be called when
   editing an existing test case; it will not warn when replacing the
   test case, UNLESS the user changes the name of the test case and
   the new name conflicts with an existing test case.  If the user
   changes the test case name, then the original test case WILL BE
   DELETED.  *)
let run_case_edit case =
   current_case := case;
   last_saved_case := case;
   original_case_name := Some (case.case_name);
   ignore (cmd_list ["-all"]);
   run_loop "edit case> " commands cmd_cancel


(* run_case_edit_select cases args
   Interface to run_case_edit.  This parses args, selecting a case to
   edit from among the list in cases.  If no arg is given, attempt to
   display a menu to allow the user to select a test case.  Only the
   test cases in cases are considered for editing, and all numbering
   is relative to these cases.  *)
let rec run_case_edit_select cases = function
   [arg] ->
      begin
      match lookup_single_case cases arg with
         None ->
            printf "Test case \"%s\" not found.@." arg
       | Some case ->
            run_case_edit case
      end
 | [] ->
      if go_curses () then begin
         let names = Array.make (StringTable.cardinal cases) (None, "", "") in
         let _ = StringTable.fold (fun n name case ->
            let command = Rei_run.eval_string case case.case_eval in
            let enabled =
               if case_enabled case then
                  ""
               else
                  "(disabled)"
            in
            let descr = sprintf "%s %s" command enabled in
               names.(n) <- None, name, descr;
               n + 1) 0 cases
         in
         let select =
            Mcc_ncurses_display.in_display (fun () ->
               Mcc_ncurses_display.menu_select true "Edit Test Case" names) ()
         in
            match select with
               None ->
                  ()
             | Some i ->
                  let _, name, _ = names.(i) in
                     printf "@.";
                     run_case_edit_select cases [name]
      end else
         no_curses ()
 | _ ->
      bad_arguments ()
