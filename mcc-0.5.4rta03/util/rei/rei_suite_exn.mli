(*
   Exception handler for suite parsing
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Format


(* pos
   Position information.  *)
type pos = string * int * int * int * int


val null_pos : pos


(* exn_loc
   Position/location information for exception handlers.  *)
type exn_loc


(* exn
   The possible exception cases for the suite parsing.  *)
type exn =
   ParseError
 | StringError of string
 | SysError of string
 | UnixError of Unix.error * string * string


(* SuiteException
   Raise an exception from the suite parsing.  *)
exception SuiteException of exn_loc * exn


val vexp_pos : symbol -> exn_loc
val exp_pos : pos -> exn_loc
val string_pos : string -> exn_loc -> exn_loc


val union_pos : pos -> pos -> pos


val pp_print_unix_error : formatter -> (Unix.error * string * string) -> unit
val pp_print_exn : formatter -> exn_loc -> exn -> unit
val print_exn : exn_loc -> exn -> unit
