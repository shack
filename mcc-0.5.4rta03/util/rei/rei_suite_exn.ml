(*
   Exception handler for suite parsing
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Format


(* pos
   Position information.  *)
type pos = string * int * int * int * int


let null_pos = "<unknown>", 0, 0, 0, 0


(* exn_loc
   Position/location information for exception handlers.  *)
type exn_loc =
   StringLoc of string * exn_loc
 | AtomLoc of pos
 | VarLoc of symbol


(* exn
   The possible exception cases for the suite parsing.  *)
type exn =
   ParseError
 | StringError of string
 | SysError of string
 | UnixError of Unix.error * string * string


(* SuiteException
   Raise an exception from the suite parsing.  *)
exception SuiteException of exn_loc * exn


let vexp_pos v = VarLoc v
let exp_pos pos = AtomLoc pos
let string_pos s loc = StringLoc (s, loc)


let min_pos (sline1, schar1) (sline2, schar2) =
   if sline1 < sline2 then
      sline1, schar1
   else if sline1 = sline2 && schar1 < schar2 then
      sline1, schar1
   else
      sline2, schar2


let max_pos (sline1, schar1) (sline2, schar2) =
   if sline1 > sline2 then
      sline1, schar1
   else if sline1 = sline2 && schar1 > schar2 then
      sline1, schar1
   else
      sline2, schar2


let union_pos (file1, sline1, schar1, eline1, echar1)
              (file2, sline2, schar2, eline2, echar2) =
   let file : string = file1 in
   let sline, schar = min_pos (sline1, schar1) (sline2, schar2) in
   let eline, echar = max_pos (eline1, echar1) (eline2, echar2) in
      file, sline, schar, eline, echar


let rec pp_print_unix_error buf (error, f, arg) =
   fprintf buf "%s: %s %s" (Unix.error_message error) f arg


let rec pp_print_exn_loc buf exn_loc =
   match exn_loc with
      StringLoc (s, pos) ->
         fprintf buf "/%s@ " s;
         pp_print_exn_loc buf pos
    | AtomLoc (file, sline, schar, eline, echar) ->
         fprintf buf "%s: %d,%d-%d,%d" file sline schar eline echar
    | VarLoc v ->
         fprintf buf "%s" (string_of_symbol v)


let pp_print_exn buf pos msg =
   pp_print_flush buf ();
   fprintf buf "@[<v 3>*** Suite Exception:@ @[<v 0>";
   pp_print_exn_loc buf pos;
   fprintf buf "@]:@ ";
   (match msg with
      ParseError ->
         fprintf buf "parse error"
    | StringError s ->
         pp_print_string buf s
    | SysError s ->
         pp_print_string buf "system error: ";
         pp_print_string buf s
    | UnixError (error, f, arg) ->
         fprintf buf "unix error: %a" pp_print_unix_error (error, f, arg));
   fprintf buf "@]@."


let print_exn = pp_print_exn err_formatter
