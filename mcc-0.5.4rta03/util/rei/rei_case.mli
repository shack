(*
   Process test cases
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mc_string_util
open Rei_type


(***  Test Case Creation  ***)


(* new_case name
   Constructs a new test case.  *)
val new_case : string -> case


(* case_enabled case
   Returns true if the test case indicated is enabled.  *)
val case_enabled : case -> bool


(***  Test Case Lookup  ***)


(* lookup_case cases name
   lookup_case_prof cases profiles name
   Attempt to lookup cases using the given descriptor.  Returns the test
   cases if matches are found, or empty list if no matching test cases
   could be found.  Priority is always given to a single test case which
   exactly matches the given name.  *)
val lookup_case_prof : case StringTable.t -> profiles -> string -> case list
val lookup_case : case StringTable.t -> string -> case list


(* lookup_single_case cases name
   Forces only one case to match.  If multiple matches are found, then
   None is returned.  *)
val lookup_single_case : case StringTable.t -> string -> case option


(* lookup_case_by_name cases name
   Lookup a case based on a specific name. This does not perform any glob
   pattern matching, and is the preferred form when you need to check for
   an exact name in the case list. *)
val lookup_case_by_name : case StringTable.t -> string -> case option


(***  Adding and Deleting Test Cases  ***)


(* case_add cases name case
   Adds a new case.  This forces the case_name in the case to match the
   name here, therefore you should prefer this function over calling the
   StringTable.add function directly.  The name is not interpreted as a
   glob pattern; this function will overwrite existing keys.  *)
val case_add : case StringTable.t -> string -> case -> case StringTable.t


(* case_delete cases name
   Deletes a case, if it exists in the cases list.  The name here is not
   interpreted as a glob pattern.  The cases list remains unmodified if
   the name is not bound.  *)
val case_delete : case StringTable.t -> string -> case StringTable.t


(***  Case Name Map  ***)


(* map_cases cases names
   map_cases_set cases names
   Maps the case names in the given set to the real case data structures.
   If a case name cannot be found, then a new (empty) case is created for it.
   Otherwise, the name is mapped to the resolved case.  This does not do any
   glob-translation on the names.  *)
val map_cases : case StringTable.t -> 'a StringTable.t -> case StringTable.t
val map_cases_set : case StringTable.t -> StringSet.t -> case StringTable.t


(***  Test Case Printer  ***)


(* display_case buf case arg
   Displays information about a test case.  Similar to the above version
   of pp_print_string, but uses a string argument to specify the printer.  *)
val display_case : formatter -> case -> string -> unit


(* display_case_against_contrast buf display_case contrast_case arg
   Displays information about a test case (the FIRST argument), given the
   contrast case (the SECOND argument).  No information about the second
   case is displayed; however, discrepancies in the second case are used
   to mark lines in the first case which differ.  The string argument has
   similar interpretation as with display_case.  *)
val display_case_against_contrast : formatter -> case -> case -> string -> unit


(* pp_print_case buf case
   This is the interface used by the suite file writer.  It emits the
   full output in a parseable format.  *)
val pp_print_case : formatter -> case -> unit


(***  Display Multiple Test Cases  ***)


(* display_case_summary buf header cases
   Displays the case summary, without using contrasts.  *)
val display_case_summary : formatter -> string -> case StringTable.t -> unit


(* display_case_summary_against_contrast
      buf header display_cases contrast_cases
   Similar to display_case_against_contrast; this takes a list of cases
   to actually list, along with a list of cases to compare against. If
   a display case differs from the corresponding entry in the contrast
   list, *or* if no entry exists for the display case in the contrast
   list, then it is displayed in bold.  *)
val display_case_summary_against_contrast : formatter -> string -> case StringTable.t ->
                                            case StringTable.t -> unit
