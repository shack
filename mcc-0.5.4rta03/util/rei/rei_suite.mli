(*
   Read and write suite files
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Rei_type


(***  Suite Creation / Loading  ***)


(* new_suite name
   Constructs a new test suite with the indicated name.  *)
val new_suite : string -> suite


(* read_suite filename
   Reads in a suite from the named file.  If the suite could not be found
   or parsed, then a SuiteException is thrown.  Otherwise, the contents of
   the test suite are returned.  *)
val read_suite : string -> suite


(***  Suite Printing  ***)


(* write_suite filename suite
   Writes the indicated test suite out to the named file.  If an error
   occurs while writing the file, then a SuiteException is raised.  *)
val write_suite : string -> suite -> unit


(***  Run a Suite  ***)


(* autorun_type
   Indicates whether a suite is being autorun, or manually run.  If it
   is autorun, then the suite run will be mostly silent, and results will
   be compared against old run stats (a previous run of the test suite).
   If it is manually run, then more verbose output is generated, and we
   will not attempt to compare against a previous test run.
      ARAuto old_stats           Auto-run, indicating old run stats.
      ARManual                   Manual run (we don't care about old runs).
 *)
type autorun_type =
   ARAuto   of stats option
 | ARManual


(* run_suite stdout autorun suite cases
   Runs a regression test suite.  The cases that should be run are listed
   in the cases argument.  This command catches exceptions, notably it will
   intercept ^C and interpret it as an aborted test run.  Assuming the test
   run is NOT interrupted, the statistics for the test run are returned;
   the statistics indicate total run time as well as a detail list of which
   test cases failed.  This function returns None if the user interrupts
   the test run with ^C.

   If the autorun flag is ARAuto, then this assumes we want an automated
   test run, in which case nothing is output unless there are differences
   from the test run currently saved in the suite (and if there are diffs,
   we will output a delta report between the existing stats in the test
   suite, and the new stats).  *)
val run_suite : formatter -> autorun_type -> suite -> case list -> stats option
