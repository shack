(*
   Statistical information on test runs in REI
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mc_string_util
open Rei_case
open Rei_suite_exn
open Rei_type
open Rei_util


(***  Test Run Stats Structure  ***)


(* stats_create suite_name cases
   Build a new test run stats structure, using reasonable defaults.
   The stats structure is based on the data in cases, however the
   runs fields are left empty at this time.  This is called before
   a test run, and is further initialised using the following calls. *)
let stats_create suite_name cases =
   let currenttime = Unix.gettimeofday () in
      { stats_suite_name   = suite_name;
        stats_start_time   = currenttime;
        stats_stop_time    = currenttime;
        stats_log          = "";
        stats_current      = Rei_state.first_case_number;
        stats_total        = List.length cases;
        stats_success_runs = StringTable.empty;
        stats_failure_runs = StringTable.empty;
      }


(* stats_update_time stats
   Updates the stop time for the run stats given.  *)
let stats_update_time stats =
   { stats with stats_stop_time = Unix.gettimeofday () }


(* stats_update_log stats log
   Sets the log field for the stats structure to a new string.  *)
let stats_update_log stats log =
   { stats with stats_log = log }


(* stats_add_success stats name time
   Add a case name to the success list, and increment count.  *)
let stats_add_success stats name time =
   let ls = stats.stats_success_runs in
   let cur = stats.stats_current in
      { stats with
        stats_success_runs = StringTable.add ls name time;
        stats_current = cur + 1;
      }


(* stats_add_failure stats name msg
   Add a case name to the failure list, and increment count.  *)
let stats_add_failure stats name msg =
   let ls = stats.stats_failure_runs in
   let cur = stats.stats_current in
      { stats with
        stats_failure_runs = StringTable.add ls name msg;
        stats_current = cur + 1;
      }


(***  Read Stats from File  ***)


(* read_stats filename
   Reads in run stats from the named file.  If the file could not be found
   or parsed, then a SuiteException is thrown.  Otherwise, the contents of
   the stats file are returned.  This will NOT extract stats from a suite
   file; use read_suite for that task.  *)
let read_stats filename =
   Rei_suite_lex.reset_pos ();
   Rei_state.current_file := filename;
   let inx =
      try open_in filename with
         Sys_error s ->
            raise (SuiteException (exp_pos (!Rei_state.current_pos), SysError s))
   in
      try
         let lexbuf = Lexing.from_channel inx in
         let stats = Rei_suite_parse.stats Rei_suite_lex.suite lexbuf in
            close_in inx;
            stats
      with
         Parsing.Parse_error ->
            close_in inx;
            raise (SuiteException (exp_pos (!Rei_state.current_pos), ParseError))
       | Sys_error s ->
            close_in inx;
            raise (SuiteException (exp_pos (!Rei_state.current_pos), SysError s))


(***  Print Stats  ***)


(* pp_print_stats buf stats
   Print out the statistical information to the indicated buffer.
   The output of this function is suitable for re-parsing.  *)
let pp_print_stats buf stats =
   fprintf buf "@[<v 3>stats:";
   fprintf buf "@ name    = \"%s\";" (String.escaped stats.stats_suite_name);
   fprintf buf "@ start   = %a;" pp_print_time_seconds stats.stats_start_time;
   fprintf buf "@ stop    = %a;" pp_print_time_seconds stats.stats_stop_time;
   fprintf buf "@ log     = \"%s\";" (String.escaped stats.stats_log);
   fprintf buf "@ total   = %d;" stats.stats_total;
   fprintf buf "@ success = [@[<v 0>";
   ignore (StringTable.fold (fun first name time ->
      if not first then
         fprintf buf ";@ ";
      fprintf buf "\"%s\", %a" (String.escaped name) pp_print_time_seconds time;
      false) true stats.stats_success_runs);
   fprintf buf "@]];@ failure = [@[<v 0>";
   ignore (StringTable.fold (fun first name msg ->
      if not first then
         fprintf buf ";@ ";
      fprintf buf "\"%s\", \"%s\"" (String.escaped name) (String.escaped msg);
      false) true stats.stats_failure_runs);
   fprintf buf "@]];@]"


(* write_stats filename stats
   Write the indicated run stats out to the named file.  If an error
   occurs while writing the file, then a SuiteException is raised.  *)
let write_stats filename stats =
   let out =
      try open_out filename with
         Sys_error s ->
            raise (SuiteException (exp_pos (filename, 0, 0, 0, 0), SysError s))
   in
      try
         let buf = formatter_of_out_channel out in
            pp_print_stats buf stats;
            fprintf buf "@.";
            close_out out
      with
         Sys_error s ->
            close_out out;
            raise (SuiteException (exp_pos (filename, 0, 0, 0, 0), SysError s))


(***  Fancy Display of Run Stats  ***)


(* display_stats_int buf current_cases contrast_cases stats
   Print out the statistical information in human-readable form.
   This supports optional contrast cases.  *)
let display_stats_int buf current_cases contrast_cases stats =
   let delta_time = stats.stats_stop_time -. stats.stats_start_time in
   let failures = stats.stats_failure_runs in
      fprintf buf "@[<v 0>Test suite took: %a" pp_print_time delta_time;
      fprintf buf "@ Total successes: %d" (StringTable.cardinal stats.stats_success_runs);
      fprintf buf "@ Total failures:  %d@]@." (StringTable.cardinal failures);
      if not (StringTable.is_empty failures) then begin
         match contrast_cases with
            None ->
               display_case_summary buf "Failed test cases" (map_cases current_cases failures)
          | Some contrast_cases ->
               display_case_summary_against_contrast buf "Failed test cases" (map_cases current_cases failures) contrast_cases
      end


(* display_stats buf suite stats
   Print out the statistical information in human-readable form.  *)
let display_stats buf suite stats =
   display_stats_int buf suite.suite_cases None stats


(* display_stats_against_contrast buf display_suite contrast_suite stats
   Similar to above, but when listing failure cases, use a contrast.  *)
let display_stats_against_contrast buf display_suite contrast_suite stats =
   display_stats_int buf display_suite.suite_cases (Some contrast_suite.suite_cases) stats


(* display_delta_stats buf suite current_stats previous_stats
   Display the changes between the current stats and a previous run of
   statistics.  *)
let display_delta_stats buf suite current_stats previous_stats =
   let current_cases = suite.suite_cases in
   let delta_time = current_stats.stats_stop_time -. current_stats.stats_start_time in
   let cur_failures = current_stats.stats_failure_runs in
   let old_failures = previous_stats.stats_failure_runs in
   let new_failures = StringTable.fold (fun new_failures name value ->
      if StringTable.mem old_failures name then
         new_failures
      else
         StringTable.add new_failures name value) StringTable.empty cur_failures
   in
   let new_repairs = StringTable.fold (fun new_repairs name value ->
      if StringTable.mem cur_failures name then
         new_repairs
      else
         StringTable.add new_repairs name value) StringTable.empty old_failures
   in
      (* If no differences in the stats, then do nothing. *)
      if not (StringTable.is_empty new_failures && StringTable.is_empty new_repairs) then begin
         fprintf buf "@[<v 0>Test suite took: %a" pp_print_time delta_time;
         fprintf buf "@ Total successes: %d" (StringTable.cardinal current_stats.stats_success_runs);
         fprintf buf "@    (previously): %d" (StringTable.cardinal previous_stats.stats_success_runs);
         fprintf buf "@ Total failures:  %d" (StringTable.cardinal cur_failures);
         fprintf buf "@    (previously): %d" (StringTable.cardinal old_failures);
         if not (StringTable.is_empty new_failures) then
            fprintf buf "@ Newly failing:   %d" (StringTable.cardinal new_failures);
         if not (StringTable.is_empty new_repairs) then
            fprintf buf "@ Newly repaired:  %d" (StringTable.cardinal new_repairs);
         fprintf buf "@]@.";
         if not (StringTable.is_empty new_failures) then begin
            pp_print_newline buf ();
            display_case_summary buf "NEW failing cases" (map_cases current_cases new_failures)
         end;
         if not (StringTable.is_empty new_repairs) then begin
            pp_print_newline buf ();
            display_case_summary buf "NEW repaired cases" (map_cases current_cases new_repairs)
         end;
         if not (StringTable.is_empty cur_failures) then begin
            pp_print_newline buf ();
            display_case_summary buf "All currently failing test cases" (map_cases current_cases cur_failures)
         end
      end
