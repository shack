(*
   Main program for REI -- profiles editor loop
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Mc_string_util
open Rei_case
open Rei_main_case
open Rei_profiles
open Rei_type
open Rei_util


(***  Profile State  ***)


(* working_profiles
   The current set of profiles we are working on.  Note that the last
   saved profiles are in the current_stats global variable.  *)
let working_profiles = ref StringTable.empty


(***  Helper Functions  ***)


(* are_profiles_modified ()
   Returns true if the working_profiles differs from what is currently
   stored in the current test suite.  *)
let are_profiles_modified () =
   !working_profiles <> (!current_suite).suite_profiles


(* check_profile_validity ()
   Make sure no profile cases have the same names as test cases, and that
   all test cases named in all profiles are valid.  Returns false if the
   errors are not correctable; otherwise, modifies the working_profile
   in place.  *)
let check_profile_validity () =
   StringTable.fold (fun continue name profile ->
      if not continue then
         false
      else if StringTable.mem (!current_suite).suite_cases name then begin
         (* The test profile conflicts with a test case of same name *)
         if yes_no (sprintf "Profile \"%s\" conflicts with test case name. Discard profile" name) false then begin
            printf "Deleting test profile \"%s\"@." name;
            working_profiles := profile_delete !working_profiles name;
            true
         end else
            false
      end else begin
         (* Check that all test cases exist. *)
         StringSet.fold (fun continue name' ->
            if not continue then
               false
            else if StringTable.mem (!current_suite).suite_cases name' then
               true
            else if yes_no (sprintf "Case \"%s\" in profile \"%s\" does not exist. Delete profile entry"
             name' name) false then begin
               printf "Deleting case \"%s\" from profile \"%s\"@." name' name;
               working_profiles := profile_delete_cases !working_profiles name [name'];
               true
            end else
               false) true profile
      end) true !working_profiles


(***  Profile Commands  ***)


let cmd_add = function
   [name] ->
      (* Create a new test profile that is empty *)
      if StringTable.mem (!current_suite).suite_cases name then
         printf "Not allowing you to create a test profile with same name as a test case.@."
      else if StringTable.mem !working_profiles name then
         printf "Profile already exists with name \"%s\".@." name
      else begin
         printf "Creating new test profile with name \"%s\".@." name;
         working_profiles := profile_add !working_profiles name []
      end
 | name :: case_names ->
      (* Append named test cases to a profile. *)
      let cases = (!current_suite).suite_cases in
      let cases = List.map (fun name ->
         lookup_case_prof cases !working_profiles name) case_names
      in
      let cases = List.flatten cases in
      let cases = List.map (fun case -> case.case_name) cases in
         if StringTable.mem (!current_suite).suite_cases name then
            printf "Not allowing you to append to a test profile with same name as a test case.@."
         else if cases = [] then
            printf "No cases to add were specified.@."
         else begin
            printf "@[<v 3>Adding all named cases to test profile \"%s\":" name;
            List.iter (fun name -> printf "@ \"%s\"" name) cases;
            printf "@]@.";
            working_profiles := profile_add_cases !working_profiles name cases
         end
 | _ ->
      bad_arguments ()

let cmd_add_help =
   "<profile_name> [<case_name> ...]" ^
   "\nIf no test cases are given, this registers a new test profile." ^
   "\n\nIf at least one test case is also named, then this appends test cases" ^
   " to the named test profile. This will create the test profile if it does" ^
   " not already exist."


let cmd_cancel args =
   if args <> [] then begin
      bad_arguments ();
      Continue
   end else if not (are_profiles_modified ()) then begin
      printf "No modifications to discard@.";
      Exit ()
   end else if yes_no "Profiles modified. Cancel anyway" true then begin
      printf "Discarding modifications@.";
      Exit ()
   end else
      Continue

let cmd_cancel_help =
   "\nExit the profiles loop and return to the main prompt, discarding any changes."


let cmd_delete = function
   [name] ->
      (* Deletes an entire profile *)
      if not (StringTable.mem !working_profiles name) then
         printf "No profile found with name \"%s\".@." name
      else if yes_no (sprintf "Are you sure you want to delete test profile \"%s\"" name) true then begin
         printf "Deleting test profile with name \"%s\".@." name;
         working_profiles := profile_delete !working_profiles name
      end
 | name :: case_names ->
      (* Delete the named test cases from an existing profile *)
      let cases = (!current_suite).suite_cases in
      let cases = List.map (fun name ->
         lookup_case_prof cases !working_profiles name) case_names
      in
      let cases = List.flatten cases in
      let cases = List.map (fun case -> case.case_name) cases in
         if cases = [] then
            printf "No cases to delete were specified.@."
         else if not (StringTable.mem !working_profiles name) then
            printf "No profile found with name \"%s\".@." name
         else begin
            printf "@[<v 3>Deleting all named cases from test profile \"%s\":" name;
            List.iter (fun name -> printf "@ \"%s\"" name) cases;
            printf "@]@.";
            working_profiles := profile_delete_cases !working_profiles name cases
         end
 | _ ->
      bad_arguments ()

let cmd_delete_help =
   "<profile_name> [<case_name> ...]" ^
   "\nIf no test cases are given, this deletes an entire test profile." ^
   "\n\nIf at least one test case is also named, then this deletes the named" ^
   " test cases from the indicated test profile. The test profile itself is" ^
   " not deleted, even if the action would result in it having zero test cases."


let cmd_list = function
   [] ->
      (* Give a brief summary of all test profiles *)
      display_profiles_against_contrast std_formatter !working_profiles (!current_suite).suite_profiles
 | ["-cases"] ->
      (* Give a brief summary of all test cases *)
      let header = sprintf "Suite \"%s\"" (!current_suite).suite_name in
      let display_cases = (!current_suite).suite_cases in
      let contrast_cases = (!last_saved_suite).suite_cases in
         display_case_summary_against_contrast std_formatter header display_cases contrast_cases
 | [name] ->
      if StringTable.mem !working_profiles name then begin
         let header = sprintf "Test profile \"%s\"" name in
         let profile = StringTable.find !working_profiles name in
         let display_cases = map_cases_set (!current_suite).suite_cases profile in
         let contrast_cases = (!last_saved_suite).suite_cases in
            display_case_summary_against_contrast std_formatter header display_cases contrast_cases
      end else
         printf "No test profile defined with name \"%s\"@." name
 | _ ->
      bad_arguments ()

let cmd_list_help =
   "[-cases | <name>]" ^
   "\nWith no arguments, this lists all defined test profiles." ^
   "\n\nIf the \"-cases\" argument is given, then this lists a brief summary of all" ^
   " test cases; this is similar to running \"list\" with no arguments at the main" ^
   " prompt." ^
   "\n\nIf a name is given, then the test cases defined for the named test profile" ^
   " are listed."


let cmd_save = function
   [] ->
      if check_profile_validity () then begin
         printf "Saving new profiles to test suite.@.";
         current_suite := { !current_suite with suite_profiles = !working_profiles };
         Exit ()
      end else begin
         printf "No action taken. Please correct errors before saving profile.@.";
         Continue
      end
 | _ ->
      bad_arguments ();
      Continue

let cmd_save_help =
   "\nSaves modifications of profiles to the current test suite. This returns" ^
   " to the main prompt automatically."


let commands =
   ["add",     continue cmd_add,    cmd_add_help;
    "cancel",  cmd_cancel,          cmd_cancel_help;
    "delete",  continue cmd_delete, cmd_delete_help;
    "list",    continue cmd_list,   cmd_list_help;
    "ls",      continue cmd_list,   cmd_list_help;
    "save",    cmd_save,            cmd_save_help]


(* run_profiles
   Runs the loop for editing the test suite profiles.  The user may exit
   this loop by pressing ^D.  If the profiles are edited and the user saves
   the changes, then the new profiles are written to the current_suite
   automagically.  *)
let run_profiles profiles =
   working_profiles := profiles;
   run_loop "profiles> " commands cmd_cancel
