(*
   Statistical information on test runs in REI
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Rei_type


(* stats_create suite_name cases
   Build a new test run stats structure, using reasonable defaults.
   The stats structure is based on the data in cases, however the
   runs fields are left empty at this time.  This is called before
   a test run, and is further initialised using the following calls. *)
val stats_create : string -> case list -> stats


(* stats_update_time stats
   Updates the stop time for the run stats given.  *)
val stats_update_time : stats -> stats


(* stats_update_log stats log
   Sets the log field for the stats structure to a new string.  *)
val stats_update_log : stats -> string -> stats


(* stats_add_success stats name time
   Add a case name to the success list, and increment count.  *)
val stats_add_success : stats -> string -> float -> stats


(* stats_add_failure stats name msg
   Add a case name to the failure list, and increment count.  *)
val stats_add_failure : stats -> string -> string -> stats


(***  Read Stats from File  ***)


(* read_stats filename
   Reads in run stats from the named file.  If the file could not be found
   or parsed, then a SuiteException is thrown.  Otherwise, the contents of
   the stats file are returned.  This will NOT extract stats from a suite
   file; use read_suite for that task.  *)
val read_stats : string -> stats


(***  Print Stats  ***)


(* pp_print_stats buf stats
   Print out the statistical information to the indicated buffer.
   The output of this function is suitable for re-parsing.  *)
val pp_print_stats : formatter -> stats -> unit


(* write_stats filename stats
   Write the indicated run stats out to the named file.  If an error
   occurs while writing the file, then a SuiteException is raised.  *)
val write_stats : string -> stats -> unit


(***  Fancy Display of Run Stats  ***)


(* display_stats buf suite stats
   Print out the statistical information in human-readable form.  *)
val display_stats : formatter -> suite -> stats -> unit


(* display_stats_against_contrast buf display_suite contrast_suite stats
   Similar to above, but when listing failure cases, use a contrast.  *)
val display_stats_against_contrast : formatter -> suite -> suite -> stats -> unit


(* display_delta_stats buf suite current_stats previous_stats
   Display the changes between the current stats and a previous run of
   statistics.  *)
val display_delta_stats : formatter -> suite -> stats -> stats -> unit
