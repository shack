(*
   Main program for REI
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Mc_string_util
open Rei_case
open Rei_main_case
open Rei_main_profiles
open Rei_main_stats
open Rei_profiles
open Rei_stats
open Rei_suite
open Rei_suite_exn
open Rei_type
open Rei_util


(***  (Local) Global State  ***)


(* load_suite
   If set, then this contains the name of a test suite to preload.  *)
let load_suite = ref None


(* load_stats
   If set, then this contains the name of a run stats file to preload.  *)
let load_stats = ref None


(* save_stats
   If set, then this contains the name of a run stats file to save on
   a successful regression run.  *)
let save_stats = ref None


(* run_only
   If true, then we are running a regression suite only (no interactive
   mode is enabled).  See also the autorun flag.  *)
let run_only = ref false


(* run_profile
   Name of test profile to run with.  If none, run entire test suite.  *)
let run_profile = ref None


(* autorun
   If true, then we are performing an automated regression run.  *)
let autorun = ref false


(* verbose
   Returns true iff autorun is false.  *)
let verbose () =
   not !autorun


(***  Simple Helpers  ***)


(* can_clobber_suite
   Returns true if it is safe to clobber (discard) changes made to the
   currently active suite.  If the suite has not been modified, then
   this function will return true.  *)
let can_clobber_suite () =
   if not (is_suite_modified ()) then
      true
   else if StringTable.cardinal (!current_suite).suite_cases = 0 then
      (* Don't bother if the test suite is empty *)
      true
   else if yes_no (sprintf "Suite \"%s\" has been modified. Discard changes" (!current_suite.suite_name)) false then begin
      printf "Will discard changes.@.";
      true
   end else
      false


(***  Toplevel Commands  ***)


let cmd_add args =
   run_case_add args

let cmd_add_help =
   "[<target_name> [<argument> ...]]" ^
   "\nAdds a new test case. If an argument is specified, it is interpreted" ^
   " as the build target and evaluation command for the test case. A blank" ^
   " test case is created if no arguments are given."


let cmd_copy = function
   [srcname; dstname] ->
      begin
      let cases = (!current_suite).suite_cases in
         match lookup_single_case cases srcname with
            None ->
               printf "Test case \"%s\" not found.@." srcname
          | Some case ->
               (* For the destination case, we need to lookup the EXACT name
                  because we must know exactly what we're about to clobber. *)
               if not (StringTable.mem cases dstname)
                || yes_no (sprintf "Are you sure you want to overwrite case \"%s\"" dstname) false then begin
                  (* Can copy without reservation *)
                  let cases = case_add cases dstname case in
                     printf "Copying case \"%s\" to new case \"%s\".@." srcname dstname;
                     current_suite := { !current_suite with suite_cases = cases }
               end
      end
 | _ ->
      bad_arguments ()

let cmd_copy_help =
   "<src_name> <dst_name>" ^
   "\nCopies data from existing case src_name to a new case dst_name. If successful," ^
   " then the new case is identical to the old case. You may edit the new case with" ^
   " the \"edit\" command (an editing session is not automatically started)."


let cmd_delete = function
   [arg] ->
      let cases = (!current_suite).suite_cases in
      let profiles = (!current_suite).suite_profiles in
      let deletes = lookup_case_prof cases profiles arg in
         if deletes = [] then
            printf "Test case \"%s\" not found.@." arg
         else
            ignore (List.fold_left (fun cancelled case ->
               if cancelled then
                  true
               else
                  match yes_no_cancel (sprintf "Are you sure you want to delete case \"%s\"" case.case_name) false with
                     Some true ->
                        printf "Deleting case \"%s\"@." case.case_name;
                        let cases = (!current_suite).suite_cases in
                        let cases = case_delete cases case.case_name in
                           current_suite := { !current_suite with suite_cases = cases };
                           false
                   | Some false ->
                        false
                   | None ->
                        true) false deletes)
 | _ ->
      bad_arguments ()

let cmd_delete_help =
   "<name>" ^
   "\nDeletes the named case."


let rec cmd_edit args =
   let cases = (!current_suite).suite_cases in
      run_case_edit_select cases args

let cmd_edit_help =
   "[<name>]" ^
   "\nEdits an existing test case in the suite. If name is omitted, then a menu" ^
   " is presented which allows you to select a test case."


let cmd_list args =
   let name, args = get_next_argument args in
   let arg, _ = get_next_argument args in
      if name = "-profiles" then begin
         (* Give a brief summary of all test profiles *)
         display_profiles std_formatter (!current_suite).suite_profiles
      end else if name = "" then begin
         (* Give a brief summary of all test cases *)
         let header = sprintf "Suite \"%s\"" (!current_suite).suite_name in
         let display_cases = (!current_suite).suite_cases in
         let contrast_cases = (!last_saved_suite).suite_cases in
         display_case_summary_against_contrast std_formatter header display_cases contrast_cases
      end else begin
         (* Give a detail summary of the indicated test cases *)
         let cases = (!current_suite).suite_cases in
         let profiles = (!current_suite).suite_profiles in
         let cases = lookup_case_prof cases profiles name in
            if cases = [] then
               printf "No case matching the name \"%s\" was found.@." name
            else
               List.iter (fun case -> display_case std_formatter case arg) cases
      end

let cmd_list_help =
   "[<name> [-all] | -profiles]" ^
   "\nLists all test cases in the current suite. If a name is specified," ^
   " then only the matching test case is displayed (but in detail). If" ^
   " the \"-all\" option is additionally given, then full details about" ^
   " the matching case will be displayed." ^
   "\n\nIf the \"-profile\" argument is given, then a list of test profiles" ^
   " that are defined in this suite is displayed instead."


let cmd_move = function
   [srcname; dstname] ->
      begin
      let cases = (!current_suite).suite_cases in
         match lookup_single_case cases srcname with
            None ->
               printf "Test case \"%s\" not found.@." srcname
          | Some case ->
               (* For the destination case, we need to lookup the EXACT name
                  because we must know exactly what we're about to clobber. *)
               if not (StringTable.mem cases dstname)
                || yes_no (sprintf "Are you sure you want to overwrite case \"%s\"" dstname) false then begin
                  (* Can copy without reservation *)
                  let cases = case_delete cases case.case_name in
                  let cases = case_add cases dstname case in
                     printf "Moving case \"%s\" to new case \"%s\".@." srcname dstname;
                     current_suite := { !current_suite with suite_cases = cases }
               end
      end
 | _ ->
      bad_arguments ()

let cmd_move_help =
   "<src_name> <dst_name>" ^
   "\nMoves data from existing case src_name to a new case dst_name. If successful," ^
   " then the new case is identical to the old case, and the original case is deleted." ^
   " This is a faster way to rename a test case than going through the \"edit\" command."


let cmd_name = function
   [name] ->
      printf "Suite name \"%s\" changed to \"%s\"@." (!current_suite).suite_name name;
      current_suite := { !current_suite with suite_name = name }
 | _ ->
      bad_arguments ()

let cmd_name_help =
   "<name>" ^
   "\nChange the name of the test suite."


let cmd_new = function
   [name] ->
      if can_clobber_suite () then begin
         printf "Creating a new suite \"%s\"@." name;
         current_suite := new_suite name;
         last_saved_suite := !current_suite
      end
 | _ ->
      bad_arguments ()

let cmd_new_help =
   "<name>" ^
   "\nCreate a new test suite (discarding the current suite)."


let cmd_profiles = function
   [] ->
      printf "Editing profiles in suite \"%s\".@." (!current_suite).suite_name;
      run_profiles (!current_suite).suite_profiles
 | _ ->
      bad_arguments ()

let cmd_profiles_help =
   "\nEdit the test profiles saved with the current suite."


let cmd_quit args =
   if args <> [] then begin
      bad_arguments ();
      Continue
   end else if can_clobber_suite () then begin
      printf "Good-bye.@.@.";
      printf "Man fears the darkness, and so he scrapes away at the edges of it with fire.@.";
      printf "  -- Ayanami Rei (Shin Seiki Evangelion)@.@.";
      Exit ()
   end else
      Continue

let cmd_quit_help =
   "\nExit the program."


let cmd_read = function
   [file] ->
      if can_clobber_suite () then begin
         if verbose () then
            printf "Reading new suite file \"%s\"@." file;
         current_suite := read_suite file;
         last_saved_suite := !current_suite
      end
 | _ ->
      bad_arguments ()

let cmd_read_help =
   "<filename>" ^
   "\nRead a test suite in from the indicated file."


let cmd_run args =
   let arg, _ = get_next_argument args in
   let cases =
      if arg = "" then begin
         if verbose () then
            printf "Running regression test suite \"%s\"@." (!current_suite).suite_name;
         let cases = StringTable.fold (fun cases _ case ->
            case :: cases) [] (!current_suite).suite_cases
         in
         let cases = List.rev cases in
            cases
      end else begin
         if verbose () then
            printf "Running regression test suite \"%s\" on \"%s\"@." (!current_suite).suite_name arg;
         let cases = (!current_suite).suite_cases in
         let profiles = (!current_suite).suite_profiles in
         let cases = lookup_case_prof cases profiles arg in
            cases
      end
   in
   let cases = List.filter case_enabled cases in
   let autorun =
      if !autorun then
         ARAuto !current_stats
      else
         ARManual
   in
   let displayf, outfmt =
      if go_curses () then
         Mcc_ncurses_display.in_display, Mcc_ncurses_display.make_formatter ()
      else
         (fun f a -> f a), std_formatter
   in
   let suite_results =
      displayf (fun () -> run_suite outfmt autorun !current_suite cases) ()
   in
      match suite_results with
         Some stats ->
            current_stats := Some stats
       | None ->
            ()

let cmd_run_help =
   "[<name>]" ^
   "\nRuns the current regression test suite. If a name is indicated," ^
   " then the named test case is run. The name may be an integer interval" ^
   " or a glob pattern to run multiple cases at once."


let cmd_stats = function
   [filename] ->
      let () = printf "Reading new run stats from file \"%s\".@." filename in
      let stats = read_stats filename in
         run_stats (Some stats)
 | [] ->
      run_stats !current_stats
 | _ ->
      bad_arguments ()

let cmd_stats_help =
   "[<filename>]" ^
   "\nIf no argument given, display and manipulate statistics associated with the current" ^
   " test suite. The statistics are usually the result of the last test run, or the last" ^
   " saved test run in the suite file if no runs have completed in the current session." ^
   "\n\nIf a filename is given, then new run stats are read from the indicated file, and then" ^
   " the newly loaded stats are displayed (any currently loaded run stats are discarded)." ^
   "\n\nNote that the stats prompt will allow you to save the current run stats."


let cmd_write = function
   [file] ->
      if ok_to_overwrite_file file then begin
         printf "Writing current suite \"%s\" to file \"%s\"@." (!current_suite).suite_name file;
         write_suite file !current_suite;
         last_saved_suite := !current_suite
      end else
         printf "No file was saved.@."
 | _ ->
      bad_arguments ()

let cmd_write_help =
   "<filename>" ^
   "\nWrites the current regression suite out to the named file."


let commands =
   ["add",        continue cmd_add,       cmd_add_help;
    "copy",       continue cmd_copy,      cmd_copy_help;
    "delete",     continue cmd_delete,    cmd_delete_help;
    "edit",       continue cmd_edit,      cmd_edit_help;
    "exit",       cmd_quit,               cmd_quit_help;
    "list",       continue cmd_list,      cmd_list_help;
    "ls",         continue cmd_list,      cmd_list_help;
    "log",        continue cmd_log,       cmd_log_help;
    "move",       continue cmd_move,      cmd_move_help;
    "name",       continue cmd_name,      cmd_name_help;
    "new",        continue cmd_new,       cmd_new_help;
    "profiles",   continue cmd_profiles,  cmd_profiles_help;
    "quit",       cmd_quit,               cmd_quit_help;
    "read",       continue cmd_read,      cmd_read_help;
    "run",        continue cmd_run,       cmd_run_help;
    "stats",      continue cmd_stats,     cmd_stats_help;
    "write",      continue cmd_write,     cmd_write_help]


(* run_main
   Runs the main program until the user quits and/or presses ^D.  This
   catches any exceptions that may have been thrown by the read-eval-
   print loop, above.  *)
let run_main () =
   run_loop "rei> " commands cmd_quit


(***  Let's Start Execution!  ***)


(* get_top_dir
   Search for the top directory of the compiler.  We search by
   looking for main/mcc, the compiler's name.  Note that we must
   find an uninstalled copy of the compiler, which has the actual
   test suite.  This returns the name of the top directory.  *)
let get_top_dir () =
   let search_path = ["../../../../"; "../../../"; "../../"; "../"; "./"] in
   let rec find = function
      path :: paths ->
         let path' = path ^ "main/mcc_main.ml" in
            if Sys.file_exists path' then
               path
            else
               find paths
    | [] ->
         printf "Cannot find the MCC compiler, aborting.@.";
         printf "Note: I need an UNINSTALLED copy of MCC.@.";
         exit 1
   in
      find search_path


(* display_top_dir
   This simply displays the top directory, then exits.  Used by
   the helper scripts to locate the base of MCC.  *)
let display_top_dir () =
   printf "%s@." (get_top_dir ());
   exit 0


(* set_top_dir
   Search for the top directory of the compiler.  We search by
   looking for main/mcc, the compiler's name.  Note that we must
   find an uninstalled copy of the compiler, which has the actual
   test suite.  *)
let set_top_dir () =
   let top_dir = get_top_dir () in
      if verbose () then
         printf "Changing to directory %s@." top_dir;
      Sys.chdir top_dir


(* run_interactive_loop
   Initialise the readline library, and call the main loop.  *)
let run_interactive_loop () =
   Sys.catch_break true;
   Mcc_readline.initialise_readline ();
   run_main ()


(* version_info
   Print version information about moogleize.  *)
let version_info () =
   printf "Ayanami REI (Regression Evaluation Interface) version %s@." Rei_state.version;
   printf "Regression system for MCC (Mojave Compiler)@.";
   exit 1


(* Define the program's options *)
let suite_name name =
   match !load_suite with
      None ->
         load_suite := Some name
    | Some _ ->
         printf "Only one test suite file may be named on the command line.@.";
         exit 1

let usage = "usage: rei [options] [test-suite]"

let spec =
  ["Basic options",
     ["-version",       Mc_arg.Unit version_info,
                        "print version information";
      "-curses",        Mc_arg.Set Rei_state.curses,
                        "Try to use curses for the user interface.";
      "-read-stats",    Mc_arg.String (fun filename -> load_stats := Some filename),
                        "This loads a stats file. The stats file is loaded before the" ^
                        " current working directory is changed."];

   "Non-interactive options",
     ["-run",           Mc_arg.Unit (fun () ->
                           Rei_state.interactive := false;
                           run_only := true;
                           run_profile := None),
                        "Run the test suite named on the command line, and report the" ^
                        " results (this does not start an interactive shell).";
      "-run-profile",   Mc_arg.String (fun s ->
                           Rei_state.interactive := false;
                           run_only := true;
                           run_profile := Some s),
                        "Run the named test profile on the command line, and report the" ^
                        " results (this does not start an interactive shell).";
      "-write-stats",   Mc_arg.String (fun filename -> save_stats := Some filename),
                        "When used with -run, this flag saves the stats for the test run to the" ^
                        " named file (file path is relative to the working directory when the" ^
                        " program is started, NOT relative to top of MCC tree). This will save" ^
                        " ONLY the stats. If this option is emitted, then the stats will not be" ^
                        " saved. This option has no effect in interactive mode.";
      "-autorun",       Mc_arg.Set autorun,
                        "When used with -run, this flag indicates that we are doing an auto-run." ^
                        " The generated stats will be compared against stats from a previous run," ^
                        " and the differences between the two runs are reported. This option has" ^
                        " no effect in interactive mode. This option does not produce any output" ^
                        " unless differences are detected; therefore it is suitable for running" ^
                        " from a cron script."];

   "Advanced options",
     ["-build-timeout", Mc_arg.Int (fun i -> Rei_state.default_build_timeout := i),
                        "number of seconds before a prebuild command will time out" ^
                        " (default: " ^ (string_of_int !Rei_state.default_build_timeout) ^ ")";
      "-run-timeout",   Mc_arg.Int (fun i -> Rei_state.default_run_timeout := i),
                        "number of seconds before a run/eval command will time out" ^
                        " (default: " ^ (string_of_int !Rei_state.default_run_timeout) ^ ")";
      "-topdir",        Mc_arg.Unit display_top_dir,
                        "used by helper scripts to locate the base install of MCC."]]


let run () =
   (* Parse the command-line arguments *)
   Mc_arg.parse spec suite_name usage;

   (* Clear run-only flags if we're not in that mode *)
   if not !run_only then begin
      save_stats := None;
      autorun := false
   end;

   (* Print banner and preload the test suite. *)
   if verbose () then
      printf "@.This is Ayanami REI (Regression Evaluation Interface)@.@.";
   (match !load_suite with
      None ->
         ()
    | Some suite ->
         if verbose () then
            printf "Preloading test suite \"%s\"@." suite;
         ignore (cmd_read [suite]);
   );
   (match !load_stats with
      None ->
         ()
    | Some stats ->
         if verbose () then
            printf "Preloading run stats file \"%s\"@." stats;
         current_stats := Some (read_stats stats)
   );

   (* Before we change the working directory, make sure save_stats is
      qualified with the current working directory. *)
   (match !save_stats with
      None ->
         ()
    | Some filename ->
         if String.length filename > 0 && filename.[0] <> '/' then
            save_stats := Some (Sys.getcwd () ^ "/" ^ filename));

   (* At this point, we need to change working directory and run. *)
   set_top_dir ();
   if !run_only then begin
      if verbose () then
         printf "Running test suite \"%s\"@." (!current_suite).suite_name;
      (match !run_profile with
         None ->
            ignore (cmd_run [])
       | Some profile ->
            ignore (cmd_run [profile]));
      match !save_stats, !current_stats with
         Some filename, Some stats ->
            if verbose () then
               printf "Saving stats to file \"%s\"@." filename;
            write_stats filename stats
       | _ ->
            ()
   end else begin
      run_interactive_loop ()
   end


let () =
   try
      run ()
   with
      Unix.Unix_error (error, f, arg) ->
         printf "Unix error: %a@." pp_print_unix_error (error, f, arg);
         exit 1
    | SuiteException (pos, e) ->
         print_exn pos e;
         exit 1
