(*
   Build a test case or sequence of test cases
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Rei_type


(***  Special return code  ***)


(* build_internal_error
   The value returned to indicate internal build errors.  Set to 127.  *)
val build_internal_error : int


(***  Build Test Targets  ***)


(* monitor_fun
   Type of a build monitor function.  See the description of the f
   argument on build_cases.  *)
type monitor_fun = formatter -> string -> int -> int -> unit


(* build_cases buf f targets
   Build all named targets in a list of test cases.  Returns the return
   code of the build process if the command was successfully run (a return
   value of 0 indicates that all test cases were successfully built; any
   other return code indicates a build error).  Returns the value 127 (the
   value of build_internal_error) if internal error occurs when we invoke
   the build command.  The output of cons is printed to the given buffer,
   if there is any output.

   WARNING: This function uses return code 127 (build_internal_error) as a
   special value; the build process should not exit with this code.

   Due to the nature of the cons build, if errors occur we will not be
   able to describe exactly which test targets failed.  We pass the -k
   flag to cons to ensure that as many targets are built as possible, in
   the event of any errors.  If you want to determine if a single test
   case builds correctly, you must build with only that test case listed
   in targets.

   Exceptions are caught by this function and translated into messages
   (note that Sys.Break may be thrown by this function, however).  All
   output is written to the indicated buffer (which may be a Y buffer).
   If this function catches an exception, then it will return 127 (the
   value of build_internal_error).

   The argument f : formatter -> string -> int -> int -> unit warrants
   some explanation.  This is a function that will be called whenever
   new output is captured from the cons process.  It will eventually
   receive the entire contents of the cons output (assuming no abnormal
   termination).  It is passed the same formatter that is given to the
   build_runner function, but it will be run in the context of the pipe
   process.  This function can be used to monitor cons's output, and
   emit escape sequences that indicate current build status.  It does
   NOT need to emit the string buffer itself; the string buffer will
   be emitted to buf automatically by the pipe process.  If you do not
   care to monitor cons's output, pass in None.  *)
val build_cases : formatter -> monitor_fun option -> string list -> int


(* build_case buf target
   Builds a single test case.  This is no different from building a list
   all at once; we may as well defer to the general form here.  *)
val build_case : formatter -> monitor_fun option -> string -> int
