(*
   Run a test case or sequence of test cases
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Unix
open Format
open Mc_string_util
open Rei_build
open Rei_case
open Rei_file
open Rei_stats
open Rei_suite_exn
open Rei_type
open Rei_util


(***  Evaluation Utilities  ***)


(* eval_string case op
   Evaluates a string op.  This pulls values from the cases structure, as
   needed.  Returns a string suitable for evaluation with system().  *)
let rec eval_string case op =
   let eval_value = function
      StrTarget ->
         case.case_target
    | StrValue s ->
         s
   in
      match op with
         v :: op ->
            eval_value v ^ eval_string case op
       | [] ->
            ""


(***  Test Run Evaluation  ***)


(* RunException
   An internal exception used to signal fatal errors in the test run.  The
   error message is included.  This exception is not exported, so it should
   not be allowed to escape from any interface function.

   In this exception, the first string is a brief summary of the error
   (used for saving statistical information), and the second string is a
   detail message suitable for log/console output.  *)
exception RunException of string * string


(* run_case_internal buf case
   Run a single test case, and return the time for the test run, the process
   exit code, the NAME of the output file, and the NAME of the standard error
   file. Raises RunException if the test case could not be run or failed in
   another abnormal way (e.g. by segfaulting).  May raise other exceptions,
   such as Unix_error.

   All output is sent to the indicated buffer.  This function does not emit
   escape sequences (e.g. to set title) directly; that task is left to the
   interface functions.  The buffer may be a Y formatter for logging.

   This function checks for obvious failure conditions (such as the existence
   of a prebuild error file or lack of target file), but does NOT check the
   final results of the evaluation.  Validation of the results is left to the
   caller, if desired.

   Temporary files created by this call (for stdout and stderr) are deleted
   if any failure occurs (even if an exception is raised).  *)
let run_case_internal buf case =
   (* Make sure the target exists, AND an error file does not exist *)
   let target_name = case.case_target in
   let err_name = (trim_extension target_name) ^ ".err" in
   let () =
      if Sys.file_exists err_name || not (Sys.file_exists target_name) then
         raise (RunException ("Build failure", "Errors from the prebuild detected"))
   in

   (* Construct the test evaluation string *)
   let eval = eval_string case case.case_eval in
   let () =
      if eval = "" then
         raise (RunException ("Bad eval string", "No program to evaluate specified"))
   in
   let () = fprintf buf "Evaluating %s@." eval in

   (* Prepare the files for capturing standard output and standard error.
      After this point, we should be prepared to delete the temporary files.
      Therefore the rest of the function exists in a try block. *)
   let stdout_name = Filename.temp_file "out" ".rei" in
   let stderr_name = Filename.temp_file "err" ".rei" in
      try
         (* Function which will actually evaluate the test case. *)
         let run () =
            redirect_output stdout_name stderr_name;
            system eval
         in

         (* Actually evaluate the test case, here. *)
         let time, status = Rei_timer.run !Rei_state.default_run_timeout run () in
         let () = fprintf buf "Test run took %a seconds@." pp_print_time_seconds time in
            match status with
               WEXITED n ->
                  time, n, stdout_name, stderr_name
             | WSIGNALED n ->
                  let short_msg = sprintf "Terminated with signal %d" n in
                  let long_msg = sprintf "Test run terminated abnormally, with signal %d" n in
                     raise (RunException (short_msg, long_msg))
             | WSTOPPED n ->
                  let short_msg = sprintf "Terminated with stop code %d" n in
                  let long_msg = sprintf "Test run terminated abnormally, with stop code %d" n in
                     raise (RunException (short_msg, long_msg))
      with
         e ->
            (* On error, remove the temporary files and then re-raise. *)
            ignore (remove_temporary_files [stdout_name; stderr_name]);
            raise e


(***  Initial Test Case Run  ***)


(* run_case buf case
   Run a test case, and return the process exit code and a buffer that
   contains the process's standard output channel.  If a failure occurs,
   then an exception is raised (either RunException or Unix_error).
   This function deletes any temporary files that are created, even if
   an exception is raised.  This function does not validate the return
   code or output of the test run; that is left to the caller, if it is
   desired.  *)
let run_case buf case =
   (* After the internal call, be prepared to delete stdout/stderr if an
      exception occurs (e.g. during the read_file call).  *)
   let _, n, stdout_name, stderr_name = run_case_internal buf case in
      try
         let buffer = read_file stdout_name in
            ignore (remove_temporary_files [stdout_name; stderr_name]);
            n, buffer
      with
         e ->
            (* On error, remove the temporary files and then re-raise. *)
            ignore (remove_temporary_files [stdout_name; stderr_name]);
            raise e


(* initial_eval_case buf case
   This is an interface function that evaluates a test case, assuming the
   return coede and output have not yet been captured.  This populates the
   build code, return code, and output fields of the test case, assuming
   the test run does not end in error.  It assumes the target/eval fields
   are already set correctly.  If successful, then the boolean value true
   is also returned.

   If the build succeeds, then the eval command is run and case_return/
   case_output are set to the results of the evaluation.  If the build
   fails, then build_code is set to the result of the build, and the
   case_return/case_output fields are cleared.  Note, initial_eval_case
   considers build failures to be ``successful'', so it is important to
   check that a build failure was actually expected (initial_eval_case
   cannot distinguish test cases which are deliberate compilation errors
   used to test type checking, e.g.).

   If a fatal error occurs, a failure message is printed to the supplied
   buf, and this function returns the pair (false, case) for the case given
   as input (unaltered). Note that this call will build the test target for
   you automatically.  *)
let initial_eval_case buf case =
   try
      let build_code = build_case buf None case.case_target in
      let () =
         if build_code = build_internal_error then
            raise (RunException ("Build failure", "Could not build test case"))
      in
      let return_code, output_data =
         if build_code = 0 then
            run_case buf case
         else
            0, ""
      in
      let case =
         { case with
           case_build  = build_code;
           case_return = return_code;
           case_output = output_data;
         }
      in
         true, case
   with
      RunException (_, msg) ->
         fprintf buf "*** Run exception occurred: %s@." msg;
         false, case
    | Unix_error (error, f, arg) ->
         fprintf buf "*** Unix error occurred during run: %a@." pp_print_unix_error (error, f, arg);
         false, case


(***  Run Test Cases with Expected Results  ***)


(* status_fun
   Type of the status monitor function.  Takes a format channel and a
   message to set the status to.  *)
type status_fun = formatter -> string -> unit


(* compare_text out_text out_off exp_text exp_off len
   Checks that the output text matches the desired text.  Returns a boolean
   value; true if the text matches within the offsets/length specified, and
   false otherwise.  *)
let compare_text out_text out_off exp_text exp_off len =
   let out_len = String.length out_text in
   let exp_len = String.length exp_text in
   let rec loop out_off exp_off len =
      if len <= 0 then
         true
      else if out_off >= out_len then
         false
      else if exp_off >= exp_len then
         false
      else if out_text.[out_off] <> exp_text.[exp_off] then
         false
      else
         loop (out_off + 1) (exp_off + 1) (len - 1)
   in
      loop out_off exp_off len


(* compare_file_to_buffer filename buffer
   Checks that a file and a text buffer agree on their output.  Returns
   unit if the file contents match the indicated buffer; if there is a
   discrepancy, then RunException is raised.  A Unix_error may also be
   raised if there is an error reading the file.  *)
let compare_file_to_buffer filename buffer =
   let size = 16384 in
   let in_buf = String.create size in
   let inx = openfile filename [O_RDONLY] 0 in
      (* From this point on, make sure that inx gets closed eventually. *)
      try
         let rec read_loop buf_off =
            let len = read inx in_buf 0 size in
               if len > 0 then begin
                  if not (compare_text in_buf 0 buffer buf_off len) then
                     raise (RunException ("Output discrepancy", "Output does not match expected output."));
                  read_loop (buf_off + len)
               end else begin
                  if buf_off <> String.length buffer then
                     raise (RunException ("Output discrepancy", "Expected more output than actually obtained."));
                  ()
               end (* Reached end of file? *)
         in
            read_loop 0;
            close inx
      with
         e ->
            (* On error, close inx then re-raise the exception *)
            close inx;
            raise e


(* check_result buf case result stdout_name stderr_name
   Checks the result of a test run.  The exit code returned by the test case
   is passed as an argument, as well as the files containing the standard
   output and standard error streams.  This function will raise an exception
   if there is a discrepancy (Unix error or mismatch); it will not delete the
   indicated files under any circumstance.  Status messages are printed to
   the given buffer to indicate the progress of the checker.  *)
let check_result buf case result stdout_name stderr_name =
   fprintf buf "Process exited normally with code %d@." result;
   if case.case_build <> 0 then begin
      (* Oops... we expected this test case to fail the build! *)
      let short_msg = "Case built/wasn't supposed to" in
      let long_msg = sprintf "Test case built fine, but was supposed to give a compilation error with code %d."
         case.case_build
      in
         raise (RunException (short_msg, long_msg))
   end;
   if result <> case.case_return then begin
      let short_msg = sprintf "Expected return code %d, got %d" case.case_return result in
      let long_msg = sprintf "Test case expected return code %d, test run terminated with return code %d"
         case.case_return result
      in
         raise (RunException (short_msg, long_msg))
   end;
   compare_file_to_buffer stdout_name case.case_output;

   (* All comparisons were successful *)
   fprintf buf "Output matches expected output.@."


(* run_and_check_case buf case
   Run a single test case, and check it against the expected return code
   and expected output.  Raises an exception if anything goes wrong.  In
   all cases (including when an exception is raised), this will clean up
   any temporary files generated.  On success, returns the total runtime
   of the test case.  *)
let run_and_check_case buf case =
   (* After this call, make sure we clean up the stdout/stderr temporary
      files even if an exception is raised.  *)
   let time, return, stdout_name, stderr_name = run_case_internal buf case in
      try
         check_result buf case return stdout_name stderr_name;
         ignore (remove_temporary_files [stdout_name; stderr_name]);
         time
      with
         e ->
            (* Clear the temporary files and re-raise the exception. *)
            ignore (remove_temporary_files [stdout_name; stderr_name]);
            raise e


(* title_prefix
   Compute the prefix text for the titlebar message.  *)
let title_prefix stats count total =
   let stats = stats_update_time stats in
   let delta_time = stats.stats_stop_time -. stats.stats_start_time in
   let percent = ((count - Rei_state.first_case_number) * 100) / total in
      sprintf "[%s]  %d/%d  %d%%  " (string_of_time_short delta_time) count total percent


(* update_title buf f text
   Updates the title to the indicated text (assuming title hook was given). *)
let update_title buf f text =
   match f with
      None ->
         ()
    | Some f ->
         f buf text


(* print_test_header buf f stats case
   Prints out the test case header information.  f is the status-update
   function.  *)
let print_test_header buf f stats case =
   (* Update the titlebar and print the test case header. *)
   let name = case.case_name in
   let title = title_prefix stats stats.stats_current stats.stats_total in
      update_title buf f (sprintf "%sRunning %s" title name);
      fprintf buf "@.=== Regression Run #%03d: %s ===@." stats.stats_current name


(* run_and_check_case buf f stats case
   This is an interface function.  This will run the indicated test case,
   and will report any errors to the buffer given.  This call will update
   the stats structure passed in, and will return the modified stats as well
   as a boolean flag indicating whether the test run was successful.  This
   function catches exceptions and prints any errors to the buffer given
   (which may be a Y-formatter).

   This call will NOT prebuild the test case.  It is expected that all test
   cases were prebuilt.  Furthermore, this function should not be called if
   you actually expected the test case to fail.  Call run_and_check_cases if
   you want build-failure test cases to be properly handled.

   f is the status-update function; if not None, then this function is
   called whenever the runner wishes to update the status line (possibly
   the title bar of the application).  Set it to None for no status update.  *)
let run_and_check_case buf f stats case =
   (* Update the titlebar and print the test case header. *)
   print_test_header buf f stats case;

   (* From this point on, be sure to catch any exceptions. *)
   try
      (* Attempt to run and check the test case. *)
      let time = run_and_check_case buf case in

         (* ...If we're still here, then the test run was a success! *)
         true, stats_add_success stats case.case_name time

   (* Errors encountered during test run if any of these fire. *)
   with
      RunException (fail, msg) ->
         fprintf buf "*** Run exception: %s@." msg;
         false, stats_add_failure stats case.case_name fail
    | Unix_error (error, f, arg) ->
         fprintf buf "*** Unix error while running: %a@." pp_print_unix_error (error, f, arg);
         false, stats_add_failure stats case.case_name "Unexpected Unix error"


(* build_and_check_build_failure buf f stats case
   Internal function that checks that a case that is supposed to produce
   a compiler error actually DOES produce a compiler error.  Build failure
   cases must be run separately from the cases given to run_and_check_case;
   see the notes above for why.  This function is not exported.  *)
let build_and_check_build_failure buf f stats case =
   (* Print the test case header *)
   print_test_header buf f stats case;

   (* Try to build the test case, and get the build result. *)
   let name = case.case_name in
   let build_result = build_case buf None case.case_target in
      if case.case_build = 0 then begin
         fprintf buf "*** Something went wrong, a case with build code 0 was treated as a build failure.@.";
         false, stats_add_failure stats name "Ok case ended up in build failure list"
      end else if build_result <> case.case_build then begin
         fprintf buf "*** Expected build code %d, got build code %d.@." case.case_build build_result;
         false, stats_add_failure stats name (sprintf "Wanted build code %d, got %d" case.case_build build_result)
      end else begin
         (* This is the ``successful'' case.  That's right folks, we
            successfully failed to build this test case!  *)
         fprintf buf "Got build code %d, as expected.@." build_result;
         true, stats_add_success stats name 0.
      end


(* prebuild_normal_cases buf f stats cases
   Prebuild the normal cases.  This returns true if the build code returned
   is zero; all of these cases are expected to succeed.  This is an internal
   function only.  *)
let prebuild_normal_cases buf f stats cases =
   let targets = List.fold_left (fun targets case ->
      StringSet.add targets case.case_target) StringSet.empty cases
   in
   let targets = StringSet.to_list targets in

   let build_count = List.length targets in
   let title_prefix () =
      let st_count = stats.stats_current in
      let st_total = stats.stats_total in
      let title = title_prefix stats st_count st_total in
      let title = sprintf "%sPrebuilding " title in
         title
   in
   let title = sprintf "%s(%d cases)" (title_prefix ()) build_count in

   (* The following code deals with the build monitor function, which will
      attempt to update the status bar by monitoring the output of cons and
      scanning for the expected target names.  This is not an efficient
      monitor; it assumes cons may forget to omit certain targets, so it
      will scan ahead gratuitously.  It will also assume the targets are
      being built in order (I hope!). *)
   let target_current = ref 0 in
   let targets_remaining = ref targets in

   (* WARNING: The build monitor is run in a separate process! *)
   let build_monitor buf input iofs ilen =
      (* Scan for the last target mentioned in the latest input from cons.
         If at least one target is found, this returns a nonzero integer
         and the list of targets still unfound; the integer indicates how
         many target names were skipped. *)
      let rec scan_for_target iofs ilen best entry = function
         target :: targets ->
            let next_offset, best =
               try
                  Mc_string_util.strpat input iofs ilen target, (entry + 1, targets)
               with
                  Not_found ->
                     0, best
            in
            let iofs = iofs + next_offset in
            let ilen = ilen - next_offset in
               scan_for_target iofs ilen best (entry + 1) targets
       | [] ->
            best
      in
      let scan_for_target targets = scan_for_target iofs ilen (0, targets) 0 targets in
      let skip, targets = scan_for_target !targets_remaining in

      (* Compute new percentage for status bar, based on latest info. *)
      let current = !target_current + skip in
      let percent = (current * 100) / build_count in
         target_current := current;
         targets_remaining := targets;
         update_title buf f (sprintf "%s(%d/%d  %d%%)" (title_prefix ()) current build_count percent)
   in

   (* ONLY enable the build monitor if interactive! *)
   let build_monitor =
      match f with
         Some _ ->
            Some build_monitor
       | None ->
            None
   in

      (* Actually build the test cases. *)
      update_title buf f title;
      fprintf buf "@.=== Prebuilding %d Cases ===@." build_count;
      (build_cases buf build_monitor targets != build_internal_error)

let prebuild_normal_cases buf f stats cases =
   if cases = [] then
      true
   else
      prebuild_normal_cases buf f stats cases


(* run_and_check_cases buf f stats cases
   Run and check a list of test cases.  This function will prebuild the test
   cases automatically.  This returns a boolean value that indicates whether
   runs were successful, as well as the statistics collected for this test
   run.  This function WILL prebuild all the test cases.  The cases list can
   include cases which are expected to fail.

   f is the status-update function; if not None, then this function is
   called whenever the runner wishes to update the status line (possibly
   the title bar of the application).  Set it to None for no status update. *)
let run_and_check_cases buf f stats cases =
   (* Filter the build failures from the ok cases. *)
   let ok_cases, bf_cases = List.fold_left (fun (ok_cases, bf_cases) case ->
      if case.case_build = 0 then
         case :: ok_cases, bf_cases
      else
         ok_cases, case :: bf_cases) ([], []) cases
   in
   let ok_cases = List.rev ok_cases in
   let bf_cases = List.rev bf_cases in

   (* Handle the expected build failures first; they're easy. *)
   let result, stats = List.fold_left (fun (result, stats) case ->
      let result', stats = build_and_check_build_failure buf f stats case in
      let result = result && result' in
         result, stats) (true, stats) bf_cases
   in

   (* Prebuild the normal test cases, and run them. *)
   let build_result = prebuild_normal_cases buf f stats ok_cases in
   let result = result && build_result in
      List.fold_left (fun (result, stats) case ->
         if build_result then
            let result', stats = run_and_check_case buf f stats case in
            let result = result && result' in
               result, stats
         else
            let stats = stats_add_failure stats case.case_name "Prebuild abnormal error" in
               false, stats) (result, stats) ok_cases
