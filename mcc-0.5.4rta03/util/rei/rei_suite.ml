(*
   Read and write suite files, and run test suites
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_format_util
open Mc_string_util
open Rei_case
open Rei_file
open Rei_profiles
open Rei_run
open Rei_stats
open Rei_suite_exn
open Rei_type
open Rei_util


(***  Suite Creation / Loading  ***)


(* new_suite name
   Constructs a new test suite with the indicated name.  *)
let new_suite name =
   { suite_name      = name;
     suite_cases     = StringTable.empty;
     suite_profiles  = StringTable.empty;
   }


(* read_suite filename
   Reads in a suite from the named file.  If the suite could not be found
   or parsed, then a SuiteException is thrown.  Otherwise, the contents of
   the test suite are returned.  *)
let read_suite filename =
   Rei_suite_lex.reset_pos ();
   Rei_state.current_file := filename;
   let inx =
      try open_in filename with
         Sys_error s ->
            raise (SuiteException (exp_pos (!Rei_state.current_pos), SysError s))
   in
      try
         let lexbuf = Lexing.from_channel inx in
         let suite = Rei_suite_parse.suite Rei_suite_lex.suite lexbuf in
            close_in inx;
            suite
      with
         Parsing.Parse_error ->
            close_in inx;
            raise (SuiteException (exp_pos (!Rei_state.current_pos), ParseError))
       | Sys_error s ->
            close_in inx;
            raise (SuiteException (exp_pos (!Rei_state.current_pos), SysError s))


(***  Suite Printing  ***)


(* pp_print_*
   Print out various components of a test suite.  *)
let pp_print_header buf name =
   fprintf buf "(* Automatically generated REI suite file *)@.";
   fprintf buf "suite \"%s\";@.@." (String.escaped name)

let pp_print_cases buf cases =
   StringTable.iter (fun _ case ->
      pp_print_case buf case;
      fprintf buf "@.@.") cases

let pp_print_profiles buf profiles =
   StringTable.iter (fun name profile ->
      pp_print_profile buf name profile;
      fprintf buf "@.@.") profiles


(* write_suite filename suite
   Writes the indicated test suite out to the named file.  If an error
   occurs while writing the file, then a SuiteException is raised.  *)
let write_suite filename suite =
   let out =
      try open_out filename with
         Sys_error s ->
            raise (SuiteException (exp_pos (filename, 0, 0, 0, 0), SysError s))
   in
      try
         let buf = formatter_of_out_channel out in
            pp_print_header buf suite.suite_name;
            pp_print_cases buf suite.suite_cases;
            pp_print_profiles buf suite.suite_profiles;
            pp_print_newline buf ();
            close_out out
      with
         Sys_error s ->
            close_out out;
            raise (SuiteException (exp_pos (filename, 0, 0, 0, 0), SysError s))


(***  Run a Suite  ***)


(* autorun_type
   Indicates whether a suite is being autorun, or manually run.  If it
   is autorun, then the suite run will be mostly silent, and results will
   be compared against old run stats (a previous run of the test suite).
   If it is manually run, then more verbose output is generated, and we
   will not attempt to compare against a previous test run.
      ARAuto old_stats           Auto-run, indicating old run stats.
      ARManual                   Manual run (we don't care about old runs).
 *)
type autorun_type =
   ARAuto   of stats option
 | ARManual


(* run_suite lbuf stdout autorun suite cases
   Runs a regression test suite.  The cases that should be run are listed
   in the cases argument.  Assuming the test run is NOT interrupted, the
   statistics for the test run are returned; the statistics indicate total
   run time as well as a detail list of which test cases failed.  This is
   an internal function; it will not catch exceptions such as Sys.Break.

   If the autorun flag is ARAuto, then this assumes we want an automated
   test run, in which case nothing is output unless there are differences
   from the test run currently saved in the suite (and if there are diffs,
   we will output a delta report between the existing stats in the test
   suite, and the new stats).

   This internal function assumes the buffer for output has already been
   setup, and that all output to the buffer is being logged somewhere.
   This function will NOT update the log entry on the suite.  *)
let run_suite lbuf stdout autorun suite cases =
   (* Construct a Y formatter that captures log output and will
      simultaneously output to the standard output channel.  *)
   let ybuf = y_formatter stdout lbuf in
   let nbuf =
      match autorun with
         ARAuto _ ->
            lbuf
       | ARManual ->
            ybuf
   in

   (* Build a new stats record for this test run.  *)
   let stats = stats_create suite.suite_name cases in

   (* Setup the status function *)
   let status =
      if Mcc_ncurses_display.display_active () then
         Some (fun buf text -> Mcc_ncurses_display.update_status text)
      else if !Rei_state.interactive then
         Some (fun buf text ->
            pp_print_string stdout (title_text text);
            pp_print_flush stdout ())
      else
         None
   in

   (* Attempt to build and run the cases. *)
   let _, stats = run_and_check_cases nbuf status stats cases in

   (* At this point, figure out exactly what happened here... *)
   let stats = stats_update_time stats in
      (* Make sure nbuf is flushed, because we're about to switch to ybuf *)
      pp_print_newline nbuf ();
      (match autorun with
         ARAuto (Some old_stats) ->
            display_delta_stats ybuf suite stats old_stats
       | ARAuto None
       | ARManual ->
            display_stats ybuf suite stats);
      stats


(* run_suite stdout autorun suite cases
   Runs a regression test suite.  The cases that should be run are listed
   in the cases argument.  This command catches exceptions, notably it will
   intercept ^C and interpret it as an aborted test run.  Assuming the test
   run is NOT interrupted, the statistics for the test run are returned;
   the statistics indicate total run time as well as a detail list of which
   test cases failed.  This function returns None if the user interrupts
   the test run with ^C.

   If the autorun flag is ARAuto, then this assumes we want an automated
   test run, in which case nothing is output unless there are differences
   from the test run currently saved in the suite (and if there are diffs,
   we will output a delta report between the existing stats in the test
   suite, and the new stats).  *)
let run_suite stdout autorun suite cases =
   (* The outermost exception block is responsible for catching ^C (which
      interrupts the test run), and any exception raised by our attempt to
      open a file for logging. *)
   try
      (* Create a temporary file for logging purposes *)
      let logfile = Filename.temp_file "log" ".rei" in

         (* At this point, the file has been created; the next try block does
            not do any exception handling except to make sure the temporary
            file is always deleted; it will re-raise any exceptions that may
            occur. *)
         try
            (* Open the log file for writing *)
            let logchan = open_out logfile in

               (* Yet ANOTHER try block; this one will make sure that logchan
                  gets closed in case we have a fatal error after this point. *)
               try
                  let logfmt = formatter_of_out_channel logchan in

                  (* Run the internal suite functions. The second stage is now
                     responsible for reading the final logfile and attaching it
                     to the run stats. *)
                  let stats = run_suite logfmt stdout autorun suite cases in

                  (* Flush and close the logfile at this point. *)
                  let () = pp_print_flush logfmt () in
                  let () = close_out logchan in

                  (* Read the logfile in, and update stats. *)
                  let log = read_file logfile in
                  let stats = stats_update_log stats log in

                  (* At this point, assume we can delete the logfile. *)
                  let _ = remove_temporary_files [logfile] in
                     Some stats
               with
                  e ->
                     (* Make sure that file handle gets closed... *)
                     close_out logchan;
                     raise e
         with
            e ->
               (* On any exception at this level, make sure the temporary
                  logfile has been deleted. *)
               ignore (remove_temporary_files [logfile]);
               raise e
   with
      Sys.Break ->
         (* User pressed ^C *)
         fprintf stdout "*** Test run was interrupted.@.";
         None
    | Sys_error msg ->
         (* Unexpected system error. *)
         fprintf stdout "*** Unexpected system error running test suite: %s@." msg;
         None
