(*
   Process test profiles
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mc_string_util
open Rei_type


(* profile_add profiles name cases
   Adds a completely new profile with the indicated list of test cases as
   the initial test profile.  Existing bindings for name will be removed. *)
val profile_add : profiles -> string -> string list -> profiles


(* profile_add_cases profiles name cases
   Appends test cases to an existing profile in the set of profiles.  If
   name is not yet bound, this is equivalent to profile_add.  *)
val profile_add_cases : profiles -> string -> string list -> profiles


(* profile_delete profiles name
   Deletes the named profile.  *)
val profile_delete : profiles -> string -> profiles


(* profile_delete_cases profiles name cases
   Deletes the named cases from the indicated test profile.  This does NOT
   remove the profile itself; any cases in the set difference of the current
   profile binding, and cases, will remain in the test profile.  If name is
   not bound, then this has no effect.  *)
val profile_delete_cases : profiles -> string -> string list -> profiles


(***  Print Test Profiles  ***)


(* pp_print_profile buf name cases
   Print out a single test profile.  *)
val pp_print_profile : formatter -> string -> StringSet.t -> unit


(* display_profiles_against_contrast buf display_profiles contrast_profiles
   Displays a summary of test profiles.  Profiles which have changed
   will be displayed in bold, if possible.  *)
val display_profiles_against_contrast : formatter -> profiles -> profiles -> unit


(* display_profiles buf profiles
   Displays a summary of test profiles.  *)
val display_profiles : formatter -> profiles -> unit
