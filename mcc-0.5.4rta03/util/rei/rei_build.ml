(*
   Build a test case or sequence of test cases
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Unix
open Format
open Rei_file
open Rei_suite_exn
open Rei_type
open Rei_util


(***  Special return code  ***)


(* build_internal_error
   The value returned to indicate internal build errors.  Set to 127.  *)
let build_internal_error = 127


(***  Build Test Targets  ***)


(* BuildException
   Exceptions that occur internally during the build are raised using
   this exception.  This exception should not escape; it should be
   caught by all interface functions.  The error message is included.  *)
exception BuildException of string


(* monitor_fun
   Type of a build monitor function.  See the description of the f
   argument on build_cases.  *)
type monitor_fun = formatter -> string -> int -> int -> unit


(* run_cons pipes command args
   Actually invoke cons.  Well, this runs any arbitrary command that is
   named in command really, but we'll call it cons :)  This is meant to
   be invoked indirectly through Rei_timer.start.  All execution paths
   will terminate in an exit call.  Also, this duplicates the pipe's W
   channel to stdout and stderr, so all output is sent through the pipe
   that is passed in.  The only exception is if an unexpected exception
   occurs; if an exception occurs, then this will write directly to the
   stderr channel.  *)
let run_cons (rd_pipe, wr_pipe) command args =
   (* Any exceptions are caught here. *)
   (try
      (* The child process runs cons.  Note that if any errors occur, this
         process will terminate with exit 127.  Both stdout and stderr are
         redirected to the write pipe.  The read pipe will be closed here. *)
      close rd_pipe;
      dup2 wr_pipe stdout;
      dup2 wr_pipe stderr;
      execvp command args
   with
      Unix_error (error, f, arg) ->
         fprintf err_formatter "*** cons process terminated with unix error: %a@." pp_print_unix_error (error, f, arg)
    | e ->
         fprintf err_formatter "*** cons process terminated with unknown exception: %s@." (Printexc.to_string e));

   (* If anything actually makes it this far, then force an exit. *)
   exit build_internal_error


(* monitor_cons pipes buf f run_info
   This is meant to be called by the parent process after Rei_timer.start
   call, to monitor a cons build.  The pipe corresponds to the pipe that
   cons is sending output through; the monitor will capture all output
   sent through this pipe and print it to the formatter named in buf. The
   monitor will also send all output to the monitor function f, if one is
   provided.  Once the pipe terminates, this will call Rei_timer.wait to
   collect the return status of the cons process.  *)
let monitor_cons (rd_pipe, wr_pipe) buf f run_info =
   (* Parent process; shovel output until child dies.  This process has
      control of the formatter channel, and is allowed to output to buf.
      The pipe is automatically closed when the child process dies because
      we explicitly close the write pipe here.  *)
   close wr_pipe;

   (* Setup to read from the pipe *)
   let size = 16384 in
   let strbuf = String.create size in
   let rec read_loop () =
      pp_print_flush buf ();
      let len = read rd_pipe strbuf 0 size in
         if len > 0 then begin
            (match f with
               None ->
                  ()
             | Some f ->
                  f buf strbuf 0 len);
            pp_print_string buf (String.sub strbuf 0 len);
            read_loop ()
         end
      in

      (* Read and echo data from rd_pipe until the pipe is closed. *)
      let () = read_loop () in

      (* Collect and return the exit status of the child process. *)
      let time, status = Rei_timer.wait run_info in
         fprintf buf "Build took %a seconds@." pp_print_time_seconds time;
         status


(* run_and_monitor_cons buf f command args
   This launches the cons process and waits for it to complete.  See the
   above two functions for details.  If the cons build is successful,
   then this will return the return status of cons.  *)
let run_and_monitor_cons buf f command args =
   (* Build the pipe which will be used to shovel data from cons to us. *)
   let pipes = pipe () in

   (* Start running (and timing) the build process. *)
   let run_cons () = run_cons pipes command args in
   let run_info = Rei_timer.start !Rei_state.default_build_timeout run_cons () in

   (* Monitor the cons process until it terminates. *)
   let status = monitor_cons pipes buf f run_info in
      status


(* build_cases_internal buf f targets
   Build all named targets in a list of test cases.  Returns the return
   code of the build process if the command was successfully run (0 if
   the build went okay, and nonzero if a build error occurred but the
   command otherwise ran normally).  If a return code is given, then the
   output will also be printed to the buffer given.  If an error occurs
   related to running the command, or the command returns due to a signal,
   this will raise a BuildException.  WARNING: This function uses return
   code 127 (build_internal_error) as a special value; the build process
   should not exit with this code.

   Due to the nature of the cons build, if errors occur we will not be
   able to describe exactly which test targets failed.  We pass the -k
   flag to cons to ensure that as many targets are built as possible, in
   the event of any errors.  If you want to determine if a single test
   case builds correctly, you must build with only that test case listed
   in targets.

   This is an internal function; errors are reported using BuildException.
   Note that normal build errors are reported by return code; the exception
   is only used to signal fatal conditions that prevented us from building
   (such as cons missing, or no test cases specified).  This function may
   also throw Unix_error.  This function cleans up any temporary files it
   creates, even if an exception is raised.

   Any messages are written out to the indicated buf (which may be a Y
   formatter).

   See build_cases for an explanation of the f argument.  *)
let build_cases_internal buf f targets =
   (* Make sure there is something to build... *)
   let () =
      if targets = [] then
         raise (BuildException "No test cases to build were specified")
   in

   (* Construct the command line to call cons *)
   let command = "cons" in
   let args = Array.of_list (command :: "-k" :: targets) in
   let num_targets = List.length targets in
   let () = fprintf buf "Building %d target%s@." num_targets (if num_targets > 1 then "s" else "") in

   (* Run cons and report its results *)
   let status = run_and_monitor_cons buf f command args in

      (* Return the appropriatesuccess/failure code. *)
      match status with
         WEXITED n
         when n = build_internal_error ->
            (* We treat exit code 127 as an exception; see warning *)
            raise (BuildException (sprintf "Prebuilds terminated abnormally, with exit code %d" n))
       | WEXITED n ->
            (* Normal return codes are passed back to the caller *)
            n
       | WSIGNALED n ->
            raise (BuildException (sprintf "Prebuilds terminated abnormally, with signal %d" n))
       | WSTOPPED n ->
            raise (BuildException (sprintf "Prebuilds terminated abnormally, with stop code %d" n))


(* build_cases buf f targets
   Build all named targets in a list of test cases.  Returns the return
   code of the build process if the command was successfully run (a return
   value of 0 indicates that all test cases were successfully built; any
   other return code indicates a build error).  Returns the value 127 (the
   value of build_internal_error) if internal error occurs when we invoke
   the build command.  The output of cons is printed to the given buffer,
   if there is any output.

   WARNING: This function uses return code 127 (build_internal_error) as a
   special value; the build process should not exit with this code.

   Due to the nature of the cons build, if errors occur we will not be
   able to describe exactly which test targets failed.  We pass the -k
   flag to cons to ensure that as many targets are built as possible, in
   the event of any errors.  If you want to determine if a single test
   case builds correctly, you must build with only that test case listed
   in targets.

   Exceptions are caught by this function and translated into messages
   (note that Sys.Break may be thrown by this function, however).  All
   output is written to the indicated buffer (which may be a Y buffer).
   If this function catches an exception, then it will return 127 (the
   value of build_internal_error).

   The argument f : formatter -> string -> int -> int -> unit warrants
   some explanation.  This is a function that will be called whenever
   new output is captured from the cons process.  It will eventually
   receive the entire contents of the cons output (assuming no abnormal
   termination).  It is passed the same formatter that is given to the
   build_runner function, but it will be run in the context of the pipe
   process.  This function can be used to monitor cons's output, and
   emit escape sequences that indicate current build status.  It does
   NOT need to emit the string buffer itself; the string buffer will
   be emitted to buf automatically by the pipe process.  If you do not
   care to monitor cons's output, pass in None.  *)
let build_cases buf f targets =
   try
      build_cases_internal buf f targets
   with
      BuildException msg ->
         fprintf buf "*** Build exception: %s@." msg;
         build_internal_error
    | Unix_error (error, f, arg) ->
         fprintf buf "*** Unix error while building cases: %a@." pp_print_unix_error (error, f, arg);
         build_internal_error


(* build_case buf target
   Builds a single test case.  This is no different from building a list
   all at once; we may as well defer to the general form here.  *)
let build_case buf f target =
   build_cases buf f [target]
