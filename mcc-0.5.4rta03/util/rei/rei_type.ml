(*
   Types for test cases and suites
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Mc_string_util


(* string_op
   Defines a component in a list of string concatenations.
      StrTarget            Insert the target name for the case.
      StrValue s           Use the indicated string value
 *)
type string_op =
   StrTarget
 | StrValue of string


(* case
   Define a single test case.
      case_name            Name of this test case.
      case_target          Build target (must be valid cons target)
      case_build           Expected BUILD return code.  If this is
                           zero, then we expect the target will be
                           constructed.  If nonzero, then we assume
                           the test case is supposed to cause a build
                           failure; in the latter case, we will never
                           attempt to evaluate the test case and will
                           report an error if the build SUCCEEDS.
      case_eval            Evaluation command for this test case.
                           Ignored if case_build is nonzero.
      case_return          Expected exit code for eval.  Ignored if
                           case_build is nonzero.
      case_output          Buffer containing expected standard out.
                           Ignored if case_build is nonzero.
      case_disable         If this is an empty string, then the test
                           case is enabled.  If this is NOT an empty
                           string, then the test case is disabled, and
                           the string contains a description of WHY
                           the test case is disabled.
 *)
type case =
   { case_name    :  string;
     case_target  :  string;
     case_build   :  int;
     case_eval    :  string_op list;
     case_return  :  int;
     case_output  :  string;
     case_disable :  string;
   }


(* profiles
   Test case profiles, keyed on a profile name.  The strings listed
   in each profile are the test case names to be run with the profile.
 *)
type profiles = StringSet.t StringTable.t


(* suite
   Define an entire test suite.
      suite_name           Name of this test suite.
      suite_cases          Table of all test cases, indexed by name.
      suite_profiles       Profiles saved with the test suite.
 *)
type suite =
   { suite_name      :  string;
     suite_cases     :  case StringTable.t;
     suite_profiles  :  profiles;
   }


(* stats
   Statistics that are collected for a test run.
      stats_suite_name     Name of the suite that gave these stats.
      stats_start_time     Time this test run was started at.
      stats_stop_time      Time this test run was stopped at.
      stats_log            Log data for the stats run.
      stats_current        Current case number.
      stats_total          Total number of cases.
      stats_success_runs   Names of cases successfully run.
      stats_failure_runs   Names of cases which failed.
   The success/failure runs track status information on the test run.
   The success run stores a float which indicates the total runtime
   of the test case (in seconds), and the failure run stores a string
   message that describes what the error was.
 *)
type stats =
   { stats_suite_name   :  string;
     stats_start_time   :  float;
     stats_stop_time    :  float;
     stats_log          :  string;
     stats_current      :  int;
     stats_total        :  int;
     stats_success_runs :  float StringTable.t;
     stats_failure_runs :  string StringTable.t;
   }


