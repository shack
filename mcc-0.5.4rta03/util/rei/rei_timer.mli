(*
   Provide timing services for children processes
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* run_info
   Information about a child process spawned off by the timer.  Contains
   the rusage data from before the child was started, as well as the PID
   of the child.  *)
type run_info


(* start timeout f arg
   Runs (f arg) in a child process, with an rlimit on CPU usage.  The CPU
   limit is set to the indicated timeout (in seconds).  The child process
   will be terminated without mercy if it exceeds this limit.  This call
   terminates immediately; therefore f runs in parallel with the caller.
   If you want the parent to block, you should use run (see below).

   Note that f MUST terminate in the child process context; if it returns
   a result code, then that result code is used to terminate the child;
   if it raises an exception, then the exception will be caught here to
   force the child to terminate.  f is, of course, welcome to terminate
   on its own; however under no circumstance will f be able to escape from
   this start call.

   Note that while f's exceptions will be caught and handled internally,
   it is still possible for the PARENT to terminate with exception.  This
   can only happen before the fork; therefore if start raises an exception
   you should assume f was never executed.

   Note that the child process may emit data to its stderr channel (beyond
   anything that f already emits); if you do not want this, you need to
   redirect the stderr channel at the beginning of f.

   STATE FRAGMENT WARNING:  f is running in a different process from the
   caller; therefore, the program state (including formatter and ncurses
   state) is not shared between f and the caller.  You are discouraged
   from sending output to the caller's standard output from within f.  DO
   NOT OUTPUT TO THE CALLER'S STDOUT IF THE CALLER IS IN NCURSES MODE!

   TIMER WARNING:  The timer module provides child timing information once
   the child terminates; however, due to inadequacies in rusage, this info
   will be inaccurate if multiple child processes are spawned during the
   timing session;  you will get the aggregate time of all children, not
   the runtime of this particular child.

   If successful, the PID of the new child process and the rusage info
   from immediately before the process was started are returned.  If an
   error occurs, then a Unix_error may be raised.  If an exception is
   raised, assume the child process was not started.  *)
val start : int -> ('a -> Unix.process_status) -> 'a -> run_info


(* wait run_info
   Waits for a child process (started with start, above) to complete. This
   call will block until the child's exit code is available. This may raise
   a Unix_error exception if the PID has already been collected.  If this
   is successful, then the total runtime (in seconds) and return status of
   the child process are returned.  *)
val wait : run_info -> float * Unix.process_status


(* run timeout f arg
   Equivalent to running start, immediately followed by run.  See the
   warnings on start; they apply to this function as well.  If this is
   successful, then the total runtime (in seconds) and return status of
   the child process are returned.  *)
val run : int -> ('a -> Unix.process_status) -> 'a -> float * Unix.process_status
