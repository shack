/*
   Parse a test suite file
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


/***  Header  ***/


%{
   open Symbol
   open Mc_string_util
   open Rei_case
   open Rei_profiles
   open Rei_suite_exn
   open Rei_type
%} /* End header */


/***  Token Definitions  ***/


/* Symbols */
%token <Rei_suite_exn.pos> TokCaret
%token <Rei_suite_exn.pos> TokColon
%token <Rei_suite_exn.pos> TokComma
%token <Rei_suite_exn.pos> TokDollar
%token <Rei_suite_exn.pos> TokEqual
%token <Rei_suite_exn.pos> TokLBrack
%token <Rei_suite_exn.pos> TokRBrack
%token <Rei_suite_exn.pos> TokSemi

/* Blocks */
%token <float         * Rei_suite_exn.pos> TokFloat
%token <Symbol.symbol * Rei_suite_exn.pos> TokIdent
%token <int           * Rei_suite_exn.pos> TokInt
%token <string        * Rei_suite_exn.pos> TokString

/* Keywords */
%token <Rei_suite_exn.pos> TokBuild
%token <Rei_suite_exn.pos> TokCase
%token <Rei_suite_exn.pos> TokDisable
%token <Rei_suite_exn.pos> TokEnabled
%token <Rei_suite_exn.pos> TokEval
%token <Rei_suite_exn.pos> TokFailure
%token <Rei_suite_exn.pos> TokFalse
%token <Rei_suite_exn.pos> TokLog
%token <Rei_suite_exn.pos> TokName
%token <Rei_suite_exn.pos> TokOutput
%token <Rei_suite_exn.pos> TokProfile
%token <Rei_suite_exn.pos> TokReturn
%token <Rei_suite_exn.pos> TokStart
%token <Rei_suite_exn.pos> TokStats
%token <Rei_suite_exn.pos> TokStop
%token <Rei_suite_exn.pos> TokSuccess
%token <Rei_suite_exn.pos> TokSuite
%token <Rei_suite_exn.pos> TokTarget
%token <Rei_suite_exn.pos> TokTotal
%token <Rei_suite_exn.pos> TokTrue

/* End-of-file */
%token <Rei_suite_exn.pos> TokEof


/***  Start Productions  ***/


%start   suite
%start   stats
%type    <Rei_type.suite> suite
%type    <Rei_type.stats> stats


%%


/***  Toplevel Suite Productions  ***/


suite:
   TokSuite TokString TokSemi cases profiles TokEof {
      let name, _ = $2 in
      let cases = $4 in
      let profiles = $5 in
         { suite_name      = name;
           suite_cases     = cases;
           suite_profiles  = profiles;
         }
   }
   ;


cases:
   cases case {
      let cases = $1 in
      let case, pos = $2 in
         if StringTable.mem cases case.case_name then
            raise (SuiteException (exp_pos pos, StringError "duplicate case name"));
         case_add cases case.case_name case
   }
 | { StringTable.empty }
   ;


profiles:
   profiles profile {
      let profiles = $1 in
      let profile_name, profile_list, pos = $2 in
         if StringTable.mem profiles profile_name then
            raise (SuiteException (exp_pos pos, StringError "duplicate profile name"));
         profile_add profiles profile_name profile_list
   }
 | { StringTable.empty }
   ;


/***  Test Case Processing  ***/


case:
   TokCase TokString TokColon case_values {
      let case = $4 in
      let name, pos = $2 in
      let pos = union_pos $1 pos in
         { case with case_name = name }, pos
   }
   ;


case_values:
   case_values TokTarget TokEqual TokString TokSemi {
      let case = $1 in
      let value, _ = $4 in
         { case with case_target = value }
   }
 | case_values TokBuild TokEqual TokInt TokSemi {
      let case = $1 in
      let value, _ = $4 in
         { case with case_build = value }
   }
 | case_values TokEval TokEqual string_op TokSemi {
      let case = $1 in
      let value = $4 in
         { case with case_eval = value }
   }
 | case_values TokOutput TokEqual TokString TokSemi {
      let case = $1 in
      let value, _ = $4 in
         { case with case_output = value }
   }
 | case_values TokReturn TokEqual TokInt TokSemi {
      let case = $1 in
      let value, _ = $4 in
         { case with case_return = value }
   }
 | case_values TokDisable TokEqual TokString TokSemi {
      let case = $1 in
      let value, _ = $4 in
         { case with case_disable = value }
   }
 | case_values TokEnabled TokSemi {
      let case = $1 in
         { case with case_disable = "" }
   }
 | { new_case "empty" }
   ;


string_op:
   string_value TokCaret string_op { $1 :: $3 }
 | string_value { [$1] }
   ;


string_value:
   TokDollar TokTarget {
      StrTarget
   }
 | TokString {
      let value, _ = $1 in
         StrValue value
   }
   ;


/***  Profiles  ***/


profile:
   TokProfile TokString TokEqual TokLBrack string_list_opt TokRBrack TokSemi {
      let profile_name, _ = $2 in
      let profile_list = $5 in
      let pos = union_pos $1 $7 in
         profile_name, profile_list, pos
   }
   ;


string_list_opt:
   string_list { $1 }
 | { [] }
   ;


string_list:
   TokString TokSemi string_list {
      let s, _ = $1 in
      let ls = $3 in
         s :: ls
   }
 | TokString {
      let s, _ = $1 in
         [s]
   }
   ;


/***  Statistics  ***/


stats:
   TokStats TokColon stats_line TokEof { $3 }
   ;


stats_line:
   stats_line TokName TokEqual TokString TokSemi {
      let stats = $1 in
      let value, _ = $4 in
         { stats with stats_suite_name = value }
   }
 | stats_line TokStart TokEqual TokFloat TokSemi {
      let stats = $1 in
      let value, _ = $4 in
         { stats with stats_start_time = value }
   }
 | stats_line TokStop TokEqual TokFloat TokSemi {
      let stats = $1 in
      let value, _ = $4 in
         { stats with stats_stop_time = value }
   }
 | stats_line TokLog TokEqual TokString TokSemi {
      let stats = $1 in
      let value, _ = $4 in
         { stats with stats_log = value }
   }
 | stats_line TokTotal TokEqual TokInt TokSemi {
      let stats = $1 in
      let value, _ = $4 in
         { stats with stats_total = value }
   }
 | stats_line TokSuccess TokEqual TokLBrack string_float_list_opt TokRBrack TokSemi {
      let stats = $1 in
      let value = $5 in
      let value = List.fold_left (fun table (name, f) ->
         StringTable.add table name f) StringTable.empty value
      in
         { stats with stats_success_runs = value }
   }
 | stats_line TokFailure TokEqual TokLBrack string_string_list_opt TokRBrack TokSemi {
      let stats = $1 in
      let value = $5 in
      let value = List.fold_left (fun table (name, msg) ->
         StringTable.add table name msg) StringTable.empty value
      in
         { stats with stats_failure_runs = value }
   }
 | {
      { stats_suite_name   = "";
        stats_start_time   = 0.;
        stats_stop_time    = 0.;
        stats_log          = "";
        stats_current      = 0;
        stats_total        = 0;
        stats_success_runs = StringTable.empty;
        stats_failure_runs = StringTable.empty;
      }
   }


string_float_list_opt:
   string_float_list { $1 }
 | { [] }
   ;


string_float_list:
   TokString TokComma TokFloat TokSemi string_float_list {
      let s, _ = $1 in
      let f, _ = $3 in
      let ls = $5 in
         (s, f) :: ls
   }
 | TokString TokComma TokFloat {
      let s, _ = $1 in
      let f, _ = $3 in
         [s, f]
   }
   ;


string_string_list_opt:
   string_string_list { $1 }
 | { [] }
   ;


string_string_list:
   TokString TokComma TokString TokSemi string_string_list {
      let s1, _ = $1 in
      let s2, _ = $3 in
      let ls = $5 in
         (s1, s2) :: ls
   }
 | TokString TokComma TokString {
      let s1, _ = $1 in
      let s2, _ = $3 in
         [s1, s2]
   }
   ;
