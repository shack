(*
   File utilities for capturing output and simple file I/O
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Unix


(***  Clearing Temporary Files  ***)


(* remove_temporary_files list
   Remove temporary files; this catches any Unix_error that may be thrown,
   and ignores the error; since this is almost always on an exit/cleanup
   path, this is reasonable.  Be aware though that this function will fail
   silently, unless you check the return code.  The number of cases that
   were successfully deleted is returned.  *)
let rec remove_temporary_files list =
   match list with
      file :: list ->
         let count =
            try
               unlink file;
               1
            with
               Unix_error _ ->
                  0
         in
            count + remove_temporary_files list
    | [] ->
         0


(***  Output Capture  ***)


(* redirect_output stdout_name stderr_name
   Redirects stdout and stderr to the named file(s).  If the filenames match
   then only one descriptor is opened for writing, and the same descriptor is
   duplicated to both stdout and stderr.  Otherwise, separate files are made
   to capture each output stream independently.  If something goes wrong,
   this will raise Unix_error.  Note that the file descriptors opened will
   not be closed (except implicitly when the process terminates).

   Note: the heuristic for determining if the filenames are the same is a
   simple string comparison;  so it will miss things like "/foo" vs. "//foo".
   If you intend to only use one descriptor, you should make sure you give
   the same string for both arguments.  *)
let redirect_output stdout_name stderr_name =
   let do_open name = openfile name [O_WRONLY; O_CREAT; O_TRUNC] 0o644 in
   let stdout_desc, stderr_desc =
      if stdout_name = stderr_name then
         (* Only create one descriptor, and clone it twice. *)
         let desc = do_open stdout_name in
            desc, desc
      else
         (* Create separate descriptors for each filename. *)
         do_open stdout_name, do_open stderr_name
   in
      dup2 stdout_desc stdout;
      dup2 stderr_desc stderr


(***  Simple File I/O  ***)


(* unix_close_silently descr
   Closes a descriptor; catches any Unix_error and ignores it.  Useful along
   exception paths, when we don't care if the close is successful or not. *)
let unix_close_silently descr =
   try
      Unix.close descr
   with
      Unix_error _ ->
         ()


(* read_file filename
   Read the entire contents of a file into a string buffer.  If an
   exception occurs, then this will raise a Unix error.  *)
let read_file filename =
   let stats = stat filename in
   let size = stats.st_size in
   let buf  = Buffer.create size in
   let str_buf = String.create size in
   let inx = Unix.openfile filename [Unix.O_RDONLY] 0 in
      (* From this point, make sure the descriptor gets closed. *)
      try
         let rec read_loop () =
            let len = Unix.read inx str_buf 0 size in
               if len > 0 then begin
                  Buffer.add_substring buf str_buf 0 len;
                  read_loop ()
               end else begin
                  Unix.close inx;
                  Buffer.contents buf
               end
         in
            read_loop ()
      with
         e ->
            unix_close_silently inx;
            raise e


(* write_file filename buffer
   Write a string buffer out to a file.  If an exception occurs, then
   this will raise a Unix error.  *)
let write_file filename buffer =
   let size = String.length buffer in
   let out = Unix.openfile filename [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC] 0o644 in
      (* From this point, make sure the descriptor gets closed. *)
      try
         ignore (Unix.write out buffer 0 size);
         Unix.close out
      with
         e ->
            unix_close_silently out;
            raise e


(***  Filename Utilities  ***)


(* trim_extension filename
   Removes the extension component from a filename, if present.
   The returned string is the original string if the final file
   component does not contain a dot.  *)
let trim_extension filename =
   if filename = "" then
      ""
   else
      let rec scan_for_last index ch =
         let start_index =
            if index = 0 then
               0
            else
               index + 1
         in
            try
               let index = String.index_from filename start_index ch in
                  scan_for_last index ch
            with
               Not_found ->
                  index
      in
      let last_slash = scan_for_last 0 '/' in
      let last_dot = scan_for_last last_slash '.' in
         if filename.[last_dot] = '.' then
            String.sub filename 0 last_dot
         else
            filename
