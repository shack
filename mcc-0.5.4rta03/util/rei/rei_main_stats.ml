(*
   Main program for REI -- stats editor loop
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Mc_string_util
open Rei_case
open Rei_file
open Rei_main_case
open Rei_stats
open Rei_type
open Rei_util


(***  Helper Functions  ***)


(* no_stats ()
   Report that no stats have been loaded yet.  *)
let no_stats () =
   printf "No run stats have been loaded yet. Either type \"run\" from the main@.";
   printf "prompt to generate run stats, or type \"read\" from the stats prompt@.";
   printf "to read in previously saved run stats.@."


(***  Stats Commands  ***)


let cmd_cancel = function
   [] ->
      Exit ()
 | _ ->
      bad_arguments ();
      Continue

let cmd_cancel_help =
   "\nExit the stats loop and return to the main prompt."


let cmd_edit args =
   match !current_stats with
      Some stats ->
         let cases = (!current_suite).suite_cases in
         let fails = stats.stats_failure_runs in
         let cases = map_cases cases fails in
            run_case_edit_select cases args
    | None ->
         no_stats ()

let cmd_edit_help =
   "[<name>]" ^
   "\nEdits an existing test case in the suite, that failed in the last test run." ^
   " In this context, if you refer to a test case by number, the index will map to" ^
   " one of the failed test cases. Use the \"list\" command to see the numbers of" ^
   " failed test cases. If name is omitted, a menu is presented which allows the user" ^
   " to select from the failed test cases."


let cmd_list = function
   [] ->
      begin
      match !current_stats with
         Some stats ->
            display_stats_against_contrast std_formatter !current_suite !last_saved_suite stats
       | None ->
            no_stats ()
      end
 | _ ->
      bad_arguments ()

let cmd_list_help =
   "\nDisplay the current statistics.  This includes information on the" ^
   " test cases which failed in the corresponding test run."


let cmd_log args =
   match !current_stats with
      Some stats ->
         begin
         match args with
            [] ->
               if go_curses () then
                  Mcc_ncurses_display.in_display (fun () ->
                     Mcc_ncurses_display.text_viewer "Test Run Log" stats.stats_log) ()
               else
                  printf "Log from last test run:@.%s@." stats.stats_log
          | [name] ->
               if ok_to_overwrite_file name then begin
                  printf "Writing stats log to file \"%s\".@." name;
                  write_file name stats.stats_log
               end
          | _ ->
               bad_arguments ()
         end
    | None ->
         no_stats ()

let cmd_log_help =
   "[<filename>]" ^
   "\nDisplays the log from the last test run. If a filename is specified, then" ^
   " the log is written to the named file. Otherwise, the log is written to the" ^
   " standard output channel."


let cmd_read = function
   [filename] ->
      printf "Reading stats from file \"%s\".@." filename;
      current_stats := Some (read_stats filename)
 | _ ->
      bad_arguments ()

let cmd_read_help =
   "<filename>" ^
   "\nReads stats from the named file."


let cmd_write = function
   [filename] ->
      begin
      match !current_stats with
         Some stats ->
            if ok_to_overwrite_file filename then begin
               printf "Writing last run stats to file \"%s\".@." filename;
               write_stats filename stats
            end
       | None ->
            no_stats ()
      end
 | _ ->
      bad_arguments ()

let cmd_write_help =
   "<filename>" ^
   "\nWrites the current run stats out to the named file."


let commands =
   ["cancel",  cmd_cancel,          cmd_cancel_help;
    "edit",    continue cmd_edit,   cmd_edit_help;
    "list",    continue cmd_list,   cmd_list_help;
    "ls",      continue cmd_list,   cmd_list_help;
    "log",     continue cmd_log,    cmd_log_help;
    "read",    continue cmd_read,   cmd_read_help;
    "write",   continue cmd_write,  cmd_write_help]


(* run_stats
   Runs the loop for interacting with stats saved from a previous
   test run.  The user may exit this loop by pressing ^D.  This
   catches any exceptions that may have been thrown by the read-
   eval-print loop, above.  *)
let run_stats stats =
   current_stats := stats;
   run_loop "stats> " commands cmd_cancel
