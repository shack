(*
   Process test cases
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Mcc_command_util
open Mc_string_util
open Rei_type
open Rei_util


(***  Test Case Creation  ***)


(* new_case name
   Constructs a new test case.  *)
let new_case name =
   { case_name    = name;
     case_target  = "";
     case_build   = 0;
     case_eval    = [];
     case_return  = 0;
     case_output  = "";
     case_disable = "";
   }


(* case_enabled case
   Returns true if the test case indicated is enabled.  *)
let case_enabled case =
   case.case_disable = ""


(***  Test Case Lookup  ***)


(* hyphenated_list name
   Returns the lower and upper bounds of a hyphenated list, or equal
   bounds if a singleton integer is given, or None if neither apply.  *)
let hyphenated_list name =
   try
      let index = String.index name '-' in
      let lower = String.sub name 0 index in
      let upper = String.sub name (index + 1) ((String.length name) - index - 1) in
      let lower = int_of_string lower in
      let upper = int_of_string upper in
         Some (lower, upper)
   with
      Not_found
    | Invalid_argument _
    | Failure _ ->
         try
            let i = int_of_string name in
               Some (i, i)
         with
            Invalid_argument _
          | Failure _ ->
               None


(* lookup_case cases name
   lookup_case_prof cases profiles name
   Attempt to lookup cases using the given descriptor.  Returns the test
   cases if matches are found, or empty list if no matching test cases
   could be found.  Priority is always given to a single test case which
   exactly matches the given name.  *)
let lookup_case_prof cases profiles name =
   if StringTable.mem cases name then
      [StringTable.find cases name]
   else
      (* We didn't find an exact match in case names; try hyphenated list *)
      match hyphenated_list name with
         Some (lower, upper) ->
            let cases, _ = StringTable.fold (fun (cases, count) _ case ->
               let cases =
                  if count >= lower && count <= upper then
                     case :: cases
                  else
                     cases
               in
                  cases, count + 1) ([], Rei_state.first_case_number) cases
            in
               List.rev cases
       | None ->
            (* Final attempt; see if this matched a profile name *)
            if StringTable.mem profiles name then
               let profile = StringTable.find profiles name in
               let cases = StringSet.fold (fun new_cases name ->
                  if StringTable.mem cases name then
                     StringTable.find cases name :: new_cases
                  else
                     new_cases) [] profile
               in
                  List.rev cases
            else
               []

let lookup_case cases name =
   lookup_case_prof cases StringTable.empty name


(* lookup_single_case cases name
   Forces only one case to match.  If multiple matches are found, then
   None is returned.  *)
let lookup_single_case cases name =
   match lookup_case cases name with
      [case] ->
         Some case
    | _ ->
         None


(* lookup_case_by_name cases name
   Lookup a case based on a specific name. This does not perform any glob
   pattern matching, and is the preferred form when you need to check for
   an exact name in the case list. *)
let lookup_case_by_name cases name =
   try
      Some (StringTable.find cases name)
   with
      Not_found ->
         None


(***  Adding and Deleting Test Cases  ***)


(* case_add cases name case
   Adds a new case.  This forces the case_name in the case to match the
   name here, therefore you should prefer this function over calling the
   StringTable.add function directly.  The name is not interpreted as a
   glob pattern; this function will overwrite existing keys.  *)
let case_add cases name case =
   let case = { case with case_name = name } in
      StringTable.add cases name case


(* case_delete cases name
   Deletes a case, if it exists in the cases list.  The name here is not
   interpreted as a glob pattern.  The cases list remains unmodified if
   the name is not bound.  *)
let case_delete cases name =
   StringTable.remove cases name


(***  Case Name Map  ***)


(* map_cases cases names
   map_cases_set cases names
   Maps the case names in the given set to the real case data structures.
   If a case name cannot be found, then a new (empty) case is created for it.
   Otherwise, the name is mapped to the resolved case.  This does not do any
   glob-translation on the names.  *)
let map_cases cases names =
   StringTable.fold (fun new_cases name _ ->
      let case =
         if StringTable.mem cases name then
            StringTable.find cases name
         else
            new_case name
      in
         StringTable.add new_cases name case) StringTable.empty names

let map_cases_set cases names =
   StringSet.fold (fun new_cases name ->
      let case =
         if StringTable.mem cases name then
            StringTable.find cases name
         else
            new_case name
      in
         StringTable.add new_cases name case) StringTable.empty names


(***  Test Case Printer  ***)


(* case_printer
   Determine how the case will be printed.  Used internally only.  *)
type case_printer =
   PrintNoOutput
 | PrintBriefOutput
 | PrintFullOutput
 | PrintToFile


(* open_if b
   close_if b
   Helpers for the printers.  Return the escape sequences for opening and
   closing bold text if the flag is true, or empty string otherwise.  An
   empty string is always returned if either escape sequence is not avail.
   on this platform.  *)
let open_if b =
   match b, tcap_set_bold, tcap_clear_attr with
      true, Some s, Some _ ->
         s
    | _ ->
         ""

let close_if b =
   match b, tcap_set_bold, tcap_clear_attr with
      true, Some _, Some s ->
         s
    | _ ->
         ""


(* pp_print_case buf display_case contrast_case printer
   Print out a test case.  The printer option determines how output will
   be displayed (whether it will be printed in concise form on the term,
   or in a full parsable format to a file).  *)
let pp_print_case buf display_case contrast_case printer =
   let disable_diff = display_case.case_disable <> contrast_case.case_disable in
   let target_diff  = display_case.case_target  <> contrast_case.case_target  in
   let build_diff   = display_case.case_build   <> contrast_case.case_build   in
   let eval_diff    = display_case.case_eval    <> contrast_case.case_eval    in
   let return_diff  = display_case.case_return  <> contrast_case.case_return  in
   let output_diff  = display_case.case_output  <> contrast_case.case_output  in
      fprintf buf "@[<v 3>case \"%s\":" (String.escaped display_case.case_name);
      if case_enabled display_case then
         fprintf buf "@ %senabled%s;" (open_if disable_diff) (close_if disable_diff)
      else
         fprintf buf "@ %sdisable = \"%s\"%s;" (open_if disable_diff)
            (String.escaped display_case.case_disable) (close_if disable_diff);
      fprintf buf "@ %starget  = \"%s\"%s;" (open_if target_diff)
         (String.escaped display_case.case_target) (close_if target_diff);
      fprintf buf "@ %sbuild   = %d%s;" (open_if build_diff)
         display_case.case_build (close_if build_diff);
      fprintf buf "@ %seval    = %a%s;" (open_if eval_diff)
         pp_print_string_op display_case.case_eval (close_if eval_diff);
      fprintf buf "@ %sreturn  = %d%s;" (open_if return_diff)
         display_case.case_return (close_if return_diff);
      (match printer with
         PrintNoOutput
       | PrintBriefOutput ->
            fprintf buf "@ %soutput  = <omitted>%s;" (open_if output_diff) (close_if output_diff);
       | PrintFullOutput
       | PrintToFile ->
            fprintf buf "@ %soutput  = \"%s\"%s;" (open_if output_diff)
               (String.escaped display_case.case_output) (close_if output_diff));
      fprintf buf "@]"


(* display_case_int buf display_case contrast_case arg
   Displays information about a test case.  Similar to above, but uses
   a string argument.  This is the internal function that actually reads
   the string argument; the contrast case is optionally given as well.
   If no contrast case should be used, set it equal to display_case.  *)
let display_case_int buf display_case contrast_case arg =
   if arg = "-all" then
      pp_print_case buf display_case contrast_case PrintFullOutput
   else if arg = "" then
      pp_print_case buf display_case contrast_case PrintNoOutput
   else
      fprintf buf "This command accepts either no argument, or \"-all\".";
   fprintf buf "@."


(* display_case buf case arg
   Displays information about a test case.  Similar to the above version
   of pp_print_string, but uses a string argument to specify the printer.  *)
let display_case buf case arg =
   display_case_int buf case case arg


(* display_case_against_contrast buf display_case contrast_case arg
   Displays information about a test case (the FIRST argument), given the
   contrast case (the SECOND argument).  No information about the second
   case is displayed; however, discrepancies in the second case are used
   to mark lines in the first case which differ.  The string argument has
   similar interpretation as with display_case.  *)
let display_case_against_contrast buf display_case contrast_case arg =
   display_case_int buf display_case contrast_case arg


(* pp_print_case buf case
   This is the interface used by the suite file writer.  It emits the
   full output in a parseable format.  *)
let pp_print_case buf case =
   pp_print_case buf case case PrintToFile


(***  Display Multiple Test Cases  ***)


(* display_case_summary_int buf header display_cases contrast_cases
   Displays a list of test cases.  The actual cases are NOT displayed;
   however some summary information that can be displayed on one line
   will be displayed.  This prints the cases in a vbox, with the given
   heading.  Note: to disable contrast cases, pass None in for the
   contrast set.  *)
let display_case_summary_int buf header display_cases contrast_cases =
   fprintf buf "@[<v 3>%s (%d cases):" header (StringTable.cardinal display_cases);
   ignore (StringTable.fold (fun count name case ->
      let bold_text =
         match contrast_cases with
            None ->
               false
          | Some cases ->
               if StringTable.mem cases name then
                  (StringTable.find cases name) <> case
               else
                  true
      in
      fprintf buf "@ %sCase #%03d: \"%s\"" (open_if bold_text) count name;
      if not (case_enabled case) then
         fprintf buf " (disabled: %s)" case.case_disable;
      fprintf buf "%s" (close_if bold_text);
      count + 1) Rei_state.first_case_number display_cases);
   fprintf buf "@]@."


(* display_case_summary buf header cases
   Displays the case summary, without using contrasts.  *)
let display_case_summary buf header cases =
   display_case_summary_int buf header cases None


(* display_case_summary_against_contrast
      buf header display_cases contrast_cases
   Similar to display_case_against_contrast; this takes a list of cases
   to actually list, along with a list of cases to compare against. If
   a display case differs from the corresponding entry in the contrast
   list, *or* if no entry exists for the display case in the contrast
   list, then it is displayed in bold.  *)
let display_case_summary_against_contrast buf header display_cases contrast_cases =
   display_case_summary_int buf header display_cases (Some contrast_cases)
