(*
   Provide timing services for children processes
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Unix
open Format
open Mcc_rusage
open Rei_suite_exn


(* run_info
   Information about a child process spawned off by the timer.  Contains
   the rusage data from before the child was started, as well as the PID
   of the child.  *)
type run_info = int * rusage


(* start timeout f arg
   Runs (f arg) in a child process, with an rlimit on CPU usage.  The CPU
   limit is set to the indicated timeout (in seconds).  The child process
   will be terminated without mercy if it exceeds this limit.  This call
   terminates immediately; therefore f runs in parallel with the caller.
   If you want the parent to block, you should use run (see below).

   Note that f MUST terminate in the child process context; if it returns
   a result code, then that result code is used to terminate the child;
   if it raises an exception, then the exception will be caught here to
   force the child to terminate.  f is, of course, welcome to terminate
   on its own; however under no circumstance will f be able to escape from
   this start call.

   Note that while f's exceptions will be caught and handled internally,
   it is still possible for the PARENT to terminate with exception.  This
   can only happen before the fork; therefore if start raises an exception
   you should assume f was never executed.

   Note that the child process may emit data to its stderr channel (beyond
   anything that f already emits); if you do not want this, you need to
   redirect the stderr channel at the beginning of f.

   STATE FRAGMENT WARNING:  f is running in a different process from the
   caller; therefore, the program state (including formatter and ncurses
   state) is not shared between f and the caller.  You are discouraged
   from sending output to the caller's standard output from within f.  DO
   NOT OUTPUT TO THE CALLER'S STDOUT IF THE CALLER IS IN NCURSES MODE!

   TIMER WARNING:  The timer module provides child timing information once
   the child terminates; however, due to inadequacies in rusage, this info
   will be inaccurate if multiple child processes are spawned during the
   timing session;  you will get the aggregate time of all children, not
   the runtime of this particular child.

   If successful, the PID of the new child process and the rusage info
   from immediately before the process was started are returned.  If an
   error occurs, then a Unix_error may be raised.  If an exception is
   raised, assume the child process was not started.  *)
let start timeout f arg =
   (* Get the current rusage of children before we start a new child. *)
   let rusage = getrusage_time RUSAGE_CHILDREN in

   (* Create a new child process. *)
   let pid = fork () in
      if pid = 0 then begin
         (* We are the child process; evaluate the function.  WARNING:
            This process cannot return to the callee, so we must catch
            any exceptions here. *)
         (try
            (* Set the rlimit for this process *)
            setrlimit_time timeout;
            match f arg with
               WEXITED n ->
                  exit n
             | WSIGNALED n ->
                  exit (128 + n)
             | WSTOPPED n ->
                  exit 127
         with
            (* Exceptions in the child must not be allowed to propagate
               all the way up to the callee.  *)
            Unix_error (error, f, arg) ->
               fprintf err_formatter "*** Unix error in timer child: %a@." pp_print_unix_error (error, f, arg)
          | e ->
               fprintf err_formatter "*** Unexpected exception in timer child: %s@." (Printexc.to_string e));
         exit 127
      end else begin
         (* This is the parent process. Return the PID and rusage data. *)
         pid, rusage
      end


(* wait run_info
   Waits for a child process (started with start, above) to complete. This
   call will block until the child's exit code is available. This may raise
   a Unix_error exception if the PID has already been collected.  If this
   is successful, then the total runtime (in seconds) and return status of
   the child process are returned.  *)
let wait (pid, rusage) =
   (* Wait for the PID to terminate, and collect its return status.
      Note that the waitpid is a blocking call by default, therefore
      this is not a busy loop.  *)
   let rec wait_for_event () =
      let pid, status = waitpid [] pid in
         if pid = 0 then
            wait_for_event ()
         else
            status
   in
   let status = wait_for_event () in

   (* Get the latest rusage numbers, so we can compute process total
      running time.  *)
   let rusage' = getrusage_time RUSAGE_CHILDREN in
   let tsec, tusec   = total_rusage rusage  in
   let tsec', tusec' = total_rusage rusage' in
   let tsec  = float_of_int tsec  +. (float_of_int tusec  /. 1000000.) in
   let tsec' = float_of_int tsec' +. (float_of_int tusec' /. 1000000.) in
   let deltasec = tsec' -. tsec in
      deltasec, status


(* run timeout f arg
   Equivalent to running start, immediately followed by run.  See the
   warnings on start; they apply to this function as well.  If this is
   successful, then the total runtime (in seconds) and return status of
   the child process are returned.  *)
let run timeout f arg =
   let run_info = start timeout f arg in
      wait run_info
