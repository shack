(*
 * Lexer for Ocaml type definitions.
 *
 * Temp: Eventually, we should use the AML lexer.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

{
open Tsgen_parser
open Tsgen_pos
open Tsgen_type

(*
 * File position.
 *)
let current_line = ref 1
let current_schar = ref 0

(*
 * Advance a line.
 *)
let set_next_line lexbuf =
   incr current_line;
   current_schar := Lexing.lexeme_end lexbuf

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
   let line = !current_line in
   let schar = Lexing.lexeme_start lexbuf - !current_schar in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let file = current_file () in
   let pos = file, line, schar, line, echar in
      set_current_position pos;
      pos

(*
 * Provide a buffer for building strings.
 *)
let stringbuf = Buffer.create 32
let string_start = ref (0, 0)

}

(*
 * Regular expressions.
 *)
let name_prefix = ['_' 'A'-'Z' 'a'-'z']
let name_suffix = ['_' 'A'-'Z' 'a'-'z' '0'-'9']
let basic_name = name_prefix name_suffix*
let name = basic_name '\''*
let quoted_name = '\'' basic_name

(*
 * Main lexer.
 *)
rule main = parse 
     [' ' '\t']+        { main lexbuf }
   | '\n'               { set_next_line lexbuf; main lexbuf }
   | '_'                { TokId ("_", set_lexeme_position lexbuf) }
   | quoted_name        { let pos = set_lexeme_position lexbuf in
                          let s = Lexing.lexeme lexbuf in
                           TokQuotedId (String.sub s 1 (String.length s - 1), pos)
                        }
   | name               { let pos = set_lexeme_position lexbuf in
                          let id = Lexing.lexeme lexbuf in
                          match id with 
                             "type" ->
                              TokType pos
                           | "and" ->
                              TokAnd pos
                           | "of" ->
                              TokOf pos
                           | _ ->
                              TokId (id, pos)
                        }

   | "(*"               { comment lexbuf; main lexbuf }

   (*
    * Special chars.
    *)
   | "="                { let pos = set_lexeme_position lexbuf in TokEq pos }
   | "*"                { let pos = set_lexeme_position lexbuf in TokStar pos }
   | "|"                { let pos = set_lexeme_position lexbuf in TokPipe pos }
   | ";"                { let pos = set_lexeme_position lexbuf in TokSemi pos }
   | ":"                { let pos = set_lexeme_position lexbuf in TokColon pos }
   | ","                { let pos = set_lexeme_position lexbuf in TokComma pos }
   | "{"                { let pos = set_lexeme_position lexbuf in TokLeftBrace pos }
   | "}"                { let pos = set_lexeme_position lexbuf in TokRightBrace pos }
   | "["                { let pos = set_lexeme_position lexbuf in TokLeftBrack pos }
   | "]"                { let pos = set_lexeme_position lexbuf in TokRightBrack pos }

   | eof                { TokEof }
   | _                  { let pos = set_lexeme_position lexbuf in
                          raise (ParseError (pos, Printf.sprintf "illegal char: '%s'"
                          (String.escaped (Lexing.lexeme lexbuf))))
                        }

and comment = parse 
     "(*"               { comment lexbuf; comment lexbuf }
   | "*)"               { () }
   | eof                { () }
   | '\n'               { set_next_line lexbuf;
                          comment lexbuf
                        }
   | _                  { comment lexbuf }

