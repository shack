(*
 * Generate term sets and glue code from Ocaml files.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

open Tsgen_type
open Tsgen_print

(*
 * Program usage.
 *)
let usage =
   "usage: tsgen [options] [files...]"

let term_decls = ref false

(*
 * This is the name of the file to process.
 *)
let filename = ref None

let set_filename s =
   filename := Some s

(*
 * Compilation.
 *)
let compile name =
   let inx = open_in name in
   let _ = Tsgen_pos.set_current_position (name, 1, 0, 1, 0) in
   let types =
      try
         let lexbuf = Lexing.from_channel inx in
         let prog = Tsgen_parser.main Tsgen_lexer.main lexbuf in
            close_in inx;
            prog
      with
         ParseError (pos, s) ->
            print_string (Tsgen_pos.string_of_pos pos);
            print_string ": ";
            print_string s;
            print_string "\n";
            exit 1
       | Parsing.Parse_error ->
            print_string (Tsgen_pos.string_of_pos (Tsgen_pos.current_position ()));
            print_string ": syntax error\n";
            exit 1
       | exn ->
            close_in inx;
            raise exn
   in
   (* For now, we just print the results *)
      print_types types

(*
 * Call the main program.
 *)
let spec =
   ["-decls", Arg.Set term_decls, "generate MetaPRL term declarations"]

let _ =
   Arg.parse spec set_filename usage;
   match !filename with
       Some name ->
          compile name
     | None ->
          print_string "error: must specify an Ocaml file!\n"

