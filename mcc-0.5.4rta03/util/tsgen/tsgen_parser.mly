/*
 * Parser for simple Ocaml files containing type definitions.
 *
 * Eventually, we should use the AML parser, but for now we
 * have our own.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 */

%{
open Tsgen_pos
open Tsgen_type

%}

%token TokEof

%token <Tsgen_pos.pos> TokEq
%token <Tsgen_pos.pos> TokStar
%token <Tsgen_pos.pos> TokPipe
%token <Tsgen_pos.pos> TokSemi
%token <Tsgen_pos.pos> TokColon
%token <Tsgen_pos.pos> TokComma
%token <Tsgen_pos.pos> TokLeftBrace
%token <Tsgen_pos.pos> TokRightBrace
%token <Tsgen_pos.pos> TokLeftBrack
%token <Tsgen_pos.pos> TokRightBrack

%token <Tsgen_pos.pos> TokType
%token <Tsgen_pos.pos> TokOf
%token <Tsgen_pos.pos> TokAnd

%token <string * Tsgen_pos.pos> TokId
%token <string * Tsgen_pos.pos> TokQuotedId

%start main
%type <Tsgen_type.ty_def list> main
%%

main:
   type_defs TokEof                 { $1 }

type_defs:
   type_defs_rev                    { List.rev $1 }

type_defs_rev:
   type_def                         { [$1] }
 | type_defs_rev type_def           { $2 :: $1 }

type_def:
   TokType TokId TokEq ty           { $2, $4 }

ty:
   union                            { $1 }
 | tuple                            { $1 }

/*******************
 * Unions
 *******************/
union:
   union_rev                        { Union (List.rev $1) }

union_rev:
   union_item                       { [$1] }
 | union_rev TokPipe union_item     { $3 :: $1 }

union_item:
   TokId TokOf tuple_type           { $1, $3 }

/*******************
 * Tuples
 *******************/
tuple:
   tuple_type_rev                   { Tuple (List.rev $1) }

tuple_type:
   tuple_type_rev                   { List.rev $1 }

tuple_type_rev:
   TokId                            { [$1] }
 | tuple_type_rev TokStar TokId     { $3 :: $1 }

