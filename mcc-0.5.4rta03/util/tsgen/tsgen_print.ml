(*
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

open Tsgen_type

let print_list_sep sep f = function
   [item] ->
      f item
 | [] ->
      ()
 | l ->
      f (List.hd l);
      List.iter (fun item ->
         print_string sep;
         f item) (List.tl l)

let print_id (v, _) =
   print_string v

let print_union_case (id, vars) =
   print_id id;
   print_string " of ";
   print_list_sep " * " print_id vars

let print_ml_type = function
   Union cases ->
      print_list_sep " | " print_union_case cases
 | Tuple vars ->
      print_list_sep " * " print_id vars

let print_type (id, ml_type) =
   print_id id;
   print_string " = ";
   print_ml_type ml_type;
   print_string "\n"

let print_types types =
   print_list_sep "\n" print_type types

