external type float = "%float"
external type unit = "%unit"

external (+.) : float -> float -> float = "%float_add"

type 'a t = { mutable f : 'a }
let (!) r = r.f

let _ =
  let x = { f = 1.0 } in
  for i = 0 to 0 do
    x.f <- !x +. 0.0
    done
