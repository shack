(* Newton's method fractal benchmark.
 * Geoffrey Irving
 * $Id: newton.ml,v 1.1 2001/09/07 01:17:13 irving Exp $ *)

(*********************** bootstrapping *)

external type int = "%int"
external type string = "%string"
external type bool = "%bool"
external type unit = "%unit"

external (+) : int -> int -> int = "%int_add"
external (-) : int -> int -> int = "%int_sub"
external (~-) : int -> int = "%int_neg"
external ( * ) : int -> int -> int = "%int_mul"
external (/) : int -> int -> int = "%int_div"
external (lsl) : int -> int -> int = "%int_shl"
external (lsr) : int -> int -> int = "%int_shr"
external (asr) : int -> int -> int = "%int_sar"

external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (<) : 'a -> 'a -> bool = "%cmp_lt"
external (>) : 'a -> 'a -> bool = "%cmp_gt"

external print_string : string -> unit = "%print_string"
external output_byte : int -> unit = "%output_byte"

(*********************** fixpoint / complex arithmetic *)

let fix i = i lsl 24
let ( *.) i j = (i asr 12) * (j asr 12)
let (/.) i j = (i / (j asr 8)) lsl 16 

type complex = int * int

let one = fix 1, 0
let (+^) (x, y) (a, b) = x + a, y + b
let (-^) (x, y) (a, b) = x - a, y - b
let ( *^) (x, y) (a, b) = x *. a - y *. b, 2 * (x *. b + y *. a)
let conj (x, y) = x, -y
let mag (x, y) = x *. x + y *. y
let (/|) (x, y) m = x /. m, y /. m
let (/^) z w = z *^ conj w /| mag w
let ( *|) m (x, y) = (m * x, m * y)

(*********************** fractal details *)

let snap = fix 1 / (800 * 800)

let step z = 
  let s = z *^ z in
  let c = z *^ s in
  let q = s *^ s in
    z -^ (q +^ c +^ z -^ one) /^ (4 *| c +^ 3 *| s +^ one)

let rec compute k z =
  if k == 0 then 
    k
  else
    let z' = step z in
      if mag (z' -^ z) < snap then
        k
      else
        compute (k - 1) z'

let top, bot, left, right = 1, -1, 1, -1
let color k = k
let size = 400

let tiff_start () =
  print_string "MM\000\042\000\000\000\008";   (* tiff header *)
  print_string "\000\011";                  (* 11 IFD entries *)

  print_string "\001\000\000\003\000\000\000\001\001\144\000\000";  (* width = 400 *) 
  print_string "\001\001\000\003\000\000\000\001\001\144\000\000";  (* length = 400 *) 
  print_string "\001\002\000\003\000\000\000\001\000\008\000\000";  (* 256 shades of grey *) 
  print_string "\001\003\000\003\000\000\000\001\000\001\000\000";  (* uncompressed *) 
  print_string "\001\006\000\003\000\000\000\001\000\001\000\000";  (* black is zero *) 
  print_string "\001\017\000\003\000\000\000\001\000\162\000\000";  (* strip offset = 162 *) 
  print_string "\001\022\000\003\000\000\000\001\001\144\000\000";  (* 400 rows in one strip *) 
  print_string "\001\023\000\004\000\000\000\001\000\002\113\000";  (* 160000 bytes total *) 
  print_string "\001\026\000\005\000\000\000\001\000\000\000\146";  (* xres offset = 146 *)
  print_string "\001\027\000\005\000\000\000\001\000\000\000\154";  (* yres offset = 154 *)
  print_string "\001\040\000\003\000\000\000\001\000\001\000\000";  (* no meaningful units *)
  print_string "\000\000\000\000";          (* only one IFD *)

  (* current offset = 146 *)
  print_string "\000\000\000\001\000\000\000\001";    (* xres = 1 / 1 *)
  print_string "\000\000\000\001\000\000\000\001"     (* yres = 1 / 1 *)

  (* start of data = 162 *)

let () =
  let sx = fix left in
  let sy = fix bot in
  let dx = (fix right - sx) / size in
  let dy = (fix top - sy) / size in
  
  tiff_start ();
  for i = 1 to size do
    for j = 1 to size do
      let k = compute 255 (sx + i * dx, sy + j * dy) in
        output_byte (color k)
      done
    done
