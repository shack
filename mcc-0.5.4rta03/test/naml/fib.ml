(*
 * Fibonacci test.
 *)
external type int = "%int"
external type bool = "%bool"
external type string = "%string"
external type unit = "%unit"
external (-) : int -> int -> int = "%int_sub"
external (+) : int -> int -> int = "%int_add"
external ( * ) : int -> int -> int = "%int_mul"
external (==) : int -> int -> bool = "%cmp_eq"
external (<=) : int -> int -> bool = "%cmp_le"

external print_int : int -> unit = "%print_int"
external print_string : string -> unit = "%print_string"

let rec fib i =
   if i == 0 || i == 1 then
      i
   else
      fib (i - 1) + fib (i - 2)

let _ =
   let i = fib 35 in
      print_string "fib(35) = ";
      print_int i;
      print_string "\n"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
