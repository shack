(* CSE testing program *)

external type int = "%int"
external type unit = "%unit"
external type bool = "%bool"
external type string = "%string"
external use : int -> unit = "%print_int"
external str : string -> unit = "%print_string"
external (+) : int -> int -> int = "%int_add"
external (-) : int -> int -> int = "%int_sub"
external (>) : int -> int -> bool = "%cmp_gt"

let rec f count a b c =
  if count > 0 then
    begin
    let i = a + b in
    let m = i + c in
    let k = b + c in
    let l = a + k in
    let count = count - 1 in
      use (i + k + l + m);
      str "\n";
      f count a b c
  end

let () = f 100 1 2 3
