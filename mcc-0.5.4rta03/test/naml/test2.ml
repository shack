(* Newton's method fractal benchmark.
 * Geoffrey Irving
 * $Id: test2.ml,v 1.4 2001/09/07 01:17:13 irving Exp $ *)

(*********************** bootstrapping *)

external type int = "%int"
external type float = "%float"
external type string = "%string"
external type bool = "%bool"
external type unit = "%unit"

external (+) : int -> int -> int = "%int_add"
external (-) : int -> int -> int = "%int_sub"
external (+.) : float -> float -> float = "%float_add"
external (-.) : float -> float -> float = "%float_sub"
external (~-.) : float -> float = "%float_neg"
external ( *.) : float -> float -> float = "%float_mul"
external (/.) : float -> float -> float = "%float_div"
external float_of_int : int -> float = "%int_float"

external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (<) : 'a -> 'a -> bool = "%cmp_lt"
external (>) : 'a -> 'a -> bool = "%cmp_gt"

external print_string : string -> unit = "%print_string"
external output_byte : int -> unit = "%output_byte"

type 'a ref = { mutable contents : 'a }
let ref x = { contents = x }
let (:=) r x = r.contents <- x
let (!) r = r.contents

let top, bot, left, right = 1.0, ~-.1.0, 1.0, ~-.1.0
let color k = k
let size = 400

let () =
  let dx = (right -. left) /. float_of_int size in
  let x = ref left in
  
  for i = 1 to size do
    for j = 1 to size do
      let k = 255 in
        output_byte (color k);
        x := !x +. dx
      done
    done
