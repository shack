(*
 * Factorial test.
 *)
external type int = "%int"
external type bool = "%bool"
external type string = "%string"
external type unit = "%unit"
external (-) : int -> int -> int = "%int_sub"
external ( * ) : int -> int -> int = "%int_mul"
external (==) : int -> int -> bool = "%cmp_eq"

external print_int : int -> unit = "%print_int"
external print_string : string -> unit = "%print_string"

let rec fact i =
   fact (i - 1)

let _ = fact 10

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
