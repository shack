external type int = "%int"
external type bool = "%bool"

type 'a ref = { mutable contents : 'a }

type t = int ref
external x : 'a * bool -> 'a = "%fst"
external y : int * 'b -> 'b = "%snd"
external y : 'a * 'a -> 'a = "%snd"
external y : 'a * 'a -> 'b = "%snd"

let ref x = { contents = x }

let _  = (x (ref 1, true))
