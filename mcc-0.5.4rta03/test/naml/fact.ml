(*
 * Factorial test.
 *)
external type int = "%int"
external type bool = "%bool"
external type string = "%string"
external type unit = "%unit"
external (-) : int -> int -> int = "%int_sub"
external ( * ) : int -> int -> int = "%int_mul"
external (==) : int -> int -> bool = "%cmp_eq"

external print_int : int -> unit = "%print_int"
external print_string : string -> unit = "%print_string"

let rec fact i =
   print_string "fact: ";
   print_int i;
   print_string "\n";
   let res = 
      if i == 0 then
         1
      else 
         i * fact (i - 1)
   in
      print_string "fact result: ";
      print_int res;
      print_string "\n";
      res

let _ =
   let i = fact 10 in
      print_string "fact(10) = ";
      print_int i;
      print_string "\n"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
