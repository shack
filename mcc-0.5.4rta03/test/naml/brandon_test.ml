external type int = "%int"
external type bool = "%bool"
external (==) : int -> int -> bool = "%cmp_eq"

let f x y = x
let _ = f 1 2
