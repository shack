external type int = "%int"
external type char = "%char"
external type unit = "%unit"
external type bool = "%bool"
external type 'a array = "%array"
external succ : int -> int = "%int_succ"

external print_int : int -> unit = "%print_int"
external print_char : char -> unit = "%print_char"
let print_string s =
  let rec ps i =
    let c = s.(i) in
    match c with
	'\000' -> ()
      | _ -> print_char c; ps (succ i) in
  ps 0

let _ = print_string "5 ="; print_int 5; print_string "\n"
