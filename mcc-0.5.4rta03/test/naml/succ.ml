(*
 * Simple arithmetic.
 *)
external type int = "%int"
external (+) : int -> int -> int = "%int_add"

let s i =
   i + 1
