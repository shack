(* Random list routines for testing 
 * Geoffrey Irving
 * 31jul01 *)

(* bootstrapping stuff *)

external type int = "%int"
external type char = "%char"
external type unit = "%unit"
external type bool = "%bool"
external type 'a list = "%list"
external type 'a array = "%array"

external (+)  : int -> int -> int = "%int_add"
external (-)  : int -> int -> int = "%int_sub"
external ( * ) : int -> int -> int = "%int_mul"
external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (!=) : 'a -> 'a -> bool = "%cmp_eq"

external print_int : int -> unit = "%print_int"
external print_char : char -> unit = "%print_char"
let print_string s =
  let rec ps i =
    let c = s.(i) in
    match c with
        '\000' -> ()
      | _ -> print_char c; ps (i + 1) in
  ps 0

(* actual routine *)

let rec print_list l =
  match l with
      [] -> ()
    | n :: l ->
        print_int n;
        print_char ' ';
        print_list l 

let rec rev' l = function
   h :: t ->
      rev' (h :: l) t
 | [] ->
      l

let rev l = rev' [] l

let rec map f = function
   h :: t ->
      f h :: map f t
 | [] ->
      []

let rec fold_map f i l =
  match l with
      [] -> i, []
    | hd :: tl ->
        let i, hd = f i hd in
        let i, l = fold_map f i tl in
          i, hd :: l

let main () =
  let p s l =
    print_string s;
    print_list l;
    print_char '\n' in
  let l = [0; 1; 2; 3; 4; 5; 6; 7; 8; 9] in
    p "start: " l;
    p "rev: " (rev l);
    p "squared: " (map (fun n -> n * n) l);
    p "simpler: " (map (fun n -> n + n + 1) l);
  let s, a = fold_map (fun s n -> let n = n+n+1 in s + n, n) 0 l in
    p "fold_map: " (s :: a)

let () = main ()
