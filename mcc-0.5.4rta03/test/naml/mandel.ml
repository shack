(*
 * Benchmark.
 * Compute Mandelbrot set over a small space.
 *
 * This is not supposed to the most efficient _source_
 * code.  MC should be able to optimize this totally.
 *)

external type int = "%int"
external type string = "%string"
external type bool = "%bool"
external type unit = "%unit"

external (+) : int -> int -> int = "%int_add"
external (-) : int -> int -> int = "%int_sub"
external (~-) : int -> int = "%int_neg"
external ( * ) : int -> int -> int = "%int_mul"
external (/) : int -> int -> int = "%int_div"
external (lsl) : int -> int -> int = "%int_shl"
external (lsr) : int -> int -> int = "%int_shr"
external (asr) : int -> int -> int = "%int_sar"
external (land) : int -> int -> int = "%int_and"
external (mod) : int -> int -> int = "%int_mod"

external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (<) : 'a -> 'a -> bool = "%cmp_lt"
external (>) : 'a -> 'a -> bool = "%cmp_gt"

external print_string : string -> unit = "%print_string"
external print_int : int -> unit = "%print_int"

(*
 * Program constants
 *)
let dimen = 400
let scale = 1000
let bound = 4095

(*
 * Fixed-point numbers.
 *)
let fix_shift = 24

let fix_int i =
   i lsl fix_shift

let fix_scale i =
   ((i lsl (fix_shift / 2)) / scale) lsl (fix_shift / 2)

let fix_add i j =
   i + j

let fix_sub i j =
   i - j

let fix_mul i j =
   (i asr (fix_shift / 2)) * (j asr (fix_shift / 2))

let fix_mul_int i j =
   i * j

let fix_div_int i j =
   i / j

(*
 * Compute a point in the Mandelbrot set.
 *)
let rec compute x y k usex usey =
   if k == bound then
      k
   else
      let xsqr = fix_mul usex usex in
      let ysqr = fix_mul usey usey in
         if fix_add xsqr ysqr > fix_int 4 then
            k
         else
            let usey = fix_add (fix_mul (fix_int 2) (fix_mul usex usey)) y in
            let usex = fix_add (fix_sub xsqr ysqr) x in
               compute x y (k + 1) usex usey

let compute x y =
   compute x y 0 x y

(*
 * Main function.
 *)
let main () =
   let left = fix_scale 0 in
   let top = fix_scale 0 in
   let scale2 = fix_scale 2000 in
   let left = fix_sub left (fix_div_int scale2 2) in
   let top = fix_sub top (fix_div_int scale2 2) in
   let step = fix_div_int scale2 dimen in

    (* Print the PGM header *)
    print_string "P2\n# CREATOR: mandel\n";
    print_int dimen;
    print_string " ";
    print_int dimen;
    print_string "\n255\n";

    (* Plot each pixel *)
    for i = 0 to dimen - 1 do
       for j = 0 to dimen - 1 do
          let x = fix_add left (fix_mul_int step i) in
          let y = fix_add top (fix_mul_int step j) in
          let k = compute x y in
             print_int (255 - (k mod 256));
             print_string " "
       done;
       print_string "\n"
    done

let _ =
   main ()