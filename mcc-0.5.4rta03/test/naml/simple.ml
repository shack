type 'a ref = { mutable contents : 'a }
let ref x = { contents = x }

let _ = ref (fun _ _ -> 0)
