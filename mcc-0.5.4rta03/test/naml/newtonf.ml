(* Newton's method fractal benchmark.
 * Geoffrey Irving
 * $Id: newtonf.ml,v 1.1 2001/09/07 01:17:13 irving Exp $ *)

(*********************** bootstrapping *)

external type int = "%int"
external type float = "%float"
external type string = "%string"
external type bool = "%bool"
external type unit = "%unit"

external (+) : int -> int -> int = "%int_add"
external (-) : int -> int -> int = "%int_sub"
external (+.) : float -> float -> float = "%float_add"
external (-.) : float -> float -> float = "%float_sub"
external (~-.) : float -> float = "%float_neg"
external ( *.) : float -> float -> float = "%float_mul"
external (/.) : float -> float -> float = "%float_div"
external float_of_int : int -> float = "%int_float"

external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (<) : 'a -> 'a -> bool = "%cmp_lt"
external (>) : 'a -> 'a -> bool = "%cmp_gt"

external print_string : string -> unit = "%print_string"
external output_byte : int -> unit = "%output_byte"

type 'a ref = { mutable contents : 'a }
let ref x = { contents = x }
let (:=) r x = r.contents <- x
let (!) r = r.contents

(*********************** complex arithmetic *)

type complex = float * float

let one = 1.0, 0.0
let (+^) (x, y) (a, b) = x +. a, y +. b
let (-^) (x, y) (a, b) = x -. a, y -. b
let ( *^) (x, y) (a, b) = x *. a -. y *. b, let i = x *. b +. y *. a in i +. i
let conj (x, y) = x, ~-.y
let mag (x, y) = x *. x +. y *. y
let (/|) (x, y) m = x /. m, y /. m
let (/^) z w = z *^ conj w /| mag w
let ( *|) m (x, y) = (m *. x, m *. y)

(*********************** fractal details *)

let snap = 1.0 /. (800.0 *. 800.0)

let step z = 
  let s = z *^ z in
  let c = z *^ s in
  let q = s *^ s in
    z -^ (q +^ c +^ z -^ one) /^ (4.0 *| c +^ 3.0 *| s +^ one)

let rec compute k z =
  if k == 0 then 
    k
  else
    let z' = step z in
      if mag (z' -^ z) < snap then
        k
      else
        compute (k - 1) z'

let top, bot, left, right = 1.0, ~-.1.0, 1.0, ~-.1.0
let color k = k
let size = 400

let tiff_start () =
  print_string "MM\000\042\000\000\000\008";   (* tiff header *)
  print_string "\000\011";                  (* 11 IFD entries *)

  print_string "\001\000\000\003\000\000\000\001\001\144\000\000";  (* width = 400 *) 
  print_string "\001\001\000\003\000\000\000\001\001\144\000\000";  (* length = 400 *) 
  print_string "\001\002\000\003\000\000\000\001\000\008\000\000";  (* 256 shades of grey *) 
  print_string "\001\003\000\003\000\000\000\001\000\001\000\000";  (* uncompressed *) 
  print_string "\001\006\000\003\000\000\000\001\000\001\000\000";  (* black is zero *) 
  print_string "\001\017\000\003\000\000\000\001\000\162\000\000";  (* strip offset = 162 *) 
  print_string "\001\022\000\003\000\000\000\001\001\144\000\000";  (* 400 rows in one strip *) 
  print_string "\001\023\000\004\000\000\000\001\000\002\113\000";  (* 160000 bytes total *) 
  print_string "\001\026\000\005\000\000\000\001\000\000\000\146";  (* xres offset = 146 *)
  print_string "\001\027\000\005\000\000\000\001\000\000\000\154";  (* yres offset = 154 *)
  print_string "\001\040\000\003\000\000\000\001\000\001\000\000";  (* no meaningful units *)
  print_string "\000\000\000\000";          (* only one IFD *)

  (* current offset = 146 *)
  print_string "\000\000\000\001\000\000\000\001";    (* xres = 1 / 1 *)
  print_string "\000\000\000\001\000\000\000\001"     (* yres = 1 / 1 *)

  (* start of data = 162 *)

let () =
  tiff_start ();

  let dx = (right -. left) /. float_of_int size in
  let dy = (top -. bot) /. float_of_int size in
  let x = ref left in
  let y = ref bot in
  
  for i = 1 to size do
    for j = 1 to size do
      let k = compute 255 (!x, !y) in
        output_byte (color k);
        x := !x +. dx;
        y := !y +. dy
      done
    done
