(*
 * Factorial test.
 *)
external type int = "%int"
external type bool = "%bool"
external (==) : int -> int -> bool = "%cmp_eq"

let rec fact i =
   if i == 0 then
      17
   else
      34

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
