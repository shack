(* Splay set code
 * Geoffrey Irving
 * 8jan0 *)

(************************************** bootstrapping stuff *)

external type int = "%int"
external type char = "%char"
external type string = "%string"
external type unit = "%unit"
external type bool = "%bool"
external type exn = "%exn"
external type 'a list = "%list"
external type 'a array = "%array"

external (+)  : int -> int -> int = "%int_add"
external (-)  : int -> int -> int = "%int_sub"
external (mod): int -> int -> int = "%int_mod"
external ( * ): int -> int -> int = "%int_mul"
external (/)  : int -> int -> int = "%int_div"
external (~-) : int -> int = "%int_neg"
external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (!=) : 'a -> 'a -> bool = "%cmp_neq"
external (<) : 'a -> 'a -> bool = "%cmp_lt"
external (>) : 'a -> 'a -> bool = "%cmp_gt"
external raise : exn -> 'a = "%raise"

type 'a ref = { mutable contents : 'a }
let ref x = { contents = x }
let (:=) r x = r.contents <- x
let (!) r = r.contents

let not = function false -> true | true -> false

external print_int : int -> unit = "%print_int"
external print_char : char -> unit = "%print_char"
external print_string : string -> unit = "%print_string"

(************************************** splay set code *)

type tree = Leaf | Node of tree * int * tree
type t = tree ref

let empty = ref Leaf

let rec parity_aux p x t =
  match t with
    Leaf -> p
  | Node (l,y,r) ->
      let c = x - y in
        if c == 0 then
          not p
        else
          parity_aux (not p) x (if c < 0 then l else r)
let parity x t = parity_aux false x t

exception Impossible

let rec splay_even x t =
  match t with
    Leaf -> raise Impossible
  | Node (l,y,r) ->
      let c = x - y in
        if c == 0 then (l,y,r)
        else if c < 0 then match l with
            Leaf -> (l,y,r)
          | Node (l',z,r') ->
              let c' = x - z in
                if c' < 0 then
                  match splay_even x l' with (l'',x,r'') ->
                    (l'', x, Node (r'', z, Node (r',y,r)))
                else
                  match splay_even x r' with (l'',x,r'') ->
                    (Node (l',z,l''), x, Node (r'',y,r))
        else match r with
            Leaf -> (l,y,r)
          | Node (l',z,r') ->
              let c' = x - z in
                if c' > 0 then
                  match splay_even x r' with (l'',x,r'') ->
                    (Node (Node (l,y,l'), z, l''), x, r'')
                else
                  match splay_even x l' with (l'',x,r'') ->
                    (Node (l,y,l''), x, Node (r'',z,r'))

let splay x t =
  if parity x t then
    match splay_even x t with
      (l,y,r) -> Node (l,y,r)
  else match t with
      Leaf -> t
    | Node (l,y,r) ->
        let c = x - y in
          if c == 0 then
            t
          else if c < 0 then
            match splay_even x l with (l',x,r') ->
              Node (l', x, Node (r',y,r))
          else
            match splay_even x r with (l',x,r') ->
              Node (Node (l,y,l'), x, r')

let mem x t =
  let s = splay x !t in
    t := s;
    match s with
        Leaf -> false
      | Node (_,y,_) -> x == y

let insert x t =
  let s = splay x !t in
    match s with
        Leaf -> ref (Node (Leaf,x,Leaf))
      | Node (l,y,r) ->
          let c = x - y in
            if c == 0 then
              (t := s; t)
            else if c < 0 then
              ref (Node (l, x, Node (Leaf,y,r)))
            else
              ref (Node (Node (l,y,Leaf), x, r))

let rec cutmax t =
  match t with
    Leaf -> raise Impossible
  | Node (l,x,r) ->
      match r with
          Leaf -> (l,x)
        | _ ->
            match cutmax r with
              (t,y) -> (Node (l,x,t), y)

let delete x t =
  let s = splay x !t in
    match s with
        Leaf -> t
      | Node (l,y,r) ->
          if x != y then
            (t := s; t)
          else match l with
              Leaf -> ref r
            | _ -> match cutmax l with
                    (t,z) -> ref (Node (t,z,r))

let rec iter_aux f t =
  match t with
    Leaf -> ()
  | Node (l,x,r) -> iter_aux f l; f x; iter_aux f r
let iter f t = iter_aux f !t

let rec cardinal_aux n t =
  match t with
    Leaf -> n
  | Node (l,_,r) -> cardinal_aux (cardinal_aux (n+1) r) l
let cardinal t = cardinal_aux 0 !t

let rec elements_aux li t =
  match t with
    Leaf -> li
  | Node (l,y,r) -> elements_aux (y :: (elements_aux li r)) l
let elements t = elements_aux [] !t

(************************************** testing code *)

let p st s =
  print_string st;
  (* iter (fun n -> print_int n; print_char ' ') s; *)
  print_char '\n'

let rec detail d s =
  match s with
      Leaf -> ()
    | Node (l, n, r) ->
        detail (d + 2) r;
        for i = 1 to d do print_char ' ' done;
        print_int n;
        print_char '\n';
        detail (d + 2) l

let rec p_p_aux n s =
  if n > 2 then
    p_p_aux (n-1) s;
  if not (mem n s) then
    (print_int n;
    print_char ' ')

let p_p st n s =
  print_string st;
  p_p_aux n s;
  print_char '\n'

let p_elt st s =
  let rec pl l =
    match l with
      [] -> ()
    | n :: l -> print_int n; print_char ' '; pl l in
  print_string st;
  pl (elements s);
  print_char '\n'

let p_all s =
  p "s: " s;
  p_elt "e: " s;
  print_string "details:\n";
  detail 0 !s

let main () =
  try
    let s = ref empty in
    let b = 10 in
    for i = 2 to b do
      if not (mem i !s) then
        for j = 2 to b*b / i do
          s := insert (i * j) !s
        done
      done;
    p_p "p: " (b*b) !s
  with Impossible ->
    print_string "The impossible just occured.\n"

let () = main ()
