external type int = "%int"
external type char = "%char"
external type unit = "%unit"
external type bool = "%bool"
external type 'a array = "%array"

external (+)  : int -> int -> int = "%int_add"
external (-)  : int -> int -> int = "%int_sub"
external ( * ) : int -> int -> int = "%int_mul"
external (==) : 'a -> 'a -> bool = "%cmp_eq"
external (!=) : 'a -> 'a -> bool = "%cmp_eq"

external print_int : int -> unit = "%print_int"
external print_char : char -> unit = "%print_char"
let print_string s =
  let rec ps i =
    let c = s.(i) in
    match c with
        '\000' -> ()
      | _ -> print_char c; ps (i + 1) in
  ps 0

let rec fib n =
  match n with
      1 .. 2 -> 1
    | _ -> fib (n-1) + fib (n-2)

let rec fact n =
  match n with
      0 -> 1
    | _ -> n * fact (n - 1)

let main () =
  let i = 20 in
  print_string "fib ";
  print_int i;
  print_string " = ";
  print_int (fib i);
  print_string "\nfact ";
  print_int i;
  print_string " = ";
  print_int (fact i);
  print_string "\n"

let () = main ()
