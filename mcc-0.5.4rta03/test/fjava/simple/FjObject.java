/*
 * We're going to assume the parseInt and println
 * are built-in (so we don't have to worry about
 * implementing the String class just now).
 *
 * This class allows you compile your code using the
 * normaljavac compiler for testing.
 */

class FjObject {
    static int parseInt(String s) {
        return Integer.parseInt(s);
    }

    static void println(String s) {
        System.out.println(s);
    }
}
