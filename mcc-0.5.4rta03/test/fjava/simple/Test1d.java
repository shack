/*
 * Fibonacci test.
 */
class Fib {
    static int fib(int i) {
        if(i == 0 || i == 1)
            return i;
        return fib(i - 1) + fib(i - 2);
    }
}

/*
 * Factorial test.
 */
class Fact {
    int i;

    Fact(int i) {
        this.i = i;
    }

    int fact() {
        int j, k = 1;
        for(j = i; j != 0; j--)
            k *= j;
        return k;
    }
}

/*
 * Test program.
 */
class Test1d
extends FjObject
{
    public static void main(String[] argv) {
        int i = 32;//parseInt(argv[1]);

        Fib fib = new Fib();
        int j = fib.fib(i);
        //println("Fib(" + i + ") = " + j);

        Fact fact = new Fact(i);
        int k = fact.fact();
        //println("Fact(" + i + ") = " + k);
    }
}
