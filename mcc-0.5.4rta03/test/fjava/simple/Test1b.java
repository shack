/*
 * Fibonacci test.
 */
class Moogle 
extends FjObject {
}
 
class Fib
extends Moogle {
    static int fib(int i) {
            return i;
    }
}

/*
 * Test program.
 */
class Test1b
extends FjObject
{
    int joe(int i) {
        return i;
    }
    
    public static void main(String[] argv) {
        int i = 0;

        Fib fib = new Fib();
        int j = fib.fib(i);
        j = i;
        // If both of these lines are commented out, then all is well,
        // otherwise we get an FIR exception
        j = fib.fib(i);
        //println("j = " + j);
        
        // But this one's just fine.
        j = joe(i);
    }
}
