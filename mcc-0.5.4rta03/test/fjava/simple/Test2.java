/*
 * An example quicksort.  This is not really that great a sorting
 * algorithm; we're just using it for testing.
 */

/*
 * An interface with a generic compare function.
 */
interface Comparable {
    int compare(Comparable x);
}

/*
 * A class of integers.
 */
class Int {
    int i;

    Int(int i) {
        this.i = i;
    }
}

/*
 * Comparison error.
 */
class IntCompareError
{
}

/*
 * Extend to implement the comparison.
 */
class IntCompare
extends Int
implements Comparable
{
    IntCompare(int i) {
        super(i);
    }

    int compare(Comparable c1) {
        if(c1 instanceof IntCompare) {
            IntCompare c2 = (IntCompare) c1;
            return i - c2.i;
        }
        else
            throw new IntCompareError();
    }
}

/*
 * A good sorting routine for large arrays.
 * Not a stable sort.
 */
class QSort
{
    void qsort(Comparable a[], int len)
    {
        /*
         * Reorder the elements in a into two contiguous groups. If
         * ret is the return value, then the first group is the
         * elements in low..ret, and the second group is the elements
         * in ret+1..high.  Each element in the second group will be at
         * least as large as every element in the first group.
         */
        int partition(int low, int high)
            {
                Comparable x = a[low];
                int i = low - 1;
                int j = high + 1;
                while(true) {
                    while(a[--j].compare(x) > 0);
                    while(a[++i].compare(x) < 0);
                    if(i < j) {
                        Comparable temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                    else
                        break;
                }
                return j;
            }

        /*
         * Inner quicksort takes bounds.
         */
        void quicksort(int low, int high)
            {
                if(low < high) {
                    int mid = partition(low, high);
                    quicksort(low, mid);
                    quicksort(mid + 1, high);
                }
            }

        /*
         * Call quick sort with the right bounds.
         */
        quicksort(0, len - 1);
    }
}

/*
 * Test program.
 */
class Test2 {
    int main(String[] argv) {
        QSort qsort = new QSort();
        IntCompare a[] = new IntCompare[10];
        int i;

        a[0] = new IntCompare(7);
        a[1] = new IntCompare(5);
        a[2] = new IntCompare(2);
        a[3] = new IntCompare(12);
        a[4] = new IntCompare(4);
        a[5] = new IntCompare(5);
        a[6] = new IntCompare(0);
        a[7] = new IntCompare(10);
        a[8] = new IntCompare(1);
        a[9] = new IntCompare(11);
        qsort.qsort(a, 10);
        for(i = 0; i != 10; i++)
            println("a[" + i + "] = " + a[i].i);
        return 0;
    }
}




