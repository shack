// Test for do loop functionality

class test_do {
    public static void main(String[] argv) {
        int i=5, j=0;
        
        // Make sure continue works as expected
        do {
            i--;
            j++;
            continue;
            i=0;
            j=0;
        } while(i>0);
        if( j==5 )
            println ( "do/continue passed" );
        else
            println ( "do/continue FAILED" );
        
        // Make sure break works as expected
        do {
            break;
            j=0;
        } while(false);
        if( j==5 )
            println ( "do/break passed" );
        else
            println ( "do/break FAILED" );

        // Make sure do loop always runs at least once
        do
            j--;
        while(false);
        if( j==4 )
            println ( "do passed" );
        else
            println ( "do FAILED" );
    }
}
