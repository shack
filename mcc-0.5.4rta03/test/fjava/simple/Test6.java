/*
 * Benchmark.
 * Compute Mandelbrot set over a small space.
 * This is a _slow_ program, meant for benchmark testing.
 */

class Mandel
extends FjObject
{
    /*
     * Program constants
     */
    int limit = 255;
    int dimen = 400;
    int scale = 1000;
    int bound = 255;

    /*
     * Fixed-point numbers.
     */
    int fix_shift = 24;

    int fix_int(int i)
    {
        return i << fix_shift;
    }

    int fix_scale(int i)
    {
        return ((i << (fix_shift / 2)) / scale) << (fix_shift / 2);
    }

    int fix_add(int i, int j)
    {
        return i + j;
    }

    int fix_sub(int i, int j)
    {
        return i - j;
    }

    int fix_mul(int i, int j)
    {
        return (i >> (fix_shift / 2)) * (j >> (fix_shift / 2));
    }

    int fix_mul_int(int i, int j)
    {
        return i * j;
    }

    int fix_div_int(int i, int j)
    {
        return i / j;
    }

    /*
     * Compute a point in the Mandelbrot set.
     */
    int compute(int x, int y)
    {
        int usex, usey;
        int xsqr, ysqr;
        int k;

        usex = x;
        usey = y;
        for(k = 0; k != bound; k++) {
            xsqr = fix_mul(usex, usex);
            ysqr = fix_mul(usey, usey);

            if(fix_add(xsqr, ysqr) > fix_int(4))
                break;
            usey = fix_add(fix_mul(fix_int(2), fix_mul(usex, usey)), y);
            usex = fix_add(fix_sub(xsqr, ysqr), x);
        }
        return k;
    }

    /*
     * Main function.
     */
    void main(String argv[])
    {
        int x, y, left, top, step, scale2;
        int i, j, k;

        /* Set up screen area */
        left = fix_scale(parseInt(argv[1]));
        top = fix_scale(parseInt(argv[2]));
        scale2 = fix_scale(parseInt(argv[3]));
        left = fix_sub(left, fix_div_int(scale2, 2));
        top = fix_sub(top, fix_div_int(scale2, 2));
        step = fix_div_int(scale2, dimen);

        /* Print the PGM header */
        println("P2\n# CREATOR: test5\n" + dimen + " " + dimen + "\n255\n");

        /* Plot each pixel */
        for(i = 0; i != dimen; i++) {
            for(j = 0; j != dimen; j++) {
                x = fix_add(left, fix_mul_int(step, i));
                y = fix_add(top, fix_mul_int(step, j));
                k = compute(x, y);
                println("" + (255 - (k % 256)));
            }
        }
    }
}

class Test6 {
    public static void main(String argv[]) {
        Mandel mandel = new Mandel();
        mandel.main(argv);
    }
}
