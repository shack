/*
 * Fibonacci test.
 */
class Fiba {
    static int fib(int i) {
        if(i == 0 || i == 1)
            return i;
        return fib(i - 1) + fib(i - 2);
    }

    public static void main(String[] argv) {
        int i = 35;
        int j = fib(i);
        println("Fib(" + i + ") = " + j);
    }
}
