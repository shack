class AnException {}
class AnotherException {}

class test_fin {
    boolean test_normal() {
        // Test finally on normal completion
        int i=0, j=0;
        try {
            for (i=0; i<10; i++) {
                j += i;
            }
        } catch (AnException x) {
            j = 999;
        } finally {
            j = 0;
        }
        return (j == 0);
    }
    
    boolean test_caught() {
        // Test on caught exception
        int i=0, j=0;
        try {
            for (i=0; i<10; i++) {
                j += i;
            }
            throw new AnException();
        } catch (AnException x) {
            j = 999;
        } finally {
            j = 0;
        }
        return (j == 0);
    }
    
    boolean test_uncaught() {
        // Test on uncaught exception
        int i=0, j=0;
        try {
            try {
                for (i=0; i<10; i++) {
                    j += i;
                }
                throw new AnotherException();
            } catch (AnException x) {
                j = 999;
            } finally {
                j = 0;
            }
            j=999;
        } catch (AnotherException x) {
            i = 0;
        }
        return (j == 0);
    }
    
    boolean test_break() {
        // Test on break
        int i=0, j=0;
        for (i=0; i<10; i++) {
            try {
                j += i;
                if (i==5) break;
            } catch (AnException x) {
                j = 999;
            } finally {
                j = 0;
            }
            j=999;
        }
        return (j == 0);
    }
    
    boolean test_lab_brk() {
        // Labeled break
        int i=0, j=0;
        L: {
            try {
                try {
                    i = 0;
                    break L;
                } catch (AnException x) {
                    j = 99;
                } finally {
                    i += 5;
                }
                j = 999;
            } finally {
                i *= 2;
            }
            j=9999;
        }
        return (i==10) && (j==0);
    }
    
    boolean test_nest() {
        // Test nested finallys
        int i=0, j=0;
        for (i=0; i<10; i++) {
            try {
                try {
                    try {
                        j = 0;
                        if (i==5) break;
                    } finally {
                        j = 35;
                    }
                } finally {
                    j *= 2;
                }
                j = 999;
            } catch (AnException x) {
                j = 999;
            } finally {
                j -= 70;
            }
            j=999;
        }
        return (j == 0);
    }
    
    boolean test_return() {
        // Test return
        int i=0, j=0;
        try {
            for (i=0; i<10; i++) {
                try {
                    try {
                        j = 0;
                        if (i==5) return false;
                    } finally {
                        j = 35;
                    }
                } finally {
                    j *= 2;
                }
            }
            j = 899;
            return false;
        } catch (AnException x) {
            j = 799;
        } finally {
            j -= 70;
            if (j==0)
                return true;
            else {
                println( "return: trouble in order of finallys" );
                println( "j = " + j );
                return false;
            }
        }
        j=699;
    }
    
    boolean test_aborted_return() {
        int i=0,j=0;
        // Try changing the reason for abrupt completion from return to throw
        for(;i<10;i++) {
            try {
                try {
                    return false;
                } finally {
                    throw new AnException();
                }
            } catch (AnException x) {
                j=i;
            }
        }
        return j == 9;
    }
        
    boolean test_exc_in_catch() {
        int i=0,j=0;
        // Make sure finally is executed on throw in catch block
        try {
            try {
                throw new AnException();
            } catch (AnException x) {
                throw new AnotherException();
            } finally {
                i=1;
            }
            i=0;
        } catch (AnotherException x) {
            j=1;
        } finally {
            return (i==1) && (j==1);
        }
        return false;
    }
    
    public static void main (String[] argv) {
        if (test_normal())
            println( "normal: passed" );
        else
            println( "normal: FAILED" );
        
        if (test_caught())
            println( "caught: passed" );
        else
            println( "caught: FAILED" );
        
        if (test_uncaught())
            println( "uncaught: passed" );
        else
            println( "uncaught: FAILED" );
        
        if (test_break())
            println( "break: passed" );
        else
            println( "break: FAILED" );
        
        if (test_lab_brk())
            println( "labeled break: passed" );
        else
            println( "labeled break: FAILED" );
        
        if (test_nest())
            println( "nest: passed" );
        else
            println( "nest: FAILED" );
        
        if (test_return())
            println( "return: passed" );
        else
            println( "return: FAILED" );
        
        if (test_aborted_return())
            println( "aborted return: passed" );
        else
            println( "aborted return: FAILED" );
        
        if (test_exc_in_catch())
            println( "throw in catch: passed" );
        else
            println( "throw in catch: FAILED" );
        
    }
}
