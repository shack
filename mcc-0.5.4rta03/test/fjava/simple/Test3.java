/*
 * Integer type.
 */
class Int {
    int i;

    Int(int i) {
        this.i = i;
    }

    int value() {
        return i;
    }
}

/*
 * A good sorting routine for large arrays.
 * Not a stable sort.
 */
class QSort
{
    void qsort(int compare(Int i1, Int i2), Int[] a, int len)
    {
        /*
         * Reorder the elements in a into two contiguous groups. If
         * ret is the return value, then the first group is the
         * elements in low..ret, and the second group is the elements
         * in ret+1..high.  Each element in the second group will be at
         * least as large as every element in the first group.
         */
        int partition(int low, int high)
            {
                Int x = a[low];
                int i = low - 1;
                int j = high + 1;
                while(true) {
                    while(compare(a[--j], x) > 0);
                    while(compare(a[++i], x) < 0);
                    if(i < j) {
                        Int temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                    else
                        break;
                }
                return j;
            }

        /*
         * Inner quicksort takes bounds.
         */
        void quicksort(int low, int high)
            {
                if(low < high) {
                    int mid = partition(low, high);
                    quicksort(low, mid);
                    quicksort(mid + 1, high);
                }
            }

        /*
         * Call quick sort with the right bounds.
         */
        quicksort(0, len - 1);
    }
}

/*
 * Test program.
 */
class Test3 {
    int main(String[] argv) {
        QSort qsort = new QSort();
        Int a[] = new Int[10];
        int i;

        int compare(Int i, Int j) {
            return j.value() - i.value();
        }

        a[0] = new Int(7);
        a[1] = new Int(5);
        a[2] = new Int(2);
        a[3] = new Int(12);
        a[4] = new Int(4);
        a[5] = new Int(5);
        a[6] = new Int(0);
        a[7] = new Int(10);
        a[8] = new Int(1);
        a[9] = new Int(11);
        qsort.qsort(compare, a, 10);
        for(i = 0; i != 10; i++)
            println("a[" + i + "] = " + a[i].i);
        return 0;
    }
}




