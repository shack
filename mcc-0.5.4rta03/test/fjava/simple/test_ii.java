interface I {}

interface J {
    int fj();
}

interface K {
    int fk();
}

interface IJ extends I, J {
    int fij();
}

interface IJK extends I, IJ, K {
    int fijk();
}

class test_ii implements IJK {
    int fj() {return 1;}
    int fk() {return 2;}
    int fij() { return 3; }
    int fijk() { return 4; }
    public static void main(String[] argv) {
        println( "hello world " );
    }
}
