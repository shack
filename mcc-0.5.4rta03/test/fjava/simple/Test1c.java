/*
 * Fibonacci test.
 */
class Fib {
    static int fib(int i) {
        return(i);
    }
}

/*
 * Test program.
 */
class Test1c
{
    static void main(String[] argv) {
        int i = parseInt(argv[1]);
        Fib fib = new Fib();
        int j = fib.fib(i);
    }
}
