// Test of labeled continue

class test_lcont extends FjObject {
    public static void main ( String[] argv ) {
        int i = 0, j=0;
        
        // for
        outer: for (; i<10 && j==i; i++) {
            for (j=0; j<10; j++) {
                if( j == i+1 )
                    continue outer;
            }
        }
        if( i != 10 || j != 10 ) {
            println( "for/for FAILED" );
        } else {
            println( "for/for passed" );
        }
        
        // while
        boolean b1=true, b2=true, b3=false;
        outer: while (b1) {
            while (b2) {
                b3 = true;
                b2 = false; b1 = false;
                continue outer;
            }
            b1 = false; b3 = false;
        }
        if( b3 ) {
            println( "while/while passed" );
        } else {
            println( "while/while FAILED" );
        }
        
        // do
        i=0; b1=true;
        outer: do {
            do {
                i++;
                if( i > 10 )
                    b1 = false;  // Should never be executed
                continue outer;
            } while( i < 20 );
        } while( i < 10 );
        if( b1 ) {
            println( "do/do passed" );
        } else {
            println( "do/do FAILED" );
        }        
        
    }
}
