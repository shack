// Test of labeled break

class test_lbr {
    public static void main ( String[] argv ) {
        int i = 0, j=0;
        
        b1: {
            i = 1;
            break b1;
            println( "oops..." );
        }
        println: println( "1 = " + i ); // OK for labels to clash with names
        
        bs: for( i=0; i<10; i++) {
             main: for( j=0; j<10; j++) {
                    if( i==3 && j==3 )
                        break bs;
                }
            }
        println( "3,3 = " + i + "," + j );
        
        for( i = 0; i<10; i++ ) {
            b2: { b3: {
                if( i == 5 )
                    break;
            } }
        }
        println: println( "5 = " + i );
        
        nest: {
            nest: {
                break nest;
            }
            i=0;
            println( "nesting passed" );
        }
        if( i != 0 )
            println( "nesting FAILED" );
        
    }
}
