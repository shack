/*
 * Benchmark.
 * Compute Mandelbrot set over a small space.
 * This is a _slow_ program, meant for benchmark testing.
 */

/*
 * A fixpoint number is a shifted integer.
 */
class Fix {
    int fix_shift = 24;

    int value;

    Fix(int i) {
        value = i;
    }

    Fix(int scale, int i) {
        value = ((i << (fix_shift / 2)) / scale) << (fix_shift / 2);
    }

    int compare(Fix j) {
        return value - j.value;
    }

    Fix add(Fix j) {
        return new Fix(value + j.value);
    }

    Fix sub(Fix j) {
        return new Fix(value - j.value);
    }

    Fix mul(Fix j) {
        return new Fix((value >> (fix_shift / 2)) * (j.value >> (fix_shift / 2)));
    }

    Fix mul(int j) {
        return new Fix(value * j);
    }

    Fix div(int j) {
        return new Fix(value / j);
    }
}

class Mandel
extends FjObject
{
    /*
     * Program constants
     */
    int limit = 255;
    int dimen = 400;
    int scale = 1000;
    int bound = 255;

    /*
     * Compute a point in the Mandelbrot set.
     */
    int compute(Fix x, Fix y)
    {
        int k;

        Fix usex = x;
        Fix usey = y;
        Fix four = new Fix(1, 4);
        Fix two = new Fix(1, 2);
        for(k = 0; k != bound; k++) {
            Fix xsqr = usex.mul(usex);
            Fix ysqr = usey.mul(usey);

            if(xsqr.add(ysqr).compare(four) > 0)
                break;
            usey = usex.mul(usey).mul(two).add(y);
            usex = xsqr.sub(ysqr).add(x);
        }
        return k;
    }

    /*
     * Main function.
     */
    void main(String[] argv)
    {
        int i, j;

        /* Set up screen area */
        Fix left = new Fix(scale, parseInt(argv[1]));
        Fix top = new Fix(scale, parseInt(argv[2]));
        Fix scale2 = new Fix(scale, parseInt(argv[3]));
        left = left.sub(scale2.div(2));
        top = top.sub(scale2.div(2));
        Fix step = scale2.div(dimen);

        /* Print the PGM header */
        println("P2\n# CREATOR: test5\n" + dimen + " " + dimen + "\n255\n");

        /* Plot each pixel */
        for(i = 0; i != dimen; i++) {
            for(j = 0; j != dimen; j++) {
                Fix x = left.add(step.mul(i));
                Fix y = top.add(step.mul(j));
                int k = compute(x, y);
                String s = "" + (255 - (k % 256));
                println(s);
            }
        }
    }
}

class Test5 {
    public static void main(String argv[]) {
        Mandel mandel = new Mandel();
        mandel.main(argv);
    }
}
