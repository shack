/*
 * Test assignments in higher-order functions.
 */
class Test4 {
    public static void main(String[] argv) {
        int i = 0;

        void f1()() {
            void f3() {
                i += 2;
            }
            return f3;
        }

        void f2() {
            i += 3;
        }

        f2();
        void f4() = f1();
        f4();

        println("i: " + i);
    }
}
