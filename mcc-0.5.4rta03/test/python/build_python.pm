package build_python;
require Exporter;
@ISA = qw(Exporter);   
@EXPORT = qw(make_test);
use build_mc;


#
# Routine for building test cases
#
sub make_test {
   my($base) = @_;
   mc_make_test 'py', $base;
}
