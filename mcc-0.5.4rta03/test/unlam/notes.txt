This file contains notes on various programs in the test suite:
Notation:
  () - already evaluated
  [] - currently being evaluated
  {f al} - function f applied to argument list al

Program 3: `r``d.*i
Evaluation:
`r``d.*i
`r[``d.*i]
`r[`(`d.*)i]
`r[`.*i]
`ri              - .*
i                - r

Program 7: `r```sd.*i
Evaluation:
`r```sd.*i
`r`[``sd.*]i
`r`(``sd.*)i     - s, s'
`r[``di`.*i] 
`r[`(`di)`.*i]
`r[`(`di)i]      - .*
`r[`ii]
`ri
i                -  r

Program 8: `r```s`kd`.*ii
Evaluation:
`r```s`kd`.*ii
`r`[``s`kd`.*i]i    
`r`[`(`s`kd)[`.*i]]i     - k s
`r`[`(`s`kd)i]i          - .*
`r`(``s`kdi)i            - s1
`r[``(`kd)i`ii]          - s2
`r[`d`ii]                - k1
`r(`d`ii)            
(`d`ii)                  - r
Sequence: ks*s1s2k1\n

Program 59: `r```sd``s`k.*`kid 
Evaluation:
`r```sd``s`k.*`kid
`r`[``sd``s`k.*`ki]d
`r`[`(`sd)[`[`s(`k.*)]`ki]]d   - s k
`r`[`(`sd)[`(`s`k.*)(`ki)]]d   - s k
`r`[`(`sd)(``s`k.*`ki)]d       - s1
`r`(``sd(``s`k.*`ki))d         - s1
`r`[`(`dd)`(``s(`k.*)(`ki))d]  - {s2 d (``s`k.*`ki) d}
`r`[`(`dd)[``(`k.*)d`(`ki)d]]  - s2
`r`[`(`dd)[`.*i]]              - k1 k1
`r`[`(`dd)i]                   - .*
`r`(`di)
(`di)                          - r
Sequence: sksks1s1s2s2k1k1*\n
