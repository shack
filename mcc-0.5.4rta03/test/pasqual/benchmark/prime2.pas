(*
 * Primality testing based on non-randomized Fermat's test.
 * Fails for Carmichael numbers.
 *
 * Test if a^n = a (mod n) for all 2 <= a <= n-1.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

program Fermats_little_theorem;

function random(i: integer): integer;
begin
    random := i - 1;
end;

function is_even(i: integer): boolean;
begin
    if i mod 2 = 0 then
	is_even := true
    else
	is_even := false
end;

function square_mod(a, m: integer): integer;
begin
    square_mod := a*a mod m;
end;

function times_mod(a, b, m: integer): integer;
begin
    times_mod := a*b mod m;
end;

function exp_mod(b, e, m: integer): integer;
begin
    if e = 0 then begin
	exp_mod := 1;
    end else 

    if is_even(e) then
	exp_mod := square_mod(exp_mod(b, e div 2, m), m)
    else
	exp_mod := times_mod(square_mod(exp_mod(b, e div 2, m), m), b, m)
end;

function flt(p, a: integer): boolean;
begin
    if exp_mod(a, p, p) = a then
	flt := true
    else
	flt := false
end;

procedure test_for_primality(num: integer);
var
    i: integer;
label 1,2;
begin
    for i:= 2 to num-1 do begin
	if not flt(num, i) then
	    goto 1;
    end;
    
    print_string('There is an excellent chance that ');
    print_int(num);
    print_string(' is a prime number.\n');
    goto 2;
1:
    print_string('The number ');
    print_int(num);
    print_string(' is NOT a prime number.\n');
2:  
end;

label 1;
begin
    if argc < 2 then begin
	print_string('must specify a number to test\n');
	goto 1;
    end;

    test_for_primality(atoi(argv[1]));
1:  
end.
