(*
 * Basic floating-point functions.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

program simple_test;

const
    almost_pi = 3.1415;

procedure nl();
begin
    print_string('\n');
end;

begin
    print_string('sin=');
    print_double(sin(-almost_pi));
    print_string('\ncos=');
    print_double(cos(almost_pi));
    print_string('\ntan=');
    print_double(tan(-almost_pi));
    print_string('\ncot=');
    print_double(cot(-almost_pi));
    print_string('\nabs=');
    print_double(abs(-almost_pi)); nl();

    print_string('\natan2=');
    print_double(atan2(1.5, 1.2));  
    print_string('\narctan=');
    print_double(arctan(1.0)); 
    print_string('\narccot=');
    print_double(arccot(1.0));  { gotta look into this one }
    print_string('\narccos=');
    print_double(arccos(1.0));  { gotta look into this one }
    print_string('\narcsin=');
    print_double(arcsin(1.0)); 
    print_string('\narcsec=');
    print_double(arcsec(1.0)); 
    print_string('\narccsc=');
    print_double(arccsc(1.0)); nl();

    print_string('\nsqrt=');
    print_double(sqrt(2));
    print_string('\nmax=');
    print_double(max(1, 2));
    print_string('\nmin=');
    print_double(min(1, 2)); nl();
end.
