(*
 * Naive primality testing.
 * Test if a | n, for all 2<=a<=sqrt(n).
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

program Naive_primality_testing;

{ Prime to be tested }
var
    ptbt = 1233423;
    times = 1000;
    i, j: integer;

label 1, 2;
begin
    for j:= 1 to times do
	for i:= 2 to sqrt(ptbt) do
	    if ptbt mod i = 0 then goto 1;

    print_string('The number ');
    print_int(ptbt);
    print_string(' IS a prime number.\n');
    goto 2;
1:
    print_string('The number ');
    print_int(ptbt);
    print_string(' is NOT a prime number.\n');
2:  
end.
