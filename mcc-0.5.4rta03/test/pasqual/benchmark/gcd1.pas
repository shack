(*
 * GCD test.
 * Find gcc(a, n) for all 1<=a<=n.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

program Naive_GCD_test;

function gcd(a, b: integer): integer;
begin
    if b = 0 then
	gcd := a
    else
	gcd := gcd(b, a mod b)
end;

{ Prime to be tested }
var
    upper = 1000;
    times = 1000;
    i, j, temp: integer;

begin
    for j:= 1 to times do
	for i:= 1 to upper do
	    temp := gcd(i, upper);

    print_string('Fininshed ');
    print_int(times * upper);
    print_string(' gcds..\n')
end.
