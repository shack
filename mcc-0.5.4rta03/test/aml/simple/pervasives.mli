(*
 * Initial built-in values.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)

(************************************************************************
 * TYPES                                                                *
 ************************************************************************)

(*
 * Base types.
 *)
external type int = "%int"
external type char = "%char"
external type float = "%float"
external type exn = "%exn"
external type string = "%string"
external type 'a array = "%array"
external type ('a, 'b, 'c) format = "%format"

(*
 * Additional types.
 *)
type 'a ref = { mutable contents : 'a }

type 'a option =
   None
 | Some of 'a

type unit =
   ()

type bool =
   false
 | true

type 'a list =
   []
 | :: of 'a * 'a list

(************************************************************************
 * EXCEPTIONS                                                           *
 ************************************************************************)

(*
 * Raise has to use a built-in command.
 *)
external raise : exn -> 'a = "%raise"

exception Assert_failure of string * int * int
exception Invalid_argument of string
exception Failure of string
exception Not_found
exception Out_of_memory
exception Stack_overflow
exception Sys_error of string
exception End_of_file
exception Division_by_zero
exception Exit
exception Sys_blocked_io

val invalid_arg : string -> 'a
val failwith : string -> 'a

(************************************************************************
 * COMPARISONS                                                          *
 ************************************************************************)

external (=)     : 'a -> 'a -> bool   = "%equal"
external (<>)    : 'a -> 'a -> bool   = "%nequal"
external (<)     : 'a -> 'a -> bool   = "%lt"
external (>)     : 'a -> 'a -> bool   = "%gt"
external (<=)    : 'a -> 'a -> bool   = "%le"
external (>=)    : 'a -> 'a -> bool   = "%ge"
external (==)    : 'a -> 'a -> bool   = "%eq"
external (!=)    : 'a -> 'a -> bool   = "%neq"

external compare : 'a -> 'a -> int    = "%compare"

val min : 'a -> 'a -> 'a
val max : 'a -> 'a -> 'a

(************************************************************************
 * BOOLEAN OPERATIONS                                                   *
 ************************************************************************)

external not  : bool -> bool         = "%not"
external (&&) : bool -> bool -> bool = "%and"
external (||) : bool -> bool -> bool = "%or"
external (&)  : bool -> bool -> bool = "%and"
external (or) : bool -> bool -> bool = "%or"

external (~-)  : int -> int          = "%uminus"
external succ  : int -> int          = "%succ"
external pred  : int -> int          = "%pred"
external (+)   : int -> int -> int   = "%add"
external (-)   : int -> int -> int   = "%sub"
external ( * ) : int -> int -> int   = "%mul"
external (/)   : int -> int -> int   = "%div"
external (mod) : int -> int -> int   = "%mod"
external abs   : int -> int          = "%abs"

val max_int : int
val min_int : int

(************************************************************************
 * BITWISE OPERATIONS                                                   *
 ************************************************************************)

external (land) : int -> int -> int = "%land"
external (lor)  : int -> int -> int = "%lor"
external (lxor) : int -> int -> int = "%lxor"
external lnot   : int -> int        = "%lnot"
external (lsl)  : int -> int -> int = "%lsl"
external (lsr)  : int -> int -> int = "%lsr"
external (asl)  : int -> int -> int = "%asl"
external (asr)  : int -> int -> int = "%asr"

(************************************************************************
 * FLOATING POINT                                                       *
 ************************************************************************)

external (~-.)  : float -> float          = "%fuminus"
external (+.)   : float -> float -> float = "%fadd"
external (-.)   : float -> float -> float = "%fsub"
external ( *. ) : float -> float -> float = "%fmul"
external (/.)   : float -> float -> float = "%fdiv"

(*
 * BUG: not implemented in MIR.
external ( ** ) : float -> float -> float = "%fpower"

external sqrt   : float -> float          = "%fsqrt"
external exp    : float -> float          = "%fexp"
external log    : float -> float          = "%flog"
external log10  : float -> float          = "%flog10"
external cos    : float -> float          = "%fcos"
external sin    : float -> float          = "%fsin"
external tan    : float -> float          = "%ftan"
external acos   : float -> float          = "%facos"
external asin   : float -> float          = "%fasin"
external atan   : float -> float          = "%fatan"
external atan2  : float -> float -> float = "%fatan2"
external cosh   : float -> float          = "%fcosh"
external sinh   : float -> float          = "%fsinh"
external tanh   : float -> float          = "%ftanh"

external ceil      : float -> float             = "%fceil"
external floor     : float -> float             = "%ffloor"
external abs_float : float -> float             = "%fabs"
external mod_float : float -> float -> float    = "%fmod"
external ldexp     : float -> int -> float      = "%fldexp"
*)

(*
 * BUG: need float boxing for this to work.
external frexp     : float -> float * int       = "frexp"
external modf      : float -> float * float     = "fmodf"
 *)

external truncate     : float -> int         = "%ftrunc"
external int_of_float : float -> int         = "%ftrunc"
(*
 * BUG: Not implemented in x86.
external float        : int -> float         = "%float_of_int"
external float_of_int : int -> float         = "%float_of_int"
 *)

(************************************************************************
 * STRINGS                                                              *
 ************************************************************************)

val (^) : string -> string -> string

(************************************************************************
 * CHARACTERS                                                           *
 ************************************************************************)

(*
 * BUG: not implemented in x86.
external int_of_char : char -> int = "%int_of_char"
 *)

val char_of_int : int -> char

(************************************************************************
 * UNIT                                                                 *
 ************************************************************************)

val ignore : 'a -> unit

(************************************************************************
 * STRING CONVERSION                                                    *
 ************************************************************************)

val string_of_bool : bool -> string
val bool_of_string : string -> bool

(*
 * Buffer for conversions.
 *)
val string_of_int   : int -> string
val int_of_string   : string -> int
val string_of_float : float -> string
val float_of_string : string -> float

(************************************************************************
 * PAIRS                                                                *
 ************************************************************************)

val fst : ('a * 'b) -> 'a
val snd : ('a * 'b) -> 'b

(************************************************************************
 * LISTS                                                                *
 ************************************************************************)

val (@) : 'a list -> 'a list -> 'a list

(************************************************************************
 * REFERENCES                                                           *
 ************************************************************************)

val ref : 'a -> 'a ref
val (!) : 'a ref -> 'a
val (:=) : 'a ref -> 'a -> unit
val incr : int ref -> unit
val decr : int ref -> unit

(************************************************************************
 * PROGRAM TERMINATION                                                  *
 ************************************************************************)

val exit : int -> 'a

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
