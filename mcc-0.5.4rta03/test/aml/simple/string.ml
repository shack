(*
 * Basic string testing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

external type string = "%string"

external type exn = "%exn"

external raise : exn -> 'a = "%raise"

exception Invalid_argument of string

type bool = false | true

(*
external type unit = "%unit"

external print_string : string -> unit = "print_string"

let _ = print_string "Hello world\n"

let string_of_bool b =
   match b with
      true -> "true"
    | false -> "false"

let _ = print_string (string_of_bool false)
*)

let bool_of_string = function
   "true" -> true
 | "false" -> false
 | _ -> raise (Invalid_argument "bool_of_string")

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
