(*
 * Factorial.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
external type int = "%int"
external type bool = "%bool"

external (=)     : 'a -> 'a -> bool   = "%equal"
external (<>)    : 'a -> 'a -> bool   = "%nequal"
external (<)     : 'a -> 'a -> bool   = "%lt"
external (>)     : 'a -> 'a -> bool   = "%gt"
external (<=)    : 'a -> 'a -> bool   = "%le"
external (>=)    : 'a -> 'a -> bool   = "%ge"
external compare : 'a -> 'a -> int    = "%compare"

let min x y =
   if x < y then
      x
   else
      y

let max x y =
   if x > y then
      x
   else
      y

external (==)    : 'a -> 'a -> bool   = "%eq"
external (!=)    : 'a -> 'a -> bool   = "%neq"

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
