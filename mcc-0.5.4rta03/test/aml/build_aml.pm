#
# Helper functions to make building FC programs easy.
# Copyright(c) 2002 Justin David Smith (who HATES perl).
#
package build_aml;
require Exporter;
@ISA = qw(Exporter);   
@EXPORT = qw(make_test);
use build_mc;


#
# AML test cases require a few extra arguments for the frontend
#
mc_declare_args '-aml -nopervasives';


#
# Routine for building test cases
#
sub make_test {
   my($base) = @_;
   mc_make_test 'ml', $base;
}
