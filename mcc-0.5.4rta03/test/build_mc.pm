#
# Helper functions to make building FC programs easy.
# Copyright(c) 2002 Justin David Smith (who HATES perl).
#
# NOTE:  The flags for each test profile are defined in main/test-mcc.
#        Go there if you need to modify the flags.  PLEASE DON'T MODIFY
#        THE FLAGS without asking Justin first; you will affect the
#        regression results if you modify flags, and may cause some
#        failing cases to drop off the radar!
#
package build_mc;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(mc_declare_args
             mc_make_test);


#
# Environment that compiles (or tries, at least) using
# GCC instead of our compiler.  Use to get a comparison
# for performance numbers.
#
$mc_env_gcc = new cons(
   CC          => 'gcc',
   CPPPATH     => '',
   CFLAGS      => '-O3 -Wall -I arch/x86/include/mojave',
   LDFLAGS     => '-O3 -Wall -I arch/x86/include/mojave',
   LIBS        => 'arch/x86/runtime/x86runtimegcc.a -lm',
   ENV  => {
      PATH     => 'main:/bin:/usr/bin:/usr/local/bin'
   }
);


#
# mc_env($profile, $extra_args):
#  Build a new environment.  The first argument should be
#  the name of the testing profile to use; the second argument
#  contains a list of extra flags which are needed for compile
#  (which may be specific to the frontend we are testing).
#
sub mc_env {
   # Figure out what options to use...
   my($profile, $extra_opts) = @_;
   my $new_env = $mc_env_gcc->clone(
      CC          => 'test-mcc',
      CFLAGS      => "-test-profile $profile $extra_opts",
      LDFLAGS     => "-test-profile $profile $extra_opts",
      LIBS        => '',
   );
   return $new_env;
}


my $mc_env_standard;
my $mc_env_high_opt;
my $mc_env_no_fir_opt;
my $mc_env_unsafe;
my $mc_env_unsafe_high_opt;

#
# mc_declare_args($extra_args):
#  Revise the environments to contain extra arguments.
#
sub mc_declare_args {
   my($extra_args) = @_;
   $mc_env_standard        = mc_env 'standard',       $extra_args;
   $mc_env_high_opt        = mc_env 'high_opt',       $extra_args;
   $mc_env_no_fir_opt      = mc_env 'no_fir_opt',     $extra_args;
   $mc_env_unsafe          = mc_env 'unsafe',         $extra_args;
   $mc_env_unsafe_high_opt = mc_env 'unsafe_high_opt',$extra_args;
}

mc_declare_args '';


#
# mc_make_test_env($env, $source_ext, $base):
#  Build the program $base.$source_ext, using the $env given.  
#  This can build programs independent of the source language 
#  used.  This builds directly from the source file.
#
sub mc_make_test_env {
   # Figure out what filenames to use...
   my($env, $source_ext, $base) = @_;
   my $exec = "${base}.exec";
   my $object = "${base}.o";
   my $source = "${base}.${source_ext}";
   my $tdir = $dir::cwd->lookupfile("")->path;
   
   # Setup nonstandard dependencies.
   Depends $env "$object", '#main/mcc';
   Depends $env "$object", '#main/test-mcc';
   Depends $env "$exec", '#net/migrate/mcc-resurrect';
   
   # Build the object and executable files.
   Command $env "$object", "$source", "$env->{CC} $env->{CFLAGS} -c -o $tdir$object $tdir$source";
   Command $env "$exec", "$object", "$env->{CC} $env->{LDFLAGS} -o $tdir$exec $tdir$object $env->{LIBS}";
}


# 
# mc_make_test_other($env, $suffix, $esuffix, $source_ext, $base):
#  Build the program $base.$source_ext, using the environment given.
#  This sets up to build along a nonstandard dependency path; the
#  object file and executable will have $suffix appended to the base
#  name, and the executable file will use the extension named in the
#  $esuffix argument.
#
sub mc_make_test_other {
   # Figure out what filenames to use...
   my($env, $suffix, $esuffix, $source_ext, $base) = @_;
   my $exec = "${base}${suffix}${esuffix}";
   my $object = "${base}${suffix}.o";
   my $source = "$base.${source_ext}";
   my $tdir = $dir::cwd->lookupfile("")->path;
   
   # Setup nonstandard dependencies.
   Depends $env "$object", '#main/mcc';
   Depends $env "$object", '#main/test-mcc';
   Depends $env "$exec", '#net/migrate/mcc-resurrect';

   # Build the object and executable files.
   Command $env "$object", "$source", "$env->{CC} $env->{CFLAGS} -c -o $tdir$object $tdir$source";
   Command $env "$exec", "$object", "$env->{CC} $env->{LDFLAGS} -o $tdir$exec $tdir$object $env->{LIBS}";
}


#
# mc_make_test($source_ext, $base):
#  Sets up all the possible build targets.
#
sub mc_make_test {
   my($source_ext, $base) = @_;
   mc_make_test_env   $mc_env_standard, $source_ext, $base;
   mc_make_test_other $mc_env_high_opt,         '.z', '.exec', $source_ext, $base;
   mc_make_test_other $mc_env_no_fir_opt,       '.o', '.exec', $source_ext, $base;
   mc_make_test_other $mc_env_unsafe,           '.u', '.exec', $source_ext, $base;
   mc_make_test_other $mc_env_unsafe_high_opt,  '.zu', '.exec', $source_ext, $base;
   mc_make_test_other $mc_env_gcc,              '.gcc', '', $source_ext, $base;
}
