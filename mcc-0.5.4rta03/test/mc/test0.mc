<language="pasqual" source=``

function f(a: integer): integer;
begin
    return a*a;
end;

``>

<language="c" source=``

static void print_int(int o) = "print_int";
static void print_string(char* s) = "print_string";

void main(int argc, char** argv) {
    print_int(f(10));
    print_string("\n");
}

``>
