<!
 ! mc demo with types and exceptions.
 ! ----------------------------------------------------------------
 !
 ! This program is free software; you can redistribute it and/or
 ! modify it under the terms of the GNU General Public License
 ! as published by the Free Software Foundation; either version 2
 ! of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful,
 ! but WITHOUT ANY WARRANTY; without even the implied warranty of
 ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ! GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License
 ! along with this program; if not, write to the Free Software
 ! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 !
 ! Author: Adam Granicz
 ! granicz@cs.caltech.edu
 !
 !> 

// Declare built-in functions, some exceptions
// and types.

<language="pasqual" source=``

    exception Basic_exception;
    type pasqual_int = integer;

``>

<language="c" source=``

    typedef pasqual_int c_int;

    static void print_int(int o) = "print_int";
    static void print_string(char* s) = "print_string";

``>

<language="c" source=``int g(int a) { return a+a; }``>

<language="pasqual" source=``

    exception Not_found;
    exception String_error: c_string;

    procedure raise_string_exception(s: c_string);
    begin
	print_string('Pasqual function is raising exception...\n');
	raise String_error(s);
    end;

    function f(a: c_int): c_int;
    begin
        return g(a)*g(a);
    end;

``>

<language="c" source=``
    union enum exn {
	C_exception;
    };

    void main(pasqual_int argc, char** argv) {
	try {
	    raise_string_exception("cool...");
	}
	catch (C_exception) {
	    print_string("C function caught C_exception...\n");
	}
	catch (Basic_exception) {
	    print_string("C function caught Basic_exception...\n");
	}
	catch (Not_found) {
	    print_string("C function caught Not_found...\n");
	}
	catch (String_error s) {
	    print_string("C function caught: ");
	    print_string(s);
	    print_string("\n");
	}
	print_string("Going about our business...\n");
	print_string("f(10)=");
        print_int(f(10));
        print_string("\n");
    }

``>
