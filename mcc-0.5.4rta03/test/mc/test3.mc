<!
 ! Pascal, Pasqual: functions with no parameters.
 !
 ! In Pascal, a function with no parameters can be called by
 ! specifying its name, such as (1+a), where a is a function.
 !
 ! In Pasqual, however, a function with no parameters must be
 ! called with (), such as (1+a()), whereas the function's
 ! name refers to the function as a value, which can be
 ! passed to, or returned from higher-order functions.
 ! ----------------------------------------------------------------
 !
 ! This program is free software; you can redistribute it and/or
 ! modify it under the terms of the GNU General Public License
 ! as published by the Free Software Foundation; either version 2
 ! of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful,
 ! but WITHOUT ANY WARRANTY; without even the implied warranty of
 ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ! GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License
 ! along with this program; if not, write to the Free Software
 ! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 !
 ! Author: Adam Granicz
 ! granicz@cs.caltech.edu
 !
 !> 

// Declare built-in functions, some exceptions
// and types.

<language="pasqual" source=``

    function h(): integer;
    begin
	return 2;
    end;

    function f(): integer;
    begin
	return h()+h();
    end;
``>

<language="pascal" source=``

    function g: integer;
    begin
	g:= 100 + f;
    end;
``>

<language="c" source=``

    static void print_int(int o) = "print_int";
    static void print_string(char* s) = "print_string";

``>


<language="pascal" source=``

    begin
    	print_int(h);
	print_string('\n');
	print_int(f);
	print_string('\n');
	print_int(g);
	print_string('\n');
    end.

``>
