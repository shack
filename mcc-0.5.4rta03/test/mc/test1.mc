<!
 ! First mc demo.
 ! ----------------------------------------------------------------
 !
 ! This program is free software; you can redistribute it and/or
 ! modify it under the terms of the GNU General Public License
 ! as published by the Free Software Foundation; either version 2
 ! of the License, or (at your option) any later version.
 !
 ! This program is distributed in the hope that it will be useful,
 ! but WITHOUT ANY WARRANTY; without even the implied warranty of
 ! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ! GNU General Public License for more details.
 !
 ! You should have received a copy of the GNU General Public License
 ! along with this program; if not, write to the Free Software
 ! Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 !
 ! Author: Adam Granicz
 ! granicz@cs.caltech.edu
 !
 !> 

// Declare built-in functions
// and types.

<language="pasqual" source=``

    type aint = integer;

``>

<language="c" source=``

    typedef aint int2;

    static void print_int(int o) = "print_int";
    static void print_string(char* s) = "print_string";

``>

<language="c" source=``int g(int a) { return a+a; }``>

<language="pasqual" source=``

    type i2 = integer;

    function f(a: int2): int2;
    begin
        return g(a)*g(a);
    // this is a \`` comment \``....
    end;

``>

<language="c" source=``

    void main(i2 argc, char** argv) {
        print_int(f(10));
        print_string("\n");
    }

``>
