(*
 * A simple programming language.
 *
 * ----------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

Module Incremental

// Specify other grammars to be included.
Include Fc_ast
Include "basic_arith.cph"

// Define the term-set to be used.
Terms -extend "@"
{
   declare TokVar
   declare TokFunction
   declare TokTypeInt{'pos}
   declare TokTypeFloat{'pos}
   declare TokBegin{'pos}
   declare TokEnd{'pos}
   declare TokReturn{'pos}

   declare TokEq{'pos}
   declare TokSemi{'pos}

   declare elist
   declare append
}

// Place global code here.
// This section can be omitted.
{}

(*
 * Define tokens.
 *)

// Designate terminals.
// Options: -longest : match longest substring.

// Tokens are __token__[param: string]{pos} terms, where param
// contains the string matched by the token's regular expression.
// An empty lexical rewrite rule leaves the token term as is.

Tokens -longest {
   VAR = "var"                            { __token__[param:s]{'pos} -> TokVar{'pos} }
   FUNCTION = "function"                  { __token__[param:s]{'pos} -> TokFunction{'pos} }
   INT = "integer"                        { __token__[param:s]{'pos} -> TokTypeInt{'pos} }
   FLOAT = "real"                         { __token__[param:s]{'pos} -> TokTypeFloat{'pos} }
   BEGIN = "begin"                        { __token__[param:s]{'pos} -> TokBegin{'pos} }
   END = "end"                            { __token__[param:s]{'pos} -> TokEnd{'pos} }
   RETURN = "return"                      { __token__[param:s]{'pos} -> TokReturn{'pos} }

   COMMA = ","                            {}
   COLON = ":"                            {}
   EQ = "="                               { __token__[param:s]{'pos} -> TokEq{'pos} }
   SEMI = ";"                             { __token__[param:s]{'pos} -> TokSemi{'pos} }

   * COMMENT = "//[^\\n]*"                {}
}

// Specify associativity rules.
// Tokens appearing first have lower, 
// and tokens appearing on the same
// line have the same precendence.
// This section is optional.

%right EQ
%left PLUS MINUS
%left TIMES DIV
%left prec_apply LPAREN RPAREN

// Specify grammar.
// Must supply a start symbol.

Grammar -start main {
main ::= stmts
      {  elist{'l; 'pos} ->
            expr{SeqExpr{list{'l}}; 'pos}
      }

stmts ::= stmts stmt
      {  elist{'l; 'pos1} expr{'e; 'pos} ->
            elist{list_append{list{'l}; expr{'e; 'pos}}; union_pos{'pos1; 'pos}}
      }
   | stmt
      { expr{'e; 'pos} ->
            elist{list_create{expr{'e; 'pos}}; 'pos}
      }

stmt ::= e SEMI
      {  expr{'e; 'pos} TokSemi{'pos1} ->
            expr{'e; 'pos}
      }
   | def
      {  expr{'e; 'pos} ->
            expr{'e; 'pos}
      }
   | RETURN e SEMI
      {  TokReturn{'pos1} expr{'e; 'pos2} TokSemi{'pos3} ->
            expr{ReturnExpr{expr{'e; 'pos2}}; union_pos{'pos1; 'pos3}}
      }

def ::= VAR ID EQ e SEMI
      {  TokVar{'pos1} TokId[id:s]{'id_pos} TokEq{'eq_pos} expr{'e; 'pos} TokSemi{'pos2} ->
            expr{
               VarDefs{
                  list{list_create{var_decl{
                     'id_pos;
                     StoreAuto{};
                     symbol[id:s]{'id_pos};
                     ty{TypeInt{StatusNormal{}}; 'id_pos}; // bogus position
                     InitExpr{'pos; expr{'e; 'pos}}}}}};
               union_pos{'pos1; 'pos2}}
      }
   | FUNCTION ID LPAREN opt_params RPAREN COLON type SEMI body SEMI
      {  TokFunction{'pos1} TokId[id:s]{'pos2} TokLeftParen{'pos3}
         list{'params} TokRightParen{'pos4} COLON ty{'ty; 'ty_pos} TokSemi{'pos5}
         list{'body} TokSemi{'pos7} ->
            expr{
               FunDef{StoreAuto{};
                  symbol[id:s]{'pos2};
                  list{'params};
                  ty{'ty; 'ty_pos};
                  expr{SeqExpr{list{'body}}; union_pos{'pos1; 'pos7}}};
               union_pos{'pos1; 'pos7}}
      }

body ::= BEGIN stmts END
      {  TokBegin{'pos1} elist{'l; 'pos2} TokEnd{'pos3} ->
            list{'l}
      }

opt_params ::= _ /* empty */
      {  __EPSILON__{} ->
            list{empty_list{}}
      }
   | params
      {  list{'l} ->
            list{'l}
      }

params ::= params SEMI param
      {  list{'l} TokSemi{'pos1} patt_decl{'pos; 'sym; 'ty} ->
            list{list_append{list{'l}; patt_decl{'pos; 'sym; 'ty}}}
      }
   | param
      {  patt_decl{'pos; 'sym; 'ty} ->
            list{list_create{patt_decl{'pos; 'sym; 'ty}}}
      }

param ::= ID COLON type
      {  TokId[id:s]{'pos1} COLON ty{'ty; 'pos2} ->
            patt_decl{'pos1; symbol[id:s]{'pos1}; ty{'ty; 'pos2}}
      }

type ::= INT
      { TokTypeInt{'pos} ->
         ty{TypeInt{StatusNormal{}}; 'pos}
      }
   | FLOAT
      { TokTypeFloat{'pos} ->
         ty{TypeFloat{StatusNormal{}}; 'pos}
      }

opt_e_list ::= _ /* empty */
      { __EPSILON__{} ->
            list{empty_list{}}
      }
   | e_list
      { list{'l} ->
            list{'l}
      }

e_list ::= e_list COMMA e
      {  list{'l} COMMA expr{'e; 'pos} ->
            list{list_append{list{'l}; expr{'e; 'pos}}}
      }
   | e
      {  expr{'e; 'pos} ->
            list{list_create{expr{'e; 'pos}}}
      }

// We inherit basic arithmetic expressions from parent module.
e ::=
   (* Assignment *)
     e EQ e
      {  expr{'e1; 'pos1} TokEq{'eq_pos} expr{'e2; 'pos2} ->
            expr{
               OpExpr{PreOp{};
                  symbol["="]{'eq_pos};
                  list{list_create2{expr{'e1; 'pos1}; expr{'e2; 'pos2}}}};
               union_pos{'pos1; 'pos2}}
      }
   (* Function application *)
   | e LPAREN opt_e_list RPAREN %prec prec_apply
      {  expr{'e; 'pos} TokLeftParen{'pos1} list{'l} TokRightParen{'pos2} ->
            expr{
               OpExpr{PreOp{};
                  symbol["()"]{union_pos{'pos1; 'pos2}};
                  list{list_insert{list{list_insert{list{'l}; expr{'e; 'pos}}}; expr{'e; 'pos}}}};
               union_pos{'pos; 'pos2}}
      }
}
