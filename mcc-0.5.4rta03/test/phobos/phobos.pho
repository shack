(*
 * Phobos for Phobos!
 *
 * Slight modification: no (* ... *)-style comments.
 * See note below.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

Module Phobos

// Define the term-set to be used.

Terms -extend "@"
{}

// Place global code here.
// This section can be omitted.
{}

(*
 * Define tokens.
 *)

Tokens -first
{
   TokEq = "="                {}
   TokRuleEq = "::="          {}
   TokArrow = "->"            {}
   TokIgnore = "\\*"          {}
   TokPipe = "|"              {}
   TokSemi = ";"              {}
   TokComma = ","             {}
   TokLeftBrace = "{"         {}
   TokRightBrace = "}"        {}
   TokLeftBrack = "\\["       {}
   TokRightBrack = "\\]"      {}
   TokUnderscore = "_"        {}

   TokInt = "[0-9]+"          {}
   TokFloat = "\\([0-9]+\\.[0-9]+\\(\\(e\|E\\)\\(+\|-\\)?[0-9]+\\)?\\)\|\\([0-9]+\\(\\(e\|E\\)\\(+\|-\\)?[0-9]+\\)?\\)"
                              {}
   TokQuotedId = "\\'[_a-zA-Z][_a-zA-Z0-9]*"
                              {}
   TokString = "\\"[^\\n]*\\""
                              {}

   TokGrammar = "Grammar"     {}
   TokTokens = "Tokens"       {}
   TokTerms = "Terms"         {}
   TokId = "[_a-zA-Z][_a-zA-Z0-9]*"
                              {}

   TokLongest = "-longest"    {}
   TokFirst = "-first"        {}
   TokStart = "-start"        {}
   TokExtend = "-extend"      {}
   TokOption = "-[_a-zA-Z][_a-zA-Z0-9]"
                              {}

   TokNonAssoc = "%nonassoc"  {}
   TokLeftAssoc = "%left"     {}
   TokRightAssoc = "%right"   {}
   
   * TokComment1 = "//[^\\n]*"
                              {}

// This regexp causes Str.match_string to go into an infinite loop...
//   * TokComment2 = "(\\*\\(\\(\\*[^)]\\)\|\\([^\\*])\\)\|\\([^\\*)]\\)*\|[\\*)]\)*\\*)"
//                              {}

   * TokEol = "\\n"           {}
   * TokWhite = "\\t\|\\n"    {}
   * TokSpace = " "           {}

}

// Specify grammar.
// Must supply a start symbol.

Grammar -start main
{
   main ::=
     preamble trms lexer opt_assocs grmmr 
                              {}
   | trms lexer opt_assocs grmmr 
                              {}

   preamble ::=
     body                     {}

   trms ::=
     TokTerms opt_term_options TokLeftBrace opt_identifiers TokRightBrace
                              {}

   opt_term_options ::=
       _                      {}
   | term_option_list         {}

   term_option_list ::=
     term_option_list term_option
                              {}
   | term_option              {}

   term_option ::=
     TokExtend TokString      {}
   | TokOption                {}
     
   lexer ::=
     TokTokens opt_lexer_options TokLeftBrace tkns TokRightBrace
                              {}

   opt_lexer_options ::=
     _                        {}
   | lexer_option_list        {}

   lexer_option_list ::=
     lexer_option_list lexer_option
                              {}
   | lexer_option             {}

   lexer_option ::=
     TokLongest               {}
   | TokFirst                 {}
   | TokOption                {}

   tkns ::=
     token_list               {}

   token_list ::=
     token_list token         {}
   | token                    {}

   token ::=
     TokId TokEq TokString body
                              {}
   | TokIgnore TokId TokEq TokString body
                              {}

   opt_identifiers ::=
       _                      {}
   | identifiers              {}

   identifiers ::=
     identifier_list          {}

   identifier_list ::=
     identifier               {}
   | identifier_list identifier
                              {}

   identifier ::=
     TokId                    {}
   | TokUnderscore            {}

   opt_assocs ::=
     _                        {}
   | assoc_list               {}

   assoc_list ::=
     assoc_list assoc         {}
   | assoc                    {}

   assoc ::=
     TokNonAssoc identifiers
                              {}
   | TokLeftAssoc identifiers
                              {}
   | TokRightAssoc identifiers
                              {}

   grmmr ::=
     TokGrammar opt_grammar_options TokLeftBrace rules TokRightBrace
                              {}

   opt_grammar_options ::=
     _                        {}
   | grammar_option_list      {}

   grammar_option_list ::=
     grammar_option_list grammar_option
                              {}
   | grammar_option           {}

   grammar_option ::=
     TokStart identifier      {}

   rules ::=
     rule_list                {}

   rule_list ::=
     rule_list rule           {}
   | rule                     {}

   rule ::=
     TokId TokRuleEq productions
                              {}

   productions ::=
     identifiers body prods   {}
   | identifiers body         {}

   prods ::=
     prod_list                {}

   prod_list ::=
     prod_list prod_list_prim {}
   | prod_list_prim           {}

   prod_list_prim ::=
     TokPipe prod_body        {}

   prod_body ::=
     identifiers body         {}

   body ::=
     TokLeftBrace opt_term_matches TokRightBrace
                              {}

   opt_term_matches ::=
     _                        {}
   | term_match_list          {}

   term_match_list ::=
     term_match_list TokPipe term_match
                              {}
   | term_match               {}

   term_match ::=
     froms TokArrow simple_term
                              {}
   | froms                    {}

   froms ::=
     from_list                {}

   from_list ::=
     from_list from           {}
   | from                     {}

   from ::=
     simple_term              {}
   | identifier               {}

   quoted_identifier ::=
     TokQuotedId              {}

   simple_term ::=
     identifier opt_term_params TokLeftBrace opt_term_list_semi TokRightBrace
                              {}
   | quoted_identifier        {}

   opt_term_list_semi ::=
     _                        {}
   | terms_with_semi          {}

   terms_with_semi ::=
     term_list_with_semi      {}

   term_list_with_semi ::=
     term_list_with_semi TokSemi simple_term
                              {}
   | simple_term              {}

   opt_term_params ::=
     _                        {}
   | TokLeftBrack term_param_list TokRightBrack
                              {}

   term_param_list ::=
     term_param_list term_param
                              {}
   | term_param               {}

   term_param ::=
     quoted_identifier        {}
   | TokInt                   {}
   | TokFloat                 {}
   | TokString                {}
}

