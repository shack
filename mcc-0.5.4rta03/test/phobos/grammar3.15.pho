(*
 * ----------------------------------------------------------------
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *)

Module Grammar3_15

// Define the term-set to be used.

Terms -extend "@"
{}

// Place global code here.
// This section can be omitted.
{}

(*
 * Define tokens.
 *)

// Designate terminals.
// Options: -longest : match longest substring.

Tokens -longest {
   LPAREN = "("                           {}
   RPAREN = ")"                           {}
   ID = "[_a-zA-Z][_a-zA-Z0-9]*"          {}
   NUM = "[1-9][0-9]*"                    {}
   TIMES = "*"                            {}
   DIV = "/"                              {}
   PLUS = "+"                             {}
   MINUS = "-"                            {}

   * EOL = "\\n"                          {}
   * SPACE = " "                          {}
}

// Specify grammar.
// Must supply a start symbol.

Grammar -start e {
   e ::= t e'                             {}

   e' ::= PLUS t e'                       {}
      | MINUS t e'                        {}
      | _                                 {}

   t ::= f t'                             {}

   t' ::= TIMES f t'                      {}
      | DIV f t'                          {}
      | _                                 {}

   f ::= ID                               {}
      | NUM                               {}
      | LPAREN e RPAREN                   {}
}
