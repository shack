(*
 * Example Naml code that can be compiled with -print_fir -O0
 * to generate a FIR token file.  This file can then be
 * processed through Phobos to inline the power5 function,
 * and then return the code to MCC for further processing.
 *
 * Naml code by Brian Aydemir (emre@its.caltech.edu).
 * Example inspired by Jason Hickey (jyh@cs.caltech.edu).
 *)

(*
 * We need to get the types and all that into our program.
 *)

external type int = "%int"
external type bool = "%bool"
external type string = "%string"
external type unit = "%unit"
external (-) : int -> int -> int = "%int_sub"
external (+) : int -> int -> int = "%int_add"
external ( * ) : int -> int -> int = "%int_mul"
external (==) : int -> int -> bool = "%cmp_eq"

external print_int : int -> unit = "%print_int"

(*
 * Define a recursive exponentiation function.
 * Not the most natural ML coding, but it produces an
 * almost readible FIR. (-:
 *)

let rec power result base exp =
   let flag = (exp == 0) in
      match flag with
         true ->
            result
       | false ->
            let new_result = result * base in
            let new_exp    = exp - 1 in
               power new_result base new_exp

(*
 * Define a version with a fixed exponent.
 *)

let power5 base = power 1 base 5

(*
 * Trivial application so that we can see that our program
 * behaves correctly.
 *)

let _ =
   let answer = power5 2 in
      print_int answer
