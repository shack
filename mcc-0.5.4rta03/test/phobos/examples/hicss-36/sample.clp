//
// Simple example
//

var global: integer = 2;

function dummy(): integer;
{
   return 1;
};

function square(a: integer): integer;
{
   return a*a;
};

program
{
   var a: integer = 3;
   var b: integer = 2;
   var c: integer = 1;
   var d: real = 1.32 + 1.68;

   var result: integer = a*(13+b-(14/c))*b+global;

   result = square(result);

   return result+dummy();
};

