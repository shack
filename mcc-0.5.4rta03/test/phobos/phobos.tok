//
// A simple functional language based on lambda calculus.
//
// ----------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Author: Adam Granicz
// granicz@cs.caltech.edu
//

// Place global code here.
// This section can be omitted.

{}

// Define the term-set to be used.

Terms -extend "fc_terms"
{}

//
// Define tokens.
//

// Designate terminals.
// Options: -longest : match longest substring.
//          -first   : match first substring that matches.

Tokens -longest
{
   ID = "[_a-zA-Z][_a-zA-Z0-9]*"          {}
   NUM = "[1-9][0-9]*"                    { num['i]{} }
   CHAR = "\\'\\\\[0-9]+\\'"              {}
   STRING = "\\"[^\\n]*\\""               {}

   LET = "let"                            {}
   LAMBDA = "lambda"                      {}
   APPLY = "apply"                        {}
   IN = "in"                              {}

   EQ = "="                               {}
   DOT = "\\."                            {}
   LPAREN = "("                           {}
   RPAREN = ")"                           {}

   * COMMENT = "//[^\\n]*"                {}
   * EOL = "\\n"                          {}
   * SPACE = " "                          {}

}

// Specify associativity rules.
// Tokens appearing first have lower, 
// and tokens appearing on the same
// line have the same precendence.
// This section is optional.



// Specify grammar.
// Must supply a start symbol.

Grammar -start main
{
   main ::= main let
      {
         elist{'l} expr{'e} ->
            elist{list_append{elist{'l}; expr{'e}}}
      }
      | let
      {
         expr{'e} ->
            elist{list_append{elist{}; expr{'e}}}
      }

   let ::= LET ID EQ expr
      {
         LET id['v]{} EQ expr{'e} ->
            expr{assign{expr{id['v]{}}; expr{'e}}}
      }

   expr ::= ID
      {
         expr{id['v]{}}
      }
      | NUM
      {
         expr{num['i]{}}
      }
      | APPLY expr expr
      {
         APPLY expr{'e1} expr{'e2} ->
            expr{apply{expr{'e1}; expr{'e2}}}
      }
      | LPAREN expr RPAREN
      {
         LPAREN expr{'e} RPAREN ->
            expr{'e}
      }
      | LET ID EQ expr IN expr
      {
         LET id['v]{} EQ expr{'e1} IN expr{'e2} ->
            expr{seq{assign{id['v]{}; expr{'e1}}; expr{'e2}}}
      }
      | LAMBDA ID DOT expr
      {
         LAMBA id['v]{} EQ expr{'e} ->
            expr{lambda{id['v]{}; expr{'e}}}
      }
}
