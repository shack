(* graph testing code *)

open Format
module Graph = Graph.Undirected

let action s g =
  let ios = int_of_string in
  let nos n = 
    let n = ios n in
      try 
        Graph.find (fun a -> Graph.get a == n) g
      with Not_found ->
        printf "not_found: %d" n;
        raise Exit in
  let rec list_pair_iter f l =
    match l with
        [] -> ()
      | n :: m :: l -> f n m; list_pair_iter f l
      | [_] -> print_string "Last argument dropped\n" in
  let output_nl out nl = List.iter (fun n -> fprintf out " %d" (Graph.get n)) nl in
  try
    match Mc_string_util.split " " s with
        ["h"] ->
          print_string "Commands:\n  \
                          an n ... - add nodes labeled n, ...\n  \
                          dn n ... - delete nodes n, ...\n  \
                          ae n m ... - add edges between n and m, ...\n  \
                          de n m ... - delete edges between n and m, ...\n  \
                          i n ... - print info about nodes n, ...\n  \
                          p - print graph\n\n"
      | "an" :: nl ->
          List.iter (fun n -> ignore (Graph.add_node g (ios n))) nl
      | "dn" :: nl ->
          List.iter (fun n -> Graph.delete_node (nos n)) nl
      | "ae" :: nl ->
          list_pair_iter (fun n m -> Graph.add_edge g (nos n) (nos m)) nl
      | "de" :: nl ->
          list_pair_iter (fun n m -> Graph.delete_edge (nos n) (nos m)) nl
      | "i" :: nl ->
          List.iter (fun n -> let n = nos n in
            printf "node: %d     degree: %d     neighbors: %a\n" (Graph.get n) (Graph.degree n) 
              output_nl (Graph.neighbors n)) nl
      | ["p"] ->
          Graph.iter (fun n -> printf "%d: %a\n" (Graph.get n) output_nl (Graph.neighbors n)) g
      | _ -> ()
  with Exit -> ()

let rec loop g =
  print_string "> ";
  let s = read_line () in
    action s g; loop g

let () =
  let g = Graph.create () in
    action "h" g;
    try 
      loop g
    with End_of_file -> 
      print_string "\n"
