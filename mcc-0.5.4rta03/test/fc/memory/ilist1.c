/*
 * A test of large integer-lists.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include "fc.h"

int i;

struct int_list {
    int value;
    struct int_list *nextp;
};

/*
 * Create a new element.
 */
static struct int_list *new_cons(int i, struct int_list *nextp)
{
    struct int_list *listp;

    listp = (struct int_list *) malloc(sizeof(struct int_list));
    listp->value = i;
    listp->nextp = nextp;
    return listp;
}

/*
 * Create a list of integers in reverse order.
 */
static struct int_list *new_list(int length)
{
    struct int_list *nextp;

    nextp = 0;
    while(length) {
#if 0
        print_string("new_list: ");
        print_int(length);
        print_string("\n");
#endif

        nextp = new_cons(length, nextp);
        length--;
    }
    return nextp;
}

/*
 * Print all the values.
 */
static void print_list(struct int_list *listp)
{
    int i = 0;
    while(listp) {
        if(++i % 1000 == 0) {
            print_string("List element: ");
            print_int(listp->value);
            print_string("\n");
        }
        listp = listp->nextp;
    }
}    

/*
 * Create a large list.
 */
int main(int argc, char **argv)
{
    struct int_list *listp;

    listp = new_list(1000000);
    print_list(listp);
}
