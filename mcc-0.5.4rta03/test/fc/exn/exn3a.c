/*
 * Basic exception test.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include "fc.h"

union enum exn {
    Not_found;
    char *Invalid_argument;
};

static int f()
{
    throw Invalid_argument("Hello world");
    return 0;
}

int main(int argc, char **argv)
{
    try {
        f();
    }
    catch(Not_found) {
        print_string("Caught Not_found\n");
    }
    catch(Invalid_argument s) {
        print_string("Caught Invalid_argument: ");
        print_string(s);
        print_string(" [");
        print_hex(*(int *)s);
        print_string("]\n");
    }
    return 0;
}
