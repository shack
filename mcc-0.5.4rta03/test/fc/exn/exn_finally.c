/* 
   Exception handling with finally
   Justin David Smith, adapted from Nathan Gray's Java suite.
 */   


#include <fc.h>


union enum exn {
    AnException;
    AnotherException;
};


#define  false    0
#define  true     (!false)


static void println(char *s) {
    print_string(s);
    print_string("\n");
}
        

static int test_normal() {
    // Test finally on normal completion
    int i=0, j=0;
    try {
        for (i=0; i<10; i++) {
            j += i;
        }
    } catch (AnException) {
        j = 999;
    } finally {
        j = 0;
    }
    return (j == 0);
}


static int test_caught() {
    // Test on caught exception
    int i=0, j=0;
    try {
        for (i=0; i<10; i++) {
            j += i;
        }
        throw AnException;
    } catch (AnException) {
        j = 999;
    } finally {
        j = 0;
    }
    return (j == 0);
}


static int test_uncaught() {
    // Test on uncaught exception
    int i=0, j=0;
    try {
        try {
            for (i=0; i<10; i++) {
                j += i;
            }
            throw AnotherException;
        } catch (AnException) {
            j = 999;
        } finally {
            j = 0;
        }
        j=999;
    } catch (AnotherException) {
        i = 0;
    }
    return (j == 0);
}


static int test_break() {
    // Test on break
    int i=0, j=0;
    for (i=0; i<10; i++) {
        try {
            j += i;
            if (i==5) break;
        } catch (AnException) {
            j = 999;
        } finally {
            j = 0;
        }
        j=999;
    }
    return (j == 0);
}


static int test_nest() {
    // Test nested finallys
    int i=0, j=0;
    for (i=0; i<10; i++) {
        try {
            try {
                try {
                    j = 0;
                    if (i==5) break;
                } finally {
                    j = 35;
                }
            } finally {
                j *= 2;
            }
            j = 999;
        } catch (AnException) {
            j = 999;
        } finally {
            j -= 70;
        }
        j=999;
    }
    return (j == 0);
}


static int test_return() {
    // Test return
    int i=0, j=0;
    try {
        for (i=0; i<10; i++) {
            try {
                try {
                    j = 0;
                    if (i==5) return false;
                } finally {
                    j = 35;
                }
            } finally {
                j *= 2;
            }
        }
        j = 899;
        return false;
    } catch (AnException) {
        j = 799;
    } finally {
        j -= 70;
        if (j==0)
            return true;
        else {
            println( "return: trouble in order of finallys" );
            println( "j = " + j );
            return false;
        }
    }
    j=699;
}


static int test_aborted_return() {
    int i=0,j=0;
    // Try changing the reason for abrupt completion from return to throw
    for(;i<10;i++) {
        try {
            try {
                return false;
            } finally {
                throw AnException;
            }
        } catch (AnException) {
            j=i;
        }
    }
    return j == 9;
}

    
static int test_exc_in_catch() {
    int i=0,j=0;
    // Make sure finally is executed on throw in catch block
    try {
        try {
            throw AnException;
        } catch (AnException) {
            throw AnotherException;
        } finally {
            i=1;
        }
        i=0;
    } catch (AnotherException) {
        j=1;
    } finally {
        return (i==1) && (j==1);
    }
    return false;
}


int main (int argc, char **argv) {
    if (test_normal())
        println( "normal: passed" );
    else
        println( "normal: FAILED" );
    
    if (test_caught())
        println( "caught: passed" );
    else
        println( "caught: FAILED" );
    
    if (test_uncaught())
        println( "uncaught: passed" );
    else
        println( "uncaught: FAILED" );
    
    if (test_break())
        println( "break: passed" );
    else
        println( "break: FAILED" );
    
    if (test_nest())
        println( "nest: passed" );
    else
        println( "nest: FAILED" );
    
    if (test_return())
        println( "return: passed" );
    else
        println( "return: FAILED" );
    
    if (test_aborted_return())
        println( "aborted return: passed" );
    else
        println( "aborted return: FAILED" );
    
    if (test_exc_in_catch())
        println( "throw in catch: passed" );
    else
        println( "throw in catch: FAILED" );
    
}
