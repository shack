static void print_string(char *) = "print_string";
static int print_int(int) = "print_int";

typedef int item;

/*
 * A good sorting routine for large arrays.
 * Not a stable sort.
 */
void qsort(item *a, int (*compare)(item, item), int len)
{
    /*
     * Reorder the elements in a into two contiguous groups. If
     * ret is the return value, then the first group is the
     * elements in low..ret, and the second group is the elements 
     * in ret+1..high. Each element in the second group will be at 
     * least as large as every element in the first group.
     */
    int partition(int low, int high)
        {
            item x = a[low]; 
            int i = low - 1; 
            int j = high + 1; 
            while(1) {
                while(compare(a[--j], x) > 0); 
                while(compare(a[++i], x) < 0); 
                if(i < j) {
                    item temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
                else
                    return j;
            }
            return 0;
        }

    /*
     * Inner quicksort takes bounds.
     */
    void quicksort(int low, int high)
        {
            if(!(low < high))
                return; 
            int mid = partition(low, high); 
            quicksort(low, mid); 
            quicksort(mid + 1, high);
        }

    /*
     * Call quick sort with the right bounds.
     */
    quicksort(0, len - 1);
}

/*
 * Test program.
 */
int main(int argc, char **argv)
{
    item a[10];
    int i;

    int compare(item a, item b)
        {
            return a - b;
        }

    a[0] = 7;
    a[1] = 5;
    a[2] = 2;
    a[3] = 12;
    a[4] = 4;
    a[5] = 5;
    a[6] = 0;
    a[7] = 10;
    a[8] = 1;
    a[9] = 11;
    qsort(a, compare, 10);
    for(i = 0; i != 10; i++) {
        print_string("a[");
        print_int(i);
        print_string("]=");
        print_int(a[i]);
        print_string("\n");
    }
    return 0;
}

