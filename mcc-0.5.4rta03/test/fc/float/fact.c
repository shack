#include <fc.h>

/*
 * Factorial.
 */
static double fact(double i)
{
    double k = 1.0;

    while(i > 0.1) {
        print_string("fact: ");
        print_float(i);
        print_string("\n");
        k *= i;
        i--;
    }
    return k;
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    double i;

    /* Check usage */
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <number>\n");
        return 1;
    }

    /* Get the number */
    i = atof(argv[1]);

    print_string("fact(");
    print_float(i);
    print_string(") = ");
    print_float(fact(i));
    print_string("\n");
    return 0;
}
