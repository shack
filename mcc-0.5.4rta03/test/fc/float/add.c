static double f(long double x)
{
    return x + 1;
}

int main(int argc, char **argv)
{
    return f(1) + f(2);
}
