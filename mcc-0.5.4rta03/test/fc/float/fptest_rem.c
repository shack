#include <fc.h>


#ifdef __mcc
#define  float_mod(x, y)   ((x) % (y))
#else
#include <math.h>
#define  float_mod(x, y)   (fmod((x), (y)))
#endif


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = 8765.4321E-10;
   float n3 = 0;
   float n4 = 1;
   float n5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[5] = { &n1, &n2, &n3, &n4, &n5 };

   double dn1 = 1234.5678E-10;
   double dn2 = 8765.4321E-10;
   double dn3 = 0;
   double dn4 = 1;
   double dn5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[5] = { &dn1, &dn2, &dn3, &dn4, &dn5 };
   
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = 8765.4321E-10;
   long double ldn3 = 0;
   long double ldn4 = 1;
   long double ldn5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[5] = { &ldn1, &ldn2, &ldn3, &ldn4, &ldn5 };
   
   /* Register the data pointers */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);
   
   fpitest_msg("float remainder");
   fptest_check_tol_float("rem1a", float_mod(n1, n1), 0);
   fptest_check_tol_float("rem1b", float_mod(n1, n2), 1.2345678E-7L);
   fptest_check_tol_float("rem1c", float_mod(n1, n4), 1234.5678E-10L);
   fptest_check_tol_float("rem1d", float_mod(n1, n5), 1234.5678E-10L);
   fptest_check_tol_float("rem1e", float_mod(n1 * 1E10, n4), 0.56786127462692093104L);
   fptest_check_tol_float("rem1f", float_mod(n1 * 1E10, n5), 0.56786127462692093104L);
   fptest_check_tol_float("rem1g", float_mod(n2, n1), 1.2345694244686455932e-08L);
   fptest_check_tol_float("rem1h", float_mod(n2, n2), 0);
   fptest_check_tol_float("rem1i", float_mod(n2, n4), 8765.4321E-10L);
   fptest_check_tol_float("rem1j", float_mod(n2, n5), 8765.4321E-10L);
   fptest_check_tol_float("rem1k", float_mod(n2 * 1E10, n4), 0.43197136925300583243L);
   fptest_check_tol_float("rem1l", float_mod(n2 * 1E10, n5), 0.43197136925300583243L);
   fptest_check_tol_float("rem1m", float_mod(n3, n1), 0);
   fptest_check_tol_float("rem1n", float_mod(n3, n2), 0);
   fptest_check_tol_float("rem1o", float_mod(n3, n4), 0);
   fptest_check_tol_float("rem1p", float_mod(n3, n5), 0);
   fptest_check_tol_float("rem1q", float_mod(n4, n1), 3.2367552194045856595E-08L);
   fptest_check_tol_float("rem1r", float_mod(n4, n2), 7.6262324455456109717E-08L);
   fptest_check_tol_float("rem1s", float_mod(n4, n4), 0);
   fptest_check_tol_float("rem1t", float_mod(n4, n5), 0);
   fptest_check_tol_float("rem1u", float_mod(n5, n1), -3.2367552194045856595E-08L);
   fptest_check_tol_float("rem1v", float_mod(n5, n2), -7.6262324455456109717E-08L);
   fptest_check_tol_float("rem1w", float_mod(n5, n4), 0);
   fptest_check_tol_float("rem1x", float_mod(n5, n5), 0);

   fpitest_msg("double remainder");
   fptest_check_tol_double("rem2a", float_mod(dn1, dn1), 0);
   fptest_check_tol_double("rem2b", float_mod(dn1, dn2), 1.2345678E-7L);
   fptest_check_tol_double("rem2c", float_mod(dn1, dn4), 1234.5678E-10L);
   fptest_check_tol_double("rem2d", float_mod(dn1, dn5), 1234.5678E-10L);
   fptest_check_tol_double("rem2e", float_mod(n1 * 1E10, dn4), 0.56786127462692093104L);
   fptest_check_tol_double("rem2f", float_mod(n1 * 1E10, dn5), 0.56786127462692093104L);
   fptest_check_tol_double("rem2g", float_mod(dn2, dn1), 1.2345750000000122312E-08L);
   fptest_check_tol_double("rem2h", float_mod(dn2, dn2), 0);
   fptest_check_tol_double("rem2i", float_mod(dn2, dn4), 8765.4321E-10L);
   fptest_check_tol_double("rem2j", float_mod(dn2, dn5), 8765.4321E-10L);
   fptest_check_tol_double("rem2k", float_mod(n2 * 1E10, dn4), 0.43197136925300583243L);
   fptest_check_tol_double("rem2l", float_mod(n2 * 1E10, dn5), 0.43197136925300583243L);
   fptest_check_tol_double("rem2m", float_mod(dn3, dn1), 0);
   fptest_check_tol_double("rem2n", float_mod(dn3, dn2), 0);
   fptest_check_tol_double("rem2o", float_mod(dn3, dn4), 0);
   fptest_check_tol_double("rem2p", float_mod(dn3, dn5), 0);
   fptest_check_tol_double("rem2q", float_mod(dn4, dn1), 8.2000000099184401333E-08L);
   fptest_check_tol_double("rem2r", float_mod(dn4, dn2), 6.1587549958248175190E-08L);
   fptest_check_tol_double("rem2s", float_mod(dn4, dn4), 0);
   fptest_check_tol_double("rem2t", float_mod(dn4, dn5), 0);
   fptest_check_tol_double("rem2u", float_mod(dn5, dn1), -8.2000000099184401333E-08L);
   fptest_check_tol_double("rem2v", float_mod(dn5, dn2), -6.1587549958248175190E-08L);
   fptest_check_tol_double("rem2w", float_mod(dn5, dn4), 0);
   fptest_check_tol_double("rem2x", float_mod(dn5, dn5), 0);

   fpitest_msg("long double remainder");
   fptest_check_tol_long_double("rem3a", float_mod(ldn1, ldn1), 0);
   fptest_check_tol_long_double("rem3b", float_mod(ldn1, ldn2), 1.2345678E-7L);
   fptest_check_tol_long_double("rem3c", float_mod(ldn1, ldn4), 1234.5678E-10L);
   fptest_check_tol_long_double("rem3d", float_mod(ldn1, ldn5), 1234.5678E-10L);
   fptest_check_tol_long_double("rem3e", float_mod(n1 * 1E10, ldn4), 0.56786127462692093104L);
   fptest_check_tol_long_double("rem3f", float_mod(n1 * 1E10, ldn5), 0.56786127462692093104L);
   fptest_check_tol_long_double("rem3g", float_mod(ldn2, ldn1), 1.2345750000000122312E-08L);
   fptest_check_tol_long_double("rem3h", float_mod(ldn2, ldn2), 0);
   fptest_check_tol_long_double("rem3i", float_mod(ldn2, ldn4), 8765.4321E-10L);
   fptest_check_tol_long_double("rem3j", float_mod(ldn2, ldn5), 8765.4321E-10L);
   fptest_check_tol_long_double("rem3k", float_mod(n2 * 1E10, ldn4), 0.43197136925300583243L);
   fptest_check_tol_long_double("rem3l", float_mod(n2 * 1E10, ldn5), 0.43197136925300583243L);
   fptest_check_tol_long_double("rem3m", float_mod(ldn3, ldn1), 0);
   fptest_check_tol_long_double("rem3n", float_mod(ldn3, ldn2), 0);
   fptest_check_tol_long_double("rem3o", float_mod(ldn3, ldn4), 0);
   fptest_check_tol_long_double("rem3p", float_mod(ldn3, ldn5), 0);
   fptest_check_tol_long_double("rem3q", float_mod(ldn4, ldn1), 8.2000000099184401333E-08L);
   fptest_check_tol_long_double("rem3r", float_mod(ldn4, ldn2), 6.1587549958248175190E-08L);
   fptest_check_tol_long_double("rem3s", float_mod(ldn4, ldn4), 0);
   fptest_check_tol_long_double("rem3t", float_mod(ldn4, ldn5), 0);
   fptest_check_tol_long_double("rem3u", float_mod(ldn5, ldn1), -8.2000000099184401333E-08L);
   fptest_check_tol_long_double("rem3v", float_mod(ldn5, ldn2), -6.1587549958248175190E-08L);
   fptest_check_tol_long_double("rem3w", float_mod(ldn5, ldn4), 0);
   fptest_check_tol_long_double("rem3x", float_mod(ldn5, ldn5), 0);
   
   fpitest_msg("done");
   return(0);
   
}
