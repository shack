#include <fc.h>

typedef double ty_float;

/*
 * Fibonacci numbers.
 */
ty_float fib(ty_float i)
{
    if(i < 2) 
        return 1;
    else
        return fib(i - 1) + fib(i - 2);
    return 0;
}

/*
 * Factorial.
 */
ty_float fact(ty_float i)
{
    ty_float k = 1;

    while(i > 0) {
        k *= i;
        i--;
    }
    return k;
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    ty_float i;

    /* Check usage */
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <number>\n");
        return 1;
    }

    /* Get the number */
    i = atof(argv[1]);

    /* Print the results */
    print_string("fib(");
    print_float(i);
    print_string(") = ");
    print_float(fib(i));
    print_string("\n");

    print_string("fact(");
    print_float(i);
    print_string(") = ");
    print_float(fact(i));
    print_string("\n");
    return 0;
}
