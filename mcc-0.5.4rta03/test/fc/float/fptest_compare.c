#include <fc.h>


int main(int argc, char **argv) {

   float n1 = 12345678;
   float n2 = 1.2345678E10;
   float n3 = -1.2345678E-10;
   float n4 = 0;
   float n5 = n2;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[5] = { &n1, &n2, &n3, &n4, &n5 };
   
   double dn1 = 12345678;
   double dn2 = 1.2345678E30;
   double dn3 = -1.2345678E-30;
   double dn4 = 0;
   double dn5 = dn2;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[5] = { &dn1, &dn2, &dn3, &dn4, &dn5 };
   
   long double ldn1 = 12345678;
   long double ldn2 = 1.2345678E50;
   long double ldn3 = -1.2345678E-50;
   long double ldn4 = 0;
   long double ldn5 = ldn2;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[5] = { &ldn1, &ldn2, &ldn3, &ldn4, &ldn5 };
   
   /* Register the data pointers. */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);

   fpitest_msg("equality");
   fpitest_check_true ("eq1a", n1 == n1);
   fpitest_check_true ("eq1b", n2 == n2);
   fpitest_check_true ("eq1c", n3 == n3);
   fpitest_check_true ("eq1d", n4 == n4);
   fpitest_check_true ("eq1e", n5 == n5);
   fpitest_check_true ("eq2a", dn1 == dn1);
   fpitest_check_true ("eq2b", dn2 == dn2);
   fpitest_check_true ("eq2c", dn3 == dn3);
   fpitest_check_true ("eq2d", dn4 == dn4);
   fpitest_check_true ("eq2e", dn5 == dn5);
   fpitest_check_true ("eq3a", ldn1 == ldn1);
   fpitest_check_true ("eq3b", ldn2 == ldn2);
   fpitest_check_true ("eq3c", ldn3 == ldn3);
   fpitest_check_true ("eq3d", ldn4 == ldn4);
   fpitest_check_true ("eq3e", ldn5 == ldn5);
   fpitest_check_false("eq4a", n1 == n2);
   fpitest_check_false("eq4b", n2 == n3);
   fpitest_check_false("eq4c", n3 == n4);
   fpitest_check_false("eq4d", n4 == n5);
   fpitest_check_false("eq4e", dn1 == dn2);
   fpitest_check_false("eq4f", dn2 == dn3);
   fpitest_check_false("eq4g", dn3 == dn4);
   fpitest_check_false("eq4h", dn4 == dn5);
   fpitest_check_false("eq4i", ldn1 == ldn2);
   fpitest_check_false("eq4j", ldn2 == ldn3);
   fpitest_check_false("eq4k", ldn3 == ldn4);
   fpitest_check_false("eq4l", ldn4 == ldn5);
   
   fpitest_msg("nonequality");
   fpitest_check_false("neq1a", n1 != n1);
   fpitest_check_false("neq1b", n2 != n2);
   fpitest_check_false("neq1c", n3 != n3);
   fpitest_check_false("neq1d", n4 != n4);
   fpitest_check_false("neq1e", n5 != n5);
   fpitest_check_false("neq2a", dn1 != dn1);
   fpitest_check_false("neq2b", dn2 != dn2);
   fpitest_check_false("neq2c", dn3 != dn3);
   fpitest_check_false("neq2d", dn4 != dn4);
   fpitest_check_false("neq2e", dn5 != dn5);
   fpitest_check_false("neq3a", ldn1 != ldn1);
   fpitest_check_false("neq3b", ldn2 != ldn2);
   fpitest_check_false("neq3c", ldn3 != ldn3);
   fpitest_check_false("neq3d", ldn4 != ldn4);
   fpitest_check_false("neq3e", ldn5 != ldn5);
   fpitest_check_true ("neq4a", n1 != n2);
   fpitest_check_true ("neq4b", n2 != n3);
   fpitest_check_true ("neq4c", n3 != n4);
   fpitest_check_true ("neq4d", n4 != n5);
   fpitest_check_true ("neq4e", dn1 != dn2);
   fpitest_check_true ("neq4f", dn2 != dn3);
   fpitest_check_true ("neq4g", dn3 != dn4);
   fpitest_check_true ("neq4h", dn4 != dn5);
   fpitest_check_true ("neq4i", ldn1 != ldn2);
   fpitest_check_true ("neq4j", ldn2 != ldn3);
   fpitest_check_true ("neq4k", ldn3 != ldn4);
   fpitest_check_true ("neq4l", ldn4 != ldn5);
   
   fpitest_msg("greater than");
   fpitest_check_false("gt1a", n1 > n2);
   fpitest_check_true ("gt1b", n2 > n3);
   fpitest_check_false("gt1c", n3 > n4);
   fpitest_check_false("gt1d", n4 > n5);
   fpitest_check_false("gt1e", n1 > n1);
   fpitest_check_false("gt1f", n2 > n2);
   fpitest_check_false("gt1g", n3 > n3);
   fpitest_check_false("gt1h", n4 > n4);
   fpitest_check_false("gt1i", n5 > n5);
   fpitest_check_false("gt2a", dn1 > dn2);
   fpitest_check_true ("gt2b", dn2 > dn3);
   fpitest_check_false("gt2c", dn3 > dn4);
   fpitest_check_false("gt2d", dn4 > dn5);
   fpitest_check_false("gt2e", dn1 > dn1);
   fpitest_check_false("gt2f", dn2 > dn2);
   fpitest_check_false("gt2g", dn3 > dn3);
   fpitest_check_false("gt2h", dn4 > dn4);
   fpitest_check_false("gt2i", dn5 > dn5);
   fpitest_check_false("gt3a", ldn1 > ldn2);
   fpitest_check_true ("gt3b", ldn2 > ldn3);
   fpitest_check_false("gt3c", ldn3 > ldn4);
   fpitest_check_false("gt3d", ldn4 > ldn5);
   fpitest_check_false("gt3e", ldn1 > ldn1);
   fpitest_check_false("gt3f", ldn2 > ldn2);
   fpitest_check_false("gt3g", ldn3 > ldn3);
   fpitest_check_false("gt3h", ldn4 > ldn4);
   fpitest_check_false("gt3i", ldn5 > ldn5);

   fpitest_msg("less than");
   fpitest_check_true ("lt1a", n1 < n2);
   fpitest_check_false("lt1b", n2 < n3);
   fpitest_check_true ("lt1c", n3 < n4);
   fpitest_check_true ("lt1d", n4 < n5);
   fpitest_check_false("lt1e", n1 < n1);
   fpitest_check_false("lt1f", n2 < n2);
   fpitest_check_false("lt1g", n3 < n3);
   fpitest_check_false("lt1h", n4 < n4);
   fpitest_check_false("lt1i", n5 < n5);
   fpitest_check_true ("lt2a", dn1 < dn2);
   fpitest_check_false("lt2b", dn2 < dn3);
   fpitest_check_true ("lt2c", dn3 < dn4);
   fpitest_check_true ("lt2d", dn4 < dn5);
   fpitest_check_false("lt2e", dn1 < dn1);
   fpitest_check_false("lt2f", dn2 < dn2);
   fpitest_check_false("lt2g", dn3 < dn3);
   fpitest_check_false("lt2h", dn4 < dn4);
   fpitest_check_false("lt2i", dn5 < dn5);
   fpitest_check_true ("lt3a", ldn1 < ldn2);
   fpitest_check_false("lt3b", ldn2 < ldn3);
   fpitest_check_true ("lt3c", ldn3 < ldn4);
   fpitest_check_true ("lt3d", ldn4 < ldn5);
   fpitest_check_false("lt3e", ldn1 < ldn1);
   fpitest_check_false("lt3f", ldn2 < ldn2);
   fpitest_check_false("lt3g", ldn3 < ldn3);
   fpitest_check_false("lt3h", ldn4 < ldn4);
   fpitest_check_false("lt3i", ldn5 < ldn5);

   fpitest_msg("greater than or equal");
   fpitest_check_false("ge1a", n1 >= n2);
   fpitest_check_true ("ge1b", n2 >= n3);
   fpitest_check_false("ge1c", n3 >= n4);
   fpitest_check_false("ge1d", n4 >= n5);
   fpitest_check_true ("ge1e", n1 >= n1);
   fpitest_check_true ("ge1f", n2 >= n2);
   fpitest_check_true ("ge1g", n3 >= n3);
   fpitest_check_true ("ge1h", n4 >= n4);
   fpitest_check_true ("ge1i", n5 >= n5);
   fpitest_check_false("ge2a", dn1 >= dn2);
   fpitest_check_true ("ge2b", dn2 >= dn3);
   fpitest_check_false("ge2c", dn3 >= dn4);
   fpitest_check_false("ge2d", dn4 >= dn5);
   fpitest_check_true ("ge2e", dn1 >= dn1);
   fpitest_check_true ("ge2f", dn2 >= dn2);
   fpitest_check_true ("ge2g", dn3 >= dn3);
   fpitest_check_true ("ge2h", dn4 >= dn4);
   fpitest_check_true ("ge2i", dn5 >= dn5);
   fpitest_check_false("ge3a", ldn1 >= ldn2);
   fpitest_check_true ("ge3b", ldn2 >= ldn3);
   fpitest_check_false("ge3c", ldn3 >= ldn4);
   fpitest_check_false("ge3d", ldn4 >= ldn5);
   fpitest_check_true ("ge3e", ldn1 >= ldn1);
   fpitest_check_true ("ge3f", ldn2 >= ldn2);
   fpitest_check_true ("ge3g", ldn3 >= ldn3);
   fpitest_check_true ("ge3h", ldn4 >= ldn4);
   fpitest_check_true ("ge3i", ldn5 >= ldn5);

   fpitest_msg("less than or equal");
   fpitest_check_true ("le1a", n1 <= n2);
   fpitest_check_false("le1b", n2 <= n3);
   fpitest_check_true ("le1c", n3 <= n4);
   fpitest_check_true ("le1d", n4 <= n5);
   fpitest_check_true ("le1e", n1 <= n1);
   fpitest_check_true ("le1f", n2 <= n2);
   fpitest_check_true ("le1g", n3 <= n3);
   fpitest_check_true ("le1h", n4 <= n4);
   fpitest_check_true ("le1i", n5 <= n5);
   fpitest_check_true ("le2a", dn1 <= dn2);
   fpitest_check_false("le2b", dn2 <= dn3);
   fpitest_check_true ("le2c", dn3 <= dn4);
   fpitest_check_true ("le2d", dn4 <= dn5);
   fpitest_check_true ("le2e", dn1 <= dn1);
   fpitest_check_true ("le2f", dn2 <= dn2);
   fpitest_check_true ("le2g", dn3 <= dn3);
   fpitest_check_true ("le2h", dn4 <= dn4);
   fpitest_check_true ("le2i", dn5 <= dn5);
   fpitest_check_true ("le3a", ldn1 <= ldn2);
   fpitest_check_false("le3b", ldn2 <= ldn3);
   fpitest_check_true ("le3c", ldn3 <= ldn4);
   fpitest_check_true ("le3d", ldn4 <= ldn5);
   fpitest_check_true ("le3e", ldn1 <= ldn1);
   fpitest_check_true ("le3f", ldn2 <= ldn2);
   fpitest_check_true ("le3g", ldn3 <= ldn3);
   fpitest_check_true ("le3h", ldn4 <= ldn4);
   fpitest_check_true ("le3i", ldn5 <= ldn5);
   
   fpitest_msg("done");
   return(0);

}
