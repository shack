/* Test multiplication and division 
   Justin David Smith */

#include <fc.h>

typedef double ty_float;

int main(int argc, char **argv) {

   /* Check basic arithmetic operations */
   ty_float f0 = 123.456;
   ty_float f1 = 987.654;
   
   ty_float fmul1 = f0 * f1;
   ty_float fmul2 = (-f0) * f1;
   if(fmul1 < 121931.0 || fmul1 > 121932.0) print_string("mul test 1 FAILED\n");
   if(fmul2 < -121932.0 || fmul2 > -121931.0) print_string("mul test 2 FAILED\n");
   print_string("fmul1 = ");
   print_float(fmul1);
   print_string(", fmul2 = ");
   print_float(fmul2);
   print_string("\n");
   
   ty_float fdiv1 = f1 / f0;
   ty_float fdiv2 = f0 / f1;
   ty_float fdiv3 = f1 / (-f0);
   if(fdiv1 < 8.00004 || fdiv1 > 8.00006) print_string("div test 1 FAILED\n");
   if(fdiv2 < 0.124 || fdiv2 > 0.125) print_string("div test 2 FAILED\n");
   if(fdiv3 > -8.00004 || fdiv3 < -8.00006) print_string("div test 3 FAILED\n");
   print_string("fdiv1 = ");
   print_float(fdiv1);
   print_string(", fdiv2 = ");
   print_float(fdiv2);
   print_string(", fdiv3 = ");
   print_float(fdiv3);
   print_string("\n");

   return(0);
}
