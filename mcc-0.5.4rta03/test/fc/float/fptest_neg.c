#include <fc.h>


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = 8765.4321E-10;
   float n3 = 0;
   float neg_n1;
   float neg_n2;
   float neg_n3;
   float neg_neg_n1;
   float neg_neg_n2;
   float neg_neg_n3;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[3] = { &n1, &n2, &n3 };
   
   double dn1 = 1234.5678E-10;
   double dn2 = 8765.4321E-10;
   double dn3 = 0;
   double neg_dn1;
   double neg_dn2;
   double neg_dn3;
   double neg_neg_dn1;
   double neg_neg_dn2;
   double neg_neg_dn3;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[3] = { &dn1, &dn2, &dn3 };
   
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = 8765.4321E-10;
   long double ldn3 = 0;
   long double neg_ldn1;
   long double neg_ldn2;
   long double neg_ldn3;
   long double neg_neg_ldn1;
   long double neg_neg_ldn2;
   long double neg_neg_ldn3;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[3] = { &ldn1, &ldn2, &ldn3 };
   
   /* Register the data pointers. */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);
   
   fpitest_msg("float negation");
   neg_n1 = -n1;
   neg_n2 = -n2;
   neg_n3 = -n3;
   neg_neg_n1 = -neg_n1;
   neg_neg_n2 = -neg_n2;
   neg_neg_n3 = -neg_n3;
   fptest_check_tol_float("neg1a", neg_n1, -1234.5678E-10);
   fptest_check_tol_float("neg1b", neg_n2, -8765.4321E-10);
   fptest_check_tol_float("neg1c", neg_n3, 0);
   fptest_check_tol_float("neg1d", neg_neg_n1, 1234.5678E-10);
   fptest_check_tol_float("neg1e", neg_neg_n2, 8765.4321E-10);
   fptest_check_tol_float("neg1f", neg_neg_n3, 0);

   fpitest_msg("double negation");
   neg_dn1 = -dn1;
   neg_dn2 = -dn2;
   neg_dn3 = -dn3;
   neg_neg_dn1 = -neg_dn1;
   neg_neg_dn2 = -neg_dn2;
   neg_neg_dn3 = -neg_dn3;
   fptest_check_tol_double("neg2a", neg_dn1, -1234.5678E-10);
   fptest_check_tol_double("neg2b", neg_dn2, -8765.4321E-10);
   fptest_check_tol_double("neg2c", neg_dn3, 0);
   fptest_check_tol_double("neg2d", neg_neg_dn1, 1234.5678E-10);
   fptest_check_tol_double("neg2e", neg_neg_dn2, 8765.4321E-10);
   fptest_check_tol_double("neg2f", neg_neg_dn3, 0);

   fpitest_msg("long double negation");
   neg_ldn1 = -ldn1;
   neg_ldn2 = -ldn2;
   neg_ldn3 = -ldn3;
   neg_neg_ldn1 = -neg_ldn1;
   neg_neg_ldn2 = -neg_ldn2;
   neg_neg_ldn3 = -neg_ldn3;
   fptest_check_tol_long_double("neg3a", neg_ldn1, -1234.5678E-10);
   fptest_check_tol_long_double("neg3b", neg_ldn2, -8765.4321E-10);
   fptest_check_tol_long_double("neg3c", neg_ldn3, 0);
   fptest_check_tol_long_double("neg3d", neg_neg_ldn1, 1234.5678E-10);
   fptest_check_tol_long_double("neg3e", neg_neg_ldn2, 8765.4321E-10);
   fptest_check_tol_long_double("neg3f", neg_neg_ldn3, 0);
   
   fpitest_msg("done");
   return(0);
   
}
