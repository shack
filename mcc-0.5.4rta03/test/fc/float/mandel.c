/*
 * Benchmark.
 * Compute Mandelbrot set over a small space.
 */
#include <fc.h>

/*
 * What precision.
 */
typedef long double ty_float;

/*
 * Program constants
 */
static int factor = 16;
static int limit = 255;
static int dimen = 400;
static int bound = 4096;

/*
 * Take an approximate log of the number.
 */
static int log(int k)
{
    if(k < 128)
        ;
    else if(k < 256)
        k = k / 2 + 128;
    else
        k = k / 64 + 192;
    return k;
}

/*
 * Compute a point in the Mandelbrot set.
 */
static int compute(ty_float x, ty_float y)
{
    ty_float usex, usey;
    ty_float xsqr, ysqr;
    int k;

    usex = x;
    usey = y;
    for(k = limit * factor; k; k--) {
        xsqr = usex * usex;
        ysqr = usey * usey;
        if(xsqr + ysqr > 4)
            break;
        usey = 2 * usex * usey + y;
        usex = xsqr - ysqr + x;
    }
    print_int(k / factor);
    print_string(" ");
    return k;
}

/*
 * Main function.
 */
int main(int argc, char **argv)
{
    ty_float x, y, left, top, step, scale;
    int i, j;

    /* Check usage */
    if(argc != 4) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" x y scale)\n");
        print_string("Examples:\n");
        print_string("\ttest5 0 0 2 > x.pgm\n");
        print_string("\ttest5 0.12 -0.640 0.1 > x.pgm\n");
        print_string("To view it, use xv\n");
        print_string("\txv x.pgm\n");
        return 1;
    }

    /* Set up screen area */
    left = atof(argv[1]);
    top = atof(argv[2]);
    scale = atof(argv[3]);
    left = left - scale / 2;
    top = top - scale / 2;
    step = scale / dimen;

    /* Print the PGM header */
    print_string("P2\n# CREATOR: mandel\n");
    print_int(dimen);
    print_string(" ");
    print_int(dimen);
    print_string("\n255\n");

    /* Plot each pixel */
    for(i = 0; i != dimen; i++) {
        for(j = 0; j != dimen; j++) {
            x = left + step * i;
            y = top + step * j;
            compute(x, y);
        }
        print_string("\n");
    }
    return 0;
}
