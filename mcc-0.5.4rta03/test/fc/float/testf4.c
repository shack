/* Test addition and subtraction 
   Justin David Smith */

#include <fc.h>

typedef double ty_float;

int main(int argc, char **argv) {

   /* Check basic arithmetic operations */
   ty_float f0 = 123.456;
   ty_float f1 = 987.654;
   if(-f0 != -123.456 || -f1 != -987.654) print_string("negate test FAILED\n");
   
   ty_float fadd1 = f0 + f1;
   ty_float fadd2 = (-f0) + (-f1);
   if(fadd1 < 1111.10999999 || fadd1 > 1111.11000001) print_string("add test 1 FAILED\n");
   if(fadd2 <-1111.11000001 || fadd2 >-1111.10999999) print_string("add test 2 FAILED\n");
   print_string("fadd1 = ");
   print_float(fadd1);
   print_string(", fadd2 = ");
   print_float(fadd2);
   print_string("\n");
   
   ty_float fsub1 = f1 - f0;
   ty_float fsub2 = f0 - f1;
   ty_float fsub3 = f1 - (-f0);
   if(fsub1 <= 864.1979999 || fsub1 >= 864.1980001) print_string("sub test 1 FAILED\n");
   if(fsub2 <=-864.1980001 || fsub2 >=-864.1979999) print_string("sub test 2 FAILED\n");
   if(fsub3 <= 1111.109999 || fsub3 >= 1111.110001) print_string("sub test 3 FAILED\n");
   print_string("fsub1 = ");
   print_float(fsub1);
   print_string(", fsub2 = ");
   print_float(fsub2);
   print_string(", fsub3 = ");
   print_float(fsub3);
   print_string("\n");

   return(0);
}
