#include <fc.h>


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = 8765.4321E-10;
   float n3 = 0;
   float n4 = 1;
   float n5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[5] = { &n1, &n2, &n3, &n4, &n5 };

   double dn1 = 1234.5678E-10;
   double dn2 = 8765.4321E-10;
   double dn3 = 0;
   double dn4 = 1;
   double dn5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[5] = { &dn1, &dn2, &dn3, &dn4, &dn5 };
   
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = 8765.4321E-10;
   long double ldn3 = 0;
   long double ldn4 = 1;
   long double ldn5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[5] = { &ldn1, &ldn2, &ldn3, &ldn4, &ldn5 };
   
   /* Register the data pointers */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);
   
   fpitest_msg("float multiplication");
   fptest_check_tol_float("mul1a", n1 * n2, 10821520.22374638E-20L);
   fptest_check_tol_float("mul1b", n1 * n3, 0);
   fptest_check_tol_float("mul1c", n1 * n4, 1234.5678E-10L);
   fptest_check_tol_float("mul1d", n1 * n5, -1234.5678E-10L);
   fptest_check_tol_float("mul1e", n2 * n3, 0);
   fptest_check_tol_float("mul1f", n2 * n4, 8765.4321E-10L);
   fptest_check_tol_float("mul1g", n2 * n5, -8765.4321E-10L);
   fptest_check_tol_float("mul1h", n3 * n4, 0);
   fptest_check_tol_float("mul1i", n3 * n5, 0);
   fptest_check_tol_float("mul1j", n4 * n5, -1);
   fptest_check_tol_float("mul1k", n1 * n1, 1524157.65279684E-20L);
   fptest_check_tol_float("mul1l", n2 * n2, 76832799.89971041E-20L);
   fptest_check_tol_float("mul1m", n3 * n3, 0);
   fptest_check_tol_float("mul1n", n4 * n4, 1);
   fptest_check_tol_float("mul1o", n5 * n5, 1);

   fpitest_msg("double multiplication");
   fptest_check_tol_double("mul2a", dn1 * dn2, 10821520.22374638E-20L);
   fptest_check_tol_double("mul2b", dn1 * dn3, 0);
   fptest_check_tol_double("mul2c", dn1 * dn4, 1234.5678E-10L);
   fptest_check_tol_double("mul2d", dn1 * dn5, -1234.5678E-10L);
   fptest_check_tol_double("mul2e", dn2 * dn3, 0);
   fptest_check_tol_double("mul2f", dn2 * dn4, 8765.4321E-10L);
   fptest_check_tol_double("mul2g", dn2 * dn5, -8765.4321E-10L);
   fptest_check_tol_double("mul2h", dn3 * dn4, 0);
   fptest_check_tol_double("mul2i", dn3 * dn5, 0);
   fptest_check_tol_double("mul2j", dn4 * dn5, -1);
   fptest_check_tol_double("mul2k", dn1 * dn1, 1524157.65279684E-20L);
   fptest_check_tol_double("mul2l", dn2 * dn2, 76832799.89971041E-20L);
   fptest_check_tol_double("mul2m", dn3 * dn3, 0);
   fptest_check_tol_double("mul2n", dn4 * dn4, 1);
   fptest_check_tol_double("mul2o", dn5 * dn5, 1);

   fpitest_msg("long double multiplication");
   fptest_check_tol_long_double("mul3a", ldn1 * ldn2, 10821520.22374638E-20L);
   fptest_check_tol_long_double("mul3b", ldn1 * ldn3, 0);
   fptest_check_tol_long_double("mul3c", ldn1 * ldn4, 1234.5678E-10L);
   fptest_check_tol_long_double("mul3d", ldn1 * ldn5, -1234.5678E-10L);
   fptest_check_tol_long_double("mul3e", ldn2 * ldn3, 0);
   fptest_check_tol_long_double("mul3f", ldn2 * ldn4, 8765.4321E-10L);
   fptest_check_tol_long_double("mul3g", ldn2 * ldn5, -8765.4321E-10L);
   fptest_check_tol_long_double("mul3h", ldn3 * ldn4, 0);
   fptest_check_tol_long_double("mul3i", ldn3 * ldn5, 0);
   fptest_check_tol_long_double("mul3j", ldn4 * ldn5, -1);
   fptest_check_tol_long_double("mul3k", ldn1 * ldn1, 1524157.65279684E-20L);
   fptest_check_tol_long_double("mul3l", ldn2 * ldn2, 76832799.89971041E-20L);
   fptest_check_tol_long_double("mul3m", ldn3 * ldn3, 0);
   fptest_check_tol_long_double("mul3n", ldn4 * ldn4, 1);
   fptest_check_tol_long_double("mul3o", ldn5 * ldn5, 1);
   
   fpitest_msg("done");
   return(0);
   
}
