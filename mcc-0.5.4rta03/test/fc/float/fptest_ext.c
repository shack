#include <fc.h>


static void print_float_newline(float f) {

   print_float(f);
   print_string("\n");

}


static void print_double_newline(double f) {

   print_double(f);
   print_string("\n");

}


static void print_long_double_newline(long double f) {

   print_long_double(f);
   print_string("\n");

}


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = -1234.5678E10;
   float n3 = 0;
   float n4 = -0;
   double dn1 = 1234.5678E-10;
   double dn2 = -1234.5678E10;
   double dn3 = 0;
   double dn4 = -0;
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = -1234.5678E10;
   long double ldn3 = 0;
   long double ldn4 = -0;

   print_float_newline(n1);
   print_float_newline(n2);
   print_float_newline(n3);
   print_float_newline(n4);
   print_float_newline(dn1);
   print_float_newline(dn2);
   print_float_newline(dn3);
   print_float_newline(dn4);
   print_float_newline(ldn1);
   print_float_newline(ldn2);
   print_float_newline(ldn3);
   print_float_newline(ldn4);
   print_float_newline(1234.5678E-10);
   print_float_newline(-1234.5678E10);
   print_float_newline(0);
   print_float_newline(-0);
   print_string("\n");

   print_double_newline(n1);
   print_double_newline(n2);
   print_double_newline(n3);
   print_double_newline(n4);
   print_double_newline(dn1);
   print_double_newline(dn2);
   print_double_newline(dn3);
   print_double_newline(dn4);
   print_double_newline(ldn1);
   print_double_newline(ldn2);
   print_double_newline(ldn3);
   print_double_newline(ldn4);
   print_double_newline(1234.5678E-10);
   print_double_newline(-1234.5678E10);
   print_double_newline(0);
   print_double_newline(-0);
   print_string("\n");

   print_long_double_newline(n1);
   print_long_double_newline(n2);
   print_long_double_newline(n3);
   print_long_double_newline(n4);
   print_long_double_newline(dn1);
   print_long_double_newline(dn2);
   print_long_double_newline(dn3);
   print_long_double_newline(dn4);
   print_long_double_newline(ldn1);
   print_long_double_newline(ldn2);
   print_long_double_newline(ldn3);
   print_long_double_newline(ldn4);
   print_long_double_newline(1234.5678E-10);
   print_long_double_newline(-1234.5678E10);
   print_long_double_newline(0);
   print_long_double_newline(-0);
   print_string("\n");
   
   return(0);

}
