#include <fc.h>


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = 8765.4321E-10;
   float n3 = 0;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[3] = { &n1, &n2, &n3 };
   
   double dn1 = 1234.5678E-10;
   double dn2 = 8765.4321E-10;
   double dn3 = 0;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[3] = { &dn1, &dn2, &dn3 };
   
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = 8765.4321E-10;
   long double ldn3 = 0;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[3] = { &ldn1, &ldn2, &ldn3 };
   
   float neg_n1 = -n1;
   float neg_n2 = -n2;
   float neg_n3 = -n3;
   double neg_dn1 = -dn1;
   double neg_dn2 = -dn2;
   double neg_dn3 = -dn3;
   long double neg_ldn1 = -ldn1;
   long double neg_ldn2 = -ldn2;
   long double neg_ldn3 = -ldn3;
   
   /* Lock the escaping arrays */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);
   
   fpitest_msg("float addition");
   fptest_check_tol_float("add1a", n1 + n2, 9999.9999E-10);
   fptest_check_tol_float("add1b", n1 + n3, 1234.5678E-10);
   fptest_check_tol_float("add1c", n2 + n3, 8765.4321E-10);
   fptest_check_tol_float("add1d", n1 + n1, 2469.1356E-10);
   fptest_check_tol_float("add1e", n2 + n2, 17530.8642E-10);
   fptest_check_tol_float("add1f", n3 + n3, 0);
   fptest_check_tol_float("add1g", n1 + n2 + n3, 9999.9999E-10);
   fptest_check_tol_float("add1h", n1 + neg_n1, 0);
   fptest_check_tol_float("add1i", n2 + neg_n2, 0);
   fptest_check_tol_float("add1j", n3 + neg_n3, 0);
   fptest_check_tol_float("add1k", n1 + n2, 9999.9999E-10);
   fptest_check_tol_float("add1l", n2 + n1, 9999.9999E-10);
   fptest_check_tol_float("add1m", n1 + neg_n2, -7530.8643E-10);
   fptest_check_tol_float("add1n", n2 + neg_n1, 7530.8643E-10);
   fptest_check_tol_float("add1o", n3 + neg_n1, -1234.5678E-10);
   fptest_check_tol_float("add1p", n3 + neg_n2, -8765.4321E-10);
   fptest_check_tol_float("add1q", n3 + neg_n3 + n3 + neg_n3 + n3, 0);

   fpitest_msg("double addition");
   fptest_check_tol_double("add2a", dn1 + dn2, 9999.9999E-10);
   fptest_check_tol_double("add2b", dn1 + dn3, 1234.5678E-10);
   fptest_check_tol_double("add2c", dn2 + dn3, 8765.4321E-10);
   fptest_check_tol_double("add2d", dn1 + dn1, 2469.1356E-10);
   fptest_check_tol_double("add2e", dn2 + dn2, 17530.8642E-10);
   fptest_check_tol_double("add2f", dn3 + dn3, 0);
   fptest_check_tol_double("add2g", dn1 + dn2 + dn3, 9999.9999E-10);
   fptest_check_tol_double("add2h", dn1 + neg_dn1, 0);
   fptest_check_tol_double("add2i", dn2 + neg_dn2, 0);
   fptest_check_tol_double("add2j", dn3 + neg_dn3, 0);
   fptest_check_tol_double("add2k", dn1 + dn2, 9999.9999E-10);
   fptest_check_tol_double("add2l", dn2 + dn1, 9999.9999E-10);
   fptest_check_tol_double("add2m", dn1 + neg_dn2, -7530.8643E-10);
   fptest_check_tol_double("add2n", dn2 + neg_dn1, 7530.8643E-10);
   fptest_check_tol_double("add2o", dn3 + neg_dn1, -1234.5678E-10);
   fptest_check_tol_double("add2p", dn3 + neg_dn2, -8765.4321E-10);
   fptest_check_tol_double("add2q", dn3 + neg_dn3 + dn3 + neg_dn3 + dn3, 0);

   fpitest_msg("long double addition");
   fptest_check_tol_long_double("add3a", ldn1 + ldn2, 9999.9999E-10);
   fptest_check_tol_long_double("add3b", ldn1 + ldn3, 1234.5678E-10);
   fptest_check_tol_long_double("add3c", ldn2 + ldn3, 8765.4321E-10);
   fptest_check_tol_long_double("add3d", ldn1 + ldn1, 2469.1356E-10);
   fptest_check_tol_long_double("add3e", ldn2 + ldn2, 17530.8642E-10);
   fptest_check_tol_long_double("add3f", ldn3 + ldn3, 0);
   fptest_check_tol_long_double("add3g", ldn1 + ldn2 + ldn3, 9999.9999E-10);
   fptest_check_tol_long_double("add3h", ldn1 + neg_ldn1, 0);
   fptest_check_tol_long_double("add3i", ldn2 + neg_ldn2, 0);
   fptest_check_tol_long_double("add3j", ldn3 + neg_ldn3, 0);
   fptest_check_tol_long_double("add3k", ldn1 + ldn2, 9999.9999E-10);
   fptest_check_tol_long_double("add3l", ldn2 + ldn1, 9999.9999E-10);
   fptest_check_tol_long_double("add3m", ldn1 + neg_ldn2, -7530.8643E-10);
   fptest_check_tol_long_double("add3n", ldn2 + neg_ldn1, 7530.8643E-10);
   fptest_check_tol_long_double("add3o", ldn3 + neg_ldn1, -1234.5678E-10);
   fptest_check_tol_long_double("add3p", ldn3 + neg_ldn2, -8765.4321E-10);
   fptest_check_tol_long_double("add3q", ldn3 + neg_ldn3 + ldn3 + neg_ldn3 + ldn3, 0);
   
   fpitest_msg("done");
   return(0);
   
}
