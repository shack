/* Test conditionals 
   Justin David Smith */

#include <fc.h>

typedef double ty_float;

int main(int argc, char **argv) {

   /* Conditional checking */
   ty_float f0 = 131.215;
   ty_float f1 = f0;
   int result1 = (f0 == f1);
   int result2 = (f0 != f1);
   if(!result1 || result2) print_string("equality test FAILED\n");
   print_string("equality: ");
   print_int(result1);
   print_string(", inequality: ");
   print_int(result2);
   print_string(", f0 = ");
   print_float(f0);
   print_string(", f1 = ");
   print_float(f1);
   print_string("\n");
   
   f0 = f1 + 1.0;
   result1 = (f0 >  f1);
   result2 = (f0 <= f1);
   if(!result1 || result2) print_string("inequality 1 test FAILED\n");
   print_string("gt: ");
   print_int(result1);
   print_string(", leq: ");
   print_int(result2);
   print_string(", f0 = ");
   print_float(f0);
   print_string(", f1 = ");
   print_float(f1);
   print_string("\n");
   
   f1 = f0 + 1.0;
   result1 = (f0 >  f1);
   result2 = (f0 <= f1);
   if(!result2 || result1) print_string("inequality 2 test FAILED\n");
   print_string("gt: ");
   print_int(result1);
   print_string(", leq: ");
   print_int(result2);
   print_string(", f0 = ");
   print_float(f0);
   print_string(", f1 = ");
   print_float(f1);
   print_string("\n");
   return(0);

}
