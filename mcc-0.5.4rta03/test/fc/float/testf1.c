/* The original FP test 
   Justin David Smith */

#include <fc.h>

typedef float ty_float;

ty_float f(ty_float x)
{
    return x + 2;
}

int main(int argc, char **argv)
{
   ty_float f1 = 2.71828;
   ty_float f2 = 3.14159;
   ty_float f3 = f1 + f2;

   print_string("Initial values:\n");
   print_string("\tf1: ");
   print_float(f1);
   print_string("\n\tf2: ");
   print_float(f2);
   print_string("\n\tf3: ");
   print_float(f3);

   print_string("\nexpecting 6, got:          ");
   print_int(f3);
   print_string(", ");
   print_float(f3);

   print_string("\nexpecting around 55, got:  ");
   print_int(f3 - f2 + 52.3);
   print_string(", ");
   print_float(f3 - f2 + 52.3);

   print_string("\nexpecting around -.4, got: ");
   print_int(f1 - f2);
   print_string(", ");
   print_float(f1 - f2);

   print_string("\nexpecting around 586, got: ");
   print_int(f3 * 100); 
   print_string(", ");
   print_float(f3 * 100); 

   print_string("\nexpecting 8, got:          ");
   print_int(f(f3)); 
   print_string(", ");
   print_float(f(f3)); 
   print_string("\n"); 
   return(0);
}
