#include <fc.h>


int main(int argc, char **argv) {

   int i1 = 0xff008800;
   int i2 = 0xffffffff;
   int i3 = 0x00000000;
   int i4 = 0x00ff0088;
   /* By escaping these, we prevent optimizations from constant folding */
   int *i[4] = { &i1, &i2, &i3, &i4 };
   
   fc_int64_t l1 = 0xffff000088880000ULL;
   fc_int64_t l2 = 0xffffffffffffffffULL;
   fc_int64_t l3 = 0x0000000000000000ULL;
   fc_int64_t l4 = 0x0000ffff00008888ULL;
   /* By escaping these, we prevent optimizations from constant folding */
   fc_int64_t *l[4] = { &l1, &l2, &l3, &l4 };
   
   /* Forward variable declarations */
   double dn1,  dn2,  dn3,  dn4;
   int    idn1, idn2, idn3, idn4;
   double *dn[4] = { &dn1, &dn2, &dn3, &dn4 };
   long double ldn1,  ldn2,  ldn3,  ldn4;
   fc_int64_t  lldn1, lldn2, lldn3, lldn4;
   long double *ldn[4] = { &ldn1, &ldn2, &ldn3, &ldn4 };
   
   /* Register the pointer arrays */
   itest_data_int(i);
   itest_data_long(l);

   fpitest_msg("32-bit coercions");
   dn1 = i1;
   dn2 = i2;
   dn3 = i3;
   dn4 = i4;
   fptest_data_double(dn);
   fptest_check_tol_double("coerce32a", dn1, -16742400.0);
   fptest_check_tol_double("coerce32b", dn2, -1.0);
   fptest_check_tol_double("coerce32c", dn3, 0.0);
   fptest_check_tol_double("coerce32d", dn4, 16711816.0);
   idn1 = dn1;
   idn2 = dn2;
   idn3 = dn3;
   idn4 = dn4;
   fpitest_check_true("coerce32e", idn1 == i1);
   fpitest_check_true("coerce32f", idn2 == i2);
   fpitest_check_true("coerce32g", idn3 == i3);
   fpitest_check_true("coerce32h", idn4 == i4);

   fpitest_msg("64-bit coercions");
   ldn1 = l1;
   ldn2 = l2;
   ldn3 = l3;
   ldn4 = l4;
   fptest_data_long_double(ldn);
   fptest_check_tol_long_double("coerce64a", ldn1, -281472686096384.0);
   fptest_check_tol_long_double("coerce64b", ldn2, -1.0);
   fptest_check_tol_long_double("coerce64c", ldn3, 0.0);
   fptest_check_tol_long_double("coerce64d", ldn4, 281470681778312.0);
   lldn1 = ldn1;
   lldn2 = ldn2;
   lldn3 = ldn3;
   lldn4 = ldn4;
   fpitest_check_true("coerce64e", lldn1 == l1);
   fpitest_check_true("coerce64f", lldn2 == l2);
   fpitest_check_true("coerce64g", lldn3 == l3);
   fpitest_check_true("coerce64h", lldn4 == l4);

   fpitest_msg("done");
   return(0);

}
