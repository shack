/* 
   Fractal cloud generator
   Geoffrey Irving (revised by Justin Smith)
   Mostly copied from Paul Bourke
   $Id: cloud.c,v 1.3 2002/04/18 03:48:34 jyh Exp $
 */

#ifdef __mcc
   #include "stdarg.h"

   static void *malloc(int) = "malloc";
   extern double fmod(double x, double y);
   extern double pow(double x, double y);
   extern int atoi(char *);
#else
   #include <math.h>
   #include <sys/time.h>
#endif

#include <stdio.h>


/***  FFT code  ***/


/* compute in-place fft on 2^m points */
static void fft(int dir, int m, double *x, double *y) {

   long nn, i, i1, j, k, i2, l, l1, l2;
   double c1, c2, tx, ty, t1, t2, u1, u2, z;

   /* Calculate the number of points */
   nn = 1 << m;

   /* Do the bit reversal */
   i2 = nn >> 1;
   j = 0;
   for(i = 0; i <nn - 1; i++) {
      if (i < j) {
         tx = x[i];
         ty = y[i];
         x[i] = x[j];
         y[i] = y[j];
         x[j] = tx;
         y[j] = ty;
      }
      k = i2;
      while (k <= j) {
         j -= k;
         k >>= 1;
      }
      j += k;
   }

   /* Compute the FFT */
   c1 = -1.0;
   c2 = 0.0;
   l2 = 1;
   for(l = 0; l < m; l++) {
      l1 = l2;
      l2 <<= 1;
      u1 = 1.0;
      u2 = 0.0;
      for(j = 0; j < l1; j++) {
         for(i = j; i < nn; i += l2) {
            i1 = i + l1;
            t1 = u1 * x[i1] - u2 * y[i1];
            t2 = u1 * y[i1] + u2 * x[i1];
            x[i1] = x[i] - t1;
            y[i1] = y[i] - t2;
            x[i] += t1;
            y[i] += t2;
         }
         z =  u1 * c1 - u2 * c2;
         u2 = u1 * c2 + u2 * c1;
         u1 = z;
      }
      c2 = sqrt((1.0 - c1) / 2.0);
      if (dir) {
         c2 = -c2;
      }
      c1 = sqrt((1.0 + c1) / 2.0);
   }

   /* Scaling for forward transform */
   if (dir) {
      for (i=0;i<nn;i++) {
         x[i] /= nn;
         y[i] /= nn;
      }
   }

}


/*** TEMP FORMAT MARKER ***/

/*** two dimensional fft for 2^mi by 2^mj square ***/

static void fft2(int d, int mi, int mj, double *x, double *y) {
  int i,j;  
  int ni = 1 << mi;
  int nj = 1 << mj;
  double tx[ni], ty[ni];
  
  /* rows */
  for (i=0;i<ni;i++)
    fft(d, mj, x + nj*i, y + nj*i);

  /* columns */
  for (j=0;j<nj;j++) {
    for (i=0;i<ni;i++) {
      tx[i] = x[j + i*nj];
      ty[i] = y[j + i*nj];
      }
    fft(d, mi, tx, ty);
    for (i=0;i<ni;i++) {
      x[j + i*nj] = tx[i];
      y[j + i*nj] = ty[i];
      }
    }
  } 

/************************************** random number generator */

static int irand_i;
static float irand_s[64];

static float irand() {
  irand_i = (irand_i - 1) & 63;
//TEMP: BUG in AIR, mistyped pointer variable as `float'.
//return irand_s[irand_i] = fmod(irand_s[(irand_i+56) & 63] + irand_s[(irand_i+25) & 63], 1);
  irand_s[irand_i] = fmod(irand_s[(irand_i+56) & 63] + irand_s[(irand_i+25) & 63], 1);
  return irand_s[irand_i];
  }

static void irand_init(float x) {
  int i;
  for (i=0;i<64;i++)
    irand_s[i] = x * (63 ^ i);
  irand_i = 0;
  for (i=0;i<100;i++)
    irand();
  }

/************************************** tiff output code */


/* 
   write the start of a tiff image to f with width w and height h.
   The actual data must be written out at the resulting file position.
   The image is a 256 shade greyscale image, so each pixel is 8 bits. 
 */


/* s must be either 2 or 4 */
static void write_int(unsigned int n, size_t s) {

   int i;

   for(i = 0; i < s; ++i) {
      putchar((n >> (i << 3)) & 0xff);
   }

}


static void write_ifd(int tag, int v) {

   write_int(tag, 2);                  /* tag */
   write_int(v < 0x10000 ? 3 : 4, 2);  /* SHORT or LONG */
   write_int(1, 4);                    /* count 1 */
   write_int(v, 4);                    /* actual value */

}


static void write_ifd_offset(int tag, int t, int c, int v) {

   write_int(tag, 2);   /* tag */
   write_int(t, 2);     /* data type */
   write_int(c, 4);     /* count */
   write_int(v, 4);     /* offset */

}

static void write_tiff(int w, int h) {

   /* write header */
   putchar('I');        /* little endian */
   putchar('I');
   write_int(42, 2);    /* magic number */
   write_int(8, 4);     /* first (and only) IFD follows */
   write_int(11, 2);    /* 11 IFD entries */

   write_ifd(256, w);   /* width */ 
   write_ifd(257, h);   /* height */ 
   write_ifd(258, 8);   /* 8 bit greyscale */ 
   write_ifd(259, 1);   /* uncompressed  */ 
   write_ifd(262, 1);   /* black is zero */ 
   write_ifd(273, 162); /* strip offset = 162 */ 
   write_ifd(278, h);   /* one strip */ 
   write_ifd(279, w * h);              /* size of strip */
   write_ifd_offset(282, 5, 1, 146);   /* xres offset = 146 */ 
   write_ifd_offset(283, 5, 1, 154);   /* yres offset = 154 */ 
   write_ifd(296, 1);   /* no meaningful units */ 
   write_int(0, 4);     /* only one IFD */

   write_int(1, 4);     /* xres = 1 / 1 */
   write_int(1, 4); 
   write_int(1, 4);     /* yres = 1 / 1 */
   write_int(1, 4); 

}



/************************************** main code */

#define SH_BITS 10
#define SW_BITS 10
#define SH (1 << SH_BITS)
#define SW (1 << SW_BITS)

int main(int argc, char **argv) {
  int i,j,c;
  double r, m;
  double *x = (double*)malloc(sizeof(double)*SH*SW);
  double *y = (double*)malloc(sizeof(double)*SH*SW);

  if (argc != 2) {
    printf("Usage: %s seed > x.tiff\n", argv[0]);
    return(-1);
    }

  irand_init((sin(atoi(argv[1])) + 1) / 2);

  /* generate a random data field */
  for (i=0;i<SH;i++)
    for (j=0;j<SW;j++) {
      x[i*SH + j] = irand() - 0.5;
      y[i*SH + j] = irand() - 0.5;
      }
  /* fourier transform */
  fft2(0, SH_BITS, SW_BITS, x, y);

  /* apply 1/f^beta transformation */
  for (i=0;i<SH;i++)
    for (j=0;j<SW;j++) {
      r = (i < SH/2 ? i*i : (i-SH) * (i-SH)) + (j < SW/2 ? j*j : (j-SW) * (j-SW));
      if (r > 0) { 
        x[i*SH + j] /= r;
        y[i*SH + j] /= r;
        }
      }

  /* reverse the fft */
  fft2(0, SH_BITS, SW_BITS, (double*)x, (double*)y);

  /* compute magnitudes and maximum magnitude */
  m = 0;
  for (i=0;i<SH;i++)
    for (j=0;j<SW;j++) {  
      r = x[i*SH+j] * x[i*SH+j] + y[i*SH+j]*y[i*SH+j];
      r = pow(r, 0.4);
      if (r > m)
        m = r;
      x[i*SH+j] = r;
      }

  /* scale image and output */
  m = 256 / 4 / m;
  write_tiff(SH/2, SW/2);
  for (i=0;i<SH/2;i++)
    for (j=0;j<SW/2;j++) {
      c = (x[i*2*SH+j*2] + x[i*2*SH+j*2+1] + x[i*2*SH+SH+j*2] + x[i*2*SH+SH+j*2+1]) * m;
      putchar(c);
      }
  return 0;
  }
