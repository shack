/* Test conditionals and coercions 
   Justin David Smith */

#include <fc.h>

typedef double ty_float;

int main(int argc, char **argv) {

   /* Conditional checking */
   ty_float f0 = 131.215;
   int result1 = (f0 == 131.215);
   int result2 = (f0 != 131.215);
   print_string("equality: ");
   print_int(result1);
   print_string(", inequality: ");
   print_int(result2);
   print_string("\n");
   
   /* Test basic constant loading */
   ty_float f1 = 0.0;
   if(f1 != 0.0) print_string("fptest1 FAILED\n");
   ty_float f2 = 0.12345E6;
   if(f2 != 0.12345E6) print_string("fptest2 FAILED\n");
   ty_float f3 = -314.159;
   if(f3 != -314.159) print_string("fptest3 FAILED\n");
   print_string("f1 = ");
   print_float(f1);
   print_string(", f2 = ");
   print_float(f2);
   print_string(", f3 = ");
   print_float(f3);
   print_string("\n");
   
   /* Test coercions */
   int i4 = 123.45;
   if(i4 != 123) print_string("fptest4 FAILED\n");
   int i5 = -4567.89;
   if(i5 != -4567) print_string("fptest5 FAILED\n");
   int i6 = .2345;
   if(i6 != 0) print_string("fptest6 FAILED\n");
   ty_float f7 = 123;
   if(f7 != 123.0) print_string("fptest7 FAILED\n");
   ty_float f8 = -45678;
   if(f8 != -45678.0) print_string("fptest8 FAILED\n");
   print_string("i4 = ");
   print_int(i4);
   print_string(", i5 = ");
   print_int(i5);
   print_string(", i6 = ");
   print_int(i6);
   print_string(", f7 = ");
   print_float(f7);
   print_string(", f8 = ");
   print_float(f8);
   print_string("\n");
   
   return(0);
}
