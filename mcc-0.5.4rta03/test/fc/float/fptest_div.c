#include <fc.h>


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = 8765.4321E-10;
   float n3 = 0;
   float n4 = 1;
   float n5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[5] = { &n1, &n2, &n3, &n4, &n5 };

   double dn1 = 1234.5678E-10;
   double dn2 = 8765.4321E-10;
   double dn3 = 0;
   double dn4 = 1;
   double dn5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[5] = { &dn1, &dn2, &dn3, &dn4, &dn5 };
   
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = 8765.4321E-10;
   long double ldn3 = 0;
   long double ldn4 = 1;
   long double ldn5 = -1;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[5] = { &ldn1, &ldn2, &ldn3, &ldn4, &ldn5 };
   
   /* Register the data pointers */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);
   
   fpitest_msg("float division");
   fptest_check_tol_float("div1a", n1 / n1, 1);
   fptest_check_tol_float("div1b", n1 / n2, 0.140845058853402L);
   fptest_check_tol_float("div1c", n1 / n4, 1234.5678E-10L);
   fptest_check_tol_float("div1d", n1 / n5, -1234.5678E-10L);
   fptest_check_tol_float("div1e", n2 / n1, 7.100000583200049L);
   fptest_check_tol_float("div1f", n2 / n2, 1);
   fptest_check_tol_float("div1g", n2 / n4, 8765.4321E-10L);
   fptest_check_tol_float("div1h", n2 / n5, -8765.4321E-10L);
   fptest_check_tol_float("div1i", n3 / n1, 0);
   fptest_check_tol_float("div1j", n3 / n2, 0);
   fptest_check_tol_float("div1k", n3 / n4, 0);
   fptest_check_tol_float("div1l", n3 / n5, 0);
   fptest_check_tol_float("div1m", n4 / n1, 8.100000664200055E+6L);
   fptest_check_tol_float("div1n", n4 / n2, 1.140845070261853E+6L);
   fptest_check_tol_float("div1o", n4 / n4, 1);
   fptest_check_tol_float("div1p", n4 / n5, -1);
   fptest_check_tol_float("div1q", n5 / n1, -8.100000664200055E+6L);
   fptest_check_tol_float("div1r", n5 / n2, -1.140845070261853E+6L);
   fptest_check_tol_float("div1s", n5 / n4, -1);
   fptest_check_tol_float("div1t", n5 / n5, 1);

   fpitest_msg("double division");
   fptest_check_tol_double("div2a", dn1 / dn1, 1);
   fptest_check_tol_double("div2b", dn1 / dn2, 0.140845058853402L);
   fptest_check_tol_double("div2c", dn1 / dn4, 1234.5678E-10L);
   fptest_check_tol_double("div2d", dn1 / dn5, -1234.5678E-10L);
   fptest_check_tol_double("div2e", dn2 / dn1, 7.100000583200049L);
   fptest_check_tol_double("div2f", dn2 / dn2, 1);
   fptest_check_tol_double("div2g", dn2 / dn4, 8765.4321E-10L);
   fptest_check_tol_double("div2h", dn2 / dn5, -8765.4321E-10L);
   fptest_check_tol_double("div2i", dn3 / dn1, 0);
   fptest_check_tol_double("div2j", dn3 / dn2, 0);
   fptest_check_tol_double("div2k", dn3 / dn4, 0);
   fptest_check_tol_double("div2l", dn3 / dn5, 0);
   fptest_check_tol_double("div2m", dn4 / dn1, 8.100000664200055E+6L);
   fptest_check_tol_double("div2n", dn4 / dn2, 1.140845070261853E+6L);
   fptest_check_tol_double("div2o", dn4 / dn4, 1);
   fptest_check_tol_double("div2p", dn4 / dn5, -1);
   fptest_check_tol_double("div2q", dn5 / dn1, -8.100000664200055E+6L);
   fptest_check_tol_double("div2r", dn5 / dn2, -1.140845070261853E+6L);
   fptest_check_tol_double("div2s", dn5 / dn4, -1);
   fptest_check_tol_double("div2t", dn5 / dn5, 1);

   fpitest_msg("long double division");
   fptest_check_tol_long_double("div3a", ldn1 / ldn1, 1);
   fptest_check_tol_long_double("div3b", ldn1 / ldn2, 0.140845058853402L);
   fptest_check_tol_long_double("div3c", ldn1 / ldn4, 1234.5678E-10L);
   fptest_check_tol_long_double("div3d", ldn1 / ldn5, -1234.5678E-10L);
   fptest_check_tol_long_double("div3e", ldn2 / ldn1, 7.100000583200049L);
   fptest_check_tol_long_double("div3f", ldn2 / ldn2, 1);
   fptest_check_tol_long_double("div3g", ldn2 / ldn4, 8765.4321E-10L);
   fptest_check_tol_long_double("div3h", ldn2 / ldn5, -8765.4321E-10L);
   fptest_check_tol_long_double("div3i", ldn3 / ldn1, 0);
   fptest_check_tol_long_double("div3j", ldn3 / ldn2, 0);
   fptest_check_tol_long_double("div3k", ldn3 / ldn4, 0);
   fptest_check_tol_long_double("div3l", ldn3 / ldn5, 0);
   fptest_check_tol_long_double("div3m", ldn4 / ldn1, 8.100000664200055E+6L);
   fptest_check_tol_long_double("div3n", ldn4 / ldn2, 1.140845070261853E+6L);
   fptest_check_tol_long_double("div3o", ldn4 / ldn4, 1);
   fptest_check_tol_long_double("div3p", ldn4 / ldn5, -1);
   fptest_check_tol_long_double("div3q", ldn5 / ldn1, -8.100000664200055E+6L);
   fptest_check_tol_long_double("div3r", ldn5 / ldn2, -1.140845070261853E+6L);
   fptest_check_tol_long_double("div3s", ldn5 / ldn4, -1);
   fptest_check_tol_long_double("div3t", ldn5 / ldn5, 1);
   
   fpitest_msg("done");
   return(0);
   
}
