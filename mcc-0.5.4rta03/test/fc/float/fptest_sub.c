#include <fc.h>


int main(int argc, char **argv) {

   float n1 = 1234.5678E-10;
   float n2 = 8765.4321E-10;
   float n3 = 0;
   /* By escaping these, we prevent optimizations from constant folding */
   float *n[3] = { &n1, &n2, &n3 };
   
   double dn1 = 1234.5678E-10;
   double dn2 = 8765.4321E-10;
   double dn3 = 0;
   /* By escaping these, we prevent optimizations from constant folding */
   double *dn[3] = { &dn1, &dn2, &dn3 };
   
   long double ldn1 = 1234.5678E-10;
   long double ldn2 = 8765.4321E-10;
   long double ldn3 = 0;
   /* By escaping these, we prevent optimizations from constant folding */
   long double *ldn[3] = { &ldn1, &ldn2, &ldn3 };
   
   float neg_n1 = -n1;
   float neg_n2 = -n2;
   float neg_n3 = -n3;
   double neg_dn1 = -dn1;
   double neg_dn2 = -dn2;
   double neg_dn3 = -dn3;
   long double neg_ldn1 = -ldn1;
   long double neg_ldn2 = -ldn2;
   long double neg_ldn3 = -ldn3;
   
   /* Register the data values. */
   fptest_data_float(n);
   fptest_data_double(dn);
   fptest_data_long_double(ldn);
   
   fpitest_msg("float subtraction");
   fptest_check_tol_float("sub1a", n1 - n2, -7530.8643E-10);
   fptest_check_tol_float("sub1b", n2 - n1, 7530.8643E-10);
   fptest_check_tol_float("sub1c", n1 - n3, 1234.5678E-10);
   fptest_check_tol_float("sub1d", n2 - n3, 8765.4321E-10);
   fptest_check_tol_float("sub1e", n1 - n1, 0);
   fptest_check_tol_float("sub1f", n2 - n2, 0);
   fptest_check_tol_float("sub1g", n3 - n3, 0);
   fptest_check_tol_float("sub1h", n1 - n2 - n3, -7530.8643E-10);
   fptest_check_tol_float("sub1i", n3 - n2 - n1, -9999.9999E-10);
   fptest_check_tol_float("sub1j", n1 - neg_n1, 2469.1356E-10);
   fptest_check_tol_float("sub1k", n2 - neg_n2, 17530.8642E-10);
   fptest_check_tol_float("sub1l", n3 - neg_n3, 0);
   fptest_check_tol_float("sub1m", n1 - n2, -7530.8643E-10);
   fptest_check_tol_float("sub1n", n2 - n1, 7530.8643E-10);
   fptest_check_tol_float("sub1o", n1 - neg_n2, 9999.9999E-10);
   fptest_check_tol_float("sub1p", n2 - neg_n1, 9999.9999E-10);
   fptest_check_tol_float("sub1q", n3 - neg_n1, 1234.5678E-10);
   fptest_check_tol_float("sub1r", n3 - neg_n2, 8765.4321E-10);
   fptest_check_tol_float("sub1s", n3 - neg_n3 - n3 - neg_n3 - n3, 0);

   fpitest_msg("double subtraction");
   fptest_check_tol_double("sub2a", dn1 - dn2, -7530.8643E-10);
   fptest_check_tol_double("sub2b", dn2 - dn1, 7530.8643E-10);
   fptest_check_tol_double("sub2c", dn1 - dn3, 1234.5678E-10);
   fptest_check_tol_double("sub2d", dn2 - dn3, 8765.4321E-10);
   fptest_check_tol_double("sub2e", dn1 - dn1, 0);
   fptest_check_tol_double("sub2f", dn2 - dn2, 0);
   fptest_check_tol_double("sub2g", dn3 - dn3, 0);
   fptest_check_tol_double("sub2h", dn1 - dn2 - dn3, -7530.8643E-10);
   fptest_check_tol_double("sub2i", dn3 - dn2 - dn1, -9999.9999E-10);
   fptest_check_tol_double("sub2j", dn1 - neg_dn1, 2469.1356E-10);
   fptest_check_tol_double("sub2k", dn2 - neg_dn2, 17530.8642E-10);
   fptest_check_tol_double("sub2l", dn3 - neg_dn3, 0);
   fptest_check_tol_double("sub2m", dn1 - dn2, -7530.8643E-10);
   fptest_check_tol_double("sub2n", dn2 - dn1, 7530.8643E-10);
   fptest_check_tol_double("sub2o", dn1 - neg_dn2, 9999.9999E-10);
   fptest_check_tol_double("sub2p", dn2 - neg_dn1, 9999.9999E-10);
   fptest_check_tol_double("sub2q", dn3 - neg_dn1, 1234.5678E-10);
   fptest_check_tol_double("sub2r", dn3 - neg_dn2, 8765.4321E-10);
   fptest_check_tol_double("sub2s", dn3 - neg_dn3 - dn3 - neg_dn3 - dn3, 0);

   fpitest_msg("long double subtraction");
   fptest_check_tol_long_double("sub3a", ldn1 - ldn2, -7530.8643E-10);
   fptest_check_tol_long_double("sub3b", ldn2 - ldn1, 7530.8643E-10);
   fptest_check_tol_long_double("sub3c", ldn1 - ldn3, 1234.5678E-10);
   fptest_check_tol_long_double("sub3d", ldn2 - ldn3, 8765.4321E-10);
   fptest_check_tol_long_double("sub3e", ldn1 - ldn1, 0);
   fptest_check_tol_long_double("sub3f", ldn2 - ldn2, 0);
   fptest_check_tol_long_double("sub3g", ldn3 - ldn3, 0);
   fptest_check_tol_long_double("sub3h", ldn1 - ldn2 - ldn3, -7530.8643E-10);
   fptest_check_tol_long_double("sub3i", ldn3 - ldn2 - ldn1, -9999.9999E-10);
   fptest_check_tol_long_double("sub3j", ldn1 - neg_ldn1, 2469.1356E-10);
   fptest_check_tol_long_double("sub3k", ldn2 - neg_ldn2, 17530.8642E-10);
   fptest_check_tol_long_double("sub3l", ldn3 - neg_ldn3, 0);
   fptest_check_tol_long_double("sub3m", ldn1 - ldn2, -7530.8643E-10);
   fptest_check_tol_long_double("sub3n", ldn2 - ldn1, 7530.8643E-10);
   fptest_check_tol_long_double("sub3o", ldn1 - neg_ldn2, 9999.9999E-10);
   fptest_check_tol_long_double("sub3p", ldn2 - neg_ldn1, 9999.9999E-10);
   fptest_check_tol_long_double("sub3q", ldn3 - neg_ldn1, 1234.5678E-10);
   fptest_check_tol_long_double("sub3r", ldn3 - neg_ldn2, 8765.4321E-10);
   fptest_check_tol_long_double("sub3s", ldn3 - neg_ldn3 - ldn3 - neg_ldn3 - ldn3, 0);
   
   fpitest_msg("done");
   return(0);
   
}
