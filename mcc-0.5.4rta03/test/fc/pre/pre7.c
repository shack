/*
 * This is an integer matrix multiplication test.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <fc.h>

/*
 * Size of the matrix.
 */
#define SIZE    150

/*
 * Square matrix definition.
 */
typedef int elem;

typedef elem matrix[SIZE][SIZE];

/*
 * Main function creates matrices with random elements,
 * then multiplies them.
 */
int main(int argc, char **argv)
{
    matrix m1;
    int i, j;

    /* Initialize random number generator */
    // srandom(0xf00babe);

    /* Initialize matrices */
    print_string("This program should exit with code ");
    print_int((SIZE - 1) * (SIZE - 1) * 7);
    print_string("\n");
    for(i = 0; i != SIZE; i++) {
        for(j = 0; j != SIZE; j++)
            m1[i][j] = i * j * 7;
    }
    return m1[SIZE - 1][SIZE - 1];
}
