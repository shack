/*
 * This is an integer matrix multiplication test.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>

void srandom(unsigned seed);
int random();

/*
 * Size of the matrix.
 */
#define SIZE    150

/*
 * Square matrix definition.
 */
typedef int elem;

typedef elem matrix[SIZE][SIZE];

/*
 * Multiplier.
 */
static void mmult(matrix result, matrix m1, matrix m2)
{
    int i, j, k;
    elem sum;

    for(i = 0; i != SIZE; i++) {
        for(j = 0; j != SIZE; j++) {
            sum = 0;
            for(k = 0; k != SIZE; k++)
                sum += m1[i][k] * m2[k][j];
            result[i][j] = sum;
        }
    }
}

/*
 * Main function creates matrices with random elements,
 * then multiplies them.
 */
int main(int argc, char **argv)
{
    matrix m1, m2, m3;
    int i, j;

    /* Initialize random number generator */
    srandom(0xf00babe);

    /* Initialize matrices */
    for(i = 0; i != SIZE; i++) {
        for(j = 0; j != SIZE; j++) {
            m1[i][j] = i + j;
            m2[i][j] = i + j;
        }
    }

    /* Multiply them */
    mmult(m3, m1, m2);
    mmult(m1, m2, m3);
    mmult(m2, m3, m1);
    return 0;
}
