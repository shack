/*
 * This is an integer matrix multiplication test.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <fc.h>
#include <stdio.h>

void srandom(unsigned seed);
int random();

/*
 * Size of the matrix.
 */
#define SIZE    150

/*
 * Square matrix definition.
 */
typedef int elem;

typedef elem vector[SIZE];

/*
 * Multiplier.
 */
static void prod(vector result, vector m1, vector m2)
{
    int i;

    for(i = 0; i != SIZE; i++)
        result[i] = m1[i] * m2[i];
}

/*
 * Main function creates matrices with random elements,
 * then multiplies them.
 */
int main(int argc, char **argv)
{
    vector m1, m2, m3;
    int a;

#if 0
    /* Initialize random number generator */
    // srandom(0xf00babe);

    /* Initialize matrices */
    for(i = 0; i != SIZE; i++) {
        m1[i] = i;
        m2[i] = i;
    }
#endif

    /* Multiply them */
    for(a = 0; a != 100; a++)
        prod(m3, m1, m2);
    return 0;
}
