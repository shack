/* Verify that character alignment is correct on external calls.
   Justin David Smith.  */


#include <fc.h>
#include <stdio.h>


int main(int argc, char **argv) {

   /* Leave the explicit coercions; they are significant. */
   char *data[4] = { "hello", " brave", " new", " world" };
   printf("%c%c%c%c%c\n", (char)'h', 'e', 'l', 'l', 'o');
   printf("%s%s%s%s\n", (char *)data[0], data[1], data[2], data[3]);
   return(0);

}
