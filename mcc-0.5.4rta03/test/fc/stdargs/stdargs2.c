/*
 * Basic printf test.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include "stdarg.h"
#include "fc.h"

static void printf(char *fmtp, ...)
{
    va_list ap;
    char c;

    va_start(ap, fmtp);
    while(*fmtp) {
        c = *fmtp++;
        if(c == '%') {
            switch(*fmtp++) {
            case 's':
                print_string(va_arg(ap, char *));
                break;
            case 'd':
                print_int(va_arg(ap, int));
                break;
            case 'c':
                print_char(va_arg(ap, int));
                break;
            }
        }
        else
            print_char(c);
    }
    va_end(ap);
}

int main(int argc, char **argv)
{
    printf("Hello world: %d, then \"%s\", then '%c'\n", 0x77777777, "A string", 'z');
    return 0;
}
