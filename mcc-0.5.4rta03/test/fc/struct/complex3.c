#include "fc.h"

struct complex {
    double x;
    double y;
};

static struct complex incr_complex(struct complex p)
{
    p.x++;
    p.y++;
    return p;
}

static void print_complex(struct complex *p)
{
    print_string("complex: (");
    print_float(p->x);
    print_string(", ");
    print_float(p->y);
    print_string(")\n");
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    struct complex zero;
    zero.x = 1;
    zero.y = 2;
    incr_complex(zero);
    print_complex(&zero);
    zero = incr_complex(zero);
    print_complex(&zero);

    print_string("End of program, should see (1, 2) followed by (2, 3)\n");
    return 0;
}
