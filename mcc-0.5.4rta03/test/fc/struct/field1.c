#include "fc.h"

struct data {
    int x : 10;
    int y : 12;
};

static struct data incr_data(struct data p)
{
    p.x++;
    p.y++;
    return p;
}

static void print_data(struct data *p)
{
    print_string("data: (");
    print_int(p->x);
    print_string(", ");
    print_int(p->y);
    print_string(")\n");
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    struct data zero;
    zero.x = 1;
    zero.y = 2;
    incr_data(zero);
    print_data(&zero);
    zero = incr_data(zero);
    print_data(&zero);

    print_string("End of program, should see (1, 2) followed by (2, 3)\n");
    return 0;
}
