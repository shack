#include "fc.h"

union data {
    int x;
    float y;
};

static union data incr_data(union data p)
{
    p.x++;
    p.y++;
    return p;
}

static void print_data(union data *p)
{
    print_string("data: (");
    print_hex(p->x);
    print_string(", ");
    print_float(p->y);
    print_string(")\n");
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    union data zero;
    zero.x = 1;
    zero.y = 2;
    incr_data(zero);
    print_data(&zero);
    zero = incr_data(zero);
    print_data(&zero);

    return 0;
}
