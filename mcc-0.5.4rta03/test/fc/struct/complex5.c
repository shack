#include "fc.h"

struct complex {
    int x;
    int y;
};

static struct complex incr_complex(struct complex p)
{
    p.x++;
    p.y++;
    return p;
}

static void print_complex(struct complex *p)
{
    print_string("complex: (");
    print_int(p->x);
    print_string(", ");
    print_int(p->y);
    print_string(")\n");
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    struct complex zero;
    zero.x = 1;
    zero.y = 2;
    zero = incr_complex(zero);
    return 0;
}
