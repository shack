#include "fc.h"

struct complex {
    double x;
    double y;
};

static void print_complex(struct complex *p)
{
    print_string("complex: (");
    print_float(p->x);
    print_string(", ");
    print_float(p->y);
    print_string(")\n");
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    struct complex zero;
    zero.x = 1;
    zero.y = 2;
    print_complex(&zero);

    print_string("End of program, should see (1, 2)\n");
    return 0;
}
