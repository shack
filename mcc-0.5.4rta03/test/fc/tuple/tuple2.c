#include "fc.h"

typedef tuple complex {
    double;
    double;
} Complex;

static void print_complex(Complex p)
{
    switch(p) {
    case { x, y }:
        print_string("complex: (");
        print_float(x);
        print_string(", ");
        print_float(y);
        print_string(")\n");
        break;
    }
}

int main(int argc, char **argv)
{
    tuple complex y = { 1.0, 2.0 };
    print_complex(y);
    return 0;
}
