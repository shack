#include "fc.h"

typedef tuple complex {
    double;
    double;
} Complex;

static void print_complex(Complex p)
{
    tuple { double; double; } f = p;
    tuple { double; double; } { x, y } = p;
    print_string("complex: (");
    print_float(x);
    print_string(", ");
    print_float(y);
    print_string(")\n");
}

int main(int argc, char **argv)
{
    Complex { x, y } = { 1.0, 2.0 };
    Complex y = { y, x };
    print_complex(y);
    return 0;
}
