/* Segfault on dereference
   Justin David Smith */

#include "fc.h"

int main(int argc, char **argv) {

   int a[10];
   print_string("This case should segfault with a bounds fault\n");
   print_int(a[12]);
   print_string("BOGUS, no fault\n");
   return(0);

}
