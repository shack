/* Segfault on assignment - large data type
   Justin David Smith */

#include "fc.h"

int main(int argc, char **argv) {

   double a[10];
   print_string("This case should segfault with a bounds fault\n");
   a[12] = 0;
   print_string("BOGUS, no fault\n");
   return(0);

}
