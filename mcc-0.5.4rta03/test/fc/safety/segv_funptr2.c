/* Segfault on function/block pointer error
   Justin David Smith */


#include "fc.h"


int foo(int x) {

   return(x);
   
}


typedef union _un {
   int (*funp)(int);
   int *intp;
} un;


int main(int argc, char **argv) {

   un U;
   U.intp = &argc;
   print_string("This case should segfault with a type fault\n");
   int (*bar)(int) = U.funp;
   print_string("BOGUS, no fault\n");
   return(bar(32));

}
