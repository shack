/* Segfault on null dereference
   Justin David Smith */

#include "fc.h"

int main(int argc, char **argv) {

   int *x = (int *)malloc(0);
   print_string("This case should segfault with a bounds fault\n");
   int y = *x;
   print_string("BOGUS, no fault\n");
   print_int(y);
   return(0);

}
