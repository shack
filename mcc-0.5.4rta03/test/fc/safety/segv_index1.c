/* Segfault on bogus memory pointer
   Justin David Smith */

#include "fc.h"

typedef union _foo {
   int *good;
   unsigned long evil;
} foo;

int main(int argc, char **argv) {

   foo a[10];
   int *i;
   print_string("This case should segfault with an index fault\n");
   a[2].evil = 0;
   i = a[2].good;
   print_string("BOGUS, no fault\n");  /* Fault will occur on read */
   print_int(*i);
   return(0);

}
