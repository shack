#include <fc.h>

void set_string(char s[20], int i)
{
    char temp = s[i + 1];
    s[i + 1] = s[i];
    s[i] = temp;
}

int main(int argc, char **argv)
{
    char s[10];
    s[0] = 'H';
    s[1] = 'e';
    s[2] = 'l';
    s[3] = 'l';
    s[4] = 'o';
    s[5] = 0;
    set_string(s, 0);
    set_string(s, 3);
    print_string(s);
    print_string("\n");
    return 0;
}
