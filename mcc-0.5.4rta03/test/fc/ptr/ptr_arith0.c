/*
 * Pointer-arithmetic test.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *
 */

#include "fc.h"

int main(int argc, char **argv) {
    int a[3] = {1, 2, 3};
    int* b = a;

    print_int(*++b);
    print_int(*b);

    b = a;

    // BUG:
    // Right now, post-order operations behave like
    // pre-order ones.
    print_int(*b++);
    print_int(*b);

    print_string("\nEnd of program (should see 2212).\n");
    return 0;
}
