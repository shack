/*
 * Pointer-arithmetic test.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * granicz@cs.caltech.edu
 *
 */

#include "fc.h"

// BUG: post-order operators don't work yet.
void scopy(char* src, char* dst, int length) {
    for (; length > 0; length--) {
        *dst = *src;
        dst++;
        src++;
    }
}

int main(int argc, char **argv) {
    char* s = "Strings work...";
    char s2[16];

    scopy(s, s2, 15);
    print_string(s2);
    print_string("\nEnd of program (should see \"Strings work...\").\n");
    return 0;
}
