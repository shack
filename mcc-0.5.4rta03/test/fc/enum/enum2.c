/*
 * Basic enumeration.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include "fc.h"

enum e1 { A = 5 };
enum e2 { B, C = 3, D = 2, E, F = 1 };

int main(int argc, char **argv)
{
    int i;
    
    // Check usage
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <i>\n");
        return 1;
    }

    // Get the number
    i = atoi(argv[1]);

    // Switch
    switch(i) {
    case A:
        print_string("A: ");
        print_int(A);
        print_string("\n");
        break;

    case B:
        print_string("B\n");
        break;

    case C:
        print_string("C\n");
        break;

    case D:
        print_string("D\n");
        break;

    case E:
        print_string("E\n");
        break;

    case F:
        print_string("F\n");
        break;

    case v:
        print_string("Sorry, you typed: ");
        print_int(v);
        print_string("\n");
        break;
    }

    return 0;
}
