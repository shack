/*
 * Basic enumeration.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include "fc.h"

enum e1 { A };
enum e2 { B, C = 0x7fffffff, D = 0xffffffff, E, F = 1 };

int main(int argc, char **argv)
{
    print_string("Enum: A=");
    print_int(A);
    print_string("\nEnum: B=");
    print_int(B);
    print_string("\nEnum: C=");
    print_int(C);
    print_string("\nEnum: D=");
    print_int(D);
    print_string("\nEnum: E=");
    print_int(E);
    print_string("\nEnum: F=");
    print_int(F);
    print_string("\n");
    return 0;
}
