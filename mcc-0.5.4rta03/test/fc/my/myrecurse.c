/* Simple Mutual Recursion Test 
   Justin David Smith */

/* This file is a test for the DSYM_BUG. */
/* This file is a test for the EXTCALL_BUG. */
/* This file triggers the DSYM_FUNCALL_ORDER_BUG. */
/* This file triggers the DSYM_EXTCALL_BUG. */
   

#include "fc.h"


/* Forward definition for b */
int b(int);


int a(int x) {
   if(x == 0) return 1;
   else return a(x - 1) + b(x - 1);
}


int b(int y) {
   if(y == 0) return 2;
   else return a(y - 1) + b(y - 1);
}


int main(int argc, char **argv) {
   print_string("Hello!\n");
   print_int(a(5));
   print_string("; ");
   print_int(b(5));
   print_string("\n");
   print_string("succ\n");
}
