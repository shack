/* Justin David Smith
   Testing ExtCalls */


#include "fc.h"


int main(int argc, char **argv) {
   int i = atoi("32");
   print_string("Hallo world!\n");
   print_int(i);
   print_string("\n");
   
   print_string("\nProgram arguments:\n");
   for(i = 0; i < argc; ++i) {
      print_string(": ");
      print_string(argv[i]);
      print_string("\n");
   }
   
   print_string("succ\n");
}
