/* Test Case for Sequential Typing Bug
   Justin David Smith */

/* NOTE: this test case triggered the SeqExpr bug. */

#include "fc.h"


char *foo() {
   char ch[10];
   char *p = ch;
   {
      { p++; }
      1;
   }
   p++;
}


int main(int argc, char **argv) {
   print_string("compile check only.\nsucc\n");
}


