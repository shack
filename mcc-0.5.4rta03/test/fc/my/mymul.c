/* Justin David Smith
   Strength Reduction Evaluation Test - Multiplication */
   

#include "fc.h"


int main(int argc, char **argv) {

   /* Data */
   int j = 37;
   int k = -5;

   print_int(k); print_string("\n");
   
   if(-4 * k != 20)  print_string("mul1: FAIL\n");

   print_int(-4 * k);    print_string(", "); print_int(k); print_string("\n");
   print_int(k << 2);    print_string(", "); print_int(k); print_string("\n");
   print_int(-(k << 2)); print_string(", "); print_int(k); print_string("\n");
 
   print_int(j); print_string("\n");
   
   if(j * -4 != -148)  print_string("mul2: FAIL\n"); 
      
   print_int(j * -4);    print_string(", "); print_int(j); print_string("\n");
   print_int(j << 2);    print_string(", "); print_int(j); print_string("\n");
   print_int(-(j << 2)); print_string(", "); print_int(j); print_string("\n");
 
   /* That's it for now. */
   print_string("succ\n");

}
