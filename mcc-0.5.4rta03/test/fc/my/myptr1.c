/* Pointer Stress Test - version 2
   Justin David Smith */

   
/* While this case passes the type checker, we get a type mismatch
   during evaluation at the following IR code.  This file is an
   attempt to simplify the code and identify the precise error.  
   The failure is introduced at stage 4.  */

/* *** Error Location:
      *p_289 : char* <- make_binop_337;
      let make_scalar_338 : int = 0 in
      let subscript_339 : char* = p_289[make_scalar_338] in
      let c_340 : char = main_frame_284->c_193 in
      *subscript_339 : char <- c_340; 
      ...

   /eval_expr
   /eval_set_subscript_expr:
   *** Error: evaluation error: set_value: type mismatch  */

#include "fc.h"
               

int main(int argc, char **argv) {

   char a[10];
   char *b;
   char **p;

   /* Stress test pointers in assignment */
   p = &b;
   *p = a + 1;

   /* Return value must agree w/ int */
   print_string("Guess we didn't crash ...\nsucc\n");

}
