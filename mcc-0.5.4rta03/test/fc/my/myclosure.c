/* Justin David Smith
   Closure Testing  */

/* This was the benchmark case in resolving the GLOBVAR_BUG problem. */

#include "fc.h"

/* Define a global var */
int x;

/* FV(g) = x */
int g() { 
   print_string("Not dead yet...\nsucc\n"); 
   return x; 
}

/* FV(f1) = x */
int f1(int (*g)()) {
   x++;  
   return g();
}

/* FV(f2) = f1, g, x */
int f2() {
   x = 0;
   return f1(g);
}

/* FV(main) = f1, f2, g, x */
int main(int argc, char **argv) {
   return f2();
}
