/* Justin David Smith
   Test Conditional Inlining */
   

#include "fc.h"
   
   
int main(int argc, char **argv) {

   /* Basic conditional elimination checks */
   if(1 == 2)     print_string("FAIL\n"); else print_string("succ\n");
   if(3 > 2)      print_string("succ\n"); else print_string("FAIL\n");
   if(3 <= 4)     print_string("succ\n"); else print_string("FAIL\n");
   if(4 != 4)     print_string("FAIL\n"); else print_string("succ\n");

   /* Test inlining for var,== */
   int x = 3;
   if(x == 4) {
      print_int(x);
   } else {
      print_int(x);
   }
   print_string("\n");
   if(4 == x) {
      print_int(x);
   } else {
      print_int(x);
   }
   print_string("\n");
   
   /* Test inlining for var,!= */
   if(x != 4) {
      print_int(x);
   } else {
      print_int(x);
   }
   print_string("\n");
   if(4 != x) {
      print_int(x);
   } else {
      print_int(x);
   }
   print_string("\n");
   
   print_string("succ\n");
   
}
