/* Justin David Smith
   Test cases for shifting */
   

#include "fc.h"
   
   
int main(int argc, char **argv) {

   int i = 3;
   int j = 4;
   int k = 5;
   
   /* Shifting with constant base */
   if(i << 1 != 6)            print_string("vc1: FAIL\n");
   if(j << 2 != 16)           print_string("vc2: FAIL\n");
   if(k << 3 != 40)           print_string("vc3: FAIL\n");
   if(k >> 1 != 2)            print_string("vc4: FAIL\n");
   if(k >> 2 != 1)            print_string("vc5: FAIL\n");
   
   /* Shifting with variable base */
   if(1 << i != 8)            print_string("cv1: FAIL\n");
   if(2 << i != 16)           print_string("cv2: FAIL\n");
   if(3 << j != 48)           print_string("cv3: FAIL\n");
   if(0xf0 >> j != 0x0f)      print_string("cv4: FAIL\n");
   if(0xf9 >> i != 0x1f)      print_string("cv5: FAIL\n");
   if(0xffff >> k != 0x07ff)  print_string("cv6: FAIL\n");

   /* Var/var shifts */
   if(i << j != 48)           print_string("vv1: FAIL\n");
   if(j << i != 32)           print_string("vv2: FAIL\n");
   if(j << k != 128)          print_string("vv3: FAIL\n");
   if(k >> i != 0)            print_string("vv4: FAIL\n");
   
   /* Done */
   print_string("succ\n");

}
