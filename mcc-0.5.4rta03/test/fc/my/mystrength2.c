/* Justin David Smith
   Strength Reduction Evaluation Tests II */
   

#include "fc.h"


int main(int argc, char **argv) {

   /* Data */
   int i = 0;
   int j = 37;
   int k = -5;

   /* Verify multiplication */
   if(3 * -2 != -6 || 17 * i != 0)     print_string("mul1: FAIL\n");
   if(j * 0 != 0 || 0 * k != 0)        print_string("mul2: FAIL\n");
   if(j * 1 != 37 || 1 * k != -5)      print_string("mul3: FAIL\n");
   if(j * -1 != -37 || -1 * k != 5)    print_string("mul4: FAIL\n");
   if(j * 2 != 74 || 2 * k != -10)     print_string("mul5: FAIL\n");
   if(j * 3 != 111 || 3 * k != -15)    print_string("mul6: FAIL\n");
   if(j * 4 != 148 || 4 * k != -20)    print_string("mul7: FAIL\n");
   if(j * 5 != 185 || 5 * k != -25)    print_string("mul8: FAIL\n");
   if(j * -2 != -74 || -2 * k != 10)   print_string("mul9: FAIL\n");
   if(j * -4 != -148 || -4 * k != 20)  print_string("mul10: FAIL\n");
   
   /* Verify division */
   if(32 / 3 != 10 || j / 32 != 1)     print_string("div1: FAIL\n");
   if(0 / j != 0 || 0 / k != 0)        print_string("div2: FAIL\n");
   if(j / 2 != 18 || k / 2 != -2) {
      print_string("div3: FAIL: ");
      print_int(j / 2);
      print_string(" (18), ");
      print_int(k / 2);
      print_string(" [0x");
      print_hex(k / 2);
      print_string("] (-2)\n");
   }
   if(j / 4 != 9 || k / 4 != -1)       print_string("div4: FAIL\n");
   if(j / -4 != -9 || k / -4 != 1)     print_string("div5: FAIL\n");
   
   /* That's it for now. */
   print_string("succ\n");

}