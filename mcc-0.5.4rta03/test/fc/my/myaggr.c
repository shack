/* Justin David Smith
   Aggregate manipulations */
   
/* This tests some basic aggregate operations. */

#include "fc.h"

struct item { int x, y; };

int foo(struct item i) {
   struct item j = i;
   struct item *pi, *pj;
   pi = &i;
   pj = pi;
   j  = i;
   
   if(pj->x == j.x && pj->y == j.y) print_string("succ\n");
   else                             print_string("FAIL\n");
}

int main(int argc, char **argv) {
   struct item i;
   i.x = 3;
   i.y = i.x * 2;
   foo(i);
}
