/* Function/sequential test case
   Justin David Smith */
   
/* This file is part of the SeqExpr bugfix abuse series. */

#include "fc.h"


int foo() {
   /* The explicit return seems to be causing problems */
   return 3;
}


int main(int argc, char **argv) {
   print_string("succ\n");
}
