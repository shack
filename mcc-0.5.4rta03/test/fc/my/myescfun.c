/* Justin David Smith
   Escaping functions */

/* ESCFUN
   This case triggered the ``escaping toplevel function'' bug.  Curiously,
   this bug is _not_ triggered for nested functions, it seems bar_inner is
   not considered an escaping variable, although bar_outer is.  */

/* Warning: this case has not really been resolved.  The system will
   still try to access escaping functions via the frames!! */

int print_string(char *);


/* A simple functional type */
typedef int (*fun)();


/* Case I: this seems to trigger the bug */
int bar_outer() { 0; }

fun foo_outer() {
   return bar_outer;
}


/* Case II: this does not trigger the bug */
fun foo_inner() {
   int bar_inner() { 0; }
   return bar_inner;
}


/* Case III: an escaping function in a nested function, ouch! */
fun foo_esc_inner() {
   int bar_esc_inner() { 0; }
   fun foo_esc_inner2() {
      return bar_esc_inner;
   }
   return foo_esc_inner2();
}


/* Main function */
int main(int argc, char **argv) {
   foo_outer();
   foo_inner();
   print_string("Guess we didn't crash...\nsucc\n");
}
