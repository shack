/* Type Coercions
   Justin David Smith */
   
   
/* This case has been updated to remove floating-point operations */

#include "fc.h"


/* Return from function coercions */
int foo() {
   return 301;
}


char bar() {
   return 1;
   return 'c';
   return 0300;
}


int victim(int a, int b) {
}


/* This is evil; we return a function but it must be coerced
   to a function POINTER or closure!  Read carefully!  */
typedef int (*baz_type)(int, int);
baz_type baz(int a, int b) {
   return victim;
}
   
   
int main(int argc, char **argv) {
   
   int a = 3;
   char b = 2;
   char c;

   /* Binary operation coercion */   
   a = a + b;
   b = b % a;
   c = a / b;
   a = a * c;
   c = c - c;
   
   /* Unary operation coercion */
   ++a;
   ++b;
   ++c;

   /* See comments in fc_ir_ast.ml, make_uarith_expr
      for the test case we are trying to capture here. */   
   char *p = &c;
   ++(*p);
   --(*p);
   
   /* Bitwise coercions */
   b = b ^ c;
   a = a | b;
   
   /* Pointer coercions */
   a = 5;
   b = 6;
   char A[10];
   char *x = A;
   x[0] = 3;
   *x = 3;
   x = x + a;
   x = x - a;
   x -= a;
   x += b;
   b = b * *x;
   
   /* Strings working? */
   char *ch;
   ch = "abc";
   ch = "def";
   ch = "";
   
   /* Oooo. Function coercions... :) */   
   int (*mainfun)(int, char **) = main;
   
   print_string("Looks like we didn't crash\nsucc\n");
   
}
