/* Justin David Smith
   Strength Reduction Evaluation Tests I */
   

#include "fc.h"


int main(int argc, char **argv) {

   /* Data */
   int i = 0;
   int j = 37;
   int k = -5;

   /* Verify basic unary operations */
   if(-i != 0)    print_string("un1: FAIL\n");
   if(-j != -37)  print_string("un2: FAIL\n");
   if(-k != 5)    print_string("un3: FAIL\n");
   
   /* Verify addition */
   if(i + 0 != 0 || 0 + j != 37)       print_string("plus1: FAIL\n");
   if(j + 1 != 38 || k + -1 != -6)     print_string("plus2: FAIL\n");
   if(i + j != 37 || k + (-j) != -42)  print_string("plus3: FAIL\n");
   if(3 + 5 != 8 || -4 + 7 != 3)       print_string("plus4: FAIL\n");
   
   /* Verify subtraction */
   if(3 - 3 != 0 || j - j != 0)        print_string("minus1: FAIL\n");
   if(j - 3 != 34 || k - 3 != -8)      print_string("minus2: FAIL\n");
   if(i - 0 != 0 || j - 0 != 37)       print_string("minus3: FAIL\n");
   if(0 - i != 0 || 0 - k != 5)        print_string("minus4: FAIL\n");
   if(1 - i != 1 || k - -3 != -2)      print_string("minus5: FAIL\n");
   
   /* That's it for now. */
   print_string("succ\n");

}