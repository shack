/* Simple Recursion Test 
   Justin David Smith */
   

#include "fc.h"


int a(int x) {
   if(x == 0) return 1;
   else return a(x - 1) + a(x - 1);
}


int b(int y) {
   if(y == 0) return 2;
   else return a(y - 1) + b(y - 1);
}


int c(int z) {
   if(z == 0) return 3;
   else return b(z - 1) + c(z - 1);
}


int main(int argc, char **argv) {
   print_int(a(5));
   print_string("; ");
   print_int(b(5));
   print_string("; ");
   print_int(c(5));
   print_string("\n");
   print_string("succ\n");
}
