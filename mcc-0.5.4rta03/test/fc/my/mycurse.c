/* Justin David Smith
   More Fun With Function Pointers */


#include "fc.h"
   
   
int foo(int (*bar)(int), int x) {
   int boo(int f) {
      return bar(x) * f;
   }
   return boo(2);
}


int test(int w) {
   
   int baz(int y) {
      return y + 1;
   }

   int z = foo(baz, w);

   print_int(w);
   print_string(", ");
   print_int(z);
   if(z != w * 2 + 2) print_string(" -- FAIL");
   print_string("\n");

}
   

int main(int argc, char **argv) {

   int i;
   for(i = 0; i < 10; ++i) test(i);   
   print_string("succ\n");
   
}


