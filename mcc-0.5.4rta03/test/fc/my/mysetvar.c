/* Justin David Smith
   Test case checking on SetVar's */
   
   
int x;


#include "fc.h"

int print(int x) { 
   print_int(x); 
   print_string("\n"); 
}


int bar() {
   ++x;
   print(x);
}


int main(int argc, char **argv) {

   x = 0;   print(x);
   ++x;     print(x);
   int (*foo)() = bar;
   ++x;     print(x);
   bar();
   ++x;     print(x);
   print_string("\nYou should see a sequence of 01234.\nsucc\n");

}
