/* Justin David Smith
   Function Pointer Stress Test */
   
   
/* This triggered the &main addressing function bug,
   where function names could end up ``escaping'' to
   the data segment.  Fixed by modifying AST->IR code. */
   
/* The original version of this file triggered FUN_NIL_BUG.
   It has been modified so the function pointer is initialised
   to something. */


#include "fc.h"


typedef int (*fun)(char);
int tmp(char x) { }


/* This triggers the data_seg standardisation error.
   The `fun' type in data_seg still refers to the type
   from previous stages, even after standardising.  */
fun f = tmp;
int x;


int foo(char c) {
   x = 0;
   fun baz = f;
   f(c);
   return baz(c + 1);
}


int bar(char c) {
   f = bar;
   f = &bar;
   return c;
}


int main(int argc, char **argv) {
   print_int(bar('a')); print_string("\n");
   print_int(foo('b')); print_string("\n");
   print_string("succ\n");
}
