/* Justin David Smith
   Test Case abuses CPS-conversion */
   
/* Based on a case posted to the bulletin board */

/* This case triggered the ``main-inversion'' bug in CPS conversion. */


#include "fc.h"


int f() {
   print_string("Hello world\n");
   5;
}


/* This is a GLOBAL variable. */
/* Note, this shouldn't be optimised away by DCE */
int i;


/* This line triggered main-inversion */
int j = f();


/* This should still be a GLOBAL variable. */
/* Note, this shouldn't be optimised away by DCE */
int k;


/* This variable doesn't escape anywhere, but it should be global. */
int l;


/* This should still be a global function. */
int main(int argc, char **argv) {
   j += i + k;
   print_int(j);
   print_string("\nsucc\n");
}
