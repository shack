/* Test While Loops
   Justin David Smith */

/* ESCBINOP:
   This case triggered the ``binop internal error'' escaping bug.
   This turned out to be an error with the IR code in pointer  
   arithmetic.  */
         
/* NOTE: this test case triggered the SeqExpr bug. */
/* This file is part of the SeqExpr bugfix abuse series. */

/* This case has been updated to disable floating-point ops. */


#include "fc.h"
   

int while1() {
   int i = 10;
   while(i > 0) {
      --i;
   }
   return i;
}


int while2() {
   int j = 100;
   while(j > 001) {
      j /= 2;
   }
   return j;
}


int while3() {
   char ch[10];
   char *p;
   for(p = ch + 9; p != ch; p--) {
      /* Do nothing ominously */
   }
   return 0;
}


int while4() {
   char ch[10];
   char *p;
   for(p = ch + 9; p != ch; p--) {
      /* A malicious break expression */
      break;
   }
   return 0;
}


int main(int argc, char **argv) {
   print_string("while1 = ");
   print_int(while1());
   print_string("\nwhile2 = ");
   print_int(while2());
   print_string("\nwhile3 = ");
   print_int(while3());
   print_string("\n");
   print_string("succ\n");
}
