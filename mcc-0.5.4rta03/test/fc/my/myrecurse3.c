/* Simple Mutual Recursion Test 
   Justin David Smith */

/* This file is a test case for the DSYM_BUG. */

#include "fc.h"
   
   
/* Forward definition for b */
int b(int);


int a(int x) {
   return b(x);
}


int b(int y) {
   return y;
}


int main(int argc, char **argv) {
   if(a(3) != 3) print_string("FAIL\n");
   else          print_string("succ\n");
}
