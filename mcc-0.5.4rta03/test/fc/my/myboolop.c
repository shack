/* Boolean Operator Tests
   Justin David Smith */

/* ESCFRAMETYPE:
   This case triggered the ``frametype-not-aggregate-star'' escaping bug. */
            
/* NOTE: this test case triggered the SeqExpr bug. */

/* This case has been updated to disable floating-point operations */

#include "fc.h"
   
   
int main(int argc, char **argv) {
   
   int   i1 = 0;
   int   i2 = 1;
   int   i3 = 2;
   char  c1 = 'a';
   char  c2 = '\n';
   char  c3 = '\t';
   int   f1 = 314;
   int   f2 = 6022;
   int   f3 = i1;
   
   char  *pc1 = &c2;
   char  *pc2 = pc1;
   int   *pf1 = &f3;
   int   *pf2 = pf1;
   
   /* Some begin matter */
   ++f1;
      
   /* Boolean comparative operators */
   i1 = (i2 < i3);
   i1 = (f2 > f3);
   i1 = (pc1 == pc2);
   i1 = (pf1 != pf2);
   i1 = (c1 >= c2);
   i1 = (c2 <= i3);
   
   /* Boolean logic operators */
   i1 = !i2;
   i1 = (i2 && i3);
   i1 = (i2 || i3);
   i1 = (!i2 || (c3 && !c1));
   
   /* More complicated expressions */
   i1 = (i2 != i3 || c2 >= c3);
   i1 = (i2 <= i3 || pc1 == pc2);
   
   /* Boolean coercion */
   if(c1) {
      if(i2) {
         i1 = 1;
      } else {
         i1 = 2;
      }
   } else if(f1) {
      i1 = 3;
   } else {
      i1 = 4;
   }
   
   print_string("Looks like we didn't crash! \nsucc\n");

   /* Some end matter */
   ++f1;
   
}
