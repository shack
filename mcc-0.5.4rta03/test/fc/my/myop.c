/* Basic Operator Tests
   Justin David Smith */

/* ESCFRAMETYPE:
   This case triggered the ``frametype-not-aggregate-star'' escaping bug. */

/* This case has been updated to disable floating-point ops. */

#include "fc.h"
      

int main(int argc, char **argv) {
   
   int   i1 = 0;
   int   i2 = 1;
   int   i3 = 2;
   char  c1 = 'a';
   char  c2 = '\n';
   char  c3 = '\t';
   int   f1 = 314;
   int   f2 = 6022;
   int   f3 = i1;
   
   char  *pc1 = &c2;
   char  *pc2 = pc1;
   int   *pf1 = &f3;
   int   *pf2 = pf1;
   
   /* Additive operators */
   i1  = i2 + i3;
   f1  = f2 + f3;
   c1  = c2 + c3;
   pc1 = &c1;
   pc2 = pc1 + 1;
   pc2 = pc1 + 3;
   pf1 = &f2;
   pf1 = pf1 + 1;
   pf2 = pf1 + 3;
   pf1 = &f2;
   pf2 = &f3;
   f3  = *pf1 + *pf2;
   
   /* Subtractive operators */
   i1  = i2 - i3;
   f1  = f2 - f3;
   c1  = c2 - c3;
   pc1 = &c2;
   pc2 = pc1 - 1;
   pc2 = pc1 - 3;
   i1  = pc2 - pc1;
   i2  = pc1 - pc2;
   pf1 = &f2;
   pf1 = pf1 - 1;
   pf2 = pf1 - 3;
   i1  = pf1 - pf2;
   pf1 = &f2;
   pf2 = &f3;
   f3  = *pf1 - *pf2;
   
   /* Multiplicative operators */
   i1 = i2 * i3;
   f1 = f2 * f3;
   c1 = c2 * c3;
   i1 = i2 / i3;
   f1 = f2 / f3;
   i1 = i2 % i3;
   
   /* Bitwise operators */
   i1 = i2 | i3;
   i1 = i2 ^ i3;
   i1 = i2 & i3;
   
   /* Unary operations */
   ++i1;
   ++f1;
   ++c1;
   ++pc1;
   --i1;
   --pc1;
   pc1++;
   f1++;
   pc1--;
   f1--;
   
   print_string("Guess we didn't crash...\nsucc\n");
   
}
