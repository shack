/* Justin David Smith
   CSE Test Cases */
   

#include "fc.h"


int main(int argc, char **argv) {

   /* Must use structures to block constant folding :) */
   struct {
      int x;
   } X, I, J;
   int i, j, k, l, m;
   
   I.x = 1;
   J.x = 2;
   i = I.x;
   j = J.x;
   
   k = i + j;
   l = i + j;
   k = i + j;
   l = i + j;
   m = k + l;
   i = m + l;
   j = m + l;
   k = i + j;
   l = i + j;
   m = k + l;
   
   X.x = m;
   
   print_int(X.x);
   if(X.x == 36) print_string("\nsucc\n");
   else          print_string("\nFAIL\n");

}
