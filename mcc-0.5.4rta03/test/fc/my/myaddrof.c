/* Basic Operator Tests
   Justin David Smith */

/* ESCFRAMETYPE:
   This case is intended to trigger the ``frametype-not-aggregate-star'' 
   escaping bug.  I think it is related to handling of array aggregates.  */

#include "fc.h"


int main(int argc, char **argv) {
   int *a;
   int  *pa = a;
   int **ppa = &a;
   
   if(ppa == &a) print_string("succ\n");
   else print_string("FAIL\n");
   
}
