/* Pointer Stress Test 
   Justin David Smith */

#include "fc.h"


int main(int argc, char **argv) {

   char **p;
   char a[10];
   char *b;
   char c = 'a';
   int i = 0;

   /* Stress test the pointers and & */
   b = a;
   b = &a[2];
   b = &*a;
   c = *(a + 2);
   c = a[2];
   c = *(&*a);
   b = &c;
// The following case is actually illegal:
// p = &a;
   b = &a[0];
   
   /* Stress test pointers in assignment */
   a[0] = c;
   a[1] = c;
   p = &b;
   b = a + 1;
   *p = a + 1;
   **p = c;
   *(&c) = c;
   i = 2;
   b = a;
   b += i;
   *b = 3;
   *b += i;
   
   /* Return value must agree w/ int */
   c;
   
   print_string("Guess we didn't crash\nsucc\n");

}
