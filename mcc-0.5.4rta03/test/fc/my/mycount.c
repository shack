/* Justin David Smith 
   Simple Counter - Test Closures */

#include "fc.h"


int cnt(int i) {
   int j = 0;
   while(j < i) ++j;
   return j;
}


int rec_cnt(int i) {
   int j = 0;
   int cnt() { 
      if(j >= i) return j; 
      ++j; 
      return cnt(); 
   }
   return cnt();
}


int main(int argc, char **argv) {
   if(cnt(10) != 10) {
      print_string("FAIL 1, got ");
      print_int(cnt(10));
      print_string("\n");
   }
   if(rec_cnt(10) != 10) {
      print_string("FAIL 2, got ");
      print_int(rec_cnt(10));
      print_string("\n");
   }
   print_string("succ\n");
}
