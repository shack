/* Justin David Smith
   Initialising function ptr's */

/* This case tests for the STARFUN_BUG. */

#include "fc.h"


int boo(int x) {
   return x;
}


int main(int argc, char **argv) {
   int (*f)(int) = boo;
   // Uncomment next line to test STARFUN_BUG
   //(*f)(2);
   (f)(3);
   (&boo)(4);
   print_string("succ\n");
}
