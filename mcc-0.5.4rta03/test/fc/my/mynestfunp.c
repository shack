/* Justin David Smith
   Function Pointer Stress Test, II */
   

/* This triggered the &main addressing function bug,
   where function names could end up `escaping'' to
   the data segment.  Fixed by modifying AST->IR code. */

/* The original version of this file triggered FUN_NIL_BUG.
   It has been modified so the function pointer is initialised
   to something. */


#include "fc.h"


int main(int argc, char **argv) {
   /* Notice the difference from myfunptr.c? */

   typedef int (*fun)(char);
   int tmp(char c) { }

   fun f = tmp;
   int x;

   int foo(char c) {
      x = 0;
      fun baz = f;
      f(c);
      return baz(c + 1);
   }

   int bar(char c) {
      f = bar;
      f = &bar;
      return c;
   }

   print_int(bar('a'));
   print_string(", ");
   print_int(foo('b'));
   print_string("\nGuess we didn't crash ...\nsucc\n");
}
