/* Justin David Smith   
   Global Variable Escape Test */

/* DATA_SEG:
   This case triggered the ``data_seg'' error.  Seems that data_seg is
   not defined.  So of course, x escapes, and hell breaks loose...  */

#include "fc.h"

int x;

int foo() {
   /* x should escape */
   return x;
}

int main(int argc, char **argv) {
   foo();
   print_string("Guess we didn't crash...\nsucc\n");
}

