/* Justin David Smith
   Structures with arrays */
   
   
/* This case triggers the projection type error. */
   

#include "fc.h"
struct item { int i[4]; };


int print_item(struct item i) {

   int j;
   for(j = 0; j < 4; ++j) {
      print_int(i.i[j]);
      print_string(" ");
   }

} 


int main(int argc, char **argv) {

   struct item i;
   i.i[0] = 3;
   i.i[1] = 2;
   i.i[2] = 1;
   i.i[3] = 0;
   print_item(i);
   print_string("\nshould see the sequence 3 2 1 0\n");
   print_string("succ\n");

}
