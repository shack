/* Justin David Smith
   Attempting to track strange division hangs. */


/* This case has been updated to disable floating-point ops */
   

#include "fc.h"


int main(int argc, char **argv) {
   int a = 48;
   char c;
   char d;
   char e;
   char f;
   char r;
   
   c = a / -1;
   c = a / 2;
   c = a / -3;
   c = a / -4;

   print_int(c);
   if(c != 244) print_string("\nFAIL\n");
   else         print_string("\nsucc\n");
}
