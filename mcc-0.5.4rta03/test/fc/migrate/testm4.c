/* Garbage collection and migration
   Justin David Smith */
   
   
#include "fc.h"
#define  CHAIN_SIZE     0x4000


typedef struct _link {
   int           value;
   struct _link *next;
} link;


/* Allocate a linked list of entries */
link *alloc_chain(int size, int initv) {

   link *n;
   if(size <= 0) return(NULL);
   n = (link *)malloc(sizeof(link));
   n->value = initv;
   n->next  = alloc_chain(size - 1, initv + 1);
   return(n);

}


/* Iterate over a chain, printing values */
void iter_chain(const link *chain) {

   if(chain == NULL) return;
   print_int(chain->value);
   print_string("\n");
   iter_chain(chain->next);

}


/* Print destination information */
void print_destination(const char *msg, const char *dst) {

   print_string(msg);
   print_string(" ");
   print_string(dst);
   print_string("\n");

}


/* Main program */
int main(int argc, char **argv) {

   if(argc != 2) {
      print_string("Usage:  ./testm4.exec <host://destination>\n");
      return(1);
   }
   
   /* Allocate a chain before migration */
   print_string("Allocating first chain...\n");
   link *chain1 = alloc_chain(CHAIN_SIZE, 0);
   print_string("Allocated first chain...\n");

   /* Migrate to destination */
   print_destination("Migrating to", argv[1]);
   migrate(argv[1]);
   print_destination("Migrated to", argv[1]);
   
   /* Allocate another chain after migration */
   print_string("Allocating second chain...\n");
   link *chain2 = alloc_chain(CHAIN_SIZE, CHAIN_SIZE);
   print_string("Allocated second chain...\n");
   
   print_string("Iterating over both chains...\n");
   iter_chain(chain1);
   iter_chain(chain2);
   
   print_string("Done\n");
   return(0);
   
}
