/* Simplest migration test
   Justin David Smith */


#include "fc.h"


int main(int argc, char **argv) { 

   print_string("Expect exit with code 42\n");
   migrate("host://localhost");
   return(42); 

}
   
