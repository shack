/* Migration Inside Atomic Levels
   Justin David Smith  */


#include <fc.h>


#define  SIZE  1000


typedef struct _link {
   int value;
   struct _link *next;
} link;


static struct _link *append_to_link(link *l) {

   int i;

   for(i = 0; i < SIZE; ++i) {
      link *old_link = l;
      l = (link *)malloc(sizeof(link));
      l->value = 0xffff0000 | i;
      l->next = old_link;
   }
   
   return(l);

}


static void check_link(link *l, int size) {

   size *= SIZE;
   print_string("Verifying link length is ");
   print_int(size);
   print_string("\n");
   while(l != NULL) {
      --size;
      l = l->next;
   }
   if(size != 0) {
      print_string("FAIL: link structure not compliant: size is now ");
      print_int(size);
      print_string("\n");
   }

}


static int atomic_entry_if(int x) {
   
   if(x == 0) {
      print_string("Entering new level\n");
      x = atomic_entry(x);
      if(x != 0) {
         print_string("Committing level from prior retry\n");
         atomic_commit();
      }
   }
   return(x);
   
}


static void do_rollback(int i) {

   print_string("About to rollback with parametre ");
   print_int(i);
   print_string("\n");
   atomic_rollback(i);
   
}


int main(int argc, char **argv) {

   if(argc < 2) {
      print_string("Usage:  atomic_migrate2.exec <host>\n");
      return(1);
   }

   link *l = NULL;
   int a, b, c, d, e;
   
   l = append_to_link(l);
   a = atomic_entry_if(0);
   print_string("After entry 1: ");
   print_int(a);
   print_string("\n");
   check_link(l, 1);

   l = append_to_link(l);
   b = atomic_entry_if(a);
   print_string("After entry 2: ");
   print_int(b);
   print_string("\n");
   check_link(l, 2);

   l = append_to_link(l);
   c = atomic_entry_if(b);
   print_string("After entry 3: ");
   print_int(c);
   print_string("\n");
   check_link(l, 3);

   l = append_to_link(l);
   d = atomic_entry_if(c);
   print_string("After entry 4: ");
   print_int(d);
   print_string("\n");
   check_link(l, 4);

   l = append_to_link(l);
   e = atomic_entry_if(d);
   print_string("After entry 5: ");
   print_int(e);
   print_string("\n");
   check_link(l, 5);

   if(e == 0) {
      print_string("About to migrate.\n");
      migrate(argv[1]);
      check_link(l, 5);
      do_rollback(1);
   } else if(e < 5) {
      /* Make sure we can rollback safely */
      l = append_to_link(l);
      check_link(l, 6);
      do_rollback(e + 1);
   }

   print_string("Transactions completed.\n");   
   return(0);

}
