/* Migration Inside Atomic Levels
   Justin David Smith  */


#include <fc.h>


int main(int argc, char **argv) {

   if(argc < 2) {
      print_string("Usage:  atomic_migrate1.exec <host>\n");
      return(1);
   }

   int n = atomic_entry(0);
   if(n == 0) {
      migrate(argv[1]);
      atomic_rollback(1);
   } else {
      atomic_commit();
      print_string("Transaction ended.\n");
   }
   
   return(0);

}
