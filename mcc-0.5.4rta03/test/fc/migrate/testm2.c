/* Simple migration test
   Justin David Smith */


#include "fc.h"


int main(int argc, char **argv) { 

   int i;

   if(argc != 3) {
      print_string("Usage:  ./testm2.exec <host://destination> <migrate-value>\n");
      return(1);
   }
   
   /* Attempt to migrate */
   i = atoi(argv[2]);
   print_string("Expect exit with code ");
   print_int(i * 2);
   print_string("\n");
   migrate(argv[1]);
   
   /* Print result */
   if(i != atoi(argv[2])) print_string("BOGUS: Values i and argv[2] disagree!\n");
   print_string("Result is ");
   print_int(i + atoi(argv[2]));
   print_string("\n");
   return(i + atoi(argv[2]));

}
