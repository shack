/* Simple timed migration test
   Justin David Smith */


#include "fc.h"

typedef unsigned long timeval;


void print_timeval(timeval time) {

   print_string("Total microseconds:  ");
   print_long(time);
   print_string("\n");

}


int main(int argc, char **argv) { 

   if(argc != 2) {
      print_string("Usage:  ./testm6.exec <host://destination>\n");
      return(1);
   }
   
   /* Attempt to migrate */
   timeval time1, time2;

   time1 = utime();
   print_timeval(time1);
   migrate(argv[1]);
   time2 = utime();
   print_timeval(time2);
   
   /* Print the final time */
   print_timeval(time2 - time1);
   return(0); 

}
