/* Simple migration test
   Justin David Smith */


#include "fc.h"


int main(int argc, char **argv) { 

   int i = 32;          /* Input value */

   if(argc != 2) {
      print_string("Usage:  ./testm1.exec <host://destination>\n");
      return(1);
   }
   
   /* Attempt to migrate */
   print_string("Expect exit with code 42\n");
   migrate(argv[1]);
   return(i + 10); 

}
