/* Sanity migration test for registers
   Justin David Smith */


#include "fc.h"


int main(int argc, char **argv) { 
   int g(int x) { x; }

   int i = 32;
   int j = 64;
   int k = 128;
   int (*foo)(int) = g;
   char *s = "Hello world!\n";

   if(argc != 2) {
      print_string("Usage:  ./testm3.exec <host://destination>\n");
      return(1);
   }
   migrate(argv[1]);
   
   if(i == 32) {
      if(j != 64) return(2);
      if(k != 128) return(3);
      if(foo(i + j + k) != g(i + j + k)) return(4);
   
      /* Here goes nothing... */
      print_string(s);
      
      /* Success! */
      return(0);
   }
   
   /* Failure... */
   return(1);
}
   
