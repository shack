/* Simple timed migration test
   Justin David Smith */


#include "fc.h"


struct timeval {
   int tv_sec;
   int tv_usec;
};
struct timezone {
   int tz_minuteswest;
   int tz_dsttime;
};

int gettimeofday(struct timeval *tv, struct timezone *tz);


void print_timeval(struct timeval *tv) {

   print_string("Total seconds:       ");
   print_int(tv->tv_sec);
   print_string("\nTotal microseconds:  ");
   print_int(tv->tv_usec);
   print_string("\n");

}


int main(int argc, char **argv) { 

   if(argc != 2) {
      print_string("Usage:  ./testm5.exec <host://destination>\n");
      return(1);
   }
   
   /* Attempt to migrate */
   struct timeval tv1 = { 0, 0 };
   struct timeval tv2 = { 0, 0 };
   gettimeofday(&tv1, NULL);
   print_timeval(&tv1);
   migrate(argv[1]);
   gettimeofday(&tv2, NULL);
   print_timeval(&tv2);
   
   /* Print the final time */
   struct timeval delta = { 0, 0 };
   delta.tv_sec  = tv2.tv_sec  - tv1.tv_sec;
   delta.tv_usec = tv2.tv_usec - tv1.tv_usec;
   while(delta.tv_usec < 0) {
      delta.tv_usec += 1000000;
      --delta.tv_sec;
   }
   print_timeval(&delta);
   return(0); 

}
