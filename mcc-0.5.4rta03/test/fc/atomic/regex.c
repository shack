/* Simple regex matcher
   Justin David Smith */
   
   
#include "fc.h"


int match(const char *pattern, const char *buffer) {

   /* Set initial pattern and buffer pointers */
   const char *ppat = pattern;
   const char *pbuf = buffer;
   
   /* Enter an outermost atomic level; if we jump out to this level,
      then it is because a pattern failed to match... */
   if(atomic_entry(0) != 0) {
      print_string("Pattern does not match this buffer\n");
      return(0);
   }
   
   /* Try to match */
   while(*ppat != '\0') {
      /* What is the next character in the pattern? */
      if(*ppat == '*') {
         /* Try to match next buffer character as part of the wildcard.
            This is a branching point, so we enter a new atomic scope.
            We try to include buf as part of the wildcard; if that fails,
            then we'll try to terminate the wildcard at this character. */
         if(*pbuf == '\0') {
            /* Must advance pattern now */
            ++ppat;
         } else {

            /* This is a branching point; initially advance only the
               buffer pointer; if that fails, we'll advance only the
               pattern pointer. */
            if(atomic_entry(0) == 0) {
               ++pbuf;
            } else {
               atomic_commit();
               ++ppat;
            }
         }
      } else {
         /* Not a wildcard character in the pattern; next
            buffer character must exactly match. */
         if(*pbuf == *ppat) {
            /* Advance both pattern and buffer */
            ++ppat;
            ++pbuf;
         } else {
            /* This match attempt failed; rollback to last branch */
            atomic_rollback(1);
         }
      } /* What is current pattern character? */

   } /* Loop until all pattern characters are resolved. */
   
   /* In the end, we succeeded if the buffer is done. */
   if(*pbuf != '\0') {
      atomic_rollback(1);
   }
   print_string("Pattern matches this buffer\n");
   return(1);

}


int main(int argc, char **argv) {

   if(argc < 3) {
      print_string("Usage:  ./regex.exec <pattern> <buffer>\n");
      return(2);
   }
   
   /* Get the pattern and the buffer */
   const char *pattern = argv[1];
   const char *buffer = argv[2];
   
   /* Try to match! */
   return(!match(pattern, buffer));

}
