/* Migration Inside Atomic Levels
   Justin David Smith  */


#include <fc.h>


/* Test case configuration */
#define  SIZE        100000
#define  LEVELS      5


typedef struct _link {
   int value;
   struct _link *next;
} link;


static struct _link *append_to_link(link *l) {

   int i;

   for(i = 0; i < SIZE; ++i) {
      link *old_link = l;
      l = (link *)malloc(sizeof(link));
      l->value = 0xffff0000 | i;
      l->next = old_link;
   }
   
   return(l);

}


static void check_link(link *l, int size) {

   size *= SIZE;
   print_string("Verifying link length is ");
   print_int(size);
   print_string("\n");
   while(l != NULL) {
      --size;
      l = l->next;
   }
   if(size != 0) {
      print_string("FAIL: link structure not compliant: size is now ");
      print_int(size);
      print_string("\n");
   }

}


static int atomic_entry_if(int x) {
   
   if(x == 0) {
      print_string("Entering new level\n");
      x = atomic_entry(x);
      if(x != 0) {
         print_string("Committing level from prior retry\n");
         atomic_commit();
      }
   }
   return(x);
   
}


static int atomic_call_then_validate(link *l, int level, int x) {

   int y = atomic_entry_if(x);
   print_string("After entry ");
   print_int(level);
   print_string(": ");
   print_int(y);
   print_string(" (entered with ");
   print_int(x);
   print_string(")\n");
   check_link(l, level);
   return(y);

}


static void do_rollback(int i) {

   print_string("About to rollback with parametre ");
   print_int(i);
   print_string("\n\n");
   atomic_rollback(i);
   
}


int main(int argc, char **argv) {

   link *l = NULL;
   int x = 0;
   int i;

   for(i = 1; i <= LEVELS; ++i) {
      l = append_to_link(l);
      x = atomic_call_then_validate(l, i, x);
   }

   if(x == 0) {
      /* Start the vicious rollback cycle */
      print_string("Initial rollback\n");
      do_rollback(1);
   } else if(x < LEVELS) {
      /* Make sure we can rollback safely */
      l = append_to_link(l);
      check_link(l, LEVELS + 1);
      do_rollback(x + 1);
   }

   print_string("Transactions completed.\n");   
   return(0);

}
