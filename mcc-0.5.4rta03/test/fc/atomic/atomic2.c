/* Trivial atomic test case
   Justin David Smith */
   
   
#include "fc.h"
         
   
int main(int argc, char **argv) {

   int x[10];
   x[0] = 26;
   int iter = atomic_entry(0);
   print_string("Entered atomic scope\n");
   ++x[0];
   print_string_int("x[0] is currently ", x[0]);
   print_string("\n");
   if(iter < 5) {
      print_string("About to rollback\n");
      atomic_rollback(++iter);
   } else {
      atomic_commit();
   }
   if(x[0] != 27) {
      print_string("FAIL: x[0] was incorrect\n");
   }
   return(iter + x[0]);

}
