/* Atomic test case - rollback in FIFO (not nested) order
   Justin David Smith */
   
   
#include "fc.h"

static int atomic_levels() = "atomic_levels";
         
   
int main(int argc, char **argv) {

   int x = 31;
   
   /* Enter the atomic levels */
   int a = atomic_entry(0);
   if(a == 0) {
      x = 90;
      int b = atomic_entry(a);
      x = 91;
      int c = atomic_entry(b);
      if(c == 0) {
         x = 92;
         int d = atomic_entry(c);
         x = 93;
         int e = atomic_entry(d);
         x = 94;
         if(e != 0) {
            print_string("FAIL:  Bad entry result code\n");
         }
         if(atomic_levels() != 5) {
            print_string("FAIL:  Bogus level count\n");
         }

         /* 5 levels */
         atomic_rollback_level(3, 1);
      } else if(c == 1) {
         /* 3 levels left */
         if(atomic_levels() != 3) {
            print_string("FAIL:  Bogus level count\n");
         }
         atomic_rollback(2);
      } else {
         /* 3 levels left, still */
         if(atomic_levels() != 3) {
            print_string("FAIL:  Bogus level count\n");
         }
         atomic_rollback_level(1, 3);
      }
   }         
   if(atomic_levels() != 1) {
      print_string("FAIL:  Bogus level count\n");
   }

   /* only the top level lives */
   atomic_commit();
   
   /* No atomic levels left */
   if(atomic_levels() != 0) {
      print_string("FAIL:  Bogus level count\n");
   }
   print_string("Return code should be 32\n");
   return(x + 1);

}
