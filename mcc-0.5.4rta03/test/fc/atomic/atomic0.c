/* Trivial atomic test case
   Justin David Smith */


#include "fc.h"
   
   
int main(int argc, char **argv) {

   int x;
   int z = atomic_entry(32);
   x = z;
   atomic_commit();
   return(x);

}
