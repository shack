/* Linked list atomic test case
   Justin David Smith */
   
   
#include "fc.h"
         

typedef struct _link {
   int value;
   struct _link *next;
} link;


struct _link *link_new(int i, link *next) {

   link *l = (link *)malloc(sizeof(link));
   l->value = i;
   l->next = next;
   return(l);

}


struct _link *link_alloc(int size) {

   link *l = NULL;
   int i;
   
   for(i = 0; i < size; ++i) {
      l = link_new(i, l);
   }
   return(l);

}


struct _link *link_last(link *l) {

   while(l != NULL && l->next != NULL) {
      l = l->next;
   }
   return(l);

}


void link_check(link *l) {

   int prev = -1;
   
   while(l != NULL) {
      if(prev != -1 && l->value != prev - 1) {
         print_string("FAIL: link_check failed\n");
         return;
      }
      prev = l->value;
      l = l->next;
   }
   if(prev != 0) {
      print_string("FAIL: link_check final failed\n");
   }

}


int main(int argc, char **argv) {

   link *l = link_alloc(32);
   link_check(l);
   
   int iter = atomic_entry(0);
   if(iter < 5) {
      l->next->next->next->next = NULL;
      atomic_rollback(++iter);
   } else {
      atomic_commit();
   }
   link_check(l);

   print_string("Done.\n");
   return(0);

}
