/* Trivial atomic test case
   Justin David Smith */
   
   
#include "fc.h"
         
   
int main(int argc, char **argv) {

   int x = 26;
   int iter = atomic_entry(0);
   print_string("Entered atomic scope\n");
   ++x;
   print_string_int("x is currently ", x);
   print_string("\n");
   if(iter < 5) {
      print_string("About to rollback\n");
      atomic_rollback(++iter);
   } else {
      atomic_commit();
   }
   if(x != 27) {
      print_string("FAIL: x was incorrect\n");
   }
   return(iter + x);

}
