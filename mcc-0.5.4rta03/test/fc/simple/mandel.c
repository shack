/*
 * Benchmark.
 * Compute Mandelbrot set over a small space.
 */
#include "fc.h"

/*
 * Program constants
 */
static int limit = 255;
static int dimen = 400;
static int scale = 1000;
static int bound = 4095;

/*
 * Fixed-point numbers.
 */
static int fix_shift = 24;

typedef int fix;

static fix fix_int(int i)
{
    return i << fix_shift;
}

static fix fix_scale(int i)
{
    return ((i << (fix_shift / 2)) / scale) << (fix_shift / 2);
}

static fix fix_add(fix i, fix j)
{
    return i + j;
}

static fix fix_sub(fix i, fix j)
{
    return i - j;
}

static fix fix_mul(fix i, fix j)
{
    return (i >> (fix_shift / 2)) * (j >> (fix_shift / 2));
}

static fix fix_mul_int(fix i, int j)
{
    return i * j;
}

static fix fix_div_int(fix i, int j)
{
    return i / j;
}

/*
 * Compute a point in the Mandelbrot set.
 */
static int compute(fix x, fix y)
{
    fix usex, usey;
    fix xsqr, ysqr;
    int k;

    usex = x;
    usey = y;
    for(k = 0; k != bound; k++) {
        xsqr = fix_mul(usex, usex);
        ysqr = fix_mul(usey, usey);

        if(fix_add(xsqr, ysqr) > fix_int(4))
            break;
        usey = fix_add(fix_mul(fix_int(2), fix_mul(usex, usey)), y);
        usex = fix_add(fix_sub(xsqr, ysqr), x);
    }
    return k;
}

/*
 * Main function.
 */
int main(int argc, char **argv)
{
    fix x, y, left, top, step, scale2;
    int i, j, k;

    /* Check usage */
    if(argc != 4) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" x y scale (numbers are multiplied by ");
        print_int(scale);
        print_string(")\n");
        print_string("Examples:\n");
        print_string("\ttest5 0 0 2000 > x.pgm\n");
        print_string("\ttest5 120 -640 100 > x.pgm\n");
        print_string("To view it, use xv\n");
        print_string("\txv x.pgm\n");
        return 1;
    }

    /* Set up screen area */
    left = fix_scale(atoi(argv[1]));
    top = fix_scale(atoi(argv[2]));
    scale2 = fix_scale(atoi(argv[3]));
    left = fix_sub(left, fix_div_int(scale2, 2));
    top = fix_sub(top, fix_div_int(scale2, 2));
    step = fix_div_int(scale2, dimen);

    /* Print the PGM header */
    print_string("P2\n# CREATOR: test5\n");
    print_int(dimen);
    print_string(" ");
    print_int(dimen);
    print_string("\n255\n");

    /* Plot each pixel */
    for(i = 0; i != dimen; i++) {
        for(j = 0; j != dimen; j++) {
            x = fix_add(left, fix_mul_int(step, i));
            y = fix_add(top, fix_mul_int(step, j));
            k = compute(x, y);
            print_int(255 - (k % 256));
            print_string(" ");
        }
        print_string("\n");
    }
    return 0;
}
