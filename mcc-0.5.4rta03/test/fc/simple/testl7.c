/* Test mul/div int manipulations
   Justin David Smith */
   

#include "fc.h"


int shift8  = 8;
int shift16 = 16;
int shift31 = 31;
int shift32 = 32;
int shift36 = 36;
int shift48 = 48;


void print_num(long l) {

   print_long(l);
   print_string_long_hex(" (0x", l);
   print_string(")");

}


void error_long_result(char *msg, long v, long c) {

   print_string(msg);
   print_string(": expected ");
   print_num(c);
   print_string(", got ");
   print_num(v);
   print_string("\n");

}


void check_long_result(char *msg, long v, long c) {
   
   if(v != c) error_long_result(msg, v, c);
   
}


int main(int argc, char **argv) {

   print_string("Running program\n");
   
   /* Shift left ops */
   print_string("Running shift left test\n");
   check_long_result("shl1", 0xDEADBEEFL << shift8,  0x000000DEADBEEF00L);
   check_long_result("shl2", 0xDEADBEEFL << shift16, 0x0000DEADBEEF0000L);
   check_long_result("shl3", 0xDEADBEEFL << shift31, 0x6F56DF7780000000L);
   check_long_result("shl4", 0xDEADBEEFL << shift32, 0xDEADBEEF00000000L);
   check_long_result("shl5", 0xDEADBEEFL << shift36, 0xEADBEEF000000000L);
   check_long_result("shl6", 0xDEADBEEFL << shift48, 0xBEEF000000000000L);
   
   /* Shift right ops */
   print_string("Running shift right test\n");
   check_long_result("shr1", 0x0000DEADBEEF0099L >> shift8,  0xDEADBEEF00L);
   check_long_result("shr2", 0x0000DEADBEEFF001L >> shift16, 0xDEADBEEFL);
   check_long_result("shr3", 0x6F56DF778F001F00L >> shift31, 0xDEADBEEFL);
   check_long_result("shr4", 0xDEADBEEFF001F001L >> shift32, 0xFFFFFFFFDEADBEEFL);
   check_long_result("shr5", 0xEADBEEFF001F001FL >> shift36, 0xFFFFFFFFFEADBEEFL);
   check_long_result("shr6", 0xBEEFDEADF001F001L >> shift48, 0xFFFFFFFFFFFFBEEFL);

   /* Bitwise tests */
   long p1 = 0xFFFF0000FFFF0000L;
   long p2 = 0x00FFFF0000FFFF00L;
   print_string("Running bitwise test\n");
   check_long_result("bit1", p1 & p2, 0x00FF000000FF0000L);
   check_long_result("bit2", p1 | p2, 0xFFFFFF00FFFFFF00L);
   check_long_result("bit3", p1 ^ p2, 0xFF00FF00FF00FF00L);
   return 0;
}
