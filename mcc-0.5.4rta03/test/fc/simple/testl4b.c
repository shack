/* 64-bit comparison checks
   Justin David Smith */
   
   
#include "fc.h"
   
   
long load_long(int hi1, int lo1) {

   long hi = hi1;
   long lo = lo1;
   return((hi << 32) + (lo & 0xFFFFFFFFL));

}
 

int main(int argc, char **argv) {
   
   long max_long = load_long(0x7FFFFFFFL, 0xFFFFFFFFL);
   long min_long = load_long(0x80000000L, 0x00000000L);
   long minus_1  = load_long(0xFFFFFFFFL, 0xFFFFFFFFL);
   long plus_1   = load_long(0x00000000L, 0x00000001L);
   long zero     = load_long(0x00000000L, 0x00000000L);

   print_string("checking comparison operators\n");   
   if(min_long >= plus_1)  print_string("comp11 FAILED\n");
   if(min_long >= zero)    print_string("comp12 FAILED\n");
   if(min_long >= minus_1) print_string("comp13 FAILED\n");
   
   if(!(plus_1 > minus_1)) print_string("comp14 FAILED\n");
   if(!(plus_1 > zero))    print_string("comp15 FAILED\n");
   if(!(zero > minus_1))   print_string("comp16 FAILED\n");
   
   /* Omission of L suffix intentional here */
   if(zero != 0)           print_string("comp17 FAILED\n");
   if(plus_1 != 1)         print_string("comp18 FAILED\n");
   if(minus_1 != -1)       print_string("comp19 FAILED\n");
   if(max_long + 1 != min_long)
                           print_string("comp20 FAILED\n");
   return 0;
}
