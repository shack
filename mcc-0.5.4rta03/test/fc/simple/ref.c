/* Test basic reference semantics */


#include <fc.h>


static void zero(int *a) {
   *a = 0;
}
   

int main(int argc, char **argv) {
   int x = 1;
   zero(&x);
   print_int(x);
}
