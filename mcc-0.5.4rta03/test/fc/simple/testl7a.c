/* Trying to catch apparent constant error
   Justin David Smith */
   

#include "fc.h"


int main(int argc, char **argv) {

   print_string_long_hex("0x00000000DEADBEEFL = ", 0x00000000DEADBEEFL);
   print_string_long_hex("\n0x0000FFFFDEADBEEFL = ", 0x0000FFFFDEADBEEFL);
   print_string_long_hex("\n0x7FFFFFFFDEADBEEFL = ", 0x7FFFFFFFDEADBEEFL);
   print_string_long_hex("\n0xFFFFFFFFDEADBEEFL = ", 0xFFFFFFFFDEADBEEFL);
   print_string_long_hex("\n0x7000000000000000L = ", 0x7000000000000000L);
   print_string_long_hex("\n0x8000000000000000L = ", 0x8000000000000000L);
   print_string_long_hex("\n0xF000000000000000L = ", 0xF000000000000000L);
   print_string_long_hex("\n0xFFFF000000000000L = ", 0xFFFF000000000000L);
   print_string_long_hex("\n0xFFFFFFFF00000000L = ", 0xFFFFFFFF00000000L);
   print_string_long_hex("\n0xFFFFFFFF7FFFFFFFL = ", 0xFFFFFFFF7FFFFFFFL);
   print_string_long_hex("\n0xFFFFFFFF80000000L = ", 0xFFFFFFFF80000000L);
   print_string_long_hex("\n              (alt) = ", 0xFFFFFFFF00000000L + 0x80000000L);
   print_string_long_hex("\n0xFFFFFFFFFFFFFFFFL = ", 0xFFFFFFFFFFFFFFFFL);
   print_string("\n");
   return 0;
}
