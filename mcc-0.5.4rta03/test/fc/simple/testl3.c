/* Test basic int manipulations
   Justin David Smith */
   

#include "fc.h"


long load_long(int hi1, int lo1) {

   long hi = hi1;
   long lo = lo1;
   return((hi << 32) + (lo & 0xFFFFFFFFL));

}


void print_num(long l) {

   print_long(l);
   print_string_long_hex(" (0x", l);
   print_string(")");

}


void error_long_result(char *msg, long v, long c) {

   print_string(msg);
   print_string(": expected ");
   print_num(c);
   print_string(", got ");
   print_num(v);
   print_string("\n");

}


void error_long_resulti(char *msg, long v, int hi, int lo) {

   long c = load_long(hi, lo);
   error_long_result(msg, v, c);

}


void check_long_result(char *msg, long v, long c) {
   
   if(v != c) error_long_result(msg, v, c);
   
}


void check_long_resulti(char *msg, long v, int hi, int lo) {

   long c = load_long(hi, lo);
   check_long_result(msg, v, c);

}


int main(int argc, char **argv) {

   print_string("Running program\n");
   long l1 = load_long(0L, 0L);
   long l2 = load_long(0x10000L, 0x10000L);
   long l3 = load_long(0xFFFFFFFFL, 0xFFFFFFFFL);
   
   /* Prechecks */
   print_string("Running prechecks\n");
   if(l1 != 0L) error_long_resulti("precheck1", l1, 0L, 0L);
   if(l2 & 0xFFFFFFFFL != 0x10000L || l2 >> 32 != 0x10000L) error_long_resulti("precheck2", l2, 0x10000L, 0x10000L);
   if(l3 != -1L) error_long_resulti("precheck3", l3, 0xFFFFFFFFL, 0xFFFFFFFFL);
   
   /* Negation */
   print_string("Running negation test\n");
   check_long_resulti("neg1", -l1, 0L, 0L);
   check_long_resulti("neg2", -l2, 0xFFFEFFFFL, 0xFFFF0000L);
   check_long_resulti("neg3", -l3, 0L, 1L);
   check_long_resulti("neg4", -1L, 0xFFFFFFFFL, 0xFFFFFFFFL);
   
   /* Bitwise negation */
   print_string("Running bitwise negation test\n");
   check_long_resulti("not1", ~l1, 0xFFFFFFFFL, 0xFFFFFFFFL);
   check_long_resulti("not2", ~l2, 0xFFFEFFFFL, 0xFFFEFFFFL);
   check_long_resulti("not3", ~l3, 0L, 0L);

   /* Addition */
   print_string("Running addition test\n");
   check_long_resulti("add1", l1 + l2, 0x10000L, 0x10000L);
   check_long_resulti("add2", l2 + l3, 0x00010000L, 0x0000FFFFL);
   check_long_resulti("add3", l1 + l3, 0xFFFFFFFFL, 0xFFFFFFFFL);
   check_long_resulti("add4", l1 + l2 + l3, 0x00010000L, 0x0000FFFFL);
   check_long_resulti("add5", l1 + l2 + l3 + l3, 0x00010000L, 0x0000FFFEL);
   
   /* Subtraction */
   print_string("Running subtraction test\n");
   check_long_result("sub1", l1 - l2, -l2);
   check_long_result("sub2", l2 - l1, l2);
   check_long_result("sub3", l3 - l2 - l1, l3 + (-l2) + (-l1));
   return 0;
}
