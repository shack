/* Test shift operations
   Justin David Smith */


#include "fc.h"

typedef int ty_int;

void check_num(char *message, ty_int num, ty_int exp) {

   if(num != exp) {
      print_string(message);
      print_string(" FAILED\n");
      print_string("got ");
      print_int(num);
      print_string(", expected ");
      print_int(exp);
      print_string("\n");
   }

}


int main(int argc, char **argv) {

   ty_int i1 = 1234;
   ty_int i2 = 5678;
   ty_int i3 = -i2;
   
   /* Test shift left */
   print_string("checking shift left\n");
   check_num("shl0", i1 << 0, 1234);
   check_num("shl1", i1 << 1, 2468);
   check_num("shl2", i1 << 2, 4936);
   check_num("shl3", i1 << 8, 315904);

   /* Test shift right - positive */
   print_string("checking shift right - positive\n");
   check_num("shr0", i2 >> 0, 5678);
   check_num("shr1", i2 >> 1, 2839);
   check_num("shr2", i2 >> 2, 1419);
   check_num("shr3", i2 >> 4, 354);

   /* Test shift right - negative */
   print_string("checking shift right - negative\n");
   check_num("sar0", i3 >> 0, -5678);
   check_num("sar1", i3 >> 1, -2839);
   check_num("sar2", i3 >> 2, -1420);
   check_num("sar3", i3 >> 4, -355);
   return 0;
}
