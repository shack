#include "fc.h"

/*
 * Factorial.
 */
static int fact(int i)
{
    int k = 1;
    int j = i;

    for(j = i; j; j--)
        k *= j;
    return k;
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    int i;

    /* Check usage */
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <number>\n");
        return 1;
    }

    /* Get the number */
    i = atoi(argv[1]);

    /* Print the results */
    print_string("fact(");
    print_int(i);
    print_string(") = ");
    print_int(fact(i));
    print_string("\n");

    return 0;
}
