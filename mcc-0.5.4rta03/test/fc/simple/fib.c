#include "fc.h"

/*
 * Fibonacci numbers.
 */
static int fib(int i)
{
    if(i == 0 || i == 1)
        return i;
    else
        return fib(i - 1) + fib(i - 2);
}

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    int i;

    /* Check usage */
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <number>\n");
        return 1;
    }

    /* Get the number */
    i = atoi(argv[1]);

    /* Print the results */
    print_string("fib(");
    print_int(i);
    print_string(") = ");
    print_int(fib(i));
    print_string("\n");
    return 0;
}
