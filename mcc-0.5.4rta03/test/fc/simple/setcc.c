/* Test case for SETcc IA32 optimization (X86_branch) */


#include <fc.h>


int main(int argc, char **argv) {
   int j = (argc >= 2 ? 32 : 64);
   int k;
   if(argc >= 2) {
      k = 32;
   } else {
      k = 64;
   }
   print_int(argc);
   print_string(", ");
   print_int(j);
   print_string(", ");
   print_int(k);
   print_string("\n");
   return(!(j == k));
}
