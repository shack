/* Test bitwise operations
   Justin David Smith */


#include "fc.h"

typedef int ty_int;

void check_num(char *message, ty_int num, ty_int exp) {

   if(num != exp) {
      print_string(message);
      print_string(" FAILED\n");
      print_string("got ");
      print_hex(num);
      print_string(", expected ");
      print_hex(exp);
      print_string("\n");
   }

}


int main(int argc, char **argv) {

   ty_int i1 = 0x0;
   ty_int i2 = 0x4100;
   ty_int i3 = 0xFFFFFFFF;
   
   /* Test negation */
   print_string("checking bitwise negation\n");
   check_num("neg0", ~i1, 0xFFFFFFFF);
   check_num("neg1", ~i2, 0xFFFFBEFF);
   check_num("neg2", ~i3, 0x00000000);

   /* Test and */
   print_string("checking bitwise and\n");
   check_num("and0", i1 & i2, 0x00000000);
   check_num("and1", i2 & i3, 0x00004100);
   
   /* Test or */
   print_string("checking bitwise or\n");
   check_num("or0", i1 | i2, 0x00004100);
   check_num("or1", i2 | i3, 0xFFFFFFFF);
   
   /* Test xor */
   print_string("checking bitwise xor\n");
   check_num("xor0", i1 ^ i2, 0x00004100);
   check_num("xor1", i2 ^ i3, 0xFFFFBEFF);
   
   /* Test combos */
   print_string("checking bitwise combos\n");
   check_num("combo0", (i1 | 0xF0) ^ (i2 ^ i3), 0xFFFFBE0F);
   check_num("combo1", (i3 & 0xF0) ^ (i2 ^ i3), 0xFFFFBE0F);
   return 0;
}
