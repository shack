static unsigned int f(long i)
{
    return i + 1;
}

int main(int argc, char **argv)
{
    return f(1) + f(2);
}
