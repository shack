#include "fc.h"

/*
 * Main program takes the fibnacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    if(argc <= 1) {
	print_string("Usage: ");
	print_string(argv[0]);
	print_string(" <arg>\n");
    }
    else {
    	print_string("Hello world: ");
    	print_string(argv[1]);
    	print_string("\n");
    }
    return 0;
}
