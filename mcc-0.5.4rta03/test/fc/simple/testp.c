/* Pointer tests - inspired by problems passing arrays 
   Justin David Smith */


#include "fc.h"


typedef signed char testtype;


void f1(testtype *list, int size) {
   int i;
   for(i = 0; i < size; ++i) {
      list[i] = -list[i];
   }
}


void f2(testtype list[20], int size) {
   int i;
   for(i = 0; i < size; ++i) {
      list[i] = -list[i];
   }
}


void print_list(testtype *list, int size) {
   int i;
   for(i = 0; i < size; ++i) {
      print_int(list[i]);
      if(i + 1 < size) print_string(", ");
   }
   print_string("\n");
}


int main(int argc, char **argv) {
   int size = 10;
   testtype a[10];
   int i;

   for(i = 0; i < size; ++i) {
      a[i] = i;
   }
   print_list(a, size);
   f1(a, size);
   print_list(a, size);
   f2(a, size);
   print_list(a, size);

   return(0);
}
