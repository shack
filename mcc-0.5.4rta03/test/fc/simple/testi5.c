/* Test division by constants
   Justin David Smith */


#include "fc.h"

typedef int ty_int;

void check_num(char *msg, ty_int num, ty_int exp) {

   if(num != exp) {
      print_string(msg);
      print_string(" FAILED\n");
      print_string("got ");
      print_hex(num);
      print_string(", expected ");
      print_hex(exp);
      print_string("\n");
   }

}


int main(int argc, char **argv) {

   ty_int i1 = 1000;
   
   /* Test constant multiplication */
   print_string("checking division table\n");
   check_num("div1", i1 / 1, 1000);
   check_num("div2", i1 / 2, 500);
   check_num("div3", i1 / 3, 333);
   check_num("div4", i1 / 4, 250);
   check_num("div8", i1 / 8, 125);
   check_num("div16", i1 / 16, 62);
   check_num("div32", i1 / 32, 31);
   check_num("div-1", i1 / (-1), -1000);
   check_num("div-2", i1 / (-2), -500);
   check_num("div-3", i1 / (-3), -333);
   check_num("div-4", i1 / (-4), -250);
   check_num("div-8", i1 / (-8), -125);
   check_num("div-16", i1 / (-16), -62);
   check_num("div-32", i1 / (-32), -31);
   return 0;
}
