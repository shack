/* Primitive long test case
   Justin David Smith */

#include "fc.h"


int main(int argc, char **argv) {

   /* Basic testing */
   long l;
   long l1;
   l = 0xdeadbeefL;
   print_string_hex("int  = ", l);
   print_string("\nlong = ");
   print_long_hex(l);
   print_string_long_hex("\nlong = ", l);
   
   /* Shift tests */
   l = l << 16;
   print_string_long_hex("\n <<  = ", l);
   l = l >> 16;
   print_string_long_hex("\n >>  = ", l);
   l1 = l;
   print_string_long_hex("\ncopy = ", l1);
   l1 = l1 ^ l1;
   print_string_long_hex("\nxor  = ", l1);
   print_string("\n***\n");
   
   /* Tests with operators */
   long m = 0xffffffffL;
   print_string_long_hex("\ninitial = ", m);
   print_string_long_hex("\nadd 1   = ", ++m);
   print_string_long_hex("\nsub 1   = ", --m);
   print_string_long_hex("\nadd 1   = ", ++m);
   print_string_long_hex("\nsub 1   = ", --m);
   print_string("\n***\n");
   
   /* Constants */
   print_string_long_hex("\n0  = ", 0);
   print_string_long_hex("\n1  = ", 1);
   print_string_long_hex("\n-1 = ", -1);
   print_string("\n");
   
   /* Done */
   return(0);

}
