#include "fc.h"

/*
 * Fibonacci numbers.
 */
long fib(long i)
{
    long result;

    if(i == 0 || i == 1)
        result = i;
    else
        result = fib(i - 1) + fib(i - 2);
    return result;
}

/*
 * Factorial.
 */
long fact(long i)
{
    long k = 1;

    while(i != 0) {
        k *= i;
        i--;
    }
    return k;
}

/*
 * Main program takes the Fibonacci number as
 * the first argument.
 */
int main(int argc, char **argv)
{
    long i;

    /* Check usage */
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <number>\n");
        return 1;
    }

    /* Get the number */
    i = atoi(argv[1]);

    /* Print the results */
    print_string("fib(");
    print_long(i);
    print_string(") = ");
    print_long(fib(i));
    print_string("\n");

    print_string("fact(");
    print_long(i);
    print_string(") = ");
    print_long(fact(i));
    print_string("\n");
    return 0;
}
