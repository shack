/* Test multiplication by constants
   Justin David Smith */


#include "fc.h"

typedef int ty_int;

void check_num(char *msg, ty_int num, ty_int exp) {

   if(num != exp) {
      print_string(msg);
      print_string(" FAILED\n");
      print_string("got ");
      print_hex(num);
      print_string(", expected ");
      print_hex(exp);
      print_string("\n");
   }

}


int main(int argc, char **argv) {

   ty_int i1 = 100;
   
   /* Test constant multiplication */
   print_string("checking multiplication table\n");
   /* JDS 2002.07.20: signed IMUL exhibits bogus behaviour on mul by 9 
      (and 8, if I get the flags right), I'm trying to track the problem 
      with this revision of the test case. */
   /* JDS: It looks like the ``signed IMUL'' rule in x86_peephole_mul.kupo */
   check_num("mul8", i1 * 8, 800);
   check_num("mul9", i1 * 9, 900);
   return 0;
}
