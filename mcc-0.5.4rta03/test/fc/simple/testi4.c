/* Test multiplication by constants
   Justin David Smith */


#include "fc.h"

typedef int ty_int;

void check_num(char *msg, ty_int num, ty_int exp) {

   if(num != exp) {
      print_string(msg);
      print_string(" FAILED\n");
      print_string("got ");
      print_hex(num);
      print_string(", expected ");
      print_hex(exp);
      print_string("\n");
   }

}


int main(int argc, char **argv) {

   ty_int i1 = 100;
   
   /* Test constant multiplication */
   print_string("checking multiplication table\n");
   check_num("mul0", i1 * 0, 0);
   check_num("mul1", i1 * 1, 100);
   check_num("mul2", i1 * 2, 200);
   check_num("mul3", i1 * 3, 300);
   check_num("mul4", i1 * 4, 400);
   check_num("mul5", i1 * 5, 500);
   check_num("mul6", i1 * 6, 600);
   check_num("mul7", i1 * 7, 700);
   check_num("mul8", i1 * 8, 800);
   check_num("mul9", i1 * 9, 900);
   check_num("mul10", i1 * 10, 1000);
   check_num("mul11", i1 * 11, 1100);
   check_num("mul12", i1 * 12, 1200);
   check_num("mul13", i1 * 13, 1300);
   check_num("mul14", i1 * 14, 1400);
   check_num("mul15", i1 * 15, 1500);
   check_num("mul16", i1 * 16, 1600);
   check_num("mul17", i1 * 17, 1700);
   check_num("mul18", i1 * 18, 1800);
   check_num("mul19", i1 * 19, 1900);
   check_num("mul20", i1 * 20, 2000);
   check_num("mul21", i1 * 21, 2100);
   check_num("mul22", i1 * 22, 2200);
   check_num("mul23", i1 * 23, 2300);
   check_num("mul24", i1 * 24, 2400);
   check_num("mul25", i1 * 25, 2500);
   check_num("mul26", i1 * 26, 2600);
   check_num("mul27", i1 * 27, 2700);
   check_num("mul28", i1 * 28, 2800);
   check_num("mul29", i1 * 29, 2900);
   check_num("mul30", i1 * 30, 3000);
   check_num("mul31", i1 * 31, 3100);
   check_num("mul32", i1 * 32, 3200);
   return 0;
}
