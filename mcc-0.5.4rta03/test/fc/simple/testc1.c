/* Test internal representation of small values
   Justin David Smith */
   
   
#include "fc.h"


void print_value(char *msg, char c) {

   print_string(msg);
   print_string(" = ");
   print_int(c);
   print_string("\n");

}


int main(int argc, char **argv) {

   char c = 0;
   char d[1];
   char v;
   int i;
   int chk1;
   int chk2;
   
   /* Build c so internally it has overflowed a bit */
   for(i = 0; i < 258; ++i) ++c;
   v = c;
   
   /* Check that c is "2" */
   d[0] = v;
   print_value("c", c);
   print_value("d", d[0]);
   
   /* Now, divide by 2; c is "1" */
   c = v / 2;
   d[0] = c;
   print_value("c / 2 -> c", c);
   print_value("c / 2 -> d", d[0]);
   chk1 = c == 1;
   
   /* Same result forcing use of memory */
   d[0] = v;
   d[0] = d[0] / 2;
   c = d[0];
   print_value("d / 2 -> c", c);
   print_value("d / 2 -> d", d[0]);
   chk2 = c == 1;

   return(chk1 && chk2 ? 0 : 1);

}
