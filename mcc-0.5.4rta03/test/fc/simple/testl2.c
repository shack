#include "fc.h"

int main(int argc, char **argv)
{
    /* Check usage */
    if(argc != 2) {
        print_string("usage: ");
        print_string(argv[0]);
        print_string(" <number>\n");
        print_string("(hint: try the number 10)\n");
        return 1;
    }
    print_string("Program is okay unless bogus is printed\n");

    /* Get the number */
    long i = atoi(argv[1]);
    print_long_hex(i);
    print_string(", ");
    
    /* Basic long test here */
    long j = i;
    j = j * 0x10000L;
    j = j * 0x10000L;
    j = j >> 32;
    print_long_hex(j);
    if(j != i) print_string(" (BOGUS!)");
    print_string(", ");
    
    /* Attempt number 2 at long test */
    long k = i;
    k = k << 32;
    k = k / 0x10000L;
    k = k / 0x10000L;
    print_long_hex(k);
    if(k != i) print_string(" (BOGUS!)");
    print_string("\n");
    return 0;
}
