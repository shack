/* Test multiplication and division 
   Justin David Smith */

#include "fc.h"

typedef int ty_int;

int main(int argc, char **argv) {

   ty_int i1 = 1234;
   ty_int i2 = 5678;
   
   /* Make sure negation works */
   print_string("checking negation\n");
   if(-i1 != -1234)              print_string("negate FAILED\n");

   /* Test signed multiplication */
   print_string("checking multiplication\n");
   if(i1 * i2 != 7006652)        print_string("mul1 FAILED\n");
   if((-i1) * i2 != -7006652)    print_string("mul2 FAILED\n");
   if((-i1) * (-i2) != 7006652)  print_string("mul3 FAILED\n");

   /* Test signed division */
   print_string("checking division\n");
   if(i2 / i1 != 4)              print_string("div1 FAILED\n");
   if(i1 / i2 != 0)              print_string("div2 FAILED\n");
   if((-i2) / i1 != -4) {
      print_string("div3 FAILED: ");
      print_int((-i2) / i1);
      print_string(" [0x");
      print_hex((-i2) / i1);
      print_string("] (-4)\n");
   }
   if((-i2) / (-i1) != 4) {
      print_string("div4 FAILED: ");
      print_int((-i2) / (-i1));
      print_string(" [0x");
      print_hex((-i2) / (-i1));
      print_string("] (4)\n");
   }
   if(-(i2 / i1) != -4)          print_string("div5 FAILED\n");
   
   /* Test signed remainder */
   print_string("checking remainder\n");
   if(i1 % i2 != 1234)           print_string("rem1 FAILED\n");
   if((-i1) % i2 != -1234)       print_string("rem2 FAILED\n");
   if(i2 % i1 != 742)            print_string("rem3 FAILED\n");
   if(i2 % (-i1) != 742) {
      print_string("rem4 FAILED: \n");
      print_int(i2 % (-i1));
      print_string(" (742)\n");
   }
   return 0;
}
