/* Test mul/div int manipulations
   Justin David Smith */
   

#include "fc.h"


void print_num(long l) {

   print_long(l);
   print_string_long_hex(" (0x", l);
   print_string(")");

}


void error_long_result(char *msg, long v, long c) {

   print_string(msg);
   print_string(": expected ");
   print_num(c);
   print_string(", got ");
   print_num(v);
   print_string("\n");

}


void check_long_result(char *msg, long v, long c) {
   
   if(v != c) error_long_result(msg, v, c);
   
}


int main(int argc, char **argv) {

   print_string("Running program\n");
   long l1 = 100L;
   long l2 = 1000000000000L;
   
   /* Multiplication */
   print_string("Running multiplication test\n");
   check_long_result("mul1", 0L * l1, 0L);
   check_long_result("mul2", 1L * l2, l2);
   check_long_result("mul3", 2L * l2, 2000000000000L);
   check_long_result("mul4", -2L * l2, -2000000000000L);
   check_long_result("mul5", l1 * l2, 100000000000000L);
   check_long_result("mul6", (-l1) * l2, -100000000000000L);
   check_long_result("mul7", (-l1) * (-l2), 100000000000000L);

   /* Division */
   print_string("Running division test\n");
   check_long_result("div1", 0L / l1, 0L);
   check_long_result("div2", 1L / l2, 0L);
   check_long_result("div3", l2 / l1, 10000000000L);
   check_long_result("div4", (-l2) / l1, -10000000000L);
   check_long_result("div5", l1 / 2L, 50L);
   check_long_result("div6", l1 / 3L, 33L);
   check_long_result("div7", l1 / -3L, -33L);
   return 0;
}
