#include <fc.h>


int main(int argc, char **argv) {

   int x = 32;
   int *y = &x;
   print_int(x);
   print_string("\n");
   *y = 64;
   print_int(x);
   print_string("\n");

}
