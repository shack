/* Argument passing segfault trap
   Justin David Smith */
   
   
#include "fc.h"


void bar(char *msg, long l, int i) {

   print_string("(bar) ");
   print_string(msg);
   print_string(": ");
   print_long(l);
   print_string(", ");
   print_int(i);
   print_string("\n");
}
   
   
void foo(char *msg, long l, int i) {

   print_string("(foo) ");
   print_string(msg);
   print_string(": ");
   print_long(l);
   print_string(", ");
   print_int(i);
   print_string("\n");
   bar(msg, l, i);

}
 

int main(int argc, char **argv) {

   int i;
   foo("0", 0, 0);
   foo("-1", -1, -1);
   foo("1", 1, 1);
   for(i = 0; i < 10; ++i) {
      foo("x", i, i);
   }
   return 0;
}
