#include <fc.h>
#include "itest_vals.h"


int main(int argc, char **argv) {

   unsigned char ctmp;
   unsigned short stmp;
   unsigned int itmp;

   fpitest_msg("char equality");
   fpitest_check_true ("eq1a", c00 == c00);
   fpitest_check_true ("eq1b", c7F == c7F);
   fpitest_check_true ("eq1c", c80 == c80);
   fpitest_check_true ("eq1d", cFF == cFF);
   fpitest_check_true ("eq1e", uc00 == c00);
   fpitest_check_true ("eq1f", uc7F == c7F);
   /* For the next two, by promoting we have signed/unsigned 
      discrepancy; therefore the comparison must be false. */
   fpitest_check_false("eq1g", uc80 == c80);
   fpitest_check_false("eq1h", ucFF == cFF);
   fpitest_check_true ("eq1i", uc00 == 0x00);
   fpitest_check_true ("eq1j", uc7F == 0x7F);
   fpitest_check_true ("eq1k", uc80 == 0x80);
   fpitest_check_true ("eq1l", ucFF == 0xFF);
   fpitest_check_false("eq1m", c00 == c7F);
   fpitest_check_false("eq1n", c00 == c80);
   fpitest_check_false("eq1o", c00 == cFF);
   fpitest_check_false("eq1p", ucFF == c7F);

   /* The next few cases ensure that masking is done properly on an 
      internal character comparison (remember: they are 32-bit internal) */
   ctmp = ucFF;
   itmp = ucFF;
   ctmp += 0x80;
   itmp += 0x80;
   fpitest_check_true ("eq2a", ctmp == 0x7F);
   fpitest_check_true ("eq2b", itmp == 0x17F);
   fpitest_check_true ("eq2c", (unsigned char)itmp == 0x7F);
   fpitest_check_false("eq2d", ctmp == 0x17F);
   fpitest_check_false("eq2e", itmp == 0x7F);
   fpitest_check_false("eq2f", (unsigned char)itmp == 0x17F);
   
   fpitest_msg("short equality");
   fpitest_check_true ("eq3a", s0000 == s0000);
   fpitest_check_true ("eq3b", s7FFF == s7FFF);
   fpitest_check_true ("eq3c", s8000 == s8000);
   fpitest_check_true ("eq3d", sFFFF == sFFFF);
   fpitest_check_true ("eq3e", us0000 == s0000);
   fpitest_check_true ("eq3f", us7FFF == s7FFF);
   /* For the next two, by promoting we have signed/unsigned 
      discrepancy; therefore the comparison must be false. */
   fpitest_check_false("eq3g", us8000 == s8000);
   fpitest_check_false("eq3h", usFFFF == sFFFF);
   fpitest_check_true ("eq3i", us0000 == 0x0000);
   fpitest_check_true ("eq3j", us7FFF == 0x7FFF);
   fpitest_check_true ("eq3k", us8000 == 0x8000);
   fpitest_check_true ("eq3l", usFFFF == 0xFFFF);
   fpitest_check_false("eq3m", s0000 == s7FFF);
   fpitest_check_false("eq3n", s0000 == s8000);
   fpitest_check_false("eq3o", s0000 == sFFFF);
   fpitest_check_false("eq3p", usFFFF == s7FFF);

   /* The next few cases ensure that masking is done properly on an 
      internal character comparison (remember: they are 32-bit internal) */
   stmp = usFFFF;
   itmp = usFFFF;
   stmp += 0x8000;
   itmp += 0x8000;
   fpitest_check_true ("eq4a", stmp == 0x7FFF);
   fpitest_check_true ("eq4b", itmp == 0x17FFF);
   fpitest_check_true ("eq4c", (unsigned short)itmp == 0x7FFF);
   fpitest_check_false("eq4d", stmp == 0x17FFF);
   fpitest_check_false("eq4e", itmp == 0x7FFF);
   fpitest_check_false("eq4f", (unsigned short)itmp == 0x17FFF);
   
   fpitest_msg("int equality");
   fpitest_check_true ("eq5a", i00000000 == i00000000);
   fpitest_check_true ("eq5b", i7FFFFFFF == i7FFFFFFF);
   fpitest_check_true ("eq5c", i80000000 == i80000000);
   fpitest_check_true ("eq5d", iFFFFFFFF == iFFFFFFFF);
   fpitest_check_true ("eq5e", ui00000000 == i00000000);
   fpitest_check_true ("eq5f", ui7FFFFFFF == i7FFFFFFF);
   fpitest_check_true ("eq5g", ui80000000 == i80000000);
   fpitest_check_true ("eq5h", uiFFFFFFFF == iFFFFFFFF);
   fpitest_check_true ("eq5i", ui00000000 == 0x00000000U);
   fpitest_check_true ("eq5j", ui7FFFFFFF == 0x7FFFFFFFU);
   fpitest_check_true ("eq5k", ui80000000 == 0x80000000U);
   fpitest_check_true ("eq5l", uiFFFFFFFF == 0xFFFFFFFFU);
   fpitest_check_false("eq5m", i00000000 == i7FFFFFFF);
   fpitest_check_false("eq5n", i00000000 == i80000000);
   fpitest_check_false("eq5o", i00000000 == iFFFFFFFF);
   fpitest_check_false("eq5p", uiFFFFFFFF == i7FFFFFFF);

   fpitest_msg("long equality");
   fpitest_check_true ("eq6a", l00000000 == l00000000);
   fpitest_check_true ("eq6b", l7FFFFFFF == l7FFFFFFF);
   fpitest_check_true ("eq6c", l80000000 == l80000000);
   fpitest_check_true ("eq6d", lFFFFFFFF == lFFFFFFFF);
   fpitest_check_true ("eq6e", ul00000000 == l00000000);
   fpitest_check_true ("eq6f", ul7FFFFFFF == l7FFFFFFF);
   fpitest_check_true ("eq6g", ul80000000 == l80000000);
   fpitest_check_true ("eq6h", ulFFFFFFFF == lFFFFFFFF);
   fpitest_check_true ("eq6i", ul00000000 == 0x0000000000000000ULL);
   fpitest_check_true ("eq6j", ul7FFFFFFF == 0x7FFFFFFFFFFFFFFFULL);
   fpitest_check_true ("eq6k", ul80000000 == 0x8000000000000000ULL);
   fpitest_check_true ("eq6l", ulFFFFFFFF == 0xFFFFFFFFFFFFFFFFULL);
   fpitest_check_false("eq6m", l00000000 == l7FFFFFFF);
   fpitest_check_false("eq6n", l00000000 == l80000000);
   fpitest_check_false("eq6o", l00000000 == lFFFFFFFF);
   fpitest_check_false("eq6p", ulFFFFFFFF == l7FFFFFFF);

   fpitest_msg("done");
   return(0);

}
