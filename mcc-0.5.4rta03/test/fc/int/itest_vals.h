#ifndef __itest_vals_included
#define __itest_vals_included


#include <fc.h>


static signed char c[4] = { '\0', '\x7F', '\x80', '\xFF' };
#define  c00   c[0]
#define  c7F   c[1]
#define  c80   c[2]
#define  cFF   c[3]

static unsigned char uc[4] = { '\0', '\x7F', '\x80', '\xFF' };
#define  uc00  uc[0]
#define  uc7F  uc[1]
#define  uc80  uc[2]
#define  ucFF  uc[3]


static signed short s[4] = { 0x0000, 0x7FFF, 0x8000, 0xFFFF };
#define  s0000    s[0]
#define  s7FFF    s[1]
#define  s8000    s[2]
#define  sFFFF    s[3]

static unsigned short us[4] = { 0x0000, 0x7FFF, 0x8000, 0xFFFF };
#define  us0000   us[0]
#define  us7FFF   us[1]
#define  us8000   us[2]
#define  usFFFF   us[3]


static signed int i[4] = { 0x00000000, 0x7FFFFFFF, 0x80000000, 0xFFFFFFFF };
#define  i00000000   i[0]
#define  i7FFFFFFF   i[1]
#define  i80000000   i[2]
#define  iFFFFFFFF   i[3]

static unsigned int ui[4] = { 0x00000000, 0x7FFFFFFF, 0x80000000, 0xFFFFFFFF };
#define  ui00000000  ui[0]
#define  ui7FFFFFFF  ui[1]
#define  ui80000000  ui[2]
#define  uiFFFFFFFF  ui[3]


static fc_int64_t l[4] = { 0x0000000000000000LL, 
                           0x7FFFFFFFFFFFFFFFLL, 
                           0x8000000000000000LL, 
                           0xFFFFFFFFFFFFFFFFLL };
#define  l00000000   l[0]
#define  l7FFFFFFF   l[1]
#define  l80000000   l[2]
#define  lFFFFFFFF   l[3]

static fc_uint64_t ul[4] = { 0x0000000000000000ULL,
                             0x7FFFFFFFFFFFFFFFULL, 
                             0x8000000000000000ULL, 
                             0xFFFFFFFFFFFFFFFFULL };
#define  ul00000000  ul[0]
#define  ul7FFFFFFF  ul[1]
#define  ul80000000  ul[2]
#define  ulFFFFFFFF  ul[3]


#endif /* __itest_vals_included? */
