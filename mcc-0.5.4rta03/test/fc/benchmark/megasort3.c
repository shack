/* Megasort
   Abuse memory by doing a bubblesort
   NOTE: this case, as written does very little allocation.
         If you modify this to do mutual function calls or
         recursion, then put the modifications in another
         file; this is a great test specifically because the
         GC is out of the way!
   Justin David Smith */
   
   
#include "fc.h"


extern void srand(unsigned int);
extern int  rand();


#define  SIZE     (1 << 14)
#define  MUNGE    1478971
#define  PRINT    0


int bubblesort(int *a, int len) {

   int i;
   int j;

   for(i = 0; i < len; ++i) {
      for(j = 0; j < i; ++j) {
         if(a[i] < a[j]) {
            int tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
         }
      }
   }

}


void assert_ordered(int *a, int len) {

   int i;

   for(i = 1; i < len; ++i) {
      if(a[i - 1] > a[i]) {  
         print_string("FAIL:  Elements ");
         print_int(i - 1);
         print_string(", ");
         print_int(i);
         print_string(" are unordered!\n");
      }
   }   

}


/* Test program. */
int main(int argc, char **argv) {

   int *a;
   int i;
    
   srand(0xCAFEBABE);
   
   a = (int *)malloc(sizeof(int) * SIZE);
   
   /* Initialise to random values */
   for(i = 0; i < SIZE; ++i) {
      a[i] = rand();
   }

   bubblesort(a, SIZE);

   assert_ordered(a, SIZE);

   #if PRINT
      for(i = 0; i < SIZE; i++) {
         print_string("a[");
         print_int(i);
         print_string("]=");
         print_int(a[i]);
         print_string("\n");
      }
   #endif /* PRINT? */

   return(0);

}
