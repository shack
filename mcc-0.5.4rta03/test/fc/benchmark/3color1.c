/* Graph 3-colouring

   The ``u'' stands for ``you bloody Americans misspell everything...''  :)

   Justin David Smith */


#ifdef __mcc
#include "stdarg.h"
#include "fc.h"

/*
 * We should probably define a directory for
 * fc-specific header files, and search it before the
 * standard includes.  This declaration would go in malloc.h
 */
static void *malloc(int) = "malloc";
extern void srand(unsigned int);
extern int  rand();
#endif

#include <stdio.h>


#define  DEFAULT_SIZE      10
#define  DEFAULT_DENSITY   50
   
   
/* Simple graph datastructure that uses a matrix to mark
   which nodes have edges between them. */
typedef struct _graph {
   int nodes;           /* Number of nodes in the graph */
   int *edges;          /* List of neighbors for each node */
   int *colours;        /* Colours of the nodes */
} graph;


/* Read and modify the edge set */
static int get_edge(graph *g, int src, int dst) {

   return(g->edges[src * g->nodes + dst]);

}


static void set_edge(graph *g, int src, int dst, int set) {

   g->edges[src * g->nodes + dst] = set;

}


/* Create a new undirected graph.  Density is a value between
   0 and 100 that determines how complete the graph is (100
   is a complete graph, 0 is fully unconnected). */
static void init_graph(graph *g, int size, int density) {

   /* Matrix is symmetric, hence the divide by 200 */
   int numedges = density * size * size / 200;
   int i, src, dst;

   /* Initialize graph structure */
   g->nodes = size;
   g->edges = (int *)malloc(size * size * sizeof(int));
   g->colours = (int *)malloc(size * sizeof(int));
   for(i = 0; i < size * size; ++i) {
      g->edges[i] = 0;
   }
   for(i = 0; i < size; ++i) {
      g->colours[i] = 0;
   }

   /* Try to setup new edges */   
   for(i = 0; i < numedges; ++i) {
      while(1) {
         src = rand() % size;
         dst = rand() % size;
         if(!get_edge(g, src, dst)) {
            set_edge(g, src, dst, 1);
            set_edge(g, dst, src, 1);
            break;
         }
      }
   }

}


/* Print out the graph's connectivity */
static void print_graph(graph *g) {

   int src, dst;

   printf("nodes: %d\n", g->nodes);

   /* Print the edge matrix */
   for(src = 0; src < g->nodes; ++src) {
      printf(src == 0 ? "[[" : " [");
      for(dst = 0; dst < g->nodes; ++dst) {
         printf(dst == 0 ? "[" : " ");
         printf("%d", get_edge(g, src, dst));
      }
      printf("]\n");
   }

}


/* Print the colouring for the graph */
static void print_colouring(graph *g) {

   int i;
   
   printf("[");
   for(i = 0; i < g->nodes; ++i) {
      printf("%d", g->colours[i]);
   }
   printf("]\n");
   
}


int main(int argc, char **argv) {

   graph g;
   int result;
   
   /* Get the random seed */
   if(argc > 1) {
      srand(atoi(argv[1]));
   } else {
      srand(42);
   }

   /* Initialize the graph */   
   init_graph(&g, DEFAULT_SIZE, DEFAULT_DENSITY);
   print_graph(&g);
   
   return(0);

}
