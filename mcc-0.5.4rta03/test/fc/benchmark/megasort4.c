/* Megasort
   Abuse memory by doing a bubblesort
   NOTE: this case, as written does very little allocation.
         If you modify this to do mutual function calls or
         recursion, then put the modifications in another
         file; this is a great test specifically because the
         GC is out of the way!
   Justin David Smith */
   
   
#ifdef __mcc
#include "stdarg.h"

/*
 * We should probably define a directory for
 * fc-specific header files, and search it before the
 * standard includes.  This declaration would go in malloc.h
 */
static void *malloc(int) = "malloc";
#endif

#include <stdio.h>

extern int abort();


/* SIZE is the total size of the array to sort */
#define  SIZE     (1 << 14)

/* Define PRINT to see the result of the sort */
#undef PRINT


int bubblesort(int *a, int len)
{
    int i, j;

    /*
     * A pedantic note: it is best to use != when iterating over
     * constant ranges.  That way, if you accidentally get the range
     * wrong, the program will iterate for a _long_ time, hopefully
     * an infinite loop, and you are more likely to catch the
     * error.  For variable ranges, it is sometimes necessary to use <.
     *
     * Anti-pedant's note: the backend is slightly friendlier toward != 
     * than < -- you alter the running characteristics with this change
     */
    for(i = 0; i != len; i++) {
        for(j = 0; j != i; j++) {
            if(a[i] < a[j]) {
                int tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
            }
        }
    }
}


void assert_ordered(int *a, int len)
{
    int i;

    for(i = 1; i != SIZE; i++) {
        /*
         * BUG: somehow this doesn't work with -O2,
         * and jyh thinks its because of FIR optimizations.
         */
        if(a[i - 1] > a[i]) {
            printf("FAIL: Elements a[%d] = %d, a[%d] = %d are unordered\n", i - 1, a[i - 1], i, a[i]);
            abort();
        }
    }   
}


/* Test program. */
int main(int argc, char **argv)
{
    int *a;
    int i;
    
    a = (int *) malloc(sizeof(int) * SIZE);
   
    /* Initialize to reverse-order values */
    for(i = 0; i != SIZE; i++) {
        a[i] = -i;
    }

    bubblesort(a, SIZE);

    assert_ordered(a, SIZE);

#ifdef PRINT
    for(i = 0; i != SIZE; i++) {
        printf("a[%d] = %d\n", i, a[i]);
    }
#endif

    return 0;
}
