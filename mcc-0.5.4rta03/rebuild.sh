#!/bin/sh
# This script is intended to be used by developers only.
# In fact, it's pretty much tailored for Justin :)


autoconf
rm -f config.cache config.log
if [ -d /usr/local/binutils ]; then
   conf_options="--prefix=/home/justins/tmp/mcc-target --with-libbfd=/usr/local/binutils"
else
   conf_options="--prefix=/home/justins/tmp/mcc-target"
fi
./configure $conf_options "$@"
