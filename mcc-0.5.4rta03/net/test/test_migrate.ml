(*
   Test the migration server
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Format
open Unix
open Fir
open Fir_marshal
open Fir_exn
open Migrate_util

let client file host port =
   let hostinfo = gethostbyname host in
   let sock_client = socket PF_INET SOCK_STREAM 0 in
   let addr_server = ADDR_INET(hostinfo.h_addr_list.(0), port) in
   let _ = connect sock_client addr_server in
   printf "Connected to server at %a\n" print_address addr_server;
   flush_stdout ();

   (* Send the version/protocol string *)
   let inc = in_channel_of_descr sock_client in
   let out = out_channel_of_descr sock_client in
   print_message addr_server "Sending version string";
   output_endline out program_version_string;
   print_message addr_server "Sent version string, waiting for reply";
   check_result inc;

   (* Send the entire program *)
   let in' = open_in file in
   let prog = ChannelMarshal.unmarshal_prog in' in
   let _ = close_in in' in
   print_message addr_server "Sending program data";
   ChannelMarshal.marshal_prog out prog;
   flush out;
   print_message addr_server "Sent program data, waiting for reply";
   check_result inc;

   (* We're done *)
   printf "Successfully sent program %s to server at %a\n" file print_address addr_server;
   flush_stdout ();
   shutdown sock_client SHUTDOWN_ALL


let file = if Array.length Sys.argv < 2
              then raise (Failure "Too few arguments to command")
              else Sys.argv.(1)

let host = if Array.length Sys.argv < 3
              then default_server_name
              else Sys.argv.(2)

let port = if Array.length Sys.argv < 4
              then default_server_port
              else int_of_string Sys.argv.(3)

let client () = client file host port

let _ = handle_unix_error client ()
