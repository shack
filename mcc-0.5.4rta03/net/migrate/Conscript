#  net/migrate Conscript file
#  Copyright(c) 2002 Justin David Smith, Caltech


# Import the default environment
Import qw( env );


# Figure out what the various libraries are that we'll need.
my $mllibs = "unix$env->{SUFLIB}";

# Figure out what C libraries we're going to need to link against.
my $libcutil = "$env->{LIBMOJAVE}/cutil/libcutil.a";
my $libmlbfd = "$env->{MC}/arch/bfd/cutil/libmlbfd.a";
my $cclibs = "-cclib $libcutil";
if ($env->{BFD_ENABLED}) {
   $cclibs = "$cclibs -cclib $libmlbfd $env->{BFD_LDFLAGS}";
}

# Construct a new environment suitable for building migration programs.
our $env = $env->clone(
    MLLIBS  => $mllibs,
    CCLIBS  => $cclibs,
);


# Declare the source files and build targets for migration.
OCamlSources $env
	'migrate.ml',
	'migrate_sig.ml',
	'migrate_util.ml',
	'migrated_main.ml',
	'resurrect_main.ml';

OCamlLibrary $env 'migrate',
	'migrate',
	'migrate_sig',
	'migrate_util';

OCamlProgram $env 'mcc-migrated', 'migrated_main';
OCamlProgram $env 'mcc-resurrect', 'resurrect_main';


# Declare all required dependencies.
Depends $env 'mcc-migrated',  $libcutil;
Depends $env 'mcc-resurrect', $libcutil;
         

# If BFD is enabled, setup a dependency on the BFD library.
if ($env->{BFD_ENABLED}) {
   Depends $env 'mcc-migrated',  $libmlbfd;
   Depends $env 'mcc-resurrect', $libmlbfd;
}


# If a runtime library is enabled, setup a dependency on the appropriate
# runtime library.  This is artificial; we don't need the runtime library
# to compile mcc, but mcc will need it to run properly.
if ($env->{RUNTIME_ENABLED}) {
   Depends $env 'mcc-migrated',  "$env->{MC}/arch/$env->{BACKEND_NAME}/runtime/$env->{BACKEND_NAME}runtime.a";
   Depends $env 'mcc-migrated',  "$env->{MC}/arch/$env->{BACKEND_NAME}/runtime/$env->{BACKEND_NAME}runtimegcc.a";
   Depends $env 'mcc-resurrect', "$env->{MC}/arch/$env->{BACKEND_NAME}/runtime/$env->{BACKEND_NAME}runtime.a";
   Depends $env 'mcc-resurrect', "$env->{MC}/arch/$env->{BACKEND_NAME}/runtime/$env->{BACKEND_NAME}runtimegcc.a";
}


# Add the migrate programs to the default target list
Default qw( mcc-migrated
            mcc-resurrect );
