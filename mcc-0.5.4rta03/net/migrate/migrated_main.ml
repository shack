(*
   Run a migration server for configured backend
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


open Migrate_util
open Migrate
open Mcc_config


module Migrate = Migrate (ArchBackend) (ArchCodegen)


(*
 * Configurable options
 *)
let port = ref default_server_port


(*
 * Arguments
 *)
let usage = "MCC (Mojave Compiler) version " ^ version_string ^ "\n" ^
            "Migration server, protocol version " ^ program_version_string ^ "\n\n" ^
            "Usage: mcc-migrated [options]"

let default_port = string_of_int default_server_port

let spec =
  ["Basic Options",
     ["-port",    Mc_arg.Int (fun i -> port := i),
                  "local port to run server on (default " ^ default_port ^ ")"]] @ debug_spec


let invalid_option opt =
   Printf.eprintf "Bogus option specified: %s\n" opt;
   exit 1


(*
 * Run the server
 *)
let _ =
   Sys.catch_break true;
   Mc_arg.parse spec invalid_option usage;
   Migrate.server !port
