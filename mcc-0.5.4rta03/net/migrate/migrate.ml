(*
   Run a migration server
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
 
 
(* 
   Uses the printf module because of certain requirements in C code.

      DO NOT REPLACE THIS WITH FORMAT.

   There will be problems with bad streams if you use Format here.
   Contact JDS for details.
 *)
open Printf


(* Useful modules *)
open Unix
open Fir
open Fir_marshal
open Fir_exn
open Fir_pos
open Fir_exn_print
open Migrate_util
open Frame_type
open Codegen_type
open Mcc_version
open Mcc_paths


(* Try to find out where the hell we're installed. *)
let mcc_paths = get_mcc_paths ()


(***  Debugging  ***)


(* If set, the binary version of the FIR that is received will be
   saved to a file.  This option is independent of the migrate_obj
   option, below. *)
let debug_emit_fir      =  ref false


(* If set, preserve the outputted object file for migrated programs.
   If false, then object files are deleted once they are no longer
   needed.  Note that executable programs are always unlinked. *)
let debug_migrate_obj   =  ref false


(* If set, migrated programs won't be executed.  You usually want
   this cleared to false, for reasons which should be obvious. :) *)
let debug_no_exec       =  ref false


(* If set, then GDB will be hooked onto the migrated program once
   it is run.  This is a very fickle feature, bug justins for more
   details if you need to debug a program across migration boundary.
   You usually want to keep this cleared to false. *)
let debug_gdb_hooks     =  ref false


(* Debug specification used for migrate toplevel programs. *)
let debug_spec =
  ["Information on compiler/installation",
     ["-version",          Mc_arg.Unit display_version_info,
                           "display version information";
      "-print-paths",      Mc_arg.Unit (fun () -> print_mcc_paths mcc_paths),
                           "print all configured paths"];

   "Debugging flags",
     ["-print-fir",        Mc_arg.Unit (fun _ -> incr Fir_state.debug_print_fir),
                           "print fir code (use twice to print more stages)";
      "-print-mir",        Mc_arg.Set Mir_state.debug_print_mir,
                           "print MIR code";
      "-print-final-mir",  Mc_arg.Set Mir_state.debug_print_final_mir,
                           "print final stage of MIR code only";
      "-print-asm",        Mc_arg.Set Fir_state.debug_print_asm,
                           "print assembly code";
      "-debug-profile",    Mc_arg.Set Fir_state.debug_profile,
                           "enable profiling of the compiler";
      "-debug-pos",        Mc_arg.Set Fir_state.debug_pos,
                           "print position information in exceptions";
      "-debug-infer",      Mc_arg.Set Fir_state.debug_infer,
                           "debug FIR type inference";
      "-debug-unify",      Mc_arg.Set Fir_state.debug_unify,
                           "debug FIR type unification"];

   "Migrate debugging flags",
     ["-migrate-emit-fir", Mc_arg.Set debug_emit_fir,
                           "emit received FIR to a file";
      "-migrate-save-obj", Mc_arg.Set debug_migrate_obj,
                           "preserve emitted compiled object file";
      "-migrate-no-exec",  Mc_arg.Set debug_no_exec,
                           "do not execute received programs";
      "-migrate-gdb",      Mc_arg.Set debug_gdb_hooks,
                           "enable GDB hooks (use with caution!)"]]


module Migrate (Backend : BackendSig) (Codegen : CodegenSig) =
struct
   (* Basic structures *)
   let link_command = mcc_paths.path_compiler
   type exec_state = NoExec
                   | Exec of string
   type migrate_type = MigrateProcess
                     | MigrateProgram


   (* Direction of the channel? *)
   let unidirectional_channel = false
   let bidirectional_channel = true


   (* establish_protocol
      Sets up the initial protocols with the client.  This procedure
      will raise Failure if an error or protocol mismatch occurs.  *)
   let establish_protocol addr inc out =
      print_message addr "Waiting for protocol version data";
      let s = input_line inc in
      if s = program_version_string then begin
         (* Receiving a binary FIR program *)
         send_ack_message addr out "Program protocol accepted";
         MigrateProgram
      end else if s = process_version_string then begin
         (* Receiving a process (FIR + data) *)
         send_ack_message addr out "Process protocol accepted";
         MigrateProcess
      end else begin
         (* Version strings do not match *)
         raise (Failure ("Protocol version mismatch:  I don't understand \"" ^ s ^ "\""))
      end


   (* input_reserve_params
      Read in the reserve parametres required for pointer table
      size and the total size of the heap, so we know how much
      to request in the initial allocation.  *)
   let input_reserve_params addr inc =
      let ptr_size    = input_int32 inc in
      let heap_size   = input_int32 inc in
         print_message addr (sprintf "reserve ptr size = %s, heap size = %s"
                             (Int32.format "%d" ptr_size)
                             (Int32.format "%d" heap_size));
         ptr_size, heap_size


   (* assemble_prog
      Attempt to assemble the assembly program in prog to a binary,
      executable program by invoking the normal GCC commands.  On
      error, Failure will be raised; in all cases, the output asm
      file will be deleted but the binary will remain.  On success
      the name of the binary is returned.  *)
   let assemble_prog addr prog =
      let outobj = Filename.temp_file "migrate" ".mo" in
      let outexe = Filename.temp_file "migrate" ".exec" in
      let compile = link_command ^ " -o \"" ^ outexe ^ "\" \"" ^ outobj ^ "\"" in
         print_message addr ("Emitting object file " ^ outobj);
         Codegen.print_object outobj prog;
         print_message addr ("Attempting to compile with command: " ^ compile);
         if system compile <> (WEXITED 0) then begin
            (* Compilation failed *)
            if not !debug_migrate_obj then begin
               print_message addr ("Unlinking object file " ^ outobj);
               unlink outobj
            end;
            raise (Failure ("Failed to compile " ^ outobj ^ " to " ^ outexe))
         end;
         if !debug_migrate_obj then
            print_message addr ("Object file written to " ^ outobj)
         else begin
            print_message addr ("Unlinking object file " ^ outobj);
            unlink outobj
         end;
         outexe


   (* build_prog
      Build the program from FIR to object file
      (then eventually the executable itself).  *)
   let build_prog addr out prog migrate =
      print_message addr "Received program, attempting to compile";
      try
         let prog = Codegen.compile_asm_of_fir prog migrate in
         let prog = assemble_prog addr prog in
            send_ack_message addr out "Received and executing program";
            Exec prog
      with
         _ as e ->
            eprintf "An exception occurred during compile, dumping state.\n";
            Flags.print_std_flag_values ();
            raise e


   (* run_program
      Attempt to run the program we received.  Returns the result code from
      running the program; None is returned if something went wrong or the
      program was not executed, otherwise Some process_state is returned. *)
   let run_program descr addr = function
      NoExec ->
         None
    | Exec prog ->
         (* Execute the program *)
         if not !debug_no_exec then begin
            print_message addr "Executing program";
            let pid = getpid () in
            let child = fork () in
            let exitcode =
               if child = 0 then begin
                  (* Run the program *)
                  execv prog [|prog|];
                  (* We should never reach this point. *)
                  None
               end else begin
                  (* Wait for program to finish, then unlink *)
                  close descr;
                  if !debug_gdb_hooks then
                     ignore (system (sprintf "/usr/bin/gdb %s %d" prog child));
                  let _, exitcode = waitpid [] child in
                  unlink prog;
                  (match exitcode with
                     WEXITED i   -> print_message addr (sprintf "Program exited with code %d" i)
                   | WSIGNALED i -> print_message addr (sprintf "Program was killed with signal %d" i)
                   | WSTOPPED i  -> print_message addr (sprintf "Program was stopped with signal %d" i));
                  Some exitcode
               end
            in
               print_message addr "Program appears to have exited";
               exitcode
         end else begin
            print_message addr "Not executing program (debugging no_exec)";
            None
         end


   (* emit_binary_fir
      Emits a binary representation of the received FIR code. *)
   let emit_binary_fir addr prog =
      if !debug_emit_fir then begin
         let outfir = Filename.temp_file "migrate" ".fir" in
         print_message addr ("Saving binary FIR to file " ^ outfir);
         let out = open_out_bin outfir in
            ChannelMarshal.marshal_prog out prog;
            close_out out
      end


   (* receive_program
      Receive program either from a network connection, or from a
      file on the disk.  If the latter, out should be an undefined
      channel (None).  The descriptor contains the information
      needed for the migrate data block that includes the file
      descriptor and what type of channel (uni/bidirectional). *)
   let receive_program descriptor addr inc out =
      let build_prog = build_prog addr out in
      let debug_flags = Int32.zero in
      let debug_flags =
         if !debug_gdb_hooks then
            Int32.logor debug_flags Backend.migrate_debug_gdbhooks
         else
            debug_flags
      in
      let descr, bidir = descriptor in
      let prog =
         try
            match establish_protocol addr inc out with
               MigrateProgram ->
                  print_message addr "Waiting for program data";
                  let prog = ChannelMarshal.unmarshal_prog inc in
                  let _ = emit_binary_fir addr prog in
                  print_message addr "Received program, attempting to compile";
                     build_prog prog None
             | MigrateProcess ->
                  (* Get the pre-compile parametres *)
                  (* Read the program data *)
                  let _    = print_message addr "Waiting for program data" in
                  let prog = ChannelMarshal.unmarshal_prog inc in
                  let _ = emit_binary_fir addr prog in
                  let _    = send_ack_message addr out "Received program" in

                  (* Read the migration key (indicates which call to migrate to use) *)
                  let _   = print_message addr "Waiting for migrate key" in
                  let key = input_line inc in
                  let _   = send_ack_message addr out "Received migrate key" in

                  (* Read the pointer block index for the register data block *)
                  let _    = print_message addr "Waiting for register block index" in
                  let regs = input_int32 inc in
                  let _    = send_ack_message addr out "Received register block index" in

                  (* Read in the data for configuring pointer table & heap *)
                  let _ = print_message addr "Waiting for pointer table/data commands" in
                  let ptr_size, heap_size = input_reserve_params addr inc in
                  let _ = send_ack_message addr out "Received reserve parametres, compiling now" in

                  (* Setup migrate data structure *)
                  let initofs = pos_in inc in
                  let descr = Obj.magic descr in   (* warning: assumes file_descr is an int *)
                  let migrate = { mig_key       = key;
                                  mig_channel   = Int32.of_int descr;
                                  mig_bidir     = bidir;
                                  mig_initofs   = Int32.of_int initofs;
                                  mig_registers = regs;
                                  mig_ptr_size  = ptr_size;
                                  mig_heap_size = heap_size;
                                  mig_debug     = debug_flags
                                } in
                     build_prog prog (Some migrate)
         with
            FirException _ as e ->
               Format.printf "%a@." pp_print_exn e;
               send_nak_message addr out "Received an FIR exception while compiling";
               NoExec
          | Failure s ->
               send_nak_message addr out s;
               NoExec
          | End_of_file ->
               (* This happens if remote terminates the socket *)
               print_message addr "Premature EOF on channel";
               NoExec
      in
         run_program descr addr prog


   (* client_connection
      Process a client connection.  *)
   let client_connection sock addr =
      let descriptor = sock, bidirectional_channel in
      let inc = in_channel_of_descr sock in
      let out = out_channel_of_descr sock in
      let _ = receive_program descriptor addr inc (Some out) in
         ()


   (* read_state_file
      Reads a program from a named state file.  Returns the result
      code of the program (assuming we didn't execv() the process).
      If something went wrong, we return 127.  *)
   let read_state_file filename =
      try
         let addr = ADDR_UNIX filename in
         let descr = openfile filename [O_RDONLY] 0 in
         let inc = in_channel_of_descr descr in
         let descriptor = descr, unidirectional_channel in
         let exitcode = receive_program descriptor addr inc None in
            match exitcode with
               None
             | Some (WSIGNALED _)
             | Some (WSTOPPED _) ->
                  127
             | Some (WEXITED i) ->
                  i
      with
         Unix_error (e, s1, s2) ->
            print_unix_error e s1 s2;
            127


   (* server
      Run the server.  *)
   let server port =
      let sock_server = socket PF_INET SOCK_STREAM 0 in
      let addr_server = ADDR_INET(inet_addr_any, port) in
      let _ = bind sock_server addr_server in
      let _ = listen sock_server 5 in
      eprintf "Server listening on port %d\n" port;
      flush_stderr ();
      let rec wait_and_accept () =
         try
            let sock_client, addr_client = accept sock_server in
            eprintf "Client connection from %a\n" print_address addr_client;
            flush_stderr ();
            if fork () = 0 then begin
               (* In child process, handle client *)
               (try
                  close sock_server;
                  client_connection sock_client addr_client;
               with
                  Unix_error(e, s1, s2) ->
                     print_unix_error e s1 s2
                | Sys.Break ->
                     eprintf "Terminating client session due to system break\n");
               exit 0
            end else begin
               (* In parent process, resume server *)
               close sock_client;
               wait_and_accept ()
            end
         with
            Unix_error (e, s1, s2) ->
               print_unix_error e s1 s2;
               wait_and_accept ()
      in
         wait_and_accept ()

   let server port = 
      try
         handle_unix_error server port
      with
         Sys.Break ->
            eprintf "Terminating server due to system break\n"


end (* Migrate module *)
