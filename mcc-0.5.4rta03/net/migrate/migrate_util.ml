(*
   Utilities for network migration code
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* 
   Uses the printf module because of certain requirements in C code.

      DO NOT REPLACE THIS WITH FORMAT.
     
   There will be problems with bad streams if you use Format here.
   Contact JDS for details.
 *)
open Printf


(* Useful modules *)
open Unix
open Fir
open Fir_marshal
open Fir_exn


(***  Protocol strings  ***)


let default_server_port = 12354
let program_version_string = "migrated-program-0.3"
let process_version_string = "migrated-process-0.3"


(***  Functions  ***)


(* flush_stderr *)
let flush_stderr () = flush Pervasives.stderr


(* output_endline *)
let output_endline out s =
   output_string out (s ^ "\n");
   flush out


(* input_line
   Note, this function will strip off extraneous \r if necessary,
   which gives it a slight advantage over the original input_line
   and allows us to test with telnet. *)
let input_line inc =
   let s = input_line inc in
   let l = String.length s in
   if s.[l - 1] = '\r'
      then String.sub s 0 (l - 1)
      else s


(* input_int32
   Reads in an int32 value. *)
let input_int32 inc =
   let s = String.create 4 in
   let int32_of_char n = Int32.of_int (int_of_char n) in
   let _ = really_input inc s 0 4 in
   let i = int32_of_char s.[0] in
   let i = Int32.logor (Int32.shift_left i 8) (int32_of_char s.[1]) in
   let i = Int32.logor (Int32.shift_left i 8) (int32_of_char s.[2]) in
   let i = Int32.logor (Int32.shift_left i 8) (int32_of_char s.[3]) in
      i


(* input_int32_block
   Reads in a block of int32 values. *)
let rec input_int32_block inc size =
   if size <= 0
      then  []
      else  let value = input_int32 inc in
               value :: input_int32_block inc (size - 1)


(* print_address_string
   Prints a socket address to a string.  *)
let print_address_string () = function
   ADDR_UNIX(s) -> s
 | ADDR_INET(addr, port) -> sprintf "%s:%d" (string_of_inet_addr addr) port


(* print_address
   Prints a socket address to the output channel.  *)
let print_address out addr =
   fprintf out "%s" (print_address_string () addr)


(* print_unix_error
   Prints out a standard UNIX error message.  *)
let print_unix_error e s1 s2 =
   let e = error_message e in
   eprintf "UNIX error: %s in %s(%s)\n" e s1 s2


(* print_message
   Prints a message to the console regarding network status.  This is
   only used for connected sockets where we have an address to give.  *)
let print_message addr msg =
   let msg = sprintf "%a: %s" print_address_string addr msg in
      output_endline Pervasives.stderr msg


(* send_ack_message
   Send an affirmative or acknowledgement message in reply.  *)
let send_ack_message addr out msg =
   let msg' = sprintf "%a: (ACK) %s" print_address_string addr msg in
   let msg = "ACK: " ^ msg in
      output_endline Pervasives.stderr msg';
      match out with
         Some out ->
            output_endline out msg
       | None ->
            ()


(* send_nak_message
   Send a negative or failure message in reply.  *)
let send_nak_message addr out msg =
   let msg' = sprintf "%a: (NAK) %s" print_address_string addr msg in
   let msg = "NAK: " ^ msg in
      output_endline Pervasives.stderr msg';
      match out with
         Some out ->
            output_endline out msg
       | None ->
            ()


(* check_result
   Check the result of a reply message on a network operation.  *)
let check_result inc =
   let s = input_line inc in
   if String.length s < 4
      then raise (Failure ("Malformed reply: \"" ^ s ^ "\""));
   if String.sub s 0 4 <> "ACK:"
      then raise (Failure s)
