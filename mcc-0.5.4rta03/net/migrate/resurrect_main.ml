(*
   Resurrect a process from a state file for configured backend
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


open Migrate_util
open Migrate
open Mcc_config


module Migrate = Migrate (ArchBackend) (ArchCodegen)


(*
 * Configurable options
 *)
let state_file = ref None


(*
 * Arguments
 *)
let usage = "MCC (Mojave Compiler) version " ^ version_string ^ "\n" ^
            "Migration resurrection program, protocol version " ^ program_version_string ^ "\n\n" ^
            "Usage: mcc-resurrect [options] state-file"

let spec = debug_spec


let set_state_file file =
   match !state_file with
      None ->
         state_file := Some file
    | Some _ ->
         Printf.eprintf "Can only specify one state file\n";
         exit 1


(*
 * Run the resurrection program
 *)
let _ =
   Sys.catch_break true;
   Mc_arg.parse spec set_state_file usage

let state_file =
   match !state_file with
      None ->
         Printf.eprintf "Must specify a state file to use\n";
         exit 1
    | Some file ->
         file
let result = Migrate.read_state_file state_file
let () = exit result
