(*
 * Position information and exception printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Location

open Fir

open X86_exn
open X86_inst_type

(*
 * Position information.
 *)
type pos

(*
 * General exception includes debugging info.
 *)
exception X86Exception of pos * x86_error

(*
 * Module for creating positions.
 * You have to specify the name of the module
 * where the exception are being created: use
 * MakePos in each file where Name.name is set
 * to the name of the module.
 *)
module type PosSig =
sig
   val loc_pos : loc -> pos

   val fir_pos : exp -> pos
   val var_exp_pos : var -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos

   val operand_exp_pos    : operand -> pos
   val inst_exp_pos       : inst -> pos
   val inst_pos           : inst -> pos -> pos

   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig

(*
 * Exception printing.
 *)
val pp_print_exn : formatter -> exn -> unit
val catch : ('a -> 'b) -> 'a -> 'b

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
