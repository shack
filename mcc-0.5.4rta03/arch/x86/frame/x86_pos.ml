(*
 * X86 back-end exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fir
open Fir_ds
open Fir_print
open Fir_state

open X86_exn
open X86_inst_type
open X86_exn_print

(*
 * Location of exception.
 *)
type item =
   Exp of exp
 | Symbol of var
 | String of string
 | Operand of operand
 | Inst of inst

type pos = item Position.pos

(*
 * FIR exception.
 *)
exception X86Exception of pos * x86_error

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the source location for an exception.
 *)
let string_loc = bogus_loc "<X86_pos>"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_exp e
    | Symbol _
    | String _
    | Operand _
    | Inst _ ->
         string_loc

(*
 * Print debugging info.
 *)
let rec pp_print_value buf x =
   match x with
      Exp e ->
         pp_print_expr buf e

    | Symbol v ->
         pp_print_symbol buf v

    | String s ->
         pp_print_string buf s

    | Operand e ->
         pp_print_operand buf e

    | Inst inst ->
         pp_print_inst buf inst

(*
 * Semant exception printer.
 *)
let pp_print_error buf exn =
   match exn with
      ArityMismatch i ->
         fprintf buf "arity mismatch: %d" i
    | IllegalStorageType ty ->
         fprintf buf "illegal storage type: %a" pp_print_type ty
    | IllegalOffsetAtom a ->
         fprintf buf "illegal offset atom: %a" pp_print_atom a
    | InvalidFieldIndex(i, ty) ->
         fprintf buf "illegal index into field list: %d in@ %a" i pp_print_type ty
    | AtomClassIncompatible (i, a) ->
         fprintf buf "atom class %d incompatible with atom: %a" i pp_print_atom a
    | NotImplemented s ->
         fprintf buf "not implemented: %s" s
    | InternalError s ->
         fprintf buf "internal error: %s" s
    | StringError s ->
         pp_print_string buf s
    | StringTypeError (s, ty) ->
         fprintf buf "%s:@ %a" s pp_print_type ty
    | IllegalRegister v ->
         fprintf buf "Illegal register: %a" pp_print_symbol v
    | IllegalOperand op ->
         fprintf buf "Illegal operand:@ %a" pp_print_operand op
    | IllegalInstruction op ->
         fprintf buf "Illegal instruction:@ %a" pp_print_inst op

(************************************************************************
 * CONSTRUCTION
 ************************************************************************)

module type PosSig =
sig
   val loc_pos : loc -> pos

   val fir_pos : exp -> pos
   val var_exp_pos : var -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos

   val operand_exp_pos    : operand -> pos
   val inst_exp_pos       : inst -> pos
   val inst_pos           : inst -> pos -> pos

   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let fir_pos e = base_pos (Exp e)
   let var_exp_pos v = base_pos (Symbol v)
   let string_exp_pos s = base_pos (String s)
   let var_pos = symbol_pos

   let operand_exp_pos op = base_pos (Operand op)
   let inst_exp_pos inst = base_pos (Inst inst)
   let inst_pos inst = cons_pos (Inst inst)
end

module Pos = MakePos (struct let name = "X86_pos" end)
open Pos

(*
 * Exception printer.
 *)
let pp_print_exn buf exn =
   match exn with
      X86Exception (loc, exn) ->
         fprintf buf "@[<v 0>%a@[<hv 3>*** Error: %a@]@]" (**)
            pp_print_pos loc
            pp_print_error exn
    | exn ->
         Mir_exn_print.pp_print_exn buf exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      X86Exception _
    | Mir_pos.MirException _
    | Fir_pos.FirException _ as exn ->
         eprintf "%a@." pp_print_exn exn;
         exit 2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
