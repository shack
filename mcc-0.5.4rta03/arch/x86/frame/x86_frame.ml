(*
 * X86 frame.  This module defines the machine-dependent
 * features for the x86 platform for use by external modules
 * like the register allocator.  Of course, the remaining modules
 * in the x86 directory are also machine-dependent, but _all_
 * core OS features are defined here, including the register sets
 * and calling convention.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mc_string_util
open Mc_map
open Symbol
open Trace
open Flags

open Fir_env
open Fir_marshal

open Frame_type
open Sizeof_const

open X86_exn
open X86_inst_type
open X86_frame_type

module MirPos = Mir_pos.MakePos (struct let name = "X86_frame" end)

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Use MMX registers for spills.
 *)
let spill_mmx () = std_flags_get_bool "x86.debug.spill_mmx"

(*
 * Spill set gives translation for each of
 * the vars.
 *)
type spset =
   { spset_int_normal_index : int;
     spset_int_stdarg_index : int;
     spset_float_normal_index : int;
     spset_float_stdarg_index : int;
     spset_mmx_normal_index : int;
     spset_mmx_stdarg_index : int;
     spset_global_index : int;
     spset_table : operand SymbolTable.t
   }

(*
 * Register classes are just numbers.
 *)
type reg_class = int
type cenv = reg_class SymbolTable.t

(*
 * External interface.
 * See comments for "type prog" in frame_type.ml
 *)
type prog = spset poly_prog

(************************************************************************
 * MACHINE-DEPENDENT SECTION
 ************************************************************************)

module FloatCompare : (OrderedType with type t = Float80.float80) = struct
   type t = Float80.float80
   let compare = Float80.compare
end (* FloatCompare *)
module FloatTable : (Mc_map.McMap with type key = Float80.float80) = Mc_map.McMake (FloatCompare)

type reg64_key = var * var
type reg64_map = reg64_key SymbolTable.t
type fval_key  = var
type fval_map  = fval_key FloatTable.t

(*
 * Names of the standard args.
 *)
let eax    = Symbol.add "eax"
let ebx    = Symbol.add "ebx"
let ecx    = Symbol.add "ecx"
let edx    = Symbol.add "edx"
let esi    = Symbol.add "esi"
let edi    = Symbol.add "edi"
let esp    = Symbol.add "esp"
let ebp    = Symbol.add "ebp"
let fp0    = Symbol.add "fp0"
let fp1    = Symbol.add "fp1"
let fp2    = Symbol.add "fp2"
let fp3    = Symbol.add "fp3"
let fp4    = Symbol.add "fp4"
let fp5    = Symbol.add "fp5"
let iflags = Symbol.add "iflags"
let fflags = Symbol.add "fflags"
let fpmap  = [fp0, 0; fp1, 1; fp2, 2; fp3, 3; fp4, 4; fp5, 5]
let mmx0   = Symbol.add "mmx0"
let mmx1   = Symbol.add "mmx1"
let mmx2   = Symbol.add "mmx2"
let mmx3   = Symbol.add "mmx3"
let mmx4   = Symbol.add "mmx4"
let mmx5   = Symbol.add "mmx5"
let mmx6   = Symbol.add "mmx6"
let mmx7   = Symbol.add "mmx7"

(*
 * General-purpose registers.
 * Divide them into int/float classes.
 * The x86 has no real floating-pointer "registers",
 * only the float stack.
 *)
let int_reg_class   = 0
let float_reg_class = 1
let mmx_reg_class   = 2
let reg_class_count = 3

let int_registers = [eax; ebx; ecx; edx; esi; edi; esp; ebp]
let int_registers_special = [esp; ebp; iflags]
let gdb_registers = [eax; ecx; edx; ebx; esp; ebp; esi; edi]

let float_registers = [fp0; fp1; fp2; fp3; fp4; fp5]
let float_registers_special = [fflags]

let mmx_registers = [mmx0; mmx1; mmx2; mmx3; mmx4; mmx5; mmx6; mmx7]
let mmx_registers_len = List.length mmx_registers
let mmx_registers_special = []

let registers = [|int_registers; float_registers; mmx_registers|]
let registers_special = [|int_registers_special; float_registers_special; mmx_registers_special|]

(*
 * Calling convention for interfacing with C code
 *
 * On entry into a function, the stack looks like this:
 *    |  Return address (PC)     |  <-- TOS
 *    +--------------------------+
 *    |  Context (EBP)           |
 *    +--------------------------+
 *    |  Arguments to function   |
 *    |  ...                     |
 *    +--------------------------+
 *
 * The callee is responsible for saving the registers; after the callee has
 * pushed registers onto the stack, the stack looks like this:
 *    |  6 hardware registers    |  <-- TOS
 *    |  ...                     |  24 bytes long
 *    +--------------------------+
 *    |  Return address (PC)     |  4 bytes long
 *    +--------------------------+
 *    |  Context (EBP)           |
 *    +--------------------------+
 *    |  Arguments to function   |
 *    |  ...                     |
 *    +--------------------------+
 *
 * callee_save : list of registers that are saved when the generated assembly
 *    code is called from C code.  The registers are saved in exactly the order
 *    listed here.
 *
 * callee_context_off : offset into the stack that we can find the Context
 *    argument after all the registers have been pushed on.
 *
 * callee_header_stack : offset to the first ``normal'' argument being passed
 *    into the function.
 *)
let callee_save = [ebx; ecx; edx; esi; edi; ebp]
let callee_context_off = succ (List.length callee_save) * sizeof_int32
let callee_header_stack = callee_context_off + sizeof_int32

(*
 * Spill costs.  These numbers are the relative costs
 * of loads/stores.  Larger numbers cost more.
 *
 * BUG: I don't know the architecture well enough to estimate
 * the cost of memory operations.  However, I assume defs are
 * more expensive that uses because of cache behavior.
 *)
let def_cost = 5
let use_cost = 3
let mov_def_cost = 2
let mov_use_cost = 1


(*
 * Get a register number.
 *)
let get_gdb_register_number r =
   let rec search i = function
      r' :: l ->
         if r = r' then
            i
         else
            search (succ i) l
    | [] ->
         Symbol.to_int r
   in
      search 0 gdb_registers

(*
 * Name of the register that holds the context.
 *)
let context_base = ebp

(* Names of some external functions *)
let extern_names =
   [|"__exit_frame";
     "__exit";
     "__migrate";
     "__atomic";
     "__atomic_retry";
     "__atomic_commit";
     "__gc";
     "__copy_on_write";
     "__uncaught_exception";
     "__format_stdargs"|]

let extern_labels = Array.map new_symbol_string extern_names

let exit_frame_label          = extern_labels.(0)
let exit_label                = extern_labels.(1)
let migrate_fun_label         = extern_labels.(2)
let atomic_fun_label          = extern_labels.(3)
let atomic_rollback_fun_label = extern_labels.(4)
let atomic_commit_fun_label   = extern_labels.(5)
let gc_label                  = extern_labels.(6)
let copy_on_write_label       = extern_labels.(7)
let uncaught_exception_label  = extern_labels.(8)
let format_stdargs_label      = extern_labels.(9)

(* Builtin functions *)
let builtin_names =
   [|"fc_print_string";
     "fc_print_block";
     "fc_print_char";
     "fc_print_int";
     "fc_print_hex";
     "fc_print_long";
     "fc_print_long_hex";
     "fc_print_float";
     "fc_print_double";
     "fc_print_long_double";
     "fc_print_string_int";
     "fc_print_string_hex";
     "fc_print_string_long";
     "fc_print_string_long_hex";
     "fc_print_string_float";
     "fc_flush";
     "fc_atoi";
     "fc_atof";
     "fc_exit";
     "fc_atomic_levels";
     "fc_utime";
     "fc_gc";

     "fc_fpitest_msg";
     "fc_fpitest_data";
     "fc_fpitest_check_true";
     "fc_fpitest_check_false";
     "fc_fptest_check_tol_float";
     "fc_fptest_check_tol_double";
     "fc_fptest_check_tol_long_double";

     "ml_print_string";
     "ml_print_char";
     "ml_print_int";
     "ml_output_byte";
     "ml_print_hex";
     "ml_print_float";
     "ml_atoi";
     "ml_atof";
     "ml_cmp";

     "aml_print_int";
     "aml_print_string";
     "aml_exit";
     "aml_string_equal";
     "aml_strcat";
     "aml_equal";
     "aml_nequal";
     "aml_lt";
     "aml_le";
     "aml_gt";
     "aml_ge";
     "aml_compare";
     "aml_string_of_int";
     "aml_int_of_string";
     "aml_string_of_float";
     "aml_float_of_string";
     "aml_frexp";
     "aml_fmodf";

     "java_println";
     "java_strcat";
     "java_itoa";
     "java_atoi";

     "unl_getchar";

     "uncaught_exception"
   |]

let builtin_names_ext = Array.map (fun s -> "__ext_" ^ s) builtin_names

let builtin_labels = Array.map new_symbol_string builtin_names_ext

let builtin_lookup =
   let names = Array.to_list builtin_names in
   let labels = Array.to_list builtin_labels in
   let names = List.combine names labels in
   let venv =
      List.fold_left (fun venv (s, v) ->
            StringTable.add venv s v) StringTable.empty names
   in
   let lookup s =
      try StringTable.find venv s with
         Not_found ->
            raise (Invalid_argument ("X86_frame.builtin_lookup: undefined external: " ^ s))
   in
      lookup

(* Seg fault *)
let other_fault                 = 0
let index_fault                 = 1
let fun_index_fault             = 2
let pointer_index_fault         = 3
let bounds_fault                = 4
let lower_bounds_fault          = 5
let upper_bounds_fault          = 6
let null_pointer_fault          = 7
let fun_arity_tag_fault         = 8
let fault_count                 = 9

let seg_fault_names =
   [|"__seg_fault_other";
     "__seg_fault_index";
     "__seg_fault_fun_index";
     "__seg_fault_pointer_index";
     "__seg_fault_bounds";
     "__seg_fault_lower_bounds";
     "__seg_fault_upper_bounds";
     "__seg_fault_null_pointer";
     "__seg_fault_fun_arity_tag"|]

let seg_fault_labels = Array.map new_symbol_string seg_fault_names

(* Debug labels *)
let debug_alloc              = 0
let debug_let_poly           = 1
let debug_let_pointer        = 2
let debug_let_function       = 3
let debug_set_poly           = 4
let debug_set_pointer        = 5
let debug_set_function       = 6
let debug_pre_let_poly       = 7
let debug_pre_let_pointer    = 8
let debug_pre_let_function   = 9
let debug_pre_set_pointer    = 10
let debug_pre_set_function   = 11
let debug_count              = 12

let debug_names =
   [|"__debug_alloc";
     "__debug_let_poly";
     "__debug_let_pointer";
     "__debug_let_function";
     "__debug_set_poly";
     "__debug_set_pointer";
     "__debug_set_function";
     "__debug_pre_let_poly";
     "__debug_pre_let_pointer";
     "__debug_pre_let_function";
     "__debug_pre_set_pointer";
     "__debug_pre_set_function"|]

let debug_labels = Array.map new_symbol_string debug_names

(* Collect all the import names *)
let import_vars =
   let names  = Array.concat [extern_names;  builtin_names_ext; seg_fault_names; debug_names]  in
   let labels = Array.concat [extern_labels; builtin_labels; seg_fault_labels; debug_labels] in
   let names  = Array.to_list names in
   let labels = Array.to_list labels in
      List.combine labels names

(*
 * The following symbols are part of the context, and
 * are described in more detail in x86_runtime.h.
 *)

(* Memory space *)
let mem_base  = Symbol.add "mem_base"
let mem_next  = Symbol.add "mem_next"
let mem_limit = Symbol.add "mem_limit"
let mem_size  = Symbol.add "mem_size"

(* Pointer table descriptors *)
let pointer_base = Symbol.add "pointer_base"
let pointer_next = Symbol.add "pointer_next"
let pointer_limit= Symbol.add "pointer_limit"
let pointer_free = Symbol.add "pointer_free"
let pointer_size = Symbol.add "pointer_size"

(* Minor heap *)
let minor_base   = Symbol.add "minor_base"
let minor_limit  = Symbol.add "minor_limit"
let minor_roots  = Symbol.add "minor_roots"
let next_root    = Symbol.add "next_root"

(* Atomic pointers *)
let atomic_info  = Symbol.add "atomic_data"
let atomic_next  = Symbol.add "atomic_next"
let atomic_size  = Symbol.add "atomic_size"

(* Allocation info *)
let copy_point   = Symbol.add "copy_point"
let gc_point     = Symbol.add "gc_point"
let gc_info      = Symbol.add "gc_info"

(* Floating point temporary registers *)
let float_reg_1  = Symbol.add "float_reg_1"
let float_reg_2  = Symbol.add "float_reg_2"

(* Other external symbols *)
let export_names =
   [|"__migrate_data";
     "__context_size";
     "__header_base";
     "__header_limit";
     "__globals_base";
     "__globals_limit";
     "__globals_size";
     "__globals_index_base";
     "__globals_index_limit";
     "__function_base";
     "__function_limit";
     "__function_size";
     "__exit.tag";
     "__uncaught_exception.tag";
     "__bin_fir";
     "__hash_index";
     "__hash_fun_index";
     "__core_debug_flags";
     "__exit.arity";
     "__uncaught_exception.arity"|]

let export_syms =
   Array.map new_symbol_string export_names

let export_vars =
   let names = Array.to_list export_names in
   let syms = Array.to_list export_syms in
      List.combine syms names

let migrate_data_label              = export_syms.(0)
let context_size_label              = export_syms.(1)
let header_base_label               = export_syms.(2)
let header_limit_label              = export_syms.(3)
let globals_base_label              = export_syms.(4)
let globals_limit_label             = export_syms.(5)
let globals_size_label              = export_syms.(6)
let globals_index_base_label        = export_syms.(7)
let globals_index_limit_label       = export_syms.(8)
let function_base_label             = export_syms.(9)
let function_limit_label            = export_syms.(10)
let function_size_label             = export_syms.(11)
let exit_tag_label                  = export_syms.(12)
let uncaught_exception_tag_label    = export_syms.(13)
let bin_fir_label                   = export_syms.(14)
let hash_index_label                = export_syms.(15)
let hash_fun_index_label            = export_syms.(16)
let debug_flags_label               = export_syms.(17)
let exit_arity_label                = export_syms.(18)
let uncaught_exception_arity_label  = export_syms.(19)

let function_base = function_base_label
let function_size = function_size_label

(*
 * Arity tags required by the backend
 *)
let exit_arity_tag = []
let uncaught_exception_arity_tag = []
let backend_arity_tags =
   [exit_arity_tag;
    uncaught_exception_arity_tag]

(************************************************************************
 * SPILL SET
 ************************************************************************)

(*
 * Empty spill set.
 *)
let spset_empty =
   { spset_int_normal_index = 0;
     spset_int_stdarg_index = 0;
     spset_float_normal_index = 0;
     spset_float_stdarg_index = 0;
     spset_mmx_normal_index = 0;
     spset_mmx_stdarg_index = 0;
     spset_global_index = 0;
     spset_table = SymbolTable.empty
   }

(*
 * Size of spill set.
 *)
let spset_size { spset_int_normal_index = int_normal_index;
                 spset_int_stdarg_index = int_stdarg_index;
                 spset_float_normal_index = float_normal_index;
                 spset_float_stdarg_index = float_stdarg_index;
                 spset_mmx_normal_index = mmx_normal_index;
                 spset_mmx_stdarg_index = mmx_stdarg_index;
                 spset_global_index = global_index
    } =
   { spill_int_normal_index = int_normal_index;
     spill_int_stdarg_index = int_stdarg_index;
     spill_float_normal_index = float_normal_index;
     spill_float_stdarg_index = float_stdarg_index;
     spill_mmx_normal_index = mmx_normal_index;
     spill_mmx_stdarg_index = mmx_stdarg_index;
     spill_global_index = global_index
   }

(*
 * Symbol name of a spill.
 *)
let symbol_of_int_normal_spill index =
   Symbol.add ("spill_int_" ^ string_of_int index)

let symbol_of_int_stdarg_spill index =
   Symbol.add ("stdarg_int_" ^ string_of_int index)

let symbol_of_float_normal_spill index =
   Symbol.add ("spill_float_" ^ string_of_int index)

let symbol_of_float_stdarg_spill index =
   Symbol.add ("stdarg_float_" ^ string_of_int index)

let symbol_of_mmx_normal_spill index =
   Symbol.add ("spill_mmx_" ^ string_of_int index)

let symbol_of_mmx_stdarg_spill index =
   Symbol.add ("stdarg_mmx_" ^ string_of_int index)

let symbol_of_global_spill index =
   Symbol.add ("global_" ^ string_of_int index)

(*
 * Add a var to the spill set.
 *)
let spset_spill_context_int32 spset v =
   let table = SymbolTable.add spset.spset_table v (SpillRegister (v, OffLabel v)) in
      { spset with spset_table = table }

let spset_spill_normal_int32 spset v name =
   let { spset_int_normal_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let op =
            if not (spill_mmx ()) || index >= mmx_registers_len then
               let label = symbol_of_int_normal_spill index in
                  SpillRegister (name, OffLabel label)
            else
               MMXRegister (List.nth mmx_registers index)
         in
            { spset with spset_int_normal_index = succ index;
                         spset_table = SymbolTable.add table v op
            }

let spset_spill_stdarg_int32 spset v name =
   let { spset_int_stdarg_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let label = symbol_of_int_stdarg_spill index in
            { spset with spset_int_stdarg_index = succ index;
                         spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel label))
            }

let spset_spill_global spset v name =
   let { spset_global_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let label = symbol_of_global_spill index in
            { spset with spset_global_index = succ index;
                         spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel label))
            }

let spset_spill_extern_int32 spset v name =
   let { spset_table = table } = spset in
      if SymbolTable.mem table v then
         spset
      else
         { spset with spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel v)) }

let spset_spill_normal_float spset v name =
   let { spset_float_normal_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let label = symbol_of_float_normal_spill index in
            { spset with spset_float_normal_index = succ index;
                         spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel label))
            }

let spset_spill_stdarg_float spset v name =
   let { spset_float_stdarg_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let label = symbol_of_float_stdarg_spill index in
            { spset with spset_float_stdarg_index = succ index;
                         spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel label))
            }

let spset_spill_normal_mmx spset v name =
   let { spset_mmx_normal_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let label = symbol_of_mmx_normal_spill index in
            { spset with spset_mmx_normal_index = succ index;
                         spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel label))
            }

let spset_spill_stdarg_mmx spset v name =
   let { spset_mmx_stdarg_index = index;
         spset_table = table
       } = spset
   in
      if SymbolTable.mem table v then
         spset
      else
         let label = symbol_of_mmx_stdarg_spill index in
            { spset with spset_mmx_stdarg_index = succ index;
                         spset_table = SymbolTable.add table v (SpillRegister (name, OffLabel label))
            }

let spset_spill spset v name reg_class =
   if reg_class = int_reg_class then
      spset_spill_normal_int32 spset v name
   else if reg_class = float_reg_class then
      spset_spill_normal_float spset v name
   else if reg_class = mmx_reg_class then
      spset_spill_normal_mmx spset v name
   else
      raise (Invalid_argument "spset_spill")

(*
 * Add a var.
 *)
let spset_add_arg spset v arg =
   { spset with spset_table = SymbolTable.add spset.spset_table v arg }

(*
 * Add a var that represents a label.
 *)
let spset_label spset v =
   { spset with spset_table = SymbolTable.add spset.spset_table v (ImmediateLabel v) }

let spset_clabel spset v =
   { spset with spset_table = SymbolTable.add spset.spset_table v (ImmediateNumber (OffLabel v)) }

let spset_int spset v i =
   { spset with spset_table = SymbolTable.add spset.spset_table v (ImmediateNumber (OffNumber (Int32.of_int i))) }

(*
 * Combine two vars.
 *)
let spset_add spset v1 v2 =
   { spset with spset_table = SymbolTable.add spset.spset_table v1 (Register v2) }

(*
 * Translate a var.
 *)
let rec spset_lookup_int32 spset v =
   try
      let src = SymbolTable.find spset.spset_table v in
         match src with
            Register v ->
               spset_lookup_int32 spset v
          | _ ->
               src
   with
      Not_found ->
         Register v

let rec spset_lookup_float spset v =
   try
      let src = SymbolTable.find spset.spset_table v in
         match src with
            Register v
          | FloatRegister v ->
               spset_lookup_float spset v
          | _ ->
               src
   with
      Not_found ->
         FloatRegister v

let rec spset_lookup_mmx spset v =
   try
      let src = SymbolTable.find spset.spset_table v in
         match src with
            Register v
          | MMXRegister v ->
               spset_lookup_mmx spset v
          | _ ->
               src
   with
      Not_found ->
         MMXRegister v

let offset_from_base op steps =
   let size = sizeof_int32 * steps in
      match op with
         MemReg ptr ->
            MemRegOff(ptr, OffNumber (Int32.of_int size))
       | MemRegOff (r, OffNumber i) ->
            MemRegOff (r, OffNumber (Int32.add i (Int32.of_int size)))
       | MemRegRegOffMul (r1, r2, OffNumber i, m) when m = Int32.one ->
            MemRegRegOffMul (r1, r2, OffNumber (Int32.add i (Int32.of_int size)), m)
       | _ ->
            op

(*
 * Print the spill set.
 *)
let spset_iter f spset =
   SymbolTable.iter f spset.spset_table

(************************************************************************
 * INTERNAL CALLING CONVENTION
 ************************************************************************)

(*
 * Get the standard argument list.  Make up the rest on the fly.
 *
 * Initially, frame_args* contain the names of the first N fixed standard
 * arguments for a particular register class.  If we run into a function
 * that requires more than N standard arguments, all arguments after the
 * first N must be spilled; they were be appended to frame_args* and also
 * to frame_*_spills, to indicate that the values must be spilled in a
 * special location in the context.
 *)
let frame_int_spills = ref []
let frame_float_spills = ref []
let frame_args = ref [eax; ebx; ecx; edx]
let frame_args_float = ref []

let get_stdargs acs =
   let get_an_arg spills app next cont =
      match next with
         next :: next' ->
            cont next app next'
       | [] ->
            let next = new_symbol_string "arg" in
               spills := next :: !spills;
               cont next (next :: app) []
   in
   let rec get_an_argument app_int app_float next_int next_float = function
      ACInt                      :: acs
    | ACRawInt (Rawint.Int8,  _) :: acs
    | ACRawInt (Rawint.Int16, _) :: acs
    | ACRawInt (Rawint.Int32, _) :: acs
    | ACPointer _                :: acs
    | ACFunction _               :: acs
    | ACPoly                     :: acs ->
         get_an_arg frame_int_spills app_int next_int (fun next app_int next_int ->
            ArgInt32 next :: get_an_argument app_int app_float next_int next_float acs)

    | ACRawInt(Rawint.Int64, _) :: acs
    | ACPointerInfix _          :: acs ->
         (* 64-bit integers use up two int32 registers at a time. *)
         get_an_arg frame_int_spills app_int next_int (fun hi app_int next_int ->
            get_an_arg frame_int_spills app_int next_int (fun lo app_int next_int ->
               ArgInt64 (hi, lo) :: get_an_argument app_int app_float next_int next_float acs))

    | ACFloat _ :: acs ->
         (* We always use 80-bit intermediary registers *)
         get_an_arg frame_float_spills app_float next_float (fun next app_float next_float ->
            let arg = ArgFloat (next, Rawfloat.LongDouble) in
               arg :: get_an_argument app_int app_float next_int next_float acs)

    | [] ->
         (* Append any new symbols to the args lists *)
         frame_args := !frame_args @ (List.rev app_int);
         frame_args_float := !frame_args_float @ (List.rev app_float);
         []
   in
      get_an_argument [] [] !frame_args !frame_args_float acs

(*
 * Convert to operand form.
 *)
let operand_args_of_var_args args =
   let venv, _ =
      List.fold_left (fun (venv, index) v ->
            let label = symbol_of_int_stdarg_spill index in
            let venv = SymbolTable.add venv v (MemRegOff (ebp, OffLabel label)) in
               venv, succ index) (SymbolTable.empty, 0) (List.rev !frame_int_spills)
   in
   let venv, _ =
      List.fold_left (fun (venv, index) v ->
            let label = symbol_of_float_stdarg_spill index in
            let venv = SymbolTable.add venv v (MemRegOff (ebp, OffLabel label)) in
               venv, succ index) (venv, 0) (List.rev !frame_float_spills)
   in
   let venv_find v =
      try SymbolTable.find venv v with
         Not_found ->
            Register v
   in
   let operand_arg_of_var_arg = function
      ArgInt32 v ->
         ArgInt32 (venv_find v)
    | ArgInt64 (v1, v2) ->
         ArgInt64 (venv_find v1, venv_find v2)
    | ArgFloat (v, pre) ->
         ArgFloat (venv_find v, pre)
   in
      List.map operand_arg_of_var_arg args

let get_std_operands acs =
   let args = get_stdargs acs in
      operand_args_of_var_args args

(*
 * Add all the normally spilled vars.
 * The ordering of global_vars is important.
 * The order is defined in x86_runtime.h
 *)
let context_vars =
   [mem_base;
    mem_next;
    mem_limit;
    mem_size;
    pointer_base;
    pointer_next;
    pointer_limit;
    pointer_free;
    pointer_size;
    float_reg_1;
    float_reg_2;
    minor_base;
    minor_limit;
    minor_roots;
    next_root;
    atomic_info;
    atomic_next;
    atomic_size;
    copy_point;
    gc_point;
    gc_info]

let context_length = List.length context_vars

let label_vars =
   []

let clabel_vars =
   [function_base;
    function_size;
    hash_index_label;
    hash_fun_index_label;
    exit_frame_label;
    exit_label]
   @ (Array.to_list seg_fault_labels)
   @ (Array.to_list debug_labels)

let initial_spset () =
   let spset = List.fold_left spset_spill_context_int32 spset_empty context_vars in
   let spset = List.fold_left spset_label spset label_vars in
   let spset = List.fold_left spset_clabel spset clabel_vars in
      if std_flags_get_bool "x86.debug.alloc_fpstack" then
         spset
      else
         List.fold_left (fun spset fpreg ->
            spset_spill_normal_float spset fpreg fpreg) spset float_registers

(*
 * Spill all block labels and all globals.
 *)
let spset_spill_prog prog =
   let { asm_blocks = blocks;
         asm_import = imports;
         asm_globals = globals;
         asm_spset = spset
       } = prog
   in

   (* Add block labels *)
   let spset =
      Trace.fold (fun spset { block_label = label } ->
            spset_clabel spset label) spset blocks
   in

   (* Add all the spilled standard arguments *)
   let spset =
      List.fold_left (fun spset v ->
            spset_spill_stdarg_int32 spset v v) spset (List.rev !frame_int_spills)
   in
   let spset =
      List.fold_left (fun spset v ->
            spset_spill_stdarg_float spset v v) spset (List.rev !frame_float_spills)
   in

   (* Add all global vars *)
   let spset =
      SymbolTable.fold (fun spset v { import_info = info } ->
            match info with
               ImportFun _ ->
                  spset_clabel spset v
             | ImportGlobal ->
                  spset_spill_extern_int32 spset v v) spset imports
   in
   let spset =
      List.fold_left (fun spset (v, _) ->
            spset_spill_global spset v v) spset globals
   in
      { prog with asm_spset = spset }

(************************************************************************
 * CODE GENERATION UTILITIES
 ************************************************************************)

(*
 * Manipulations to the reg64 structure
 *)
let reg64_add reg64 v =
   let vhi = new_symbol_string (Symbol.to_string v ^ "_hi") in
   let vlo = new_symbol_string (Symbol.to_string v ^ "_lo") in
      SymbolTable.add reg64 v (vhi, vlo)

let reg64_add_hi_lo reg64 v vhi vlo =
   SymbolTable.add reg64 v (vhi, vlo)

let reg64_lookup reg64 pos v =
   let pos = MirPos.string_pos "reg64_lookup" pos in
   try SymbolTable.find reg64 v with
      Not_found -> raise (Mir_pos.MirException (pos, Mir_exn.UnboundVar v))

let reg64_lookup_exn reg64 v =
   SymbolTable.find reg64 v

(*
 * Manipulations to the fvals structure
 *)
let fvals_add fvals f =
   let s = Printf.sprintf "float_%s" (Float80.format "%1.5Le" f) in
   let _ =
      for iter = 0 to pred (String.length s) do
         if s.[iter] = '.' then s.[iter] <- 'D'
         else if s.[iter] = '+' then s.[iter] <- 'P'
         else if s.[iter] = '-' then s.[iter] <- 'M'
      done
   in
   let v = new_symbol_string s in
      FloatTable.add fvals f v

let fvals_lookup fvals pos f =
   let pos = MirPos.string_pos "fvals_lookup" pos in
   try ImmediateLabel (FloatTable.find fvals f) with
      Not_found -> raise (Mir_pos.MirException (pos, Mir_exn.InternalError "Missing floating point value"))


(*
 * Registers in a src operand.
 *)
let src_regs src regs =
   match src with
      ImmediateNumber _
    | ImmediateLabel _
    | FPStack _
    | SpillRegister _ ->
         regs
    | Register r
    | MemReg r
    | MemRegOff (r, _)
    | FloatRegister r
    | MMXRegister r ->
         r :: regs
    | MemRegRegOffMul (r1, r2, _, _) ->
         r1 :: r2 :: regs

let dst_regs dst regs =
   match dst with
      Register r
    | FloatRegister r
    | MMXRegister r ->
         r :: regs
    | FPStack _
    | ImmediateNumber _
    | ImmediateLabel _
    | MemReg _
    | MemRegOff _
    | MemRegRegOffMul _
    | SpillRegister _ ->
         regs

let rec off_labels off labels =
   match off with
      OffNumber _ ->
         labels
    | OffLabel label ->
         label :: labels

    | OffUMinus off
    | OffUNot off
    | OffUAbs off
    | OffULog2 off
    | OffUPow2 off ->
         off_labels off labels

    | OffAdd (off1, off2)
    | OffSub (off1, off2)
    | OffMul (off1, off2)
    | OffDiv (off1, off2)
    | OffSal (off1, off2)
    | OffShr (off1, off2)
    | OffSar (off1, off2)
    | OffAnd (off1, off2)
    | OffOr  (off1, off2)
    | OffXor (off1, off2) ->
         off_labels off1 (off_labels off2 labels)

    | OffRel (off, label) ->
         off_labels off (label :: labels)

let dst_labels dst labels =
   match dst with
      ImmediateLabel l ->
         l :: labels
    | ImmediateNumber off
    | MemRegOff (_, off)
    | MemRegRegOffMul (_, _, off, _)
    | SpillRegister (_, off) ->
         off_labels off labels
    | Register _
    | FloatRegister _
    | MMXRegister _
    | FPStack _
    | MemReg _ ->
         labels

(*
 * Load a value into a register.
 *)
let code_load_uint8 r1 src =
   match src with
      Register _
    | ImmediateNumber _
    | SpillRegister _ ->
         MOV (IL, Register r1, src)
    | FPStack _
    | ImmediateLabel _
    | MemReg _
    | MemRegOff _
    | MemRegRegOffMul _ ->
         MOVZ (IB, Register r1, src)
    | FloatRegister _
    | MMXRegister _ ->
         raise (Invalid_argument "code_load_uint8")

let code_load_int8 r1 src =
   match src with
      Register _
    | ImmediateNumber _
    | SpillRegister _ ->
         MOV (IL, Register r1, src)
    | FPStack _
    | ImmediateLabel _
    | MemReg _
    | MemRegOff _
    | MemRegRegOffMul _ ->
         MOVS (IB, Register r1, src)
    | FloatRegister _
    | MMXRegister _ ->
         raise (Invalid_argument "code_load_int8")

let code_load_uint16 r1 src =
   match src with
      Register _
    | ImmediateNumber _
    | SpillRegister _ ->
         MOV (IL, Register r1, src)
    | FPStack _
    | ImmediateLabel _
    | MemReg _
    | MemRegOff _
    | MemRegRegOffMul _ ->
         MOVZ (IW, Register r1, src)
    | FloatRegister _
    | MMXRegister _ ->
         raise (Invalid_argument "code_load_uint16")

let code_load_int16 r1 src =
   match src with
      Register _
    | ImmediateNumber _
    | SpillRegister _ ->
         MOV (IL, Register r1, src)
    | FPStack _
    | ImmediateLabel _
    | MemReg _
    | MemRegOff _
    | MemRegRegOffMul _ ->
         MOVS (IW, Register r1, src)
    | FloatRegister _
    | MMXRegister _ ->
         raise (Invalid_argument "code_load_int16")

let code_load_uint32 r1 src = MOV (IL, Register r1, src)
let code_load_int32 = code_load_uint32

(*
 * Store a value from a register.
 *)
let code_store_uint8  dst r2 = MOV (IB, dst, Register r2)
let code_store_uint16 dst r2 = MOV (IW, dst, Register r2)
let code_store_uint32 dst r2 = MOV (IL, dst, Register r2)
let code_store_int8  = code_store_uint8
let code_store_int16 = code_store_uint16
let code_store_int32 = code_store_uint32

(*
 * Global header saves callee-save registers.
 *)
let callee_header =
   let insts = Listbuf.of_elt (CommentString "Callee header for global function") in
   let insts = Listbuf.add_list insts (List.map (fun r -> PUSH (IL, Register r)) callee_save) in
   let insts = Listbuf.add insts (MOV (IL, Register ebp, MemRegOff (esp, OffNumber (Int32.of_int callee_context_off)))) in
      insts

(************************************************************************
 * RUNTIME CONSTANTS
 ************************************************************************)

(* Hash value for pointer entries *)
let index_shift32 = X86_runtime.index_shift32

(* Tag for RTTD blocks *)
let rttd_tag = X86_runtime.rttd_tag

(* Pointer table masks *)
let ptable_pointer_mask = X86_runtime.ptable_pointer_mask
let ptable_valid_bit_off = X86_runtime.ptable_valid_bit_off

(* Size constants *)
let aggr_header_size = X86_runtime.aggr_header_size
let aggr_header_size32 = X86_runtime.aggr_header_size32
let block_header_size = X86_runtime.block_header_size
let block_header_size32 = X86_runtime.block_header_size32

let () = std_flags_register_list_help "x86.debug"
  ["x86.debug.alloc_fpstack",    FlagBool true,
                                 "If set, then floating-point values will be allocated on" ^
                                 " the Intel FP stack.  Otherwise, they will be stored in" ^
                                 " memory.  This flag should only be cleared when debugging" ^
                                 " certain problems with the X86 floating point code.";
   "x86.debug.spill_mmx",        FlagBool false,
                                 "If set, then the register allocator will attempt to" ^
                                 " allocate using the Intel MMX registers.  This is intended" ^
                                 " for research only and should not be used in production systems."]
