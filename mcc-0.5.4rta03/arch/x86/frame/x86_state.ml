(*
 * Debugging variables.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Flags


(*
 * Register standard options for the X86 state.
 *)
let flag_state = "x86.state"
let flag_asm_comments_fir  = flag_state ^ ".comments.fir"
let flag_asm_comments_mir  = flag_state ^ ".comments.mir"
let flag_asm_comments_text = flag_state ^ ".comments.text"

let () = std_flags_help_section_text flag_state
   "General flags which are used to control the X86 backend."

let () = std_flags_register_list_help flag_state
  [flag_asm_comments_fir,  FlagBool false,
                           "Print FIR comments in assembly code.";
   flag_asm_comments_mir,  FlagBool false,
                           "Print MIR comments in assembly code.";
   flag_asm_comments_text, FlagBool false,
                           "Print miscellaneous text comments in assembly code."]


(*
 * Include code in the ASM.
 *)
let asm_comments_fir ()  = std_flags_get_bool flag_asm_comments_fir
let asm_comments_mir ()  = std_flags_get_bool flag_asm_comments_mir
let asm_comments_text () = std_flags_get_bool flag_asm_comments_text


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
