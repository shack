(*
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Frame_type

open X86_inst_type
open X86_frame_type

(*
 * Spill set gives translation for each of
 * the vars.
 *)
type spset
type reg_class = int
type prog = spset poly_prog
type cenv = reg_class SymbolTable.t

(* List of general-purpose registers *)
val registers : var list array
val registers_special : var list array

(* Costs of spilled-register memory operations *)
val def_cost : int
val use_cost : int
val mov_def_cost : int
val mov_use_cost : int

(* Get the standard argument list: pass the param count *)
val get_stdargs : atom_class list -> var arg list
val get_std_operands : atom_class list -> operand arg list

(* Save callee-save registers *)
val callee_header : inst Listbuf.t

(* Name of the "exit" function that returns a value to C *)
val exit_frame_label : label
val exit_label : label
val uncaught_exception_label : label
val gc_label : label
val copy_on_write_label : label

(* Translate externals *)
val builtin_lookup : string -> label

(* Seg faults *)
val other_fault : int
val index_fault : int
val fun_index_fault : int
val pointer_index_fault : int
val bounds_fault : int
val lower_bounds_fault : int
val upper_bounds_fault : int
val null_pointer_fault : int
val fun_arity_tag_fault : int
val fault_count : int
val seg_fault_labels : label array

(* Debug labels *)
val debug_alloc : int
val debug_let_poly : int
val debug_let_pointer : int
val debug_let_function : int
val debug_set_poly : int
val debug_set_pointer : int
val debug_set_function : int
val debug_pre_let_poly : int
val debug_pre_let_pointer : int
val debug_pre_let_function : int
val debug_pre_set_pointer : int
val debug_pre_set_function : int
val debug_count : int
val debug_labels : label array

(* Name of the register that holds the data segment *)
val context_base : var

(* Memory space *)
val mem_base : var
val mem_next : var
val mem_limit : var

(* Pointer table descriptors *)
val pointer_base : var
val pointer_next : var
val pointer_free : var
val pointer_size : var

(* Closure table descriptors *)
val function_base : var
val function_size : var

(* Minor heap *)
val minor_base : var
val minor_limit : var
val minor_roots : var
val next_root : var

(* Atomic pointers *)
val atomic_info : var
val atomic_next : var
val copy_point : var
val gc_point : var

(* Other globals *)
val migrate_data_label : var
val context_size_label : var
val header_base_label : var
val header_limit_label : var
val globals_base_label : var
val globals_limit_label : var
val globals_size_label : var
val globals_index_base_label : var
val globals_index_limit_label : var
val function_base_label : var
val function_limit_label : var
val function_size_label : var
val exit_tag_label : var
val uncaught_exception_tag_label : var
val format_stdargs_label : var
val debug_flags_label : var
val exit_arity_label : var
val uncaught_exception_arity_label : var

val import_vars : (var * string) list
val export_vars : (var * string) list

(* Spill set *)
val initial_spset : unit -> spset
val spset_spill : spset -> var -> var -> reg_class -> spset
val spset_label : spset -> var -> spset
val spset_clabel : spset -> var -> spset
val spset_int : spset -> var -> int -> spset
val spset_add : spset -> var -> var -> spset
val spset_lookup_int32 : spset -> var -> operand
val spset_lookup_float : spset -> var -> operand
val spset_lookup_mmx : spset -> var -> operand
val spset_size : spset -> spill_counts
val spset_spill_prog : prog -> prog

val symbol_of_int_normal_spill : int -> symbol
val symbol_of_int_stdarg_spill : int -> symbol
val symbol_of_float_normal_spill : int -> symbol
val symbol_of_float_stdarg_spill : int -> symbol
val symbol_of_mmx_normal_spill : int -> symbol
val symbol_of_mmx_stdarg_spill : int -> symbol
val symbol_of_global_spill : int -> symbol

(************************************************************************
 * EXTRA X86 LOCAL INFO
 ************************************************************************)

(*
 * Modules.
 *)
module FloatTable : Mc_map.McMap with type key = Float80.float80

(*
 * Maps.
 *)
type reg64_key = var * var
type reg64_map = reg64_key SymbolTable.t
type fval_key = label
type fval_map = fval_key FloatTable.t

val reg64_add : reg64_map -> var -> reg64_map
val reg64_add_hi_lo : reg64_map -> var -> var -> var -> reg64_map
val reg64_lookup : reg64_map -> Mir_pos.pos -> var -> reg64_key
val reg64_lookup_exn : reg64_map -> var -> reg64_key

val fvals_add : fval_map -> Float80.float80 -> fval_map
val fvals_lookup : label FloatTable.t -> Mir_pos.pos -> Float80.float80 -> operand

val offset_from_base : operand -> int -> operand

(*
 * Names of integer register.
 *)
val eax : reg
val ebx : reg
val ecx : reg
val edx : reg
val edi : reg
val esi : reg
val esp : reg
val ebp : reg
val iflags : reg  (* Integer flags register *)

(*
 * Names of floating-point registers.  The names are
 * indexed with 0 representing the top-of-stack when
 * NO pending floating-point operations are working on
 * the stack.  The translation from these names to
 * FPStack is fickle because the top-of-stack does
 * move around on us...  two positions in the stack
 * must be reserved for current floating-point ops.
 *)
val fp0 : reg
val fp1 : reg
val fp2 : reg
val fp3 : reg
val fp4 : reg
val fp5 : reg
val fflags : reg  (* Floating-point control word/flags *)
val fpmap : (reg * int) list

(*
 * An extra int64 for holding temporary values.
 *)
val float_reg_1 : reg
val float_reg_2 : reg

(*
 * Register classes.
 *)
val int_reg_class : int
val float_reg_class : int
val mmx_reg_class : int
val reg_class_count : int

(*
 * Printer needs register _numbers_.
 *)
val get_gdb_register_number : reg -> int

(*
 * Space required by context struct.
 *)
val context_vars : symbol list
val context_length : int

(*
 * Get info about operands.
 *)
val src_regs : operand -> reg list -> reg list
val dst_regs : operand -> reg list -> reg list
val dst_labels : operand -> label list -> label list

(*
 * Generate code for simple operations.
 *)

val code_load_int8   : reg -> src -> inst
val code_load_int16  : reg -> src -> inst
val code_load_int32  : reg -> src -> inst
val code_load_uint8  : reg -> src -> inst
val code_load_uint16 : reg -> src -> inst
val code_load_uint32 : reg -> src -> inst

val code_store_int8   : dst -> reg -> inst
val code_store_int16  : dst -> reg -> inst
val code_store_int32  : dst -> reg -> inst
val code_store_uint8  : dst -> reg -> inst
val code_store_uint16 : dst -> reg -> inst
val code_store_uint32 : dst -> reg -> inst

(*
 * Extra label need for runtime.
 *)
val bin_fir_label : label
val migrate_fun_label : label
val atomic_fun_label : label
val atomic_rollback_fun_label : label
val atomic_commit_fun_label : label

(*
 * Extra spill operations.
 *)
val spset_iter : (var -> operand -> unit) -> spset -> unit

(*
 * Extra code headers.
 *)
val callee_header_stack : int

(*
 * Runtime constants (to pass to MIR).
 *)

(* Hash value for pointer entries *)
val hash_index_label : label
val hash_fun_index_label : label
val index_shift32 : int32

(* Special tags *)
val rttd_tag : int

(* Pointer table masks *)
val ptable_pointer_mask : int32
val ptable_valid_bit_off : int

(* Size constants *)
val aggr_header_size : int
val aggr_header_size32 : int32
val block_header_size : int
val block_header_size32 : int32

(* Arity tag information *)
val exit_arity_tag : arity list
val uncaught_exception_arity_tag : arity list
val backend_arity_tags : arity list list

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
