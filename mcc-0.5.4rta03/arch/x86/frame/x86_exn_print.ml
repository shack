(*
 * Instruction printing for exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open X86_inst_type
open X86_state

let float_format = "%1.24Le"

(*
 * String name for a register.
 *)
let string_of_register r =
   "%" ^ Symbol.string_of_symbol r

(*
 * Print a raw int value.
 *)
let pp_print_rawint buf i =
   Format.pp_print_string buf (Rawint.to_string i)

(*
 * Rawfloat printing.
 *)
let pp_print_rawfloat buf x =
   Format.pp_print_string buf (Rawfloat.to_string x)

(*
 * Print an operand.
 *)
let rec string_of_offset = function
   OffNumber i ->
      Int32.to_string i
 | OffLabel l ->
      string_of_symbol l
 | OffUMinus off ->
      sprintf "(-%s)" (string_of_offset off)
 | OffUNot off ->
      sprintf "(~%s)" (string_of_offset off)
 | OffUAbs off ->
      sprintf "abs(%s)" (string_of_offset off)
 | OffULog2 off ->
      sprintf "lg (%s)" (string_of_offset off)
 | OffUPow2 off ->
      sprintf "pow (%s)" (string_of_offset off)

 | OffAdd (off1, off2) ->
      sprintf "(%s + %s)" (string_of_offset off1) (string_of_offset off2)
 | OffSub (off1, off2) ->
      sprintf "(%s - %s)" (string_of_offset off1) (string_of_offset off2)
 | OffRel (off, label) ->
      sprintf "(%s.rel.%s)" (string_of_offset off) (string_of_symbol label)
 | OffMul (off1, off2) ->
      sprintf "(%s * %s)" (string_of_offset off1) (string_of_offset off2)
 | OffDiv (off1, off2) ->
      sprintf "(%s / %s)" (string_of_offset off1) (string_of_offset off2)
 | OffSal (off1, off2) ->
      sprintf "(%s << %s)" (string_of_offset off1) (string_of_offset off2)
 | OffShr (off1, off2) ->
      sprintf "(%s >>l %s)" (string_of_offset off1) (string_of_offset off2)
 | OffSar (off1, off2) ->
      sprintf "(%s >>a %s)" (string_of_offset off1) (string_of_offset off2)
 | OffAnd (off1, off2) ->
      sprintf "(%s & %s)" (string_of_offset off1) (string_of_offset off2)
 | OffOr  (off1, off2) ->
      sprintf "(%s | %s)" (string_of_offset off1) (string_of_offset off2)
 | OffXor  (off1, off2) ->
      sprintf "(%s ^ %s)" (string_of_offset off1) (string_of_offset off2)

let string_of_operand = function
   ImmediateNumber off ->
      "$" ^ string_of_offset off
 | ImmediateLabel l ->
      string_of_symbol l
 | Register r ->
      string_of_register r
 | FloatRegister r ->
      "/*float*/" ^ (string_of_register r)
 | FPStack i ->
      "%st(" ^ (Int32.to_string i) ^ ")"
 | MMXRegister r ->
      string_of_register r
 | MemReg r ->
      sprintf "(%s)" (string_of_register r)
 | MemRegOff (r, i) ->
      sprintf "%s(%s)" (string_of_offset i) (string_of_register r)
 | MemRegRegOffMul (r1, r2, i1, i2) ->
      sprintf "%s(%s,%s,%s)" (**)
         (string_of_offset i1)
         (string_of_register r1)
         (string_of_register r2)
         (Int32.to_string i2)
 | SpillRegister (r, off) ->
      sprintf "/*%s*/%s(frame)" (**)
         (string_of_register r)
         (string_of_offset off)

let pp_print_operand buf op =
   Format.pp_print_string buf (string_of_operand op)

let pp_print_operand_byte buf = function
   ImmediateNumber (OffNumber i) ->
      Format.fprintf buf "$0x%02lx" i
 | op ->
      pp_print_operand buf op

let pp_print_operand_word buf = function
   ImmediateNumber (OffNumber i) ->
      Format.fprintf buf "$0x%04lx" i
 | op ->
      pp_print_operand buf op

(*
 * Print conditional jump.
 *)
let get_cc_op = function
   LT  -> "l"
 | LE  -> "le"
 | EQ  -> "e"
 | NEQ -> "ne"
 | GT  -> "g"
 | GE  -> "ge"
 | ULT -> "b"
 | ULE -> "be"
 | UGT -> "a"
 | UGE -> "ae"

let pp_print_jcc buf op =
   Format.fprintf buf "j%s" (get_cc_op op)

let pp_print_set buf op =
   Format.fprintf buf "set%s" (get_cc_op op)

(*
 * Print an instruction
 *)
let pp_print_inst_iprec buf iprec =
   match iprec with
      IB -> Format.pp_print_string buf "b"
    | IW -> Format.pp_print_string buf "w"
    | IL -> Format.pp_print_string buf "l"

let pp_print_inst_fprec buf fprec =
   match fprec with
      FS -> Format.pp_print_string buf "s"
    | FL -> Format.pp_print_string buf "l"
    | FT -> Format.pp_print_string buf "t"

let pp_print_operand_iprec buf iprec =
   match iprec with
      IB -> pp_print_operand_byte buf
    | IW -> pp_print_operand_word buf
    | IL -> pp_print_operand buf

let pp_print_inst_iop1 buf name pre op1 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      Format.fprintf buf "\t%s%a\t%a" name pp_print_inst_iprec pre pp_print_operand op1

let pp_print_inst_iop2 buf name pre op1 op2 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      Format.fprintf buf "\t%s%a\t%a, %a" name pp_print_inst_iprec pre pp_print_operand op1 pp_print_operand op2

let pp_print_inst_iop2_shift buf name pre op1 op2 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      Format.fprintf buf "\t%s%a\t%a, %a" name pp_print_inst_iprec pre pp_print_operand_byte op1 pp_print_operand op2

let pp_print_inst_iop3_shift buf name pre op1 op2 op3 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      Format.fprintf buf "\t%s%a\t%a, %a, %a" name pp_print_inst_iprec pre pp_print_operand_byte op1 pp_print_operand op2 pp_print_operand op3

let rec pp_print_inst buf inst =
   match inst with
      MOV (pre, dst, src)
    | MOVS (IL as pre, dst, src)
    | MOVZ (IL as pre, dst, src) ->
         pp_print_inst_iop2 buf "mov" pre src dst
    | MOVS (IB, dst, src) ->
         Format.fprintf buf "\tmovsbl\t%a, %an" pp_print_operand_byte src pp_print_operand dst
    | MOVS (IW, dst, src) ->
         Format.fprintf buf "\tmovswl\t%a, %a" pp_print_operand_word src pp_print_operand dst
    | MOVZ (IB, dst, src) ->
         Format.fprintf buf "\tmovzbl\t%a, %a" pp_print_operand_byte src pp_print_operand dst
    | MOVZ (IW, dst, src) ->
         Format.fprintf buf "\tmovzwl\t%a, %a" pp_print_operand_word src pp_print_operand dst
    | RMOVS pre ->
         Format.fprintf buf "\trep";
         Format.fprintf buf "\tmovs%a" pp_print_inst_iprec pre
    | NEG (pre, dst) ->
         pp_print_inst_iop1 buf "neg" pre dst
    | NOT (pre, dst) ->
         pp_print_inst_iop1 buf "not" pre dst
    | INC (pre, dst) ->
         pp_print_inst_iop1 buf "inc" pre dst
    | DEC (pre, dst) ->
         pp_print_inst_iop1 buf "dec" pre dst
    | LEA (pre, dst, src) ->
         pp_print_inst_iop2 buf "lea" pre src dst
    | ADD (pre, dst, src) ->
         pp_print_inst_iop2 buf "add" pre src dst
    | ADC (pre, dst, src) ->
         pp_print_inst_iop2 buf "adc" pre src dst
    | SUB (pre, dst, src) ->
         pp_print_inst_iop2 buf "sub" pre src dst
    | SBB (pre, dst, src) ->
         pp_print_inst_iop2 buf "sbb" pre src dst
    | MUL (pre, dst) ->
         pp_print_inst_iop1 buf "mul" pre dst
    | IMUL (pre, dst, src) ->
         pp_print_inst_iop2 buf "imul" pre src dst
    | DIV (pre, dst) ->
         pp_print_inst_iop1 buf "div" pre dst
    | IDIV (pre, dst) ->
         pp_print_inst_iop1 buf "idiv" pre dst
    | CLTD ->
         Format.fprintf buf "\tcltd"
    | AND (pre, dst, src) ->
         pp_print_inst_iop2 buf "and" pre src dst
    | OR (pre, dst, src) ->
         pp_print_inst_iop2 buf "or" pre src dst
    | XOR (pre, dst, src) ->
         pp_print_inst_iop2 buf "xor" pre src dst
    | SAL (pre, dst, src) ->
         pp_print_inst_iop2_shift buf "sal" pre src dst
    | SAR (pre, dst, src) ->
         pp_print_inst_iop2_shift buf "sar" pre src dst
    | SHL (pre, dst, src) ->
         pp_print_inst_iop2_shift buf "shl" pre src dst
    | SHR (pre, dst, src) ->
         pp_print_inst_iop2_shift buf "shr" pre src dst
    | SHLD (pre, dst, src, shift) ->
         pp_print_inst_iop3_shift buf "shld" pre shift src dst
    | SHRD (pre, dst, src, shift) ->
         pp_print_inst_iop3_shift buf "shrd" pre shift src dst

    | MOVD (dst, src) ->
         Format.fprintf buf "\tmovd\t%a, %a\n" pp_print_operand src pp_print_operand dst
    | MOVQ (dst, src) ->
         Format.fprintf buf "\tmovq\t%a, %a\n" pp_print_operand src pp_print_operand dst

    | FINIT ->
         Format.fprintf buf "\tfinit"
    | FSTCW dst ->
         Format.fprintf buf "\tfstcw\t%a\n" pp_print_operand dst
    | FLDCW src ->
         Format.fprintf buf "\tfldcw\t%a\n" pp_print_operand src
    | FMOV (dpre, dst, spre, src) ->
         Format.fprintf buf "\t# FMOV";
         Format.fprintf buf "\tfld%a\t%a" pp_print_inst_fprec spre pp_print_operand src;
         Format.fprintf buf "\tfst%a\t%a" pp_print_inst_fprec dpre pp_print_operand dst
    | FMOVP (dpre, dst, spre, src) ->
         Format.fprintf buf "\t# MOVP";
         Format.fprintf buf "\tfld%a\t%a"  pp_print_inst_fprec spre pp_print_operand src;
         Format.fprintf buf "\tfstp%a\t%a" pp_print_inst_fprec dpre pp_print_operand dst
    | FLD (_, FPStack i) ->
         Format.fprintf buf "\tfld\t%a" pp_print_operand (FPStack i)
    | FLD (pre, src) ->
         Format.fprintf buf "\tfld%a\t%a" pp_print_inst_fprec pre pp_print_operand src
    | FILD src ->
         Format.fprintf buf "\tfildl\t%a" pp_print_operand src
    | FILD64 src ->
         Format.fprintf buf "\tfildq\t%a" pp_print_operand src
    | FST (_, FPStack i) ->
         Format.fprintf buf "\tfst\t%a" pp_print_operand (FPStack i)
    | FST (pre, dst) ->
         Format.fprintf buf "\tfst%a\t%a" pp_print_inst_fprec pre pp_print_operand dst
    | FSTP (_, FPStack i) ->
         Format.fprintf buf "\tfstp\t%a" pp_print_operand (FPStack i)
    | FSTP (pre, dst) ->
         Format.fprintf buf "\tfstp%a\t%a" pp_print_inst_fprec pre pp_print_operand dst
    | FIST dst ->
         Format.fprintf buf "\tfistl\t%a" pp_print_operand dst
    | FISTP dst ->
         Format.fprintf buf "\tfistpl\t%a" pp_print_operand dst
    | FISTP64 dst ->
         Format.fprintf buf "\tfistpq\t%a" pp_print_operand dst
    | FXCH (FPStack i) when i = Int32.one ->
         Format.fprintf buf "\tfxch"
    | FXCH dst ->
         Format.fprintf buf "\tfxch\t%a" pp_print_operand dst
    | FCHS ->
         Format.fprintf buf "\tfchs"
    | FADDP ->
         Format.fprintf buf "\tfaddp"
    | FSUBP ->
         Format.fprintf buf "\tfsubp"
    | FMULP ->
         Format.fprintf buf "\tfmulp"
    | FDIVP ->
         Format.fprintf buf "\tfdivp"
    | FSUBRP ->
         Format.fprintf buf "\tfsubrp"
    | FDIVRP ->
         Format.fprintf buf "\tfdivrp"
    | FADD (dst, src) ->
         Format.fprintf buf "\tfadd\t%a, %a" pp_print_operand src pp_print_operand dst
    | FSUB (dst, src) ->
         Format.fprintf buf "\tfsub\t%a, %a" pp_print_operand src pp_print_operand dst
    | FMUL (dst, src) ->
         Format.fprintf buf "\tfmul\t%a, %a" pp_print_operand src pp_print_operand dst
    | FDIV (dst, src) ->
         Format.fprintf buf "\tfdiv\t%a, %a" pp_print_operand src pp_print_operand dst
    | FSUBR (dst, src) ->
         Format.fprintf buf "\tfsubr\t%a, %a" pp_print_operand src pp_print_operand dst
    | FDIVR (dst, src) ->
         Format.fprintf buf "\tfdivr\t%a, %a" pp_print_operand src pp_print_operand dst
    | FPREM ->
         Format.fprintf buf "\tfprem"
    | FSIN ->
         Format.fprintf buf "\tfsin"
    | FCOS ->
         Format.fprintf buf "\tfcos"
    | FPATAN ->
         Format.fprintf buf "\tfpatan"
    | FSQRT ->
         Format.fprintf buf "\tfsqrt"
    | FUCOM ->
         Format.fprintf buf "\tfucom"
    | FUCOMP ->
         Format.fprintf buf "\tfucomp"
    | FUCOMPP ->
         Format.fprintf buf "\tfucompp"
    | FSTSW ->
         Format.fprintf buf "\tfstsw\t%%ax"
    | PUSH (pre, src) ->
         pp_print_inst_iop1 buf "push" pre src
    | POP (pre, dst) ->
         pp_print_inst_iop1 buf "pop" pre dst
    | CALL src
    | FCALL src ->
         Format.fprintf buf "\tcall\t%a" pp_print_operand src
    | RET ->
         Format.fprintf buf "\tret"
    | CMP (pre, src1, src2) ->
         pp_print_inst_iop2 buf "cmp" pre src2 src1
    | TEST (pre, src1, src2) ->
         pp_print_inst_iop2 buf "test" pre src2 src1
    | JMP dst
    | IJMP (_, dst) ->
         (match dst with
             ImmediateLabel _ ->
                Format.fprintf buf "\tjmp\t%a" pp_print_operand dst
           | _ ->
                Format.fprintf buf "\tjmp\t*%a" pp_print_operand dst)
    | FJMP dst ->
         (* These are "fake" jumps *)
         (match dst with
             ImmediateLabel _ ->
                Format.fprintf buf "#\tjmp\t%a" pp_print_operand dst
           | _ ->
                Format.fprintf buf "#\tjmp\t*%a" pp_print_operand dst)
    | JCC (op, dst) ->
         (match dst with
             ImmediateLabel _ ->
                Format.fprintf buf "\t%a\t%a" pp_print_jcc op pp_print_operand dst
           | _ ->
                Format.fprintf buf "\t%a\t*%a" pp_print_jcc op pp_print_operand dst)
    | SET (op, dst) ->
         Format.fprintf buf "\t%a\t%a" pp_print_set op pp_print_operand_byte dst
    | RES (label, _, mem, ptr) ->
         Format.fprintf buf "\tpushl\t%a" pp_print_operand label;
         Format.fprintf buf "\tpushl\t%a" pp_print_operand mem;
         Format.fprintf buf "\tpushl\t%a" pp_print_operand ptr;
         Format.fprintf buf "\tcall\t__gc";
         Format.fprintf buf "\taddl\t$12, %%esp"
    | COW (label, _, mem, ptr, copy_ptr) ->
         Format.fprintf buf "\tpushl\t%a" pp_print_operand copy_ptr;
         Format.fprintf buf "\tpushl\t%a" pp_print_operand label;
         Format.fprintf buf "\tpushl\t%a" pp_print_operand mem;
         Format.fprintf buf "\tpushl\t%a" pp_print_operand ptr;
         Format.fprintf buf "\tcall\t__copy_on_write";
         Format.fprintf buf "\taddl\t$16, %%esp"
    | RESP (label, _) ->
         Format.fprintf buf "\tpushl\t%a\n" pp_print_operand label;
         Format.fprintf buf "\tpushl\t$0\n";
         Format.fprintf buf "\tpushl\t$0\n";
         Format.fprintf buf "\tpushl\t$0\n";
         Format.fprintf buf "\tpushl\t%%ebp\n";
         Format.fprintf buf "\tpushl\t%%esi\n";
         Format.fprintf buf "\tpushl\t%%edx\n";
         Format.fprintf buf "\tpushl\t%%ecx\n";
         Format.fprintf buf "\tpushl\t%%ebx\n";
         Format.fprintf buf "\tpushl\t%%eax\n";
         Format.fprintf buf "\tpushl\t%%esp\n"
    | CommentFIR exp ->
         if asm_comments_fir () then
            Format.fprintf buf "@[<v 3>/* FIR:@ %a@]@ */" (Fir_print.pp_print_expr_size 1) exp
    | CommentMIR exp ->
         if asm_comments_mir () then
            Format.fprintf buf "@[<v 3>/* MIR:@ %a@]@ */" (Mir_print.pp_print_expr_size 1) exp
    | CommentString s ->
         if asm_comments_text () then
            Format.fprintf buf "\t# %s" s
    | CommentInst (s, inst) ->
         if asm_comments_text () then
            Format.fprintf buf "\t/* %s:\t%a\t*/" s pp_print_inst inst
    | NOP ->
         Format.fprintf buf "\tnop"

(*
 * Rename the functions.
 *)
let format_operand = pp_print_operand
let format_inst = pp_print_inst

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
