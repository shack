(*
 * Assembly printout.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mc_string_util
open Format
open Symbol
open Location

open Fir_set
open Fir_marshal

open Frame_type
open Sizeof_const
open Sizeof_type

open Mir_arity_map

open X86_pos
open X86_exn
open X86_stab
open X86_state
open X86_frame
open X86_runtime
open X86_inst_type
open X86_print_util
open X86_frame_type

let float_format = "%1.24Le"

module Pos = MakePos (struct let name = "X86_print" end)
open Pos

(************************************************************************
 * CODE PRINTING
 ************************************************************************)

(*
 * Alignment.
 *)
let do_align pos bfd_align =
   let bfd_align = pred bfd_align in
      (pos + bfd_align) land (lnot bfd_align)

(*
 * Size of a global.
 *)
let sizeof_init_exp = function
   InitRawInt i ->
      sizeof_rawint (Rawint.precision i)
 | InitRawFloat x ->
      sizeof_rawfloat (Rawfloat.precision x)
 | InitTagBlock _
 | InitAggrBlock _
 | InitRawData _
 | InitVar _ ->
      align_pointer, sizeof_pointer

(*
 * Print a file class.
 *)
let pp_print_file_class out fclass =
   let i =
      match fclass with
         Fir.FileFC -> file_fc
       | Fir.FileJava -> file_java
       | Fir.FileNaml -> file_naml
       | Fir.FileAml -> file_aml
   in
      fprintf out "%s" (Int32.to_string i)

(*
 * Print a raw int value.
 *)
let length_of_rawint i =
   match Rawint.precision i with
      Rawint.Int8 -> 1
    | Rawint.Int16 -> 2
    | Rawint.Int32 -> 4
    | Rawint.Int64 -> 8

let pp_print_rawint out i =
   match Rawint.precision i with
      Rawint.Int8 -> fprintf out "\t.byte\t%s\n" (Rawint.to_string i)
    | Rawint.Int16 -> fprintf out "\t.value\t%s\n" (Rawint.to_string i)
    | Rawint.Int32 -> fprintf out "\t.long\t%s\n" (Rawint.to_string i)
    | Rawint.Int64 ->
         let i = Rawint.to_int64 i in
         let lo = Int64.logand i (Int64.shift_right_logical (Int64.of_int (-1)) 32) in
         let hi = Int64.shift_right_logical i 32 in
            fprintf out "\t.long\t%s\n" (Int64.to_string lo);
            fprintf out "\t.long\t%s\n" (Int64.to_string hi)

(*
 * Rawfloat printing.
 *)
let length_of_rawfloat x =
   match Rawfloat.precision x with
      Rawfloat.Single -> 4
    | Rawfloat.Double -> 8
    | Rawfloat.LongDouble -> 12

let pp_print_rawfloat out x =
   let s =
      match Rawfloat.precision x with
         Rawfloat.Single -> ".float"
       | Rawfloat.Double -> ".double"
       | Rawfloat.LongDouble -> ".tfloat"
   in
      fprintf out "\t%s\t%s\n" s (Rawfloat.to_string x)

(*
 * Print an operand.
 *)
let rec string_of_offset = function
   OffNumber i ->
      Printf.sprintf "0x%lx" i
 | OffLabel l ->
      string_of_symbol l
 | OffUMinus off ->
      sprintf "(-%s)" (string_of_offset off)
 | OffUNot off ->
      sprintf "(~%s)" (string_of_offset off)
 | OffUAbs off ->
      sprintf "abs(%s)" (string_of_offset off)
 | OffULog2 off ->
      sprintf "lg (%s)" (string_of_offset off)
 | OffUPow2 off ->
      sprintf "pow (%s)" (string_of_offset off)

 | OffAdd (off1, off2) ->
      sprintf "(%s + %s)" (string_of_offset off1) (string_of_offset off2)
 | OffSub (off1, off2) ->
      sprintf "(%s - %s)" (string_of_offset off1) (string_of_offset off2)
 | OffRel (off, label) ->
      sprintf "(%s - %s)" (string_of_offset off) (string_of_symbol label)
 | OffMul (off1, off2) ->
      sprintf "(%s * %s)" (string_of_offset off1) (string_of_offset off2)
 | OffDiv (off1, off2) ->
      sprintf "(%s / %s)" (string_of_offset off1) (string_of_offset off2)
 | OffSal (off1, off2) ->
      sprintf "(%s << %s)" (string_of_offset off1) (string_of_offset off2)
 | OffShr (off1, off2) ->
      sprintf "(%s >>l %s)" (string_of_offset off1) (string_of_offset off2)
 | OffSar (off1, off2) ->
      sprintf "(%s >>a %s)" (string_of_offset off1) (string_of_offset off2)
 | OffAnd (off1, off2) ->
      sprintf "(%s & %s)" (string_of_offset off1) (string_of_offset off2)
 | OffOr  (off1, off2) ->
      sprintf "(%s | %s)" (string_of_offset off1) (string_of_offset off2)
 | OffXor  (off1, off2) ->
      sprintf "(%s ^ %s)" (string_of_offset off1) (string_of_offset off2)

let string_of_operand = function
   ImmediateNumber off ->
      "$" ^ string_of_offset off
 | ImmediateLabel l ->
      string_of_symbol l
 | Register r ->
      string_of_register r
 | FloatRegister r ->
      "/*float*/" ^ (string_of_register r)
 | FPStack i ->
      "%st(" ^ (Int32.to_string i) ^ ")"
 | MMXRegister r ->
      string_of_register r
 | MemReg r ->
      sprintf "(%s)" (string_of_register r)
 | MemRegOff (r, i) ->
      sprintf "%s(%s)" (string_of_offset i) (string_of_register r)
 | MemRegRegOffMul (r1, r2, i1, i2) ->
      sprintf "%s(%s,%s,%s)" (**)
         (string_of_offset i1)
         (string_of_register r1)
         (string_of_register r2)
         (Int32.to_string i2)
 | SpillRegister (r, off) ->
      let s = sprintf "%s(%%ebp)" (string_of_offset off) in
         sprintf "/*%s*/%s" (string_of_symbol r) s

let pp_print_operand out op =
   pp_print_string out (string_of_operand op)

let pp_print_padded_operand out len op =
   pp_print_padded_string out len (string_of_operand op)

let pp_print_padded_operand len out op =
   pp_print_padded_string len out (string_of_operand op)

let pp_print_reserve_operand out op =
   match op with
      ResBase op ->
         pp_print_operand out op
    | ResInfix (op1, op2) ->
         fprintf out "<%a,%a>" pp_print_operand op1 pp_print_operand op2

let pp_print_operand_byte out = function
   Register r when r = eax -> fprintf out "%%al"
 | Register r when r = ebx -> fprintf out "%%bl"
 | Register r when r = ecx -> fprintf out "%%cl"
 | Register r when r = edx -> fprintf out "%%dl"
 | ImmediateNumber (OffNumber i) -> pp_print_string out ("$0x" ^ Int32.format "%02x" i)
 | _ as op                 -> pp_print_operand out op

let pp_print_operand_word out = function
   Register r when r = eax -> fprintf out "%%ax"
 | Register r when r = ebx -> fprintf out "%%bx"
 | Register r when r = ecx -> fprintf out "%%cx"
 | Register r when r = edx -> fprintf out "%%dx"
 | Register r when r = esi -> fprintf out "%%si"
 | Register r when r = edi -> fprintf out "%%di"
 | ImmediateNumber (OffNumber i) -> pp_print_string out ("$0x" ^ Int32.format "%04x" i)
 | _ as op                 -> pp_print_operand out op

(*
 * Print conditional jump.
 *)
let get_cc_op = function
   LT  -> "l"
 | LE  -> "le"
 | EQ  -> "e"
 | NEQ -> "ne"
 | GT  -> "g"
 | GE  -> "ge"
 | ULT -> "b"
 | ULE -> "be"
 | UGT -> "a"
 | UGE -> "ae"

let pp_print_jcc out op =
   fprintf out "j%s" (get_cc_op op)

let pp_print_set out op =
   fprintf out "set%s" (get_cc_op op)

(*
 * Print the variable in a reserve.
 * BUG: this is borkne for now.  Jyh will fix it.
 *)
let pp_print_reserve out label globals vars =
   let rec collect_regs = function
      arg :: args ->
         (match arg with
             ResBase (Register r) ->
                r :: collect_regs args
           | _ ->
                collect_regs args)
    | [] ->
         []
   in
   let rec collect_off = function
      arg :: args ->
         (match arg with
             ResBase (SpillRegister (_, off)) ->
                off :: collect_off args
           | ResBase (Register _)
           | ResInfix _ ->
                collect_off args
           | _ ->
                raise (Failure "pp_print_reserve buf"))
    | [] ->
         []
   in
   let regs = collect_regs vars in
   let offs = collect_off vars in

   (* Get the byte mask for the registers *)
   let rec reg_mask i mask = function
      r :: regs' ->
         let mask =
            if List.mem r regs then
               mask lor (1 lsl i)
            else
               mask
         in
            reg_mask (succ i) mask regs'
    | [] ->
         mask
   in
   let mask = reg_mask 0 0 registers.(int_reg_class) in
      (* Print a mask of the register numbers that contain pointers *)
      fprintf out "\t.bfd_align\t4\n";
      fprintf out "%a:\n" pp_print_symbol label;
      fprintf out "\t/* Regs:";
      List.iter (fun r -> fprintf out " %a" pp_print_register r) regs;
      fprintf out " */\n";
      fprintf out "\t.long\t0x%08x\n" mask;

      (* Print the names of the spills that contain pointers *)
      List.iter (fun off ->
            fprintf out "\t.long\t%s\n" (string_of_offset off)) offs;

      (* Print the name of the globals that contain pointers *)
      List.iter (fun v ->
            fprintf out "\t.long\t%a\n" pp_print_symbol v) globals;

      (* Terminate the list *)
      fprintf out "\t.long\t-1\n"

let pp_print_reserve out globals = function
   RES  (ImmediateLabel label, vars, _, _)
 | RES  (ImmediateNumber (OffLabel label), vars, _, _)
 | RESP (ImmediateLabel label, vars)
 | RESP (ImmediateNumber (OffLabel label), vars)
 | COW  (ImmediateLabel label, vars, _, _, _)
 | COW  (ImmediateNumber (OffLabel label), vars, _, _, _) ->
      pp_print_reserve out label globals vars
 | _ ->
      ()

let pp_print_reserve_block out globals { block_code = insts } =
   List.iter (pp_print_reserve out globals) insts

let pp_print_reserve_blocks out globals blocks =
   Trace.iter (pp_print_reserve_block out globals) blocks

(*
 * Print an instruction
 *)
let pp_print_inst_iprec out = function
   IB -> fprintf out "b"
 | IW -> fprintf out "w"
 | IL -> fprintf out "l"

let pp_print_inst_fprec out = function
   FS -> fprintf out "s"
 | FL -> fprintf out "l"
 | FT -> fprintf out "t"

let pp_print_operand_iprec buf = function
   IB -> pp_print_operand_byte buf
 | IW -> pp_print_operand_word buf
 | IL -> pp_print_operand buf

let pp_print_inst_iop1 out name pre op1 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      fprintf out "\t%s%a\t%a\n" name pp_print_inst_iprec pre pp_print_operand op1

let pp_print_inst_iop2 out name pre op1 op2 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      fprintf out "\t%s%a\t%a, %a\n" name pp_print_inst_iprec pre pp_print_operand op1 pp_print_operand op2

let pp_print_inst_iop2_shift out name pre op1 op2 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      fprintf out "\t%s%a\t%a, %a\n" name pp_print_inst_iprec pre pp_print_operand_byte op1 pp_print_operand op2

let pp_print_inst_iop3_shift out name pre op1 op2 op3 =
   let pp_print_operand buf = pp_print_operand_iprec buf pre in
      fprintf out "\t%s%a\t%a, %a, %a\n" name pp_print_inst_iprec pre pp_print_operand_byte op1 pp_print_operand op2 pp_print_operand op3

let rec pp_print_inst out = function
   MOV (pre, dst, src)
 | MOVS (IL as pre, dst, src)
 | MOVZ (IL as pre, dst, src) ->
      pp_print_inst_iop2 out "mov" pre src dst
 | MOVS (IB, dst, src) ->
      fprintf out "\tmovsbl\t%a, %a\n" pp_print_operand_byte src pp_print_operand dst
 | MOVS (IW, dst, src) ->
      fprintf out "\tmovswl\t%a, %a\n" pp_print_operand_word src pp_print_operand dst
 | MOVZ (IB, dst, src) ->
      fprintf out "\tmovzbl\t%a, %a\n" pp_print_operand_byte src pp_print_operand dst
 | MOVZ (IW, dst, src) ->
      fprintf out "\tmovzwl\t%a, %a\n" pp_print_operand_word src pp_print_operand dst
 | RMOVS pre ->
      fprintf out "\trep\tmovs%a\n" pp_print_inst_iprec pre
 | NEG (pre, dst) ->
      pp_print_inst_iop1 out "neg" pre dst
 | NOT (pre, dst) ->
      pp_print_inst_iop1 out "not" pre dst
 | INC (pre, dst) ->
      pp_print_inst_iop1 out "inc" pre dst
 | DEC (pre, dst) ->
      pp_print_inst_iop1 out "dec" pre dst
 | LEA (pre, dst, src) ->
      pp_print_inst_iop2 out "lea" pre src dst
 | ADD (pre, dst, src) ->
      pp_print_inst_iop2 out "add" pre src dst
 | ADC (pre, dst, src) ->
      pp_print_inst_iop2 out "adc" pre src dst
 | SUB (pre, dst, src) ->
      pp_print_inst_iop2 out "sub" pre src dst
 | SBB (pre, dst, src) ->
      pp_print_inst_iop2 out "sbb" pre src dst
 | MUL (pre, dst) ->
      pp_print_inst_iop1 out "mul" pre dst
 | IMUL (pre, dst, src) ->
      pp_print_inst_iop2 out "imul" pre src dst
 | DIV (pre, dst) ->
      pp_print_inst_iop1 out "div" pre dst
 | IDIV (pre, dst) ->
      pp_print_inst_iop1 out "idiv" pre dst
 | CLTD ->
      fprintf out "\tcltd\n"
 | AND (pre, dst, src) ->
      pp_print_inst_iop2 out "and" pre src dst
 | OR (pre, dst, src) ->
      pp_print_inst_iop2 out "or" pre src dst
 | XOR (pre, dst, src) ->
      pp_print_inst_iop2 out "xor" pre src dst
 | SAL (pre, dst, src) ->
      pp_print_inst_iop2_shift out "sal" pre src dst
 | SAR (pre, dst, src) ->
      pp_print_inst_iop2_shift out "sar" pre src dst
 | SHL (pre, dst, src) ->
      pp_print_inst_iop2_shift out "shl" pre src dst
 | SHR (pre, dst, src) ->
      pp_print_inst_iop2_shift out "shr" pre src dst
 | SHLD (pre, dst, src, shift) ->
      pp_print_inst_iop3_shift out "shld" pre shift src dst
 | SHRD (pre, dst, src, shift) ->
      pp_print_inst_iop3_shift out "shrd" pre shift src dst

 | MOVD (dst, src) ->
      fprintf out "\tmovd\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | MOVQ (dst, src) ->
      fprintf out "\tmovq\t%a, %a\n" pp_print_operand src pp_print_operand dst

 | FINIT ->
      fprintf out "\tfinit\n"
 | FSTCW dst ->
      fprintf out "\tfstcw\t%a\n" pp_print_operand dst
 | FLDCW src ->
      fprintf out "\tfldcw\t%a\n" pp_print_operand src
 | FMOV (dpre, dst, spre, src) ->
      fprintf out "\t# FMOV\n";
      fprintf out "\tfld%a\t%a\n" pp_print_inst_fprec spre pp_print_operand src;
      fprintf out "\tfst%a\t%a\n" pp_print_inst_fprec dpre pp_print_operand dst
 | FMOVP (dpre, dst, spre, src) ->
      fprintf out "\t# MOVP\n";
      fprintf out "\tfld%a\t%a\n"  pp_print_inst_fprec spre pp_print_operand src;
      fprintf out "\tfstp%a\t%a\n" pp_print_inst_fprec dpre pp_print_operand dst
 | FLD (_, FPStack i) ->
      fprintf out "\tfld\t%a\n" pp_print_operand (FPStack i)
 | FLD (pre, src) ->
      fprintf out "\tfld%a\t%a\n" pp_print_inst_fprec pre pp_print_operand src
 | FILD src ->
      fprintf out "\tfildl\t%a\n" pp_print_operand src
 | FILD64 src ->
      fprintf out "\tfildq\t%a\n" pp_print_operand src
 | FST (_, FPStack i) ->
      fprintf out "\tfst\t%a\n" pp_print_operand (FPStack i)
 | FST (pre, dst) ->
      fprintf out "\tfst%a\t%a\n" pp_print_inst_fprec pre pp_print_operand dst
 | FSTP (_, FPStack i) ->
      fprintf out "\tfstp\t%a\n" pp_print_operand (FPStack i)
 | FSTP (pre, dst) ->
      fprintf out "\tfstp%a\t%a\n" pp_print_inst_fprec pre pp_print_operand dst
 | FIST dst ->
      fprintf out "\tfistl\t%a\n" pp_print_operand dst
 | FISTP dst ->
      fprintf out "\tfistpl\t%a\n" pp_print_operand dst
 | FISTP64 dst ->
      fprintf out "\tfistpq\t%a\n" pp_print_operand dst
 | FXCH (FPStack i) when i = Int32.one ->
      fprintf out "\tfxch\n"
 | FXCH dst ->
      fprintf out "\tfxch\t%a\n" pp_print_operand dst
 | FCHS ->
      fprintf out "\tfchs\n"
 | FADDP ->
      fprintf out "\tfaddp\n"
 | FSUBP ->
      fprintf out "\tfsubp\n"
 | FMULP ->
      fprintf out "\tfmulp\n"
 | FDIVP ->
      fprintf out "\tfdivp\n"
 | FSUBRP ->
      fprintf out "\tfsubrp\n"
 | FDIVRP ->
      fprintf out "\tfdivrp\n"
 | FADD (dst, src) ->
      fprintf out "\tfadd\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | FSUB (dst, src) ->
      fprintf out "\tfsub\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | FMUL (dst, src) ->
      fprintf out "\tfmul\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | FDIV (dst, src) ->
      fprintf out "\tfdiv\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | FSUBR (dst, src) ->
      fprintf out "\tfsubr\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | FDIVR (dst, src) ->
      fprintf out "\tfdivr\t%a, %a\n" pp_print_operand src pp_print_operand dst
 | FPREM ->
      fprintf out "\tfprem\n"
 | FSIN ->
      fprintf out "\tfsin\n"
 | FCOS ->
      fprintf out "\tfcos\n"
 | FPATAN ->
      fprintf out "\tfpatan\n"
 | FSQRT ->
      fprintf out "\tfsqrt\n"
 | FUCOM ->
      fprintf out "\tfucom\n"
 | FUCOMP ->
      fprintf out "\tfucomp\n"
 | FUCOMPP ->
      fprintf out "\tfucompp\n"
 | FSTSW ->
      fprintf out "\tfstsw\t%%ax\n"
 | PUSH (pre, src) ->
      pp_print_inst_iop1 out "push" pre src
 | POP (pre, dst) ->
      pp_print_inst_iop1 out "pop" pre dst
 | CALL src
 | FCALL src ->
      fprintf out "\tcall\t%a\n" pp_print_operand src
 | RET ->
      fprintf out "\tret\n"
 | CMP (pre, src1, src2) ->
      pp_print_inst_iop2 out "cmp" pre src2 src1
 | TEST (pre, src1, src2) ->
      pp_print_inst_iop2 out "test" pre src2 src1
 | JMP dst
 | IJMP (_, dst) ->
      (match dst with
          ImmediateLabel _ ->
             fprintf out "\tjmp\t%a\n" pp_print_operand dst
        | _ ->
             fprintf out "\tjmp\t*%a\n" pp_print_operand dst)
 | FJMP dst ->
      (match dst with
          ImmediateLabel _ ->
             fprintf out "\t#jmp\t%a\n" pp_print_operand dst
        | _ ->
             fprintf out "\t#jmp\t*%a\n" pp_print_operand dst)
 | JCC (op, dst) ->
      (match dst with
          ImmediateLabel _ ->
             fprintf out "\t%a\t%a\n" pp_print_jcc op pp_print_operand dst
        | _ ->
             fprintf out "\t%a\t*%a\n" pp_print_jcc op pp_print_operand dst)
 | SET (op, dst) ->
      fprintf out "\t%a\t%a\n" pp_print_set op pp_print_operand_byte dst
 | RES (label, _, mem, ptr) ->
      fprintf out "\tpushl\t%a\n" pp_print_operand label;
      fprintf out "\tpushl\t%a\n" pp_print_operand mem;
      fprintf out "\tpushl\t%a\n" pp_print_operand ptr;
      fprintf out "\tcall\t%a\n" pp_print_symbol gc_label;
      fprintf out "\taddl\t$12, %%esp\n"
 | COW (label, srcs, mem, ptr, copy_ptr) ->
      if asm_comments_text () then
         List.iter (fun src ->
               fprintf out "\t# src = %a\n" pp_print_reserve_operand src) srcs;
      fprintf out "\tpushl\t%a\n" pp_print_operand copy_ptr;
      fprintf out "\tpushl\t%a\n" pp_print_operand label;
      fprintf out "\tpushl\t%a\n" pp_print_operand mem;
      fprintf out "\tpushl\t%a\n" pp_print_operand ptr;
      fprintf out "\tcall\t%a\n" pp_print_symbol copy_on_write_label;
      fprintf out "\taddl\t$16, %%esp\n"
 | RESP (label, _) ->
      Format.fprintf out "\tpushl\t%a\n" pp_print_operand label;
      Format.fprintf out "\tpushl\t$0\n";
      Format.fprintf out "\tpushl\t$0\n";
      Format.fprintf out "\tpushl\t$0\n";
      Format.fprintf out "\tpushl\t%%ebp\n";
      Format.fprintf out "\tpushl\t%%edi\n";
      Format.fprintf out "\tpushl\t%%esi\n";
      Format.fprintf out "\tpushl\t%%edx\n";
      Format.fprintf out "\tpushl\t%%ecx\n";
      Format.fprintf out "\tpushl\t%%ebx\n";
      Format.fprintf out "\tpushl\t%%eax\n";
      Format.fprintf out "\tpushl\t%%esp\n"
 | CommentFIR exp ->
      if asm_comments_fir () then
         fprintf out "@[<v 3>/* FIR:@ %a@]@ */@." (Fir_print.pp_print_expr_size 1) exp
 | CommentMIR exp ->
      if asm_comments_mir () then
         fprintf out "@[<v 3>/* MIR:@ %a@]@ */@." (Mir_print.pp_print_expr_size 1) exp
 | CommentString s ->
      if asm_comments_text () then
         fprintf out "\t# %s\n" s
 | CommentInst (s, inst) ->
      if asm_comments_text () then
         fprintf out "\t/* %s:%a\t*/\n\t" s pp_print_inst inst
 | NOP ->
      fprintf out "\tnop\n"

(*
 * Print a code block.
 *)
let pp_print_code out
    { code_src = src;
      code_dst = dst;
      code_class = cclass;
      code_inst = inst;
    } =
   let pp_print_var_list vars =
      ignore (List.fold_left (fun flag v ->
                    if flag then
                       pp_print_string out ", ";
                    fprintf out "%s" (string_of_symbol v);
                    true) false vars)
   in
   let pp_print_code_class out cclass =
      match cclass with
         CodeNormal ->
            fprintf out "CodeNormal"
       | CodeMove ->
            fprintf out "CodeMove"
       | CodeJump l ->
            fprintf out "CodeJump %a" pp_print_symbol l
   in
      fprintf out "# src = ";
      pp_print_var_list src;
      fprintf out "\n# dst = ";
      pp_print_var_list dst;
      fprintf out "\n# class = %a\n" pp_print_code_class cclass;
      pp_print_inst out inst

let pp_print_live out
    { live_src = src;
      live_dst = dst;
      live_out = live;
      live_class = cclass;
      live_inst = inst;
    } =
   let pp_print_var_list vars =
      ignore (List.fold_left (fun flag v ->
                    if flag then
                       pp_print_string out ", ";
                    fprintf out "%s" (string_of_symbol v);
                    true) false vars)
   in
   let pp_print_var_set vars =
      ignore (SymbolSet.fold (fun flag v ->
                    if flag then
                       pp_print_string out ", ";
                    fprintf out "%s" (string_of_symbol v);
                    true) false vars)
   in
   let pp_print_code_class out cclass =
      match cclass with
         CodeNormal ->
            fprintf out "CodeNormal"
       | CodeMove ->
            fprintf out "CodeMove"
       | CodeJump l ->
            fprintf out "CodeJump %a" pp_print_symbol l
   in
      fprintf out "# src = ";
      pp_print_var_list src;
      fprintf out "\n# dst = ";
      pp_print_var_list dst;
      fprintf out "\n# live_out = ";
      pp_print_var_set live;
      fprintf out "\n# class = %a\n" pp_print_code_class cclass;
      pp_print_inst out inst

(*
 * Print a single block.
 *)
let pp_print_block pp_print_inst stab out
    { block_debug  = debug;
      block_label  = label;
      block_code   = code;
      block_index  = index;
      block_jumps  = jumps;
      block_align  = bfd_align;
      block_arity  = arity;
      block_opt    = opt;
    } =
   let () =
      if !Fir_state.debug_stab then
         pp_print_stab_block_header stab out false label debug
   in
   let () =
      let loc, _ = debug in
         fprintf out "# %a\n" pp_print_location loc
   in
   let () =
      if bfd_align then
         fprintf out "\t.bfd_align\t64,0x90\n"
   in
   let () =
      match index, arity with
         Some tag, Some arity ->
            fprintf out "\t.bfd_skip\t52,0x90\n";
            fprintf out "\t.long\t%a\t# Arity tag\n" pp_print_symbol arity;
            fprintf out "\t.long\t0x0\t# Block size 0\n";
            fprintf out "\t.long\t(%a | %s)\t# Closure index\n"
               pp_print_symbol tag (Int32.to_string X86_runtime.aggr_shift_mask32)
       | _ ->
            ()
   in
      (* Print the code *)
      fprintf out "# %s\n" (if opt then "Optimizable block" else "Unoptimizable block");
      fprintf out "%a:\n" pp_print_symbol label;
      List.iter (pp_print_inst out) code;

      (* Print jumps *)
      List.iter (fun v -> fprintf out "\t# Jumps to: %a\n" pp_print_symbol v) jumps;

      (* Print newline if there is code here *)
      if code <> [] then
         fprintf out "\n"

let pp_print_blocks stab out blocks =
   Trace.iter (pp_print_block pp_print_inst stab out) blocks

let pp_print_code_blocks stab out blocks =
   Trace.iter (pp_print_block pp_print_code stab out) blocks

let pp_print_live_blocks stab out blocks =
   Trace.iter (pp_print_block pp_print_live stab out) blocks

(*
 * Print the function table.
 *)
let pp_print_function_label out block =
   let { block_label = label;
         block_index = index
       } = block
   in
      match index with
         Some _ ->
            fprintf out "\t.long\t%a\n" pp_print_symbol label
       | None ->
            ()

let pp_print_function_table out blocks =
   fprintf out "\t.bfd_align\t4\n";
   fprintf out "%a:\n" pp_print_symbol function_base_label;
   fprintf out "\t.long\t%a\n" pp_print_symbol exit_label;
   fprintf out "\t.long\t%a\n" pp_print_symbol uncaught_exception_label;
   Trace.iter (pp_print_function_label out) blocks;
   fprintf out "%a:\n" pp_print_symbol function_limit_label

(*
 * Print the arity tags.
 *)
let pp_print_arity_table out arity =
   let lookup_arity name =
      try
         snd (ArityTable.find arity name)
      with
         Not_found ->
            Int32.zero
   in
   let tag_exit = lookup_arity exit_arity_tag in
   let tag_uncaught_exception = lookup_arity uncaught_exception_arity_tag in
      ArityTable.iter (fun _ (label, index) ->
         fprintf out "\t.equ\t%a,%s\n" pp_print_symbol label (Int32.to_string index)) arity;
      fprintf out "\t.equ\t%a,%s\n" pp_print_symbol exit_arity_label (Int32.to_string tag_exit);
      fprintf out "\t.equ\t%a,%s\n" pp_print_symbol uncaught_exception_arity_label (Int32.to_string tag_uncaught_exception)

(*
 * Print the function tags.
 *)
let pp_print_function_tag out off block =
   let { block_index = index } = block in
      match index with
         Some tag ->
            fprintf out "\t.equ\t%a,%d\n" pp_print_symbol tag off;
            off + 4
       | None ->
            off

let pp_print_function_tag_table out blocks =
   fprintf out "\t.equ\t%a,0\n" pp_print_symbol exit_tag_label;
   fprintf out "\t.equ\t%a,4\n" pp_print_symbol uncaught_exception_tag_label;
   let size = Trace.fold (pp_print_function_tag out) 8 blocks in
      fprintf out "\t.equ\t%a,%d\n" pp_print_symbol function_size_label size

(*
 * Print an initialization value.
 *)
let pp_print_init_value out = function
   InitValInt32 i ->
      fprintf out "\t.long\t%d\t# Integer value\n" init_val_int32;
      fprintf out "\t.long\t0x%08lx\n" i
 | InitValSingle f ->
      fprintf out "\t.long\t%d\t# Single-precision value\n" init_val_single;
      fprintf out "\t.single\t%s\n" (Float80.format float_format f)
 | InitValDouble f ->
      fprintf out "\t.long\t%d\t# Double-precision value\n" init_val_double;
      fprintf out "\t.double\t%s\n" (Float80.format float_format f)
 | InitValLongDouble f ->
      fprintf out "\t.long\t%d\t# 80-bit-precision value\n" init_val_long_double;
      fprintf out "\t.tfloat\t%s\n" (Float80.format float_format f);
      fprintf out "\t.bfd_align\t4\n"
 | InitValFunction (label, i, arity_tag) ->
      (* TEMP OMITTING ARITY TAG *)
      fprintf out "\t.long\t%d\t# Function %a\n" init_val_function pp_print_symbol label;
      fprintf out "\t.long\t%ld\n" i
 | InitValGlobal (label, v) ->
      fprintf out "\t.long\t%d\t# Global %a\n" init_val_global pp_print_symbol label;
      fprintf out "\t.long\t%a\n" pp_print_symbol v

(*
 * Print the binary FIR data.
 *)
let pp_print_binary_fir_data out bin_fir =
   (* Construct the FIR data blocks *)
   let buf = Buffer.create 0 in
   let () = BufferMarshal.marshal_prog buf bin_fir in
   let bin_fir = Buffer.contents buf in

   (* Output the resultant string data *)
   let linesize = 16 in
   let count = String.length bin_fir in
   fprintf out "\t.long\t%d\t# Size of the binary FIR\n" count;
   for i = 0 to count - 1 do
      if i > 0 && i mod (linesize * 64) = 0
         then fprintf out "\t# Binary FIR Offset %d\n" i;
      if i mod linesize = 0 then fprintf out "\t.byte\t";
      fprintf out "0x%02x" (int_of_char bin_fir.[i]);
      if i + 1 = count || (i + 1) mod linesize = 0
         then fprintf out "\n"
         else fprintf out ",";
      done

(*
 * Print migration data block.
 *)
let pp_print_migrate_data out migrate =
   let migrate =
      match migrate with
         None ->
            fprintf out "\t# This output file is NOT compiled as a migration target\n";
            fprintf out "\t.long\t0x00000000\t# Migration disabled\n";
             { mig_key       = string_of_symbol seg_fault_labels.(other_fault);
               mig_channel   = Int32.zero;
               mig_bidir     = false;
               mig_initofs   = Int32.zero;
               mig_registers = Int32.pred Int32.zero;
               mig_ptr_size  = Int32.zero;
               mig_heap_size = Int32.zero;
               mig_debug     = Int32.zero
             }
       | Some(migrate) ->
            fprintf out "\t# This output file is a migration target!\n";
            fprintf out "\t# This file will not function as a normal executable file\n";
            fprintf out "\t.long\t0xffffffff\t# Migration enabled\n";
            migrate
   in
   let pp_print_bool out i = pp_print_string out (if i then "00000001" else "00000000") in
   let bfd_print_int32 out i = pp_print_string out (Int32.format "%08x" i) in
      fprintf out "\t.long\t%s\t# Migrate dest label\n" migrate.mig_key;
      fprintf out "\t.long\t0x%a\t# Incoming channel ID\n" bfd_print_int32 migrate.mig_channel;
      fprintf out "\t.long\t0x%a\t# Channel is bidirectional?\n" pp_print_bool migrate.mig_bidir;
      fprintf out "\t.long\t0x%a\t# Initial offset to read from\n" bfd_print_int32 migrate.mig_initofs;
      fprintf out "\t.long\t0x%a\t# Register block index\n" bfd_print_int32 migrate.mig_registers;
      fprintf out "\t.long\t0x%a\t# Pointer table size\n" bfd_print_int32 migrate.mig_ptr_size;
      fprintf out "\t.long\t0x%a\t# Total size of data\n" bfd_print_int32 migrate.mig_heap_size;
      fprintf out "\t.long\t0x%a\t# Migrate debug flags\n" bfd_print_int32 migrate.mig_debug

(*
 * Print the assembly code.
 *
 * The assembly has these sections:
 *    1. A string table
 *       a. The string constants.  At initialization, these strings
 *          are copied into the heap.
 *       b. A table of string pointers.  The runtime will adjust
 *          the pointers to point to the heap-allocated strings.
 *)
let pp_print_prog out
    { asm_file     = file;
      asm_types    = tenv;
      asm_spills   = spills;
      asm_closures = closures;
      asm_export   = export;
      asm_import   = import;
      asm_globals  = globals;
      asm_floats   = floats;
      asm_blocks   = blocks;
      asm_bin_fir  = bin_fir;
      asm_spset    = spset;
      asm_migrate  = migrate;
      asm_init     = inits;
      asm_arity    = arity;
      asm_debug    = debug;
    } =
   let { file_dir = file_dir; file_name = file_name } = file in
   let glob_count = List.length globals in
   let global_names, _ =
      List.fold_left (fun (globals, i) (_, init) ->
            let globals =
               match init with
                  InitRawData _
                | InitAggrBlock _
                | InitTagBlock _
                | InitVar _ ->
                     let v = symbol_of_global_spill i in
                        v :: globals
                | InitRawInt _
                | InitRawFloat _ ->
                     globals
            in
               globals, succ i) ([], 0) globals
   in
   let global_names = List.rev global_names in
   let stab = pp_print_stab_header out tenv file_dir file_name in
      (* Define the globals *)
      fprintf out "\n\n/*** Exported labels ***/\n";
      SymbolTable.iter (fun v { export_name = s } ->
            fprintf out "\t.global\t%s\n" s;
            fprintf out "\t.equ\t%s,%a\n" s pp_print_symbol v) export;
      List.iter (fun (v, s) ->
            fprintf out "\t.global\t%s\n" s;
            fprintf out "\t.equ\t%s,%a\n" s pp_print_symbol v) export_vars;

      fprintf out "\n\n/*** Imported labels ***/\n";
      SymbolTable.iter (fun v { import_name = s } ->
            fprintf out "\t.equ\t%a,%s\n" pp_print_symbol v s) import;
      List.iter (fun (v, s) ->
            fprintf out "\t.equ\t%a,%s\n" pp_print_symbol v s) import_vars;

      (* Dump the (known) compile options *)
      fprintf out "\n\n/*** Flags ***/\n";
      StringTable.iter (fun flag value ->
         fprintf out "# Flag %s is " flag;
         match value with
            Flags.FlagBool value -> fprintf out "%s\n" (if value then "set" else "cleared")
          | Flags.FlagInt value -> fprintf out "%d\n" value) (!Flags.std_flags).Flags.flag_values;

      (* Dump hash codes *)
      if Fir_state.safety_enabled () then
         begin
            fprintf out "\t.equ\t%a,0x%08x\n" pp_print_symbol hash_index_label X86_runtime.hash_index_code;
            fprintf out "\t.equ\t%a,0x%08x\n" pp_print_symbol hash_fun_index_label X86_runtime.hash_fun_index_code
         end
      else
         begin
            fprintf out "\t.equ\t%a,0\n" pp_print_symbol hash_index_label;
            fprintf out "\t.equ\t%a,0\n" pp_print_symbol hash_fun_index_label
         end;

      (* Migration data *)
      fprintf out "\n\n/*** Migration ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"Migration data \"\n\t.bfd_align\t4\n";
      fprintf out "%a:\n" pp_print_symbol migrate_data_label;
      pp_print_migrate_data out migrate;

      (* Print initializers *)
      fprintf out "\n\n/*** Program header ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"Program header \"\n\t.bfd_align\t4\n";
      fprintf out "%a:\n" pp_print_symbol header_base_label;
      List.iter (fun (fclass, init_sym, main_sym) ->
            fprintf out "\t.long\t%d\t# Version number\n" X86_runtime.version_number;
            fprintf out "\t.long\t%d\t# Context length\n" context_length;
            fprintf out "\t.long\t%a\t# File class\n" pp_print_file_class fclass;
            fprintf out "\t.long\t%a\t# Initialization function\n" pp_print_symbol init_sym;
            fprintf out "\t.long\t%a\t# Main function\n" pp_print_symbol main_sym) inits;
      fprintf out "%a:\n" pp_print_symbol header_limit_label;

      (* Print a block reserving space for the context *)
      fprintf out "\n\n/*** Context variables ***/\n";
      let off =
         List.fold_left (fun i v ->
               fprintf out "\t.equ\t%a,%d\n" pp_print_symbol v i;
               i + sizeof_int32) 0 context_vars
      in

      (* Print the spills *)
      fprintf out "/*** Normal integer spills: there are %d spills ***/\n" spills.spill_int_normal_index;
      for i = 0 to pred spills.spill_int_normal_index do
         let label = symbol_of_int_normal_spill i in
            fprintf out "\t.equ\t%a,%d\n" pp_print_symbol label (off + i * sizeof_int32)
      done;
      let off = off + spills.spill_int_normal_index * sizeof_int32 in

      fprintf out "/*** Standard argument integer spills: there are %d spills ***/\n" spills.spill_int_stdarg_index;
      for i = 0 to pred spills.spill_int_stdarg_index do
         let label = symbol_of_int_stdarg_spill i in
            fprintf out "\t.equ\t%a,%d\n" pp_print_symbol label (off + i * sizeof_int32)
      done;
      let off = off + spills.spill_int_stdarg_index * sizeof_int32 in

      fprintf out "/*** Normal floating-point spills: there are %d spills ***/\n" spills.spill_float_normal_index;
      for i = 0 to pred spills.spill_float_normal_index do
         let label = symbol_of_float_normal_spill i in
            fprintf out "\t.equ\t%a,%d\n" pp_print_symbol label (off + i * align_long_double)
      done;
      let off = off + spills.spill_float_normal_index * align_long_double in

      fprintf out "/*** Standard argument floating-point spills: there are %d spills ***/\n" spills.spill_float_stdarg_index;
      for i = 0 to pred spills.spill_float_stdarg_index do
         let label = symbol_of_float_stdarg_spill i in
            fprintf out "\t.equ\t%a,%d\n" pp_print_symbol label (off + i * align_long_double)
      done;
      let off = off + spills.spill_float_stdarg_index * align_long_double in

      fprintf out "/*** Global variables: there are %d spills ***/\n" spills.spill_global_index;
      fprintf out "\t.equ\t%a,%d\n" pp_print_symbol globals_index_base_label off;
      let off, _ =
         List.fold_left (fun (off, i) (_, init) ->
               let bfd_align, size = sizeof_init_exp init in
               let label = symbol_of_global_spill i in
               let off = do_align off bfd_align in
                  fprintf out "\t.equ\t%a,%d\n" pp_print_symbol label off;
                  off + size, succ i) (off, 0) globals
      in
      fprintf out "\t.equ\t%a,%d\n" pp_print_symbol globals_index_limit_label off;
      fprintf out "\t.equ\t%a,%d\n" pp_print_symbol globals_size_label spills.spill_global_index;
      fprintf out "\t.equ\t%a,%d\n" pp_print_symbol context_size_label off;

      (* Print the contents of the global initializers *)
      fprintf out "\n\n/*** Global initializers ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"Global init    \"\n\t.bfd_align\t4\n";
      List.iter (fun (v, op) ->
         match op with
            InitRawData (pre, s) ->
               let len = Array.length s in
               let word, len =
                  match pre with
                     Rawint.Int8 -> ".byte", len
                   | Rawint.Int16 -> ".word", 2 * len
                   | Rawint.Int32
                   | Rawint.Int64 ->
                        raise (Invalid_argument "bogus string precision")
               in
                  fprintf out "\t.bfd_align\t4\n%a:\n" pp_print_symbol v;
                  fprintf out "\t.long\t%d\t# Length of the string\n" (len + 2);
                  for i = 0 to pred len do
                     fprintf out "\t%s\t%d\n" word s.(i)
                  done;
                  fprintf out "\t.byte\t0\n\t.byte\t0\n"
          | InitTagBlock (tag, vals) ->
               fprintf out "\t.bfd_align\t4\n%a:\n" pp_print_symbol v;
               fprintf out "\t.long\t%d\t# Size of this block\n" (List.length vals);
               fprintf out "\t.long\t%d\t# Tag for this block\n" tag;
               List.iter (pp_print_init_value out) vals
          | InitRawInt i ->
               fprintf out "\t.bfd_align\t4\n%a:\n" pp_print_symbol v;
               fprintf out "\t.long\t%d\t# Length of the rawint\n" (length_of_rawint i);
               pp_print_rawint out i
          | InitRawFloat x ->
               fprintf out "\t.bfd_align\t4\n%a:\n" pp_print_symbol v;
               fprintf out "\t.long\t%d\t# Length of the rawfloat\n" (length_of_rawfloat x);
               pp_print_rawfloat out x
          | InitVar _
          | InitAggrBlock _ ->
               ()) globals;

      (* Floats *)
      fprintf out "\n\n/*** Floating point values ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"Float values   \"\n\t.bfd_align\t4\n";
      SymbolTable.iter (fun v f ->
            fprintf out "%a:\n\t.tfloat\t%s\n" pp_print_symbol v (Float80.format float_format f)) floats;

      (* Global info *)
      fprintf out "\n\n/*** Globals ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"Global table   \"\n\t.bfd_align\t4\n";
      fprintf out "%a:\n" pp_print_symbol globals_base_label;
      ignore (List.fold_left (fun index (v, op) ->
         let label = symbol_of_global_spill index in
         let () =
            match op with
               InitRawData _ ->
                  fprintf out "\t# %a (#%d) is a string\n" pp_print_symbol v index;
                  fprintf out "\t.long\t%d\n\t.long\t%a\n\t.long\t%a\n" (**)
                     rawdata_global pp_print_symbol v pp_print_symbol label
             | InitTagBlock _ ->
                  fprintf out "\t# %a (#%d) is a tagged block\n" pp_print_symbol v index;
                  fprintf out "\t.long\t%d\n\t.long\t%a\n\t.long\t%a\n" (**)
                     tagblock_global pp_print_symbol v pp_print_symbol label
             | InitAggrBlock i ->
                  fprintf out "\t# %a (#%d) takes %a bytes\n" pp_print_symbol v index pp_print_rawint i;
                  fprintf out "\t.long\t%d\n\t.long\t%a\n\t.long\t%a\n" (**)
                     aggrblock_global pp_print_rawint i pp_print_symbol label
             | InitRawInt i ->
                  fprintf out "\t# %a (#%d) is a rawint\n" pp_print_symbol v index;
                  fprintf out "\t.long\t%d\n\t.long\t%a\n\t.long\t%a\n" (**)
                     rawint_global pp_print_symbol v pp_print_symbol label
             | InitRawFloat x ->
                  fprintf out "\t# %a (#%d) is a rawfloat\n" pp_print_symbol v index;
                  fprintf out "\t.long\t%d\n\t.long\t%a\n\t.long\t%a\n" (**)
                     rawfloat_global pp_print_symbol v pp_print_symbol label
             | InitVar (label, i) ->
                  fprintf out "\t# %a is a reference to global %a\n" (**)
                     pp_print_symbol v pp_print_symbol label;
                  fprintf out "\t.long\t%d\n\t.long\t%a\n\t.long\t%a\n" (**)
                     var_global pp_print_symbol v pp_print_symbol label
         in
            succ index) 0 globals);
      fprintf out "%a:\n" pp_print_symbol globals_limit_label;

      (* Print debugging info for all the symbols *)
      if !Fir_state.debug_print_asm then
         spset_iter (fun v op ->
               fprintf out "\t# %a = %a = %a\n" (**)
                  (pp_print_padded_symbol 32) v
                  (pp_print_padded_operand 32) op
                  pp_print_operand (spset_lookup_int32 spset v)) spset;

      (* Print the reserve data *)
      fprintf out "\n\n/*** Reserve data ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"Reserve data   \"\n\t.bfd_align\t4\n";
      pp_print_reserve_blocks out global_names blocks;

      (* Print the function tables *)
      fprintf out "\n\n/*** Function tags ***/\n";
      pp_print_function_tag_table out blocks;
      fprintf out "\n\n/*** Function table ***/\n";
      pp_print_function_table out blocks;
      fprintf out "\n\n/*** Arity tag table ***/\n";
      pp_print_arity_table out arity;

      (* Print the debug flags *)
      fprintf out "\n\n/*** Debug flags ***/\n";
      fprintf out "%a:\n\t.long\t%s\n" pp_print_symbol debug_flags_label (Int32.to_string debug);

      (* Print the blocks *)
      fprintf out "\n\n/*** Code ***/\n";
      fprintf out "\t.text\n\t.bfd_align\t4\n";
      pp_print_blocks stab out blocks;

      (* Print debug info *)
      if !Fir_state.debug_stab then
         begin
            fprintf out "\n\n/*** STAB ***/\n";
            pp_print_stab_trailer stab out
         end;

      (* Print the binary FIR data *)
      fprintf out "\n\n/*** FIR ***/\n";
      fprintf out "\t.data\n\t.bfd_align\t16\n\t.string\t\"FIR data       \"\n\t.bfd_align\t4\n";
      fprintf out "\n%a:\n" pp_print_symbol bin_fir_label;
      pp_print_binary_fir_data out bin_fir

let debug_prog debug prog =
   eprintf "%s@.%a@." debug pp_print_prog prog

let pp_print_prog out prog = Fir_state.profile "X86_print.pp_print_prog buf" (pp_print_prog out) prog

(*
 * Wrap the block printer.
 *)
let pp_print_blocks out blocks =
   let tenv = SymbolTable.empty in
   let stab = pp_print_stab_header out tenv "." "<unknown>" in
      ignore (pp_print_blocks stab out blocks)

let pp_print_code_blocks out blocks =
   let tenv = SymbolTable.empty in
   let stab = pp_print_stab_header out tenv "." "<unknown>" in
      ignore (pp_print_code_blocks stab out blocks)

let pp_print_live_blocks out blocks =
   let tenv = SymbolTable.empty in
   let stab = pp_print_stab_header out tenv "." "<unknown>" in
      ignore (pp_print_live_blocks stab out blocks)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
