(*
 * x86 special instructions that don't conform to the usual
 * conventions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_frame
open X86_exn
open X86_pos

open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm
open X86_as_unary
open X86_as_binary

module Pos = MakePos (struct let name = "X86_as_special" end)
open Pos

(*
 * IMUL is just weird.
 * The destination has to be a a register,
 * and the opcode is different depending on
 * whether the second operand is an immediate.
 *
 * Just treat them as two totally different instructions.
 *)
let as_print_imul_inst buf pos pre op1 op2 next =
   let pos = string_pos "as_print_imul_inst buf" pos in
      match op1, op2 with
         Register _, Register _ ->
            as_print_binary_int_inst buf imul2_opcodes pos pre op2 op1 next
       | Register _, ImmediateLabel _
       | Register _, MemReg _
       | Register _, MemRegOff _
       | Register _, MemRegRegOffMul _ ->
            as_print_binary_int_inst buf imul2_opcodes pos pre op1 op2 next
       | Register reg, ImmediateNumber off ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre 0x69;
               as_print_modrm_reg_reg buf reg reg;
               as_print_pre_imm buf pre off
       | _ ->
            raise (X86Exception (pos, StringError "as_print_imul_inst buf: illegal operand combination"))

(*
 * JMP instruction allows a short displacement.
 *)
let as_print_jmp_inst buf pos op next =
   let pos = string_pos "as_print_jmp_inst buf" pos in
      match op with
         ImmediateLabel label ->
            (* We only allow direct jumps *)
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
            let off = OffRel (OffLabel label, next) in

               (* 8-bit displacement *)
               bfd_print_int8 buf8 0xeb;
               as_print_imm8 buf8 off;

               (* 32-bit displacement *)
               bfd_print_int8 buf32 0xe9;
               as_print_imm32 buf32 off;

               (* Make the choice *)
               as_print_choice buf off [interval_signed8, buf8;
                                        interval_all, buf32]

       | _ ->
            as_print_unary_int_inst buf jmp_opcodes pos IL op next

(*
 * JCC instruction adds flags as a part of the opcode.
 *)
let as_print_jcc_inst buf pos cc op next =
   let pos = string_pos "as_print_jcc_inst buf" pos in
   let cc = number_of_cc cc in
      match op with
         ImmediateLabel label ->
            (* We only allow direct jumps *)
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
            let off = OffRel (OffLabel label, next) in

               (* 8-bit displacement *)
               bfd_print_int8 buf8 (0x70 lor cc);
               as_print_imm8 buf8 off;

               (* 32-bit displacement *)
               bfd_print_int8 buf32 0x0f;
               bfd_print_int8 buf32 (0x80 lor cc);
               as_print_imm32 buf32 off;

               (* Make the choice *)
               as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]

       | _ ->
            raise (X86Exception (pos, StringError "as_print_jcc_inst buf: illegal operand"))

(*
 * Copy a cc to a register.
 *)
let as_print_setcc_inst buf pos cc op =
   let pos = string_pos "as_print_setcc_inst buf" pos in
   let cc = number_of_cc cc in
   let extra3 = 0 in
      bfd_print_int8 buf 0x0f;
      bfd_print_int8 buf (0x90 lor cc);
      match op with
         Register reg ->
            let reg = number_of_register pos reg in
               as_print_modrm_reg_reg buf extra3 reg
       | MemReg reg ->
            let reg = number_of_register pos reg in
               as_print_modrm_reg_memreg buf extra3 reg
       | MemRegOff (reg, off) ->
            let reg = number_of_register pos reg in
               as_print_modrm_reg_memregoff buf extra3 reg off
       | MemRegRegOffMul (r1, r2, off, mul) ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
               as_print_modrm_reg_memregregoffmul buf pos extra3 r1 r2 off mul
       | _ ->
            raise (X86Exception (pos, StringError "as_print_setcc_inst buf: illegal operand"))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
