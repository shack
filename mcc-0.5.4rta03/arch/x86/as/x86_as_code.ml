(*
 * Assemble the code.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Bfd

open Frame_type

open Mir_arity_map

open X86_inst_type
open X86_frame_type
open X86_frame
open X86_exn
open X86_pos

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm
open X86_as_empty
open X86_as_unary
open X86_as_binary
open X86_as_shift
open X86_as_special
open X86_as_float
open X86_as_mmx
open X86_as_section

module Pos = MakePos (struct let name = "X86_as_code" end)
open Pos

(*
 * Assemble the instructon.
 *)
let assem_inst buf pos inst =
   let pos = inst_pos inst pos in
   let next = new_symbol_string "inst" in
   let () =
      match inst with
         (* Nops *)
         CommentString _
       | CommentInst _
       | CommentMIR _
       | CommentFIR _
       | FJMP _ ->
            ()
       | NOP ->
            as_print_empty_inst buf nop_opcodes

         (* Copy *)
       | MOV (pre, op1, op2) ->
            as_print_binary_int_inst buf mov_opcodes pos pre op1 op2 next
       | MOVS (pre, op1, op2) ->
            as_print_binary_int_inst buf movs_opcodes pos pre op1 op2 next
       | MOVZ (pre, op1, op2) ->
            as_print_binary_int_inst buf movz_opcodes pos pre op1 op2 next
       | RMOVS pre ->
            as_print_empty_pre_inst buf rmovs_opcodes pre

         (* Arithmetic *)
       | NOT (pre, op) ->
            as_print_unary_int_inst buf not_opcodes pos pre op next
       | NEG (pre, op) ->
            as_print_unary_int_inst buf neg_opcodes pos pre op next
       | INC (pre, op) ->
            as_print_unary_int_inst buf inc_opcodes pos pre op next
       | DEC (pre, op) ->
            as_print_unary_int_inst buf dec_opcodes pos pre op next
       | LEA (pre, op1, op2) ->
            as_print_binary_int_inst buf lea_opcodes pos pre op1 op2 next
       | ADD (pre, op1, op2) ->
            as_print_binary_int_inst buf add_opcodes pos pre op1 op2 next
       | ADC (pre, op1, op2) ->
            as_print_binary_int_inst buf adc_opcodes pos pre op1 op2 next
       | SUB (pre, op1, op2) ->
            as_print_binary_int_inst buf sub_opcodes pos pre op1 op2 next
       | SBB (pre, op1, op2) ->
            as_print_binary_int_inst buf sbb_opcodes pos pre op1 op2 next
       | MUL (pre, op) ->
            as_print_unary_int_inst buf mul_opcodes pos pre op next
       | IMUL (pre, op1, op2) ->
            as_print_imul_inst buf pos pre op1 op2 next
       | DIV (pre, op) ->
            as_print_unary_int_inst buf div_opcodes pos pre op next
       | IDIV (pre, op) ->
            as_print_unary_int_inst buf idiv_opcodes pos pre op next
       | CLTD ->
            as_print_empty_inst buf cltd_opcodes
       | AND (pre, op1, op2) ->
            as_print_binary_int_inst buf and_opcodes pos pre op1 op2 next
       | OR (pre, op1, op2) ->
            as_print_binary_int_inst buf or_opcodes pos pre op1 op2 next
       | XOR (pre, op1, op2) ->
            as_print_binary_int_inst buf xor_opcodes pos pre op1 op2 next
       | SAL (pre, op1, op2) ->
            as_print_shift_inst buf sal_opcodes pos pre op1 op2
       | SAR (pre, op1, op2) ->
            as_print_shift_inst buf sar_opcodes pos pre op1 op2
       | SHL (pre, op1, op2) ->
            as_print_shift_inst buf shl_opcodes pos pre op1 op2
       | SHR (pre, op1, op2) ->
            as_print_shift_inst buf shr_opcodes pos pre op1 op2
       | SHLD (pre, op1, op2, op3) ->
            as_print_shiftd_inst buf shld_opcodes pos pre op1 op2 op3
       | SHRD (pre, op1, op2, op3) ->
            as_print_shiftd_inst buf shrd_opcodes pos pre op1 op2 op3

       | PUSH (pre, op) ->
            as_print_unary_int_inst buf push_opcodes pos pre op next
       | POP (pre, op) ->
            as_print_unary_int_inst buf pop_opcodes pos pre op next
       | CALL op
       | FCALL op ->
            as_print_unary_int_inst buf call_opcodes pos IL op next
       | RET ->
            as_print_empty_inst buf ret_opcodes

       | CMP (pre, op1, op2) ->
            as_print_binary_int_inst buf cmp_opcodes pos pre op1 op2 next
       | TEST (pre, op1, op2) ->
            as_print_binary_int_inst buf test_opcodes pos pre op1 op2 next

       | JMP op
       | IJMP (_, op) ->
            as_print_jmp_inst buf pos op next
       | JCC (cc, op) ->
            as_print_jcc_inst buf pos cc op next
       | SET (cc, op) ->
            as_print_setcc_inst buf pos cc op

         (* MMX *)
       | MOVD (op1, op2) ->
            as_print_binary_mmx_inst buf movd_opcodes pos op1 op2 next
       | MOVQ (op1, op2) ->
            as_print_binary_mmx_inst buf movq_opcodes pos op1 op2 next

         (* Floating point *)
       | FINIT ->
            as_print_empty_float_inst buf finit_opcodes
       | FSTCW op ->
            as_print_unary_float_inst buf fstcw_opcodes pos FS op
       | FLDCW op ->
            as_print_unary_float_inst buf fldcw_opcodes pos FS op
       | FLD (pre, op) ->
            as_print_unary_float_inst buf fld_opcodes pos pre op
       | FILD op ->
            as_print_unary_float_inst buf fild_opcodes pos FS op
       | FILD64 op ->
            as_print_unary_float_inst buf fild_opcodes pos FL op
       | FST (pre, op) ->
            as_print_unary_float_inst buf fst_opcodes pos pre op
       | FSTP (pre, op) ->
            as_print_unary_float_inst buf fstp_opcodes pos pre op
       | FIST op ->
            as_print_unary_float_inst buf fist_opcodes pos FS op
       | FISTP op ->
            as_print_unary_float_inst buf fistp_opcodes pos FS op
       | FISTP64 op ->
            as_print_unary_float_inst buf fistp_opcodes pos FL op
       | FXCH op ->
            as_print_unary_float_inst buf fxch_opcodes pos FT op
       | FCHS ->
            as_print_empty_float_inst buf fchs_opcodes
       | FADDP ->
            as_print_unary_float_inst buf faddp_opcodes pos FT st1
       | FSUBP ->
            as_print_unary_float_inst buf fsubp_opcodes pos FT st1
       | FMULP ->
            as_print_unary_float_inst buf fmulp_opcodes pos FT st1
       | FDIVP ->
            as_print_unary_float_inst buf fdivp_opcodes pos FT st1
       | FSUBRP ->
            as_print_unary_float_inst buf fsubrp_opcodes pos FT st1
       | FDIVRP ->
            as_print_unary_float_inst buf fdivrp_opcodes pos FT st1
       | FPREM ->
            as_print_empty_float_inst buf fprem_opcodes
       | FSIN ->
            as_print_empty_float_inst buf fsin_opcodes
       | FCOS ->
            as_print_empty_float_inst buf fcos_opcodes
       | FPATAN ->
            as_print_empty_float_inst buf fpatan_opcodes
       | FSQRT ->
            as_print_empty_float_inst buf fsqrt_opcodes
       | FUCOM ->
            as_print_unary_float_inst buf fucom_opcodes pos FT st1
       | FUCOMP ->
            as_print_unary_float_inst buf fucomp_opcodes pos FT st1
       | FUCOMPP ->
            as_print_empty_float_inst buf fucompp_opcodes
       | FSTSW ->
            as_print_empty_float_inst buf fstsw_opcodes
       | FADD (op1, op2) ->
            as_print_binary_float_inst buf fadd_opcodes pos FL op1 op2
       | FSUB (op1, op2) ->
            as_print_binary_float_inst buf fsub_opcodes pos FL op1 op2
       | FMUL (op1, op2) ->
            as_print_binary_float_inst buf fmul_opcodes pos FL op1 op2
       | FDIV (op1, op2) ->
            as_print_binary_float_inst buf fdiv_opcodes pos FL op1 op2
       | FSUBR (op1, op2) ->
            as_print_binary_float_inst buf fsubr_opcodes pos FL op1 op2
       | FDIVR (op1, op2) ->
            as_print_binary_float_inst buf fdivr_opcodes pos FL op1 op2
       | FMOV (pre1, op1, pre2, op2) ->
            as_print_unary_float_inst buf fld_opcodes pos pre2 op2;
            as_print_unary_float_inst buf fst_opcodes pos pre1 op1
       | FMOVP (pre1, op1, pre2, op2) ->
            as_print_unary_float_inst buf fld_opcodes pos pre2 op2;
            as_print_unary_float_inst buf fstp_opcodes pos pre1 op1

         (* Special instructions *)
       | RES (label, _, mem, ptr) ->
            as_print_unary_int_inst buf push_opcodes pos IL label next;
            as_print_unary_int_inst buf push_opcodes pos IL mem next;
            as_print_unary_int_inst buf push_opcodes pos IL ptr next;
            as_print_unary_int_inst buf call_opcodes pos IL (ImmediateLabel gc_label) next;
            as_print_binary_int_inst buf add_opcodes pos IL (Register esp) (ImmediateNumber (OffNumber (Int32.of_int 12))) next
       | COW (label, _, mem, ptr, copy_ptr) ->
            as_print_unary_int_inst buf push_opcodes pos IL copy_ptr next;
            as_print_unary_int_inst buf push_opcodes pos IL label next;
            as_print_unary_int_inst buf push_opcodes pos IL mem next;
            as_print_unary_int_inst buf push_opcodes pos IL ptr next;
            as_print_unary_int_inst buf call_opcodes pos IL (ImmediateLabel copy_on_write_label) next;
            as_print_binary_int_inst buf add_opcodes pos IL (Register esp) (ImmediateNumber (OffNumber (Int32.of_int 16))) next
       | RESP (label, _) ->
            as_print_unary_int_inst buf push_opcodes pos IL label next;
            as_print_unary_int_inst buf push_opcodes pos IL (ImmediateNumber (OffNumber (Int32.of_int 0))) next;
            as_print_unary_int_inst buf push_opcodes pos IL (ImmediateNumber (OffNumber (Int32.of_int 0))) next;
            as_print_unary_int_inst buf push_opcodes pos IL (ImmediateNumber (OffNumber (Int32.of_int 0))) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register ebp) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register edi) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register esi) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register edx) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register ecx) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register ebx) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register eax) next;
            as_print_unary_int_inst buf push_opcodes pos IL (Register esp) next

   in
      bfd_print_label buf SymTemp next;
      pos

(*
 * Assemble a block.
 *)
let assem_block buf export block =
   let { block_label = label;
         block_code = insts;
         block_index = index_flag;
         block_align = align_flag;
         block_arity = arity_tag;
       } = block
   in
   let pos = var_exp_pos label in
   let pos = string_pos "assem_block" pos in
      (* Align if necessary *)
      if align_flag then
         bfd_align buf align_block
      else
         bfd_align buf 4;

      (* If the index is set, then add space and the index *)
      (match index_flag, arity_tag with
         Some index, Some arity ->
            (* Add the index to the function index table.
               See arch/x86/runtime/x86_runtime.h for format. *)
            bfd_skip buf index_skip 0x90; (* NOP *)
            (* Function header begins here *)
            as_print_imm32 buf (OffLabel arity);
            bfd_print_int32 buf Int32.zero;
            as_print_imm32 buf (OffOr (OffLabel index, OffNumber X86_runtime.aggr_shift_mask32));
       | None, None ->
            ()
       | _ ->
            raise (X86Exception (pos, StringError "block_index and block_arity must be both set or both cleared")));

      (* If there is a global binding, add it at this point *)
      (try
         let { export_name = s } = SymbolTable.find export label in
            bfd_print_label buf SymGlobal (Symbol.add s)
      with
         Not_found ->
            ());

      (* Add the label at this program point *)
      bfd_print_label buf SymLocal label;

      (* Print all the instructons *)
      ignore (List.fold_left (assem_inst buf) pos insts)

(*
 * Assemble a list.of blocks.
 *)
let assem_blocks buf export blocks =
   List.iter (assem_block buf export) blocks

(*
 * Assemble all the block's text.
 * We'll sweep for data separately.
 *)
let as_print_code buf blocks export =
   let sect = text_section buf in
   let buf = section_buf sect in
      bfd_print_label buf SymLocal X86_stab.text_sym;
      Trace.iter (assem_block buf export) blocks;
      bfd_align buf align_block

(*
 * Assemble the code and print the function table info.
 *)
let assem_code buf info blocks =
   let { bfd_export = export } = info in
      as_print_code buf blocks export

(*
 * Print the arity tag table.
 *)
let as_print_arity_table buf arity =
   let lookup_arity name =
      try
         snd (ArityTable.find arity name)
      with
         Not_found ->
            Int32.zero
   in
   let sect = absolute_section buf in
   let buf = section_buf sect in
   let tag_exit = lookup_arity exit_arity_tag in
   let tag_uncaught_exception = lookup_arity uncaught_exception_arity_tag in
      ArityTable.iter (fun _ (label, index) ->
         as_print_equ_int32 buf SymLocal label index) arity;
      as_print_equ_int32 buf SymGlobal exit_arity_label tag_exit;
      as_print_equ_int32 buf SymGlobal uncaught_exception_arity_label tag_uncaught_exception

(*
 * Print the function table.
 *)
let as_print_function_label buf (label, _) =
   as_print_abs32 buf label

let as_print_function_table buf funs =
   let sect = data_section buf in
   let buf = section_buf sect in
      bfd_align buf 4;
      bfd_print_label buf SymGlobal function_base_label;
      as_print_abs32 buf exit_label;
      as_print_abs32 buf uncaught_exception_label;
      List.iter (as_print_function_label buf) funs;
      bfd_print_label buf SymGlobal function_limit_label

(*
 * Print the function tags.
 *)
let as_print_function_tag buf off (_, tag) =
   as_print_equ_int buf SymLocal tag off;
   off + 4

let as_print_function_tag_table buf funs =
   let sect = absolute_section buf in
   let buf = section_buf sect in
      as_print_equ_int buf SymGlobal exit_tag_label 0;
      as_print_equ_int buf SymGlobal uncaught_exception_tag_label 4;
      let size = List.fold_left (as_print_function_tag buf) 8 funs in
         as_print_equ_int buf SymGlobal function_size_label size

let assem_fun_table buf info =
   let { bfd_fun_tags = funs;
         bfd_arity_tags = arity;
       } = info
   in
      as_print_function_table buf funs;
      as_print_function_tag_table buf funs;
      as_print_arity_table buf arity

(*
 * Collect all the blocks with index labels.
 *)
let get_block_tag funs block =
   let { block_label = label;
         block_index = index
       } = block
   in
      match index with
         Some tag ->
            (label, tag) :: funs
       | None ->
            funs

let get_fun_tags blocks =
   List.rev (Trace.fold get_block_tag [] blocks)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
