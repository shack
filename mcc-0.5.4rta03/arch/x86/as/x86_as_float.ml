(*
 * x86 floating-point instructions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_exn
open X86_pos

open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm

module Pos = MakePos (struct let name = "X86_as_float" end)
open Pos

(*
 * Empty floating point instruction emits two opcode bytes.
 *)
let as_print_empty_float_inst buf opcodes =
   let code16 = opcodes lsr 16 in
      if code16 <> 0 then
         bfd_print_int8 buf code16;
      bfd_print_int8 buf (opcodes lsr 8);
      bfd_print_int8 buf (opcodes land 0xff)

(*
 * This is a floating point memory operation,
 * get the opcode and extra 3 bits.
 *)
let float_pre_op pos pre opcodes =
   let pos = string_pos "float_pre_op" pos in
      match pre, opcodes with
         FS, { funop_mem_single = Some (opcode, extra3) } ->
            opcode, extra3
       | FL, { funop_mem_double = Some (opcode, extra3) } ->
            opcode, extra3
       | FT, { funop_mem_long_double = Some (opcode, extra3) } ->
            opcode, extra3
       | _ ->
            raise (X86Exception (pos, StringError "as_print_unary_float_inst buf: illegal operand"))

(*
 * Print an instruction.
 * this is effectively a unary instruction,
 * but the dst argument may be used to modify the opcode.
 *)
let as_print_float_inst buf opcodes pos dst pre op =
   let pos = string_pos "as_print_float_inst buf" pos in
      match op, opcodes with
         FPStack reg, { funop_stack = Some (opcode, extra3) } ->
            let reg = Int32.to_int reg in
               bfd_print_int8 buf (opcode lor dst);
               as_print_modrm_reg_reg buf extra3 reg

       | ImmediateLabel label, _ ->
            let opcode, extra3 = float_pre_op pos pre opcodes in
               bfd_print_int8 buf (opcode lor dst);
               as_print_modrm_reg_label buf extra3 label false label

       | MemReg reg, _ ->
            let reg = number_of_register pos reg in
            let opcode, extra3 = float_pre_op pos pre opcodes in
               bfd_print_int8 buf (opcode lor dst);
               as_print_modrm_reg_memreg buf extra3 reg
       | MemRegOff (reg, off), _ ->
            let reg = number_of_register pos reg in
            let opcode, extra3 = float_pre_op pos pre opcodes in
               bfd_print_int8 buf (opcode lor dst);
               as_print_modrm_reg_memregoff buf extra3 reg off
       | MemRegRegOffMul (r1, r2, off, mul), _ ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode, extra3 = float_pre_op pos pre opcodes in
               bfd_print_int8 buf (opcode lor dst);
               as_print_modrm_reg_memregregoffmul buf pos extra3 r1 r2 off mul

       | _ ->
            raise (X86Exception (pos, StringError "as_print_unary_float_inst buf: illegal operand"))

(*
 * For a unary instruction, the dst field is always 0.
 *)
let as_print_unary_float_inst buf opcodes pos pre op =
   as_print_funop_prefix buf opcodes;
   as_print_float_inst buf opcodes pos 0 pre op

(*
 * Print a binary instruction.
 * dst operand must be stack register 0 or 1.
 *)
let as_print_binary_float_inst buf opcodes pos pre op1 op2 =
   let pos = string_pos "as_print_binary_float_inst buf" pos in
   let dst, op =
      match op1, op2 with
         FPStack i, FPStack j ->
            (match Int32.to_int i, Int32.to_int j with
                0, _ ->
                   opcode_float_st0_is_dst_flag, op2
              | _, 0 ->
                   opcode_float_st0_is_src_flag, op1
              | _ ->
                   raise (X86Exception (pos, StringError "as_print_binary_float_inst buf: illegal FP register combination")))
       | _ ->
            raise (X86Exception (pos, StringError "as_print_binary_float_inst buf: source, dest must both be FP registers"))
   in
      as_print_funop_prefix buf opcodes;
      as_print_float_inst buf opcodes pos dst pre op

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
