(*
 * x86 binary integer instructions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_frame
open X86_exn
open X86_pos

open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm

module Pos = MakePos (struct let name = "X86_as_binary" end)
open Pos

(*
 * Operand directions.
 * The operands are always specified in some canonical
 * order based on operand type.  The opcode contains a bit
 * specifying whether to swap the operands, and we compute that
 * biut here.
 *)
let binop_sort opcodes pos op1 op2 =
   let pos = string_pos "binop_sort" pos in
      match op1, op2 with
         Register _, Register _
       | ImmediateLabel _, Register _
       | MemReg _, Register _
       | MemRegOff _, Register _
       | MemRegRegOffMul _, Register _ ->
            0, op2, op1

       | Register _,        ImmediateNumber _
       | MemReg _,          ImmediateNumber _
       | MemRegOff _,       ImmediateNumber _
       | MemRegRegOffMul _, ImmediateNumber _
       | Register _, ImmediateLabel _
       | Register _, MemReg _
       | Register _, MemRegOff _
       | Register _, MemRegRegOffMul _ ->
            (match opcodes.binop_can_swap with
                Some swap ->
                   swap, op1, op2
              | None ->
                   raise (X86Exception (pos, StringError "illegal operand order")))
       | _ ->
            raise (X86Exception (pos, StringError "illegal operand combination"))

(*
 * Print binary operands.
 * This is the usual case for MOV, ADD, etc. instructions.
 *)
let rec as_print_binary_operands buf opcodes pos pre op1 op2 next =
   let pos = string_pos "as_print_binary_operands buf" pos in
   let binop_dir, op1, op2 = binop_sort opcodes pos op1 op2 in
      match pre, op1, op2, opcodes with
         (* Register values *)
         _, Register r1, Register r2, { binop_reg_reg = Some opcode } ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf r1 r2

         (* Immediate values *)
       | IL, Register reg, ImmediateNumber off, { binop_reg_imm = Some (opcode, extra3);
                                                  binop_sign_bit = Some sign_bit } ->
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
            let opcodes = { opcodes with binop_sign_bit = None } in
            let reg = number_of_register pos reg in
               (* Short form *)
               as_print_pre_opcode buf8 pre (opcode lor sign_bit);
               as_print_modrm_reg_reg buf8 extra3 reg;
               as_print_imm8 buf8 off;

               (* Recursive call to print long form *)
               as_print_binary_operands buf32 opcodes pos pre op1 op2 next;

               (* Make the choice *)
               as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]
       | _, Register reg, ImmediateNumber off, { binop_eax_imm = Some opcode } when reg = eax ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_pre_imm buf pre off
       | _, Register reg, ImmediateNumber off, { binop_short_reg_imm = Some opcode } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode_shortreg buf pre opcode reg;
               as_print_pre_imm buf pre off
       | _, Register reg, ImmediateNumber off, { binop_reg_imm = Some (opcode, extra3) } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf extra3 reg;
               as_print_pre_imm buf pre off

       | IL, MemReg reg, ImmediateNumber off, { binop_mem_imm = Some (opcode, extra3);
                                                binop_sign_bit = Some sign_bit } ->
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
            let reg = number_of_register pos reg in
               (* Short form *)
               as_print_pre_opcode buf8 pre (opcode lor sign_bit);
               as_print_modrm_reg_memreg buf8 extra3 reg;
               as_print_imm8 buf8 off;

               (* Long form *)
               as_print_pre_opcode buf32 pre opcode;
               as_print_modrm_reg_memreg buf32 extra3 reg;
               as_print_imm32 buf32 off;

               (* Make the choice *)
               as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]
       | IL, MemRegOff (reg, off1), ImmediateNumber off2, { binop_mem_imm = Some (opcode, extra3);
                                                            binop_sign_bit = Some sign_bit } ->
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
            let reg = number_of_register pos reg in
               (* Short form *)
               as_print_pre_opcode buf8 pre (opcode lor sign_bit);
               as_print_modrm_reg_memregoff buf8 extra3 reg off1;
               as_print_imm8 buf8 off2;

               (* Long form *)
               as_print_pre_opcode buf32 pre opcode;
               as_print_modrm_reg_memregoff buf32 extra3 reg off1;
               as_print_imm32 buf32 off2;

               (* Make the choice *)
               as_print_choice buf off2 [interval_signed8, buf8;
                                      interval_all, buf32]
       | IL, MemRegRegOffMul (r1, r2, off1, mul), ImmediateNumber off2, { binop_mem_imm = Some (opcode, extra3);
                                                                          binop_sign_bit = Some sign_bit } ->
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
               (* Long form *)
               as_print_pre_opcode buf8 pre (opcode lor sign_bit);
               as_print_modrm_reg_memregregoffmul buf8 pos extra3 r1 r2 off1 mul;
               as_print_imm8 buf8 off2;

               (* Long form *)
               as_print_pre_opcode buf32 pre opcode;
               as_print_modrm_reg_memregregoffmul buf32 pos extra3 r1 r2 off1 mul;
               as_print_imm32 buf32 off2;

               (* Make the choice *)
               as_print_choice buf off2 [interval_signed8, buf8;
                                      interval_all, buf32]

       | _, MemReg reg, ImmediateNumber off, { binop_mem_imm = Some (opcode, extra3) } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memreg buf extra3 reg;
               as_print_pre_imm buf pre off
       | _, MemRegOff (reg, off1), ImmediateNumber off2, { binop_mem_imm = Some (opcode, extra3) } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregoff buf extra3 reg off1;
               as_print_pre_imm buf pre off2
       | _, MemRegRegOffMul (r1, r2, off1, mul), ImmediateNumber off2, { binop_mem_imm = Some (opcode, extra3) } ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregregoffmul buf pos extra3 r1 r2 off1 mul;
               as_print_pre_imm buf pre off2

         (* Direct addressing *)
       | _, Register reg, ImmediateLabel label, { binop_eax_label = Some opcode } when reg = eax ->
            let reg = number_of_register pos reg in
            let opcode = opcode lor binop_dir in
               as_print_pre_opcode buf pre opcode;
               as_print_label32 buf label opcodes.binop_relative next
       | _, Register reg, ImmediateLabel label, { binop_reg_label = Some (opcode, extra3) } ->
            let reg = number_of_register pos reg in
            let opcode = opcode lor binop_dir in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_label buf reg label opcodes.binop_relative next

         (* Indirect addressing *)
       | _, Register r1, MemReg r2, { binop_reg_mem = Some opcode } ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcode lor binop_dir in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memreg buf r1 r2
       | _, Register r1, MemRegOff (r2, off), { binop_reg_mem = Some opcode } ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcode lor binop_dir in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregoff buf r1 r2 off
       | _, Register r1, MemRegRegOffMul (r2, r3, off, mul), { binop_reg_mem = Some opcode } ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let r3 = number_of_register pos r3 in
            let opcode = opcode lor binop_dir in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregregoffmul buf pos r1 r2 r3 off mul
       | _ ->
            raise (X86Exception (pos, StringError "as_print_binop_int_inst buf: illegal operand combination"))

(*
 * Print a simple instruction, like a MOV.
 * We have several special cases here:
 *)
let as_print_binary_int_inst buf opcodes pos pre op1 op2 =
   as_print_binop_prefix buf opcodes;
   as_print_binary_operands buf opcodes pos pre op1 op2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
