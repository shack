(*
 * ModR/M byte printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Debug

open Bfd_state

open X86_inst_type
open X86_pos
open X86_exn

open X86_as_bfd.Buf
open X86_as_util

module Pos = MakePos (struct let name = "X86_as_modrm" end)
open Pos

(*
 * This is one of the more complicated parts.
 * The modr/m and SIB (scale/index/base) bytes
 * specify the operands.  Here's how it works.
 *
 * Section 1: simple addressing modes.
 *
 * --- In these addresses, r1 is not allowed to be %esp or %ebp.
 *
 *     operand1,operand2         mod nnn r/m ss index base off
 *     (%r1),%r3                 00  r3  r1
 *     disp8(%r1),%r3            01  r3  r1                disp8
 *     disp32(%r1),%r3           10  r3  r1                disp32
 *
 * Section 2: addressing modes with r/m=5 (%ebp)
 *
 *     operand1,operand2         mod nnn r/m ss index base off
 *     disp32,%r3                00  r3  5                 disp32
 *     disp8(%ebp),%r3           01  r3  5                 disp8
 *     disp32(%ebp),%r3          10  r3  5                 disp32
 *
 * Section 3: addressing modes with r/m=4 (%esp), including a SIB byte
 *
 * --- In these modes, %r2 is not allowed to be %esp,
 *     and %r1 is not allowed to be %ebp if mod=0
 *
 *     operand1,operand2         mod nnn r/m ss index base off
 *     (%r1+%r2*2^i),%r3         00  r3  4   i   r2    r1
 *     disp8(%r1+%r2*2^i),%r3    01  r3  4   i   r2    r1  disp8
 *     disp32(%r1+%r2*2^i),%r3   10  r3  4   i   r2    r1  disp32
 *
 * Section 4: addressing modes with r/m=4 (%esp), including
 *    a SIB byte, with no index (%r2 = %esp).
 *
 * --- In these modes, %r1 is not allowed to be %ebp
 *
 *     operand1,operand2         mod nnn r/m ss index base off
 *     (%r1),%r3                 00  r3  4   xx  4    r1
 *     disp8(%r1),%r3            01  r3  4   xx  4    r1   disp8
 *     disp32(%r1),%r3           10  r3  4   xx  4    r1   disp32
 *
 * Section 5: register-register addressing modes
 *
 *     operand1,operand2         mod nnn r/m ss index base off
 *     %r1,%r3                   11  r3  r1
 *
 * Note that several of these modes are redundant, but the earlier ones
 * are shorter generally, and so we hope they should be faster.
 *
 * To figure out how to code an operand, we have to invert this.
 * Here are the steps:
 *
 *     operand1,operand2
 *     disp32:
 *         Use section 2
 *
 *     (%r1),%r3:
 *         If r1 = %esp, then use section 4
 *         If r1 = %ebp, then use section 2, with disp8 = 0
 *         Otherwise, use section 1
 *
 *     disp(%r1),%r3:
 *         If r1 = %esp, then use section 4
 *         If r1 = %ebp, then use section 2, disp8 = 0
 *         Otherwise, use section 1
 *
 *     (%r1+%r2*2^i),%r3
 *         If r2 = %esp, abort
 *         If r1 = %ebp, then use section 3, with disp8 = 0
 *         Otherwise, use section 3
 *
 *     disp(%r1+%r2*2^i),%r3
 *         If r2 = %esp, abort
 *         Otherwise, use section 3
 *)

(*
 * Operands are a register and a register.
 *)
let as_print_modrm_reg_reg buf r1 r2 =
   bfd_print_int8 buf ((3 lsl 6) lor (r1 lsl 3) lor r2)

(*
 * Operand 1: %r1
 * Operand 2: (%r2)
 *)
let as_print_modrm_reg_memreg buf r1 r2 =
   match r2 with
      4 ->
         (* esp: use section 4 *)
         bfd_print_int8 buf ((r1 lsl 3) lor 4);
         bfd_print_int8 buf ((4 lsl 3) lor r2)
    | 5 ->
         (* ebp: use section 2 *)
         bfd_print_int8 buf ((1 lsl 6) lor (r1 lsl 3) lor 5);
         bfd_print_int8 buf 0
    | _ ->
         (* normal: use section 1 *)
         bfd_print_int8 buf ((r1 lsl 3) lor r2)

(*
 * Operand 1: %r1
 * Operand 2: off(%r2)
 *)
let as_print_modrm_reg_memregoff buf r1 r2 off =
   match r2 with
      4 ->
         (* esp: use section 4 *)
         let buf8 = new_buffer buf in
         let buf32 = new_buffer buf in
            (* 8-bit immediate *)
            bfd_print_int8 buf8 ((1 lsl 6) lor (r1 lsl 3) lor 4);
            bfd_print_int8 buf8 ((4 lsl 3) lor r2);
            as_print_imm8 buf8 off;

            (* 32-bit immediate *)
            bfd_print_int8 buf32 ((2 lsl 6) lor (r1 lsl 3) lor 4);
            bfd_print_int8 buf32 ((4 lsl 3) lor r2);
            as_print_imm32 buf32 off;

            (* Make the choice *)
            as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]
    | 5 ->
         (* ebp: use section 2 *)
         let buf8 = new_buffer buf in
         let buf32 = new_buffer buf in
            (* 8-bit immediate *)
            bfd_print_int8 buf8 ((1 lsl 6) lor (r1 lsl 3) lor 5);
            as_print_imm8 buf8 off;

            (* 32-bit immediate *)
            bfd_print_int8 buf32 ((2 lsl 6) lor (r1 lsl 3) lor 5);
            as_print_imm32 buf32 off;

            (* Make the choice *)
            as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]
    | _ ->
         (* Normal, use section 1 *)
         let buf8 = new_buffer buf in
         let buf32 = new_buffer buf in
            (* 8-bit immediate *)
            bfd_print_int8 buf8 ((1 lsl 6) lor (r1 lsl 3) lor r2);
            as_print_imm8 buf8 off;

            (* 32-bit immediate *)
            bfd_print_int8 buf32 ((2 lsl 6) lor (r1 lsl 3) lor r2);
            as_print_imm32 buf32 off;

            (* Make the choice *)
            as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]

(*
 * Operand 1: %r1
 * Operand 2: off(%r2,%r3,mul)
 *)
let as_print_modrm_reg_memregregoffmul buf pos r1 r2 r3 off mul =
   let pos = string_pos "as_print_modrm_reg_memmemregregoffmul buf" pos in
   let mul = log2 mul in
   let buf0 = new_buffer buf in
   let buf8 = new_buffer buf in
   let buf32 = new_buffer buf in
      if r3 = esp_index then
         raise (X86Exception (pos, StringError "as_print_modrm_memregregoffmul buf: esp can't be an index"));

      (* no offset *)
      bfd_print_int8 buf0 ((0 lsl 6) lor (r1 lsl 3) lor 4);
      bfd_print_int8 buf0 ((mul lsl 6) lor (r3 lsl 3) lor r2);

      (* 8-bit offset *)
      bfd_print_int8 buf8 ((1 lsl 6) lor (r1 lsl 3) lor 4);
      bfd_print_int8 buf8 ((mul lsl 6) lor (r3 lsl 3) lor r2);
      as_print_imm8 buf8 off;

      (* 32-bit offset *)
      bfd_print_int8 buf32 ((2 lsl 6) lor (r1 lsl 3) lor 4);
      bfd_print_int8 buf32 ((mul lsl 6) lor (r3 lsl 3) lor r2);
      as_print_imm32 buf32 off;

      (* Choices *)
      let choices =
         [interval_signed8, buf8;
          interval_all, buf32]
      in
      let choices =
         if debug debug_gas || r2 = ebp_index then
            choices
         else
            (interval_zero, buf0) :: choices
      in
          as_print_choice buf off choices

(*
 * Operand 1: a register
 * Operand 2: a label
 *)
let as_print_modrm_reg_label buf reg label rel next =
   bfd_print_int8 buf ((reg lsl 3) lor 5);
   as_print_label32 buf label rel next

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
