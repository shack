(*
 * Print migration data.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open X86_inst_type

open Frame_type
open X86_frame_type
open X86_frame

open Bfd

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_bfd.Buf
open X86_as_util
open X86_as_section
open X86_as_state

(*
 * Print migration data block.
 *)
let assem_migrate bfd info =
   if debug_as () then
      eprintf "*** AS: migrate@.";
   let { bfd_migrate = migrate } = info in
   let sect = data_section bfd in
   let buf = section_buf sect in

   (* Print identifier *)
   let () = bfd_align buf 16 in
   let () = bfd_print_ascii buf "Migration data " in

   (* Set the label *)
   let () = bfd_align buf 4 in
   let () = bfd_print_label buf SymGlobal migrate_data_label in

   (* Print the migration data *)
   let label, migrate =
      match migrate with
         None ->
            bfd_print_int32 buf Int32.zero;
            let label = seg_fault_labels.(other_fault) in
            let migrate =
               { mig_key       = string_of_symbol label;
                 mig_channel   = Int32.zero;
                 mig_bidir     = false;
                 mig_initofs   = Int32.zero;
                 mig_registers = Int32.pred Int32.zero;
                 mig_ptr_size  = Int32.zero;
                 mig_heap_size = Int32.zero;
                 mig_debug     = Int32.zero
               }
            in
               label, migrate
       | Some(migrate) ->
            bfd_print_int32 buf (Int32.of_int (-1));
            let label = Symbol.add migrate.mig_key in
               label, migrate
   in
      as_print_abs32 buf label;
      bfd_print_int32 buf migrate.mig_channel;
      bfd_print_int32 buf (if migrate.mig_bidir then Int32.one else Int32.zero);
      bfd_print_int32 buf migrate.mig_initofs;
      bfd_print_int32 buf migrate.mig_registers;
      bfd_print_int32 buf migrate.mig_ptr_size;
      bfd_print_int32 buf migrate.mig_heap_size;
      bfd_print_int32 buf migrate.mig_debug

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
