(*
 * x86 assembler utilities.  Notably, translation of
 * registers to numbers, and simple operand printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_frame
open X86_exn
open X86_pos

open Bfd
open X86_as_bfd.Buf

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get register numbers.
 *)
let registers = [eax; ecx; edx; ebx; esp; ebp; esi; edi]

let eax_index = 0
let ecx_index = 1
let edx_index = 2
let ebx_index = 3
let esp_index = 4
let ebp_index = 5
let esi_index = 6
let edi_index = 7

let number_of_register pos v =
   let rec search i v regs =
      match regs with
         v' :: regs ->
            if Symbol.eq v' v then
               i
            else
               search (succ i) v regs
       | [] ->
            raise (X86Exception (pos, IllegalRegister v))
   in
      search 0 v registers

(*
 * MMX numbers.
 *)
let mmx_registers = X86_frame.registers.(mmx_reg_class)

let number_of_mmx_register pos v =
   let rec search i v regs =
      match regs with
         v' :: regs ->
            if Symbol.eq v' v then
               i
            else
               search (succ i) v regs
       | [] ->
            raise (X86Exception (pos, IllegalRegister v))
   in
      search 0 v mmx_registers

(*
 * Condition codes.
 *)
let number_of_cc = function
   LT  -> 0xc
 | LE  -> 0xe
 | EQ  -> 0x4
 | NEQ -> 0x5
 | GT  -> 0xf
 | GE  -> 0xd
 | ULT -> 0x2
 | ULE -> 0x6
 | UGT -> 0x7
 | UGE -> 0x3

(*
 * Build a BFD expression from the offset.
 *)
let rec exp_of_offset = function
   OffNumber i ->
      ExpInt32 i
 | OffLabel l ->
      ExpLabel l
 | OffUMinus off ->
      ExpUMinus (exp_of_offset off)
 | OffUNot off ->
      ExpUNot (exp_of_offset off)
 | OffUAbs off ->
      ExpUAbs (exp_of_offset off)
 | OffULog2 off ->
      ExpULog2 (exp_of_offset off)
 | OffUPow2 off ->
      ExpUPow2 (exp_of_offset off)
 | OffAdd (off1, off2) ->
      ExpAdd (exp_of_offset off1, exp_of_offset off2)
 | OffSub (off1, off2) ->
      ExpSub (exp_of_offset off1, exp_of_offset off2)
 | OffRel (off, l) ->
      ExpRel (exp_of_offset off, l)
 | OffMul (off1, off2) ->
      ExpMul (exp_of_offset off1, exp_of_offset off2)
 | OffDiv (off1, off2) ->
      ExpDiv (exp_of_offset off1, exp_of_offset off2)
 | OffSal (off1, off2) ->
      ExpSal (exp_of_offset off1, exp_of_offset off2)
 | OffShr (off1, off2) ->
      ExpShr (exp_of_offset off1, exp_of_offset off2)
 | OffSar (off1, off2) ->
      ExpSar (exp_of_offset off1, exp_of_offset off2)
 | OffAnd (off1, off2) ->
      ExpAnd (exp_of_offset off1, exp_of_offset off2)
 | OffOr (off1, off2) ->
      ExpOr (exp_of_offset off1, exp_of_offset off2)
 | OffXor (off1, off2) ->
      ExpXor (exp_of_offset off1, exp_of_offset off2)

(*
 * Print an immediate.
 *)
let as_print_imm8 buf off =
   bfd_print_exp8 buf (exp_of_offset off)

let as_print_imm32 buf off =
   bfd_print_exp32 buf (exp_of_offset off)

let as_print_pre_imm buf pre off =
   let e = exp_of_offset off in
      match pre with
         IB ->
            bfd_print_exp8 buf e
       | IW ->
            bfd_print_exp16 buf e
       | IL ->
            bfd_print_exp32 buf e

(*
 * Possibly-relative label.
 *)
let as_print_abs32 buf label =
   bfd_print_exp32 buf (ExpLabel label)

let as_print_label32 buf label rel next =
   let exp =
      if rel then
         ExpRel (ExpLabel label, next)
      else
         ExpLabel label
   in
      bfd_print_exp32 buf exp

(*
 * Definitions.
 *)
let as_print_equ_int buf sym_type label i =
   bfd_print_equ buf sym_type label (ExpInt32 (Int32.of_int i))

let as_print_equ_int32 buf sym_type label i =
   bfd_print_equ buf sym_type label (ExpInt32 i)

(*
 * Intervals.
 *)
let interval_zero    = IntRange (Int32.zero, Int32.zero)
let interval_signed8 = IntRange (Int32.of_int (-128), Int32.of_int 127)
let interval_all     = IntAll

(*
 * Choice, given an offset.
 *)
let as_print_choice buf off choices =
   bfd_print_choice buf (exp_of_offset off) choices

(*
 * Take the log of a number.
 *)
let log2 i =
   let rec search j =
      let k = Int32.shift_left Int32.one j in
         if k = i then
            j
         else
            search (succ j)
   in
      search 0

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
