(*
 * x86 ModR/M byte printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_pos

open X86_as_bfd

(*
 * Register/register operands.
 *)
val as_print_modrm_reg_reg : buf -> int -> int -> unit

(*
 * Register/label operands.
 * Bool is true iff this should be a relative
 * address, and the second label subtracted.
 *)
val as_print_modrm_reg_label : buf -> int -> label -> bool -> label -> unit

(*
 * Second operand is memreg.
 *)
val as_print_modrm_reg_memreg : buf -> int -> int -> unit

(*
 * Second operand is memregoff.
 *)
val as_print_modrm_reg_memregoff : buf -> int -> int -> offset -> unit

(*
 * Second operand is memregregoffmul.
 *)
val as_print_modrm_reg_memregregoffmul : buf -> pos -> int -> int -> int -> offset -> int32 -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
