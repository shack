(*
 * x86 assembler utilities.  Notably, translation of
 * registers to numbers, and simple operand printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open X86_inst_type
open X86_pos

open Bfd
open X86_as_bfd

(*
 * Translate a register number into a symbol.
 *)
val number_of_register : pos -> symbol -> int
val number_of_mmx_register : pos -> symbol -> int

val eax_index : int
val ebx_index : int
val ecx_index : int
val edx_index : int
val esp_index : int
val ebp_index : int
val esi_index : int
val edi_index : int

(*
 * Condition codes.
 *)
val number_of_cc : cc -> int

(*
 * Bfd printing.
 *)
val as_print_pre_imm : buf -> iprec -> offset -> unit

val as_print_imm8 : buf -> offset -> unit
val as_print_imm32 : buf -> offset -> unit

val as_print_abs32 : buf -> label -> unit
val as_print_label32 : buf -> label -> bool -> label -> unit

(*
 * Definitions.
 *)
val as_print_equ_int : buf -> symbol_type -> label -> int -> unit
val as_print_equ_int32 : buf -> symbol_type -> label -> int32 -> unit

(*
 * Choices.
 *)
val interval_zero : interval
val interval_signed8 : interval
val interval_all : interval

val as_print_choice : buf -> offset -> (interval * buf) list -> unit

(*
 * Random utilities.
 *)
val log2 : int32 -> int

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
