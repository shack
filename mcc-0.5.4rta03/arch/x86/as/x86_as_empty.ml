(*
 * 0-ary integer instructions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode

(*
 * Instruction has no operands.
 *)
let as_print_empty_inst buf opcodes =
   as_print_nilop_prefix buf opcodes;
   bfd_print_int8 buf opcodes.nilop_normal

let as_print_empty_pre_inst buf opcodes pre =
   as_print_nilop_prefix buf opcodes;
   as_print_pre_opcode buf pre opcodes.nilop_normal



(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
