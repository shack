(*
 * Debug vars for the assembler.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Flags


(*
 * Register standard options for debugging.
 *)
let flag_as = "x86.as"
let flag_debug       = flag_as ^ ".debug"
let flag_make_stub   = flag_as ^ ".make_stub"
let flag_debug_stub  = flag_as ^ ".debug_stub"

let () = std_flags_help_section_text flag_as
   "Flags which are used to control the built-in X86 assembler."

let () = std_flags_register_list_help flag_as
  [flag_debug,       FlagBool false,
                     "Debug the assembler.";
   flag_make_stub,   FlagBool false,
                     "Generate stub code for linking to external C library functions.";
   flag_debug_stub,  FlagBool false,
                     "Warn about generated stub functions used for linking."]


(*
 * General debugging.
 *)
let debug_as ()   = std_flags_get_bool flag_debug


(*
 * Stub generation.
 *)
let make_stub ()  = std_flags_get_bool flag_make_stub
let debug_stub () = std_flags_get_bool flag_debug_stub


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
