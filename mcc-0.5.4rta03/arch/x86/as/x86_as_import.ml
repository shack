(*
 * Add the imports to the BFD.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open X86_frame

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_section
open X86_as_state

(*
 * Assemble all the block's text.
 * We'll sweep for data separately.
 *)
let assem_import buf info =
   if debug_as () then
      eprintf "*** AS: import@.";
   let { bfd_import = import } = info in
   let sect = undefined_section buf in
      (* Add standard imports *)
      SymbolTable.iter (fun v _ ->
            bfd_print_undef buf sect v) import;

      (* Add builtin imports *)
      List.iter (fun (v, _) ->
            bfd_print_undef buf sect v) import_vars

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
