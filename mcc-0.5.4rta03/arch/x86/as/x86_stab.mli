(*
 * Symbol table info for debugging.  The debugging support is partial
 * because gdb doesn't know much about our type system.  Also, we don't
 * really have _functions_ as far as gdb can tell because we have no stack.
 * Instead, we pretend that the entire program is a single function containing
 * a set of blocks in which variable declarations differ.
 *
 * This file provides the basic support for generating and
 * printing symbol table information.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol

open Fir_env

open X86_inst_type
open X86_frame

(*
 * We track debugging info.
 *)
type stab

(*
 * First test symbol.
 *)
val text_sym : symbol

(*
 * Stab formats.
 *)
val pp_print_stab_header  : formatter -> tenv -> string -> string -> stab
val pp_print_stab_trailer : stab -> formatter -> unit

(*
 * Initial block handling.
 *)
val pp_print_stab_block_header :
   stab ->                      (* Stab context *)
   formatter ->                 (* Assembly channel *)
   bool ->                      (* Start a new function? *)
   var ->                       (* Block name *)
   debug_info ->                (* Block info *)
   unit

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
