(*
 * Opcode definitions for the x86 platform.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type

(************************************************************************
 * CONSTANTS
 ************************************************************************)

(*
 * Simple opcodes have the following bit modifiers.
 *)
let opcode_prefix16                 = 0x66
let opcode_word_flag                = 0x01
let opcode_swap_operands_flag       = 0x02
let opcode_float_st0_is_dst_flag    = 0x00
let opcode_float_st0_is_src_flag    = 0x04
let opcode_sign_bit                 = 0x02
let opcode_mmx_swap_operands_flag   = 0x10

(*
 * Some default operands.
 *)
let st1 = FPStack Int32.one

(*
 * Alignment for function blocks.
 *)
let align_block = 64
let index_skip  = 52

(************************************************************************
 * 0-ARY OPCODES
 ************************************************************************)

(*
 * Integer instructions.
 *)
type nilop =
   { (*
      * Optional extra prefix byte.
      *)
      nilop_prefix     : int option;

      (*
       * Normal opcode.
       *)
      nilop_normal     : int
   }

let rmovs_opcodes =
   { nilop_prefix        = Some 0xf3;
     nilop_normal        = 0xa4
   }

let cltd_opcodes =
   { nilop_prefix        = None;
     nilop_normal        = 0x99
   }

let ret_opcodes =
   { nilop_prefix        = None;
     nilop_normal        = 0xc3
   }

let nop_opcodes =
   { nilop_prefix        = None;
     nilop_normal        = 0x90
   }

(************************************************************************
 * UNARY OPCODES
 ************************************************************************)

(*
 * Unary opcodes.
 *)
type unop =
   { (*
      * Optional extra prefix byte.
      *)
      unop_prefix      : int option;

      (*
       * Extra 3 bits of opcode.
       *)
      unop_extra3      : int;

      (*
       * Whether label operands should be relative.
       *)
      unop_relative    : bool;

      (*
       * If register operations are allowed,
       * this is the opcode to use.  The register
       * is coded as part of the modrm byte.
       *)
      unop_reg         : int option;

      (*
       * Sometimes the instruction has a short form
       * when the operand is a register.  This is the
       * opcode in that case, and the register is coded
       * as the lest 3 bits of the opcode.  The first
       * int is the opcode, the second is the word flag.
       *)
      unop_short_reg   : int option;

      (*
       * If immediate operands are allowed,
       * this is the opcode to use.
       *)
      unop_imm         : int option;

      (*
       * Sometimes an immediate operand can be sign-extended.
       *)
      unop_short_imm   : int option;

      (*
       * If label operands are allowed, this
       * is the opcode to use.
       *)
      unop_label       : int option;

      (*
       * Sometimes a label operand only requires a
       * single-byte opcode.
       *)
      unop_short_label : int option;

      (*
       * If indirect addresses are used, this is
       * the opcode to use.
       *)
      unop_mem         : int option
   }

let not_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 2;
     unop_relative      = false;
     unop_reg           = Some 0xf6;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xf6;
     unop_short_label   = None;
     unop_mem           = Some 0xf6
   }

let neg_opcodes =
   { unop_prefix        = Some 0xf3;
     unop_extra3        = 3;
     unop_relative      = false;
     unop_reg           = Some 0xf6;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xf6;
     unop_short_label   = None;
     unop_mem           = Some 0xf6
   }

let inc_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 0;
     unop_relative      = false;
     unop_reg           = Some 0xfe;
     unop_short_reg     = Some 0x40;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xfe;
     unop_short_label   = None;
     unop_mem           = Some 0xfe
   }

let dec_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 1;
     unop_relative      = false;
     unop_reg           = Some 0xfe;
     unop_short_reg     = Some 0x48;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xfe;
     unop_short_label   = None;
     unop_mem           = Some 0xfe;
   }

let mul_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 4;
     unop_relative      = false;
     unop_reg           = Some 0xf6;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xf6;
     unop_short_label   = None;
     unop_mem           = Some 0xf6
   }

let div_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 6;
     unop_relative      = false;
     unop_reg           = Some 0xf6;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xf6;
     unop_short_label   = None;
     unop_mem           = Some 0xf6
   }

let idiv_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 7;
     unop_relative      = false;
     unop_reg           = Some 0xf6;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0xf6;
     unop_short_label   = None;
     unop_mem           = Some 0xf6
   }

let push_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 6;
     unop_relative      = false;
     unop_reg           = Some 0xff;
     unop_short_reg     = Some 0x50;
     unop_imm           = Some 0x68;
     unop_short_imm     = Some 0x6a;
     unop_label         = Some 0xff;
     unop_short_label   = None;
     unop_mem           = Some 0xff
   }

let pop_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 0;
     unop_relative      = false;
     unop_reg           = Some 0x8f;
     unop_short_reg     = Some 0x58;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = Some 0x8f;
     unop_short_label   = None;
     unop_mem           = Some 0x8f
   }

let call_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 2;
     unop_relative      = true;
     unop_reg           = Some 0xff;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = None;
     unop_short_label   = Some 0xe8;
     unop_mem           = Some 0xff
   }

let jmp_opcodes =
   { unop_prefix        = None;
     unop_extra3        = 4;
     unop_relative      = true;
     unop_reg           = Some 0xff;
     unop_short_reg     = None;
     unop_imm           = None;
     unop_short_imm     = None;
     unop_label         = None;
     unop_short_label   = Some 0xe9;
     unop_mem           = Some 0xff
   }

(************************************************************************
 * BINARY OPCODES
 ************************************************************************)

(*
 * Opcodes for integer instructions.
 *)
type binop =
   { (*
      * Optional extra prefix byte.
      *)
      binop_prefix     : int option;

      (*
       * True iff label addresses should be specified using
       * relative addressing.
       *)
      binop_relative   : bool;

      (*
       * True iff the operand direction can be reversed.
       * This applies only to reg,mem and reg,label
       * operand combinations.
       *)
      binop_can_swap   : int option;

      (*
       * The normal register/register opcode.
       *)
      binop_reg_reg     : int option;

      (*
       * If register,imm forms are allowed, this is the
       * opcode to use.  The first int is the opcode, the second
       * is an extra 3 bits of instruction information.
       *
       * If this is not set, the reg,imm operand combination is
       * not allowed.
       *)
      binop_reg_label  : (int * int) option;
      binop_reg_imm    : (int * int) option;

      (*
       * Sometimes an opcode can take a byte immediate value
       * that is sign-extended if this bit is set.
       *)
      binop_sign_bit   : int option;

      (*
       * This is the opcode to use with reg,mem combinations.
       * mem is not a label, it is an indirect address (one of
       * the MemReg* forms).
       *
       * If this opcode is not set, then these kinds of
       * operands are not allowed.
       *)
      binop_reg_mem    : int option;

      (*
       * Sometimes we can have a mem,imm operation,
       * like for MOV.  This is the opcode and extra3 to use in
       * that case.
       *)
      binop_mem_imm    : (int * int) option;

      (*
       * Sometimes a unary instruction with a single
       * register operand has a short representation
       * where the register is coded directly as the least 3
       * bits of the opcode.
       *
       * INC %r => 0b01000rrr
       *)
      binop_short_reg  : int option;

      (*
       * Sometimes an instruction with register and an immediate
       * operand has a short representation where the register
       * is coded as the least 3 bits of this opcode, followed
       * by the immediate.
       *
       * MOV (%reg, $imm) = 0b1011wrrr imm
       *)
      binop_short_reg_imm  : int option;

      (*
       * Many instructions have a short form when %eax is used with
       * a label or immediate value.  These two fields give the opcodes for
       * those cases.  If these are not set, the reg_imm and reg_label
       * opcodes are used instead.
       *)
      binop_eax_label  : int option;
      binop_eax_imm    : int option
   }

let mov_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x88;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = Some 0xb0;
     binop_sign_bit      = None;
     binop_reg_imm       = Some (0xc6, 0);
     binop_mem_imm       = Some (0xc6, 0);
     binop_reg_label     = Some (0x88, 0);
     binop_reg_mem       = Some 0x88;
     binop_eax_label     = Some 0xa0;
     binop_eax_imm       = None
   }

let movs_opcodes =
   { binop_prefix        = Some 0x0f;
     binop_reg_reg       = Some 0xbe;
     binop_relative      = false;
     binop_can_swap      = Some 0;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = None;
     binop_reg_imm       = None;
     binop_mem_imm       = None;
     binop_reg_label     = Some (0xbe, 0);
     binop_reg_mem       = Some 0xbe;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

let movz_opcodes =
   { binop_prefix        = Some 0x0f;
     binop_reg_reg       = Some 0xb6;
     binop_relative      = false;
     binop_can_swap      = Some 0;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_mem_imm       = None;
     binop_sign_bit      = None;
     binop_reg_imm       = None;
     binop_reg_label     = Some (0xb6, 0);
     binop_reg_mem       = Some 0xb6;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

let lea_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = None;
     binop_relative      = false;
     binop_can_swap      = Some 0;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_mem_imm       = None;
     binop_sign_bit      = None;
     binop_reg_imm       = None;
     binop_reg_label     = Some (0x8d, 0);
     binop_reg_mem       = Some 0x8d;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

let add_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x00;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 0);
     binop_mem_imm       = Some (0x80, 0);
     binop_reg_label     = Some (0x00, 0);
     binop_reg_mem       = Some 0x00;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x04
   }

let adc_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x10;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 2);
     binop_mem_imm       = Some (0x80, 2);
     binop_reg_label     = Some (0x10, 0);
     binop_reg_mem       = Some 0x10;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x14
   }

let sub_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x28;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 5);
     binop_mem_imm       = Some (0x80, 5);
     binop_reg_label     = Some (0x28, 0);
     binop_reg_mem       = Some 0x28;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x2c
   }

let sbb_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x18;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 3);
     binop_mem_imm       = Some (0x80, 3);
     binop_reg_label     = Some (0x18, 0);
     binop_reg_mem       = Some 0x18;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x1c
   }

let and_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x20;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 4);
     binop_mem_imm       = Some (0x80, 4);
     binop_reg_label     = Some (0x20, 0);
     binop_reg_mem       = Some 0x20;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x24
   }

let or_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x08;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 1);
     binop_mem_imm       = Some (0x80, 1);
     binop_reg_label     = Some (0x08, 0);
     binop_reg_mem       = Some 0x08;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x0c
   }

let xor_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x30;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 6);
     binop_mem_imm       = Some (0x80, 6);
     binop_reg_label     = Some (0x30, 0);
     binop_reg_mem       = Some 0x30;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x34
   }

let cmp_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x38;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = Some opcode_sign_bit;
     binop_reg_imm       = Some (0x80, 7);
     binop_mem_imm       = Some (0x80, 7);
     binop_reg_label     = Some (0x38, 0);
     binop_reg_mem       = Some 0x38;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0x3c
   }

let test_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = Some 0x84;
     binop_relative      = false;
     binop_can_swap      = Some opcode_swap_operands_flag;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = None;
     binop_reg_imm       = Some (0xf6, 0);
     binop_mem_imm       = Some (0xf6, 0);
     binop_reg_label     = Some (0x84, 0);
     binop_reg_mem       = Some 0x84;
     binop_eax_label     = None;
     binop_eax_imm       = Some 0xa8
   }

(*
 * Doubleword shift.
 *)
let shld_opcodes =
   { binop_prefix        = Some 0x0f;
     binop_reg_reg       = Some 0xa5;
     binop_relative      = false;
     binop_can_swap      = None;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = None;
     binop_reg_imm       = Some (0xa4, 0);
     binop_mem_imm       = None;
     binop_reg_label     = Some (0xa5, 0);
     binop_reg_mem       = Some 0xa5;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

let shrd_opcodes =
   { binop_prefix        = Some 0x0f;
     binop_reg_reg       = Some 0xad;
     binop_relative      = false;
     binop_can_swap      = None;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = None;
     binop_reg_imm       = Some (0xac, 0);
     binop_mem_imm       = None;
     binop_reg_label     = Some (0xad, 0);
     binop_reg_mem       = Some 0xad;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

(*
 * IMUL opcodes are weird.
 * This is for a reg,imm operand combo.
 *)
let imul1_opcodes =
   { binop_prefix        = None;
     binop_reg_reg       = None;
     binop_relative      = false;
     binop_can_swap      = Some 0;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = None;
     binop_reg_imm       = Some (0x69, 0);
     binop_mem_imm       = None;
     binop_reg_label     = None;
     binop_reg_mem       = None;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

(*
 * This case covers reg/mem
 *)
let imul2_opcodes =
   { binop_prefix        = Some 0x0f;
     binop_reg_reg       = Some 0xaf;
     binop_relative      = false;
     binop_can_swap      = Some 0;
     binop_short_reg     = None;
     binop_short_reg_imm = None;
     binop_sign_bit      = None;
     binop_reg_imm       = None;
     binop_mem_imm       = None;
     binop_reg_label     = Some (0xaf, 0);
     binop_reg_mem       = Some 0xaf;
     binop_eax_label     = None;
     binop_eax_imm       = None
   }

(************************************************************************
 * MMX OPCODES
 ************************************************************************)

(*
 * Opcodes for integer instructions.
 *)
type mmxop =
   { (*
      * Optional extra prefix byte.
      *)
      mmx_prefix     : int option;

      (*
       * True iff the operand direction can be reversed.
       * This applies only to reg,mem and reg,label
       * operand combinations.
       *)
      mmx_can_swap   : int option;

      (*
       * The normal register/register opcode.
       *)
      mmx_reg_mmx_reg : int option;
      mmx_reg_reg : int option;

      (*
       * This is the opcode to use with reg,mem combinations.
       * mem is not a label, it is an indirect address (one of
       * the MemReg* forms).
       *
       * If this opcode is not set, then these kinds of
       * operands are not allowed.
       *)
      mmx_reg_mem    : int option;
   }

let movd_opcodes =
   { mmx_prefix        = Some 0x0f;
     mmx_reg_mmx_reg   = None;
     mmx_reg_reg       = Some 0x6e;
     mmx_can_swap      = Some opcode_mmx_swap_operands_flag;
     mmx_reg_mem       = Some 0x6e
   }

let movq_opcodes =
   { mmx_prefix        = Some 0x0f;
     mmx_reg_mmx_reg   = Some 0x6f;
     mmx_reg_reg       = None;
     mmx_can_swap      = Some opcode_mmx_swap_operands_flag;
     mmx_reg_mem       = Some 0x6f
   }

(************************************************************************
 * SHIFT OPCODES
 ************************************************************************)

(*
 * Shift operations are special.
 *)
type shift =
   { shift_reg_reg       : int;
     shift_reg_one       : int;
     shift_reg_imm       : int;
     shift_extra3        : int
   }

let sal_opcodes =
   { shift_reg_reg       = 0xd2;
     shift_reg_one       = 0xd0;
     shift_reg_imm       = 0xc0;
     shift_extra3        = 4
   }

let sar_opcodes =
   { shift_reg_reg       = 0xd2;
     shift_reg_one       = 0xd0;
     shift_reg_imm       = 0xc0;
     shift_extra3        = 7
   }

let shl_opcodes =
   { shift_reg_reg       = 0xd2;
     shift_reg_one       = 0xd0;
     shift_reg_imm       = 0xc0;
     shift_extra3        = 4
   }

let shr_opcodes =
   { shift_reg_reg       = 0xd2;
     shift_reg_one       = 0xd0;
     shift_reg_imm       = 0xc0;
     shift_extra3        = 5
   }

(************************************************************************
 * FLOAT OPCODES
 ************************************************************************)

(*
 * No operators.
 *)
let fchs_opcodes    = 0xd9e0
let fprem_opcodes   = 0xd9f8
let fsin_opcodes    = 0xd9fe
let fcos_opcodes    = 0xd9ff
let fpatan_opcodes  = 0xd9f3
let fsqrt_opcodes   = 0xd9fa
let fucompp_opcodes = 0xdae9
let fstsw_opcodes   = 0x9bdfe0
let finit_opcodes   = 0x9bdbe3

(*
 * Unary operations.
 *)
type funop =
   { funop_prefix          : int option;
     funop_mem_single      : (int * int) option;
     funop_mem_double      : (int * int) option;
     funop_mem_long_double : (int * int) option;
     funop_stack           : (int * int) option
   }

let fld_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd9, 0);
     funop_mem_double      = Some (0xdd, 0);
     funop_mem_long_double = Some (0xdb, 5);
     funop_stack           = Some (0xd9, 0)
   }

let fild_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xdb, 0);
     funop_mem_double      = Some (0xdf, 5);
     funop_mem_long_double = None;
     funop_stack           = None
   }

let fst_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd9, 2);
     funop_mem_double      = Some (0xdd, 2);
     funop_mem_long_double = None;
     funop_stack           = Some (0xdd, 2)
   }

let fstp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd9, 3);
     funop_mem_double      = Some (0xdd, 3);
     funop_mem_long_double = Some (0xdb, 7);
     funop_stack           = Some (0xdd, 3)
   }

let fist_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xdb, 2);
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = None
   }

let fistp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xdb, 3);
     funop_mem_double      = Some (0xdf, 7);
     funop_mem_long_double = None;
     funop_stack           = None
   }

let fxch_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xd9, 1)
   }

let faddp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xde, 0)
   }

let fsubp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xde, 4)
   }

let fmulp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xde, 1)
   }

let fdivp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xde, 6)
   }

let fsubrp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xde, 5)
   }

let fdivrp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xde, 7)
   }

let fucom_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xdd, 4)
   }

let fucomp_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = None;
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = Some (0xdd, 5)
   }

let fadd_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd8, 0);
     funop_mem_double      = Some (0xdc, 0);
     funop_mem_long_double = None;
     funop_stack           = Some (0xd8, 0)
   }

let fsub_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd8, 4);
     funop_mem_double      = Some (0xdc, 4);
     funop_mem_long_double = None;
     funop_stack           = Some (0xd8, 4)
   }

let fmul_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd8, 1);
     funop_mem_double      = Some (0xdc, 1);
     funop_mem_long_double = None;
     funop_stack           = Some (0xd8, 1)
   }

let fdiv_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd8, 6);
     funop_mem_double      = Some (0xdc, 6);
     funop_mem_long_double = None;
     funop_stack           = Some (0xd8, 6)
   }

let fsubr_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd8, 5);
     funop_mem_double      = Some (0xdc, 5);
     funop_mem_long_double = None;
     funop_stack           = Some (0xd8, 5)
   }

let fdivr_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd8, 7);
     funop_mem_double      = Some (0xdc, 7);
     funop_mem_long_double = None;
     funop_stack           = Some (0xd8, 7)
   }

let fstcw_opcodes =
   { funop_prefix          = Some 0x9b;
     funop_mem_single      = Some (0xd9, 7);
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = None
   }

let fldcw_opcodes =
   { funop_prefix          = None;
     funop_mem_single      = Some (0xd9, 5);
     funop_mem_double      = None;
     funop_mem_long_double = None;
     funop_stack           = None
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
