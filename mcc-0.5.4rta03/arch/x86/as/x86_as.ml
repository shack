(*
 * X86 assembler.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open X86_frame_type
open X86_frame
open X86_subst

open Bfd

open Mir_exn
open Mir_pos

open X86_as_bfd
open X86_as_prog
open X86_as_header
open X86_as_context
open X86_as_code
open X86_as_glob
open X86_as_debug
open X86_as_import
open X86_as_reserve
open X86_as_migrate
open X86_as_section
open X86_as_link
open X86_as_stub
open X86_as_fir

module Pos = MakePos (struct let name = "X86_as" end)
open Pos

(*
 * Check that block labels are unique.
 *)
let check_block_labels blocks =
   let add_block table block =
      let v = block.block_label in
         if SymbolSet.mem table v then
            raise (MirException (var_exp_pos v, StringVarError ("duplicate block", v)));
         SymbolSet.add table v
   in
      ignore (List.fold_left add_block SymbolSet.empty blocks)

(*
 * Add all the glue code.
 *)
let assem_glue bfd prog =
   let prog = assem_stubs bfd prog in
      assem_import bfd prog;
      assem_context bfd prog;
      assem_migrate bfd prog;
      assem_header bfd prog;
      assem_globals bfd prog;
      assem_reserve bfd prog;
      assem_debug bfd prog;
      assem_fun_table bfd prog;
      assem_fir bfd prog

(*
 * Assemble a single program.
 *)
let assem_prog outfile asm =
   let bfd = Bfd.create LittleEndian in
   let asm = spill_prog asm in
   let prog = bfd_of_asm_prog bfd asm in
   let blocks = asm.asm_blocks in
      assem_code bfd prog blocks;
      set_bfd_section bfd prog;
      assem_glue bfd prog;
      Bfd.bfd_print_elf outfile bfd

(*
 * Link multiple ELF files.
 *)
let link outfile infiles =
   let prog = assem_link infiles in
   let bfd = Bfd.bfd_of_bfd_io prog.bfd_bfd in
      set_bfd_section bfd prog;
      assem_glue bfd prog;
      Bfd.bfd_print_elf outfile bfd

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
