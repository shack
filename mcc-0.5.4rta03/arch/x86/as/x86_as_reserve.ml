(*
 * Print reserves for RES and COW instructions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open X86_inst_type

open Frame_type
open X86_frame_type
open X86_frame

open Bfd

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_bfd.Buf
open X86_as_util
open X86_as_section
open X86_as_state

(*
 * Print the variable in a reserve.
 *)
let as_print_reserve buf globals (label, vars) =
   let rec collect_regs = function
      arg :: args ->
         (* BUG: this code is not correct *)
         let arg =
            match arg with
               ResBase arg ->
                  arg
             | ResInfix (arg, _) ->
                  arg
         in
            (match arg with
                Register r ->
                   r :: collect_regs args
              | _ ->
                   collect_regs args)
    | [] ->
         []
   in
   let rec collect_off = function
      arg :: args ->
         (* BUG: this code is not correct *)
         let arg =
            match arg with
               ResBase arg ->
                  arg
             | ResInfix (arg, _) ->
                  arg
         in
            (match arg with
                MemReg ebp' ->
                   if Symbol.eq ebp' ebp then
                      OffNumber Int32.zero :: collect_off args
                   else
                      raise (Failure "as_print_reserve buf: address is not relative to ebp")
              | MemRegOff (ebp', off) ->
                   if Symbol.eq ebp' ebp then
                      off :: collect_off args
                   else
                      raise (Failure "as_print_reserve buf: address is not relative to ebp")
              | Register _ ->
                   collect_off args
              | _ ->
                   raise (Failure "as_print_reserve buf"))
    | [] ->
         []
   in
   let regs = collect_regs vars in
   let offs = collect_off vars in

   (* Get the byte mask for the registers *)
   let rec reg_mask i mask = function
      r :: regs' ->
         let mask =
            if List.mem r regs then
               mask lor (1 lsl i)
            else
               mask
         in
            reg_mask (succ i) mask regs'
    | [] ->
         mask
   in
   let mask = reg_mask 0 0 registers.(int_reg_class) in
      (* Print a mask of the register numbers that contain pointers *)
      bfd_align buf 4;
      bfd_print_label buf SymLocal label;
      bfd_print_int32 buf (Int32.of_int mask);

      (* Print the names of the spills that contain pointers *)
      List.iter (as_print_imm32 buf) offs;

      (* Always print the names of the global regs *)
      List.iter (as_print_abs32 buf) globals;

      (* Terminate the list *)
      bfd_print_int32 buf (Int32.of_int (-1))

(*
 * Find all the reserves in all the blocks.
 *)
let assem_reserve bfd info =
   if debug_as () then
      eprintf "*** AS: reserve@.";
   let { bfd_globals = globals;
         bfd_reserve = reserve
       } = info
   in
   let sect = data_section bfd in
   let buf = section_buf sect in

   (* Get the names of all the globals *)
   let global_names, _ =
      List.fold_left (fun (globals, i) (_, init) ->
            let globals =
               match init with
                  InitRawData _
                | InitAggrBlock _
                | InitTagBlock _
                | InitVar _ ->
                     let v = symbol_of_global_spill i in
                        v :: globals
                | InitRawInt _
                | InitRawFloat _ ->
                     globals
            in
               globals, succ i) ([], 0) globals
   in
   let global_names = List.rev global_names in
      (* Identify the data *)
      bfd_align buf 16;
      bfd_print_ascii buf "Reserve data   ";

      (* Issue all the blocks *)
      List.iter (as_print_reserve buf global_names) reserve

(*
 * Scan the instructions for reserve statements.
 *)
let reserve_inst reserves = function
   RES  (ImmediateLabel label, vars, _, _)
 | RES  (ImmediateNumber (OffLabel label), vars, _, _)
 | RESP (ImmediateLabel label, vars)
 | RESP (ImmediateNumber (OffLabel label), vars)
 | COW  (ImmediateLabel label, vars, _, _, _)
 | COW  (ImmediateNumber (OffLabel label), vars, _, _, _) ->
      (label, vars) :: reserves
 | _ ->
      reserves

(*
 * Print all the reserves in the instruction list.
 *)
let reserve_block reserves block =
   let { block_code = insts } = block in
      List.fold_left reserve_inst reserves insts

(*
 * Get all the reserves in all the blocks.
 *)
let get_reserves blocks =
   List.rev (Trace.fold reserve_block [] blocks)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
