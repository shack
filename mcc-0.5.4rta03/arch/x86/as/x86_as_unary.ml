(*
 * Unary instruction printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_exn
open X86_pos

open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm

module Pos = MakePos (struct let name = "X86_as_unary" end)
open Pos

(*
 * Print an operand for a unary instruction.
 *)
let as_print_unary_operand buf opcodes pos pre op next =
   let pos = string_pos "as_print_unary_operand buf" pos in
   let extra3 = opcodes.unop_extra3 in
      match pre, op, opcodes with
         (* Register values *)
         IL, Register reg, { unop_short_reg = Some opcode } ->
            let reg = number_of_register pos reg in
               as_print_opcode_shortreg buf opcode reg
       | _, Register reg, { unop_reg = Some opcode } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf extra3 reg

         (* Immediate *)
       | _, ImmediateNumber off, { unop_short_imm = Some opcode_short;
                                   unop_imm = Some opcode_long } ->
            let buf8 = new_buffer buf in
            let buf32 = new_buffer buf in
               (* Short form *)
               as_print_opcode buf8 opcode_short;
               as_print_imm8 buf8 off;

               (* Long form *)
               as_print_opcode buf32 opcode_long;
               as_print_imm32 buf32 off;

               (* Make the choice *)
               as_print_choice buf off [interval_signed8, buf8;
                                     interval_all, buf32]
       | _, ImmediateNumber off, { unop_imm = Some opcode } ->
            as_print_opcode buf opcode;
            as_print_imm32 buf off

         (* Direct addressing *)
       | IL, ImmediateLabel label, { unop_short_label = Some opcode } ->
            as_print_opcode buf opcode;
            as_print_label32 buf label opcodes.unop_relative next

       | _, ImmediateLabel label, { unop_label = Some opcode } ->
            as_print_pre_opcode buf pre opcode;
            as_print_modrm_reg_label buf extra3 label opcodes.unop_relative next

         (* Indirect addressing *)
       | _, MemReg reg, { unop_mem = Some opcode } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memreg buf extra3 reg
       | _, MemRegOff (reg, off), { unop_mem = Some opcode } ->
            let reg = number_of_register pos reg in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregoff buf extra3 reg off
       | _, MemRegRegOffMul (r1, r2, off, mul), { unop_mem = Some opcode } ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregregoffmul buf pos extra3 r1 r2 off mul
       | _ ->
            raise (X86Exception (pos, IllegalOperand op))

(*
 * Instruction has one operand.
 *)
let as_print_unary_int_inst buf opcodes pos pre op =
   let pos = string_pos "as_print_unary_int_inst buf" pos in
      as_print_unop_prefix buf opcodes;
      as_print_unary_operand buf opcodes pos pre op

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
