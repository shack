(*
 * Convert the ASM to a BFD prog.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Sizeof_const

open Frame_type
open X86_frame_type
open X86_frame

open Fir_marshal

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_reserve
open X86_as_code

let context_vars = context_vars
let import_vars  = List.map fst import_vars
let export_vars  = List.map fst export_vars

let spill_names new_spill count =
   let rec loop spills count =
      let count = pred count in
         if count < 0 then
            spills
         else
            loop (new_spill count :: spills) count
   in
      loop [] count

(*
 * Convert the import list.
 *)
let get_import info =
   let { Frame_type.import_name = name;
         Frame_type.import_info = info
       } = info
   in
      { import_name = name;
        import_info = info
      }

let get_imports import =
   SymbolTable.map get_import import

(*
 * Map each global to its global spill name.
 *)
let build_global_index globals =
   let globals, _ =
      List.fold_left (fun (globals, index) (v, _) ->
            let globals = SymbolTable.add globals v index in
               globals, succ index) (SymbolTable.empty, 0) globals
   in
      globals

(*
 * For the exports, we have to compute the globals index.
 *)
let get_export globals v info =
   let { Frame_type.export_name = name;
         Frame_type.export_is_fun = funp
       } = info
   in
   let glob =
      if funp then
         None
      else
         Some (SymbolTable.find globals v)
   in
      { export_name = name;
        export_glob = glob
      }

(*
 * Collect all the exports.
 *)
let get_exports globals exports =
   let globals = build_global_index globals in
      SymbolTable.mapi (get_export globals) exports

(*
 * Convert the assembly program.
 *)
let bfd_of_asm_prog bfd asm =
   let { asm_import = import;
         asm_export = export;
         asm_globals = globals;
         asm_floats = floats;
         asm_spills = spills;
         asm_bin_fir = fir;
         asm_migrate = migrate;
         asm_init = inits;
         asm_blocks = blocks;
         asm_arity = arity;
         asm_debug = debug;
       } = asm
   in
      { bfd_version = version;

        bfd_bfd = bfd_io_of_bfd bfd;
        bfd_import = get_imports import;
        bfd_export = get_exports globals export;
        bfd_globals = globals;
        bfd_floats = floats;
        bfd_fir = fir;
        bfd_migrate = migrate;
        bfd_init = inits;
        bfd_reserve = get_reserves blocks;
        bfd_fun_tags = get_fun_tags blocks;
        bfd_arity_tags = arity;
        bfd_debug = debug;

        bfd_context_vars = context_vars;
        bfd_import_vars = import_vars;
        bfd_export_vars = export_vars;

        bfd_spill_int_normal = spill_names symbol_of_int_normal_spill spills.spill_int_normal_index;
        bfd_spill_int_stdarg = spill_names symbol_of_int_stdarg_spill spills.spill_int_stdarg_index;
        bfd_spill_float_normal = spill_names symbol_of_float_normal_spill spills.spill_float_normal_index;
        bfd_spill_float_stdarg = spill_names symbol_of_float_stdarg_spill spills.spill_float_stdarg_index;
        bfd_spill_global = spill_names symbol_of_global_spill spills.spill_global_index;
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
