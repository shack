(*
 * Assemble the program header.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open X86_frame_type
open X86_frame
open Sizeof_const
open Sizeof_type

open Bfd

open X86_as_bfd
open X86_as_bfd.Buf
open X86_as_bfd.Bfd
open X86_as_section
open X86_as_util
open X86_as_state

(*
 * Alignment.
 *)
let do_align pos bfd_align =
   let bfd_align = pred bfd_align in
      (pos + bfd_align) land (lnot bfd_align)

(*
 * Size of a global.
 *)
let sizeof_init_exp = function
   InitRawInt i ->
      sizeof_rawint (Rawint.precision i)
 | InitRawFloat x ->
      sizeof_rawfloat (Rawfloat.precision x)
 | InitTagBlock _
 | InitAggrBlock _
 | InitRawData _
 | InitVar _ ->
      align_pointer, sizeof_pointer

(*
 * Print the spills in their own sections.
 *)
let assem_context bfd info =
   if debug_as () then
      eprintf "*** AS: context@.";
   let { bfd_globals = globals;
         bfd_context_vars = context_vars;
         bfd_spill_int_normal = spill_int_normal;
         bfd_spill_int_stdarg = spill_int_stdarg;
         bfd_spill_float_normal = spill_float_normal;
         bfd_spill_float_stdarg = spill_float_stdarg;
         bfd_spill_global = spill_global;
       } = info
   in
   let sect = absolute_section bfd in
   let buf = section_buf sect in

   (* Normal context vars *)
   let off =
      List.fold_left (fun off label ->
            as_print_equ_int buf SymLocal label off;
            off + sizeof_int32) 0 context_vars
   in

   let off =
      (* Normal integer spills *)
      List.fold_left (fun off label ->
            as_print_equ_int buf SymLocal label off;
            off + sizeof_int32) off spill_int_normal
   in

   let off =
      (* Standard argument integer spills go in same section as int spills *)
      List.fold_left (fun off label ->
            as_print_equ_int buf SymLocal label off;
            off + sizeof_int32) off spill_int_stdarg
   in

   let off =
      (* Float spills *)
      List.fold_left (fun off label ->
            as_print_equ_int buf SymLocal label off;
            off + align_long_double) off spill_float_normal
   in

   let off =
      List.fold_left (fun off label ->
            as_print_equ_int buf SymLocal label off;
            off + align_long_double) off spill_float_stdarg
   in

   let off =
      (* Globals *)
      let len1 = List.length spill_global in
      let len2 = List.length globals in
      let _ =
         if len1 <> len2 then
            raise (Invalid_argument (Printf.sprintf "X86_as_context.assem_context: Len1<>len2: %d, %d" len1 len2))
      in
         as_print_equ_int buf SymGlobal globals_index_base_label off;
         List.fold_left2 (fun off label (_, init) ->
               let bfd_align, size = sizeof_init_exp init in
               let off = do_align off bfd_align in
                  as_print_equ_int buf SymLocal label off;
                  off + size) off spill_global globals
   in
      as_print_equ_int buf SymGlobal globals_index_limit_label off;
      as_print_equ_int buf SymGlobal globals_size_label (List.length spill_global);
      as_print_equ_int buf SymGlobal context_size_label off;

      (* Hash codes *)
      if Fir_state.safety_enabled () then
         begin
            as_print_equ_int buf SymGlobal hash_index_label X86_runtime.hash_index_code;
            as_print_equ_int buf SymGlobal hash_fun_index_label X86_runtime.hash_fun_index_code
         end
      else
         begin
            as_print_equ_int buf SymGlobal hash_index_label 0;
            as_print_equ_int buf SymGlobal hash_fun_index_label 0
         end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
