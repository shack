(*
 * x86 opcode printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type

open X86_as_bfd.Buf

open X86_as_opcodes

(************************************************************************
 * OPCODE FORMATTING
 ************************************************************************)

(*
 * Many instructions have a simple opcode
 * format.
 *)
let as_print_opcode buf = bfd_print_int8 buf

(*
 * Some instructions code the register as the
 * least significant 3 bits of the opcode.
 *)
let as_print_opcode_shortreg buf opcode reg =
   let opcode = opcode lor reg in
      as_print_opcode buf opcode

(*
 * Many instructions have a simple opcode
 * format.
 *
 * The opcode is:
 *    IB: opcode
 *    IW: short_prefix (opcode lor binop_word_flag)
 *    IL: opcode lor binop_word_flag
 *)
let as_print_pre_opcode buf pre opcode =
   match pre with
      IB ->
         bfd_print_int8 buf opcode
    | IW ->
         bfd_print_int8 buf opcode_prefix16;
         bfd_print_int8 buf (opcode lor opcode_word_flag)
    | IL ->
         bfd_print_int8 buf (opcode lor opcode_word_flag)

(*
 * Some instructions code the register as the
 * least significant 3 bits of the opcode.
 *)
let as_print_pre_opcode_shortreg buf pre opcode reg =
   let opcode = opcode lor reg in
      match pre with
         IB ->
            bfd_print_int8 buf opcode
       | IW ->
            bfd_print_int8 buf opcode_prefix16;
            bfd_print_int8 buf (opcode lor (opcode_word_flag lsl 3))
       | IL ->
            bfd_print_int8 buf (opcode lor (opcode_word_flag lsl 3))

(*
 * Print the opcode prefix if there is one.
 *)
let as_print_nilop_prefix buf = function
   { nilop_prefix = Some opcode } ->
      bfd_print_int8 buf opcode
 | { nilop_prefix = None } ->
      ()

let as_print_unop_prefix buf = function
   { unop_prefix = Some opcode } ->
      bfd_print_int8 buf opcode
 | { unop_prefix = None } ->
      ()

let as_print_binop_prefix buf = function
   { binop_prefix = Some opcode } ->
      bfd_print_int8 buf opcode
 | { binop_prefix = None } ->
      ()

let as_print_mmx_prefix buf = function
   { mmx_prefix = Some opcode } ->
      bfd_print_int8 buf opcode
 | { mmx_prefix = None } ->
      ()

let as_print_funop_prefix buf = function
   { funop_prefix = Some opcode } ->
      bfd_print_int8 buf opcode
 | { funop_prefix = None } ->
      ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
