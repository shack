(*
 * Assemble the program header (nitialization+main functions).
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open X86_frame
open Sizeof_const

open Bfd

open X86_as_bfd
open X86_as_bfd.Buf
open X86_as_bfd.Bfd
open X86_as_section
open X86_as_util
open X86_as_state

(*
 * Default name for the context.
 *)
let context_sym = Symbol.add "c.ontext"

(*
 * File classes.
 *)
let assem_file_class fclass =
   match fclass with
      Fir.FileFC   -> X86_runtime.file_fc
    | Fir.FileJava -> X86_runtime.file_java
    | Fir.FileNaml -> X86_runtime.file_naml
    | Fir.FileAml  -> X86_runtime.file_aml

(*
 * Print the spills in their own sections.
 *)
let assem_header bfd info =
   if debug_as () then
      eprintf "*** AS: header@.";
   let { bfd_init = inits } = info in
   let sect = data_section bfd in
   let buf = section_buf sect in
      bfd_align buf 16;
      bfd_print_ascii buf "Program header ";

      bfd_align buf 4;
      bfd_print_label buf SymGlobal header_base_label;
      List.iter (fun (fclass, init_sym, main_sym) ->
            bfd_print_int32 buf (Int32.of_int X86_runtime.version_number);
            bfd_print_int32 buf (Int32.of_int context_length);
            bfd_print_int32 buf (assem_file_class fclass);
            as_print_abs32 buf init_sym;
            as_print_abs32 buf main_sym) inits;
      bfd_print_label buf SymGlobal header_limit_label

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
