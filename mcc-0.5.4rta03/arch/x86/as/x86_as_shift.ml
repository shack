(*
 * x86 shift instruction are special.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_frame
open X86_exn
open X86_pos

open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm

open X86_print

module Pos = MakePos (struct let name = "X86_as_shift" end)
open Pos

(*
 * Shifts are another weird instruction.
 *)
let as_print_shift_inst buf opcodes pos pre op1 op2 =
   (* Add the sign-extension prefix *)
   let pos = string_pos "as_print_shift_inst buf" pos in
   let extra3 = opcodes.shift_extra3 in
      match op1, op2 with
         (* Shift by cl *)
         Register r1, Register r2 ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcodes.shift_reg_reg in
               if r2 <> ecx_index then
                  raise (Invalid_argument "as_print_shift_inst buf: bogus second operand (must be immediate or ECX)");
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf extra3 r1

         (* Shift by 1 *)
       | Register reg, ImmediateNumber (OffNumber off) when off = Int32.one ->
            let reg = number_of_register pos reg in
            let opcode = opcodes.shift_reg_one in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf extra3 reg

         (* Shift by a non-zero constant *)
       | Register reg, ImmediateNumber (OffNumber off) ->
            let reg = number_of_register pos reg in
            let opcode = opcodes.shift_reg_imm in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf extra3 reg;
               bfd_print_int8 buf (Int32.to_int off)

         (* Shift memory a non-zero constant *)
       | ImmediateLabel label, ImmediateNumber (OffNumber off) ->
            let opcode = opcodes.shift_reg_imm in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_label buf extra3 label false label;
               bfd_print_int8 buf (Int32.to_int off)

       | MemReg reg, ImmediateNumber (OffNumber off) ->
            let reg = number_of_register pos reg in
            let opcode = opcodes.shift_reg_imm in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memreg buf extra3 reg;
               bfd_print_int8 buf (Int32.to_int off)

       | MemRegOff (reg, off1), ImmediateNumber (OffNumber off2) ->
            let reg = number_of_register pos reg in
            let opcode = opcodes.shift_reg_imm in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregoff buf extra3 reg off1;
               bfd_print_int8 buf (Int32.to_int off2)

       | MemRegRegOffMul (r1, r2, off1, mul), ImmediateNumber (OffNumber off2) ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcodes.shift_reg_imm in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregregoffmul buf pos extra3 r1 r2 off1 mul;
               bfd_print_int8 buf (Int32.to_int off2)

       | MemRegOff (r1, off1), Register r2 ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcodes.shift_reg_reg in
               if r2 <> ecx_index then
                  raise (Invalid_argument "as_print_shift_inst buf: bogus second operand (must be immediate or ECX)");
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregoff buf extra3 r1 off1

       | _ ->
            raise (X86Exception (pos, StringError "as_print_shift_inst buf: illegal operand combination"))

(*
 * 64-bit shifts.
 *)
let as_print_shiftd_inst buf opcodes pos pre op1 op2 op3 =
   as_print_binop_prefix buf opcodes;
   let pos = string_pos "as_print_shiftd_inst buf" pos in
   let opcode, off =
      match op3, opcodes with
         Register reg, { binop_reg_reg = Some opcode } ->
            if not (Symbol.eq reg ecx) then
               raise (Invalid_argument "as_print_shifd_inst buf: illegal shift register (must be ECX)");
            opcode, None
       | ImmediateNumber (OffNumber off), { binop_reg_imm = Some (opcode, _) } ->
            opcode, Some (Int32.to_int off)
       | _ ->
            raise (Invalid_argument ("as_print_shiftd_inst buf: illegal shift operand: " ^ (string_of_operand op3)))
   in
   let r3 =
      match op2 with
         Register r2 ->
            number_of_register pos r2
       | _ ->
            raise (Invalid_argument ("as_print_shiftd_inst buf: illegal second operand: " ^ (string_of_operand op2)))
   in
   let () =
      match op1 with
         (* Register *)
         Register r1 ->
            let r1 = number_of_register pos r1 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_reg buf r3 r1

         (* Indirect addressing *)
       | MemReg r1 ->
            let r1 = number_of_register pos r1 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memreg buf r3 r1
       | MemRegOff (r1, off) ->
            let r1 = number_of_register pos r1 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregoff buf r3 r1 off
       | MemRegRegOffMul (r1, r2, off, mul) ->
            let r1 = number_of_register pos r1 in
            let r2 = number_of_register pos r2 in
               as_print_pre_opcode buf pre opcode;
               as_print_modrm_reg_memregregoffmul buf pos r3 r1 r2 off mul
       | _ ->
            raise (Invalid_argument ("as_print_shiftd_inst buf: illegal first operand: " ^ (string_of_operand op1)))
   in
      match off with
         Some off ->
            bfd_print_int8 buf off
       | None ->
            ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
