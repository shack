(*
 * Assemble debug data.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open X86_runtime
open X86_frame_type
open X86_frame

open Bfd

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_bfd.Buf
open X86_as_util
open X86_as_section
open X86_as_state

(************************************************************************
 * DEBUG DATA
 ************************************************************************)

(*
 * Emit the debug variable.
 *)
let assem_debug bfd info =
   let debug = info.bfd_debug in
   let data = data_section bfd in
   let buf = section_buf data in
      bfd_align buf 16;
      bfd_print_ascii buf "Debug flags     ";
      bfd_print_label buf SymGlobal debug_flags_label;
      bfd_print_int32 buf debug

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
