(*
 * x86 opcode printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type

open X86_as_bfd
open X86_as_opcodes

(*
 * Opcode that is just a byte.
 *)
val as_print_opcode : buf -> int -> unit

(*
 * Short register version: reg is the least-significant 3 bits of opcode.
 *)
val as_print_opcode_shortreg : buf -> int -> int -> unit

(*
 * Opcode with integer precision.
 *)
val as_print_pre_opcode : buf -> iprec -> int -> unit

(*
 * Short register version: reg is the least-significant 3 bits of opcode.
 *)
val as_print_pre_opcode_shortreg : buf -> iprec -> int -> int -> unit

(*
 * Printing prefix bytes.
 *)
val as_print_nilop_prefix : buf -> nilop -> unit
val as_print_unop_prefix  : buf -> unop -> unit
val as_print_binop_prefix : buf -> binop -> unit
val as_print_mmx_prefix   : buf -> mmxop -> unit
val as_print_funop_prefix : buf -> funop -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
