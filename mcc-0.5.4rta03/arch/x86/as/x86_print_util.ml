(*
 * Basic printing commands.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Symbol

(************************************************************************
 * SYMBOL PRINTING
 ************************************************************************)

(*
 * Convert non-printable characters.
 *)
let rec buffer_add_escape buf s i len =
   if len <> 0 then
      let c = s.[i] in
      let () =
         match c with
            'a'..'z'
          | 'A'..'Z'
          | '0'..'9'
          | '_'
          | '.' ->
               Buffer.add_char buf c
          | _ ->
               Buffer.add_char buf '.';
               Buffer.add_string buf (string_of_int (Char.code c));
               Buffer.add_char buf '.'
      in
         buffer_add_escape buf s (succ i) (pred len)

(*
 * String padding.
 *)
let left_pad_string len s =
   let len' = String.length s in
      if len > len' then
         let s' = String.make len ' ' in
            String.blit s 0 s' 0 len';
            s'
      else
         s

let pp_print_padded_string len out s =
   let s = left_pad_string len s in
      pp_print_string out s

(*
 * Registers are preceded by a % symbol.
 *)
let string_of_register r =
   "%" ^ Symbol.string_of_symbol r

let pp_print_register out r =
   pp_print_string out (string_of_register r)

(*
 * Symbols are prefixed by a __
 *)
let buf = Buffer.create 100

let string_of_symbol l =
   let s = string_of_symbol l in
   let len = String.length s in
   let () =
      Buffer.clear buf;
      buffer_add_escape buf s 0 len
   in
      Buffer.contents buf

let pp_print_symbol out l =
   pp_print_string out (string_of_symbol l)

let pp_print_padded_symbol len out l =
   pp_print_padded_string len out (string_of_symbol l)

let string_of_short_symbol l =
   let s = Symbol.string_of_symbol l in
   let len = String.length s in
   let () =
      Buffer.clear buf;
      buffer_add_escape buf s 0 len
   in
      Buffer.contents buf

let pp_print_short_symbol out l =
   pp_print_string out (string_of_short_symbol l)

(*
 * Print a rawint.
 *)
let pp_print_rawint out i =
   fprintf out "%s" (Rawint.to_string i)

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
