(*
 * x86 binary integer instructions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open X86_inst_type
open X86_frame
open X86_exn
open X86_pos

open X86_as_bfd.Buf
open X86_as_opcodes
open X86_as_util
open X86_as_opcode
open X86_as_modrm

module Pos = MakePos (struct let name = "X86_as_mmx" end)
open Pos

(*
 * Operand directions.
 * The operands are always specified in some canonical
 * order based on operand type.  The opcode contains a bit
 * specifying whether to swap the operands, and we compute that
 * biut here.
 *)
let binop_sort opcodes pos op1 op2 =
   let pos = string_pos "mmx_binop_sort" pos in
      match op1, op2 with
         MMXRegister _, MMXRegister _
       | Register _, MMXRegister _
       | MemReg _, MMXRegister _
       | MemRegOff _, MMXRegister _
       | MemRegRegOffMul _, MMXRegister _ ->
            (match opcodes.mmx_can_swap with
                Some swap ->
                   swap, op2, op1
              | None ->
                   raise (X86Exception (pos, StringError "MMX: illegal operand order")))

       | MMXRegister _, Register _
       | MMXRegister _, ImmediateLabel _
       | MMXRegister _, MemReg _
       | MMXRegister _, MemRegOff _
       | MMXRegister _, MemRegRegOffMul _ ->
            0, op1, op2
       | _ ->
            raise (X86Exception (pos, StringError "MMX: illegal operand combination"))

(*
 * Print binary operands.
 * This is the usual case for MOV, ADD, etc. instructions.
 *)
let rec as_print_binary_operands buf opcodes pos op1 op2 next =
   let pos = string_pos "as_print_mmx_binary_operands buf" pos in
   let binop_dir, op1, op2 = binop_sort opcodes pos op1 op2 in
      match op1, op2, opcodes with
         (* Register values *)
         MMXRegister r1, MMXRegister r2, { mmx_reg_mmx_reg = Some opcode } ->
            let r1 = number_of_mmx_register pos r1 in
            let r2 = number_of_mmx_register pos r2 in
            let opcode = opcode lor binop_dir in
               as_print_opcode buf opcode;
               as_print_modrm_reg_reg buf r1 r2

       | MMXRegister r1, Register r2, { mmx_reg_reg = Some opcode } ->
            let r1 = number_of_mmx_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcode lor binop_dir in
               as_print_opcode buf opcode;
               as_print_modrm_reg_reg buf r1 r2

         (* Direct addressing *)
       | MMXRegister reg, ImmediateLabel label, { mmx_reg_mem = Some opcode } ->
            let reg = number_of_mmx_register pos reg in
            let opcode = opcode lor binop_dir in
               as_print_opcode buf opcode;
               as_print_modrm_reg_label buf reg label false next

         (* Indirect addressing *)
       | MMXRegister r1, MemReg r2, { mmx_reg_mem = Some opcode } ->
            let r1 = number_of_mmx_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcode lor binop_dir in
               as_print_opcode buf opcode;
               as_print_modrm_reg_memreg buf r1 r2
       | MMXRegister r1, MemRegOff (r2, off), { mmx_reg_mem = Some opcode } ->
            let r1 = number_of_mmx_register pos r1 in
            let r2 = number_of_register pos r2 in
            let opcode = opcode lor binop_dir in
               as_print_opcode buf opcode;
               as_print_modrm_reg_memregoff buf r1 r2 off
       | MMXRegister r1, MemRegRegOffMul (r2, r3, off, mul), { mmx_reg_mem = Some opcode } ->
            let r1 = number_of_mmx_register pos r1 in
            let r2 = number_of_register pos r2 in
            let r3 = number_of_register pos r3 in
            let opcode = opcode lor binop_dir in
               as_print_opcode buf opcode;
               as_print_modrm_reg_memregregoffmul buf pos r1 r2 r3 off mul

       | _ ->
            raise (X86Exception (pos, StringError "as_print_binop_mmx_inst buf: illegal operand combination"))

(*
 * Print a simple instruction, like a MOV.
 * We have several special cases here:
 *)
let as_print_binary_mmx_inst buf opcodes pos op1 op2 next =
   as_print_mmx_prefix buf opcodes;
   as_print_binary_operands buf opcodes pos op1 op2 next

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
