(*
 * Symbol table info for debugging.  The debugging support is partial
 * because gdb doesn't know much about our type system.  Also, we don't
 * really have _functions_ as far as gdb can tell because we have no stack.
 * Instead, we pretend that the entire program is a single function containing
 * a set of blocks in which variable declarations differ.
 *
 * This file provides the basic support for generating and
 * printing symbol table information.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2000 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format

open Debug
open Symbol
open Location

open Fir
open Fir_exn
open Fir_pos
open Fir_set
open Fir_env

open Frame_type
open Sizeof_const
open Sizeof_type

open X86_frame
open X86_inst_type
open X86_print_util

module Pos = MakePos (struct let name = "X86_stab" end)
open Pos

(************************************************************************
 * ABSTRACT STAB ENTRIES
 ************************************************************************)

type entry =
   (* Name of the file *)
   StabFile of string * label

   (*
    * Line numbering: line, pos, fun
    * This is line number "line", marked by label "pos"
    * within function "fun".
    *)
 | StabLine of int * label * label

   (*
    * Function info.
    * StabFunction (fun, line): function "fun" start at line "line".
    * StabTermFunction (term, fun): function "fun" ends at label "term".
    * StabBlock (flabel, line, slabel, elabel, vars):
    *   the variables in "vars" are in scope between "slabel"
    *   and "elabel" within the function "flabel"
    *)
 | StabBlock of label * int * label * label * debug_vars
 | StabFunction of label * int
 | StabTermFunction of label * label

   (* Type definitions *)
 | StabTypedef of ty_var * tydef
 | StabBuiltinIntType of string * int * bool * Rawint.int_precision
 | StabBuiltinFloatType of string * int * ty

(************************************************************************
 * Debug Context
 ************************************************************************)

(*
 * We keep:
 *   stab_tenv: type environment
 *   stab_file: current file being processed
 *   stab_line: current line number
 *   stab_vars: current variables in scope
 *   stab_term: label that will terminate the current scoping block
 *   stab_name: name of the current function
 *   stab_stab: stab entries
 *)
type stab =
   { stab_tenv : tenv;
     mutable stab_file : string;
     mutable stab_line : int;
     mutable stab_vars : (var * ty * operand) list;
     mutable stab_term : label;
     mutable stab_name : label;
     mutable stab_info : entry list
   }

(*
 * Default start text symbol.
 *)
let text_sym = new_symbol_string "text"

(************************************************************************
 * STAB PRINTING
 ************************************************************************)

(*
 * Stab keys.
 *)
let stab_n_gsym         = 0x20          (* Global symbol *)
let stab_n_fname        = 0x22          (* Function name (for BSD Fortran) *)
let stab_n_fun          = 0x24          (* Function name or text segment variable *)
let stab_n_stsym        = 0x26          (* Data segment file-scope variable *)
let stab_n_lcsym        = 0x28          (* BSS segment file-scope variable *)
let stab_n_main         = 0x2a          (* Name of main routine *)
let stab_n_rosyn        = 0x2c          (* Variable in .rodata section *)
let stab_n_pc           = 0x30          (* Global symbol (for Pascal) *)
let stab_n_nsyms        = 0x32          (* Number of symbols (according to Ultrix V4.0) *)
let stab_n_nomap        = 0x34          (* No DST map *)
let stab_n_obj          = 0x38          (* Object file (Solaris2) *)
let stab_n_opt          = 0x3c          (* Debugger options (Solaris2) *)
let stab_n_rsym         = 0x40          (* Register variable *)
let stab_n_m2c          = 0x42          (* Modula-2 compilation unit *)
let stab_n_sline        = 0x44          (* Line number in text segment *)
let stab_n_dsline       = 0x46          (* Line number in data segment *)
let stab_n_bsline       = 0x48          (* Line number in bss segment *)
let stab_n_brows        = 0x48          (* Sun source code browser, path to `.cb' file *)
let stab_n_defd         = 0x4a          (* GNU Modula2 definition module dependency *)
let stab_n_fline        = 0x4c          (* Function start/body/end line numbers (Solaris2) *)
let stab_n_ehdecl       = 0x50          (* GNU C++ exception variable *)
let stab_n_mod2         = 0x50          (* Modula2 info "for imc" (according to Ultrix V4.0) *)
let stab_n_catch        = 0x54          (* GNU C++ catch clause *)
let stab_n_ssym         = 0x60          (* Structure of union element *)
let stab_n_endm         = 0x62          (* Last stab for module (Solaris2) *)
let stab_n_so           = 0x64          (* Path and name of source file *)
let stab_n_lsym         = 0x80          (* Stack variable *)
let stab_n_bincl        = 0x82          (* Beginning of an include file (Sun only) *)
let stab_n_sol          = 0x84          (* Name of include file *)
let stab_n_psym         = 0xa0          (* Parameter variable *)
let stab_n_eincl        = 0xa2          (* End of an include file *)
let stab_n_entry        = 0xa4          (* Alternate entry point *)
let stab_n_lbrac        = 0xc0          (* Beginning of a lexical block *)
let stab_n_excl         = 0xc2          (* Place holder for a deleted include file *)
let stab_n_scope        = 0xc4          (* Modula2 scope information (Sun linker) *)
let stab_n_rbrac        = 0xe0          (* End of a lexical block *)
let stab_n_bcomm        = 0xe2          (* Begin named common block *)
let stab_n_ecomm        = 0xe4          (* End named common block *)
let stab_n_ecoml        = 0xe8          (* Member of a common block *)
let stab_n_with         = 0xea          (* Pascal with statement: type,,0,0,offset (Solaris2) *)
let stab_n_nbtext       = 0xf0          (* Gould non-base registers *)
let stab_n_nbdata       = 0xf2          (* Gould non-base registers *)
let stab_n_nvvss        = 0xf4          (* Gould non-base registers *)
let stab_n_nbsts        = 0xf6          (* Gould non-base registers *)
let stab_n_nblcs        = 0xf8          (* Gould non-base registers *)

(*
 * Print builtin types.
 *)
let int32_type_index  = 1
let int8_type_index   = 2
let int16_type_index  = 8
let int64_type_index  = 6
let uint32_type_index = 4
let uint8_type_index  = 11
let uint16_type_index = 9
let uint64_type_index = 7
let void_type_index   = 0
let double_type_index = 13

let builtin_raw_int_types =
   ["int", int32_type_index, true, Rawint.Int32;
    "char", int8_type_index, true, Rawint.Int8;
    "short", int16_type_index, true, Rawint.Int16;
    "long",  int64_type_index, true, Rawint.Int64;
    "unsigned int", uint32_type_index, false, Rawint.Int32;
    "short unsigned int", uint16_type_index, false, Rawint.Int16;
    "long unsigned int", uint64_type_index, false, Rawint.Int64;
    "unsigned char", uint8_type_index, false, Rawint.Int8;
    "void", void_type_index, false, Rawint.Int32]

let builtin_float_types =
   ["double", double_type_index, TyFloat Rawfloat.Double]

(************************************************************************
 * STAB OPERATIONS
 ************************************************************************)

(*
 * Add a stab command.
 *)
let add_stab stab info =
   stab.stab_info <- info :: stab.stab_info

(************************************************************************
 * PRINTING
 ************************************************************************)

(*
 * Print a int definition.
 *)
let pp_print_rawint_type out name index signed pre =
   let s_min = Rawint.to_string (Rawint.min_int pre signed) in
   let s_max = Rawint.to_string (Rawint.max_int pre signed) in
      fprintf out "\t.stabs\t\"%s:t(0,%d)=r(0,%d);%s;%s;\",%d,0,0,0\t/* N_LSYM */\n" (**)
         name index index s_min s_max stab_n_lsym

(*
 * Print a float definition.
 *)
let pp_print_float_type out tenv pos name index ty =
   let pos = string_pos "pp_print_float_type buf" pos in
      fprintf out "\t.stabs\t\"%s:t(0,%d)=r(0,1);%d;0;\",%d,0,0,0\t/* N_LSYM */\n" (**)
         name index (sizeof_aligned_type_dir tenv pos ty) stab_n_lsym

(*
 * Print the fields in a struct.
 * Give them numeric labels.
 *)
let rec pp_print_aggr_field buf tenv pos out (index, off) ty =
   let pos = string_pos "pp_print_aggr_field buf" pos in
   let size = sizeof_aligned_type_dir tenv pos ty in
      fprintf out "l%d:" index;
      pp_print_type tenv (int_pos 2 pos) out ty;
      fprintf out ",%d,%d;" (off * 8) (size * 8);
      succ index, off + size

(*
 * Print a type definition.
 *)
and pp_print_type tenv pos out ty =
   let pos = string_pos "pp_print_type buf" pos in
      match ty with
         TyInt
       | TyEnum _ ->
            fprintf out "(0,%d)" int32_type_index
       | TyRawInt (pre, signed) ->
            let index =
               match pre, signed with
                  Rawint.Int8,  true -> int8_type_index
                | Rawint.Int16, true -> int16_type_index
                | Rawint.Int32, true -> int32_type_index
                | Rawint.Int64, true -> int64_type_index
                | Rawint.Int8,  false -> uint8_type_index
                | Rawint.Int16, false -> uint16_type_index
                | Rawint.Int32, false -> uint32_type_index
                | Rawint.Int64, false -> uint64_type_index
            in
               fprintf out "(0,%d)" index
       | TyFloat pre ->
            fprintf out "(0,%d)" double_type_index
       | TyFun _
       | TyRawData
       | TyVar _
       | TyArray _
       | TyUnion _
       | TyTuple _
       | TyProject _
       | TyCase _
       | TyObject _
       | TyPointer _
       | TyFrame _
       | TyDTuple _
       | TyTag _ ->
            fprintf out "*(0,%d)" void_type_index
       | TyApply (v, _) ->
            fprintf out "(0,%d)" (Symbol.to_int v)
       | TyExists (_, ty)
       | TyAll (_, ty) ->
            pp_print_type tenv pos out ty
       | TyDelayed ->
            raise (FirException (pos, StringError "TyDelayed not allowed"))

(*
 * Typedefs.
 *)
let pp_print_tydef tenv pos out tydef =
   let pos = string_pos "pp_print_tydef" pos in
      match tydef with
         TyDefLambda ([], ty) ->
            pp_print_type tenv pos out ty
       | TyDefUnion _
       | TyDefLambda _
       | TyDefDTuple _ ->
            fprintf out "*(0,%d)" void_type_index

let pp_print_typedef tenv pos out v tydef =
   let pos = string_pos "pp_print_typedef buf" pos in
      fprintf out "\t.stabs\t\"%s:t(0,%d)=" (**)
      (Symbol.to_string v) (Symbol.to_int v);
      pp_print_tydef tenv pos out tydef;
      fprintf out "\",%d,0,0,0\t/* N_LSYM */\n" stab_n_lsym

(*
 * Print debug info for all the vars.
 *)
let pp_print_debug_var tenv line out (v, ty, op) =
   let pos = string_pos "pp_print_debug_var buf" (var_exp_pos v) in
      match op with
         Register r ->
            fprintf out "\t.stabs\t\"%a:r%a\"," pp_print_short_symbol v (pp_print_type tenv pos) ty;
            fprintf out "%d,0,%d,%d\t/* N_RSYM */\n" stab_n_rsym line (get_gdb_register_number r)
       | MemRegOff (_, OffNumber off) ->
            fprintf out "\t.stabs\t\"%a:%a\"," pp_print_short_symbol v (pp_print_type tenv pos) ty;
            fprintf out "%d,0,%d,%s\t/* N_LSYM */\n" stab_n_lsym line (Int32.to_string off)
       | _ ->
            ()

(*
 * Print a block scope.
 *)
let pp_print_scope out tenv flabel line slabel elabel vars =
   if vars <> [] then
      begin
         List.iter (pp_print_debug_var tenv line out) vars;
         fprintf out "\t.stabn\t%d,0,%d,%a-%a\t/* N_LBRAC */\n" (**)
            stab_n_lbrac line pp_print_symbol slabel pp_print_symbol flabel;
         fprintf out "\t.stabn\t%d,0,%d,%a-%a\t/* N_RBRAC */\n" (**)
            stab_n_rbrac line pp_print_symbol elabel pp_print_symbol flabel
      end

(*
 * Print a file.
 *)
let pp_print_file out name label =
   fprintf out "\t.stabs\t\"%s\",%d,0,0,%a\t/* N_SO */\n" (**)
      name stab_n_so pp_print_symbol label

(*
 * Print a line.
 *)
let pp_print_line out line clabel flabel =
   fprintf out "\t.stabn\t%d,0,%d,%a-%a\t/* N_SLINE */\n" (**)
      stab_n_sline line
      pp_print_symbol clabel
      pp_print_symbol flabel

(*
 * Print the function declaration.
 *)
let pp_print_function out flabel line =
   (fprintf out "\t.stabs\t\"%a:F(0,%d)\",%d,0,%d,%a\t/* N_FUN */\n" (**)
       pp_print_symbol flabel
       void_type_index
       stab_n_fun line
       pp_print_symbol flabel)

let pp_print_term_function out clabel flabel =
   fprintf out "\t.stabs\t\"\",%d,0,0,%a-%a\t/* N_FUN */\n" (**)
      stab_n_fun pp_print_symbol clabel pp_print_symbol flabel

(*
 * Print a stab entry.
 *)
let pp_print_stab_entry out tenv info =
   match info with
      StabFile (name, label) ->
         pp_print_file out name label
    | StabLine (line, clabel, flabel) ->
         pp_print_line out line clabel flabel
    | StabBlock (flabel, line, slabel, elabel, vars) ->
         pp_print_scope out tenv flabel line slabel elabel vars
    | StabFunction (flabel, line) ->
         pp_print_function out flabel line
    | StabTermFunction (clabel, flabel) ->
         pp_print_term_function out clabel flabel
    | StabTypedef (v, tydef) ->
         pp_print_typedef tenv (var_exp_pos v) out v tydef
    | StabBuiltinIntType (name, index, signed, pre) ->
         pp_print_rawint_type out name index signed pre
    | StabBuiltinFloatType (name, index, size) ->
         pp_print_float_type out tenv (var_exp_pos (Symbol.add name)) name index size

(*
 * Print the stabs info.
 *)
let pp_print_stabs stab out =
   let { stab_info = info; stab_tenv = tenv } = stab in
      fprintf out "#\n# Symbol table info (this can be safely deleted)\n#\n";
      List.iter (pp_print_stab_entry out tenv) (List.rev info)

(************************************************************************
 * BLOCK FORMATTING
 ************************************************************************)

(*
 * Switch context between functions.
 *)
let pp_print_stab_fun_trailer stab out =
   let { stab_term = term; stab_name = name } = stab in
      fprintf out "%a:\n\t.size\t%a,%a-%a\n" (**)
         pp_print_symbol term
         pp_print_symbol name
         pp_print_symbol term
         pp_print_symbol name;
      add_stab stab (StabTermFunction (term, name))

let pp_print_stab_block_trailer stab out =
   if stab.stab_name <> text_sym then
      pp_print_stab_fun_trailer stab out

(*
 * Start a new function block.
 *)
let pp_print_stab_block_header stab out gflag label debug =
   let { stab_file   = file1;
         stab_line   = line1;
         stab_vars   = vars1;
         stab_term   = term;
         stab_name   = name
       } = stab
   in
   let loc, vars2 = debug in
   let file2, line2, _, _, _ = dest_loc loc in
   let file2 = Symbol.to_string file2 in
      if gflag then
         let label2 = new_symbol label in
            pp_print_stab_block_trailer stab out;
            add_stab stab (StabFunction (label, line2));
            add_stab stab (StabBlock (label, line2, label, label2, vars2));
            stab.stab_file <- file2;
            stab.stab_line <- line2;
            stab.stab_vars <- vars2;
            stab.stab_name <- label;
            stab.stab_term <- label2;
      else if file2 <> file1 || line2 <> line1 then
         let label2 = new_symbol label in
            if term <> text_sym then
               fprintf out "%a:\n" pp_print_symbol term;
            add_stab stab (StabLine (line2, label, name));
            add_stab stab (StabBlock (name, line2, label, label2, vars2));
            stab.stab_file <- file2;
            stab.stab_line <- line2;
            stab.stab_vars <- vars2;
            stab.stab_term <- label2

(************************************************************************
 * FILE FORMATTING
 ************************************************************************)

(*
 * Add all the raw int types.
 *)
let add_raw_int_types table =
   List.fold_left (fun table (s, index, signed, pre) ->
         let entry = StabBuiltinIntType (s, index, signed, pre) in
            entry :: table) table builtin_raw_int_types

(*
 * Print the entire type environment.
 *)
let add_tenv table tenv =
   SymbolTable.fold (fun table v tydef ->
         let pos = string_pos "add_tenv" (var_exp_pos v) in
         let entry = StabTypedef (v, tydef) in
            entry :: table) table tenv

(*
 * Print stab header.
 *)
let pp_print_stab_header out tenv dir file =
   let entry1 = StabFile (dir ^ "/", text_sym) in
   let entry2 = StabFile (file, text_sym) in
   let table = add_raw_int_types [entry2; entry1] in
   let table = add_tenv table tenv in
      fprintf out "\t.file\t\"%s\"\n\t.version  \"01.01\"\n" (Mc_string_util.c_escaped file);
      fprintf out "\t.text\n%a:\n" pp_print_symbol text_sym;

      (* Initial state *)
      { stab_tenv = tenv;
        stab_file = file;
        stab_line = 0;
        stab_vars = [];
        stab_term = text_sym;
        stab_name = text_sym;
        stab_info = table
      }

(*
 * Print the trailer.
 *)
let pp_print_stab_trailer stab out =
   (* Make sure the current block is terminated *)
   pp_print_stab_block_trailer stab out;

   (* Now print the stab info *)
   pp_print_stabs stab out

(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
