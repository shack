(*
 * Stub generator for calling external functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Sizeof_const

open Mir

open Frame_type
open X86_inst_type
open X86_frame_type
open X86_frame
open X86_runtime
open X86_build

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_section
open X86_as_code
open X86_as_state

(*
 * Empty debugging info.
 *)
let debug_empty = bogus_loc "<X86_as_stub>", []

(*
 * There should be exactly 4 initial argument:
 *    1. continuation fun
 *    2. continuation env
 *    3. exception fun
 *    4. exception env
 *
 * We don't need to push the exception handler.
 *)
let rec get_pre_args args =
   match args with
      [cont_fun; cont_env; _; _] ->
         [], cont_fun, cont_env
    | arg :: tl ->
         let args, cont_fun, cont_env = get_pre_args tl in
            arg :: args, cont_fun, cont_env
    | [] ->
         raise (Invalid_argument "X86_as_stub.push_cont_args: function does not have continuation")

let push_pre_args insts args =
   let asm_args, cont_fun, cont_env = get_pre_args args in
   let insts, _ = push_external_args insts [cont_env; cont_fun] in
   let insts = Listbuf.add insts (PUSH (IL, Register ebp)) in
      insts, (cont_fun, cont_env), asm_args

(*
 * Push the called arguments.
 *)
let push_args insts name orig_args asm_args =
   let rec push_args insts size orig_args asm_args =
      match orig_args, asm_args with
         Fir.ArgPointer :: orig_args, ArgInt32 base :: ArgInt32 off :: asm_args ->
            let insts = Listbuf.add_list insts
               [MOV (IL, Register eax, base);
                ADD (IL, Register eax, off);
                PUSH (IL, Register eax)]
            in
               push_args insts (size + sizeof_int32) orig_args asm_args
       | Fir.ArgRawInt (Rawint.Int32, _) :: orig_args, ArgInt32 op :: asm_args ->
            let insts = Listbuf.add insts (PUSH (IL, op)) in
               push_args insts (size + sizeof_int32) orig_args asm_args
       | Fir.ArgRawFloat pre :: orig_args, ArgFloat (op, _) :: asm_args ->
            let size' = float_prec_size pre in
            let pre   = float_prec pre in
            let insts = Listbuf.add_list insts
               [SUB (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size')));
                FMOVP (pre, MemReg esp, FT, op)]
            in
               push_args insts (size + size') orig_args asm_args
       | Fir.ArgFunction :: _, _ ->
            raise (Invalid_argument "X86_as_stub.push_args: can't pass functions to external C")
       | [], [] ->
            insts, size
       | [], _ :: _ ->
            eprintf "X86_as_stub.push_args: extra args in %s@." name;
            insts, size
       | _ ->
            raise (Invalid_argument "X86_as_stub.push_args: illegal operand combination")
   in
      push_args insts 0 orig_args asm_args

(*
 * Push the called arguments,
 * but don't convert pointers.
 *)
let push_stdargs insts name orig_args asm_args =
   let rec push_args insts size orig_args asm_args =
      match orig_args, asm_args with
         Fir.ArgPointer :: orig_args, ArgInt32 base :: ArgInt32 off :: asm_args ->
            let insts = Listbuf.add_list insts
               [PUSH (IL, off);
                PUSH (IL, base)]
            in
               push_args insts (size + 2 * sizeof_int32) orig_args asm_args
       | Fir.ArgRawInt (Rawint.Int32, _) :: orig_args, ArgInt32 op :: asm_args ->
            let insts = Listbuf.add insts (PUSH (IL, op)) in
               push_args insts (size + sizeof_int32) orig_args asm_args
       | Fir.ArgRawFloat pre :: orig_args, ArgFloat (op, _) :: asm_args ->
            let size' = float_prec_size pre in
            let pre   = float_prec pre in
            let insts = Listbuf.add_list insts
               [SUB (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size')));
                FMOVP (pre, MemReg esp, FT, op)]
            in
               push_args insts (size + size') orig_args asm_args
       | Fir.ArgFunction :: _, _ ->
            raise (Invalid_argument "X86_as_stub.push_stdargs: can't pass functions to external C")
       | [], [] ->
            insts, size
       | [], _ :: _ ->
            eprintf "X86_as_stub.push_args: extra args in %s@." name;
            insts, size
       | _ ->
            raise (Invalid_argument "X86_as_stub.push_stdargs: illegal operand combination")
   in
      push_args insts 0 orig_args asm_args

(*
 * Make space for the varargs.
 *)
let format_stdargs insts =
   Listbuf.add_list insts
      [(* Get the block size *)
       MOV (IL, Register eax, MemRegOff (ebp, OffNumber Int32.zero));
       MOV (IL, Register eax, MemRegOff (eax, size_offset));
       AND (IL, Register eax, ImmediateNumber (OffNumber size_aggr_mask32));

       (* Reserve this much more space on the stack *)
       SUB (IL, Register esp, Register eax);

       (* Push the format args *)
       PUSH (IL, Register esp);
       PUSH (IL, MemRegOff (ebp, OffNumber (Int32.of_int 4)));
       PUSH (IL, MemRegOff (ebp, OffNumber Int32.zero));
       PUSH (IL, MemRegOff (ebp, OffNumber (Int32.of_int 8)));

       (* Call the formatter *)
       CALL (ImmediateLabel format_stdargs_label);

       (* Pop the extra args *)
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int 16)))]

(*
 * Call the function.
 *)
let call_extern insts label cont_args =
   let insts = Listbuf.add insts (CALL (ImmediateLabel label)) in
      match cont_args with
         [_; ArgInt32 result] ->
            Listbuf.add insts (MOV (IL, result, Register eax))
       | _ ->
            raise (Invalid_argument "X86_as_stub.call_extern: result is not a variable")

(*
 * Pop the result arguments.
 *)
let pop_args insts size =
   Listbuf.add insts (LEA (IL, Register esp, MemRegOff (ebp, OffNumber (Int32.of_int size))))

(*
 * Pop the remaining arguments.
 *)
let pop_post_args insts args =
   let insts = Listbuf.add insts (POP (IL, Register ebp)) in
   let insts, _ = pop_external_args insts [List.hd args] in
      insts

(*
 * Call the continuation.
 *)
let do_cont insts =
   Listbuf.add insts RET

(*
 * Generate a function stub.
 * We have these calling conventions:
 *    arg1: the continuation function
 *    arg2: the continuation environment
 *
 * The arguments are given by X86_frame.
 *)
let function_stub label name stdargsp orig_args asm_args =
   if debug_stub () then
      eprintf "Generating stub for %s@." name;

   (* This block gets a new label *)
   let label' = new_symbol label in
   let insts = Listbuf.empty in

   (* Push the continuation *)
   let asm_args = get_std_operands asm_args in
   let insts, cont_args, asm_args = push_pre_args insts asm_args in
   let cont_args = get_std_operands [ACRawInt (Rawint.Int32, false); ACRawInt (Rawint.Int32, false)] in

   (* Push all the remaining args *)
   let insts, size =
      if stdargsp then
         push_stdargs insts name orig_args asm_args
      else
         push_args insts name orig_args asm_args
   in

   (* Framepointer linkage *)
   let insts = Listbuf.add insts (MOV (IL, Register ebp, Register esp)) in

   (* Format the varargs *)
   let insts =
      if stdargsp then
         format_stdargs insts
      else
         insts
   in

   (* Call the function *)
   let insts = call_extern insts label' cont_args in

   (* Pop the args *)
   let insts = pop_args insts size in

   (* Pop the context *)
   let insts = pop_post_args insts cont_args in

   (* Call the continuation *)
   let insts = do_cont insts in

   (* Collect the instrunctions into a block *)
   let block, _ = code_build_block debug_empty label insts in
      block, label'

(*
 * Add stubs for all the imports.
 *)
let as_print_stubs buf info =
   let { bfd_import = import;
         bfd_export = export
       } = info
   in
   let sect = text_section buf in
   let buf = section_buf sect in

   (* Build all the stubs *)
   let blocks, import =
      SymbolTable.fold (fun (blocks, import) label info ->
            let { import_name = name;
                  import_info = args
                } = info
            in
               match args with
                  ImportGlobal ->
                     let import = SymbolTable.add import label info in
                        blocks, import
                | ImportFun (b, orig_args, asm_args) ->
                     let block, label' = function_stub label name b orig_args asm_args in
                     let blocks = block :: blocks in
                     let import = SymbolTable.add import label' info in
                        blocks, import) ([], SymbolTable.empty) import
   in
   let info = { info with bfd_import = import } in
      assem_blocks buf export blocks;
      info

(*
 * Generate only if enabled.
 *)
let assem_stubs buf info =
   if make_stub () then
      as_print_stubs buf info
   else
      info

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
