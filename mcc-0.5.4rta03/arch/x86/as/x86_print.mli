(*
 * Printing assembly code.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Trace

open Frame_type

open X86_inst_type
open X86_frame_type
open X86_frame

(*
 * String utilities.
 *)
val string_of_operand : operand -> string

(*
 * Print assembly blocks.
 *)
val pp_print_operand      : formatter -> operand -> unit
val pp_print_blocks       : formatter -> inst_block trace -> unit
val pp_print_code_blocks  : formatter -> code_block trace -> unit
val pp_print_live_blocks  : formatter -> live_block trace -> unit

(*
 * Print a program.
 *)
val pp_print_prog  : formatter -> prog -> unit
val debug_prog     : string -> prog -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
