(*
 * Define a BFD for this assembler.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Float80

open Symbol
open Trace

open Frame_type
open X86_frame_type
open X86_frame
open X86_inst_type

open Mir_arity_map

module Buf = Bfd_main.Buf
module Bfd = Bfd_main.Bfd

(************************************************************************
 * TYPES
 ************************************************************************)

(*
 * Version number.
 *)
let version = Hashtbl.hash "$Id;"

(*
 * Import and export info.
 *)
type 'a import =
   { import_name : 'a;
     import_info : import_info
   }

type 'a export =
   { export_name : 'a;
     export_glob : int option
   }

(*
 * Inherit some types.
 *)
type mprog = Fir.mprog

(*
 * BFD types.
 *)
type buf = Buf.t
type bfd_io = Bfd.bfd_io
type section = Bfd.raw_section

(*
 * Values in the BFD.
 *)
type reserve_info = label * reserve list
type fun_tag = label * label

type imports = string import SymbolTable.t
type exports = string export SymbolTable.t

type  bfd_prog =
   { bfd_version : int;
     bfd_bfd : bfd_io;
     bfd_fir : mprog;
     bfd_import : imports;
     bfd_export : exports;
     bfd_globals : global list;
     bfd_floats : float80 SymbolTable.t;
     bfd_migrate : migrate option;
     bfd_init : (Fir.file_class * var * var) list;
     bfd_reserve : reserve_info list;
     bfd_fun_tags : fun_tag list;
     bfd_arity_tags : (symbol * int32) ArityTable.t;
     bfd_debug : int32;

     (* Spill names *)
     bfd_context_vars     : symbol list;
     bfd_import_vars      : symbol list;
     bfd_export_vars      : symbol list;
     bfd_spill_int_normal : symbol list;
     bfd_spill_int_stdarg : symbol list;
     bfd_spill_float_normal : symbol list;
     bfd_spill_float_stdarg : symbol list;
     bfd_spill_global : symbol list;
   }

(*
 * More BFD types.
 *)
type bfd = bfd_prog Bfd.bfd
type val_section = bfd_prog Bfd.val_section

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
