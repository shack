(*
 * Linker.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mc_string_util
open Format
open Debug
open Symbol

open Fir_standardize

open Frame_type

open Mir_arity_map

open X86_inst_type
open X86_frame_type
open X86_frame

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_section
open X86_as_state

(************************************************************************
 * FILE LOADING
 ************************************************************************)

(*
 * Load the BFD from the specified position
 * in the file.
 *)
let load_bfd filename fd off =
   let _ = Unix.lseek fd off Unix.SEEK_SET in
   let inx = Unix.in_channel_of_descr fd in
   let (prog : bfd_prog) = Marshal.from_channel inx in
      if prog.bfd_version <> version then
         raise (Invalid_argument (sprintf "%s: invalid version number 0x%08x" filename prog.bfd_version));
         prog

(*
 * Open the ELF file, get the section offsets, and
 * load the program data.
 *)
let load_file filename =
   let offs = Bfd.elf_sec_filepos filename bfd_name in
   let fd = Unix.openfile filename [Unix.O_RDONLY] 0 in
      try
         let progs = Array.map (load_bfd filename fd) offs in
            Unix.close fd;
            progs
      with
         Unix.Unix_error (error, sfun, str) ->
            Unix.close fd;
            raise (Failure (sprintf "load_bfd: %s: %s" filename (Unix.error_message error)))

(*
 * Load all the programs from all the files.
 *)
let load_files filenames =
   let progs = List.map load_file filenames in
   let progs = Array.concat progs in
      Array.to_list progs

(************************************************************************
 * RENAMING
 ************************************************************************)

(*
 * Variable renaming environment.
 *)
type venv = symbol SymbolTable.t

let venv_empty = SymbolTable.empty

let venv_add = SymbolTable.add

let venv_find venv v =
   try SymbolTable.find venv v with
      Not_found ->
         raise (Invalid_argument ("X86_as_link: unbound var " ^ string_of_symbol v))

(*
 * Rename all the globals.
 *)
let venv_global venv (v, _) =
   venv_add venv v (new_symbol v)

let venv_globals venv globals =
   List.fold_left venv_global venv globals

(*
 * Rename all the function tags.
 *)
let venv_funtag venv (label, tag) =
   let venv = venv_add venv label (new_symbol label) in
      venv_add venv tag (new_symbol tag)

let venv_funtags venv funtags =
   List.fold_left venv_funtag venv funtags

(*
 * Standardize float labels.
 *)
let venv_float venv v _ =
   venv_add venv v (new_symbol v)

let venv_floats venv floats =
   SymbolTable.fold venv_float venv floats

(*
 * Standardize reserve labels.
 *)
let venv_reserve venv (v, _) =
   venv_add venv v (new_symbol v)

let venv_reserves venv reserves =
   List.fold_left venv_reserve venv reserves

(*
 * Add the spills.
 *)
let venv_vars venv vars1 vars2 =
   assert (List.length vars1 = List.length vars2);
   List.fold_left2 venv_add venv vars1 vars2

let venv_cat_spills venv count new_symbol spills =
   List.fold_left (fun (venv, i) v ->
         let v' = new_symbol i in
         let venv = venv_add venv v v' in
            venv, succ i) (venv, count) spills

let venv_spills venv new_symbol spills =
   let venv, _ = venv_cat_spills venv 0 new_symbol spills in
      venv

(*
 * Standardize the BFD individually.
 *)
let venv_prog gcount prog =
   let { bfd_globals = globals;
         bfd_reserve = reserve;
         bfd_fun_tags = funtags;
         bfd_floats = floats;

         bfd_context_vars = context_vars;
         bfd_import_vars = import_vars;
         bfd_export_vars = export_vars;
         bfd_spill_int_normal = spill_int_normal;
         bfd_spill_int_stdarg = spill_int_stdarg;
         bfd_spill_float_normal = spill_float_normal;
         bfd_spill_float_stdarg = spill_float_stdarg;
         bfd_spill_global = spill_global
       } = prog
   in
   let venv = venv_empty in
   let venv = venv_globals venv globals in
   let venv = venv_funtags venv funtags in
   let venv = venv_floats venv floats in
   let venv = venv_reserves venv reserve in
   let venv = venv_vars venv context_vars X86_as_prog.context_vars in
   let venv = venv_vars venv import_vars  X86_as_prog.import_vars in
   let venv = venv_vars venv export_vars  X86_as_prog.export_vars in
   let venv = venv_spills venv symbol_of_int_normal_spill spill_int_normal in
   let venv = venv_spills venv symbol_of_int_stdarg_spill spill_int_stdarg in
   let venv = venv_spills venv symbol_of_float_normal_spill spill_float_normal in
   let venv = venv_spills venv symbol_of_float_stdarg_spill spill_float_stdarg in
   let venv, gcount = venv_cat_spills venv gcount symbol_of_global_spill spill_global in
      gcount, venv

(*
 * Create the environments for each bfd.
 *)
let venv_progs progs =
   let gcount, progs =
      List.fold_left (fun (gcount, progs) prog ->
            let gcount, venv = venv_prog gcount prog in
            let progs = (gcount, venv, prog) :: progs in
               gcount, progs) (0, []) progs
   in
      if debug_as () then
         eprintf "Total: %d globals@." gcount;
      List.rev progs

(************************************************************************
 * LINKING
 ************************************************************************)

(*
 * Create the global inversion table.
 *)
let add_export venv export v info =
   let { export_name = s } = info in
      StringTable.filter_add export s (function
         Some _ ->
            raise (Invalid_argument ("Duplicate definition of symbol " ^ s))
       | None ->
            { info with export_name = venv_find venv v })

let build_export_prog venv exports prog =
   SymbolTable.fold (add_export venv) exports prog.bfd_export

let build_exports progs =
   List.fold_left (fun exports (_, venv, prog) ->
         build_export_prog venv exports prog) StringTable.empty progs

(*
 * Invert it.
 *)
let invert_exports exports =
   StringTable.fold (fun exports s info ->
         let { export_name = v } = info in
         let info = { info with export_name = s } in
            SymbolTable.add exports v info) SymbolTable.empty exports

(*
 * Resolve all the imports.
 *)
let venv_import_prog exports (imports, progs) (gcount, venv, prog) =
   let venv, imports =
      SymbolTable.fold (fun (venv, imports) v info ->
            let { import_name = s } = info in
               try
                  let v' =
                     match StringTable.find exports s with
                        { export_glob = Some index } ->
                           symbol_of_global_spill (index + gcount)
                      | { export_name = v' } ->
                           v'
                  in
                  let venv = venv_add venv v v' in
                     venv, imports
               with
                  Not_found ->
                     let imports = SymbolTable.add imports v info in
                        venv, imports) (venv, imports) prog.bfd_import
   in
   let progs = (venv, prog) :: progs in
      imports, progs

let venv_imports exports progs =
   let imports, progs =
      List.fold_left (venv_import_prog exports) (SymbolTable.empty, []) progs
   in
      imports, List.rev progs

(************************************************************************
 * SUBSTITUTION
 ************************************************************************)

(*
 * Apply substitution to the globals.
 *)
let subst_init_value venv initv =
   match initv with
      InitValGlobal (label, i) ->
         InitValGlobal (venv_find venv label, i)
    | InitValFunction (label, i, arity_tag) ->
         InitValFunction (venv_find venv label, i, venv_find venv arity_tag)
    | InitValInt32 _
    | InitValSingle _
    | InitValDouble _
    | InitValLongDouble _ ->
         initv

let subst_init venv init =
   match init with
      InitTagBlock (i, initv) ->
         InitTagBlock (i, List.map (subst_init_value venv) initv)
    | InitVar (label, i) ->
         InitVar (venv_find venv label, i)
    | InitRawData _
    | InitAggrBlock _
    | InitRawInt _
    | InitRawFloat _ ->
         init

let subst_global venv (v, init) =
   let v = venv_find venv v in
   let init = subst_init venv init in
      v, init

let subst_globals venv globals =
   List.map (subst_global venv) globals

(*
 * Float subst.
 *)
let subst_floats venv floats =
   SymbolTable.fold (fun floats v x ->
         let v = venv_find venv v in
            SymbolTable.add floats v x) SymbolTable.empty floats

(*
 * Initializers.
 *)
let subst_init venv (fclass, init_sym, main_sym) =
   let init_sym = venv_find venv init_sym in
   let main_sym = venv_find venv main_sym in
      fclass, init_sym, main_sym

let subst_inits venv inits =
   List.map (subst_init venv) inits

(*
 * Operand substitution.
 *)
let rec subst_offset venv off =
   match off with
      OffNumber _ ->
         off
    | OffLabel v ->
         OffLabel (venv_find venv v)
    | OffUMinus off ->
         OffUMinus (subst_offset venv off)
    | OffUNot off ->
         OffUNot (subst_offset venv off)
    | OffUAbs off ->
         OffUAbs (subst_offset venv off)
    | OffULog2 off ->
         OffULog2 (subst_offset venv off)
    | OffUPow2 off ->
         OffUPow2 (subst_offset venv off)
    | OffAdd (off1, off2) ->
         OffAdd (subst_offset venv off1, subst_offset venv off2)
    | OffSub (off1, off2) ->
         OffSub (subst_offset venv off1, subst_offset venv off2)
    | OffRel (off, label) ->
         OffRel (subst_offset venv off, venv_find venv label)
    | OffMul (off1, off2) ->
         OffMul (subst_offset venv off1, subst_offset venv off2)
    | OffDiv (off1, off2) ->
         OffDiv (subst_offset venv off1, subst_offset venv off2)
    | OffSal (off1, off2) ->
         OffSal (subst_offset venv off1, subst_offset venv off2)
    | OffSar (off1, off2) ->
         OffSar (subst_offset venv off1, subst_offset venv off2)
    | OffShr (off1, off2) ->
         OffShr (subst_offset venv off1, subst_offset venv off2)
    | OffAnd (off1, off2) ->
         OffAnd (subst_offset venv off1, subst_offset venv off2)
    | OffOr (off1, off2) ->
         OffOr (subst_offset venv off1, subst_offset venv off2)
    | OffXor (off1, off2) ->
         OffXor (subst_offset venv off1, subst_offset venv off2)

let subst_operand venv op =
   match op with
      ImmediateNumber off ->
         ImmediateNumber (subst_offset venv off)
    | ImmediateLabel label ->
         ImmediateLabel (venv_find venv label)
    | Register _
    | FloatRegister _
    | MMXRegister _
    | FPStack _
    | MemReg _ ->
         op
    | MemRegOff (reg, off) ->
         MemRegOff (reg, subst_offset venv off)
    | MemRegRegOffMul (r1, r2, off, mul) ->
         MemRegRegOffMul (r1, r2, subst_offset venv off, mul)
    | SpillRegister (r, off) ->
         SpillRegister (r, subst_offset venv off)

let subst_reserve venv op =
   match op with
      ResBase op ->
         ResBase (subst_operand venv op)
    | ResInfix (op1, op2) ->
         ResInfix (subst_operand venv op1, subst_operand venv op2)

(*
 * Reserves.
 *)
let subst_reserve_info venv (label, ops) =
   venv_find venv label, List.map (subst_reserve venv) ops

let subst_reserves venv reserves =
   List.map (subst_reserve_info venv) reserves

(*
 * Function tags.
 *)
let subst_funtag venv (label, tag) =
   let label = venv_find venv label in
   let tag = venv_find venv tag in
      label, tag

let subst_funtags venv funtags =
   List.map (subst_funtag venv) funtags

(*
 * Arity tags.
 *)
let subst_arity venv arity =
   ArityTable.map (fun (label, index) -> venv_find venv label, index) arity

(*
 * Subst symbol lists.
 *)
let subst_spill_list venv spills =
   List.map (venv_find venv) spills

(*
 * Apply the venv to the bfd.
 *)
let subst_bfd (venv, prog) =
   let { bfd_bfd = bfd;
         bfd_fir = fir;
         bfd_import = import;
         bfd_export = export;
         bfd_globals = globals;
         bfd_floats = floats;
         bfd_migrate = migrate;
         bfd_init = inits;
         bfd_reserve = reserves;
         bfd_fun_tags = funtags;
         bfd_arity_tags = arity;
         bfd_debug = debug;

         bfd_spill_int_normal = spill_int_normal;
         bfd_spill_int_stdarg = spill_int_stdarg;
         bfd_spill_float_normal = spill_float_normal;
         bfd_spill_float_stdarg = spill_float_stdarg;
         bfd_spill_global = spill_global;
       } = prog
   in
   let venv, bfd = standardize_bfd_io venv bfd in
      { bfd_version = version;

        bfd_bfd = bfd;
        bfd_fir = fir;
        bfd_import = SymbolTable.empty;
        bfd_export = SymbolTable.empty;
        bfd_globals = subst_globals venv globals;
        bfd_floats = subst_floats venv floats;
        bfd_migrate = migrate;
        bfd_init = subst_inits venv inits;
        bfd_reserve = subst_reserves venv reserves;
        bfd_fun_tags = subst_funtags venv funtags;
        bfd_arity_tags = subst_arity venv arity;
        bfd_debug = debug;

        bfd_context_vars       = X86_as_prog.context_vars;
        bfd_import_vars        = X86_as_prog.import_vars;
        bfd_export_vars        = X86_as_prog.export_vars;

        bfd_spill_int_normal   = subst_spill_list venv spill_int_normal;
        bfd_spill_int_stdarg   = subst_spill_list venv spill_int_stdarg;
        bfd_spill_float_normal = subst_spill_list venv spill_float_normal;
        bfd_spill_float_stdarg = subst_spill_list venv spill_float_stdarg;
        bfd_spill_global       = subst_spill_list venv spill_global;
      }

let subst_progs progs =
   List.map subst_bfd progs

(************************************************************************
 * LINKING
 ************************************************************************)

(*
 * Link the FIR parts.
 *)
let link_firs progs =
   let firs =
      List.map (fun prog ->
            Fir_standardize.standardize_mprog prog.bfd_fir) progs
   in
      Fir_link.link_mprog firs

(*
 * Link the BFD parts.
 *)
let link_bfds progs =
   let bfds =
      List.map (fun prog ->
            prog.bfd_bfd) progs
   in
      link_bfd_io bfds

(*
 * Add the globals.
 *)
let cat_globals globals1 globals2 =
   globals1 @ globals2

let cat_floats floats1 floats2 =
   SymbolTable.fold SymbolTable.add floats1 floats2

let cat_inits inits1 inits2 =
   inits1 @ inits2

let cat_reserves reserves1 reserves2 =
   reserves1 @ reserves2

let cat_funtags funtags1 funtags2 =
   funtags1 @ funtags2

let cat_spills spills1 spills2 =
   let len1 = List.length spills1 in
   let len2 = List.length spills2 in
      if len1 < len2 then
         spills2
      else
         spills1

let cat_global_spills spills1 spills2 =
   spills1 @ spills2

(*
 * Concatenate all the fields.
 *)
let link_progs bfd fir exports imports progs =
   let cat bfd1 bfd2 =
      { bfd1 with bfd_globals  = cat_globals  bfd1.bfd_globals  bfd2.bfd_globals;
                  bfd_floats   = cat_floats   bfd1.bfd_floats   bfd2.bfd_floats;
                  bfd_init     = cat_inits    bfd1.bfd_init     bfd2.bfd_init;
                  bfd_reserve  = cat_reserves bfd1.bfd_reserve  bfd2.bfd_reserve;
                  bfd_fun_tags = cat_funtags  bfd1.bfd_fun_tags bfd2.bfd_fun_tags;

                  bfd_spill_int_normal   = cat_spills bfd1.bfd_spill_int_normal   bfd2.bfd_spill_int_normal;
                  bfd_spill_int_stdarg   = cat_spills bfd1.bfd_spill_int_stdarg   bfd2.bfd_spill_int_stdarg;
                  bfd_spill_float_normal = cat_spills bfd1.bfd_spill_float_normal bfd2.bfd_spill_float_normal;
                  bfd_spill_float_stdarg = cat_spills bfd1.bfd_spill_float_stdarg bfd2.bfd_spill_float_stdarg;
                  bfd_spill_global       = cat_global_spills bfd1.bfd_spill_global bfd2.bfd_spill_global
      }
   in
      match progs with
         bfd1 :: bfds ->
            let bfd1 =
               { bfd1 with bfd_bfd = bfd;
                           bfd_fir = fir;
                           bfd_import = imports;
                           bfd_export = exports;
                           bfd_migrate = None
               }
            in
               List.fold_left cat bfd1 bfds
       | [] ->
            raise (Invalid_argument "X86_as_link: empty link list")

(*
 * Link multiple ELF files.
 *)
let assem_link infiles =
   let progs = load_files infiles in
   let progs = venv_progs progs in
   let exports = build_exports progs in
   let imports, progs = venv_imports exports progs in
   let progs = subst_progs progs in
   let exports = invert_exports exports in

   let bfd = link_bfds progs in
   let fir = link_firs progs in
      link_progs bfd fir exports imports progs

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
