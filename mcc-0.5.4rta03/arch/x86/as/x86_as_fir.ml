(*
 * Save the fir to the file.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir
open Fir_set
open Fir_marshal

open X86_frame
open Sizeof_const
open Sizeof_type

open Bfd

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_bfd.Buf
open X86_as_section
open X86_as_util

(*
 * Print the binary FIR data.
 *)
let as_print_binary_fir_data buf fir =
   (* Construct the FIR data blocks *)
   let data = Buffer.create 1024 in
   let () = BufferMarshal.marshal_prog data fir in
   let data = Buffer.contents data in

   (* Output the resultant string data *)
   let count = String.length data in
      bfd_print_int32 buf (Int32.of_int count);
      bfd_print_data buf data

(*
 * Find all the reserves in all the blocks.
 *)
let assem_fir bfd info =
   let { bfd_fir = fir } = info in
   let sect = data_section bfd in
   let buf = section_buf sect in
      (* Identify the data *)
      bfd_align buf 16;
      bfd_print_ascii buf "FIR data       ";

      (* Print the binary FIR data *)
      bfd_align buf 4;
      bfd_print_label buf SymGlobal bin_fir_label;
      as_print_binary_fir_data buf fir;

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
