(*
 * Section changing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Bfd
open X86_as_bfd
open X86_as_bfd.Bfd

(*
 * Default sections.
 *)
let text_sym = Symbol.add ".text"
let text_flags = [SectionAlloc; SectionExecutable; SectionReloc]
let text_section bfd =
   find_raw_section bfd text_sym text_flags 0x90  (* Fill with 0x90=NOP *)

(*
 * Our data is not writable.
 *)
let data_sym = Symbol.add ".data"
let data_flags = [SectionAlloc; SectionReloc]
let data_section bfd =
   find_raw_section bfd data_sym data_flags 0x00

(*
 * Section containing undefined symbols.
 *)
let undefined_flags = [SectionBuiltin]
let undefined_section bfd =
   find_raw_section bfd undefined_sym undefined_flags 0x0

(*
 * Section containing absolute symbols.
 *)
let absolute_flags = [SectionBuiltin]
let absolute_section bfd =
   find_raw_section bfd absolute_sym absolute_flags 0x0

(*
 * BFD section includes the BFD
 * itself.
 *)
let bfd_name = ".mc.bfd"

let bfd_sym = Symbol.add bfd_name

let bfd_section bfd =
   find_val_section bfd bfd_sym

let set_bfd_section bfd info =
   let bfd_io = bfd_io_of_bfd bfd in
   let info = { info with bfd_bfd = bfd_io } in
      set_val_section bfd bfd_sym info

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
