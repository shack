(*
 * Assemble globals data.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open X86_runtime
open X86_frame_type
open X86_frame

open Bfd

open X86_as_bfd
open X86_as_bfd.Bfd
open X86_as_bfd.Buf
open X86_as_util
open X86_as_section
open X86_as_state

(************************************************************************
 * GLOBAL INITIALIZATION DATA
 ************************************************************************)

(*
 * Global raw data.
 *)
let as_print_glob_raw_data buf pre s =
   let len = Array.length s in
   let print, total_len =
      match pre with
         Rawint.Int8 -> bfd_print_int8, len
       | Rawint.Int16 -> bfd_print_int16, len * 2
       | Rawint.Int32
       | Rawint.Int64 ->
            raise (Invalid_argument "bogus string precision")
   in
      bfd_print_int32 buf (Int32.of_int (total_len + 2));
      for i = 0 to pred len do
         print buf s.(i)
      done;
      bfd_print_int16 buf 0

(*
 * Print an initialization value.
 *)
let init_val_int32_code       = Int32.of_int init_val_int32
let init_val_single_code      = Int32.of_int init_val_single
let init_val_double_code      = Int32.of_int init_val_double
let init_val_long_double_code = Int32.of_int init_val_long_double
let init_val_function_code    = Int32.of_int init_val_function
let init_val_global_code      = Int32.of_int init_val_global

let as_print_init_value buf init =
   bfd_align buf 4;
   match init with
      InitValInt32 i ->
         bfd_print_int32 buf init_val_int32_code;
         bfd_print_int32 buf i
    | InitValSingle x ->
         bfd_print_int32 buf init_val_single_code;
         bfd_print_float32 buf x
    | InitValDouble x ->
         bfd_print_int32 buf init_val_double_code;
         bfd_print_float64 buf x
    | InitValLongDouble x ->
         bfd_print_int32 buf init_val_long_double_code;
         bfd_print_float80 buf x
    | InitValFunction (_, i, arity_tag) ->
         (* TEMP IGNORING ARITY TAG *)
         bfd_print_int32 buf init_val_function_code;
         bfd_print_int32 buf i
    | InitValGlobal (_, v) ->
         bfd_print_int32 buf init_val_global_code;
         as_print_abs32 buf v

(*
 * Print a ML block.
 *)
let as_print_glob_tag_block buf tag vals =
   bfd_print_int32 buf (Int32.of_int (List.length vals));
   bfd_print_int32 buf (Int32.of_int tag);
   List.iter (as_print_init_value buf) vals

(*
 * Print a rawint.
 *)
let glob_int8_index  = Int32.of_int 1
let glob_int16_index = Int32.of_int 2
let glob_int32_index = Int32.of_int 4
let glob_int64_index = Int32.of_int 8

let as_print_glob_rawint buf i =
   match Rawint.precision i with
      Rawint.Int8 ->
         bfd_print_int32 buf glob_int8_index;
         bfd_print_int8 buf (Rawint.to_int i)
    | Rawint.Int16 ->
         bfd_print_int32 buf glob_int16_index;
         bfd_print_int16 buf (Rawint.to_int i)
    | Rawint.Int32 ->
         bfd_print_int32 buf glob_int32_index;
         bfd_print_int32 buf (Rawint.to_int32 i)
    | Rawint.Int64 ->
         bfd_print_int32 buf glob_int64_index;
         bfd_print_int64 buf (Rawint.to_int64 i)

(*
 * Print a rawfloat.
 *)
let glob_float32_index = Int32.of_int 4
let glob_float64_index = Int32.of_int 8
let glob_float80_index = Int32.of_int 10

let as_print_glob_rawfloat buf x =
   match Rawfloat.precision x with
      Rawfloat.Single ->
         bfd_print_int32 buf glob_float32_index;
         bfd_print_float32 buf (Rawfloat.to_float80 x)
    | Rawfloat.Double ->
         bfd_print_int32 buf glob_float64_index;
         bfd_print_float64 buf (Rawfloat.to_float80 x)
    | Rawfloat.LongDouble ->
         bfd_print_int32 buf glob_float80_index;
         bfd_print_float80 buf (Rawfloat.to_float80 x)

(*
 * Add global initializers.
 *)
let as_print_glob_data buf (v, exp) =
   let () =
      bfd_align buf 4;
      bfd_print_label buf SymLocal v;
      match exp with
         InitRawData (pre, s) ->
            as_print_glob_raw_data buf pre s
       | InitTagBlock (tag, vals) ->
            as_print_glob_tag_block buf tag vals
       | InitRawInt i ->
            as_print_glob_rawint buf i
       | InitRawFloat x ->
            as_print_glob_rawfloat buf x
       | InitVar _
       | InitAggrBlock _ ->
            ()
   in
      (v, v, exp)

(*
 * Place the initializers in the data segment.
 *)
let assem_glob_data bfd globals =
   let data = data_section bfd in
   let buf = section_buf data in
      bfd_align buf 16;
      bfd_print_ascii buf "Global init    ";
      List.map (as_print_glob_data buf) globals

(************************************************************************
 * GLOBAL TABLE
 ************************************************************************)

(*
 * Add a table entry.
 *)
let rawdata_global_code    = Int32.of_int rawdata_global
let tagblock_global_code   = Int32.of_int tagblock_global
let aggrblock_global_code  = Int32.of_int aggrblock_global
let rawint_global_code     = Int32.of_int rawint_global
let rawfloat_global_code   = Int32.of_int rawfloat_global
let var_global_code        = Int32.of_int var_global

let as_print_glob_table_entry buf i (v, init_name, exp) =
   let spill_name = symbol_of_global_spill i in
   let () =
      match exp with
         InitRawData _ ->
            bfd_print_int32 buf rawdata_global_code;
            as_print_abs32 buf init_name;
            as_print_abs32 buf spill_name
       | InitTagBlock _ ->
            bfd_print_int32 buf tagblock_global_code;
            as_print_abs32 buf init_name;
            as_print_abs32 buf spill_name
       | InitAggrBlock i ->
            bfd_print_int32 buf aggrblock_global_code;
            bfd_print_int32 buf (Rawint.to_int32 i);
            as_print_abs32 buf spill_name
       | InitRawInt _ ->
            bfd_print_int32 buf rawint_global_code;
            as_print_abs32 buf init_name;
            as_print_abs32 buf spill_name
       | InitRawFloat _ ->
            bfd_print_int32 buf rawfloat_global_code;
            as_print_abs32 buf init_name;
            as_print_abs32 buf spill_name
       | InitVar (_, v) ->
            bfd_print_int32 buf var_global_code;
            as_print_abs32 buf v;
            as_print_abs32 buf spill_name
   in
      succ i

(*
 * Add the entries to the glob table.
 *)
let assem_glob_table bfd globals =
   let sect = data_section bfd in
   let buf = section_buf sect in
      bfd_align buf 16;
      bfd_print_ascii buf "Global table   ";

      bfd_align buf 4;
      bfd_print_label buf SymGlobal globals_base_label;
      ignore (List.fold_left (as_print_glob_table_entry buf) 0 globals);
      bfd_print_label buf SymGlobal globals_limit_label

(************************************************************************
 * FLOATS
 ************************************************************************)

(*
 * Print the float constants.
 *)
let assem_float_data bfd floats =
   let sect = data_section bfd in
   let buf = section_buf sect in
      bfd_align buf 16;
      bfd_print_ascii buf "Float values   ";
      SymbolTable.iter (fun v x ->
            bfd_print_label buf SymLocal v;
            bfd_print_float80 buf x) floats

(************************************************************************
 * MAIN FUNCTION
 ************************************************************************)

(*
 * Format the global initializers.
 * We have three passes.
 *    1. First, define all the initialization data
 *    2. Next, add the initializer array
 *    3. Finally, add the global labels to the global spill section
 *)
let assem_globals bfd info =
   if debug_as () then
      eprintf "*** AS: global@.";
   let { bfd_globals = globals;
         bfd_floats = floats
       } = info
   in
   let globals = assem_glob_data bfd globals in
      assem_float_data bfd floats;
      assem_glob_table bfd globals

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
