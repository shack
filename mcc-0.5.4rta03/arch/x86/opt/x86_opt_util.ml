(*
   Utilities for optimizations
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Flags

open Frame_type
open X86_inst_type
open X86_frame_type
open X86_frame
open X86_util


(***  Registers and Operands  ***)


(* regs_in_operand
   Identify registers _used_ in an operand.  This for some reason
   appears to not be consistent with src_regs; at any rate, we need
   this computation for AGI.  *)
let regs_in_operand = function
   ImmediateNumber _
 | ImmediateLabel _
 | FPStack _ ->
      []
 | Register r
 | FloatRegister r
 | MMXRegister r
 | MemReg r
 | MemRegOff (r, _) ->
      [r]
 | MemRegRegOffMul (r1, r2, _, _) ->
      [r1; r2]
 | SpillRegister _ ->
      [ebp]


(* reg_in_operand
   Returns true if the register named appears in the operand *)
let reg_in_operand op r =
   List.mem r (regs_in_operand op)


(* is_fpstack
   Returns true if the operand indicated is an FPStack register *)
let is_fpstack = function
   FPStack _ -> true
 | _ -> false


(***  Liveness  ***)


(* make_comment_string
   Builds a comment that is placed immediately (before, after) the
   instruction given (this is to ensure live-out and depth are ok.  *)
let make_comment_string inst s =
   { live_src   = [];
     live_dst   = [];
     live_out   = inst.live_out;
     live_class = CodeNormal;
     live_depth = inst.live_depth;
     live_inst  = CommentString s;
   }


(* make_comment_inst
   Similar to above, but build a CommentInst instead.  *)
let make_comment_inst inst s =
   { live_src   = [];
     live_dst   = [];
     live_out   = inst.live_out;
     live_class = CodeNormal;
     live_depth = inst.live_depth;
     live_inst  = CommentInst (s, inst.live_inst);
   }


let () = std_flags_help_section_text "opt.x86"
   "Optimizations specific to the X86 backend."
