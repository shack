(*
   Instruction scheduling
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* TEMP
   NEED A PUSH LIMIT!!
   DOING REALLY STUPID THINGS ON MANDEL.ML
   PUSHING A BLOCK OF 40 INSTRUCTIONS DOWN!!
 *)


open Symbol
open Flags

open Fir_state
open Frame_type
open X86_inst_type
open X86_frame_type
open X86_opt_util
open X86_sched_util
open X86_frame
open X86_arch


module Live = Live.MakeLive (X86_backend.Frame)


(***  Constants  ***)


(* max_push_count
   Maximum number of instructions we may cross before we must stop
   pushing down a block of instructions.  Used in AGI and in PRE. *)
let max_push_count = 4


(***  Instruction Pairing  ***)


(* find_inst
   Returned by the find_*_inst family of instructions;  FindVInst
   is returned when we have an instruction that may be placed to
   run in the V pipe, immediately AFTER the instruction that is
   under consideration.  FindUInst is returned when we have inst
   which may be run in the U pipe, with the current instruction
   run in the V pipe; for FindUInst we place the instruction that
   is returned PRIOR to the instruction under consideration.  *)
type find_inst =
   FindVInst of inst live
 | FindUInst of inst live
 | FindNone


(* find_v_inst
   Try to find an instruction following inst1 that may be paired
   such that inst1 runs in the U pipe and inst2 runs in the V
   pipe.  If returned, inst2 should be placed AFTER inst1.  *)
let find_v_inst inst1 insts =
   let env = pd_empty in
   let rec find env = function
      [] ->
         FindNone, []
    | inst2 :: insts ->
         if pd_inst_can_cross env inst2 && are_insts_pairable inst1 inst2 then
            (* These instructions can be paired successfully *)
            FindVInst inst2, insts
         else
            (* These instructions simply cannot be paired *)
            let env = pd_push_down_inst env inst2 in
            let found, insts = find env insts in
               found, inst2 :: insts
   in
      find env insts


(* find_u_inst
   Try to find an instruction following inst1 that may be paired
   such that inst1 runs in the V pipe and inst2 runs in the U
   pipe; if returned, inst2 should be placed PRIOR to inst1.  *)
let find_u_inst inst1 insts =
   let env = pd_push_down_inst pd_empty inst1 in
   let rec find env = function
      [] ->
         FindNone, []
    | inst2 :: insts ->
         if pd_inst_can_cross env inst2 && are_insts_pairable inst2 inst1 then
            (* These instructions can be paired successfully *)
            FindUInst inst2, insts
         else
            (* These instructions simply cannot be paired *)
            let env = pd_push_down_inst env inst2 in
            let found, insts = find env insts in
               found, inst2 :: insts
   in
      find env insts


(* find_uv_inst
   Find either a U- or V-pipe instruction to pair with inst1.  *)
let find_uv_inst inst1 insts =
   match find_v_inst inst1 insts with
      FindVInst inst, insts ->
         FindVInst inst, insts
    | _, _ ->
         match find_u_inst inst1 insts with
            FindUInst inst, insts ->
               FindUInst inst, insts
          | _, _ ->
               FindNone, insts


(* find_nonpair_inst
   Find a nonpairable instruction, e.g. to force the next
   instructions to be paired; useful for example at entry
   into a new block.  *)
let find_nonpair_inst insts =
   let env = pd_empty in
   let inst_nonpair inst =
      not (trivial_inst inst.live_inst || pairu inst.live_inst || pairv inst.live_inst)
   in
   let rec find env = function
      [] ->
         None, []
    | inst :: insts ->
         if inst_nonpair inst && pd_inst_can_cross env inst then
            (* Inst can be brought to the top of the block *)
            Some inst, insts
         else
            (* This instruction cannot be brought to the top *)
            let env = pd_push_down_inst env inst in
            let found, insts = find env insts in
               found, inst :: insts
   in
      find env insts


(* pairable_insts
   Try to pair instructions in a block.  *)
let rec pairable_insts insts =
   match insts with
      [] ->
         []
    | inst1 :: inst2 :: insts when are_insts_pairable inst1 inst2 ->
         (* This isn't an optimization, these were already ok *)
         inst1 :: inst2 :: pairable_insts insts
    | inst1 :: insts ->
         (* Try to find a match *)
         match find_uv_inst inst1 insts with
            FindVInst inst2, insts ->
               (* We run in U pipe, inst2 runs in V pipe *)
               let comm = make_comment_string inst1 "OPT:  PAIRV pairing next 2 instructions" in
                  comm :: inst1 :: inst2 :: pairable_insts insts
          | FindUInst inst2, insts ->
               (* We run in V pipe, inst2 runs in U pipe *)
               let comm = make_comment_string inst2 "OPT:  PAIRU pairing next 2 instructions" in
                  comm :: inst2 :: inst1 :: pairable_insts insts
          | FindNone, _ ->
               inst1 :: pairable_insts insts


(* pairable_insts
   Same as above, but try to begin the block with a nonpairable
   instruction to force pairing to work out the way we want.  *)
let pairable_insts insts =
   match find_nonpair_inst insts with
      Some inst, insts ->
         (* Move the nonpairable instruction to top *)
         let comm = make_comment_string inst "OPT:  NONPAIR moving nonpairable inst to top" in
            comm :: inst :: pairable_insts insts
    | None, _ ->
         (* No nonpairable instructions can be placed at top *)
         pairable_insts insts

let pairable_insts insts =
   if optimize "opt.x86.schedule.pair" then
      pairable_insts insts
   else
      insts


(***  Avoiding Partial Register Stalls  ***)


(* TEMP
   need a limited form that will only push down past X
   instructions at a time; this will be useful for AGI
   and probably is overkill in other situations as well,
   not to mention wreaks havoc on the time complexity  *)
(* push_insts_using_reg_down
   Try to push instructions which make use of any register
   named in env _down_.  See the description of pd_env in
   x86_sched_util.ml for a better explanation of the proc. *)
let push_insts_using_reg_down env insts =
   let place_push_insts push =
      match push with
         inst :: insts ->
            let comm1 = make_comment_string inst "OPT:  PUSHED instructions begin here" in
            let comm2 = make_comment_string inst "OPT:  End of PUSHED instructions" in
               comm1 :: List.rev (comm2 :: push)
       | [] ->
            []
   in
   let rec push_down env count push insts =
      if count <= 0 then
         (* Count limit: All instructions being pushed must end here *)
         place_push_insts push @ insts
      else
         match insts with
            [] ->
               (* End block: All instructions being pushed must end here *)
               place_push_insts push
          | inst :: insts ->
               if pd_inst_can_cross env inst then
                  (* This instruction doesn't mess with anything in env *)
                  inst :: push_down env (pred count) push insts
               else
                  (* A register in env is read or modified here; move down!
                     This does not alter count since we do not cross any
                     instruction here. *)
                  let env = pd_push_down_inst env inst in
                  let push = inst :: push in
                     push_down env count push insts
      in
      push_down env max_push_count [] insts


(* partial_register_stalls
   Try to remove partial register stalls in the code.  These
   are very expensive and should be avoided whenever possible. *)
let rec partial_register_stalls insts =
   match insts with
      [] ->
         []
    | inst :: insts ->
         let insts = partial_register_stalls insts in
            match inst.live_inst with
               SET (_, Register r)
             | MOV (IB, Register r, _)
             | MOV (IW, Register r, _) ->
               (* TEMP: other instructions can now have small regs as a dest! *)
                  (* Destination is a small register *)
                  let env = pd_add_dst pd_empty inst in
                  let insts = push_insts_using_reg_down env insts in
                  let comm = make_comment_string inst "OPT:  PRS, partial register stall" in
                     comm :: inst :: insts
             | _ ->
                  (* Destination is a normal register *)
                  inst :: insts

let partial_register_stalls insts =
   if optimize "opt.x86.schedule.prs" then
      partial_register_stalls insts
   else
      insts


(***  Address Generation Interlock resolution  ***)


(* address_generation_interlocks
   Attempt to spread out potential AGI so they will not occur. *)
let rec address_generation_interlocks insts =
   match insts with
      []
    | [_] ->
         insts
    | inst1 :: inst2 :: insts ->
         if agi_interlock_occurs inst1 inst2 then
            (* AGI can occur; try to move inst2 DOWN *)
            let env = pd_add_dst pd_empty inst1 in
            let insts = push_insts_using_reg_down env (inst2 :: insts) in
            let comm = make_comment_string inst1 "OPT:  avoiding AGI resolution" in
               comm :: inst1 :: address_generation_interlocks insts
         else
            (* No AGI suspected; continue *)
            inst1 :: address_generation_interlocks (inst2 :: insts)

let address_generation_interlocks insts =
   if optimize "opt.x86.schedule.agi" then
      address_generation_interlocks insts
   else
      insts


(***  Main Program  ***)


(* uses_defs_insts
   Document the uses, defs, and live out of every (non-trivial)
   instruction.  Used mostly for debugging purposes...  *)
let rec uses_defs_insts insts =
   match insts with
      inst :: insts when trivial_inst inst.live_inst ->
         inst :: uses_defs_insts insts
    | inst :: insts ->
         let comm_src = List.fold_left (fun s v -> s ^ " " ^ (string_of_symbol v)) "src:" inst.live_src in
         let comm_dst = List.fold_left (fun s v -> s ^ " " ^ (string_of_symbol v)) "dst:" inst.live_dst in
         let comm_live = SymbolSet.fold (fun s v -> s ^ " " ^ (string_of_symbol v)) "live:" inst.live_out in
            make_comment_string inst comm_src :: inst ::
            make_comment_string inst comm_dst ::
            make_comment_string inst comm_live :: uses_defs_insts insts
    | [] ->
         []

let uses_defs_insts insts =
   if std_flags_get_bool "opt.x86.schedule.uses_defs" then
      uses_defs_insts insts
   else
      insts


(* schedule_insts
   Schedule a block of instructions.  *)
let partial_register_stalls insts =
   if std_flags_get_bool "opt.x86.schedule.prs" then
      partial_register_stalls insts
   else
      insts

let address_generation_interlocks insts =
   if std_flags_get_bool "opt.x86.schedule.agi" then
      address_generation_interlocks insts
   else
      insts

let pairable_insts insts =
   if std_flags_get_bool "opt.x86.schedule.pair" then
      pairable_insts insts
   else
      insts

let schedule_insts insts =
   let insts = partial_register_stalls insts in
   let insts = address_generation_interlocks insts in
   let insts = pairable_insts insts in
      insts


(* schedule_prog
   Schedule instructions in an entire program.  *)
let schedule_prog prog =
   let blocks = prog.asm_blocks in
   let live_info = Live.create_live_benv [] blocks in
   let schedule_block block =
      if block.block_opt then
         let block = Live.create_live_block live_info 0 block.block_label in
         let block = insts_in_block schedule_insts block in
         let block = insts_in_block uses_defs_insts block in
         let insts = block.block_code in
         let insts = List.map (fun inst -> inst.live_inst) insts in
         let insts = List.filter (fun inst -> inst <> NOP) insts in
            { block with block_code = insts }
      else
         block
   in
   let blocks = Trace.map schedule_block blocks in
      { prog with asm_blocks = blocks }

let schedule_prog = Fir_state.profile "X86_schedule.schedule_prog" schedule_prog

let schedule_prog prog =
   if optimize_asm_level "opt.x86.schedule" 2 then
      schedule_prog prog
   else
      prog


(* Register optimization names *)
let () = std_flags_help_section_text "opt.x86.schedule"
   "Flags that apply to X86 instruction scheduling."

let () = std_flags_register_list_help "opt.x86.schedule"
  ["opt.x86.schedule",              FlagBool true,
                                    "Enable X86 instruction scheduling.  This is run at the very" ^
                                    " end of assembly code processing.  It is generally recommended.";
   "opt.x86.schedule.uses_defs",    FlagBool false,
                                    "If enabled, the uses and defs for each instruction, as well as" ^
                                    " live-out information, is printed in assembly dumps before each" ^
                                    " instruction.";
   "opt.x86.schedule.prs",          FlagBool true,
                                    "Enable elimination of partial register stalls on the Intel platform" ^
                                    " (when an 8-bit register is set and the corresponding 16/32-bit register" ^
                                    " is immediately read from).";
   "opt.x86.schedule.pair",         FlagBool true,
                                    "Enable standard instruction pairing based on Intel guidelines.";
   "opt.x86.schedule.agi",          FlagBool true,
                                    "Enable elimination of address generation interlocks (when an addressing" ^
                                    " operand refers to registers initialised in immediately prior instructions" ^
                                    " or on a parallel pipe)."]
