(*
   Run peephole optimizations on the x86 code.
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Debug
open Flags
open Symbol
open Fir_state
open Frame_type
open X86_build
open X86_inst_type
open X86_frame_type
open X86_opt_util
open X86_frame


module Live = Live.MakeLive (X86_backend.Frame)


(* peephole_insts
   Run peephole on a block of instructions.  *)
let rec peephole_insts nreg ph =
   match ph.Moogle_lib.rd_insts with
      inst :: insts ->
         let build_try optname f fallthrough nreg ph =
            if optimize_asm_level optname 1 then
               f fallthrough nreg ph
            else
               fallthrough nreg ph
         in
         let try_default =
            X86_peephole_basic.rule_build_default
         in
         let try_dead =
            build_try "opt.x86.peephole.dead"  X86_peephole_dead.process_rules  try_default
         in
         let try_memop =
            build_try "opt.x86.peephole.memop" X86_peephole_memop.process_rules try_dead
         in
         let try_fp =
            build_try "opt.x86.peephole.fp"    X86_peephole_fp.process_rules    try_memop
         in
         let try_mul =
            build_try "opt.x86.peephole.mul"   X86_peephole_mul.process_rules   try_fp
         in
         let try_ml =
            build_try "opt.x86.peephole.ml"    X86_peephole_ml.process_rules    try_mul
         in
         let try_basic =
            build_try "opt.x86.peephole.basic" X86_peephole_basic.process_rules try_ml
         in
         let ph = peephole_insts nreg (try_basic nreg ph) in
            ph
    | [] ->
         ph

let peephole_insts buf nreg insts =
   let ph = X86_peephole_basic.rule_create buf insts in
   let ph = peephole_insts nreg ph in
      ph.Moogle_lib.rd_buf


(* peephole
   Optimize an entire program.  *)
let peephole nreg prog =
   let prog = X86_comment.filter_comments prog in
   let blocks = prog.asm_blocks in
   let live_info = Live.create_live_benv [] blocks in
   let peephole_block block =
      if block.block_opt then
         let block = Live.create_live_block live_info 0 block.block_label in
         let insts = block.block_code in
         let buf = Listbuf.empty in
         let buf = peephole_insts buf nreg insts in
         let insts = Listbuf.to_list buf in
         let insts = List.filter (fun i -> i <> NOP) insts in
            { block with block_code = insts }
      else
         block
   in
   let blocks = Trace.map peephole_block blocks in
      { prog with asm_blocks = blocks }


let peephole count nreg prog =
   let rec loop count prog =
      if count > 0 then
         loop (pred count) (peephole nreg prog)
      else
         prog
   in
      loop count prog

let peephole count nreg = profile "X86_peephole.peephole" (peephole count nreg)

let peephole nreg prog =
   if optimize_asm_level "opt.x86.peephole" 1 then
      let () =
         if debug Fir_state.debug_print_asm then
            X86_print.debug_prog "*** X86_peephole: before peephole" prog
      in
      let prog = peephole (std_flags_get_int "opt.x86.peephole.count") nreg prog in
      let () =
         if debug Fir_state.debug_print_asm then
            X86_print.debug_prog "*** X86_peephole: after peephole" prog
      in
         prog
   else
      prog

let () = std_flags_help_section_text "opt.x86.peephole"
  ("X86 peephole optimizations.  These are pattern rewrite rules that are applied" ^
   " to small sections of assembly instructions, used to perform strength reduction" ^
   " across multiple instructions.")

let () = std_flags_register_list_help "opt.x86.peephole"
  ["opt.x86.peephole",        FlagBool true,
                              "Enable X86 peephole optimizations.  If enabled, peephole" ^
                              " will be run both before and after register allocation.";
   "opt.x86.peephole.basic",  FlagBool true,
                              "Enable basic peephole patterns.";
   "opt.x86.peephole.ml",     FlagBool true,
                              "Enable peephole patterns that tend to occur in ML programs.";
   "opt.x86.peephole.mul",    FlagBool true,
                              "Enable constant multiply/divide peephole patterns.";
   "opt.x86.peephole.fp",     FlagBool true,
                              "Enable X86 floating-point peephole patterns.";
   "opt.x86.peephole.memop",  FlagBool true,
                              "Enable advanced patterns for memory operations.";
   "opt.x86.peephole.dead",   FlagBool true,
                              "Enable dead instruction elimination.";
   "opt.x86.peephole.count",  FlagInt 8,
                              "Number of times to repeat peephole optimization on the assembly" ^
                              " code each time it is invoked.  Currently there is no fixpoint" ^
                              " computation or heuristic for peephole optimizations; they are" ^
                              " run a constant number of times."]
