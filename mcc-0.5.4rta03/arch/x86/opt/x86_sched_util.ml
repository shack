(*
   Instruction scheduling utilities
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Frame_type
open X86_inst_type
open X86_frame_type
open X86_opt_util
open X86_fpstack
open X86_frame
open X86_util
open X86_arch


(***  Push down environment  ***)


(* pd_env
   Captures the state of the instructions which are being pushed down
   (NOT the instructions which are moved up!).  This environment is
   used to determine whether a new instruction at the bottom can be
   placed above the pushed instructions.  This environment contains:
      pd_regs     : registers which conflict.  If an inst at the
                    bottom uses any of these registers, OR defines
                    a register in this set WHICH IS LIVE OUT, then
                    it cannot cross the push instructions.
      pd_mem_dst  : true if the push stack modifies memory.  Insts
                    which use OR set memory cannot cross.
      pd_mem_src  : true if the push stack uses memory.  Insts which
                    SET memory cannot cross.
      pd_fpstack  : true if the push stack uses or modifies the FP
                    stack.  Insts which use OR modify the stack may
                    not cross.
 *)
(* TEMP:
   Make the memory handling scheme more intelligent, i.e. ok to
   cross if we cannot access/revise the same memory locations.  *)
(* TEMP:
   Make the handling of FPstack more intelligent.  The current
   ``never-cross'' scheme severely weakens FP scheduling.  *)
type pd_env =
   { pd_regs      : unit SymbolTable.t;
     pd_mem_dst   : bool;
     pd_mem_src   : bool;
     pd_fpstack   : bool;
   }


(* pd_empty
   Constructs an initial PD environment.  *)
let pd_empty =
   { pd_regs      = SymbolTable.empty;
     pd_mem_dst   = false;
     pd_mem_src   = false;
     pd_fpstack   = false;
   }


(* pd_inst_can_cross
   Returns true if the specified instruction can safely be placed
   above the instructions being pushed down.  If FALSE, then the
   instruction should be appended to the push-down list.  *)
let pd_inst_can_cross env inst =
   let pd_live_src_conflict =
      List.exists (fun v -> SymbolTable.mem env.pd_regs v) inst.live_src
   in
   let pd_live_dst_conflict =
      List.exists (fun v -> SymbolTable.mem env.pd_regs v) inst.live_dst
   in
   let pd_live_conflict = pd_live_src_conflict || pd_live_dst_conflict in
   let pd_mem_dst_conflict =
      let dst, src = memory_inst inst.live_inst in
         env.pd_mem_dst && (dst <> [] || src <> [])
   in
   let pd_mem_src_conflict =
      let dst, src = memory_inst inst.live_inst in
         env.pd_mem_src && dst <> []
   in
   let pd_mem_conflict = pd_mem_dst_conflict || pd_mem_src_conflict in
   let pd_fpstack_conflict =
      env.pd_fpstack && inst_uses_fpstack inst.live_inst
   in
   let conflict = pd_live_conflict || pd_mem_conflict || pd_fpstack_conflict in
      not conflict


(* pd_add_dst
   Adds the destination registers of inst to the environment.  In this
   context inst is an instruction which will NOT be pushed down.  We
   use this for PRS for example, to indicate that instructions using
   these registers should be pushed down if possible.  *)
let pd_add_dst env inst =
   let regs = env.pd_regs in
   let regs = List.fold_left (fun regs v -> SymbolTable.add regs v ()) regs inst.live_dst in
      { env with pd_regs = regs }


(* pd_push_down_inst
   Called whenever an instruction is appended to the list of instructions
   being pushed down.  This adds the effects that inst will have to the
   environment so they may be taken into account when we try to cross with
   other instructions.  *)
let pd_push_down_inst env inst =
   let { pd_regs     = regs;
         pd_mem_dst  = mem_dst;
         pd_mem_src  = mem_src;
         pd_fpstack  = fpstack;
       } = env in
   let regs = List.fold_left (fun regs v -> SymbolTable.add regs v ()) regs inst.live_src in
   let regs = List.fold_left (fun regs v -> SymbolTable.add regs v ()) regs inst.live_dst in
   let dst, src = memory_inst inst.live_inst in
   let mem_dst = mem_dst || dst <> [] in
   let mem_src = mem_src || src <> [] in
   let fpstack = fpstack || inst_uses_fpstack inst.live_inst in
      { pd_regs      = regs;
        pd_mem_dst   = mem_dst;
        pd_mem_src   = mem_src;
        pd_fpstack   = fpstack;
      }


(***  Instruction Pairing  ***)


(* TEMP
   does not take into account instruction length restrictions in the
   V pipe; V pipe can only hold an inst of 7 bytes in len or LESS. *)
(* are_insts_pairable
   Returns true if the indicated instructions are pairable; that is,
   inst1 may run in U, inst2 may run in V, they have no output dep.
   conflicts, and they do not have register conflicts.  This does not
   take into account other factors, such as whether the instructions
   may actually be reordered to accomodate pairing.  *)
let are_insts_pairable inst1 inst2 =
   if List.exists trivial_inst [inst1.live_inst; inst2.live_inst] then
      (* Trivial instructions cannot pair by nature *)
      false
   else
      (* Determine which pipe each instruction can run in *)
      let u_ok = pairu inst1.live_inst in
      let v_ok = pairv inst2.live_inst in
      (* Check if we have any register flow or output dependencies *)
      let flow_dep = List.exists (fun v -> List.mem v inst1.live_dst) inst2.live_src in
      let output_dep = List.exists (fun v -> List.mem v inst1.live_dst) inst2.live_dst in
         u_ok && v_ok && not (flow_dep || output_dep)


(***  Address Generation Interlock  ***)


(* agi_interlock_occurs
   Returns true if an address generation interlock occurs or
   is suspected to occur if inst1 is followed by inst2.  *)
let agi_interlock_occurs inst1 inst2 =
   let dst = inst1.live_dst in
   let mem_dst, mem_src = memory_inst inst2.live_inst in
   let mem_regs = List.flatten (List.map regs_in_operand (mem_dst @ mem_src)) in
   let conflict = List.exists (fun v -> List.mem v dst) mem_regs in
      conflict


(***  Higher Order Functions  ***)


(* insts_in_block
   Runs f on the list of instructions in a block.  This handles
   boundary instructions; i.e. if a boundary instruction (e.g. JCC)
   is present in the instruction list, the list will be broken into
   two parts and f is run on each part separately so it does not
   have to worry about boundaries (the boundary instruction is never
   given to f directly).  *)
let insts_in_block f block =
   let insts = block.block_code in
   let rec apply insts =
      match insts with
         inst :: insts ->
            if boundary_inst inst.live_inst then
               let insts, tail = apply insts in
                  [], inst :: (f insts) @ tail
            else
               let insts, tail = apply insts in
                  inst :: insts, tail
       | [] ->
            [], []
   in
   let insts, tail = apply insts in
   let insts = (f insts) @ tail in
      { block with block_code = insts }


