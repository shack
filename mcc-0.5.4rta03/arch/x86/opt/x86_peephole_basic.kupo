(*
   Basic peephole optimizations for X86
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open X86_arch


(*** Basic peephole optimizations ***)


input "Removing NOP":
   nop;
output:


(* Always remove move to self *)
input "Removing MOV to self":
   mov   _, op, op;
output:


(* Several simple optimizations to try to remove intermediary
   registers, which will hopefully allow dead instruction
   elimination to remove some of these redundant instructions. *)
name "Removing MOV intermediary"
input:
   movl  %reg,    src !reg;   (* Make sure one of src, dst is *)
   movl  %dst,    %reg;       (* not a memory operand; hence 2 *)
output:
   copy  1;
   movl  dst,     src;

(* For the multiline version, src has to be a register, otherwise
   we cannot verify that it is left unaltered during the interleaved
   instructions... *)
name "Removing MOV intermediary (multiline)"
input:
   movl  %reg,    %src !reg;
   *     :  preserves [reg; src];
   movl  dst,     %reg;
input:
   movl  %reg,    $src;       (* Moving constant to memory? *)
   *     :  preserves reg;
   movl  dst,     %reg;
output:
   copy  1, 2;
   movl  dst,     src;


(* The MOVB, MOVW require a register that can be broken
   into subregisters, i.e. EAX-EDX but not ESI, EDI. *)
input:
   movl  %reg,    src !reg small;
   movb  %dst,    %reg;
input:
   movl  %reg,    %src !reg small;
   movb  dst,     %reg;
output:
   copy  1;
   movb  dst,     src;

input:
   movl  %reg,    src !reg;
   movw  %dst,    %reg;
input:
   movl  %reg,    %src !reg;
   movw  dst,     %reg;
output:
   copy  1;
   movw  dst,     src;


(* Several optimizations to clean up sloppy backend programming
   with respect to MemRegRegOffMul; let's try to merge bitshifts
   and offset calculations directly into the operand, shall we? *)
name "Cleaning MemRegRegOffMul"
input:
   movl  %reg,    $value;
   *     :  preserves reg;
   movl  reg',    *(ptr, reg, off, 1);
output:
   copy  1, 2;
   movl  reg',    *(ptr, off + value);

(* The next one merges a shift operation into the multiplier. *)
input:
   shll  %reg,    $value;
   *     :  ignores reg;
   movl  %reg',   *(ptr, reg, off, 1)   :  endscope reg;
when value & 31 <= 3
output:
   copy  2;
   movl  reg',    *(ptr, reg, off, pow2 value);

(* The next one removes a register alias which may be a redundant
   operation, and certainly creates a wasted flow dependency. *)
input "Remove register alias in MemRegRegOffMul":
   movl  %reg,    %src;
   *     :  preserves [reg; src];
   movl  %reg',   *(ptr, reg, off, mul);
output:
   copy  1, 2;
   movl  reg',    *(ptr, src, off, mul);

(* Reduce movz instructions when possible; they are expensive. *)
name "Rewriting MOVZ instruction"
input:
   movzb %dst small, src !dst;
output:
   xorl  dst,     dst;
   movb  dst,     src;

input:
   movzw %dst,    src !dst;
output:
   xorl  dst,     dst;
   movw  dst,     src;


(* Seen in mandel.ml to store a value to memory.  These seem
   to show up most often in long AllocTuple blocks... *)
name "Merging LEA with MemRegOff"
input:
   leal  %reg,    *(src, off1);
   movl  *(reg, off2), op2 !reg;
   movl  *(reg, off3), op3 !reg;
   movl  *(reg, off4), op4 !reg;
   movl  *(reg, off5), op5 !reg;
   movl  *(reg, off6), op6 !reg;
   movl  *(reg, off7), op7 !reg;
output:
   movl  *(src, off1 + off2), op2;
   movl  *(src, off1 + off3), op3;
   movl  *(src, off1 + off4), op4;
   movl  *(src, off1 + off5), op5;
   movl  *(src, off1 + off6), op6;
   movl  *(src, off1 + off7), op7;
   copy  1;

input:
   leal  %reg,    *(src, off1);
   movl  *(reg, off2), op2 !reg;
   movl  *(reg, off3), op3 !reg;
   movl  *(reg, off4), op4 !reg;
   movl  *(reg, off5), op5 !reg;
   movl  *(reg, off6), op6 !reg;
output:
   movl  *(src, off1 + off2), op2;
   movl  *(src, off1 + off3), op3;
   movl  *(src, off1 + off4), op4;
   movl  *(src, off1 + off5), op5;
   movl  *(src, off1 + off6), op6;
   copy  1;

input:
   leal  %reg,    *(src, off1);
   movl  *(reg, off2), op2 !reg;
   movl  *(reg, off3), op3 !reg;
   movl  *(reg, off4), op4 !reg;
   movl  *(reg, off5), op5 !reg;
output:
   movl  *(src, off1 + off2), op2;
   movl  *(src, off1 + off3), op3;
   movl  *(src, off1 + off4), op4;
   movl  *(src, off1 + off5), op5;
   copy  1;

input:
   leal  %reg,    *(src, off1);
   movl  *(reg, off2), op2 !reg;
   movl  *(reg, off3), op3 !reg;
   movl  *(reg, off4), op4 !reg;
output:
   movl  *(src, off1 + off2), op2;
   movl  *(src, off1 + off3), op3;
   movl  *(src, off1 + off4), op4;
   copy  1;

input:
   leal  %reg,    *(src, off1);
   movl  *(reg, off2), op2 !reg;
   movl  *(reg, off3), op3 !reg;
output:
   movl  *(src, off1 + off2), op2;
   movl  *(src, off1 + off3), op3;
   copy  1;

(* This is the simple base case of the above *)
input:
   leal  %reg,    *(src, off1);
   movl  *(reg, off2), op2 !reg;
output:
   movl  *(src, off1 + off2), op2;
   copy  1;

(* The next few cases are a bit conservative, but they also
   do a good job of eliminating redundant LEA's in ML code.
   They also usually instigate the above cases. *)
input "Merging distant LEA with MemRegOff":
   leal  %reg,    *(src, off1);
   *     :  ignores reg;
   movl  *(reg, off2), op !reg;
output:
   movl  reg,     src;
   copy  2;
   movl  *(reg, off1 + off2), op;
   leal  reg,     *(reg, off1);


(* LEA is discouraged for MemRegOff since it can cause an AGI.
   This should appear AFTER the above optimizations since it
   may interfere with propagation of above through AllocTuple *)
(* TEMP BUG
   Cannot handle in current Moogle/Kupo system due to the
   way offsets are handled, sorry...
input "Rewriting LEA as ADD":
   leal  %reg,    *(reg, off);
output:
   addl  reg,     off;
 *)


(* Seen in mandel.ml, this is a senseless redundant MOV
   operation which should be reduced when possible. *)
input "Removing redundant MOV through memory":
   movl  *mem,    %src;
   movl  %dst,    *mem;
output:
   copy  1;
   movl  dst,     src;


(* Also seen in mandel.ml... more memory operations
   which are redundant...  *)
input "Removing copy-to-self through memory":
   movl  %reg,    *(ptr, off);
   *     :  preserves [memory; reg; ptr];
   movl  *(ptr, off), %reg;
output:
   copy  1, 2;


(***  Basic Arithmetic Optimizations  ***)


(* Negation *)
input "Removing NEG of immediate":
   movl  dst,     $value;
   negl  dst      :  dead iflags;
output:
   movl  dst,     $-value;


(* Addition of constants, or by a constant *)
name "Optimizing ADD"
input:
   movl  dst,     $value1;
   addl  dst,     $value2  :  dead iflags;
output:
   movl  dst,     $value1 + value2;

input:
   addl  dst,     $value   :  dead iflags;
output when value = 0:
output when value = 1:
   incl  dst;
output when value = -1:
   decl  dst;


(* Subtraction of constants, or by a constant *)
name "Optimizing SUB"
input:
   movl  dst,     $value1;
   subl  dst,     $value2  :  dead iflags;
output:
   movl  dst,     $value1 - value2;

input:
   movl  %dst,    $0;
   subl  %dst,    src !dst;
output:
   movl  dst,     src;
   negl  dst;

input:
   subl  dst,     $value   :  dead iflags;
output when value = 0:
output when value = 1:
   decl  dst;
output when value = -1:
   incl  dst;


(* Note that a lot of inc/dec/add/sub streams are specific to ML,
   and therefore have been moved to the x86_peephole_ml.kupo file. *)


(* AMD recommends using a single POP instead of ADD $4, %esp.
   The former is a smaller instruction, thus more efficient. *)
input "ESP adjust optimization":
   addl  %esp,    $4;
output:
   new   %temp;
   popl  temp;


(***  Bit Operations  ***)


(* Remove trivial bitoperations; generally bit operations
   by a constant when the constant is either zero or -1. *)
name "Optimizing trivial bitops"
input:
   andl  dst,     $value   :  dead iflags;
output when value = -1:
   (* No output; inert instruction *)
output when value = 0:
   movl  dst,     $0;

input:
   orl   dst,     $value   :  dead iflags;
output when value = 0:
   (* No output; inert instruction *)
output when value = -1:
   movl  dst,     $-1;

input:
   xorl  dst,     $value   :  dead iflags;
output when value = 0:
   (* No output; inert instruction *)
output when value = -1:
   negl  dst;


(* Rewrite SAL as SHL *)
input "Rewriting SAL as SHL":
   sall  dst,     src;
output:
   shll  dst,     src;


(* Optimize those shift operations!  Shifts cannot run
   in the V pipe so if we can find an alternate form that
   is about the same latency, let's do it!  *)
name "Optimizing shift-by-zero operations"
input:
   shll  _,       $0       :  dead iflags;
input:
   shrl  _,       $0       :  dead iflags;
input:
   sarl  _,       $0       :  dead iflags;
input:
   shldl _, _,    $0       :  dead iflags;
input:
   shrdl _, _,    $0       :  dead iflags;
output:


(* Get rid of the shift if possible. *)
name "Optimizing shift left by 1"
input:
   shll  %dst,    $1;
output:
   addl  dst,     dst;


(* A number of shift-optimizations are ML specific, and
   therefore have been moved over to x86_peephole_ml.kupo. *)


(* Optimizations suited for header manipulations *)
(* TEMP:
   Verify that this optimization is active! *)
name "[EXP HEADER] Header shift/mask optimization"
input:
   andl  %reg,    $mask;
   *                       :  ignores reg;
   shrl  %reg,    $shift   :  dead iflags;
when 0xffffffff << shift = mask
input:
   shrl  %reg,    $shift;
   *                       :  ignores reg;
   andl  %reg,    $mask    :  dead iflags;
when 0xffffffff >> shift = mask
output:
   copy  2;
   shrl  reg,     shift;

(* Some header manipulations may ``mask'' by shifting R/L *)
name "[EXP HEADER] Shift-right/shift-left to mask value"
input:
   shrl  %reg,    $shift;
   *                       :  ignores reg;
   shll  %reg,    $shift   :  dead iflags;
output:
   copy  2;
   andl  reg,     $(0xffffffff << shift);

input:
   shll  %reg,    $shift;
   *                       :  ignores reg;
   shrl  %reg,    $shift   :  dead iflags;
output:
   copy  2;
   andl  reg,     $(0xffffffff >> shift);


(***  Conditional Expressions  ***)


(* Try to remove destinations and false output dependencies
   when possible; the backend generates code like this at
   times when doing other optimizations. *)
name "Rewriting destructive op as conditional"
input:
   subl  %dst,    src      :  dead dst;
output:
   cmpl  dst,     src;

input:
   andl  %dst,    src      :  dead dst;
output:
   testl dst,     src;


(* The next few occur a _lot_, presumably as a result of some sanity
   check in the Mir.  The secret here is that the AND instruction
   sets the ZF if its result is zero; therefore the following CMP
   instruction is redundant.  This optimization will only work
   after adjacent blocks have been merged together. *)
name "cond: Performing AND flags optimization"
input:
   andl  dst,     $1;
   cmpl  dst,     $1;
   _     :  cond_any_equality;
output:
   copy  1;
   cond_invert 3;


(* Comparison against zero is inefficient; use TEST instead *)
name "cond: Rewriting CMP against zero using equality"
input:
   cmpl  %dst,    $0;
   *     :  preserves iflags;
   _     :  cond_any_equality;
output:
   testl dst,     dst;
   copy  2, 3;


(* Special case for the SETcc instruction to set booleans *)
input:
   set   op,      %dst;
   decl  %dst;
   andl  %dst,    $-2;
   addl  %dst,    $3       :  dead iflags;
output:
   copy  1;
   shll  dst,     $1;
   orl   dst,     $1;


(* Part of the ``I can't believe I actually saw this code in
   the backend'' series of comparative optimizations, mostly
   from the NAML frontend... *)
name "cond: Identifying compare-against-constant code"
input:
   xorl  %reg,    %reg;
   *     :  preserves reg;
   cmpl  %reg,    %src;
   _     :  cond_any;
output:
   copy  1, 2;
   cmpl  src,     $0;
   cond_reverse 4;

input:
   movl  %reg,    $value;
   *     :  preserves reg;
   cmpl  %reg,    %src;
   _     :  cond_any;
output:
   copy  1, 2;
   cmpl  src,     $value;
   cond_reverse 4;


(* This next one is not an optimization, in and of itself,
   however it opens the door for optimizations that require
   equality.  Seen in mandel.ml... *)
name "Rewriting CMP to use equality"
input:
   cmpl  _,       $0;
   jcc   ULE,     label;
output:
   copy  1;
   jcc   EQ,      label;

input:
   cmpl  src,     $1;
   jcc   ULT,     label;
output:
   cmpl  src,     $0;
   jcc   EQ,      label;

input:
   cmpl  src,     $1;
   jcc   UGE,     label;
output:
   cmpl  src,     $0;
   jcc   NEQ,     label;

input:
   cmpl  _,       $0;
   jcc   UGT,     label;
output:
   copy  1;
   jcc   NEQ,     label;


(* Yes, I actually saw this code. *)
input "Removing unconditional CMP":
   cmpl  _,       $0;
   jcc   UGE,     label    :  endscope iflags;
output:
   jmp   label;

input "Removing unconditional CMP":
   cmpl  _,       $0;
   jcc   ULT,     _        :  endscope iflags;
output:


(* Some removal of redundant comparisons in certain cases *)
name "cond: Removing redundant compare"
input:
   andl  %reg,    _;
   testl %reg,    %reg;
   _     :  cond_any_equality;
input:
   incl  %reg;
   testl %reg,    %reg;
   _     :  cond_any_equality;
output:
   copy  1, 3;


(* The next one was seen in FC; I don't know what is causing the
   set based on result of a set, but obviously we can optimize
   this expression significantly. *)
input "Removing SETcc based on result of another SET":
   set   _,       %reg1;
   movl  %dst,    %reg1;
   xorl  %reg2,   %reg2 !dst;
   testl %dst,    %dst;
   set   EQ,      %reg2;
output:
   copy  1, 2;
   negl  reg1;
   incl  reg1;
   movl  reg2,    reg1;

(* Next few work because SET does not modify flags.  We can
   remove the redundant TEST on the result, because of this.  *)
name "Removing redundant TEST after SET"
input:
   set   NEQ,     %reg1;
   movl  %reg2,   %reg1;
   testl %reg2,   %reg2;
   _     :  cond_any_equality;
output:
   copy  1, 2, 4;

input:
   set   NEQ,     %reg1;
   testl %reg1,   %reg1;
   _     :  cond_any_equality;
output:
   copy  1, 3;

input:
   set   op,      %reg1;
   movl  %reg2,   %reg1;
   testl %reg2,   %reg2;
   jcc   NEQ,     label;
output:
   copy  1, 2;
   jcc   op,      label;

input:
   set   op,      %reg1;
   testl %reg1,   %reg1;
   jcc   NEQ,     label;
output:
   copy  1;
   jcc   op,      label;


(* The next few optimizations deal with removing
   redundant set operations (usually sets on the
   binary result of a previous set). *)
name "Redundant SET elimination"
input:
   xorl  %reg1,   %reg1;
   cmpl  dst !reg1, src !reg1;
   set   GT,      %reg1;
   movl  %reg2 !reg1, %reg1;
   xorl  %reg1,   %reg1;
   cmpl  dst !reg2, src !reg2;
   set   LT,      %reg1;
output:
   copy  1, 2, 3, 4, 7;


(* Part of the ``I actually saw this'' series of optimizations.
   This one tests a bit right after masking out the damn bit! *)
name "Unconditional jump due to masking"
input:
   andl  %reg,    $0xfffffffe;
   *     :  preserves reg;
   testl %reg,    $1;
   jcc   EQ,      label;
output:
   copy  1, 2;
   jmp   label;

input:
   andl  %reg,    $0xfffffffe;
   *     :  preserves reg;
   testl %reg,    $1;
   jcc   NEQ,     label;
output:
   copy  1, 2;


(***  Removal of Redundant Registers  ***)


(* Attempting to remove cases of a potentially redundant
   register.  This was first seen in mandel.ml; in this
   case, dst could be eliminated.  This requires DCE to
   fully optimize. *)
name "Redundant register elimination"
input:
   movl  dst,     %reg;
   cmpl  dst,     $value;
output:
   copy  1;
   cmpl  reg,     value;

(* Also in mandel.c; removes redundant registers. *)
input:
   movl  %dst1,   %reg;
   imull %dst1,   src      :  endscope reg;
   movl  dst2,    %dst1    :  endscope dst1;
output:
   copy  1;
   imull reg,     src;
   movl  dst2,    reg;

(* Also seen in mandel.c... *)
input:
   movl  %reg,    src !reg;
   imull dst,     src;
input:
   (* Using pointer here to prevent a feedback loop *)
   movl  *src,    %reg;
   imull dst,     *src;
output:
   copy  1;
   imull dst,     reg;

(* Again, mandel.c... *)
input:
   movl  dst,     %reg;
   movl  %reg,    dst;
output:
   copy  1;

(* Memory optimization, again involving a register *)
input:
   movl  %reg,    *src;
   imull %dst !reg, %reg      :  endscope reg;
output:
   imull dst,     src;

(* Seen in mandel.c,  %reg might be redundant. *)
input:
   movl  %reg,    %src1;
   cmpl  %reg,    %src2;
output:
   copy  1;
   cmpl  src1,    src2;



(***  EXPERIMENTAL Optimizations  ***)


