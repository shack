(*
 * This is the definition of general data structures
 * for the X86 backend.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace

open Fir_env
open Fir_exn

open Frame_type

open Mir_arity_map

open X86_inst_type


(*
 * Standard definition of vars.
 *)
type var = Symbol.symbol
type label = Symbol.symbol
type asm_string = var * string


(*
 * Inherit some types from the FIR.
 *)
type int_signed = Fir.int_signed
type int_precision = Fir.int_precision
type float_precision = Fir.float_precision
type tydef = Fir.tydef


(*
 * Register are grouped into classes.
 * For example, we may have a set of
 * int registers, and a set of floating
 * point registers, and the two sets
 * are distinct.
 *)
type reg = var
type reg_class = int


(*
 * A var_class pairs the var with
 * the class (for example, int/float)
 * that it belongs to.
 *)
type var_class = reg * reg_class


(*
 * A basic block.
 * The type of instructions is as-yet undefined.
 *   block_debug     Debugging information
 *   block_label     (local) label for this block
 *   block_code      Instructions for this block
 *   block_jumps     Local labels this block jumps to
 *   block_index     Optional index for this block. Should only
 *                   be set for escape functions.
 *   block_align     Set to true if block must be aligned
 *   block_start     Set to true if this block is the beginning
 *                   of an escape function.
 *   block_arity     Optional arity tag for this block. Should
 *                   only be set for escape functions.
 *   block_opt       True if peephole optimizations and scheduling
 *                   should be run on this block.  The compiler
 *                   generates a number of standard blocks (such
 *                   as for exception handler invokation) which
 *                   are already mostly optimized and/or not in a
 *                   common execution path; it is not efficient to
 *                   attempt to optimize such blocks.
 *)
type ('debug, 'inst) poly_block =
   { block_debug  : 'debug;
     block_label  : label;
     block_code   : 'inst list;
     block_jumps  : label list;
     block_index  : label option;
     block_align  : bool;
     block_start  : bool;
     block_arity  : label option;
     block_opt    : bool;
   }


(*
 * Type of assembly blocks.
 *)
type inst_block = (debug_info, inst) poly_block
type code_block = (debug_info, inst code) poly_block
type live_block = (debug_info, inst live) poly_block


(*
 * Spill counts.
 *)
type spill_counts =
   { spill_int_normal_index : int;
     spill_int_stdarg_index : int;
     spill_float_normal_index : int;
     spill_float_stdarg_index : int;
     spill_mmx_normal_index : int;
     spill_mmx_stdarg_index : int;
     spill_global_index : int;
   }


(*
 * Argument union type
 *)
type 'a arg =
   ArgInt32 of 'a                    (* single 32-bit register *)
 | ArgInt64 of 'a * 'a               (* pair of 32-bit registers *)
 | ArgFloat of 'a * float_precision  (* single 80-bit register *)


(*
 * Globals.  Each global is either a string, or an uninitialized data block,
 * or possibly an initialized ML data block.  In the latter case, we use the
 * init_val type to id the initialization values.
 *
 * InitValInt32 (i):
 *    Standard 32-bit integer
 *
 * InitValSingle (f)
 * InitValDouble (f)
 * InitValLongDouble (f):
 *    Floating-point values of various precisions.
 *
 * InitValFunction (name, index, arity):
 *    Function with given name; closure index; and arity tag.
 *
 * InitValGlobal (label, name):
 *    Label and name of a global spill variable.
 *)
type init_val =
   InitValInt32      of int32
 | InitValSingle     of Float80.float80
 | InitValDouble     of Float80.float80
 | InitValLongDouble of Float80.float80
 | InitValFunction   of label * int32 * label
 | InitValGlobal     of label * var

type init_exp =
   InitRawData of int_precision * int array
 | InitAggrBlock of Rawint.rawint
 | InitTagBlock of int * init_val list
 | InitRawInt of Rawint.rawint
 | InitRawFloat of Rawfloat.rawfloat
 | InitVar of label * var

type global = var * init_exp


(*
 * External interface.
 *    asm_file     : name of the file that was compiled
 *    asm_import   : Import table for file
 *    asm_export   : Export table for file
 *    asm_presreve : Set of labels for blocks that must be kept
 *    asm_types    : type definitions (for symbol table and migration)
 *    asm_spills   : Information on spilled registers
 *    asm_closures : names if escaping functions
 *    asm_globals  : definitions of global vars
 *    asm_floats   : floating-point constants
 *    asm_blocks   : the basic blocks that define the program
 *    asm_bin_fir  : binary representation of the FIR
 *    asm_spset    : spilled registers
 *    asm_migrate  : migration data (None = process didn't migrate)
 *    asm_init     : names of initialization blocks
 *    asm_arity    : list of arity tags (for escaping functions)
 *    asm_debug    : debugging flags for program (see CORE_DEBUG_* flags)
 *)
type 'spset poly_prog =
   { asm_file     : file_info;
     asm_import   : import SymbolTable.t;
     asm_export   : export SymbolTable.t;
     asm_preserve : SymbolSet.t;
     asm_types    : tydef SymbolTable.t;
     asm_spills   : spill_counts;
     asm_closures : var list;
     asm_globals  : global list;
     asm_floats   : Float80.float80 SymbolTable.t;
     asm_blocks   : inst_block trace;
     asm_bin_fir  : Fir.mprog;
     asm_spset    : 'spset;
     asm_migrate  : migrate option;
     asm_init     : (Fir.file_class * var * var) list;
     asm_arity    : (symbol * int32) ArityTable.t;
     asm_debug    : int32;
   }
