(*
 * X86 instruction set.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Location


(*
 * Precision qualifiers.  For most instructions, this determines
 * the precision of both the source and destination operands; for
 * a few instructions (such as sign/zero extend instructions), one
 * operand is assumed to be a fixed precision.
 *)
type iprec =
   IB    (* 8-bit integer *)
 | IW    (* 16-bit integer *)
 | IL    (* 32-bit integer *)

type fprec =
   FS    (* Single - 32bit *)
 | FL    (* Double - 64bit *)
 | FT    (* Temporary floating point - 80bit *)


(*
 * Operand basics.
 *)
type reg = Symbol.symbol
type var = Symbol.symbol
type label = Symbol.symbol


(*
 * Offsets.  Used for operands that allow simple arithmetic operations
 * (e.g. adding an offset to a label).  Any labels must be valid labels
 * in the emitted assembly code.
 *)
type offset =
   (* Constants *)
   OffNumber of int32         (* Numbers *)
 | OffLabel of label          (* Label in the code *)

   (* Unary operations *)
 | OffUMinus of offset        (* Arithmetic negation *)
 | OffUNot   of offset        (* Bitwise negation *)
 | OffUAbs   of offset        (* Absolute value *)
 | OffULog2  of offset        (* Logarithm, base 2 *)
 | OffUPow2  of offset        (* Exponentiation, 2^x *)

   (* Binary operations *)
 | OffAdd of offset * offset
 | OffSub of offset * offset
 | OffRel of offset * label   (* Offset relative to a label *)
 | OffMul of offset * offset
 | OffDiv of offset * offset
 | OffSal of offset * offset
 | OffSar of offset * offset
 | OffShr of offset * offset
 | OffAnd of offset * offset
 | OffOr  of offset * offset
 | OffXor of offset * offset


(*
 * Operand types.
 *)
type operand =
   ImmediateNumber of offset
 | ImmediateLabel of label
 | Register of reg

   (*
    * Memory register containing a float -- this is a pseudo-op
    * which should probably be eliminated by time of emission.
    * FP registers should always be spilled.
    *)
 | FloatRegister of reg

   (*
    * Floating point stack: 0 = top of FP stack, 1 = next down...
    *)
 | FPStack of int32

   (*
    * MMX register with indicated name.
    *)
 | MMXRegister of reg

   (*
    * Memory operations.  For MemRegRegOffMul, the operands are
    * rbase, roff, ioff, imul; the resulting address will be
    * rbase + (roff * imul) + ioff; note that imul must be 1, 2,
    * 4, or 8.
    *)
 | MemReg of reg
 | MemRegOff of reg * offset
 | MemRegRegOffMul of reg * reg * offset * int32

   (*
    * Treat spills specially, the register is just a comment
    * used to indicate what the original name of the value was.
    *)
 | SpillRegister of reg * offset


(*
 * Condition codes for (integer) conditional operations.
 *)
type cc =
   EQ
 | NEQ
 | LT    (* Signed comparisons *)
 | LE
 | GT
 | GE
 | ULT   (* Unsigned comparisons *)
 | ULE
 | UGT
 | UGE


(*
 * Abstract assembly code operands.
 *)
type dst = operand
type src = operand


(*
 * Reservations are either base pointers or infix pointers.
 * These are operands because they may refer to Registers,
 * SpillRegisters, or MemRegOff forms (into the context).
 *
 * For the infix pointers, the first operand is the base
 * pointer, and the second operand is the infix pointer
 * (which is a real pointer).
 *)
type reserve =
   ResBase of operand
 | ResInfix of operand * operand

(*
 * Info for debugging.
 *)
type debug_line = loc
type debug_vars = (var * Fir.ty * operand) list
type debug_info = debug_line * debug_vars


(*
 * Assembly instructions.
 *)
type inst =
   (* Copy *)
   MOV   of iprec * dst * src    (* integer value copy *)
   (*
    * WARNING: For MOVS/MOVB, when iprec == IB, src must NOT
    * be a 32-bit-only register (esi, edi, esp, or ebp).
    *)
 | MOVS  of iprec * dst * src    (* sign extend - dst always has IL precision *)
 | MOVZ  of iprec * dst * src    (* zero extend - dst always has IL precision *)

   (* String copy *)
 | RMOVS of iprec                (* Copy %ecx words of iprec from source %esi, to dest %edi *)

   (* Arithmetic operations *)
 | NOT  of iprec * dst           (* 1's complement negation -- bitwise *)
 | NEG  of iprec * dst           (* 2's complement negation -- arithmetic *)
 | INC  of iprec * dst           (* increment value *)
 | DEC  of iprec * dst           (* decrement value *)
 | LEA  of iprec * dst * src     (* load effective address from src *)
 | ADD  of iprec * dst * src     (* add two operands *)
 | SUB  of iprec * dst * src     (* subtract two operands *)
 | ADC  of iprec * dst * src     (* add with carry from CF *)
 | SBB  of iprec * dst * src     (* subtract with borrow in CF *)
 | MUL  of iprec * src           (* unsigned mul %edx:%eax <- %eax * src *)
 | IMUL of iprec * dst * src     (* signed mul, dst <- dst * src *)
 | DIV  of iprec * src           (* unsigned div, %eax <- %edx:%eax / src, %edx = remainder *)
 | IDIV of iprec * src           (* signed div, %eax <- %edx:%eax / src, %edx = remainder *)
 | CLTD                          (* sign-extend %eax to %edx:%eax *)
 | AND  of iprec * dst * src     (* bitwise and *)
 | OR   of iprec * dst * src     (* bitwise or *)
 | XOR  of iprec * dst * src     (* bitwise xor *)
   (*
    * Note: for shift operations, the shift count must either be an
    * immediate value or must be the register %ecx.  Caution:  the
    * shift count is taken modulo 32; the high 3 bits of the shift
    * count are IGNORED.
    *)
 | SAL  of iprec * dst * src     (* arithmetic shift-left (same as SHL) *)
 | SAR  of iprec * dst * src     (* arithmetic shift-right (MSB preserved) *)
 | SHL  of iprec * dst * src     (* logical shift-left (same as SAL) *)
 | SHR  of iprec * dst * src     (* logical shift-right (high bits become zero) *)
 | SHLD of iprec * dst * src * src  (* dest, source bits, shift count *)
 | SHRD of iprec * dst * src * src  (* dest, source bits, shift count *)

   (* MMX operations *)
 | MOVD  of dst * src      (* integer value copy *)
 | MOVQ  of dst * src      (* integer value copy *)

   (* Floating-point operations *)
 | FINIT                   (* initialize FPU state *)
 | FSTCW   of dst          (* Save control word *)
 | FLDCW   of src          (* Load control word *)
 | FLD     of fprec * src  (* load float to stack *)
 | FILD    of src          (* load int32 to stack *)
 | FILD64  of src          (* load int64 to stack *)
   (*
    * WARNING: Intel does NOT support the FT precision for FST!
    * (the FT precision is supported for FSTP, however).
    *)
 | FST     of fprec * dst  (* store float from stack, NO POP *)
 | FSTP    of fprec * dst  (* store float from stack, pop *)
 | FIST    of dst          (* store int32 from stack, no pop *)
 | FISTP   of dst          (* store int32 from stack, pop *)
 | FISTP64 of dst          (* store int64 from stack, pop *)
   (*
    * FMOV: Move fpsrc to fpdst via stack, NO POP. Give dest, src precisions
    * WARNING: Intel does NOT support the FT precision for FST, used by FMOV
    *)
 | FMOV    of fprec * dst * fprec * src
   (*
    * FMOVP: Move fpsrc to fpdst via stack, pop. Give dest, src precisions.
    *)
 | FMOVP   of fprec * dst * fprec * src
 | FXCH    of dst          (* exchange; dst must be %st(?) register *)
 | FCHS                    (* %st(0) <- negate %st(0) *)
 | FADDP                   (* %st(1) <- %st(1) + %st(0), pop stack *)
 | FSUBP                   (* %st(1) <- %st(1) - %st(0), pop stack *)
 | FMULP                   (* %st(1) <- %st(1) * %st(0), pop stack *)
 | FDIVP                   (* %st(1) <- %st(1) / %st(0), pop stack *)
 | FSUBRP                  (* %st(1) <- %st(0) - %st(1), pop stack *)
 | FDIVRP                  (* %st(1) <- %st(0) / %st(1), pop stack *)
 | FADD    of dst * src    (* dst <- dst + src, must be stack regs *)
 | FSUB    of dst * src    (* dst <- dst - src, must be stack regs *)
 | FMUL    of dst * src    (* dst <- dst * src, must be stack regs *)
 | FDIV    of dst * src    (* dst <- dst / src, must be stack regs *)
 | FSUBR   of dst * src    (* dst <- src - dst, must be stack regs *)
 | FDIVR   of dst * src    (* dst <- src / dst, must be stack regs *)
 | FPREM                   (* %st(0) <- %st(0) % %st(1) *)
 | FSIN                    (* %st(0) <- sin %st(0) *)
 | FCOS                    (* %st(0) <- cos %st(0) *)
 | FPATAN                  (* %st(0) <- atan (%st(1) / %st(0)) *)
 | FSQRT                   (* %st(0) <- sqrt %st(0) *)
 | FUCOM                   (* unordered compare %st(0), %st(1), no pop *)
 | FUCOMP                  (* unordered compare %st(0), %st(1), pop stack once *)
 | FUCOMPP                 (* unordered compare %st(0), %st(1), pop stack twice *)
 | FSTSW                   (* stores FP status word to AX *)

   (* Stack operations *)
 | PUSH  of iprec * src    (* push src onto stack *)
 | POP   of iprec * dst    (* pop stack into dst *)
 | CALL  of src            (* call external: return to EAX or EDX:EAX *)
 | FCALL of src            (* call external: return value to top of FP stack *)
 | RET

   (* Branching *)
 | CMP   of iprec * src * src    (* similar to SUB, but only flags modified *)
 | TEST  of iprec * src * src    (* similar to AND, but only flags modified *)
 | JMP   of dst                  (* unconditional jump to label named *)
 | FJMP  of dst                  (* unconditional jump to label named (implicit) *)
 | IJMP  of src list * dst       (* indirect jump to code pointer in dst *)
 | JCC   of cc * dst             (* conditional jump based on cc, to label named *)
   (*
    * WARNING: for SET, dst must be an 8-bit integer register.
    *)
 | SET   of cc * dst             (* set dst to (0,1) depending on cc *)

   (*
    * Info for reserve/copy-on-write:
    *    First arg is label/unique identifier
    *    Second arg is list of live pointers
    *    Third arg is #bytes to allocate
    *    Fourth arg is #ptrs to allocate
    *
    * COW adds a FIFTH argument, the copy pointer.
    *
    * RESP pushes the GC arguments.
    *)
 | RES  of src * reserve list * src * src
 | COW  of src * reserve list * src * src * src
 | RESP of src * reserve list

   (* Comment and nil pseudo-codes *)
 | CommentFIR of Fir.exp
 | CommentMIR of Mir.exp
 | CommentString of string
 | CommentInst of string * inst
 | NOP


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
