(*
   Simple offset arithmetic
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open X86_inst_type


(*
   Conversion.
   This will abort if the offset is not a number.
   Note, this should never happen if the relations
   below have been applied.
 *)
let to_int32 = function
   OffNumber a ->
      a
 | _ ->
      raise (Invalid_argument "Moogle_offset.to_int32")


(*
   Comparisons.
 *)
type moogle_offset_bool =
   True
 | False
 | Unknown

let equal32 a b =
   match a with
      OffNumber a ->
         a = b
    | _ ->
         false


let equal a b =
   match a, b with
      OffNumber a, OffNumber b ->
         if a = b then
            True
         else
            False
    | _ ->
         Unknown


let nequal a b =
   match a, b with
      OffNumber a, OffNumber b ->
         if a <> b then
            True
         else
            False
    | _ ->
         Unknown


let lt a b =
   match a, b with
      OffNumber a, OffNumber b ->
         if a < b then
            True
         else
            False
    | _ ->
         Unknown


let gt a b =
   match a, b with
      OffNumber a, OffNumber b ->
         if a > b then
            True
         else
            False
    | _ ->
         Unknown


let le a b =
   match a, b with
      OffNumber a, OffNumber b ->
         if a <= b then
            True
         else
            False
    | _ ->
         Unknown


let ge a b =
   match a, b with
      OffNumber a, OffNumber b ->
         if a >= b then
            True
         else
            False
    | _ ->
         Unknown


let ult a b =
   raise (Failure "not implemented: Moogle_offset.ult")


let ugt a b =
   raise (Failure "not implemented: Moogle_offset.ugt")


let ule a b =
   raise (Failure "not implemented: Moogle_offset.ule")


let uge a b =
   raise (Failure "not implemented: Moogle_offset.uge")


let is_power2 a =
   match a with
      OffNumber a ->
         if Moogle_lib.is_power2 a then
            True
         else
            False
    | _ ->
         Unknown


(*
   Take the log of a number.
 *)
let rec add a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.add a b)
    | _ ->
         OffAdd (a, b)


let rec sub a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.sub a b)
    | _ ->
         OffSub (a, b)


let rec mul a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.mul a b)
    | _ ->
         OffMul (a, b)


let rec div a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.div a b)
    | _ ->
         OffDiv (a, b)


let rec shift_left a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.shift_left a (Int32.to_int b))
    | _ ->
         OffSal (a, b)


let rec shift_right_logical a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.shift_right_logical a (Int32.to_int b))
    | _ ->
         OffShr (a, b)


let rec shift_right a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.shift_right a (Int32.to_int b))
    | _ ->
         OffSar (a, b)


let rec logand a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.logand a b)
    | _ ->
         OffAnd (a, b)


let rec logor a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.logor a b)
    | _ ->
         OffOr (a, b)


let rec logxor a b =
   match a, b with
      OffNumber a, OffNumber b ->
         OffNumber (Int32.logxor a b)
    | _ ->
         OffXor (a, b)


let rec neg a =
   match a with
      OffNumber a ->
         OffNumber (Int32.neg a)
    | _ ->
         OffUMinus a


let rec lognot a =
   match a with
      OffNumber a ->
         OffNumber (Int32.lognot a)
    | _ ->
         OffUNot a


let rec abs a =
   match a with
      OffNumber a ->
         OffNumber (Int32.abs a)
    | _ ->
         OffUAbs a


let rec log2 a =
   match a with
      OffNumber a ->
         OffNumber (Moogle_lib.log2 a)
    | _ ->
         OffULog2 a


let rec pow2 a =
   match a with
      OffNumber a ->
         OffNumber (Int32.shift_left Int32.one (Int32.to_int a))
    | _ ->
         OffUPow2 a
