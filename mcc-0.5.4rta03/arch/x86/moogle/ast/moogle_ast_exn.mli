(*
   Exception handler for moogle/kupo AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_ast_type


(* exn_loc
   Position/location information for exception handlers.  *)
type exn_loc =
   StringLoc of string * exn_loc
 | AtomLoc of pos
 | VarLoc of symbol


(* exn
   The possible exception cases for the AST.  *)
type exn =
   ParseError
 | UnboundVar of symbol
 | TypeError of Moogle_prog_type.ty * Moogle_prog_type.ty
 | TypeNotAllowed of Moogle_prog_type.ty
 | TypeInconsistency1 of Moogle_prog_type.ty
 | TypeInconsistency2 of Moogle_prog_type.ty * Moogle_prog_type.ty
 | StringError of string
 | SysError of string


(* ASTException
   Raise an exception from the AST.  *)
exception ASTException of exn_loc * exn


val vexp_pos      : symbol -> exn_loc
val exp_pos       : pos -> exn_loc
val string_pos    : string -> exn_loc -> exn_loc

val pos_of_operand            : ('a, 'b) operand -> pos
val pos_of_in_inst            : in_inst -> pos
val pos_of_out_inst           : out_inst -> pos
val pos_of_op_restriction     : op_restriction -> pos
val pos_of_inst_restriction   : inst_restriction -> pos
val pos_of_arith              : arith -> pos
val pos_of_clause             : clause -> pos

val union_pos                       : pos -> pos -> pos
val union_pos_operand_list          : pos -> ('a, 'b) operand list -> pos
val union_pos_arith_list            : pos -> arith list -> pos
val union_pos_in_inst_list          : pos -> in_inst list -> pos
val union_pos_out_inst_list         : pos -> out_inst list -> pos
val union_pos_op_restriction_list   : pos -> op_restriction list -> pos
val union_pos_inst_restriction_list : pos -> inst_restriction list -> pos

val pp_print_exn_loc : formatter -> exn_loc -> unit
val pp_print_exn     : formatter -> exn_loc -> exn -> unit
val print_exn        : exn_loc -> exn -> unit
