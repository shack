(*
   Type definition for the moogle/kupo languages
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol


(***  Position, Debugging, Basic Types  ***)


type pos = string * int * int * int * int

type opcode = symbol


(* unop
   binop
   relop
   Operators for arithmetic expressions.  *)
type unop =
   UMinusOp
 | UNotOp
 | UAbsOp
 | UPow2Op           (* Power of 2 *)
 | ULog2Op           (* Logarithm, base 2 *)

type binop =
   PlusOp
 | MinusOp
 | MulOp
 | DivOp
 | AndOp
 | OrOp
 | ShlOp             (* Logical/arithmetic shift left *)
 | ShrOp             (* Logical shift right *)
 | SarOp             (* Arithmetic shift right *)

type relop =
   EqOp
 | NeqOp
 | LtOp              (* Signed operations *)
 | LeOp
 | GtOp
 | GeOp
 | ULtOp             (* Unsigned operations *)
 | ULeOp
 | UGtOp
 | UGeOp

type boolop =
   BAndOp            (* Boolean, as in && *)
 | BOrOp             (* Boolean, as in || *)


(* ty
   Defines a simple type system for symbols.  For this stage, we only
   define the types of the registers (variable names).  Registers are
   either floating-point or integer registers.
      TyReg          Standard (integer) register
      TyFlReg        Floating-point register
      TyLabel        Symbol is a label name
      TyOper         Symbol is an arbitrary operand
      TyInt          Symbol binds to an int32 constant.
      TyInt31        Symbol binds to an int31 (ML) constant.
 *)
type ty =
   TyReg
 | TyFlReg
 | TyLabel
 | TyOper
 | TyInt
 | TyInt31


(***  AST Expressions  ***)


(* arith
   Simple arithmetic expressions:
      ArithInt (i, pos)             Represents integer i
      ArithReg (v, pos)             Represents variable v
      ArithUnop (op, a, pos)        Represents a unary operation
      ArithBinop (op, a1, a2, pos)  Represents a binary operation
 *)
type arith =
   ArithInt          of int32 * pos
 | ArithReg          of symbol * pos
 | ArithUnop         of unop * arith * pos
 | ArithBinop        of binop * arith * arith * pos


(* clause
   Simple clause expressions:
      ClauseCompare (op, a1, a2, pos)  Compare two integer values.
      ClauseIspower2 (a, pos)          Return true if a is a power of 2.
      ClauseBoolean (op, c1, c2, pos)  Combine two clauses with &&, ||.
 *)
type clause =
   ClauseCompare     of relop * arith * arith * pos
 | ClauseIspower2    of arith * pos
 | ClauseBoolean     of boolop * clause * clause * pos


(* op_restriction
   inst_restriction
   Defines the restrictions which are allowed on a pattern.  Operand-
   level restrictions include the containment and size limits, where
   as inst-level restrictions generally involve liveness.  These may
   only appear on input patterns.

   The operand level restrictions are:
      OpNotContains (v, pos)  Operand cannot contain reference to register.
      OpNotFPStack pos        Operand cannot be a floating-point stack reg.
      OpIsMem pos             Operand must be a memory operand (denoted * ).
      OpSmall pos             Operand must be a register with a small form.

   For ``small'' on X86, the register must be eax, ebx, ecx, or edx (a
   register which can be read using an 8- or 16-bit register name).

   The instruction level restrictions are:
      InstPreserves (v, pos)  Instruction preserves the value in register v.
      InstIgnores (v, pos)    Instruction ignores the value in register v.
      InstNotLive (v, pos)    Register v is not live out of the instruction.
      InstEndscope (v, pos)   Register v's scope ends at instruction.
      InstPreservesMem pos    Instruction does not modify memory.
      InstIgnoresMem pos      Instruction does not read or modify memory.
      InstIrrelevant pos      Instruction has no computational effect.
      InstTag (v, pos)        Named property v applies to this instruction.

   Note that InstEndScope is *not* the same as InstNotLive.  If the register
   is redefined at this instruction, then InstEndScope holds for the register
   while InstNotLive would not hold.
 *)
type op_restriction =
   OpNotContains     of symbol * pos
 | OpNotFPStack      of pos
 | OpIsMem           of pos
 | OpSmall           of pos

type inst_restriction =
   InstPreserves     of symbol * pos
 | InstIgnores       of symbol * pos
 | InstNotLive       of symbol * pos
 | InstEndscope      of symbol * pos
 | InstPreservesMem  of pos
 | InstIgnoresMem    of pos
 | InstIrrelevant    of pos
 | InstTag           of symbol * pos


(* value
   Defines an input value which may either be a register name, or
   an actual integer.  For inputs, these are the only two cases
   that may occur.  *)
type value =
   ValueInt          of int32
 | ValueReg          of symbol


(* operand
   Defines an operand.  Parameterized by the restriction type (if
   applicable) and the value type, to be used for slots expecting
   an integer value.  This form of operand is suitable for both
   input and output patterns.  The operands are listed below:
      Op (v, r, pos)                Any operand, named v.
      OpRegister (v, r, pos)        Integer register named v.
      OpFloatRegister (v, r, pos)   Floating-point register named v.
      OpFPStack (a, pos)            Floating-point stack register a.
      OpMemReg (v, r, pos)          Memory pointer stored in register.
      OpMemRegOff (v, a, r, pos)    Memory base/offset pointer.
      OpMemRegRegOffMul ...         Full Intel addressing mode.
      OpConstructor (v, pos)        ML constructor named v.
      OpInteger (a, pos)            Integer expression/value.
      OpOffset (a, pos)             Offset operand expression/value.
      OpLabel (label, pos)          Label name (for ImmediateLabel).
      OpString (text, pos)          String constant.
      OpExpr (text, pos)            OCaml expression that computes a
                                    single operand.  Cannot occur in
                                    an in operand.
 *)
type ('rest, 'value) operand =
   Op                   of symbol * 'rest * pos
 | OpRegister           of symbol * 'rest * pos
 | OpFloatRegister      of symbol * 'rest * pos
 | OpFPStack            of 'value * pos
 | OpMemReg             of symbol * 'rest * pos
 | OpMemRegOff          of symbol * 'value * 'rest * pos
 | OpMemRegRegOffMul    of symbol * symbol * 'value * 'value * 'rest * pos
 | OpConstructor        of symbol * pos
 | OpInteger            of 'value * pos
 | OpOffset             of 'value * pos
 | OpLabel              of symbol * pos
 | OpString             of string * pos
 | OpExpr               of string * pos


(* in_operand
   out_operand
   Operands specifically for input or output instructions.  The in_operand
   form is parameterized on operand restrictions and arithmetic values (note
   that arithmetic expressions are not permitted).  The out_operand form is
   parameterized on arithmetic expressions, but does not permit restrictions
   (therefore the restriction parametre has type unit).  *)
type in_operand = (op_restriction list, value) operand
type out_operand = (unit, arith) operand


(* in_inst
   Defines legal input instruction patterns.  These may
   include restrictions, unlike output instructions:
      InInst (opcode, operands, rests, pos)  Match a specific instruction.
      InStar (rests, pos)                    Match many instructions...
 *)
type in_inst =
   InInst               of opcode * in_operand list * inst_restriction list * pos
 | InStar               of inst_restriction list * pos


(* expr_type
   Expression types for OutExpr.  These indicate how an OCaml expression
   escape should be interpreted.  Expressions are mainly used in Kupo
   code blocks embedded in OCaml code (InlineKupo statements).
      ExprAddInst          OCaml code returns a single instruction to
                           add to the current listbuf.
      ExprAddList          OCaml code returns a list of instructions
                           to add to the current listbuf.
      ExprAddListbuf       OCaml code returns a listbuf of instructions
                           to append to the current listbuf.
      ExprModifyListbuf    OCaml code takes the listbuf, appends zero
                           or more instructions to it, and returns a
                           modified (new) listbuf for further processing.

   Note the difference between ExprAddListbuf and ExprModifyListbuf!
 *)
type expr_type =
   ExprAddInst
 | ExprAddList
 | ExprAddListbuf
 | ExprModifyListbuf


(* out_inst
   Defines legal output instructions.  The instructions
   may include the infamous Copy command, and transforms:
      OutInst (opcode, operands, pos)  Emit a specific output instruction.
      OutCopy (inst_numbers, pos)      Copy instructions from input pattern.
      OutTransform (v, i, args, pos)   Instruction generated by transform.
      OutNew (decls, pos)              Create new int/float register names.
      OutDecl (decls, pos)             Declare var types (for OCaml blocks).
      OutDef (decls, pos)              Define new integer variables.
      OutExpr (type, op, pos)          Arbitrary expression that computes
                                       instructions (see expr_type for more
                                       details; usually op is an OpExpr).
 *)
type out_inst =
   OutInst              of opcode * out_operand list * pos
 | OutCopy              of int32 list * pos
 | OutTransform         of symbol * int32 * arith list * pos
 | OutNew               of (symbol * ty) list * pos
 | OutDecl              of (symbol * ty) list * pos
 | OutDef               of (symbol * arith) list * pos
 | OutExpr              of expr_type * out_operand * pos


(* in_pattern
   out_pattern
   Defines the two critical pattern types.  *)
type in_pattern =
   InPattern            of in_inst list * clause option * pos

type out_pattern =
   OutPattern           of out_inst list * clause option * pos


(* instruction
   transform
   Defines an instruction name, operands list, and constructor. *)
type instruction =
   Instruction          of symbol * in_operand list * symbol * in_operand list * symbol list * pos

type transform =
   Transform            of symbol * in_operand list * symbol * out_operand list * pos


(* kupo_list
   Operation to perform on a list of instructions in Kupo.  You can either
   append to an existing Listbuf.t, or create a new Listbuf.t.
      KupoAppendList ident    Append to existing listbuf named ident.
      KupoNewList ident       Create a new listbuf.  When creating a new
                              listbuf, an identifier must be specified only
                              if we escape to OCaml code that manipulates
                              the listbuf.
 *)
type kupo_list =
   KupoAppendList       of symbol
 | KupoNewList          of symbol option


(* ocaml_block
   In OCaml blocks, identifies the next section as either raw OCaml
   code, or a set of Kupo output expressions.
      OCamlCode (text, pos)               Raw OCaml program text.
      KupoCode (buf, rtns, insts, pos)    List of instructions to emit.

   For Kupo code, the name of the instruction buffer to write to is given
   as buf.  The rtns is a list of operands that should be returned by the
   expression in addition to the Listbuf containing new instructions.  If
   non-empty, the operands will be returned in the listed order.  This is
   useful for returning the operands created by ``new'' statements within
   the kupo block, for further use.
 *)
type ocaml_block =
   OCamlCode            of string * pos
 | KupoCode             of kupo_list * out_operand list * out_inst list * pos


(* expr
   Defines a single AST expression in the language:
      Rule (inlist, outlist, pos)   Rewrite rule(s).
      Open (name, pos)              Open an ML module.
      Name (name, pos)              Set default rule names.
      Declarations (decls, pos)     Declare hardware registers and context.
      Instructions (instdefs, pos)  Define instruction set.
      InstructionType (ty, pos)     Name of ML instruction type.
      Transforms (f, args, xf, pos) Define a transform function.
      Trivial (v, pos)              Set the trivial property name.
      InlineKupo (data, pos)        OCaml block code/embedded kupo code.
 *)
type expr =
   Rule                 of (string option * in_pattern) list * out_pattern list * pos
 | Open                 of symbol * pos
 | Name                 of string * pos
 | Declarations         of (symbol * ty) list * pos
 | Instructions         of instruction list * pos
 | InstructionType      of symbol * pos
 | Transforms           of symbol * symbol list * transform list * pos
 | Trivial              of symbol * pos
 | InlineKupo           of ocaml_block list * pos
