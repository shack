(*
   Exception handler for moogle/kupo AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_ast_type
open Moogle_util


(* exn_loc
   Position/location information for exception handlers.  *)
type exn_loc =
   StringLoc of string * exn_loc
 | AtomLoc of pos
 | VarLoc of symbol


(* exn
   The possible exception cases for the AST.  *)
type exn =
   ParseError
 | UnboundVar of symbol
 | TypeError of Moogle_prog_type.ty * Moogle_prog_type.ty
 | TypeNotAllowed of Moogle_prog_type.ty
 | TypeInconsistency1 of Moogle_prog_type.ty
 | TypeInconsistency2 of Moogle_prog_type.ty * Moogle_prog_type.ty
 | StringError of string
 | SysError of string


(* ASTException
   Raise an exception from the AST.  *)
exception ASTException of exn_loc * exn


let vexp_pos v = VarLoc v
let exp_pos pos = AtomLoc pos
let string_pos s loc = StringLoc (s, loc)


let min_pos (sline1, schar1) (sline2, schar2) =
   if sline1 < sline2 then
      sline1, schar1
   else if sline1 = sline2 && schar1 < schar2 then
      sline1, schar1
   else
      sline2, schar2


let max_pos (sline1, schar1) (sline2, schar2) =
   if sline1 > sline2 then
      sline1, schar1
   else if sline1 = sline2 && schar1 > schar2 then
      sline1, schar1
   else
      sline2, schar2


let union_pos (file1, sline1, schar1, eline1, echar1)
              (file2, sline2, schar2, eline2, echar2) =
   let file : string = file1 in
   let sline, schar = min_pos (sline1, schar1) (sline2, schar2) in
   let eline, echar = max_pos (eline1, echar1) (eline2, echar2) in
      file, sline, schar, eline, echar


let pos_of_expr expr =
   match expr with
      Rule (_, _, pos)
    | Open (_, pos)
    | Name (_, pos)
    | Declarations (_, pos)
    | Instructions (_, pos)
    | InstructionType (_, pos)
    | Transforms (_, _, _, pos)
    | Trivial (_, pos)
    | InlineKupo (_, pos) ->
         pos


let pos_of_operand expr =
   match expr with
      Op (_, _, pos)
    | OpRegister (_, _, pos)
    | OpFloatRegister (_, _, pos)
    | OpFPStack (_, pos)
    | OpMemReg (_, _, pos)
    | OpMemRegOff (_, _, _, pos)
    | OpMemRegRegOffMul (_, _, _, _, _, pos)
    | OpConstructor (_, pos)
    | OpInteger (_, pos)
    | OpOffset (_, pos)
    | OpLabel (_, pos)
    | OpString (_, pos)
    | OpExpr (_, pos) ->
         pos


let pos_of_in_inst expr =
   match expr with
      InInst (_, _, _, pos)
    | InStar (_, pos) ->
         pos


let pos_of_out_inst expr =
   match expr with
      OutInst (_, _, pos)
    | OutCopy (_, pos)
    | OutTransform (_, _, _, pos)
    | OutNew (_, pos)
    | OutDecl (_, pos)
    | OutDef (_, pos)
    | OutExpr (_, _, pos) ->
         pos


let pos_of_op_restriction expr =
   match expr with
      OpNotContains (_, pos)
    | OpNotFPStack pos
    | OpIsMem pos
    | OpSmall pos ->
         pos


let pos_of_inst_restriction expr =
   match expr with
      InstPreserves (_, pos)
    | InstIgnores (_, pos)
    | InstNotLive (_, pos)
    | InstEndscope (_, pos)
    | InstPreservesMem pos
    | InstIgnoresMem pos
    | InstIrrelevant pos
    | InstTag (_, pos) ->
         pos


let pos_of_arith expr =
   match expr with
      ArithInt (_, pos)
    | ArithReg (_, pos)
    | ArithUnop (_, _, pos)
    | ArithBinop (_, _, _, pos) ->
         pos


let pos_of_clause expr =
   match expr with
      ClauseCompare (_, _, _, pos)
    | ClauseIspower2 (_, pos)
    | ClauseBoolean (_, _, _, pos) ->
         pos


let rec union_pos_list lookup pos1 list =
   match list with
      [] ->
         pos1
    | expr :: list ->
         let pos2 = lookup expr in
            union_pos_list lookup (union_pos pos1 pos2) list


let union_pos_operand_list pos ls   = union_pos_list pos_of_operand pos ls
let union_pos_arith_list            = union_pos_list pos_of_arith
let union_pos_in_inst_list          = union_pos_list pos_of_in_inst
let union_pos_out_inst_list         = union_pos_list pos_of_out_inst
let union_pos_op_restriction_list   = union_pos_list pos_of_op_restriction
let union_pos_inst_restriction_list = union_pos_list pos_of_inst_restriction


let rec pp_print_exn_loc buf exn_loc =
   match exn_loc with
      StringLoc (s, pos) ->
         fprintf buf "/%s@ " s;
         pp_print_exn_loc buf pos
    | AtomLoc (file, sline, schar, eline, echar) ->
         fprintf buf "%s: %d,%d-%d,%d" file sline schar eline echar
    | VarLoc v ->
         fprintf buf "%s" (string_of_symbol v)


let pp_print_exn buf pos msg =
   pp_print_flush buf ();
   fprintf buf "@[<v 0>*** Moogle AST Exception:@ @[<v 0>";
   pp_print_exn_loc buf pos;
   fprintf buf "@]:@ ";
   (match msg with
      ParseError ->
         fprintf buf "parse error"
    | UnboundVar v ->
         fprintf buf "unbound var:@ %s" (string_of_symbol v)
    | TypeError (ty1, ty2) ->
         fprintf buf "this expression has type@ %s@ but should have type@ %s" (string_of_type ty1) (string_of_type ty2)
    | TypeNotAllowed ty ->
         fprintf buf "expressions of this type are not allowed here:@ %s" (string_of_type ty)
    | TypeInconsistency1 ty ->
         fprintf buf "type inconsistency with type@ %s" (string_of_type ty)
    | TypeInconsistency2 (ty1, ty2) ->
         fprintf buf "type inconsistency with type@ %s * %s" (string_of_type ty1) (string_of_type ty2)
    | StringError s ->
         pp_print_string buf s
    | SysError s ->
         fprintf buf "system error:@ %s" s);
   fprintf buf "@]@."


let print_exn = pp_print_exn err_formatter
