(*
   Parse a peephole optimization file, stored in .kupo files
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(***  Header  ***)


{
   open Symbol
   open Moogle_ast_exn
   open Moogle_ast_parse


   (* Reference cells containing the file position. *)
   let current_line = ref 1
   let current_schar = ref 0


   (* reset_pos
      Resets the current position. *)
   let reset_pos () =
      current_line := 1;
      current_schar := 0


   (* set_next_line : lexbuf -> unit
      Advance by a single line. *)
   let set_next_line lexbuf =
      incr current_line;
      current_schar := Lexing.lexeme_end lexbuf


   (* set_lexeme_position : lexbuf -> pos
      Get the position of the current lexeme. We assume the entire
      lexeme is all on one line.  Brave assumption, that.  *)
   let set_lexeme_position lexbuf =
      let line = !current_line in
      let schar = Lexing.lexeme_start lexbuf - !current_schar in
      let echar = Lexing.lexeme_end lexbuf - !current_schar in
      let file = !Moogle_state.current_file in
      let pos = file, line, schar, line, echar in
         Moogle_state.current_pos := pos;
         pos


   (* As we are parsing, we will build up a stack of lexers as we enter
      nested comments and OCaml block code.  A stack is maintained here
      indicating the position of the beginning of the block, as well as
      a buffer for that block designed to store temporary text.  The
      buffer can be used to obtain the full text constructed by the nested
      lexer. *)
   let lexer_stack = Stack.create ()
   let lexer_buffer = Buffer.create 256

   let push_lexer lexbuf =
      let pos = set_lexeme_position lexbuf in
         Stack.push pos lexer_stack

   let push_lexer_init_buffer lexbuf =
      push_lexer lexbuf;
      Buffer.clear lexer_buffer

   let pop_lexer () =
      Stack.pop lexer_stack

   let pop_lexer_and_buffer () =
      let pos = pop_lexer () in
      let buf = Buffer.contents lexer_buffer in
         pos, buf

   let run_lexer f lexbuf =
      push_lexer lexbuf;
      f lexbuf;
      let _ = pop_lexer () in
         ()

   let buffer_append_string s =
      Buffer.add_string lexer_buffer s

   let buffer_append_lexbuf lexbuf =
      buffer_append_string (Lexing.lexeme lexbuf)
} (* End header *)


(***  Regexp's  ***)


(* Basic structures *)
let whitespace    = ['\t' ' ']+


(* Language tokens *)
let ident_end     = ['a'-'z' 'A'-'Z' '0'-'9' '_' '\'']*
let identifier1   = ['a'-'z'] ident_end
let identifier2   = '_' ['a'-'z' 'A'-'Z' '0'-'9' '\''] ident_end
let identifier    = identifier1 | identifier2
let cap_ident1    = ['A'-'Z'] ident_end
let cap_identifier= cap_ident1 ("." cap_ident1)*
let decimal       = '-'? ['1'-'9'] ['0'-'9']*
let hexadecimal   = "0x" ['0'-'9' 'a'-'f' 'A'-'F']+
let octal         = '0' ['0'-'7']*


(***  Lexing Rules  ***)


(* The main rule is invoked for normal lexing. *)
rule main = parse
   whitespace { main lexbuf }
 | '\n'       { set_next_line lexbuf; main lexbuf }
 | "/*"       { run_lexer comment_c lexbuf; main lexbuf }
 | "(*"       { run_lexer comment_ml lexbuf; main lexbuf }
 | octal {
      let pos = set_lexeme_position lexbuf in
         TokInt (Int32.of_string ("0o" ^ (Lexing.lexeme lexbuf)), pos)
   }
 | hexadecimal
 | decimal {
      let pos = set_lexeme_position lexbuf in
         TokInt (Int32.of_string (Lexing.lexeme lexbuf), pos)
   }
 | identifier {
      let pos = set_lexeme_position lexbuf in
      let ident = Lexing.lexeme lexbuf in
         match ident with
            "abs"                -> TokAbs pos
          | "add_inst"           -> TokAddInst pos
          | "add_list"           -> TokAddList pos
          | "add_listbuf"        -> TokAddListbuf pos
          | "begin_ocaml_code"   -> TokBeginOCaml pos
          | "copy"               -> TokCopy pos
          | "dead"               -> TokDead pos
          | "dcl"                -> TokDecl pos
          | "declarations"       -> TokDeclarations pos
          | "def"                -> TokDef pos
          | "end_ocaml_code"     -> TokEndOCaml pos
          | "endscope"           -> TokEndscope pos
          | "ignores"            -> TokIgnores pos
          | "inline_kupo"        -> TokInlineKupo pos
          | "input"              -> TokInput pos
          | "instruction_type"   -> TokInstType pos
          | "instructions"       -> TokInsts pos
          | "int31"              -> TokInt31 pos
          | "irrelevant"         -> TokIrrelevant pos
          | "is_power2"          -> TokIspower2 pos
          | "lbl"                -> TokLabel pos
          | "log2"               -> TokLog2 pos
          | "memory"             -> TokMemory pos
          | "modify_listbuf"     -> TokModifyListbuf pos
          | "name"               -> TokName pos
          | "new"                -> TokNew pos
          | "open"               -> TokOpen pos
          | "opr"                -> TokOper pos
          | "output"             -> TokOutput pos
          | "pow2"               -> TokPow2 pos
          | "preserves"          -> TokPreserves pos
          | "small"              -> TokSmall pos
          | "st"                 -> TokStack pos
          | "transform"          -> TokTrans pos
          | "trivial"            -> TokTrivial pos
          | "when"               -> TokWhen pos
          | _                    -> TokIdent (Symbol.add (Lexing.lexeme lexbuf), pos)
   }
 | cap_identifier {
      let pos = set_lexeme_position lexbuf in
      let ident = Lexing.lexeme lexbuf in
         TokCapIdent (Symbol.add (Lexing.lexeme lexbuf), pos)
   }
 | '"' {
      push_lexer_init_buffer lexbuf;
      let pos2 = string lexbuf in
      let pos1, text = pop_lexer_and_buffer () in
      let pos = union_pos pos1 pos2 in
         TokString (text, pos)
   }
 | '`' {
      push_lexer_init_buffer lexbuf;
      let pos2 = ocaml_block_unbraced lexbuf in
      let pos1, block = pop_lexer_and_buffer () in
      let pos = union_pos pos1 pos2 in
         TokOCamlBlock (block, pos)
   }
 | "`{" {
      push_lexer_init_buffer lexbuf;
      let pos2 = ocaml_block_braced lexbuf in
      let pos1, block = pop_lexer_and_buffer () in
      let pos = union_pos pos1 pos2 in
         TokOCamlBlock (block, pos)
   }
 | "->" | "<>" | "<=" | ">=" | "<u" | "<=u" | ">u" | ">=u" | "<<" | ">>" | ">>a" | "&&" | "||"
 | [':' ';' ',' '$' '%' '@' '+' '-' '*' '/' '^' '&' '|' '~' '(' ')' '[' ']' '!' '_' '?' '=' '<' '>'] {
      let pos = set_lexeme_position lexbuf in
      let op = Lexing.lexeme lexbuf in
         match op with
            ":"   -> TokColon pos
          | ";"   -> TokSemi pos
          | ","   -> TokComma pos
          | "$"   -> TokDollar pos
          | "%"   -> TokPercent pos
          | "@"   -> TokAt pos
          | "+"   -> TokPlus pos
          | "-"   -> TokMinus pos
          | "*"   -> TokStar pos
          | "/"   -> TokSlash pos
          | "^"   -> TokCaret pos
          | "&"   -> TokAmp pos
          | "|"   -> TokPipe pos
          | "~"   -> TokTilde pos
          | "("   -> TokLParen pos
          | ")"   -> TokRParen pos
          | "["   -> TokLBrack pos
          | "]"   -> TokRBrack pos
          | "!"   -> TokBang pos
          | "_"   -> TokUnder pos
          | "->"  -> TokArrow pos
          | "?"   -> TokQuery pos
          | "="   -> TokEqual pos
          | "<>"  -> TokNotEqual pos
          | "<"   -> TokLessThan pos
          | "<="  -> TokLessEqual pos
          | ">"   -> TokGreaterThan pos
          | ">="  -> TokGreaterEqual pos
          | "<u"  -> TokULessThan pos
          | "<=u" -> TokULessEqual pos
          | ">u"  -> TokUGreaterThan pos
          | ">=u" -> TokUGreaterEqual pos
          | "<<"  -> TokShl pos
          | ">>"  -> TokShr pos
          | ">>a" -> TokSar pos
          | "&&"  -> TokAmpAmp pos
          | "||"  -> TokPipePipe pos
          | _     -> raise (ASTException (exp_pos pos, StringError ("illegal operator: " ^ (String.escaped op))))
   }
 | _ {
      let pos = set_lexeme_position lexbuf in
      let s = Lexing.lexeme lexbuf in
         raise (ASTException (exp_pos pos, StringError ("illegal character: " ^ (String.escaped s))))
   }
 | eof {
      let pos = set_lexeme_position lexbuf in
         TokEof pos
   }


(***  Comment Lexers  ***)


and comment_c = parse
   '\n' { set_next_line lexbuf; comment_c lexbuf }
 | "/*" { run_lexer comment_c lexbuf; comment_c lexbuf }
 | "*/" { () }
 | _    { comment_c lexbuf }
 | eof  {
      let pos = pop_lexer () in
         raise (ASTException (exp_pos pos, StringError "comment unterminated"))
   }


and comment_ml = parse
   '\n' { set_next_line lexbuf; comment_ml lexbuf }
 | "(*" { run_lexer comment_ml lexbuf; comment_ml lexbuf }
 | "*)" { () }
 | _    { comment_ml lexbuf }
 | eof  {
      let pos = pop_lexer () in
         raise (ASTException (exp_pos pos, StringError "comment unterminated"))
   }


(***  String Lexer  ***)


and string = parse
   '\n' { buffer_append_lexbuf lexbuf; set_next_line lexbuf; string lexbuf }
 | '"'  { set_lexeme_position lexbuf }
 | '\\' _ {
      let s = Lexing.lexeme lexbuf in
      let c =
         match s.[1] with
            'b' -> '\b'
          | 'n' -> '\n'
          | 'r' -> '\r'
          | 't' -> '\t'
          |  c  ->  c
      in
         buffer_append_string (String.make 1 c); string lexbuf }
 | [^ '"' '\n' '\\']+ {
      buffer_append_lexbuf lexbuf; string lexbuf
   }
 | eof {
      let pos = pop_lexer () in
         raise (ASTException (exp_pos pos, StringError "string unterminated"))
   }


(***  OCaml Block Lexer  ***)


and ocaml_block_braced = parse
   '\n' { buffer_append_lexbuf lexbuf; set_next_line lexbuf; ocaml_block_braced lexbuf }
 | "``" { buffer_append_string "`"; ocaml_block_braced lexbuf }
 | "`}" { set_lexeme_position lexbuf }
 | "`{" | '`' {
      let pos = set_lexeme_position lexbuf in
         raise (ASTException (exp_pos pos, StringError "this backquote sequence is not allowed in OCaml braced block data"))
   }
 | [^ '`' '\n']+ {
      buffer_append_lexbuf lexbuf; ocaml_block_braced lexbuf
   }
 | eof  {
      let pos = pop_lexer () in
         raise (ASTException (exp_pos pos, StringError "ocaml braced block unterminated"))
   }


and ocaml_block_unbraced = parse
   '\n' { buffer_append_lexbuf lexbuf; set_next_line lexbuf; ocaml_block_unbraced lexbuf }
 | "``" { buffer_append_string "`"; ocaml_block_unbraced lexbuf }
 | "`}" | "`{" {
      let pos = set_lexeme_position lexbuf in
         raise (ASTException (exp_pos pos, StringError "this backquote sequence is not allowed in OCaml unbraced block data"))
   }
 | '`'  { set_lexeme_position lexbuf }
 | [^ '`' '\n']+ {
      buffer_append_lexbuf lexbuf; ocaml_block_unbraced lexbuf
   }
 | eof  {
      let pos = pop_lexer () in
         raise (ASTException (exp_pos pos, StringError "ocaml unbraced block unterminated"))
   }
