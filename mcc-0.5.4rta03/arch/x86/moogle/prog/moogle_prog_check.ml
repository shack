(*
   Typechecking code
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol

open Moogle_prog_env
open Moogle_prog_exn
open Moogle_prog_type


(***  Type Checking  ***)


(* check_type
   Check that the type of v is compatible with ty.  This is
   the basic type-checking primitive for register names.  *)
let check_type venv loc v ty =
   let loc = string_pos "check_type" loc in
   let ty' = venv_lookup venv loc v in
      match ty', ty with
         TyOp, TyOp
       | TyReg, TyReg
       | TyFlReg, TyFlReg
       | TyInt, TyInt
       | TyInt31, TyInt31
       | TyOff, TyOff
       | TyLabel, TyLabel ->
            ()
       | TyInt, TyOff
       | TyInt31, TyOff ->
            (* Ints can be promoted *)
            ()
       | _ ->
            raise (ProgException (loc, TypeError (ty', ty)))


(* check_operand
   Check that the types of symbols appearing in the operand
   are valid.  value_fn is the checker for values and will
   be passed the venv and the value in question.  *)
let check_operand value_fn venv op =
   let loc = string_pos "check_operand" (exp_pos (pos_of_operand op)) in
      match op with
         Op (v, _) ->
            check_type venv loc v TyOp
       | OpReg (v, _) ->
            check_type venv loc v TyReg
       | OpFlReg (v, _) ->
            check_type venv loc v TyFlReg
       | OpCons (_, _) ->
            ()
       | OpFPStack (stk, _) ->
            value_fn   ArithInt32  venv stk
       | OpMemReg (name, ptr, _) ->
            check_type venv loc name TyOp;
            check_type venv loc ptr TyReg
       | OpMemRegOff (name, ptr, off, _) ->
            check_type venv loc name TyOp;
            check_type venv loc ptr TyReg;
            value_fn   ArithInt32  venv off
       | OpMemRegRegOffMul (name, ptr, reg, off, mul, _) ->
            check_type venv loc name TyOp;
            check_type venv loc ptr TyReg;
            check_type venv loc reg TyReg;
            value_fn   ArithOffset venv off;
            value_fn   ArithInt32  venv mul
       | OpInt (i, _) ->
            value_fn   ArithInt32  venv i
       | OpOff (i, _) ->
            value_fn   ArithOffset  venv i
       | OpLabel (label, _) ->
            check_type venv loc label TyLabel
       | OpString (_, _) ->
            ()
       | OpExpr (_, _, _) ->
            ()


(* check_arith
   Checks the types in an arithmetic expression for correctness.  *)
let rec check_arith ty venv op =
   let loc = string_pos "check_arith" (exp_pos (pos_of_arith op)) in
      match ty, op with
         _, ArithInt _ ->
            ()
       | _, ArithReg (v, _) ->
            check_type venv loc v TyInt
       | _, ArithReg31 (v, _) ->
            check_type venv loc v TyInt31
       | ArithInt32, ArithOff (_, _) ->
            ()
       | ArithOffset, ArithOff (v, _) ->
            check_type venv loc v TyOff
       | _, ArithLabel (v, _) ->
            check_type venv loc v TyLabel
       | _, ArithUnop (_, v, _) ->
            check_arith ty venv v
       | _, ArithBinop (_, v1, v2, _) ->
            check_arith ty venv v1;
            check_arith ty venv v2


(* check_arith_operand
   Checks the types in an operand with arithmetic values.  *)
let check_arith_operand = check_operand check_arith
