(*
   Manipulate operands from the AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol

open Moogle_ast_exn
open Moogle_prog_check
open Moogle_prog_env
open Moogle_prog_ocaml
open Moogle_prog_type
open Moogle_util

module AST = Moogle_ast_type


(***  Operand Rewriting - Arithmetic  ***)


(* rewrite_unop
   rewrite_binop
   rewrite_relop
   rewrite_boolop
   Rewrites the operator indicated.  *)
let rewrite_unop op =
   match op with
      AST.UMinusOp   -> UMinusOp
    | AST.UNotOp     -> UNotOp
    | AST.UAbsOp     -> UAbsOp
    | AST.ULog2Op    -> ULog2Op
    | AST.UPow2Op    -> UPow2Op

let rewrite_binop op =
   match op with
      AST.PlusOp     -> PlusOp
    | AST.MinusOp    -> MinusOp
    | AST.MulOp      -> MulOp
    | AST.DivOp      -> DivOp
    | AST.ShlOp      -> ShlOp
    | AST.ShrOp      -> ShrOp
    | AST.SarOp      -> SarOp
    | AST.AndOp      -> AndOp
    | AST.OrOp       -> OrOp

let rewrite_relop op =
   match op with
      AST.EqOp       -> EqOp
    | AST.NeqOp      -> NeqOp
    | AST.LtOp       -> LtOp
    | AST.LeOp       -> LeOp
    | AST.GtOp       -> GtOp
    | AST.GeOp       -> GeOp
    | AST.ULtOp      -> ULtOp
    | AST.ULeOp      -> ULeOp
    | AST.UGtOp      -> UGtOp
    | AST.UGeOp      -> UGeOp

let rewrite_boolop op =
   match op with
      AST.BAndOp     -> BAndOp
    | AST.BOrOp      -> BOrOp


(* rewrite_arith
   Rewrites an AST arithmetic expression into a program's arithmetic expr.
   Types of symbols appearing in op are checked that they have an int-happy
   type using the venv given.  The new arith is returned on success.  *)
let rec rewrite_arith venv op =
   let loc = string_pos "rewrite_arith" (exp_pos (pos_of_arith op)) in
      match op with
         AST.ArithInt (i, pos) ->
            ArithInt (i, pos)
       | AST.ArithReg (v, pos) ->
            (* Symbols may either match an integer value, or may match
               an offset (sub)expression.  If the symbol matches an offset
               (sub)expression, then we must use the ArithOff form to
               indicate the type explicitly. *)
            (match venv_lookup venv loc v with
               TyInt ->
                  ArithReg (v, pos)
             | TyInt31 ->
                  ArithReg31 (v, pos)
             | TyOff ->
                  ArithOff (v, pos)
             | TyLabel ->
                  ArithLabel (v, pos)
             | _ ->
                  raise (ASTException (loc, StringError "register must be an Int, an Offset, or a label.")))
       | AST.ArithUnop (op, v, pos) ->
            ArithUnop (rewrite_unop op, rewrite_arith venv v, pos)
       | AST.ArithBinop (op, v1, v2, pos) ->
            ArithBinop (rewrite_binop op, rewrite_arith venv v1, rewrite_arith venv v2, pos)


(* rewrite_clause
   Writes a clause from an expression.  *)
let rec rewrite_clause venv expr =
   match expr with
      AST.ClauseCompare (op, a, b, pos) ->
         ClauseCompare (rewrite_relop op, rewrite_arith venv a, rewrite_arith venv b, pos)
    | AST.ClauseIspower2 (e, pos) ->
         ClauseIspower2 (rewrite_arith venv e, pos)
    | AST.ClauseBoolean (op, a, b, pos) ->
         ClauseBoolean (rewrite_boolop op, rewrite_clause venv a, rewrite_clause venv b, pos)


(* clause_of_expr_option
   Writes an optional clause from an optional expression.  *)
let rewrite_clause_option venv expr =
   match expr with
      None ->
         None
    | Some e ->
         Some (rewrite_clause venv e)


(* alias_operand
   Aliases an operand to its proper type, if necessary.  *)
let alias_operand aenv value_reg op =
   let pos = pos_of_operand op in
   let loc = string_pos "alias_operand" (exp_pos pos) in
      match op with
         AST.Op (v, rest, _) ->
            let ty = venv_lookup aenv loc v in
            let op =
               match ty with
                  TyOp ->
                     AST.Op (v, rest, pos)
                | TyReg ->
                     AST.OpRegister (v, rest, pos)
                | TyFlReg ->
                     AST.OpFloatRegister (v, rest, pos)
                | TyOff ->
                     AST.OpOffset (value_reg v, pos)
                | TyInt
                | TyInt31 ->
                     AST.OpInteger (value_reg v, pos)
                | TyLabel ->
                     AST.OpLabel (v, pos)
                | TyInst
                | TyInsts ->
                     raise (ASTException (loc, TypeNotAllowed ty))
            in
               op
       | AST.OpRegister _
       | AST.OpFloatRegister _
       | AST.OpFPStack _
       | AST.OpMemReg _
       | AST.OpMemRegOff _
       | AST.OpMemRegRegOffMul _
       | AST.OpConstructor _
       | AST.OpInteger _
       | AST.OpOffset _
       | AST.OpLabel _
       | AST.OpString _
       | AST.OpExpr _ ->
            op


(* rewrite_arith_operand
   Rewrites an operand containing arithmetic values from the AST given.
   Types of symbols are checked against the provided venv, and the prog
   arith operand is returned on success.  *)
let rewrite_arith_operand venv op =
   let name = new_symbol_string "__operand" in
   let pos = pos_of_operand op in
   let loc = string_pos "rewrite_arith_operand" (exp_pos pos) in
   let op = alias_operand venv (fun v -> AST.ArithReg (v, pos)) op in
      match op with
         AST.Op (v, _, _) ->
            let () = check_type venv loc v TyOp in
               Op (v, pos)
       | AST.OpRegister (v, _, _) ->
            let () = check_type venv loc v TyReg in
               OpReg (v, pos)
       | AST.OpFloatRegister (v, _, _) ->
            let () = check_type venv loc v TyFlReg in
               OpFlReg (v, pos)
       | AST.OpFPStack (v, _) ->
            let v = rewrite_arith venv v in
            let () = check_arith ArithInt32  venv v in
               OpFPStack (v, pos)
       | AST.OpMemReg (ptr, _, _) ->
            let () = check_type venv loc ptr TyReg in
            let v = new_operand_name () in
               OpMemReg (v, ptr, pos)
       | AST.OpMemRegOff (ptr, off, _, _) ->
            let off = rewrite_arith venv off in
            let () = check_type venv loc ptr TyReg in
            let () = check_arith ArithOffset venv off in
            let v = new_operand_name () in
               OpMemRegOff (v, ptr, off, pos)
       | AST.OpMemRegRegOffMul (ptr, reg, off, mul, _, _) ->
            let off = rewrite_arith venv off in
            let mul = rewrite_arith venv mul in
            let () = check_type venv loc ptr TyReg in
            let () = check_type venv loc reg TyReg in
            let () = check_arith ArithOffset venv off in
            let () = check_arith ArithInt32  venv mul in
            let v = new_operand_name () in
               OpMemRegRegOffMul (v, ptr, reg, off, mul, pos)
       | AST.OpConstructor (v, _) ->
            OpCons (v, pos)
       | AST.OpInteger (v, _) ->
            let v = rewrite_arith venv v in
            let () = check_arith ArithInt32  venv v in
               OpInt (v, pos)
       | AST.OpOffset (v, _) ->
            let v = rewrite_arith venv v in
            let () = check_arith ArithOffset  venv v in
               OpOff (v, pos)
       | AST.OpLabel (label, _) ->
            let () = check_type venv loc label TyLabel in
               OpLabel (label, pos)
       | AST.OpString (text, _) ->
            OpString (text, pos)
       | AST.OpExpr (text, orig_pos) ->
            let file_pos = ocaml_file_pos_of_pos orig_pos in
               OpExpr (file_pos, text, pos)


(* rewrite_arith_operands
   Process multiple operands at once.  *)
let rewrite_arith_operands venv = List.map (rewrite_arith_operand venv)


(***  Match Operands  ***)


(* rename_symbol
   Checks the pattern environment; if v is already defined in
   the variable environment, rename it here so we don't get a
   multiple-bindings error when printing out the pattern.  The
   new symbol (or original symbol) is added to the environment
   with the given type.  Note: constructors should not be
   passed to this function.  *)
let rename_symbol venv rest loc v pos ty =
   let loc = string_pos "rename_symbol" loc in
   let rest, v' =
      if venv_mem venv v then
         let ty' = venv_lookup venv loc v in
         let v' = new_symbol_string ("__" ^ (string_of_symbol v)) in
         let () =
            if ty <> ty' then
               raise (ASTException (loc, TypeError (ty, ty')))
         in
            (OperandsEqual (v, v', pos)) :: rest, v'
      else
         rest, v
   in
   let venv = venv_add venv v' ty in
      venv, rest, v'


(* add_op_restrictions
   Add the operand restrictions provided to the restriction
   environment.  v should be a _base_ name, not a name that
   is generated by rename_symbol.  *)
let add_op_restrictions venv rest v rest' =
   let add_restriction rest rest' =
      let pos = pos_of_op_restriction rest' in
      let loc = string_pos "add_op_restrictions" (exp_pos pos) in
         match rest' with
            AST.OpNotContains (v', _) ->
               begin
               let ty  = venv_lookup venv loc v in
               let ty' = venv_lookup venv loc v' in
                  match ty, ty' with
                     TyOp, TyReg ->
                        (OperandNotContain (v, v', pos)) :: rest
                   | TyReg, TyReg
                   | TyFlReg, TyFlReg ->
                        (RegistersNotEqual (v, v', pos)) :: rest
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency2 (ty, ty')))
               end (* OpNotContains *)
          | AST.OpNotFPStack _ ->
               begin
               let ty = venv_lookup venv loc v in
                  match ty with
                     TyOp ->
                        (OperandNotFPStack (v, pos)) :: rest
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency1 ty))
               end (* OpNotFPStack *)
          | AST.OpIsMem _ ->
               begin
               let ty = venv_lookup venv loc v in
                  match ty with
                     TyOp ->
                        (OperandIsMem (v, pos)) :: rest
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency1 ty))
               end (* OpIsMem *)
          | AST.OpSmall _ ->
               begin
               let ty = venv_lookup venv loc v in
                  match ty with
                     TyOp ->
                        (OperandSmall (v, pos)) :: rest
                   | TyReg ->
                        (RegisterSmall (v, pos)) :: rest
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency1 ty))
               end (* OpSmall *)
   in
      List.fold_left add_restriction rest rest'


(* create_symbol
   Creates a new generic symbol with type indicated.  Useful for
   matching memory operands or integer values where we must create
   a fake name to pattern in.  *)
let create_symbol venv ty =
   let v = new_symbol_string "__temp" in
   let venv = venv_add venv v ty in
      venv, v


(* create_int_reg
   Creates the symbols (as necessary) to bind an argument of
   type int_reg (which may be an integer or a register value). *)
let create_int_reg venv rest loc v pos =
   let loc = string_pos "create_int_reg" loc in
      match v with
         AST.ValueReg v ->
            let venv, rest, v = rename_symbol venv rest loc v pos TyInt in
               venv, rest, v
       | AST.ValueInt i ->
            let venv, v = create_symbol venv TyInt in
            let rest = (IntegerEqual (v, i, pos)) :: rest in
               venv, rest, v


(* create_off_reg
   Creates the symbols (as necessary) to bind an argument of
   type off_reg (which may be an integer or a register value). *)
let create_off_reg venv rest loc v pos =
   let loc = string_pos "create_off_reg" loc in
      match v with
         AST.ValueReg v ->
            let venv, rest, v = rename_symbol venv rest loc v pos TyOff in
               venv, rest, v
       | AST.ValueInt i ->
            let venv, v = create_symbol venv TyInt in
            let rest = (OffsetEqual (v, i, pos)) :: rest in
               venv, rest, v


(* rewrite_match_operand_using_env
   rewrite_match_operand
   Rewrites an AST operand into an operand suitable for match expressions.
   This takes a venv and restriction list which will be extended to include
   the new symbols bound, and the restrictions added by this AST operand.
   The prog-happy operand will be returned, in addition to revised venv and
   restriction list.  Each symbol is guaranteed to be bound at most once in
   the resulting operand.  *)
let rewrite_match_operand venv rest op =
   let name = new_operand_name () in
   let pos = pos_of_operand op in
   let loc = string_pos "rewrite_match_operand" (exp_pos pos) in
      match op with
         AST.Op (v, rest', _) ->
            let venv, rest, v' = rename_symbol venv rest loc v pos TyOp in
            let rest = add_op_restrictions venv rest v rest' in
               venv, rest, Op (v', pos)
       | AST.OpRegister (v, rest', _) ->
            let venv, rest, v' = rename_symbol venv rest loc v pos TyReg in
            let rest = add_op_restrictions venv rest v rest' in
               venv, rest, OpReg (v', pos)
       | AST.OpFloatRegister (v, rest', _) ->
            let venv, rest, v' = rename_symbol venv rest loc v pos TyFlReg in
            let rest = add_op_restrictions venv rest v rest' in
               venv, rest, OpFlReg (v', pos)
       | AST.OpFPStack (v, _) ->
            let venv, rest, v' = create_int_reg venv rest loc v pos in
               venv, rest, OpFPStack (v', pos)
       | AST.OpMemReg (ptr, rest', _) ->
            let venv, v = create_symbol venv TyOp in
            let venv, rest, ptr' = rename_symbol venv rest loc ptr pos TyReg in
            let rest = add_op_restrictions venv rest v rest' in
               venv, rest, OpMemReg (v, ptr', pos)
       | AST.OpMemRegOff (ptr, off, rest', _) ->
            let venv, v = create_symbol venv TyOp in
            let venv, rest, ptr' = rename_symbol venv rest loc ptr pos TyReg in
            let venv, rest, off' = create_off_reg venv rest loc off pos in
            let rest = add_op_restrictions venv rest v rest' in
               venv, rest, OpMemRegOff (v, ptr', off', pos)
       | AST.OpMemRegRegOffMul (ptr, reg, off, mul, rest', _) ->
            let venv, v = create_symbol venv TyOp in
            let venv, rest, ptr' = rename_symbol venv rest loc ptr pos TyReg in
            let venv, rest, reg' = rename_symbol venv rest loc reg pos TyReg in
            let venv, rest, off' = create_off_reg venv rest loc off pos  in
            let venv, rest, mul' = create_int_reg venv rest loc mul pos in
            let rest = add_op_restrictions venv rest v rest' in
               venv, rest, OpMemRegRegOffMul (v, ptr', reg', off', mul', pos)
       | AST.OpConstructor (v, _) ->
            venv, rest, OpCons (v, pos)
       | AST.OpInteger (v, _) ->
            let venv, rest, v = create_int_reg venv rest loc v pos in
               venv, rest, OpInt (v, pos)
       | AST.OpOffset (v, _) ->
            let venv, rest, v = create_off_reg venv rest loc v pos in
               venv, rest, OpOff (v, pos)
       | AST.OpLabel (label, _) ->
            let venv, rest, label' = rename_symbol venv rest loc label pos TyLabel in
               venv, rest, OpLabel (label', pos)
       | AST.OpString (text, _) ->
            venv, rest, OpString (text, pos)
       | AST.OpExpr (_, _) ->
            raise (ASTException (loc, StringError "match expressions cannot accept an OCaml expression"))

let rewrite_match_operand_using_env aenv venv rest op =
   let op = alias_operand aenv (fun v -> AST.ValueReg v) op in
      rewrite_match_operand venv rest op


(* rewrite_match_operands_using_env
   rewrite_match_operands
   Same thing, but for multiple operands.  *)
let rewrite_match_operands_using_env aenv venv rest operands =
   let venv, rest, operands =
      List.fold_left (fun (venv, rest, operands) op ->
         let venv, rest, op = rewrite_match_operand_using_env aenv venv rest op in
            venv, rest, op :: operands) (venv, rest, []) operands
   in
      venv, rest, List.rev operands

let rewrite_match_operands venv rest operands =
   let venv, rest, operands =
      List.fold_left (fun (venv, rest, operands) op ->
         let venv, rest, op = rewrite_match_operand venv rest op in
            venv, rest, op :: operands) (venv, rest, []) operands
   in
      venv, rest, List.rev operands
