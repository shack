(*
   Manipulate operands from the AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_ast_exn
open Moogle_prog_type


(* Manipulation of operands and clauses *)
val rewrite_arith : ty SymbolTable.t -> Moogle_ast_type.arith -> arith
val rewrite_arith_operand : ty SymbolTable.t -> Moogle_ast_type.out_operand -> arith operand
val rewrite_arith_operands : ty SymbolTable.t -> Moogle_ast_type.out_operand list -> arith operand list

val rewrite_match_operand : ty SymbolTable.t -> restriction list -> Moogle_ast_type.in_operand ->
                            ty SymbolTable.t * restriction list * symbol operand
val rewrite_match_operands : ty SymbolTable.t -> restriction list -> Moogle_ast_type.in_operand list ->
                             ty SymbolTable.t * restriction list * symbol operand list
val rewrite_match_operand_using_env : ty SymbolTable.t -> ty SymbolTable.t -> restriction list ->
                                      Moogle_ast_type.in_operand ->
                                      ty SymbolTable.t * restriction list * symbol operand
val rewrite_match_operands_using_env : ty SymbolTable.t -> ty SymbolTable.t -> restriction list ->
                                       Moogle_ast_type.in_operand list ->
                                       ty SymbolTable.t * restriction list * symbol operand list
val rewrite_clause_option : ty SymbolTable.t -> Moogle_ast_type.clause option -> clause option
