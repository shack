(*
   IO Rule Processing for the moogle/kupo languages
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format

open Symbol
open Moogle_ast_exn
open Moogle_prog_type
open Moogle_prog_pattern
open Moogle_util
module AST = Moogle_ast_type


(***  I/O Rules  ***)


(* rule_create
   Build a new rule, using the information given as a guide-
   line.  Each rule contains one input and one output pattern,
   so we must produce a rule for every output, not just once
   for every InputOutput expression.  *)
let rule_create name input output =
   { rule_name    =  name;
     rule_input   =  input;
     rule_output  =  output;
   }


(* rule_outputs
   Process all the outputs for this rule, building up the
   inputs as necessary.  Each output produces a new ruleset,
   so the input block will be interleaved with each and every
   output in the list. *)
let rule_outputs prog name input expr =
   let rec rule_outputs expr =
      match expr with
         AST.OutPattern _ as pattern :: expr ->
            let output = new_out_pattern input.ipat_venv in
            let output = out_pattern_of_expr prog output pattern in
               rule_create name input output :: rule_outputs expr
       | [] ->
            []
   in
      rule_outputs expr

let rule_outputs prog name inputs expr =
   List.flatten (List.map (fun input -> rule_outputs prog name input expr) inputs)


(* rule_inputs
   Similar to above, but we process inputs here.  The name
   is applied at this level.  *)
let rule_inputs prog expr output_expr_list =
   let name = prog.prog_name in
   let rec rule_inputs expr =
      match expr with
         (Some name, (AST.InPattern _ as pattern)) :: expr ->
            let rules = rule_inputs expr in
            let input = new_in_pattern prog.prog_decls in
            let inputs = in_patterns_of_expr prog input pattern in
            let rules' = rule_outputs prog name inputs output_expr_list in
               rules' @ rules
       | (None, (AST.InPattern _ as pattern)) :: expr ->
            let rules = rule_inputs expr in
            let input = new_in_pattern prog.prog_decls in
            let inputs = in_patterns_of_expr prog input pattern in
            let rules' = rule_outputs prog name inputs output_expr_list in
               rules' @ rules
       | [] ->
            []
   in
      rule_inputs expr


(* rules_of_expr
   Builds rules for the expression data given.  This
   code properly handles the Name command for providing a
   default name to anonymous rules.  One rule is produced
   for every pair of INPUT and OUTPUT commands.  *)
let rules_of_expr prog pos inputs outputs =
   let name = prog.prog_name in
   let venv = prog.prog_decls in
   let rules = rule_inputs prog inputs outputs in
   let rules = prog.prog_rules @ rules in
      { prog with prog_rules = rules }
