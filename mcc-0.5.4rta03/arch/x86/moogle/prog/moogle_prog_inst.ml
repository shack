(*
   Construct instructions from the AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(*
   Gee, you know they really kept us locked up for a
   long time.  My hair's THIS long as a result!
      -- Shinichiro Okaniwa (as Magus)
         Dream Team Ending, Chrono Trigger
 *)


open Format
open Symbol

open Moogle_ast_exn
open Moogle_prog_env
open Moogle_prog_operand
open Moogle_prog_type
open Moogle_state
open Moogle_util

module AST = Moogle_ast_type


(* insts_of_expr
   Returns a definition of the instructions found in an instruction
   block which was preceded by the instruction tags listed in the
   tags argument.  The new instruction definitions will be added to
   prog with these tags.  *)
let insts_of_expr prog pos inst_list =
   let pos = string_pos "insts_of_expr" (exp_pos pos) in
   let add_inst (insts, prog_tags, rorder) inst =
      let AST.Instruction (name, operands, cons, cons_operands, tags, _) = inst in
      let prog_tags, tags = List.fold_left (fun (prog_tags, tags) t ->
         venv_add prog_tags t (), venv_add tags t ()) (prog.prog_tags, venv_empty) tags
      in
      let venv, rest, operands = rewrite_match_operands prog.prog_decls [] operands in
      let () =
         if rest <> [] then
            raise (ASTException (pos, StringError "restrictions not allowed here"))
      in
      let venv, rest, cons_operands = rewrite_match_operands_using_env venv prog.prog_decls [] cons_operands in
      let () =
         if rest <> [] then
            raise (ASTException (pos, StringError "restrictions not allowed here"))
      in
      let inst = { inst_constructor = cons, cons_operands;
                   inst_operands    = operands;
                   inst_tags        = tags;
                 }
      in
         venv_add insts name inst, prog_tags, name :: rorder
   in
   let insts, prog_tags, rorder = List.fold_left add_inst (prog.prog_insts, prog.prog_tags, prog.prog_inst_rorder) inst_list in
      { prog with prog_insts        = insts;
                  prog_inst_rorder  = rorder;
                  prog_tags         = prog_tags;
      }


(* process_inst
   Lookup the instruction with name opcode, and rewrite it and its
   operands to the native (and proper) instruction and opcodes. Will
   raise an error if opcode isn't the wildcard, and the instruction
   is not defined in prog.  *)
let process_inst prog pos opcode operands =
   let pos = string_pos "process_inst" pos in
   let rec bind_operands venv vars ops =
      match vars, ops with
         Op (v, _) :: vars, op :: ops ->
            bind_operands (venv_add venv v op) vars ops
       | [], [] ->
            venv
       | _ :: _, _ :: _ ->
            raise (ASTException (pos, StringError "cannot bind to non-OPERAND patterns yet"))
       | _ ->
            raise (ASTException (pos, StringError ("arity mismatch in operands in binding for " ^ (string_of_symbol opcode))))
   in
   let rec eval_operands venv ops =
      match ops with
         Op (v, _) :: ops ->
            let op = venv_lookup venv pos v in
               op :: eval_operands venv ops
       | OpCons _ as op :: ops ->
            op :: eval_operands venv ops
       | [] ->
            []
       | _ :: _ ->
            raise (ASTException (pos, StringError "cannot eval non-OPERAND patterns yet"))
   in
      if Symbol.eq opcode underscore then
         (* Wildcard instruction, pass through *)
         opcode, operands
      else
         (* Instruction defined; do type checking and rewrite *)
         let inst = venv_lookup prog.prog_insts pos opcode in
         let venv = bind_operands venv_empty inst.inst_operands operands in
         let cons, operands = inst.inst_constructor in
         let operands = eval_operands venv operands in
            cons, operands


(* transforms_of_expr
   Constructs a transform function with name given.  Transforms
   map instructions of one type into instructions of another type,
   and are useful f.e. in the floating-point code.  *)
let transforms_of_expr prog pos name args inst_list =
   let pos = string_pos "transforms_of_expr" (exp_pos pos) in
   let venv = List.fold_left (fun venv v -> venv_add venv v TyInt) prog.prog_decls args in
   let rewrite_inst (AST.Transform (opcode1, operand1, opcode2, operand2, pos)) =
      let pos = string_pos "transforms_of_expr" (exp_pos pos) in
      let venv, rest, operand1 = rewrite_match_operands venv [] operand1 in
      let () =
         if rest <> [] then
            raise (ASTException (pos, StringError "restrictions not allowed here"))
      in
      let operand2 = rewrite_arith_operands venv operand2 in
      let opcode1, operand1 = process_inst prog pos opcode1 operand1 in
      let opcode2, operand2 = process_inst prog pos opcode2 operand2 in
         opcode1, operand1, opcode2, operand2
   in
   let inst_list = List.map rewrite_inst inst_list in
   let transform =
      { transform_args = args;
        transform_data = inst_list;
      }
   in
   let transforms = venv_add prog.prog_transforms name transform in
      { prog with prog_transforms = transforms }
