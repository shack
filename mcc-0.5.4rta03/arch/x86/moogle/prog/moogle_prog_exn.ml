(*
   Exception handler for moogle/kupo Prog
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_prog_type
open Moogle_util


(* exn_loc
   Position/location information for exception handlers.  *)
type exn_loc = Moogle_ast_exn.exn_loc


(* exn
   The possible exception cases for the Prog.  *)
type exn =
   UnboundVar of symbol
 | TypeError of Moogle_prog_type.ty * Moogle_prog_type.ty
 | StringError of string


(* ProgException
   Raise an exception from the Prog.  *)
exception ProgException of exn_loc * exn


let vexp_pos = Moogle_ast_exn.vexp_pos
let exp_pos = Moogle_ast_exn.exp_pos
let string_pos = Moogle_ast_exn.string_pos


let pos_of_operand op =
   match op with
      Op (_, pos)
    | OpReg (_, pos)
    | OpFlReg (_, pos)
    | OpFPStack (_, pos)
    | OpMemReg (_, _, pos)
    | OpMemRegOff (_, _, _, pos)
    | OpMemRegRegOffMul (_, _, _, _, _, pos)
    | OpCons (_, pos)
    | OpInt (_, pos)
    | OpOff (_, pos)
    | OpLabel (_, pos)
    | OpString (_, pos)
    | OpExpr (_, _, pos) ->
         pos


let pos_of_arith op =
   match op with
      ArithInt (_, pos)
    | ArithReg (_, pos)
    | ArithReg31 (_, pos)
    | ArithOff (_, pos)
    | ArithLabel (_, pos)
    | ArithUnop (_, _, pos)
    | ArithBinop (_, _, _, pos) ->
         pos


let pp_print_exn_loc = Moogle_ast_exn.pp_print_exn_loc


let pp_print_exn buf pos msg =
   pp_print_flush buf ();
   fprintf buf "@[<v 0>*** Moogle Prog Exception:@ @[<v 0>";
   pp_print_exn_loc buf pos;
   fprintf buf "@]:@ ";
   (match msg with
      UnboundVar v ->
         fprintf buf "unbound var:@ %s" (string_of_symbol v)
    | TypeError (ty1, ty2) ->
         fprintf buf "this expression has type@ %s@ but should have type@ %s" (string_of_type ty1) (string_of_type ty2)
    | StringError s ->
         pp_print_string buf s);
   fprintf buf "@]@."


let print_exn = pp_print_exn err_formatter
