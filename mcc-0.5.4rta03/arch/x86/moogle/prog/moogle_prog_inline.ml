(*
   Inline Kupo Processing for the moogle/kupo languages
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol

open Moogle_ast_exn
open Moogle_prog_env
open Moogle_prog_inst
open Moogle_prog_ocaml
open Moogle_prog_operand
open Moogle_prog_type
open Moogle_util
module AST = Moogle_ast_type


(***  Inline Kupo Processing  ***)


(* kupo_convert_type
   Convert an AST type to a program type.  ``new'' statements cannot
   construct new labels, nor integer values, but ``dcl'' statements
   accept these types.  Therefore the labelok flag is true if we are
   allowed to have label/integer types here.  *)
let kupo_convert_type loc labelok ty =
   let loc = string_pos "kupo_convert_type" loc in
   let requires_label ty =
      if labelok then
         ty
      else
         raise (ASTException (loc, TypeNotAllowed ty))
   in
      match ty with
         AST.TyReg ->
            TyReg
       | AST.TyFlReg ->
            TyFlReg
       | AST.TyLabel ->
            requires_label TyLabel
       | AST.TyOper ->
            requires_label TyOp
       | AST.TyInt ->
            requires_label TyInt
       | AST.TyInt31 ->
            requires_label TyInt31


(* kupo_consolidate_insts
   Consolidate kupo listbuf manipulations as much as possible.  *)
let kupo_consolidate_insts insts =
   let rec consolidate_insts = function
      KupoAddInsts insts1 :: KupoAddInsts insts2 :: insts ->
         consolidate_insts ((KupoAddInsts (insts1 @ insts2)) :: insts)
    | insts1 :: insts ->
         insts1 :: consolidate_insts insts
    | [] ->
         []
   in (* end of consolidate_insts *)
      consolidate_insts insts


(* kupo_process_insts
   Process instructions.  This function returns a final venv, decls
   and transformed list of instructions.  This function takes care
   of repair work on the instructions; initially they are emitted as
   separate Listbuf manipulations, but the returned list of manips
   will consolidate compatible listbuf manipulations whenever it is
   feasible.  *)
let kupo_process_insts prog insts =
   let make_pos pos = string_pos "kupo_process_insts" (exp_pos pos) in
   let rec process_insts venv decls insts =
      match insts with
         AST.OutInst (opcode, operands, pos) :: insts ->
            (* Convert the opcode and operands. *)
            let loc = make_pos pos in
            let operands = rewrite_arith_operands venv operands in
            let opcode, operands = process_inst prog loc opcode operands in
            let inst = KupoAddInsts [OutInst (opcode, operands, pos)] in
            let venv, decls, insts = process_insts venv decls insts in
               venv, decls, inst :: insts
       | AST.OutNew (vars, pos) :: insts ->
            (* Generate declarations for new register names. *)
            let venv, decls = List.fold_left (fun (venv, decls) (v, ty) ->
               let loc = make_pos pos in
               let ty = kupo_convert_type loc false ty in
               let venv = venv_add venv v ty in
               let decls = (v, ty, KupoNewVar) :: decls in
                  venv, decls) (venv, decls) vars
            in
               process_insts venv decls insts
       | AST.OutDecl (vars, pos) :: insts ->
            (* Add variable declarations to the list of declares. *)
            let venv, decls = List.fold_left (fun (venv, decls) (v, ty) ->
               let loc = make_pos pos in
               let ty = kupo_convert_type loc true ty in
               let venv = venv_add venv v ty in
               let decls = (v, ty, KupoDeclaredVar) :: decls in
                  venv, decls) (venv, decls) vars
            in
               process_insts venv decls insts
       | AST.OutDef (vars, pos) :: insts ->
            (* Add variable integer expressions to the list of declares. *)
            let venv, decls = List.fold_left (fun (venv, decls) (v, e) ->
               let e = rewrite_arith venv e in
               let venv = venv_add venv v TyInt in
               let decls = (v, TyInt, KupoDefinedVar e) :: decls in
                  venv, decls) (venv, decls) vars
            in
               process_insts venv decls insts
       | AST.OutExpr (ty, op, pos) :: insts ->
            (* Add an arbitrary OCaml call here. *)
            let file_pos = ocaml_file_pos_of_pos pos in
            let file_pos, text =
               match rewrite_arith_operand venv op with
                  OpExpr (file_pos, text, _) ->
                     file_pos, text
                | Op (v, pos) ->
                     ocaml_file_pos_of_pos pos, string_of_symbol v
                | _ ->
                     raise (ASTException (make_pos pos, StringError "invalid operand used for OutExpr statement"))
            in
            let inst =
               match ty with
                  AST.ExprAddInst ->
                     (* Single-instruction expressions use a normal
                        output instruction form.  *)
                     KupoAddInsts [OutExpr (file_pos, text, pos)]
                | AST.ExprAddList ->
                     (* To add a list of generated instructions to the
                        current listbuf we need to use a new listbuf
                        context.  *)
                     KupoAddExprList (file_pos, text, pos)
                | AST.ExprAddListbuf ->
                     (* To add a listbuf of generated instructions to
                        the current listbuf we need to use a new listbuf
                        context.  *)
                     KupoAddExprListbuf (file_pos, text, pos)
                | AST.ExprModifyListbuf ->
                     (* Instructions manipulating a listbuf must use
                        a new listbuf context, since we are clearly
                        going to trash the existing listbuf...  *)
                     KupoModifyExprListbuf (file_pos, text, pos)
            in
            let venv, decls, insts = process_insts venv decls insts in
               venv, decls, inst :: insts
       | AST.OutCopy (_, pos) :: _
       | AST.OutTransform (_, _, _, pos) :: _ ->
            raise (ASTException (make_pos pos, StringError "Inline kupo doesn't support this instruction form."))
       | [] ->
            venv, decls, []
   in (* end of process_insts *)

   (* Build the new instructions and declarations *)
   let venv, decls, insts = process_insts prog.prog_decls [] insts in
   let insts = kupo_consolidate_insts insts in
   let decls = List.rev decls in
      venv, decls, insts


(* kupo_process_block
   Process an OCaml code or Kupo block expression.  *)
let kupo_process_block prog block =
   match block with
      AST.OCamlCode (text, pos) ->
         let file_pos = ocaml_file_pos_of_pos pos in
            file_pos, OCamlCode (text, pos)
    | AST.KupoCode (buf, rtns, insts, pos) ->
         (* Get the declarations and transformed instructions. *)
         let venv, decls, insts = kupo_process_insts prog insts in

         (* Rewrite the buffer operand. *)
         let buf =
            match buf with
               AST.KupoAppendList buf ->
                  KupoAppendList buf
             | AST.KupoNewList buf ->
                  KupoNewList buf
         in

         (* Rewrite the returns list to include the types of each
            argument, to simplify the final stage of printing.  *)
         let rtns = rewrite_arith_operands venv rtns in

         (* Build the final KupoCode expression. *)
         let file_pos = ocaml_file_pos_of_pos pos in
            file_pos, KupoCode (buf, rtns, decls, insts, pos)


(* inline_kupo_of_expr
   Append an inline kupo block to the program.  *)
let inline_kupo_of_expr prog pos blocks =
   (* Process all transform blocks in this AST expression and append
      them to the Inline Kupo field listed in the program structure.  *)
   let blocks = List.map (kupo_process_block prog) blocks in
   let blocks = prog.prog_inline_kupo @ blocks in
      { prog with prog_inline_kupo = blocks }
