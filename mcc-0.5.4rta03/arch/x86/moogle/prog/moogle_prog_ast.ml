(*
   Program Processing for the moogle/kupo languages
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format

open Symbol
open Moogle_ast_exn
open Moogle_prog_inline
open Moogle_prog_type
open Moogle_prog_rule
open Moogle_prog_inst
open Moogle_prog_operand
open Moogle_util
module AST = Moogle_ast_type


(***  Program Processing  ***)


(* new_prog
   Returns a default, empty program. *)
let new_prog =
   { prog_file       = "";
     prog_name       = "<unnamed>";
     prog_trivial    = None;
     prog_decls      = SymbolTable.empty;
     prog_tags       = SymbolTable.empty;
     prog_insts      = SymbolTable.empty;
     prog_inst_rorder= [];
     prog_inst_type  = None;
     prog_transforms = SymbolTable.empty;
     prog_opens      = [];
     prog_rules      = [];
     prog_inline_kupo= [];
   }


(* open_of_expr
   Find all Open calls in an expression list.  *)
let rec open_of_expr prog pos name =
   { prog with prog_opens = name :: prog.prog_opens }


(* declarations_of_expr
   Finds and processes all variable declarations in a list of
   expressions.  Once processed, the resulting type environment
   is returned.  *)
let declarations_of_expr prog pos regs =
   let env = prog.prog_decls in
   let env = List.fold_left (fun env (v, ty) ->
      let ty =
         match ty with
            AST.TyReg ->
               TyReg
          | AST.TyFlReg ->
               TyFlReg
          | AST.TyLabel ->
               TyLabel
          | AST.TyInt ->
               TyInt
          | AST.TyInt31 ->
               TyInt31
          | AST.TyOper ->
               TyOp
      in
         SymbolTable.add env v ty) env regs
   in
      { prog with prog_decls = env }


(* name_of_expr
   Identifies a new name in the program.  *)
let name_of_expr prog pos name =
   { prog with prog_name = name }


(* trivial_of_expr
   Identifies the tag to determine trivial instructions.  *)
let trivial_of_expr prog pos ident =
   { prog with prog_trivial = Some ident }


(* inst_type_of_expr
   Identifies the name of the ML type for a single instruction.  *)
let inst_type_of_expr prog pos ident =
   { prog with prog_inst_type = Some ident }


(* prog_of_expr_list
   Process an expression list and build a program.  *)
let prog_of_expr prog file expr =
   (* Construct the filename *)
   let file =
      if prog.prog_file = "" then
         file
      else
         prog.prog_file ^ ", " ^ file
   in
   let prog = { prog with prog_file = file } in

   (* Process a single expression at a time, from left. *)
   let process_expr prog = function
      AST.Rule (inputs, outputs, pos) ->
         rules_of_expr prog pos inputs outputs
    | AST.Open (name, pos) ->
         open_of_expr prog pos name
    | AST.Name (name, pos) ->
         name_of_expr prog pos name
    | AST.Declarations (regs, pos) ->
         declarations_of_expr prog pos regs
    | AST.Instructions (insts, pos) ->
         insts_of_expr prog pos insts
    | AST.InstructionType (ty, pos) ->
         inst_type_of_expr prog pos ty
    | AST.Trivial (ident, pos) ->
         trivial_of_expr prog pos ident
    | AST.Transforms (name, args, transforms, pos) ->
         transforms_of_expr prog pos name args transforms
    | AST.InlineKupo (blocks, pos) ->
         inline_kupo_of_expr prog pos blocks
   in

   (* Construct the program *)
   let prog = List.fold_left process_expr prog expr in

   (* Fix-up some entries (such as open list, which is reversed *)
   let opens = List.rev prog.prog_opens in
   let prog =
      { prog with
         prog_opens = opens;
      }
   in

   (* Warnings *)
   let () =
      match prog.prog_inst_type with
         None ->
            eprintf "warning:  no instruction_type declaration was present, will not be able to declare many MLI values.@."
       | Some _ ->
            ()
   in
      prog
