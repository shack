(*
   Construct instructions from the AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_ast_exn
open Moogle_prog_type


val insts_of_expr          : prog -> pos -> Moogle_ast_type.instruction list -> prog
val transforms_of_expr     : prog -> pos -> symbol -> symbol list -> Moogle_ast_type.transform list -> prog
val process_inst           : prog -> exn_loc -> opcode -> 'a operand list -> opcode * 'a operand list
