(*
   Typechecking code
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Moogle_prog_exn
open Moogle_prog_type


(***  Type Checking  ***)


val check_type : ty SymbolTable.t -> exn_loc -> symbol -> ty -> unit
val check_arith : arith_type -> ty SymbolTable.t -> arith -> unit
val check_arith_operand : ty SymbolTable.t -> arith operand -> unit
