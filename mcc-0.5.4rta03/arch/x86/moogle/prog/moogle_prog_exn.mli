(*
   Exception handler for moogle/kupo Prog
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_prog_type


(* exn_loc
   Position/location information for exception handlers.  *)
type exn_loc = Moogle_ast_exn.exn_loc


(* exn
   The possible exception cases for the Prog.  *)
type exn =
   UnboundVar of symbol
 | TypeError of Moogle_prog_type.ty * Moogle_prog_type.ty
 | StringError of string


(* ProgException
   Raise an exception from the Prog.  *)
exception ProgException of exn_loc * exn


val vexp_pos      : symbol -> exn_loc
val exp_pos       : pos -> exn_loc
val string_pos    : string -> exn_loc -> exn_loc

val pos_of_operand   : 'a operand -> pos
val pos_of_arith     : arith -> pos

val pp_print_exn  : formatter -> exn_loc -> exn -> unit
val print_exn     : exn_loc -> exn -> unit
