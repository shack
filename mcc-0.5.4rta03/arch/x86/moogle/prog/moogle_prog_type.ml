(*
   Type definition for the moogle/kupo languages
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol


(***  Position, Debugging, Basic Types  ***)


type pos = Moogle_ast_type.pos

type ml_file_pos = string * int

type opcode = symbol


(* arith_type
   Indicate whether an integer operand should be treated as a raw integer,
   or an offset expression.  This affects type checking and printing...  *)
type arith_type =
   ArithInt32           (* Arithmetic expr used as a normal int *)
 | ArithOffset          (* Arithmetic expr used as an offset expr *)


(* unop
   binop
   relop
   Operators for arithmetic expressions.  *)
type unop =
   UMinusOp
 | UNotOp
 | UAbsOp
 | UPow2Op           (* Power of 2 *)
 | ULog2Op           (* Logarithm, base 2 *)

type binop =
   PlusOp
 | MinusOp
 | MulOp
 | DivOp
 | AndOp
 | OrOp
 | ShlOp             (* Logical/arithmetic shift left *)
 | ShrOp             (* Logical shift right *)
 | SarOp             (* Arithmetic shift right *)

type relop =
   EqOp
 | NeqOp
 | LtOp              (* Signed operations *)
 | LeOp
 | GtOp
 | GeOp
 | ULtOp             (* Unsigned operations *)
 | ULeOp
 | UGtOp
 | UGeOp

type boolop =
   BAndOp            (* Boolean, as in && *)
 | BOrOp             (* Boolean, as in || *)


(* ty
   Defines a simple type system for symbols.  The type system here is much
   more specific; we need to know exactly what various symbols were used
   to match against here.
      TyOp           Symbol used to match ANY operand.
      TyReg          Symbol used to match an integer register.
      TyFlReg        Symbol used to match a floating-point register.
      TyInt          Symbol used to match a 32-bit integer only.
      TyInt31        Symbol used to match a 31-bit (ML) integer only.
      TyOff          Symbol used to match an offset expression.
      TyInst         Symbol used to match exactly one instruction.
      TyInsts        Symbol used to match zero or more instructions.
      TyLabel        Symbol used to match a label name.

   TyInt always matches a real integer value; by contrast, TyOff is used
   for symbols which match offset (sub)expressions.  TyInsts is used for
   the Star input pattern, which matches multiple instructions, whose
   binding names are listed in the type.
 *)
type ty =
   TyOp
 | TyReg
 | TyFlReg
 | TyInt
 | TyInt31
 | TyOff
 | TyInst
 | TyInsts
 | TyLabel


(***  Program Type  ***)


(* operand
   Defines an operand of a symbol, and the type of operands and
   symbol within patterns.  Symbols should be resolved as required
   when they are printed, using the appropriate lookup environment.
   We also define the type of an instruction, which may include the
   copy command or a real instruction.  The operands are listed
   below:
      Op (v, pos)                   Any operand, named v.
      OpReg (v, pos)                Integer register named v.
      OpFlReg (v, pos)              Floating-point register named v.
      OpFPStack (a, pos)            Floating-point stack register a.
      OpMemReg (name, v, pos)       Memory pointer stored in register.
      OpMemRegOff (name, v, a, pos) Memory base/offset pointer.
      OpMemRegRegOffMul ...         Full Intel addressing mode.
      OpCons (v, pos)               ML constructor named v.
      OpInt (a, pos)                Integer expression/value.
      OpOff (a, pos)                Offset operand expression/value.
      OpLabel (label, pos)          Label name, for ImmediateLabel.
      OpString (text, pos)          String constant.
      OpExpr (file_pos, text, pos)  OCaml expression that computes a
                                    single operand.  This cannot occur
                                    in an input operand.

   Note that restrictions are not part of the operand specification at
   this point; restrictions have been pulled out into a higher-level
   data structure.  Some operands contain a ``name'' symbol; this is
   the name that the *entire* operand will be bound to; other symbols
   in the operand will be bound to specific parts of the operand only.
 *)
type 'value operand =
   Op                of symbol * pos
 | OpReg             of symbol * pos
 | OpFlReg           of symbol * pos
 | OpFPStack         of 'value * pos
 | OpMemReg          of symbol * symbol * pos
 | OpMemRegOff       of symbol * symbol * 'value * pos
 | OpMemRegRegOffMul of symbol * symbol * symbol * 'value * 'value * pos
 | OpCons            of symbol * pos
 | OpInt             of 'value * pos
 | OpOff             of 'value * pos
 | OpLabel           of symbol * pos
 | OpString          of string * pos
 | OpExpr            of ml_file_pos * string * pos


(* restriction
   Defines restrictions on an instruction pattern.  This incorporates both
   operand-level restrictions and instruction-level restrictions.  Some
   restrictions apply to named registers or operands; other restrictions
   apply to specific instructions which are named below:
      OperandsEqual (op1, op2, pos)    Two operands must be equivalent.
      OperandNotContain (r, op, pos)   Operand cannot refer to register r.
      OperandNotFPStack (op, pos)      Operand is not an FP stack register.
      OperandIsMem (op, pos)           Operand is a memory address.
      OperandSmall (op, pos)           Operand is not a large-only register.
      RegisterSmall (r, pos)           Register is a valid small register.
      RegistersNotEqual (r1, r2, pos)  Registers are NOT equivalent.
      IntegerEqual (v, i, pos)         Bound integer v is equal to constant.
      OffsetEqual (v, i, pos)          Bound offset v is a constant integer.
      InstPreserves (r, name, pos)     Named instruction preserves register r
      InstIgnores (r, name, pos)       Named instruction ignores register r.
      InstNotLiveI (r, name, pos)      Integer register r is not live out...
      InstNotLiveF (r, name, pos)      Float register r is not live out...
      InstEndscopeI (r, name, pos)     Integer register r scope ends here...
      InstEndscopeF (r, name, pos)     Float register r scope ends here...
      InstPreservesMem (name, pos)     Named instruction preserved memory.
      InstIgnoresMem (name, pos)       Named instruction ignores memory.
      InstIrrelevant (name, pos)       Named instruction is irrelevant.
      InstTag (tag, name, pos)         Named instruction has property tag.

   Wherever ``name'' occurs, the name of an instruction is expected.
 *)
type restriction =
   OperandsEqual     of symbol * symbol * pos
 | OperandNotContain of symbol * symbol * pos   (* register, operand *)
 | OperandNotFPStack of symbol * pos
 | OperandIsMem      of symbol * pos
 | OperandSmall      of symbol * pos
 | RegisterSmall     of symbol * pos
 | RegistersNotEqual of symbol * symbol * pos
 | IntegerEqual      of symbol * int32 * pos
 | OffsetEqual       of symbol * int32 * pos
 | InstPreserves     of symbol * symbol * pos   (* register, instruction *)
 | InstIgnores       of symbol * symbol * pos   (* register, instruction *)
 | InstNotLiveI      of symbol * symbol * pos   (* register, instruction *)
 | InstNotLiveF      of symbol * symbol * pos   (* register, instruction *)
 | InstEndscopeI     of symbol * symbol * pos   (* register, instruction *)
 | InstEndscopeF     of symbol * symbol * pos   (* register, instruction *)
 | InstPreservesMem  of symbol * pos
 | InstIgnoresMem    of symbol * pos
 | InstIrrelevant    of symbol * pos
 | InstTag           of symbol * symbol * pos   (* tag function, instruction *)


(* arith
   Simple arithmetic expressions:
      ArithInt (i, pos)             Represents integer i
      ArithReg (v, pos)             Represents variable v bound to int32.
      ArithReg31 (v, pos)           Represents variable v bound to int31.
      ArithOff (v, pos)             Represents an offset expression v
      ArithLabel (v, pos)           Represents a label (OffLabel) v
      ArithUnop (op, a, pos)        Represents a unary operation
      ArithBinop (op, a1, a2, pos)  Represents a binary operation

   The ArithOff constructor doesn't exist in the AST.  It is used for
   register names that occur as part of an offset expression; when a
   register matches a component of an offset, it matches an expression
   rather than a specific integer value, therefore it must be emitted
   differently.  ArithInt and ArithReg have type TyInt, and match/emit
   real integers; ArithOff has type TyOff, and matches/emits an offset
   expression.
 *)
type arith =
   ArithInt          of int32 * pos
 | ArithReg          of symbol * pos
 | ArithReg31        of symbol * pos
 | ArithOff          of symbol * pos
 | ArithLabel        of symbol * pos
 | ArithUnop         of unop * arith * pos
 | ArithBinop        of binop * arith * arith * pos


(* in_inst
   Defines an input/match instruction.  Instructions are listed below:
      InInst (name, opcode, args, rest, pos) Instruction with given opcode.
      InWild (name, rest, pos)               Wildcard, match 0+ instructions

   Instructions are bound to specific symbolic names, which are indicated
   in the ``name'' field.  For wildcard, the name will be a list of all
   instructions matched by the wildcard.
 *)
type in_inst =
   InInst            of symbol * opcode * symbol operand list * restriction list * pos
 | InWild            of symbol * restriction list * pos


(* out_inst
   Defines an output instruction.  The instructions are listed below:
      OutInst (opcode, args, pos)   Instruction with given opcode.
      OutCopy (name, islist, pos)   Clone instruction(s) with given name.
                                    If the boolean flag is true, then the
                                    name is bound to a list of instructions.
      OutTrans (f, inst, args, pos) Invoke transform function f on inst.
      OutExpr (filepos, text, pos)  An OCaml expression that computes
                                    exactly one instruction (this is only
                                    valid for output instructions).

   Copy copies instructions bound to the names in ``names'' list, and the
   Transform invokes transformation function ``f'', on the instruction
   named ``inst'', and with given additional arguments.  Note that the
   instruction passed to transform must match exactly one instruction (it
   may not refer to a wildcard).
 *)
type out_inst =
   OutInst           of opcode * arith operand list * pos
 | OutCopy           of symbol * bool * pos
 | OutTrans          of symbol * symbol * arith list * pos
 | OutExpr           of ml_file_pos * string * pos


(* clause
   Simple clause expressions:
      ClauseCompare (op, a1, a2, pos)  Compare two integer values.
      ClauseIsPower2 (a, pos)          Return true if a is a power of 2.
      ClauseBoolean (op, c1, c2, pos)  Combine two clauses with &&, ||.
 *)
type clause =
   ClauseCompare     of relop * arith * arith * pos
 | ClauseIspower2    of arith * pos
 | ClauseBoolean     of boolop * clause * clause * pos


(* in_pattern
   Defines an input pattern.  The fields are documented below:
      ipat_insts:       List of instructions for this input pattern.  Since
                        input patterns cannot directly match constants, the
                        value type is used as symbol here; the symbol is
                        restricted to a constant value in the restrictions,
                        listed later.
      ipat_venv:        List of symbols bound by this pattern, with their
                        types.  Used to determine what is bound in output
                        patterns.  The environment may include bindings for
                        hardware registers.  Includes generated names for
                        instruction labels, etc.
      ipat_clause:      Additional ``when'' clause applied to this input
                        pattern, which is an arbitrary boolean expression
                        and may refer to variables bound in venv.
 *)
type in_pattern =
   { ipat_insts         :  in_inst list;
     ipat_venv          :  ty SymbolTable.t;
     ipat_clause        :  clause option;
   }


(* out_pattern
   Defines an output pattern.  The fields are documented below:
      opat_insts:       List of instructions for this output pattern.  The
                        values in output patterns may be arithmetic expr's.
                        Variables referred to in the output pattern must
                        be bound, either by the corresponding input pattern
                        or a newly generated temporary name.
      opat_venv:        List of symbols bound for this pattern, with types.
                        Generally copied from the input pattern, but may
                        include newly generated temporaries.
      opat_clause:      Additional ``when'' clause applied to this output
                        pattern, which is an arbitrary boolean expression
                        and may refer to variables bound in venv.
      opat_new:         List of new temporaries that must be generated. No
                        type is required here, since the type has already
                        been qualified whenever the symbol is used in an
                        operand.
 *)
type out_pattern =
   { opat_insts         :  out_inst list;
     opat_venv          :  ty SymbolTable.t;
     opat_clause        :  clause option;
     opat_new           :  symbol list;
   }


(* rule
   Type of a rewrite-rule.  Rules can only contain a single input
   and a single output pattern, which is different from the language;
   you must do a bit of reprocessing to build multiple rules from a
   single InputOutput AST expression:
      rule_name:        Text name of a rule.  Multiple rules may (and
                        often will) have the same name.
      rule_input:       Exactly one input pattern.
      rule_output:      Exactly one output pattern.
 *)
type rule =
   { rule_name          :  string;
     rule_input         :  in_pattern;
     rule_output        :  out_pattern;
   }


(* inst_info
   Contains information about an instruction.  The tags are symbolic tags
   which may be attached to an instruction to determine its type.
      inst_constructor: Name of the ML constructor to build this instruction,
                        along with how to build its operands.  Symbols that
                        are referenced in the operands must be hardware reg
                        names, or symbols bound in inst_operands.
      inst_operands:    Names of the formals, used when actually building
                        the instruction.
      inst_tags:        Set of tags associated with the instruction.  This
                        is a SymbolTable to work around a long-standing bug
                        in SymbolSets...
 *)
type inst_info =
   { inst_constructor   :  symbol * symbol operand list;
     inst_operands      :  symbol operand list;
     inst_tags          :  unit SymbolTable.t;
   }


(* transform
   Type of a transformation.  This consists of the source
   opcode and operand names, as well as the revised opcode
   and revised operand names.
      transform_args:   Names of the formals for a transform function
                        (does not include the instruction being transformed,
                        which is implicitly passed in and matched against).
      transform_data:   List of rewrites, mapping original opcode with
                        indicated operands to a new opcode, with possibly
                        new operands.
 *)
type transform =
   { transform_args     :  symbol list;
     transform_data     :  (opcode * symbol operand list * opcode * arith operand list) list;
   }


(* kupo_list
   Operation to perform on a list of instructions in Kupo.  You can either
   append to an existing Listbuf.t, or create a new Listbuf.t.
      KupoAppendList ident    Append to existing listbuf named ident.
      KupoNewList ident       Create a new listbuf.  When creating a new
                              listbuf, an identifier must be specified only
                              if we escape to OCaml code that manipulates
                              the listbuf.
 *)
type kupo_list =
   KupoAppendList          of symbol
 | KupoNewList             of symbol option


(* kupo_decl
   Information on a declared Kupo variable.
      KupoNewVar              Variable is newly instantiated.
      KupoDeclaredVar         Variable is bound in surrounding OCaml code.
      KupoDefinedVar e        Variable is defined as integer expression e.
 *)
type kupo_decl =
   KupoNewVar
 | KupoDeclaredVar
 | KupoDefinedVar          of arith


(* kupo_listbuf_manip
   Describes a manipulation to perform on a current listbuf.  May be
   divided into several categories, depending on the operation.
      KupoAddInsts insts                     Add the output instructions
                                             in insts to listbuf.
      KupoAddExprList (filepos, expr, pos)   Call OCaml expression that
                                             computes a list to append.
      KupoAddExprListbuf (filepos, expr, pos)   Call OCaml expression that
                                             computes a listbuf to append.
      KupoModifyExprListbuf (filepos, expr, pos)  Call OCaml expression to
                                             modify the existing listbuf.

   Note the difference between KupoAddExprListbuf and KupoModifyExprListbuf.
 *)
type kupo_listbuf_manip =
   KupoAddInsts            of out_inst list
 | KupoAddExprList         of ml_file_pos * string * pos
 | KupoAddExprListbuf      of ml_file_pos * string * pos
 | KupoModifyExprListbuf   of ml_file_pos * string * pos


(* ocaml_block
   An OCaml block stream consists of raw text interspersed with lists of
   output instructions.  The variable binding rules are weak here since
   many of the variables are bound in the surrounding (unparsed) text, not
   by the Kupo sections.  Note that many output instructions (copy and
   transforms) are not valid in OCaml block code.
      OCamlCode (text, pos)                     Raw OCaml program text.
      KupoCode (buf, rtns, decls, insts, pos)   Instructions to emit.

   A note on decls.  All symbols which are declared in a block are listed
   in the decls, with their types.  The flag indicates what type of decl
   the variable was (was it in a ``new'', ``dcl'', or ``def'' statement?).
   If it is listed as a ``dcl'' variable, we assume it has already been
   bound in the surrounding OCaml code.

   The rtns is a list of operands that should be returned by the expression
   in addition to the Listbuf containing new instructions.  If non-empty,
   the operands will be returned in the listed order.  This is useful for
   returning the operands created by `new'' statements within the kupo
   block, for further use.
 *)
type ocaml_declare_vars = (symbol * ty * kupo_decl) list
type ocaml_return_vars = arith operand list
type ocaml_insts = kupo_listbuf_manip list

type ocaml_block =
   OCamlCode               of string * pos
 | KupoCode                of kupo_list * ocaml_return_vars * ocaml_declare_vars * ocaml_insts * pos


(* prog
   Type of an entire program.  The tags are a list of all the
   valid tags which may be associated with an instruction:
      prog_file:        Name of file(s) used to generate this program.
      prog_name:        Result of last ``name'' statement (current default).
      prog_trivial:     Name of the ``trivial'' instruction property.
      prog_decls:       List of variable declarations, along with types.
      prog_tags:        List of instruction properties that are defined.
      prog_insts:       List of valid instructions, indexed by opcode name.
      prog_inst_rorder: Actual ordering the instructions were defined in.
                        The order is relevant for generating valid property
                        classifiers, since the expressions matched by each
                        instruction may not be entirely disjoint.
      prog_inst_type:   Name of the instruction type in ML.
      prog_transforms:  List of transform functions, indexed by name.
      prog_opens:       List of ``open'' statements, in order of use.
      prog_rules:       List of rewrite rules, in order of definition.
      prog_inline_kupo: Code consisting of OCaml text with inlined Kupo
                        expressions.  Each InlineKupo block is concatenated
                        together in order, to form a single list of blocks.
                        The ml_file_pos header on each block refers to the
                        source file and start line number for the block.
 *)
type prog =
   { prog_file          :  string;
     prog_name          :  string;
     prog_trivial       :  symbol option;
     prog_decls         :  ty SymbolTable.t;
     prog_tags          :  unit SymbolTable.t;
     prog_insts         :  inst_info SymbolTable.t;
     prog_inst_rorder   :  symbol list;
     prog_inst_type     :  symbol option;
     prog_transforms    :  transform SymbolTable.t;
     prog_opens         :  symbol list;
     prog_rules         :  rule list;
     prog_inline_kupo   :  (ml_file_pos * ocaml_block) list;
   }
