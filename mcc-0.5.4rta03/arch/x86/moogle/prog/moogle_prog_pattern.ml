(*
   Construct patterns from the AST
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(*
   Only by mastering time itself do you stand a chance against Lavos.
      -- Belthasar, Chrono Trigger
 *)


open Format

open Symbol
open Moogle_ast_exn
open Moogle_prog_env
open Moogle_prog_operand
open Moogle_prog_type
open Moogle_prog_inst
open Moogle_util
open Moogle_state
module AST = Moogle_ast_type


(***  Pattern Construction  ***)


(* name_of_inst
   Names this instruction.  *)
let name_of_inst id =
   Symbol.add (Printf.sprintf "__inst%d" id)


(* new_in_pattern
   new_out_pattern
   Builds an empty pattern containing no clauses, instructions,
   or restrictions of any kind whatsoever.  How boring...  The
   output pattern form takes a type environment (which originates
   from an in_pattern) which is used to determine which symbols
   are bound and typed in the pattern.  The input pattern takes
   an initial environment which contains all the special symbols
   (i.e. the names of the machine registers).  *)
let new_in_pattern venv =
   { ipat_insts      =  [];
     ipat_venv       =  venv;
     ipat_clause     =  None;
   }

let new_out_pattern venv =
   { opat_insts      =  [];
     opat_venv       =  venv;
     opat_new        =  [];
     opat_clause     =  None;
   }


(***  Input Patterns  ***)


(* in_add_inst_restrictions
   Add instruction-level restrictions to the pattern given.  These are
   usually related to the liveness properties associated with inst, e.g.
   live_out.  The restrictions build from rest' are appended to the
   list of restrictions given in rest.  *)
let in_add_inst_restrictions prog name pattern rest rest' =
   let venv = pattern.ipat_venv in
   let add_restriction rest rest' =
      let pos = pos_of_inst_restriction rest' in
      let loc = string_pos "in_add_inst_restrictions" (exp_pos pos) in
         match rest' with
            AST.InstPreserves (v, _) ->
               (InstPreserves (v, name, pos)) :: rest
          | AST.InstIgnores (v, _) ->
               (InstIgnores (v, name, pos)) :: rest
          | AST.InstPreservesMem _ ->
               (InstPreservesMem (name, pos)) :: rest
          | AST.InstIgnoresMem _ ->
               (InstIgnoresMem (name, pos)) :: rest
          | AST.InstTag (tag, _) ->
               if venv_mem prog.prog_tags tag then
                  (InstTag (tag, name, pos)) :: rest
               else
                  raise (ASTException (loc, UnboundVar tag))
          | AST.InstIrrelevant _ ->
               (InstIrrelevant (name, pos)) :: rest
          | AST.InstNotLive (v, _) ->
               begin
               let ty = venv_lookup venv loc v in
                  match ty with
                     TyReg ->
                        (InstNotLiveI (v, name, pos)) :: rest
                   | TyFlReg ->
                        (InstNotLiveF (v, name, pos)) :: rest
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency1 ty))
            end (* InstNotLive *)
          | AST.InstEndscope (v, _) ->
               begin
               let ty = venv_lookup venv loc v in
                  match ty with
                     TyReg ->
                        (InstEndscopeI (v, name, pos)) :: rest
                   | TyFlReg ->
                        (InstEndscopeF (v, name, pos)) :: rest
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency1 ty))
               end (* InInstEndscope *)
   in
      List.fold_left add_restriction rest rest'


(* in_append_inst
   Adds a new instruction to the pattern.  Note that while we
   are appending an instruction, in implementation we PREPEND
   it to the list; someone needs to reverse the list at the
   end of it all. *)
let in_append_inst pattern inst =
   { pattern with ipat_insts = inst :: pattern.ipat_insts }


(* in_pattern_of_inst
   Setup a normal instruction for this pattern.  The second
   form iterates over a list of input patterns.  *)
let in_pattern_of_inst prog pos pattern count opcode operands rest' =
   let loc = string_pos "in_pattern_of_inst" (exp_pos pos) in
   let name = name_of_inst count in
   let venv, rest, operands = rewrite_match_operands pattern.ipat_venv [] operands in
   let venv = venv_add venv name TyInst in
   let pattern = { pattern with ipat_venv = venv } in
   let opcode, operands = process_inst prog loc opcode operands in
   let rest = in_add_inst_restrictions prog name pattern rest rest' in
   let pattern = in_append_inst pattern (InInst (name, opcode, operands, rest, pos)) in
      pattern

let in_patterns_of_inst prog pos patterns count opcode operands rest =
   List.map (fun pattern ->
      in_pattern_of_inst prog pos pattern count opcode operands rest) patterns


(* in_pattern_of_star
   Setup a star match, which matches zero or more instructions
   all of which must satisfy the common restrictions given. We
   implement this by forking, or creating multiple input
   patterns, with zero, one, two, ... fake instructions inserted
   to the pattern with appropriate restrictions.  We are limited
   to the maximum size of a rule, of course.  The copy aliases
   are setup so the virtual count'th instruction maps to the
   appropriate list of wildcard instructions. *)
let in_pattern_of_star prog pos pattern count rest =
   let loc = string_pos "in_patterns_of_star" (exp_pos pos) in
   let name = name_of_inst count in
   let rest = in_add_inst_restrictions prog name pattern [] rest in
   let venv = venv_add pattern.ipat_venv name TyInsts in
   let pattern = { pattern with ipat_venv = venv } in
   let pattern = in_append_inst pattern (InWild (name, rest, pos)) in
      pattern

let in_patterns_of_star prog pos patterns count rest =
   List.map (fun pattern ->
      in_pattern_of_star prog pos pattern count rest) patterns


(* in_patterns_of_expr
   Builds input patterns from an expression. *)
let in_patterns_of_expr prog pattern expr =
   (* Construct the list of input patterns from a list
      of instructions.  Count refers to the current pos
      of the InInst form (not number of instructions
      actually present in a given pattern!)  *)
   let in_patterns_of_inst patterns count inst =
      let pos = pos_of_in_inst inst in
         match inst with
            AST.InInst (opcode, operands, rest, _) ->
               in_patterns_of_inst prog pos patterns count opcode operands rest
          | AST.InStar (rest, _) ->
               in_patterns_of_star prog pos patterns count rest
   in
   let in_patterns_of_insts patterns insts =
      let patterns, _ =
         List.fold_left (fun (patterns, count) inst ->
            let patterns = in_patterns_of_inst patterns count inst in
               patterns, succ count) (patterns, 1) insts
      in
         patterns
   in

   (* Instructions are appended in reverse order;
      correct the ordering of the insts here.  This
      also adds the clause specified in the pattern *)
   let reverse_insts pattern =
      { pattern with ipat_insts = List.rev pattern.ipat_insts }
   in

   (* Compute the intersect of multiple venvs *)
   let compute_clause clause pattern =
      let clause = rewrite_clause_option pattern.ipat_venv clause in
         { pattern with ipat_clause = clause }
   in

   (* Process the instructions and build input patterns *)
   let AST.InPattern (insts, clause, _) = expr in
   let patterns = in_patterns_of_insts [pattern] insts in
   let patterns = List.map (compute_clause clause) patterns in
   let patterns = List.map reverse_insts patterns in
      patterns


(***  Output Patterns  ***)


(* out_pattern_of_inst
   Processes a single instruction in the pattern.
   This takes into account the restrictions on the
   instruction, such as preservation clauses.  *)
let out_pattern_of_inst prog pattern i inst =
   let pos = pos_of_out_inst inst in
   let loc = string_pos "out_pattern_of_inst" (exp_pos pos) in
      match inst with
         AST.OutInst (opcode, operands, _) ->
            let operands = rewrite_arith_operands pattern.opat_venv operands in
            let opcode, operands = process_inst prog loc opcode operands in
               pattern, [OutInst (opcode, operands, pos)]
       | AST.OutCopy (ls, _) ->
            let insts =
               List.fold_left (fun insts i ->
                  let i = name_of_inst (Int32.to_int i) in
                  let ty = venv_lookup pattern.opat_venv loc i in
                  let list =
                     match ty with
                        TyInst ->
                           false
                      | TyInsts ->
                           true
                      | _ ->
                           raise (ASTException (loc, TypeInconsistency1 ty))
                  in
                     OutCopy (i, list, pos) :: insts) [] ls
            in
            let insts = List.rev insts in
               pattern, insts
       | AST.OutTransform (transform, i, operands, _) ->
            if SymbolTable.mem prog.prog_transforms transform then
               let i = name_of_inst (Int32.to_int i) in
               let operands = List.map (rewrite_arith pattern.opat_venv) operands in
               let ty = venv_lookup pattern.opat_venv loc i in
                  match ty with
                     TyInst ->
                        pattern, [OutTrans (transform, i, operands, pos)]
                   | _ ->
                        raise (ASTException (loc, TypeInconsistency1 ty))
               else
                  raise (ASTException (loc, UnboundVar transform))
       | AST.OutNew (vars, _) ->
            let pattern = List.fold_left (fun pattern (v, ty) ->
               let ty =
                  match ty with
                     AST.TyReg ->
                        TyReg
                   | AST.TyFlReg ->
                        TyFlReg
                   | AST.TyLabel
                   | AST.TyOper
                   | AST.TyInt
                   | AST.TyInt31 ->
                        raise (ASTException (loc, StringError "label, operand types not allowed in new declarations."))
               in
               let venv = venv_add pattern.opat_venv v ty in
                  { pattern with opat_venv = venv;
                                 opat_new = v :: pattern.opat_new
                  }) pattern vars
            in
               pattern, []
       | AST.OutDecl _
       | AST.OutDef _
       | AST.OutExpr _ ->
            raise (ASTException (loc, StringError "this statement type is not allowed in a pattern"))


(* out_pattern_of_expr
   Produces a pattern from an expression list.  *)
let out_pattern_of_expr prog pattern expr =
   let rec out_pattern_of_insts pattern i insts =
      match insts with
         inst :: insts ->
            let pattern, insts' = out_pattern_of_inst prog pattern i inst in
            let pattern, insts = out_pattern_of_insts pattern (succ i) insts in
            let insts = insts' @ insts in
               pattern, insts
       | [] ->
            pattern, []
   in
   let AST.OutPattern (insts, clause, _) = expr in
   let clause = rewrite_clause_option pattern.opat_venv clause in
   let pattern, insts = out_pattern_of_insts pattern 1 insts in
      { pattern with opat_insts = insts;
                     opat_clause = clause }
