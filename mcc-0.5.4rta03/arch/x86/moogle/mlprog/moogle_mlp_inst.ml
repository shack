(*
   Construct ML code from instructions
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Moogle_prog_exn
open Moogle_mlp_env
open Moogle_mlp_operand
open Moogle_mlp_syms
open Moogle_mlp_type

module Prog = Moogle_prog_type


(***  Convert Instructions to ML Code  ***)


(* mlp_of_opcode
   mlp_of_match_opcode
   mlp_of_arith_opcode
   Emit the opcodes with their operands, ML-style.
   An atom constructor is returned in all cases.  *)
let mlp_of_opcode printer opcode operands =
   let const_name = opcode in
   let operands = List.map printer operands in
      AtomConst (const_name, operands)

let mlp_of_arith_opcode = mlp_of_opcode mlp_of_arith_operand
let mlp_of_match_opcode = mlp_of_opcode mlp_of_match_operand


(* mlp_of_inst_tag
   Emit an instruction tag block.  Appends to an existing ML buffer.  *)
let mlp_of_inst_tag inst_env rorder tag =
   let make_pos name = string_pos "mlp_of_inst_tag" (vexp_pos name) in
   let insts = List.fold_left (fun insts name ->
      venv_lookup inst_env (make_pos name) name :: insts) [] rorder
   in
   let mlp_of_inst inst =
      let match_pattern = mlp_of_match_opcode (fst inst.Prog.inst_constructor) (snd inst.Prog.inst_constructor) in
      let match_result = Value (AtomBool (venv_mem inst.Prog.inst_tags tag)) in
         match_pattern, None, match_result
   in
   let cases = List.map mlp_of_inst insts in
      Match (inst_var, cases)


(* mlp_of_inst_transform
   Prints out an instruction transform block.  Appends to buffer.  *)
let mlp_of_inst_transform args =
   let mlp_of_inst (opcode1, operand1, opcode2, operand2) =
      let match_pattern = mlp_of_match_opcode opcode1 operand1 in
      let match_result = mlp_of_arith_opcode opcode2 operand2 in
         match_pattern, None, Value match_result
   in
   let default_case = wildcard_var, None, Raise (AtomConst (failure_sym, [AtomString "bad transform"])) in
   let cases = List.map mlp_of_inst args in
   let cases = cases @ [default_case] in
      Match (inst_var, cases)
