(*
   Construct ML code for Program patterns
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol

open Moogle_prog_exn
open Moogle_mlp_operand
open Moogle_mlp_syms
open Moogle_mlp_type

module Prog = Moogle_prog_type


(***  Basic Conversion  ***)


(* mlp_of_clause
   Emit an ML clause. *)
let atom_of_relop op =
   let b =
      match op with
         Prog.EqOp ->
            "equal"
       | Prog.NeqOp ->
            "nequal"
       | Prog.LtOp ->
            "lt"
       | Prog.LeOp ->
            "le"
       | Prog.GtOp ->
            "gt"
       | Prog.GeOp ->
            "ge"
       | Prog.ULtOp ->
            "ult"
       | Prog.ULeOp ->
            "ule"
       | Prog.UGtOp ->
            "ugt"
       | Prog.UGeOp ->
            "uge"
   in
      AtomDot (moogle_offset_var, AtomVar (Symbol.add b))

let mlp_op_of_boolop op =
   match op with
      Prog.BAndOp ->
         BAndOp
    | Prog.BOrOp ->
         BOrOp

let mlp_of_arith32  = mlp_of_arith Prog.ArithInt32
let mlp_of_arithoff = mlp_of_arith Prog.ArithOffset

let rec mlp_of_clause clause =
   match clause with
      Prog.ClauseCompare (op, a, b, _) ->
         let clause = AtomFuncall (atom_of_relop op, [mlp_of_arithoff a; mlp_of_arithoff b]) in
            AtomBinop (EqOp, clause, moogle_offset_true_var)
    | Prog.ClauseIspower2 (a, _) ->
         let clause = AtomFuncall (moogle_offset_power2_var, [mlp_of_arithoff a]) in
            AtomBinop (EqOp, clause, moogle_offset_true_var)
    | Prog.ClauseBoolean (op, a, b, _) ->
         AtomBinop (mlp_op_of_boolop op, mlp_of_clause a, mlp_of_clause b)


(* mlp_of_clause
   This is the real clause builder.  This takes a Prog.clause and an
   atom option, and builds a new clause as an option.  If both the
   clause and the atom are None, then this returns None, indicating
   the continued absence of a clause. *)
let mlp_of_clause clause a =
   match clause, a with
      None, a ->
         a
    | Some clause, None ->
         Some (mlp_of_clause clause)
    | Some clause, Some a ->
         Some (AtomBinop (BAndOp, a, mlp_of_clause clause))


(***  Building a Match Pattern  ***)


(* mlp_of_match_operands
   Construct all operands for an instruction in a match statement.  *)
let mlp_of_match_operands operands =
   List.map mlp_of_match_operand operands


(* mlp_of_match_restrict
   Construct a single match restriction.  *)
let mlp_of_match_restrict rest =
   let dot_inst inst       = AtomDot (AtomVar inst, ic_inst_var) in
   let dot_live inst       = AtomDot (AtomVar inst, ic_live_var) in
   let bool_op op v1 v2    = AtomBinop (op, AtomVar v1, AtomVar v2) in
   let fun_op op v1 v2     = AtomFuncall (op, [AtomVar v1; AtomVar v2]) in
   let fun_op_live op v1 v2= AtomFuncall (op, [AtomVar v1; dot_live v2]) in
   let bool_int_op op v1 v2= AtomBinop (op, AtomVar v1, AtomInt32 v2) in
   let bool_off_op op v1 v2= AtomFuncall (op, [AtomVar v1; AtomInt32 v2]) in
   let unary_op op v       = AtomFuncall (op, [AtomVar v]) in
   let unary_op_live op v  = AtomFuncall (op, [dot_live v]) in
   let unary_op_inst op v  = AtomFuncall (op, [dot_inst v]) in
      match rest with
         Prog.OperandsEqual (v1, v2, _) ->
            bool_op EqOp v1 v2
       | Prog.OperandNotContain (v1, v2, _) ->
            fun_op not_in_operand_var v1 v2
       | Prog.OperandNotFPStack (v, _) ->
            unary_op not_fpstack_var v
       | Prog.OperandIsMem (v, _) ->
            unary_op is_memory_operand_var v
       | Prog.OperandSmall (v, _) ->
            unary_op operand_is_small_var v
       | Prog.RegisterSmall (v, _) ->
            unary_op register_is_small_var v
       | Prog.RegistersNotEqual (v1, v2, _) ->
            bool_op NEqOp v1 v2
       | Prog.OffsetEqual (v, i, _) ->
            bool_off_op moogle_offset_equal32_var v i
       | Prog.IntegerEqual (v, i, _) ->
            bool_int_op EqOp v i
       | Prog.InstPreserves (reg, inst, _) ->
            fun_op_live not_a_dst_var reg inst
       | Prog.InstIgnores (reg, inst, _) ->
            fun_op_live not_used_var reg inst
       | Prog.InstNotLiveI (reg, inst, _) ->
            fun_op_live not_live_outi_var reg inst
       | Prog.InstNotLiveF (reg, inst, _) ->
            fun_op_live not_live_outf_var reg inst
       | Prog.InstEndscopeI (reg, inst, _) ->
            fun_op_live end_of_scopei_var reg inst
       | Prog.InstEndscopeF (reg, inst, _) ->
            fun_op_live end_of_scopef_var reg inst
       | Prog.InstPreservesMem (inst, _) ->
            unary_op_live memory_not_dst_var inst
       | Prog.InstIgnoresMem (inst, _) ->
            unary_op_live memory_ignored_var inst
       | Prog.InstTag (tag, inst, _) ->
            unary_op_inst (AtomVar tag) inst
       | Prog.InstIrrelevant (inst, _) ->
            unary_op_live is_inst_live_empty_var inst


(* mlp_of_match_restrictions
   Builds an atom expression based off the list of restrictions given.  If
   no retrictions are present, then this returns None; otherwise, it returns
   an atom expression which evaluates to the boolean result of the rest's. *)
let mlp_of_match_restrictions rest =
   let build_rest a rest =
      let rest = mlp_of_match_restrict rest in
         match a with
            None ->
               Some rest
          | Some a ->
               Some (AtomBinop (BAndOp, a, rest))
   in
      List.fold_left build_rest None rest


(* mlp_of_match_restrictions
   Similar to the above expression, but this form returns either None or
   an ML expression containing the restrictions.  *)
let mlp_of_match_restrictions rest =
   match mlp_of_match_restrictions rest with
      None ->
         None
    | Some a_when ->
         Some (Value a_when)


(* mlp_build_block
   Converts the expression into a block.  This is used for when a Match
   expression has to be embedded inside another match case; we need to
   explicitly delimit the inner match so its cases are separate from the
   cases of the outer match.  This function adds the delimiters if they
   are necessary, assuming that e will be a case in another match.  *)
let mlp_build_block e =
   let rec ends_in_match = function
      LetIn (_, _, _, e)
    | LetRec (_, _, _, e)
    | Pragma (_, e) ->
         ends_in_match e
    | If (_, e1, e2) ->
         ends_in_match e1 || ends_in_match e2
    | Match _ ->
         true
    | Block _
    | Raise _
    | Value _ ->
         false
   in
      if ends_in_match e then
         Block e
      else
         e


(* mlp_of_match_insts
   Construct patterns for the instructions in a match statement.  *)
let rec mlp_of_match_insts insts a_match e_succ =
   match insts with
      [] ->
         e_succ
    | inst :: insts ->
         let e_succ = mlp_of_match_insts insts insts_var e_succ in
            match inst with
               Prog.InInst (name, opcode, operands, rest, _) ->
                  mlp_of_match_inst name opcode operands rest a_match e_succ
             | Prog.InWild (name, rest, _) ->
                  mlp_of_match_wild name rest a_match e_succ


(* mlp_of_match_inst
   Constructs a match statement to match a single instruction in an input
   pattern.  The expression will look something like this:

      match a_match with
         ({ ic_inst = opcode (operands) } as name) :: insts
         when restrictions_are_satisfied ->
            e_succ
       | _ ->
            None

   Note that the match statement will return None if the pattern
   for this instruction could not be applied.  e_succ may be another
   pattern applied to __insts; it should expect to find the remainder
   of the instructions in insts_var (defined to be insts), and is
   allowed to look at other variables defined by this pattern.  *)
and mlp_of_match_inst name opcode operands rest a_match e_succ =
   (* Build the pattern expression for this instruction. *)
   let a_inst =
      if Symbol.eq opcode wildcard_sym then
         AtomVar name
      else
         let a = AtomConst (opcode, mlp_of_match_operands operands) in
         let a = AtomAs (AtomStruct [ic_inst_sym, a], name) in
            a
   in
   let a_insts = AtomListCons ([a_inst], insts_var) in

   (* Build the ``when'' clause (if any). *)
   let e_when = mlp_of_match_restrictions rest in

   (* Build the actual match statement. *)
   let succ_case = a_insts, e_when, mlp_build_block e_succ in
   let fail_case = wildcard_var, None, Value (AtomConst (none_sym, [])) in
   let cases = [succ_case; fail_case] in
      Match (a_match, cases)


(* mlp_of_match_wild
   Matches zero or more instructions in an input pattern, subject to the
   indicated restrictions.  May be a longest-pattern or shortest-pattern
   match; see next two functions.  *)
and mlp_of_match_wild name rest a_match e_succ =
   if !Moogle_state.rule_wildcard_long then
      mlp_of_match_wild_long name rest a_match e_succ
   else
      mlp_of_match_wild_short name rest a_match e_succ


(* mlp_of_match_wild_long
   Constructs a recursive function designed to match 0 or more instructions
   in an input pattern, subject to indicated restrictions.  This recursive
   function is designed to take the *longest* possible match.  The code that
   is emitted will look something like this:

      (* Continuation function; exit from match loop *)
      let cont rev_wild_matched insts =
         (* Note: name is reversed, here *)
         let name = rev_wild_matched in
            e_succ
      in

      (* Match zero or more instructions which satisfy the restriction.
         rev_wild_matched is a *reversed* list of instructions that are
         already matched.  insts is a list of instructions to attempt
         to match against. *)
      let rec match_wild count rev_wild_matched insts =
         if count < wildcard_max then
            match insts with
               name :: insts'
               when restrictions_are_met ->
                  begin
                  let result =
                     match_wild (count + 1) (name :: rev_wild_matched) insts'
                  in
                     match result with
                        Some _ ->
                           result
                      | None ->
                           (* If the recursive call failed, then it means we
                              couldn't even apply this rule if we had ONLY
                              added insts to the matched list.  So we must
                              discard it as a match candidate here. *)
                           cont rev_wild_matched insts
                  end
             | _ ->
                  (* Cannot/will not match name. *)
                  cont rev_wild_matched insts
         else
            (* We are not allowed to match any other terms, so our only
               chance at this point is if the wildcard is able to match
               zero instructions. *)
            cont rev_wild_matched insts
      in
         match_wild 0 [] a_match

   This will return None if no expansion of the wildcard could match the
   instruction stream.  *)
and mlp_of_match_wild_long name rest a_match e_succ =
   let name_var = AtomVar name in

   (* Various ways to call the final (success) continuation ... *)
   let a_cont_call = AtomFuncall (cont_var, [rev_wild_match_var; insts_var]) in

   (* Build the recursive body component. *)
   let succ_case = AtomConst (some_sym, [wildcard_var]), None, Value result_var in
   let fail_case = AtomConst (none_sym, []), None, Value a_cont_call in
   let cases = [succ_case; fail_case] in
   let e_recursive = Match (result_var, cases) in
   let a_rev_wild_match = AtomListCons ([name_var], rev_wild_match_var) in
   let a_incr_count = AtomFuncall (succ_var, [count_var]) in
   let a_rec_call = AtomFuncall (match_wild_var, [a_incr_count; a_rev_wild_match; instsp_var]) in
   let e_recursive = Block (LetIn (result_sym, [], Value a_rec_call, e_recursive)) in

   (* Build the pattern match for a single instruction. *)
   let a_succ = AtomListCons ([name_var], instsp_var) in
   let e_when = mlp_of_match_restrictions rest in
   let succ_case = a_succ, e_when, e_recursive in
   let fail_case = wildcard_var, None, Value a_cont_call in
   let cases = [succ_case; fail_case] in
   let e_wild_body = Match (insts_var, cases) in
   let a_count_cmp = AtomBinop (LtOp, count_var, AtomInt !Moogle_state.rule_max_star) in
   let e_wild_body = If (a_count_cmp, e_wild_body, Value a_cont_call) in

   (* Build the final continuation function. *)
   let e_cont_body = LetIn (name, [], Value rev_wild_match_var, e_succ) in

   (* Build the wildcard code *)
   let e_succ = Value (AtomFuncall (match_wild_var, [AtomInt 0; AtomList []; a_match])) in
   let e_succ = LetRec (match_wild_sym, [count_sym; rev_wild_match_sym; insts_sym], e_wild_body, e_succ) in
   let e_succ = LetIn (cont_sym, [rev_wild_match_sym; insts_sym], e_cont_body, e_succ) in
      e_succ


(* mlp_of_match_wild_short
   Constructs a recursive function designed to match 0 or more instructions
   in an input pattern, subject to indicated restrictions.  This recursive
   function is designed to take the *shortest* possible match. The code that
   is emitted will look something like this:

      (* Continuation function; exit from match loop *)
      let cont rev_wild_matched insts =
         (* Note: name is reversed, here *)
         let name = rev_wild_matched in
            e_succ
      in

      (* Match zero or more instructions which satisfy the restriction.
         rev_wild_matched is a *reversed* list of instructions that are
         already matched.  insts is a list of instructions to attempt
         to match against. *)
      let rec match_wild count rev_wild_matched insts =
         let result = cont rev_wild_matched insts in
            match result with
               Some _ ->
                  (* Success with this match *)
                  result
             | None ->
                  (* No luck with this match; try to add another
                     instruction, if possible. *)
                  if count < wildcard_max then
                     match insts with
                        name :: insts
                        when restrictions_are_met ->
                           match_wild (count + 1)
                                      (name :: rev_wild_matched)
                                      insts
                      | _ ->
                           None
      in
         match_wild 0 [] a_match

   This will return None if no expansion of the wildcard could match the
   instruction stream.  *)
and mlp_of_match_wild_short name rest a_match e_succ =
   let name_var = AtomVar name in

   (* Build the recursive body component. *)
   let a_incr_count = AtomFuncall (succ_var, [count_var]) in
   let a_rev_wild_match = AtomListCons ([name_var], rev_wild_match_var) in
   let a_recursive = AtomFuncall (match_wild_var, [a_incr_count; a_rev_wild_match; insts_var]) in
   let e_when = mlp_of_match_restrictions rest in
   let succ_case = AtomListCons ([name_var], insts_var), e_when, Value a_recursive in
   let fail_case = wildcard_var, None, Value (AtomConst (none_sym, [])) in
   let cases = [succ_case; fail_case] in
   let e_recursive = Match (insts_var, cases) in

   (* Toss in the upper bound on the wildcard size *)
   let a_count_cmp = AtomBinop (LtOp, count_var, AtomInt !Moogle_state.rule_max_star) in
   let e_recursive = If (a_count_cmp, e_recursive, Value (AtomConst (none_sym, []))) in

   (* Build the outer match statement that matches against the result of
      the continuation call.  *)
   let succ_case = AtomConst (some_sym, [wildcard_var]), None, Value result_var in
   let fail_case = AtomConst (none_sym, []), None, e_recursive in
   let cases = [succ_case; fail_case] in
   let e_recursive = Match (result_var, cases) in

   (* Build the call that assigns result, based on the continuation call. *)
   let a_cont_call = AtomFuncall (cont_var, [rev_wild_match_var; insts_var]) in
   let e_recursive = LetIn (result_sym, [], Value a_cont_call, e_recursive) in

   (* Build the final continuation function. *)
   let e_cont_body = LetIn (name, [], Value rev_wild_match_var, e_succ) in

   (* Build the wildcard code *)
   let e = Value (AtomFuncall (match_wild_var, [AtomInt 0; AtomList []; a_match])) in
   let e = LetRec (match_wild_sym, [count_sym; rev_wild_match_sym; insts_sym], e_recursive, e) in
   let e = LetIn (cont_sym, [rev_wild_match_sym; insts_sym], e_cont_body, e) in
      e


(* mlp_of_match_pattern
   Emits a pattern suitable for a match statement.  *)
let mlp_of_match_pattern name out_nreg out_clause pattern a_match e_succ e_fail =
   (* Build wrapper on e_succ; if there is an input/output clause, it must
      be applied to the innermost expression, since it may refer to vars
      that are bound in the input pattern. *)
   let clause = mlp_of_clause pattern.Prog.ipat_clause None in
   let clause = mlp_of_clause out_clause clause in
   let e_succ =
      match clause with
         None ->
            e_succ
       | Some a_clause ->
            If (a_clause, e_succ, Value (AtomConst (none_sym, [])))
   in

   (* Build toplevel expression and main match expression *)
   let e = mlp_of_match_insts pattern.Prog.ipat_insts a_match e_succ in
   let succ_case = AtomConst (some_sym, [result_var]), None, Value result_var in
   let fail_case = AtomConst (none_sym, []), None, e_fail in
   let cases = [succ_case; fail_case] in
   let e = LetIn (result_sym, [], e, Match (result_var, cases)) in

   (* Clause component that considers nreg. *)
   let e =
      if out_nreg then
         If (nreg_var, e, e_fail)
      else
         e
   in

      (* That's all! *)
      Pragma (Comment name, e)


(***  Writing an output pattern  ***)


(* mlp_of_output_operands
   Emits the output operands. *)
let mlp_of_output_operands operands =
   List.map mlp_of_arith_operand operands


(* mlp_of_output_inst
   Emits an instruction. *)
let mlp_of_output_inst pattern inst e' =
   let make_pos pos = string_pos "mlp_of_output_inst" (exp_pos pos) in
   let e =
      match inst with
         Prog.OutInst (opcode, operands, _) ->
            Value (AtomFuncall (listbuf_add_var, [buf_var; AtomConst (opcode, mlp_of_output_operands operands)]))
       | Prog.OutCopy (v, true, pos) ->
            let f_sym, f_var = build_sym_var_local "__inst_of_ic" in
            let e_live_inst = Value (AtomDot (v_var, ic_inst_var)) in
            let a_live_inst = AtomFuncall (list_map_var, [f_var; AtomVar v]) in
            let e = Value (AtomFuncall (listbuf_add_list_var, [buf_var; a_live_inst])) in
            let e = LetIn (f_sym, [v_sym], e_live_inst, e) in
               e
       | Prog.OutCopy (v, false, pos) ->
            let live_inst = AtomDot (AtomVar v, ic_inst_var) in
               Value (AtomFuncall (listbuf_add_var, [buf_var; live_inst]))
       | Prog.OutTrans (transform, i, operands, pos) ->
            let i = AtomDot (AtomVar i, ic_inst_var) in
            let args = List.map mlp_of_arith32 operands in
            let a_call = AtomFuncall (AtomVar transform, i :: args) in
               Value (AtomFuncall (listbuf_add_var, [buf_var; a_call]))
       | Prog.OutExpr (_, _, pos) ->
            raise (ProgException (make_pos pos, StringError "this instruction type is not allowed in output instructions"))
   in
      LetIn (buf_sym, [], e, e')


let mlp_of_output_new pattern e =
   let rec print_new vars =
      match vars with
         v :: vars ->
            let e = print_new vars in
            let nstr = Symbol.to_string v in
               LetIn (v, [], Value (AtomFuncall (new_symbol_string_var, [AtomString nstr])), e)
       | [] ->
            e
   in
      print_new pattern.Prog.opat_new


(* mlp_append_comments
   Append comments from matched instructions to the output buffer.  *)
let mlp_append_comments inst e =
   let e_append =
      match inst with
         Prog.InInst (name, _, _, _, _) ->
            let a_comments = AtomDot (AtomVar name, ic_comments_var) in
            let a_add_comm = AtomFuncall (listbuf_add_list_var, [buf_var; a_comments]) in
               Value a_add_comm
       | Prog.InWild (name, _, _) ->
            let f_sym, f_var = build_sym_var_local "__add_comments" in
            let a_comments = AtomDot (inst_var, ic_comments_var) in
            let a_add_comm = AtomFuncall (listbuf_add_list_var, [buf_var; a_comments]) in
            let f_body = Value a_add_comm in
            let a_call = AtomFuncall (list_fold_left_var, [f_var; buf_var; AtomVar name]) in
            let e = LetIn (f_sym, [buf_sym; inst_sym], f_body, Value a_call) in
               e
   in
      LetIn (buf_sym, [], e_append, e)


(* mlp_reverse_wildcards
   Reverse the lists that store wildcard matches.  These lists are
   generated in reverse order for efficiency's sake, above; they
   need to be reversed before we attempt to use them.  We defer the
   actual reversal until this point so we don't waste computation
   time if the rule could not be applied.  *)
let mlp_reverse_wildcards inst e =
   match inst with
      Prog.InInst _ ->
         e
    | Prog.InWild (name, _, _) ->
         LetIn (name, [], Value (AtomFuncall (list_rev_var, [AtomVar name])), e)


(* mlp_of_output_insts
   Emits a list of instructions. *)
let mlp_of_output_insts rule ipattern opattern =
   (* Step 1: append comments associated with old instructions.
      This has to be done within the builder currently, because
      the rule_build function doesn't know the names of all the
      instructions that were skipped.  This step also takes
      care of reversing wildcard match lists (they are built
      in reverse order, see the wildcard code above).  *)
   let rec step_1 insts e =
      match insts with
         inst :: insts ->
            (* Note: wildcards need to be reversed before we process
               the comments in the wildcard.  The reverse_wildcards
               call should appear LAST, this will make it the first
               instruction actually executed in the ML code. *)
            let e = step_1 insts e in
            let e = mlp_append_comments inst e in
            let e = mlp_reverse_wildcards inst e in
               e
       | [] ->
            e
   in (* end of step_1 *)

   (* Step 2: allocate all new register names *)
   let step_2 e = mlp_of_output_new opattern e in

   (* Step 3: append new instructions to the output buffer *)
   let rec step_3 insts e =
      match insts with
         inst :: insts ->
            let e = step_3 insts e in
            let e = mlp_of_output_inst opattern inst e in
               e
       | [] ->
            e
   in (* end of step_3 *)

   (* Step 4: Return the value of the buffer *)
   let step_4 = Value buf_var in
   let e = step_1 ipattern.Prog.ipat_insts (step_2 (step_3 opattern.Prog.opat_insts step_4)) in

   let build_sym, build_var = build_sym_var_local "__builder" in
   let a_build = AtomFuncall (rule_build_var, [ruledata_var; insts_var; AtomString rule; build_var]) in
   let a_build = AtomConst (some_sym, [a_build]) in
      LetIn (build_sym, [buf_sym], e, Value a_build)


(* mlp_of_output_pattern
   Emits an output pattern rule. *)
let mlp_of_output_pattern rule ipattern opattern =
   mlp_of_output_insts rule ipattern opattern
