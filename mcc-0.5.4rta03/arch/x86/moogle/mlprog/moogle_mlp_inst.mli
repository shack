(*
   Construct ML code from instructions
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Moogle_mlp_type


val mlp_of_inst_tag : Moogle_prog_type.inst_info SymbolTable.t -> symbol list -> symbol -> expression
val mlp_of_inst_transform : (Moogle_prog_type.opcode * symbol Moogle_prog_type.operand list *
                             Moogle_prog_type.opcode * Moogle_prog_type.arith Moogle_prog_type.operand list) list -> expression
