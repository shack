(*
   Print an ML program
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol

open Moogle_mlp_ocaml
open Moogle_mlp_type
open Moogle_util


(***  Basic Printers  ***)


(* pp_print_pragma
   Print out a pragma definition.  This will inherit the box of the caller,
   although line info pragmas will often significantly distort the boxes
   anywho...  *)
let pp_print_pragma buf pragma =
   match pragma with
      Comment text ->
         fprintf buf "(* @[<v 0>";
         pp_print_ocaml_block_text buf text;
         fprintf buf "@] *)@ "
    | LineInfo (file_pos, mark) ->
         pp_print_ocaml_line_break buf file_pos mark


(* pp_print_delim_space_list
   Print a list of elements, separated by delim followed by print_space.  *)
let pp_print_delim_space_list buf delim f ls =
   let fold_fun first l =
      if not first then
         fprintf buf "%s@ " delim;
      f buf l;
      false
   in
      ignore (List.fold_left fold_fun true ls)


(* pp_print_space_delim_list
   Print a list of elements, separated by print_space followed by delim.
   This prints the delimiter and space in the opposite order of above...  *)
let pp_print_space_delim_list buf delim f ls =
   let fold_fun first l =
      if not first then
         fprintf buf "@ %s" delim;
      f buf l;
      false
   in
      ignore (List.fold_left fold_fun true ls)


(***  Atom Printing  ***)


(* atom_expr_type
   Determines the type of an atom expression, which is used to
   determine if a child atom can be printed without explicit
   parenthesization inside of a parent atom.  *)
type atom_expr_type =
   ATSafe
 | ATDot
 | ATBinop of binop
 | ATList
 | ATListCons
 | ATTuple
 | ATConst
 | ATAs
 | ATFuncall


(* safe_atom
   Returns true if an atom of type self can be printed within an
   atom of type parent without explicit parenthesization around a.  *)
let safe_atom parent self =
   match parent, self with
      ATSafe, _
      (* Dot expressions bind very tightly *)
    | (ATDot | ATBinop _ | ATList | ATListCons | ATTuple | ATConst | ATFuncall), ATDot
      (* Binary operators must consider precedence *)
    | ATBinop BAndOp, ATBinop BAndOp
    | ATBinop BOrOp, ATBinop BOrOp
    | ATBinop (BAndOp | BOrOp), ATBinop (EqOp | NEqOp | LtOp)
    | ATBinop _, ATFuncall
      (* Structures which are permissible inside lists... *)
    | (ATList | ATListCons | ATTuple), (ATBinop _ | ATList | ATFuncall | ATConst)
    | ATList, (ATListCons | ATTuple)
      (* Tuples can contain lists (not vice versa) *)
    | ATTuple, ATListCons
      (* Single-argument constructors are not parenthesized, so if we
         have a constructor parent, we cannot accept arithmetic ops. *)
    | ATConst, ATList
    | ATFuncall, ATList ->
         true

      (* Cases which are not safe *)
    | ATDot, (ATBinop _ | ATList | ATListCons | ATTuple | ATConst | ATFuncall)
    | ATBinop (EqOp | NEqOp | LtOp), ATBinop _
    | ATBinop BAndOp, ATBinop BOrOp
    | ATBinop BOrOp, ATBinop BAndOp
    | ATBinop _, (ATList | ATListCons | ATTuple | ATConst)
    | ATListCons, (ATListCons | ATTuple)
    | ATTuple, ATTuple
    | ATConst, (ATBinop _ | ATListCons | ATTuple | ATConst | ATFuncall)
    | ATFuncall, (ATBinop _ | ATListCons | ATTuple | ATConst | ATFuncall)
      (* Don't know where I need to be strict about as *)
    | ATAs, _
    | _, ATAs
    | _, ATSafe ->
         false


(* pp_print_binop
   Print an infix binary operator.  *)
let pp_print_binop buf op =
   match op with
      BAndOp ->
         fprintf buf "&&"
    | BOrOp ->
         fprintf buf "||"
    | EqOp ->
         fprintf buf "="
    | NEqOp ->
         fprintf buf "<>"
    | LtOp ->
         fprintf buf "<"


(* pp_print_atom
   Prints an atom.  The parent indicates the type of the parent atom, and is
   used to determine if this atom expression must be parenthesized.  External
   calls to pp_print_atom should use ATSafe for the parent type.  *)
let rec pp_print_atom buf parent atom =
   let parens_if_unsafe self f =
      let not_safe = not (safe_atom parent self) in
         if not_safe then
            pp_print_string buf "(";
         f ();
         if not_safe then
            pp_print_string buf ")"
   in
      match atom with
         AtomVar v ->
            fprintf buf "%s" (string_of_symbol v)
       | AtomBool b ->
            fprintf buf "%b" b
       | AtomInt i ->
            fprintf buf "%d" i
       | AtomInt32 i ->
            fprintf buf "%s" (string_of_int32 i)
       | AtomString s ->
            fprintf buf "\"%s\"" (String.escaped s)
       | AtomDot (a1, a2) ->
            pp_print_atom buf ATDot a1;
            fprintf buf ".";
            pp_print_atom buf ATDot a2;
       | AtomBinop (op, a1, a2) ->
            parens_if_unsafe (ATBinop op) (fun () ->
               fprintf buf "@[<hov 0>";
               pp_print_atom buf (ATBinop op) a1;
               fprintf buf "@ ";
               pp_print_binop buf op;
               fprintf buf "@ ";
               pp_print_atom buf (ATBinop op) a2;
               fprintf buf "@]")
       | AtomList atoms ->
            fprintf buf "[@[<hov 0>";
            pp_print_delim_space_list buf ";" (fun buf -> pp_print_atom buf ATList) atoms;
            fprintf buf "@]]";
       | AtomListCons (atoms, tl) ->
            parens_if_unsafe ATListCons (fun () ->
               fprintf buf "@[<hv 0>";
               pp_print_delim_space_list buf " ::" (fun buf -> pp_print_atom buf ATListCons) (atoms @ [tl]);
               fprintf buf "@]")
       | AtomTuple atoms ->
            parens_if_unsafe ATTuple (fun () ->
               fprintf buf "@[<hov 0>";
               pp_print_delim_space_list buf "," (fun buf -> pp_print_atom buf ATTuple) atoms;
               fprintf buf "@]")
       | AtomStruct fields ->
            fprintf buf "{ @[<v 0>";
            pp_print_delim_space_list buf ";" (fun buf (field, a) ->
               fprintf buf "%s = " (string_of_symbol field);
               pp_print_atom buf ATSafe a) fields;
            fprintf buf "@] }";
       | AtomConst (name, atoms) ->
            begin
            match atoms with
               [] ->
                  fprintf buf "%s" (string_of_symbol name)
             | [atom] ->
                  parens_if_unsafe ATConst (fun () ->
                     fprintf buf "@[<hov 3>%s" (string_of_symbol name);
                     fprintf buf "@ ";
                     pp_print_atom buf ATConst atom;
                     fprintf buf "@]")
             | _ ->
                  parens_if_unsafe ATConst (fun () ->
                     fprintf buf "@[<hov 3>%s" (string_of_symbol name);
                     fprintf buf "@ (";
                     pp_print_delim_space_list buf "," (fun buf -> pp_print_atom buf ATConst) atoms;
                     fprintf buf ")@]")
            end
       | AtomAs (a, name) ->
            parens_if_unsafe ATAs (fun () ->
               fprintf buf "@[<hov 0>";
               pp_print_atom buf ATAs a;
               fprintf buf "@ as %s@]" (string_of_symbol name))
       | AtomOCaml text ->
            fprintf buf "(@[<v -9999>";
            pp_print_ocaml_block_text buf text;
            fprintf buf "@])"
       | AtomFuncall (f, atoms) ->
            parens_if_unsafe ATFuncall (fun () ->
               fprintf buf "@[<hov 3>";
               pp_print_atom buf ATFuncall f;
               List.iter (fun a ->
                  fprintf buf "@ ";
                  pp_print_atom buf ATFuncall a) atoms;
               fprintf buf "@]")
       | AtomPragma (p, a) ->
            fprintf buf "@[<v 0>";
            pp_print_pragma buf p;
            pp_print_atom buf parent a;
            fprintf buf "@]"


(***  Expression/Statement Printers  ***)


(* pp_print_match_cases
   Print out the cases in a match statement.  *)
let rec pp_print_match_cases buf cases =
   let pp_print_match_case buf (pattern, clause, e) =
      fprintf buf "@[<v 3>";
      (match clause with
         None ->
            pp_print_atom buf ATSafe pattern;
            fprintf buf " ->@ "
       | Some e ->
            fprintf buf "@[<v 0>";
            pp_print_atom buf ATSafe pattern;
            fprintf buf "@ @[<hv 3>when@ ";
            pp_print_expression buf e;
            fprintf buf "@]@ ->@]@ ");
      pp_print_expression buf e;
      fprintf buf "@]"
   in
      fprintf buf "   ";
      pp_print_space_delim_list buf " | " pp_print_match_case cases


(* pp_print_expression
   Print out an entire expression.  This call may recurse.  *)
and pp_print_expression buf expr =
   fprintf buf "@[<v 0>";
   (match expr with
      Value a ->
         pp_print_atom buf ATSafe a
    | Raise a ->
         fprintf buf "raise (";
         pp_print_atom buf ATSafe a;
         fprintf buf ")"
    | LetIn (v, args, e1, e2) ->
         fprintf buf "@[<hv 3>let@[<h>";
         List.iter (fun v -> fprintf buf "@ %s" (string_of_symbol v)) (v :: args);
         fprintf buf "@] =@ ";
         pp_print_expression buf e1;
         fprintf buf " in@]@ ";
         pp_print_expression buf e2
    | LetRec (v, args, e1, e2) ->
         fprintf buf "@[<hv 3>let rec@[<h>";
         List.iter (fun v -> fprintf buf "@ %s" (string_of_symbol v)) (v :: args);
         fprintf buf "@] =@ ";
         pp_print_expression buf e1;
         fprintf buf " in@]@ ";
         pp_print_expression buf e2
    | Match (a, cases) ->
         fprintf buf "@[<v 0>match ";
         pp_print_atom buf ATSafe a;
         fprintf buf " with@ ";
         pp_print_match_cases buf cases;
         fprintf buf "@]"
    | If (a, e1, e2) ->
         fprintf buf "@[<v 0>@[<hov 3>if@ ";
         pp_print_atom buf ATSafe a;
         fprintf buf "@]@ @[<v 3>then@ ";
         pp_print_expression buf e1;
         fprintf buf "@]@ @[<v 3>else@ ";
         pp_print_expression buf e2;
         fprintf buf "@]@]"
    | Block e ->
         fprintf buf "@[<v 0>begin@ ";
         pp_print_expression buf e;
         fprintf buf "@ end@]"
    | Pragma (p, e) ->
         pp_print_pragma buf p;
         pp_print_expression buf e);
   fprintf buf "@]"


(* pp_print_statement
   Print out a top-level ML statement.  *)
let pp_print_statement buf stmt =
   match stmt with
      Open name ->
         fprintf buf "open %s@ @ " (string_of_symbol name)
    | Let (v, args, e) ->
         fprintf buf "@[<hv 3>let@[<h>";
         List.iter (fun v -> fprintf buf "@ %s" (string_of_symbol v)) (v :: args);
         fprintf buf "@] =@ ";
         pp_print_expression buf e;
         fprintf buf "@]@ @ "
    | OCaml text ->
         pp_print_ocaml_block_text buf text;
    | Expression e ->
         pp_print_expression buf e;
         fprintf buf "@ "
    | TopPragma p ->
         pp_print_pragma buf p


(***  Interface Statements  ***)


(* pp_print_itype
   Print out a type for an MLI file.  *)
let rec pp_print_itype buf ty =
   match ty with
      ITyName name ->
         fprintf buf "%s" (string_of_symbol name)
    | ITyFun (t1, t2) ->
         fprintf buf "@[<hov 0>(";
         pp_print_itype buf t1;
         fprintf buf ") ->@ ";
         pp_print_itype buf t2;
         fprintf buf "@]"
    | ITyDot (t1, t2) ->
         pp_print_itype buf t1;
         fprintf buf ".";
         pp_print_itype buf t2
    | ITyApply (t1, t2) ->
         fprintf buf "@[<hov 0>(";
         pp_print_itype buf t1;
         fprintf buf ")@ ";
         pp_print_itype buf t2;
         fprintf buf "@]"


(* pp_print_istatement
   Print out a top-level MLI statement.  *)
let pp_print_istatement buf stmt =
   match stmt with
      IOpen name ->
         fprintf buf "open %s@ @ " (string_of_symbol name)
    | IVal (v, ty) ->
         fprintf buf "@[<hv 3>val %s :@ " (string_of_symbol v);
         pp_print_itype buf ty;
         fprintf buf "@]@ @ "
    | ITopPragma p ->
         pp_print_pragma buf p


(***  External Interface  ***)


(* pp_print_program
   Print an ML file.  *)
let pp_print_program buf prog =
   let prog = Listbuf.to_list prog in
      fprintf buf "@[<v 0>";
      List.iter (pp_print_statement buf) prog;
      fprintf buf "@]@."


(* pp_print_interface
   Print an MLI file.  *)
let pp_print_interface buf prog =
   let prog = Listbuf.to_list prog in
      fprintf buf "@[<v 0>";
      List.iter (pp_print_istatement buf) prog;
      fprintf buf "@]@."


