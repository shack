(*
   ML Program Data Structure
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol


(* ml_file_pos
   File position information (imported from Prog).  *)
type ml_file_pos = Moogle_prog_type.ml_file_pos


(***  Program Code (ML file)  ***)


(* binop
   Operators for binary atom trees.  *)
type binop =
   BAndOp
 | BOrOp
 | EqOp
 | NEqOp
 | LtOp


(* pragma
   Types of pragma directives.
      Comment text               ML comment.
      LineInfo (pos, mark)       ML line information.
 *)
type pragma =
   Comment        of string
 | LineInfo       of ml_file_pos * bool


(* atom
   Various types of primitive atoms for ML.
      AtomVar v                  Variables.
      AtomBool b                 Boolean values.
      AtomInt i                  ML (31-bit) integers.
      AtomInt32 i                32-bit integers.
      AtomString text            Emit a string.
      AtomDot (a, b)             Emit projection.
      AtomList atoms             Emit a list.
      AtomListCons (atoms, tl)   Emit a ::'d list.
      AtomTuple atoms            Emit a tuple.
      AtomStruct fields          Emit a structure definition.
      AtomConst (name, args)     Emit a constructor.
      AtomAs (a, name)           Emit as binding (patterns only!).
      AtomOCaml text             Emit a raw OCaml expression.
      AtomBinop (binop, a1, a2)  Emit a binary operation.
      AtomFuncall (f, args)      Emit a call to function f.
      AtomPragma (p, a)          Pragma information.
 *)
type atom =
   AtomVar        of symbol
 | AtomBool       of bool
 | AtomInt        of int
 | AtomInt32      of int32
 | AtomString     of string
 | AtomDot        of atom * atom
 | AtomList       of atom list
 | AtomListCons   of atom list * atom
 | AtomTuple      of atom list
 | AtomStruct     of (symbol * atom) list
 | AtomConst      of symbol * atom list
 | AtomAs         of atom * symbol
 | AtomOCaml      of string
 | AtomBinop      of binop * atom * atom
 | AtomFuncall    of atom * atom list
 | AtomPragma     of pragma * atom


(* expression
   An ML expression.
      Value a                    Return a value.
      Raise a                    Raise an exception.
      LetIn (f, args, e1, e2)    Bind e1 to f with args, in e2.
      LetRec (f, args, e1, e2)   Bind e1 to f with args, in e2.
      Match (a, cases)           Match expression, ordered cases.
      If (a, e1, e2)             Conditional expression.
      Block                      Explicit grouping for match/if.
      Pragma (p, e)              Pragma information.
 *)
type expression =
   Value          of atom
 | Raise          of atom
 | LetIn          of symbol * symbol list * expression * expression
 | LetRec         of symbol * symbol list * expression * expression
 | Match          of atom * (atom * expression option * expression) list
 | If             of atom * expression * expression
 | Block          of expression
 | Pragma         of pragma * expression


(* statement
   An ML top-level statement.
      Open name                  Open the named module.
      Let (f, args, e)           Emit a top-level function definition.
      OCaml text                 Raw OCaml code.
      Expression e               Toplevel expression.
      TopPragma p                Pragma information.
 *)
type statement =
   Open           of symbol
 | Let            of symbol * symbol list * expression
 | OCaml          of string
 | Expression     of expression
 | TopPragma      of pragma


(* program
   Describes a ML program.  Currently defined as a list of ordered,
   top-level ML statements.  *)
type program = statement Listbuf.t


(***  Interface Code (MLI file)  ***)


(* itype
   Type information for an interface file.
      ITyName name               Named type
      ITyFun (t1, t2)            Application
      ITyDot (t1, t2)            Type qualification
      ITyApply (t1, t2)          Type application
 *)
type itype =
   ITyName        of symbol
 | ITyFun         of itype * itype
 | ITyDot         of itype * itype
 | ITyApply       of itype * itype


(* istatement
   Statements which are valid in an interface.
      IOpen name                 Open the named module
      IVal (name, ty)            Value declaration with type info
      ITopPragma pragma          Pragma information
 *)
type istatement =
   IOpen          of symbol
 | IVal           of symbol * itype
 | ITopPragma     of pragma


(* interface
   Describes an ML interface.  Currently defined as a list of ordered
   declarations.  *)
type interface = istatement Listbuf.t
