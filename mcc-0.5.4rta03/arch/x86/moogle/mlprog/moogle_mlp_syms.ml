(*
   ML Program Symbols
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Moogle_mlp_type


(* build_sym
   Builds a symbol with the given name.  *)
let build_sym name =
   Symbol.add name


(* build_sym_var
   Builds a symbol and a variable expression with the given name.  *)
let build_sym_var name =
   let sym = build_sym name in
      sym, AtomVar sym

let build_sym_var_local name =
   let sym = new_symbol_string name in
      sym, AtomVar sym


(* build_sym_ty
   Builds a symbol and an interface type with the given name.  *)
let build_sym_ty name =
   let sym = build_sym name in
      sym, ITyName sym


(* build_module_val
   Builds a dotted pair of a module variable and a value name.  *)
let build_module_val m name =
   AtomDot (m, AtomVar (build_sym name))


(* build_module_ty
   Builds a dotted pair of a module name and a type name.  *)
let build_module_ty m name =
   ITyDot (ITyName m, ITyName (build_sym name))


(***  Symbol Declarations  ***)


(* Module names *)
let failure_sym,        failure_var       =  build_sym_var "Failure"
let frame_type_sym,     frame_type_var    =  build_sym_var "Frame_type"
let int32_sym,          int32_var         =  build_sym_var "Int32"
let list_sym,           list_var          =  build_sym_var "List"
let listbuf_sym,        listbuf_var       =  build_sym_var "Listbuf"
let moogle_lib_sym,     moogle_lib_var    =  build_sym_var "Moogle_lib"
let moogle_offset_sym,  moogle_offset_var =  build_sym_var "Moogle_offset"
let not_sym,            not_var           =  build_sym_var "not"
let succ_sym,           succ_var          =  build_sym_var "succ"
let symbol_sym,         symbol_var        =  build_sym_var "Symbol"
let wildcard_sym,       wildcard_var      =  build_sym_var "_"


(* Constructor names *)
let none_sym                              =  build_sym "None"
let off_label_sym                         =  build_sym "OffLabel"
let some_sym                              =  build_sym "Some"


(* Typical function calls *)
let int32_of_int_var                      =  build_module_val int32_var "of_int"
let int32_of_offset_var                   =  build_module_val moogle_offset_var "to_int32"
let list_fold_left_var                    =  build_module_val list_var "fold_left"
let list_map_var                          =  build_module_val list_var "map"
let list_rev_var                          =  build_module_val list_var "rev"
let listbuf_add_var                       =  build_module_val listbuf_var "add"
let listbuf_add_list_var                  =  build_module_val listbuf_var "add_list"
let listbuf_add_listbuf_var               =  build_module_val listbuf_var "add_listbuf"
let listbuf_empty_var                     =  build_module_val listbuf_var "empty"
let new_symbol_string_var                 =  build_module_val symbol_var "new_symbol_string"


(* General purpose types *)
let bool_ty_sym,        bool_ty           =  build_sym_ty "bool"
let int32_ty_sym,       int32_ty          =  build_sym_ty "int32"
let list_ty_sym,        list_ty           =  build_sym_ty "list"
let live_ty_sym,        live_ty           =  build_sym_ty "live"
let ruledata_ty_sym,    ruledata_ty       =  build_sym_ty "rule_data"
let listbuf_ty                            =  build_module_ty listbuf_sym "t"


(* Moogle offset expressions *)
let moogle_offset_power2_var              =  build_module_val moogle_offset_var "is_power2"
let moogle_offset_equal32_var             =  build_module_val moogle_offset_var "equal32"
let moogle_offset_true_var                =  build_module_val moogle_offset_var "True"


(* Operand constructor names *)
let float_register_sym                    =  build_sym "FloatRegister"
let fp_stack_sym                          =  build_sym "FPStack"
let immediate_number_sym                  =  build_sym "ImmediateNumber"
let immediate_label_sym                   =  build_sym "ImmediateLabel"
let off_number_sym                        =  build_sym "OffNumber"
let mem_reg_sym                           =  build_sym "MemReg"
let mem_reg_off_sym                       =  build_sym "MemRegOff"
let mem_reg_reg_off_mul_sym               =  build_sym "MemRegRegOffMul"
let register_sym                          =  build_sym "Register"


(* Variables used in rewrite rules *)
let buf_sym,            buf_var           =  build_sym_var "__buf"
let cont_sym,           cont_var          =  build_sym_var "__cont"
let count_sym,          count_var         =  build_sym_var "__count"
let fallthrough_sym,    fallthrough_var   =  build_sym_var "__fallthrough"
let insts_sym,          insts_var         =  build_sym_var "__insts"
let instsp_sym,         instsp_var        =  build_sym_var "__insts'"
let match_wild_sym,     match_wild_var    =  build_sym_var "__match_wild"
let nreg_sym,           nreg_var          =  build_sym_var "__nreg"
let process_rules_sym,  process_rules_var =  build_sym_var "process_rules"
let rd_insts_sym,       rd_insts_var      =  build_sym_var "rd_insts"
let result_sym,         result_var        =  build_sym_var "__result"
let rev_wild_match_sym, rev_wild_match_var=  build_sym_var "__rev_wild_matched"
let ruledata_sym,       ruledata_var      =  build_sym_var "__ruledata"
let v_sym,              v_var             =  build_sym_var "__v"


(* These are moogle-lib functions exported for rewrites *)
let rule_build_sym,           rule_build_var          =  build_sym_var "rule_build"
let rule_build_default_sym,   rule_build_default_var  =  build_sym_var "rule_build_default"
let rule_create_sym,          rule_create_var         =  build_sym_var "rule_create"


(* External names and labels used in rewrite rules *)
let end_of_scopef_sym,        end_of_scopef_var       =  build_sym_var "end_of_scopef"
let end_of_scopei_sym,        end_of_scopei_var       =  build_sym_var "end_of_scopei"
let is_inst_live_empty_sym,   is_inst_live_empty_var  =  build_sym_var "is_inst_live_empty"
let is_memory_operand_sym,    is_memory_operand_var   =  build_sym_var "is_memory_operand"
let memory_ignored_sym,       memory_ignored_var      =  build_sym_var "memory_ignored"
let memory_not_dst_sym,       memory_not_dst_var      =  build_sym_var "memory_not_dst"
let not_a_dst_sym,            not_a_dst_var           =  build_sym_var "not_a_dst"
let not_fpstack_sym,          not_fpstack_var         =  build_sym_var "not_fpstack"
let not_in_operand_sym,       not_in_operand_var      =  build_sym_var "not_in_operand"
let not_live_outf_sym,        not_live_outf_var       =  build_sym_var "not_live_outf"
let not_live_outi_sym,        not_live_outi_var       =  build_sym_var "not_live_outi"
let not_used_sym,             not_used_var            =  build_sym_var "not_used"
let operand_is_small_sym,     operand_is_small_var    =  build_sym_var "operand_is_small"
let register_is_small_sym,    register_is_small_var   =  build_sym_var "register_is_small"


(* Structure fields used in rewrite rules *)
let ic_comments_sym,    ic_comments_var   =  build_sym_var "ic_comments"
let ic_inst_sym,        ic_inst_var       =  build_sym_var "ic_inst"
let ic_live_sym,        ic_live_var       =  build_sym_var "ic_live"


(* Variables used in instruction tables *)
let inst_sym,           inst_var          =  build_sym_var "__inst"
let trivial_sym,        trivial_var       =  build_sym_var "trivial"
