(*
   Inline Kupo Processing for the moogle/kupo languages
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol

open Moogle_prog_exn
open Moogle_mlp_ocaml
open Moogle_mlp_operand
open Moogle_mlp_syms
open Moogle_mlp_type
open Moogle_util

module Prog = Moogle_prog_type


(***  Convert Inline Kupo Code to ML  ***)


(* mlp_of_inline_kupo_inst
   Emit an instruction in an Inline Kupo block.  *)
let mlp_of_inline_kupo_inst inst =
   let make_pos pos = string_pos "mlp_of_inline_kupo_inst" (exp_pos pos) in
      match inst with
         Prog.OutInst (opcode, operands, _) ->
            AtomConst (opcode, List.map mlp_of_arith_operand operands)
       | Prog.OutExpr (file_pos, text, _) ->
            AtomPragma (LineInfo (file_pos, false), AtomOCaml text)
       | Prog.OutTrans (_, _, _, pos)
       | Prog.OutCopy (_, _, pos) ->
            raise (ProgException (make_pos pos, StringError "only Inst form is valid here"))


(* mlp_of_kupo_block_decls
   Emit the block declarations.  *)
let mlp_of_kupo_block_decls file_pos decls e =
   let rec build_decls decls =
      match decls with
         (v, ty, decl_type) :: decls ->
            let comment = "symbol " ^ (string_of_symbol v) ^ " is declared with type " ^ (string_of_type ty) in
            let e = build_decls decls in
            let e =
               match decl_type with
                  Prog.KupoNewVar ->
                     Pragma (LineInfo (file_pos, true),
                     LetIn (v, [], Value (AtomFuncall (new_symbol_string_var, [AtomString (string_of_symbol v)])), e))
                | Prog.KupoDeclaredVar ->
                     e
                | Prog.KupoDefinedVar e' ->
                     Pragma (LineInfo (file_pos, true),
                     LetIn (v, [], Value (mlp_of_arith Prog.ArithInt32 e'), e))
            in
            let e = Pragma (Comment comment, e) in
               e
       | [] ->
            e
   in
      build_decls decls


(* mlp_of_inline_kupo_listbuf_manip
   Emit a listbuf manipulation.  *)
let mlp_of_inline_kupo_listbuf_manip file_pos instbuf manip =
   let add_pragma mark a = AtomPragma (LineInfo (file_pos, mark), a) in
      match manip with
         Prog.KupoAddInsts insts ->
            (* We are constructing our own Listbuf.add call, with a
               sequence of Kupo instructions in a nice boxed list.  *)
            let insts = List.map (fun inst ->
               add_pragma true (mlp_of_inline_kupo_inst inst)) insts
            in
               AtomFuncall (listbuf_add_list_var, [AtomVar instbuf; AtomList insts])
       | Prog.KupoAddExprList (file_pos, e, _) ->
            (* We are calling an OCaml function which returns a list
               of instructions to append to the current listbuf.  *)
            let insts = add_pragma false (AtomOCaml e) in
               AtomFuncall (listbuf_add_list_var, [AtomVar instbuf; insts])
       | Prog.KupoAddExprListbuf (file_pos, e, _) ->
            (* We are calling an OCaml function which returns listbuf
               of instructions to append to the current listbuf.  *)
            let insts = add_pragma false (AtomOCaml e) in
               AtomFuncall (listbuf_add_listbuf_var, [AtomVar instbuf; insts])
       | Prog.KupoModifyExprListbuf (file_pos, e, _) ->
            (* We are calling an OCaml function which returns a new
               listbuf at this point...  *)
            add_pragma false (AtomOCaml e)


(* mlp_of_inline_kupo_block
   Emit a Kupo code block.  *)
let mlp_of_inline_kupo_block file_pos instbuf rtns decls insts =
   (* Print the operation to append instructions to a listbuf.  First,
      find out what instbuf we are supposed to be writing to...  *)
   let new_buf, instbuf =
      match instbuf with
         Prog.KupoAppendList instbuf ->
            false, instbuf
       | Prog.KupoNewList (Some instbuf) ->
            true, instbuf
       | Prog.KupoNewList None ->
            true, new_symbol_string "__kupo_new_inst_buf"
   in

   (* Print new variable declarations here *)
   let step_1 e = Pragma (Comment "Begin Kupo Code", e) in
   let step_2 e = mlp_of_kupo_block_decls file_pos decls e in

   (* If this is a fresh instbuf, then create it now *)
   let step_3 e =
      if new_buf then
         Pragma (LineInfo (file_pos, true), LetIn (instbuf, [], Value listbuf_empty_var, e))
      else
         e
   in

   (* Each insts entry describes a manipulation of the listbuf;
      process them in order here, until we reach the end.  *)
   let rec step_4 manips e =
      match manips with
         manip :: manips ->
            (* At this point, our next step depends on what listbuf action
               we are taking at this point; do we emit a stream of Kupo
               instructions, or call an OCaml function that returns a new
               listbuf?  *)
            Pragma (LineInfo (file_pos, true),
            LetIn (instbuf, [],
               Pragma (LineInfo (file_pos, true),
               Value (mlp_of_inline_kupo_listbuf_manip file_pos instbuf manip)), step_4 manips e))
       | [] ->
            e
   in

   (* Once all the manipulations are printed, we need to return
      the appropriate set of results.  *)
   let step_5 =
      let tuple_tail = List.map (fun op -> mlp_of_arith_operand op) rtns in
      let tuple = AtomVar instbuf :: tuple_tail in
         Pragma (Comment "End Kupo Code", Pragma (LineInfo (file_pos, true), Value (AtomTuple tuple)))
   in

      (* Assemble the final expression *)
      step_1 (step_2 (step_3 (step_4 insts step_5)))


(* mlp_of_inline_kupo_block
   Emit a single Inline Kupo block.  *)
let mlp_of_inline_kupo_block buf (file_pos, block) =
   match block with
      Prog.OCamlCode (text, _) ->
         Listbuf.add_list buf
            [TopPragma (LineInfo (file_pos, false));
             OCaml text]
    | Prog.KupoCode (instbuf, rtns, decls, insts, _) ->
         Listbuf.add buf (Expression (mlp_of_inline_kupo_block file_pos instbuf rtns decls insts))
