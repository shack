(*
   Convert a Prog structure into an ML program
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol

open Moogle_mlp_inline
open Moogle_mlp_inst
open Moogle_mlp_rule
open Moogle_mlp_syms
open Moogle_mlp_type
open Moogle_util

module Prog = Moogle_prog_type


(***  Convert a Program into an ML Program  ***)


(* mlp_of_rules
   Prints the rules, divided across multiple functions.  These functions use
   continuations to indicate the fallthrough case.  A rewrite rule function
   looks like this:

      let process_rules fallthrough nreg ruledata =
         ...
      val process_rules : (bool -> rule_data -> rule_data) ->
                          bool -> rule_data -> rule_data

   Where nreg is a boolean indicating if new register names can be interned,
   rule_data contains information on the buffer to append instructions to
   and the instructions to rewrite, and fallthrough is a function which is
   invoked if unable to rewrite this rule.

   If any rewrite rule in thie set can be applied to the first nontrivial
   instructions in the given rule_data, then a modified rule_data is
   returned, with the rewritten instructions (and any comments within them)
   added to the output buffer, and the tail of the instruction stream for
   further processing. If no rewrite rule can be applied, then the original
   rule_data structure is passed to the fallthrough function for further
   processing.  The last fallthrough should move one nontrivial instruction
   to the output stream.
 *)
let mlp_of_rules buf ibuf rules =
   (* Emit a call to the label for a prior rewrite rule.  If there are
      no previous rewrite rules, then call the fallthrough argument. *)
   let call_to_prev_label = function
      None ->
         Value (AtomFuncall (fallthrough_var, [nreg_var; ruledata_var]))
    | Some prev_label ->
         Value (AtomFuncall (AtomVar prev_label, [fallthrough_var; nreg_var; ruledata_var]))
   in

   (* At this point, we emit the rules.  The way we fold, rule N is given
      the label of rule (N-1) to fallback to; since the earlier rules are
      supposed to take precedence over later rules, the fold is basically
      backwards.  The easiest way to correct this is to reverse the list
      in advance (this allows us to use the nice tail-recursive fold). *)
   let rules = List.rev rules in
   let buf, prev_label =
      List.fold_left (fun (buf, prev_label) rule ->
         let label = new_symbol_string "__process_rules" in
         let e_fail = call_to_prev_label prev_label in
         let e_rule = mlp_of_rule rule (AtomDot (ruledata_var, rd_insts_var)) e_fail in
         let fun_def = Let (label, [fallthrough_sym; nreg_sym; ruledata_sym], e_rule) in
            Listbuf.add buf fun_def, Some label) (buf, None) rules
   in

   (* Build the main (toplevel) function *)
   let e_main = call_to_prev_label prev_label in
   let buf = Listbuf.add buf (Let (process_rules_sym, [fallthrough_sym; nreg_sym; ruledata_sym], e_main)) in

   (* Construct the interface code *)
   let ty_fallthrough = ITyFun (bool_ty, ITyFun (ruledata_ty, ruledata_ty)) in
   let ty_process = ITyFun (ty_fallthrough, ITyFun (bool_ty, ITyFun (ruledata_ty, ruledata_ty))) in
   let ibuf = Listbuf.add ibuf (IVal (process_rules_sym, ty_process)) in
      buf, ibuf


(* mlp_of_tag_function
   Prints a classification function for the indicated instruction tag.
   All instructions defined in prog will be included in the list.  *)
let mlp_of_tag_function buf ibuf prog tag =
   let body = mlp_of_inst_tag prog.Prog.prog_insts prog.Prog.prog_inst_rorder tag in
   let buf  = Listbuf.add buf  (Let (tag, [inst_sym], body)) in

   (* Construct the interface code *)
   let ibuf =
      match prog.Prog.prog_inst_type with
         None ->
            ibuf
       | Some inst ->
            Listbuf.add ibuf (IVal (tag, ITyFun (ITyName inst, ITyName bool_ty_sym)))
   in
      buf, ibuf


(* mlp_of_transform_function
   Appends a transformation function.  *)
let mlp_of_transform_function buf ibuf prog name transform =
   let args = inst_sym :: transform.Prog.transform_args in
   let body = mlp_of_inst_transform transform.Prog.transform_data in
   let buf  = Listbuf.add buf (Let (name, args, body)) in

   (* Construct the interface code *)
   let ibuf =
      match prog.Prog.prog_inst_type with
         None ->
            ibuf
       | Some inst ->
            let ity  = List.fold_left (fun ity _ ->
               ITyFun (ITyName int32_ty_sym, ity)) (ITyName inst) transform.Prog.transform_args
            in
               Listbuf.add ibuf (IVal (name, ITyFun (ITyName inst, ity)))
   in
      buf, ibuf


(* mlp_of_prog_header
   Append the program header to the current instruction stream.  *)
let mlp_of_prog_header prog =
   let add_pragma (buf, ibuf) pragma =
      Listbuf.add buf (TopPragma pragma), Listbuf.add ibuf (ITopPragma pragma)
   in
   let add_open (buf, ibuf) name =
      Listbuf.add buf (Open name), Listbuf.add ibuf (IOpen name)
   in

   (* Print the header, including open calls *)
   let buf = Listbuf.empty in
   let bufs = buf, buf in
   let bufs = add_pragma bufs
      (Comment ("Moogle/KUPO output file\n" ^
                "WARNING: This is an automatically generated file!\n" ^
                "Do not edit directly!  Instead, edit the source\n" ^
                "file, " ^ prog.Prog.prog_file))
   in
   let bufs = add_pragma bufs
      (Comment ("The room was terrible. Also, that thing crying ``Kupo-Kupo'' kept me\n" ^
                "up all night. Throw him out ASAP, or I'm never coming back. -Lani\n" ^
                "   -- Comment form for Lindblum Inn, Final Fantasy IX"))
   in
   let bufs = add_open bufs moogle_lib_sym in
   let bufs = add_open bufs frame_type_sym in
   let bufs = List.fold_left add_open bufs prog.Prog.prog_opens in
      bufs


(* mlp_of_prog_rules
   Append the ML code for all rule tables to the buffer.  *)
let mlp_of_prog_rules buf ibuf prog =
   (* Print process_rules helpers. *)
   let trivial_code =
      match prog.Prog.prog_trivial with
         None ->
            Let (trivial_sym, [inst_sym], Value (AtomBool false))
       | Some trivial ->
            Let (trivial_sym, [], Value (AtomVar trivial))
   in
   let moogle_lib_prefix v = AtomDot (moogle_lib_var, AtomVar v) in
   let build_assignment symbol =
      Let (symbol, [], Value (moogle_lib_prefix symbol))
   in
   let build_assignment_trivial symbol =
      Let (symbol, [], Value (AtomFuncall (moogle_lib_prefix symbol, [trivial_var])))
   in
   let buf = Listbuf.add_list buf
      [trivial_code;
       build_assignment_trivial rule_create_sym;
       build_assignment rule_build_sym;
       build_assignment rule_build_default_sym]
   in

   (* Build the interface code *)
   let ibuf =
      match prog.Prog.prog_inst_type with
         None ->
            ibuf
       | Some inst ->
            let inst_listbuf_ty = ITyApply (ITyName inst, listbuf_ty) in
            let inst_live_list_ty = ITyApply (ITyApply (ITyName inst, live_ty), list_ty) in
            let ty_create = ITyFun (inst_listbuf_ty, ITyFun (inst_live_list_ty, ruledata_ty)) in
            let ty_builddef = ITyFun (bool_ty, ITyFun (ruledata_ty, ruledata_ty)) in
            let ibuf = Listbuf.add_list ibuf
               [IVal (rule_create_sym, ty_create);
                IVal (rule_build_default_sym, ty_builddef)]
            in
               ibuf
   in
      mlp_of_rules buf ibuf prog.Prog.prog_rules


(* mlp_of_prog_insts
   Append the ML code for all instruction tables to the buffer.  *)
let mlp_of_prog_insts buf ibuf prog =
   let buf, ibuf =
      SymbolTable.fold (fun (buf, ibuf) tag _ ->
         mlp_of_tag_function buf ibuf prog tag) (buf, ibuf) prog.Prog.prog_tags
   in
   let buf, ibuf =
      SymbolTable.fold (fun (buf, ibuf) name info ->
         mlp_of_transform_function buf ibuf prog name info) (buf, ibuf) prog.Prog.prog_transforms
   in
      buf, ibuf


(* mlp_of_prog_inline_kupo
   Append the ML code for all inline Kupo blocks to the buffer.  *)
let mlp_of_prog_inline_kupo buf ibuf prog =
   let buf =
      List.fold_left (fun buf block ->
         mlp_of_inline_kupo_block buf block) buf prog.Prog.prog_inline_kupo
   in
      buf, ibuf
