(*
   Convert program operands into ML expressions
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol

open Moogle_prog_exn
open Moogle_mlp_syms
open Moogle_mlp_type

module Prog = Moogle_prog_type


(***  Generate ML Code for Operands  ***)


(* mlp_of_operand
   Emits an arbitrary operand.  value_fn tells us how to print the
   values in the operand.  If print_temps is true, the "as" clause
   and operand binding name will be printed for certain operands (the
   memory operands in particular); if print_temps is false then these
   names will be ignored.  *)
let mlp_of_operand value_fn print_temps op =
   let temps_wrapper name a =
      if print_temps then
         AtomAs (a, name)
      else
         a
   in
      match op with
         Prog.Op (v, _) ->
            AtomVar v
       | Prog.OpReg (v, _) ->
            AtomConst (register_sym, [AtomVar v])
       | Prog.OpFlReg (v, _) ->
            AtomConst (float_register_sym, [AtomVar v])
       | Prog.OpFPStack (i, _) ->
            AtomConst (fp_stack_sym, [value_fn Prog.ArithInt32 i])
       | Prog.OpCons (v, _) ->
            AtomConst (v, [])
       | Prog.OpMemReg (name, ptr, _) ->
            temps_wrapper name (AtomConst (mem_reg_sym, [AtomVar ptr]))
       | Prog.OpMemRegOff (name, ptr, off, _) ->
            temps_wrapper name (AtomConst (mem_reg_off_sym, [AtomVar ptr; value_fn Prog.ArithOffset off]))
       | Prog.OpMemRegRegOffMul (name, ptr, reg, off, mul, _) ->
            temps_wrapper name (AtomConst (mem_reg_reg_off_mul_sym,
               [AtomVar ptr; AtomVar reg; value_fn Prog.ArithOffset off; value_fn Prog.ArithInt32 mul]))
       | Prog.OpInt (i, _)
       | Prog.OpOff (i, _) ->
            AtomConst (immediate_number_sym, [value_fn Prog.ArithOffset i])
       | Prog.OpLabel (label, _) ->
            AtomConst (immediate_label_sym, [AtomVar label])
       | Prog.OpString (text, _) ->
            AtomString text
       | Prog.OpExpr (file_pos, text, _) ->
            AtomPragma (LineInfo (file_pos, false), AtomOCaml text)


(* atom_of_int32_unop
   atom_of_int32_binop
   Emits an operator intended to be used somewhere where an int32
   value is required.  For operands that require the special offset
   type, use the next functions instead.  *)
let atom_of_int32_unop op =
   let a, b =
      match op with
         Prog.UMinusOp ->
            int32_var, "neg"
       | Prog.UNotOp ->
            int32_var, "lognot"
       | Prog.UAbsOp ->
            int32_var, "abs"
       | Prog.ULog2Op ->
            moogle_lib_var, "log2"
       | Prog.UPow2Op ->
            moogle_lib_var, "pow2"
   in
      AtomDot (a, AtomVar (Symbol.add b))

let atom_of_int32_binop op =
   let a, b =
      match op with
         Prog.PlusOp ->
            int32_var, "add"
       | Prog.MinusOp ->
            int32_var, "sub"
       | Prog.MulOp ->
            int32_var, "mul"
       | Prog.DivOp ->
            int32_var, "div"
       | Prog.ShlOp ->
            moogle_lib_var, "shl32"
       | Prog.ShrOp ->
            moogle_lib_var, "shr32"
       | Prog.SarOp ->
            moogle_lib_var, "sar32"
       | Prog.AndOp ->
            int32_var, "logand"
       | Prog.OrOp ->
            int32_var, "logor"
   in
      AtomDot (a, AtomVar (Symbol.add b))


(* mlp_of_int32_arith
   Emits an arithmetic expression in a location where an int32 value
   is expected.  Do not use this function where offsets are expected;
   use the next batch instead.  *)
let rec mlp_of_int32_arith v =
   let make_pos pos = string_pos "mlp_of_int32_arith" (exp_pos pos) in
      match v with
         Prog.ArithInt (i, _) ->
            AtomInt32 i
       | Prog.ArithReg (v, _) ->
            AtomVar v
       | Prog.ArithReg31 (v, _) ->
            AtomFuncall (int32_of_int_var, [AtomVar v])
       | Prog.ArithOff (v, _) ->
            AtomFuncall (int32_of_offset_var, [AtomVar v])
       | Prog.ArithLabel (v, pos) ->
            raise (ProgException (make_pos pos, StringError "ArithLabel not allowed where an int32 expression is expected"))
       | Prog.ArithUnop (op, v, _) ->
            AtomFuncall (atom_of_int32_unop op, [mlp_of_int32_arith v])
       | Prog.ArithBinop (op, v1, v2, _) ->
            AtomFuncall (atom_of_int32_binop op, [mlp_of_int32_arith v1; mlp_of_int32_arith v2])


(* atom_of_offset_unop
   atom_of_offset_binop
   Emits an operator intended to be used somewhere where an offset
   value is required (e.g. the offset parametre of MemRegOff). *)
let atom_of_offset_unop op =
   let f =
      match op with
         Prog.UMinusOp ->
            "neg"
       | Prog.UNotOp ->
            "lognot"
       | Prog.UAbsOp ->
            "abs"
       | Prog.ULog2Op ->
            "log2"
       | Prog.UPow2Op ->
            "pow2"
   in
      AtomDot (moogle_offset_var, AtomVar (Symbol.add f))

let atom_of_offset_binop op =
   let f =
      match op with
         Prog.PlusOp ->
            "add"
       | Prog.MinusOp ->
            "sub"
       | Prog.MulOp ->
            "mul"
       | Prog.DivOp ->
            "div"
       | Prog.ShlOp ->
            "shift_left"
       | Prog.ShrOp ->
            "shift_right_logical"
       | Prog.SarOp ->
            "shift_right"
       | Prog.AndOp ->
            "logand"
       | Prog.OrOp ->
            "logor"
   in
      AtomDot (moogle_offset_var, AtomVar (Symbol.add f))


(* mlp_of_offset_arith
   Emits an arithmetic expression in a location where an offset
   value is expected (e.g. the offset in MemRegOff).  *)
let rec mlp_of_offset_arith v =
   match v with
      Prog.ArithInt (i, _) ->
         AtomConst (off_number_sym, [AtomInt32 i])
    | Prog.ArithReg (v, _) ->
         AtomConst (off_number_sym, [AtomVar v])
    | Prog.ArithReg31 (v, _) ->
         AtomConst (off_number_sym, [AtomFuncall (int32_of_int_var, [AtomVar v])])
    | Prog.ArithOff (v, _) ->
         AtomVar v
    | Prog.ArithLabel (v, _) ->
         AtomConst (off_label_sym, [AtomVar v])
    | Prog.ArithUnop (op, v, _) ->
         AtomFuncall (atom_of_offset_unop op, [mlp_of_offset_arith v])
    | Prog.ArithBinop (op, v1, v2, pos) ->
         AtomFuncall (atom_of_offset_binop op, [mlp_of_offset_arith v1; mlp_of_offset_arith v2])


(* Main conversion functions *)
let mlp_of_arith ty v =
   match ty with
      Prog.ArithInt32  -> mlp_of_int32_arith  v
    | Prog.ArithOffset -> mlp_of_offset_arith v

let mlp_of_arith_operand = mlp_of_operand mlp_of_arith false
let mlp_of_match_operand = mlp_of_operand (fun _ s -> AtomVar s) true
