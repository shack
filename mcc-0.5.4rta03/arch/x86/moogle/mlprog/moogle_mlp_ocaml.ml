(*
   Processing for raw OCaml expressions
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format


(***  Print OCaml Expressions  ***)


(* pp_print_ocaml_line_break
   Print a line break in an output stream.  If line mangling is on,
   then we'll emit the line override for the beginning of the block
   (we can't get more specific than that, not easily at least), and
   optionally a unique marker for debugging purposes in case we get
   a compile error inside the generated code.  If the flag is true,
   then make a new KCM marker.  *)
let pp_kupo_marker = ref 0

let pp_print_ocaml_line_break buf (file, line) make_kcm =
   if !Moogle_state.mangle_position_info then begin
      (* The -9999 is a blatant hack to force the next line to be
         unindented.  If the line override is indented then the
         OCaml compiler misses it completely...  *)
      fprintf buf "@[<v -9999>@ # %d \"" line;
      if make_kcm then
         fprintf buf "generated kupo code marker KCM%04d: %s" !pp_kupo_marker file
      else
         pp_print_string buf file;
      fprintf buf "\"@]@ ";
      incr pp_kupo_marker
   end else
      fprintf buf "@ "


(* pp_print_ocaml_block_text
   Prints out the OCaml block text.  Note that this function does NOT print
   inside a self-contained box.  If you want the OCaml code to be boxed, then
   you need to box it externally. *)
let pp_print_ocaml_block_text buf text =
   let rec print_to_next_newline start =
      try
         let stop = String.index_from text start '\n' in
         let text = String.sub text start (stop - start) in
            fprintf buf "%s@ " text;
            print_to_next_newline (stop + 1)
      with
         Not_found ->
            let text = String.sub text start (String.length text - start) in
               fprintf buf "%s" text
   in (* end of print_to_next_newline *)
      print_to_next_newline 0
