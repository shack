(*
   IO Rule Processing for the moogle/kupo languages
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Moogle_mlp_pattern

module Prog = Moogle_prog_type


(* mlp_of_rule
   Emits a single rule.  The result is an expression which will process
   a_match and evaluate the rule's contents if successful; on failure,
   the e_fail expression will be evaluated.  This is the body of one of
   the process_rules functions.  *)
let mlp_of_rule rule a_match e_fail =
   (* Determine if the output pattern interns new symbols;
      also, get the output clause restriction (if any). *)
   let out_uses_new = rule.Prog.rule_output.Prog.opat_new <> [] in
   let out_clause = rule.Prog.rule_output.Prog.opat_clause in

   (* Build the output expression. *)
   let rule_name = rule.Prog.rule_name in
   let rule_input = rule.Prog.rule_input in
   let rule_output = rule.Prog.rule_output in
   let e_result = mlp_of_output_pattern rule_name rule_input rule_output in

   (* Build the match expressions(s).  The output expression is
      embedded in the match expression.  The first list of insts
      is passed into the top of the match.  *)
   let e_match = mlp_of_match_pattern rule_name out_uses_new out_clause rule_input a_match e_result e_fail in
      e_match
