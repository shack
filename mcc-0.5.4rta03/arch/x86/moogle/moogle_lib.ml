(*
   Moogle runtime library for generated files
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol


(*
   3.... 2.... 1.... Restart!
   [Screen goes blank]
   Just joking!
      -- Tetsuya Nomura, Dream Team Ending, Chrono Trigger
         (A truly *evil* joke if you've ever gotten this ending)
 *)


open Format

open Frame_type
open X86_inst_type   (* TEMP: parameterize to remove this dependence *)
open X86_frame
open X86_util


(***  Utilities  ***)


(* TEMP
   currently importing from outside *)
let not_in_operand reg op = not (X86_opt_util.reg_in_operand reg op)


(* TEMP
   currently importing from outside *)
let is_memory_operand = is_memory_operand


(* TEMP
   currently importing from outside *)
let not_memory_operand op = not (is_memory_operand op)


(* TEMP
   currently importing from outside *)
let not_fpstack op = not (X86_opt_util.is_fpstack op)


(* TEMP
   architecture dependence *)
let register_is_small v = List.mem v [eax; ebx; ecx; edx]


(* TEMP
   architecture dependence *)
let operand_is_small = function
   Register v -> register_is_small v
 | _ -> true


(* TEMP
   architecture dependence *)
let memory_not_dst inst =
   let dst, _ = memory_inst inst.live_inst in
      dst = []


(* TEMP
   architecture dependence *)
let memory_ignored inst =
   let dst, src = memory_inst inst.live_inst in
      dst = [] && src = []


(* not_a_src
   Returns true if the named register is not a source
   of THIS instruction only.  *)
let not_a_src reg inst =
   not (List.mem reg inst.live_src)


(* not_a_dst
   Returns true if the named register is not a
   destination of THIS instruction only.  *)
let not_a_dst reg inst =
   not (List.mem reg inst.live_dst)


(* not_used
   Returns true if the named register is not used
   at ALL by the given instruction.  *)
let not_used reg inst =
   not_a_dst reg inst && not_a_src reg inst


(* not_live_outi
   Returns true if the named INT register is not
   live out of this instruction.  This does not
   take into account the possibility that reg is
   defined at this instruction.  *)
let not_live_outi reg inst =
   not (SymbolSet.mem inst.live_out reg)


(* not_live_outf
   Returns true if the named FLOAT register is not
   live out of this instruction.  This does not
   take into account the possibility that reg is
   defined at this instruction.  *)
let not_live_outf reg inst =
   not (SymbolSet.mem inst.live_out reg)


(* end_of_scopei
   Returns true if this is the end of scope for
   reg; that is, reg is either not live-out to
   the specified instruction, or is redefined at
   this point (thus its previous value is not
   relevant).  This does not consider if reg is
   used as a source AT THIS INSTRUCTION.  *)
let end_of_scopei reg inst =
   not_live_outi reg inst || not (not_a_dst reg inst)


(* is_inst_live_empty
   Returns true if this instruction appears to be
   dead; that is, none of its dst's appear to be
   live.  This does not take into account whether
   the instruction manipulates the floating-point
   stack or writes to memory, yet.  *)
let is_inst_live_empty inst =
   let live_out = inst.live_out in
   let live_dst = inst.live_dst in
      not (List.exists (SymbolSet.mem live_out) live_dst)


(***  Basic Arithmetic  ***)


(* Basic arithmetic operations *)
let is_power2 i =
   let rec search k =
      if k > i || k = (Int32.shift_left i 31) then
         false
      else
         k = i || search (Int32.shift_left k 1)
   in
      search Int32.one

let log2 i =
   let rec search j =
      let k = (Int32.shift_left Int32.one j) in
      if k = i then
         Int32.of_int j
      else
         search (succ j)
   in
      search 0

let pow2 i =
   Int32.shift_left (Int32.of_int 1) (Int32.to_int i)

let shl32 i j =
   Int32.shift_left i (Int32.to_int j)

let shr32 i j =
   Int32.shift_right_logical i (Int32.to_int j)

let sar32 i j =
   Int32.shift_right i (Int32.to_int j)


(***  Rules  ***)


(* inst_comment
   A pair of an instruction with the list of all comment (trivial)
   instructions which immediatel PRECEDE it.  Any comments at the
   tail of a block are stored separately. *)
type inst_comment =
   { ic_inst      : inst;
     ic_live      : inst live;
     ic_comments  : inst list;
   }

(* rule_data
   Data structure that is towed around for convenience while we are
   rewriting live instructions.  This contains the output buffer which
   instructions are appended to, and the list of original instructions
   that we are attempting to rewrite.  *)
type rule_data =
   { rd_insts     : inst_comment list;
     rd_tail      : inst list;
     rd_buf       : inst Listbuf.t;
   }


(* rule_update
   Update a ruledata structure.  Normally, this simply replaces the
   buf and insts fields; hwoever, if the new insts list is empty, then
   it appends the tail comments in the ruledata structure to the output
   stream.  This should be called anytime the ruledata needs to be
   updated to ensure the tail comments in a block are not lost.  *)
let rule_update ruledata buf insts =
   match insts with
      _ :: _ ->
         { ruledata with
           rd_insts = insts;
           rd_buf   = buf;
         }
    | [] ->
         (* No more instructions; append the tail comments to the
            output buffer at this time. *)
         { rd_insts = [];
           rd_tail  = [];
           rd_buf   = Listbuf.add_list buf ruledata.rd_tail;
         }


(* rule_create
   Create a new ruledata structure.  This preprocesses all comments
   so the rewrite rules do not have to worry about them.  The rewrite
   engine is responsible for emitting comments which show up in matched
   instructions; however, tail comments in a block should be handled
   automatically by the rule_build* functions.  *)
let rule_create trivial buf insts =
   let rev_insts, rev_tail =
      List.fold_left (fun (rev_ic_list, rev_comments) live ->
         let inst = live.live_inst in
            if trivial inst then
               rev_ic_list, inst :: rev_comments
            else
               let ic =
                  { ic_inst     = inst;
                    ic_live     = live;
                    ic_comments = List.rev rev_comments;
                  }
               in
                  ic :: rev_ic_list, []) ([], []) insts
   in
   let insts = List.rev rev_insts in
   let ruledata =
      { rd_insts = insts;
        rd_tail  = List.rev rev_tail;
        rd_buf   = buf;
      }
   in
      (* We need to call rule_update here; if there were no nontrivial
         instructions in the block, then we need to forcibly add the
         tail comments to the output buffer now. *)
      rule_update ruledata buf insts


(* rule_build
   Build a new ruledata.  Called if a rewrite rule is successfully
   applied.  The insts is the *new* list of instructions to process
   (should be a tail of the current rd_insts), name is the name of the
   rule being applied, and builder is a function to call to add the
   new instructions to the output stream.  *)
let rule_build ruledata insts name builder =
   let message = sprintf "OPT:  %s" name in
   let buf = ruledata.rd_buf in
   let buf = Listbuf.add buf (CommentString message) in
   let buf = builder buf in
      rule_update ruledata buf insts


(* rule_build_default
   Copy one instruction from the input stream to the output (along
   with any associated comments).  This is called only if *no*
   rewrite rule could be applied.  *)
let rule_build_default _ ruledata =
   let { rd_buf   = buf;
         rd_insts = insts;
       } = ruledata
   in
      match insts with
         { ic_inst = inst; ic_comments = comments } :: insts ->
            let buf = Listbuf.add_list buf comments in
            let buf = Listbuf.add buf inst in
               rule_update ruledata buf insts
       | [] ->
            ruledata
