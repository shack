(*
   Main program for the moogle/kupo language
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Moogle_ast_exn


(***  Generic program options  ***)


let output_file = ref None       (* Name of .ml file (None = stdout) *)
let output_ifile = ref None      (* Name of .mli file (None = no interface) *)
let output_file_ro = ref false   (* If true, make output file read-only *)
let print_insts = ref false      (* Print instruction/transform tables? *)
let print_rules = ref false      (* Print rewrite rules? *)
let print_inline = ref false     (* Print inlined Kupo code? *)


(* As input files are read, the kupo program we are building
   is stored here, in a single AST prog data structure.  *)
let main_prog = ref Moogle_prog_ast.new_prog


(***  Compilation  ***)


(* parse_prog inx
   Parses a program that is read from the indicated input channel.
   The program data structure for this file only is returned.  *)
let parse_prog inx =
   try
      let () = Moogle_ast_lex.reset_pos () in
      let lexbuf = Lexing.from_channel inx in
      let prog = Moogle_ast_parse.prog Moogle_ast_lex.main lexbuf in
         prog
   with
      Parsing.Parse_error ->
         raise (ASTException (exp_pos (!Moogle_state.current_pos), ParseError))
    | Sys_error s ->
         raise (ASTException (exp_pos (!Moogle_state.current_pos), SysError s))


(* compile
   Compile the kupo file named into an AST program structure.
   After compilation, the AST structure will be concatenated to
   the end of the main_prog structure, defined above.  *)
let compile file =
   Moogle_state.current_file := file;
   let inx =
      try open_in file with
         Sys_error s ->
            raise (ASTException (exp_pos (!Moogle_state.current_pos), SysError s))
   in
      try
         let prog = parse_prog inx in
         let () = close_in inx in
            main_prog := Moogle_prog_ast.prog_of_expr !main_prog file prog
      with
         Sys_error s ->
            close_in inx;
            raise (ASTException (exp_pos (!Moogle_state.current_pos), SysError s))
       | ASTException (pos, msg) as e ->
            close_in inx;
            raise e


(* compile
   Similar to above function, but this version catches moogle
   error exceptions and prints the exception out, then exits
   with an abnormal return code.  *)
let compile file =
   try
      compile file
   with
      ASTException (pos, msg) ->
         print_exn pos msg;
         exit 1


(***  File Permission/Access Code  ***)


(* get_file_permissions
   Get the current permissions on a named file.  *)
let get_file_permissions file =
   let stats = Unix.stat file in
      stats.Unix.st_perm


(* clear_write_bits
   Clear all write bits (user, group, and world) on a named file.
   This call only has an effect if the output_file_ro flag is true.  *)
let clear_write_bits file =
   if !output_file_ro then begin
         let perm = get_file_permissions file in
         let perm = perm land (lnot 0o222) in
            Unix.chmod file perm
   end


(* set_write_bit
   Set the user write bit on a named file, so it can be modified.
   This call only has an effect if the output_file_ro flag is true.  *)
let set_write_bit file =
   if !output_file_ro then begin
      try
         let perm = get_file_permissions file in
         let perm = perm land 0o200 in
            Unix.chmod file perm
      with
         (* If the file doesn't exist here, it's not a big deal. *)
         Unix.Unix_error (Unix.ENOENT, _, _) ->
            ()
   end


(* close_nonstdout_channel
   Closes the indicated output channel, *if* it is not stdout.  *)
let close_nonstdout_channel out =
   if out <> stdout then
      close_out out


(***  Parse Options  ***)


(* version_info
   Print version information about moogleize.  *)
let version_info () =
   Printf.printf "Moogleize for MCC (Mojave Compiler)\n";
   Printf.printf "Using Kupo Language Specification version %s\n" Moogle_state.kupo_language_specification;
   exit 1


(* Define the program's options *)
let usage = "usage: moogleize [options] [files...]"

let spec =
  ["Basic options",
     ["-o",             Mc_arg.String (fun s -> output_file := Some s),
                        "name of .ml file to write to (default: stdout)";
      "-oi",            Mc_arg.String (fun s -> output_ifile := Some s),
                        "name of .mli file to write to (default: no interface)";
      "-insts",         Mc_arg.Set print_insts,
                        "print instruction tables";
      "-rules",         Mc_arg.Set print_rules,
                        "print pattern-match rules";
      "-inline",        Mc_arg.Set print_inline,
                        "print inlined Kupo code blocks";
      "-version",       Mc_arg.Unit version_info,
                        "print version information"];
   "Advanced options (use with caution)",
     ["-max-star",      Mc_arg.Int (fun i -> Moogle_state.rule_max_star := i),
                        "set maximum number of instructions a * wildcard can match in an input pattern" ^
                        " (default: " ^ (string_of_int !Moogle_state.rule_max_star) ^ ")";
      "-long-match",    Mc_arg.Set Moogle_state.rule_wildcard_long,
                        "wildcards match the longest possible instruction sequence (may weaken peephole optimizations)" ^
                        (if !Moogle_state.rule_wildcard_long then " (default)" else "");
      "-short-match",   Mc_arg.Clear Moogle_state.rule_wildcard_long,
                        "wildcards match the shortest possible instruction sequence (recommended)" ^
                        (if not !Moogle_state.rule_wildcard_long then " (default)" else "");
      "-no-mangle",     Mc_arg.Clear Moogle_state.mangle_position_info,
                        "if specified, do not mangle position information in emitted ML files.";
      "-rdonly",        Mc_arg.Set output_file_ro,
                        "if specified, the output ML file is set as a read-only file, preventing" ^
                        " accidental modifications to the generated file.  If this is set, then" ^
                        " moogleize will be allowed to delete read-only output files as well."]]


(***  Compile the Program  ***)


(* run
   Take the main_prog expression (generated by the front-end compile above)
   and attempt to produce a real ML file as output.  Output is either sent
   to stdout, or to a named file.  The write bits on the file may be cleared
   if the -rdonly flag was specified.  *)
let run () =
   (* Get the main_prog. *)
   Mc_arg.parse spec compile usage;

   (* Make sure at least one interesting option was specified *)
   let () =
      if not (!print_insts || !print_rules || !print_inline) then
         raise (ASTException (exp_pos (!Moogle_state.current_pos),
            StringError "Neither -insts, -rules, nor -inline were specified on the command-line"))
   in

   (* Compile the prog into an ML program *)
   let mlp, mlpi = Moogle_mlp_prog.mlp_of_prog_header !main_prog in
   let mlp, mlpi =
      if !print_insts then
         Moogle_mlp_prog.mlp_of_prog_insts mlp mlpi !main_prog
      else
         mlp, mlpi
   in
   let mlp, mlpi =
      if !print_rules then
         Moogle_mlp_prog.mlp_of_prog_rules mlp mlpi !main_prog
      else
         mlp, mlpi
   in
   let mlp, mlpi =
      if !print_inline then
         Moogle_mlp_prog.mlp_of_prog_inline_kupo mlp mlpi !main_prog
      else
         mlp, mlpi
   in

   (* Create the output channel for the final ML file. *)
   let out =
      try
         match !output_file with
            Some file ->
               set_write_bit file;
               open_out file
          | None ->
               stdout
      with
         Sys_error s ->
            raise (ASTException (exp_pos (!Moogle_state.current_pos), SysError s))
       | Unix.Unix_error (err, fn, _) ->
            raise (ASTException (exp_pos (!Moogle_state.current_pos), SysError (sprintf "%s: %s" fn (Unix.error_message err))))
   in
   let outf = formatter_of_out_channel out in
   let () = pp_set_margin outf 128 in
   let pos () = exp_pos (!Moogle_state.current_pos) in
   let () =
      try
         Moogle_mlp_print.pp_print_program outf mlp;
         pp_print_newline outf ();
         close_nonstdout_channel out;
         match !output_file with
            Some file ->
               clear_write_bits file
          | None ->
               ()
      with
         Sys_error s ->
            pp_print_newline outf ();
            close_nonstdout_channel out;
            raise (ASTException (pos (), SysError s))
       | Unix.Unix_error (err, fn, _) ->
            close_nonstdout_channel out;
            raise (ASTException (pos (), SysError (sprintf "%s: %s" fn (Unix.error_message err))))
       | ASTException (pos, msg) as e ->
            close_nonstdout_channel out;
            raise e
   in

   (* Now, attempt to emit the MLI file *)
   let () =
      match !output_ifile with
         Some file ->
            begin
            let () = set_write_bit file in
            let out = open_out file in
            let outf = formatter_of_out_channel out in
            let () = pp_set_margin outf 128 in
               try
                  Moogle_mlp_print.pp_print_interface outf mlpi;
                  pp_print_newline outf ();
                  close_out out;
                  clear_write_bits file
               with
                  Sys_error s ->
                     pp_print_newline outf ();
                     close_out out;
                     raise (ASTException (pos (), SysError s))
                | Unix.Unix_error (err, fn, _) ->
                     close_out out;
                     raise (ASTException (pos (), SysError (sprintf "%s: %s" fn (Unix.error_message err))))
                | ASTException (pos, msg) as e ->
                     close_out out;
                     raise e
            end
       | None ->
            ()
   in
      ()


let () =
   try run () with
      ASTException (pos, msg) ->
         print_exn pos msg;
         exit 1
    | Moogle_prog_exn.ProgException (pos, msg) ->
         Moogle_prog_exn.print_exn pos msg;
         exit 1
