(*
   Moogle runtime library for generated files
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Frame_type
open X86_inst_type


val not_in_operand      : operand   -> symbol -> bool
val not_fpstack         : operand   -> bool
val is_memory_operand   : operand   -> bool
val not_memory_operand  : operand   -> bool
val register_is_small   : symbol    -> bool
val operand_is_small    : operand   -> bool
val memory_not_dst      : inst live -> bool
val memory_ignored      : inst live -> bool
val not_a_src           : symbol    -> inst live -> bool
val not_a_dst           : symbol    -> inst live -> bool
val not_used            : symbol    -> inst live -> bool
val not_live_outi       : symbol    -> inst live -> bool
val not_live_outf       : symbol    -> inst live -> bool
val end_of_scopei       : symbol    -> inst live -> bool
val is_inst_live_empty  : inst live -> bool


val is_power2  : int32 -> bool
val log2       : int32 -> int32
val pow2       : int32 -> int32
val shl32      : int32 -> int32 -> int32
val shr32      : int32 -> int32 -> int32
val sar32      : int32 -> int32 -> int32


type inst_comment =
   { ic_inst      : inst;
     ic_live      : inst live;
     ic_comments  : inst list;
   }

type rule_data =
   { rd_insts     : inst_comment list;
     rd_tail      : inst list;
     rd_buf       : inst Listbuf.t;
   }


val rule_create            : (inst -> bool) -> inst Listbuf.t -> inst live list -> rule_data
val rule_build             : rule_data -> inst_comment list -> string -> (inst Listbuf.t -> inst Listbuf.t) -> rule_data
val rule_build_default     : bool -> rule_data -> rule_data
