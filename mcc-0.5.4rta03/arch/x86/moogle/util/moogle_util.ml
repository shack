(*
   Utilities for the Moogle/KUPO language
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol
open Moogle_prog_type


(***  Utilities and General Symbol Names  ***)


let underscore = Symbol.add "_"
let new_operand_name () = new_symbol_string "operand"


(* groups_of_size
   Returns a list of lists.  Each nested list contains elements in ls,
   with order preserved, but such that each inner list contains at most
   size elements.  *)
let groups_of_size ls size =
   let rec divide ls count =
      match ls with
         _ :: _ when count <= 0 ->
            let ls, groups = divide ls size in
               [], ls :: groups
       | elt :: ls ->
            let ls, groups = divide ls (pred count) in
               elt :: ls, groups
       | [] ->
            [], []
   in
   let ls, groups = divide ls size in
      ls :: groups


(***  Simple Output  ***)


let int32_sub_80000000 i =
   Int32.sub i (Int32.shift_left Int32.one 31)

let string_of_int32 i =
   if i = Int32.zero then
      "Int32.zero"
   else if i = Int32.one then
      "Int32.one"
   else if i = Int32.minus_one then
      "Int32.minus_one"
   else if i >= Int32.of_int 0x00000000 && i <= Int32.of_int 0x3fffffff then
      (* Int32 interval from 0 (0x00000000) to +1073741823 (0x3fffffff) *)
      Printf.sprintf "(* 0x%08lx *)(Int32.of_int 0x%08lx)" i i
   else if i >= Int32.of_int 0x40000000 && i <= Int32.of_int 0x7fffffff then
      (* Int32 interval from -1073741824 (0xc0000000) to -1 (0xffffffff) *)
      Printf.sprintf "(* 0x%08lx *)(Int32.of_int 0x%08lx)" i (int32_sub_80000000 i)
   else
      Printf.sprintf "(Int32.of_string \"0x%08lx\")" i


let string_of_type = function
   TyOp ->
      "op"
 | TyReg ->
      "reg"
 | TyFlReg ->
      "flreg"
 | TyInt ->
      "int32"
 | TyInt31 ->
      "int31"
 | TyOff ->
      "offset"
 | TyInst ->
      "inst"
 | TyInsts ->
      "insts"
 | TyLabel ->
      "label"


let pp_print_delimited_list buf delim printer list =
   let rec print_list = function
      elt :: (_ :: _ as list) ->
         printer buf elt;
         pp_print_string buf delim;
         print_list list
    | [elt] ->
         printer buf elt
    | [] ->
         ()
   in
      print_list list


let pp_print_delimited_list_post buf delim printer list =
   let rec print_list = function
      elt :: (_ :: _ as list) ->
         printer buf elt;
         pp_print_string buf delim;
         pp_print_space buf ();
         print_list list
    | [elt] ->
         printer buf elt
    | [] ->
         ()
   in
      print_list list


