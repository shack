(*
   Utilities for the Moogle/KUPO language
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format
open Symbol


val underscore       : symbol
val new_operand_name : unit -> symbol
val groups_of_size   : 'a list -> int -> 'a list list

val string_of_int32  : int32 -> string
val string_of_type   : Moogle_prog_type.ty -> string

val pp_print_delimited_list      : formatter -> string -> (formatter -> 'a -> unit) -> 'a list -> unit
val pp_print_delimited_list_post : formatter -> string -> (formatter -> 'a -> unit) -> 'a list -> unit
