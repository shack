/*
 * X86 runtime utility functions for Java.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "x86_context.h"
#include "x86_runtime.h"
#include "x86_alloc.h"

#define MIN(i, j)       ((i) < (j) ? (i) : (j))

/************************************************************************
 * UTILITIES
 ************************************************************************/

/*
 * String buffer utilities.
 * A string is a block with STRING_TAG,
 * or it is a tuple of strings.  The
 * flatten_string function concatenates
 * the tuple.
 */
static int flatten_string(void **tablep, char *bufp, unsigned long off, unsigned long bound, void *blockp)
{
    unsigned long size, amount, length, i;
    void *tmpp;

    if(blockp) {
        if(IS_AGGR(blockp)) {
            /* This is a normal string */
            length = strlen((char *) blockp);
            amount = MIN(length, bound - off);
            memcpy(bufp + off, blockp, amount);
            off += amount;
        }
        else {
            /* This is a string tuple */
            size = GET_BLOCK_SIZE(blockp) / sizeof(mc_field);
            for(i = 0; i != size; i++) {
                tmpp = tablep[GET_FIELD(blockp, i) >> INDEX_SHIFT];
                off += flatten_string(tablep, bufp, off, bound, tmpp);
            }
        }
    }
    return off;
}

/*
 * Walk through the string and print the parts.
 */
static void print_string(void **tablep, void *blockp)
{
    unsigned long index, size, i;

    if(blockp) {
        if(IS_AGGR(blockp))
            printf("%s", (char *) blockp);
        else {
            size = GET_BLOCK_SIZE(blockp) / sizeof(mc_field);
            for(i = 0; i != size; i++) {
                index = GET_FIELD(blockp, i) >> INDEX_SHIFT;
                print_string(tablep, tablep[index]);
            }
        }
    }
}

/************************************************************************
 * GLOBAL FUNS
 ************************************************************************/

/*
 * Build a string from the integer.
 * Guaranteed this won't cause a garbage collection.
 */
void *__ext_java_itoa(Context *contextp, long i)
{
    char buffer[20];
    sprintf(buffer, "%ld", i);
    return alloc_string(contextp, buffer);
}

/*
 * Concatenate two strings.
 * This actually creates a string tuple.
 * Guaranteed this won't cause a garbage collection.
 */
void *__ext_java_strcat(Context *contextp, void *str1, void *str2)
{
    void *blockp;

    blockp = alloc_block(contextp, 2);
    SET_FIELD(blockp, 0, GET_BLOCK_INDEX(str1) << INDEX_SHIFT);
    SET_FIELD(blockp, 1, GET_BLOCK_INDEX(str2) << INDEX_SHIFT);
    return blockp;
}

/*
 * Printing a string buffer.
 */
int __ext_java_println(Context *contextp, void *blockp)
{
    print_string(contextp->pointer_base, blockp);
    putchar('\n');
    return 0;
}

/*
 * Concat the string buffer.
 */
int __ext_java_atoi(Context *contextp, void *blockp)
{
    char buffer[100];
    int index;

    if(blockp == 0) {
        fprintf(stderr, "__atoi: nil pointer exception\n");
        abort();
    }
    index = flatten_string(contextp->pointer_base, buffer, 0, 99, blockp);
    buffer[index] = 0;
    return atoi(buffer);
}
