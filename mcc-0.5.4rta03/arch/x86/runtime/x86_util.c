/*
 * X86 runtime utility functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "x86_context.h"
#include "x86_runtime.h"

/************************************************************************
 * PRINTING
 ************************************************************************/

/*
 * Print an aggregate block.
 * Look for pointers in the block.
 */
static void print_aggr_block(Context *contextp, void *blockp)
{
    unsigned long size, index;
    unsigned int *datap;
    int i;

    index = GET_AGGR_INDEX(blockp);
    size = GET_AGGR_SIZE(blockp);
    fprintf(stderr, "\tAggregate block: addr=0x%08lx, index=%lu, size=%lu\n", (unsigned long) blockp, index, size);
    datap = (unsigned int *) blockp;
    for(i = 0; i < size; i += 4) {
        unsigned int data = *datap;
        unsigned int data_ptr = data ^ HASH_INDEX;
        unsigned int data_fun = data ^ HASH_INDEX_FUN;
        fprintf(stderr, "\tdata[0x%08x] = 0x%08x", i, data);
        if(data_ptr < contextp->pointer_size)
            fprintf(stderr, " pointer[0x%x+%d]\n", data_ptr >> INDEX_SHIFT, data_ptr & 3);
        else if(data_fun < FUNCTION_SIZE)
            fprintf(stderr, " function[%d]\n", data_fun >> INDEX_SHIFT);
        else
            putc('\n', stderr);
        datap++;
    }
}

/*
 * Print a normal block.
 */
static void print_normal_block(Context *contextp, void *blockp)
{
    unsigned long index, size;
    unsigned int *datap;
    int i, tag;

    tag = GET_BLOCK_TAG(blockp);
    index = GET_BLOCK_INDEX(blockp);
    size = GET_BLOCK_SIZE(blockp);
    fprintf(stderr, "\tNormal block: addr=0x%08lx, tag=%d, index=%lu, size=%lu\n", (unsigned long) blockp, tag, index, size);
    datap = (unsigned int *) blockp;
    for(i = 0; i < size; i += 4) {
        unsigned int data = *datap;
        if(data & 1)
            fprintf(stderr, "\tdata[%04d] = 0x%08x int[%d]\n", i, data, data >> 1);
        else if(data < FUNCTION_SIZE)
            fprintf(stderr, "\tdata[%04d] = 0x%08x function[%d]\n", i, data, data >> INDEX_SHIFT);
        else
            fprintf(stderr, "\tdata[%04d] = 0x%08x pointer[0x%x+%d]\n", i, data, data >> INDEX_SHIFT, data & 3);
        datap++;
    }
}

/*
 * Print an arbitrary block.
 */
static void print_block(Context *contextp, void *blockp)
{
    if(blockp < contextp->mem_base || blockp >= contextp->mem_limit)
        fprintf(stderr, "\tBlock pointer 0x%08lx is not valid\n", (unsigned long)blockp);
    else if(IS_AGGR(blockp))
        print_aggr_block(contextp, blockp);
    else
        print_normal_block(contextp, blockp);
}

/*
 * Print a dump of memory.
 * Walk the pointer table, and print the blocks.
 */
static void print_dump(Context *contextp)
{
    void *blockp;
    int i, count;

    count = contextp->pointer_size / sizeof(void *);
    fprintf(stderr, "\nPointer table has %d entries\n", count);
    for(i = 0; i != count; i++) {
        blockp = contextp->pointer_base[i];
        if(IS_BIT_CLEARED(blockp, PTBL_FREE_OFF)) {
            fprintf(stderr, "Block %d [0x%08lx]:\n", i, (unsigned long) blockp);
            print_block(contextp, blockp);
        }
    }
}

/*
 * Print a block, and the index it came with.
 */
static void print_block_index(Context *contextp, void *blockp, int index)
{
    fprintf(stderr, "This is the block you referred to with subscript=0x%x:\n", index);
    print_block(contextp, blockp);
}

static void print_block_reference(Context *contextp, void *blockp)
{
    fprintf(stderr, "This is the block you referred to:\n");
    print_block(contextp, blockp);
}

/************************************************************************
 * DEBUGGING
 ************************************************************************/

/*
 * Print the result of an allocation.
 */
int __debug_alloc(Context *contextp, int label, void *blockp)
{
    fprintf(stderr, "*** L%d: Alloc: 0x%08lx\n", label, (unsigned long) blockp);
    print_block(contextp, blockp);
    return 0;
}

/*
 * Loaded a pointer from memory.
 */
int __debug_let_pointer(Context *contextp, int label, void *blockp, int index, int index2, void *p)
{
    fprintf(stderr, "*** L%d: Let pointer: index=%d, value=0x%08lx\n", label, index2, (unsigned long) p);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Loaded a pointer from memory.
 */
int __debug_let_function(Context *contextp, int label, void *blockp, int index, void *p)
{
    fprintf(stderr, "*** L%d: Let function: 0x%08lx\n", label, (unsigned long) p);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Loaded a pointer from memory.
 */
int __debug_let_poly(Context *contextp, int label, void *blockp, int index, void *p)
{
    fprintf(stderr, "*** L%d: Let poly: 0x%08lx\n", label, (unsigned long) p);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Store a pointer in memory.
 */
int __debug_set_pointer(Context *contextp, int label, void *blockp, int index, void *p)
{
    fprintf(stderr, "*** L%d: Set pointer: 0x%08lx\n", label, (unsigned long) p);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Store a pointer in memory.
 */
int __debug_set_function(Context *contextp, int label, void *blockp, int index, void *p)
{
    fprintf(stderr, "*** L%d: Set function: 0x%08lx\n", label, (unsigned long) p);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Loaded a pointer from memory.
 */
int __debug_pre_let_pointer(Context *contextp, int label, void *blockp, int index)
{
    fprintf(stderr, "*** L%d: Pre-let pointer\n", label);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Loaded a pointer from memory.
 */
int __debug_pre_let_function(Context *contextp, int label, void *blockp, int index)
{
    fprintf(stderr, "*** L%d: Pre-let function\n", label);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Loaded a poly from memory.
 */
int __debug_pre_let_poly(Context *contextp, int label, void *blockp, int index)
{
    fprintf(stderr, "*** L%d: Pre-let poly\n", label);
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Store a pointer in memory.
 */
int __debug_pre_set_pointer(Context *contextp, int label, void *blockp, int index, void *p)
{
    fprintf(stderr, "*** L%d: Pre-set pointer: 0x%08lx, block[%ld]\n", label, (unsigned long) p, GET_BLOCK_INDEX(p));
    print_block_index(contextp, blockp, index);
    return 0;
}

/*
 * Store a pointer in memory.
 */
int __debug_pre_set_function(Context *contextp, int label, void *blockp, int index, void *p)
{
    fprintf(stderr, "*** L%d: Pre-set function: 0x%08lx, function[%ld]\n", label, (unsigned long) p, GET_FUNCTION_INDEX(p));
    print_block_index(contextp, blockp, index);
    return 0;
}

/************************************************************************
 * SEG FAULT
 ************************************************************************/


/*
 * Seg fault handlers.
 */
static void print_seg_fault_banner(Context *contextp, const char *ty)
{
    fprintf(stderr, "MC: Segmentation fault (%s)\n", ty);
}


int __seg_fault_other()
{
    fprintf(stderr, "*** MC: Segmentation fault\n");
    fprintf(stderr, "Unknown error\n");
    abort();
}


/* index_fault is called when a hashed index cannot be converted to real
   pointer because the hashed index is out of bounds of the ptable. */
int __seg_fault_index(Context *contextp, unsigned int hashed_index, unsigned int unhashed_index)
{
    print_seg_fault_banner(contextp, "index");
    fprintf(stderr, "Illegal pointer index: 0x%08x\n", unhashed_index);
    fprintf(stderr, "Original index value (before hash): 0x%08x\n", hashed_index);
    if(hashed_index == 0)
        fprintf(stderr, "This was probably a NULL value.\n");
    hashed_index ^= HASH_INDEX_FUN ^ HASH_INDEX;
    if(hashed_index < FUNCTION_SIZE)
        fprintf(stderr, "This is probably function pointer %d.\n", hashed_index >> 2);
    fprintf(stderr, "This probably occurred while trying\n");
    fprintf(stderr, "to read a pointer value from memory.\n\n");
    print_context(stderr, contextp);
    fprintf(stderr, "\n");
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* fun_index_fault is called when a hashed index cannot be converted to
   a real function pointer because it is out of bounds of the ftable. */
int __seg_fault_fun_index(Context *contextp, unsigned int hashed_index, unsigned int unhashed_index)
{
    print_seg_fault_banner(contextp, "fun_index");
    fprintf(stderr, "Illegal function index: 0x%08x\n", unhashed_index);
    fprintf(stderr, "Original index value (before hash): 0x%08x\n", hashed_index);
    if(hashed_index == 0)
        fprintf(stderr, "This was probably a NULL value.\n");
    hashed_index ^= HASH_INDEX_FUN ^ HASH_INDEX;
    if(hashed_index < contextp->pointer_size)
        fprintf(stderr, "This is probably block/aggr pointer %d.\n", hashed_index >> 2);
    fprintf(stderr, "This probably occurred while trying\n");
    fprintf(stderr, "to read a function pointer from memory.\n\n");
    print_context(stderr, contextp);
    fprintf(stderr, "\n");
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* fun_arity_tag_fault is called when a function pointer has an arity
   tag that is not compatible with the tag we were expecting. */
int __seg_fault_fun_arity_tag(Context *contextp, unsigned int hashed_index, unsigned int unhashed_index,
                              unsigned int expected_tag, unsigned int actual_tag)
{
    print_seg_fault_banner(contextp, "fun_arity_tag");
    fprintf(stderr, "Illegal arity tag on function pointer: 0x%08x\n", unhashed_index);
    fprintf(stderr, "Original index value (before hash): 0x%08x\n", hashed_index);
    if(hashed_index == 0)
        fprintf(stderr, "This was probably a NULL value.\n");
    hashed_index ^= HASH_INDEX_FUN ^ HASH_INDEX;
    if(hashed_index < contextp->pointer_size)
        fprintf(stderr, "This is probably block/aggr pointer %d.\n", hashed_index >> 2);
    fprintf(stderr, "Expected arity tag at this point:     0x%08x\n", expected_tag);
    fprintf(stderr, "Actual arity tag on function pointer: 0x%08x\n", actual_tag);
    fprintf(stderr, "This probably occurred while trying\n");
    fprintf(stderr, "to read a function pointer from memory.\n\n");
    print_context(stderr, contextp);
    fprintf(stderr, "\n");
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* pointer_index_fault is called when a hashed index is a valid index
   into the ptable, but the entry at that location is an empty entry
   (it has the EMPTY_BIT set). */
int __seg_fault_pointer_index(Context *contextp, unsigned int hashed_index,
                              unsigned int unhashed_index, unsigned long pointer_value)
{
    int j;

    print_seg_fault_banner(contextp, "pointer_index");
    fprintf(stderr, "Reference to free value 0x%08lx in pointer table\n", pointer_value);
    fprintf(stderr, "Hashed index: 0x%08x,  unhashed index: 0x%08x\n", hashed_index, unhashed_index);
    fprintf(stderr, "Pointer table entry number: 0x%08x\n\n", unhashed_index / sizeof(int));
    print_context(stderr, contextp);
    fprintf(stderr, "\nPointer table\n");
    for(j = 0; j <= unhashed_index / sizeof(int); j++) {
        fprintf(stderr, "0x%08lx: pointer_base[0x%08x] = 0x%08lx\n",
                (unsigned long) (contextp->pointer_base + j), j * 4, (unsigned long) contextp->pointer_base[j]);
    }
    fprintf(stderr, "\n");
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* bounds_fault is called when we attempt to dereference into a block
   but we either overflow (past end of block) or underflow (into header
   or before beginning of block). */
int __seg_fault_bounds(Context *contextp, void *block, unsigned int deref_start,
                       unsigned int deref_stop, unsigned int block_size)
{
    print_seg_fault_banner(contextp, "bounds");
    fprintf(stderr, "Index out of bounds.\nDesired start offset %d (0x%08x)\nDesired stop offset %d (0x%08x)\n",
            deref_start, deref_start, deref_stop, deref_stop);
    fprintf(stderr, "Block 0x%08lx has size %d (0x%x)\n", (unsigned long)block, block_size, block_size);
    fprintf(stderr, "\n");
    print_context(stderr, contextp);
    fprintf(stderr, "\n");
    print_block_reference(contextp, block);
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* lower_bounds_fault is called when we attempt to dereference into a block
   but we underflowed (into header or before beginning of block). */
int __seg_fault_lower_bounds(Context *contextp, void *block, unsigned int deref_start,
                             unsigned int block_size)
{
    print_seg_fault_banner(contextp, "lower_bounds");
    fprintf(stderr, "Index out of bounds: desired start offset %d\n", deref_start);
    fprintf(stderr, "Block 0x%08lx has size %d\n", (unsigned long)block, block_size);
    fprintf(stderr, "\n");
    print_context(stderr, contextp);
    fprintf(stderr, "\n");
    print_block_reference(contextp, block);
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* upper_bounds_fault is called when we attempt to dereference into a block
   but we overflowed (past the end of the block). */
int __seg_fault_upper_bounds(Context *contextp, void *block, unsigned int deref_stop,
                             unsigned int block_size)
{
    print_seg_fault_banner(contextp, "upper_bounds");
    fprintf(stderr, "Index out of bounds: desired stop offset %d\n", deref_stop);
    fprintf(stderr, "Block 0x%08lx has size %d\n", (unsigned long)block, block_size);
    fprintf(stderr, "\n");
    print_context(stderr, contextp);
    fprintf(stderr, "\n");
    print_block_reference(contextp, block);
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/* null_pointer_fault is called when we attempt to dereference a null
   pointer (as in a pointer that has numeric zero value).  */
int __seg_fault_null_pointer(Context *contextp) {
    print_seg_fault_banner(contextp, "null_pointer");
    fprintf(stderr, "\n");
    print_context(stderr, contextp);
    if(__core_debug_flags & CORE_DEBUG_VERBOSE_FAULT) {
        print_dump(contextp);
    }
    abort();
}


/************************************************************************
 * GLOBAL FUNS
 ************************************************************************/

/*
 * FC Utilities.
 */
int __ext_fc_print_block(Context *contextp, Pointer *p, unsigned i)
{
    printf("*** print_block: offset 0x%08x\n", i);
    print_block(contextp, p);
    return 0;
}

void __ext_uncaught_exception(Context *contextp, Pointer *p)
{
    unsigned int index = (unsigned int) (GET_FIELD(p, 0) ^ HASH_INDEX) >> INDEX_SHIFT;
    if(index < contextp->pointer_size) {
        p = contextp->pointer_base[index];
        if(IS_BIT_CLEARED(p, PTBL_FREE_OFF)) {
            fprintf(stderr, "Uncaught exception: %s\n", (char *) p);
            exit(2);
        }
    }
    fprintf(stderr, "Uncaught exception: (unknown)\n");
    exit(2);
}

/************************************************************************
 * VARARGS
 ************************************************************************/

/*
 * The first argument is a pointer to
 * a string telling which of the remaing args are pointers.
 */
value __format_stdargs(Context *contextp, void *fromp, unsigned long from_off, unsigned long *top)
{
    unsigned long size, index, field_index, offset;
    unsigned char *fmtp;
    void *blockp;

    /* Get the argument format string */
    field_index = ((unsigned long *)(fromp + from_off))[0] ^ HASH_INDEX;
    offset = ((unsigned long *)(fromp + from_off))[1];
    fmtp = contextp->pointer_base[field_index >> 2] + offset;

    /* Now collect the remaining args */
    size = GET_AGGR_SIZE(fromp);
    index = from_off + 8;
    while(index < size && *fmtp) {
        switch(*fmtp) {
        case 'i':
            /* Normal integer */
            *top++ = ((unsigned long *)(fromp + index))[0];
            index += 4;
            break;

        case 'p':
            /* String */
            field_index = ((unsigned long *)(fromp + index))[0] ^ HASH_INDEX;
            offset = ((unsigned long *)(fromp + index))[1];
            blockp = contextp->pointer_base[field_index >> 2];
            *top++ = (unsigned long) (blockp + offset);
            index += 8;
            break;

        default:
            /* Don't know */
            fprintf(stderr, "__format_stdargs: illegal argument type '%c'\n", *fmtp);
            break;
        }
        fmtp++;
    }
    return 0;
}
