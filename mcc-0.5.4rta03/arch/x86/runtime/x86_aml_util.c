/*
 * X86 runtime utility functions for AML.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "x86_context.h"
#include "x86_runtime.h"
#include "x86_alloc.h"

/*
 * Printers.
 */
value __ext_aml_print_int(Context *contextp, value i)
{
    printf("%d", Int_val(i));
    return Val_unit;
}

value __ext_aml_print_string(Context *contextp, value p)
{
    printf("%s", (char *) p);
    return Val_unit;
}

value __ext_aml_exit(Context *contextp, value p)
{
    exit(Int_val(p));
}

/*
 * String comparison.
 * This returns a Boolean.
 */
value __ext_aml_string_equal(Context *contextp, value s1, value s2)
{
    int flag, len1, len2;

    len1 = string_length(s1);
    len2 = string_length(s2);
    if(len1 != len2)
        return Val_int(0);
    flag = memcmp(s1, s2, len1) == 0 ? 1 : 0;
    return Val_int(flag);
}

/*
 * String concatenation.
 */
value __ext_aml_strcat(Context *contextp, value s1, value s2)
{
    int len_raw, len_rnd, len1, len2, size;
    value blockp;

    /* Compute the new length */
    len1 = string_length(s1);
    len2 = string_length(s2);
    len_raw = len1 + len2;
    len_rnd = STRING_BYTES(len_raw);

    /* Allocate the new block */
    blockp = alloc_aggr_block(contextp, len_rnd);
    memcpy((char *) blockp, s1, len1);
    memcpy((char *) blockp + len1, s2, len2);
    size = len_rnd - len_raw;

    /* Terminate the string */
    ((char *) blockp)[len_rnd - 1] = size;
    return blockp;
}

/*
 * Block comparison.
 * This is not implemented for now.
 */
static int compare(Context *contextp, value b1, value b2)
{
    return 0;
}


/*
 * Wrapped versions.
 */
value __ext_aml_equal(Context *contextp, value b1, value b2)
{
    return compare(contextp, b1, b2) == 0 ? Val_true : Val_false;
}

value __ext_aml_nequal(Context *contextp, value b1, value b2)
{
    return compare(contextp, b1, b2) != 0 ? Val_true : Val_false;
}

value __ext_aml_lt(Context *contextp, value b1, value b2)
{
    return compare(contextp, b1, b2) < 0 ? Val_true : Val_false;
}

value __ext_aml_le(Context *contextp, value b1, value b2)
{
    return compare(contextp, b1, b2) <= 0 ? Val_true : Val_false;
}

value __ext_aml_gt(Context *contextp, value b1, value b2)
{
    return compare(contextp, b1, b2) > 0 ? Val_true : Val_false;
}

value __ext_aml_ge(Context *contextp, value b1, value b2)
{
    return compare(contextp, b1, b2) >= 0 ? Val_true : Val_false;
}

value __ext_aml_compare(Context *contextp, value b1, value b2)
{
    return Val_int(compare(contextp, b1, b2));
}

/*
 * String formatting.
 */
value __ext_aml_string_of_int(Context *contextp, value i)
{
    char buf[100];

    sprintf(buf, "%d", Int_val(i));
    return alloc_string(contextp, buf);
}

value __ext_aml_int_of_string(Context *contextp, value s)
{
    return Val_int(atoi(s));
}

value __ext_aml_string_of_float(Context *contextp, double x)
{
    char buf[100];

    sprintf(buf, "%g", x);
    return alloc_string(contextp, buf);
}

double __ext_aml_float_of_string(Context *contextp, value s)
{
    return atof(s);
}




