/*
 * Basic allocation functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#ifndef _X86_ALLOC_H
#define _X86_ALLOC_H

unsigned long alloc_index(Context *contextp, void *blockp);
void *alloc_block(Context *contextp, int words);
void *alloc_closure(Context *contextp, void *funp, void *envp);
void *alloc_aggr_block(Context *contextp, int size);
void *alloc_string(Context *contextp, const char *s);
void *alloc_tag_block(Context *contextp, BlockTagInfo *infop);
void init_tag_block(Context *contextp, void *blockp, BlockTagInfo *infop);

#endif /* _X86_ALLOC_H */
