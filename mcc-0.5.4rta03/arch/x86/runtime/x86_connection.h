/*
   x86 Migration Connection runtime
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __x86_connection_included
#define __x86_connection_included


/* Local includes */
#include "x86_runtime.h"
#include "x86_type.h"


/* Protocol Definitions */
#define  PROCESS_VERSION_STRING  "migrated-process-0.3"
#define  DEFAULT_SERVER_PORT     12354


/* There are several migration protocols that can be used; these
   determine whether we are migrating to a host, or checkpointing,
   or even suspending our program state for later resurrection. */
typedef enum _migrate_type {
   MIGRATE_TO_HOST,        /* host:// protocol */
   MIGRATE_SUSPEND,        /* suspend:// protocol */
   MIGRATE_CHECKPOINT      /* checkpoint:// protocol */
} migrate_type;


/* Describes the migration connection. */
typedef struct _migrate_connection {
   migrate_type protocol;  /* Which protocol are we using? */
   char name[0x1000];      /* Destination host/filename */
   int port;               /* Destination port (for network) */
   int io;                 /* IO channel description number */
   bool bidir;             /* True if channel is bidirectional */
} migrate_connection;


/* Setup the migration connection based on the URL-style descriptor
   we were given.  Returns zero on success, -1 if the input URL
   could not be parsed.  Results are stored in conn.  */
int setup_connection(migrate_connection *conn, const char *dest);


/* Open a new connection */
int open_source_connection(migrate_connection *conn);


/* Configure a connection on the destination side */
int open_dest_connection(migrate_connection *conn, Migrate *m);


/* Close an existing connection */
void close_connection(migrate_connection *conn);


/* Send a sized buffer out */
int send_buffer(migrate_connection *conn, const char *buf, int size);


/* Receive/read a sized buffer in */
int recv_buffer(migrate_connection *conn, char *buf, int size);


/* Send a string over the network */
int send_string(migrate_connection *conn, const char *msg);


/* Receive a string from the network */
int recv_string(migrate_connection *conn, char *buffer, int size);


/* Send an integer over the network */
int send_int32(migrate_connection *conn, unsigned int value);


/* Receive an int32 value */
int recv_int32(migrate_connection *conn, unsigned int *value);


/* Send an integer block over the network, INCLUDING size parametre */
int send_int32_block(migrate_connection *conn, const unsigned int *values, int size);


/* Receive an integer block over the network */
int recv_int32_block(migrate_connection *conn, unsigned int *values, int size);


#endif /* __x86_connection_included */
