/*
 * Equality and comparison operators on values in the heap.
 * Only pertially implemented.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>

#include "x86_runtime.h"

/*
 * Compare two values.
 */
value __ext_ml_cmp(Context *contextp, value left, value right)
{
    int result;

    if(IS_ML_INT(left)) {
        int i = Int_val(left);
        if(IS_ML_INT(right)) {
            int j = Int_val(right);

            if(i < j)
                result = -1;
            else if(i > j)
                result = 1;
            else
                result = 0;
        }
        else
            result = -1;
    }
    else if(IS_ML_INT(right))
        result = 1;
    else {
        fprintf(stderr, "__cmp: non-integer values not implemented:  left: 0x%08x  right: 0x%08x\n",
                (unsigned)left, (unsigned)right);
        exit(1);
    }

    return Val_int(result);
}
