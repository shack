/*
   x86 Migration Connection runtime
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


/* System includes */
#include <assert.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


/* Local includes */
#include "x86_connection.h"
#include "x86_net.h"


/* Protocol names */
static const char *host_proto = "host://";
static const char *suspend_proto = "suspend://";
static const char *checkpoint_proto = "checkpoint://";


/***  Connection Setup  ***/


/* Setup the migration connection based on the URL-style descriptor
   we were given.  Returns zero on success, -1 if the input URL
   could not be parsed.  Results are stored in conn.  */
int setup_connection(migrate_connection *conn, const char *dest) {

   char *p;
   int size;

   assert(conn != NULL);
   assert(dest != NULL);

   /* Check which protocol we should use */
   if(strncmp(dest, host_proto, strlen(host_proto)) == 0) {
      conn->protocol = MIGRATE_TO_HOST;
      /* Check for a port number */
      if((p = strchr(dest + strlen(host_proto), ':')) == NULL) {
         /* No port number specified */
         strncpy(conn->name, dest + strlen(host_proto), sizeof(conn->name));
         conn->port = DEFAULT_SERVER_PORT;
      } else {
         /* Copy only part up to the colon */
         size = p - (dest + strlen(host_proto));
         if(size > sizeof(conn->name)) size = sizeof(conn->name);
         strncpy(conn->name, dest + strlen(host_proto), size);
         /* Extract port number */
         conn->port = atoi(p + 1);
      } /* Port number given? */
   } else if(strncmp(dest, suspend_proto, strlen(suspend_proto)) == 0) {
      conn->protocol = MIGRATE_SUSPEND;
      strncpy(conn->name, dest + strlen(suspend_proto), sizeof(conn->name));
   } else if(strncmp(dest, checkpoint_proto, strlen(checkpoint_proto)) == 0) {
      conn->protocol = MIGRATE_CHECKPOINT;
      strncpy(conn->name, dest + strlen(checkpoint_proto), sizeof(conn->name));
   } else {
      /* Unrecognized protocol name; guessing old-form protocol */
      fprintf(stderr, "migrate:  Unrecognized protocol:  \"%s\"\n", dest);
      fprintf(stderr, "migrate:  Guessing this is the old-form protocol\n");
      conn->protocol = MIGRATE_TO_HOST;
      strncpy(conn->name, dest, sizeof(conn->name));
      conn->port = DEFAULT_SERVER_PORT;
   }

   /* No known IO channels (yet) */
   conn->io = -1;
   conn->bidir = false;

   /* Make sure name is null-terminated */
   conn->name[sizeof(conn->name) - 1] = '\0';

   /* Verify name is not entirely bogus */
   if(*conn->name == '\0') {
      fprintf(stderr, "migrate:  Name is empty, cannot migrate\n");
      return(-1);
   }

   /* Verify port number */
   if(conn->protocol == MIGRATE_TO_HOST && (conn->port <= 0 || conn->port >= 0x10000)) {
      fprintf(stderr, "migrate:  Invalid port number, cannot migrate\n");
      return(-1);
   }

   /* Return success */
   return(0);

}


/***  Dispatch Functions for Open/Close connection  ***/


/* Open a new source connection */
int open_source_connection(migrate_connection *conn) {

   assert(conn != NULL);

   /* Branch on the possible connection types */
   switch(conn->protocol) {
   case MIGRATE_TO_HOST:
      conn->io = net_open_connection(conn->name, conn->port);
      if(conn->io < 0) return(-1);
      conn->bidir = true;
      break;
   case MIGRATE_SUSPEND:
   case MIGRATE_CHECKPOINT:
      conn->io = open(conn->name, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
      if(conn->io < 0) {
         perror("open_source_connection");
         return(-1);
      }
      conn->bidir = false;
      break;
   }

   /* We were successful */
   return(0);

}


/* Configure a connection on the destination side */
int open_dest_connection(migrate_connection *conn, Migrate *m) {

   assert(conn != NULL);

   /* Branch on the possible connection types */
   if(m->bidir) {
      /* Socket connection */
      conn->protocol = MIGRATE_TO_HOST;
   } else {
      /* Reading from a file; we need to seek to the initial
         offset we are supposed to be reading from. */
      conn->protocol = MIGRATE_SUSPEND;
      if(lseek(m->channel, m->initofs, SEEK_SET) != m->initofs) {
         perror("open_dest_connection");
         return(-1);
      }
   }

   /* Setup IO data members */
   conn->io = m->channel;
   conn->bidir = m->bidir;
   return(0);

}


/* Close an existing connection */
void close_connection(migrate_connection *conn) {

   assert(conn != NULL);

   /* Branch on the possible connection types */
   switch(conn->protocol) {
   case MIGRATE_TO_HOST:
      shutdown(conn->io, 2);
      close(conn->io);
      break;
   case MIGRATE_SUSPEND:
   case MIGRATE_CHECKPOINT:
      close(conn->io);
      break;
   }

}


/***  IO/Communication  ***/


/* Send a sized buffer out */
int send_buffer(migrate_connection *conn, const char *buf, int size) {

   int sent;         /* Amount of data transmitted */

   assert(conn != NULL);
   assert(buf != NULL);
   assert(size >= 0);

   switch(conn->protocol) {
   case MIGRATE_TO_HOST:
      return(net_send_buffer(conn->io, buf, size));
      break;
   case MIGRATE_SUSPEND:
   case MIGRATE_CHECKPOINT:
      sent = write(conn->io, buf, size);
      if(sent < 0) {
         perror("send_buffer");
         return(-1);
      } else if(sent < size) {
         fprintf(stderr, "send_buffer:  Failed to transmit all required data\n");
         return(-1);
      }
      return(0);
   }

   /* Something went wrong */
   fprintf(stderr, "send_buffer:  Bogus protocol, aborting.\n");
   return(-1);

}


/* Receive/read a sized buffer in */
int recv_buffer(migrate_connection *conn, char *buf, int size) {

   int rcvd;         /* Amount of data received */

   assert(conn != NULL);
   assert(buf != NULL);
   assert(size >= 0);

   switch(conn->protocol) {
   case MIGRATE_TO_HOST:
      return(net_recv_buffer(conn->io, buf, size));
      break;
   case MIGRATE_SUSPEND:
   case MIGRATE_CHECKPOINT:
      rcvd = read(conn->io, buf, size);
      if(rcvd < 0) {
         perror("recv_buffer");
         return(-1);
      } else if(rcvd < size) {
         fprintf(stderr, "recv_buffer:  Failed to receive all expected data\n");
         return(-1);
      }
      return(0);
   }

   /* Something went wrong */
   fprintf(stderr, "recv_buffer:  Bogus protocol, aborting.\n");
   return(-1);

}


/* Send a string over the network */
int send_string(migrate_connection *conn, const char *msg) {

   int count = strlen(msg);      /* Number of bytes in msg */
   char buf[count + 2];          /* Real buffer to send */

   assert(conn != NULL);
   assert(msg != NULL);

   /* Construct the buffer message */
   snprintf(buf, sizeof(buf), "%s\n", msg);
   ++count;

   /* Send the string as a buffer */
   return(send_buffer(conn, buf, count));

}


/* Receive a string from the network */
int recv_string(migrate_connection *conn, char *buffer, int size) {

   char *p = buffer;             /* Pointer into result buffer */

   assert(conn != NULL);
   assert(buffer != NULL);
   assert(size >= 0);

   /* Receive some data */
   memset(buffer, '\0', size);
   while((p == buffer || *(p - 1) != '\n') && p - buffer < size - 1) {
      if(recv_buffer(conn, p, 1) < 0) return(-1);
      ++p;
   }
   if(p > buffer && *(p - 1) == '\n') *(p - 1) = '\0';

   /* Success! */
   return(0);

}


/* Send an integer over the network */
int send_int32(migrate_connection *conn, unsigned int value) {

   assert(conn != NULL);
   value = htonl(value);
   return(send_buffer(conn, (char *)&value, sizeof(unsigned int)));

}


/* Receive an int32 value */
int recv_int32(migrate_connection *conn, unsigned int *value) {

   assert(conn != NULL);
   assert(value != NULL);
   if(recv_buffer(conn, (char *)value, sizeof(unsigned int)) != 0) return(-1);
   *value = ntohl(*value);
   return(0);

}


/* Send an integer block over the network, INCLUDING size parametre */
int send_int32_block(migrate_connection *conn, const unsigned int *values, int size) {

   assert(conn != NULL);
   assert(values != NULL);
   assert(size >= 0);
   if(send_int32(conn, size) < 0) return(-1);
   while(size > 0) {
      if(send_int32(conn, *values) < 0) return(-1);
      ++values;
      --size;
   }
   return(0);

}


/* Receive an integer block over the network */
int recv_int32_block(migrate_connection *conn, unsigned int *values, int size) {

   assert(conn != NULL);
   assert(values != NULL);
   assert(size >= 0);
   while(size > 0) {
      if(recv_int32(conn, values) < 0) return(-1);
      ++values;
      --size;
   }
   return(0);

}


