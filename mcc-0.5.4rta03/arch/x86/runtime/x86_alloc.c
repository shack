/*
 * Basic allocation functions.
 * These functions assume that the heap has enough space.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#include "x86_runtime.h"
#include "x86_alloc.h"


/*
 * Allocate an entry in the pointer table.
 */
unsigned long alloc_index(Context *contextp, void *blockp)
{
    unsigned long index;
    void *nextp;

    /* Allocate an index */
    index = contextp->pointer_next - contextp->pointer_base;
    nextp = *contextp->pointer_next;
    nextp = CLEAR_BIT(nextp, PTBL_FREE_OFF);
    *contextp->pointer_next = blockp;
    contextp->pointer_next = nextp;
    contextp->pointer_free--;
    return index;
}


/*
 * Allocate a block with tag no initialization data
 * this assumes that there is enough space on the heap.
 */
void *alloc_block(Context *contextp, int words)
{
    unsigned long index, size;
    void *blockp;

    /* Allocate block */
    size = words << 2;
    blockp = contextp->mem_next + BLOCK_HEADER_SIZE;
    index = alloc_index(contextp, blockp);

    /* Setup the block header */
    SET_BLOCK_HEADER(blockp, 0, index, size);

    /* Adjust memory pointers */
    contextp->mem_next = blockp + size;
    return blockp;
}


/*
 * Allocate an aggregate (tag-127) block.
 * this assumes that there is enough space on the heap.
 */
void *alloc_aggr_block(Context *contextp, int size)
{
    void *blockp;
    int index;

    /* Round up the size */
    size = SIZEUP(size);

    /* Allocate block */
    blockp = contextp->mem_next + BLOCK_HEADER_SIZE;
    index = alloc_index(contextp, blockp);

    /* Update header */
    SET_AGGR_HEADER(blockp, index, size);

    /* Adjust memory pointers */
    contextp->mem_next = blockp + size;
    return blockp;
}


/*
 * Allocate a block with tag and initialization data
 * this assumes that there is enough space on the heap.
 */
void *alloc_tag_block(Context *contextp, BlockTagInfo *infop)
{
    unsigned long index, size;
    int *blockp;

    /* Allocate block */
    size = infop->size << 2;
    blockp = contextp->mem_next + BLOCK_HEADER_SIZE;
    index = alloc_index(contextp, blockp);

    /* Setup the block header */
    SET_BLOCK_HEADER(blockp, infop->tag, index, size);

    /* Adjust memory pointers */
    contextp->mem_next = blockp + size;
    return blockp;
}


/*
 * Initialize the block.
 */
void init_tag_block(Context *contextp, void *blockp, BlockTagInfo *infop)
{
    int *src_data, *dst_data;
    int i, index;
    void *funp;

    /* Initialize the memory space */
    src_data = (int *) infop->datap;
    dst_data = (int *) blockp;
    for(i = 0; i < infop->size; i++) {
        int kind = *src_data++;
        switch (kind) {
        case INIT_VAL_LONG_DOUBLE:
            *dst_data++ = *src_data++;
            /* fallthrough */
        case INIT_VAL_DOUBLE:
            *dst_data++ = *src_data++;
            /* fallthrough */
        case INIT_VAL_SINGLE:
        case INIT_VAL_INT32:
            *dst_data++ = *src_data++;
            break;
        case INIT_VAL_FUNCTION:
            index = *src_data++;
            funp = ((void **) &__function_base)[index];
            index = GET_FUNCTION_INDEX(funp) << INDEX_SHIFT;
            *dst_data++ = index;
            break;
        case INIT_VAL_GLOBAL:
            index = *src_data++;
            index = GET_BLOCK_INDEX(Global(contextp, index)) << INDEX_SHIFT;
            *dst_data++ = index;
            break;
        default:
            fprintf(stderr, "Illegal global value initializer\n");
            abort();
        }
    }
}


/*
 * Allocate a string.
 */
void *alloc_string(Context *contextp, const char *s)
{
    char *strp;
    int len, total;

    len = strlen(s);
    total = STRING_BYTES(len);
    strp = (char *) alloc_aggr_block(contextp, total);
    memcpy(strp, s, len + 1);
    /* TEMP JDS BUG:
     * Jason, I think I know what you were trying to accomplish here,
     * but this code is flat out wrong; it clobbers the null terminator
     * if (len mod 4 == 3).  If you really want to have a marker that
     * indicates the real size of the block, then STRING_BYTES needs to
     * account for one more byte of storage.
    strp[total - 1] = total - len;
     */
    return strp;
}


/*
 * Allocate a closure.
 */
void *alloc_closure(Context *contextp, void *funp, void *envp)
{
    void *blockp;

    blockp = alloc_block(contextp, 2);
    SET_FIELD(blockp, 0, GET_BLOCK_INDEX(envp));
    SET_FIELD(blockp, 1, GET_FUNCTION_INDEX(funp));
    return blockp;
}
