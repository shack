/*
 * Glue functions for X86 code.
 *
 * Exit function gets an argument in %ebx,
 * and it returns that value to the caller function.
 */

/*
 * Floating-point control word
 */
   .data
   .align   4
   .global  __fpu_control_word
__fpu_control_word:
   .long    0

/*
 * __exit is called as the final continuation.
 */
   .text
   .align   64
   .skip    52
   .long    __exit.arity   # arity tag
   .long    0x0            # block size
   .long    __exit.tag     # closure tag
   .global  __exit
__exit:
   movl  %ebx, %eax
   popl  %ebp
   popl  %edi
   popl  %esi
   popl  %edx
   popl  %ecx
   popl  %ebx
   ret

/*
 * __uncaught_exception is called as the final continuation.
 */
   .text
   .align   64
   .skip    52
   .long    __uncaught_exception.arity # arity tag
   .long    0x0                        # block size
   .long    __uncaught_exception.tag   # closure tag
   .global  __uncaught_exception
__uncaught_exception:
   pushl %ebx
   pushl %ebp
   call __ext_uncaught_exception

/*
 * _exit can be used in a LetExternal call.
 */
   .global  __ext_exit
__ext_exit:
   /* Ignore the base pointer and environment */
   addl  $8, %esp
   /* Next member on the stack will contain return code */
   popl  %ebx
   /* Restore remaining state */
   jmp   __exit

/*
 * Garbage collector pushes all the registers,
 * and the address of the calling function.
 */
   .global  __gc
__gc:
   pushl %ebp
   pushl %edi
   pushl %esi
   pushl %edx
   pushl %ecx
   pushl %ebx
   pushl %eax
   pushl %esp
   call  gc
   addl  $4,%esp
   popl  %eax
   popl  %ebx
   popl  %ecx
   popl  %edx
   popl  %esi
   popl  %edi
   popl  %ebp
   ret

/*
 * Copy-on-write does the same thing GC does
 * above.  Sigh, I hate copy-and-paste code...
 */
   .global  __copy_on_write
__copy_on_write:
   pushl %ebp
   pushl %edi
   pushl %esi
   pushl %edx
   pushl %ecx
   pushl %ebx
   pushl %eax
   pushl %esp
   call  copy_on_write
   addl  $4,%esp
   popl  %eax
   popl  %ebx
   popl  %ecx
   popl  %edx
   popl  %esi
   popl  %edi
   popl  %ebp
   ret
