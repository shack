/*
 * Runtime for x86.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001,2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "x86_runtime.h"
#include "x86_context.h"
#include "x86_alloc.h"
#include "x86_gc.h"
#include "x86_atomic.h"


/************************************************************************
 * ALLOCATION
 ************************************************************************/


/*
 * copy the rawdata block into the globals.
 */
static void copy_rawdata(Context *contextp, int index, BlockInfo *infop)
{
    memcpy(&Global(contextp, index), infop->datap, infop->size);
}


/*
 * Allocate a rawdata block.
 */
static void *alloc_rawdata(Context *contextp, BlockInfo *infop)
{
    void *blockp;

    blockp = alloc_aggr_block(contextp, infop->size);
    memcpy(blockp, infop->datap, infop->size);
    return blockp;
}


/*
 * Allocate all the arguments.
 */
static void *alloc_argv_fc(Context *contextp, int argc, char **argv)
{
    void *argvp, *strp;
    int i;

    /* Allocate arguments */
    argvp = alloc_aggr_block(contextp, (SIZEOF_POINTER + SIZEOF_INT) * argc);
    for(i = 0; i != argc; i++) {
        strp = alloc_string(contextp, argv[i]);
        SET_FIELD(argvp, i * 2, HASH_AGGR_INDEX(GET_BLOCK_INDEX(strp)));
        SET_FIELD(argvp, i * 2 + 1, 0);
    }

    /* BUG: put a nil pointer at the end */
    return argvp;
}


/*
 * Allocate all the arguments.
 */
static void *alloc_argv_java(Context *contextp, int argc, char **argv)
{
    void *argvp, *strp;
    int i;

    /* Allocate arguments */
    argvp = alloc_aggr_block(contextp, SIZEOF_POINTER * argc);
    for(i = 0; i != argc; i++) {
        strp = alloc_string(contextp, argv[i]);
        SET_FIELD(argvp, i, GET_BLOCK_INDEX(strp) << INDEX_SHIFT);
    }
    return argvp;
}


/*
 * Use the information in globals to initialize a context.
 */
static Context *alloc_context(unsigned int ptr_size, unsigned int heap_size)
{
    int i, function_count, context_size;
    void **p, **roots, *funp;
    unsigned long index;
    Context *contextp;
    GlobalDesc *descp;
    GCInfo *gc_info;

    /* Allocate space for the context */
    context_size = CONTEXT_SIZE;
    contextp = (Context *) malloc(context_size);
    if(contextp == 0) {
        fprintf(stderr, "Can't allocate context\n");
        exit(2);
    }
    /* this is just for debugging initialization */
    memset(contextp, 0xaa, context_size);

    /* Allocate the pointer table */
    contextp->pointer_base = (void **) malloc(ptr_size * SIZEOF_POINTER);
    if(contextp->pointer_base == 0) {
        fprintf(stderr, "Can't allocate pointer table\n");
        exit(2);
    }

    /* Copy the function pointers into the initial pointer table */
    function_count = FUNCTION_SIZE / sizeof(void *);
    for(i = 0; i != function_count; i++) {
        funp = __function_base[i];
        index = GET_FUNCTION_INDEX(funp);
        contextp->pointer_base[index] = funp;
    }

    /* Link the pointer area */
    p = contextp->pointer_base + function_count;
    for(i = 0; i != ptr_size - function_count - 1; i++) {
        *p = SET_BIT(p + 1, PTBL_FREE_OFF);
        p++;
    }
    *p = (void *) 1;

    /* Initialize the pointer area */
    contextp->pointer_next = contextp->pointer_base + function_count;
    contextp->pointer_free = ptr_size - function_count - 1;
    contextp->pointer_size = ptr_size << 2;
    contextp->pointer_limit = contextp->pointer_base + ptr_size;

    /* Initialize the GC area */
    gc_info = (GCInfo *) malloc(sizeof(GCInfo));
    if(gc_info == 0) {
        fprintf(stderr, "Can't allocate GCInfo\n");
        abort();
    }
    gc_info->gc_level = 0;
    gc_info->major_live = 0;
    gc_info->minor_live = 0;
    gc_info->major_count = 0;
    gc_info->minor_count = 0;
    gc_info->gc_abs_time = 0;
    gc_info->gc_cpu_time = 0;
    contextp->gc_info = gc_info;

    /* Initialize rollback data */
    contextp->atomic_size = MIN_ATOMIC_SIZE;
    contextp->atomic_info = (Atomic **) malloc(MIN_ATOMIC_SIZE * sizeof(Atomic *));
    if(contextp->atomic_info == 0) {
        fprintf(stderr, "Can't allocate storage for atomic info\n");
        abort();
    }
    contextp->atomic_next = 0;

    /* Set up the minor roots */
    roots = (void **) malloc(ptr_size * sizeof(void *));
    if(roots == 0) {
        fprintf(stderr, "Can't allocate space for minor roots\n");
        abort();
    }
    contextp->minor_roots = roots;
    contextp->next_root = roots;

    /*
     * Allocate the heap.
     * This should be the very final allocation
     * so that it is allocated as the last memry area
     * in the process.
     */
    contextp->mem_base = malloc(heap_size);
    if(contextp->mem_base == 0) {
        fprintf(stderr, "Can't allocate heap\n");
        abort();
    }
    contextp->mem_size = heap_size;
    contextp->mem_next = contextp->mem_base;
    contextp->mem_limit = contextp->mem_base + contextp->mem_size;

    /*
     * Allocate the globals area.
     * The globals are allocated on the major heap.
     */
    for(i = 0; i != GLOBALS_SIZE; i++) {
        descp = __globals_base + i;
        index = descp->index;
        switch(descp->kind) {
        case RAWDATA_GLOBAL:
            Global(contextp, index) = alloc_rawdata(contextp, (BlockInfo *) descp->infop);
            break;
        case TAGBLOCK_GLOBAL:
            Global(contextp, index) = alloc_tag_block(contextp, (BlockTagInfo *) descp->infop);
            break;
        case AGGRBLOCK_GLOBAL:
            Global(contextp, index) = alloc_aggr_block(contextp, (int) descp->infop);
            break;
        case RAWINT_GLOBAL:
        case RAWFLOAT_GLOBAL:
            copy_rawdata(contextp, descp->index, (BlockInfo *) descp->infop);
            break;
        case VAR_GLOBAL:
            break;
        default:
            fprintf(stderr, "Illegal global initializer\n");
            exit(3);
        }
    }

    /*
     * Initialize the globals area.
     * The globals are allocated on the major heap.
     */
    for(i = 0; i != GLOBALS_SIZE; i++) {
        descp = __globals_base + i;
        index = descp->index;
        switch(descp->kind) {
        case RAWDATA_GLOBAL:
        case AGGRBLOCK_GLOBAL:
        case RAWINT_GLOBAL:
        case RAWFLOAT_GLOBAL:
            break;
        case TAGBLOCK_GLOBAL:
            init_tag_block(contextp, Global(contextp, index), (BlockTagInfo *) descp->infop);
            break;
        case VAR_GLOBAL:
            Global(contextp, index) = Global(contextp, (int) descp->infop);
            break;
        default:
            fprintf(stderr, "Illegal global initializer\n");
            exit(3);
        }
    }

    /* Set up the minor heap */
    contextp->minor_base = contextp->mem_next;
    contextp->minor_limit = contextp->minor_base + MIN_MINOR_SIZE;

    /* Set the copy point */
    contextp->gc_point = contextp->minor_base;
    contextp->copy_point = contextp->mem_base;

    /* Done */
    return contextp;
}


/************************************************************************
 * MAIN FUNCTION
 ************************************************************************/


/*
 * Default context in case a real segfault occurs.  This is needed
 * so we have some clue of where to poke when we encounter a segfault;
 * at some point, I may revise this code so it attempts to find the
 * context in %ebp, but until we have actual thread support it is not
 * an issue.  -JDS
 */
static Context *default_contextp = NULL;


/*
 * Hard segfault handler.  These kinds of segfaults are very bad.
 * Any segfault that prints out the ``This should not have happened''
 * banner should be corrected as soon as possible.
 */
static void segv_handler(int signum, siginfo_t *infop, void *argp)
{
    unsigned long addr = (unsigned long) infop->si_addr;
    fprintf(stderr, "\n*** RAW SEGMENTATION FAULT OCCURRED\n");
    fprintf(stderr, "*** Fault address = 0x%08lx, hashed address = 0x%08lx\n", addr, addr ^ HASH_INDEX);
    fprintf(stderr, "*** This should not have happened!\n");
    print_context(stderr, default_contextp);
    abort();
}


static void init_segv_handler(Context *contextp)
{
    struct sigaction segv_action;

    memset(&segv_action, 0, sizeof(segv_action));
    segv_action.sa_sigaction = segv_handler;
    segv_action.sa_flags = SA_SIGINFO;
    sigaction(SIGSEGV, &segv_action, (struct sigaction *) 0);

    /* Set pointer to context */
    default_contextp = contextp;
}


/*
 * Main function initializes the memory areas, then jumps.
 * NOTE: this file uses the jyh style policy.  Please preserve it.
 * JDS:  fine by me, but LEAVE THE EXTRA LINE BREAKS please.
 */
int main(int argc, char **argv)
{
    Context *contextp;
    Header *headerp;
    void *argvp, *nilp, *exitp, *exnhp;
    int code, return_code;
    unsigned int heap_size;

    /* Print GC messages */
    trace_gc = getenv("TRACE_GC") != 0;
    return_code = 0;

    /* Initialise atomic core */
    atomic_init();

    /* Check migration code */
    if(__migrate_data.migrated != 0) {
        /* Create and initialize the context */
        /*
         * TEMP: Migrated programs segfault on GC unless we set the
         * real heap size at least twice as large as the amount of
         * live data...
         */
        heap_size = __migrate_data.heap_size * 2;
        if(heap_size < INITIAL_HEAP_SIZE) {
            heap_size = INITIAL_HEAP_SIZE;
        }
        contextp = alloc_context(__migrate_data.ptr_size, heap_size);

        /* Configure real segfault handler */
        init_segv_handler(contextp);

        /* Get data from socket, and restart program */
        return_code = migrate_server(contextp, &__migrate_data);

        /* Made it out alive... */
        fprintf(stderr, "MC: migrated process made it out\n");
    }
    else {
        /* Create the context */
        contextp = alloc_context(INITIAL_PTR_COUNT, INITIAL_HEAP_SIZE);

        /* Configure real segfault handler */
        init_segv_handler(contextp);

        /* Copy argv */
        nilp = alloc_aggr_block(contextp, 0);

        /* Call all initializers */
        headerp = __header_base;
        while(headerp != __header_limit) {
            if(headerp->version != MC_VERSION || headerp->context_length != CONTEXT_LENGTH) {
                fprintf(stderr, "Bogus version number\n");
                abort();
            }
            headerp->init(contextp, __exit);
            headerp++;
        }

        /* Call all main functions */
        headerp = __header_base;
        while(headerp != __header_limit) {
            if(headerp->version != MC_VERSION || headerp->context_length != CONTEXT_LENGTH) {
                fprintf(stderr, "Bogus version number\n");
                abort();
            }
            switch(headerp->file_class) {
            case FILE_FC:
                argvp = alloc_argv_fc(contextp, argc, argv);
                code = headerp->main.full(contextp, argc, argvp, 0, __exit, nilp, __uncaught_exception, nilp);
                break;
            case FILE_JAVA:
                argvp = alloc_argv_java(contextp, argc, argv);
                exitp = alloc_closure(contextp, (void *) __exit, nilp);
                exnhp = alloc_closure(contextp, (void *) __uncaught_exception, nilp);
                code = headerp->main.java(contextp, exitp, exnhp, argvp);
                break;
            case FILE_NAML:
                argvp = alloc_argv_java(contextp, argc, argv);
                code = headerp->main.full(contextp, argc, argvp, 0, __exit, nilp, __uncaught_exception, nilp);
                break;
            case FILE_AML:
                code = headerp->main.aml(contextp, __exit, nilp, __uncaught_exception, nilp) >> 1;
                break;
            default:
                fprintf(stderr, "Unknown file type %ld\n", headerp->file_class);
                abort();
            }
            return_code = code > return_code ? code : return_code;
            headerp++;
        }
    }

    /* Print GC stats */
    if(trace_gc) {
        struct rusage rusage;
        double user_time, sys_time, cpu_time;

        getrusage(RUSAGE_SELF, &rusage);
        user_time = rusage.ru_utime.tv_sec + rusage.ru_utime.tv_usec * 1e-6;
        sys_time = rusage.ru_stime.tv_sec + rusage.ru_stime.tv_usec * 1e-6;
        cpu_time = user_time + sys_time;

        fprintf(stderr, "*** MC: done\n");
        fprintf(stderr, "\tTotal CPU time: %.3g\n", cpu_time);
        print_stats(contextp);
    }

    /* End of program */
    fflush(stdout);
    fflush(stderr);
    if(return_code != 0) {
        fprintf(stderr, "MC: exited with code %d\n", return_code);
    }
    return return_code;
}
