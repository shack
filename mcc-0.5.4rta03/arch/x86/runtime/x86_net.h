/*
   x86 Network utilities
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __x86_net_included
#define __x86_net_included


/* Initiate a connection with the server */
int net_open_connection(const char *servername, int port);

/* Send a sized buffer over the network */
int net_send_buffer(int sock, const char *buf, int size);

/* Receive a (pre)sized buffer over the network */
int net_recv_buffer(int sock, char *buf, int size);


#endif /* __x86_net_included */
