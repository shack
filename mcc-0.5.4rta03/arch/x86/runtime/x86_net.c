/*
   x86 Network utilities
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>


/* Required include files */
#include "x86_net.h"


/***  Utilities and Protocols  ***/


/* Initiate a connection with the server */
int net_open_connection(const char *servername, int port) {

   int sock;                     /* Socket descriptor */
   struct hostent *host;         /* Name of remote host */
   struct sockaddr_in addr;      /* Server address */

   /* Determine the hostname and port data */
   host = gethostbyname(servername);
   if(host == NULL) {
      herror("net_open_connection");
      return(-1);
   }
   memset((char *)&addr, '\0', sizeof(addr));
   memcpy((char *)&addr.sin_addr, host->h_addr, host->h_length);
   addr.sin_family = host->h_addrtype;
   addr.sin_port = htons(port);

   /* Construct the socket connection */
   if((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
      perror("net_open_connection");
      return(-1);
   }

   /* Attempt a connection */
   if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
      perror("net_open_connection");
      close(sock);
      return(-1);
   }

   /* Return the new socket */
   return(sock);

}


/* Send a sized buffer over the network */
int net_send_buffer(int sock, const char *buf, int size) {

   int sent = 0;                 /* Number of bytes sent */
   int result;                   /* Result of send() call */

   /* Keep sending until full buffer is transmitted */
   while(sent < size) {
      result = send(sock, buf + sent, size - sent, 0);
      if(result < 0) {
         /* Error occurred */
         perror("net_send_buffer");
         return(-1);
      }
      sent += result;
   }

   /* Success! */
   return(0);

}


/* Receive a (pre)sized buffer over the network */
int net_recv_buffer(int sock, char *buf, int size) {

   int rcvd = 0;                 /* Number of bytes received */
   int result;                   /* Result of recv() call */

   /* Keep sending until full buffer is transmitted */
   while(rcvd < size) {
      result = recv(sock, buf + rcvd, size - rcvd, 0);
      if(result < 0) {
         /* Error occurred */
         perror("net_recv_buffer");
         return(-1);
      }
      rcvd += result;
   }

   /* Success! */
   return(0);

}


