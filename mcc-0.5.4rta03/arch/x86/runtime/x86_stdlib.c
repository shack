/*
 * X86 standard library functions.
 * Copyright (C) 2002 Justin David Smith, Caltech
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "x86_runtime.h"


/***  Global FC functions  ***/


int print_string(const char *msg) {

   printf("%s", msg);
   return 0;

}


int __ext_fc_print_string(Context *contextp, Pointer *p, int i) {

   return(print_string((char *)p + i));

}


int print_char(int i) {

   printf("%c", i);
   return 0;

}


int __ext_fc_print_char(Context *contextp, int i) {

   return(print_char(i));

}


int print_int(int i) {

   printf("%d", i);
   return 0;

}


int __ext_fc_print_int(Context *contextp, int i) {

   return(print_int(i));

}


int print_hex(int i) {

   printf("%08x", i);
   return 0;

}


int __ext_fc_print_hex(Context *contextp, int i) {

   return(print_hex(i));

}


int print_long(long long i) {

   printf("%lld", i);
   return 0;

}


int __ext_fc_print_long(Context *contextp, long long i) {

   return(print_long(i));

}


int print_long_hex(long long i) {

   printf("%016llx", i);
   return 0;

}


int __ext_fc_print_long_hex(Context *contextp, long long i) {

   return(print_long_hex(i));

}


int print_float(float x) {

   printf("%.6g", x);
   return 0;

}


int __ext_fc_print_float(Context *contextp, float x) {

   return(print_float(x));

}


int print_double(double x) {

   printf("%.16g", x);
   return 0;

}


int __ext_fc_print_double(Context *contextp, double x) {

   return(print_double(x));

}


int print_long_double(long double x) {

   printf("%.16Lg", x);
   return 0;

}


int __ext_fc_print_long_double(Context *contextp, long double x) {

   return(print_long_double(x));

}


int __ext_fc_print_string_int(Context *contextp, Pointer *p, int off, int i) {

   __ext_fc_print_string(contextp, p, off);
   __ext_fc_print_int(contextp, i);
   return 0;

}


int __ext_fc_print_string_hex(Context *contextp, Pointer *p, int off, int i) {

   __ext_fc_print_string(contextp, p, off);
   __ext_fc_print_hex(contextp, i);
   return 0;

}


int __ext_fc_print_string_long(Context *contextp, Pointer *p, int off, long long i) {

   __ext_fc_print_string(contextp, p, off);
   __ext_fc_print_long(contextp, i);
   return 0;

}


int __ext_fc_print_string_long_hex(Context *contextp, Pointer *p, int off, long long i) {

   __ext_fc_print_string(contextp, p, off);
   __ext_fc_print_long_hex(contextp, i);
   return 0;

}


int __ext_fc_print_string_float(Context *contextp, Pointer *p, int off, double x) {

   __ext_fc_print_string(contextp, p, off);
   __ext_fc_print_float(contextp, x);
   return 0;

}


int __ext_fc_flush(Context *contextp) {

   fflush(stdout);
   return 0;

}


int __ext_fc_atoi(Context *contextp, Pointer *p) {

   return atoi((char *) p);

}


double __ext_fc_atof(Context *contextp, Pointer *p) {

   return atof((char *) p);

}


void __ext_fc_exit(Context *contextp, int arg) {

   exit(arg);

}


/*
 * This is just a quick hack to return the time in microseconds
 * for performance testing.  This will wrap prematurely, but oh well.
 * --jyh, for PoPL testing.
 */
unsigned long long __ext_fc_utime(Context *contextp) {

   struct timeval tv;
   gettimeofday(&tv, NULL);
   return(((unsigned long long)tv.tv_sec) * 1000000ULL + (unsigned long long)tv.tv_usec);

}


/***  ML Utilities  ***/


/*
 * ML Utilities.
 */
value __ext_ml_print_string(Context *contextp, value p) {

   int i, size;

   size = GET_BLOCK_WORD_SIZE(p) - 1;
   for(i = 0; i < size; i++)
      putchar((int) GET_FIELD(p, i));
   return Val_unit;

}


value __ext_ml_print_char(Context *contextp, char c) {

   putchar(c);
   return Val_unit;

}


value __ext_ml_print_int(Context *contextp, value i) {

   printf("%d", Int_val(i));
   return Val_unit;

}


value __ext_ml_output_byte(Context *contextp, value i) {

   putchar(Int_val(i));
   return Val_unit;

}


value __ext_ml_print_hex(Context *contextp, value i) {

   printf("%x", Int_val(i));
   return Val_unit;

}


value __ext_ml_print_float(Context *contextp, double x) {

   printf("%g", x);
   return Val_unit;

}


value __ext_ml_atoi(Context *contextp, value p) {

   int size = GET_BLOCK_WORD_SIZE(p) - 1;
   char buffer[size + 1];
   int i;

   for(i = 0; i < size; i++)
      buffer[i] = Int_val(GET_FIELD(p, i));
   buffer[i] = 0;
   return Val_int(atoi(buffer));

}


double __ext_ml_atof(Context *contextp, value p) {

   int size = GET_BLOCK_WORD_SIZE(p) - 1;
   char buffer[size + 1];
   int i;

   for(i = 0; i < size; i++)
      buffer[i] = Int_val(GET_FIELD(p, i));
   buffer[i] = 0;

   return atof(buffer);

}


/*
 * Unlambda utilities.
 */
value __ext_unl_getchar(Context *contextp) {

   return Val_int(getchar());

}
