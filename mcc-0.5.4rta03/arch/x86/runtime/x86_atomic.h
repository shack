/*
 * Code for handling copy-on-write.
 */
#ifndef _X86_ATOMIC_H
#define _X86_ATOMIC_H

/* Initialization */
void atomic_init();

/* Statistics */
void atomic_reset();
void atomic_stats();

#endif /* _X86_ATOMIC_H */
