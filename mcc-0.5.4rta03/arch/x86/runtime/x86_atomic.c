/*
   x86 Atomic runtime
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


/* Required include files */
#include "x86_context.h"
#include "x86_runtime.h"
#include "x86_reserve.h"
#include "x86_atomic.h"
#include "x86_gc.h"


/* Debugging flags */
int debug_atomic;           /* Show COW operations if true. */
int cow_fault_count;        /* Total COW faults that occurred */


/* atomic_init
   Initialize atomic debugging information.  */
void atomic_init() {

   debug_atomic = getenv("DEBUG_ATOMIC") != 0;
   cow_fault_count = 0;

}


/* atomic_reset
   Reset the atomic debugging information (in particular, statistics).  */
void atomic_reset() {

   cow_fault_count = 0;

}


/* atomic_stats
   Display the current atomic stats, if debugging is enabled.  */
void atomic_stats() {

   if(debug_atomic) {
      fprintf(stderr, "COW faults: %d\n", cow_fault_count);
   }

}


/* grow_atomic_pointer_table
   Grow an atomic block to allow for more pointer storage.  This only
   affects the size of the pointer table in the current level.  */
static Atomic *grow_atomic_pointer_table(Atomic *atomicp) {

   Atomic *newp;
   unsigned size;

   /* New desired size */
   size = atomicp->ptr_size * 2;
   newp = (Atomic *)realloc(atomicp, sizeof(Atomic) + size * sizeof(void *));
   if(newp == NULL) {
      fprintf(stderr, "grow_atomic_pointer_table: out of memory\n");
      abort();
   }
   newp->ptr_size = size;
   return newp;

}


/* grow_atomic_levels
   Grow the top atomic block to allow for new atomic levels.  This
   will attempt to double the number of levels that the context can
   accomodate.  */
static void grow_atomic_levels(Context *contextp) {

   unsigned int size;
   Atomic **atomicpp;

   size = contextp->atomic_size * 2;
   atomicpp = (Atomic **)realloc(contextp->atomic_info, size * sizeof(Atomic *));
   if(atomicpp == NULL) {
      fprintf(stderr, "grow_atomic_levels: can't allocate storage for %u atomic entries\n", size);
      abort();
   }
   contextp->atomic_size = size;
   contextp->atomic_info = atomicpp;

}


/* __atomic_levels
   Return the current number of atomic levels in the system.  If
   the result is zero, then we are currently not in an atomic level. */
int __atomic_levels(Context *contextp) {

   return(contextp->atomic_next);

}


/* __ext_fc_atomic_levels
   FC stub to __atomic_levels. */
int __ext_fc_atomic_levels(Context *contextp) {

   return(__atomic_levels(contextp));

}


/* convert_level
   Converts a level that might be ``0'' (for most recent level) into
   an absolute level, that is guaranteed to be between 1 and N.  If
   N is zero, this function will always fail.  If this returns, then
   the level is guaranteed to be valid.  */
static int convert_level(Context *contextp, int level) {

   int num_levels = contextp->atomic_next;

   /* Check if the user wants to rollback the most recent level;
      then, check to make sure the level request is within bounds. */
   if(level == 0) {
      level = num_levels;
   }
   if(level < 1 || level > num_levels) {
      fprintf(stderr, "convert_level:  requested level %d is out-of-bounds:  [1, %d].\n", level, num_levels);
      abort();
   }

   /* Return the converted level value */
   return(level);

}


/* __atomic_fun
   Return the function index associated with atomic level indicated.
   If level is zero, then the function index for the most recent
   level is returned. */
int __atomic_fun(Context *contextp, int level) {

   level = convert_level(contextp, level);
   return(ATOMIC_LEVEL(contextp, level)->fun);

}


/* __atomic_env
   Return the environment index associated with atomic level indicated.
   If level is zero, then the environment index for the most recent
   level is returned. */
int __atomic_env(Context *contextp, int level) {

   level = convert_level(contextp, level);
   return(ATOMIC_LEVEL(contextp, level)->env);

}


/* __atomic
   Main atomic call - invoked by Atomic() MIR call.  Note that this
   function is not responsible for actually calling f(); it is only
   responsible for setting up the Atomic data structure that is
   required when we rollback, and setting the copy bits in the ptr
   table.  It does accept a reference to the environment and the
   function, so that rollback will know what state to restore to. */
void __atomic(Context *contextp, unsigned int fun, unsigned int env) {

   Atomic *atomicp;              /* Atomic environment data */
   unsigned int total_size;      /* Total amount of memory required */
   GCInfo *gc_info;
   void *nextp;

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** atomic: validating context on entry\n");
      validate_context(contextp);
   }

   /* Print debugging info */
   if(debug_atomic) {
      fprintf(stderr, "*** atomic: entering a new scope, with abort function %d, environment %d\n", fun / sizeof(int), env / sizeof(int));
   }

   /* Allocate the spare pointer table/environment data */
   total_size = sizeof(Atomic) + INITIAL_PTR_DIFF_COUNT * sizeof(void *);
   atomicp = (Atomic *)malloc(total_size);
   if(atomicp == NULL) {
      fprintf(stderr, "Failed to allocate new pointer table for atomic.\n");
      abort();
   }

   /* Initialize basic Atomic arguments */
   nextp = contextp->mem_next;
   atomicp->fun = fun;
   atomicp->env = env;
   atomicp->copy_point = nextp;
   atomicp->ptr_size = INITIAL_PTR_DIFF_COUNT;
   atomicp->ptr_next = 0;
   atomicp->ptr_mark = -1;
   atomicp->ptr_sweep = 0;

   /* Set the minor heap to be inside the new copy generation.
      We can blow away the minor roots because the new minor
      heap is empty.  The rule is that the minor heap must be
      contained within the current atomic generation; this 
      assumption is required by mark_minor in x86_gc.c.  */
   contextp->minor_base = nextp;
   clear_minor_roots(contextp);

   /* Set up the context.  The copy_point is always set to the
      copy_point of the *last* atomic level in the system; the
      gc_point is the later of the copy_point and the minor_base. */
   contextp->copy_point = nextp;
   contextp->gc_point = nextp;

   /* Grow the context if necessary, and add the new atomic block. */
   if(contextp->atomic_next == contextp->atomic_size) {
      grow_atomic_levels(contextp);
   }
   contextp->atomic_info[contextp->atomic_next] = atomicp;
   contextp->atomic_next++;

   /* Keep stats */
   gc_info = contextp->gc_info;
   if(contextp->atomic_next > gc_info->atomic_nest)
      gc_info->atomic_nest = contextp->atomic_next;
   gc_info->atomic_total++;

   /* We are done */
   if(debug_atomic) {
      fprintf(stderr, "*** atomic: we are ready to run at level %d\n", contextp->atomic_next);
   }

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** atomic: validating context on exit\n");
      validate_context(contextp);
   }

}


/* __atomic_rollback
   Rollback/fail function for atomic code.  Note this function does
   not do the final call to f; it leaves that task up to the caller.
   This function merely restores the pointer table to its original
   state.

   The level (1 <= level <= N) indicates what level to rollback;
   the indicated level and ALL LATER LEVELS will be rolled back. If
   level = 0, then we will roll back the most recent level N.  */
void __atomic_rollback(Context *contextp, int level) {

   unsigned long index;
   int i, cur_level, count;
   Atomic *atomicp;
   GCInfo *gc_info;
   void **tablep;
   void *blockp;

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** atomic_rollback: validating context on entry\n");
      validate_context(contextp);
   }

   /* Get the current atomic level information */
   cur_level = contextp->atomic_next;
   if(cur_level <= 0) {
      fprintf(stderr, "__atomic_rollback:  at atomic level %d, nowhere to rollback!\n", cur_level);
      abort();
   }

   /* Adjust the level in case it is 0, for most recent level. */
   level = convert_level(contextp, level);

   /* Continue to rollback until we *pass* the desired level.
      Note that we must run this loop at least once... */
   do {
      /* get the atomic data for this level */
      atomicp = ATOMIC_LEVEL(contextp, cur_level);

      /* Debugging */
      if(debug_atomic) {
         fprintf(stderr, "*** atomic_rollback: rolling back to beginning of level %d, function %d, environment %d\n",
                 cur_level, atomicp->fun / sizeof(int), atomicp->env / sizeof(int));
      }

      /* Decrement cur_level to reflect the level we are
         now rolling back *to*, not the level being aborted. */
      --cur_level;

      /* Restore pointer table.  This will destroy the current
         pointer table entirely and restore the last known state. */
      count = atomicp->ptr_next;
      tablep = contextp->pointer_base;
      for(i = 0; i != count; i++) {
         blockp = atomicp->ptr_entries[i];
         index = GET_BLOCK_INDEX(blockp);
         if(debug_atomic) {
            fprintf(stderr, "*** atomic_rollback: restoring block %ld: 0x%08lx->0x%08lx\n",
                    index, (unsigned long)tablep[index], (unsigned long)blockp);
         }
         tablep[index] = blockp;
      }

      /* Release the (now-dead) structure */
      free(atomicp);
   } while(cur_level >= level);

   /* At this point, cur_level is the most recent level that
      was NOT rolled back. */

   /* Update the atomic level stored in the context, to reflect
      the devastation wreaked by the rollback operation. */
   contextp->atomic_next = cur_level;

   /* Downgrade the copy pointer, if applicable. */
   if(cur_level > 0) {
      contextp->copy_point = ATOMIC_LEVEL(contextp, cur_level)->copy_point;
   } else {
      /* No surviving atomic levels, so copy_point reverts to memory base. */
      contextp->copy_point = contextp->mem_base;
   }
   
   /* Downgrade the gc_level */
   gc_info = contextp->gc_info;
   if(cur_level < gc_info->gc_level) {
      gc_info->gc_level = cur_level;
   }

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** atomic_rollback: validating context on exit\n");
      validate_context(contextp);
   }

}


/* __atomic_retry
   Usually rollback is a RETRY operation; that is, we abort the
   atomic scope starting at level, and then re-enter the atomic
   scope for level from the beginning (but with a different
   status code). This function does both the rollback and entry.

   The level (1 <= level <= N) indicates what level to rollback;
   the indicated level and ALL LATER LEVELS will be rolled back.
   If level = 0, then we will roll back the most recent level N.  */
void __atomic_retry(Context *contextp, int level) {

   Atomic *atomicp;
   unsigned int fun_index;
   unsigned int env_index;

   /* Adjust the level in case it is 0, for most recent level. */
   level = convert_level(contextp, level);

   /* Get the current function and environment index values */
   atomicp = ATOMIC_LEVEL(contextp, level);
   fun_index = atomicp->fun;
   env_index = atomicp->env;

   /* Do the rollback */
   __atomic_rollback(contextp, level);

   /* Re-enter the atomic block */
   __atomic(contextp, fun_index, env_index);

}


/* __atomic_commit
   Commit function for atomic code. This removes the atomic
   environment associated with level from the context (allowing
   the garbage collector to ignore it from here on out, as well),
   and commits changes for that level with the previous levels.

   The level (1 <= level <= N) indicates what level to commit;
   the indicated level ONLY is committed with the data in the
   previous level.  If level = 0, then we will commit the most
   recent level N.  */
void __atomic_commit(Context *contextp, int level) {

   Atomic *atomicp;
   Atomic *parentp;
   GCInfo *gc_info;
   void **entries;
   void *blockp;
   int num_levels;
   int count;
   int i;

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** atomic_commit: validating context on entry\n");
      validate_context(contextp);
   }

   /* Make sure we actually have an atomic scope right now */
   num_levels = contextp->atomic_next;
   if(num_levels <= 0) {
      fprintf(stderr, "__atomic_commit:  at atomic level %d, nowhere to commit!\n", num_levels);
      abort();
   }

   /* Check if the user wants to commit the most recent level;
      then, check to make sure the level request is within bounds. */
   level = convert_level(contextp, level);

   /* Debugging code */
   if(debug_atomic) {
      fprintf(stderr, "*** atomic_commit: committing and exiting level %d of %d\n", level, num_levels);
      fprintf(stderr, "*** atomic_commit: we will be running at level %d now\n", num_levels - 1);
   }

   /* NOTE: Actions are different depending on if level == num_levels.
      If actions are being taken on the most recent level, then they
      affect the major/minor heaps. */

   /* Save a pointer to the atomic block being committed. */
   atomicp = ATOMIC_LEVEL(contextp, level);

   /* If there is a prior level, then we need to coalesce our difference
      table with the previous table.  Entries in our table that point to
      blocks below our base need to be added to the previous difference
      table. */
   if(level > 1) {
      count = atomicp->ptr_next;
      parentp = ATOMIC_LEVEL(contextp, level - 1);
      entries = atomicp->ptr_entries;
      
      /* Copy entries into the parent */
      for(i = 0; i < count; i++) {
         blockp = entries[i];
         if(blockp <= parentp->copy_point) {
            /* Make sure parent has enough space */
            if(parentp->ptr_next == parentp->ptr_size) {
               parentp = grow_atomic_pointer_table(parentp);
               ATOMIC_LEVEL(contextp, level - 1) = parentp;
            }
            parentp->ptr_entries[parentp->ptr_next] = blockp;
            parentp->ptr_next++;
         }
      } /* Transfer difference entries to parent */
   } /* Is there a parent level? */

   /* Update context, removing the level being deleted. */
   if(level < num_levels) {
      memmove(&ATOMIC_LEVEL(contextp, level),
              &ATOMIC_LEVEL(contextp, level + 1),
              (num_levels - level) * sizeof(Atomic *));
   }
   --contextp->atomic_next;

   /* Revert the copy pointer -- only affected when committing the
      most recent atomic level. */
   if(num_levels == level) {
      if(level > 1) {
         parentp = ATOMIC_LEVEL(contextp, level - 1);
         contextp->copy_point = parentp->copy_point;
      } else {
         contextp->copy_point = contextp->mem_base;
      }
   }

   /* Downgrade the gc_level.  Note that we need to do this even if both
      levels involved in the commit have been collected; this is because
      the coalesce may generate dead blocks that came about as a result
      of a COW-fault in the older level.  */
   --num_levels;
   gc_info = contextp->gc_info;
   if(level - 1 < gc_info->gc_level) {
      gc_info->gc_level = level - 1;
   }

   /* Release this atomic block */
   free(atomicp);

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** atomic_commit: validating context on exit\n");
      validate_context(contextp);
   }

}


/* copy_on_write
   Copy-on-write fault handler.  This is called when we detect a block
   we are going to write to, that should be regarded as an immutable
   block.  Just for the record, this uses the same dismal calling
   convention that the (current) GC uses; see x86_reserve.h for info.

   Note that the copy_on_write function is not called directly by the
   backend; it is called indirectly by the x86_glue.s file.  */
void copy_on_write(void **regs) {

   Context *contextp;                 /* Pointer to the current context */
   Atomic *atomicp;                   /* Pointer to the current atomic blk */
   void *blockp;                      /* Pointer to the original block */
   void *newp;                        /* Pointer to the newly alloc'd block */
   unsigned long size;                /* Size of the block (data only) */
   unsigned long size_with_header;    /* Size of the block (include header) */
   unsigned long index;               /* Index of this block */
   void **blockpp;                    /* Used to find aliases in globals */
   int *maskp;                        /* Walk through list of spilled ptrs */
   int mask;                          /* Walk through list of spilled ptrs */
   int i;                             /* Boring old iterator... */

   /* Extract the relevant arguments out of regs */
   contextp = regs[RES_EBP];
   blockp = regs[RES_COPY_POINTER];

   /* TEMP: this is temporary code for handling minor roots.
      We'll eventually migrate this directly into the assembly
      so that we don't have to pay the cost of a COW fault
      for minor-generation faults.  */
   if(blockp >= contextp->copy_point) {
      if(!IS_ROOT(blockp)) {
         SET_ROOT_BIT(blockp);
         *contextp->next_root++ = blockp;
      }
      return;
   }

   /*** From this point on, a fault has occurred. ***/

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** copy_on_write: validating context on entry\n");
      validate_context(contextp);
   }

   /* This is a proper copy fault */
   maskp = (int *)regs[RES_REG_MASK];

   /* Save the difference in the parent's table */
   assert(contextp->atomic_next > 0);
   atomicp = ATOMIC_LEVEL(contextp, contextp->atomic_next);
   if(atomicp->ptr_next == atomicp->ptr_size) {
      atomicp = grow_atomic_pointer_table(atomicp);
      ATOMIC_LEVEL(contextp, contextp->atomic_next) = atomicp;
   }
   atomicp->ptr_entries[atomicp->ptr_next] = blockp;
   atomicp->ptr_next++;

   /* Get information about the existing block */
   size = GET_BLOCK_SIZE(blockp);
   index = GET_BLOCK_INDEX(blockp);
   if(debug_atomic) {
      fprintf(stderr, "*** copy_on_write: copying block index=%lu, size=%lu (in level %d)\n", index, size, contextp->atomic_next);
   }

   /* Allocate the new block - we need to reserve space in
      the heap for this new block; revise the RES_NUM_BYTES
      entry to account for the size of this block. */
   size_with_header = size + BLOCK_HEADER_SIZE;

   /* BUG JYH: strictly speaking a typecast is not an l-value */
   /* XXX JW: and sure enough, recent versions of gcc whine about it! */
   {
       unsigned long *const regs_ul = (unsigned long *const) regs;
       regs_ul[RES_NUM_BYTES] += size_with_header;
   }

   if((unsigned long)regs[RES_NUM_BYTES] >= (contextp->minor_limit - contextp->mem_next) ||
      (unsigned long)regs[RES_NUM_POINTERS] >= contextp->pointer_free) {
       /* Either this COW or an upcoming allocation requires reserve.
          Call the garbage collector now to make space for the copy. */
       gc(regs);
       /* Hope that succeeded... */
   
       /* If we called GC, then we need to re-fetch the blockp pointer,
          because GC may have moved it if it did a major (compacting)
          collection. */
       blockp = contextp->pointer_base[index];
   }

   newp = contextp->mem_next + BLOCK_HEADER_SIZE;
   contextp->mem_next += size_with_header;
   
   /* Copy current block contents to the new location */
   memcpy(newp - BLOCK_HEADER_SIZE, blockp - BLOCK_HEADER_SIZE, size_with_header);
   if(debug_atomic) {
      fprintf(stderr, "*** copy_on_write: pointer rewrite 0x%08lx->0x%08lx\n",
              (unsigned long)blockp, (unsigned long)newp);
   }

   /* Update stats */
   cow_fault_count++;

   /* At this point, the NEW block will become mutable; the current
      pointer table must be updated to point to the new location.  */
   contextp->pointer_base[index] = newp;

   /* Now, update all aliases to this block that are currently
      in registers.  This is the painful part, unfortunately.
      All registers that currently hold pointers are in regs.
      Use the mask to figure out which of the hardware registers
      contain pointers.  */
   mask = *maskp++;
   for(i = 0; i < RES_MAX_REGS; i++) {
      if((mask & (1 << i)) != 0) {
         /* Rewrite register pointer */
         if(regs[i] == blockp) {
            regs[i] = newp;
         }
      } /* Did this reg contain a ptr? */
   } /* Scan all regs */

   /* Next, the spilled register set may also contain aliases.
      The rest of the mask indicates which spills hold pointers.  */
   while(1) {
      mask = *maskp++;
      if(mask == -1) {
         break;
      }
      blockpp = &Global(contextp, mask);
      if(*blockpp == blockp) {
         *blockpp = newp;
      }
   } /* Scan through all spills */

   /* Validation hook (if enabled) */
   if(VALIDATE_CONTEXT) {
      fprintf(stderr, "*** copy_on_write: validating context on exit\n");
      validate_context(contextp);
   }

}
