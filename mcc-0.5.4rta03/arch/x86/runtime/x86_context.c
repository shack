/*
   x86 Context Information utility
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


/* Required include files */
#include "x86_context.h"
#include "x86_type.h"


/***  Context Information  ***/


/* Prints out the context information (debugging info) */
void print_context(FILE *out, const Context *c) {

   int i;

   /* Beginning of context information */
   fprintf(out, "--begin Context information--\n");

   /* Memory heap checks */
   fprintf(out, "mem_base: 0x%08x  mem_limit: 0x%08x | mem_next: 0x%08x  mem_size: %d\n",
                (unsigned)c->mem_base, (unsigned)c->mem_limit, (unsigned)c->mem_next, c->mem_size);
   if(c->mem_base >= c->mem_limit) {
      fprintf(out, "*** Memory base/limit are bogus\n");
   }
   if(c->mem_next < c->mem_base || c->mem_next >= c->mem_limit) {
      fprintf(out, "*** Memory next out of bounds\n");
   }
   if(c->mem_size != c->mem_limit - c->mem_base) {
      fprintf(out, "*** Memory size inconsistent with base/limit\n");
   }

   /* Pointer table checks */
   fprintf(out, "ptr_base: 0x%08x  ptr_limit: 0x%08x | ptr_next: 0x%08x  ptr_size: %d  ptr_free: %d\n",
                (unsigned)c->pointer_base, (unsigned)c->pointer_limit, (unsigned)c->pointer_next,
                c->pointer_size, c->pointer_free);
   if(c->pointer_base >= c->pointer_limit) {
      fprintf(out, "*** Pointer base/limit are bogus\n");
   }
   if(c->pointer_next < c->pointer_base || c->pointer_next >= c->pointer_limit) {
      fprintf(out, "*** Pointer next is out of bounds\n");
   }
   if(c->pointer_size != (c->pointer_limit - c->pointer_base) * sizeof(void *)) {
      fprintf(out, "*** Pointer size inconsistent with base/limit\n");
   }
   if(c->pointer_free > c->pointer_size) {
      fprintf(out, "*** Pointer free inconsistent with size\n");
   }

   /* Minor heap information */
   fprintf(out, "min_base: 0x%08x  min_limit: 0x%08x | min_root: 0x%08x  next_root: 0x%08x\n",
                (unsigned)c->minor_base, (unsigned)c->minor_limit,
                (unsigned)c->minor_roots, (unsigned)c->next_root);
   if(c->minor_base >= c->minor_limit) {
      fprintf(out, "*** Minor base/limit are bogus\n");
   }
   if(c->minor_base < c->mem_base || c->minor_limit > c->mem_limit) {
      fprintf(out, "*** Minor base/limit inconsistent with memory base/limit\n");
   }

   /* Copy and GC points */
   fprintf(out, "cp_point: 0x%08x  gc_point:  0x%08x\n", (unsigned)c->copy_point, (unsigned)c->gc_point);
   if(c->copy_point > c->gc_point) {
      fprintf(out, "*** Copy point is set after GC point\n");
   }
   if(c->copy_point < c->mem_base) {
      fprintf(out, "*** Copy point before beginning of heap\n");
   }
   if(c->gc_point > c->mem_limit) {
      fprintf(out, "*** GC point after end of heap\n");
   }

   /* Atomic block information */
   fprintf(out, "atomic:   0x%08x  atomic_next: %d  atomic_size: %d\n",
                (unsigned)c->atomic_info, c->atomic_next, c->atomic_size);
   if(c->atomic_next > 0) {
      fprintf(out, "atomic generations:\n");
      for(i = 1; i <= c->atomic_next; ++i) {
         const Atomic *atomic = ATOMIC_LEVEL(c, i);
         fprintf(out, "   level %03d  fun: %d  env: %d  cp_point: 0x%08x\n", i, atomic->fun, atomic->env, (unsigned)atomic->copy_point);
      }
   }

   /* End of context information */
   fprintf(out, "--end Context information--\n");

}


/* Validates the context and certain aspects of the heap. This will abort
   if an inconsistency is discovered (bad invariant or malformed block in
   the heap). */
static bool __validate_context_area(const Context *c, const void *basep, const void *limitp) {

   const void *blockp;
   unsigned int index;
   unsigned int size;

   /* Verify that the heap is okay */
   blockp = basep + BLOCK_HEADER_SIZE;
   while(blockp < limitp) {
      if(IS_AGGR(blockp)) {
         index = GET_AGGR_INDEX(blockp);
         size = GET_AGGR_SIZE(blockp);
      } else {
         index = GET_BLOCK_INDEX(blockp);
         size = GET_BLOCK_SIZE(blockp);
      }

      /* Check that the block size is sane */
      if(blockp + size > limitp) {
         /* Something wrong; block extends past memory limit */
         fprintf(stderr, "*** validate_context failed: block %p (size %d) extends past area [%p, %p]\n",
            blockp, size, basep, limitp);
         return(false);
      }

      /* Check that the block index is valid */
      if(index * 4 >= c->pointer_size) {
         fprintf(stderr, "*** validate_context failed; block %p (index %d) index not valid\n", blockp, index);
         return(false);
      }
      if(c->pointer_base[index] != blockp) {
         /* Okay, we didn't get a reverse match, however that may be because 
            there is a newer version of the block in the heap. Check! */
         if(!PTBL_ENTRY_VALID(c->pointer_base[index]) ||
            GET_BLOCK_INDEX(c->pointer_base[index]) != index) {
            fprintf(stderr, "*** validate_context failed: block %p (index %d) pointer table entry refers to block %p\n",
               blockp, index, c->pointer_base[index]);
            return(false);
         }
      }

      /* Advance to the next block */
      blockp += size + BLOCK_HEADER_SIZE;
   }

   /* We were successful */
   return(true);

}


static bool __validate_context(const Context *c) {

   const void *prevp;
   int i;

   /* Memory heap checks */
   if(c->mem_base >= c->mem_limit) {
      fprintf(stderr, "*** validate_context failed: Memory base/limit are bogus\n");
      return(false);
   }
   if(c->mem_next < c->mem_base || c->mem_next >= c->mem_limit) {
      fprintf(stderr, "*** validate_context failed: Memory next out of bounds\n");
      return(false);
   }
   if(c->mem_size != c->mem_limit - c->mem_base) {
      fprintf(stderr, "*** validate_context failed: Memory size inconsistent with base/limit\n");
      return(false);
   }

   /* Pointer table checks */
   if(c->pointer_base >= c->pointer_limit) {
      fprintf(stderr, "*** validate_context failed: Pointer base/limit are bogus\n");
      return(false);
   }
   if(c->pointer_next < c->pointer_base || c->pointer_next >= c->pointer_limit) {
      fprintf(stderr, "*** validate_context failed: Pointer next is out of bounds\n");
      return(false);
   }
   if(c->pointer_size != (c->pointer_limit - c->pointer_base) * sizeof(void *)) {
      fprintf(stderr, "*** validate_context failed: Pointer size inconsistent with base/limit\n");
      return(false);
   }
   if(c->pointer_free > c->pointer_size) {
      fprintf(stderr, "*** validate_context failed: Pointer free inconsistent with size\n");
      return(false);
   }

   /* Minor heap information */
   if(c->minor_base >= c->minor_limit) {
      fprintf(stderr, "*** validate_context failed: Minor base/limit are bogus\n");
      return(false);
   }
   if(c->minor_base < c->mem_base || c->minor_limit > c->mem_limit) {
      fprintf(stderr, "*** validate_context failed: Minor base/limit inconsistent with memory base/limit\n");
      return(false);
   }

   /* Copy and GC points */
   if(c->copy_point > c->gc_point) {
      fprintf(stderr, "*** validate_context failed: Copy point is set after GC point\n");
      return(false);
   }
   if(c->copy_point < c->mem_base) {
      fprintf(stderr, "*** validate_context failed: Copy point before beginning of heap\n");
      return(false);
   }
   if(c->gc_point > c->mem_limit) {
      fprintf(stderr, "*** validate_context failed: GC point after end of heap\n");
      return(false);
   }

   /* Verify that the heap is okay */
   prevp = c->mem_base;
   for(i = 0; i < c->atomic_next; ++i) {
      if(!__validate_context_area(c, prevp, c->atomic_info[i]->copy_point)) {
         return(false);
      }
      prevp = c->atomic_info[i]->copy_point;
   }
   if(c->copy_point != prevp) {
      fprintf(stderr, "*** validate_context failed: c->copy_point %p does not agree with last atomic copy point %p\n",
         c->copy_point, prevp);
      return(false);
   }
   if(!__validate_context_area(c, c->copy_point, c->gc_point)) {
      return(false);
   }
   if(!__validate_context_area(c, c->gc_point, c->mem_next)) {
      return(false);
   }

   /* All was okay */
   return(true);

}


void validate_context(const Context *c) {

   if(!__validate_context(c)) {
      print_context(stderr, c);
      abort();
   }

}
