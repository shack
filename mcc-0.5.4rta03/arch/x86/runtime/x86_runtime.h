/*
 * Runtime header for x86.
 * Copyright(c) 2002,2001 Justin David Smith, Caltech
 * Copyright(c) 2002,2001 Jason Hickey, Caltech
 */
#ifndef __x86_runtime_included
#define __x86_runtime_included


/*
 * MEMORY BLOCK FORMATS
 *
 * Invariants:
 *
 * 1. Pointers during runtime refer to the first DATA word in the block.
 *    Infix pointers can point to any offset in the data area, but the
 *    embedded base pointer must refer to first DATA word in the block.
 *
 * 2. The index is the WORD index into the pointer table.  Currently,
 *    there are two bits below the index field; therefore, to get the
 *    byte offset into the pointer table, we can simply mask the field
 *    with 0xfffffffc, instead of shifting.
 *
 * 3. The size field is always in WORDS.  Note that currently, there are
 *    two bits below the size field; therefore to get the byte size, we
 *    can simply mask the field with 0xfffffffc, instead of shifting.
 *
 * 4. M is the ``mark'' bit, used by GC.
 *
 * 5. R is the ``root'' bit, indicating whether this block is in the set
 *    of root pointers.
 *
 * 6. For function headers, the arity tag indicates the basic arity of
 *    the function and the types of the arguments.  This is simply an
 *    integer ID to a label; see arch/x86/util/x86_arity.ml for details.
 *
 * Aggregate header format:
 *      MSB                     LSB
 *     .---------------------------.
 *     | size:30 (words)        |0R|
 *     |---------------------------|
 *     | index:30 (word)        |1M|
 *     |---------------------------|
 *     |      ...                  |
 *
 * Function header format:
 *      MSB                     LSB
 *     .---------------------------.
 *     | arity_tag:32              |
 *     |---------------------------|
 *     | size = 0               |00|
 *     |---------------------------|
 *     | index:30 (word)        |10|
 *     |---------------------------|
 *     |      function code        |
 *     |      ...                  |
 *
 * Block (tuple/array) format:
 *      MSB                     LSB
 *     .---------------------------.
 *     | tag:7 | size:23 (words)|0R|
 *     |---------------------------|
 *     | index:30 (word)        |0M|
 *     |---------------------------|
 *     |      ...                  |
 *
 * ML-int format:
 *      MSB                     LSB
 *     .---------------------------.
 *     | i:31                    |1|
 *     "---------------------------"
 *
 * Pointer table entry:
 *      MSB                     LSB
 *     .---------------------------.
 *     | valid block pointer:30 |00|
 *     |---------------------------|
 *     |      ...                  |
 *     |---------------------------|
 *     | next free entry ptr:30 |01|
 *     |---------------------------|
 *     |      ...                  |
 */


/************************************************************************
 * CONSTANTS
 ************************************************************************/


/*
 * Change this version number any time significant
 * changes are made to the runtime.  I'm using the
 * date as the version number yymmddhhmm
 */
#define MC_VERSION              112161758


/*
 * Initial heap parameters.
 */
#define INITIAL_HEAP_SIZE               (1 << 20)
#define INITIAL_PTR_COUNT               (1 << 10)
#define INITIAL_PTR_DIFF_COUNT          (1 << 10)
#define MIN_MINOR_SIZE                  (1 << 18)
#define MIN_ATOMIC_SIZE                 (1 << 10)
#define MIN_MINOR_ROOTS                 INITIAL_PTR_COUNT
#define MIN_FREE_POINTERS               (1 << 10)


/*
 * These ratios specify how much free space should be left after a
 * garbage collection.
 */
#define HEAP_MAX_LIVE(size)             ((size) * 3 / 4)
#define POINTER_MAX_LIVE(size)          ((size) * 3 / 4)
#define MINOR_MAX_LIVE(size)            ((size) / 4)


/*
 * Some convenient macros for manipulating header words.
 *
 *    BIT_OF_OFF(i)        Produce a mask for i'th bit of a word (0 = LSB).
 *    CLEAR_BIT(n, i)      Clears the i'th bit in the word n.
 *    SET_BIT(n, i)        Sets the i'th bit in the word n.
 *    IS_BIT_SET(n, i)     Returns true if the i'th bit is set in word n.
 *    IS_BIT_CLEARED(n, i) Returns true if the i'th bit is clear in word n.
 */
#define BIT_OF_OFF(off)                 (1UL << (off))
#define CLEAR_BIT(p, off)               ((void *) ((mc_field) (p) & (~BIT_OF_OFF(off))))
#define SET_BIT(p, off)                 ((void *) ((mc_field) (p) | BIT_OF_OFF(off)))
#define IS_BIT_SET(p, off)              (((mc_field) (p)) & BIT_OF_OFF(off))
#define IS_BIT_CLEARED(p, off)          (!IS_BIT_SET((p), (off)))


/*
 * GC constants.
 *
 * Values in memory blocks are not stored as pointers, but as hashed indexes.
 * A normal pointer is represented as its index into the pointer table xor'ed
 * with HASH_INDEX.  A function pointer is represented as its index into the
 * function tables xor'ed with HASH_FUN_INDEX.
 */
extern char __hash_index[];
extern char __hash_fun_index[];

#define HASH_INDEX                      ((int) __hash_index)
#define HASH_INDEX_FUN                  ((int) __hash_fun_index)


/*
 * Round up the allocation size to the nearest word.
 * i is some value in bytes.
 */
#define SIZEUP(i)                       (((i) + 3) & ~3)


/*
 * Header words.  See the block diagrams above.
 *
 *    *_WORD:  indicates which word of the header the value is stored in.
 *    *_SHIFT: indicates how much to right-shift the header word to get
 *             to the LSB of the value.
 *    *_MASK:  indicates what to mask the value with, AFTER it has been
 *             shifted right by the SHIFT constant.
 */
#define TAG_WORD           (-2)
#define TAG_SHIFT          25
#define TAG_MASK           127

/* These constants are configured to read out the size in BYTES. */
#define SIZE_WORD          (-2)
#define SIZE_SHIFT         0
#define SIZE_AGGR_MASK     0xfffffffc
#define SIZE_BLOCK_MASK    0x01fffffc

#define MARK_WORD          (-1)
#define MARK_SHIFT         0
#define MARK_MASK          1

#define AGGR_WORD          (-1)
#define AGGR_SHIFT         1
#define AGGR_MASK          1

#define ROOT_WORD          (-2)
#define ROOT_SHIFT         0
#define ROOT_MASK          1

/* These constants are configured to read index as a WORD offset */
#define INDEX_WORD         (-1)
#define INDEX_SHIFT        2
#define INDEX_MASK         0x3fffffff

/* These constants are configured to read index as a BYTE offset */
#define INDEX_BYTE_SHIFT   0
#define INDEX_BYTE_MASK    0xfffffffc

/* Function blocks have an arity tag in the header */
#define ARITY_TAG_WORD     (-3)
#define ARITY_TAG_SHIFT    0
#define ARITY_TAG_MASK     0xffffffff


/*
 * Pointer table manipulation constants.  Pointer table has two types of
 * entries.  Free entries have a 1 in LSB, and with the bit cleared, they
 * point to the next free entry.  Other entries have 0 in the LSB, and
 * point to valid blocks in the heap.
 *
 *    PTBL_POINTER_MASK:   Mask the entry to extract real pointer.
 *    PTBL_FREE_OFF:       Offset to bit that indicates if entry is empty.
 */
#define PTBL_POINTER_MASK  0xfffffffc
#define PTBL_FREE_OFF      0


/*
 * Pointer table helpers.
 *
 *    PTBL_POINTER(entry)        Extracts the pointer embedded in entry.
 *    PTBL_ENTRY_VALID(entry)    Returns true if entry is a block pointer.
 *    PTBL_ENTRY_EMPTY(entry)    Returns true if table entry is empty.
 */
#define PTBL_POINTER(p)                 ((void *)((mc_field)(p) & PTBL_POINTER_MASK))
#define PTBL_ENTRY_VALID(p)             IS_BIT_CLEARED((p), PTBL_FREE_OFF)
#define PTBL_ENTRY_EMPTY(p)             IS_BIT_SET((p), PTBL_FREE_OFF)


/*
 * Tag classes.
 */
#define RTTD_TAG                        100
#define MAX_BLOCK_TAG                   RTTD_TAG   /* actual max block tag is 99 */


/*
 * Tag tests.
 */
#define IS_BLOCK_TAG(tag)               ((tag) < MAX_BLOCK_TAG)
#define IS_RTTD_TAG(tag)                ((tag) == RTTD_TAG)
#define IS_OPAQUE_TAG(tag)              ((tag) > MAX_BLOCK_TAG)


/*
 * Field reading and writing.  This can be used for reading header
 * fields as well as normal data.  p is a pointer, and i is the
 * WORD offset from that pointer to read/write.
 */
#define GET_FIELD(p, i)                 (((mc_field *) p)[i])
#define SET_FIELD(p, i, j)              (GET_FIELD(p, i) = (mc_field) (j))


/*
 * Manipulation of header bits.
 * Here, p is always the base pointer for a block.
 */
#define IS_MARKED(p)                    (GET_FIELD(p, MARK_WORD) & (MARK_MASK << MARK_SHIFT))
#define SET_MARK_BIT(p)                 (GET_FIELD(p, MARK_WORD) |= (MARK_MASK << MARK_SHIFT))
#define CLEAR_MARK_BIT(p)               (GET_FIELD(p, MARK_WORD) &= ~(MARK_MASK << MARK_SHIFT))

#define IS_AGGR(p)                      (GET_FIELD(p, AGGR_WORD) & (AGGR_MASK << AGGR_SHIFT))
#define SET_AGGR_BIT(p)                 (GET_FIELD(p, AGGR_WORD) |= (AGGR_MASK << AGGR_SHIFT))
#define CLEAR_AGGR_BIT(p)               (GET_FIELD(p, AGGR_WORD) &= ~(AGGR_MASK << AGGR_SHIFT))

#define IS_ROOT(p)                      (GET_FIELD(p, ROOT_WORD) & (ROOT_MASK << ROOT_SHIFT))
#define SET_ROOT_BIT(p)                 (GET_FIELD(p, ROOT_WORD) |= (ROOT_MASK << ROOT_SHIFT))
#define CLEAR_ROOT_BIT(p)               (GET_FIELD(p, ROOT_WORD) &= ~(ROOT_MASK << ROOT_SHIFT))


/*
 * Size of a block header, in bytes.
 */
#define BLOCK_HEADER_SIZE               8


/*
 * Reading information from block headers.
 * Here, p is the pointer to the base of a block.
 */
#define GET_BLOCK_TAG(p)                ((GET_FIELD(p, TAG_WORD) >> TAG_SHIFT) & TAG_MASK)
#define GET_BLOCK_INDEX(p)              ((GET_FIELD(p, INDEX_WORD) >> INDEX_SHIFT) & INDEX_MASK)
#define GET_BLOCK_SIZE(p)               ((GET_FIELD(p, SIZE_WORD) >> SIZE_SHIFT) & SIZE_BLOCK_MASK)
#define GET_BLOCK_WORD_SIZE(p)          (GET_BLOCK_SIZE(p) / sizeof(mc_field))

#define GET_AGGR_INDEX(p)               (GET_BLOCK_INDEX(p))
#define GET_AGGR_SIZE(p)                ((GET_FIELD(p, SIZE_WORD) >> SIZE_SHIFT) & SIZE_AGGR_MASK)

#define GET_FUNCTION_INDEX(p)           (GET_BLOCK_INDEX(p))
#define SET_FUNCTION_INDEX(p, i)        (SET_FIELD(p, INDEX_WORD, (i) << INDEX_SHIFT))

#define SET_BLOCK_HEADER(p, tag, index, size)                                                   \
        do {                                                                                    \
                SET_FIELD(p, SIZE_WORD, ((tag) << TAG_SHIFT) | (size));                         \
                SET_FIELD(p, INDEX_WORD, (index) << INDEX_SHIFT);                               \
        } while(0)

#define SET_AGGR_HEADER(p, index, size)                                                         \
        do {                                                                                    \
                SET_FIELD(p, SIZE_WORD, (size));                                                \
                SET_FIELD(p, INDEX_WORD, ((index) << INDEX_SHIFT) | (AGGR_MASK << AGGR_SHIFT)); \
        } while(0)

#define HASH_AGGR_INDEX(i)              (((i) << INDEX_SHIFT) ^ HASH_INDEX)
#define HASH_FUN_INDEX(i)               (((i) << INDEX_SHIFT) ^ HASH_INDEX_FUN)


/*
 * Return true if the indicated value is an ML-style integer.
 */
#define IS_ML_INT(p)                    IS_BIT_SET((p), 0)

/************************************************************************
 * UTILITIES
 ************************************************************************/

/*
 * Types of a field and a value.
 */
typedef unsigned long mc_field;
typedef void *value;

/*
 * An AML string is an aggr block, ending with the
 * following byte sequence:  '\000'^n '\nnn'.
 * The string does not include this byte sequence.
 * That is, strings are always followed by a null
 * character.
 *
 * The length of the string is computed as follows.
 * Let m be the total number of bytes in the block s.
 * The string_length(s) = m - s[m - 1] - 1.
 */
static inline int string_length(value p)
{
    int len, c;

    len = GET_BLOCK_SIZE(p);
    c = ((char *) p)[len - 1];
    return len - c - 1;
}

/*
 * Total number of bytes neede to represent the string.
 */
#define STRING_BYTES(len)       (((len) + 4) & ~3)


/************************************************************************
 * STUFF THAT ONLY C WOULD CARE ABOUT
 ************************************************************************/

/*
 * Some data structures require a forward declaration of the context.
 */
struct context;


/*
 * ML value conversions
 */
#define Int_val(v)      (((int) (v)) >> 1)
#define Val_int(i)      ((value) (((i) << 1) | 1))
#define Val_unit        ((value) Val_int(0))
#define Val_false       ((value) Val_int(0))
#define Val_true        ((value) Val_int(1))


/*
 * Sizes.
 */
#define SIZEOF_VALUE          4
#define SIZEOF_POINTER        4
#define SIZEOF_INT            4


/*
 * This is the shape of the __globals area.
 * If this changes, make sure to update X86_print.ml
 */
#define RAWDATA_GLOBAL        0     /* Reads infop as BlockInfo, as raw data */
#define TAGBLOCK_GLOBAL       1     /* Reads infop as BlockTagInfo; contains INIT_VALs */
#define AGGRBLOCK_GLOBAL      2     /* Reads infop as BlockInfo; contains INIT_VALs */
#define RAWINT_GLOBAL         3     /* Reads infop as BlockInfo, as raw data */
#define RAWFLOAT_GLOBAL       4     /* Reads infop as BlockInfo, as raw data */
#define VAR_GLOBAL            5

#define INIT_VAL_INT32        0     /* Raw copy of next 32 bits */
#define INIT_VAL_SINGLE       1     /* Raw copy of next 32 bits */
#define INIT_VAL_DOUBLE       2     /* Raw copy of next 64 bits */
#define INIT_VAL_LONG_DOUBLE  3     /* Raw copy of next 96 bits */
#define INIT_VAL_FUNCTION     4     /* 32-bits, real (unhashed) function index */
#define INIT_VAL_GLOBAL       5     /* 32-bits, real (unhashed) global index */

#define Global(contextp, index)     (*(void **)((void *) contextp + index))

typedef struct block_info {
    int size;
    char datap[0];
} BlockInfo;

typedef struct block_tag_info {
    int size;
    int tag;
    char datap[0];
} BlockTagInfo;

typedef struct global_desc {
    int kind;                       /* These match the *_GLOBAL values, above */
    void *infop;                    /* Interpretation depends on value of kind */
    unsigned long index;            /* Index for this global in the context */
} GlobalDesc;


/*
 * Pointer argument.
 */
typedef struct pointer {
    int index;
    int offset;
} Pointer;


/*
 * Migration data structure
 * This is the format of the __migrate_data space
 */
#define  MIGRATE_DEBUG_GDBHOOKS_SHIFT  0
#define  MIGRATE_DEBUG_GDBHOOKS        (1 << MIGRATE_DEBUG_GDBHOOKS_SHIFT)

typedef int (*MigrateFun)(struct context *contextp, unsigned int regs);

/*
 * See the documentation under arch/type/frame_type.ml for more information.
 * This data structure corresponds exactly to type migrate in that file.
 */
typedef struct migrate {
    unsigned int migrated;       /* Nonzero if this is a migrated process */
    MigrateFun target;           /* Function to call after migrate is done */
    unsigned int channel;        /* Channel to receive data on */
    unsigned int bidir;          /* Nonzero if channel is bidirectional */
    unsigned int initofs;        /* Initial offset in channel to read from */
    unsigned int registers;      /* Index to block containing registers */
    unsigned int ptr_size;       /* Size of the pointer table */
    unsigned int heap_size;      /* Size of the heap currently in use */
    unsigned int debug;          /* Migrate debugging information */
} Migrate;


/*
 * Atomic data structures.  This is the runtime atomic structure used to
 * store information about each atomic level that is currently active.
 */
typedef struct atomic {
    /*
     * First two elements MUST remain in order; there are
     * hardcoded offsets to these elements in the assembly.
     */
    #define ATOMIC_FUN_BYTE_OFFSET  0
    #define ATOMIC_ENV_BYTE_OFFSET  4
    unsigned int fun;             /* HASHED FUN INDEX to the atomic function */
    unsigned int env;             /* HASHED AGGR INDEX to the environment */

    /*
     * The next value is the copy pointer for this atomic block.  Data less than
     * this pointer MUST be immutable and preserved!  The copy_point for
     * the topmost atomic block is the largest such copy_point and should
     * be used for copy-on-write checks.
     *
     * Note that checking this variable directly is time-consuming; as an
     * optimization we store the real pointer of the current copy_point in
     * the copy_point variable in the context; data below that pointer is
     * immutable (it is NULL if no atomic blocks exist).
     */
    void *copy_point;

    /*
     * Parent keeps a series of inverse-differences for the pointer table.
     * Apply each of these differences to the current pointer table to
     * recover the parent's table.  Note that the index can be recovered
     * from the block header, so we just keep pointers in this table.
     */
    unsigned long ptr_size;        /* Max number of entries in the inverted table */
    unsigned long ptr_next;        /* Current index into inverted table */
    unsigned long ptr_mark;        /* Used for roots during mark phase of GC */
    unsigned long ptr_sweep;       /* Used for relocation during sweep phase of GC */
    void *ptr_entries[0];          /* Entries in the inverted table */
} Atomic;

/*
 * Return a pointer to the Atomic structure corresponding to the indicated
 * level.  The atomic level *must* be between 1 and N, where there are N
 * levels active.  Zero is *not* allowed here.
 */
#define  ATOMIC_LEVEL(contextp, level)    ((contextp)->atomic_info[(level) - 1])


/*
 * GC statistics.
 *
 * gc_level is the number of copy generations that
 * have already been collected; they don't need to
 * be collected again.
 *
 * The live values indicate the number of live bytes
 * after the last collection.
 *
 * The count values indicate how many times the collector
 * has been called.
 *
 * gc_time is the number of CPU seconds spent in GC.
 */
typedef struct gc_info {
    unsigned gc_level;
    unsigned major_live;
    unsigned minor_live;
    unsigned major_count;
    unsigned minor_count;
    double gc_abs_time;
    double gc_cpu_time;
    unsigned atomic_total;
    unsigned atomic_nest;
} GCInfo;


/*
 * Core debugging flags
 */
#define  CORE_DEBUG_VERBOSE_FAULT_SHIFT   0
#define  CORE_DEBUG_VERBOSE_FAULT         (1 << CORE_DEBUG_VERBOSE_FAULT_SHIFT)

extern unsigned int __core_debug_flags;


/*
 * THIS IS THE CONTEXT.
 *
 * Format of glob_regs.
 * The format of this is critical: if you change this
 * make sure to update global_vars in X86_frame.ml
 *
 * The context is divided into sections.
 *    1. All the stuff in the struct below
 *    2. Additional globals, __globals.count
 *    3. Additional space for spilled registers, __globals.reg_size
 */
typedef struct context {
    /*
     * Memory area.  The major and minor heaps are contiguous.
     * The minor heap is always contained within exactly one atomic
     * block.
     *
     * mem_base, mem_limit define the base/bounds of the entire heap.
     * mem_next is the location of the next allocation.  We assume
     * a compacting collector, so the free space is always contiguous.
     *
     * mem_size is the total size of the heap (mem_limit - mem_base)
     * (in bytes).
     */
    void *mem_base;             /* 0 */
    void *mem_next;             /* 1 */
    void *mem_limit;            /* 2 */
    unsigned mem_size;          /* 3 */

    /*
     * Pointer table.  The pointer table defines a set of pointers
     * into the heap.  Every block in the heap has an index, and
     * the correspoing entry in the pointer table should point to
     * that block.  Currently, all pointers in the heap are actually
     * indexes into the pointer table.
     *
     * Free entries in the pointer table are linked.  The order
     * is random; it corresponds to the order in which the
     * entries were released by the GC.
     *
     * pointer_base, pointer_limit define the base/bounds of the
     * pointer table.  pointer_next points to the first entry
     * on the free list.  pointer_free is the total number of
     * free entries, and pointer_size is the total size of the
     * pointer table (in BYTES) ((limit - base) * sizeof(void *))
     */
    void **pointer_base;        /* 4 */
    void **pointer_next;        /* 5 */
    void **pointer_limit;       /* 6 */
    unsigned pointer_free;      /* 7 */
    unsigned pointer_size;      /* 8 */

    /*
     * Floating point state.
     * These are used for floating-point temporaries.  They
     * are a bandaid for Intel's broken floating point arch,
     * in particular they show up in int/float conversions.
     * The rest of the compiler can ignore these two fields.
     */
    unsigned float_reg;         /* 9 */
    unsigned float_reg_2;       /* 10 */

    /*
     * The minor heap.
     * minor_base is the start of the minor heap.
     * minor_roots are the set of root pointers,
     * and minor_next points to the next location
     * for storing a root pointer.  This table
     * should be the same size as the pointer
     * table so we never have to worry about
     * overflow.
     */
    void *minor_base;           /* 11 */
    void *minor_limit;          /* 12 */
    void **minor_roots;         /* 13 */
    void **next_root;           /* 14 */

    /*
     * Copy generations.
     *
     * We keep an array of nested copy generations.
     */
    Atomic **atomic_info;       /* 15 */
    unsigned atomic_next;       /* 16 */
    unsigned atomic_size;       /* 17 */

    /*
     * COW info.
     *
     * Invariant: gc_point >= copy_point
     *
     * gc_point is max(minor_base, copy_point), where
     * minor_base is the start of the minor heap, and
     * copy_point is the start of the youngest copy
     * generation.
     *
     * There are two cases:
     *    1. if gc_point == copy_point, the current copy
     *       generation is a subset of the minor heap.  In
     *       this case, minor_base <= gc_point, and the minor
     *       roots are not useful.
     *    2. if gc_point > copy_point, then the current
     *       minor heap is a proper subset of the current
     *       copy generation.
     *
     *   TEMP BUG JDS:  case 1 above, can the copy generation
     *   ever be a strict subset of the minor heap?  The assumes
     *   on mark_minor, x86_gc.c seem to require the minor heap
     *   always be a subset of the current copy generation!
     *      2002.12.18
     *
     * On a COW(p) fault for some pointer p, we are guaranteed
     * that p < gc_point.
     *    1. If p >= copy_point, then this is a reference to
     *       a major block in the current generation.  The
     *       block must be added to the set of minor roots.
     *    2. If p < copy_point, then this is a proper COW fault.
     *       Copy the block.
     */
    void *copy_point;           /* 18 */
    void *gc_point;             /* 19 */
    GCInfo *gc_info;            /* 20 */
#define CONTEXT_LENGTH          21

    /*
     * Globals.  See the comment above.
     * The first part of the globals are pointers to global
     * values (like string constants, etc).  The number of
     * global entries is defined by __globals.count.
     *
     * After the globals, we have the area for spilled registers.
     *
     * jyh (12/8/01): these should really go in the opposite
     * order because the spills are accessed much more frequently
     * than the globals.  At least on a x86, smaller offsets will
     * produce smaller instructions, and better performance.
     */
    void *globals[0];
} Context;


/*
 * File classes determine calling conventions for
 * main function.
 */
#define FILE_FC         0
#define FILE_JAVA       1
#define FILE_NAML       2
#define FILE_AML        3


/*
 * Program headers.
 */
typedef void (*exit_fun_type)(void);
typedef void (*exnh_fun_type)(void);
typedef void (*InitFunction)(Context *contextp, exit_fun_type exit);
typedef int  (*MainFunctionJava)(Context *contextp, void *exitp, void *envp, void *argvp);
typedef int  (*MainFunctionAml)(Context *contextp,
                                exit_fun_type exit, void *exit_env,
                                exnh_fun_type exnh, void * exnh_env);
typedef int  (*MainFunctionFull)(Context *contextp,
                                 int argc, void *argvp, int off,
                                 exit_fun_type exit, void *exit_env,
                                 exnh_fun_type exnh, void *exnh_env);

typedef union main_function {
    MainFunctionJava java;
    MainFunctionFull full;
    MainFunctionAml aml;
} MainFunction;

typedef struct header {
    unsigned long version;
    unsigned long context_length;
    unsigned long file_class;
    InitFunction init;
    MainFunction main;
} Header;


/************************************************************************
 * EXTERNALS
 ************************************************************************/


/*
 * Function pointer table.  The function pointer table contains real
 * pointers to every escape function header in the program.  The first
 * two entries are reserved for the __exit and __exnh continuations.
 * The remainder of the function table should be ordered based on the
 * Mir.prog_fun_order field.  Each pointer points to the first word of
 * real code (a function header is always at negative offset from a
 * pointer in this table).
 *
 *     .---------------------------.   <---  __function_base
 *     | Pointer to __exit         |0
 *     |---------------------------|
 *     | Pointer to __exnh         |1
 *     |---------------------------|
 *     | prog_fun_order[0]         |2        __function_size = N
 *     |---------------------------|
 *     | prog_fun_order[1]         |3
 *     |           ...             |
 *     | prog_fun_order[N-3]       |N-1
 *     "---------------------------"   <--   __function_limit
 */
extern void *__function_base[];
extern void *__function_limit[];
extern char __function_size[];
#define FUNCTION_SIZE   ((unsigned) __function_size)


/*
 * Global descriptors.
 */
extern GlobalDesc __globals_base[];
extern GlobalDesc __globals_limit[];
extern char __globals_size[];
#define GLOBALS_SIZE    ((unsigned) __globals_size)

extern long __globals_index_base[];
extern long __globals_index_limit[];


/*
 * Total size of the context.
 */
extern char __context_size[];
#define CONTEXT_SIZE    ((unsigned) __context_size)


/*
 * Migration data.
 */
extern Migrate __migrate_data;


/*
 * Exit function.
 */
extern void __exit(void);
extern void __uncaught_exception(void);


/*
 * Initialization pointers.
 */
extern Header __header_base[];
extern Header __header_limit[];


/*
 * Migrate server hook.
 */
extern int migrate_server(Context *contextp, Migrate *migrate);


#endif /* __x86_runtime_included */
