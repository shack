/*
   x86 Context Information utility
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __x86_context_included
#define __x86_context_included


/* System includes */
#include <stdio.h>


/* Local includes */
#include "x86_runtime.h"


/* If this flag is nonzero, then the context structure and heap are verified
   at the end of any atomic call (atomic, atomic_retry and atomic_commit).
   This is expensive, and should only be used when debugging GC or the atomic
   operations. */
#define  VALIDATE_CONTEXT  0


/* Prints out the context information (debugging info) */
void print_context(FILE *out, const Context *c);


/* Validates the context and certain aspects of the heap. This will abort
   if an inconsistency is discovered (bad invariant or malformed block in
   the heap). */
void validate_context(const Context *c);


#endif /* __x86_context_included */
