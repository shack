/*
 * This collector is documented in the directory doc/gc.
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include "x86_context.h"
#include "x86_runtime.h"
#include "x86_reserve.h"
#include "x86_alloc.h"
#include "x86_gc.h"

/************************************************************************
 * DEBUGGING
 ************************************************************************/

/*
 * Debugging.
 */
int trace_gc = 1;

/*
 * An array of checksums.
 */
#ifdef CHECKSUM
static int checksums[1 << 24];
#endif

/************************************************************************
 * UTILITIES
 ************************************************************************/

/*
 * Clear the root bits in the minor roots,
 * and reset the roots area.
 */
void clear_minor_roots(Context *contextp)
{
    void **roots;
    int max_index, i;

    /* Reset all the root bits */
    roots = contextp->minor_roots;
    max_index = contextp->next_root - roots;
    for(i = 0; i != max_index; i++)
        CLEAR_ROOT_BIT(roots[i]);

    /* Reset the roots area */
    contextp->next_root = contextp->minor_roots;
}

/************************************************************************
 * NORMALIZATION
 ************************************************************************/

/*
 * Normalize all the pointers indicated by the bit mask.
 */
static void normalize(Context *contextp, int *maskp, void **regs)
{
    void *ptrp, **blockpp;
    int i, mask;

    /* Normalize the blocks in the registers */
    mask = *maskp++;
    for(i = 0; i != RES_MAX_REGS; i++) {
        if(mask & (1 << i)) {
            ptrp = regs[i];
            assert(!IS_ML_INT(ptrp) && ptrp >= contextp->mem_base && ptrp < contextp->mem_limit);
            regs[i] = (void *) GET_BLOCK_INDEX(ptrp);
        }
    }

    /* Normalize the blocks in the spilled registers */
    while(1) {
        mask = *maskp++;
        if(mask == -1)
            break;
        blockpp = &Global(contextp, mask);
        ptrp = *blockpp;
        assert(!IS_ML_INT(ptrp) && ptrp >= contextp->mem_base && ptrp < contextp->mem_limit);
        *blockpp = (void *) GET_BLOCK_INDEX(ptrp);
    }
}

/*
 * Restore the pointer values.
 */
static void denormalize(Context *contextp, int *maskp, void **regs)
{
    void **blockpp, **ptbl;
    int i, mask;

    /* Get pointers from the pointer table */
    ptbl = contextp->pointer_base;

    /* Restore the registers */
    mask = *maskp++;
    for(i = 0; i != RES_MAX_REGS; i++) {
        if(mask & (1 << i))
            regs[i] = PTBL_POINTER(ptbl[(unsigned long) regs[i]]);
    }

    /* Restore the spilled registers */
    while(1) {
        mask = *maskp++;
        if(mask == -1)
            break;
        blockpp = &Global(contextp, mask);
        *blockpp = PTBL_POINTER(ptbl[(unsigned long) *blockpp]);
    }
}

/************************************************************************
 * SORTING AND SEARCHING
 ************************************************************************/

/*
 * Sort all the ptr_entries in each of the generations.
 * This is called only on a major collection.
 */
static int compare_pointer(const void **p1, const void **p2)
{
    unsigned long i = (unsigned long) *p1;
    unsigned long j = (unsigned long) *p2;
    int k;
    
    if(i < j)
        k = -1;
    else if(i > j)
        k = 1;
    else
        k = 0;
    return k;
}

typedef int (*QSortFun)(const void *, const void *);

static void sort_level(Atomic *atomicp)
{
    qsort(atomicp->ptr_entries, atomicp->ptr_next, sizeof(void *), (QSortFun) compare_pointer);
}

static void sort_major(Context *contextp)
{
    Atomic **levels, *atomicp;
    int i, max_level;

    levels = contextp->atomic_info;
    max_level = contextp->atomic_next;
    for(i = contextp->gc_info->gc_level; i != max_level; i++) {
        atomicp = levels[i];
        sort_level(atomicp);
        atomicp->ptr_mark = atomicp->ptr_next - 1;
        atomicp->ptr_sweep = 0;
    }
}

/*
 * Update a COW pointer to the new location.  This function will advance
 * at most one ptr_sweep.  Note that this function assumes differential
 * pointer tables are already sorted in increasing order, and assumes
 * that you call it in order of increasing oldp.  This function assumes
 * that there are no ``dead'' differential table entries; it must be
 * called once for each pointer that is in a differential table.
 */
static inline void update_cow_pointer(Context *contextp, unsigned level, void *oldp, void *newp)
{
    Atomic **levels, *atomicp;
    int i, max_level;
    void *blockp;
    
    /* Pointer may exist in any of the levels */
    levels = contextp->atomic_info;
    max_level = contextp->atomic_next;
    for(i = level; i != max_level; i++) {
        atomicp = levels[i];
        if(atomicp->ptr_sweep != atomicp->ptr_next) {
            blockp = atomicp->ptr_entries[atomicp->ptr_sweep];
            if(blockp == oldp) {
                /* Found it! */
                atomicp->ptr_entries[atomicp->ptr_sweep] = newp;
                atomicp->ptr_sweep++;
                return;
            }
        }
    }

    /* 
     * If we make it to here, then it is possible that the block only 
     * exists in the youngest generation.
     */
}

/************************************************************************
 * POINTER TABLE REVERSION
 ************************************************************************/

/*
 * Revert the pointer table so it is correct for
 * the specified atomic level.  We install the entries
 * from the difference table.
 *
 * This function assumes that all younger atomic levels
 * have already been installed for the invariants to
 * hold true in mark_major.
 */
static void revert_pointer_table(Atomic *atomicp, void **tablep)
{
    unsigned long index, limit, i;
    void *blockp, **entries;

    /* Get the COW table */
    limit = atomicp->ptr_next;
    entries = atomicp->ptr_entries;

    /*
     * Replace the pointer table entry with the original
     * block specified in the difference table.
     */
    for(i = 0; i != limit; i++) {
        blockp = entries[i];
        index = GET_BLOCK_INDEX(blockp);
        tablep[index] = blockp;
    }
}

/************************************************************************
 * MARK
 ************************************************************************/

/*
 * Mark a block.  This phase is overly complicated
 * because we don't want to use a recursive algorithm that
 * might blow out the runtime C stack.
 *
 * Invariants:
 *    1. The header fields are static except for the mark bit.
 *       The mark bit is set iff the block is being or has
 *       been examined.
 *
 *    2. While a block is being marked, its pointer table
 *       entry refers back to its parent location, and the
 *       parent location refers to the start of the parent
 *       block.
 *
 *    3. Before and after a block is marked, the pointer table
 *       entry refers to the block itself.
 */
#if CHECKSUM
static int compute_checksum(void *blockp)
{
    int size, sum, i;

    size = GET_BLOCK_SIZE(blockp);
    sum = 0;
    for(i = 0; i != size; i++)
        sum += GET_FIELD(blockp, i);
    return sum;
}

static void set_checksum(void *blockp)
{
    int sum = compute_checksum(blockp);
    checksums[GET_BLOCK_INDEX(blockp)] = sum;
}

static void check_checksum(void *blockp)
{
    int index = GET_BLOCK_INDEX(blockp);
    int sum = compute_checksum(blockp);
    if(sum != checksums[index]) {
        fprintf(stderr, "check_checksum: index=%d before=0x%08x after=0x%08x\n", index, checksums[index], sum);
        abort();
    }
}
#endif

static int mark_contents(void **tablep, unsigned long psize, void *basep, void *limitp, void *blockp)
{
    enum { Block, Aggr, Rttd } type_flag;
    unsigned long size, index, i;
    int level, found, *rttdp;
    void *fieldp;

    /* Initial params */
    i = 0;
    level = 0;
    found = 0;
    index = 0;
    rttdp = 0;
    if(IS_AGGR(blockp)) {
        size = GET_AGGR_SIZE(blockp) / sizeof(mc_field);
        type_flag = Aggr;
    }
    else if(IS_BLOCK_TAG(GET_BLOCK_TAG(blockp))) {
        size = GET_BLOCK_SIZE(blockp) / sizeof(mc_field);
        type_flag = Block;
    }
    else if(IS_RTTD_TAG(GET_BLOCK_TAG(blockp))) {
        size = GET_BLOCK_SIZE(blockp) / sizeof(mc_field);
        type_flag = Rttd;
        rttdp = (int *) GET_FIELD(blockp, 0);
    }
    else {
        /* We don't know how to handle this block. */
        return 0;
    }

    /*
     * Now scan the blocks.
     */
    while(1) {
        while(i < size) {
            /* We're in the middle of a block; get the next field */
            switch(type_flag) {
            case Aggr:
                index = (GET_FIELD(blockp, i) ^ HASH_INDEX);
                break;
            case Block:
                index = GET_FIELD(blockp, i);
                break;
            case Rttd:
                if(rttdp[i] == 0) {
                    i++;
                    continue;
                }
                index = GET_FIELD(blockp, i);
                break;
            }
            if(index & 3) {
                i++;
                continue;
            }
            index >>= INDEX_SHIFT;

            /* Check if this index refers to a pointer */
            if(index < psize) {
                fieldp = tablep[index];
                if(fieldp >= basep && fieldp < limitp && PTBL_ENTRY_VALID(fieldp) && !IS_MARKED(fieldp)) {
                    /* Note that we found an unmarked block */
                    found = 1;

                    /* This field is a valid pointer, so recursively mark the block */
                    SET_MARK_BIT(fieldp);

                    /* Get the new block's size */
                    if(IS_AGGR(fieldp)) {
                        size = GET_AGGR_SIZE(fieldp) / sizeof(mc_field);
                        type_flag = Aggr;
                    }
                    else if(IS_BLOCK_TAG(GET_BLOCK_TAG(fieldp))) {
                        size = GET_BLOCK_SIZE(fieldp) / sizeof(mc_field);
                        type_flag = Block;
                    }
                    else if(IS_RTTD_TAG(GET_BLOCK_TAG(fieldp))) {
                        size = GET_BLOCK_SIZE(fieldp) / sizeof(mc_field);
                        type_flag = Rttd;
                        rttdp = (int *) GET_FIELD(fieldp, 0);
                    }
                    else {
                        /* This is an opaque block, so ignore it and continue scanning */
                        i++;
                        continue;
                    }

                    /*
                     * Set the pointer table entry to point to the parent field,
                     * and set the parent field to point to the parent.  The
                     * pointer table entry is marked as "free" so that we'll
                     * avoid future references to the entry.
                     */
                    tablep[index] = SET_BIT(&GET_FIELD(blockp, i), PTBL_FREE_OFF);
                    SET_FIELD(blockp, i, blockp);

                    /* Set up parameters to scan the new block */
#if CHECKSUM
                    set_checksum(fieldp);
#endif
                    blockp = fieldp;
                    level++;
                    i = 0;

                    /* Restart the scanner */
                    continue;
                }
            }

            /* It wasn't a pointer, so skip it and continue scanning */
            i++;
        } /* end: while(i < size) */

        /*
         * We're finished examining this block.
         * If we have returned to the outermost level,
         * we're done.
         */
        if(--level < 0)
            break;

        /*
         * Return to the parent block.  Fetch the pointer table entry,
         * and restore the pointer table entry to point to this block.
         */
#if CHECKSUM
        check_checksum(blockp);
#endif
        index = GET_BLOCK_INDEX(blockp);
        fieldp = CLEAR_BIT(tablep[index], PTBL_FREE_OFF);
        tablep[index] = blockp;

        /* Get the parent block by dereferencing the field */
        blockp = *(void **)fieldp;

        /* Recover the field index */
        i = (fieldp - blockp) / sizeof(mc_field);

        /* Restore the field to its original value */
        if(IS_AGGR(blockp)) {
            size = GET_AGGR_SIZE(blockp) / sizeof(mc_field);
            SET_FIELD(blockp, i, (index << INDEX_SHIFT) ^ HASH_INDEX);
            type_flag = Aggr;
        }
        else if(IS_BLOCK_TAG(GET_BLOCK_TAG(blockp))) {
            size = GET_BLOCK_SIZE(blockp) / sizeof(mc_field);
            SET_FIELD(blockp, i, index << INDEX_SHIFT);
            type_flag = Block;
        }
        else {
            assert(IS_RTTD_TAG(GET_BLOCK_TAG(blockp)));
            size = GET_BLOCK_SIZE(blockp) / sizeof(mc_field);
            SET_FIELD(blockp, i, index << INDEX_SHIFT);
            rttdp = (int *) GET_FIELD(blockp, 0);
            type_flag = Rttd;
        }

        /* Move on to the next field */
        i++;
    }

    /* Indicate if we found anything interesting in the block */
    return found;
}

/*
 * Before marking the block, check to make sure the block
 * is in bounds.
 */
static void mark_block(void **tablep, unsigned long psize, void *basep, void *limitp, void *blockp)
{
    if(blockp >= basep && blockp < limitp && !IS_MARKED(blockp)) {
        SET_MARK_BIT(blockp);
        mark_contents(tablep, psize, basep, limitp, blockp);
    }
}

/*
 * Mark all blocks that are between base and limit.
 */
static void mark_area(Context *contextp, void **tablep, unsigned long psize, void *basep, void *limitp, int *maskp, void **regs)
{
    int mask, i;

    /* Now recursively mark all blocks */
    mask = *maskp++;
    for(i = 0; i != RES_MAX_REGS; i++) {
        if(mask & (1 << i))
            mark_block(tablep, psize, basep, limitp, regs[i]);
    }

    /* Mark all the blocks in the spill set */
    while(1) {
        mask = *maskp++;
        if(mask == -1)
            break;
        mark_block(tablep, psize, basep, limitp, Global(contextp, mask));
    }
}

/*
 * Mark phase for a minor collection.
 * ASSUMPTION: all copy_points are smaller
 * than minor_base, and the root atomic block
 * contains the minor info.
 */
static void mark_minor(Context *contextp, int *maskp, void **regs)
{
    void **tablep, **fromp, **top, *basep, *limitp, *blockp;
    unsigned long psize;
    int count, i;
    
    /* Enforce the assumptions stated above */
    assert(contextp->copy_point <= contextp->minor_base);

    /* Mark the minor area */
    basep = contextp->minor_base;
    limitp = contextp->mem_next;
    tablep = contextp->pointer_base;
    psize = contextp->pointer_size / sizeof(void *);

    /* Mark all blocks in the minor heap */
    mark_area(contextp, tablep, psize, basep, limitp, maskp, regs);

    /* Examine all blocks indicated as possible minor roots */
    fromp = contextp->minor_roots;
    count = contextp->next_root - fromp;
    top = fromp;
    for(i = 0; i != count; i++) {
        blockp = *fromp++;
        if(mark_contents(tablep, psize, basep, limitp, blockp)) {
            /* The block contained pointers into the minor heap, so keep it */
            *top++ = blockp;
        }
        else {
            /* The block did not contain pointers into the minor heap, so reset the root bit */
            CLEAR_ROOT_BIT(blockp);
        }
    }
    contextp->next_root = top;
}

/*
 * Mark roots from younger difference tables.
 * The difference tables are all sorted, so we scan
 * down from the top of the table, marking pointers
 * that point into the current level.
 */
static void mark_copy_area(Context *contextp, unsigned level)
{
    Atomic **levels, *atomicp;
    void **tablep, *blockp, *basep, *limitp;
    unsigned i, max_level;
    unsigned long psize;
    
    /* Context values */
    tablep = contextp->pointer_base;
    psize = contextp->pointer_size / sizeof(void *);
    levels = contextp->atomic_info;
    max_level = contextp->atomic_next;
    
    /* Bounds of current level */
    atomicp = levels[level];
    limitp = atomicp->copy_point;
    if(level > 0)
        basep = contextp->atomic_info[level - 1]->copy_point;
    else
        basep = contextp->mem_base;

    /* Scan all difference tables */
    for(i = level; i != max_level; i++) {
        atomicp = levels[i];
        while(atomicp->ptr_mark >= 0) {
            blockp = atomicp->ptr_entries[atomicp->ptr_mark];

            /* Stop when we get a pointer into an older level */
            if(blockp < basep)
                break;

            /* Include pointers into current level */
            if(blockp < limitp)
                mark_block(tablep, psize, contextp->mem_base, limitp, blockp);

            atomicp->ptr_mark--;
        }
    }
}

/*
 * Mark phase for a major collection.  Mark all copy
 * generation that haven't been collected yet.
 *
 * Invariant:
 *    A. When copy generation $level$ is marked, the pointer
 *       table entry for a block with index $i$ refers to one
 *       of the following, in order of precedence:
 *       1. the youngest block with index $i$ in generation $level - 1$
 *          or older, if one exists, or
 *       2. the oldest block with index $i$
 *
 * To achieve this invariant, we mark the generations
 * from youngest to oldest.  As we work back, we revert
 * the pointer table in each step.
 *
 * This takes time linear in the number of COW entries.
 * This is probably not too bad since each copy generation
 * is collected at most once.
 *
 * The postcondition of mark_major matches the precondition of
 * sweep_copy_area (defined below).
 */
static void mark_major(Context *contextp, int *maskp, void **regs)
{
    void **tablep, *limitp, *envp;
    unsigned long psize;
    GCInfo *gc_info;
    Atomic *atomicp;
    int i;

    /* Specs and pointer table */
    gc_info = contextp->gc_info;
    tablep = contextp->pointer_base;
    psize = contextp->pointer_size / sizeof(void *);

    /*
     * Get base/limit for the current generation.
     * During marking, we also scan older generations to
     * uncover pointers into the current generation;
     * so base is always mem_base.
     */
    limitp = contextp->mem_next;

    /* Mark all reachable blocks in current generation */
    mark_area(contextp, tablep, psize, contextp->mem_base, limitp, maskp, regs);

    /* Collect all the copy generations */
    for(i = contextp->atomic_next - 1; i >= (int) gc_info->gc_level; i--) {
        /* Get the current atomic block */
        atomicp = contextp->atomic_info[i];

        /* Compute limit of this generation */
        limitp = atomicp->copy_point;
        
        /* 
         * Revert the pointer table. After this point the pointer table
         * will point at blocks belonging to this generation or older,
         * for blocks that existed at this level (newer pointer table
         * entries may still point at younger generations).
         */
        revert_pointer_table(atomicp, tablep);

        /* Mark all blocks reachable from the roots */
        mark_area(contextp, tablep, psize, contextp->mem_base, limitp, maskp, regs);

        /* Mark all blocks reachable from child difference tables */
        mark_copy_area(contextp, i);

        /*
         * Mark the environment. We should never have a case where there is
         * a pointer to older generations that is not reachable through this
         * env, since env is the only data that is passed across a generation
         * boundary.
         */
        assert(atomicp->env % 4 == 0);
        assert(atomicp->env >= 0);
        assert(atomicp->env < contextp->pointer_size);
        envp = tablep[atomicp->env / sizeof(void *)];
        mark_block(tablep, psize, contextp->mem_base, limitp, envp);
    }

    /*
     * To satisfy the sweep_copy_area precondition, we revert
     * the pointer table one more level so that pointer table entries
     * point to _older_ blocks if they exist.  
     *
     * Note that i = gc_info->gc_level - 1 at this point.
     */
    if(i >= 0) {
        atomicp = contextp->atomic_info[i];
        revert_pointer_table(atomicp, tablep);
    }
}

/************************************************************************
 * UP-MARKING
 ************************************************************************/

/*
 * When we perform a minor collection, we save all the parent's COW
 * blocks in the pointer table.  Note that the collection will only
 * be on the MINOR heap, so we should not revert pointer entries to
 * blocks residing in the current generation's major heap.
 */
static void ptable_minor(Context *contextp)
{
    unsigned long i, count, index;
    void **tablep, **roots, *blockp, *minor_base;
    Atomic *atomicp;
    
    /* Get the current generation */
    if(contextp->atomic_next > 0) {
        atomicp = contextp->atomic_info[contextp->atomic_next - 1];

        /* 
         * Find out where the minor heap begins; pointers below 
         * this point will not be reverted.
         */
        minor_base = contextp->minor_base;

        /* Install all the COW pointer table entries */
        tablep = contextp->pointer_base;
        count = atomicp->ptr_next;
        roots = atomicp->ptr_entries;
        for(i = 0; i != count; i++) {
            blockp = roots[i];
            index = GET_BLOCK_INDEX(blockp);
            if(tablep[index] >= minor_base)
                tablep[index] = blockp;
        }
    }
}

/************************************************************************
 * SWEEP
 ************************************************************************/

/*
 * Clear mark bits in all blocks in the uncollected area
 * below gc_level.  Since all the blocks are contiguous,
 * we just scan from left-to-right, clearing the mark bits.
 */
static void sweep_old_area(Context *contextp)
{
    void *blockp, *limitp;
    unsigned long size;
    unsigned gc_level;
    GCInfo *gc_info;
    
    /* Context values */
    gc_info = contextp->gc_info;
    gc_level = gc_info->gc_level;

    if(gc_level > 0) {
        /* Sweep all blocks below the current gc_level */
        limitp = contextp->atomic_info[gc_level - 1]->copy_point;
    
        /* The block pointer is offset by the header */
        blockp = contextp->mem_base + BLOCK_HEADER_SIZE;

        /* Clear marks in all the blocks */
        while(blockp < limitp) {
            if(IS_AGGR(blockp))
                size = GET_AGGR_SIZE(blockp) + BLOCK_HEADER_SIZE;
            else
                size = GET_BLOCK_SIZE(blockp) + BLOCK_HEADER_SIZE;
            CLEAR_MARK_BIT(blockp);
            blockp += size;
        }
    }
}

/*
 * Sweep a copy generation or the minor heap.  The blocks in the
 * area are scanned from left-to-right.
 *
 * PRECONDITION: on entry into this procedure:
 *    A. for the pointer table, the entry for index $i$ contains one
 *       of the following, in order of precedence:
 *       1. the youngest block with index $i$ in generation
 *          $level - 1$ or older, if one exists, or
 *       2. the oldest block with index $i$.
 *    B. for the the difference table
 *       1. entries in all difference tables are sorted (this
 *          is the precondition for update_cow_pointer).
 *       2. the entries in $level + 1$ point to _live_ blocks
 *          in this level.
 *
 * POSTCONDITION: on exit from this procedure:
 *    C. the pointer table entry for index $i$ is
 *       one of the following, in order of precedence:
 *       1. a live block in this generation with index $i$, or
 *       2. the youngest block with index $i$ in generation
 *          $level - 1$ or older, or
 *       3. it is free.
 *    D. if this is not the youngest generation, the difference table
 *       entries for $level + 1$ point to live blocks in this level. 
 *
 * For each block that is marked:
 *    o Reset the mark bit.
 *    o Copy the block left.
 *    o Update the pointer table to point to the new location.
 *      This satisfies C.1.
 *    o If this is not a minor sweep, update the difference tables
 *      to point to the new location to satisfy postcondition D.
 *
 * If a block is not marked, then it is dead.
 *    o If the pointer table entry points to this block,
 *      then by A.1 this is the oldest block with
 *      the given index, and the block is dead.  Younger generations
 *      _cannot_ have a block with this index; if so, the block would
 *      still be live.  Release the pointer table entry,
 *      satisfying C.3.
 *    o If the pointer table entry does not point to this
 *      block, then A.1 must hold, and the entry must point to a
 *      version in an older generation.  The entry is left intact,
 *      satisfying C.2.
 */
static void *sweep_copy_area(Context *contextp, unsigned level, void *top, void *basep, void *limitp)
{
    Atomic *atomicp;
    void **tablep, *fromp;
    unsigned long index, size, i, max_level;

#ifdef DEBUG
    fprintf(stderr, "sweep_copy: base: 0x%08lx, limit: 0x%08lx\n",
            (unsigned long) top, (unsigned long) limitp);
#endif

    /* Get the pointer table */
    tablep = contextp->pointer_base;
    max_level = contextp->atomic_next;

    /* The copied block is offset by the header */
    fromp = basep + BLOCK_HEADER_SIZE;

    /* Sweep the heap */
    while(fromp < limitp) {
        index = GET_BLOCK_INDEX(fromp);
        if(IS_AGGR(fromp))
            size = GET_AGGR_SIZE(fromp) + BLOCK_HEADER_SIZE;
        else
            size = GET_BLOCK_SIZE(fromp) + BLOCK_HEADER_SIZE;
#ifdef DEBUG
        fprintf(stderr, "sweep_copy: fromp: 0x%08lx, index=%ld, size=%ld\n",
                (unsigned long) fromp, index, size);
#endif

        /* 
         * JDS: This sanity check is here because an error in GC/block
         * management results in a block with size ~600MB and index of
         * around ~200MB, on the following test case:
         *    test/fc/atomic/regex2.exec '*h*e*l*l*o*w*'
         * This assertion added on 2002.09.10. 
         *
         * JDS: Lately (2002.09.13) this assertion is triggered by the
         * test case test/fc/atomic/atomic4.c; see the comments in the
         * case for details.
         *
         * All problems here appear to have been resolved.
         */
        if(fromp + size > limitp + BLOCK_HEADER_SIZE) {
            fprintf(stderr, "*** sweep_copy_area: Bad block size detected.\n");
            fprintf(stderr, "*** fromp: %p, blocksize: %ld, limitp: %p\n", fromp, size - BLOCK_HEADER_SIZE, limitp);
            abort();
        }

        /*
         * If the block is marked, copy it left.
         * Update the pointer table to point to this block.
         * As we sweep from left to right, the pointer table
         * entries will always point to the most recent block.
         */
        if(IS_MARKED(fromp)) {
            /* Copy the block left */
            CLEAR_MARK_BIT(fromp);
            if(top != fromp - BLOCK_HEADER_SIZE) {
                assert(top < fromp - BLOCK_HEADER_SIZE);
                memmove(top, fromp - BLOCK_HEADER_SIZE, size);
            }

            /*
             * We must update the pointer table entry even if we did
             * not move the block (above), to make sure the table
             * reflects the most recent LIVE version of this block.
             */
            tablep[index] = top + BLOCK_HEADER_SIZE;
            if(level != max_level) {
                /*
                 * This conditional is just a performance optimization,
                 * since update_cow_pointer ignores all pointers in max_level.
                 */
                update_cow_pointer(contextp, level, fromp, top + BLOCK_HEADER_SIZE);
            }
            top += size;
        }

        /*
         * If the block is not marked, then it is free.
         * The pointer table entry becomes free iff the entry
         * points to this block.  If it does not point to this block
         * then it points to an older block that is still live, and
         * the entry must be retained.
         */
        else if(tablep[index] == fromp) {
            /* Add this block to the free list */
            tablep[index] = SET_BIT(contextp->pointer_next, PTBL_FREE_OFF);
            contextp->pointer_next = tablep + index;
            contextp->pointer_free++;
        }
        fromp += size;
    }

    /* Return the final position */
    return top;
}

/*
 * Sweep for a minor collection.
 */
static void sweep_minor(Context *contextp)
{
    void *top, *limitp;
    unsigned max_level;
    GCInfo *gc_info;

    /* Get the minor info */
    top = contextp->minor_base;
    limitp = contextp->mem_next;
    max_level = contextp->atomic_next;

    /* Sweep the minor area */
    top = sweep_copy_area(contextp, max_level, top, top, limitp);

    /* Set the position of the next allocation */
    contextp->mem_next = top;

    /* Statistics */
    gc_info = contextp->gc_info;
    gc_info->minor_live = top - contextp->minor_base;
    gc_info->minor_count++;
}

/*
 * Sweep all blocks that have not already been collected.
 * This function repairs any damage to the copy_points.
 */
static void sweep_major(Context *contextp)
{
    Atomic *atomicp, **levels;
    unsigned i, gc_level, max_level;
    void *top, *basep, *limitp;
    GCInfo *gc_info;

    /* Clear marks in older levels */
    sweep_old_area(contextp);

    /* Sweep all but the current generation */
    levels = contextp->atomic_info;
    max_level = contextp->atomic_next;
    gc_info = contextp->gc_info;
    gc_level = gc_info->gc_level;
    if(gc_level > 0)
        basep = levels[gc_level - 1]->copy_point;
    else
        basep = contextp->mem_base;
    top = basep;

    /*
     * Sweep all but current generation. When this loop completes,
     * basep will point to the base of the current generation, and top will
     * point to the base of the location to copy current generation TO. The
     * copy_points of each atomic level are updated here. 
     */
    for(i = gc_level; i < max_level; i++) {
        limitp = levels[i]->copy_point;
        top = sweep_copy_area(contextp, i, top, basep, limitp);
        levels[i]->copy_point = top;
        basep = limitp;
    }
    contextp->copy_point = top;
    
    /*
     * Sweep the current generation.  basep already points where we want it
     * to (if there were no atomic levels, it was initialized to mem_base).
     */
    limitp = contextp->mem_next;
    top = sweep_copy_area(contextp, max_level, top, basep, limitp);

    /* Save the position of the next allocation */
    contextp->mem_next = top;

    /* Reset the minor area */
    contextp->minor_base = top;

    /* Statistics */
    gc_info->gc_level = max_level;
    gc_info->minor_live = 0;
    gc_info->major_live = top - contextp->mem_base;
    gc_info->major_count++;
}

/************************************************************************
 * HEAP EXPANSION
 ************************************************************************/

/*
 * Expand the heap when the minor heap does not have space for the next
 * allocation.  Then requested allocation desires "pointers" space in the
 * pointer table, and "bytes" extra bytes in the minor heap.
 */
static void expand_heap(Context *contextp,
                        unsigned long bytes,
                        unsigned long pointers)
{
    unsigned long live_bytes, live_pointers;
    unsigned long old_size, new_size, old_count, new_count, offset;
    long index, max_index, i;
    void *old_base, *new_base, *ptrp;
    void **tablep, **nextp;
    GCInfo *gc_info;

    /* Grow the heap if it is full */
    gc_info = contextp->gc_info;
    live_bytes = gc_info->major_live;
    if(live_bytes + bytes > HEAP_MAX_LIVE(contextp->mem_size)) {
        old_size = contextp->mem_size;
        new_size = (old_size + bytes) * 2;
        old_base = contextp->mem_base;
        if(trace_gc)
            fprintf(stderr, "*** GC: increasing heap size from %lu to %lu bytes\n", old_size, new_size);
        new_base = realloc(old_base, new_size);
        if(new_base == 0) {
            fprintf(stderr, "Out of memory during request for %lu bytes\n", bytes);
            abort();
        }

        /* New minor heap */
        offset = new_base - old_base;
        contextp->minor_base += offset;
        contextp->minor_limit += offset;
        tablep = contextp->minor_roots;
        max_index = contextp->next_root - contextp->minor_roots;
        for(index = 0; index != max_index; index++)
            tablep[index] += offset;

        /* copy point */
        contextp->copy_point += offset;
        contextp->gc_point   += offset;

        /* Update atomic blocks */
        for(i = 0; i != contextp->atomic_next; i++) {
            Atomic *atomicp = contextp->atomic_info[i];

            /* New copy point */
            atomicp->copy_point += offset;

            /* Update all COW pointers */
            tablep = atomicp->ptr_entries;
            max_index = atomicp->ptr_next;
            for(index = 0; index != max_index; index++)
                tablep[index] += offset;
        }

        /* New heap */
        contextp->mem_limit = new_base + new_size;
        contextp->mem_next += offset;
        contextp->mem_base  = new_base;
        contextp->mem_size  = new_size;

        /* Update all the pointers with the new offset */
        tablep = contextp->pointer_base;
        max_index = contextp->pointer_size / sizeof(void *);
        for(index = 0; index != max_index; index++) {
            ptrp = tablep[index];
            if(PTBL_ENTRY_VALID(ptrp))
                tablep[index] += offset;
        }
    }

    /* Grow the pointer table */
    old_count = contextp->pointer_size / sizeof(void *);
    live_pointers = old_count - contextp->pointer_free;
    if(contextp->pointer_free < MIN_FREE_POINTERS || live_pointers + pointers > POINTER_MAX_LIVE(old_count)) {
        new_count = (old_count + pointers) * 2;
        new_size = new_count * sizeof(void *);
        if(trace_gc)
            fprintf(stderr, "*** GC: increasing pointer table size from %lu to %lu entries\n", old_count, new_count);
        tablep = realloc(contextp->pointer_base, new_size);
        if(tablep == 0) {
            fprintf(stderr, "Out of pointer table memory during request for %lu entries\n", pointers);
            abort();
        }

        /* Link the new entries into the free list */
        nextp = (void *) 1;
        index = new_count - 1;
        while(index >= old_count) {
            tablep[index] = SET_BIT(nextp, PTBL_FREE_OFF);
            nextp = tablep + index;
            index--;
        }
        while(index >= 0) {
            if(PTBL_ENTRY_EMPTY(tablep[index])) {
                tablep[index] = SET_BIT(nextp, PTBL_FREE_OFF);
                nextp = tablep + index;
            }
            index--;
        }
        contextp->pointer_next = nextp;
        contextp->pointer_free += new_count - old_count;
        contextp->pointer_size = new_size;
        contextp->pointer_base = tablep;
        contextp->pointer_limit = tablep + new_count;

        /* Make sure the minor roots also grows */
        tablep = realloc(contextp->minor_roots, new_size);
        if(tablep == 0) {
            fprintf(stderr, "Out of space for minor roots during request for %lu entries\n", pointers);
            abort();
        }
        contextp->next_root = tablep + (contextp->next_root - contextp->minor_roots);
        contextp->minor_roots = tablep;
    }
}

/*
 * Adjust the minor heap after a major collection.
 * The minor heap does not fill up the entire remainder of
 * the major heap in general.
 */
static void adjust_minor(Context *contextp, unsigned long bytes)
{
    unsigned long free_space, desired_space;

    /* Calculate the desired size of the minor heap */
    free_space = contextp->mem_limit - contextp->mem_next;
    desired_space = SIZEUP(MIN_MINOR_SIZE + bytes);
    assert(desired_space <= free_space);

    /* Set up the minor heap */
    contextp->minor_base = contextp->mem_next;
    contextp->minor_limit = contextp->minor_base + desired_space;
    contextp->gc_point = contextp->minor_base;
}

/*
 * Shift the minor heap if we have space and it is getting full.
 */
static void shift_minor(Context *contextp)
{
    unsigned long minor_live, minor_size, max_minor_live, major_free;
    unsigned long size, count, desired_shift;
    void *blockp;

    /* Get minor params */
    minor_live = contextp->mem_next - contextp->minor_base;
    minor_size = contextp->minor_limit - contextp->minor_base;
    max_minor_live = MINOR_MAX_LIVE(minor_size);
    major_free = contextp->mem_limit - contextp->minor_limit;

    /* Check if we should shift */
    if(minor_live > max_minor_live && major_free) {
        /* Shift so that we have only half as much live */
        desired_shift = minor_live - max_minor_live / 2;

        /* Can't shift past the end of the heap */
        if(desired_shift > major_free)
            desired_shift = major_free;

        /* Shift the limit */
        contextp->minor_limit += desired_shift;

        /* Shift the base to the first block after the shift value */
        count = 0;
        blockp = contextp->minor_base + BLOCK_HEADER_SIZE;
        while(count < desired_shift) {
            if(IS_AGGR(blockp))
                size = GET_AGGR_SIZE(blockp) + BLOCK_HEADER_SIZE;
            else
                size = GET_BLOCK_SIZE(blockp) + BLOCK_HEADER_SIZE;

            /*
             * The block _may_ contain pointers into the
             * shifted heap.  This is conservative: we add the
             * block to the major heap regardless.
             */
            if(!IS_ROOT(blockp)) {
                SET_ROOT_BIT(blockp);
                *contextp->next_root++ = blockp;
            }

            /* Advance */
            blockp += size;
            count += size;
        }

        /* Set the base to the new block */
        contextp->minor_base = blockp - BLOCK_HEADER_SIZE;

        /* Adjust gc_point */
        if(contextp->gc_point < contextp->minor_base)
            contextp->gc_point = contextp->minor_base;
    }
}

/************************************************************************
 * MAIN GC
 ************************************************************************/

/*
 * A minor collection.
 */
static void gc_minor(Context *contextp, int *maskp, void **regs)
{
    if(trace_gc)
        fprintf(stderr, "*** GC: performing minor collection\n");

    /* Mark phase */
    mark_minor(contextp, maskp, regs);

    /* Convert roots to indexes */
    normalize(contextp, maskp, regs);

    /* Add roots of outermost copy generation */
    ptable_minor(contextp);

    /* Sweep */
    sweep_minor(contextp);

    /* Shift minor heap if it is getting full */
    shift_minor(contextp);

    /* Convert back to pointers */
    denormalize(contextp, maskp, regs);
}

/*
 * A major collection.
 */
static void gc_major(Context *contextp,
                     int *maskp, void **regs,
                     unsigned long req_bytes,
                     unsigned long req_pointers)
{
    GCInfo *gc_info;

    if(trace_gc)
        fprintf(stderr, "*** GC: performing major collection\n");

    /* Clear the minor roots */
    clear_minor_roots(contextp);

    /* Mark phase */
    mark_major(contextp, maskp, regs);

    /* Convert roots to indexes */
    normalize(contextp, maskp, regs);

    /* Sort the copy generation pointers */
    sort_major(contextp);

    /* Sweep the heap */
    sweep_major(contextp);

    /* Expand the heap if necessary */
    expand_heap(contextp, req_bytes, req_pointers);

    /* Set up the minor heap */
    adjust_minor(contextp, req_bytes);

    /* Convert back to pointers */
    denormalize(contextp, maskp, regs);
}

/*
 * Print GC stats.
 */
void print_stats(Context *contextp)
{
    GCInfo *gc_info = contextp->gc_info;

    fprintf(stderr, "\tLive major bytes: %d\n", gc_info->major_live);
    fprintf(stderr, "\tLive minor bytes: %d\n", gc_info->minor_live);
    fprintf(stderr, "\tLive pointers: %d\n", contextp->pointer_size / sizeof(mc_field) - contextp->pointer_free);
    fprintf(stderr, "\tFree pointer entries: %d\n", contextp->pointer_free);
    fprintf(stderr, "\tTotal major collections: %d\n", gc_info->major_count);
    fprintf(stderr, "\tTotal minor collections: %d\n", gc_info->minor_count);
    fprintf(stderr, "\tTotal ABS time spent in GC so far: %.3g\n", gc_info->gc_abs_time);
    fprintf(stderr, "\tTotal CPU time spent in GC so far: %.3g\n", gc_info->gc_cpu_time);
    fprintf(stderr, "\tTotal number of transactions: %d\n", gc_info->atomic_total);
    fprintf(stderr, "\tMaximum transaction nesting: %d\n", gc_info->atomic_nest);
}

/*
 * Garbage collector.
 * The argument is an array containing the registers.
 *
 * regs[0] = eax
 * regs[1] = ebx
 * regs[2] = ecx
 * regs[3] = edx
 * regs[4] = esi
 * regs[5] = edi
 * regs[6] = ebp
 * regs[7] = ret
 *
 * Get the register mask from the return address
 */
static void gc_main(int code, void **regs)
{
    struct timeval now;
    struct rusage rusage;
    double abs_before, abs_after, abs_total;
    double cpu_before, cpu_after, cpu_total;
    double user_time, sys_time;
    unsigned long minor_size, minor_free, req_bytes, req_pointers;
    int *maskp, do_minor, do_major;
    Context *contextp;
    GCInfo *gc_info;

    /* Timing stats */
    gettimeofday(&now, (struct timezone *) 0);
    abs_before = now.tv_sec + now.tv_usec * 1e-6;
    getrusage(RUSAGE_SELF, &rusage);
    cpu_before =
        rusage.ru_utime.tv_sec
        + rusage.ru_utime.tv_usec * 1e-6
        + rusage.ru_stime.tv_sec
        + rusage.ru_stime.tv_usec * 1e-6;

    /* Get the context pointer from %ebp */
    contextp = (Context *) regs[RES_EBP];
    gc_info = contextp->gc_info;
    req_pointers = (unsigned long)regs[RES_NUM_POINTERS];
    req_bytes = (unsigned long)regs[RES_NUM_BYTES];
    maskp = (int *)regs[RES_REG_MASK];

    /* Validate the context if applicable */
    if(VALIDATE_CONTEXT) {
        fprintf(stderr, "*** Validating context before GC.\n");
        validate_context(contextp);
    }

    /* Remember the number of free pointers; grow if it is small */

    /* Print out the register names that contain pointers */
    if(trace_gc) {
        fprintf(stderr, "*** GC: start\n");
        fprintf(stderr, "\tTotal heap size: %d bytes\n", contextp->mem_size);
        fprintf(stderr, "\tPointer table size: %d words, %u free\n",
                contextp->pointer_size / sizeof(void *),
                contextp->pointer_free);
        fprintf(stderr, "\tRequested pointers %lu, requested bytes %lu\n", req_pointers, req_bytes);
    }

    /*
     * Do a minor collection if all of the following hold:
     *    1. The request is no larger than half the size of the minor heap
     *    2. The minor heap is at least MIN_MINOR_SIZE
     * This is just an estimate, we may need to do a major
     * collection if the minor collection is not successful.
     */
    minor_size = contextp->mem_limit - contextp->minor_base;
    if(req_bytes < minor_size / 2 && minor_size >= MIN_MINOR_SIZE && code <= GC_MINOR) {
        do_minor = 1;
        do_major = 0;
    }
    else {
        do_minor = 0;
        do_major = 1;
    }

    if(do_minor) {
        /* Usual case: do a minor collection */
        gc_minor(contextp, maskp, regs);

        /* Check again, and do a major collection if necessary */
        minor_free = contextp->minor_limit - contextp->mem_next;
        if(req_bytes >= minor_free || req_pointers >= contextp->pointer_free) {
            do_major = 1;
        }
    }

    if(do_major) {
        /* In extreme cases, collect all copy generations that have not been collected already */
        gc_major(contextp, maskp, regs, req_bytes, req_pointers);
    }

    /* GC timing stats */
    gettimeofday(&now, (struct timezone *) 0);
    abs_after = now.tv_sec + now.tv_usec * 1e-6;
    abs_total = abs_after - abs_before;
    gc_info->gc_abs_time += abs_total;

    /* Validate the context if applicable */
    if(VALIDATE_CONTEXT) {
        fprintf(stderr, "*** Validating context after GC.\n");
        validate_context(contextp);
    }

    /* Process timing stats */
    getrusage(RUSAGE_SELF, &rusage);
    user_time = rusage.ru_utime.tv_sec + rusage.ru_utime.tv_usec * 1e-6;
    sys_time = rusage.ru_stime.tv_sec + rusage.ru_stime.tv_usec * 1e-6;
    cpu_after = user_time + sys_time;
    cpu_total = cpu_after - cpu_before;
    gc_info->gc_cpu_time += cpu_total;

    /* Print out the results */
    if(trace_gc) {
        fprintf(stderr, "*** GC: done\n");
        fprintf(stderr, "\tRequested pointers %lu, requested bytes %lu\n", req_pointers, req_bytes);
        fprintf(stderr, "\tTotal ABS time for this collection: %.3g\n", abs_total);
        fprintf(stderr, "\tTotal CPU time for this collection: %.3g\n", cpu_total);
        fprintf(stderr, "\tTotal CPU time for this process: %.3g (user), %.3g (sys)\n", user_time, sys_time);
        fprintf(stderr, "\tPercent of CPU time spent in GC: %2.2g%%\n", gc_info->gc_cpu_time * 100 / (user_time + sys_time));
        print_stats(contextp);
    }
}

/*
 * Main GC functions.
 */
void gc(void **regs)
{
    gc_main(GC_DEFAULT, regs);
}

/*
 * Allow FC programs to call the garbage collector.
 */
int __ext_fc_gc(Context *contextp, int command, void **regs)
{
    int old_trace_gc = trace_gc;
    trace_gc = 1; 
    gc_main(command, regs);
    trace_gc = old_trace_gc;
    return 0;
}
