/*
   Contains definitions for the various functions that use the
   void **regs calling convention (currently this is the GC and
   the COW mechanism).
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __x86_reserve_included
#define __x86_reserve_included


/* Register indexes when GC is called.  The registers are always
   pushed onto the stack before these functions are called.  */
#define RES_EAX            0
#define RES_EBX            1
#define RES_ECX            2
#define RES_EDX            3
#define RES_ESI            4
#define RES_EDI            5
#define RES_EBP            6
#define RES_MAX_REGS       7

/* Other arguments shared between GC and COW. */
#define RES_CALLER         7
#define RES_NUM_POINTERS   8
#define RES_NUM_BYTES      9
#define RES_REG_MASK       10
#define RES_REG_COUNT      10

/* Other arguments specific to COW (GC ignores these). */
#define RES_COPY_POINTER   11

/* GC control gets an extra argument */
#define RES_GC_CONTROL     11


#endif /* __x86_reserve_included */
