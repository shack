/*
 * x86 garbage collector.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#ifndef _X86_GC
#define _X86_GC

extern int trace_gc;

/* GC control */
#define GC_DEFAULT      0
#define GC_MINOR        1
#define GC_MAJOR        2

/* Main garbage collector */
void gc(void **regs);
void gc_control(void **regs);

/* Utilities */
void clear_minor_roots(Context *contextp);
void print_stats(Context *contextp);

#endif /* _X86_GC */
