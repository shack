/*
   x86 Network Migration runtime
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


/* Required include files */
#include "x86_connection.h"
#include "x86_context.h"
#include "x86_runtime.h"


/* Debugging flags */
static int debug_migrate = 0;


/* Control definitions */
#define  ENABLE_VALIDATION       0


/* We fake the tag for aggregate blocks (for transmission) */
#define  AGGR_TAG                (TAG_MASK + 1)
#define  END_HEAP_FLAG           0x80ffffff
#define  END_ATOMIC_FLAG         0x81ffffff


/* Set the NULL, empty value for pointer tables */
#define  PTBL_EMPTY_ENTRY        SET_BIT(0, PTBL_FREE_OFF)


/* For migrate sender (source):  true if we were on a bidirectional
   connection, AND the server sent a NAK reply to our check. */
#define  SRC_CHECK_NAK(conn)     ((conn)->bidir && (check_reply((conn)) != 0))
#define  DST_SEND_STRING(conn, msg) \
                                 ((conn)->bidir ? send_string((conn), (msg)) : 0)


/***  Utilities and Protocols  ***/


/* Check that the reply is an acknowledgement */
static int check_reply(migrate_connection *conn) {

   char buffer[0x1000];          /* Temporary buffer for ack */
   char *p = buffer;             /* Pointer into the buffer */

   assert(conn != NULL);

   /* Read the reply */
   if(recv_string(conn, buffer, sizeof(buffer)) < 0) return(-1);
   if(strncmp(buffer, "ACK:", 4) != 0) {
      /* NAK or bogus message was received. */
      fprintf(stderr, "check_reply: \"%s\"\n", buffer);
      return(-1);
   }

   /* Debugging messages */
   if(debug_migrate) {
      while(*p != '\0' && *p != '\n') ++p;
      *p = '\0';
      fprintf(stderr, "migrate:  check_reply: \"%s\"\n", buffer);
   }

   /* Success */
   return(0);

}


/***  Higher level client primitives  ***/


/* Negotiate over the protocol version number */
static int establish_protocol(migrate_connection *conn) {

   assert(conn != NULL);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  establishing migrate protocol\n");
   }

   /* Send the version string */
   if(send_string(conn, PROCESS_VERSION_STRING) != 0) return(-1);

   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success! */
   return(0);

}


/* Send the binary FIR data.  This is a straightforward transmission
   of the buffer to the destination; no difficulties here... */
static int send_binary_fir(migrate_connection *conn, unsigned int count, const char *bin_fir) {

   assert(conn != NULL);
   assert(count >= 0);
   assert(bin_fir != NULL);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sending binary FIR code\n");
   }

   /* Send the binary FIR over */
   if(send_buffer(conn, bin_fir, count) < 0) return(-1);

   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success! */
   return(0);

}


/* Send the migration key over.  This is the label we will be jumping
   to when the destination process starts up.  This number must be
   embedded in the FIR, associated with the appropriate migrate call,
   and must not be modified by the destination. */
static int send_key(migrate_connection *conn, unsigned int key) {

   char buf[0x1000];

   assert(conn != NULL);

   /* Setup the label buffer that will be transmitted */
   snprintf(buf, sizeof(buf), "migrate_label_%05d", key);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sending key: \"%s\"\n", buf);
   }

   /* Send the key */
   if(send_string(conn, buf) < 0) return(-1);

   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success! */
   return(0);

}


/* Send the register block index over.  This is the block in memory that
   all registers were packed into.  From the FIR perspective, these are
   the registers that are named as arguments to the continuation function,
   and must be packed in the order they are named.  This block is packed
   and unpacked by the MIR, so we don't have to do any work here.  */
static int send_register_index(migrate_connection *conn, unsigned int regs) {

   assert(conn != NULL);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sending register index: %08x (unhashed %d)\n", regs, regs ^ HASH_INDEX);
   }

   /* Send the index */
   if(send_int32(conn, regs) < 0) return(-1);

   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success! */
   return(0);

}


/* Send the pointer table/data over.  The pointer table data must contain
   the number of pointer table entries used and the amount of heap currently
   in use, so the destination can allocate enough pointer table space and
   heap space to transfer all of our context.  The client is responsible for
   reconstructing mem_* and pointer_* variables in the context before we
   begin transferring data blocks.

   Note:  because of the generations, we must assume the blocks are already
   compressed and are referenced by the pointer table.  It is probably in
   the client's best interests to run a full garbage collection before
   migration; we will not perform GC here. */
static int send_pointer_table_data(migrate_connection *conn, const Context *c) {

   unsigned int min_pointer_size;      /* Minimum pointer table size */
   unsigned int min_heap_size;         /* Minimum heap size required */

   assert(conn != NULL);
   assert(c != NULL);

   /* Get those numbers */
   min_pointer_size = c->pointer_size / sizeof(void *);
   min_heap_size = c->mem_next - c->mem_base;

   /* Debugging */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  there are %d pointers and %d bytes in heap\n", min_pointer_size, min_heap_size);
   }

   /* Send the required size of the pointer table and heap */
   if(send_int32(conn, min_pointer_size) < 0) return(-1);
   if(send_int32(conn, min_heap_size) < 0) return(-1);

   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success */
   return(0);

}


/***  Data Block Migration  ***/


/* Send an aggregate data block over.  The data pointer points to
   the first word of data in the block (after the header information).  */
static int send_aggr_data_block(migrate_connection *conn, const char *blockp, unsigned int block_size) {

   /* Send the information describing this data block */
   assert(conn != NULL);
   assert(blockp != NULL);
   assert(block_size >= 0);
   assert(IS_AGGR(blockp));

   /* Send the aggregate header information */
   if(send_int32(conn, AGGR_TAG) < 0) return(-1);
   if(send_int32(conn, GET_AGGR_INDEX(blockp)) < 0) return(-1);

   /* Send the data block itself (note: send_int32_block transmits size) */
   return(send_int32_block(conn, (const int *)blockp, block_size));

}


/* Send all the data blocks over.  We transmit every block in the heap
   (this is why it is probably advantageous to run GC before doing this
   step). */
static int send_data_blocks(migrate_connection *conn, const Context *c) {

   const char *mem_base = (const char *)c->mem_base;  /* Beginning of heap blocks */
   const char *mem_next = (const char *)c->mem_next;  /* Endpoint of heap blocks */
   const char *current = mem_base;        /* Current block beginning to examine */
   const char *blockp;                    /* Current block (points after header) */
   unsigned int block_size;               /* Size of the current block (data only) */

   assert(conn != NULL);
   assert(c != NULL);

   /* Send all appropriate data over */
   while(current < mem_next) {
      /* Blocks must be word-aligned; get the pointer to the
         block data (skip over the block header)... */
      assert(((unsigned int)current & 3) == 0);
      blockp = current + BLOCK_HEADER_SIZE;
      assert(blockp <= mem_next);

      /* What type of block are we transmitting? */
      if(IS_AGGR(blockp)) {
         /* Get the aggregate block size */
         block_size = GET_AGGR_SIZE(blockp);
         assert(blockp + block_size <= mem_next);
         /* Transmit this block's information */
         send_aggr_data_block(conn, blockp, block_size);
      } else {
         /* Unknown block type */
         fprintf(stderr, "migrate:  Unknown block type in heap at %p (offset %d)\n", current, current - mem_base);
         return(-1);
      } /* Block type? */

      /* Advance to next block; current points to next header */
      current = blockp + block_size;
   } /* Scan memory until we hit mem_limit. */

   /* Send an end-of-heap marker for convenience */
   if(send_int32(conn, END_HEAP_FLAG) < 0) return(-1);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sent heap data (%d bytes), waiting for confirmation\n", c->mem_next - c->mem_base);
   }

   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success */
   return(0);

}


/* Receive an aggregate block.  We've already read the block type
   at this point, so we only need to read the block header and
   size information, setup the header, and update the pointer table
   to have a pointer to this block.  The final block size should be
   returned in block_size.  */
static int recv_aggr_data_block(migrate_connection *conn, Context *c, char *blockp, unsigned int *block_size) {

   unsigned int block_index;           /* Pointer index for this block */
   unsigned int block_size_local;      /* Local variable for block size */

   assert(conn != NULL);
   assert(c != NULL);
   assert(blockp != NULL);
   assert(block_size != NULL);

   /* Receive the block index and size */
   if(recv_int32(conn, &block_index) < 0) return(-1);
   if(recv_int32(conn, &block_size_local) < 0) return(-1);
   *block_size = block_size_local;

   /* Make sure the size doesn't overflow heap */
   if(blockp + block_size_local > (char *)c->mem_limit) {
      /* Buffer overflow, abort */
      fprintf(stderr, "migrate:  Heap overflow reading block index %d, of size %d\n",
              block_index, block_size_local);
      fprintf(stderr, "migrate:  blockp      = %p\n", blockp);
      fprintf(stderr, "migrate:  block_size  = %d\n", block_size_local);
      fprintf(stderr, "migrate:  mem_limit   = %p\n", c->mem_limit);
      DST_SEND_STRING(conn, "NAK: Heap overflow reading block");
      return(-1);
   }

   /* Update pointer table - the -1 is to ignore the final NULL
      pointer for the free linked list in the pointer table... */
   if(block_index >= (c->pointer_size / sizeof(void *)) - 1) {
      /* Pointer table overflow, abort */
      fprintf(stderr, "migrate:  Pointer table overflow reading block index %d, table size is %d\n",
              block_index, c->pointer_size / sizeof(void *));
      DST_SEND_STRING(conn, "NAK: Pointer table overflow reading block");
      return(-1);
   }
   if(!PTBL_ENTRY_EMPTY(c->pointer_base[block_index])) {
      /* Pointer table entry already occupied; generations use this
         feature but we do not handle them correctly yet, so fail
         for now. */
      fprintf(stderr, "migrate:  Pointer table entry already occupied for index %d\n", block_index);
      DST_SEND_STRING(conn, "NAK: Pointer table entry already occupied while reading block");
      return(-1);
   }
   c->pointer_base[block_index] = blockp;

   /* Setup block header and read in the data */
   SET_AGGR_HEADER(blockp, block_index, block_size_local);
   if(recv_int32_block(conn, (int *)blockp, block_size_local) < 0) return(-1);

   /* We have success! */
   return(0);

}


/* Clear the pointer table entries */
static void ptbl_clear_entries(Context *c) {

   unsigned int pointer_size;             /* Size of pointer table */
   void **pointers = c->pointer_base;     /* Pointer table base */
   unsigned int index;                    /* Pointer table iterator */

   /* Clear out the pointer table */
   pointer_size = c->pointer_size / sizeof(void *);
   for(index = 0; index < pointer_size; ++index) {
      pointers[index] = PTBL_EMPTY_ENTRY;
   }

}


/* Construct the linked list in the pointer table. */
static void ptbl_build_linked_list(Context *c) {

   unsigned int pointer_size;             /* Size of the pointer table */
   unsigned int *pointers = (unsigned int *)c->pointer_base;   /* Pointer table */
   unsigned int **lastlink = (unsigned int **)&c->pointer_next;/* Linking source */
   unsigned int index;                    /* Pointer table iterator */

   /* Update pointer table/build linked list */
   pointer_size = c->pointer_size / sizeof(void *);
   c->pointer_free = pointer_size - 1;
   for(index = 0; index < pointer_size; ++index) {
      if(PTBL_ENTRY_EMPTY(pointers[index])) {
         if(lastlink == (unsigned int **)&c->pointer_next) {
            /* First free entry in the list */
            *lastlink = &(pointers[index]);
         } else {
            *lastlink = SET_BIT(&(pointers[index]), PTBL_FREE_OFF);
         }
         lastlink = (unsigned int **)&(pointers[index]);
      } else {
         /* One less free pointer to play with */
         --c->pointer_free;
      }
   }

}


/* Receive all data blocks, and fix up the context as needed.  We assume
   by this point that the heap and pointer tables are allocated; therefore
   all mem_* and pointer_* fields in the context should be initialised
   except for pointer_next and pointer_free (the buffer contents may be
   arbitrary).  */
static int recv_data_blocks(migrate_connection *conn, Context *c) {

   unsigned int tag;                      /* Tag of block to read */
   char *current = (char *)c->mem_base;   /* Current spot in heap */
   char *mem_next = (char *)c->mem_next;  /* Endpoint for blocks in heap */
   char *blockp;                          /* Current block (points after header) */
   unsigned int block_size;               /* Size of the current block (data only) */
   unsigned int flag;                     /* Heap end-of-data flag */

   /* Sanity check on context */
   assert(c->mem_limit - c->mem_base == c->mem_size);
   assert(c->mem_next >= c->mem_base && c->mem_next <= c->mem_limit);
   assert(c->pointer_limit - c->pointer_base == c->pointer_size / sizeof(void *));
   /* pointer_next and pointer_free are set by this function */

   /* Clear the pointer table entries */
   ptbl_clear_entries(c);

   /* Read until we reach the end of the heap data */
   while(current < mem_next) {
      /* Get the ``proper'' pointer (the pointer to the first word
         of actual data) for this block. */
      blockp = current + BLOCK_HEADER_SIZE;
      if(blockp > mem_next) {
         fprintf(stderr, "migrate:  Heap overflow reading block header\n");
         DST_SEND_STRING(conn, "NAK: Heap overflow reading block header");
         return(-1);
      }

      /* Find out what type of block we're looking at */
      if(recv_int32(conn, &tag) < 0) return(-1);
      if(tag == AGGR_TAG) {
         /* Aggregate block; process! */
         if(recv_aggr_data_block(conn, c, blockp, &block_size) < 0) return(-1);
      } else {
         /* Unknown type */
         fprintf(stderr, "migrate:  Only know how to receive aggregate blocks; got tag 0x%08x\n", tag);
         DST_SEND_STRING(conn, "NAK: Only know how to receive aggregate blocks");
         return(-1);
      } /* Block type? */

      /* Update memory pointers */
      current = blockp + block_size;
   }

   /* Make sure the next flag transmitted is the end-heap flag */
   if(recv_int32(conn, &flag) < 0) return(-1);
   if(flag != END_HEAP_FLAG) {
      fprintf(stderr, "migrate:  Expected end-of-heap flag, got %d\n", flag);
      DST_SEND_STRING(conn, "NAK: Expected end-of-heap flag");
      return(-1);
   }

   /* Update pointer table/build linked list */
   ptbl_build_linked_list(c);

   /* Report ok */
   if(DST_SEND_STRING(conn, "ACK: got all data blocks") != 0) return(-1);

   /* Success! */
   return(0);

}


/***  Globals Migration  ***/


/* Send the global index parametres.  The globals will not be allocated
   in the same locations on the destination machine, so we iterate over
   the globals in order, and transmit their block indices.  On destination
   we will lookup the block in the pointer table (the block data will have
   already been copied) and store the real pointer back into the context. */
static int send_globals(migrate_connection *conn, const Context *c) {

   unsigned int *global;         /* Pointer to a global block */
   unsigned int index;           /* Iterate over all globals */
   unsigned int global_index;    /* Index of global in context */

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sending global index table\n");
   }

   /* Try to send indices of all globals across */
   if(send_int32(conn, GLOBALS_SIZE) != 0) return(-1);
   for(index = 0; index < GLOBALS_SIZE; ++index) {
      global_index = __globals_base[index].index;
      global = Global(c, global_index);
      if(IS_AGGR(global)) {
         if(send_int32(conn, GET_AGGR_INDEX(global)) != 0) return(-1);
      } else {
         fprintf(stderr, "migrate:  don't know about non-aggregate globals\n");
         return(-1);
      }
   }

   /* Wait for a response */
   if(SRC_CHECK_NAK(conn)) return(-1);

   /* Success! */
   return(0);

}


/* Receive the global index parametres.  See above. */
static int recv_globals(migrate_connection *conn, Context *c) {

   unsigned int count;           /* Number of globals to expect */
   unsigned int index;           /* Iterate over all globals */
   unsigned int block_index;     /* Index for a global block */
   unsigned int global_index;    /* Index of global in context */

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  waiting for global index table\n");
   }

   /* Verify the size of the globals */
   if(recv_int32(conn, &count) != 0) return(-1);
   if(GLOBALS_SIZE != count) {
      fprintf(stderr, "migrate:  Global count size mismatch: expected %d, got %d\n", GLOBALS_SIZE, count);
      DST_SEND_STRING(conn, "NAK: Global count size mismatch");
      return(-1);
   }

   /* Update all global pointers based on indices */
   for(index = 0; index < GLOBALS_SIZE; ++index) {
      if(recv_int32(conn, &block_index) != 0) return(-1);
      if(block_index >= (c->pointer_size / sizeof(void *)) - 1) {
         fprintf(stderr, "migrate:  Global index %d out of range, table size is %d\n",
                 block_index, c->pointer_size / sizeof(void *));
         DST_SEND_STRING(conn, "NAK: Global index out of range");
         return(-1);
      }
      if(PTBL_ENTRY_EMPTY(c->pointer_base[block_index])) {
         fprintf(stderr, "migrate:  Global index %d points to empty table entry\n", block_index);
         DST_SEND_STRING(conn, "NAK: Global index points to empty table entry");
         return(-1);
      }
      global_index = __globals_base[index].index;
      Global(c, global_index) = c->pointer_base[block_index];
   }

   /* Send ok message */
   if(DST_SEND_STRING(conn, "ACK: global index table received") < 0) return(0);

   /* Success! */
   return(0);

}


/***  Atomic Levels Migration  ***/


/* Send the atomic level data.  This sends the copy_points, fun/env pointer
   indices, and the differential pointer table for each atomic level.  Many
   pointers must be sent relative to the mem_base value.  */
static int send_atomic_levels(migrate_connection *conn, Context *c) {

   const char *mem_base = (const char *)c->mem_base;  /* Beginning of heap blocks */
   const Atomic *atomicp;                 /* Pointer to an atomic level to send */
   unsigned int i, j;                     /* These aren't iterators... really! */
   
   assert(conn != NULL);
   assert(c != NULL);
   
   /* Send the number of atomic levels over; if this value is zero,
      then there are no current atomic levels. */
   if(send_int32(conn, c->atomic_next) < 0) return(-1);
   
   /* Send the atomic level information */
   for(i = 0; i < c->atomic_next; ++i) {
      atomicp = c->atomic_info[i];
      
      /* Send the function/env indices */
      if(send_int32(conn, atomicp->fun) < 0) return(-1);
      if(send_int32(conn, atomicp->env) < 0) return(-1);
      
      /* Send the copy point (relative to mem_base) */
      if(send_int32(conn, (const char *)atomicp->copy_point - mem_base) < 0) return(-1);
      
      /* Send the differential pointer table statistics */
      if(send_int32(conn, atomicp->ptr_next) < 0) return(-1);
      
      /* Send the differential pointer table. Each pointer is relative 
         to the mem_base pointer; each entry is a real pointer. */
      for(j = 0; j < atomicp->ptr_next; ++j) {
         if(send_int32(conn, (const char *)atomicp->ptr_entries[j] - mem_base) < 0) return(-1);
      }
   }
   
   /* Send an end-of-levels marker for convenience */
   if(send_int32(conn, END_ATOMIC_FLAG) < 0) return(-1);
   
   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sent %d atomic levels, waiting for confirmation\n", c->atomic_next);
   }
   
   /* Wait for reply */
   if(SRC_CHECK_NAK(conn)) return(-1);
   
   /* Success */
   return(0);

}


/* Receive the atomic level data.  See above.  */
static int recv_atomic_levels(migrate_connection *conn, Context *c) {

   char *mem_base = (char *)c->mem_base;  /* Beginning of heap blocks */
   Atomic *atomicp;                       /* Allocated data for current level */
   unsigned int copy_point_offset;        /* Copy point (offset) for a level */
   unsigned int ptr_entry_offset;         /* Entry (offset) in a diff ptbl */
   unsigned int fun_index;                /* Function index for a level */
   unsigned int env_index;                /* Environment index for a level */
   unsigned int ptr_next;                 /* Number of valid entries in diff ptbl */
   unsigned int flag;                     /* Atomic level end-of-data flag */
   unsigned int size;                     /* Size of blocks we are allocating */
   unsigned int i, j;                     /* The usual suspects... */
   
   assert(conn != NULL);
   assert(c != NULL);
   
   /* We do not release memory here, so make sure there were not already
      some atomic levels. */
   assert(c->atomic_next == 0);
   
   /* Receive the total number of atomic levels over; if this value is 
      zero, then there are no current atomic levels. */
   if(recv_int32(conn, &c->atomic_next) < 0) return(-1);
   if(c->atomic_next < 0) {
      fprintf(stderr, "recv_atomic_levels: Malformed atomic_next value: %d\n", c->atomic_next);
      DST_SEND_STRING(conn, "NAK: malformed atomic_next");
      return(-1);
   }
   
   /* The rest of this code only applies if atomic_next > 0; we guard
      here because the malloc calls will complain otherwise, when we
      try to allocate 0 levels and whatnot. */
   if(c->atomic_next > 0) {
      /* TEMP WARNING about missing safety checks */
      fprintf(stderr, "WARNING:  Certain parametres are not validated for safety in recv_atomic_levels!\n");
   
      /* Prealloc the new atomic levels. Note: this assumes we were called
         immediately after the runtime stub started; that is, we assume here
         that the atomic levels are NOT INITIALISED and that we can discard
         existing levels without releasing any memory. */
      size = c->atomic_next * sizeof(Atomic *);
      c->atomic_info = (Atomic **)realloc(c->atomic_info, size);
      if(c->atomic_info == NULL) {
         fprintf(stderr, "recv_atomic_levels: out of memory attempting to allocate %d bytes\n", size);
         DST_SEND_STRING(conn, "NAK: ran out of memory");
         return(-1);
      }
      c->atomic_size = c->atomic_next;
      
      /* Attempt to receive atomic level information */
      for(i = 0; i < c->atomic_next; ++i) {
         /* Read in the function/env indices */
         if(recv_int32(conn, &fun_index) < 0) return(-1);
         if(recv_int32(conn, &env_index) < 0) return(-1);
         
         /* Make sure both indices are valid -- safety check! */
         if(fun_index < 0 || fun_index >= FUNCTION_SIZE || fun_index % 4 != 0) {
            fprintf(stderr, "recv_atomic_levels: bad function index %d (out of bounds!)\n", fun_index);
            DST_SEND_STRING(conn, "NAK: bad function index sent");
            return(-1);
         }
         if(env_index < 0 || env_index >= c->pointer_size || env_index % 4 != 0) {
            fprintf(stderr, "recv_atomic_levels: bad pointer index %d (out of bounds!)\n", env_index);
            DST_SEND_STRING(conn, "NAK: bad pointer index sent");
            return(-1);
         }
         if(!PTBL_ENTRY_VALID(c->pointer_base[env_index / 4])) {
            fprintf(stderr, "recv_atomic_levels: bad pointer index %d (entry is free!)\n", env_index);
            DST_SEND_STRING(conn, "NAK: bad pointer index sent");
            return(-1);
         }
         
         /* Receive the copy point (char* offset relative to mem_base) */
         if(recv_int32(conn, &copy_point_offset) < 0) return(-1);
         /* TEMP WARNING: copy_point is not validated! */
         
         /* Receive the differential pointer table statistics */
         if(recv_int32(conn, &ptr_next) < 0) return(-1);
         if(ptr_next < 0) {
            fprintf(stderr, "recv_atomic_levels: Malformed ptr_next: %d\n", ptr_next);
            DST_SEND_STRING(conn, "NAK: malformed ptr_next");
            return(-1);
         }
         
         /* Attempt to allocate the atomic level, including sufficient
            space for the initial differential pointer table. */
         size = sizeof(Atomic) + ptr_next * sizeof(void *);
         atomicp = (Atomic *)malloc(size);
         if(atomicp == NULL) {
            fprintf(stderr, "recv_atomic_levels: out of memory attempting to allocate %d bytes.\n", size);
            DST_SEND_STRING(conn, "NAK: ran out of memory");
            return(-1);
         }
         
         /* Initialise the atomicp data structure. */
         atomicp->fun = fun_index;
         atomicp->env = env_index;
         atomicp->copy_point = mem_base + copy_point_offset;
         atomicp->ptr_size = ptr_next;
         atomicp->ptr_next = ptr_next;
         atomicp->ptr_mark = -1;
         atomicp->ptr_sweep = 0;
         
         /* Read the differential pointer table. */
         for(j = 0; j < ptr_next; ++j) {
            if(recv_int32(conn, &ptr_entry_offset) < 0) return(-1);
            atomicp->ptr_entries[j] = mem_base + ptr_entry_offset;
            /* TEMP WARNING: This block pointer is not validated! */
         }
         
         /* Install this level into the overall atomic_info array. */
         c->atomic_info[i] = atomicp;
      }

      /* Repair the GC pointers */      
      atomicp = ATOMIC_LEVEL(c, c->atomic_next);
      c->copy_point = atomicp->copy_point;
      c->gc_point = atomicp->copy_point;
   } /* Was atomic_next > 0? */
   
   /* Send an end-of-levels marker for convenience */
   if(recv_int32(conn, &flag) < 0) return(-1);
   if(flag != END_ATOMIC_FLAG) {
      fprintf(stderr, "migrate:  Expected end-of-atomic flag, got %d\n", flag);
      DST_SEND_STRING(conn, "NAK: Expected end-of-atomic flag");
      return(-1);
   }

   /* Report ok */
   if(DST_SEND_STRING(conn, "ACK: got all atomic data") != 0) return(-1);
   
   /* Success */
   return(0);

}


/***  Validation functions  ***/


/* Checksum a single datablock */
static int checksum_data_block(const int *data) {

   int data_size = GET_BLOCK_SIZE(data);
   int sum = 0;
   int index;

   for(index = 0; index < data_size; ++index) {
      sum += data[index] ^ index;
   }
   return(sum);

}


/* Send data across to be validated - this is sent by the NEW process */
static int send_validate_data(migrate_connection *conn, const Context *c) {

   unsigned int *block;
   unsigned int *global;
   unsigned int *pointers = (unsigned int *)c->pointer_base;
   unsigned int index;

   if(!ENABLE_VALIDATION) return(0);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  sending parametres for validation\n");
   }

   /* Verify elements in the context - memory */
   if(send_int32(conn, c->mem_limit - c->mem_base) != 0) return(-1);
   if(send_int32(conn, c->mem_next - c->mem_base) != 0) return(-1);
   if(send_int32(conn, c->mem_size) != 0) return(-1);

   /* Verify elements in the context - pointer table */
   if(send_int32(conn, c->pointer_limit - c->pointer_base) != 0) return(-1);
   if(send_int32(conn, c->pointer_free) != 0) return(-1);
   if(send_int32(conn, c->pointer_size) != 0) return(-1);

   /* Check the reply */
   if(check_reply(conn) != 0) return(-1);

   /* Verify data blocks and pointer table linked list */
   for(index = 0; index < c->pointer_size; ++index) {
      if(pointers[index] == 1) {
         /* This is the NULL free pointer */
         if(send_int32(conn, 0xFFFFFFFE) != 0) return(-1);
      } else if(PTBL_ENTRY_EMPTY(pointers[index])) {
         /* This is a free pointer */
         if(send_int32(conn, 0xFFFFFFFF) != 0) return(-1);
         if(send_int32(conn, (void **)CLEAR_BIT(pointers[index], PTBL_FREE_OFF) - c->pointer_base) != 0) return(-1);
      } else {
         /* This points to a data block */
         block = (unsigned int *)PTBL_POINTER(pointers[index]);
         if(send_int32(conn, 0x00000000) != 0) return(-1);
         /* BUG: aggregate blocks don't have tags
            if(send_int32(conn, GET_BLOCK_TAG(block)) != 0) return(-1); */
         if(IS_AGGR(block)) {
            if(send_int32(conn, GET_AGGR_INDEX(block)) != 0) return(-1);
            if(send_int32(conn, GET_AGGR_SIZE(block)) != 0) return(-1);
         } else {
            fprintf(stderr, "migrate:  cannot validate non-aggregate blocks\n");
            return(-1);
         }
         if(send_int32(conn, checksum_data_block(block)) != 0) return(-1);
      }
   }

   /* Check the reply */
   if(check_reply(conn) != 0) return(-1);

   /* Verify globals */
   if(send_int32(conn, GLOBALS_SIZE) != 0) return(-1);
   for(index = 0; index < GLOBALS_SIZE; ++index) {
      global = (unsigned int *)c->globals[index];
      if(IS_AGGR(global)) {
         if(send_int32(conn, GET_AGGR_INDEX(global)) != 0) return(-1);
      } else {
         fprintf(stderr, "migrate:  cannot validate non-aggregate globals\n");
         return(-1);
      }
   }

   /* Check the reply */
   if(check_reply(conn) != 0) return(-1);

   /* Success! */
   return(0);

}


/* Check data that is coming over the line to be verified by OLD process */
static int recv_validate_data(migrate_connection *conn, const Context *c) {

   unsigned int i;
   unsigned int *block;
   unsigned int *pointers = (unsigned int *)c->pointer_base;
   unsigned int index;
   char buf[0x1000];

   if(!ENABLE_VALIDATION) return(0);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  waiting for validation parametres\n");
   }

   /* Verify elements in the context - memory */
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i < c->mem_next - c->mem_base) {
      send_string(conn, "NAK: Memory limit is too small to hold all data");
      return(-1);
   }
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i != c->mem_next - c->mem_base) {
      snprintf(buf, sizeof(buf), "NAK: Memory next pointer misplaced, want offset %d, got offset %d\n", c->mem_next - c->mem_base, i);
      send_string(conn, buf);
      return(-1);
   }
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i < c->mem_next - c->mem_base) {
      send_string(conn, "NAK: Memory size is too small to hold all data");
      return(-1);
   }

   /* Verify elements in the context - pointer table */
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i != c->pointer_limit - c->pointer_base) {
      send_string(conn, "NAK: Pointer limit is bogus");
      return(-1);
   }
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i != c->pointer_free) {
      snprintf(buf, sizeof(buf), "NAK: Pointer free is bogus, expecting it to be %d, but got %d", c->pointer_free, i);
      send_string(conn, buf);
      return(-1);
   }
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i != c->pointer_size) {
      send_string(conn, "NAK: Pointer size is bogus");
      return(-1);
   }

   /* Report ok so far */
   if(send_string(conn, "ACK: parametres verified") != 0) return(-1);

   /* Verify data blocks and pointer table linked list */
   for(index = 0; index < c->pointer_size; ++index) {
      if(pointers[index] == 1) {
         /* This is the NULL pointer for free list */
         if(recv_int32(conn, &i) != 0) return(-1);
         if(i != 0xFFFFFFFE) {
            snprintf(buf, sizeof(buf), "NAK: Pointer %d, expected NULL free, but wasn't", index);
            send_string(conn, buf);
            return(-1);
         }
      } else if(PTBL_ENTRY_EMPTY(pointers[index])) {
         /* This is a free pointer */
         if(recv_int32(conn, &i) != 0) return(-1);
         if(i != 0xFFFFFFFF) {
            snprintf(buf, sizeof(buf), "NAK: Pointer %d, expected free, but was allocated", index);
            send_string(conn, buf);
            return(-1);
         }
         if(recv_int32(conn, &i) != 0) return(-1);
         if(i != (void **)CLEAR_BIT(pointers[index], PTBL_FREE_OFF) - c->pointer_base) {
            snprintf(buf, sizeof(buf), "NAK: Pointer %d, free pointer pointing to wrong location", index);
            send_string(conn, buf);
            return(-1);
         }
      } else {
         /* This points to a data block */
         block = (unsigned int *)PTBL_POINTER(pointers[index]);
         if(recv_int32(conn, &i) != 0) return(-1);
         if(i != 0x00000000) {
            snprintf(buf, sizeof(buf), "NAK: Pointer %d, expected allocated, but was free", index);
            send_string(conn, buf);
            return(-1);
         }

         /* Get the block tag */
         if(recv_int32(conn, &i) != 0) return(-1);
         if(i != GET_BLOCK_TAG(block)) {
            snprintf(buf, sizeof(buf), "NAK: Pointer %d, block tag bogus, expected %ld, got %d", index, GET_BLOCK_TAG(block), i);
            send_string(conn, buf);
            return(-1);
         }

         /* What type of block are we examining? */
         if(IS_AGGR(block)) {
            /* Aggregate block */
            if(recv_int32(conn, &i) != 0) return(-1);
            if(i != GET_AGGR_INDEX(block)) {
               snprintf(buf, sizeof(buf), "NAK: Pointer %d, block index bogus, expected %ld, got %d", index, GET_AGGR_INDEX(block), i);
               send_string(conn, buf);
               return(-1);
            }
            if(recv_int32(conn, &i) != 0) return(-1);
            if(i != GET_AGGR_SIZE(block)) {
               snprintf(buf, sizeof(buf), "NAK: Pointer %d, block size bogus, expected %ld, got %d", index, GET_AGGR_SIZE(block), i);
               send_string(conn, buf);
               return(-1);
            }
         } else {
            /* Unknown block */
            fprintf(stderr, "migrate:  cannot deal with non-aggregate blocks in validate\n");
            return(-1);
         }

         /* Validate the checksum on block data */
         if(recv_int32(conn, &i) != 0) return(-1);
         if(i != checksum_data_block(block)) {
            snprintf(buf, sizeof(buf), "NAK: Pointer %d, block checksum bogus", index);
            send_string(conn, buf);
            return(-1);
         }
      }
   }

   /* Report ok so far */
   if(send_string(conn, "ACK: parametres verified") != 0) return(-1);

   /* Verify globals */
   if(recv_int32(conn, &i) != 0) return(-1);
   if(i != GLOBALS_SIZE) {
      send_string(conn, "NAK: disagree on globals size");
      return(-1);
   }
   for(index = 0; index < GLOBALS_SIZE; ++index) {
      if(recv_int32(conn, &i) != 0) return(-1);
      if(i != GET_AGGR_INDEX(c->globals[index])) {
         send_string(conn, "NAK: disagree in globals table");
         return(-1);
      }
   }

   /* Report ok */
   if(send_string(conn, "ACK: parametres verified") != 0) return(-1);

   /* Success! */
   return(0);

}


/***  Main Client and Server Functions  ***/


/* Handy debugging functions */
#define  CLIENT_FAIL(conn)          do { close_connection((conn)); return; } while(0)
#define  CLIENT_FAIL_MSG(msg, conn) do { fprintf(stderr, "migrate:  %s\n", (msg)); CLIENT_FAIL(conn); } while(0)
#define  SERVER_FAIL(conn)          do { close_connection((conn)); return(1); } while(0)


/* Main migrate function */
void __migrate(Context *c, const char *bin_fir, unsigned int key, unsigned int regs, Pointer *destp, int off) {

   /* Data pointers */
   const char *dest = (const char *)destp + off;
   const unsigned int bin_fir_size = *(const unsigned int *)bin_fir;
   const char *bin_fir_data = bin_fir + sizeof(unsigned int);

   migrate_connection conn;      /* Connection characteristics */
   int i;                        /* Misc. iterator */

   /* Determine whether to debug migration */
   debug_migrate = (getenv("NO_DEBUG_MIGRATE") == 0);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  initiating connection to \"%s\"\n", dest);
      fprintf(stderr, "migrate:  Size of bin_fir = %d\n", bin_fir_size);
      fprintf(stderr, "migrate:  First 8 bytes of bin_fir: ");
      for(i = 0; i < 8; ++i) fprintf(stderr, "%02x ", (unsigned char)bin_fir_data[i]);
      fprintf(stderr, "\n");
      print_context(stderr, c);
   }

   /* Setup the connection information */
   if(setup_connection(&conn, dest) < 0) return;
   if(open_source_connection(&conn) < 0) return;

   /* Communicate data to server */
   if(establish_protocol(&conn) < 0)         CLIENT_FAIL(&conn);
   if(send_binary_fir(&conn, bin_fir_size, bin_fir_data) < 0)
                                             CLIENT_FAIL(&conn);
   if(send_key(&conn, key) < 0)              CLIENT_FAIL(&conn);
   if(send_register_index(&conn, regs) < 0)  CLIENT_FAIL(&conn);
   if(send_pointer_table_data(&conn, c) < 0) CLIENT_FAIL(&conn);

   /* At this point, we hold for the destination to start up the new process */
   if(SRC_CHECK_NAK(&conn) != 0)             CLIENT_FAIL(&conn);

   /* Send the data blocks over */
   if(send_data_blocks(&conn, c) != 0)       CLIENT_FAIL(&conn);
   if(send_globals(&conn, c) != 0)           CLIENT_FAIL(&conn);
   if(send_atomic_levels(&conn, c) != 0)     CLIENT_FAIL(&conn);

   /* Wait for verification */
   if(recv_validate_data(&conn, c) != 0)     CLIENT_FAIL_MSG("validation failed", &conn);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  migration successful, %s\n",
             conn.protocol == MIGRATE_CHECKPOINT ? "continuing process" : "shutting down this process");
   }

   /* All done! Now that we've migrated, we can terminate this process. */
   close_connection(&conn);
   if(conn.protocol != MIGRATE_CHECKPOINT) {
      exit(0);
   }

}


/* Stub function to be called when the new process is started.
   This function takes the data blocks as input from the given
   socket and sets up the rest of the process.  */
int migrate_server(Context *c, Migrate *m) {

   migrate_connection conn;

   /* Determine whether to debug migration */
   debug_migrate = (getenv("DEBUG_MIGRATE") != NULL);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  attempting to get %d bytes of data\n", m->heap_size);
   }

   /* We set mem_next to where it *ought* to be to indicate
      to migration code where the end of the live data is */
   c->mem_next = c->mem_base + __migrate_data.heap_size;

   /* Initialise */
   if(open_dest_connection(&conn, m) != 0)   return(1);

   /* Receive the data blocks */
   if(recv_data_blocks(&conn, c) != 0)    SERVER_FAIL(&conn);
   if(recv_globals(&conn, c) != 0)        SERVER_FAIL(&conn);
   if(recv_atomic_levels(&conn, c) != 0)  SERVER_FAIL(&conn);
   if(send_validate_data(&conn, c) != 0)  SERVER_FAIL(&conn);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  migration successful, closing socket\n");
   }

   /* Shutdown socket and begin execution */
   close_connection(&conn);

   /* Debugging messages */
   if(debug_migrate) {
      fprintf(stderr, "migrate:  returning to main program ...\n");
      print_context(stderr, c);
      fflush(stderr);
   }

   if((m->debug & MIGRATE_DEBUG_GDBHOOKS) != 0) {
      char buf[0x1000];
      fprintf(stderr, "migrate pid %d, waiting for GDB hook, press [Enter] to continue...\n", getpid());
      fgets(buf, sizeof(buf), stdin);
   }

   return(m->target(c, m->registers));

}
