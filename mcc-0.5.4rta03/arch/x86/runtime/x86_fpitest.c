/*
 * X86 FPTEST/ITEST check functions
 * These are defined here for performance enhancement, and also
 * because the FPTEST/ITEST checks have to use the very arithmetic
 * ops that the FPTEST/ITEST core is trying to verify.
 *
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>

#include "x86_runtime.h"


int fpitest_msg(const char *msg) {

   printf("check %s\n", msg);
   return(0);

}


int __ext_fc_fpitest_msg(Context *c, Pointer *p, unsigned i) {

   return(fpitest_msg((char *)p + i));

}


int fpitest_data(const void **data) {

   return(0);

}


int __ext_fc_fpitest_data(Context *c, Pointer *data, unsigned i) {

   return(fpitest_data((const void **)data + i));

}


int fpitest_check_true(const char *msg, int value) {

   if(!value) {
      printf("check %s: expected true, got false\n", msg);
   }
   return(0);

}


int __ext_fc_fpitest_check_true(Context *c, Pointer *p, unsigned i, int value) {

   return(fpitest_check_true((char *)p + i, value));

}


int fpitest_check_false(const char *msg, int value) {

   if(value) {
      printf("check %s: expected false, got true\n", msg);
   }
   return(0);

}


int __ext_fc_fpitest_check_false(Context *c, Pointer *p, unsigned i, int value) {

   return(fpitest_check_false((char *)p + i, value));

}


static int fptest_check_tolerance(const char *msg, long double actual, long double expected, long double tolerance) {

   long double expected_tolerance = expected * tolerance;
   long double expected_lower;
   long double expected_upper;

   if(expected_tolerance < 0) {
      expected_tolerance = -expected_tolerance;
   }
   expected_lower = expected - expected_tolerance;
   expected_upper = expected + expected_tolerance;

   if(actual < expected_lower || actual > expected_upper) {
      printf("check %s: expected %.20Lg, got %.20Lg (tolerance [%.20Lg,%.20Lg])\n", msg,
         expected, actual, expected_lower, expected_upper);
   }
   return(0);

}


int fptest_check_tol_float(const char *msg, float actual, float expected) {

   return(fptest_check_tolerance(msg, actual, expected, 1E-6));

}


int __ext_fc_fptest_check_tol_float(Context *c, Pointer *p, unsigned i, float actual, float expected) {

   return(fptest_check_tol_float((char *)p + i, actual, expected));

}


int fptest_check_tol_double(const char *msg, double actual, double expected) {

   return(fptest_check_tolerance(msg, actual, expected, 1E-12));

}


int __ext_fc_fptest_check_tol_double(Context *c, Pointer *p, unsigned i, double actual, double expected) {

   return(fptest_check_tol_double((char *)p + i, actual, expected));

}


int fptest_check_tol_long_double(const char *msg, long double actual, long double expected) {

   return(fptest_check_tolerance(msg, actual, expected, 1E-15));

}


int __ext_fc_fptest_check_tol_long_double(Context *c, Pointer *p, unsigned i, long double actual, long double expected) {

   return(fptest_check_tol_long_double((char *)p + i, actual, expected));

}


