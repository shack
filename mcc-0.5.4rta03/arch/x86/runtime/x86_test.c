/*
 * Test functions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#include <stdio.h>
#include <string.h>

#include "x86_runtime.h"

/*
 * A test function.
 */
int linktest_int(int i)
{
    printf("linktest_int(%d)\n", i);
    return i + 1;
}

int linktest_string(char *p)
{
    printf("linktest_string(%s)\n", p);
    return strlen(p);
}

int linktest_string_int_string(char *p1, int i, char *p2)
{
    printf("linktest_string_int_string(%s,%d,%s)\n", p1, i, p2);
    return strlen(p1) + i + strlen(p2);
}
