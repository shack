/*
 * Basic definitions for FC.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#ifndef _FC_H
#define _FC_H

#ifdef __mcc

typedef signed long fc_int64_t;
typedef unsigned long fc_uint64_t;

/* Basic functions */
static void print_char(char x) = "print_char";
static void print_int(int x) = "print_int";
static void print_hex(int x) = "print_hex";
static void print_long(long x) = "print_long";
static void print_long_hex(long x) = "print_long_hex";
static void print_float(float x) = "print_float";
static void print_double(double x) = "print_double";
static void print_long_double(long double x) = "print_long_double";
static void print_string(const char *s) = "print_string";
static void print_string_int(const char *s, int) = "print_string_int";
static void print_string_hex(const char *s, int) = "print_string_hex";
static void print_string_long_hex(const char *s, long) = "print_string_long_hex";
static void flush() = "flush";
static int atoi(const char *) = "atoi";
static double atof(const char *) = "atof";
static void exit(int) = "exit";
static void *malloc(int) = "malloc";
static unsigned long utime() = "utime";

/* Atomic primitives.  Note that atomic_commit and atomic_rollback both
   *optionally* take a level argument; if no level is specified, then
   level 0 (corresponds to the most recent level) is used.  */
static int atomic_entry(int c) = "atomic_entry";
static void atomic_commit() = "atomic_commit";
static void atomic_commit_level(int level) = "atomic_commit_level";
static void atomic_rollback(int c) = "atomic_rollback";
static void atomic_rollback_level(int level, int c) = "atomic_rollback_level";

/* Migration primitives */
static void migrate(const char *dest) = "migrate";

/* FPTEST/ITEST test core */
static void fpitest_msg(const char *msg) = "fpitest_msg";
static void fptest_data_float(const float **data) = "fpitest_data";
static void fptest_data_double(const double **data) = "fpitest_data";
static void fptest_data_long_double(const long double **data) = "fpitest_data";
static void itest_data_char(const char **data) = "fpitest_data";
static void itest_data_short(const short **data) = "fpitest_data";
static void itest_data_int(const int **data) = "fpitest_data";
static void itest_data_long(const long **data) = "fpitest_data";
static void fpitest_check_true(const char *msg, int value) = "fpitest_check_true";
static void fpitest_check_false(const char *msg, int value) = "fpitest_check_false";
static void fptest_check_tol_float(const char *msg,
   float actual, float expected) = "fptest_check_tol_float";
static void fptest_check_tol_double(const char *msg,
   double actual, double expected) = "fptest_check_tol_double";
static void fptest_check_tol_long_double(const char *msg,
   long double actual, long double expected) = "fptest_check_tol_long_double";

/* ITEST test core */
static void itest_check_msg(const char *msg) = "itest_check_msg";
static void itest_check_true(const char *msg, int value) = "itest_check_true";
static void itest_check_false(const char *msg, int value) = "itest_check_false";

/* Set a default definition for NULL */
#define  NULL  0

/* If using GCC's preprocessor, then __GNUC__ is set.
   Let's undefine it here, to avoid breakage in other
   header files... */
#undef   __GNUC__

#else /* not __mcc */

typedef signed long long fc_int64_t;
typedef unsigned long long fc_uint64_t;

#include <stdio.h>
#include <stdlib.h>

/* Basic functions */
void print_char(char x);
void print_int(int x);
void print_hex(int x);
void print_long(long x);
void print_long_hex(long x);
void print_float(float x);
void print_double(double x);
void print_long_double(long double x);
void print_string(const char *s);
void print_string_int(const char *s, int);
void print_string_hex(const char *s, int);
void print_string_long_hex(const char *s, long);

/* FPTEST/ITEST test core */
void fpitest_msg(const char *msg);
void fpitest_data(const void **data);
void fpitest_check_true(const char *msg, int value);
void fpitest_check_false(const char *msg, int value);
void fptest_check_tol_float(const char *msg, float actual, float expected);
void fptest_check_tol_double(const char *msg, double actual, double expected);
void fptest_check_tol_long_double(const char *msg, long double actual, long double expected);
#define  __fpitest_data(p)          (fpitest_data((const void **)(p)))
#define  fptest_data_float(p)       (__fpitest_data(p))
#define  fptest_data_double(p)      (__fpitest_data(p))
#define  fptest_data_long_double(p) (__fpitest_data(p))
#define  itest_data_char(p)         (__fpitest_data(p))
#define  itest_data_short(p)        (__fpitest_data(p))
#define  itest_data_int(p)          (__fpitest_data(p))
#define  itest_data_long(p)         (__fpitest_data(p))

#endif /* __mcc? */

#endif /* _FC_H */
