(*
 * Build utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mir

open Frame_type
open X86_inst_type
open X86_frame_type
open X86_frame

(*
 * Utilities.
 *)
val is_power2 : int32 -> bool
val log2 : int32 -> int32
val pow2 : int32 -> int32
val float_prec_size : Rawfloat.float_precision -> int
val float_prec : Rawfloat.float_precision -> fprec

(*
 * int32 conversion.
 *)
val code_int32_of_int : int -> int32
val code_operand_of_int : int -> operand

(*
 * Collect the destination labels.
 *)
val collect_jumps : inst list -> var list

(*
 * construct basic blocks.
 *)
val code_build_block : debug_info -> label -> inst Listbuf.t -> inst_block * label
val code_build_noopt_block : debug_info -> label -> inst Listbuf.t -> inst_block * label
val code_block_set_index : inst_block -> label -> label -> inst_block
val code_build_dst_block : debug_info -> string -> inst Listbuf.t -> label -> inst_block * label
(* TEMP
val code_block_jump_comment : string -> inst_block -> inst_block
 *)

(*
 * External calls.
 *)
val push_external_args  : inst Listbuf.t -> operand arg list -> inst Listbuf.t * int
val pop_external_args  : inst Listbuf.t -> operand arg list -> inst Listbuf.t * int

val jump_external_int   : Mir_pos.pos -> label -> operand arg list -> int * inst Listbuf.t
val jump_external_float : Mir_pos.pos -> label -> operand arg list -> int * inst Listbuf.t

val call_external_int32 : Mir_pos.pos -> label -> label -> operand arg list -> inst Listbuf.t
val call_external_float : Mir_pos.pos -> label -> label -> operand arg list -> inst Listbuf.t
val call_external_int64 : Mir_pos.pos -> label -> label -> label -> operand arg list -> inst Listbuf.t

val call_external_reserve_int32 : Mir_pos.pos -> label -> reserve list -> label -> label -> operand arg list -> inst Listbuf.t
val call_external_reserve_float : Mir_pos.pos -> label -> reserve list -> label -> label -> operand arg list -> inst Listbuf.t
val call_external_reserve_int64 : Mir_pos.pos -> label -> reserve list -> label -> label -> label -> operand arg list -> inst Listbuf.t

(*
 * Relation operations
 *)
val fucom_lt_set : int32
val fucom_eq_set : int32
val fucom_gt_unset : int32
val build_relop_int   : (src -> src -> inst) -> inst Listbuf.t -> cc -> src -> src -> label -> inst Listbuf.t
val build_relop_int32 : (src -> src -> inst) -> inst Listbuf.t -> cc -> src -> src -> label -> inst Listbuf.t
val build_relop_int64 : (src -> src -> inst) -> inst Listbuf.t -> cc -> src -> src -> src -> src -> label -> label -> inst Listbuf.t
val build_relop_float : inst Listbuf.t -> cc -> src -> src -> label -> inst Listbuf.t
val build_shrop_int : dst -> int -> inst

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
