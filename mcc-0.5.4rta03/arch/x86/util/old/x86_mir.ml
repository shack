(*
   Generate x86 code from MIR code.
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(*
   Yes, yes! Shake, earth! This is a sign. We are in the final days.
   My time is come! Glory, glory! ...  What do you think? 5.1?
      -- The Master, ``Prophecy Girl''
 *)


(* Useful modules *)
open Format
open Flags

open Debug
open Interval_set
open Symbol
open Mir_exn
open Mir_arity_map
open Sizeof_const

open Frame_type

open X86_frame
open X86_backend

open X86_runtime
open X86_build
open X86_mir_env
open X86_mir_atom
open X86_inst_type
open X86_frame_type
open X86_util


(***  Modules  ***)


module MirSimplify = Mir_simplify.MirSimplify (Backend)
module MirUtil = Mir_util.MirUtil (Backend)
module MirType = Mir_type.MirType (Backend)
module MirArity = Mir_arity.MirArity (Backend)


(***  Debugging  ***)


let vexp_pos pos = string_pos "X86_mir" (vexp_pos pos)
let exp_pos pos = string_pos "X86_mir" (exp_pos pos)


(***  Some numbers and other useless data  ***)


let two32 = Int32.of_int 2
let four32 = Int32.of_int 4


(***  Handy types  ***)


type prog = X86_frame.prog
type inst_block = X86_frame_type.inst_block
type spset = X86_frame.spset


(***  Aliases to Frame Functions  ***)


let reg64_lookup { cg_reg64 = reg64 } = reg64_lookup reg64
let reg64_lookup_exn { cg_reg64 = reg64 } = reg64_lookup_exn reg64
let atom_class_of_atom = MirUtil.atom_class_of_atom


(***  Utilities  ***)


(* migrate_label_of_key
   Constructs a migration label from the key.  This migration
   label is the label used to jump to the entry stub after a
   successful migration.  While it resembles a function entry
   point, it lacks certain setup code that normal entry points
   have; therefore it should not be exported. *)
let migrate_label_of_key key =
   (* Generate the key name *)
   let key = sprintf "migrate_label_%05d" key in
   let key = Symbol.add key in
      key


(* migrate_labels
   Adds the migration labels found in the expression to the
   symbol set passed in.  These labels eventually become part
   of the asm_preserve set, for blocks that must be preserved
   but are not exportable. *)
let rec migrate_labels venv = function
   Mir.SysMigrate (key, _, _, _, _) ->
      let key = migrate_label_of_key key in
      let venv = SymbolSet.add venv key in
         venv
 | Mir.Match (_, _, cases) ->
      let process_case venv (_, e) = migrate_labels venv e in
      let venv = List.fold_left process_case venv cases in
         venv
 | Mir.IfThenElse (_, _, _, e1, e2) ->
      let venv = migrate_labels venv e1 in
      let venv = migrate_labels venv e2 in
         venv
 | Mir.IfType (_, _, _, _, e1, e2) ->
      let venv = migrate_labels venv e1 in
      let venv = migrate_labels venv e2 in
         venv
 | Mir.TailCall _
 | Mir.SpecialCall _
 | Mir.SysMigrate _
 | Mir.Atomic _
 | Mir.AtomicRollback _ ->
      venv
 | Mir.LetAtom (_, _, _, e)
 | Mir.LetExternal (_, _, _, _, e)
 | Mir.AtomicCommit (_, e)
 | Mir.CopyOnWrite (_, _, e)
 | Mir.PrintDebug (_, _, _, e)
 | Mir.LetAtom (_, _, _, e)
 | Mir.LetExternal (_, _, _, _, e)
 | Mir.LetAlloc (_, _, e)
 | Mir.SetMem (_, _, _, _, _, e)
 | Mir.SetGlobal (_, _, _, e)
 | Mir.BoundsCheck (_, _, _, _, _, e)
 | Mir.LowerBoundsCheck (_, _, _, _, e)
 | Mir.UpperBoundsCheck (_, _, _, _, e)
 | Mir.PointerIndexCheck (_, _, _, _, _, e)
 | Mir.FunctionIndexCheck (_, _, _, _, _, e)
 | Mir.Memcpy (_, _, _, _, _, _, e)
 | Mir.Debug (_, e)
 | Mir.CommentFIR (_, e)
 | Mir.Reserve (_, e) ->
      migrate_labels venv e


(* collect_globals
   Put all the globals from the Mir into the structure
   for storing Frame (assembly) global values.  *)
let collect_globals esc_funs indexes globals =
   let pos = string_pos "collect_globals" (vexp_pos (Symbol.add "collect_globals")) in

   (* rewrite_atom
      Builds initialization value.  *)
   let rewrite_atom a =
      let pos = string_pos "rewrite_atom" pos in
         match a with
            Mir.AtomInt i ->
               InitValInt32 (code_int32_of_int i)
          | Mir.AtomFunVar (_, f)
          | Mir.AtomUnop (Mir.MemFunIndexOp _, Mir.AtomFunVar (_, f)) ->
               let f, index, arity_tag =
                  try SymbolTable.find esc_funs f with
                     Not_found -> raise (MirException (pos, UnboundVar f))
               in
                  InitValFunction (f, index, arity_tag)
          | Mir.AtomRawInt i ->
               begin
                  match Rawint.precision i with
                     Rawint.Int8
                   | Rawint.Int16
                   | Rawint.Int32 ->
                        InitValInt32 (Rawint.to_int32 i)
                   | Rawint.Int64 ->
                        raise (MirException (pos, NotImplemented "collect_globals: AtomRawInt, Int64 case"))
               end (* AtomRawInt *)
          | Mir.AtomFloat f ->
               begin
                  match Rawfloat.precision f with
                     Rawfloat.Single ->
                        InitValSingle (Rawfloat.to_float80 f)
                   | Rawfloat.Double ->
                        InitValDouble (Rawfloat.to_float80 f)
                   | Rawfloat.LongDouble ->
                        InitValLongDouble (Rawfloat.to_float80 f)
               end (* AtomFloat *)
          | Mir.AtomVar (_, v)
          | Mir.AtomUnop (Mir.MemIndexOp _, Mir.AtomVar (_, v)) ->
               (try
                   let f, index, arity_tag = SymbolTable.find esc_funs v in
                      InitValFunction (f, index, arity_tag)
                with
                   Not_found ->
                      let index =
                         try SymbolTable.find indexes v with
                            Not_found ->
                               raise (MirException (pos, UnboundVar v))
                      in
                         InitValGlobal (v, symbol_of_global_spill index))
          | Mir.AtomUnop _
          | Mir.AtomBinop _ ->
               raise (MirException (pos, StringAtomError ("collect_globals: AtomUnop/AtomBinop", a)))
   in

   (* make_tag_block
      Makes an init block for ML.  The major accomplishment here is that
      we rewrite the atoms so they may be emitted in a block init.  *)
   let make_tag_block tag atoms =
      let pos = string_pos "make_tag_block" pos in
      let atoms = List.map rewrite_atom atoms in
         InitTagBlock (tag, atoms)
   in

   (* make_init_blocks
      Constructs the necessary init blocks for this program.  *)
   let make_init_blocks = function
      Mir.InitRawData (pre, s) ->
         InitRawData (pre, s)
    | Mir.InitAlloc (Mir.AllocMalloc (Mir.AtomRawInt size)) ->
         InitAggrBlock size
    | Mir.InitAlloc (Mir.AllocUnion (tag, atoms)) ->
         make_tag_block tag atoms
    | Mir.InitAlloc (Mir.AllocTuple atoms)
    | Mir.InitAlloc (Mir.AllocArray atoms) ->
         make_tag_block 0 atoms
    | Mir.InitAlloc (Mir.AllocMArray (atoms, a)) ->
         raise (MirException (pos, NotImplemented "collect_globals: InitAlloc of AllocMArray"))
    | Mir.InitAlloc (Mir.AllocMalloc a) ->
         raise (MirException (pos, NotImplemented "collect_globals: InitAlloc of AllocMalloc"))
    | Mir.InitAtom (Mir.AtomRawInt i) ->
         InitRawInt i
    | Mir.InitAtom (Mir.AtomFloat x) ->
         InitRawFloat x
    | Mir.InitAtom _ ->
         raise (MirException (pos, NotImplemented "collect_globals: InitAtom"))
   in
      SymbolTable.map make_init_blocks globals


(***  64-bit Registers  ***)


(* collect_int64_regs
   Locate all the 64-bit registers in the program.  We identify
   them by checking the atomclass of a LetAtom or a LetExternal,
   wherever a new variable is bound.  This pass is necessary so
   we can associate two 32-bit registers with the ``meta'' 64bit
   register.  *)
let rec collect_int64_regs reg64 = function
   Mir.LetAtom (v, ACRawInt (Rawint.Int64, _), _, e)
 | Mir.LetAtom (v, ACPointerInfix _, _, e)
 | Mir.LetExternal (v, ACRawInt (Rawint.Int64, _), _, _, e) ->
      collect_int64_regs (reg64_add reg64 v) e
 | Mir.Match (_, _, cases) ->
      List.fold_left (fun reg64 (_, e) -> collect_int64_regs reg64 e) reg64 cases
 | Mir.IfThenElse (_, _, _, e1, e2) ->
      collect_int64_regs (collect_int64_regs reg64 e1) e2
 | Mir.IfType (_, _, _, _, e1, e2) ->
      collect_int64_regs (collect_int64_regs reg64 e1) e2
 | Mir.TailCall _
 | Mir.SpecialCall _
 | Mir.SysMigrate _
 | Mir.Atomic _
 | Mir.AtomicRollback _ ->
      reg64
 | Mir.AtomicCommit (_, e)
 | Mir.CopyOnWrite (_, _, e)
 | Mir.PrintDebug (_, _, _, e)
 | Mir.LetAtom (_, _, _, e)
 | Mir.LetExternal (_, _, _, _, e)
 | Mir.LetAlloc (_, _, e)
 | Mir.SetMem (_, _, _, _, _, e)
 | Mir.SetGlobal (_, _, _, e)
 | Mir.BoundsCheck (_, _, _, _, _, e)
 | Mir.LowerBoundsCheck (_, _, _, _, e)
 | Mir.UpperBoundsCheck (_, _, _, _, e)
 | Mir.PointerIndexCheck (_, _, _, _, _, e)
 | Mir.FunctionIndexCheck (_, _, _, _, _, e)
 | Mir.Memcpy (_, _, _, _, _, _, e)
 | Mir.Debug (_, e)
 | Mir.CommentFIR (_, e)
 | Mir.Reserve (_, e) ->
      collect_int64_regs reg64 e

let collect_int64_regs funs =
   let collect_int64_fun_regs reg64 _ (_, args, e) =
      let args = List.filter (fun (v, ac) ->
         match ac with
            ACInt
          | ACPoly
          | ACFloat _
          | ACPointer _
          | ACFunction _
          | ACRawInt (Rawint.Int8, _)
          | ACRawInt (Rawint.Int16, _)
          | ACRawInt (Rawint.Int32, _) ->
               false
          | ACPointerInfix _
          | ACRawInt (Rawint.Int64, _) ->
               true) args
      in
      let args = List.map fst args in
      let reg64 = List.fold_left reg64_add reg64 args in
         collect_int64_regs reg64 e
   in
   let reg64 = SymbolTable.fold collect_int64_fun_regs SymbolTable.empty funs in
      reg64


(* rewrite_args
   Rewrite arguments returned from a get_stdargs call such that
   the list is a list of real variables, with 64-bit values mapped
   into the reg64 using a fake, temporary variable name.  *)
let rewrite_args cg args =
   let reg64_add_hi_lo cg v vhi vlo =
      let reg64 = cg.cg_reg64 in
      let reg64 = reg64_add_hi_lo reg64 v vhi vlo in
         { cg with cg_reg64 = reg64 }
   in
   let rewrite_arg (cg, vars) = function
      ArgInt32 v
    | ArgFloat (v, _) ->
         cg, v :: vars
    | ArgInt64 (vhi, vlo) ->
         let v = new_symbol_string "reg64_tmp" in
         let cg = reg64_add_hi_lo cg v vhi vlo in
            cg, v :: vars
   in
   let cg, vars = List.fold_left rewrite_arg (cg, []) args in
   let vars = List.rev vars in
      cg, vars


(***  Floating-point Constants  ***)


(* collect_float_atom_val
   Find any floating point values in an atom.  *)
let rec collect_float_atom_val fvals = function
   Mir.AtomFloat x ->
      fvals_add fvals (Rawfloat.to_float80 x)
 | Mir.AtomUnop (_, a) ->
      collect_float_atom_val fvals a
 | Mir.AtomBinop (_, a1, a2) ->
      collect_float_atom_val (collect_float_atom_val fvals a1) a2
 | Mir.AtomInt _
 | Mir.AtomRawInt _
 | Mir.AtomVar _
 | Mir.AtomFunVar _ ->
      fvals


(* collect_float_atom_vals
   Find floating point values in a list of atoms.  *)
let collect_float_atom_vals = List.fold_left collect_float_atom_val


(* collect_float_set_vals
   Find floating point values in an interval set.  *)
let collect_float_set_vals fvals s =
   let collect_float_bound_val fvals = function
      Open f | Closed f -> fvals_add fvals f
    | Infinity          -> fvals
   in
   let collect_float_bound_vals fvals lower upper =
      collect_float_bound_val (collect_float_bound_val fvals lower) upper
   in match s with
         Mir.IntSet _
       | Mir.RawIntSet _ -> fvals


(* collect_float_vals
   Find all floating point values in the program.  Floating-point
   values have to be emitted specially in the header of the program,
   using the .double (or equivalent) directive.  As a result, we
   need to collect all the floating point numbers and associate each
   of them with a symbol name which can be used to refer to number. *)
let rec collect_float_vals fvals = function
   Mir.LetAtom (v, _, a, e) ->
      collect_float_vals (collect_float_atom_val fvals a) e
 | Mir.LetExternal (_, _, _, atoms, e) ->
      collect_float_vals (collect_float_atom_vals fvals atoms) e
 | Mir.Match (a, _, cases) ->
      let fvals = collect_float_atom_val fvals a in
         List.fold_left (fun fvals (s, e) ->
            collect_float_vals (collect_float_set_vals fvals s) e) fvals cases
 | Mir.IfThenElse (_, a1, a2, e1, e2) ->
      let fvals = collect_float_atom_val fvals a1 in
      let fvals = collect_float_atom_val fvals a2 in
      let fvals = collect_float_vals fvals e1 in
      let fvals = collect_float_vals fvals e2 in
         fvals
 | Mir.IfType (_, _, _, _, e1, e2) ->
      let fvals = collect_float_vals fvals e1 in
      let fvals = collect_float_vals fvals e2 in
         fvals
 | Mir.TailCall (_, _, atoms) ->
      collect_float_atom_vals fvals atoms
 | Mir.SpecialCall _ ->
      (* Shouldn't have made it this far; oh hell, it'll be caught... *)
      fvals
 | Mir.Atomic (_, i, env) ->
      collect_float_atom_vals fvals [i; env]
 | Mir.SysMigrate (_, dptr, doff, _, env) ->
      collect_float_atom_vals fvals [dptr; doff; env]
 | Mir.AtomicRollback (level, i) ->
      collect_float_atom_vals fvals [level; i]
 | Mir.LetAlloc (_, op, e) ->
      let fvals =
         match op with
            Mir.AllocUnion (_, atoms)
          | Mir.AllocTuple atoms
          | Mir.AllocArray atoms ->
               collect_float_atom_vals fvals atoms
          | Mir.AllocMArray (_, a)
          | Mir.AllocMalloc a ->
               collect_float_atom_val fvals a
      in
         collect_float_vals fvals e
 | Mir.SetMem (_, _, a1, a2, a3, e) ->
      collect_float_vals (collect_float_atom_vals fvals [a1; a2; a3]) e
 | Mir.SetGlobal (_, _, a, e) ->
      collect_float_vals (collect_float_atom_val fvals a) e
 | Mir.BoundsCheck (_, _, a1, a2, a3, e) ->
      collect_float_vals (collect_float_atom_vals fvals [a1; a2; a3]) e
 | Mir.LowerBoundsCheck (_, _, a1, a2, e)
 | Mir.UpperBoundsCheck (_, _, a1, a2, e) ->
      collect_float_vals (collect_float_atom_vals fvals [a1; a2]) e
 | Mir.PointerIndexCheck (_, _, _, _, a, e)
 | Mir.FunctionIndexCheck (_, _, _, _, a, e) ->
      collect_float_vals (collect_float_atom_val fvals a) e
 | Mir.AtomicCommit (_, e )
 | Mir.CopyOnWrite (_, _, e)
 | Mir.PrintDebug (_, _, _, e)
 | Mir.Debug (_, e)
 | Mir.CommentFIR (_, e)
 | Mir.Memcpy (_, _, _, _, _, _, e) ->
      collect_float_vals fvals e
 | Mir.Reserve ((_, _, a1, a2), e) ->
      collect_float_vals (collect_float_atom_vals fvals [a1; a2]) e

let collect_float_vals funs =
   SymbolTable.fold (fun fvals _ (_, _, e) -> collect_float_vals fvals e) FloatTable.empty funs


(***  Atom Copying and Datatype Manipulation  ***)


(* code_copy_atom
   Copies data in one register to another.  The continuation
   is passed the list of coded operands for the result.  *)
let code_copy_atom_nonself cg pos debug v ac a cont =
   let pos = string_pos "code_copy_atom_nonself" pos in
   let ac' = atom_class_of_atom a in

   (* All int32 types have the same structure, just different
      code_atom function. *)
   let copy_int32 code_atom =
      let v = Register v in
      let r = new_symbol_string "copy_atom" in
      code_atom cg pos debug a (fun op ->
         let insts = Listbuf.of_list
            [code_load_int32 r op;
             code_store_int32 v r]
         in
         let blocks, label = cont (ArgInt32 v) in
         let block, label = code_build_dst_block debug "copy_atom" insts label in
            block :: blocks, label)
   in (* end copy_int32 *)

   (* The int64 and infix pointer types share the same copy
      code, but use a different code_atom function. *)
   let copy_int64 code_atom =
      let vhi, vlo = reg64_lookup cg pos v in
      let vhi = Register vhi in
      let vlo = Register vlo in
      let rhi = new_symbol_string "copy_atom_hi" in
      let rlo = new_symbol_string "copy_atom_lo" in
      code_atom cg pos debug a (fun ohi olo ->
         let insts = Listbuf.of_list
            [code_load_int32 rhi ohi;
             code_store_int32 vhi rhi;
             code_load_int32 rlo olo;
             code_store_int32 vlo rlo]
         in
         let blocks, label = cont (ArgInt64 (vhi, vlo)) in
         let block, label = code_build_dst_block debug "copy_atom_int64" insts label in
            block :: blocks, label)
   in (* end copy_int64 *)

   (* Determine what instructions to build based on atom class.
      The pair is destination type, then source type. *)
   match ac, ac' with
      (* ML Ints require special handling *)
      ACInt, ACInt ->
         copy_int32 code_atom_int

      (* Each of the following are stored as 32-bit registers *)
    | ACRawInt (Rawint.Int8, _), ACRawInt (Rawint.Int8, _)
    | ACRawInt (Rawint.Int16, _), ACRawInt (Rawint.Int16, _)
    | ACRawInt (Rawint.Int32, _), ACRawInt (Rawint.Int32, _) ->
         copy_int32 code_atom_int32

      (* Polymorphic values *)
    | ACPoly, ACPoly ->
         copy_int32 code_atom_poly

      (* Pointer-to-pointer allowed (no coercions however!) *)
    | ACFunction funty, ACFunction funty'
      when MirType.atom_classes_agree ac ac' ->
         copy_int32 code_atom_ptr_base
    | ACPointer ptrty, ACPointer ptrty'
      when ptrty = ptrty' ->
         copy_int32 code_atom_ptr_base

      (* Infix pointers are like Int64 *)
    | ACPointerInfix _, ACPointerInfix _ ->
         copy_int64 code_atom_ptr_infix

      (* Int64 has its own atom handler *)
    | ACRawInt (Rawint.Int64, _), ACRawInt (Rawint.Int64, _) ->
         copy_int64 code_atom_int64

      (* Floats are just a pain to deal with, but internally
         they are always stored as temporary real values. *)
    | ACFloat pre, ACFloat pre' when pre = pre' ->
         let v = FloatRegister v in
         code_atom_float cg pos debug a (fun op ->
            let insts = Listbuf.of_elt (FMOVP (FT, v, FT, op)) in
            let blocks, label = cont (ArgFloat (v, pre)) in
            let block, label = code_build_dst_block debug "copy_atom_float" insts label in
               block :: blocks, label)

      (* Bogus cases *)
    | _ ->
         raise (MirException (pos, ImplicitCoercion2 (ac, ac')))


(* When an atom is copied, catch the special case where it
   it copied to itself. *)
let code_copy_atom cg pos debug v ac a cont =
   let pos = string_pos "code_copy_atom" pos in
      match a with
         Mir.AtomVar (ac', v') when Symbol.eq v' v ->
            if ac' <> ac then
               raise (MirException (pos, ImplicitCoercion2 (ac, ac')));
            let arg =
               match ac with
                  ACInt
                | ACRawInt (Rawint.Int8, _)
                | ACRawInt (Rawint.Int16, _)
                | ACRawInt (Rawint.Int32, _)
                | ACPoly
                | ACFunction _
                | ACPointer _ ->
                     ArgInt32 (Register v)
                | ACRawInt (Rawint.Int64, _)
                | ACPointerInfix _ ->
                     let vhi, vlo = reg64_lookup cg pos v in
                        ArgInt64 (Register vhi, Register vlo)
                | ACFloat pre ->
                     ArgFloat (Register v, pre)
            in
               cont arg
       | _ ->
            code_copy_atom_nonself cg pos debug v ac a cont


(* code_copy_atom_list
   Generate blocks to copy a list of atoms to respective vars.  The
   continuation will be passed a list of the coded result variables.  *)
let code_copy_atom_list cg pos debug vars acs atoms cont =
   let pos = string_pos "code_copy_atom_list" pos in
   let rec code_copy_atom_list res vars acs atoms =
      match vars, acs, atoms with
         v :: vars, ac :: acs, a :: atoms ->
            code_copy_atom cg pos debug v ac a (fun v ->
               code_copy_atom_list (v :: res) vars acs atoms)
       | [], [], [] ->
            cont (List.rev res)
       | _ ->
            raise (MirException (pos, InternalError "code_copy_atom_list"))
   in
      code_copy_atom_list [] vars acs atoms


(* code_read_atoms
   Copies atoms from source into temporary registers.  The registers and
   their corresponding atom classes are passed to the given continuation.
   Useful when doing a call_external...  *)
let code_read_atoms cg pos debug args cont =
   let pos = string_pos "code_read_atoms" pos in
   let rec code_read_atoms regs = function
      arg :: args ->
         begin
         match atom_class_of_atom arg with
            ACInt ->
               code_atom_int cg pos debug arg (fun reg ->
                  code_read_atoms (ArgInt32 reg :: regs) args)
          | ACRawInt (Rawint.Int8, _)
          | ACRawInt (Rawint.Int16, _)
          | ACRawInt (Rawint.Int32, _) ->
               code_atom_int32 cg pos debug arg (fun reg ->
                  code_read_atoms (ArgInt32 reg :: regs) args)
          | ACPoly ->
               code_atom_poly cg pos debug arg (fun reg ->
                  code_read_atoms (ArgInt32 reg :: regs) args)
          | ACFunction _
          | ACPointer _ ->
               code_atom_ptr_base cg pos debug arg (fun reg ->
                  code_read_atoms (ArgInt32 reg :: regs) args)
          | ACFloat pre ->
               code_atom_float cg pos debug arg (fun reg ->
                  code_read_atoms (ArgFloat (reg, pre) :: regs) args)
          | ACRawInt (Rawint.Int64, _)
          | ACPointerInfix _ ->
               code_atom_int64 cg pos debug arg (fun rhi rlo ->
                     code_read_atoms (ArgInt64 (rhi, rlo) :: regs) args)
         end (* something to process *)
    | [] ->
         cont (List.rev regs)
   in
      code_read_atoms [] args


(***  Codegen  ***)


(* code_expr
   Build assembly blocks for the FIR expression e. *)
let rec code_expr cg debug e =
   let pos = string_pos "code_expr" (exp_pos e) in
   match e with
      Mir.LetAtom (v, ac, a, e) ->
         code_atom_expr cg pos debug v ac a e
    | Mir.TailCall (directp, f, atoms) ->
         code_tailcall_expr cg pos debug f directp atoms
    | Mir.Match (a, ac, cases) ->
         code_match_expr cg pos debug a ac cases
    | Mir.IfThenElse (op, a1, a2, e1, e2) ->
         code_if_then_else_expr cg pos debug op a1 a2 e1 e2
    | Mir.IfType (obj, names, name, v, e1, e2) ->
         code_if_type_expr cg pos debug obj names name v e1 e2
    | Mir.PrintDebug (label, f, args, e) ->
         code_print_debug_expr cg pos debug label f args e
    | Mir.LetExternal (v, ac, f, args, e) ->
         code_external_expr cg pos debug v ac f args e
    | Mir.Reserve (rinfo, e) ->
         code_reserve_expr cg pos debug rinfo e
    | Mir.LetAlloc (v, op, e) ->
         code_alloc_expr cg pos debug v op e
    | Mir.SetMem (ac, ptrty, a1, a2, a3, e) ->
         code_set_mem_expr cg pos debug ac ptrty a1 a2 a3 e
    | Mir.SetGlobal (ac, v, a, e) ->
         code_set_global_expr cg pos debug ac v a e
    | Mir.BoundsCheck (label, ptrty, ptr, start, stop, e) ->
         code_bounds_check_expr cg pos debug label ptrty ptr start stop e
    | Mir.LowerBoundsCheck (label, ptrty, ptr, start, e) ->
         code_lower_bounds_check_expr cg pos debug label ptrty ptr start e
    | Mir.UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
         code_upper_bounds_check_expr cg pos debug label ptrty ptr stop e
    | Mir.PointerIndexCheck (label, ptrty, v, ac, index, e) ->
         code_pointer_index_check_expr cg pos debug label ptrty v ac index e
    | Mir.FunctionIndexCheck (label, funty, v, ac, index, e) ->
         code_function_index_check_expr cg pos debug label funty v ac index e
    | Mir.Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
         code_memcpy_expr cg pos debug ptrty a1 a2 a3 a4 a5 e
    | Mir.Debug (info, e) ->
         code_debug_expr cg pos debug info e
    | Mir.CommentFIR (e', e) ->
         code_comment_fir_expr cg pos debug e' e
    | Mir.SpecialCall _ ->
         raise (MirException (pos, InternalError "SpecialCall not allowed here"))
    | Mir.SysMigrate (label, dptr, doff, f, env) ->
         code_migrate_expr cg pos debug label dptr doff f env
    | Mir.Atomic (f, i, env) ->
         code_atomic_expr cg pos debug f i env
    | Mir.AtomicRollback (level, i) ->
         code_atomic_rollback_expr cg pos debug level i
    | Mir.AtomicCommit (level, e) ->
         code_atomic_commit_expr cg pos debug level e
    | Mir.CopyOnWrite (rinfo, copy_ptr, e) ->
         code_copy_on_write_expr cg pos debug rinfo copy_ptr e


(* code_debug_expr
   Code the debugging info.  Have to look up the types for the vars.  *)
and code_debug_expr cg pos _ debug e =
   let pos = string_pos "code_debug_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.Debug (debug, e))) in
   let line, info = debug in
   let info = List.map (fun (name, ty, v) -> name, ty, Register v) info in
   let debug = line, info in
   let blocks, label = code_expr cg debug e in
   let block, label = code_build_dst_block debug "debug" insts label in
      block :: blocks, label


(* code_comment_fir_expr
   Construct an FIR comment in the final program.  *)
and code_comment_fir_expr cg pos debug e' e =
   let pos = string_pos "code_comment_fir_expr" pos in
   let insts = Listbuf.of_elt (CommentFIR e') in
   let blocks, label = code_expr cg debug e in
   let block, label = code_build_dst_block debug "comment" insts label in
      block :: blocks, label


(* code_atom_expr
   Code an expression containing an atom.  *)
and code_atom_expr cg pos debug v ac a e =
   let pos = string_pos "code_atom_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.LetAtom (v, ac, a, e))) in
   let blocks, label = code_copy_atom cg pos debug v ac a (fun _ -> code_expr cg debug e) in
   let block, label = code_build_dst_block debug "letatom" insts label in
      block :: blocks, label


(* code_set_global_expr
   Code an expression assigning a value to a global.  *)
and code_set_global_expr cg pos debug ac v a e =
   let pos = string_pos "code_set_global_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.SetGlobal (ac, v, a, e))) in
   let blocks, label = code_copy_atom cg pos debug v ac a (fun _ -> code_expr cg debug e) in
   let block, label = code_build_dst_block debug "setglobal" insts label in
      block :: blocks, label


(* code_tailcall_expr
   Code a tailcall; the tailcall can either be to an immediate function
   label, or it is a variable containing a function pointer (escaped!).
   No other cases are allowed.  *)
and code_tailcall_expr cg pos debug f op atoms =
   let pos = string_pos "code_tailcall_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.TailCall (op, f, atoms))) in
   let { cg_fun_args = fun_args } = cg in

   (* tailcall
      Constructs the actual argument-copy code and the tailcall. *)
   let tailcall cg pos debug vars cont =
      let pos = string_pos "tailcall" pos in
      (* Copy atoms to temporaries, and then the temporaries are copied to
         arguments.  Do the 2-step copy to prevent any value interference,
         and hope the register allocator will remove the redundant copies. *)
      let reg64_add cg v =
         let reg64 = cg.cg_reg64 in
         let reg64 = reg64_add reg64 v in
            { cg with cg_reg64 = reg64 }
      in

      (* Construct the list of temporaries to copy args to *)
      let vars, acs = List.split vars in
      let temps = List.map (fun v -> new_symbol_string (Symbol.to_string v)) vars in
      let cg = List.fold_left2 (fun cg v ac ->
         match ac with
            ACInt
          | ACPoly
          | ACFloat _
          | ACPointer _
          | ACFunction _
          | ACRawInt (Rawint.Int8,  _)
          | ACRawInt (Rawint.Int16, _)
          | ACRawInt (Rawint.Int32, _) -> cg
          | ACRawInt (Rawint.Int64, _)
          | ACPointerInfix _ -> reg64_add cg v) cg temps acs
      in
      let tatoms = List.map2 (fun ac t -> Mir.AtomVar(ac, t)) acs temps in

      (* WARNING: A double-copy is REQUIRED here; consider a function which
         recurses on itself and swaps the arguments (e.g. gcd() when arguments
         are reversed). *)
      code_copy_atom_list cg (int_pos 1 pos) debug temps acs atoms (fun _ ->
         code_copy_atom_list cg (int_pos 2 pos) debug vars acs tatoms cont)
   in

   (* What type of tailcall?  Immediate or indirect? *)
   let blocks, label =
      match op with
         Mir.DirectCall ->
            (* Symbol was a function name -- this is the easy case. *)
            let pos = string_pos "Mir.DirectCall" pos in
            let vars = try SymbolTable.find fun_args f with
               Not_found -> raise (MirException (pos, UnboundVar f))
            in
               tailcall cg pos debug vars (fun _ ->
                  let block, label = code_build_dst_block debug "tailcall" insts f in
                     [block], label)

       | Mir.IndirectCall ->
            (* We are calling a escaped fun. This case is not so trivial. *)
            (* Must pass all our arguments into the standard arguments. *)
            let pos = string_pos "IndirectCall" pos in
            let acs = List.map atom_class_of_atom atoms in
            let vars = get_stdargs acs in
            let cg, vars = rewrite_args cg vars in
            let vars = List.combine vars acs in
               tailcall cg pos debug vars (fun srcs ->
                  let srcs = List.fold_left (fun srcs s ->
                     match s with
                        ArgInt32 v
                      | ArgFloat (v, _) ->
                           v :: srcs
                      | ArgInt64 (v1, v2) ->
                           v1 :: v2 :: srcs) [] srcs
                  in
                  let r = new_symbol_string "indirect" in
                  let f = Register f in
                  let label = new_symbol_string "tailcall" in
                  let insts = Listbuf.add_list insts
                     [code_load_int32 r f;
                      IJMP (srcs, Register r)]
                  in
                  let block, label = code_build_block debug label insts in
                     [block], label)

       | Mir.ExternalCall ->
            (* External function call *)
            let pos = string_pos "ExternalCall" pos in
            code_read_atoms cg pos debug atoms (fun regs ->
               let _, jump = jump_external_int pos f regs in
               let insts = Listbuf.add_listbuf insts jump in
               let label = new_symbol_string "extcall" in
               let block, label = code_build_block debug label insts in
                  [block], label)
   in
      blocks, label


(* code_match_expr
   Construct a match expression in assembly code.  This function produces
   a large number of blocks, roughly one block per interval and additional
   blocks for every success case.  If the match fails, then we must abort
   the program since we'll have nowhere to go.  *)
and code_match_expr cg pos debug a ac cases =
   let pos = string_pos "code_match_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.Match (a, ac, cases))) in
   let v = new_symbol_string "match_atom" in

   (* construct_set
      This function constructs an entire <type>-set case when
      we can afford to use the normal <type> instructions. To
      specify how to construct the type, set cons_relop to the
      relop builder of your choice in x86_build, and give me a
      value-builder in cons_value.  *)
   let construct_set cons_relop cons_value signed v ublocks ulabel s e =
      (* Assemble the ``success'' blocks *)
      let blocks, lsucc = code_expr cg debug e in

      (* construct_interval
         Constructs a block for each interval check we must do *)
      let rec construct_interval = function
         (i1, i2) :: s ->
            (* The ``fail'' for this interval is to check next interval *)
            let blocks', lfail = construct_interval s in
            let blocks = blocks @ blocks' in

            (* compare_bound_op
               Checks if we lie outside the bound i, using op to perform
               the check.  Jumps to lfail if it was outside, otherwise
               runs the instructions listed in tail. *)
            let compare_bound_op insts op i =
               cons_relop insts op v (cons_value i) lfail
            in

            (* compare_bound
               Checks if we lie outside the bound given.  The two sets
               of operands are operands to be used if the bound is open
               and closed, respectively.  The tail is the set of instrs
               to append to what is generated here -- they are run if
               the bound check is successful only. *)
            let compare_bound insts opopen opclosed bound =
               match bound with
                  Open i   -> compare_bound_op insts opopen   i
                | Closed i -> compare_bound_op insts opclosed i
                | Infinity -> insts
            in

            (* compare_bounds
               Compare both sets of bounds at once. *)
            let compare_bounds insts i1 i2 =
               if signed then
                  let insts = compare_bound insts  LE  LT i1 in
                  let insts = compare_bound insts  GE  GT i2 in
                     insts
               else
                  let insts = compare_bound insts ULE ULT i1 in
                  let insts = compare_bound insts UGE UGT i2 in
                     insts
            in
            let compare_bounds insts i1 i2 =
               match i1, i2 with
                  Closed i, Closed j when i = j ->
                     (* This catches the special case of an isolated point. *)
                     compare_bound_op insts NEQ i
                | _ ->
                     (* Definitely not isolated point. *)
                     compare_bounds insts i1 i2
            in

            (* Compute the instructions for this bounds check. *)
            let insts = compare_bounds Listbuf.empty i1 i2 in
            let block, label = code_build_dst_block debug "interval" insts lsucc in
               block :: blocks, label

       | [] ->
            (* Base case if all intervals failed; call ulabel. *)
            ublocks, ulabel
      in
         (* Construct the full list for this case *)
         construct_interval s
   in (* End of construct_set *)

   (* construct_int_set
      Wrapper function for constructing integer intervals *)
   let construct_int_set blocks label s e =
      let pos = string_pos "construct_int_set" pos in
      let v = Register v in
      let cons_relop = build_relop_int32 (fun op1 op2 -> CMP (IL, op1, op2)) in
      let cons_value = code_operand_of_int in
      let s = Fir_set.IntSet.fold (fun ls i1 i2 -> (i1, i2) :: ls) [] s in
         construct_set cons_relop cons_value true v blocks label s e
   in (* End of construct_int_set *)

   (* construct_raw_int_set
      Wrapper function for constructing integer intervals *)
   let construct_raw_int_set blocks label s e =
      let pos = string_pos "construct_raw_int_set" pos in
      let signed = (Fir_set.RawIntSet.signed s) in
      match Fir_set.RawIntSet.precision s with
         Rawint.Int8
       | Rawint.Int16
       | Rawint.Int32 ->
            let v = Register v in
            let cons_relop = build_relop_int32 (fun op1 op2 -> CMP (IL, op1, op2)) in
            let cons_value i = ImmediateNumber (OffNumber (Rawint.to_int32 i)) in
            let s = Fir_set.RawIntSet.fold (fun ls i1 i2 -> (i1, i2) :: ls) [] s in
               construct_set cons_relop cons_value signed v blocks label s e
       | Rawint.Int64 ->
            raise (MirException (pos, NotImplemented "construct_int_set"))
   in (* End of construct_raw_int_set *)

   (* construct_cases
      This constructs code and blocks for all cases that appear in the
      match statement; this invokes the above functions to handle the
      individual cases as needed.  *)
   let rec construct_cases = function
      [] ->
         raise (MirException (pos, InternalError "Match statements must have at least one case"))
    | (_, e) :: [] ->
         (* The final case can be dealt with w/o conditionals *)
         code_expr cg debug e
    | (Mir.IntSet s, e) :: cases ->
         let blocks, label = construct_cases cases in
            construct_int_set blocks label s e
    | (Mir.RawIntSet s, e) :: cases ->
         let blocks, label = construct_cases cases in
            construct_raw_int_set blocks label s e
   in (* End of construct_cases *)

   (* Construct all the interval checks *)
   let blocks, label = construct_cases cases in

   (* Construct block to copy atom to a register *)
   let code_int32_cont op =
      let insts = Listbuf.add insts (code_load_int32 v op) in
      let block, label = code_build_dst_block debug "match" insts label in
         block :: blocks, label
   in
   let code_float_cont op =
      let insts = Listbuf.add insts (FMOVP (FT, FloatRegister v, FT, op)) in
      let block, label = code_build_dst_block debug "match" insts label in
         block :: blocks, label
   in
   match ac, atom_class_of_atom a with
      ACInt, _ ->
         code_atom_int cg pos debug a code_int32_cont
      (* 8- and 16-bit values must be masked before comparison *)
    | ACRawInt (Rawint.Int8,  signed), _ ->
         code_atom_int8  cg pos debug signed a code_int32_cont
    | ACRawInt (Rawint.Int16, signed), _ ->
         code_atom_int16 cg pos debug signed a code_int32_cont
      (* For 32-bit values, the union special case permits a block
         pointer to be used as the atom, but treats the pointer as
         an atom.  I really dislike constant constructors... *)
    | ACRawInt (Rawint.Int32, _), ACPointer PtrBlock ->
         code_atom_ptr_base cg pos debug a code_int32_cont
    | ACRawInt (Rawint.Int32, _), _ ->
         code_atom_int32 cg pos debug a code_int32_cont
    | ACFloat _, _ ->
         code_atom_float cg pos debug a code_float_cont
    | ACRawInt (Rawint.Int64, _), _ ->
         raise (MirException (pos, NotImplemented "code_match_expr (inst1)"))
    | ACPoly, _ ->
         raise (MirException (pos, StringError "code_match_expr: ACPoly not allowed"))
    | ACPointer _, _
    | ACPointerInfix _, _
    | ACFunction _, _ ->
         raise (MirException (pos, StringError "code_match_expr: Not sure about the pointers yet"))


(* code_if_then_else_expr
   Constructs a branching expression with two cases, dependent
   on a relative operator and two atoms given.  *)
and code_if_then_else_expr cg pos debug op a1 a2 e1 e2 =
   let pos = string_pos "code_if_then_else_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.IfThenElse (op, a1, a2, e1, e2))) in
   let v = new_symbol_string "match_atom" in

   (* Compute the success and failure code *)
   let bsucc, lsucc = code_expr cg debug e1 in
   let bfail, lfail = code_expr cg debug e2 in

   (* Find out what atomclass we're working with *)
   let ac =
      match op with
         Mir.REqOp(ac)
       | Mir.RNeqOp(ac)
       | Mir.RLtOp(ac)
       | Mir.RLeOp(ac)
       | Mir.RGtOp(ac)
       | Mir.RGeOp(ac)
       | Mir.RAndOp (ac) ->
            ac
   in

   (* Map the operator *)
   let build_and op1 op2 = AND (IL, op1, op2) in
   let build_cmp op1 op2 = CMP (IL, op1, op2) in
   let map_operator signed =
      match op, signed with
         Mir.REqOp _, _     -> EQ,  build_cmp
       | Mir.RAndOp _, _    -> NEQ, build_and
       | Mir.RNeqOp _, _    -> NEQ, build_cmp
       | Mir.RLtOp _, true  -> LT,  build_cmp
       | Mir.RLeOp _, true  -> LE,  build_cmp
       | Mir.RGtOp _, true  -> GT,  build_cmp
       | Mir.RGeOp _, true  -> GE,  build_cmp
       | Mir.RLtOp _, false -> ULT, build_cmp
       | Mir.RLeOp _, false -> ULE, build_cmp
       | Mir.RGtOp _, false -> UGT, build_cmp
       | Mir.RGeOp _, false -> UGE, build_cmp
   in

   (* Compare two values *)
   let compare cons signed op1 op2 =
      let op, cmp = map_operator signed in
      let insts' = cons cmp Listbuf.empty op op1 op2 lsucc in
      let insts = Listbuf.add_listbuf insts insts' in
      let block, label = code_build_dst_block debug "cond" insts lfail in
         block :: bsucc @ bfail, label
   in
   let build_relop_float _ = build_relop_float in
   let compare_int   = compare build_relop_int in
   let compare_int32 = compare build_relop_int32 in
   let compare_float = compare build_relop_float true in
   let compare_int64 signed ohi1 olo1 ohi2 olo2 =
      let op, cmp = map_operator signed in
      let insts' = build_relop_int64 cmp Listbuf.empty op ohi1 olo1 ohi2 olo2 lsucc lfail in
      let insts = Listbuf.add_listbuf insts insts' in
      let block, label = code_build_dst_block debug "cond" insts lfail in
         block :: bsucc @ bfail, label
   in

   (* What atomclass are we looking at? *)
   match ac with
      ACInt ->
         code_atom_int cg pos debug a1 (fun op1 ->
            code_atom_int cg pos debug a2 (fun op2 ->
               compare_int true op1 op2))
    | ACRawInt (Rawint.Int8, s) ->
         code_atom_int8 cg pos debug s a1 (fun op1 ->
            code_atom_int8 cg pos debug s a2 (fun op2 ->
               compare_int32 s op1 op2))
    | ACRawInt (Rawint.Int16, s) ->
         code_atom_int16 cg pos debug s a1 (fun op1 ->
            code_atom_int16 cg pos debug s a2 (fun op2 ->
               compare_int32 s op1 op2))
    | ACRawInt (Rawint.Int32, s) ->
         code_atom_int32 cg pos debug a1 (fun op1 ->
            code_atom_int32 cg pos debug a2 (fun op2 ->
               compare_int32 s op1 op2))
    | ACRawInt (Rawint.Int64, s) ->
         code_atom_int64 cg pos debug a1 (fun ohi1 olo1 ->
            code_atom_int64 cg pos debug a2 (fun ohi2 olo2 ->
               compare_int64 s ohi1 olo1 ohi2 olo2))
    | ACFloat _ ->
         code_atom_float cg pos debug a1 (fun op1 ->
            code_atom_float cg pos debug a2 (fun op2 ->
               compare_float op1 op2))
    | ACPoly ->
         raise (MirException (pos, StringError "code_ifthenelse_expr: ACPoly not allowed"))
    | ACPointer _
    | ACFunction _ ->
         code_atom_ptr_off cg pos debug a1 (fun op1 ->
            code_atom_ptr_off cg pos debug a2 (fun op2 ->
               match op with
                  Mir.REqOp _
                | Mir.RNeqOp _ ->
                     compare_int32 false op1 op2
                | _ ->
                     raise (MirException (pos, StringError "code_ifthenelse_expr: Only allowed to test pointers for (in)equality."))))
    | ACPointerInfix _ ->
         (* BUG: need to implement this  --jyh *)
         raise (MirException (pos, StringError "code_ifthenelse_expr: not implemented; if you hit this error, tell jyh"))


(* code_if_type_expr
   Constructs a branching expression with two cases, dependent
   on whether the named object is an instance of a particular type.
   This is the type coercion for objects. *)
and code_if_type_expr cg pos debug obj names name v e1 e2 =
   let pos = string_pos "code_if_type_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.IfType (obj, names, name, v, e1, e2))) in
      raise (MirException (pos, NotImplemented "code_if_type_expr:  IfType not supported in backend yet"))


(* code_external_expr
   Constructs a call to the outside world...  *)
and code_external_expr cg pos debug v ac f args e =
   let pos = string_pos "code_external_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.LetExternal (v, ac, f, args, e))) in
   code_read_atoms cg pos debug args (fun regs ->
      let f = builtin_lookup f in
      let insts' =
         match ac with
            ACInt
          | ACPointer _
          | ACFunction _
          | ACRawInt (Rawint.Int8, _)
          | ACRawInt (Rawint.Int16, _)
          | ACRawInt (Rawint.Int32, _) ->
               call_external_int32 pos v f regs
          | ACRawInt (Rawint.Int64, _) ->
               let vhi, vlo = reg64_lookup cg pos v in
                  call_external_int64 pos vhi vlo f regs
          | ACFloat _ ->
               call_external_float pos v f regs
          | ACPointerInfix _
          | ACPoly ->
               raise (MirException (pos, StringError "code_external_expr: ACPoly not allowed"))
      in
      let insts = Listbuf.add_listbuf insts insts' in
      let blocks, label = code_expr cg debug e in
      let block, label = code_build_dst_block debug "external" insts label in
         block :: blocks, label)


(* code_print_debug_expr
   Prints debugging information.  *)
and code_print_debug_expr cg pos debug label f args e =
   let pos = string_pos "code_print_debug_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.PrintDebug (label, f, args, e))) in
   let blocks, label =
      if Debug.debug Mir_fir.debug_code then
         let a = Mir.AtomRawInt (Rawint.of_int Rawint.Int32 true (Symbol.to_int label)) in
         code_read_atoms cg pos debug (a :: args) (fun regs ->
            let v = new_symbol_string "debug" in
            let insts = call_external_int32 pos v f regs in
            let blocks, label = code_expr cg debug e in
            let block, label = code_build_dst_block debug "external" insts label in
               block :: blocks, label)
      else
         code_expr cg debug e
   in
   let block, label = code_build_dst_block debug "print_debug" insts label in
      block :: blocks, label


(* code_reserve_expr
   Reserve memory based on memory requirements. *)
and code_reserve_expr cg pos debug rinfo e =
   let pos = string_pos "code_reserve_expr" pos in
   let label, vars, mem, ptr = rinfo in
      code_atom_int32 cg pos debug mem (fun rssize ->
         code_atom_int32 cg pos debug ptr (fun rsptrs ->
            let blocks, lbegin = code_expr cg debug e in

            (* Construct the GC block *)
            let vars =
               List.fold_left (fun ls v ->
                     let op =
                        try
                           let hi, lo = reg64_lookup_exn cg v in
                              ResInfix (Register hi, Register lo)
                        with
                           Not_found ->
                              ResBase (Register v)
                     in
                        op :: ls) [] vars
            in
            let insts = Listbuf.of_elt
               (CommentString (Printf.sprintf "%s: reserving memory and pointers"
                              (Symbol.to_string label)))
            in
            let insts =
               match rssize with
                  ImmediateNumber (OffNumber i) ->
                     Listbuf.add insts (CommentString (Printf.sprintf "%s: constant memory reserve, %s bytes"
                                                      (Symbol.to_string label) (Int32.format "%d" i)))
                | _ ->
                     insts
            in
            let insts =
               match rsptrs with
                  ImmediateNumber (OffNumber i) ->
                     Listbuf.add insts (CommentString (Printf.sprintf "%s: constant pointer reserve, %s ptrs"
                                                      (Symbol.to_string label) (Int32.format "%d" i)))
                | _ ->
                     insts
            in
            let insts = Listbuf.add insts (RES (ImmediateNumber (OffLabel label), vars, rssize, rsptrs)) in
            let bres, lres = code_build_dst_block debug "reserve" insts lbegin in

            (* Check for sufficient memory *)
            let minor_limit = Register minor_limit in
            let mem_next    = Register mem_next in
            let rmemsize    = new_symbol_string "memsize" in
            let insts = Listbuf.of_list
               [code_load_int32 rmemsize minor_limit;
                SUB (IL, Register rmemsize, mem_next);
                CMP (IL, Register rmemsize, rssize);
                JCC (ULT, ImmediateLabel lres)]
            in

            (* Check for sufficient number of pointers *)
            let pointer_free = Register pointer_free in
            let insts = Listbuf.add_list insts
               [CMP (IL, pointer_free, rsptrs);
                JCC (ULT, ImmediateLabel lres)]
            in

            (* Build the sanity check blocks *)
            let bchk, lchk = code_build_dst_block debug "check" insts lbegin in
               bres :: bchk :: blocks, lchk))


(* code_alloc_expr
   This function constructs the alloc case.  Reserves have already been
   computed (even for the AllocMalloc case), so we may assume that the
   pointer table and heap both have sufficient space.  *)
and code_alloc_expr cg pos debug v op e =
   let pos = string_pos "code_alloc_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.LetAlloc (v, op, e))) in
   let blocks, label = code_expr cg debug e in
   let blocks, label =
      match op with
         Mir.AllocMalloc size ->
            code_alloc_unsafe cg pos debug v size blocks label
       | Mir.AllocTuple args
       | Mir.AllocArray args ->
            code_alloc_tuple cg pos debug v 0 args blocks label
       | Mir.AllocMArray (args, a) ->
            raise (Invalid_argument "code_alloc_expr: not implemented yet")
       | Mir.AllocUnion (tag, args) ->
            code_alloc_tuple cg pos debug v tag args blocks label
   in
   let block, label = code_build_dst_block debug "alloc" insts label in
      block :: blocks, label


(* code_alloc_unsafe
   Allocate a block of raw memory.  The block must be added to the pointer
   table.  Refer to arch/x86/runtime/x86_runtime.h for the current block
   format.  *)
and code_alloc_unsafe cg pos debug v size blocks label =
   let pos = string_pos "code_alloc_unsafe" pos in
      code_atom_int32 cg pos debug size (fun size ->
         (* Allocate an unsafe pointer for the given type *)
         let v = Register v in
         let insts =
            match size with
               ImmediateNumber (OffNumber i) ->
                  Listbuf.of_elt (CommentString (Printf.sprintf "Allocating %s bytes of memory" (Int32.format "%d" i)))
             | _ ->
                  Listbuf.of_elt (CommentString "Allocating variable number of bytes")
         in
         let pointer_next = Register pointer_next in
         let pointer_base = Register pointer_base in
         let pointer_free = Register pointer_free in
         let mem_next     = Register mem_next in
         let rptrnext = new_symbol_string "alloc_ptr_next" in
         let rptrfree = new_symbol_string "alloc_ptr_free" in
         let rnextval = new_symbol_string "alloc_next_val" in
         let rmemnext = new_symbol_string "alloc_mem_next" in
         let rsize    = new_symbol_string "alloc_size" in
         let _ = assert (index_word = aggr_word) in

         (* Build the instructions for an unsafe allocation *)
         let insts = Listbuf.add_list insts
            [(* Update *pointer_next *)
             code_load_int32 rptrnext pointer_next;
             code_load_int32 rnextval (MemReg rptrnext);
             AND (IL, Register rnextval, ImmediateNumber (OffNumber ptable_pointer_mask));
             code_load_int32 rmemnext mem_next;
             ADD (IL, Register rmemnext, ImmediateNumber (OffNumber (Int32.of_int aggr_header_size)));
             code_store_int32 (MemReg rptrnext) rmemnext;

             (* Setup block header *)
             SUB (IL, Register rptrnext, pointer_base);
             OR  (IL, Register rptrnext, ImmediateNumber (OffNumber aggr_shift_mask32));
             code_store_int32 (MemRegOff (rmemnext, index_offset)) rptrnext;
             code_load_int32 rsize size;
             code_store_int32 (MemRegOff (rmemnext, size_offset)) rsize;

             (* Now we can update pointer_next *)
             code_store_int32 pointer_next rnextval;

             (* Compute the base pointer here *)
             code_store_int32 v rmemnext;

             (* Update mem_next *)
             ADD (IL, Register rmemnext, size);
             code_store_int32 mem_next rmemnext;

             (* Update pointer_free *)
             DEC (IL, pointer_free)]
         in
         let block, label = code_build_dst_block debug "alloc_unsafe" insts label in
            block :: blocks, label)


(* code_alloc_tuple
   Alloc a block of memory containing ML-ints and pointers.  Refer to
   arch/x86/runtime/x86_runtime.h for the current block format.  *)
and code_alloc_tuple cg pos debug v tag args blocks label =
   let pos = string_pos "code_alloc_tuple" pos in
   let len = List.length args in
   let size = len * sizeof_pointer in
   let isize = ImmediateNumber (OffNumber (Int32.of_int size)) in
   let insts_alloc = Listbuf.of_elt (CommentString (Printf.sprintf "Size of aligned type is %d" size)) in

   (* Allocate an entry in the pointer table *)
   let rv = Register v in
   let pointer_next = Register pointer_next in
   let pointer_base = Register pointer_base in
   let pointer_free = Register pointer_free in
   let mem_next     = Register mem_next in
   let rptrnext = new_symbol_string "alloc_ptr_next" in
   let rptrfree = new_symbol_string "alloc_ptr_free" in
   let rnextval = new_symbol_string "alloc_next_val" in
   let rmemnext = new_symbol_string "alloc_mem_next" in
   let rsize    = new_symbol_string "alloc_size" in
   let _ = assert (tag_word = size_word) in

   (* Build the first part of the instructions; this allocates
      the memory space but does not initialise the memory.  *)
   let insts_alloc = Listbuf.add_list insts_alloc
      [(* Update *pointer_next *)
       code_load_int32 rptrnext pointer_next;
       code_load_int32 rnextval (MemReg rptrnext);
       AND (IL, Register rnextval, ImmediateNumber (OffNumber ptable_pointer_mask));
       code_load_int32 rmemnext mem_next;
       ADD (IL, Register rmemnext, ImmediateNumber (OffNumber (Int32.of_int block_header_size)));
       code_store_int32 (MemReg rptrnext) rmemnext;

       (* Setup block header *)
       SUB (IL, Register rptrnext, pointer_base);
       code_store_int32 (MemRegOff (rmemnext, index_offset)) rptrnext;
       code_load_int32 rsize isize;
       OR (IL, Register rsize, ImmediateNumber (OffNumber (Int32.shift_left (Int32.of_int tag) tag_shift)));
       code_store_int32 (MemRegOff (rmemnext, size_offset)) rsize;

       (* Now we can update pointer_next *)
       code_store_int32 pointer_next rnextval;

       (* Compute the base pointer here *)
       code_store_int32 rv rmemnext;

       (* Update mem_next *)
       ADD (IL, Register rmemnext, isize);
       code_store_int32 mem_next rmemnext;

       (* Update pointer_free *)
       DEC (IL, pointer_free)]
   in

   (* Save all the args into the memory block *)
   let rec store_args off = function
      a :: args ->
         begin
            let offset_ptr = MemRegOff (v, OffNumber (Int32.of_int off)) in
            let int32_cont op =
               let r = new_symbol_string "code_alloc_tuple.int32" in
               let insts = Listbuf.of_list
                  [code_load_int32 r op;
                   code_store_int32 offset_ptr r]
               in
               let blocks, label = store_args (off + sizeof_int32) args in
               let block, label = code_build_dst_block debug "set_tuple_int32" insts label in
                  block :: blocks, label
            in
               match atom_class_of_atom a with
                  ACInt ->
                     code_atom_int cg pos debug a int32_cont
                | ACPointer _
                | ACFunction _
                | ACRawInt (Rawint.Int8, _)
                | ACRawInt (Rawint.Int16, _)
                | ACRawInt (Rawint.Int32, _) ->
                     code_atom_int32 cg pos debug a int32_cont
                | ACPointerInfix _
                | ACRawInt (Rawint.Int64, _) ->
                     code_atom_int64 cg pos debug a (fun ohi olo ->
                        let rhi = new_symbol_string "copy_atom_int64_hi" in
                        let rlo = new_symbol_string "copy_atom_int64_lo" in
                        let offsetlo_ptr = offset_ptr in
                        let offsethi_ptr = offset_from_base offsetlo_ptr 1 in
                        let insts = Listbuf.of_list
                           [code_load_int32 rlo olo;
                            code_store_int32 offsetlo_ptr rlo;
                            code_load_int32 rhi ohi;
                            code_store_int32 offsethi_ptr rhi]
                        in
                        let blocks, label = store_args (off + sizeof_int64) args in
                        let block, label = code_build_dst_block debug "set_tuple_arg" insts label in
                           block :: blocks, label)
                | ACFloat pre ->
                     code_atom_float cg pos debug a (fun op ->
                        let r = FloatRegister (new_symbol_string "copy_atom_float") in
                        let insts = Listbuf.of_list
                           [FMOVP (FT, r, FT, op);
                            FMOVP (float_prec pre, offset_ptr, FT, r)]
                        in
                        let blocks, label = store_args (off + (float_prec_size pre)) args in
                        let block, label = code_build_dst_block debug "set_tuple_arg" insts label in
                           block :: blocks, label)
                | ACPoly ->
                     raise (MirException (pos, NotImplemented "code_alloc_tuple: ACPoly"))
         end (* Atom argument case *)
    | [] ->
         (* Continuation blocks, after the alloc operation *)
         blocks, label
   in

   (* Prepend the alloc blocks before the set-tuple blocks *)
   let blocks, label = store_args 0 args in
   let block, label = code_build_dst_block debug "alloc_tuple" insts_alloc label in
      block :: blocks, label


(* code_set_mem_expr
   Sets a location in memory to some random value.  This function
   should deal with sizing and signedness issues properly; see its
   mirror code let_mem in x86_mir_atom.ml.  *)
and code_set_mem_expr cg pos debug ac ptrty aptr aofs aval e =
   let pos = string_pos "code_set_mem_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.SetMem (ac, ptrty, aptr, aofs, aval, e))) in
   let rptr  = new_symbol_string "set_mem_ptr" in
   let rdata = new_symbol_string "set_mem_data" in
   let blocks, label = code_expr cg debug e in
   code_atom_ptr_off cg pos debug aptr (fun optr ->
      let insts = Listbuf.add insts (code_load_int32 rptr optr) in

      (* Dispatch function *)
      let store_insts insts operand =
         (* Storing ML int values *)
         let store_code_int insts =
            code_atom_int cg pos debug aval (fun oval ->
               let insts = Listbuf.add_list insts
                  [CommentString ("Storing an ML int to memory");
                   code_load_int32 rdata oval;
                   code_store_int32 operand rdata]
               in
               let block, label = code_build_dst_block debug "set_mem_int32" insts label in
                  block :: blocks, label)
         in

         (* Storing int32 values *)
         let store_code_int32 insts name rdata store =
            code_atom_int32 cg pos debug aval (fun oval ->
               let insts = Listbuf.add_list insts
                  [CommentString ("Storing a " ^ name ^ " to memory");
                   code_load_int32 rdata oval;
                   store operand rdata]
               in
               let block, label = code_build_dst_block debug "set_mem_int32" insts label in
                  block :: blocks, label)
         in

         (* Storing polymorphic values.
            We have to be careful here, because we don't know if the
            value is an int or a pointer.  *)
         let store_code_poly insts =
            code_atom_poly cg pos debug aval (fun oval ->
               (* Once we have the value, store it *)
               let insts1 = Listbuf.of_elt (code_store_uint32 operand rdata) in
               let block1, label1 = code_build_dst_block debug "set_mem_poly" insts1 label in

               (* At this point we have a pointer, so look up the index *)
               let insts2 = Listbuf.of_elt (MOV (IL, Register rdata, MemRegOff (rdata, index_offset))) in
               let block2, label2 = code_build_dst_block debug "set_mem_poly" insts2 label in

               (* Initial block: check if it is an int *)
               let insts = Listbuf.add_list insts
                  [CommentString ("Storing a <poly> to memory");
                   code_load_int32 rdata oval;
                   TEST (IL, Register rdata, ImmediateNumber (OffNumber Int32.one));
                   JCC (NEQ, ImmediateLabel label1)]
               in
               let block3, label3 = code_build_dst_block debug "set_mem_poly" insts label2 in
                  block3 :: block2 :: block1 :: blocks, label3)
         in

         (* Storing int64 values *)
         let store_code_int64 insts =
            code_atom_int64 cg pos debug aval (fun ohi olo ->
               let operand2 = offset_from_base operand 1 in
               let rhi = new_symbol_string "set_mem_hi" in
               let rlo = new_symbol_string "set_mem_lo" in
               let insts = Listbuf.add_list insts
                  [CommentString ("Storing a 64-bit value to memory");
                   code_load_int32 rlo olo;
                   code_store_int32 operand rlo;
                   code_load_int32 rhi ohi;
                   code_store_int32 operand2 rhi]
               in
               let block, label = code_build_dst_block debug "set_mem_int64" insts label in
                  block :: blocks, label)
         in

         (* Storing float values *)
         let store_code_float insts pre =
            code_atom_float cg pos debug aval (fun op ->
               let pre = float_prec pre in
               let insts = Listbuf.add_list insts
                  [CommentString ("Storing a float value to memory");
                   FMOVP (pre, operand, FT, op)]
               in
               let block, label = code_build_dst_block debug "set_mem_float" insts label in
                  block :: blocks, label)
         in

         match ac with
            (* For the 8-bit cases, we have to store the value into a
               temporary register which can be broken up into an 8-bit
               value.  We choose ecx since it also gets clobbered by
               shifting operations... *)
            ACRawInt (Rawint.Int8,  false) -> store_code_int32 insts "uint8"  ecx   code_store_uint8
          | ACRawInt (Rawint.Int8,  true)  -> store_code_int32 insts "int8"   ecx   code_store_int8
          | ACRawInt (Rawint.Int16, false) -> store_code_int32 insts "uint16" rdata code_store_uint16
          | ACRawInt (Rawint.Int16, true)  -> store_code_int32 insts "int16"  rdata code_store_int16
          | ACPointer _
          | ACFunction _
          | ACRawInt (Rawint.Int32, false) -> store_code_int32 insts "uint32" rdata code_store_uint32
          | ACRawInt (Rawint.Int32, true)  -> store_code_int32 insts "int32"  rdata code_store_int32
          | ACRawInt (Rawint.Int64, _)     -> store_code_int64 insts
          | ACInt                          -> store_code_int   insts
          | ACFloat  pre                   -> store_code_float insts pre
          | ACPoly                         -> store_code_poly  insts
          | ACPointerInfix _ ->
               raise (MirException (pos, StringAtomClassError ("set_mem", ac)))
      in (* end of store_insts *)

      (* Compute all instructions *)
      match aofs with
         Mir.AtomInt i ->
            (* Offset is constant *)
            let operand = MemRegOff (rptr, OffNumber (Int32.of_int i)) in
               store_insts insts operand
       | Mir.AtomRawInt i ->
            (* Offset is constant *)
            let operand = MemRegOff (rptr, OffNumber (Rawint.to_int32 i)) in
               store_insts insts operand
       | _ ->
            (* Offset in a register; it must be a byte-offset *)
            let rofs = new_symbol_string "set_mem_off" in
            code_atom_int32 cg pos debug aofs (fun oofs ->
               let insts = Listbuf.add insts (code_load_int32 rofs oofs) in
               let operand = MemRegRegOffMul (rptr, rofs, OffNumber Int32.zero, Int32.one) in
                  store_insts insts operand))


(* code_bounds_check_expr
   Currently BoundsCheck is simplified at the end of MIR.  This should
   not escape to the backend (makes sense; it's more painful to express
   in assembly, so why bother) *)
and code_bounds_check_expr cg pos debug label ptrty a_ptr a_start a_stop e =
   let pos = string_pos "code_bounds_check_expr" pos in
      raise (MirException (pos, InternalError "X86_mir.code_bounds_check_expr:  BoundsCheck should have already been rewritten"))


(* code_lower_bounds_check_expr
   Currently LowerBoundsCheck is simplified at the end of MIR. This should
   not escape to the backend (makes sense; it's more painful to express
   in assembly, so why bother) *)
and code_lower_bounds_check_expr cg pos debug label ptrty a_ptr a_start e =
   let pos = string_pos "code_lower_bounds_check_expr" pos in
      raise (MirException (pos, InternalError "X86_mir.code_lower_bounds_check_expr:  LowerBoundsCheck should have already been rewritten"))


(* code_upper_bounds_check_expr
   Currently UpperBoundsCheck is simplified at the end of MIR. This should
   not escape to the backend (makes sense; it's more painful to express
   in assembly, so why bother) *)
and code_upper_bounds_check_expr cg pos debug label ptrty a_ptr a_stop e =
   let pos = string_pos "code_upper_bounds_check_expr" pos in
      raise (MirException (pos, InternalError "X86_mir.code_upper_bounds_check_expr:  UpperBoundsCheck should have already been rewritten"))


(* code_pointer_index_check_expr
   Currently PointerIndexCheck is simplified at the end of MIR.  This should
   not escape to the backend (makes sense; it's more painful to express
   in assembly, so why bother) *)
and code_pointer_index_check_expr cg pos debug label ptrty v ac index e =
   let pos = string_pos "code_pointer_index_check_expr" pos in
      raise (MirException (pos, InternalError "X86_mir.code_pointer_index_check_expr:  PointerIndexCheck should have already been rewritten"))


(* code_function_index_check_expr
   Currently PointerIndexCheck is simplified at the end of MIR.  This should
   not escape to the backend (makes sense; it's more painful to express
   in assembly, so why bother) *)
and code_function_index_check_expr cg pos debug label funty v ac index e =
   let pos = string_pos "code_function_index_check_expr" pos in
      raise (MirException (pos, InternalError "X86_mir.code_function_index_check_expr:  FunctionIndexCheck should have already been rewritten"))


(* code_memcpy_expr
   Codes up a memory-copy operation.  *)
   (* dst, dst_off, src, src_off, len, e *)
and code_memcpy_expr cg pos debug ptrty dst dst_off src src_off len e =
   let pos = string_pos "code_memcpy_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.Memcpy (ptrty, dst, dst_off, src, src_off, len, e))) in
      code_atom_ptr_off cg pos debug dst (fun dst ->
      code_atom_int32   cg pos debug dst_off (fun dst_off ->
      code_atom_ptr_off cg pos debug src (fun src ->
      code_atom_int32   cg pos debug src_off (fun src_off ->
      code_atom_int32   cg pos debug len (fun len ->
         let insts = Listbuf.add_list insts
            [CommentString "Copying a memory block";
             MOV (IL, Register esi, src);
             ADD (IL, Register esi, src_off);
             MOV (IL, Register edi, dst);
             ADD (IL, Register edi, dst_off);
             MOV (IL, Register ecx, len);
             RMOVS IB]
         in
         let blocks, label = code_expr cg debug e in
         let block, label = code_build_dst_block debug "memcpy" insts label in
            block :: blocks, label)))))


(***  Migration  ***)


(* code_migrate_expr
   Codes up a migration expression.  *)
and code_migrate_expr cg pos debug key dptr doff f env =
   let pos = string_pos "code_migrate_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.SysMigrate (key, dptr, doff, f, env))) in
   let ac_native_unsigned = MirUtil.ac_native_unsigned in

   (* Convert the atom arguments into assembly-friendly forms *)
   code_atom_ptr_off cg pos debug dptr (fun dptr ->
      code_atom_int32 cg pos debug doff (fun doff ->
         code_atom_int32 cg pos debug env (fun env ->
            (* Encode variables and name the temporaries *)
            let env' = new_symbol_string "migrate_env_index" in
            let rbin = new_symbol_string "migrate_bin_fir" in
            let ridx = new_symbol_string "migrate_register_index" in
            let roff = new_symbol_string "migrate_doff" in
            let rptr = new_symbol_string "migrate_dptr" in
            let rtmp = new_symbol_string "migrate_tmp" in
            let rres = new_symbol_string "migrate_res" in

            (* Construct the assembly instructions *)
            let insts = Listbuf.add_list insts
               [(* Push arguments onto the stack *)
                CommentString "Beginning MIGRATE code";
                CommentString "Loading destination offset";
                code_load_int32 roff doff;
                PUSH (IL, Register roff);
                CommentString "Loading destination pointer";
                code_load_int32 rptr dptr;
                PUSH (IL, Register rptr);
                CommentString "Loading register memory block index";
                code_load_int32 ridx env;
                PUSH (IL, Register ridx);
                CommentString (sprintf "Loading migrate key %d" key);
                PUSH (IL, ImmediateNumber (OffNumber (Int32.of_int key)));
                CommentString "Loading pointer to binary FIR";
                LEA  (IL, Register rbin, ImmediateLabel bin_fir_label);
                PUSH (IL, Register rbin);
                CommentString "Loading pointer to context structure";
                PUSH (IL, Register ebp);

                (* Call the migrate external function *)
                CALL (ImmediateLabel migrate_fun_label);

                (* If we reach this point, migration failed. Recover gracefully *)
                ADD  (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int (6 * sizeof_int))));
                code_load_int32 rtmp env;
                code_store_int32 (Register env') rtmp]
            in

            (* On failure case, we currently fallthrough; therefore the insts
               block defined above will be terminated by a call to label.  In
               this case, the label is the tailcall that we generate now.  *)
            let args = [Mir.AtomVar (ac_native_unsigned, env')] in
            let blocks, label = code_tailcall_expr cg pos debug f Mir.DirectCall args in

            (* Compute code for continuation from migrate (success case) *)
            let key = migrate_label_of_key key in
            let stk = MemRegOff (esp, OffNumber (Int32.of_int callee_header_stack)) in
            let icont = Listbuf.empty in
            let icont = Listbuf.add_list icont callee_header in
            let icont = Listbuf.add_list icont
               [code_load_int32 rres stk;
                code_store_int32 (Register env') rres;
                JMP (ImmediateLabel label)]
            in
            let bcont, _ = code_build_block debug key icont in
            let block, label = code_build_dst_block debug "migrate" insts label in
               block :: bcont :: blocks, label)))


(***  Rollback  ***)


(* code_atomic_expr
   Codes an atomic expression into assembly.  This makes up
   the basic primitive that is required for rollback.  *)
and code_atomic_expr cg pos debug f i env =
   let pos = string_pos "code_atomic_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.Atomic (f, i, env))) in
   let vatomic = new_symbol_string "atomic_info" in
   let ac_native_int = MirUtil.ac_native_int in
   let ac_aggr_pointer = MirUtil.ac_aggr_pointer in
   let unsigned_int = MirUtil.unsigned_int in
   let index_ac = ACRawInt (precision_native_int, unsigned_int) in

   (* atomic() requires the indexed versions of f and env,
      so let's compute the indices here.  If we don't pass
      in the indices then we'd have problems on rollback. *)
   let funty = FunArgs [ac_native_int; ac_aggr_pointer] in
   let f' = Mir.AtomUnop (Mir.MemFunIndexOp (funty, index_ac), Mir.AtomFunVar (ACFunction funty, f)) in
   let env' = Mir.AtomUnop (Mir.MemIndexOp (PtrAggr, index_ac), env) in

   (* Call the atomic external function to setup the Atomic struct.
      The atomic structure is returned to vatomic; from there we'll
      simply build a tailcall to f with appropriate args.  Note that
      the real Atomic* is returned, not an index.  *)
   code_read_atoms cg pos debug [f'; env'] (fun args ->
      let vvoid = new_symbol_string "rtn_void"  in
      let insts = Listbuf.add insts (CommentString "Beginning ATOMIC code") in
      let insts' = call_external_int32 pos vvoid atomic_fun_label args in
      let insts = Listbuf.add_listbuf insts insts' in

      (* Call the function f with appropriate arguments *)
      let args = [i; env] in
      let blocks, label = code_tailcall_expr cg pos debug f Mir.DirectCall args in
      let block, label = code_build_dst_block debug "atomic" insts label in
         block :: blocks, label)


(* code_atomic_rollback_expr
   Codes an atomic rollback expression into assembly.  This
   makes up the recovery primitive required by rollback.  *)
and code_atomic_rollback_expr cg pos debug level i =
   let pos = string_pos "code_atomic_rollback_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.AtomicRollback (level, i))) in
   let vvoid = new_symbol_string "rtn_void" in
   let ac_native_int = MirUtil.ac_native_int in
   let ac_native_unsigned = MirUtil.ac_native_unsigned in
   let ac_aggr_pointer = MirUtil.ac_aggr_pointer in

   (* Convert the level argument to a register; that argument needs
      to be passed to the runtime function, not a MIR tailcall. *)
   code_read_atoms cg pos debug [level] (fun args ->

      (* Call the atomic rollback function to restore the old pointer
         table.  After this is called, we can read off the function
         and env indices, convert them back to real pointers, and then
         call f.  Note that atomic_rollback() takes a real pointer.  *)
      let insts = Listbuf.add insts (CommentString "Beginning ATOMIC ROLLBACK code") in
      let insts' = call_external_int32 pos vvoid atomic_rollback_fun_label args in
      let insts = Listbuf.add_listbuf insts insts' in

      (* NOTE: After rollback, fun and env of the MOST RECENT level will
         actually contain the right function and environment; because the
         rollback argument preserves the fun and env of the OLDEST level
         rolled back.  Since this code runs after the runtime rollback
         call, we will get the right values here. *)

      (* We actually need to read fields off of atomic_info[atomic_next - 1] *)
      let vinfo = new_symbol_string "atomic_info" in
      let anext = Mir.AtomBinop (Mir.MulOp ac_native_unsigned,
                                 Mir.AtomBinop (Mir.MinusOp ac_native_unsigned,
                                                Mir.AtomVar (ac_native_unsigned, atomic_next),
                                                Mir.AtomRawInt (Rawint.of_int precision_native_int false 1)),
                                 Mir.AtomRawInt (Rawint.of_int precision_native_int false sizeof_pointer))
      in
      let ainfo = Mir.AtomBinop (Mir.MemOp (ac_aggr_pointer, PtrAggr),
                                 Mir.AtomVar (ac_aggr_pointer, atomic_info), anext)
      in

      (* Determine a pointer to the function escape to recall *)
      let afunoff = Mir.AtomBinop (Mir.MemOp (ac_native_unsigned, PtrAggr),
                                   Mir.AtomVar (ac_aggr_pointer, vinfo),
                                   Mir.AtomRawInt (Rawint.of_int precision_native_int false atomic_fun_byte_offset))
      in
      let funty = FunArgs [ac_native_int; ac_aggr_pointer] in
      let afun = Mir.AtomUnop (Mir.SafeFunctionOfIndexOp funty, afunoff) in
      let vfun = new_symbol_string "rollback_function" in

      (* Determine the environment pointer based on index; note that because
         we have already applied the rollback, the index should give us the
         old copy of the pointer table again, hooray! *)
      let aenvoff = Mir.AtomBinop (Mir.MemOp (ac_native_unsigned, PtrAggr),
                                   Mir.AtomVar (ac_aggr_pointer, vinfo),
                                   Mir.AtomRawInt (Rawint.of_int precision_native_int false atomic_env_byte_offset))
      in
      let aenv = Mir.AtomUnop (Mir.SafePointerOfIndexOp PtrAggr, aenvoff) in

      (* Recall the atomic function w/ new arguments *)
      let e =
         Mir.LetAtom (vinfo, ac_aggr_pointer, ainfo,
         Mir.LetAtom (vfun, ACFunction funty, afun,
         Mir.TailCall (Mir.IndirectCall, vfun, [i; aenv])))
      in
      (* The pointer/index coercions are removed by simplify, so we have to
         manually call that here to convert them to their final MIR form.
         I should note here that none of this gets optimized; we should
         try to do these computations in the MIR, shouldn't we? *)
      let e = MirSimplify.simplify_expr cg.cg_arity e in
      let blocks, label = code_expr cg debug e in
      let block, label = code_build_dst_block debug "atomic_rollback" insts label in
         block :: blocks, label)


(* code_atomic_commit_expr
   Writes an atomic commit statement into the code.  This primitive
   should not be used unless we are actually within an atomic block
   at this time.  *)
and code_atomic_commit_expr cg pos debug level e =
   let pos = string_pos "code_atomic_commit_expr" pos in
   let insts = Listbuf.of_elt (CommentMIR (Mir.AtomicCommit (level, e))) in
   let vvoid = new_symbol_string "rtn_void" in

   (* Convert the level argument to a register; that argument needs
      to be passed to the runtime function, not a MIR tailcall. *)
   code_read_atoms cg pos debug [level] (fun args ->

      (* Call the atomic COMMIT function to destroy the highest atomic
         level and commit any pending changes to the pointer table.
         We do this via an external function which will make sure all,
         including the copy pointer, are updated. *)
      let insts = Listbuf.add insts (CommentString "Beginning ATOMIC COMMIT code") in
      let insts' = call_external_int32 pos vvoid atomic_commit_fun_label args in
      let insts = Listbuf.add_listbuf insts insts' in
      let blocks, label = code_expr cg debug e in
      let block, label = code_build_dst_block debug "atomic_commit" insts label in
         block :: blocks, label)


(* code_copy_on_write_expr
   Checks if the block pointed to by the real pointer ptr has the
   copy-on-write bit set; if so, then the block is copied as required
   by the MIR specification.  *)
and code_copy_on_write_expr cg pos debug rinfo copy_ptr e =
   let pos = string_pos "code_copy_on_write_expr" pos in
   let lcow, vars, mem, ptr = rinfo in

   (* Code the blocks and label for the continuation *)
   let bcont, lcont = code_expr cg debug e in

   (* First things first: code the pointer itself into an operand. *)
   code_atom_ptr_base cg (int_pos 1 pos) debug copy_ptr (fun op_ptr ->

      (* Construct the inner block which actually performs the copy,
         and updates the applicable pointer table entry.  For now,
         this is just implemented as a call to the fault handler. *)
      let bcopy, lcopy =
         code_atom_int32 cg (int_pos 2 pos) debug mem (fun mem ->
            code_atom_int32 cg (int_pos 3 pos) debug ptr (fun ptr ->
               let insts = Listbuf.of_elt (CommentString "Entering COW copy code") in
               let vars =
                  List.fold_left (fun ls v ->
                     let op =
                        try
                           let vhi, vlo = reg64_lookup_exn cg v in
                              ResInfix (Register vhi, Register vlo)
                        with
                           Not_found ->
                              ResBase (Register v)
                     in
                        op :: ls) [] vars
               in
               let insts = Listbuf.add insts (COW (ImmediateNumber (OffLabel lcow), vars, mem, ptr, op_ptr)) in
               let block, label = code_build_dst_block debug "copy_on_write_copying" insts lcont in
                  [block], label))
      in (* End of copying operation *)

      (* Helpful flag *)
      let insts = Listbuf.of_elt (CommentString "Entering COW test code") in

      (* Now bcopy, lcopy contain the code for copying.  We need to add
         a test against the copy pointer; if we are less than, then we
         are immutable.  Note that atomic_info must be valid if the copy
         pointer is any value other than NULL. *)
      let rptr = new_symbol_string "reg_pointer" in
      let insts = Listbuf.add_list insts
         [code_load_int32 rptr op_ptr;
          CMP (IL, Register rptr, Register gc_point);
          JCC (UGE, ImmediateLabel lcont)]
      in
      let block, label = code_build_dst_block debug "copy_on_write_testing" insts lcopy in
         (* Assemble the final blocks *)
         block :: bcopy @ bcont, label)


(***  Code for One-Time blocks and Function Headings  ***)


(* collect_fun_args
   Extract the arguments lists for each function. *)
let collect_fun_args funs =
   SymbolTable.map (fun (_, args, _) -> args) funs


(* closure_table
   Extract escape mapping data for all functions.  This builds an
   environment mapping the original function name to:
      1. the name of the escaping version of the function
      2. the index into the function closure table for the function
      3. the arity tag for the escaped function
   The initial arity table is also returned by this function.  *)
let closure_table arity_tags funs fun_order =
   (* Construct the closure table/names of the escape functions *)
   let esc_name f = new_symbol_string (Symbol.to_string f ^ "_escape") in
   let closures = List.map esc_name fun_order in

   (* Construct the esc_funs symboltable. *)
   let fun_escape (index, esc_funs) name escaped =
      (* Lookup the function's formals *)
      let pos = string_pos "closure_table" (vexp_pos name) in
      let _, formals, _ =
         try SymbolTable.find funs name with
            Not_found ->
               raise (MirException (pos, UnboundVar name))
      in
      let arity_tag, _ = MirArity.arity_lookup_by_formals arity_tags pos formals in
      let esc_funs = SymbolTable.add esc_funs name (escaped, index, arity_tag) in
      let index = Int32.succ index in
         index, esc_funs
   in

   (* Save room for:
      __exit: entry 0
      __uncaught_exception: entry 1
    *)
   let init_index = Int32.of_int 2 in
   let initialiser = init_index, SymbolTable.empty in
   let _, esc_funs = List.fold_left2 fun_escape initialiser fun_order closures in
      closures, esc_funs


(* global_fun_args
   Load the function arguments into registers.  This only occurs for the
   global function stubs, which should have a callee_header on the stack
   already.  The arguments go as:  data_seg, __exit, <stack args>. *)
let global_fun_args cg pos debug args =
   let pos = string_pos "global_fun_args" pos in
   let rec global_fun_args insts off = function
      (arg, ac) :: args ->
         begin
         let stk = MemRegOff (esp, OffNumber (Int32.of_int off)) in
         let insts = Listbuf.add insts
            (CommentString (Printf.sprintf "Loading argument %s at offset %d" (string_of_symbol arg) off))
         in
         match ac with
            ACInt
          | ACPointer _
          | ACFunction _
          | ACRawInt (Rawint.Int8, _)
          | ACRawInt (Rawint.Int16, _)
          | ACRawInt (Rawint.Int32, _)
            (* Note: Poly arguments are transferred verbatim *)
          | ACPoly ->
               let arg = Register arg in
               let reg = new_symbol_string "arg" in
               let insts = Listbuf.add_list insts
                  [code_load_int32 reg stk;
                   code_store_int32 arg reg]
               in
                  global_fun_args insts (off + sizeof_int32) args
          | ACFloat pre ->
               let size  = float_prec_size pre in
               let pre   = float_prec pre in
               let arg   = FloatRegister arg in
               let insts = Listbuf.add insts (FMOVP (FT, arg, pre, stk)) in
                  global_fun_args insts (off + size) args
          | ACRawInt (Rawint.Int64, _) ->
               let ahi, alo = reg64_lookup cg pos arg in
               let ahi = Register ahi in
               let alo = Register alo in
               let rhi = new_symbol_string "arg" in
               let rlo = new_symbol_string "arg" in
               let stk2 = offset_from_base stk 1 in
               let insts = Listbuf.add_list insts
                  [code_load_int32 rlo stk;
                   code_store_int32 alo rlo;
                   code_load_int32 rhi stk2;
                   code_store_int32 ahi rhi]
               in
                  global_fun_args insts (off + sizeof_int64) args
          | ACPointerInfix _ ->
               raise (MirException (pos, StringAtomClassError ("global_fun_args", ac)))
         end (* processing an argument *)
    | [] ->
         insts
   in
   let insts = global_fun_args Listbuf.empty callee_header_stack args in
      insts


(* compile_fun
   Compile the specified function. *)
let compile_fun cg debug name (line, args, e) =
   let pos = string_pos "compile_fun" (exp_pos e) in
   let insts = Listbuf.of_elt (CommentString ("Entering function " ^ (string_of_symbol name))) in
   let debug = line, [] in

   (* Construct a helpful, informative comment *)
   let comm = List.fold_left (fun comm (v, _) -> comm ^ " " ^ (string_of_symbol v)) "" args in
   let insts = Listbuf.add insts (CommentString ("Arguments: " ^ comm)) in

   (* Build the body *)
   let blocks, lbegin = code_expr cg debug e in

   (* Code to build the function header *)
   let insts = Listbuf.add insts (JMP (ImmediateLabel lbegin)) in
   let block, _ = code_build_block debug name insts in
      block :: blocks


(* compile_fun_escape
   Compile a function escape for the specified function.
   The function escape is the destination of indirect jumps,
   and remote calls.  *)
let compile_fun_escape cg debug funs (export, blocks) name =
   let line, args, e = SymbolTable.find funs name in
   let pos = string_pos "compile_fun_escape" (exp_pos e) in
   let insts = Listbuf.of_elt (CommentString ("Entering escape function " ^ (string_of_symbol name))) in
   let debug = line, [] in
   let { cg_esc_funs = esc_funs } = cg in

   (* Find the real argument names *)
   let ename, index, arity_tag =
      try SymbolTable.find esc_funs name with
         Not_found ->
            raise (MirException (pos, UnboundVar name))
   in

   (* Get the arguments these were copied into *)
   let acs = List.map (fun (_, ac) -> ac) args in
   let vars = get_stdargs acs in
   let cg, vars = rewrite_args cg vars in

   (* Construct a helpful, informative comment *)
   let comm = List.fold_left (fun comm v -> comm ^ " " ^ (string_of_symbol v)) "" vars in
   let insts = Listbuf.add insts (CommentString ("Arguments: " ^ comm)) in

   (* Construct a real tailcall to the original function *)
   let atoms = List.map2 (fun v ac -> Mir.AtomVar(ac, v)) vars acs in
   let blocks', label = code_tailcall_expr cg pos debug name Mir.DirectCall atoms in
   let blocks = blocks' @ blocks in
   let insts = Listbuf.add insts (JMP (ImmediateLabel label)) in
   let block, _ = code_build_block debug ename insts in
   let block = code_block_set_index block ename arity_tag in
   let blocks = block :: blocks in

   (* If this is a global function, add it *)
   let export =
      try
         let sname = SymbolTable.find export name in
         let export = SymbolTable.remove export name in
            SymbolTable.add export ename sname
      with
         Not_found ->
            export
   in
      export, blocks


(* extract_block_list
   Get all blocks out of the function table and
   concatenate them all together. *)
let extract_block_list funs def =
   SymbolTable.fold (fun blocks _ blocks' -> blocks' @ blocks) def funs


(***  Outer Functions  ***)


(* Names of canonical functions. *)
let main_name = "main"
let init_name = "init"


(* global_block
   Generates code for a block that can be called from native C. *)
let global_block cg debug export name =
   let pos = string_pos "global_block" (vexp_pos (Symbol.add "global_block")) in
   let label = new_symbol name in

   (* Get the arguments of the function. *)
   let { cg_fun_args = fun_args } = cg in
   let args =
      try SymbolTable.find fun_args name with
         Not_found ->
            raise (MirException (pos, UnboundVar name))
   in
   let glob = global_fun_args cg pos debug args in

   (* Goto the user's init function *)
   let insts = Listbuf.empty in
   let insts = Listbuf.add_list insts callee_header in
   let insts = Listbuf.add_listbuf insts glob in
   let insts = Listbuf.add insts (JMP (ImmediateLabel name)) in
   let block, _ = code_build_block debug label insts in

   (* Update the exports *)
   let export = SymbolTable.remove export name in
      export, label, block


(* init_block
   Generates code for the __mc_init block. *)
let init_block cg debug export init_sym =
   let pos = string_pos "init_block" (vexp_pos (Symbol.add "init_block")) in
   let label = new_symbol init_sym in

   (* Get the two arguments of the init function.
      Pad the argument list if necessary.  *)
   let { cg_fun_args = fun_args } = cg in
   let args =
      try SymbolTable.find fun_args init_sym with
         Not_found ->
            raise (MirException (pos, UnboundVar init_sym))
   in
   let glob = global_fun_args cg pos debug args in

   (* Goto the user's init function *)
   let insts = Listbuf.empty in
   let insts = Listbuf.add_list insts callee_header in
   let insts = Listbuf.add_listbuf insts glob in
   let insts = Listbuf.add_list insts
      [FINIT;
       FSTCW (Register float_reg_1);
       OR (IW, Register float_reg_1, ImmediateNumber (OffNumber (Int32.of_int 0xc00)));
       FLDCW (Register float_reg_1);
       JMP (ImmediateLabel init_sym)]
   in
   let block, _ = code_build_block debug label insts in

   (* Update the exports *)
   let export = SymbolTable.remove export init_sym in
      export, label, block


(* empty_block
   Generates code for an empty block that just returns. *)
let empty_block cg debug export =
   let pos = string_pos "empty_block" (vexp_pos (Symbol.add "empty_block")) in
   let label = new_symbol_string "empty" in
   let insts = Listbuf.of_list
      [MOV (IL, Register eax, ImmediateNumber (OffNumber Int32.zero));
       RET]
   in
   let block, _ = code_build_block debug label insts in
      export, label, block


(* find_symbol
   Searches the export table for a particular global symbol; if
   found, return name of the real symbol to use for the global. *)
let find_symbol export sym =
   SymbolTable.fold (fun result v { export_name = name } ->
      if name = sym then
         Some v
      else
         result) None export


(* compile_prog
   Build the entire assembly program. *)
let compile prog =
   let { Mir.prog_file           = file_info;
         Mir.prog_import         = import;
         Mir.prog_export         = export;
         Mir.prog_globals        = globals;
         Mir.prog_funs           = funs;
         Mir.prog_mprog          = mfir;
         Mir.prog_global_order   = global_order;
         Mir.prog_fun_order      = fun_order;
         Mir.prog_arity_tags     = arity_tags;
       } = prog
   in
   let fir = mfir.Fir.marshal_fir in
   let reg64 = collect_int64_regs funs in
   let fvals = collect_float_vals funs in
   let debug = (file_info.file_name, 0), [] in

   (* Construct function argument maps, and make the escape functions *)
   let fun_args = collect_fun_args funs in
   let closures, esc_funs = closure_table arity_tags funs fun_order in

   (* Setup an initial, clean variable environment. *)
   let cg =
      { cg_fun_args  = fun_args;
        cg_esc_funs  = esc_funs;
        cg_reg64     = reg64;
        cg_fvals     = fvals;
        cg_arity     = arity_tags;
      }
   in

   (* Collect all the globals. *)
   let indexes, index =
      List.fold_left (fun (indexes, i) v ->
         if SymbolTable.mem globals v then
            let indexes = SymbolTable.add indexes v i in
               indexes, succ i
         else
            indexes, i) (SymbolTable.empty, 0) global_order
   in

   (* Add all the globals not listed by the mfir *)
   let indexes, _ =
      SymbolTable.fold (fun (indexes, i) v _ ->
         if SymbolTable.mem indexes v then
            indexes, i
         else
            let indexes = SymbolTable.add indexes v i in
               indexes, succ i) (indexes, index) globals
   in
   let globals = collect_globals esc_funs indexes globals in

   (* Build the ordered globals table. *)
   let globals =
      SymbolTable.fold (fun globals v init ->
         let pos = string_pos "x86_mir: compile globals" (vexp_pos v) in
         let index = SymbolTable.find indexes v in
            (v, init, index) :: globals) [] globals
   in
   let globals = List.sort (fun (_, _, i1) (_, _, i2) -> Pervasives.compare i1 i2) globals in
   let globals = List.map (fun (v, init, _) -> v, init) globals in

   (* Collect floating-point values *)
   let floats =
      FloatTable.fold (fun floats f v ->
         SymbolTable.add floats v f) SymbolTable.empty fvals
   in

   (* Create the init block *)
   let export, init_sym, iblock =
      match find_symbol export init_name with
         Some init_sym ->
            init_block cg debug export init_sym
       | None ->
            empty_block cg debug export
   in

   (* Main block *)
   let export, main_sym, mblock =
      match find_symbol export main_name with
         Some main_sym ->
            global_block cg debug export main_sym
       | _ ->
            empty_block cg debug export
   in

   (* Compile all functions *)
   let preserve = SymbolTable.fold (fun venv _ (_, _, e) ->
      migrate_labels venv e) SymbolSet.empty funs
   in
   let funblocks = SymbolTable.mapi (compile_fun cg debug) funs in
   let blocks = extract_block_list funblocks [] in
   let export, blocks = List.fold_left (compile_fun_escape cg debug funs) (export, blocks) fun_order in

   (* Assemble the blocks *)
   let blocks = mblock :: iblock :: blocks in

   (* Build the trace *)
   let blocks = Trace.of_list blocks in

   (* Figure out the final debug flags *)
   let debug = Int32.zero in
   let debug =
      if std_flags_get_bool "x86.debug.verbose_fault" then
         Int32.logor debug core_debug_verbose_fault
      else
         debug
   in

   (* No spills yet *)
   let empty_spill_counts =
      { spill_int_normal_index = 0;
        spill_int_stdarg_index = 0;
        spill_float_normal_index = 0;
        spill_float_stdarg_index = 0;
        spill_mmx_normal_index = 0;
        spill_mmx_stdarg_index = 0;
        spill_global_index = 0;
      }
   in

      (* Construct the program. *)
      { asm_types      = fir.Fir.prog_types;
        asm_import     = import;
        asm_export     = export;
        asm_preserve   = preserve;
        asm_file       = file_info;
        asm_spills     = empty_spill_counts;
        asm_closures   = closures;
        asm_globals    = globals;
        asm_floats     = floats;
        asm_blocks     = blocks;
        asm_bin_fir    = mfir;
        asm_spset      = initial_spset ();
        asm_migrate    = None;
        asm_init       = [file_info.file_class, init_sym, main_sym];
        asm_arity      = arity_tags;
        asm_debug      = debug;
      }


let compile = Fir_state.profile "X86_mir.compile" compile


let () = std_flags_register_list_help "x86.debug"
  ["x86.debug.verbose_fault",    FlagBool true,
                                 "If set, then fault handlers will dump the pointer table" ^
                                 " and heap contents (this can generate a very large amount" ^
                                 " of output when an exception occurs).  Not recommended for" ^
                                 " production programs."]
