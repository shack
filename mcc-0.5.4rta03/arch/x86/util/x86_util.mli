(*
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace

open Frame_type
open X86_inst_type
open X86_frame_type
open X86_frame

(*
 * Get the free variables in the blocks.
 *)
val vars_frame : cenv
val vars_blocks  : cenv -> (debug_info, inst) poly_block trace -> cenv
val vars_globals : cenv -> global list -> cenv

(*
 * Get the code form for the instructions in the blocks.
 * The var list is the list of variables that should not
 * be included in the defs/uses for the instructions.
 *)
val block_code : var list -> inst_block -> inst code list
val classify_block : var list -> inst_block -> code_block
val classify_blocks : var list -> inst_block trace -> code_block trace

(*
 * Invert a condition code.
 *)
val invert_cc : cc -> cc

(*
 * Operand maps.
 *)
val map_operands : (operand -> operand) -> inst -> inst
val map_operands_prog : (operand -> operand) -> prog -> prog
val fold_operands : ('a -> operand -> 'a) -> 'a -> inst -> 'a
val fold_operands_list : ('a -> operand -> 'a) -> 'a -> inst list -> 'a

(*
 * Misc instruction utilities.
 *)
val get_operands : inst -> inst code

(*
 * Check whether operands refer to memory.
 *)
val is_memory_operand : operand -> bool
val memory_inst : inst -> operand list * operand list

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
