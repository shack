(*
   Environment for MIR->x86 conversion
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open X86_frame
open Sizeof_const
open Symbol
open Mir
open Mir_arity_map


(*** Generic codegen environment ***)

type esc_funs = (var * int32 * var) SymbolTable.t
type fun_args = ((var * atom_class) list) SymbolTable.t

type codegen =
   { cg_fun_args : fun_args;
     cg_esc_funs : esc_funs;
     cg_reg64 : reg64_map;
     cg_fvals : fval_map;
     cg_arity : (symbol * int32) ArityTable.t;
   }
