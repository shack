(*
 * Compute a random permutation of the symbol table.
 * This module exists to help track down the symbol order bug.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format
open Symbol
open Flags

open Mir_arity_map

open X86_inst_type
open X86_frame_type
open X86_frame
open X86_util


(*  Register flags  ***)


let flag_prefix      = "x86.debug.permute"
let flag_before_ra   = flag_prefix ^ ".before_ra"
let flag_after_ra    = flag_prefix ^ ".after_ra"
let flag_print       = flag_prefix ^ ".print"
let flag_seed        = flag_prefix ^ ".seed"

let () = std_flags_help_section_text flag_prefix
   "Symbol permutations are used to identify rare problems related to symbol order in the backend."

let () = std_flags_register_list_help flag_prefix
  [flag_before_ra,   FlagBool false,
                     "Permute all symbols in the program before register allocation";
   flag_after_ra,    FlagBool false,
                     "Permute all symbols in the program after register allocation";
   flag_print,       FlagBool false,
                     "Print debug information when the permutation module is run";
   flag_seed,        FlagInt 0,
                     "Random seed for generating a permutation." ^
                     " If 0, then the identity permutation is generated." ^
                     " If negative, a single-transpose permutation is generated."]


(***  Types  ***)


type backend_stage =
   BeforeRA
 | AfterRA


(***  Permutation generation  ***)


let generate_random_permutation seed n =
   let rec find_unique_number ls =
      let k = Random.int n in
         if List.mem k ls then
            find_unique_number ls
         else
            k
   in
   let rec generate_list ls m =
      if m >= n then
         ls
      else
         generate_list (find_unique_number ls :: ls) (succ m)
   in
   let () = Random.init seed in
      generate_list [] 0


let generate_sequential_permutation n =
   let rec generate_from m =
      if m >= n then
         []
      else
         m :: generate_from (succ m)
   in
      generate_from 0


let generate_transpose_permutation xpose n =
   let rec generate_from m =
      if m >= n then
         []
      else if m = xpose - 1 then
         (m + 1) :: m :: generate_from (m + 2)
      else
         m :: generate_from (succ m)
   in
      generate_from 0


let generate_transpose_permutation xpose n =
   if xpose < 1 || xpose >= n then
      generate_sequential_permutation n
   else
      generate_transpose_permutation xpose n


let generate_permutation seed n =
   if seed < 0 then
      generate_transpose_permutation (-seed) n
   else if seed = 0 then
      generate_sequential_permutation n
   else
      generate_random_permutation seed n


let print_permutation seed ls =
   if std_flags_get_bool flag_print then begin
      eprintf "@[<v 3>*** X86_permutomatic:  Permutation used for seed %d:@ @[<hov 0>" seed;
      List.iter (fun i -> eprintf "%-5d@ " i) ls;
      eprintf "@]@]@.@."
   end


(***  Intern new symbols in permuted order  ***)


let intern_new_symbols env =
   let n = SymbolSet.fold (fun n _ -> succ n) 0 env in
   let seed = std_flags_get_int flag_seed in
   let perm = generate_permutation seed n in
   let () = print_permutation seed perm in

   let tsym = new_symbol_string "tsym" in
   let itable = Array.make n tsym in
   let add_symbol_entry (itable, perm) s =
      match perm with
         p :: perm ->
            itable.(p) <- s;
            itable, perm
       | [] ->
            raise (Failure "X86_permutomatic.intern_new_symbols: premature end of permutation list")
   in
   let itable, _ = SymbolSet.fold add_symbol_entry (itable, perm) env in

   let env = SymbolTable.empty in
   let env = Array.fold_left (fun env s -> SymbolTable.add env s (new_symbol s)) env itable in
      env


let print_renamed_symbols_in_prog env =
   if std_flags_get_bool flag_print then begin
      eprintf "@[<v 3>*** X86_permutomatic:  Symbols in program:";
      SymbolTable.iter (fun s1 s2 -> eprintf "@ %-16s -> %-16s" (string_of_symbol s1) (string_of_symbol s2)) env;
      eprintf "@]@.@."
   end


(***  Filter out well-known names, notably the hardware registers  ***)


let filter_symbols prog env =
   (* Filter out all hardware registers *)
   let env =
      Array.fold_left (fun env registers ->
         SymbolSet.subtract_list env registers) env registers
   in
   (* Filter out all globals *)
   let globals = prog.asm_globals in
   let env = SymbolSet.subtract_list env (List.map fst globals) in
      env


(***  Compute the set of all symbols in program  ***)


let symbols_in_table env table =
   SymbolTable.fold (fun env s _ -> SymbolSet.add env s) env table


let symbols_in_arity_table env table =
   ArityTable.fold (fun env _ (s, _) -> SymbolSet.add env s) env table


let symbols_in_set env set =
   SymbolSet.union env set


let symbols_in_list env ls =
   List.fold_left (fun env s -> SymbolSet.add env s) env ls


let symbols_in_global_list env ls =
   let symbols_in_init_val env init =
      match init with
         InitValInt32 _
       | InitValSingle _
       | InitValDouble _
       | InitValLongDouble _ ->
            env
       | InitValFunction (s1, _, s2)
       | InitValGlobal (s1, s2) ->
            SymbolSet.add (SymbolSet.add env s1) s2
   in
   let symbols_in_init_exp env init =
      match init with
         InitRawData _
       | InitAggrBlock _
       | InitRawInt _
       | InitRawFloat _ ->
            env
       | InitVar (label, v) ->
            SymbolSet.add (SymbolSet.add env label) v
       | InitTagBlock (_, init_ls) ->
            List.fold_left symbols_in_init_val env init_ls
   in
      List.fold_left (fun env (s, init) ->
         let env = SymbolSet.add env s in
            symbols_in_init_exp env init) env ls


let symbols_in_init_list env init =
   List.fold_left (fun env (_, s1, s2) ->
      SymbolSet.add (SymbolSet.add env s1) s2) env init


let symbol_in_option env = function
   None ->
      env
 | Some s ->
      SymbolSet.add env s


let symbols_in_block env block =
   let { block_label = label;
         block_index = index;
         block_arity = arity;
       } = block
   in
   let env = SymbolSet.add env label in
   let env = symbol_in_option env index in
   let env = symbol_in_option env arity in
      env


let symbols_in_blocks env blocks =
   let block_env = vars_blocks SymbolTable.empty blocks in
   let env = SymbolTable.fold (fun env s _ -> SymbolSet.add env s) env block_env in
      Trace.fold symbols_in_block env blocks


let symbols_in_prog prog =
   let { asm_import  = import;
         asm_export  = export;
         asm_preserve= preserve;
         asm_types   = types;
         asm_closures= closures;
         asm_globals = globals;
         asm_floats  = floats;
         asm_blocks  = blocks;
         asm_init    = init;
         asm_arity   = arity;
       } = prog in
   let env = SymbolSet.empty in
   let env = symbols_in_table env import in
   let env = symbols_in_table env export in
   let env = symbols_in_table env types in
   let env = symbols_in_table env floats in
   let env = symbols_in_arity_table env arity in
   let env = symbols_in_set env preserve in
   let env = symbols_in_list env closures in
   let env = symbols_in_global_list env globals in
   let env = symbols_in_init_list env init in
   let env = symbols_in_blocks env blocks in
      env


(***  Rename all symbols in program  ***)


let rename_symbol env s =
   try
      SymbolTable.find env s
   with
      Not_found ->
         s
         (*
         raise (Failure ("X86_permutomatic.rename_symbol:  could not find entry for " ^ (string_of_symbol s)))
          *)


let rename_symbol_option env = function
   None ->
      None
 | Some s ->
      Some (rename_symbol env s)


let rename_symbols_in_table env table =
   SymbolTable.fold (fun table s v ->
      let s = rename_symbol env s in
         SymbolTable.add table s v) SymbolTable.empty table


let rename_symbols_in_arity_table env table =
   ArityTable.fold (fun table arity (s, i) ->
      let s = rename_symbol env s in
         ArityTable.add table arity (s, i)) ArityTable.empty table


let rename_symbols_in_set env set =
   SymbolSet.fold (fun set s ->
      let s = rename_symbol env s in
         SymbolSet.add set s) SymbolSet.empty set


let rename_symbols_in_list env ls =
   List.map (rename_symbol env) ls


let rename_symbols_in_global_list env ls =
   let rename_symbols_in_init_val env init =
      match init with
         InitValInt32 _
       | InitValSingle _
       | InitValDouble _
       | InitValLongDouble _ ->
            init
       | InitValFunction (s1, i, s2) ->
            InitValFunction (rename_symbol env s1, i, rename_symbol env s2)
       | InitValGlobal (s1, s2) ->
            InitValGlobal (rename_symbol env s1, rename_symbol env s2)
   in
   let rename_symbols_in_init_exp env init =
      match init with
         InitRawData _
       | InitAggrBlock _
       | InitRawInt _
       | InitRawFloat _ ->
            init
       | InitVar (label, v) ->
            InitVar (rename_symbol env label, rename_symbol env v)
       | InitTagBlock (i, init_ls) ->
            InitTagBlock (i, List.map (rename_symbols_in_init_val env) init_ls)
   in
      List.map (fun (s, init) ->
         let s = rename_symbol env s in
         let init = rename_symbols_in_init_exp env init in
            s, init) ls


let rename_symbols_in_init_list env init =
   List.map (fun (fc, s1, s2) ->
      fc, rename_symbol env s1, rename_symbol env s2) init


let rename_symbols_in_inst env inst =
   map_operands (fun op ->
      match op with
         ImmediateNumber _
       | FPStack _ ->
            op
       | ImmediateLabel l ->
            ImmediateLabel (rename_symbol env l)
       | Register r ->
            Register (rename_symbol env r)
       | FloatRegister r ->
            FloatRegister (rename_symbol env r)
       | MMXRegister r ->
            MMXRegister (rename_symbol env r)
       | MemReg r ->
            MemReg (rename_symbol env r)
       | MemRegOff (r, i) ->
            MemRegOff (rename_symbol env r, i)
       | MemRegRegOffMul (r1, r2, i, j) ->
            MemRegRegOffMul (rename_symbol env r1, rename_symbol env r2, i, j)
       | SpillRegister (r, i) ->
            SpillRegister (rename_symbol env r, i)) inst


let rename_symbols_in_code env code =
   List.map (rename_symbols_in_inst env) code


let rename_symbols_in_block env block =
   let { block_label = label;
         block_code  = code;
         block_index = index;
         block_arity = arity;
       } = block
   in
   let label = rename_symbol env label in
   let code  = rename_symbols_in_code env code in
   let index = rename_symbol_option env index in
   let arity = rename_symbol_option env arity in
      { block with
         block_label = label;
         block_code  = code;
         block_index = index;
         block_arity = arity;
       }


let rename_symbols_in_blocks env blocks =
   Trace.map (rename_symbols_in_block env) blocks


let rename_symbols_in_prog env prog =
   let { asm_import  = import;
         asm_export  = export;
         asm_preserve= preserve;
         asm_types   = types;
         asm_closures= closures;
         asm_globals = globals;
         asm_floats  = floats;
         asm_blocks  = blocks;
         asm_init    = init;
         asm_arity   = arity;
       } = prog in
   let import  = rename_symbols_in_table env import in
   let export  = rename_symbols_in_table env export in
   let types   = rename_symbols_in_table env types in
   let floats  = rename_symbols_in_table env floats in
   let arity   = rename_symbols_in_arity_table env arity in
   let preserve= rename_symbols_in_set env preserve in
   let closures= rename_symbols_in_list env closures in
   let globals = rename_symbols_in_global_list env globals in
   let init    = rename_symbols_in_init_list env init in
   let blocks  = rename_symbols_in_blocks env blocks in
      { prog with
         asm_import  = import;
         asm_export  = export;
         asm_preserve= preserve;
         asm_types   = types;
         asm_closures= closures;
         asm_globals = globals;
         asm_floats  = floats;
         asm_blocks  = blocks;
         asm_init    = init;
         asm_arity   = arity;
      }


(***  Main function  ***)


let permute_program_symbols prog stage =
   let permute_flag =
      match stage with
         BeforeRA ->
            flag_before_ra
       | AfterRA ->
            flag_after_ra
   in
      if std_flags_get_bool permute_flag then
         let env = symbols_in_prog prog in
         let env = filter_symbols prog env in
         let env = intern_new_symbols env in
         let ()  = print_renamed_symbols_in_prog env in
         let prog = rename_symbols_in_prog env prog in
            prog
      else
         (* No permutation requested *)
         prog


let permute_program_symbols prog stage =
   Fir_state.profile "X86_permutomatic.permute_program_symbols" (permute_program_symbols prog) stage


