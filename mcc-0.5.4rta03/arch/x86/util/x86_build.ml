(*
   Build simple x86 instructions.
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Symbol
open Mir
open Mir_exn
open Mir_pos
open Sizeof_const

open Frame_type
open X86_inst_type
open X86_frame_type
open X86_frame
open X86_runtime

module Pos = MakePos (struct let name = "X86_build" end)
open Pos


(***  Utilities  ***)


(* Powers of 2. *)
let is_power2 i =
   let rec search k =
      if k > i || k = (Int32.shift_left i 31) then
         false
      else
         k = i || search (Int32.shift_left k 1)
   in
      search Int32.one

let log2 i =
   let rec search j =
      let k = (Int32.shift_left Int32.one j) in
      if k = i then
         Int32.of_int j
      else
         search (succ j)
   in
      search 0

let pow2 i =
   Int32.shift_left (Int32.of_int 1) (Int32.to_int i)


(* code_int32_of_int
   Code a int32 from an int.  The representation
   must have a 1 in the least significant bit.  *)
let code_int32_of_int i =
   let i = Int32.of_int i in
   let i = Int32.shift_left i 1 in
      Int32.logor i Int32.one


(* code_operand_of_int
   Codes an operand suitable for encoding an ML int value. *)
let code_operand_of_int i =
   let i = code_int32_of_int i in
      ImmediateNumber (OffNumber i)


(* collect_jumps
   Collect names of all jumps in this list of instructions. *)
let rec collect_jumps = function
   inst :: insts ->
      begin
         let jumps = collect_jumps insts in
            match inst with
               JMP(dst)    -> dst_labels dst jumps
             | JCC(_, dst) -> dst_labels dst jumps
             | _ -> jumps
      end
 | [] ->
      []


(* float_prec_size
   This takes a Rawfloat class and returns its size. *)
let float_prec_size = function
   Rawfloat.Single     -> sizeof_single
 | Rawfloat.Double     -> sizeof_double
 | Rawfloat.LongDouble -> sizeof_long_double


(* float_prec
   This takes a Rawfloat class and turns it into an FPU class. *)
let float_prec = function
   Rawfloat.Single     -> FS
 | Rawfloat.Double     -> FL
 | Rawfloat.LongDouble -> FT


(***  Construct Blocks  ***)


(* code_build_block
   This constructs a block which contains the instructions named in
   insts, and with the name given as its label.  The block and symbol
   label are returned. *)
let code_build_block debug label insts =
   let insts = Listbuf.to_list insts in
   let jumps = collect_jumps insts in
   let block =
      { block_debug  = debug;
        block_label  = label;
        block_code   = insts;
        block_jumps  = jumps;
        block_index  = None;
        block_align  = false;
        block_start  = false;
        block_arity  = None;
        block_opt    = true;
      }
   in
      block, label


(* code_build_noopt_block
   Similar to the above, but this block has the optimize flag set to
   false.  Should be used mainly for internal blocks, or for blocks
   which we know are in a little-traversed/exit path.  *)
let code_build_noopt_block debug label insts =
   let insts = Listbuf.to_list insts in
   let jumps = collect_jumps insts in
   let block =
      { block_debug  = debug;
        block_label  = label;
        block_code   = insts;
        block_jumps  = jumps;
        block_index  = None;
        block_align  = false;
        block_start  = false;
        block_arity  = None;
        block_opt    = false;
      }
   in
      block, label


(* code_block_set_index
   Sets the index value for this block.  This is used in conjunction
   with the closure table to determine what the index of an escaped
   function is.  The ename should be the escape tag, and arity_tag is
   the tag for arity information for this function. *)
let code_block_set_index block ename arity_tag =
   let index = new_symbol ename in
      { block with block_align = true;
                   block_index = Some index;
                   block_arity = Some arity_tag;
                   block_start = true;
      }


(* code_build_dst_block
   This constructs a new block, which contains the instructions named
   in insts followed by a JMP instruction to the immediate label named
   in dst.  *)
let code_build_dst_block debug name insts dst =
   let insts  = Listbuf.add insts (JMP (ImmediateLabel dst)) in
   let label = new_symbol_string name in
      code_build_block debug label insts


(* code_block_jump_comment *)
let code_block_jump_comment msg block =
   let msg = List.fold_left (fun msg v -> msg ^ " " ^ (string_of_symbol v)) (msg ^ ":") block.block_jumps in
      { block with block_code = CommentString msg :: block.block_code }


(***  External Calls  ***)


(* jump_external
   Generate an external call.  On x86 Linux, all arguments
   are pushed on the stack.  The return value is in eax.  *)
let push_external_args insts args =
   let rec push_args insts size = function
      a :: args ->
         let insts, size =
            match a with
               ArgInt32 a ->
                  let insts = Listbuf.add_list insts
                     [CommentString "Pushing int32 onto stack";
                      PUSH (IL, a)]
                  in
                     insts, size + sizeof_int32
             | ArgInt64 (ahi, alo) ->
                  let insts = Listbuf.add_list insts
                     [CommentString "Pushing int64 onto stack";
                      PUSH (IL, ahi);
                      PUSH (IL, alo)]
                  in
                     insts, size + sizeof_int64
             | ArgFloat (a, pre) ->
                  let size' = float_prec_size pre in
                  let size' = (size' + 3) land (lnot 3) in
                  let pre   = float_prec pre in
                  let insts = Listbuf.add_list insts
                     [CommentString "Pushing float onto stack";
                      SUB (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size')));
                      FMOVP (pre, MemReg esp, FT, a)]
                  in
                     insts, size + size'
         in
            push_args insts size args
    | [] ->
         insts, size
   in
      push_args insts 0 (List.rev args)

let pop_external_args insts args =
   let rec pop_args insts size = function
      a :: args ->
         let insts, size =
            match a with
               ArgInt32 a ->
                  let insts = Listbuf.add_list insts
                     [CommentString "Popping int32 from stack";
                      POP (IL, a)]
                  in
                     insts, size + sizeof_int32
             | ArgInt64 (ahi, alo) ->
                  let insts = Listbuf.add_list insts
                     [CommentString "Popping int64 from stack";
                      POP (IL, alo);
                      POP (IL, ahi)]
                  in
                     insts, size + sizeof_int64
             | ArgFloat (a, pre) ->
                  let size' = float_prec_size pre in
                  let pre   = float_prec pre in
                  let insts = Listbuf.add_list insts
                     [CommentString "Pushing float onto stack";
                      FMOVP (pre, a, FT, MemReg esp);
                      ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size')))]
                  in
                     insts, size + size'
         in
            pop_args insts size args
    | [] ->
         insts, size
   in
      pop_args insts 0 args

let jump_external call pos label args =
   let pos = string_pos "jump_external" pos in
   let insts, size = push_external_args Listbuf.empty args in
   let insts = Listbuf.add_list insts
      [CommentString "Calling external function";
       PUSH (IL, Register ebp);
       call (ImmediateLabel label)]
   in
      size, insts

let jump_external_int = jump_external (fun dst -> CALL dst)

let jump_external_float = jump_external (fun dst -> FCALL dst)


(* call_external_int32
   Calls an external function which is returning an int32
   (or like) value in the register EAX.  *)
let call_external_int32 pos v label args =
   let pos = string_pos "call_external_int32" pos in
   let size, insts = jump_external_int pos label args in
   let insts = Listbuf.add_list insts
      [POP (IL, Register ebp);
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size)));
       MOV (IL, Register v, Register eax)]
   in
      insts


(* call_external_int64
   Calls an external function which is returning an int64
   (or like) value in the register EDX:EAX.  *)
let call_external_int64 pos vhi vlo label args =
   let pos = string_pos "call_external_int64" pos in
   let size, insts = jump_external_int pos label args in
   let insts = Listbuf.add_list insts
      [POP (IL, Register ebp);
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size)));
       MOV (IL, Register vhi, Register edx);
       MOV (IL, Register vlo, Register eax)]
   in
      insts


(* call_external_float
   Calls an external function which returns a floating
   point value at the top of the FP stack.  *)
let call_external_float pos v label args =
   let pos = string_pos "call_external_float" pos in
   let size, insts = jump_external_float pos label args in
   (* Floating points are always stored as 80-bit registers *)
   let insts = Listbuf.add_list insts
      [POP (IL, Register ebp);
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int size)));
       FSTP (FT, FloatRegister v)]
   in
      insts


(* call_external_reserve_int32
   Calls an external function that is returning an int32
   (or like) value in the register EAX.
   Uses GC calling convention.  *)
let call_external_reserve_int32 pos rlabel vars v label args =
   let pos = string_pos "call_external_reserve_int32" pos in
   let insts = Listbuf.of_elt (RESP (ImmediateNumber (OffLabel rlabel), vars)) in
   let size, insts' = jump_external_int pos label args in
   let insts = Listbuf.add_listbuf insts insts' in
   let insts = Listbuf.add_list insts
      [POP (IL, Register ebp);
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int (size + res_call_size))));
       MOV (IL, Register v, Register eax)]
   in
      insts


(* call_external_reserve_int64
   Calls an external function which is returning an int64
   (or like) value in the register EDX:EAX.
   Uses the GC calling convention.  *)
let call_external_reserve_int64 pos rlabel vars vhi vlo label args =
   let pos = string_pos "call_external_reserve_int64" pos in
   let insts = Listbuf.of_elt (RESP (ImmediateNumber (OffLabel rlabel), vars)) in
   let size, insts' = jump_external_int pos label args in
   let insts = Listbuf.add_listbuf insts insts' in
   let insts = Listbuf.add_list insts
      [POP (IL, Register ebp);
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int (size + res_call_size))));
       MOV (IL, Register vhi, Register edx);
       MOV (IL, Register vlo, Register eax)]
   in
      insts


(* call_external_reserve_float
   Calls an external function which returns a floating
   point value at the top of the FP stack.
   Uses the GC calling convention.  *)
let call_external_reserve_float pos rlabel vars v label args =
   let pos = string_pos "call_external_float" pos in
   let insts = Listbuf.of_elt (RESP (ImmediateNumber (OffLabel rlabel), vars)) in
   let size, insts' = jump_external_float pos label args in
   (* Floating points are always stored as 80-bit registers *)
   let insts = Listbuf.add_listbuf insts insts' in
   let insts = Listbuf.add_list insts
      [POP (IL, Register ebp);
       ADD (IL, Register esp, ImmediateNumber (OffNumber (Int32.of_int (size + res_call_size))));
       FSTP (FT, FloatRegister v)]
   in
      insts


(***  Conditional Expressions  ***)


(* build_relop_int32
   Builds a 32-bit relational operator.  Note, the operands MUST
   be masked beforehand if they are 8- or 16-bit operands, this
   function will assume the high-order bits of such an operand
   are already cleared to zero or sign-extended as necessary.  *)
let build_relop_int32 cmp insts op op1 op2 label =
   let r = new_symbol_string "relop_tmp" in
      Listbuf.add_list insts
         [code_load_int32 r op1;
          cmp (Register r) op2;
          JCC (op, ImmediateLabel label)]


(* build_relop_int
   Builds a 31-bit relational operator.  Currently, this can
   be implemented with int32 operands.  *)
let build_relop_int = build_relop_int32


(* build_relop_int64
   Builds a 64-bit relational operator.  We need to compare the
   MSB first, if equality occurs there then we do an UNSIGNED
   compare on the LSB to determine the order of the two operands.
   This generates code which will ALWAYS terminate in an
   unconditional jump.  *)
let build_relop_int64 cmp insts op ohi1 olo1 ohi2 olo2 lsucc lfail =
   (* build_equality
      Build the cases for (in)equality, where we can check the
      MSB and LSB independently and (I might add) much more
      easily. *)
   let build_equality lfail lsucc =
      let insts = build_relop_int32 cmp insts NEQ ohi1 ohi2 lfail in
      let insts = build_relop_int32 cmp insts NEQ olo1 olo2 lfail in
         Listbuf.add insts (JMP (ImmediateLabel lsucc))
   in

   (* build_ordered
      This case is a bit harder, since we have to check the
      MSB first to make sure all is well there.  The first
      operand is the strict (signed/unsigned) operand used
      on the MSB, the second operand is the (strict/weak)
      unsigned operand used on the LSB. *)
   let build_ordered msbop lsbop =
      let insts = build_relop_int32 cmp insts msbop ohi1 ohi2 lsucc in
      let insts = build_relop_int32 cmp insts NEQ   ohi1 ohi2 lfail in
      let insts = build_relop_int32 cmp insts lsbop olo1 olo2 lsucc in
         Listbuf.add insts (JMP (ImmediateLabel lfail))
   in

   (* Figure out what to do *)
   match op with
      EQ    -> build_equality lfail lsucc
    | NEQ   -> build_equality lsucc lfail
    | ULT   -> build_ordered ULT ULT
    | ULE   -> build_ordered ULT ULE
    | UGT   -> build_ordered UGT UGT
    | UGE   -> build_ordered UGT UGE
    | LT    -> build_ordered LT  ULT
    | LE    -> build_ordered LT  ULE
    | GT    -> build_ordered GT  UGT
    | GE    -> build_ordered GT  UGE


(* The constants are in Intel V1, page 7-36 *)
let fucom_lt_set   = Int32.of_int 0x0100
let fucom_eq_set   = Int32.of_int 0x4000
let fucom_gt_unset = Int32.of_int 0x4100


(* build_relop_float
   Builds a floating-point comparison operator.  The operands
   should be provided in FloatRegister data or a spilled register
   in memory, so they can be properly loaded.  We jump to label
   on success, otherwise we fall through.  *)
let build_relop_float insts op op1 op2 label =
   let insts = Listbuf.add_list insts
      [FLD (FT, op2);   (* this is put in %st(1) *)
       FLD (FT, op1);   (* this is put in %st(0) *)
       FUCOMPP;         (* unordered compare, pop stack twice *)
       FSTSW]           (* eax now contains the control register *)
   in
   let lt_set   = ImmediateNumber (OffNumber fucom_lt_set) in
   let eq_set   = ImmediateNumber (OffNumber fucom_eq_set) in
   let gt_unset = ImmediateNumber (OffNumber fucom_gt_unset) in
   let label = ImmediateLabel label in
   let eax = Register eax in
      match op with
         EQ       -> Listbuf.add_list insts [TEST (IL, eax, eq_set);   JCC (NEQ, label)]
       | NEQ      -> Listbuf.add_list insts [TEST (IL, eax, eq_set);   JCC (EQ,  label)]
       | LT | ULT -> Listbuf.add_list insts [TEST (IL, eax, lt_set);   JCC (NEQ, label)]
       | GE | UGE -> Listbuf.add_list insts [TEST (IL, eax, lt_set);   JCC (EQ,  label)]
       | GT | UGT -> Listbuf.add_list insts [TEST (IL, eax, gt_unset); JCC (EQ,  label)]
       | LE | ULE -> Listbuf.add_list insts [TEST (IL, eax, gt_unset); JCC (NEQ, label)]


(* Build a SHR op.
   The shift may be negative.  *)
let build_shrop_int op shift =
   if shift < 0 then
      SHL (IL, op, ImmediateNumber (OffNumber (Int32.of_int (-shift))))
   else
      SHR (IL, op, ImmediateNumber (OffNumber (Int32.of_int shift)))
