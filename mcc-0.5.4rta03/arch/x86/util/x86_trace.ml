(*
 * Construct a block trace.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Symbol
open Location

open Frame_type

open Mir_exn
open Mir_pos

open X86_inst_type
open X86_frame_type
open X86_frame
open X86_util
open X86_util
open X86_build

module Pos = MakePos (struct let name = "X86_trace" end)
open Pos

(************************************************************************
 * SUBSTITUTION AND CODE MOTION
 ************************************************************************)

module type SubstSig =
sig
   type subst

   exception Cycle

   val empty : subst
   val lookup : subst -> label -> label
   val find : subst -> label -> label * inst list
   val add : subst -> label -> label -> inst list -> subst
   val fold : ('a -> label -> (label * inst list) -> 'a) -> 'a -> subst -> 'a
end

module Subst : SubstSig =
struct
   type subst = (label * inst list) SymbolTable.t

   exception Cycle

   (*
    * Empty table.
    *)
   let empty = SymbolTable.empty

   (*
    * Get the value.
    *)
   let find table label =
      try
         SymbolTable.find table label
      with
         Not_found ->
            raise (Failure ("X86_trace.find, cannot locate " ^ (string_of_symbol label)))

   (*
    * Apply the label substitution.
    *)
   let rec lookup table label =
      try
         let label', _ = SymbolTable.find table label in
            lookup table label'
      with
         Not_found ->
            label

   (*
    * Add a substitution.
    * Refuse to add cycles.
    *)
   let add table label label' code =
      let label'' = lookup table label' in
         if label'' = label then
            raise Cycle
         else
            SymbolTable.add table label (label', code)

   (*
    * Interation.
    *)
   let fold = SymbolTable.fold
end

(*
 * Instruction substitutions.
 *)
let subst_operand table operand =
   match operand with
      ImmediateLabel label ->
         ImmediateLabel (Subst.lookup table label)
    | _ ->
         operand

let subst_inst_labels table inst =
   map_operands (subst_operand table) inst

let subst_block_label table block =
   let { block_code = code;
         block_jumps = jumps
       } = block
   in
      { block with block_code = List.map (subst_inst_labels table) code;
                   block_jumps = List.map (Subst.lookup table) jumps
      }

let subst_block_labels table blocks =
   List.map (subst_block_label table) blocks

(*
 * Flatten a table.
 *)
let flatten_table table =
   Subst.fold (fun ftable label (label', com) ->
         Subst.add ftable label (Subst.lookup table label') com) Subst.empty table

(*
 * Migrate code from the caller to the callee.
 * For this, we invert the flattened table to collect
 * all the comments.
 *)
let invert_table table =
   Subst.fold (fun ctable _ (label, code) ->
         let code =
            try
               let code' = SymbolTable.find ctable label in
                  code' @ code
            with
               Not_found ->
                  code
         in
            SymbolTable.add ctable label code) SymbolTable.empty table

let migrate_down table blocks =
   let table = flatten_table table in
   let ctable = invert_table table in
   let blocks =
      List.map (fun block ->
            try
               let code = SymbolTable.find ctable block.block_label in
                  { block with block_code = code @ block.block_code }
            with
               Not_found ->
                  block) blocks
   in
      subst_block_labels table blocks

(************************************************************************
 * CHECKING
 ************************************************************************)

(*
 * Check that block labels are unique.
 *)
let check_block_labels blocks =
   let add_block table block =
      let v = block.block_label in
         if SymbolSet.mem table v then
            raise (MirException (var_exp_pos v, StringVarError ("duplicate block", v)));
         SymbolSet.add table v
   in
      ignore (List.fold_left add_block SymbolSet.empty blocks)

(************************************************************************
 * TRIVIAL JUMP ELIMINATION
 ************************************************************************)

(*
 * Get the effective last instruction.
 *)
let is_trivial_inst = function
   CommentFIR _
 | CommentMIR _
 | CommentString _
 | NOP ->
      true
 | _ ->
      false

let rec effective_code code =
   match code with
      inst :: insts ->
         if is_trivial_inst inst then
            let com, insts = effective_code insts in
               inst :: com, insts
         else
            [], code
    | _ ->
         [], code

(*
 * Check if a block contains a trivial jump.
 *)
let block_trivial_jump globals { block_label = label; block_code = code } =
   if SymbolSet.mem globals label then
      None
   else
      match effective_code code with
         com, [JMP (ImmediateLabel label)] ->
            Some (com, label)
       | _ ->
            None

(*
 * Eliminate blocks that contain a single jump.
 *)
let prune_trivial_jumps globals blocks =
   let collect (table, blocks') block =
      match block_trivial_jump globals block with
         Some (com, label) ->
            (try
                let table = Subst.add table block.block_label label com in
                   table, blocks'
             with
                Subst.Cycle ->
                   table, block :: blocks')
       | None ->
            table, block :: blocks'
   in
   let table, blocks = Trace.fold collect (Subst.empty, []) blocks in
      migrate_down table (List.rev blocks)

(************************************************************************
 * JUMP REMOVAL
 ************************************************************************)

(*
 * Remove the trailing jump for blocks that are contiguous.
 * If the jump is conditional, the condition may have to be
 * reordered to remove the jump.
 *)
let prune_block_jump block label =
   let rec prune code =
      match code with
         [JCC (cc, ImmediateLabel l1) as inst1;
          JMP (ImmediateLabel l2)] ->
            if l1 = label then
               [JCC (invert_cc cc, ImmediateLabel l2); FJMP (ImmediateLabel label)]
            else if l2 = label then
               [inst1; FJMP (ImmediateLabel label)]
            else
               code
       | [JMP (ImmediateLabel l1)] ->
            if l1 = label then
               [FJMP (ImmediateLabel label)]
            else
               code
       | []
       | [_] ->
            code
       | inst :: code ->
            inst :: prune code
   in
      { block with block_code = prune block.block_code }

(*
 * Rewrite the FJMP back to a JMP.
 *)
let rewrite_block_fjump blocks =
   let rewrite_fjump inst =
      match inst with
         FJMP label ->
            JMP label
       | _ ->
            inst
   in
      List.map (fun block -> { block with block_code = List.map rewrite_fjump block.block_code }) blocks

(*
 * Remove trailing jumps from adjacent blocks.
 * This is a simple optimization that removes the trailing JMP
 * instruction from blocks when the JMP is to the block that
 * immediately follows.  Once this optimization is performed,
 * the blocks must NOT be reordered.
 *)
let rec prune_block_jumps blocks =
   match blocks with
      block1 :: ((block2 :: _) as blocks) ->
         let label2 = block2.block_label in
            if block2.block_index = None && List.mem label2 block1.block_jumps then
               let block1 = prune_block_jump block1 label2 in
                  block1 :: prune_block_jumps blocks
            else
               block1 :: prune_block_jumps blocks
    | _ ->
         blocks

(*
 * Remove code following an unconditional jump or return in a block
 *)
let rec prune_code_after_jump code =
   match code with
      (JMP _ as inst) :: _
    | (RET as inst) :: _ ->
         [inst]
    | [FJMP _] ->
         code
    | FJMP _ :: insts ->
         prune_code_after_jump insts
    | inst :: insts ->
         inst :: prune_code_after_jump insts
    | [] ->
         []

let prune_code_after_jump block =
   let code = block.block_code in
   let code = prune_code_after_jump code in
      { block with block_code = code }

let prune_code_after_jumps blocks =
   List.map prune_code_after_jump blocks

(************************************************************************
 * BLOCK COALESCE
 ************************************************************************)

(*
 * Add the free labels to the set.
 *)
let add_labels set op =
   SymbolSet.add_list set (dst_labels op [])

(*
 * If a block's label is not referenced anywhere in the
 * program (except in a preceding block), then merge the
 * block with its parent.
 *)
let prune_trivial_labels globals blocks =
   (* Get the free labels *)
   let free =
      List.fold_left (fun free block ->
         List.fold_left (fun free code ->
            match code with
               FJMP _ ->
                  free
             | _ ->
                  fold_operands add_labels free code) free block.block_code) globals blocks
   in

   (* This helper returns true if we find any jumps to destinations
      other than the named label, or if we find any conditional jump
      (which implies conditional flow-control inside the block).  *)
   let rec find_jump label = function
      JCC _ :: _ ->
         true
    | JMP (ImmediateLabel label') :: _ ->
         not (Symbol.eq label label')
    | _ :: insts ->
         find_jump label insts
    | [] ->
         false
   in

   (* Now migrate code *)
   let rec collect = function
      block1 :: ((block2 :: blocks1) as blocks2) ->
         let label2 = block2.block_label in
            if SymbolSet.mem free label2 then
               block1 :: collect blocks2
            else
               (* Determine block optimizations.  Remember the block_opt flag
                  is false on blocks that are outside the normal execution
                  path, therefore we need to determine if the merged block
                  is still *completely* on a non-normal execution path... *)
               let opt =
                  match block2.block_opt with
                     true ->
                        (* If the tail block is set to optimize, then
                           we will always optimize the merged block. *)
                        true
                   | false ->
                        (* If the tail block is set to unoptimized, then
                           we will only optimize the merged block if we
                           find conditional jumps in the first block; if
                           not, then we assume this is not a common exec
                           path and will not optimize the merged block. *)
                        find_jump label2 block1.block_code
               in
               let inst = CommentString ("Label " ^ (Symbol.string_of_symbol label2) ^ " removed") in
               let jumps = Mc_list_util.remove label2 block1.block_jumps in
               let jumps = Mc_list_util.union block2.block_jumps jumps in
               let code = block1.block_code @ (inst :: block2.block_code) in
               let block1 =
                  { block1 with block_code  = code;
                                block_jumps = jumps;
                                block_opt   = opt;
                  }
               in
                  collect (block1 :: blocks1)
    | blocks ->
         blocks
   in
      collect blocks

(************************************************************************
 * BLOCK ALIGNMENTS
 ************************************************************************)

(*
 * Signal that blocks that cannot be fallen into (must be explicitly
 * jumped to) start a new instruction area.
 *)
let rec start_blocks prev blocks =
   let rec get_last_inst = function
      inst :: insts when is_trivial_inst inst ->
         get_last_inst insts
    | [] ->
         None
    | inst :: insts ->
         match get_last_inst insts with
            None ->
               Some inst
          | Some inst ->
               Some inst
   in
   let can_align_next_block block =
      match get_last_inst block.block_code with
         Some (JMP _)
       | Some (IJMP _)
       | Some RET ->
            true
       | _ ->
            false
   in
      match blocks with
         block :: blocks ->
            let block =
               if prev then
                  { block with block_start = true }
               else
                  block
            in
            let prev = can_align_next_block block in
               block :: start_blocks prev blocks
       | [] ->
            []

let start_blocks = start_blocks true

(************************************************************************
 * TRACE
 ************************************************************************)

(*
 * Produce the trace from the block list.
 * Create a fake root block that contains
 * branches to all the top-level functions,
 * and defines esp, ebp.
 *)
let imm_zero = ImmediateNumber (OffNumber (Int32.of_int 0x55555555))
let root_name = new_symbol_string "root_block"
let root_loc = bogus_loc "<X86_trace>"

let build_trace imports exports globals blocks =
   (* Collect all the block names *)
   let blocks = Trace.to_list blocks in
   let jumps =
      List.fold_left (fun names { block_label = name } ->
            SymbolSet.add names name) SymbolSet.empty blocks
   in

   (* Remove all the blocks that are referenced somewhere *)
   let jumps =
      List.fold_left (fun names { block_jumps = jumps } ->
            List.fold_left SymbolSet.remove names jumps) jumps blocks
   in
   let jumps = SymbolSet.to_list jumps in

   (* Some default code that pretends to initialize registers *)
   let code =
      List.fold_left (fun code label ->
            JCC (UGT, ImmediateLabel label) :: code) [] jumps
   in
   let code =
      SymbolTable.fold (fun code v { import_info = info } ->
            match info with
               ImportGlobal ->
                  MOV (IL, Register v, imm_zero) :: code
             | ImportFun _ ->
                  code) code imports
   in
   let code =
      SymbolTable.fold (fun code v { export_is_fun = funp } ->
            if funp then
               code
            else
               MOV (IL, Register v, imm_zero) :: code) code exports
   in
   let code =
      List.fold_left (fun code v ->
            MOV (IL, Register v, imm_zero) :: code) code registers.(int_reg_class)
   in
   let root =
      { block_debug = root_loc, [];
        block_label = root_name;
        block_code  = code;
        block_jumps = jumps;
        block_index = None;
        block_align = false;
        block_start = false;
        block_arity = None;
        block_opt   = false;
      }
   in
   let loop =
      Loop.create "X86_trace"
         (fun block -> block.block_label)
         (fun block -> block.block_jumps)
         root blocks
   in
      List.tl (Loop.loop_nest loop (fun block -> block.block_label))

(************************************************************************
 * GLOBAL
 ************************************************************************)

(*
 * Build table of global blocks.
 *)
let build_globals prog =
   let { asm_export = export;
         asm_preserve = preserve;
         asm_closures = closures;
         asm_init = init
       } = prog
   in

   (* Collect all the escaping functions *)
   let globals =
      List.fold_left SymbolSet.add SymbolSet.empty closures
   in

   (* Collect all the exported vars *)
   let globals =
      SymbolTable.fold (fun globals v { export_is_fun = funp } ->
            if funp then
               SymbolSet.add globals v
            else
               globals) globals export
   in

   (* Collect all blocks that must be preserved *)
   let globals = SymbolSet.union globals preserve in

   (* Collect all initialization blocks *)
   let globals =
      List.fold_left (fun globals (_, init_sym, main_sym) ->
            let globals = SymbolSet.add globals init_sym in
            let globals = SymbolSet.add globals main_sym in
               globals) globals init
   in
      globals

(*
 * Sort the blocks and remove redundant branches.
 * Do _not_ prune trivial labels before register
 * allocation because this will make live ranges
 * too long.
 *)
let before_ra prog =
   let { asm_import = import;
         asm_export = export;
         asm_blocks = blocks
       } = prog
   in

   (* Phase 1: build the trace *)
   let globals = build_globals prog in
   let blocks = build_trace import export globals blocks in

   (* Phase 2: optimize slightly *)
   let globals =
      List.fold_left (fun globals block ->
            SymbolSet.add globals block.block_label) globals (Trace.special_nodes blocks)
   in
   let blocks = prune_trivial_jumps globals blocks in
   let blocks = prune_block_jumps blocks in
   let blocks = prune_trivial_labels globals blocks in
   let blocks = prune_code_after_jumps blocks in
   let blocks = rewrite_block_fjump blocks in

   (* Phase 3: rebuild the trace *)
   let blocks = Trace.of_list blocks in
   let blocks = build_trace import export globals blocks in
      { prog with asm_blocks = blocks }

(*
 * After register allocation, there is no need to reconstruct
 * the trace.  However, there may be additional empty blocks
 * because MOV instructions have been deleted.
 *)
let after_ra prog =
   let blocks = prog.asm_blocks in
   let globals = build_globals prog in

   (* Optimize *)
   let _ =
      if debug Fir_state.debug_print_asm then
         let prog = { prog with asm_blocks = blocks  } in
            X86_print.debug_prog "*** X86_trace: after_ra begin" prog
   in
   let blocks = prune_trivial_jumps globals blocks in
   let blocks = prune_block_jumps blocks in
   let blocks = prune_trivial_labels globals blocks in
   let blocks = prune_code_after_jumps blocks in
   let blocks = start_blocks blocks in
   let _ = check_block_labels blocks in
   let blocks = Trace.of_list blocks in
   let _ =
      if debug Fir_state.debug_print_asm then
         let prog = { prog with asm_blocks = blocks  } in
            X86_print.debug_prog "*** X86_trace: after_ra done" prog
   in
      { prog with asm_blocks = blocks }

let before_ra = Fir_state.profile "X86_trace.before_ra" before_ra
let after_ra  = Fir_state.profile "X86_trace.after_ra"  after_ra

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
