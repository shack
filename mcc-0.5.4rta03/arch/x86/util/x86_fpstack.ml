(*
   Rewrite FP pseudo-registers to use real stack registers
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(*
   NOTE:  this code assumes no transients are atop the
   stack upon entering any block, i.e. on entering the
   block the stack ONLY contains the allocated registers
   fp0..fp5.
 *)


(* Useful junk *)
open Format
open Symbol
open Frame_type
open X86_exn
open X86_inst_type
open X86_frame_type
open X86_frame
open X86_util


(***  Utilities  ***)


(* fpclassify
   Classifies this instruction.  Only used internally, see the
   next two functions for a general (and better) overview. *)
let fpclassify = function
   FLD      _
 | FILD     _
 | FILD64   _
 | FMOV     _
 | FCALL    _ ->
      true, 1

   (* Normal store operations *)
 | FSTP     _
 | FISTP    _
 | FISTP64  _ ->
      true, -1

   (* Arithmetic operations *)
 | FADDP
 | FSUBP
 | FMULP
 | FDIVP
 | FSUBRP
 | FDIVRP ->
      true, -1
 | FCHS
 | FADD  _
 | FSUB  _
 | FMUL  _
 | FDIV  _
 | FSUBR _
 | FDIVR _
 | FPREM
 | FSIN
 | FCOS
 | FPATAN
 | FSQRT ->
      true, 0

   (* Comparison pops nil, once, or twice *)
 | FUCOM ->
      true, 0
 | FUCOMP ->
      true, -1
 | FUCOMPP ->
      true, -2

   (* Misc. floating-point ops *)
 | FST   _
 | FIST  _
 | FMOVP _
 | FXCH  _ ->
      true, 0

   (* FSTSW doesn't use the stack *)
 | FINIT
 | FSTCW _
 | FLDCW _
 | FSTSW ->
      false, 0

   (* Non-FP operations *)
 | MOV   _
 | MOVS  _
 | MOVZ  _
 | MOVD  _
 | MOVQ  _
 | RMOVS _
 | LEA   _
 | NOT   _
 | NEG   _
 | INC   _
 | DEC   _
 | ADD   _
 | ADC   _
 | SUB   _
 | SBB   _
 | MUL   _
 | IMUL  _
 | DIV   _
 | IDIV  _
 | CLTD
 | AND   _
 | OR    _
 | XOR   _
 | SAL   _
 | SAR   _
 | SHL   _
 | SHR   _
 | SHLD  _
 | SHRD  _
 | PUSH  _
 | POP   _
 | CALL  _
 | RET
 | CMP   _
 | TEST  _
 | JMP   _
 | FJMP  _
 | IJMP  _
 | JCC   _
 | SET   _
 | RES   _
 | COW   _
 | RESP  _
 | CommentFIR _
 | CommentMIR _
 | CommentInst _
 | CommentString _
 | NOP ->
      false, 0


(* inst_uses_fpstack
   Returns true if the given instruction uses registers from the
   floating-point stack (either explicitly or implicitly).  These
   instructions cannot be so easily moved around.  *)
let inst_uses_fpstack inst =
   let fpstack, _ = fpclassify inst in
      fpstack


(* fpstack_tos_delta
   This instruction returns the change in the TOS pointer after
   executing the named floating-point instruction.  If the value
   is zero, no change is made; if positive, values are pushed on
   to the stack; if negative, the stack is popped.  *)
let fpstack_tos_delta inst =
   let _, delta = fpclassify inst in
      delta


(***  Rewrite FP Pseudo-regs as FPStack  ***)


(* rewrite_operand
   Rewrites the stack pseudo-registers with real FPStack
   operands that are adjusted to consider temporary values
   pushed onto the top of the stack.
 *)
let rewrite_operand tos op =
   match op with
      FloatRegister r ->
         let sym_eq (reg, _) = Symbol.eq reg r in
         if List.exists sym_eq fpmap then
            let index = Int32.of_int (tos + (snd (List.find sym_eq fpmap))) in
               FPStack index
         else
            op
    | ImmediateNumber _
    | ImmediateLabel _
    | Register _
    | MMXRegister _
    | FPStack _
    | MemReg _
    | MemRegOff _
    | MemRegRegOffMul _
    | SpillRegister _ ->
         op


(* rewrite_inst
   Rewrite operands in an instruction.  This function
   returns the revised instruction as well as the new
   TOS, or new number of temporaries sitting at the top
   of the floating-point stack after this operation. *)
let rec rewrite_insts tos insts =
   let modify_tos delta tos insts =
      if delta <> 0 then
         let tos = tos + delta in
         let inst = CommentString (sprintf "Modified TOS: now %d" tos) in
            inst :: rewrite_insts tos insts
      else
         rewrite_insts tos insts
   in
   match insts with
      [] ->
         []
    | inst :: insts ->
         let inst = map_operands (rewrite_operand tos) inst in
         let delta = fpstack_tos_delta inst in
            inst :: modify_tos delta tos insts


(* rewrite_block
   Rewrites a block of instructions.  The block is assumed to
   start out with tos = 0, or no temporary values on the floating
   point stack (only the permanent values). *)
let rewrite_block block =
   let insts = block.block_code in
   let insts = rewrite_insts 0 insts in
      { block with block_code = insts }


(* expand_fmov_insts
   This function expands out FMOV and FMOVP instructions into
   FLD and FST/FSTP instructions.  This is important to run
   /before/ we rewrite since we currently assume the TOS ptr
   is revised at most once per instruction in the above code. *)
let rec expand_fmov_insts = function
   FMOVP (dpre, dst, spre, src) :: insts ->
      FLD (spre, src) :: FSTP (dpre, dst) :: expand_fmov_insts insts
 | FMOV (dpre, dst, spre, src) :: insts ->
      FLD (spre, src) :: FST (dpre, dst) :: expand_fmov_insts insts
 | inst :: insts ->
      inst :: expand_fmov_insts insts
 | [] ->
      []


(* expand_fmov_block
   See above, this works on a block at a time.  *)
let expand_fmov_block block =
   let insts = block.block_code in
   let insts = expand_fmov_insts insts in
      { block with block_code = insts }


(* rewrite_prog
   Rewrites the entire program. *)
let rewrite_prog prog =
   let blocks = prog.asm_blocks in
   let blocks = Trace.map expand_fmov_block blocks in
   let blocks = Trace.map rewrite_block blocks in
      { prog with asm_blocks = blocks }

let rewrite_prog = Fir_state.profile "X86_fpstack.rewrite_prog" rewrite_prog
