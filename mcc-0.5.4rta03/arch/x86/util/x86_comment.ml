(*
   Filter comments out of assembly code
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Flags

open X86_frame_type
open X86_frame


let rec filter_comments insts =
   match insts with
      inst :: insts ->
         let insts = filter_comments insts in
            if X86_arch.trivial_inst inst then
               insts
            else
               inst :: insts
    | [] ->
         []


let filter_comments blocks =
   Trace.map (fun block ->
      let insts = block.block_code in
      let insts = filter_comments insts in
      let block = { block with block_code = insts } in
         block) blocks


let filter_comments prog =
   if std_flags_get_bool "opt.x86.process_comments" then
      prog
   else
      { prog with asm_blocks = filter_comments prog.asm_blocks }


let () = std_flags_register_list_help "opt.x86"
   ["opt.x86.process_comments",  FlagBool false,
                                 "Determines whether comments are processed by the backend." ^
                                 " If this flag is false, then comments will be stripped from" ^
                                 " the instruction stream; this radically speeds up parts of" ^
                                 " the backend, but makes the backend code much more difficult" ^
                                 " to debug.  Set to false with caution."]
