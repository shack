(*
 * Substitition is used to rewrite the code after register allocation.
 *
 * Here are the naming conventions: each form has a code that specifies
 * what kind of operand is wanted.  For example, subst_XX_src makes sure
 * the src has form XX, where XX is one of:
 *    reg: an int32 register
 *    rm: a register or memory operand
 *    fprm: a float register or memory location
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Trace

open X86_inst_type
open X86_frame_type
open X86_frame
open X86_util

(*
 * Check if two operands are the same.
 * This is used primarilty to determine when
 * MOV instructions can be deleted.
 *)
let equal_operands spset op1 op2 =
   match op1, op2 with
      ImmediateNumber i1, ImmediateNumber i2 -> i1 = i2
    | ImmediateLabel l1, ImmediateLabel l2 -> l1 = l2
    | FPStack i1, FPStack i2 -> i1 = i2
    | Register r1, Register r2 ->
         spset_lookup_int32 spset r1 = spset_lookup_int32 spset r2
    | FloatRegister r1, FloatRegister r2 ->
         spset_lookup_float spset r1 = spset_lookup_float spset r2
    | MMXRegister r1, MMXRegister r2 ->
         spset_lookup_mmx spset r1 = spset_lookup_mmx spset r2
    | MemReg r1, MemReg r2 ->
         spset_lookup_int32 spset r1 = spset_lookup_int32 spset r2
    | MemRegOff (r1, i1), MemRegOff (r2, i2) ->
         i1 = i2 && spset_lookup_int32 spset r1 = spset_lookup_int32 spset r2
    | MemRegRegOffMul (r11, r12, i11, i12), MemRegRegOffMul (r21, r22, i21, i22) ->
         (i11 = i21)
         && (i12 = i22)
         && (spset_lookup_int32 spset r11 = spset_lookup_int32 spset r21)
         && (spset_lookup_int32 spset r12 = spset_lookup_int32 spset r22)
    | SpillRegister (_, off1), SpillRegister (_, off2) ->
         off1 = off2
    | _ ->
         false

(*
 * "Expand" an operand.
 * This means resolve the outermost register operator.
 *)
let expand_operand spset op =
   match op with
      Register v ->
         spset_lookup_int32 spset v
    | MMXRegister v ->
         spset_lookup_mmx spset v
    | FloatRegister v ->
         spset_lookup_float spset v
    | op ->
         op

(************************************************
 * SRC SUBSTITUTION
 *)

(*
 * Load the src operand into a register.
 *)
let subst_reg_src spset op cont =
   match op with
      Register v ->
         cont v
    | FloatRegister _
    | FPStack _ ->
         raise (Invalid_argument "subst_reg_src")
    | MMXRegister _ ->
         let tmp = new_symbol_string "subst_tmp" in
         let inst = MOVD (Register tmp, op) in
            inst :: cont tmp
    | op ->
         let tmp = new_symbol_string "subst_tmp" in
         let inst = MOV (IL, Register tmp, op) in
            inst :: cont tmp

(*
 * This is the most complicated operand.
 * Rewrite it so that the parts are normal registers.
 *)
let subst_mem_reg_reg_off_mul_src spset r1 r2 i1 i2 cont =
   subst_reg_src spset (spset_lookup_int32 spset r1) (fun r1 ->
   subst_reg_src spset (spset_lookup_int32 spset r2) (fun r2 ->
         cont (MemRegRegOffMul (r1, r2, i1, i2))))

(*
 * Substitute for an integer operand.
 *)
let subst_rm_src spset op cont =
   let op = expand_operand spset op in
      match op with
         ImmediateNumber _
       | ImmediateLabel _
       | Register _
       | SpillRegister _ ->
            cont op
       | MMXRegister v ->
            let tmp = Register (new_symbol_string "subst_tmp") in
            let inst = MOVD (tmp, op) in
               inst :: cont tmp
       | MemReg r ->
            subst_reg_src spset (spset_lookup_int32 spset r) (fun r -> cont (MemReg r))
       | MemRegOff (r, i) ->
            subst_reg_src spset (spset_lookup_int32 spset r) (fun r -> cont (MemRegOff (r, i)))
       | MemRegRegOffMul (r1, r2, i1, i2) ->
            subst_mem_reg_reg_off_mul_src spset r1 r2 i1 i2 cont
       | FloatRegister _
       | FPStack _ ->
            raise (Invalid_argument "subst_rm_src: float register bogus here")

(*
 * Float operands.
 *)
let subst_fprm_src spset op cont =
   let op = expand_operand spset op in
      match op with
         ImmediateNumber _
       | ImmediateLabel _
       | FPStack _
       | Register _
       | FloatRegister _
       | SpillRegister _ ->
            cont op
       | MemReg r ->
            subst_reg_src spset (spset_lookup_int32 spset r) (fun r -> cont (MemReg r))
       | MemRegOff (r, i) ->
            subst_reg_src spset (spset_lookup_int32 spset r) (fun r -> cont (MemRegOff (r, i)))
       | MemRegRegOffMul (r1, r2, i1, i2) ->
            subst_mem_reg_reg_off_mul_src spset r1 r2 i1 i2 cont
       | MMXRegister _ ->
            raise (Invalid_argument "subst_fprm_src")

(************************************************
 * DST SUBSTITUTION
 *)

(*
 * Dest must be a register.
 *)
let subst_rm_dst spset op cont =
   match expand_operand spset op with
      MMXRegister _ ->
         cont (Register (new_symbol_string "subst_tmp"))
    | op ->
         cont op

(*
 * Write-back the operand.
 * The current value is in v, and the result
 * should be placed in op.
 *)
let subst_rm_store spset op1 op2 cont =
   let op1 = expand_operand spset op1 in
      if op1 = op2 then
         cont
      else
         match op1 with
            MMXRegister _ ->
               let inst = MOVD (op1, op2) in
                  inst :: cont
          | _ ->
               let inst = MOV (IL, op1, op2) in
                  inst :: cont

(************************************************
 * GENERIC INSTRUCTION SUBSTITUTION
 *)

(*
 * Substitution for a unary src-only instruction.
 * This is for instructions like MUL, where the side-effect
 * is to a well-known location.
 *)
let subst_rm_src1 spset inst src cont =
   subst_rm_src spset src (fun src ->
         inst src :: cont)

(*
 * Substitution for a unary dst-only combination.
 *)
let subst_rm_dst1 spset inst dst cont =
   subst_rm_dst spset dst (fun dst' ->
         inst dst' :: subst_rm_store spset dst dst' cont)

(*
 * Substition for a unary dst/src combination.
 *)
let subst_rm_dst_src1 spset inst dst cont =
   subst_rm_src spset dst (fun src ->
         inst src :: subst_rm_store spset dst src cont)

(*
 * Replace the operands in a two-address src-only instruction.
 * Some operand combinations are not allowed.
 *)
let subst_rm_src_src2 spset inst src0 src1 cont =
   subst_rm_src spset src0 (fun src0 ->
   subst_rm_src spset src1 (fun src1 ->
         match src0, src1 with
            Register _, _
          | _, Register _
          | _, ImmediateNumber _ ->
               inst src0 src1 :: cont
          | _ ->
               let tmp = Register (new_symbol_string "subst_tmp") in
               let inst1 = MOV (IL, tmp, src1) in
                  inst1 :: inst src0 tmp :: cont))

(*
 * Replace the operands in a three-address src-only instruction.
 * Some operand combinations are not allowed.
 *)
let subst_rm_src_src_src3 spset inst src0 src1 src2 cont =
   subst_rm_src spset src0 (fun src0 ->
   subst_rm_src spset src1 (fun src1 ->
   subst_rm_src spset src2 (fun src2 ->
         match src0, src1 with
            Register _, _
          | _, Register _
          | _, ImmediateNumber _ ->
               inst src0 src1 src2 :: cont
          | _ ->
               let tmp = Register (new_symbol_string "subst_tmp") in
               let inst1 = MOV (IL, tmp, src1) in
                  inst1 :: inst src0 tmp src2 :: cont)))

(*
 * Replace the operands in a two-address dst/src instruction.
 * Some operand combinations are not allowed.
 *)
let subst_rm_dst_src2 spset inst dst src cont =
   subst_rm_src spset dst (fun src0 ->
   subst_rm_src spset src (fun src1 ->
         match src0, src1 with
            Register _, _
          | _, Register _
          | _, ImmediateNumber _ ->
               inst src0 src1 :: subst_rm_store spset dst src0 cont
          | _ ->
               let tmp = Register (new_symbol_string "subst_tmp") in
               let inst1 = MOV (IL, tmp, src1) in
                  inst1 :: inst src0 tmp :: cont))

(*
 * Three-operand register instructions.  This is used by
 * SHLD/SHRD.  Note that these instructions REQUIRE a register
 * in the second operand.
 *)
let subst_rm_dst_src_src3 spset inst dst src1 src2 cont =
   subst_rm_src spset dst  (fun src0 ->
   subst_rm_src spset src1 (fun src1 ->
   subst_rm_src spset src2 (fun src2 ->
         match src1 with
            Register _ ->
               inst src0 src1 src2 :: subst_rm_store spset dst src0 cont
          | _ ->
               let tmp = Register (new_symbol_string "subst_tmp") in
               let inst1 = MOV (IL, tmp, src1) in
                  inst1 :: inst src0 tmp src2 :: cont)))


(*
 * Be careful about IMUL and MOVZ/MOVS, the destination
 * must always be a register.  This function codes the
 * destination as a plain registers.
 *)
let subst_reg_dst_src2 spset inst dst src cont =
   subst_rm_src spset dst (fun src1 ->
   subst_rm_src spset src (fun src2 ->
         match src1 with
            Register _ ->
               inst src1 src2 :: subst_rm_store spset dst src1 cont
          | _ ->
               let tmp = Register (new_symbol_string "subst_tmp") in
               let inst1 = MOV (IL, tmp, src1) in
                  inst1 :: inst tmp src2 :: subst_rm_store spset dst tmp cont))

(*
 * Be careful about LEA, it should not become a MOV.
 *)
let subst_lea spset inst dst src cont =
   subst_rm_src spset dst (fun src1 ->
   subst_rm_src spset src (fun src2 ->
         match src1, src2 with
            Register v, _ ->
               inst src1 src2 :: subst_rm_store spset dst src1 cont
          | _ ->
               (* JDS TEMP: This code is bogus
               let tmp = new_symbol_string "subst_tmp" in
               let inst1 = LEA (IL, Register tmp, src) in
                  inst1 :: inst src1 (MemReg tmp) :: cont))
                *)
               let tmp = Register (new_symbol_string "subst_tmp") in
                  inst tmp src2 :: subst_rm_store spset dst tmp cont))

(*
 * Replace the operands in a two-address dst/src instruction.
 * Some operand combinations are not allowed.
 *)
let subst_rm_mov spset pre dst src cont =
   let dst = expand_operand spset dst in
   let src = expand_operand spset src in
      match dst, src with
         MMXRegister _, MMXRegister _ ->
            MOVQ (dst, src) :: cont
       | MMXRegister _, _ ->
            subst_rm_src spset src (fun src ->
                  MOVD (dst, src) :: cont)
       | _, MMXRegister _ ->
            subst_rm_src spset dst (fun dst ->
                  MOVD (dst, src) :: cont)
       | Register _, _ ->
            subst_rm_src spset src (fun src ->
                  MOV (pre, dst, src) :: cont)
       | _, Register _
       | _, ImmediateNumber _ ->
            subst_rm_src spset dst (fun dst ->
                  MOV (pre, dst, src) :: cont)
       | _ ->
            let tmp =
               match pre with
                  IW
                | IL ->
                     (* Can use any register here *)
                     Register (new_symbol_string "subst_tmp")
                | IB ->
                     (* ESI/EDI cannot be used for byte-moves, therefore
                        we must force use of a standard register here. *)
                     raise (Failure "subst_rm_mov:  Unable to safely introduce byte temporaries with current allocator")
            in
               subst_rm_src spset dst (fun dst ->
               subst_rm_src spset src (fun src ->
                     MOV (pre, tmp, src) :: MOV (pre, dst, tmp) :: cont))

(*
 * Reserve.
 *)
let spset_register spset r =
   match spset_lookup_int32 spset r with
      Register r ->
         r
    | _ ->
         raise (Invalid_argument "spset_register")

let spset_reserve_operand spset op =
   match op with
      Register r ->
         spset_lookup_int32 spset r
    | _ ->
         op

let rec subst_reserve spset = function
   src :: srcs ->
      let srcs = subst_reserve spset srcs in
         (match src with
             ResBase (Register r) ->
                ResBase (spset_lookup_int32 spset r) :: srcs
           | ResBase (MemReg r) ->
                ResBase (MemReg (spset_register spset r)) :: srcs
           | ResBase (MemRegOff (r, i)) ->
                ResBase (MemRegOff (spset_register spset r, i)) :: srcs
           | ResBase (SpillRegister _) ->
                src :: subst_reserve spset srcs
           | ResInfix (op1, op2) ->
                ResInfix (spset_reserve_operand spset op1, spset_reserve_operand spset op2) :: srcs
           | _ ->
                srcs)
 | [] ->
      []

let rec subst_vars spset srcs =
   match srcs with
      src :: srcs ->
         let srcs = subst_vars spset srcs in
            (match src with
                ImmediateNumber _
              | ImmediateLabel _
              | FPStack _ ->
                   srcs
              | Register r ->
                   spset_lookup_int32 spset r :: srcs
              | FloatRegister r ->
                   spset_lookup_float spset r :: srcs
              | MMXRegister r ->
                   (* spset_lookup_mmx spset r :: *) srcs
              | MemReg r ->
                   MemReg (spset_register spset r) :: srcs
              | MemRegOff (r, i) ->
                   MemRegOff (spset_register spset r, i) :: srcs
              | MemRegRegOffMul _ ->
                   srcs
              | SpillRegister _ ->
                   src :: srcs)
    | [] ->
         []

(************************************************
 * FLOATING POINT GENERIC SUBSTITUTION
 *)

(*
 * Substitution for a unary src-only instruction.
 * This is for instructions like MUL, where the side-effect
 * is to a well-known location.
 *)
let subst_fprm_src1 spset inst src cont =
   subst_fprm_src spset src (fun src ->
         inst src :: cont)

(*
 * Substitution for a unary dst-only combination.
 *)
let subst_fprm_dst1 = subst_fprm_src1

(*
 * Replace the operands in a two-address dst/src instruction.
 * Some operand combinations are not allowed.
 *)
let subst_fprm_dst_src2 spset inst dst src cont =
   subst_fprm_src spset dst (fun src0 ->
   subst_fprm_src spset src (fun src1 ->
         (* TEMP: this code is bogus for floating-point.  I
            don't think we actually need to do anything here,
            since floating-point operations always unfold
            into stack references, yes?  -JDS, 2002.04.10
         match src0, src1 with
            FloatRegister _, _
          | FPStack _, _
          | _, Register _
          | _, ImmediateNumber _ ->
               inst src0 src1 :: cont
          | _ ->
               let tmp = Register (new_symbol_string "subst_tmp") in
               let inst1 = MOV (IL, tmp, src1) in
                  inst1 :: inst src0 src1 :: cont))
          *)
         inst src0 src1 :: cont))


(************************************************
 * INSTRUCTION SUBSTITUTION
 *)
let subst_inst spset inst cont =
   match inst with
      (* Integer instructions *)
      MOVD (dst, src)
    | MOVQ (dst, src) ->
         if equal_operands spset dst src then
            CommentString "Removed MOVD/MOVQ to self" :: cont
         else
            subst_rm_mov spset IL dst src cont
    | MOV  (pre, dst, src) ->
         if equal_operands spset dst src then
            CommentString "Removed MOV to self" :: cont
         else
            subst_rm_mov spset pre dst src cont
    | MOVS (pre, dst, src) ->
         subst_reg_dst_src2 spset (fun dst src -> MOVS (pre, dst, src)) dst src cont
    | MOVZ (pre, dst, src) ->
         subst_reg_dst_src2 spset (fun dst src -> MOVZ (pre, dst, src)) dst src cont
    | RMOVS _ ->
         inst :: cont
    | ADD  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> ADD (pre, dst, src)) dst src cont
    | ADC  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> ADC (pre, dst, src)) dst src cont
    | LEA  (pre, dst, src) ->
         subst_lea spset (fun dst src ->
               match src with
                  MemReg src ->
                     MOV (pre, dst, Register src)
                | _ ->
                     LEA (pre, dst, src)) dst src cont
    | SUB  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> SUB (pre, dst, src)) dst src cont
    | SBB  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> SBB (pre, dst, src)) dst src cont
    | IMUL (pre, dst, src) ->
         subst_reg_dst_src2 spset (fun dst src -> IMUL (pre, dst, src)) dst src cont
    | AND  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> AND (pre, dst, src)) dst src cont
    | OR   (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> OR (pre, dst, src)) dst src cont
    | XOR  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> XOR (pre, dst, src)) dst src cont
    | SAL  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> SAL (pre, dst, src)) dst src cont
    | SAR  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> SAR (pre, dst, src)) dst src cont
    | SHL  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> SHL (pre, dst, src)) dst src cont
    | SHR  (pre, dst, src) ->
         subst_rm_dst_src2 spset (fun dst src -> SHR (pre, dst, src)) dst src cont
    | SHLD (pre, dst, src, shift) ->
         subst_rm_dst_src_src3 spset (fun dst src1 src2 -> SHLD (pre, dst, src1, src2)) dst src shift cont
    | SHRD (pre, dst, src, shift) ->
         subst_rm_dst_src_src3 spset (fun dst src1 src2 -> SHRD (pre, dst, src1, src2)) dst src shift cont
    | CMP  (pre, dst, src) ->
         subst_rm_src_src2 spset (fun dst src -> CMP (pre, dst, src)) dst src cont
    | TEST (pre, dst, src) ->
         subst_rm_src_src2 spset (fun dst src -> TEST (pre, dst, src)) dst src cont
    | NEG  (pre, dst) ->
         subst_rm_dst_src1 spset (fun dst -> NEG (pre, dst)) dst cont
    | NOT  (pre, dst) ->
         subst_rm_dst_src1 spset (fun dst -> NOT (pre, dst)) dst cont
    | INC  (pre, dst) ->
         subst_rm_dst_src1 spset (fun dst -> INC (pre, dst)) dst cont
    | DEC  (pre, dst) ->
         subst_rm_dst_src1 spset (fun dst -> DEC (pre, dst)) dst cont
    | MUL  (pre, src) ->
         subst_rm_src1 spset (fun src -> MUL (pre, src)) src cont
    | DIV  (pre, src) ->
         subst_rm_src1 spset (fun src -> DIV (pre, src)) src cont
    | IDIV (pre, src) ->
         subst_rm_src1 spset (fun src -> IDIV (pre, src)) src cont
    | CLTD ->
         inst :: cont

      (* Float instructions *)
    | FMOV (dpre, dst, spre, src) ->
         if equal_operands spset dst src then
            let cont = subst_fprm_src1 spset (fun src -> FLD (spre, src)) src cont in
               CommentString "Removed FMOV" :: cont
         else
            subst_fprm_dst_src2 spset (fun dst src -> FMOV (dpre, dst, spre, src)) dst src cont
    | FMOVP (dpre, dst, spre, src) ->
         if equal_operands spset dst src then
            CommentString "Removed FMOVP" :: cont
         else
            subst_fprm_dst_src2 spset (fun dst src -> FMOVP (dpre, dst, spre, src)) dst src cont
    | FLD (pre, src) ->
         subst_fprm_src1 spset (fun src -> FLD (pre, src)) src cont
    | FILD src ->
         subst_fprm_src1 spset (fun src -> FILD src) src cont
    | FILD64 src ->
         subst_fprm_src1 spset (fun src -> FILD64 src) src cont
    | FST (pre, dst) ->
         subst_fprm_dst1 spset (fun dst -> FST (pre, dst)) dst cont
    | FSTP (pre, dst) ->
         subst_fprm_dst1 spset (fun dst -> FSTP (pre, dst)) dst cont
    | FSTCW dst ->
         subst_fprm_dst1 spset (fun dst -> FSTCW dst) dst cont
    | FLDCW src ->
         subst_fprm_src1 spset (fun src -> FLDCW src) src cont
    | FIST dst ->
         subst_fprm_dst1 spset (fun dst -> FIST dst) dst cont
    | FISTP dst ->
         subst_fprm_dst1 spset (fun dst -> FISTP dst) dst cont
    | FISTP64 dst ->
         subst_fprm_dst1 spset (fun dst -> FISTP64 dst) dst cont
    | FXCH dst ->
         subst_fprm_dst1 spset (fun dst -> FXCH dst) dst cont
    | FADD (dst, src) ->
         subst_fprm_dst_src2 spset (fun dst src -> FADD (dst, src)) dst src cont
    | FSUB (dst, src) ->
         subst_fprm_dst_src2 spset (fun dst src -> FSUB (dst, src)) dst src cont
    | FMUL (dst, src) ->
         subst_fprm_dst_src2 spset (fun dst src -> FMUL (dst, src)) dst src cont
    | FDIV (dst, src) ->
         subst_fprm_dst_src2 spset (fun dst src -> FDIV (dst, src)) dst src cont
    | FSUBR (dst, src) ->
         subst_fprm_dst_src2 spset (fun dst src -> FSUBR (dst, src)) dst src cont
    | FDIVR (dst, src) ->
         subst_fprm_dst_src2 spset (fun dst src -> FDIVR (dst, src)) dst src cont
    | FINIT
    | FCHS
    | FADDP
    | FSUBP
    | FMULP
    | FDIVP
    | FSUBRP
    | FDIVRP
    | FPREM
    | FSIN
    | FCOS
    | FPATAN
    | FSQRT
    | FUCOM
    | FUCOMP
    | FUCOMPP
    | FSTSW ->
         inst :: cont

      (* Control flow *)
    | PUSH (pre, src) ->
         subst_rm_src1 spset (fun src -> PUSH (pre, src)) src cont
    | POP (pre, dst) ->
         subst_rm_dst1 spset (fun dst -> POP (pre, dst)) dst cont
    | CALL src ->
         subst_rm_src1 spset (fun src -> CALL src) src cont
    | FCALL src ->
         subst_rm_src1 spset (fun src -> FCALL src) src cont
    | JMP   src ->
         subst_rm_src1 spset (fun src -> JMP src) src cont
    | FJMP  src ->
         subst_rm_src1 spset (fun src -> FJMP src) src cont
    | IJMP (srcs, src) ->
         subst_rm_src1 spset (fun src -> IJMP (subst_vars spset srcs, src)) src cont
    | JCC  (cc, src) ->
         subst_rm_src1 spset (fun src -> JCC (cc, src)) src cont
    | SET  (cc, dst) ->
         subst_rm_dst1 spset (fun dst -> SET (cc, dst)) dst cont
    | RET ->
         inst :: cont
    | RES  (label, vars, mem, ptr) ->
         subst_rm_src_src2 spset (fun mem ptr ->
               RES (label, subst_reserve spset vars, mem, ptr)) mem ptr cont
    | COW  (label, vars, mem, ptr, copy_ptr) ->
         subst_rm_src_src_src3 spset (fun mem ptr copy_ptr ->
               COW (label, subst_reserve spset vars, mem, ptr, copy_ptr)) mem ptr copy_ptr cont
    | RESP (label, vars) ->
         RESP (label, subst_reserve spset vars) :: cont
    | CommentFIR _
    | CommentMIR _
    | CommentInst _
    | CommentString _
    | NOP ->
         inst :: cont

let rec subst_code spset = function
   inst :: code ->
      let code = subst_code spset code in
         subst_inst spset inst code
 | [] ->
      []

(*
 * Subst debug info.
 *)
let subst_src spset op =
   match op with
      ImmediateNumber _
    | ImmediateLabel _
    | FPStack _
    | MemReg _
    | MemRegOff _
    | SpillRegister _ ->
         op
    | Register r ->
         spset_lookup_int32 spset r
    | FloatRegister r ->
         spset_lookup_float spset r
    | MMXRegister r ->
         spset_lookup_mmx spset r
    | MemRegRegOffMul _ ->
         raise (Invalid_argument "subst_src")

let subst_debug spset (line, info) =
   let info =
      List.map (fun (v, ty, src) -> v, ty, subst_src spset src) info
   in
      line, info

(*
 * Rewrite regs according to the spset.
 *)
let subst_block spset block =
   let { block_debug = debug;
         block_code = code
       } = block
   in
   let debug = subst_debug spset debug in
   let code = subst_code spset code in
      { block with block_debug = debug;
                   block_code = code
      }

let subst_blocks spset blocks =
   Trace.map (subst_block spset) blocks

(************************************************************************
 * SPILL ELIM
 ************************************************************************)

(*
 * Convert a spill operand.
 *)
let spill_operand op =
   match op with
      SpillRegister (_, off) ->
         MemRegOff (ebp, off)
    | _ ->
         op

let spill_prog prog =
   map_operands_prog spill_operand prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
