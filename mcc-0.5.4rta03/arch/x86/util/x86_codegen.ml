(*
   Assemble the given FIR code to x86 code.
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Debug
open Symbol
open Flags

open X86_frame_type


(***  Register Allocator Module  ***)


module X86Backend = X86_backend.Backend
module X86Frame = X86_backend.Frame
module RA = Ra_main.Make (X86Frame)
module MirCodegen = Mir_codegen.MirCodegen (X86Backend)
module MirType = Mir_type.MirType (X86Backend)


(***  Compilation (low-level)  ***)


(* compile_mir_of_fir
   Compile the given FIR code by translating first to MIR.
   This will typecheck the FIR (which may come from a source
   that is not trusted).  *)
let compile_mir_of_fir prog =
   MirCodegen.compile_mir_of_fir prog


(* compile_asm_of_mir
   Compile the MIR given, translating to assembly code.
   This is a low-level function that permits migration
   data to be passed in, but does not do any I/O or
   evaluation.  *)
let compile_asm_of_mir prog migrate =
   (* Make sure the MIR is standardized before continuing.  This will
      merrily throw an exception if any variable is bound multiple
      times.  If the program is not standardized, chaos will ensue in
      the backend... *)
   let () = MirType.check_prog prog in

   (* Generate assembly *)
   let prog = X86_mir.compile prog in
   let () =
      if debug Fir_state.debug_print_asm then
         X86_print.debug_prog "*** Initial assembly" prog
   in

   (* Spill the registers we know about so far *)
   let blocks = X86_subst.subst_blocks prog.asm_spset prog.asm_blocks in
   let prog = { prog with asm_blocks = blocks } in

   (* Eliminate/do branch prediction. *)
   let prog = X86_branch.branch_prog prog in

   (* Construct the trace.  The register allocator will run faster if the
      blocks are in rough order and there are no trivial blocks.  Also, the
      before_ra pass consolidates blocks, which will speed up peephole.  *)
   (* WARNING:  If you move this to after peephole, you *will* impair the
                peephole optimizations significantly.  Contact JDS...  *)
   let prog = X86_trace.before_ra prog in

   (* At least one pass of peephole optimization should be performed
      before the register allocator gets ahold of us and mucks things up. *)
   (* NOTE:     Do not comment out this line... use a flag instead if you
                need to disable this optimization.  *)
   (* WARNING:  If you move this to after peephole, you *will* impair the
                peephole optimizations significantly.  Contact JDS...  *)
   let prog = X86_peephole.peephole true prog in

   (* Add all the block labels and globals
      to the spset.  *)
   let prog = X86_frame.spset_spill_prog prog in
   let () =
      if debug Fir_state.debug_print_asm then
         X86_print.debug_prog "*** assembly before register allocation" prog
   in

   (* Optional: permute the program symbols *)
   let prog = X86_permutomatic.permute_program_symbols prog X86_permutomatic.BeforeRA in

   (* Run the register allocator *)
   let prog =
      let globals = X86_util.vars_globals X86_util.vars_frame prog.asm_globals in
      let spset, blocks = RA.compile globals prog.asm_spset prog.asm_blocks in
      let spills = X86_frame.spset_size spset in
         { prog with asm_spills = spills;
                     asm_spset = spset;
                     asm_blocks = blocks
         }
   in

   (* Optional: permute the program symbols *)
   let prog = X86_permutomatic.permute_program_symbols prog X86_permutomatic.AfterRA in

   (* Optimizations after allocation *)
   let prog = X86_trace.after_ra prog in

   (* Rewrite FP registers at the end *)
   let prog = X86_fpstack.rewrite_prog prog in

   (* Do one last peephole optimization before scheduling.
      At this point, since special register names have been
      resolved by now, it should be safe to run dead inst
      analysis on the assembly. *)
   let prog = X86_peephole.peephole false prog in

   (* The last step is instruction scheduling *)
   let prog = X86_schedule.schedule_prog prog in
      { prog with asm_migrate = migrate }

let compile_asm_of_mir prog migrate =
   Fir_state.profile "X86_codegen.compile_asm_of_mir" (compile_asm_of_mir prog) migrate


(* compile_asm_of_fir
   Calls the above two functions.  *)
let compile_asm_of_fir prog migrate =
   let prog = compile_mir_of_fir prog in
   let prog = compile_asm_of_mir prog migrate in
      prog


(*
 * Build this as a module.
 *)
module Codegen =
struct
   type prog = X86_frame.prog

   let print_prog = X86_print.pp_print_prog
   let print_object = X86_as.assem_prog
   let link_object = X86_as.link

   let compile_asm_of_fir = compile_asm_of_fir
   let compile_asm_of_mir = compile_asm_of_mir
   let compile_mir_of_fir = compile_mir_of_fir

   let catch = X86_pos.catch
end


(* Register miscellaneous flag sections *)
let () = std_flags_help_section_text "x86.debug"
   "Debugging flags specific to the X86 backend."
