(*
 * Assembly printout.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Symbol

open Frame_type

open X86_inst_type
open X86_frame_type
open X86_frame
open X86_build

(************************************************************************
 * GENERIC UTILITIES
 ************************************************************************)

(*
 * Invert a condition code.
 *)
let invert_cc cc =
   match cc with
      LT -> GE
    | LE -> GT
    | EQ -> NEQ
    | NEQ -> EQ
    | GT -> LE
    | GE -> LT
    | ULT -> UGE
    | ULE -> UGT
    | UGT -> ULE
    | UGE -> ULT

(************************************************************************
 * OPERAND CALCULATIONS
 ************************************************************************)

(*
 * Generic operand map.
 *)
let apply_reserve f op =
   match op with
      ResBase op ->
         ResBase (f op)
    | ResInfix (op1, op2) ->
         ResInfix (f op1, f op2)

let map_operands f inst =
   match inst with
      MOV   (pre, dst, src) -> MOV   (pre, f dst, f src)
    | MOVD  (dst, src)      -> MOVD  (f dst, f src)
    | MOVQ  (dst, src)      -> MOVQ  (f dst, f src)
    | MOVS  (pre, dst, src) -> MOVS  (pre, f dst, f src)
    | MOVZ  (pre, dst, src) -> MOVZ  (pre, f dst, f src)
    | RMOVS pre             -> RMOVS pre
    | ADD   (pre, dst, src) -> ADD   (pre, f dst, f src)
    | SUB   (pre, dst, src) -> SUB   (pre, f dst, f src)
    | ADC   (pre, dst, src) -> ADC   (pre, f dst, f src)
    | LEA   (pre, dst, src) -> LEA   (pre, f dst, f src)
    | SBB   (pre, dst, src) -> SBB   (pre, f dst, f src)
    | IMUL  (pre, dst, src) -> IMUL  (pre, f dst, f src)
    | AND   (pre, dst, src) -> AND   (pre, f dst, f src)
    | OR    (pre, dst, src) -> OR    (pre, f dst, f src)
    | XOR   (pre, dst, src) -> XOR   (pre, f dst, f src)
    | SAL   (pre, dst, src) -> SAL   (pre, f dst, f src)
    | SAR   (pre, dst, src) -> SAR   (pre, f dst, f src)
    | SHL   (pre, dst, src) -> SHL   (pre, f dst, f src)
    | SHR   (pre, dst, src) -> SHR   (pre, f dst, f src)
    | CMP   (pre, dst, src) -> CMP   (pre, f dst, f src)
    | TEST  (pre, dst, src) -> TEST  (pre, f dst, f src)

    | SHLD  (pre, dst, src, shift) -> SHLD (pre, f dst, f src, f shift)
    | SHRD  (pre, dst, src, shift) -> SHRD (pre, f dst, f src, f shift)

    | NOT   (pre, src) -> NOT  (pre, f src)
    | NEG   (pre, src) -> NEG  (pre, f src)
    | INC   (pre, dst) -> INC  (pre, f dst)
    | DEC   (pre, dst) -> DEC  (pre, f dst)
    | MUL   (pre, dst) -> MUL  (pre, f dst)
    | DIV   (pre, dst) -> DIV  (pre, f dst)
    | IDIV  (pre, dst) -> IDIV (pre, f dst)
    | JMP   dst        -> JMP  (f dst)
    | FJMP  dst        -> FJMP (f dst)
    | IJMP  (srcs, dst)-> IJMP (srcs, f dst)
    | PUSH  (pre, src) -> PUSH (pre, f src)
    | POP   (pre, dst) -> POP  (pre, f dst)
    | CALL  src        -> CALL (f src)
    | FCALL src        -> FCALL(f src)
    | JCC   (cc, src)  -> JCC  (cc, f src)
    | SET   (cc, dst)  -> SET  (cc, f dst)

    | RES   (label, vars, mem, ptr) ->
         RES (f label, List.map (apply_reserve f) vars, f mem, f ptr)
    | COW   (label, vars, mem, ptr, copy_ptr) ->
         COW (f label, List.map (apply_reserve f) vars, f mem, f ptr, f copy_ptr)
    | RESP (label, vars) ->
         RESP (f label, List.map (apply_reserve f) vars)

    | CLTD      -> CLTD
    | FINIT     -> FINIT

    | FSTCW   dst          -> FSTCW   (f dst)
    | FLDCW   src          -> FLDCW   (f src)
    | FMOV    (dpre, dst, spre, src) -> FMOV  (dpre, f dst, spre, f src)
    | FMOVP   (dpre, dst, spre, src) -> FMOVP (dpre, f dst, spre, f src)
    | FLD     (pre, src)   -> FLD     (pre, f src)
    | FILD    src          -> FILD    (f src)
    | FILD64  src          -> FILD64  (f src)
    | FST     (pre, dst)   -> FST     (pre, f dst)
    | FSTP    (pre, dst)   -> FSTP    (pre, f dst)
    | FIST    dst          -> FIST    (f dst)
    | FISTP   dst          -> FISTP   (f dst)
    | FISTP64 dst          -> FISTP64 (f dst)
    | FXCH    dst          -> FXCH    (f dst)
    | FADD    (dst, src)   -> FADD    (f dst, f src)
    | FSUB    (dst, src)   -> FSUB    (f dst, f src)
    | FMUL    (dst, src)   -> FMUL    (f dst, f src)
    | FDIV    (dst, src)   -> FDIV    (f dst, f src)
    | FSUBR   (dst, src)   -> FSUBR   (f dst, f src)
    | FDIVR   (dst, src)   -> FDIVR   (f dst, f src)
    | FCHS
    | FADDP
    | FSUBP
    | FMULP
    | FDIVP
    | FSUBRP
    | FDIVRP
    | FPREM
    | FSIN
    | FCOS
    | FPATAN
    | FSQRT
    | FUCOM
    | FUCOMP
    | FUCOMPP
    | FSTSW
    | NOP
    | RET
    | CommentFIR _
    | CommentMIR _
    | CommentInst _
    | CommentString _ ->
         inst

let map_operands_list f insts =
   List.map (map_operands f) insts

let map_operands_block f block =
   { block with block_code = map_operands_list f block.block_code }

let map_operands_prog f prog =
   { prog with asm_blocks = Trace.map (map_operands_block f) prog.asm_blocks }

(*
 * Generic operand fold.
 * The instructions are not changed.
 *)
let fold_reserve f arg op =
   match op with
      ResBase op ->
         f arg op
    | ResInfix (op1, op2) ->
         f (f arg op1) op2

let fold_operands f arg inst =
   match inst with
      MOV    (_, dst, src)
    | MOVS   (_, dst, src)
    | MOVZ   (_, dst, src)
    | MOVD   (dst, src)
    | MOVQ   (dst, src)
    | ADD    (_, dst, src)
    | SUB    (_, dst, src)
    | ADC    (_, dst, src)
    | LEA    (_, dst, src)
    | SBB    (_, dst, src)
    | IMUL   (_, dst, src)
    | AND    (_, dst, src)
    | OR     (_, dst, src)
    | XOR    (_, dst, src)
    | SAL    (_, dst, src)
    | SAR    (_, dst, src)
    | SHL    (_, dst, src)
    | SHR    (_, dst, src)
    | CMP    (_, dst, src)
    | TEST   (_, dst, src)
    | FADD   (dst, src)
    | FSUB   (dst, src)
    | FMUL   (dst, src)
    | FDIV   (dst, src)
    | FSUBR  (dst, src)
    | FDIVR  (dst, src)
    | FMOV   (_, dst, _, src)
    | FMOVP  (_, dst, _, src) ->
         f (f arg dst) src

    | SHLD   (_, dst, src, shift)
    | SHRD   (_, dst, src, shift) ->
         f (f (f arg dst) src) shift

    | NEG     (_, src)
    | NOT     (_, src)
    | INC     (_, src)
    | DEC     (_, src)
    | MUL     (_, src)
    | DIV     (_, src)
    | IDIV    (_, src)
    | JMP     src
    | FJMP    src
    | PUSH    (_, src)
    | POP     (_, src)
    | CALL    src
    | FCALL   src
    | JCC     (_, src)
    | SET     (_, src)
    | FLD     (_, src)
    | FILD    src
    | FILD64  src
    | FST     (_, src)
    | FSTP    (_, src)
    | FIST    src
    | FISTP   src
    | FISTP64 src
    | FXCH    src
    | FSTCW   src
    | FLDCW   src ->
         f arg src

    | IJMP  (srcs, dst) ->
         List.fold_left f (f arg dst) srcs
    | RES   (label, vars, mem, ptr) ->
         List.fold_left (fold_reserve f) (f (f (f arg mem) ptr) label) vars
    | COW   (label, vars, mem, ptr, copy_ptr) ->
         List.fold_left (fold_reserve f) (f (f (f (f arg mem) ptr) copy_ptr) label) vars
    | RESP  (label, vars) ->
         List.fold_left (fold_reserve f) (f arg label) vars

    | RMOVS _
    | CLTD
    | FINIT
    | FCHS
    | FADDP
    | FSUBP
    | FMULP
    | FDIVP
    | FSUBRP
    | FDIVRP
    | FPREM
    | FSIN
    | FCOS
    | FPATAN
    | FSQRT
    | FUCOM
    | FUCOMP
    | FUCOMPP
    | FSTSW
    | NOP
    | RET
    | CommentFIR _
    | CommentMIR _
    | CommentInst _
    | CommentString _ ->
         arg

let fold_operands_list f arg insts =
   List.fold_left (fold_operands f) arg insts

(************************************************************************
 * LISTED VARS
 ************************************************************************)

(*
 * Get the vars in an operand.
 *)
let vars_operand vars = function
   ImmediateNumber _
 | ImmediateLabel _
 | FPStack _ ->
      vars
 | SpillRegister _ ->
      SymbolTable.add vars ebp int_reg_class
 | Register r
 | MemReg r
 | MemRegOff (r, _) ->
      SymbolTable.add vars r int_reg_class
 | FloatRegister v ->
      SymbolTable.add vars v float_reg_class
 | MMXRegister v ->
      SymbolTable.add vars v mmx_reg_class
 | MemRegRegOffMul (r1, r2, _, _) ->
      SymbolTable.add (SymbolTable.add vars r1 int_reg_class) r2 int_reg_class

let vars_operand2 vars op1 op2 =
   vars_operand (vars_operand vars op1) op2

let vars_operand3 vars op1 op2 op3 =
   vars_operand (vars_operand2 vars op1 op2) op3

let vars_operand_mul vars op =
   let vars = SymbolTable.add vars eax int_reg_class in
   let vars = SymbolTable.add vars edx int_reg_class in
      vars_operand vars op

let vars_operands vars ops =
   List.fold_left vars_operand vars ops

let vars_reserve vars op =
   match op with
      ResBase op ->
         vars_operand vars op
    | ResInfix (op1, op2) ->
         vars_operand2 vars op1 op2

let vars_reserves vars ops =
   List.fold_left vars_reserve vars ops

(*
 * Get the vars in an instruction.
 *)
let vars_inst vars inst =
   match inst with
      MOV     (_, dst, src)
    | MOVS    (_, dst, src)
    | MOVZ    (_, dst, src)
    | MOVD    (dst, src)
    | MOVQ    (dst, src)
    | ADD     (_, dst, src)
    | SUB     (_, dst, src)
    | ADC     (_, dst, src)
    | LEA     (_, dst, src)
    | SBB     (_, dst, src)
    | IMUL    (_, dst, src)
    | AND     (_, dst, src)
    | OR      (_, dst, src)
    | XOR     (_, dst, src)
    | SAL     (_, dst, src)
    | SAR     (_, dst, src)
    | SHL     (_, dst, src)
    | SHR     (_, dst, src)
    | CMP     (_, dst, src)
    | TEST    (_, dst, src) ->
         vars_operand2 vars dst src
    | RMOVS   _ ->
         vars
    | SHLD    (_, dst, src, shift)
    | SHRD    (_, dst, src, shift) ->
         vars_operand3 vars dst src shift
    | NOT     (_, dst)
    | NEG     (_, dst)
    | INC     (_, dst)
    | DEC     (_, dst) ->
         vars_operand vars dst
    | MUL     (_, dst)
    | DIV     (_, dst)
    | IDIV    (_, dst) ->
         vars_operand_mul vars dst
    | CLTD ->
         vars_operand2 vars (Register edx) (Register eax)

    | FMOV    (_, dst, _, src)
    | FMOVP   (_, dst, _, src) ->
         vars_operand2 vars dst src
    | FLD     (_, dst)
    | FILD    dst
    | FILD64  dst
    | FST     (_, dst)
    | FSTP    (_, dst)
    | FIST    dst
    | FISTP   dst
    | FISTP64 dst
    | FXCH    dst
    | FSTCW   dst
    | FLDCW   dst ->
         vars_operand vars dst
    | FADD    (dst, src)
    | FSUB    (dst, src)
    | FMUL    (dst, src)
    | FDIV    (dst, src)
    | FSUBR   (dst, src)
    | FDIVR   (dst, src) ->
         vars_operand2 vars dst src
    | FINIT
    | FCHS
    | FADDP
    | FSUBP
    | FMULP
    | FDIVP
    | FSUBRP
    | FDIVRP
    | FPREM
    | FSIN
    | FCOS
    | FPATAN
    | FSQRT
    | FUCOM
    | FUCOMP
    | FUCOMPP ->
         vars
    | FSTSW ->
         vars_operand vars (Register eax)

    | PUSH    (_, dst)
    | POP     (_, dst)
    | CALL    dst
    | FCALL   dst
    | JMP     dst
    | FJMP    dst
    | IJMP    (_, dst)
    | JCC     (_, dst)
    | SET     (_, dst) ->
         vars_operand vars dst
    | RET ->
         vars
    | RES     (_, rvars, mem, ptr) ->
         let vars = vars_operand2 vars mem ptr in
            vars_reserves vars rvars
    | COW     (_, rvars, mem, ptr, copy_ptr) ->
         let vars = vars_operand3 vars mem ptr copy_ptr in
            vars_reserves vars rvars
    | RESP    (_, rvars) ->
         vars_reserves vars rvars
    | CommentFIR _
    | CommentMIR _
    | CommentInst _
    | CommentString _
    | NOP ->
         vars

(*
 * Get the vars in a block.
 *)
let vars_block vars
    { block_debug = (_, info);
      block_code = code
    } =
   let vars =
      List.fold_left (fun vars (_, _, src) ->
            vars_operand vars src) vars info
   in
      List.fold_left vars_inst vars code

(*
 * Initial variable table.
 *)
let vars_frame =
   let rec collect vars i =
      if i = reg_class_count then
         vars
      else
         let vars = List.fold_left (fun vars v -> SymbolTable.add vars v i) vars registers.(i) in
            collect vars (succ i)
   in
      collect SymbolTable.empty 0

(*
 * Get the vars in the blocks.
 *)
let vars_blocks vars blocks =
   Trace.fold vars_block vars blocks

(*
 * Classify the globals.
 *)
let vars_globals vars globals =
   List.fold_left (fun vars (v, init) ->
         let code =
            match init with
               InitRawData _
             | InitAggrBlock _
             | InitTagBlock _
             | InitRawInt _
             | InitVar _ ->
                  int_reg_class
             | InitRawFloat _ ->
                  float_reg_class
         in
            SymbolTable.add vars v code) vars globals

(************************************************************************
 * CODE FORM
 ************************************************************************)


(***  Convert inst forms to code  ***)


(* TEMP  Justin thinks this code is a mess *)


(* Flag modifer states *)
type flag =
   FlagTest   of reg * reg_class
 | FlagModify of reg * reg_class

let modify_iflags = FlagModify (iflags, int_reg_class)
let modify_fflags = FlagModify (fflags, float_reg_class)
let test_iflags   = FlagTest   (iflags, int_reg_class)
let test_fflags   = FlagTest   (fflags, float_reg_class)


(* Flag filters *)
let src_flags flags src =
   let filter_flags flags = function
      FlagTest (r, _) ->
         r :: flags
    | _ ->
         flags
   in
      List.fold_left filter_flags src flags

let dst_flags flags dst =
   let filter_flags flags = function
      FlagModify (r, _) ->
         r :: flags
    | _ ->
         flags
   in
      List.fold_left filter_flags dst flags

let int_src_flags flags src =
   let filter_flags flags = function
      FlagTest (r, c) when c = int_reg_class ->
         r :: flags
    | _ ->
         flags
   in
      List.fold_left filter_flags src flags

let int_dst_flags flags dst =
   let filter_flags flags = function
      FlagModify (r, c) when c = int_reg_class ->
         r :: flags
    | _ ->
         flags
   in
      List.fold_left filter_flags dst flags

let float_src_flags flags src =
   let filter_flags flags = function
      FlagTest (r, c) when c = float_reg_class ->
         r :: flags
    | _ ->
         flags
   in
      List.fold_left filter_flags src flags

let float_dst_flags flags dst =
   let filter_flags flags = function
      FlagModify (r, c) when c = float_reg_class ->
         r :: flags
    | _ ->
         flags
   in
      List.fold_left filter_flags dst flags


(* move_operands
   Constructs a move expression from src to dst.  *)
let get_operands_mov_int32 inst dst src =
   { code_dst   = [dst];
     code_src   = [src];
     code_class = CodeMove;
     code_inst  = inst
   }

let get_operands_movb_int32 inst dst src =
   { code_dst   = [dst];
     code_src   = [src; dst];
     code_class = CodeMove;
     code_inst  = inst
   }

let get_operands_mov_float inst dst src =
   { code_dst  = [dst; fflags];
     code_src  = [src];
     code_class = CodeMove;
     code_inst = inst
   }

let get_operands_mov_mmx inst dst src =
   { code_dst   = [dst];
     code_src   = [src];
     code_class = CodeMove;
     code_inst  = inst
   }


(* get_operands_mov
   Get the operands for a move instruction, with one source
   as well as one destination (the destination may be in
   memory in which case dst is a source regs, but not dest).  *)
let get_operands_mov inst dst src =
   let dst, src =
      match dst with
         Register r
       | FloatRegister r
       | MMXRegister r ->
            [r], src_regs src []
       | _ ->
            [], src_regs dst (src_regs src [])
   in
      { code_dst   = dst;
        code_src   = src;
        code_class = CodeNormal;
        code_inst  = inst
      }


(*
 * Floating store.
 * Add floating flags to dst.
 *)
let get_operands_fst flags inst dst =
   let dst, src =
      match dst with
         Register r
       | FloatRegister r
       | MMXRegister r ->
            [r], []
       | _ ->
            [], src_regs dst []
   in
      { code_dst   = dst_flags flags dst;
        code_src   = dst_flags flags src;
        code_class = CodeNormal;
        code_inst  = inst
      }


(* get_operands_src
   Get sources of a single src-operator.  *)
let get_operands_src flags inst src =
   { code_dst   = dst_flags flags [];
     code_src   = src_flags flags (src_regs src []);
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_dst
   Get destination for a dst-operator.  *)
let get_operands_dst flags inst dst =
   { code_dst   = dst_flags flags (dst_regs dst []);
     code_src   = src_flags flags [];
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_push
   Get the inst for ALL register classes, for a push operation.
   The source may be a floating-point register.  This returns
   a list for all register classes.  *)
let get_operands_push inst src =
   { code_dst   = [esp];
     code_src   = esp :: src_regs src [];
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_pop
   Same as above, only for a POP operation.  *)
let get_operands_pop inst dst =
   match dst with
      Register r
    | FloatRegister r
    | MMXRegister r ->
         { code_dst   = [esp; r];
           code_src   = [esp];
           code_class = CodeNormal;
           code_inst  = inst
         }
    | _ ->
         { code_dst   = esp :: dst_regs dst [];
           code_src   = esp :: src_regs dst [];
           code_class = CodeNormal;
           code_inst  = inst
         }


(* get_operands_ijmp
   Returns a list for all register classes.  This sifts through the
   list of source registers, creating both the integer operands class
   and the float operands class in one fell swoop.  NOTE:  We assume
   that we don't care about flags on any unconditional jump.  *)
let get_operands_ijmp inst srcs dst =
   let rec sift_sources src =
      match src with
         src :: srcs ->
            src_regs src (sift_sources srcs)
       | [] ->
            src_regs dst []
   in
   let srcs = sift_sources srcs in
      { code_dst   = [];
        code_src   = esp :: ebp :: srcs;
        code_class = CodeNormal;
        code_inst  = inst
      }


(*
 * Branches.
 *)
let get_operands_jmp inst src =
   match src with
      ImmediateLabel l ->
         { code_dst   = [];
           code_src   = [];
           code_class = CodeJump l;
           code_inst  = inst
         }
    | _ ->
         { code_dst   = [];
           code_src   = src_regs src [];
           code_class = CodeNormal;
           code_inst  = inst
         }

let get_operands_jcc inst src =
   match src with
      ImmediateLabel l ->
         { code_dst   = [];
           code_src   = [iflags];
           code_class = CodeJump l;
           code_inst  = inst
         }
    | _ ->
         { code_dst   = [];
           code_src   = iflags :: src_regs src [];
           code_class = CodeNormal;
           code_inst  = inst
         }


(* get_operands1
   Get the operands for an instruction with src = dst.  *)
let get_operands1 flags inst src =
   match src with
      Register r ->
         { code_dst   = r :: int_dst_flags flags [];
           code_src   = r :: int_src_flags flags [];
           code_class = CodeNormal;
           code_inst  = inst
         }
    | _ ->
         { code_dst   = int_dst_flags flags [];
           code_src   = int_src_flags flags (src_regs src []);
           code_class = CodeNormal;
           code_inst  = inst
         }


(* get_operands_mul
   Construct the special case for MUL, where eax:edx are
   destination registers, and eax is a source register. *)
let get_operands_mul inst src =
   { code_dst   = [eax; edx; iflags];
     code_src   = eax :: src_regs src [];
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_cltd
   Construct the special case for CLTD, where eax:edx are
   destination registers, and eax is the only source. *)
let get_operands_cltd inst =
   { code_dst   = [eax; edx; iflags];
     code_src   = [eax];
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_div
   Construct the special case for DIV and IDIV, where
   eax and edx are destination and source registers.  *)
let get_operands_div inst src =
   { code_dst   = [eax; edx; iflags];
     code_src   = eax :: edx :: src_regs src [];
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_list
   Returns the operands in a list of srcs.  *)
let get_operands_list flags inst srcs =
   let srcs =
      List.fold_left (fun srcs src ->
            src_regs src srcs) [] srcs
   in
      { code_dst   = int_dst_flags flags [];
        code_src   = int_src_flags flags srcs;
        code_class = CodeNormal;
        code_inst  = inst
      }

(*
 * The the operands for a reserve.
 *)
let get_operands_reserve flags inst vars srcs =
   let srcs =
      List.fold_left (fun srcs src ->
            src_regs src srcs) [] srcs
   in
   let srcs =
      List.fold_left (fun srcs src ->
            match src with
               ResBase src ->
                  src_regs src srcs
             | ResInfix (src1, src2) ->
                  src_regs src1 (src_regs src2 srcs)) srcs vars
   in
      { code_dst   = int_dst_flags flags [];
        code_src   = int_src_flags flags srcs;
        code_class = CodeNormal;
        code_inst  = inst
      }

(*
 * Get the debugging operands for a block.
 * This will set the live-in set.
 *)
let get_operands_debug (_, info) =
   let srcs =
      List.fold_left (fun srcs (_, _, src) ->
            src_regs src srcs) [] info
   in
      { code_dst   = [];
        code_src   = srcs;
        code_class = CodeNormal;
        code_inst  = NOP
      }


(* get_operands2
   Get operands for a typical instruction which reads both
   arguments and writes to the destination argument only.  *)
let get_operands2 flags inst dst src =
   let dst, src =
      match dst with
         Register r ->
            [r], r :: src_regs src []
       | _ ->
            [], src_regs dst (src_regs src [])
   in
      { code_dst   = int_dst_flags flags dst;
        code_src   = int_src_flags flags src;
        code_class = CodeNormal;
        code_inst  = inst
      }


(* get_operands3
   Get operands for a typical instruction which reads three
   arguments and writes to the destination argument only.  *)
let get_operands3 flags inst dst src1 src2 =
   let dst, src =
      match dst with
         Register r ->
            [r], r :: src_regs src1 (src_regs src2 [])
       | _ ->
            [], src_regs dst (src_regs src1 (src_regs src2 []))
   in
      { code_dst   = int_dst_flags flags dst;
        code_src   = int_src_flags flags src;
        code_class = CodeNormal;
        code_inst  = inst
      }


(* get_operands_cmp
   Get operands for an instruction which reads both arguments
   as its sources, but makes no modification and has no dst.  *)
let get_operands_cmp inst src1 src2 =
   { code_dst   = [iflags];
     code_src   = src_regs src1 (src_regs src2 []);
     code_class = CodeNormal;
     code_inst  = inst
   }


(* get_operands_xor
   Get operands for an XOR.  This is a special case; if src = dst,
   then there are no sources to this operation because of the unique
   behaviour of XOR for clearing registers.  *)
let get_operands_xor inst dst src =
   if dst = src then
      { code_dst   = iflags :: dst_regs dst [];
        code_src   = [];
        code_class = CodeNormal;
        code_inst  = inst
      }
   else
      get_operands2 [modify_iflags] inst dst src


(* get_operands_call
   Returns the operands in a call instruction.  We
   assume that everything gets trashed by the call...  *)
let call_dst_regs =
   [eax; ebx; ecx; edx; esi; edi; ebp; iflags;
    fp0; fp1; fp2; fp3; fp4; fp5; fflags]

let get_operands_call inst src =
   { code_dst   = call_dst_regs;
     code_src   = esp :: src_regs src [];
     code_class = CodeNormal;
     code_inst  = inst
   }

(* null_inst has no dst,src, however they may still manipulate flags. *)
let get_operands_null flags inst =
   { code_dst   = int_dst_flags flags [];
     code_src   = int_src_flags flags [];
     code_class = CodeNormal;
     code_inst  = inst
   }


let get_operands inst =
   match inst with
      MOV    (IL, Register dst, Register src) ->
         get_operands_mov_int32 inst dst src

    | MOV    (IB, Register dst, Register src)
    | MOV    (IW, Register dst, Register src) ->
         get_operands_movb_int32 inst dst src

    | FMOV   (_, FloatRegister dst, _, FloatRegister src)
    | FMOVP  (_, FloatRegister dst, _, FloatRegister src) ->
         get_operands_mov_float inst dst src

    | MOVQ   (MMXRegister dst, MMXRegister src) ->
         get_operands_mov_mmx inst dst src

    | MOV    (_, dst, src)
    | MOVS   (_, dst, src)
    | MOVZ   (_, dst, src)
    | LEA    (_, dst, src)
    | FMOV   (_, dst, _, src)
    | FMOVP  (_, dst, _, src)
    | MOVD   (dst, src)
    | MOVQ   (dst, src) ->
         get_operands_mov inst dst src

    | ADD    (_, dst, src)
    | SUB    (_, dst, src)
    | IMUL   (_, dst, src)
    | AND    (_, dst, src)
    | OR     (_, dst, src)
    | SAL    (_, dst, src)
    | SAR    (_, dst, src)
    | SHL    (_, dst, src)
    | SHR    (_, dst, src) ->
         get_operands2 [modify_iflags] inst dst src

    | SHLD   (_, dst, src, shift)
    | SHRD   (_, dst, src, shift) ->
         get_operands3 [modify_iflags] inst dst src shift

    | ADC    (_, dst, src)
    | SBB    (_, dst, src) ->
         get_operands2 [test_iflags; modify_iflags] inst dst src

    | XOR    (_, dst, src) ->
         get_operands_xor inst dst src

    | CMP    (_, src1, src2)
    | TEST   (_, src1, src2) ->
         get_operands_cmp inst src1 src2

    | NEG    (_, src)
    | NOT    (_, src)
    | INC    (_, src)
    | DEC    (_, src) ->
         get_operands1 [modify_iflags] inst src

    | MUL    (_, src) ->
         get_operands_mul inst src

    | CLTD ->
         get_operands_cltd inst

    | DIV    (_, src)
    | IDIV   (_, src) ->
         get_operands_div inst src

    | JMP    src
    | FJMP   src ->
         get_operands_jmp inst src

    | JCC    (_, src) ->
         get_operands_jcc inst src

    | IJMP   (srcs, dst) ->
         get_operands_ijmp inst srcs dst

    | SET    (_, dst) ->
         (* We want to look like we see the (cleared) high bits *)
         get_operands1 [test_iflags] inst dst

    | FLD    (_, src)
    | FLDCW  src ->
         get_operands_src [modify_fflags] inst src

    | FST    (_, dst)
    | FSTP   (_, dst)
    | FSTCW  dst ->
         get_operands_fst [modify_fflags] inst dst

    | FXCH   dst ->
         get_operands1 [modify_fflags] inst dst

    | FILD   src
    | FILD64 src ->
         (* This reads from an int32 register *)
         get_operands_src [modify_fflags] inst src
    | FIST    dst
    | FISTP   dst
    | FISTP64 dst ->
         (* This stores an int32 register *)
         get_operands_fst [modify_fflags] inst dst
    | FSTSW ->
         (* This stores an int32 register value *)
         get_operands_dst [test_fflags; modify_fflags] inst (Register eax)

    | FADD  (dst, src)
    | FSUB  (dst, src)
    | FMUL  (dst, src)
    | FDIV  (dst, src)
    | FSUBR (dst, src)
    | FDIVR (dst, src) ->
         get_operands2 [modify_fflags] inst dst src

    | FINIT
    | FCHS
    | FADDP
    | FSUBP
    | FMULP
    | FDIVP
    | FSUBRP
    | FDIVRP
    | FPREM
    | FSIN
    | FCOS
    | FPATAN
    | FSQRT
    | FUCOM
    | FUCOMP
    | FUCOMPP ->
         get_operands_null [modify_fflags] inst

    | PUSH  (_, src) ->
         get_operands_push inst src
    | POP   (_, src) ->
         get_operands_pop inst src
    | CALL  src
    | FCALL src ->
         get_operands_call inst src
    | RET ->
         { code_dst   = [esp];
           code_src   = [esp; eax];
           code_class = CodeNormal;
           code_inst  = inst
         }
    | RES   (_, vars, mem, ptr) ->
         (* RES only cares about int32 registers -- pointers *)
         get_operands_reserve [modify_iflags] inst vars [mem; ptr]
    | COW   (_, vars, mem, ptr, copy_ptr) ->
         get_operands_reserve [modify_iflags] inst vars [mem; ptr; copy_ptr]
    | RESP  (_, vars) ->
         get_operands_reserve [modify_iflags] inst vars []

      (* String copies both use and mutilate ecx, esi, edi *)
    | RMOVS _ ->
         { code_dst   = ecx :: esi :: edi :: dst_flags [modify_iflags] [];
           code_src   = ecx :: esi :: edi :: src_flags [modify_iflags] [];
           code_class = CodeNormal;
           code_inst  = inst
         }

    | CommentFIR _
    | CommentMIR _
    | CommentInst _
    | CommentString _
    | NOP ->
         get_operands_null [] inst

(*
 * Filter operands lists.
 *)
let filter_code_operands filter code =
   let { code_src = src; code_dst = dst } = code in
      { code with code_src = List.filter filter src;
                  code_dst = List.filter filter dst
      }

let filter_block_operands filter block =
   let { block_code = code } = block in
      { block with block_code = List.map (filter_code_operands filter) code }

let filter_blocks_operands filter blocks =
   Trace.map (filter_block_operands filter) blocks

(*
 * Derive dst/src for each instruction.
 *)
let block_code block =
   List.map get_operands block.block_code

let block_code ignore block =
   let code = block_code block in
      if ignore = [] then
         code
      else
         List.map (filter_code_operands (fun v -> not (List.mem v ignore))) code

(*
 * Convert the inst block list to a code block list.
 *)
let classify_block ignore block =
   (* Get operands for the code *)
   let code = block_code ignore block in
   let inst = get_operands_debug block.block_debug in
      { block with block_code = inst :: code }

let classify_blocks ignore blocks =
   Trace.map (classify_block ignore) blocks


(* is_memory_operand
   Returns true if the operand given references memory -- AT ALL.
   Eventually we need to be smarter about this, but for now the
   barrier invariant is simple -- memory instructions must not
   cross, at all.  *)
let is_memory_operand = function
   ImmediateNumber _
 | ImmediateLabel _
 | Register _
 | FloatRegister _
 | MMXRegister _
 | FPStack _ ->
      false
 | SpillRegister _
 | MemReg _
 | MemRegOff _
 | MemRegRegOffMul _ ->
      true


(* memory_inst
   Returns a pair of memory cells used as a dest and a source
   (respectively in the pair) during an instruction.  This is
   important so we know which instructions may cross each other,
   when both instructions reference memory in some way.  We
   need to distinguish dest from source since sources may cross
   arbitrarily, but once we write memory we must tip-toe... *)
let memory_inst inst =
   let mem = List.filter is_memory_operand in
   let memory_ds dst = mem [dst], mem [dst] in
   let memory_src src = [], mem [src] in
   let memory_src_list ls = [], mem ls in
   let memory_dst dst = mem [dst], [] in
   let memory_dst_src dst src = mem [dst], mem [src] in
   let memory_ds_src dst src = mem [dst], mem [dst; src] in
   let memory_src_src src1 src2 = [], mem [src1; src2] in
      match inst with
         MOV   (_, dst, src)
       | MOVS  (_, dst, src)
       | MOVZ  (_, dst, src)
       | MOVD  (dst, src)
       | MOVQ  (dst, src)
       | FMOV  (_, dst, _, src)
       | FMOVP (_, dst, _, src) ->
            memory_dst_src dst src

         (* String repeat insts's have funny semantics *)
       | RMOVS _ ->
            memory_dst_src (MemReg edi) (MemReg esi)

         (* The source of LEA is never a memory operand; rather,
            it is (hopefully) the address of a value in memory. *)
       | LEA   (_, dst, _)
       | FST   (_, dst)
       | FSTP  (_, dst)
       | FIST  dst
       | FISTP dst
       | FISTP64 dst ->
            memory_dst dst

       | NEG   (_, dst)
       | NOT   (_, dst)
       | INC   (_, dst)
       | DEC   (_, dst)
       | SHL   (_, dst, _)
       | SHR   (_, dst, _)
       | SAL   (_, dst, _)
       | SAR   (_, dst, _)
       | SHLD  (_, dst, _, _)
       | SHRD  (_, dst, _, _)
       | SET   (_, dst) ->
            memory_ds dst

       | ADD   (_, dst, src)
       | SUB   (_, dst, src)
       | ADC   (_, dst, src)
       | SBB   (_, dst, src)
       | IMUL  (_, dst, src)
       | AND   (_, dst, src)
       | OR    (_, dst, src)
       | XOR   (_, dst, src) ->
            memory_ds_src dst src

       | CMP   (_, src1, src2)
       | TEST  (_, src1, src2) ->
            memory_src_src src1 src2

       | MUL   (_, src)
       | DIV   (_, src)
       | IDIV  (_, src)
       | CALL  src
       | FCALL src
       | JMP   src
       | FJMP  src
       | IJMP  (_, src)
       | JCC   (_, src)
       | FLD   (_, src)
       | FILD  src
       | FILD64 src ->
            memory_src src

       | RES   (label, _, mem, ptr) ->
            memory_src_list [mem; ptr; label]
       | COW   (label, _, mem, ptr, copy_ptr) ->
            memory_src_list [mem; ptr; label; copy_ptr]
       | RESP  (label, _) ->
            memory_src_list [label]

         (* PUSH and POP, by definition, use memory *)
       | PUSH  (_, src) ->
            memory_dst_src (MemReg esp) src
       | POP   (_, dst) ->
            memory_dst_src dst (MemReg esp)

       | RET
       | NOP
       | CLTD
       | FINIT
       | FCHS
       | FADDP
       | FSUBP
       | FMULP
       | FDIVP
       | FSUBRP
       | FDIVRP
       | FADD  _
       | FSUB  _
       | FMUL  _
       | FDIV  _
       | FSUBR _
       | FDIVR _
       | FPREM
       | FSIN
       | FCOS
       | FPATAN
       | FSQRT
       | FUCOM
       | FUCOMP
       | FUCOMPP
       | FSTSW
       | FXCH  _
       | FSTCW _
       | FLDCW _
       | CommentFIR _
       | CommentMIR _
       | CommentInst _
       | CommentString _ ->
            [], []

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
