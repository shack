(*
   Generate x86 code from an MIR atom expression tree.
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Mir
open Mir_pos

open X86_mir_env
open X86_inst_type
open X86_frame_type
open X86_frame


(* Interface *)
val code_atom_int     : codegen -> pos -> debug_info -> atom -> (dst -> inst_block list * label) -> inst_block list * label
val code_atom_int8    : codegen -> pos -> debug_info -> bool -> atom -> (dst -> inst_block list * label) -> inst_block list * label
val code_atom_int16   : codegen -> pos -> debug_info -> bool -> atom -> (dst -> inst_block list * label) -> inst_block list * label
val code_atom_int32   : codegen -> pos -> debug_info -> atom -> (dst -> inst_block list * label) -> inst_block list * label
val code_atom_int64   : codegen -> pos -> debug_info -> atom -> (dst -> dst -> inst_block list * label) -> inst_block list * label
val code_atom_float   : codegen -> pos -> debug_info -> atom -> (dst -> inst_block list * label) -> inst_block list * label

val code_atom_poly    : codegen -> pos -> debug_info -> atom -> (dst -> inst_block list * label) -> inst_block list * label

val code_atom_ptr_base  : codegen -> pos -> debug_info -> atom -> (dst -> inst_block list * label) -> inst_block list * label
val code_atom_ptr_off   : codegen -> pos -> debug_info -> atom -> (dst -> inst_block list * label) -> inst_block list * label
val code_atom_ptr_infix : codegen -> pos -> debug_info -> atom -> (dst -> dst -> inst_block list * label) -> inst_block list * label
