(*
 * This is the generic backend modules.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Frame_type
open X86_inst_type
open X86_frame_type

module Backend : BackendSig =
struct
   (* Types *)
   type block = X86_frame_type.inst_block

   (* Pointer table descriptors *)
   let pointer_base = X86_frame.pointer_base
   let pointer_size = X86_frame.pointer_size

   (* Closure table descriptors *)
   let function_base = X86_frame.function_base
   let function_size = X86_frame.function_size

   (* Segmentation faults *)
   let other_fault = X86_frame.other_fault
   let index_fault = X86_frame.index_fault
   let fun_index_fault = X86_frame.fun_index_fault
   let pointer_index_fault = X86_frame.pointer_index_fault
   let bounds_fault = X86_frame.bounds_fault
   let lower_bounds_fault = X86_frame.lower_bounds_fault
   let upper_bounds_fault = X86_frame.upper_bounds_fault
   let null_pointer_fault = X86_frame.null_pointer_fault
   let fun_arity_tag_fault = X86_frame.fun_arity_tag_fault
   let fault_count = X86_frame.fault_count
   let seg_fault_labels = X86_frame.seg_fault_labels

   (* Debug labels *)
   let debug_alloc = X86_frame.debug_alloc
   let debug_let_poly = X86_frame.debug_let_poly
   let debug_let_pointer = X86_frame.debug_let_pointer
   let debug_let_function = X86_frame.debug_let_function
   let debug_set_poly = X86_frame.debug_set_poly
   let debug_set_pointer = X86_frame.debug_set_pointer
   let debug_set_function = X86_frame.debug_set_function
   let debug_pre_let_poly = X86_frame.debug_pre_let_poly
   let debug_pre_let_pointer = X86_frame.debug_pre_let_pointer
   let debug_pre_let_function = X86_frame.debug_pre_let_function
   let debug_pre_set_pointer = X86_frame.debug_pre_set_pointer
   let debug_pre_set_function = X86_frame.debug_pre_set_function
   let debug_count = X86_frame.debug_count
   let debug_labels = X86_frame.debug_labels

   (* Hash letue for pointer entries *)
   let hash_index_label = X86_frame.hash_index_label
   let hash_fun_index_label = X86_frame.hash_fun_index_label
   let index_shift32 = X86_frame.index_shift32

   (* Pointer table masks *)
   let ptable_pointer_mask = X86_frame.ptable_pointer_mask
   let ptable_valid_bit_off = X86_frame.ptable_valid_bit_off

   (* Size constants *)
   let aggr_header_size = X86_frame.aggr_header_size
   let block_header_size = X86_frame.block_header_size

   (* Special tags *)
   let rttd_tag = X86_frame.rttd_tag

   (* Migration values *)
   let migrate_debug_gdbhooks = X86_runtime.migrate_debug_gdbhooks

   (* Arity tags *)
   let backend_arity_tags = X86_frame.backend_arity_tags
end

(*
 * The abstract frame.
 *)
module Frame =
struct
   (* Types *)
   type 'a block = (debug_info, 'a) X86_frame_type.poly_block
   type spset = X86_frame.spset
   type reg_class = X86_frame.reg_class
   type inst = X86_inst_type.inst

   (* Costs *)
   let def_cost = X86_frame.def_cost
   let use_cost = X86_frame.use_cost
   let mov_def_cost = X86_frame.mov_def_cost
   let mov_use_cost = X86_frame.mov_use_cost

   (* Basic blocks *)
   let block_label { block_label = label } =
      label

   let block_code = X86_util.block_code

   let block_live block live =
      { block with block_code = live }

   let print_blocks = X86_print.pp_print_blocks

   let print_live_blocks = X86_print.pp_print_live_blocks

   (*
    * Register sets.
    *)
   let registers = X86_frame.registers
   let registers_special = X86_frame.registers_special
   let reg_class_count = X86_frame.reg_class_count

   (*
    * Spilling.
    *)
   let spset_spill = X86_frame.spset_spill
   let spset_add = X86_frame.spset_add

   (*
    * Apply a substitution.
    *)
   let subst_blocks = X86_subst.subst_blocks

   (*
    * Get all the vars defined in any instruction.
    *)
   let vars_blocks = X86_util.vars_blocks
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
