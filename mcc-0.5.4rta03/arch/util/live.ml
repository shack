(*
 * Generate the live sets for abstract assembly instructions.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Jason Hickey, Caltech
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Format
open Debug

open Symbol
open Trace

open Frame_type
open Symbol_matrix

let debug_live = ref false


(*
 * Stats for a variable.
 *)
type stats =
   { mutable stats_defs : int array;
     mutable stats_uses : int array
   }


(*
 * Move information.
 * Keep track of interference with dst var.
 *)
type move_stats =
   { move_depth   : int array;
     move_live    : SymbolSet.t
   }


(*
 * The result of liveness is a list of neighbors for each
 * var, and a set of moves.
 *)
type igraph =
   { igraph_stats : stats SymbolTable.t;
     igraph_graph : bool SymSymbolMatrix.t;
     igraph_moves : move_stats AsymSymbolMatrix.t
   }


module type LiveSig =
sig
   type 'a block
   type reg_class = int
   type inst
   type benv

   (* Create the liveness graph *)
   val create_graph : reg_class SymbolTable.t -> var list -> inst block trace -> igraph
   val create_live  : reg_class SymbolTable.t -> var list -> inst block trace -> inst live block trace

   (* Functions for a 2-phase liveness analysis *)
   val create_live_benv  : var list -> inst block trace -> benv
   val create_live_block : benv -> int -> label -> inst live block
end


module MakeLive (Frame : FrameSig) : LiveSig
   with type 'a block = 'a Frame.block
   with type reg_class = Frame.reg_class
   with type inst = Frame.inst =
struct
   (***  Types  ***)


   (*
    * A program is a list of blocks.
    *)
   type 'a block = 'a Frame.block
   type reg_class = Frame.reg_class
   type inst = Frame.inst


   (*
    * A basic block contains a sequence of assembly instructions.  It
    * begins with a label, ends with a jump, and contains no other labels.
    * We use faster algorithms for computing defs/uses on basic blocks.
    *)
   type 'inst live_block =
      { (* Save all the block info *)
        block_label  : label;
        block_code   : 'inst code list;

        (* Defs and uses *)
        block_defs   : (label * SymbolSet.t) list;
        block_uses   : SymbolSet.t;

        (* Dataflow *)
        block_in     : SymbolSet.t;

        (* Remember the original block *)
        block_block  : inst Frame.block
      }


   (*
    * Type used for the environment of live_block structures,
    * indexed on the block labels.
    *)
   type benv = inst live_block SymbolTable.t


   (*
    * Lookup a value from the classification table.
    *)
   let cenv_lookup cenv v =
      try SymbolTable.find cenv v with
         Not_found ->
            raise (Invalid_argument ("liveness: unclassified var: " ^ Symbol.to_string v))


   (***  Control Flow Graph  ***)


   (*
    * Get the block liveness.  Some destinations won't exist (like the
    * seg_fault handler).  Assume those destinations have no uses.
    *)
   let block_live blocks label =
      try (SymbolTable.find blocks label).block_in with
         Not_found ->
            SymbolSet.empty


   (*
    * Compute the defs/uses in a basic block.  The defs are the union
    * of all the defined values, captured at the branch points.
    *)
   let block_defs code =
      let rec collect dtable defs code =
         match code with
            { code_dst = defs'; code_class = cclass } :: code ->
               let defs = SymbolSet.add_list defs defs' in
               let dtable =
                  match cclass with
                     CodeJump label ->
                        (label, defs) :: dtable
                   | CodeNormal
                   | CodeMove ->
                        dtable
               in
                  collect dtable defs code
          | [] ->
               dtable
      in
         collect [] SymbolSet.empty code


   (*
    * The uses are computed by propagating backward, subtracting defs.
    *)
   let block_uses code =
      let rec collect = function
         { code_dst = defs; code_src = uses } :: rest ->
            let uses' = collect rest in
            let uses' = SymbolSet.subtract_list uses' defs in
               SymbolSet.add_list uses' uses
       | [] ->
            SymbolSet.empty
      in
         collect code


   (*
    * Build a block from the assembly instruction list.
    * The first instruction should be a label, and there
    * should be no jumps except for the last instruction.
    *)
   let basic_block ignore block =
      let label = Frame.block_label block in
      let code  = Frame.block_code ignore block in
      let defs  = block_defs code in
      let uses  = block_uses code in
         { block_label = label;
           block_code  = code;
           block_defs  = defs;
           block_uses  = uses;
           block_in    = uses;
           block_block = block
         }


   (***  Block Dataflow Graph  ***)


   (*
    * Compute the least-fixed point of the dataflow equations:
    *     in[n] = uses[n] + (out[n] - defs[n])
    *     out[n] = union s in succ[n]. in[n]
    *
    * The dataflow equations are monotone, and the size of the sets
    * is limited by the total number of vars in the program,
    * so we can calculate the least-fixed point by iteration
    * (and be sure of termination).
    *
    * The input includes the control-flow graph,
    * a table that maps nodes to basic blocks,
    * and a set of nodes.
    *)
   let dataflow benv labels =
      (*
       * Compute the dataflow assignment for a single node.
       * Returns true iff the live sets have changed.
       *)
      let rec update_block updated benv label =
         let block = SymbolTable.find benv label in
         let { block_in = live;
               block_uses = uses;
               block_defs = defs
             } = block
         in
         let live' =
            List.fold_left (fun live (label, defs) ->
                  let live' = block_live benv label in
                     SymbolSet.union live (SymbolSet.diff live' defs)) uses defs
         in
            if SymbolSet.equal live live' then
               updated, benv
            else
               let block = { block with block_in = live' } in
               let benv = SymbolTable.add benv label block in
                  true, benv
      in

      (* Compute the fixpoint *)
      let rec fixpoint benv =
         let rec fixpoint_list benv updated l =
            match l with
               h :: t ->
                  let updated, benv = fixpoint_list benv updated t in
                     fixpoint_elem benv updated h
             | [] ->
                  updated, benv
         and fixpoint_elem benv updated e =
            match e with
               Elem label ->
                  update_block updated benv label
             | Trace (x, l) ->
                  fixpoint_list benv updated (Elem x :: l)
         in
         let flag, benv = fixpoint_list benv false labels in
            if flag then
               fixpoint benv
            else
               benv
      in
         fixpoint benv


   (***  Interference Graph  ***)


   (*
    * Def statistics.
    *)
   let add_depth defs depth =
      let len = Array.length defs in
      let defs =
         if depth < len then
            defs
         else
            let defs' = Array.create (succ depth) 0 in
               Array.blit defs 0 defs' 0 len;
               defs'
      in
         defs.(depth) <- succ defs.(depth);
         defs

   let graph_add get set graph depth v =
      let stats = SymbolTable.find graph.igraph_stats v in
      let defs = get stats in
      let len = Array.length defs in
      let defs =
         if depth < len then
            defs
         else
            let defs' = Array.create (succ depth) 0 in
               Array.blit defs 0 defs' 0 len;
               set stats defs';
               defs'
      in
         defs.(depth) <- succ defs.(depth)

   let graph_add_defs graph depth defs =
      List.iter (graph_add (fun stats -> stats.stats_defs) (fun stats defs -> stats.stats_defs <- defs) graph depth) defs

   let graph_add_uses graph depth uses =
      List.iter (graph_add (fun stats -> stats.stats_uses) (fun stats uses -> stats.stats_uses <- uses) graph depth) uses


   (*
    * Interferance graph operations.  For a normal instruction, all the defs
    * interfere with all the live vars.  Only add interferences within a
    * register class.
    *)
   let graph_add_normal cenv graph depth defs uses live =
      let igraph = graph.igraph_graph in
         graph_add_defs graph depth defs;
         graph_add_uses graph depth uses;
         List.iter (fun v1 ->
               let (rc1 : int) = cenv_lookup cenv v1 in
                  SymbolSet.iter (fun v2 ->
                        let (rc2 : int) = cenv_lookup cenv v2 in
                           if rc1 = rc2 && not (Symbol.eq v1 v2) then
                              SymSymbolMatrix.add igraph v1 v2 true) live) defs


   (*
    * New move stats.
    *)
   let move_new_stats depth live =
      let depth' = Array.create (succ depth) 0 in
         depth'.(depth) <- 1;
         { move_depth = depth';
           move_live = live
         }


   (*
    * Add the stats to an existing move stat table.
    *)
   let move_add_stats stats depth live =
      let { move_depth = depth';
            move_live = live'
          } = stats
      in
         { move_depth = add_depth depth' depth;
           move_live = SymbolSet.union live' live
         }


   (*
    * Add a move.
    *)
   let graph_add_move_stats graph def use depth live =
      AsymSymbolMatrix.filter_add graph.igraph_moves def use (fun stats ->
            match stats with
               Some stats ->
                  move_add_stats stats depth live
             | None ->
                  move_new_stats depth live)


   (*
    * Moves are special: the defs should be a single variable.
    * The def _does not_ interfere with its uses.
    *)
   let graph_add_move cenv graph depth defs uses live =
      let igraph = graph.igraph_graph in
         match defs, uses with
            [def], [use] ->
               let (rc1 : int) = cenv_lookup cenv def in
               let (rc2 : int) = cenv_lookup cenv use in
               let _ =
                  if rc1 <> rc2 then
                     raise (Invalid_argument (**)
                               (Printf.sprintf "graph_add_move: cenv for def, use disagree: def %s = %d, use %s = %d" (**)
                                   (string_of_symbol def) rc1 (string_of_symbol use) rc2))
               in
               let live =
                  SymbolSet.fold (fun live v3 ->
                        let (rc3 : int) = cenv_lookup cenv v3 in
                           if rc3 = rc1 && not (Symbol.eq v3 def) then
                              SymbolSet.add live v3
                           else
                              live) SymbolSet.empty (SymbolSet.remove live use)
               in
                  graph_add_move_stats graph def use depth live
          | _ ->
               raise (Invalid_argument "graph_add_move: move has wrong number of operands")


   (*
    * Update the interference graph for the instructions within
    * the block.  The live edges are compute from last to first
    * in the instruction order.  Whenever we encounter a jump,
    * we add live_in from the destination.  This builds the graph
    * alone, no live blocks are created.
    *)
   let block_dataflow_graph cenv graph benv depth label =
      let block = SymbolTable.find benv label in
      let rec collect code =
         match code with
            [] ->
               SymbolSet.empty
          | code :: tl ->
               let { code_src   = uses;
                     code_dst   = defs;
                     code_class = cclass;
                     code_inst  = inst
                   } = code
               in
               let live = collect tl in
               let live =
                  match cclass with
                     CodeNormal ->
                        graph_add_normal cenv graph depth defs uses live;
                        live
                   | CodeMove ->
                        graph_add_move cenv graph depth defs uses live;
                        live
                   | CodeJump label ->
                        let live = SymbolSet.union live (block_live benv label) in
                           graph_add_normal cenv graph depth defs uses live;
                           live
               in
                  SymbolSet.add_list (SymbolSet.subtract_list live defs) uses
      in
         ignore (collect block.block_code)


   (*
    * This is just like creating the dataflow graph,
    * but we create liveness information for the instructions
    * instead.
    *)
   let block_dataflow_live benv depth label =
      let block = SymbolTable.find benv label in
      let rec collect code =
         match code with
            [] ->
               SymbolSet.empty, []
          | code :: tl ->
               let { code_src   = uses;
                     code_dst   = defs;
                     code_class = cclass;
                     code_inst  = inst
                   } = code
               in
               let live, code = collect tl in
                  match cclass with
                     CodeNormal ->
                        let inst =
                           { live_src = uses;
                             live_dst = defs;
                             live_out = live;
                             live_class = cclass;
                             live_depth = depth;
                             live_inst = inst
                           }
                        in
                        let live = SymbolSet.add_list (SymbolSet.subtract_list live defs) uses in
                           live, inst :: code
                   | CodeMove ->
                        let inst =
                           { live_src = uses;
                             live_dst = defs;
                             live_out = live;
                             live_class = cclass;
                             live_depth = depth;
                             live_inst = inst
                           }
                        in
                        let live = SymbolSet.add_list (SymbolSet.subtract_list live defs) uses in
                           live, inst :: code
                   | CodeJump label ->
                        let live = SymbolSet.union live (block_live benv label) in
                        let inst =
                           { live_src = uses;
                             live_dst = defs;
                             live_out = live;
                             live_class = cclass;
                             live_depth = depth;
                             live_inst = inst
                           }
                        in
                        let live = SymbolSet.add_list (SymbolSet.subtract_list live defs) uses in
                           live, inst :: code
      in
      let _, code = collect block.block_code in
         Frame.block_live block.block_block code


   (*
    * Build the interference graph from all the blocks.
    *)
   let build_igraph cenv benv labels =
      let size = SymbolTable.cardinal cenv in
      let graph =
         { igraph_stats = SymbolTable.map (fun _ -> { stats_defs = [||]; stats_uses = [||] }) cenv;
           igraph_graph = SymSymbolMatrix.create size;
           igraph_moves = AsymSymbolMatrix.create size
         }
      in
         Trace.iter_depth (block_dataflow_graph cenv graph benv) labels;
         graph


   (*
    * Build the interference graph from all the blocks.
    *)
   let build_live cenv benv labels =
      Trace.map_depth (block_dataflow_live benv) labels


   (***  Global Interface  ***)


   (*
    * Builds the benv: computes the dataflow between blocks.
    * To calculate liveness:
    *    1. create the blocks
    *    2. perform block dataflow
    *    3. map dataflow through the blocks
    *)
   let create_aux ignore blocks =
      (* Convert all the blocks, and put them in a table *)
      let blocks = Trace.map (basic_block ignore) blocks in
      let benv =
         Trace.fold (fun benv block ->
               SymbolTable.add benv block.block_label block) SymbolTable.empty blocks
      in

      (* Reduce the block trace to a label trace *)
      let labels = Trace.map (fun block -> block.block_label) blocks in

      (* Perform dataflow *)
      let benv = dataflow benv labels in
         benv, labels


   (*
    * Create an interference graph, for use by register allocation.
    *)
   let create_graph cenv ignore blocks =
      let benv, labels = create_aux ignore blocks in
         build_igraph cenv benv labels


   (*
    * Create a trace of live inst blocks.  This is a one-pass algorithm.
    * You may find the next two functions, for a two-pass algorithm, to
    * be more efficient.  This function always gets depths right; the next
    * two functions rely on the user to specify proper depths.
    *)
   let create_live cenv ignore blocks =
      let benv, labels = create_aux ignore blocks in
      let blocks = build_live cenv benv labels in
         if debug debug_live then
            Frame.print_live_blocks err_formatter blocks;
         blocks


   (*
    * Create a benv for block-by-block construction of live information.
    * This is used by algorithms which can afford to compute liveness
    * information on blocks ``on demand''; by not precomputing everything,
    * we save a bit of memory and some thrashing (and some real compute
    * time if the algorithm does not require liveness information on all
    * blocks).
    *
    * This returns the ``benv'', which contains dataflow info (but has not
    * computed liveness information for individual insts).  Pass the benv
    * to create_live_block, with the block label of interest, to build the
    * final (inst live block) structure.
    *)
   let create_live_benv ignore blocks =
      fst (create_aux ignore blocks)


   (*
    * Create a live block, based on benv information that was previously
    * computed.  This is slightly more efficient than calling create_live,
    * since we only compute the data structures on demand (as opposed to
    * preallocating everything).
    *
    * Typically, you would do Trace.map_depth over the blocks of interest
    * to call this; if you really don't care about depth, then you can do
    * Trace.map and give 0 here.
    *)
   let create_live_block benv depth label =
      block_dataflow_live benv depth label
end (* struct *)
