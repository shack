(*
 * Size constants.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

(*
 * Size constants.
 *)
let sizeof_int          = 4
let sizeof_int8         = 1
let sizeof_int16        = 2
let sizeof_int32        = 4
let sizeof_int64        = 8
let sizeof_single       = 4
let sizeof_double       = 8
let sizeof_long_double  = 10
let sizeof_subscript    = 4
let sizeof_pointer      = 4

let size_align          = 4

let align_aggr          = 4
let align_int           = 4
let align_int8          = 1
let align_int16         = 2
let align_int32         = 4
let align_int64         = 4
let align_single        = 8
let align_double        = 8
let align_long_double   = 16
let align_subscript     = 4
let align_pointer       = 4

let precision_int        = Rawint.Int32
let precision_native_int = Rawint.Int32
let precision_pointer    = Rawint.Int32
let precision_subscript  = Rawint.Int32

let signed_subscript     = true

let sizeof_native_int    = 4
let sizeof_pointer       = 4

(*
 * We don't allow enums over a certain size.  The runtime tries to
 * guarantee that no value in [0,max_enum) is a valid pointer, therefore
 * enums will never be confused as pointers.
 *)
let max_enum             = 2048

(*
 * Maximum normal block tag.
 *)
let max_union_tag        = 100

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
