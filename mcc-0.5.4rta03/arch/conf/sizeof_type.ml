(*
 * Size on types.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Fir
open Fir_exn
open Fir_pos
open Fir_type

open Sizeof_const

module Pos = MakePos (struct let name = "Sizeof_type" end)
open Pos

(* align_off
   Align to a general alignment address.  *)
let align off align =
   let align = pred align in
      (off + align) land (lnot align)


(* align_size
   Round the size up to the nearest boundary.  *)
let align_size size =
   align size size_align


(* align_type
   align_type_list
   Align to the next type.  *)
let align_type_dir tenv pos ty i =
   let alignment =
      match tenv_expand_dir tenv pos ty with
         TyInt
       | TyEnum _ ->
            align_int
       | TyRawInt (Rawint.Int8, _) ->
            align_int8
       | TyRawInt (Rawint.Int16, _) ->
            align_int16
       | TyRawInt (Rawint.Int32, _) ->
            align_int32
       | TyRawInt (Rawint.Int64, _) ->
            align_int64
       | TyFloat Rawfloat.Single ->
            align_single
       | TyFloat Rawfloat.Double ->
            align_double
       | TyFloat Rawfloat.LongDouble ->
            align_long_double
       | TyFun _
       | TyRawData
       | TyUnion _
       | TyTuple _
       | TyArray _
       | TyVar _
       | TyExists _
       | TyAll _
       | TyProject _
       | TyCase _
       | TyObject _
       | TyPointer _
       | TyFrame _
       | TyDTuple _
       | TyTag _ ->
            align_pointer

       | TyDelayed
       | TyApply _ ->
            raise (FirException (pos, StringTypeError ("align_type", ty)))
   in
      align i alignment

let align_type_list_dir tenv pos tyl i =
   match tyl with
      ty :: _ ->
         align_type_dir tenv pos ty i
    | [] ->
         align_size i


(* sizeof_rawint
   How much space to store the rawint. *)
let sizeof_rawint = function
   Rawint.Int8  -> align_int8,  sizeof_int8
 | Rawint.Int16 -> align_int16, sizeof_int16
 | Rawint.Int32 -> align_int32, sizeof_int32
 | Rawint.Int64 -> align_int64, sizeof_int64


(* sizeof_rawfloat
   How much space to store the rawfloat. *)
let sizeof_rawfloat = function
   Rawfloat.Single -> align_single, sizeof_single
 | Rawfloat.Double -> align_double, sizeof_double
 | Rawfloat.LongDouble -> align_long_double, sizeof_long_double


(* sizeof_type
   sizeof_type_list
   Calculate the size of a value.  *)
let rec sizeof_type_dir tenv pos off ty =
   match tenv_expand_dir tenv pos ty with
      TyInt
    | TyEnum _ ->
         let off = align off align_int in
            off + sizeof_int
    | TyRawInt (pre, _) ->
         let align', size = sizeof_rawint pre in
         let off = align off align' in
            off + size
    | TyFloat pre ->
         let align', size = sizeof_rawfloat pre in
         let off = align off align' in
            off + size

    | TyExists (_, ty)
    | TyAll (_, ty) ->
         sizeof_type_dir tenv pos off ty

    | TyFun _
    | TyRawData
    | TyUnion _
    | TyTuple _
    | TyArray _
    | TyVar _
    | TyProject _
    | TyCase _
    | TyObject _
    | TyFrame _
    | TyDTuple _
    | TyTag _ ->
         let off = align off align_pointer in
            off + sizeof_pointer

    | TyPointer _ ->
         let off = align off align_pointer in
            off + 2 * sizeof_pointer

    | TyApply _
    | TyDelayed ->
         raise (FirException (pos, StringError "sizeof: type error"))

and sizeof_type_list_dir tenv pos off tyl =
   match tyl with
      ty :: tyl ->
         let off = sizeof_type_dir tenv pos off ty in
            sizeof_type_list_dir tenv pos off tyl
    | [] ->
         off


(* sizeof_aligned_type
   sizeof_aligned_type_list
   Aligned interface for zero offset.  *)
let sizeof_aligned_type_dir tenv pos ty =
   sizeof_type_dir tenv pos 0 ty

let sizeof_aligned_type_list_dir tenv pos tyl =
   sizeof_type_list_dir tenv pos 0 tyl

(*
 * General versions.
 *)
let sizeof_type genv = sizeof_type_dir (Fir_env.tenv_of_genv genv)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
