(*
 * Expression and value printing.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol

open Bfd
open Bfd_type

(*
 * Print a label.
 *)
let pp_print_label buf v =
   Format.pp_print_string buf (string_of_symbol v)

(*
 * Print a value.
 *)
let pp_print_value buf v =
   match v with
      ValAbsolute i ->
         Format.fprintf buf "abs(%s)" (Int32.format "0x%08x" i)
    | ValSection (sect, off) ->
         Format.fprintf buf "section(%s+%s)" (**)
            (string_of_symbol sect)
            (Int32.format "0x%08x" off)
    | ValLabel (v, off) ->
         Format.fprintf buf "label(%s+%s)" (string_of_symbol v) (Int32.format "0x%08x" off)
    | ValRelative (v1, v2, off) ->
         Format.fprintf buf "relative(%s-%s+%s)" (**)
            (string_of_symbol v1)
            (string_of_symbol v2)
            (Int32.format "0x%08x" off)
    | ValUndefined ->
         Format.fprintf buf "undefined"

(*
 * Print an expression.
 *)
let rec pp_print_exp buf e =
   match e with
      ExpInt32 i ->
         Format.pp_print_string buf (Int32.format "0x%08x" i)
    | ExpLabel v ->
         Format.pp_print_string buf (string_of_symbol v)
    | ExpUMinus e ->
         Format.pp_print_string buf "-";
         pp_print_exp buf e
    | ExpUNot e ->
         Format.pp_print_string buf "~";
         pp_print_exp buf e
    | ExpUAbs e ->
         Format.pp_print_string buf "abs(";
         pp_print_exp buf e;
         Format.pp_print_string buf ")"
    | ExpULog2 e ->
         Format.pp_print_string buf "log2(";
         pp_print_exp buf e;
         Format.pp_print_string buf ")"
    | ExpUPow2 e ->
         Format.pp_print_string buf "pow2(";
         pp_print_exp buf e;
         Format.pp_print_string buf ")"
    | ExpAdd (e1, e2) ->
         pp_print_binary buf "+" e1 e2
    | ExpSub (e1, e2) ->
         pp_print_binary buf "-" e1 e2
    | ExpRel (e, l) ->
         pp_print_rel buf ".rel." e l
    | ExpMul (e1, e2) ->
         pp_print_binary buf "*" e1 e2
    | ExpDiv (e1, e2) ->
         pp_print_binary buf "/" e1 e2
    | ExpSal (e1, e2) ->
         pp_print_binary buf "<<" e1 e2
    | ExpSar (e1, e2) ->
         pp_print_binary buf ">>a" e1 e2
    | ExpShr (e1, e2) ->
         pp_print_binary buf ">>l" e1 e2
    | ExpAnd (e1, e2) ->
         pp_print_binary buf "&" e1 e2
    | ExpOr (e1, e2) ->
         pp_print_binary buf "|" e1 e2
    | ExpXor (e1, e2) ->
         pp_print_binary buf "^" e1 e2
    | ExpAlign (e1, e2) ->
         pp_print_binary buf ".bfd_align." e1 e2

and pp_print_binary buf op e1 e2 =
   Format.pp_open_hvbox buf 1;
   Format.pp_print_string buf "(";
   pp_print_exp buf e1;
   Format.pp_print_space buf ();
   Format.pp_print_string buf op;
   Format.pp_print_string buf " ";
   pp_print_exp buf e2;
   Format.pp_print_string buf ")";
   Format.pp_close_box buf ()

and pp_print_rel buf op e l =
   Format.pp_open_hvbox buf 1;
   Format.pp_print_string buf "(";
   pp_print_exp buf e;
   Format.pp_print_space buf ();
   Format.pp_print_string buf op;
   Format.pp_print_string buf " ";
   Format.pp_print_string buf (string_of_symbol l);
   Format.pp_print_string buf ")";
   Format.pp_close_box buf ()

(*
 * Print a relocation.
 *)
let pp_print_reloc_type buf reloc_type =
   let s =
      match reloc_type with
         RelocInt8 -> "int8"
       | RelocInt16 -> "int16"
       | RelocInt24 -> "int24"
       | RelocInt32 -> "int32"
       | RelocInt64 -> "int64"
   in
      Format.pp_print_string buf s

let pp_print_reloc buf reloc =
   let { reloc_type = reloc_type;
         reloc_pos = pos;
         reloc_offset = off;
         reloc_exp = e
       } = reloc
   in
      Format.pp_open_hvbox buf 3;
      Format.fprintf buf "%a,@ %d, %d,@ %a" (**)
         pp_print_reloc_type reloc_type
         pos
         off
         pp_print_exp e;
      Format.pp_close_box buf ()

(*
 * Print an interval.
 *)
let pp_print_interval buf = function
   IntAll ->
      Format.pp_print_string buf "_"
 | IntRange (lower, upper) ->
      Format.pp_print_string buf "[";
      Format.pp_print_string buf (Int32.format "0x%08x" lower);
      Format.pp_print_string buf ", ";
      Format.pp_print_string buf (Int32.format "0x%08x" upper);
      Format.pp_print_string buf "]"

(*
 * Print a symbol type.
 *)
let pp_print_sym_type buf t =
   let s = match t with
              SymGlobal -> "global"
            | SymUndefined -> "undefined"
            | SymLocal -> "local"
            | SymTemp -> "temp"
   in
      Format.pp_print_string buf s

(*
 * Print an item.
 *)
let rec pp_print_item buf item =
   match item with
      BufAlign i ->
         Format.fprintf buf "bfd_align(%d)" i
    | BufSkip (bfd_skip, fill) ->
         Format.fprintf buf "bfd_skip(%d,%d)" bfd_skip fill
    | BufChoice (e, choices) ->
         Format.pp_open_hvbox buf 3;
         Format.pp_print_string buf "choice(";
         pp_print_exp buf e;
         Format.pp_print_string buf ";";
         List.iter (fun { choice_enabled = enabled;
                          choice_interval = interval;
                          choice_buffer = buffer
                        } ->
               Format.pp_print_space buf ();
               Format.pp_open_hvbox buf 3;
               pp_print_interval buf interval;
               Format.pp_print_string buf " ->";
               Format.pp_print_space buf ();
               pp_print_buffer buf buffer;
               Format.pp_close_box buf ()) choices;
         Format.pp_print_string buf ")";
         Format.pp_close_box buf ()
    | BufLabel (sym_type, label) ->
         Format.fprintf buf "%a: %a" (**)
            pp_print_label label
            pp_print_sym_type sym_type
    | BufEqu (sym_type, label, e) ->
         Format.fprintf buf ".equ(%a,%a,%a)" (**)
            pp_print_sym_type sym_type
            pp_print_label label
            pp_print_exp e
    | BufCommon (sym_type, label, i) ->
         Format.fprintf buf ".comm(%a,%a,%d)" (**)
            pp_print_sym_type sym_type
            pp_print_label label
            i
    | BufReloc reloc ->
         Format.fprintf buf ".reloc(%a)" (**)
            pp_print_reloc reloc

    | BufData s ->
         Format.fprintf buf ".data %s" (String.escaped s)
    | BufAscii s ->
         Format.fprintf buf ".ascii \"%s\"" (String.escaped s)
    | BufInt8 i ->
         Format.fprintf buf "\t.int8 0x%02x" i
    | BufInt16 i ->
         Format.fprintf buf ".int16 %d" i
    | BufInt24 i ->
         Format.fprintf buf ".int24 %d" i
    | BufInt32 i ->
         Format.fprintf buf ".int32 %s" (Int32.to_string i)
    | BufInt64 i ->
         Format.fprintf buf ".int64 %s" (Int64.to_string i)
    | BufFloat32 x ->
         Format.fprintf buf ".float32 %s" (Float80.to_string x)
    | BufFloat64 x ->
         Format.fprintf buf ".float64 %s" (Float80.to_string x)
    | BufFloat80 x ->
         Format.fprintf buf ".float80 %s" (Float80.to_string x)

(*
 * Print a buffer.
 *)
and pp_print_buffer buf buffer =
   let items = buffer.buf_forward_list @ List.rev buffer.buf_reverse_list in
      ignore (List.fold_left (fun first item ->
                    if not first then
                       pp_print_space buf ();
                    pp_print_item buf item;
                    false) true items)

(*
 * Print a section.
 *)
let pp_print_sect buf sect =
   let { sect_name = name;
         sect_data = buffer
       } = sect
   in
      Format.pp_open_vbox buf 3;
      Format.fprintf buf ".section %a" pp_print_label name;
      Format.pp_print_space buf ();
      pp_print_buffer buf buffer;
      Format.pp_close_box buf ()

(*
 * Print the bfd.
 * Print all the sections.
 *)
let pp_print_bfd buf bfd =
   let { bfd_sections = sects } = bfd in
      Format.pp_open_hvbox buf 3;
      Format.pp_print_string buf "BFD:";
      SymbolTable.iter (fun _ sect ->
            Format.pp_print_space buf ();
            match sect with
               RawSection sect ->
                  pp_print_sect buf sect
             | ValueSection _ ->
                  Format.pp_print_string buf ".section <value>") sects;
      Format.pp_close_box buf ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
