(*
 * Resolve symbol locations as much as possible.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Bfd
open Bfd_type
open Bfd_buf
open Bfd_bfd
open Bfd_env
open Bfd_util
open Bfd_eval
open Bfd_state
open Bfd_sizeof

(************************************************************************
 * INITIAL VENV
 ************************************************************************)

(*
 * Initial variable values.
 *)
let abs_zero = ValAbsolute Int32.zero

(*
 * Look at all buffer in the choice.
 *)
let rec init_case sect venv case =
   case.choice_enabled <- true;
   init_buf sect venv case.choice_buffer

(*
 * Set all labels to 0.
 *)
and init_item sect venv item =
   match item with
      BufLabel (sym_type, label)
    | BufEqu (sym_type, label, _)
    | BufCommon (sym_type, label, _) ->
         let info =
            { sym_section = sect;
              sym_type = sym_type;
              sym_value = abs_zero
            }
         in
            venv_update_preserve venv label info
    | BufChoice (_, cases) ->
         List.fold_left (init_case sect) venv cases
    | _ ->
         venv

(*
 * Look at all items.
 *)
and init_items sect venv items =
   List.fold_left (init_item sect) venv items

(*
 * Run through the entire buffer and set all labels to 0.
 *)
and init_buf sect venv buf =
   init_items sect venv (buffer_items buf)

(*
 * Get all the labels in the section.
 *)
let init_sect venv sect =
   init_buf sect.sect_name venv sect.sect_data

(*
 * Get the initial venv.
 *)
let init_venv venv bfd =
   List.fold_left init_sect venv (raw_sections bfd)

(************************************************************************
 * RESOLVE A SECTION
 ************************************************************************)

(*
 * Resolve the cases.
 * Choose the case that fits the best.
 *)
let rec resolve_cases sect venv changed pos e cases =
   let pos' = eval_eager_exp venv e in
   let rec search = function
      choice :: cases ->
         let { choice_enabled = enabled;
               choice_interval = interval;
               choice_buffer = buf
             } = choice
         in
            if enabled && interval_test pos' interval then
               begin
                  choice.choice_enabled <- true;
                  resolve_buf sect venv changed pos buf
               end
            else
               begin
                  choice.choice_enabled <- false;
                  search cases
               end
    | [] ->
         raise (Failure "resolve_cases")
   in
      search cases

(*
 * Resolve a single item.
 *)
and resolve_item sect venv changed pos item =
   match item with
      BufAlign bfd_align ->
         venv, changed, do_align pos bfd_align
    | BufSkip (count, _) ->
         venv, changed, pos + count
    | BufData s ->
         venv, changed, pos + String.length s
    | BufAscii s ->
         venv, changed, pos + succ (String.length s)
    | BufInt8 _ ->
         venv, changed, pos + sizeof_int8
    | BufInt16 _ ->
         venv, changed, pos + sizeof_int16
    | BufInt24 _ ->
         venv, changed, pos + sizeof_int24
    | BufInt32 _ ->
         venv, changed, pos + sizeof_int32
    | BufInt64 _ ->
         venv, changed, pos + sizeof_int64
    | BufFloat32 _ ->
         venv, changed, pos + sizeof_float32
    | BufFloat64 _ ->
         venv, changed, pos + sizeof_float64
    | BufFloat80 _ ->
         venv, changed, pos + sizeof_float80
    | BufLabel (_, v) ->
         let svalue = ValSection (sect, Int32.of_int pos) in
         let venv, changed = venv_update venv v svalue changed in
            venv, changed, pos
    | BufEqu (_, v, e) ->
         let svalue = eval_eager_exp venv e in
         let venv, changed = venv_update venv v svalue changed in
            venv, changed, pos
    | BufCommon (_, v, size) ->
         let svalue = ValSection (sect, Int32.of_int pos) in
         let venv, changed = venv_update venv v svalue changed in
            venv, changed, pos + size
    | BufReloc _ ->
         venv, changed, pos
    | BufChoice (e, cases) ->
         resolve_cases sect venv changed pos e cases

(*
 * Resolve the sequence of items.
 *)
and resolve_items sect venv changed pos items =
   let rec resolve venv changed pos = function
      item :: items ->
         let venv, changed, pos = resolve_item sect venv changed pos item in
            resolve venv changed pos items
    | [] ->
         venv, changed, pos
   in
      resolve venv changed pos items

(*
 * Resolve a single buffer.
 *)
and resolve_buf sect venv changed pos buf =
   resolve_items sect venv changed pos (buffer_items buf)

(*
 * Resolve a section.
 *)
let resolve_sect venv changed sect =
   let { sect_data = buf;
         sect_name = name
       } = sect
   in
   let venv, changed, pos = resolve_buf name venv changed 0 buf in
   let pos = do_align pos 64 in
      sect.sect_length <- pos;
      venv, changed

(*
 * Resolve the BFD.
 * Compute the fixpoint.
 *)
let resolve_bfd venv bfd =
   let rec step venv changed sects =
      match sects with
         sect :: sects ->
            let venv, changed = resolve_sect venv changed sect in
               step venv changed sects
       | [] ->
            venv, changed
   in
   let rec fixpoint venv index sects =
      let venv, changed = step venv false sects in
         if changed then
            fixpoint venv (succ index) sects
         else
            venv
   in
      fixpoint venv 0 (raw_sections bfd)

(************************************************************************
 * GLOBAL FUNS
 ************************************************************************)

(*
 * Resolve all the sections in the BFD.
 *)
let resolve_bfd bfd =
   let venv = init_venv bfd.bfd_venv bfd in
   let venv = resolve_bfd venv bfd in

   (*
    * The second pass is not strictly necessary,
    * but it catches some edge cases where values
    * are close to interval boundaries.
    *)
   let venv = init_venv venv bfd in
   let venv = resolve_bfd venv bfd in

   (*
    * Sometimes we need even another pass to match gas.
    *)
   let venv =
      if debug debug_gas then
         let venv = init_venv venv bfd in
         let venv = resolve_bfd venv bfd in
            venv
      else
         venv
   in
      bfd.bfd_venv <- venv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
