(*
 * Label environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Bfd
open Bfd_type

(*
 * Basic operations.
 *)
val venv_empty : venv
val venv_add : venv -> symbol -> symbol_info -> venv
val venv_lookup : venv -> symbol -> symbol_info

(*
 * Update function checks if the new value
 * is a change.
 *)
val venv_update : venv -> symbol -> symbol_value -> bool-> venv * bool
val venv_update_preserve : venv -> symbol -> symbol_info -> venv
val venv_update_if_undefined : venv -> symbol -> symbol_info -> venv

(*
 * Iteration.
 *)
val venv_fold : ('a -> symbol -> symbol_info -> 'a) -> 'a -> venv -> 'a

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
