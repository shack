(*
 * Some common utilities.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Bfd
open Bfd_buf
open Bfd_state

(*
 * Arithmetic.
 *)
let log2 i =
   let rec search j =
      let k = (Int32.shift_left Int32.one j) in
         if k = i then
            Int32.of_int j
         else
            search (succ j)
   in
      search 0

let pow2 i =
   Int32.shift_left (Int32.of_int 1) (Int32.to_int i)

(*
 * Test for a value in an interval.
 *)
let interval_test pos interval =
   match pos, interval with
      _, IntAll ->
         true
    | ValAbsolute pos, IntRange (lower, upper) ->
         let test = pos >= lower && pos <= upper in
            if debug debug_bfd_choice then
               eprintf "Testing %s in [%s, %s] (%b)@." (**)
                  (Int32.format "0x%08x" pos)
                  (Int32.format "0x%08x" lower)
                  (Int32.format "0x%08x" upper)
                  test;
            test
    | _ ->
         false

(*
 * Alignment.
 *)
let do_align pos bfd_align =
   let bfd_align = pred bfd_align in
      (pos + bfd_align) land (lnot bfd_align)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
