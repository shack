(*
 * Remove local labels that are never referenced.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Bfd
open Bfd_type
open Bfd_buf
open Bfd_bfd
open Bfd_util
open Bfd_eval
open Bfd_sizeof
open Bfd_state

(************************************************************************
 * LIVENESS
 ************************************************************************)

(*
 * Renaming.
 *)
let live_var = SymbolSet.add
let live_mem = SymbolSet.mem

(*
 * Add each var in the exp.
 *)
let rec live_exp live e =
   match e with
      ExpInt32 _ ->
         live
    | ExpLabel v ->
         live_var live v
    | ExpUMinus e
    | ExpUNot e
    | ExpUAbs e
    | ExpULog2 e
    | ExpUPow2 e ->
         live_exp live e
    | ExpRel (e, l) ->
         live_exp (live_var live l) e
    | ExpAdd (e1, e2)
    | ExpSub (e1, e2)
    | ExpMul (e1, e2)
    | ExpDiv (e1, e2)
    | ExpSal (e1, e2)
    | ExpSar (e1, e2)
    | ExpShr (e1, e2)
    | ExpAnd (e1, e2)
    | ExpOr  (e1, e2)
    | ExpXor (e1, e2)
    | ExpAlign (e1, e2) ->
         live_exp (live_exp live e1) e2

(*
 * Make the choice, and print the buffer.
 *)
let rec live_choice live cases =
   let rec search = function
      choice :: cases ->
         let { choice_enabled = enabled;
               choice_buffer = buf
             } = choice
         in
            if enabled then
               live_buf live buf
            else
               search cases
    | [] ->
         raise (Failure "Bfd_live.live_choice.search")
   in
      search cases

(*
 * Print an item.
 *)
and live_item live item =
   match item with
      BufAlign _
    | BufSkip _
    | BufData _
    | BufAscii _
    | BufInt8 _
    | BufInt16 _
    | BufInt24 _
    | BufInt32 _
    | BufInt64 _
    | BufFloat32 _
    | BufFloat64 _
    | BufFloat80 _
    | BufCommon _
    | BufLabel _
    | BufEqu _ ->
         live
    | BufReloc { reloc_exp = e } ->
         live_exp live e
    | BufChoice (_, cases) ->
         live_choice live cases

and live_items live items =
   List.fold_left live_item live items

and live_buf live buf =
   live_items live (buffer_items buf)

let live_section live sect =
   live_buf live sect.sect_data

(*
 * Produce the object for the BFD.
 *)
let live_bfd bfd =
   List.fold_left live_section SymbolSet.empty (raw_sections bfd)

(************************************************************************
 * DEAD LABEL ELIM
 ************************************************************************)

let dead_items live items =
   let items =
      List.fold_left (fun items item ->
            match item with
               BufLabel (SymLocal, v) when not (live_mem live v) ->
                  items
             | _ ->
                  item :: items) [] items
   in
      items

let dead_section live sect =
   let buf = sect.sect_data in
   let items = buffer_items buf in
   let items = dead_items live items in
      buf.buf_forward_list <- [];
      buf.buf_reverse_list <- items

let dead_bfd bfd =
   let live = live_bfd bfd in
      List.iter (dead_section live) (raw_sections bfd)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
