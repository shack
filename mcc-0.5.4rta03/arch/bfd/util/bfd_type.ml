(*
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Float80

open Bfd

type venv = symbol_info SymbolTable.t

(*
 * Choices.
 * The choice may be disabled during resolution.
 *)
type 'a choice =
   { mutable choice_enabled : bool;
     choice_interval : interval;
     choice_buffer : 'a
   }

(*
 * These are the things that can be placed in a buffer.
 *)
type 'a poly_item =
   (* Control *)
   BufAlign of int                      (* bfd_align next position to mult of arg (power of 2) *)
 | BufSkip of int * int                 (* fill some number bytes with second arg *)

   (* Nested buffers *)
 | BufChoice of exp * 'a choice list       (* Conditional expression *)

   (* Labels and relocation records *)
 | BufLabel of symbol_type * label         (* Define label at this position *)
 | BufEqu of symbol_type * label * exp     (* Define label=exp *)
 | BufCommon of symbol_type * label * int  (* A common symbol with desired size, takes no space *)
 | BufReloc of reloc                       (* Relocation record *)

   (* Data *)
 | BufData of string
 | BufAscii of string
 | BufInt8 of int
 | BufInt16 of int
 | BufInt24 of int
 | BufInt32 of int32
 | BufInt64 of int64

 | BufFloat32 of float80
 | BufFloat64 of float80
 | BufFloat80 of float80

(*
 * A buffer has a list of items.
 *)
type buf =
   { byte_order : byte_order;
     mutable buf_forward_list : buf_item list;
     mutable buf_reverse_list : buf_item list
   }

and buf_item = buf poly_item

(*
 * A file has a table of sections.
 * A "raw" section is used for normal
 * code and data.
 *)
type raw_section =
   { sect_name : symbol;
     sect_byte_order : byte_order;
     sect_flags : section_flag list;
     sect_fill : char;
     sect_data : buf;
     mutable sect_length : int;
     mutable sect_object : string;
     mutable sect_relocs : reloc list
   }

(*
 * A "value" section contains an ML value.
 *)
type 'a val_section =
   { val_name : symbol;
     val_data : 'a;
     mutable val_object : string
   }

(*
 * Different kinds of sections.
 *)
and 'a info =
   RawSection of raw_section
 | ValueSection of 'a val_section

(*
 * A file contains a table of sections.
 *)
and 'a bfd =
   { mutable bfd_sections : 'a info SymbolTable.t;
     mutable bfd_venv : venv;
     bfd_byte_order : byte_order
   }

(*
 * IO versions.
 *)
type buffer_io =
   Buffer of (buffer_io poly_item) list

type section_io =
   { sect_io_name : symbol;
     sect_io_flags : section_flag list;
     sect_io_fill : char;
     sect_io_data : buffer_io
   }

type bfd_io =
   { bfd_io_sections : section_io list;
     bfd_io_byte_order : byte_order
   }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
