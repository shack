(*
 * Wrap it all together.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Bfd

(*
 * Buffer type.
 *)
module Buf =
struct
   type t = Bfd_type.buf
   type item = Bfd_type.buf_item

   (*
    * Creation.
    *)
   let create = Bfd_buf.create_buffer
   let new_buffer = Bfd_buf.new_buffer

   (*
    * Fill.
    *)
   let bfd_align       = Bfd_buf.bfd_align
   let bfd_skip        = Bfd_buf.bfd_skip

   (*
    * Symbols.
    *)
   let bfd_print_label = Bfd_buf.bfd_print_label
   let bfd_print_equ   = Bfd_buf.bfd_print_equ
   let bfd_print_comm  = Bfd_buf.bfd_print_comm
   let bfd_print_reloc = Bfd_buf.bfd_print_reloc

   (*
    * Normal data.
    *)
   let bfd_print_data  = Bfd_buf.bfd_print_data
   let bfd_print_ascii = Bfd_buf.bfd_print_ascii
   let bfd_print_int8  = Bfd_buf.bfd_print_int8
   let bfd_print_int16 = Bfd_buf.bfd_print_int16
   let bfd_print_int24 = Bfd_buf.bfd_print_int24
   let bfd_print_int32 = Bfd_buf.bfd_print_int32
   let bfd_print_int64 = Bfd_buf.bfd_print_int64

   let bfd_print_float32 = Bfd_buf.bfd_print_float32
   let bfd_print_float64 = Bfd_buf.bfd_print_float64
   let bfd_print_float80 = Bfd_buf.bfd_print_float80

   (*
    * Relocatable expressions.
    *)
   let bfd_print_exp8  = Bfd_buf.bfd_print_exp8
   let bfd_print_exp16 = Bfd_buf.bfd_print_exp16
   let bfd_print_exp32 = Bfd_buf.bfd_print_exp32

   (*
    * Choice points.
    *)
   let bfd_print_choice = Bfd_buf.bfd_print_choice
end

module Bfd =
struct
   (*
    * Types.
    *)
   type buf = Bfd_type.buf
   type bfd_io = Bfd_type.bfd_io
   type 'a bfd = 'a Bfd_type.bfd
   type 'a val_section = 'a Bfd_type.val_section
   type raw_section = Bfd_type.raw_section

   (*
    * Create an empty bfd.
    *)
   let create = Bfd_bfd.create_bfd

   (*
    * Link some BFDs.
    *)
   let standardize_bfd_io = Bfd_standardize.standardize_bfd_io
   let link_bfd_io  = Bfd_link.link_bfd_io

   (*
    * Adding sections.
    *
    * A relocatable section contains binary data,
    * with possible relocation information.
    *
    * A "letue" section contains arbitrary letues.
    *)
   let find_raw_section = Bfd_bfd.find_raw_section
   let find_val_section = Bfd_bfd.find_val_section
   let set_val_section = Bfd_bfd.set_val_section

   (*
    * Names of special sections.
    *)
   let absolute_sym = Bfd_bfd.absolute_sym
   let undefined_sym = Bfd_bfd.undefined_sym

   (*
    * Get the section buffer.
    *)
   let section_buf = Bfd_bfd.section_buf
   let section_name = Bfd_bfd.section_name
   let section_flags = Bfd_bfd.section_flags
   let section_object = Bfd_bfd.section_object

   (*
    * Add an undefined symbol.
    *)
   let bfd_print_undef = Bfd_bfd.bfd_print_undef

   (*
    * ELF functions.
    *)
   let bfd_print_elf = Bfd_elf.bfd_print_elf
   let elf_sec_filepos = Bfd_elf.elf_sec_filepos

   (*
    * Conversion to IO forms.
    *)
   let bfd_io_of_bfd = Bfd_bfd.bfd_io_of_bfd
   let bfd_of_bfd_io = Bfd_bfd.bfd_of_bfd_io

   (*
    * Printing.
    *)
   let pp_print_bfd = Bfd_print.pp_print_bfd
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables=
 * Caml-master=
 * End=
 * -*-
 *)
