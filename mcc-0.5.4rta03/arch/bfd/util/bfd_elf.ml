(*
 * Print the BFD to an ELF file.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Bfd
open Bfd_type
open Bfd_buf
open Bfd_bfd
open Bfd_env
open Bfd_eval
open Bfd_resolve
open Bfd_flatten
open Bfd_reloc
open Bfd_state
open Bfd_const
open Bfd_dead

(*
 * BFD functions.
 *)
type bfd
type asection
type asymbol
type arelent

external bfd_openw : string -> bfd = "ml_bfd_openw"
external bfd_close : bfd -> unit = "ml_bfd_close"
external bfd_make_section : bfd -> string -> asection = "ml_bfd_make_section"
external bfd_set_section_flags : bfd -> asection -> int -> unit = "ml_bfd_set_section_flags"
external bfd_set_section_size : bfd -> asection -> int -> unit = "ml_bfd_set_section_size"
external bfd_set_section_contents : bfd -> asection -> string -> unit = "ml_bfd_set_section_contents"
external bfd_new_symbol : bfd -> string -> asection -> int -> int -> asymbol = "ml_bfd_new_symbol"
external bfd_set_symtab : bfd -> asymbol array -> int -> unit = "ml_bfd_set_symtab"
external bfd_new_reloc : bfd -> int -> int -> int -> int -> arelent = "ml_bfd_new_reloc"
external bfd_set_reloc : bfd -> asection -> arelent array -> int -> unit = "ml_bfd_set_reloc"

external bfd_open_sec : string -> string -> int array = "ml_bfd_open_sec"

(************************************************************************
 * SECTION DATA
 ************************************************************************)

(*
 * Convert flags to ELF format.
 *)
let elf_flags flags =
   let rec collect code = function
      flag :: flags ->
         let code =
            match flag with
               SectionAlloc ->
                  code lor elf_sec_alloc lor elf_sec_load lor elf_sec_has_contents
             | SectionWritable ->
                  code land (lnot elf_sec_readonly)
             | SectionExecutable ->
                  code lor elf_sec_code
             | SectionCommon ->
                  code lor elf_sec_is_common
             | SectionReloc ->
                  code lor elf_sec_reloc
             | SectionBuiltin ->
                  code
         in
            collect code flags
    | [] ->
         code
   in
      collect elf_sec_readonly flags

(*
 * Create the raw section and reserve space for the data.
 *)
let reserve_raw_section out senv sect =
   let label = sect.sect_name in
   let name = Symbol.to_string label in
      try
         let flags = elf_flags sect.sect_flags in
         let bsec = bfd_make_section out name in
         let obj = sect.sect_object in
         let obj, size =
            if (flags land elf_sec_has_contents) <> 0 && obj <> "" then
               Some obj, String.length obj
            else
               None, 0
         in
            bfd_set_section_flags out bsec flags;
            bfd_set_section_size out bsec size;
            SymbolTable.add senv label (sect, bsec, flags, obj)
      with
         Failure s ->
            raise (Failure (Printf.sprintf "%s: %s" name s))

(*
 * Create the value section and reserve space for the data.
 *)
let reserve_val_section out senv sect =
   let label = sect.val_name in
   let name = Symbol.to_string label in
      try
         let flags = elf_sec_has_contents in
         let bsec = bfd_make_section out name in
         let obj = sect.val_object in
         let size = String.length obj in
            bfd_set_section_flags out bsec flags;
            bfd_set_section_size out bsec size;
            SymbolTable.add senv label (bsec, obj)
      with
         Failure s ->
            raise (Failure (Printf.sprintf "%s: %s" name s))

(*
 * Add the section contents.
 *)
let output_raw_section_contents out _ (_, bsec, _, obj) =
   match obj with
      Some obj ->
         bfd_set_section_contents out bsec obj
    | None ->
         ()

let output_val_section_contents out _ (bsec, obj) =
   bfd_set_section_contents out bsec obj

(*
 * Print section data.
 * First reserve space, then output the data.
 *)
let reserve_raw_sections out bfd =
   List.fold_left (reserve_raw_section out) SymbolTable.empty (raw_sections bfd)

let reserve_val_sections out bfd =
   List.fold_left (reserve_val_section out) SymbolTable.empty (val_sections bfd)

let output_raw_sections out senv =
   SymbolTable.iter (output_raw_section_contents out) senv

let output_val_sections out senv =
   SymbolTable.iter (output_val_section_contents out) senv

(************************************************************************
 * SYMBOL TABLE
 ************************************************************************)

(*
 * Create the section symbols.
 *)
let section_symbol out symtab _ (sect, bsec, _, _) =
   let v = sect.sect_name in
   let sname = Symbol.to_string v in
   let sym = bfd_new_symbol out sname bsec elf_bsf_section 0 in
      (v, sym) :: symtab

(*
 * Create all the BFD symbols
 * and add them to a list.
 *)
let new_symbol out senv symtab v info =
   let { sym_type = sym_type;
         sym_value = pos;
         sym_section = secname
       } = info
   in
   let pos =
      match pos with
         ValAbsolute pos
       | ValSection (_, pos) ->
            Int32.to_int pos
       | ValLabel _
       | ValRelative _
       | ValUndefined ->
            0
   in
      match sym_type with
         SymTemp ->
            symtab
       | SymUndefined ->
            let gname = Symbol.to_string v in
            let _, bsec, _, _ = SymbolTable.find senv secname in
            let gsym = bfd_new_symbol out gname bsec elf_bsf_global pos in
               (v, gsym) :: symtab
       | SymGlobal ->
            let gname = Symbol.to_string v in
            let lname = Symbol.string_of_symbol v in
            let _, bsec, _, _ = SymbolTable.find senv secname in
            let lsym = bfd_new_symbol out lname bsec elf_bsf_local pos in
            let gsym = bfd_new_symbol out gname bsec elf_bsf_global pos in
               (v, lsym) :: (v, gsym) :: symtab
       | SymLocal ->
            let lname = Symbol.string_of_symbol v in
            let _, bsec, _, _ = SymbolTable.find senv secname in
            let sym = bfd_new_symbol out lname bsec elf_bsf_local pos in
               (v, sym) :: symtab

(*
 * Build the index table for relocations.
 *)
let build_symtab symtab =
   let ienv, _ =
      List.fold_left (fun (ienv, i) (v, _) ->
            let ienv = SymbolTable.add ienv v i in
               ienv, succ i) (SymbolTable.empty, 0) symtab
   in
   let symtab = List.map snd symtab in
   let symtab = Array.of_list symtab in
      ienv, symtab

(*
 * Reserve and save data for all sections.
 *)
let bfd_print_symtab out bfd senv =
   let symtab = SymbolTable.fold (section_symbol out) [] senv in
   let symtab = venv_fold (new_symbol out senv) symtab bfd.bfd_venv in
   let ienv, symtab = build_symtab symtab in
      bfd_set_symtab out symtab (Array.length symtab);
      ienv

(************************************************************************
 * RELOCATIONS
 ************************************************************************)

(*
 * Add a single relocation.
 *)
let build_reloc out venv ienv sect bsec relocs reloc =
   let { reloc_type = reloc_type;
         reloc_pos = pos;
         reloc_exp = exp
       } = reloc
   in

   (* Get the relocation value *)
   let lazy_val = eval_lazy_exp venv exp in
   let eager_val = eval_eager_value venv lazy_val in
   let info =
      match eager_val, lazy_val with
         ValAbsolute _, _
       | _, ValAbsolute _ ->
            if debug debug_bfd_reloc then
               eprintf "Absolute relocation omitted@.";
            None
       | ValSection (v, i), _ when debug debug_gas ->
            (*
             * GAS likes to issue relocations relative to the
             * section symbol.  If the gas switch is on, the ELF
             * output relocations will point to the start of the
             * section, and the field will contain the offset
             * to the real label.
             *)
            bfd_print_reloc_value sect reloc_type pos i;
            Some (v, Int32.zero, false)
       | _, ValLabel (v, addend) ->
            Some (v, addend, false)
       | _, ValRelative (v, _, addend) ->
            Some (v, addend, true)
       | _, ValSection _
       | _, ValUndefined ->
            raise (Invalid_argument "Bfd_elf.bfd_print_reloc: illegal relocation expression")
   in
      match info with
         Some (v, addend, rel) ->
            let howto =
               match reloc_type, rel with
                  RelocInt8,  false -> ml_reloc_8
                | RelocInt8,  true  -> ml_reloc_8_pcrel
                | RelocInt16, false -> ml_reloc_16
                | RelocInt16, true  -> ml_reloc_16_pcrel
                | RelocInt24, false -> ml_reloc_24
                | RelocInt24, true  -> ml_reloc_24_pcrel
                | RelocInt32, false -> ml_reloc_32
                | RelocInt32, true  -> ml_reloc_32_pcrel
                | RelocInt64, false -> ml_reloc_64
                | RelocInt64, true  -> ml_reloc_64_pcrel
            in
            let vindex =
               try SymbolTable.find ienv v with
                  Not_found ->
                     raise (Invalid_argument ("Bfd_elf.bfd_print_reloc: undefined var " ^ string_of_symbol v))
            in
            let addend = Int32.to_int addend in
            let reloc = bfd_new_reloc out howto vindex pos addend in
               reloc :: relocs
       | None ->
            relocs

(*
 * Add all the relocations in this section.
 *)
let reloc_section out venv ienv sect bsec =
   let relocs = List.fold_left (build_reloc out venv ienv sect bsec) [] sect.sect_relocs in
   let relocs = Array.of_list (List.rev relocs) in
      bfd_set_reloc out bsec relocs (Array.length relocs)

let reloc_sections out venv senv ienv =
   SymbolTable.iter (fun _ (sect, bsec, flags, _) ->
         if (flags land elf_sec_reloc) <> 0 then
            reloc_section out venv ienv sect bsec) senv

(************************************************************************
 * MAIN
 ************************************************************************)

(*
 * Create the ELF.
 *)
let bfd_print_elf name bfd =
   (* Remove dead labels *)
   if not !debug_gas then
      dead_bfd bfd;

   (* Symbol resolution *)
   resolve_bfd bfd;

   (* Create data *)
   flatten_bfd bfd;

   (* Perform as much relocation as possible *)
   relocate_bfd bfd;

   (* Open the file *)
   let out = bfd_openw name in

   (* Reserve space for all of the sections *)
   let raw_senv = reserve_raw_sections out bfd in
   let val_senv = reserve_val_sections out bfd in

   (* Print the symbol table, and get the symbol index table *)
   let ienv = bfd_print_symtab out bfd raw_senv in

      (* Save the relocations *)
      reloc_sections out bfd.bfd_venv raw_senv ienv;

      (* Save the section contents *)
      output_raw_sections out raw_senv;
      output_val_sections out val_senv;

      (* Finally, we can close the file *)
      bfd_close out

(*
 * Open a BFD section for reading.
 *)
let elf_sec_filepos filename secname =
   bfd_open_sec filename secname

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
