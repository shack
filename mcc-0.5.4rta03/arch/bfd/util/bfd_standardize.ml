(*
 * Rename all the non-global vars in the BFD.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Bfd
open Bfd_buf
open Bfd_env
open Bfd_type

(*
 * Lookup a variable.
 * Not_found should not happen.
 *)
let venv_find venv v =
   try venv, SymbolTable.find venv v with
      Not_found ->
         let v' = new_symbol v in
         let venv = SymbolTable.add venv v v' in
            venv, v'

(*
 * Standardize symbol_value.
 * Rename all mentioned symbols.
 *)
let standardize_symbol_value venv symv =
   match symv with
      ValSection (v, i) ->
         let venv, v = venv_find venv v in
            venv, ValSection (v, i)
    | ValLabel (v, i) ->
         let venv, v = venv_find venv v in
            venv, ValLabel (v, i)
    | ValRelative (v1, v2, i) ->
         let venv, v1 = venv_find venv v1 in
         let venv, v2 = venv_find venv v2 in
            venv, ValRelative (v1, v2, i)
    | ValAbsolute _
    | ValUndefined ->
         venv, symv

(*
 * Standardize symbol_info.
 * We have to rename the section and the value.
 *)
let standardize_symbol_info venv info =
   let { sym_section = section;
         sym_type = sym_type;
         sym_value = sym_value
       } = info
   in
   let venv, section = venv_find venv section in
   let venv, sym_value = standardize_symbol_value venv sym_value in
   let info =
      { sym_section = section;
        sym_type = sym_type;
        sym_value = sym_value
      }
   in
      venv, info

(*
 * Standardize an expression.
 * Search for ExpLabel.
 *)
let rec standardize_exp venv e =
   match e with
      ExpInt32 _ ->
         venv, e
    | ExpLabel v ->
         let venv, v = venv_find venv v in
            venv, ExpLabel v
    | ExpUMinus e ->
         let venv, e = standardize_exp venv e in
            venv, ExpUMinus e
    | ExpUNot e ->
         let venv, e = standardize_exp venv e in
            venv, ExpUNot e
    | ExpUAbs e ->
         let venv, e = standardize_exp venv e in
            venv, ExpUAbs e
    | ExpULog2 e ->
         let venv, e = standardize_exp venv e in
            venv, ExpULog2 e
    | ExpUPow2 e ->
         let venv, e = standardize_exp venv e in
            venv, ExpUPow2 e
    | ExpAdd (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpAdd (e1, e2)
    | ExpSub (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpSub (e1, e2)
    | ExpRel (e, l) ->
         let venv, e = standardize_exp venv e in
         let venv, l = venv_find venv l in
            venv, ExpRel (e, l)
    | ExpMul (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpMul (e1, e2)
    | ExpDiv (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpDiv (e1, e2)
    | ExpSal (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpSal (e1, e2)
    | ExpSar (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpSar (e1, e2)
    | ExpShr (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpShr (e1, e2)
    | ExpAnd (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpAnd (e1, e2)
    | ExpOr (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpOr (e1, e2)
    | ExpXor (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpXor (e1, e2)
    | ExpAlign (e1, e2) ->
         let venv, e1 = standardize_exp venv e1 in
         let venv, e2 = standardize_exp venv e2 in
            venv, ExpAlign (e1, e2)

(*
 * Standardize the reloc.
 *)
let standardize_reloc venv reloc =
   let venv, e = standardize_exp venv reloc.reloc_exp in
      venv, { reloc with reloc_exp = e }

(*
 * Standardize a buffer item.
 * Rename all symbols we can find.
 *)
let rec standardize_item venv item =
   match item with
      BufLabel (sym_type, v) ->
         let venv, v = venv_find venv v in
            venv, BufLabel (sym_type, v)
    | BufEqu (sym_type, v, e) ->
         let venv, v = venv_find venv v in
         let venv, e = standardize_exp venv e in
            venv, BufEqu (sym_type, v, e)
    | BufCommon (sym_type, v, i) ->
         let venv, v = venv_find venv v in
            venv, BufCommon (sym_type, v, i)
    | BufReloc reloc ->
         let venv, reloc = standardize_reloc venv reloc in
            venv, BufReloc reloc
    | BufChoice (e, choices) ->
         let venv, e = standardize_exp venv e in
         let venv, choices =
            List.fold_left (fun (venv, choices) choice ->
                  let venv, buf = standardize_buffer_io venv choice.choice_buffer in
                  let choice = { choice with choice_buffer = buf } in
                  let choices = choice :: choices in
                     venv, choices) (venv, []) choices
         in
            venv, BufChoice (e, List.rev choices)
    | BufAlign _
    | BufSkip _
    | BufData _
    | BufAscii _
    | BufInt8 _
    | BufInt16 _
    | BufInt24 _
    | BufInt32 _
    | BufInt64 _
    | BufFloat32 _
    | BufFloat64 _
    | BufFloat80 _ ->
         venv, item

(*
 * Standardize a buffer.
 * Rename all the items.
 *)
and standardize_buffer_io venv (Buffer items) =
   let venv, items =
      List.fold_left (fun (venv, items) item ->
            let venv, item = standardize_item venv item in
               venv, item :: items) (venv, []) items
   in
      venv, Buffer (List.rev items)

(*
 * Standardize a section.
 * Rename the section name, the buffer.
 * Clear out the rest of the data.
 *)
let standardize_section_io venv sect =
   let { sect_io_name = name;
         sect_io_data = buf
       } = sect
   in
   let name = Symbol.add (Symbol.to_string name) in
   let venv, buf = standardize_buffer_io venv buf in
   let sect =
      { sect with sect_io_name = name;
                  sect_io_data = buf
      }
   in
      venv, sect

(*
 * Standardize the entire BFD.
 * Build the renaming environment from the
 * symbol names and the symtab.
 *)
let standardize_bfd_io venv bfd =
   let { bfd_io_sections = sections } = bfd in
   let venv, sections =
      List.fold_left (fun (venv, sections) sect ->
            let venv, sect = standardize_section_io venv sect in
               venv, sect :: sections) (venv, []) sections
   in
      venv, { bfd with bfd_io_sections = List.rev sections }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
