(*
 * Expression evaluation.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fir_exn
open Fir_pos

open Bfd
open Bfd_env
open Bfd_buf
open Bfd_util
open Bfd_state

module Pos = MakePos (struct let name = "Bfd_eval" end)
open Pos

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * For expressions that cant be evaluated yet.
 *)
exception CantEval

(*
 * Some int32 arithmetic.
 *)
let shift_left i1 i2 =
   Int32.shift_left i1 (Int32.to_int i2)

let shift_right i1 i2 =
   Int32.shift_right i1 (Int32.to_int i2)

let shift_right_logical i1 i2 =
   Int32.shift_right_logical i1 (Int32.to_int i2)

let bfd_align i1 i2 =
   let i2 = Int32.pred i2 in
      Int32.logand (Int32.add i1 i2) (Int32.lognot i2)

(************************************************************************
 * LAZY EVALUATION
 ************************************************************************)

(*
 * Lazy evaluation doesn't try too hard.
 * Resolving to ValLabel, ValRelative is ok.
 *)

(*
 * Apply a unary expression.
 *)
let rec lazy_unary venv op e =
   match eager_exp venv e with
      ValAbsolute i ->
         ValAbsolute (op i)
    | ValSection _
    | ValLabel _
    | ValRelative _
    | ValUndefined ->
         raise CantEval

(*
 * Apply a binary expression.
 * Evaluation is only allowed if the expressions
 * are absolute numbers.
 *)
and lazy_binary venv op e1 e2 =
   match eager_exp venv e1, eager_exp venv e2 with
      ValAbsolute i1, ValAbsolute i2 ->
         ValAbsolute (op i1 i2)
    | _ ->
         raise CantEval

(*
 * For addition, it is also allowed to
 * add a constant to a relative expression.
 *)
and lazy_binary_add venv op e1 e2 =
   match lazy_exp venv e1, eager_exp venv e2 with
      ValAbsolute i1, ValAbsolute i2 ->
         ValAbsolute (op i1 i2)
    | ValSection (sect, i1), ValAbsolute i2 ->
         ValSection (sect, op i1 i2)
    | ValLabel (v, i1), ValAbsolute i2 ->
         ValLabel (v, op i1 i2)
    | ValRelative (v1, v2, i1), ValAbsolute i2 ->
         ValRelative (v1, v2, op i1 i2)
    | _ ->
         raise CantEval

(*
 * For differences, evaluation can also be carried out if
 * the two expressions are both offsets into a section.
 *)
and lazy_binary_sub venv op e1 e2 =
   match eager_exp venv e1, eager_exp venv e2 with
      ValAbsolute i1, ValAbsolute i2 ->
         ValAbsolute (op i1 i2)
    | ValSection (sect, i1), ValAbsolute i2 ->
         ValSection (sect, op i1 i2)
    | ValSection (sect1, i1), ValSection (sect2, i2)
      when Symbol.eq sect1 sect2 ->
         ValAbsolute (op i1 i2)
    | _ ->
         raise CantEval

(*
 * Relative addresses.
 *)
and lazy_binary_rel venv op e l2 =
   match lazy_exp venv e with
      ValAbsolute i1 ->
         (match eager_label SymbolSet.empty venv l2 with
             ValAbsolute i2 ->
                ValAbsolute (op i1 i2)
           | _ ->
                raise CantEval)
    | ValSection (sect1, i1) ->
         (match eager_label SymbolSet.empty venv l2 with
             ValAbsolute i2 ->
                ValAbsolute (op i1 i2)
           | ValSection (sect2, i2)
             when Symbol.eq sect1 sect2 ->
                ValAbsolute (op i1 i2)
           | _ ->
                raise CantEval)
    | ValLabel (l1, i1) ->
         ValRelative (l1, l2, i1)
    | _ ->
         raise CantEval

(*
 * Lazy evaluation is happy resolving to labels.
 *)
and lazy_exp venv e =
   match e with
      ExpInt32 i ->
         ValAbsolute i
    | ExpLabel v ->
         ValLabel (v, Int32.zero)
    | ExpUMinus e ->
         lazy_unary venv Int32.neg e
    | ExpUNot e ->
         lazy_unary venv Int32.lognot e
    | ExpUAbs e ->
         lazy_unary venv Int32.abs e
    | ExpULog2 e ->
         lazy_unary venv log2 e
    | ExpUPow2 e ->
         lazy_unary venv pow2 e
    | ExpAdd (e1, e2) ->
         lazy_binary_add venv Int32.add e1 e2
    | ExpSub (e1, e2) ->
         lazy_binary_sub venv Int32.sub e1 e2
    | ExpRel (e, l) ->
         lazy_binary_rel venv Int32.sub e l
    | ExpMul (e1, e2) ->
         lazy_binary venv Int32.mul e1 e2
    | ExpDiv (e1, e2) ->
         lazy_binary venv Int32.div e1 e2
    | ExpSal (e1, e2) ->
         lazy_binary venv shift_left e1 e2
    | ExpSar (e1, e2) ->
         lazy_binary venv shift_right e1 e2
    | ExpShr (e1, e2) ->
         lazy_binary venv shift_right_logical e1 e2
    | ExpAnd (e1, e2) ->
         lazy_binary venv Int32.logand e1 e2
    | ExpOr  (e1, e2) ->
         lazy_binary venv Int32.logor e1 e2
    | ExpXor (e1, e2) ->
         lazy_binary venv Int32.logxor e1 e2
    | ExpAlign (e1, e2) ->
         lazy_binary venv bfd_align e1 e2

(************************************************************************
 * EAGER EVALUATION
 ************************************************************************)

(*
 * Eager evaluation tries harder.
 * It is _not_ acceptable to evaluate to
 * a ValLabel or ValRelative.
 *)

(*
 * Resolve the value of a variable.
 *)
and eager_label vars venv v =
   let vars =
      if SymbolSet.mem vars v then
         raise (Invalid_argument ("Bfd_eval: recursive definition of symbol " ^ string_of_symbol v));
      SymbolSet.add vars v
   in
   let info =
      try venv_lookup venv v with
         Not_found ->
            let pos = var_exp_pos v in
            let pos = string_pos "eager_label" pos in
               raise (FirException (pos, UnboundVar v))
   in
      eager_value vars venv info.sym_value

(*
 * Try hard to resolve a value.
 *)
and eager_value vars venv v =
   match v with
      ValAbsolute _
    | ValSection _ ->
         v
    | ValLabel (v, i2) ->
         (match eager_label vars venv v with
             ValAbsolute i1 ->
                ValAbsolute (Int32.add i1 i2)
           | ValSection (sect, i1) ->
                ValSection (sect, Int32.add i1 i2)
           | _ ->
                raise CantEval)
    | ValRelative (v1, v2, i3) ->
         (match eager_label vars venv v1, eager_label vars venv v2 with
             ValAbsolute i1, ValAbsolute i2 ->
                ValAbsolute (Int32.add (Int32.sub i1 i2) i3)
           | ValSection (sect, i1), ValAbsolute i2 ->
                ValSection (sect, Int32.add (Int32.sub i1 i2) i3)
           | ValSection (sect1, i1), ValSection (sect2, i2)
             when Symbol.eq sect1 sect2 ->
                ValAbsolute (Int32.add (Int32.sub i1 i2) i3)
           | _ ->
                raise CantEval)
    | ValUndefined ->
         raise CantEval

(*
 * Eager evalution tries harder.
 * It never evaluates to ValLabel, ValRelative, or ValUndefined.
 *)
and eager_exp venv e =
   eager_value SymbolSet.empty venv (lazy_exp venv e)

(************************************************************************
 * GLOBAL WRAPPERS
 ************************************************************************)

(*
 * Wrapper catches the Not_found case.
 *)
let eval_eager_exp venv e =
   try eager_exp venv e with
      CantEval ->
         ValUndefined

let eval_eager_value venv v =
   try eager_value SymbolSet.empty venv v with
      CantEval ->
         ValUndefined

let eval_lazy_exp venv e =
   try lazy_exp venv e with
      CantEval ->
         ValUndefined

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
