(*
 * Once the label resolution has been performed,
 * build the actual binary data.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Symbol

open Bfd
open Bfd_type
open Bfd_buf
open Bfd_bfd
open Bfd_util
open Bfd_eval
open Bfd_sizeof
open Bfd_state

(*
 * Raw printers.
 *)
let bfd_print_int8 data pos i =
   data.[pos] <- Char.chr (i land 0xff)

let bfd_print_int16 data pos i =
   data.[pos + 0] <- Char.chr ((i lsr 0) land 0xff);
   data.[pos + 1] <- Char.chr ((i lsr 8) land 0xff)

let bfd_print_int24 data pos i =
   data.[pos + 0] <- Char.chr ((i lsr 0) land 0xff);
   data.[pos + 1] <- Char.chr ((i lsr 8) land 0xff);
   data.[pos + 2] <- Char.chr ((i lsr 16) land 0xff)

let bfd_print_int32 data pos i =
   data.[pos + 0] <- Char.chr ((Int32.to_int (Int32.shift_right i 0)) land 0xff);
   data.[pos + 1] <- Char.chr ((Int32.to_int (Int32.shift_right i 8)) land 0xff);
   data.[pos + 2] <- Char.chr ((Int32.to_int (Int32.shift_right i 16)) land 0xff);
   data.[pos + 3] <- Char.chr ((Int32.to_int (Int32.shift_right i 24)) land 0xff)

let bfd_print_int64 data pos i =
   data.[pos + 0] <- Char.chr ((Int64.to_int (Int64.shift_right i 0)) land 0xff);
   data.[pos + 1] <- Char.chr ((Int64.to_int (Int64.shift_right i 8)) land 0xff);
   data.[pos + 2] <- Char.chr ((Int64.to_int (Int64.shift_right i 16)) land 0xff);
   data.[pos + 3] <- Char.chr ((Int64.to_int (Int64.shift_right i 24)) land 0xff);
   data.[pos + 4] <- Char.chr ((Int64.to_int (Int64.shift_right i 32)) land 0xff);
   data.[pos + 5] <- Char.chr ((Int64.to_int (Int64.shift_right i 40)) land 0xff);
   data.[pos + 6] <- Char.chr ((Int64.to_int (Int64.shift_right i 48)) land 0xff);
   data.[pos + 7] <- Char.chr ((Int64.to_int (Int64.shift_right i 56)) land 0xff)

(*
 * Make the choice, and print the buffer.
 *)
let rec bfd_print_choice venv data pos e cases =
   let rec search = function
      choice :: cases ->
         let { choice_enabled = enabled;
               choice_interval = interval;
               choice_buffer = buf
             } = choice
         in
            if enabled then
               begin
                  assert (interval_test (eval_eager_exp venv e) interval);
                  bfd_print_buf venv data pos buf
               end
            else
               begin
                  if debug debug_bfd_choice && interval_test (eval_eager_exp venv e) interval then
                     eprintf "Bfd_flatten.bfd_print_choice: choice was mistaken@.";
                  search cases
               end
    | [] ->
         raise (Failure "Bfd_flatten.bfd_print_item.search")
   in
      search cases

(*
 * Print an item.
 *)
and bfd_print_item venv data pos item =
   match item with
      BufAlign size ->
         do_align pos size
    | BufSkip (bfd_skip, fill) ->
         let fill = Char.chr fill in
            for i = 0 to pred bfd_skip do
               data.[pos + i] <- fill
            done;
            pos + bfd_skip
    | BufData s ->
         let len = String.length s in
            String.blit s 0 data pos len;
            pos + len
    | BufAscii s ->
         let len = String.length s in
            String.blit s 0 data pos len;
            data.[pos + len] <- '\000';
            pos + len + 1
    | BufInt8 i ->
         bfd_print_int8 data pos i;
         pos + sizeof_int8
    | BufInt16 i ->
         bfd_print_int16 data pos i;
         pos + sizeof_int16
    | BufInt24 i ->
         bfd_print_int24 data pos i;
         pos + sizeof_int24
    | BufInt32 i ->
         bfd_print_int32 data pos i;
         pos + sizeof_int32
    | BufInt64 i ->
         bfd_print_int64 data pos i;
         pos + sizeof_int64
    | BufFloat32 x ->
         Float80.blit_float32 x data pos;
         pos + sizeof_float32
    | BufFloat64 x ->
         Float80.blit_float64 x data pos;
         pos + sizeof_float64
    | BufFloat80 x ->
         Float80.blit_float80 x data pos;
         pos + sizeof_float80
    | BufCommon (_, _, size) ->
         pos + size
    | BufLabel _
    | BufEqu _ ->
         pos
    | BufReloc reloc ->
         reloc.reloc_pos <- reloc.reloc_offset + pos;
         pos
    | BufChoice (e, cases) ->
         bfd_print_choice venv data pos e cases

and bfd_print_items venv data pos items =
   List.fold_left (bfd_print_item venv data) pos items

and bfd_print_buf venv data pos buf =
   bfd_print_items venv data pos (buffer_items buf)

(*
 * Print the data for a section.
 *)
let bfd_print_raw_section venv sect =
   let { sect_name = name;
         sect_length = len;
         sect_fill = fill
       } = sect
   in
   let data = String.make len fill in
   let len' = bfd_print_buf venv data 0 sect.sect_data in
   let len' = do_align len' 64 in
      assert (len' = len);
      sect.sect_object <- data

(*
 * Marshal a value section.
 *)
let bfd_print_value_section sect =
   let { val_data = data } = sect in
      sect.val_object <- Marshal.to_string data []

(*
 * Produce the object for the BFD.
 *)
let flatten_bfd bfd =
   List.iter (bfd_print_raw_section bfd.bfd_venv) (raw_sections bfd);
   List.iter bfd_print_value_section (val_sections bfd)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
