(*
 * BFD linking.
 * The main thing we have to do here is
 * connect all the Undefined to Global definitions.
 * Fortunately, the Symbol table does this for us.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Bfd
open Bfd_type

(*
 * Add the section to the table.
 *)
let sect_add sectab sect2 =
   SymbolTable.filter_add sectab sect2.sect_io_name (function
      Some sect1 ->
         let Buffer data1 = sect1.sect_io_data in
         let Buffer data2 = sect2.sect_io_data in
         let data = Buffer (data1 @ data2) in
            { sect1 with sect_io_data = data }
    | None ->
         sect2)

(*
 * Concatenate the io sections.
 *)
let link_bfd_io bfds =
   let byte_order =
      match bfds with
         bfd :: _ ->
            bfd.bfd_io_byte_order
       | [] ->
            LittleEndian
   in
   let sectab =
      List.fold_left (fun sectab bfd ->
            List.fold_left sect_add sectab bfd.bfd_io_sections) SymbolTable.empty bfds
   in
   let sections =
      SymbolTable.fold (fun sections _ sect ->
            sect :: sections) [] sectab
   in
      { bfd_io_sections = List.rev sections;
        bfd_io_byte_order = byte_order
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
