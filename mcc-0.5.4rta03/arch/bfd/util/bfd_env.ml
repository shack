(*
 * Label environments.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Bfd
open Bfd_type
open Bfd_print
open Bfd_state

let venv_empty =
   SymbolTable.empty

let venv_add = SymbolTable.add

let venv_lookup = SymbolTable.find

let venv_fold = SymbolTable.fold

(*
 * Update function also checks if the value has
 * changed.
 *)
exception Unchanged

let venv_update venv v i changed =
   let add = function
      Some info ->
         let j = info.sym_value in
            if i = j then
               raise Unchanged;
            if debug debug_bfd then
               Format.printf "@[<hv 0>Changing %s from @ %a to @ %a@]@." (**)
                  (string_of_symbol v)
                  pp_print_value j
                  pp_print_value i;
            { info with sym_value = i }
    | None ->
         raise (Invalid_argument "venv_update")
   in
      try
         let venv = SymbolTable.filter_add venv v add in
            venv, true
      with
         Unchanged ->
            venv, changed

let venv_update_preserve venv v info =
   let add = function
      Some _ ->
         raise Unchanged
    | None ->
         info
   in
      try SymbolTable.filter_add venv v add with
         Unchanged ->
            venv

let venv_update_if_undefined venv v info =
   let add = function
      Some info' ->
         (match info'.sym_type, info.sym_type with
             SymUndefined, _ ->
                info
           | _, SymUndefined ->
                raise Unchanged
           | _ ->
                raise (Invalid_argument ("bfd_env: duplicate symbol definition for " ^ string_of_symbol v)))
    | None ->
         info
   in
      try SymbolTable.filter_add venv v add with
         Unchanged ->
            venv

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
