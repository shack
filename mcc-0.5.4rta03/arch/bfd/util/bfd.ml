(*
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Float80
open Symbol
open Format

(*
 * Labels.
 *)
type label = symbol

(*
 * Symbol flags.
 *)
type symbol_type =
   SymGlobal
 | SymUndefined
 | SymLocal
 | SymTemp

(*
 * Possible values for a symbol.
 *
 * For ValSection, the first symbol is the name of the
 * section, and the second symbol is the original symbol name.
 *)
type symbol_value =
   ValAbsolute of int32
 | ValSection of symbol * int32
 | ValLabel of symbol * int32
 | ValRelative of symbol * symbol * int32
 | ValUndefined

(*
 * Symbol info.
 *)
type symbol_info =
   { sym_section : symbol;
     sym_type : symbol_type;
     sym_value : symbol_value
   }

(*
 * Byte ordering.
 *)
type byte_order =
   LittleEndian
 | BigEndian

(*
 * An expression is basic arithmetic on
 * integers and labels.  ExpRel is the same
 * as ExpSub, but the label is guaranteed to
 * be the label of the next instruction.
 *)
type exp =
   ExpInt32 of int32
 | ExpLabel of label
 | ExpUMinus of exp
 | ExpUNot   of exp
 | ExpUAbs   of exp
 | ExpULog2  of exp
 | ExpUPow2  of exp
 | ExpAdd of exp * exp
 | ExpSub of exp * exp
 | ExpRel of exp * label        (* See above *)
 | ExpMul of exp * exp
 | ExpDiv of exp * exp
 | ExpSal of exp * exp
 | ExpSar of exp * exp
 | ExpShr of exp * exp
 | ExpAnd of exp * exp
 | ExpOr  of exp * exp
 | ExpXor of exp * exp
 | ExpAlign of exp * exp

(*
 * Decriptions of how to store the
 * relocation value.
 *)
type reloc_type =
   RelocInt8
 | RelocInt16
 | RelocInt24
 | RelocInt32
 | RelocInt64

(*
 * A relocation record indicates how to represent
 * the label value.  Note, relocations take zero space,
 * they will be followed by additional data.  The offset
 * is relative to the current location.  The position
 * is the absolute position in the section, set during
 * the flatten phase.
 *)
type reloc =
   { reloc_type : reloc_type;
     mutable reloc_pos : int;
     reloc_offset : int;
     reloc_exp : exp
   }

(*
 * Intervals are used for testing expression values.
 *)
type interval =
   IntAll
 | IntRange of int32 * int32

(*
 * Section flags.
 *)
type section_flag =
   SectionCommon
 | SectionAlloc
 | SectionWritable
 | SectionExecutable
 | SectionBuiltin
 | SectionReloc

(*
 * BFD substitutions.
 *)
type subst = symbol SymbolTable.t

(*
 * Buffer type.
 *)
module type BufSig =
sig
   type t
   type item

   (*
    * Creation.
    *)
   val create : byte_order -> t
   val new_buffer : t -> t

   (*
    * Fill.
    *)
   val bfd_align       : t -> int -> unit
   val bfd_skip        : t -> int -> int -> unit

   (*
    * Symbols.
    *)
   val bfd_print_label : t -> symbol_type -> label -> unit
   val bfd_print_equ   : t -> symbol_type -> label -> exp -> unit
   val bfd_print_comm  : t -> symbol_type -> label -> int -> unit
   val bfd_print_reloc : t -> reloc -> unit

   (*
    * Normal data.
    *)
   val bfd_print_data  : t -> string -> unit
   val bfd_print_ascii : t -> string -> unit
   val bfd_print_int8  : t -> int -> unit
   val bfd_print_int16 : t -> int -> unit
   val bfd_print_int24 : t -> int -> unit
   val bfd_print_int32 : t -> int32 -> unit
   val bfd_print_int64 : t -> int64 -> unit

   val bfd_print_float32 : t -> float80 -> unit
   val bfd_print_float64 : t -> float80 -> unit
   val bfd_print_float80 : t -> float80 -> unit

   (*
    * Relocatable expressions.
    *)
   val bfd_print_exp8  : t -> exp -> unit
   val bfd_print_exp16 : t -> exp -> unit
   val bfd_print_exp32 : t -> exp -> unit

   (*
    * Choice points.
    *)
   val bfd_print_choice : t -> exp -> (interval * t) list -> unit
end

(*
 * Binary file descriptor.
 *)
module type BfdSig =
sig
   type buf
   type 'a bfd
   type bfd_io
   type 'a val_section
   type raw_section

   (*
    * Create an empty bfd.
    *)
   val create : byte_order -> 'a bfd

   (*
    * Linking functions.
    *)
   val standardize_bfd_io : subst -> bfd_io -> subst * bfd_io
   val link_bfd_io : bfd_io list -> bfd_io

   (*
    * Adding sections.
    *
    * A relocatable section contains binary data,
    * with possible relocation information.
    *
    * A "value" section contains arbitrary values.
    *)
   val find_raw_section : 'a bfd -> symbol -> section_flag list -> int -> raw_section

   val find_val_section : 'a bfd -> symbol -> 'a val_section
   val set_val_section : 'a bfd -> symbol -> 'a -> unit

   (*
    * Names of special sections.
    *)
   val absolute_sym : symbol
   val undefined_sym : symbol

   (*
    * Get the section buffer.
    *)
   val section_buf : raw_section -> buf
   val section_name : raw_section -> symbol
   val section_flags : raw_section -> section_flag list
   val section_object : raw_section -> string

   (*
    * Add an undefined symbol.
    *)
   val bfd_print_undef : 'a bfd -> raw_section -> symbol -> unit

   (*
    * Print the BFD to an ELF file.
    *)
   val bfd_print_elf : string -> 'a bfd -> unit
   val elf_sec_filepos : string -> string -> int array

   (*
    * Conversion to IO form.
    *)
   val bfd_io_of_bfd : 'a bfd -> bfd_io
   val bfd_of_bfd_io : bfd_io -> 'a bfd

   (*
    * Debugging.
    *)
   val pp_print_bfd : formatter -> 'a bfd -> unit
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
