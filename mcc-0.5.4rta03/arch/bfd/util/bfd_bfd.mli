(*
 * Basic BFD implementation.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Bfd
open Bfd_type

(*
 * Create an empty bfd.
 *)
val create_bfd : byte_order -> 'a bfd

(*
 * Adding sections.
 *
 * A relocatable section contains binary data,
 * with possible relocation information.
 *
 * A "value" section contains arbitrary values.
 *)
val find_raw_section : 'a bfd -> symbol -> section_flag list -> int -> raw_section
val find_val_section : 'a bfd -> symbol -> 'a val_section
val set_val_section : 'a bfd -> symbol -> 'a -> unit

(*
 * Get the sections.
 *)
val raw_sections : 'a bfd -> raw_section list
val val_sections : 'a bfd -> 'a val_section list

(*
 * Names of special sections.
 *)
val absolute_sym : symbol
val undefined_sym : symbol

(*
 * Get the section buffer.
 *)
val section_buf : raw_section -> buf
val section_name : raw_section -> symbol
val section_flags : raw_section -> section_flag list
val section_object : raw_section -> string

(*
 * Add an undefined symbol.
 *)
val bfd_print_undef : 'a bfd -> raw_section -> symbol -> unit

(*
 * Conversion to IO form.
 *)
val bfd_io_of_bfd : 'a bfd -> bfd_io
val bfd_of_bfd_io : bfd_io -> 'a bfd

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
