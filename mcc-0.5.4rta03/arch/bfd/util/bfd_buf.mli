(*
 * Basic BFD buffer.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Float80

open Bfd
open Bfd_type

(*
 * Creation.
 *)
val create_buffer : byte_order -> buf
val new_buffer : buf -> buf

(*
 * List conversion.
 *)
val buffer_items : buf -> buf_item list
val buffer_of_list : buf -> buf_item list -> buf

(*
 * Fill.
 *)
val bfd_align       : buf -> int -> unit
val bfd_skip        : buf -> int -> int -> unit

(*
 * Symbols.
 *)
val bfd_print_label : buf -> symbol_type -> label -> unit
val bfd_print_equ   : buf -> symbol_type -> label -> exp -> unit
val bfd_print_comm  : buf -> symbol_type -> label -> int -> unit
val bfd_print_reloc : buf -> reloc -> unit

(*
 * Normal data.
 *)
val bfd_print_data  : buf -> string -> unit
val bfd_print_ascii : buf -> string -> unit
val bfd_print_int8  : buf -> int -> unit
val bfd_print_int16 : buf -> int -> unit
val bfd_print_int24 : buf -> int -> unit
val bfd_print_int32 : buf -> int32 -> unit
val bfd_print_int64 : buf -> int64 -> unit

val bfd_print_float32 : buf -> float80 -> unit
val bfd_print_float64 : buf -> float80 -> unit
val bfd_print_float80 : buf -> float80 -> unit

(*
 * Relocatable expressions.
 *)
val bfd_print_exp8  : buf -> exp -> unit
val bfd_print_exp16 : buf -> exp -> unit
val bfd_print_exp32 : buf -> exp -> unit

(*
 * Choice points.
 *)
val bfd_print_choice : buf -> exp -> (interval * buf) list -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
