(*
 * Perform relocation (as much as possible).
 * We assume that relocation positions have been set
 * by the flatten phase.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Bfd
open Bfd_type
open Bfd_buf
open Bfd_bfd
open Bfd_sizeof
open Bfd_flatten
open Bfd_state
open Bfd_eval

(************************************************************************
 * RELOCATION COLLECTION
 ************************************************************************)

(*
 * Collect all the relocations.
 *)
let rec collect_item relocs item =
   match item with
      BufReloc reloc ->
         reloc :: relocs
    | BufChoice (_, cases) ->
         collect_choice relocs cases
    | _ ->
         relocs

and collect_choice relocs cases =
   match cases with
      choice :: cases ->
         if choice.choice_enabled then
            collect_buf relocs choice.choice_buffer
         else
            collect_choice relocs cases
    | [] ->
         raise (Invalid_argument "collect_choice")

and collect_buf relocs buf =
   List.fold_left collect_item relocs (buffer_items buf)

(*
 * Get all the relocs in the section.
 *)
let relocs_of_section sect =
   let buf = sect.sect_data in
      List.rev (collect_buf [] buf)

(************************************************************************
 * RELOCATION
 ************************************************************************)

(*
 * Write a relocatable value.
 *)
let bfd_print_reloc_int data pos reloc_type i =
   if debug debug_bfd_reloc then
      eprintf "bfd_print_reloc_int: 0x%08x: %s@." pos (Int32.format "0x%08x" i);
   match reloc_type with
      RelocInt8 ->
         bfd_print_int8  data pos (Int32.to_int i)
    | RelocInt16 ->
         bfd_print_int16 data pos (Int32.to_int i)
    | RelocInt24 ->
         bfd_print_int24 data pos (Int32.to_int i)
    | RelocInt32 ->
         bfd_print_int32 data pos i
    | RelocInt64 ->
         bfd_print_int64 data pos (Int64.of_int32 i)

(*
 * Relocation offset.
 *)
let int8_rel   = Int32.of_int (-sizeof_int8)
let int16_rel  = Int32.of_int (-sizeof_int16)
let int24_rel  = Int32.of_int (-sizeof_int24)
let int32_rel  = Int32.of_int (-sizeof_int32)
let int64_rel  = Int32.of_int (-sizeof_int64)

let off_of_reloc_type = function
   RelocInt8  -> int8_rel
 | RelocInt16 -> int16_rel
 | RelocInt24 -> int24_rel
 | RelocInt32 -> int32_rel
 | RelocInt64 -> int64_rel

(*
 * Print a relocation if possible.
 *)
let bfd_print_reloc venv data reloc =
   let { reloc_type = reloc_type;
         reloc_pos = pos;
         reloc_exp = e
       } = reloc
   in
   let lazy_val = eval_lazy_exp venv e in
   let eager_val = eval_eager_value venv lazy_val in
      match lazy_val, eager_val with
         _, ValAbsolute i ->
            bfd_print_reloc_int data pos reloc_type i
       | ValRelative _, _ ->
            bfd_print_reloc_int data pos reloc_type (off_of_reloc_type reloc_type)
       | _ ->
            ()

(*
 * Print relocation value.
 *)
let bfd_print_reloc_value sect reloc_type pos off =
   bfd_print_reloc_int sect.sect_object pos reloc_type off

(*
 * Relocate the section as much as possible.
 *)
let relocate_section venv sect =
   let relocs = relocs_of_section sect in
   let { sect_length = len;
         sect_object = data
       } = sect
   in
      sect.sect_relocs <- relocs;
      List.iter (bfd_print_reloc venv data) relocs

(*
 * Perform relocation for the BFD.
 *)
let relocate_bfd bfd =
   List.iter (relocate_section bfd.bfd_venv) (raw_sections bfd)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
