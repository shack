(*
 * A buffer for collecting binary data.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Float80

open Bfd
open Bfd_env
open Bfd_type
open Bfd_const
open Bfd_buf

(*
 * Initialization.
 *)
external bfd_init : unit -> unit = "ml_bfd_init"

let _ = bfd_init ()

(*
 * Create an empty bfd.
 *)
let create_bfd byte_order =
   { bfd_sections = SymbolTable.empty;
     bfd_byte_order = byte_order;
     bfd_venv = venv_empty
   }

(*
 * Add a new section.
 *)
let add_section bfd name flags fill =
   let byte_order = bfd.bfd_byte_order in
   let sect =
      { sect_name = name;
        sect_byte_order = byte_order;
        sect_flags = flags;
        sect_data = create_buffer byte_order;
        sect_length = 0;
        sect_fill = Char.chr (fill land 0xff);
        sect_object = "Uninitialized";
        sect_relocs = []
      }
   in
      bfd.bfd_sections <- SymbolTable.add bfd.bfd_sections name (RawSection sect);
      sect

(*
 * Find it if it already exists,
 * create it otherwise.
 *)
let find_raw_section bfd name flags fill =
   try
      match SymbolTable.find bfd.bfd_sections name with
         RawSection sect ->
            sect
       | ValueSection _ ->
            raise (Invalid_argument "find_raw_section")
   with
      Not_found ->
         add_section bfd name flags fill

(*
 * Find it if it already exists,
 * create it otherwise.
 *)
let find_val_section bfd name =
   match SymbolTable.find bfd.bfd_sections name with
      ValueSection sect ->
         sect
    | RawSection _ ->
         raise (Invalid_argument "find_value_section")

(*
 * Add a value section.
 *)
let set_val_section bfd name v =
   let info =
      { val_name = name;
        val_data = v;
        val_object = "Uninitialized"
      }
   in
      bfd.bfd_sections <- SymbolTable.add bfd.bfd_sections name (ValueSection info)

(************************************************************************
 * SECTIONS
 ************************************************************************)

(*
 * Manifest section names.
 *)
let absolute_sym  = Symbol.add abs_sec_name
let undefined_sym = Symbol.add und_sec_name

(*
 * Add an external symbol.
 *)
let bfd_print_undef bfd sect v =
   let info =
      { sym_section = sect.sect_name;
        sym_type = SymUndefined;
        sym_value = ValUndefined
      }
   in
      bfd.bfd_venv <- venv_add bfd.bfd_venv v info

(*
 * Get a list of all the raw sections.
 *)
let raw_sections bfd =
   let collect sections _ sect =
      match sect with
         RawSection sect ->
            sect :: sections
       | ValueSection _ ->
            sections
   in
      SymbolTable.fold collect [] bfd.bfd_sections

(*
 * Get a list of all the value sections.
 *)
let val_sections bfd =
   let collect sections _ sect =
      match sect with
         ValueSection sect ->
            sect :: sections
       | RawSection _ ->
            sections
   in
      SymbolTable.fold collect [] bfd.bfd_sections

(*
 * Get the section buffer.
 *)
let section_buf { sect_data = buf } =
   buf

let section_name { sect_name = name } =
   name

let section_flags { sect_flags = flags } =
   flags

let section_object { sect_object = obj } =
   obj

(*
 * Convert to an IO section.
 *)
let choice_map f item =
   let item =
      match item with
         BufChoice (e, choices) ->
            let choices =
               List.map (fun choice ->
                     let { choice_interval = interval;
                           choice_buffer = buf
                         } = choice
                     in
                        { choice_enabled = true;
                          choice_interval = interval;
                          choice_buffer = f buf
                        }) choices
            in
               BufChoice (e, choices)
       | BufAlign i ->
            BufAlign i
       | BufSkip (i, j) ->
            BufSkip (i, j)
       | BufLabel (sym_type, l) ->
            BufLabel (sym_type, l)
       | BufEqu (sym_type, l, e) ->
            BufEqu (sym_type, l, e)
       | BufCommon (sym_type, l, i) ->
            BufCommon (sym_type, l, i)
       | BufReloc reloc ->
            BufReloc reloc
       | BufData s ->
            BufData s
       | BufAscii s ->
            BufAscii s
       | BufInt8 i ->
            BufInt8 i
       | BufInt16 i ->
            BufInt16 i
       | BufInt24 i ->
            BufInt24 i
       | BufInt32 i ->
            BufInt32 i
       | BufInt64 i ->
            BufInt64 i
       | BufFloat32 i ->
            BufFloat32 i
       | BufFloat64 i ->
            BufFloat64 i
       | BufFloat80 i ->
            BufFloat80 i
   in
      item

let rec io_buffer_of_buffer buf =
   Buffer (List.map (choice_map io_buffer_of_buffer) (buffer_items buf))

let io_section_of_section sect =
   let { sect_name = name;
         sect_flags = flags;
         sect_fill = fill;
         sect_data = buf
       } = sect
   in
      { sect_io_name = name;
        sect_io_flags = flags;
        sect_io_fill = fill;
        sect_io_data = io_buffer_of_buffer buf
      }

let bfd_io_of_bfd bfd =
   let { bfd_sections = sections;
         bfd_byte_order = byte_order
       } = bfd
   in
   let sections =
      SymbolTable.fold (fun sections _ sect ->
            match sect with
               RawSection sect ->
                  io_section_of_section sect :: sections
             | ValueSection _ ->
                  sections) [] sections
   in
   let sections = List.rev sections in
      { bfd_io_sections = sections;
        bfd_io_byte_order = byte_order
      }

(*
 * Convert back to normal form.
 *)
let rec buffer_of_io_buffer byte_order (Buffer items) =
   { byte_order = byte_order;
     buf_forward_list = List.map (choice_map (buffer_of_io_buffer byte_order)) items;
     buf_reverse_list = []
   }

let section_of_io_section byte_order sections sect =
   let { sect_io_name = name;
         sect_io_flags = flags;
         sect_io_fill = fill;
         sect_io_data = items
       } = sect
   in
   let sect =
      { sect_name = name;
        sect_byte_order = byte_order;
        sect_flags = flags;
        sect_fill = fill;
        sect_data = buffer_of_io_buffer byte_order items;
        sect_length = 0;
        sect_object = "Uninitialized";
        sect_relocs = []
      }
   in
      SymbolTable.add sections name (RawSection sect)

let bfd_of_bfd_io bfd =
   let { bfd_io_sections = sections;
         bfd_io_byte_order = byte_order
       } = bfd
   in
   let sections = List.fold_left (section_of_io_section byte_order) SymbolTable.empty sections in
      { bfd_sections = sections;
        bfd_venv = venv_empty;
        bfd_byte_order = byte_order
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
