(*
 * A buffer for collecting binary data.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol
open Float80

open Bfd
open Bfd_env
open Bfd_type
open Bfd_const

(*
 * Create a new buffer.
 *)
let create_buffer byte_order =
   { byte_order = byte_order;
     buf_forward_list = [];
     buf_reverse_list = []
   }

(*
 * Create a new sub-buffer, using the
 * current byte order.
 *)
let new_buffer { byte_order = byte_order } =
   create_buffer byte_order

(*
 * Normalize the lists.
 *)
let buffer_items buf =
   match buf.buf_reverse_list with
      [] ->
         buf.buf_forward_list
    | l ->
         let l = buf.buf_forward_list @ List.rev l in
            buf.buf_forward_list <- l;
            buf.buf_reverse_list <- [];
            l

let buffer_of_list buf l =
   { buf with buf_forward_list = l;
              buf_reverse_list = []
   }

(*
 * Append an item to the buffer.
 *)
let append buf item =
   buf.buf_reverse_list <- item :: buf.buf_reverse_list

(*
 * Basic buffer operations.
 *)
let bfd_align buf size =
   append buf (BufAlign size)

let bfd_skip buf size fill =
   append buf (BufSkip (size, fill))

let bfd_print_label buf sym_type label =
   append buf (BufLabel (sym_type, label))

let bfd_print_equ buf sym_type label exp =
   append buf (BufEqu (sym_type, label, exp))

let bfd_print_comm buf sym_type label size =
   append buf (BufCommon (sym_type, label, size))

let bfd_print_reloc buf info =
   append buf (BufReloc info)

let bfd_print_data buf s =
   append buf (BufData s)

let bfd_print_ascii buf s =
   append buf (BufAscii s)

let bfd_print_int8 buf i =
   append buf (BufInt8 i)

let bfd_print_int16 buf i =
   append buf (BufInt16 i)

let bfd_print_int24 buf i =
   append buf (BufInt24 i)

  let bfd_print_int32 buf i =
   append buf (BufInt32 i)

let bfd_print_int64 buf i =
   append buf (BufInt64 i)

let bfd_print_float32 buf x =
   append buf (BufFloat32 x)

let bfd_print_float64 buf x =
   append buf (BufFloat64 x)

let bfd_print_float80 buf x =
   append buf (BufFloat80 x)

(*
 * Relocation utilities.
 *)
let bfd_print_exp8 buf exp =
   let reloc =
      { reloc_type = RelocInt8;
        reloc_offset = 0;
        reloc_pos = 0;
        reloc_exp = exp
      }
   in
      bfd_print_reloc buf reloc;
      bfd_print_int8 buf 0

let bfd_print_exp16 buf exp =
   let reloc =
      { reloc_type = RelocInt16;
        reloc_offset = 0;
        reloc_pos = 0;
        reloc_exp = exp
      }
   in
      bfd_print_reloc buf reloc;
      bfd_print_int16 buf 0

let bfd_print_exp32 buf exp =
   let reloc =
      { reloc_type = RelocInt32;
        reloc_offset = 0;
        reloc_pos = 0;
        reloc_exp = exp
      }
   in
      bfd_print_reloc buf reloc;
      bfd_print_int32 buf Int32.zero

(*
 * Choice printing.
 *)
let bfd_print_choice buf exp choices =
   let choices =
      List.map (fun (interval, buf) ->
            { choice_enabled = true;
              choice_interval = interval;
              choice_buffer = buf
            }) choices
   in
      append buf (BufChoice (exp, choices))

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
