/*
 * This is the ML interface to BFD.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
/* BFD */
#include <bfd.h>

/* Standard includes */
#include <stdio.h>
#include <memory.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* Caml includes */
#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

/* HOWTO definitions */
#include "ml_bfd.h"

/*
 * XXX JW, 2007-06-21 12:10: 
 * 
 * arch/bfd/cutil/ml_bfd.c: In function 'ml_bfd_openw':
 * arch/bfd/cutil/ml_bfd.c:237: error: 'false' undeclared (first use in this function)
 *
 * <kludge>
 */
#define false 0
/*
 * </kludge>
 */ 

PTR bfd_alloc (bfd *abfd, size_t wanted);

/************************************************************************
 * TYPES
 ************************************************************************/

/*
 * This is the data structure we keep for the BFD.
 */
typedef struct _bfd_info {
    /* Bfd info */
    bfd *bfdp;

    /* Relocation howtos */
    reloc_howto_type *howtos[ML_RELOC_COUNT];
} BfdInfo;    

/*
 * HOWTO translation.
 */
static bfd_reloc_code_real_type reloc_codes[] = {
    BFD_RELOC_8,
    BFD_RELOC_16,
    BFD_RELOC_24,
    BFD_RELOC_32,
    BFD_RELOC_64,

    BFD_RELOC_8_PCREL,
    BFD_RELOC_16_PCREL,
    BFD_RELOC_24_PCREL,
    BFD_RELOC_32_PCREL,
    BFD_RELOC_64_PCREL,
};

/*
 * Info for collecting sections.
 */
#define INITIAL_SEC_LENGTH      10

typedef struct _sec_list {
    /* This is the name of the section we want to collect */
    char *name;

    /* This is an array of these sections */
    unsigned long length;
    unsigned long index;
    unsigned long *filepos;
} SecList;

/************************************************************************
 * UTILITIES
 ************************************************************************/

/*
 * Allocate memory, abort on error.
 */
static void *ealloc(unsigned size)
{
    void *datap;

    datap = malloc(size);
    if(datap == 0) {
        fprintf(stderr, "Ml_bfd.ealloc: out of memory requesting %u bytes\n", size);
        abort();
    }
    memset(datap, 0, size);
    return datap;
}

/*
 * Allocate a new BFD.
 */
static BfdInfo *new_bfd(bfd *bfdp)
{
    BfdInfo *infop;

    infop = (BfdInfo *) ealloc(sizeof(BfdInfo));
    infop->bfdp = bfdp;
    return infop;
}

/*
 * Free the BFD.
 */
static void free_bfd(BfdInfo *infop)
{
    /* Free the info */
    free(infop);
}

/*
 * Allocate a new section list.
 */
static SecList *new_sec_list(char *name)
{
    SecList *listp;

    /* Allocate section struct */
    listp = (SecList *) ealloc(sizeof(SecList));
    listp->name = name;
    listp->length = INITIAL_SEC_LENGTH;
    listp->index = 0;
    listp->filepos = (unsigned long *) ealloc(sizeof(unsigned long) * INITIAL_SEC_LENGTH);
    return listp;
}

/*
 * Free the section list.
 */
static void free_sec_list(SecList *secp)
{
    free(secp->filepos);
    free(secp);
}

/*
 * Add a number to the section list.
 */
static void add_sec_off(SecList *listp, unsigned long off)
{
    /* May need to expand the list */
    if(listp->index == listp->length) {
        unsigned long *datap;

        datap = (unsigned long *) ealloc(listp->length * 2 * sizeof(unsigned long));
        memcpy(datap, listp->filepos, listp->length * sizeof(unsigned long));
        free(listp->filepos);
        listp->filepos = datap;
        listp->length *= 2;
    }

    /* Add the entry */
    listp->filepos[listp->index++] = off;
}    

/*
 * This is the iterator for collecting sections.
 */
static void sec_off_fun(bfd *bfdp, asection *secp, PTR obj)
{
    SecList *listp = (SecList *) obj;
    if(strcmp(secp->name, listp->name) == 0)
        add_sec_off(listp, secp->filepos * 4);
}

/************************************************************************
 * WIRITNG THE BFD
 ************************************************************************/

/*
 * Generic error handling.
 */
static void ml_bfd_error(const char *strp)
{
    bfd_error_type error;
    const char *msgp;
    char buffer[1024];

    error = bfd_get_error();
    msgp = bfd_errmsg(error);
    snprintf(buffer, sizeof(buffer), "%s: %s", strp, msgp);
    failwith(buffer);
}

/*
 * Initialize bfd.
 */
value ml_bfd_init(value unit)
{
    bfd_init();
    return Val_unit;
}

/*
 * Open a BFD with the default target.
 * BUG: we probably want to specify the arch
 * as an argument.  We'll have to do this when
 * we have multiple architectures.
 */
value ml_bfd_openw(value file_val)
{
    const char *filename;
    bfd *bfdp;

    filename = String_val(file_val);
    bfdp = bfd_openw(filename, "elf32-i386");
    if(bfdp == 0)
        ml_bfd_error("bfd_openw");
    if(bfd_set_format(bfdp, bfd_object) == false)
        ml_bfd_error("bfd_set_format");
    if(bfd_set_arch_mach(bfdp, bfd_arch_i386, bfd_mach_i386_i386) == false)
        ml_bfd_error("bfd_set_arch_mach");
    return (value) new_bfd(bfdp);
}

/*
 * Close the BFD.
 */
value ml_bfd_close(BfdInfo *infop)
{
    if(bfd_close(infop->bfdp) == false)
        ml_bfd_error("bfd_close");
    free_bfd(infop);
    return Val_unit;
}

/*
 * Create a section.
 */
value ml_bfd_make_section(BfdInfo *infop, value name_val)
{
    char *name;
    asection *sectp;

    /* Get the name and create the section */
    name = String_val(name_val);
    sectp = bfd_make_section_old_way(infop->bfdp, name);
    if(sectp == 0)
        ml_bfd_error("bfd_make_section_old_way");
    return (value) sectp;
}

/*
 * Set the section flags.
 */
value ml_bfd_set_section_flags(BfdInfo *infop, asection *secp, value flags_val)
{
    int flags;

    flags = Int_val(flags_val);
    if(bfd_set_section_flags(infop->bfdp, secp, flags) == false)
        ml_bfd_error("bfd_set_section_flags");
    return Val_unit;
}

/*
 * Set the section contents.
 */
value ml_bfd_set_section_size(BfdInfo *infop, asection *secp, value count_val)
{
    unsigned long count;

    /* Get the data */
    count = Int_val(count_val);

    /* BFD commands */
    if(bfd_set_section_size(infop->bfdp, secp, count) == false)
        ml_bfd_error("bfd_set_section_size");
    return Val_unit;
}

/*
 * Set the section contents.
 */
value ml_bfd_set_section_contents(BfdInfo *infop, asection *secp, value data_val)
{
    char *data;
    unsigned long count;

    /* Get the data */
    data = String_val(data_val);
    count = string_length(data_val);

    /* BFD commands */
    if(bfd_set_section_contents(infop->bfdp, secp, data, 0, count) == false)
        ml_bfd_error("bfd_set_section_contents");
    return Val_unit;
}

/*
 * Create a new symbol.
 */
value ml_bfd_new_symbol(BfdInfo *infop, value name_val, asection *secp, value flags_val, value pos_val)
{
    char *oldp, *strp;
    asymbol *newp;
    int length;

    /* Tell the BFD to make a new symbol */
    newp = bfd_make_empty_symbol(infop->bfdp);
    if(newp == 0)
        ml_bfd_error("bfd_make_empty_symbol");

    /* Allocate the string */
    oldp = String_val(name_val);
    length = strlen(oldp) + 1;
    strp = (char *) bfd_alloc(infop->bfdp, length);
    memcpy(strp, oldp, length);

    /* Change section for builtin sections */
    if(strcmp(secp->name, ABS_SEC_NAME) == 0)
        secp = bfd_abs_section_ptr;
    else if(strcmp(secp->name, UND_SEC_NAME) == 0)
        secp = bfd_und_section_ptr;

    /* Set the new symbol */
    newp->name = strp;
    newp->section = secp;
    newp->flags = Int_val(flags_val);
    newp->value = Int_val(pos_val);

    return (value) newp;
}

/*
 * Set the symbol table.
 * copy the array and save it.
 */
value ml_bfd_set_symtab(BfdInfo *infop, value symtab_val, value count_val)
{
    asymbol **symtabpp;
    int count;

    /* Copy the table */
    count = Int_val(count_val);
    symtabpp = (asymbol **) bfd_alloc(infop->bfdp, sizeof(asymbol *) * (count + 1));
    if(symtabpp == 0) {
        fprintf(stderr, "Ml_bfd.bfd_set_symtab: out of memory\n");
        exit(5);
    }
    memcpy(symtabpp, &Field(symtab_val, 0), sizeof(asymbol *) * count);
    symtabpp[count] = 0;

#if 0
    {
        int i;
        asymbol *symp;

        for(i = 0; i != count; i++) {
            symp = symtabpp[i];
            fprintf(stderr, "Symbol: %s, sec=%s, flags=0x%08x, value=0x%08lx\n",
                    symp->name, symp->section->name, symp->flags, (unsigned long) symp->value);
        }
    }
#endif

    /* Set the BFD symtab */
    if(bfd_set_symtab(infop->bfdp, symtabpp, count) == false)
        ml_bfd_error("bfd_set_symtab");
    return Val_unit;
}

/*
 * Add an absolute int32 relocation.
 */
value ml_bfd_new_reloc(BfdInfo *infop, value howto_val, value index_val, value pos_val, value addend_val)
{
    unsigned long index, howto;
    reloc_howto_type *howtop;
    arelent *relocp;
    asymbol **sympp;

    /* Translate the symbol */
    index = Int_val(index_val);
    sympp = infop->bfdp->outsymbols + index;

    /* Get the HOWTO */
    howto = Int_val(howto_val);
    assert(howto < ML_RELOC_COUNT);
    howtop = infop->howtos[howto];
    if(howtop == 0) {
        howtop = bfd_reloc_type_lookup(infop->bfdp, reloc_codes[howto]);
        if(howtop == 0)
            ml_bfd_error("bfd_reloc_type_lookup");
        infop->howtos[howto] = howtop;
    }

    /* Allocate the relocation */
    relocp = (arelent *) bfd_alloc(infop->bfdp, sizeof(arelent));
    if(relocp == 0) {
        fprintf(stderr, "Ml_bfd.ml_bfd_reloc_abs32: out of memory\n");
        exit(5);
    }
    memset(relocp, 0, sizeof(arelent));
    relocp->sym_ptr_ptr = sympp;
    relocp->address = Int_val(pos_val);
    relocp->addend = Int_val(addend_val);
    relocp->howto = howtop;

#if 0
    {
        fprintf(stderr, "Relocation: %lu=%s, howto=%lu, pos=0x%08lx, addend=0x%08lx\n",
                index, (*sympp)->name, howto,
                (unsigned long) relocp->address,
                (unsigned long) relocp->addend);
    }
#endif
    return (value) relocp;
}

/*
 * Install the relocations in the section.
 */
value ml_bfd_set_reloc(BfdInfo *infop, asection *secp, value reloc_val, value count_val)
{
    arelent **relocpp;
    int count;

    /* Get the relocation count */
    count = Int_val(count_val);
    if(count == 0)
        return Val_unit;

    /* Copy the table */
    relocpp = (arelent **) bfd_alloc(infop->bfdp, sizeof(arelent *) * (count + 1));
    if(relocpp == 0) {
        fprintf(stderr, "Ml_bfd.bfd_set_reloc: out of memory\n");
        exit(5);
    }
    memcpy(relocpp, &Field(reloc_val, 0), sizeof(arelent *) * count);
    relocpp[count] = 0;

#if 0
    {
        int i;
        arelent *relocp;

        for(i = 0; i != count; i++) {
            relocp = relocpp[i];
            fprintf(stderr, "Adding relocation %8s, off=0x%08lx, addend=0x%08lx, %s\n",
                    secp->name,
                    (unsigned long) relocp->address,
                    (unsigned long) relocp->addend,
                    (*relocp->sym_ptr_ptr)->name);
        }
    }
#endif
                    
    /* Set the relocation info */
    bfd_set_reloc(infop->bfdp, secp, relocpp, count);

    return Val_unit;
}

/************************************************************************
 * READING THE BFD
 ************************************************************************/

/*
 * This is a very limited form for reading a
 * specific section from the BFD.  We open the
 * BFD, find the section, then position the file
 * descriptor to the start of the section.
 */
value ml_bfd_open_sec(value file_val, value name_val)
{
    CAMLparam2(file_val, name_val);
    CAMLlocal1(blockp);
    SecList *listp;
    char *filename;
    bfd *bfdp;
    int i;

    /* Open the file for reading */
    filename = String_val(file_val);

    /* Open the BFD */
    bfdp = bfd_openr(filename, "elf32-i386");
    if(bfdp == 0)
        ml_bfd_error("bfd_openr");
    if(bfd_check_format(bfdp, bfd_object) == false)
        ml_bfd_error("bfd_check_format");

    /* Get the offsets of all the sections and close the BFD */
    listp = new_sec_list(String_val(name_val));
    bfd_map_over_sections(bfdp, sec_off_fun, (PTR) listp);
    if(bfd_close(bfdp) == false) {
        free_sec_list(listp);
        ml_bfd_error("bfd_close");
    }

    /* Allocate the array of offsets */
    blockp = alloc_tuple(listp->index);
    for(i = 0; i != listp->index; i++)
        Field(blockp, i) = Int_val(listp->filepos[i]);
    free_sec_list(listp);

    /* Return the pair */
    CAMLreturn(blockp);
}
