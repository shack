/*
 * Extra HOWTO definitions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#ifndef _ML_BFD_H
#define _ML_BFD_H

/*
 * Howto definitions.
 */
#define ML_RELOC_8              0
#define ML_RELOC_16             1
#define ML_RELOC_24             2
#define ML_RELOC_32             3
#define ML_RELOC_64             4

#define ML_RELOC_8_PCREL        5
#define ML_RELOC_16_PCREL       6
#define ML_RELOC_24_PCREL       7
#define ML_RELOC_32_PCREL       8
#define ML_RELOC_64_PCREL       9

#define ML_RELOC_COUNT          10

/*
 * Special section names.
 */
#define ABS_SEC_NAME            ".mc.absolute"
#define UND_SEC_NAME            ".mc.undefined"

#endif /* _ML_BFD_H */
