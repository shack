open Byte_simulator

(*
 * This is the main function of the simlulator. All execution begins here.
 * The program takes at least 2 arguments. The 2nd argument is the name of
 * the bytecode file. The following arguments are arguments passed to the 
 * main function of the bytecode itself.
 *)
let _ = 
	let exit_status = 
		begin match Array.length Sys.argv with
		  0 ->
		  	failwith "MVM: this error occurs, the world is ending!!!"
		| 1 -> 
		  	print_string "bytecode not specified...\n"; 
				1
		| _ ->
			load_prog Sys.argv.(1);
				exec_prog()
		end
	in
	
	Printf.printf "MVM: exited with code %d\n" exit_status;
		exit exit_status
