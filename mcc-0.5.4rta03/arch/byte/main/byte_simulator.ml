open Symbol
open Trace

open Fir_set

open Byte_inst_type
open Byte_frame_type
open Byte_spset
open Byte_frame

(*
 * A useful defintion/symbol for a NULL pointer.
 *)
let null_pointer = Symbol.add "null"

(*
 * This is the main components of the simulator.
 *)
let memory = ref [| |]
let init_tbl = ref SymbolTable.empty
let func_tbl = ref SymbolTable.empty
let loaded = ref false

let main_function = ref null_pointer
let return_value = ref DTNone

(*
 * to_address, to_char, to_int, to_float, to_pointer, to_string, to_tuple
 * to_array
 *
 * Purpose:
 *	extracts the neccessary information from the data type or memory type.
 *)
let to_address = function
	  Memory(i) -> i
	| _ -> failwith "to_address: not a memory address"

and to_char = function
	  DTChar(c) -> c
	| _-> failwith "to_char: not an integer"

and to_int = function
	  DTInt(i) -> i
	| _-> failwith "to_int: not an integer"

and to_float = function
	  DTFloat(f) -> f
	| _ -> failwith "to_float: not a floating-point"

and to_pointer = function
	  DTPointer(ptr) -> ptr
	| _ -> failwith "to_pointer: not a pointer"
	
and to_string = function
	  DTString(s) -> s
	| _ -> failwith "to_string: not a string"
	
and to_tuple = function
	  DTTuple(tup) -> tup
	| _ -> failwith "to_tuple: not a tuple"
	
and to_array = function
	  DTArray(ar) -> ar
	| _ -> failwith "to_array: not an array"
	
(*
 * load_tables
 *
 * Purpose:
 *	load the 'init_tbl' and the 'func_tbl' with their correct functions from
 *	the block trace.
 *)
let load_tables prog =
	let blocks = prog.byte_funs in

	(* go through the entire trace an group the function accordingly *)
	Trace.fold (fun (init_table, func_table) block ->
		let {
			block_label = name;
			block_args = args;
			block_inst = code;
		} = block in
		
		(* JDS: block_has_prefix checks if a block has the specified
		   prefix.  We must fix this eventually to use the export table
		   to find the right symbols.  *)
		let block_has_prefix block prefix =
		   let block_name = string_of_symbol block.block_label in
		   let block_name_len = String.length block_name in
		   let prefix_len = String.length prefix in
		      if prefix_len <= block_name_len then
		         let block_prefix = String.sub block_name 0 prefix_len in
		            (Pervasives.compare block_prefix prefix) = 0
		      else
		         false
      in
		
		(* add the function to the correct table *)
		let init_table, func_table =
			if block_has_prefix block "init" then
				(SymbolTable.add init_table name (args, code)), func_table
			else
				(* save the main function's name *)
				let _ = 
					if block_has_prefix block "main" then
						main_function := block.block_label
					else
					 	()
				in
				init_table, (SymbolTable.add func_table name (args, code))
		in init_table, func_table) (SymbolTable.empty, SymbolTable.empty) blocks

(*
 * create_memory
 *
 * Purpose:
 *	Create the the memory for the simulator and initialize the cell that must
 *	be initialized before execution begins.
 *)
let create_memory prog = 
	let {
		byte_global = globals;
		byte_data = data;
	} = prog in
	
	(* create a new array of memory *)
	let memory = Array.make (spset_spill_count data) DTNone in

	(* initialize all the cells that refer to global variables *)
	SymbolTable.iter (fun v dt ->
		let addy = spset_spill_location data v in
			memory.(addy) <- dt) globals;
		memory	

(*
 * load_prog
 *
 * Purpose:
 *	loads a program into the simulator by create and initializing the memory
 *	and creating the 'init_tbl' and 'func_tbl'. The function is one of the 
 * 	functions that allows the outside world to interface w/ the simulator.
 *)
let load_prog in_filename =
	let prog = Byte_parser.prog_of_file in_filename in
	let inits,funcs = load_tables prog in
	
	init_tbl := inits;
	func_tbl := funcs;
	memory := create_memory prog;
		loaded := true
		
(*********************************************************************************************
* This is the where the simulator execution occurs
*********************************************************************************************)

(*
 * return_zero
 *
 * Purpose:
 *	This is useful when returning zero
 *)
let return_zero = return_value := DTInt(Rawint.of_int Rawint.Int32 true 0)

(*
 * native_func
 *
 * Purpose:
 *	Executes a native/extern function. These function are usually those that 
 *	involve interaction w/ the computer hardware. These functions always return
 *	a value, which is stored in 'return_value'.
 *)
let native_func args = function
	  "exit" -> 
	  	return_value := !memory.(to_address (List.nth args 1))
	| "exit_init" ->
		return_value := !memory.(to_address (List.nth args 0))
	| "uncaught_excpetion" ->
		failwith "uncaught exception"
	| "fc_print_string" ->
		print_string (to_string (!memory.(to_address (List.hd args))));
		return_zero
	| "fc_print_int" ->
		print_string (Rawint.to_string (to_int (!memory.(to_address (List.hd args)))));
		return_zero
	| "fc_print_char" ->	
		print_char (Char.chr (Rawint.to_int (to_int (!memory.(to_address (List.hd args))))));
		return_zero
	| _ as v -> 
		failwith ("not a native function (" ^ v ^ ")")

(*
 * load_func
 *
 * Purpose:
 *	Loads the next function so the simulator can begin executing its instructions.
 *)	
let rec load_func v src_args =
	if is_native_fun v then
		native_func src_args (Symbol.to_string v) 
	else
		let args,code = 
		   try
		      SymbolTable.find !func_tbl v
		   with
		      Not_found ->
		         failwith ("Cannot find symbol " ^ (string_of_symbol v) ^ " in Byte_simulator.load_func")
      in

		(* load the arguments for the function and execute the instructions *)
		copy_args args src_args;
			List.iter (fun inst -> exec_inst inst) code		

(*
 * copy_args
 *
 * Purpose:
 *	This function is responsible for passing the arguments from the calling
 *	function to the function being called. It takes two list, the first is 
 *	the memory address of the destination function argments and the second is the
 *	memory address of the arguments being passed to the function.
 *)
and copy_args dest src =
 	List.iter2 (fun d s ->
		!memory.(to_address d) <- !memory.(to_address s)) dest src

(*
 * exec_inst
 *
 * Purpose:
 *	This function executes the current instruction. Because everything instruction has
 *	operands that come from memory, each instruction involves reading and writing array
 *	elements.
 *)
and exec_inst = function
	  Add(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.add (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Sub(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.sub (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Div(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.div (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Mul(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.mul (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Rem(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.rem (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Max(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.max (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Min(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.min (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Shl(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.shift_left (to_int !memory.(s1)) (to_int !memory.(s2)))
	| AShr(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.shift_right (to_int !memory.(s1)) (to_int !memory.(s2)))
	| LShr(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.shift_right (to_int !memory.(s1)) (to_int !memory.(s2)))
	| And(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.logand (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Or(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.logor (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Xor(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTInt(Rawint.logxor (to_int !memory.(s1)) (to_int !memory.(s2)))
	| Eq(Memory d, Memory s1, Memory s2) ->
	  	if Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) = 0 then 
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 1)
		else
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
	| Neq(Memory d, Memory s1, Memory s2) ->
	  	if Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) != 0 then 
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
		else
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 1)
	| Gt(Memory d, Memory s1, Memory s2) ->
	  	if Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) > 0 then 
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 1)
		else
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
	| Lt(Memory d, Memory s1, Memory s2) ->
	  	if Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) < 0 then 
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 1)
		else
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
	| Gte(Memory d, Memory s1, Memory s2) ->
	  	if Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) >= 0 then 
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 1)
		else
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
	| Lte(Memory d, Memory s1, Memory s2) ->
	  	if Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) <= 0 then 
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 1)
		else
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
	| Cmp(Memory d, Memory s1, Memory s2) ->
	  	let cmp = Rawint.compare (to_int !memory.(s1)) (to_int !memory.(s2)) in
			!memory.(d) <- DTInt(Rawint.of_int Rawint.Int32 true cmp)
	| Not(Memory d, Memory s) ->
	  	!memory.(d) <- DTInt(Rawint.lognot (to_int !memory.(s)))
	| UMinus(Memory d, Memory s) ->
	  	!memory.(d) <- DTInt(Rawint.neg (to_int !memory.(s)))
	| Abs(Memory d, Memory s) ->
	  	!memory.(d) <- DTInt(Rawint.abs (to_int !memory.(s)))
	| FAdd(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.add (to_float !memory.(s1)) (to_float !memory.(s2)))
	| FSub(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.sub (to_float !memory.(s1)) (to_float !memory.(s2)))
	| FDiv(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.div (to_float !memory.(s1)) (to_float !memory.(s2)))
	| FMul(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.mul (to_float !memory.(s1)) (to_float !memory.(s2)))
	| FRem(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.rem (to_float !memory.(s1)) (to_float !memory.(s2)))
	| FMax(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.max (to_float !memory.(s1)) (to_float !memory.(s2)))
	| FMin(Memory d, Memory s1, Memory s2) ->
	  	!memory.(d) <- DTFloat(Rawfloat.min (to_float !memory.(s1)) (to_float !memory.(s2)))	
	| FUMinus(Memory d, Memory s) ->
	  	!memory.(d) <- DTFloat(Rawfloat.neg (to_float !memory.(s)))
	| FAbs(Memory d, Memory s) ->
	  	!memory.(d) <- DTFloat(Rawfloat.abs (to_float !memory.(s)))
	| Sin(Memory d, Memory s) ->
	  	!memory.(d) <- DTFloat(Rawfloat.sin (to_float !memory.(s)))
	| Cos(Memory d, Memory s) ->
	  	!memory.(d) <- DTFloat(Rawfloat.cos (to_float !memory.(s)))
	| Sqrt(Memory d, Memory s) ->
	  	!memory.(d) <- DTFloat(Rawfloat.sqrt (to_float !memory.(s)))
	| FToUInt8(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int8 false (to_float !memory.(s)))
	| FToUInt16(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int16 false (to_float !memory.(s)))
	| FToUInt32(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int32 false (to_float !memory.(s)))
	| FToUInt64(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int64 false (to_float !memory.(s)))
	| FToInt8(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int8 true (to_float !memory.(s)))
	| FToInt16(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int16 true (to_float !memory.(s)))
	| FToInt32(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int32 true (to_float !memory.(s)))
	| FToInt64(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawfloat.to_rawint Rawint.Int64 true (to_float !memory.(s)))
	| FToFloat32(Memory d, Memory s) ->
		!memory.(d) <- DTFloat(Rawfloat.of_rawfloat Rawfloat.Single (to_float !memory.(s)))
	| FToFloat64(Memory d, Memory s) ->
		!memory.(d) <- DTFloat(Rawfloat.of_rawfloat Rawfloat.Double (to_float !memory.(s)))
	| FToFloat80(Memory d, Memory s) ->
		!memory.(d) <- DTFloat(Rawfloat.of_rawfloat Rawfloat.LongDouble (to_float !memory.(s)))
	| IToFloat32(Memory d, Memory s) ->
		!memory.(d) <- DTFloat(Rawfloat.of_rawint Rawfloat.Single (to_int !memory.(s)))
	| IToFloat64(Memory d, Memory s) ->
		!memory.(d) <- DTFloat(Rawfloat.of_rawint Rawfloat.Double (to_int !memory.(s)))
	| IToFloat80(Memory d, Memory s) ->
		!memory.(d) <- DTFloat(Rawfloat.of_rawint Rawfloat.LongDouble (to_int !memory.(s)))
	| IToInt8(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int8 true (to_int !memory.(s)))
	| IToInt16(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int16 true (to_int !memory.(s)))
	| IToInt32(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int32 true (to_int !memory.(s)))
	| IToInt64(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int64 true (to_int !memory.(s)))
	| IToUInt8(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int8 false (to_int !memory.(s)))
	| IToUInt16(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int16 false (to_int !memory.(s)))
	| IToUInt32(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int32 false (to_int !memory.(s)))
	| IToUInt64(Memory d, Memory s) ->
		!memory.(d) <- DTInt(Rawint.of_rawint Rawint.Int64 false (to_int !memory.(s)))
	| Set(Memory d, Memory s) ->
		!memory.(d) <- !memory.(s)	
	| Goto(Memory d, args) ->
		load_func (Symbol.add (to_string !memory.(d))) args 
	| Call(Label f, args) ->
		load_func f args
	| CallEx(Label f, args) -> 
		native_func args (Symbol.to_string f)
	| Extern(Memory d,f,args) ->  
		native_func args f;
			!memory.(d) <- !return_value
	| Match(v,cases) ->
		(* find out with case satisfies the condition *)
		List.iter (fun (s,code) ->
			if RawIntSet.mem_point (to_int (!memory.(to_address v))) s then
				List.iter (fun inst -> exec_inst inst) code
			else
				()
		) cases
	| _ -> failwith "exec_inst: undefined instruction"

(*
 * exec_inits
 *
 * Purpose:
 *	This function iterates throught the 'init_tbl' executing each function
 *	in the table. If any 'init' function fails then the simulator terminates.
 *)
let exec_inits() = 
	SymbolTable.iter (fun v (args,code) ->
		(* load the 'exit_init' function *)
		!memory.(to_address(List.hd args)) <- DTString(string_of_symbol fun_exit_init);
		
		(* execute the instructions in the 'init' function *)
		List.iter (fun inst -> exec_inst inst) code;
		if Rawint.is_zero (to_int(!return_value)) then
			()
		else
			failwith ((string_of_symbol v) ^ " failed")) !init_tbl
		
(*
 * exec_main
 *
 * Purpose:
 *	This is where the program begins execution. Remember we saved the name of the main
 *	function when loading the tables. Well, that named is found in 'func_tbl' and its
 *	arguments and are initialized and it's instrutions are executed.
 *)
and exec_main() =
	let args,code = 
	   try
	      SymbolTable.find !func_tbl !main_function
	   with
	      Not_found ->
	         failwith ("Could not find variable " ^ (string_of_symbol !main_function) ^ " in Byte_simulator.exec_main")
   in
	let _ = List.fold_left (fun index arg ->
		let _ = begin match index with
		  0 -> !memory.(to_address arg) <- DTInt(Rawint.of_int Rawint.Int32 true ((Array.length Sys.argv) - 1))
		| 1 -> !memory.(to_address arg) <- DTNone
		| 2 -> !memory.(to_address arg) <- DTInt(Rawint.of_int Rawint.Int32 true 0)
		| 3 -> !memory.(to_address arg) <- DTString(string_of_symbol fun_exit)
		| 4 -> !memory.(to_address arg) <- DTPointer(Int32.of_int 0)
		| 5 -> !memory.(to_address arg) <- DTString(string_of_symbol fun_uncaught_exception)
		| 6 -> !memory.(to_address arg) <- DTPointer(Int32.of_int 0)
		| _ -> failwith "exec_main: to many arguments to main function"
		end in
			succ index
	) 0 args in
	
	(* begin the execution of the program *)
	List.iter (fun inst -> exec_inst inst) code;
		to_int !return_value

(*
 * exec_prog
 * 
 * Purpose:
 *	This is the function that allows the outside world to interface with the 
 *	simulator. Once the program has been loaded with 'load_prog' then this
 *	function is called. If the program has not been loaded then an error
 *	message is displayed to the screen.
 *)
let exec_prog() = 
	if !loaded = false then
		failwith "MVM: no program loaded"
	else
		();

	exec_inits();
		Rawint.to_int (exec_main())
