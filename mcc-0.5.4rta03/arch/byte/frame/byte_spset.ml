open Symbol

open Byte_frame_type
open Byte_util

(*
 * spset_mem
 *
 * Purpose
 *	Checks whether or not a variable has already been added
 *	to the spill set. We don't want duplicate variables in
 *	the spill set.
 *)
let spset_mem sp_set v = SymbolTable.mem sp_set.spset_vars v

(*
 * spset_spill
 *
 * Purpose:
 *	This function adds a new variable to the spill set and 
 * 	assigns it a memroy location (register number) if it 
 *	is not already in the spill set.
 *)
let spset_spill sp_set v cls =
	if spset_mem sp_set v then
		sp_set
	else
		let index = sp_set.spset_spill_count in
		{ 
			spset_spill_count = succ index;
			spset_vars = SymbolTable.add sp_set.spset_vars v (index,cls);
		}

(*
 * spset_initialize
 *
 * Purpose:
 *	This initialize the spill set with the globals and the
 *	imports of the current source file.
 *)
let spset_initialize globals imports =
	let sp_set = 
	{
		spset_spill_count = 0;
		spset_vars = SymbolTable.empty
	} in
	
	let sp_set = SymbolTable.fold (fun sp_set v dt ->
		spset_spill sp_set v (data_class_of_data_type dt)) sp_set globals in
	
	SymbolTable.fold (fun sp_set v import ->
		match import with 
		  Variable ->
			{ sp_set with spset_vars = SymbolTable.add sp_set.spset_vars v (-1,DCNone) }
		| _ ->
			sp_set
	) sp_set imports

(*
 * spset_add
 *
 * Purpose:
 *	add a new variable to the spill set
 *)
let spset_add sp_set v addy dt =
	if SymbolTable.mem sp_set.spset_vars v then
		sp_set
	else
		{ sp_set with
			spset_vars = SymbolTable.add sp_set.spset_vars v (addy,(data_class_of_data_type dt));
		}
	
(*
 * spset_spill_count
 *
 * Purpose:
 *	returns the size of the spill set (number of registers allocated)
 *)
let spset_spill_count sp_set = sp_set.spset_spill_count	

(*
 * spset_spill_location
 *
 * Purpose:
 *	returns the register/memory location of the variable
 *)
and spset_spill_location sp_set v =
	let index,_ = SymbolTable.find sp_set.spset_vars v in
		index

(*
 * spset_spill_data_class 
 *
 * Purpose:
 *	returns the 'data_class' of the specified variable in the 
 *	spill set
 *)		
and spset_spill_data_class sp_set v =
	let _,cls = SymbolTable.find sp_set.spset_vars v in
		cls

(*
 * spset_empty
 *
 * Purpose:
 *	Creates an empty spill set.
 *)
and spset_empty() =
	{
		spset_spill_count = 0;
		spset_vars = SymbolTable.empty;
	}

(*
 * spset_set_spill_count
 *
 * Purpose:
 *	Forces the size of the spill set. This function should never be
 *	used when compiling source code. This function is mainly for 
 *	linking multiple files, or reconstrunting 'prog' from a bytecode
 *	file.
 *)
and spset_set_spill_count sp_set count =
	{ sp_set with
		spset_spill_count = count;
	}
