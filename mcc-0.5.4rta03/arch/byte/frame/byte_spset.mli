open Symbol

open Byte_frame_type

(*
 * These are functions used to create the data section of the bytecode. These
 * functions are not used by the RA. As a matter of fact, there is no 
 * interaction w/ the register allocator b/c it is all done by hand when 
 * transforming the code.
 *)
val spset_empty : unit -> spset
val spset_initialize : data_type SymbolTable.t -> symbol_type SymbolTable.t -> spset
val spset_mem : spset -> symbol -> bool
val spset_spill : spset -> var -> data_class -> spset

val spset_spill_count : spset -> int

val spset_spill_location : spset -> symbol -> int
val spset_spill_data_class : spset -> symbol -> data_class


(*
 * The following function are used when reconstructing the data spset
 * from an bytecode obejct file or executable.
 *
 * NOTE: NEVER use these functions when compiling source code to an
 * object file.
 *)
val spset_add : spset -> symbol -> int -> data_type -> spset
val spset_set_spill_count : spset -> int -> spset
