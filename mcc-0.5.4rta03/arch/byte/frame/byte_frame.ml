open Symbol

(*
 * Exit function used by the bytecode/simulator
 *) 
let fun_exit = Symbol.add "exit"
let fun_exit_init = Symbol.add "exit_init"
let fun_uncaught_exception = Symbol.add "uncaught_excpetion"

let exit_funs = [ fun_exit;
				  fun_exit_init;
				  fun_uncaught_exception ]

(*
 * Extern functions
 *)
let fc_print_string = Symbol.add "fc_print_string"
let fc_print_char = Symbol.add "fc_print_char"
let fc_print_int = Symbol.add "fc_print_int"

let util_funs = [ fc_print_string;
				  fc_print_char;
				  fc_print_int ]
				
let native_funs = exit_funs @ util_funs
	
let is_native_fun v = List.mem v native_funs
