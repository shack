/*
 * FC stdarg.
 * Functions with variabl number of argument have their arguments
 * passed as a tuple.  Argument access is simple, we just walk through
 * the tuple in order.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#ifndef _STDARG_H
#define _STDARG_H

/*
 * Pick up fc.h contents
 */
#include <fc.h>

/*
 * Pointer into the arg list.
 */
typedef char *va_list;

/* Fake the type for gcc */
typedef va_list __gnuc_va_list;

/*
 * Post-increment.
 */
static va_list __va_arg_post_incr(va_list &ap, unsigned size)
{
    va_list ap_orig = ap;
    ap += size;
    return ap_orig;
}

/*
 * When starting, step past the supplied argument.
 */
#define va_start(ap, v) ((ap) = (va_list) &(v) + sizeof(v))

/*
 * Get an argument at the current position,
 * then increment the position.
 */
#define va_arg(ap, t)   (*(t *)__va_arg_post_incr(ap, sizeof(t)))

/*
 * Don't need to do anything at the end.
 */
#define va_end(ap)

#endif /* _STDARG_H */
