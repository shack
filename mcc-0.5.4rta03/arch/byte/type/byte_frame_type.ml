open Symbol
open Trace

open Frame_type 

open Byte_inst_type

(*
 * Some helpful types
 *)
type label 		= symbol
type var 		= symbol
type inst 		= Byte_inst_type.inst
type debug_line = Byte_inst_type.debug_line

(*
 * This is the different data classes with its equivalent data type that can 
 * occur in the bytecode. When a new data type is added to the MIR, or the
 * the bytecode itself, both types must be updated to handle the changes. It
 * would be best to keep the same naming convention already established when
 * adding new types and classes.
 *)
type data_class =
	  DCChar
	| DCInt 	of Rawint.int_precision * Rawint.int_signed
	| DCFloat 	of Rawfloat.float_precision
	| DCPointer
	| DCString
	| DCTuple	of data_class list
	| DCArray	of data_class
	| DCNone
	
and data_type =
	  DTChar	of char
	| DTInt 	of Rawint.rawint	
	| DTFloat	of Rawfloat.rawfloat
	| DTPointer of int32					(* this type is incorrect *)
	| DTString	of string
	
	(* these type are for special 'data_types' *)
	| DTTuple	of data_type list			(* tuples/unions *)
	| DTArray	of data_type array			(* arrays *)
	| DTNone

(*
 * This is used when converting the import and export tables. More infomation
 * can be added to each constructor, such as function type information, if
 * neccessary. These types are easily expandable.
 *)
and symbol_type =
	  Function	(* of data_class list *)
	| Variable

(*
 * This is the basic block for the bytecode.
 *)
type 'inst block = {
	block_debug : debug_line option;
	block_label : label;
	block_args : operand list;
	block_inst : 'inst list;
}

(*
 * These are the different type of blocks. See 'frame_type.ml' for more
 * information.
 *)
type inst_block = inst block
type code_block = inst code block
type live_block = inst live block

(*
 * This is the spill set used to create the data section of the bytecode,
 * or you can look at it as the register set for the bytecode.
 *)
type spset = {
	spset_spill_count : int;
	spset_vars : (int * data_class) SymbolTable.t;
}

(*
 * This the program layout for each source file that must be converted
 * into a bytecode file.
 *)
type prog = {
	byte_import		: symbol_type SymbolTable.t;
	byte_export		: symbol_type SymbolTable.t;
	byte_global		: data_type SymbolTable.t;
	byte_funs		: inst_block trace;
	byte_data		: spset;
}
