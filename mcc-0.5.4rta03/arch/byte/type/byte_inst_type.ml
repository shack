open Symbol
open Fir_set

(* 
 * basic types
 *)
type var = Symbol.symbol
type label = Symbol.symbol

type prec =
	  IP
	| FP

type imm =
	  Int of Rawint.rawint
	| Float of Rawfloat.rawfloat	
(*
 * There are your basic operand types for instructions. When emiting the 
 * final output for the source file, you want each operand to be of the 
 * type 'Memory i', except for those instructions where they require an
 * operand of the other type.
 *)
type operand = 
	  Label 	of label	
 	| Var 		of var
	| Memory	of int

(*
 * Info for debugging.
 *)
type debug_line = Location.loc

(* basic registers types *)
type dest = operand
type src = operand
type addy = operand

(******************************************************************************
 * Bytecode instructions
 ******************************************************************************)
 type inst =
 	(* integer operations *)
 	  Add of dest * src * src			(* addition *)
	| Sub of dest * src * src			(* subtraction *)
	| Mul of dest * src * src			(* multiplication *)
	| Div of dest * src * src			(* division *)
	| Rem of dest * src * src			(* remainder *)
	| Max of dest * src * src			(* max *)
	| Min of dest * src * src			(* min *)
	
	| Shl 	of dest * src * src			(* shift left *)
	| AShr	of dest * src * src			(* arithemetic shift right *)
	| LShr	of dest * src * src			(* logical shift right set*)
	| And	of dest * src * src			(* bit-wise AND *)
	| Or	of dest * src * src			(* bit-wise OR *)
	| Xor	of dest * src * src			(* bit-wise XOR *)

	| Not	of dest * src				(* bit-wise NOT *)
	
	| UMinus 	of dest * src			(* arithmetic negation *)
	| Abs		of dest * src			(* absolute value *)
	
	(* floating-point operations *)
	| FAdd of dest * src * src			(* addition *)
	| FSub of dest * src * src			(* subtraction *)
	| FMul of dest * src * src			(* multiplication *)
	| FDiv of dest * src * src			(* division *)
	| FRem of dest * src * src			(* remainder *)
	| FMax of dest * src * src			(* max *)
	| FMin of dest * src * src			(* min *)
	
	| FUMinus	of dest * src			(* arithmetic negation *)
	| FAbs		of dest * src			(* absolute value *)

	| Sin	of dest * src				(* sine *)
	| Cos	of dest * src				(* cosine *)
	| Sqrt	of dest * src				(* square root *)
	
	(* coersion operations *)
	| FToUInt8		of dest * src		(* float to unsigned Rawint.Int8 *)
	| FToUInt16		of dest * src		(* float to unsigned Rawint.Int16 *)
	| FToUInt32		of dest * src		(* float to unsigned Rawint.Int32 *)
	| FToUInt64		of dest * src		(* float to unsigned Rawint.Int64 *)

	| FToInt8		of dest * src		(* float to signed Rawint.Int8 *)
	| FToInt16		of dest * src		(* float to signed Rawint.Int16 *)
	| FToInt32		of dest * src		(* float to signed Rawint.Int32 *)
	| FToInt64		of dest * src		(* float to signed Rawint.Int64 *)
	
	| FToFloat32	of dest * src		(* float to Rawfloat.Single *)
	| FToFloat64	of dest * src		(* float to Rawfloat.Double *)
	| FToFloat80	of dest * src		(* float to Rawfloat.LongDouble *)
	
	| IToFloat32	of dest * src		(* integer to Rawfloat.Single *)
	| IToFloat64	of dest * src		(* integer to Rawfloat.Double *)
	| IToFloat80 	of dest * src		(* integer to Rawfloat.LongDouble *)
	
	| IToInt8		of dest * src		(* integer to signed Rawint.Int8 *)
	| IToInt16		of dest * src		(* integer to signed Rawint.Int16 *)
	| IToInt32		of dest * src		(* integer to signed Rawint.Int32 *)
	| IToInt64		of dest * src		(* integer to signed Rawint.Int64 *)
	| IToUInt8		of dest * src		(* integer to unsigned Rawint.Int8 *)
	| IToUInt16		of dest * src		(* integer to unsigned Rawint.Int16 *)
	| IToUInt32		of dest * src		(* integer to unsigned Rawint.Int32 *)
	| IToUInt64		of dest * src		(* integer to unsigned Rawint.Int64 *)
	
	(* assigment operation *)
	| Set of dest * src					(* move data b/t memory locations *)
	
	(* integer comparision operations *)
	| Eq	of dest * src * src			(* equal *)
	| Neq	of dest * src * src			(* not equal *)
	| Gt	of dest * src * src			(* greater than *)
	| Lt	of dest * src * src			(* less than *)
	| Gte	of dest * src * src			(* greater than or equal *)
	| Lte	of dest * src * src			(* less than or equal *)
	| Cmp	of dest * src * src			(* compare *)
	
	(* floating-pointer comparision operations *)
	| FEq	of dest * src * src			(* equal *)
	| FNeq	of dest * src * src			(* not equal *)
	| FGt	of dest * src * src			(* greater than *)
	| FLt	of dest * src * src			(* less than *)
	| FGte	of dest * src * src			(* greater than or equal *)
	| FLte	of dest * src * src			(* less than or equal *)
	| FCmp	of dest * src * src			(* compare *)
	
	(* if-then-else/match operations *)
	| Match	of src * (RawIntSet.t * inst list) list
	
	(* pointer operations *)
	| Load	of addy * dest							(* load data at pointer address *)
	| Store of addy * src							(* store data at pointer/memory address *)

	(* function operations *)
	| Goto		of dest * src list					(* indirect tail-call *)
	| Call		of operand * src list				(* direct tail-call *)
	| CallEx	of operand * src list				(* external C [native] tail-call *)
	| Extern	of operand * string * src list		(* external C [native] function w/ return *)
	
   	(* Comment and nil pseudo-codes *)
 	| CommentString of string
 	| CommentInst 	of string * inst
 	| NOP
