(*
 * This is a temporary hack to borrow the backend/frame from X86.
 * This artificial dependency needs to be removed so the bytecode
 * compiler does not depend on an architecture.
 *)
open Frame_type

module Backend : BackendSig = X86_backend.Backend

module Frame : FrameSig = X86_backend.Frame
