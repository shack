open Trace

open Byte_frame_type

val print_blocks : Format.formatter -> inst block trace -> unit
val print_object : string -> prog -> unit
val print_prog : Format.formatter -> prog -> unit
