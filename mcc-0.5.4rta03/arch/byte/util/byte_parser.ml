open Genlex

open Symbol

open Interval_set
open Fir_set

open Byte_frame_type
open Byte_inst_type
open Byte_spset
open Byte_util

(*
 * The keywords for the bytecode divided into their subgroups. The subgroups
 * can be divided into smaller subgroups if neccessary.
 *)
let keyword_inst = ["add"; "sub"; "div"; "mul"; "rem"; "min"; "max";
					"f_add"; "f_sub"; "f_div"; "f_mul"; "f_rem"; "f_max"; "f_min";
					"eq"; "neq"; "gt"; "lt"; "gte"; "lte"; "cmp";
					"f_eq"; "f_neq"; "f_gt"; "f_lt"; "f_gte"; "f_lte"; "f_cmp";
					"shl"; "ashr"; "lshr"; "and"; "or"; "xor"; "not"; "uminus"; "abs";
					"f_uminus"; "f_abs"; "sin"; "cos"; "sqrt"; 
					"f_2_uint8"; "f_2_uint16"; "f_2_uint32"; "f_2_uint64";
					"f_2_int8"; "f_2_int16"; "f_2_int32"; "f_2_int64";
					"f_2_float32"; "f_2_floot64"; "f_2_float80";
					"i_2_float32"; "i_2_float64"; "i_2_float80";
					"i_2_int8"; "i_2_int16"; "i_2_int32"; "i_2_int64";
					"i_2_uint8"; "i_2_uint16"; "i_2_uint32"; "i_2_uint64";
					"set";
					"goto"; "call"; "callex"; "extern";
					"match"; "case";
					"end"]
					
let keyword_operator = ["$"; "."; ":"; ","; "("; ")"; "{"; "}"; "["; "]"; "="; "..."]

let keyword_word = ["function"; "data"; "datasize"; "init"; "as"; "empty"; "imports"; "exports";
					"fun"; "var"]

let keyword_type = ["char"; "string";
					"int8"; "int16"; "int32"; "int64";
					"uint8"; "uint16"; "uint32"; "uint64";
					"float32"; "float64"; "float80";
					"ptr";
					"tuple"; "array"]

(* 
 * A list of all the keywords in the bytecode.
 *)
let keyword = keyword_inst @ keyword_operator @ keyword_type @ keyword_word

(* 
 * The lexer used to parse the source code .
 *)
let lexer = make_lexer keyword

(*
 * prog_of_stream
 *
 * Purpose:
 *	This function is the brains of the bytecode parser. It is responsible for
 *	creating the program given a stream. Based on the header that is read by
 *	this function, it calls its helper functions to construct the specified 
 *	section.
 *)
let rec prog_of_stream prog stream = 
	let pos = "prog_of_stream: " in

	try 
		match type_token_of_stream stream with
		  Kwd("datasize") ->
			let prog = datasize_of_stream prog stream in
				prog_of_stream prog stream
		| Kwd("data") ->
			let prog = data_of_stream prog stream in
				prog_of_stream prog stream
		| Kwd("function") ->
			let prog = function_of_stream prog stream in 
				prog_of_stream prog stream
	 	| Kwd("imports") ->
			let prog = imports_of_stream prog stream in
				prog_of_stream prog stream
		| Kwd("exports") ->
			let prog = exports_of_stream prog stream in
				prog_of_stream prog stream
 	 	| _  as header ->
			failwith (pos ^ "undefined section header (" ^ (string_of_token header) ^ ")")		 
	with
	  Stream.Failure -> prog
	| Stream.Error err -> failwith (pos ^ err)

(*
 * datasize_of_stream
 *
 * Purpose:
 *	The datasize section of the bytecode is constructed with this function
 *	and is added to the program structure.
 *)	
and datasize_of_stream prog stream =
	let size = value_of_stream stream in
	
	match size with
	  Genlex.Int(i) -> 
	  	{ prog with
			byte_data = spset_set_spill_count prog.byte_data i;
		}
	| _ -> failwith "datasize_of_stream: expected an integer"

(*
 * data_of_stream
 *
 * Purpose:
 *	This function is responsible for handling the data section
 * 	of the bytecode file. The following functions are considered
 *	helper functions of this function:
 *
 * 		tuple_of_stream
 *		addy_of_stream
 *		as_var_of_stream
 *		value_of_stream
 *		data_type_of_token
 *)
and data_of_stream prog stream =
	let pos = "data_of_stream: " in
	let dt = type_token_of_stream stream in
	
	match dt with
	  Kwd("char")
	| Kwd("string") 
	| Kwd("int8")
	| Kwd("int16")
	| Kwd("int32")
	| Kwd("int64")
	| Kwd("uint8")
	| Kwd("uint16")
	| Kwd("uint32")
	| Kwd("uint64")
	| Kwd("float32")
	| Kwd("float64")
	| Kwd("float80") as ty ->
		let addy = addy_of_stream stream in
		let val' = value_of_stream stream in
		let v = as_var_of_stream stream in
		
		let dt = data_type_of_token val' ty in

		let prog = { prog with
			byte_global = SymbolTable.add prog.byte_global v dt;
			byte_data = spset_add prog.byte_data v addy dt;
		} in
			data_of_stream prog stream			
	| Kwd("tuple") ->
		let prog = tuple_of_stream prog stream in 
			data_of_stream prog stream
	| Kwd("end") ->
		prog
	| Kwd("array")
	| Kwd("ptr")
	| _  as dt -> 
		failwith (pos ^ "undefined data type (" ^ (string_of_token dt) ^ ")")

(*
 * exports_of_stream
 *
 * Purpose:
 *	This handles this export section of the bytecode file. There is nothing
 *	complex about this function. The following functions are considered
 *	helper functions of this function:
 *
 *		type_token_of_stream
 *)
and exports_of_stream prog stream =
	let pos = "imports_of_stream: " in
	
	match type_token_of_stream stream with
	  Kwd("fun")
	| Kwd("var") as ty ->
		let v = begin match Stream.next stream with
		  Ident(s) -> Symbol.add s
		  | _ -> failwith (pos ^ "expected symbol")
		end in
		
		(* Is the export a function or a variable? *)
		begin match ty with	  
		  Kwd("fun") -> 
			let prog = { prog with byte_export = SymbolTable.add prog.byte_export v Function } in
				exports_of_stream prog stream
		| Kwd("var") ->
			let prog = { prog with byte_export = SymbolTable.add prog.byte_export v Variable } in
				exports_of_stream prog stream
		| _ -> failwith (pos ^ "expected symbol type")
		end
	| Kwd("end") ->
		(* we have reached the end of this section *)
		prog
	| _ -> failwith (pos ^ "syntax error")
	
(*
 * imports_of_stream
 *
 * Purpose:
 *	This function handles the import section of the bytecode file. It is
 * 	the exact same function as 'export_of_stream', the function above. The
 *	only difference is that it saves to the import table, instead of the 
 *	export table.
 *)
and imports_of_stream prog stream =
	let pos = "imports_of_stream: " in
	
	match type_token_of_stream stream with
	  Kwd("fun")
	| Kwd("var") as ty ->
		let v = begin match Stream.next stream with
		  Ident(s) -> Symbol.add s
		  | _ -> failwith (pos ^ "expected symbol")
		end in
		
		(* Is the import a function or a variable? *)
		begin match ty with	  
		  Kwd("fun") -> 
			let prog = { prog with byte_import = SymbolTable.add prog.byte_import v Function } in
				imports_of_stream prog stream
		| Kwd("var") ->
			let prog = { prog with byte_import = SymbolTable.add prog.byte_import v Variable } in
				imports_of_stream prog stream
		| _ -> failwith (pos ^ "expected symbol type")
		end
	| Kwd("end") ->
		(* we have reached the end of this section *)
		prog
	| _ -> failwith (pos ^ "syntax error")

(*
 * function_of_stream
 *
 * Purpose:
 *	This function is resposible for reconstructing a single function from
 *	the given stream. The first token of the stream should be the symbol
 *	name for the function, followed by its arguments then its instructions.
 * 	The following functions are helper functions of this functions:
 *
 *		arg_list_of_stream
 *		inst_of_stream
 *)
and function_of_stream prog stream =
	let pos = "function_of_stream: " in
	
	(* read the name of the function *)
	let fun_label = 
		begin match Stream.next stream with
		  Ident(s) -> Symbol.add s
		| _ -> failwith (pos ^ "expected function name")
		end
	in
	
	(* get the arguments for the function *)
	let args = arg_list_of_stream [] stream in
	
	(* get the instructions for the function *)
	let code = inst_of_stream [] stream in
	
	let block = {
		block_debug = None;
		block_label = fun_label;
		block_args = args;
		block_inst = code;
	} in
	
	(* add the function to the instruction trace *)
	let trace = prog.byte_funs in
	let fun_list = Trace.to_list trace in
	let fun_list = block :: fun_list in
		{ prog with	byte_funs = Trace.of_list fun_list }

(*
 * tuple_of_stream
 *
 * Purpose:
 *	Returns a tuple 'data_type' from the stream, given a tuple
 *	'data_type' is next in the stream.
 *)	
and tuple_of_stream prog stream =
	let pos = "tuple_of_stream: " in
	
	let tuple_value_of_stream stream = 
		let pos = pos ^ "tuple_value_of_stream: " in
		match Stream.next stream with
		  Kwd(":") -> Stream.next stream
		| _ -> failwith (pos ^ "expected colon (:) operator")
	in
	
	let rec tuple_list_of_stream l stream =
		let pos = pos ^ "tuple_list_of_stream: " in
		
		match Stream.peek stream with
		  Some(Kwd "(") ->
		  	(* create a new tuple data type *)
		 	ignore(Stream.next stream);
		 	let dt = DTTuple(tuple_list_of_stream [] stream) in
				dt :: l
		| Some(Kwd ")") ->
			(* there are no more elements in the tuple *)
			ignore(Stream.next stream);
				List.rev l
		| Some(Kwd ",") ->
			(* there are more elements in the tuple *)
			ignore(Stream.next stream);
				tuple_list_of_stream l stream
		| _ ->
		 	(* add the element to the tuple *)
			let ty = type_token_of_stream stream in
			let v = tuple_value_of_stream stream in
			let dt = data_type_of_token v ty in
				tuple_list_of_stream (dt :: l) stream
	in

	let addy = addy_of_stream stream in
	
	(* check for the equal sign *)
	let _ = begin match Stream.next stream with 
	  Kwd("=") -> ()
	| _ -> failwith (pos ^ "expected equal (=) sign")
	end in

	(* check for the open parenthesis; it marks the beginning of the tuple *)
	let _ = begin match Stream.next stream with
	  Kwd("(") -> ()
	| _ -> failwith (pos ^ "expected open parenthesis")
	end in
		
	(* read the data elements of the tuple *)
	let dt = DTTuple(tuple_list_of_stream [] stream) in
	let v = as_var_of_stream stream in
	
		{ prog with
			byte_global = SymbolTable.add prog.byte_global v dt;
			byte_data = spset_add prog.byte_data v addy dt;
		}

(*
 * addy_of_stream
 *
 * Purpose:
 *	returns the memory operand ($i), which is the next value in the stream
 *)		
and addy_of_stream stream =
	let pos = "addy_of_stream: " in

	let _ = begin match Stream.next stream with
 	  Kwd(":") -> ();
	| _ -> failwith (pos ^ "expected colon (:) operator")
	end in
	
	let _ = begin match Stream.next stream with
	  Kwd("$") -> ()
	| _ -> failwith (pos ^ "expected memory ($) operator")
	end in
		
	match Stream.next stream with
	  Genlex.Int(i) -> i
	| _ -> failwith (pos ^ "invalid address")

(*
 * as_var_of_stream
 *
 * Purpose:
 *	This is used when reading the data section of the stream. It returns the 
 *	variable name associated w/ the data.
 *)
and as_var_of_stream stream =
	let pos = "as_var_of_stream: " in
	
	match Stream.next stream with 
	  Kwd("as") ->
		(* read the varaible name *)
		let next = Stream.next stream in
		begin match next with
		  Ident(s) ->
		  	Symbol.add s
		| Kwd(".") ->
			begin match Stream.next stream with
			  Ident(s) -> 
			  	Symbol.add ("." ^ s)
			| _ -> failwith (pos ^ "expected a variable name")
			end
		| _ -> failwith (pos ^ "expected a variable name")
		end
	| _ -> failwith (pos ^ "expected the keyword \"as\" operator")

(*
 * type_token_of_stream
 *
 * Purpose:
 *	This function returns type information from the next token in the
 *	stream. Type informtion are keywords that follow a period (.) operator.
 *)
and type_token_of_stream stream =
	match Stream.next stream with 
	  Kwd(".") -> Stream.next stream
	| _ -> failwith "type_token_of_stream: expected dot (.) operator\n"
		
(*
 * value_of_stream
 *
 * Purpose:
 *	This function validates that there is an equal (=) sign as the 
 *	next token in the stream. If there is, then it returns the next
 *	token in the stream. If not, then an error message is displayed
 * 	and the program terminates.
 *)
and value_of_stream stream =
	match Stream.next stream with
	  Kwd("=") -> Stream.next stream
	| _ -> failwith "value_of_stream: expected equal (=) sign"

(*
 * data_type_of_token
 *
 * Purpose:
 *	The function returns the 'data_type' given a token. See
 *	'byte_frame_type.ml' for more information on the different
 *	data types.
 *)
and data_type_of_token v tok =
	let pos = "data_type_of_token: " in
	
	match tok with
	  Kwd("char") -> 
	  	begin match v with
		  Genlex.Char(c) -> DTChar(c)
		| _ -> failwith (pos ^ "expected char value")
		end
	| Kwd("string") ->
		begin match v with
		  Genlex.String(s) -> DTString(s)
		| _ -> failwith (pos ^ "expected string value")
		end
	| Kwd("int8") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int8 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("int16") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int16 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("int32") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int32 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("int64") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int64 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint8") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int8 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint16") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int16 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint32") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int32 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint64") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int64 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("float32") ->
		begin match v with
		  Genlex.String(s) -> DTFloat(Rawfloat.of_string Rawfloat.Single s)
		| _ -> failwith (pos ^ "expected floating-point value")
		end
	| Kwd("float64") ->
		begin match v with
		  Genlex.String(s) -> DTFloat(Rawfloat.of_string Rawfloat.Double s)
		| _ -> failwith (pos ^ "expected floating-point value")
		end
	| Kwd("float80") ->
		begin match v with
		  Genlex.String(s) -> DTFloat(Rawfloat.of_string Rawfloat.LongDouble s)
		| _ -> failwith (pos ^ "expected floating-point value")
		end
	| _  as data -> failwith (pos ^ "undefined data type (" ^ (string_of_token data) ^ ")")	

(*
 * inst_of_stream
 *
 * Purpose:
 *	This function is is responsible for handling the instructions
 *	for a function. Based on the classification of the instruction
 *	it is dispatched to the correct of the helper function, or an
 *	error message is displayed. The following functions are helpers
 * 	of this function.
 *
 *		binop_inst_of_stream
 *		unop_inst_of_stream
 *		tailcall_of_stream
 *		extern_of_stream
 *)
and inst_of_stream l stream = 
	let pos = "inst_of_stream: " in
	
	match Stream.next stream with
	  Kwd "add"
	| Kwd "sub"
	| Kwd "div"
	| Kwd "mul"
	| Kwd "rem"
	| Kwd "min"
	| Kwd "max"
	| Kwd "f_add"
	| Kwd "f_sub"
	| Kwd "f_div"
	| Kwd "f_mul"
	| Kwd "f_rem"
	| Kwd "f_max"
	| Kwd "f_min"
	| Kwd "eq"
	| Kwd "neq"
	| Kwd "gt"
	| Kwd "lt"
	| Kwd "gte"
	| Kwd "lte"
	| Kwd "cmp"
	| Kwd "f_eq"
	| Kwd "f_neq"
	| Kwd "f_gt"
	| Kwd "f_lt"
	| Kwd "f_gte"
	| Kwd "f_lte"
	| Kwd "f_cmp"
	| Kwd "shl"
	| Kwd "ashr"
	| Kwd "lshr"
	| Kwd "and"
	| Kwd "or"
	| Kwd "xor" as inst ->
		let l = (binop_inst_of_stream inst stream) :: l in
			inst_of_stream l stream			
	| Kwd "not"
	| Kwd "uminus"
	| Kwd "abs"
	| Kwd "f_uminus"
	| Kwd "f_abs"
	| Kwd "sin"
	| Kwd "cos"
	| Kwd "sqrt"
	| Kwd "f_2_uint8"
	| Kwd "f_2_uint16"
	| Kwd "f_2_uint32"
	| Kwd "f_2_uint64"
	| Kwd "f_2_int8"
	| Kwd "f_2_int16"
	| Kwd "f_2_int32"
	| Kwd "f_2_int64"
	| Kwd "f_2_float32"
	| Kwd "f_2_floot64"
	| Kwd "f_2_float80"
	| Kwd "i_2_float32"
	| Kwd "i_2_float64"
	| Kwd "i_2_float80"
	| Kwd "i_2_int8"
	| Kwd "i_2_int16"
	| Kwd "i_2_int32"
	| Kwd "i_2_int64"
	| Kwd "i_2_uint8"
	| Kwd "i_2_uint16"
	| Kwd "i_2_uint32"
	| Kwd "i_2_uint64"
	| Kwd "set" as inst ->
		let l = (unop_inst_of_stream inst stream) :: l in
			inst_of_stream l stream
	| Kwd "goto"
	| Kwd "call"
	| Kwd "callex" as inst ->
		let l = (tailcall_of_stream inst stream) :: l in
			inst_of_stream l stream
	| Kwd "extern" ->
		let l = (extern_of_stream stream) :: l in
			inst_of_stream l stream
	| Kwd "match" ->
		let l = (match_of_stream stream) :: l in
			inst_of_stream l stream
	| Kwd "case"
	| Kwd "end" ->
		(* we have reached the end of a match or case *)
		List.rev l
	| Kwd "." ->
		(* have we reached the end of the function *)
		begin match Stream.next stream with
		  Kwd "end" -> 
		  	List.rev l
		| _ -> 
			failwith (pos ^ "expected end keyword")
		end
	| _ as inst -> 
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")
	
(*
 * binop_inst_of_stream
 *
 * Purpose:
 *	This function returns an binary instruction of the stream if it
 *	is the next token of the stream, or it displays an error message.
 *)	

and binop_inst_of_stream inst stream =
	let pos = "binop_inst_of_stream: " in
	
	let d = operand_of_stream stream in
	skip_comma stream;
	let s1 = operand_of_stream stream in
	skip_comma stream;
	let s2 = operand_of_stream stream in
	
	match inst with
	  Kwd "add" -> Add(d,s1,s2)	  
	| Kwd "sub" -> Sub(d,s1,s2)	  
	| Kwd "div" -> Div(d,s1,s2)	  
	| Kwd "mul" -> Mul(d,s1,s2)	  
	| Kwd "rem" -> Rem(d,s1,s2)	  
	| Kwd "min" -> Min(d,s1,s2)	  
	| Kwd "max" -> Max(d,s1,s2)	  
	| Kwd "f_add" -> FAdd(d,s1,s2)	  
	| Kwd "f_sub" -> FSub(d,s1,s2)	  
	| Kwd "f_div" -> FDiv(d,s1,s2)	  
	| Kwd "f_mul" -> FMul(d,s1,s2)	  
	| Kwd "f_rem" -> FRem(d,s1,s2)	  
	| Kwd "f_max" -> FMax(d,s1,s2)	  
	| Kwd "f_min" -> FMin(d,s1,s2)	  
	| Kwd "eq" -> Eq(d,s1,s2)	  
	| Kwd "neq" -> Neq(d,s1,s2)	  
	| Kwd "gt" -> Gt(d,s1,s2)	  
	| Kwd "lt" -> Lt(d,s1,s2)	  
	| Kwd "gte" -> Gte(d,s1,s2)	  
	| Kwd "lte" -> Lte(d,s1,s2)	  
	| Kwd "cmp" -> Cmp(d,s1,s2)	  
	| Kwd "f_eq" -> FEq(d,s1,s2)	  
	| Kwd "f_neq" -> FNeq(d,s1,s2)	  
	| Kwd "f_gt" -> FGt(d,s1,s2)	  
	| Kwd "f_lt" -> FLt(d,s1,s2)	  
	| Kwd "f_gte" -> FGte(d,s1,s2)	  
	| Kwd "f_lte" -> FLte(d,s1,s2)	  
	| Kwd "f_cmp" -> FCmp(d,s1,s2)	  
	| Kwd "shl" -> Shl(d,s1,s2)	  
	| Kwd "ashr" -> AShr(d,s1,s2)	  
	| Kwd "lshr" -> LShr(d,s1,s2)	  
	| Kwd "and" -> And(d,s1,s2)	  
	| Kwd "or" -> Or(d,s1,s2)	  
	| Kwd "xor" -> Xor(d,s1,s2)	  
	| _ as inst ->
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")

(*
 * unop_inst_of_stream
 *
 * Purpose:
 *	This function returns an unary instruction of the stream if it
 *	is the next token of the stream, or it displays an error message.
 *)	
and unop_inst_of_stream inst stream =
	let pos = "binop_inst_of_stream: " in
	
	let d = operand_of_stream stream in
	skip_comma stream;
	let s = operand_of_stream stream in
	
	match inst with
	  Kwd "not" -> Not(d,s)
	| Kwd "uminus" -> UMinus(d,s)
	| Kwd "abs" -> Abs(d,s)
	| Kwd "f_uminus" -> FUMinus(d,s)
	| Kwd "f_abs" -> FAbs(d,s)
	| Kwd "sin" -> Sin(d,s)
	| Kwd "cos" -> Cos(d,s)
	| Kwd "sqrt" -> Sqrt(d,s)
	| Kwd "f_2_uint8" -> FToUInt8(d,s)
	| Kwd "f_2_uint16" -> FToUInt16(d,s)
	| Kwd "f_2_uint32" -> FToUInt32(d,s)
	| Kwd "f_2_uint64" -> FToUInt64(d,s)
	| Kwd "f_2_int8" -> FToInt8(d,s)
	| Kwd "f_2_int16" -> FToInt16(d,s)
	| Kwd "f_2_int32" -> FToInt32(d,s)
	| Kwd "f_2_int64" -> FToInt64(d,s)
	| Kwd "f_2_float32" -> FToFloat32(d,s)
	| Kwd "f_2_floot64" -> FToFloat64(d,s)
	| Kwd "f_2_float80" -> FToFloat80(d,s)
	| Kwd "i_2_float32" -> IToFloat32(d,s)
	| Kwd "i_2_float64" -> IToFloat64(d,s)
	| Kwd "i_2_float80" -> IToFloat80(d,s)
	| Kwd "i_2_int8" -> IToInt8(d,s)
	| Kwd "i_2_int16" -> IToInt16(d,s)
	| Kwd "i_2_int32" -> IToInt32(d,s)
	| Kwd "i_2_int64" -> IToInt64(d,s)
	| Kwd "i_2_uint8" -> IToUInt8(d,s)
	| Kwd "i_2_uint16" -> IToUInt16(d,s)
	| Kwd "i_2_uint32" -> IToUInt32(d,s)
	| Kwd "i_2_uint64" -> IToUInt64(d,s)
	| Kwd "set" -> Set(d,s)
	| _ as inst ->
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")

(* 
 * tailcall_of_stream
 *
 * Purpose:
 *	This function returns an tailcall instruction of the stream if it
 *	is the next token of the stream, or it displays an error message.
 *)	
and tailcall_of_stream inst stream =
	let pos = "tailcall_of_stream: " in
	
	let v = operand_of_stream stream in
	let args = arg_list_of_stream [] stream in
	
	match inst with
	  Kwd "goto" ->	Goto(v,args) 	
	| Kwd "call" -> Call(v,args)	
	| Kwd "callex" -> CallEx(v,args)
	| _ -> 
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")

(*
 * match_of_stream
 *
 * Purpose:
 *	Reconstructs a match instrucntion from the stream. The helper functions for this
 *	function are the following:
 *
 *		case_of_stream
 *		rawset_of_stream
 *)
and match_of_stream stream =
	let pos = "match_of_stream: " in
	
	(* read the match as a argument list that only contains 1 argument *)
	let v = arg_list_of_stream [] stream in
	let v = List.hd v in
	
	(* begin the cases *)
	match Stream.next stream with
	  Kwd "case" ->
		let cases = case_of_stream [] stream in
			Match(v,cases)
	| _ -> failwith (pos ^ "expected case statement")

(*
 * case_of_stream
 *
 * Purpose:
 *	This function returns a case for the match statement if 
 *	if is next in the stream.
 *)		
and case_of_stream cases stream =
	let pos = "case_of_stream: " in
	
	match Stream.peek stream with
	  Some(Kwd "(") -> 
		(* create a new case *)
		ignore(Stream.next stream);
		
	  	let s = rawset_of_stream stream in
		let code = inst_of_stream [] stream in
		let cases = (s,code) :: cases in
			case_of_stream cases stream
	| _ ->
		List.rev cases
(*
 * rawset_of_stream
 *
 * Purpose:
 *	Returns a rawset of a stream, given it is next in the stream. This is 
 *	used in conjunction w/ the function 'match_of_stream'. A sample rawset
 *	is as follows:
 *
 *		.type	{[lo,hi],(lo,hi]}
 *)	
and rawset_of_stream stream =
	let pos = "rawset_of_stream: " in
	
	let bounds_of_stream (p,s) stream =
		let lo_bound = begin match Stream.next stream with
		  Kwd "[" ->
		  	(* closed interval lower bound *)
		  	begin match Stream.next stream with
			  Genlex.Int(i) -> Closed(Rawint.of_int p s i)
			| _ -> failwith (pos ^ "expected integer value")
			end 
		| Kwd "(" ->
			(* open interval lower bound *)
		  	begin match Stream.next stream with
			  Genlex.Int i -> Open(Rawint.of_int p s i)
			| Kwd "." -> Infinity
			| _ -> failwith (pos ^ "expected bound value")
			end
		| _ -> failwith (pos ^ "expected bounds operator")
		end in
		
		ignore(Stream.next stream);
		
		let hi_bound = begin match Stream.next stream with
		  Genlex.Int i ->
		  	begin match Stream.next stream with
			  Kwd ")" -> Open(Rawint.of_int p s i)
			| Kwd "]" -> Closed(Rawint.of_int p s i)
			| _ -> failwith (pos ^ "expected integer value")
			end
		| Kwd "." -> 
			ignore(Stream.next stream);
			Infinity
	 	| _ -> failwith (pos ^ "expected upper bound value")
		end in
			lo_bound, hi_bound
	in
 	
	let rec set_of_stream (p,s) set stream =
		match Stream.next stream with
		  Kwd "{" -> 
		  	let lo,hi = bounds_of_stream (p,s) stream in
			let new_set = RawIntSet.of_interval p s lo hi in
			let set = RawIntSet.union set new_set in
			  	set_of_stream (p,s) set stream
		| Kwd "}" -> 
			set
		| Kwd "," ->
		  	let lo,hi = bounds_of_stream (p,s) stream in
			let new_set = RawIntSet.of_interval p s lo hi in
			let set = RawIntSet.union set new_set in
			  	set_of_stream (p,s) set stream
		| _ -> 
			failwith (pos ^ "expected set operator")
	in
 	
	(* get the type of the rawint set *)
	let dc = type_token_of_stream stream in
	let p,s = begin match dc with 
	  Kwd "int8" -> Rawint.Int8,true
	| Kwd "int16" -> Rawint.Int16,true
	| Kwd "int32" -> Rawint.Int32,true
	| Kwd "int64" -> Rawint.Int64,true
	| Kwd "uint8" -> Rawint.Int8,false
	| Kwd "uint16" -> Rawint.Int16,false
	| Kwd "uint32" -> Rawint.Int32,false
	| Kwd "uint64" -> Rawint.Int64,false
	| _ as tok -> failwith (pos ^ "not a int type (" ^ (string_of_token tok) ^ ")")
	end in
	
	(* create the rawint set *)
	let set = RawIntSet.empty p s in
	let set = set_of_stream (p,s) set stream in 
	
	match Stream.next stream with
	  Kwd ")" -> set
	| _ -> failwith (pos ^ "expected close parenthesis")
	
(* 
 * extern_of_stream
 *
 * Purpose:
 *	This function returns an extern instruction of the stream if it
 *	is the next token of the stream, or it displays an error message.
 *)	
and extern_of_stream stream =
	let pos = "extern_of_stream: " in

	let v = operand_of_stream stream in
	skip_comma stream;
	let name = 
		begin match Stream.next stream with
		  Ident(s) -> s
		| _ -> failwith (pos ^ "expected function name")
		end
	in
	
	let args = arg_list_of_stream [] stream in
		Extern(v,name,args)
	
(*
 * arg_list_of_stream
 *
 * Purpose:
 *	This function reconstructs the argument list for both function
 * 	definitions and function calls. The the stream is invalid, then
 * 	an error message is displayed and the program terminates.
 *)	
and arg_list_of_stream l stream =
	let pos = "arg_list_of_stream: " in
	
	match Stream.next stream with
	  Kwd "(" ->
	  	(* begin a new argument list *)
	  	let l = [(operand_of_stream stream)] in
			arg_list_of_stream l stream
	| Kwd "," ->
		(* read the next argument *)
	  	let l = (operand_of_stream stream) :: l in
			arg_list_of_stream l stream
	| Kwd ")" ->
		(* end of the argument list *)
		List.rev l
	| _ as err ->
		failwith (pos ^ "syntax error (" ^ (string_of_token err) ^ ")")
(*
 * operand_of_stream
 *
 * Purpose:
 *	This function returns an operand, either 'Memory(i)' or 'Var(v)'
 *	if it is the next token in the stream. Otherwise, it prints an
 *	error message and the program terminates.
 *)		
and operand_of_stream stream =
	let pos = "operand_of_stream: " in
	
	match Stream.next stream with
	  Ident(s) -> 
	  	Var(Symbol.add s)
	| Kwd("$") ->
		begin match Stream.next stream with
		| Genlex.Int(i) ->  Memory(i)
		| _ -> failwith (pos ^ "expected memory address")
		end
	| _ as op -> failwith (pos ^ "illegal operand (" ^ (string_of_token op) ^ ")")				

(*
 * skip_comma
 *
 * Purpose:
 *	This function skips the next token in the stream if it is a
 *  comma (,). Otherwise, the function prints an error message to 
 *	the screen and the program terminates.
 *)		
and skip_comma stream =
	match Stream.peek stream with
	  Some(Kwd ",") -> ignore(Stream.next stream)
	| _ -> failwith "expected comma operator"

(*
 * string_of_token
 * 
 * Purpose:
 *	This function is used with printing errors messages to the
 *	screen. It returns a string representing the value of the 
 *	token it is given.
 *)	 
and string_of_token = function
	  Genlex.Kwd(s) -> s
	| Genlex.Ident(s) -> s
	| Genlex.Int(i) -> string_of_int i
	| Genlex.Float(f) -> string_of_float f
	| Genlex.String(s) -> s
	| Genlex.Char(c) -> String.make 1 c
	
(*
 * prog_of_file
 *
 * Purpose:
 *	This function is the entry point for the parser. Given the name of the
 *	file, it is opened and transformed into a token stream. The tokens are
 * 	then parsed to create a 'prog' data structure.
 *)
let prog_of_file in_filename =
	let prog = { 
		byte_import = SymbolTable.empty;
		byte_export = SymbolTable.empty;
		byte_global = SymbolTable.empty;
		byte_funs = [];
		byte_data = spset_empty();
	} in
	
	(* open the output file and transform it into tokens *)
	let in_file = open_in in_filename in
	let token_stream = lexer (Stream.of_channel in_file) in
	
(*  	close_in in_file;   *)
	

	(* parse the tokens to create a 'prog' *)
	let prog = prog_of_stream prog token_stream in
		prog
