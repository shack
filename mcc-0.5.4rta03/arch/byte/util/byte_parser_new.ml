open Genlex

open Symbol
open Trace

open Byte_frame_type
open Byte_inst_type
open Byte_spset
open Byte_util

let no_symbol = new_symbol_string "none"

let keyword_inst = ["add"; "sub"; "div"; "mul"; "rem"; "min"; "max";
					"f_add"; "f_sub"; "f_div"; "f_mul"; "f_rem"; "f_max"; "f_min";
					"eq"; "neq"; "gt"; "lt"; "gte"; "lte"; "cmp";
					"f_eq"; "f_neq"; "f_gt"; "f_lt"; "f_gte"; "f_lte"; "f_cmp";
					"shl"; "ashr"; "lshr"; "and"; "or"; "xor"; "not"; "uminus"; "abs";
					"f_uminus"; "f_abs"; "sin"; "cos"; "sqrt"; 
					"f_2_uint8"; "f_2_uint16"; "f_2_uint32"; "f_2_uint64";
					"f_2_int8"; "f_2_int16"; "f_2_int32"; "f_2_int64";
					"f_2_float32"; "f_2_floot64"; "f_2_float80";
					"i_2_float32"; "i_2_float64"; "i_2_float80";
					"i_2_int8"; "i_2_int16"; "i_2_int32"; "i_2_int64";
					"i_2_uint8"; "i_2_uint16"; "i_2_uint32"; "i_2_uint64";
					"set";
					"goto"; "call"; "callex"; "extern";
					"match"; "case";
					"end"]
					
let keyword_operator = ["$"; "."; ":"; ","; "("; ")"; "{"; "}"; "["; "]"; "="; "..."]

let keyword_word = ["function"; "data"; "datasize"; "init"; "as"; "empty"; "imports"; "exports";
					"fun"; "var"]

let keyword_type = ["char"; "string";
					"int8"; "int16"; "int32"; "int64";
					"uint8"; "uint16"; "uint32"; "uint64";
					"float32"; "float64"; "float80";
					"ptr";
					"tuple"; "array"]

let keyword = keyword_inst @ keyword_operator @ keyword_type @ keyword_word

let lexer = make_lexer keyword

(*
 * prog_of_stream
 *
 * Purpose:
 *	This function is the brains of the bytecode parser. It is responsible for
 *	creating the program given a stream. Based on the header that is read by
 *	this function, it calls its helper functions to construct the specified 
 *	section.
 *)
let rec prog_of_stream prog stream = 
	let pos = "prog_of_stream: " in

	try 
		match type_token_of_stream stream with
		  Kwd("datasize") ->
			let prog = datasize_of_stream prog stream in
				prog_of_stream prog stream
		| Kwd("data") ->
			let prog = data_of_stream prog stream in
				prog_of_stream prog stream
		| Kwd("function") ->
			let prog = function_of_stream prog stream in 
				prog_of_stream prog stream
	 	| Kwd("imports") ->
			let prog = imports_of_stream prog stream in
				prog_of_stream prog stream
		| Kwd("exports") ->
			let prog = exports_of_stream prog stream in
				prog_of_stream prog stream
 	 	| _  as header ->
			failwith (pos ^ "undefined section header (" ^ (string_of_token header) ^ ")")		 
	with
	  Stream.Failure -> prog
	| Stream.Error err -> failwith (pos ^ err)

(*
 * datasize_of_stream
 *
 * Purpose:
 *	The datasize section of the bytecode is constructed with this function
 *	and is added to the program structure.
 *)	
and datasize_of_stream prog stream =
	let size = value_of_stream stream in
	
	match size with
	  Genlex.Int(i) -> 
	  	{ prog with
			byte_data = spset_set_spill_count prog.byte_data i;
		}
	| _ -> failwith "datasize_of_stream: expected an integer"

and data_of_stream prog stream =
	let pos = "data_of_stream: " in
	let dt = type_token_of_stream stream in
	
	match dt with
	  Kwd("char")
	| Kwd("string") 
	| Kwd("int8")
	| Kwd("int16")
	| Kwd("int32")
	| Kwd("int64")
	| Kwd("uint8")
	| Kwd("uint16")
	| Kwd("uint32")
	| Kwd("uint64")
	| Kwd("float32")
	| Kwd("float64")
	| Kwd("float80") as ty ->
		let addy = addy_of_stream stream in
		let val' = value_of_stream stream in
		let v = as_var_of_stream stream in
		
		let dt = data_type_of_token val' ty in

		let prog = { prog with
			byte_global = SymbolTable.add prog.byte_global v dt;
			byte_data = spset_add prog.byte_data v addy dt;
		} in
			data_of_stream prog stream			
	| Kwd("ptr") ->
(* 		let prog = ptr_of_stream prog stream in *)
			data_of_stream prog stream
	| Kwd("tuple") ->
		let prog = tuple_of_stream prog stream in 
			data_of_stream prog stream
	| Kwd("array") ->
(* 		let prog = array_of_stream prog stream in *)
			data_of_stream prog stream
	| Kwd("end") ->
		prog
	| _ ->
		failwith (pos ^ "invalid data type")

and imports_of_stream prog stream =
	let pos = "imports_of_stream: " in
	
	match type_token_of_stream stream with
	  Kwd("fun")
	| Kwd("var") as ty ->
		let v = begin match Stream.next stream with
		  Ident(s) -> symbol_of_string s
		  | _ -> failwith (pos ^ "expected symbol")
		end in
		
		begin match ty with	  
		  Kwd("fun") -> 
			let prog = { prog with byte_import = SymbolTable.add prog.byte_import v Function } in
				imports_of_stream prog stream
		| Kwd("var") ->
			let prog = { prog with byte_import = SymbolTable.add prog.byte_import v Variable } in
				imports_of_stream prog stream
		| _ -> failwith (pos ^ "expected symbol type")
		end
	| Kwd("end") ->
		prog
	| _ -> failwith (pos ^ "syntax error")

and exports_of_stream prog stream =
	let pos = "imports_of_stream: " in
	
	match type_token_of_stream stream with
	  Kwd("fun")
	| Kwd("var") as ty ->
		let v = begin match Stream.next stream with
		  Ident(s) -> symbol_of_string s
		  | _ -> failwith (pos ^ "expected symbol")
		end in
		
		begin match ty with	  
		  Kwd("fun") -> 
			let prog = { prog with byte_export = SymbolTable.add prog.byte_export v Function } in
				exports_of_stream prog stream
		| Kwd("var") ->
			let prog = { prog with byte_export = SymbolTable.add prog.byte_export v Variable } in
				exports_of_stream prog stream
		| _ -> failwith (pos ^ "expected symbol type")
		end
	| Kwd("end") ->
		prog
	| _ -> failwith (pos ^ "syntax error")

and function_of_stream prog stream =
	let pos = "function_of_stream: " in
	
	(* read the name of the function *)
	let fun_label = 
		begin match Stream.next stream with
		  Ident(s) -> symbol_of_string s
		| _ -> failwith (pos ^ "expected function name")
		end
	in
	
	(* get the arguments for the function *)
	let args = arg_list_of_stream [] stream in
	
	(* get the instructions for the function *)
	let code = inst_of_stream [] stream in
	
	let block = {
		block_debug = None;
		block_label = fun_label;
		block_args = args;
		block_inst = code;
	} in
	
	(* add the function to the instruction trace *)
	let trace = prog.byte_funs in
	let fun_list = Trace.to_list trace in
	let fun_list = block :: fun_list in
		{ prog with	byte_funs = Trace.of_list fun_list }
	
and tuple_of_stream prog stream =
	let pos = "tuple_of_stream: " in
	
	let tuple_value_of_stream stream = 
		let pos = pos ^ "tuple_value_of_stream: " in
		match Stream.next stream with
		  Kwd(":") -> Stream.next stream
		| _ -> failwith (pos ^ "expected colon (:) operator")
	in
	
	let rec tuple_list_of_stream l stream =
		let pos = pos ^ "tuple_list_of_stream: " in
		
		match Stream.peek stream with
		  Some(Kwd "(") ->
		  	(* create a new tuple data type *)
		 	ignore(Stream.next stream);
		 	let dt = DTTuple(tuple_list_of_stream [] stream) in
				dt :: l
		| Some(Kwd ")") ->
			(* there are no more elements in the tuple *)
			ignore(Stream.next stream);
				List.rev l
		| Some(Kwd ",") ->
			(* there are more elements in the tuple *)
			ignore(Stream.next stream);
				tuple_list_of_stream l stream
		| _ ->
		 	(* add the element to the tuple *)
			let ty = type_token_of_stream stream in
			let v = tuple_value_of_stream stream in
			let dt = data_type_of_token v ty in
				tuple_list_of_stream (dt :: l) stream
	in

	let addy = addy_of_stream stream in
	
	(* check for the equal sign *)
	let _ = begin match Stream.next stream with 
	  Kwd("=") -> ()
	| _ -> failwith (pos ^ "expected equal (=) sign")
	end in

	(* check for the open parenthesis; it marks the beginning of the tuple *)
	let _ = begin match Stream.next stream with
	  Kwd("(") -> ()
	| _ -> failwith (pos ^ "expected open parenthesis")
	end in
		
	(* read the data elements of the tuple *)
	let dt = DTTuple(tuple_list_of_stream [] stream) in
	let v = as_var_of_stream stream in
	
		{ prog with
			byte_global = SymbolTable.add prog.byte_global v dt;
			byte_data = spset_add prog.byte_data v addy dt;
		}
		
and addy_of_stream stream =
	let pos = "addy_of_stream: " in

	let _ = begin match Stream.next stream with
 	  Kwd(":") -> ();
	| _ -> failwith (pos ^ "expected colon (:) operator")
	end in
	
	let _ = begin match Stream.next stream with
	  Kwd("$") -> ()
	| _ -> failwith (pos ^ "expected memory ($) operator")
	end in
		
	match Stream.next stream with
	  Genlex.Int(i) -> i
	| _ -> failwith (pos ^ "invalid address")

and as_var_of_stream stream =
	let pos = "as_var_of_stream: " in
	
	match Stream.next stream with 
	  Kwd("as") ->
		(* read the varaible name *)
		let next = Stream.next stream in
		begin match next with
		  Ident(s) ->
		  	symbol_of_string s
		| Kwd(".") ->
			begin match Stream.next stream with
			  Ident(s) -> 
			  	symbol_of_string ("." ^ s)
			| _ -> failwith (pos ^ "expected a variable name")
			end
		| _ -> failwith (pos ^ "expected a variable name")
		end
	| _ -> failwith (pos ^ "expected the keyword \"as\" operator")

and type_token_of_stream stream =
	match Stream.next stream with 
	  Kwd(".") -> Stream.next stream
	| _ -> failwith "type_token_of_stream: expected dot (.) operator\n"
		
and value_of_stream stream =
	match Stream.next stream with
	  Kwd("=") -> Stream.next stream
	| _ -> failwith "value_of_stream: expected equal (=) sign"

and data_type_of_token v tok =
	let pos = "data_type_of_token: " in
	
	match tok with
	  Kwd("char") -> 
	  	begin match v with
		  Genlex.Char(c) -> DTChar(c)
		| _ -> failwith (pos ^ "expected char value")
		end
	| Kwd("string") ->
		begin match v with
		  Genlex.String(s) -> DTString(s)
		| _ -> failwith (pos ^ "expected string value")
		end
	| Kwd("int8") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int8 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("int16") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int16 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("int32") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int32 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("int64") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int64 true s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint8") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int8 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint16") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int16 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint32") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int32 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("uint64") ->
		begin match v with
		  Genlex.String(s) -> DTInt(Rawint.of_string Rawint.Int64 false s)
		| _ -> failwith (pos ^ "expected integer value")
		end
	| Kwd("float32") ->
		begin match v with
		  Genlex.String(s) -> DTFloat(Rawfloat.of_string Rawfloat.Single s)
		| _ -> failwith (pos ^ "expected floating-point value")
		end
	| Kwd("float64") ->
		begin match v with
		  Genlex.String(s) -> DTFloat(Rawfloat.of_string Rawfloat.Double s)
		| _ -> failwith (pos ^ "expected floating-point value")
		end
	| Kwd("float80") ->
		begin match v with
		  Genlex.String(s) -> DTFloat(Rawfloat.of_string Rawfloat.LongDouble s)
		| _ -> failwith (pos ^ "expected floating-point value")
		end
	| _  as data -> failwith (pos ^ "undefined data type (" ^ (string_of_token data) ^ ")")	

and inst_of_stream l stream = 
	let pos = "inst_of_stream: " in
	
	match Stream.next stream with
	  Kwd "add"
	| Kwd "sub"
	| Kwd "div"
	| Kwd "mul"
	| Kwd "rem"
	| Kwd "min"
	| Kwd "max"
	| Kwd "f_add"
	| Kwd "f_sub"
	| Kwd "f_div"
	| Kwd "f_mul"
	| Kwd "f_rem"
	| Kwd "f_max"
	| Kwd "f_min"
	| Kwd "eq"
	| Kwd "neq"
	| Kwd "gt"
	| Kwd "lt"
	| Kwd "gte"
	| Kwd "lte"
	| Kwd "cmp"
	| Kwd "f_eq"
	| Kwd "f_neq"
	| Kwd "f_gt"
	| Kwd "f_lt"
	| Kwd "f_gte"
	| Kwd "f_lte"
	| Kwd "f_cmp"
	| Kwd "shl"
	| Kwd "ashr"
	| Kwd "lshr"
	| Kwd "and"
	| Kwd "or"
	| Kwd "xor" as inst ->
		let l = (binop_inst_of_stream inst stream) :: l in
			inst_of_stream l stream			
	| Kwd "not"
	| Kwd "uminus"
	| Kwd "abs"
	| Kwd "f_uminus"
	| Kwd "f_abs"
	| Kwd "sin"
	| Kwd "cos"
	| Kwd "sqrt"
	| Kwd "f_2_uint8"
	| Kwd "f_2_uint16"
	| Kwd "f_2_uint32"
	| Kwd "f_2_uint64"
	| Kwd "f_2_int8"
	| Kwd "f_2_int16"
	| Kwd "f_2_int32"
	| Kwd "f_2_int64"
	| Kwd "f_2_float32"
	| Kwd "f_2_floot64"
	| Kwd "f_2_float80"
	| Kwd "i_2_float32"
	| Kwd "i_2_float64"
	| Kwd "i_2_float80"
	| Kwd "i_2_int8"
	| Kwd "i_2_int16"
	| Kwd "i_2_int32"
	| Kwd "i_2_int64"
	| Kwd "i_2_uint8"
	| Kwd "i_2_uint16"
	| Kwd "i_2_uint32"
	| Kwd "i_2_uint64"
	| Kwd "set" as inst ->
		let l = (unop_inst_of_stream inst stream) :: l in
			inst_of_stream l stream
	| Kwd "goto"
	| Kwd "call"
	| Kwd "callex"
	| Kwd "native" as inst ->
		let l = (tailcall_of_stream inst stream) :: l in
			inst_of_stream l stream
	| Kwd "extern" ->
		let l = (extern_of_stream stream) :: l in
			inst_of_stream l stream
	| Kwd "." ->
		(* have we reached the end of the function *)
		begin match Stream.next stream with
		  Kwd "end" -> 
		  	List.rev l
		| _ -> 
			failwith (pos ^ "expected end keyword")
		end
	| _ as inst -> 
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")
	
and binop_inst_of_stream inst stream =
	let pos = "binop_inst_of_stream: " in
	
	let d = operand_of_stream stream in
	skip_comma stream;
	let s1 = operand_of_stream stream in
	skip_comma stream;
	let s2 = operand_of_stream stream in
	
	match inst with
	  Kwd "add" -> Add(d,s1,s2)	  
	| Kwd "sub" -> Sub(d,s1,s2)	  
	| Kwd "div" -> Div(d,s1,s2)	  
	| Kwd "mul" -> Mul(d,s1,s2)	  
	| Kwd "rem" -> Rem(d,s1,s2)	  
	| Kwd "min" -> Min(d,s1,s2)	  
	| Kwd "max" -> Max(d,s1,s2)	  
	| Kwd "f_add" -> FAdd(d,s1,s2)	  
	| Kwd "f_sub" -> FSub(d,s1,s2)	  
	| Kwd "f_div" -> FDiv(d,s1,s2)	  
	| Kwd "f_mul" -> FMul(d,s1,s2)	  
	| Kwd "f_rem" -> FRem(d,s1,s2)	  
	| Kwd "f_max" -> FMax(d,s1,s2)	  
	| Kwd "f_min" -> FMin(d,s1,s2)	  
	| Kwd "eq" -> Eq(d,s1,s2)	  
	| Kwd "neq" -> Neq(d,s1,s2)	  
	| Kwd "gt" -> Gt(d,s1,s2)	  
	| Kwd "lt" -> Lt(d,s1,s2)	  
	| Kwd "gte" -> Gte(d,s1,s2)	  
	| Kwd "lte" -> Lte(d,s1,s2)	  
	| Kwd "cmp" -> Cmp(d,s1,s2)	  
	| Kwd "f_eq" -> FEq(d,s1,s2)	  
	| Kwd "f_neq" -> FNeq(d,s1,s2)	  
	| Kwd "f_gt" -> FGt(d,s1,s2)	  
	| Kwd "f_lt" -> FLt(d,s1,s2)	  
	| Kwd "f_gte" -> FGte(d,s1,s2)	  
	| Kwd "f_lte" -> FLte(d,s1,s2)	  
	| Kwd "f_cmp" -> FCmp(d,s1,s2)	  
	| Kwd "shl" -> Shl(d,s1,s2)	  
	| Kwd "ashr" -> AShr(d,s1,s2)	  
	| Kwd "lshr" -> LShr(d,s1,s2)	  
	| Kwd "and" -> And(d,s1,s2)	  
	| Kwd "or" -> Or(d,s1,s2)	  
	| Kwd "xor" -> Xor(d,s1,s2)	  
	| _ as inst ->
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")
	
and unop_inst_of_stream inst stream =
	let pos = "binop_inst_of_stream: " in
	
	let d = operand_of_stream stream in
	skip_comma stream;
	let s = operand_of_stream stream in
	
	match inst with
	  Kwd "not" -> Not(d,s)
	| Kwd "uminus" -> UMinus(d,s)
	| Kwd "abs" -> Abs(d,s)
	| Kwd "f_uminus" -> FUMinus(d,s)
	| Kwd "f_abs" -> FAbs(d,s)
	| Kwd "sin" -> Sin(d,s)
	| Kwd "cos" -> Cos(d,s)
	| Kwd "sqrt" -> Sqrt(d,s)
	| Kwd "f_2_uint8" -> FToUInt8(d,s)
	| Kwd "f_2_uint16" -> FToUInt16(d,s)
	| Kwd "f_2_uint32" -> FToUInt32(d,s)
	| Kwd "f_2_uint64" -> FToUInt64(d,s)
	| Kwd "f_2_int8" -> FToInt8(d,s)
	| Kwd "f_2_int16" -> FToInt16(d,s)
	| Kwd "f_2_int32" -> FToInt32(d,s)
	| Kwd "f_2_int64" -> FToInt64(d,s)
	| Kwd "f_2_float32" -> FToFloat32(d,s)
	| Kwd "f_2_floot64" -> FToFloat64(d,s)
	| Kwd "f_2_float80" -> FToFloat80(d,s)
	| Kwd "i_2_float32" -> IToFloat32(d,s)
	| Kwd "i_2_float64" -> IToFloat64(d,s)
	| Kwd "i_2_float80" -> IToFloat80(d,s)
	| Kwd "i_2_int8" -> IToInt8(d,s)
	| Kwd "i_2_int16" -> IToInt16(d,s)
	| Kwd "i_2_int32" -> IToInt32(d,s)
	| Kwd "i_2_int64" -> IToInt64(d,s)
	| Kwd "i_2_uint8" -> IToUInt8(d,s)
	| Kwd "i_2_uint16" -> IToUInt16(d,s)
	| Kwd "i_2_uint32" -> IToUInt32(d,s)
	| Kwd "i_2_uint64" -> IToUInt64(d,s)
	| Kwd "set" -> Set(d,s)
	| _ as inst ->
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")
	
and tailcall_of_stream inst stream =
	let pos = "tailcall_of_stream: " in
	
	let v = operand_of_stream stream in
	let args = arg_list_of_stream [] stream in
	
	match inst with
	  Kwd "goto" ->	Goto(v,args) 	
	| Kwd "call" -> Call(v,args)	
	| Kwd "callex" -> CallEx(v,args)
	| Kwd "native" -> Native(v,args)
	| _ -> 
		failwith (pos ^ "undefined instruction (" ^ (string_of_token inst) ^ ")")

and extern_of_stream stream =
	let pos = "extern_of_stream: " in

	let v = operand_of_stream stream in
	let name = 
		begin match Stream.next stream with
		  Ident(s) -> s
		| _ -> failwith (pos ^ "expected function name")
		end
	in
	
	let args = arg_list_of_stream [] stream in
		Extern(v,name,args)
		
and arg_list_of_stream l stream =
	let pos = "arg_list_of_stream: " in
	
	match Stream.next stream with
	  Kwd "(" ->
	  	(* begin a new argument list *)
	  	let l = [(operand_of_stream stream)] in
			arg_list_of_stream l stream
	| Kwd "," ->
		(* read the next argument *)
	  	let l = (operand_of_stream stream) :: l in
			arg_list_of_stream l stream
	| Kwd ")" ->
		(* end of the argument list *)
		List.rev l
	| _ ->
		failwith (pos ^ "syntax error")
		
and operand_of_stream stream =
	let pos = "operand_of_stream: " in
	
	match Stream.next stream with
	  Ident(s) -> 
	  	Var(symbol_of_string s)
	| Kwd("$") ->
		begin match Stream.next stream with
		| Genlex.Int(i) ->  Memory(i)
		| _ -> failwith (pos ^ "expected memory address")
		end
	| _ as op -> failwith (pos ^ "illegal operand (" ^ (string_of_token op) ^ ")")				
		
and skip_comma stream =
	match Stream.peek stream with
	  Some(Kwd ",") -> ignore(Stream.next stream)
	| _ -> failwith "expected comma operator"

(*
 * string_of_token
 * 
 * Purpose:
 *	This function is used with printing errors messages to the
 *	screen. It returns a string representing the value of the 
 *	token it is given.
 *)	 
and string_of_token = function
	  Genlex.Kwd(s) -> s
	| Genlex.Ident(s) -> s
	| Genlex.Int(i) -> string_of_int i
	| Genlex.Float(f) -> string_of_float f
	| Genlex.String(s) -> s
	| Genlex.Char(c) -> String.make 1 c
	
(**********************************************************************
 *
 *)
let prog_of_file in_filename =
	let prog = { 
		byte_import = SymbolTable.empty;
		byte_export = SymbolTable.empty;
		byte_global = SymbolTable.empty;
		byte_funs = [];
		byte_data = spset_empty();
	} in
	
	(* open the output file and transform it into tokens *)
	let in_file = open_in in_filename in
	let token_stream = lexer (Stream.of_channel in_file) in
(* 	close_in in_file; *)
	
	(* parse the tokens to create a 'prog' *)
	let prog = prog_of_stream prog token_stream in

	flush_all();
	
	(* see what happened *)
	Byte_print.print_prog Format.std_formatter prog;

	(* end see what happend *)
		prog
	
