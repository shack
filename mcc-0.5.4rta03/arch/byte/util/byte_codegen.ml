open Symbol

open Byte_parser
open Byte_inst_type
open Byte_frame_type

(*
 * compile_mir_of_fir
 *
 * Purpose:
 *	used the x86 backend to transform the FIR to the MIR
 *)
let compile_mir_of_fir prog =
	let prog = X86_codegen.Codegen.compile_mir_of_fir prog in
		prog

(*
 * compile_asm_of_mir
 *
 * Purpose:
 *	transforms the MIR to its equivalent bytecode
 *)
let compile_asm_of_mir prog migrate = 
	Byte_mir.compile prog

(*
 * compile_asm_of_fir
 *
 * Purpose:
 *	transform the FIR to its equivalent bytecode
 *)	
let compile_asm_of_fir prog migrate =
	let prog = compile_mir_of_fir prog in
	let prog = compile_asm_of_mir prog migrate in
		prog

(*
 * link_object
 *
 * Purpose:
 *	link multiple object files to create on bytecode file
 *)
let link_object out_filename in_filenames =
	failwith "Byte_codegen.link_object: linkage not supported"

(*
 * Build this as a module.
 *)
module Codegen =
struct
	type prog = Byte_frame_type.prog

   	let print_prog = Byte_print.print_prog
   	let print_object = Byte_print.print_object
   	let link_object = link_object

   	let compile_asm_of_fir = compile_asm_of_fir
   	let compile_asm_of_mir = compile_asm_of_mir
   	let compile_mir_of_fir = compile_mir_of_fir
   
   	let catch f x = f x
end
