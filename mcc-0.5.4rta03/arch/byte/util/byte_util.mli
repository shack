open Frame_type

open Byte_frame_type
open Byte_inst_type

(*
 * These are some simple functions that help with transforming
 * the types in the MIR into its equivalent bytecode type
 *)
val atom_class_to_data_class : atom_class -> data_class
val atom_class_to_prec : atom_class -> prec
val atom_to_data_type : Mir.atom -> data_type
val data_type_list_of_atom_list : Mir.atom list -> data_type list

(*
 * This function is used to help with going converting from
 * the 'data_class' to 'data_type' in the bytecode
 *)
val data_class_of_data_type : data_type -> data_class
