open Symbol
open Attribute

open Frame_type

open Fir_set
open Interval_set

open Byte_frame_type
open Byte_inst_type
open Byte_util
open Byte_spset

(*
 * transform_immediate_value
 *
 * Purpose:
 *	converts a immediate value to a variable that contains its value
 *)
let transform_immediate_value globals data v =
	let v' = ref (new_symbol_string "const") in

	(* create a new constant symbol *)
	while spset_mem data !v' do
		v' := new_symbol !v';
	done;
	
	(* add the new variable to the spill set *)
	match v with
	  Float(f) -> 
		let globals = SymbolTable.add globals !v' (DTFloat(f)) in
		let data = spset_spill data !v' (data_class_of_data_type (DTFloat(f))) in
			globals,data,!v'
	| Int(i) ->
		let globals = SymbolTable.add globals !v' (DTInt(i)) in
		let data = spset_spill data !v' (data_class_of_data_type (DTInt(i))) in
			globals,data,!v'

(*
 * code_atom, code_atom_list
 *
 * Purpose:
 *	code an MIR atom(s) into equivalent bytecode operands; 
 *	preferable memory locations
 *)
let code_atom globals data = function
	  Mir.AtomInt(i) -> 
	  	let globals,data,v = transform_immediate_value globals data (Int(Rawint.of_int Rawint.Int32 true i)) in
			globals,data,Memory(spset_spill_location data v)
	| Mir.AtomRawInt(i) -> 
		let globals,data,v = transform_immediate_value globals data (Int(i)) in
			globals,data,Memory(spset_spill_location data v)
	| Mir.AtomFloat(f) -> 
		let globals,data,v = transform_immediate_value globals data (Float(f)) in
			globals,data,Memory(spset_spill_location data v)
	| Mir.AtomVar(ac,v)
	| Mir.AtomFunVar(ac,v) ->
		let data = spset_spill data v (atom_class_to_data_class ac) in
		let index = spset_spill_location data v in
		
		(* if the variable location is -1 then it is an import and has to be resolved at link time *)
		if index = -1 then 
			globals,data,Var(v) 
		else
			globals,data,Memory(spset_spill_location data v)
	| _ -> failwith "code_atom: cannot code atom"

let rec code_atom_list globals data = function
	  a :: l ->
		let globals,data,a_list = code_atom_list globals data l in
	  	let globals,data,a = code_atom globals data a in
			globals,data,(a :: a_list)
	| [] -> globals,data,[]
	
(*
 * code_atom_unop
 *
 * Purpose:
 *	code a MIR unary operation into its equivalent bytecode instruction
 *)
let code_atom_unop globals data v ac a1 op = 
	let pos = "code_atom_unop: " in
	
	(* code the source atom *)
	let globals,data,a1 = code_atom globals data a1 in
	let data = spset_spill data v (atom_class_to_data_class ac) in
	
	(* code the destination variable *)
	let index = spset_spill_location data v in
	let v = if index = -1 then Var(v) else Memory(index) in
	
	(* code the operation *)
	let op = 
		begin match op with
		  Mir.NotOp(ac) ->
		  	if (atom_class_to_prec ac) = FP then
				failwith (pos ^ "integer operation only")
			else Not(v,a1)
		| Mir.AbsOp(ac) ->
			if (atom_class_to_prec ac) = IP then Abs(v,a1) else FAbs(v,a1)
		| Mir.UMinusOp(ac) ->
			if (atom_class_to_prec ac) = IP then UMinus(v,a1) else FUMinus(v,a1)
		| Mir.SinOp(_) -> Sin(v,a1)
		| Mir.CosOp(_) -> Cos(v,a1)
		| Mir.SqrtOp(_) -> Sqrt(v,a1)
		| Mir.IntOfFloatOp(_) -> FToInt32(v, a1)
	  	| Mir.FloatOfIntOp(_) -> IToFloat32(v, a1)
		| Mir.RawIntOfRawIntOp(ip,is,_,_)
		| Mir.RawIntOfIntOp(ip,is) ->
			( match (ip,is) with
			  Rawint.Int8,true -> IToInt8(v, a1)
			| Rawint.Int8,false -> IToUInt8(v, a1)
			| Rawint.Int16,true -> IToInt16(v, a1)
			| Rawint.Int16,false -> IToUInt16(v, a1)
			| Rawint.Int32,true -> IToInt32(v, a1)
			| Rawint.Int32,false -> IToUInt32(v, a1)
			| Rawint.Int64,true -> IToInt64(v, a1)
			| Rawint.Int64,false -> IToUInt64(v, a1))
		| Mir.FloatOfFloatOp(fp,_) -> 
			( match fp with
			  Rawfloat.Single -> FToFloat32(v, a1)
			| Rawfloat.Double -> FToFloat64(v, a1)
			| Rawfloat.LongDouble -> FToFloat80(v, a1))
		| Mir.RawIntOfFloatOp(ip,is,_) ->
			( match (ip,is) with
			  Rawint.Int8,true -> FToInt8(v, a1)
			| Rawint.Int8,false -> FToUInt8(v, a1)
			| Rawint.Int16,true -> FToInt16(v, a1)
			| Rawint.Int16,false -> FToUInt16(v, a1)
			| Rawint.Int32,true -> FToInt32(v, a1)
			| Rawint.Int32,false -> FToUInt32(v, a1)
			| Rawint.Int64,true -> FToInt64(v, a1)
			| Rawint.Int64,false -> FToUInt64(v, a1))
		| Mir.FloatOfRawIntOp(fp,_,_) ->
			( match fp with
			  Rawfloat.Single -> IToFloat32(v, a1)
			| Rawfloat.Double -> IToFloat64(v, a1)
			| Rawfloat.LongDouble -> IToFloat80(v, a1))
		| Mir.IntOfRawIntOp(_,_) -> IToInt32(v, a1)
		| _ -> failwith (pos ^ "cannot code operation")
		end 
	in
		globals,data,op

(*
 * code_atom_binop
 *
 * Purpose:
 *	Transform a Mir.binop into its equivalent bytecode instruction.
 *)
let code_atom_binop globals data v ac a1 a2 op = 
	let pos = "code_atom_binop: " in
	
 	(* code the two source atoms *)
	let globals,data,a1 = code_atom globals data a1 in
	let globals,data,a2 = code_atom globals data a2 in
	let data = spset_spill data v (atom_class_to_data_class ac) in

	(* code the destination variable *)
	let index = spset_spill_location data v in
	let v = if index = -1 then Var(v) else Memory(index) in
	
	(* code the operation *)
	let op = 
		match op with
		  Mir.PlusOp(ac) -> 
		  	if (atom_class_to_prec ac) = IP then Add(v,a1,a2) else FAdd(v,a1,a2)
		| Mir.MinusOp(ac) -> 
			if (atom_class_to_prec ac) = IP then Sub(v,a1,a2) else FSub(v,a1,a2)
		| Mir.MulOp(ac) -> 
			if (atom_class_to_prec ac) = IP then Mul(v,a1,a2) else FMul(v,a1,a2)
		| Mir.DivOp(ac) -> 
			if (atom_class_to_prec ac) = IP then Div(v,a1,a2) else FDiv(v,a1,a2)
		| Mir.RemOp(ac) ->
			if (atom_class_to_prec ac) = IP then Rem(v,a1,a2) else FRem(v,a1,a2)
		| Mir.MaxOp(ac) ->
			if (atom_class_to_prec ac) = IP then Max(v,a1,a2) else FMax(v,a1,a2)
		| Mir.MinOp(ac) ->
			if (atom_class_to_prec ac) = IP then Min(v,a1,a2) else FMin(v,a1,a2)
		| Mir.EqOp(ac) ->
			if (atom_class_to_prec ac) = IP then Eq(v,a1,a2) else FEq(v,a1,a2)
		| Mir.NeqOp(ac) ->
			if (atom_class_to_prec ac) = IP then Neq(v,a1,a2) else FNeq(v,a1,a2)
		| Mir.LtOp(ac) ->
			if (atom_class_to_prec ac) = IP then Lt(v,a1,a2) else FLt(v,a1,a2)
		| Mir.LeOp(ac) ->
			if (atom_class_to_prec ac) = IP then Lte(v,a1,a2) else FLte(v,a1,a2)
		| Mir.GtOp(ac) ->
			if (atom_class_to_prec ac) = IP then Gt(v,a1,a2) else FGt(v,a1,a2)
		| Mir.GeOp(ac) ->
			if (atom_class_to_prec ac) = IP then Gte(v,a1,a2) else FGte(v,a1,a2)
		| Mir.CmpOp(ac) ->
			if (atom_class_to_prec ac) = IP then Cmp(v,a1,a2) else FCmp(v,a1,a2)
		| Mir.SlOp(ac)
		| Mir.ASrOp(ac)
		| Mir.LSrOp(ac)
		| Mir.AndOp(ac)
		| Mir.OrOp(ac)
		| Mir.XorOp(ac) as op ->
			(* these are integer-only operations *)
			if (atom_class_to_prec ac) = FP then
				failwith (pos ^ "integer only operation")
			else begin match op with
				  Mir.SlOp(_) -> Shl(v,a1,a2) 	
				| Mir.ASrOp(_) -> AShr(v,a1,a2)
				| Mir.LSrOp(_) -> LShr(v,a1,a2)					
				| Mir.AndOp(_) -> And(v,a1,a2)
				| Mir.OrOp(_) -> Or(v,a1,a2)
				| Mir.XorOp(_) -> Xor(v,a1,a2)
				| _ -> failwith (pos ^ "oops... not supposed to be here")
				end	
		| _ -> failwith (pos ^ "cannot code atom")
	in
		globals,data,op

(*
 * code_expr
 *
 * Purpose:
 *	code a Mir.exp into its equivalent bytecode. As you can see, the full
 *	MIR is not supported.
 *)
let rec code_expr globals data = function
	  Mir.LetAtom(v,ac,a,e) -> 
	  	let globals,data,code = code_expr_letatom globals data v ac a in 
		let globals,data,code' = code_expr globals data (dest_simple_core e) in
			globals,data,(Listbuf.add_listbuf (Listbuf.of_elt code) code')
	
	(* function call expressions *)		
	| Mir.TailCall(op,v,a_list) -> 
			code_expr_tailcall globals data op v a_list
	| Mir.LetExternal(v,ac,s,a_list,e) -> 
		let globals,data,code = code_expr_letexternal globals data v ac s a_list in
		let globals,data,code' = code_expr globals data (dest_simple_core e) in
 			globals,data,(Listbuf.add_listbuf code code')
	
	(* flow control expressions *)
	| Mir.IfThenElse(op,a1,a2,e1,e2) -> 
		globals,data,Listbuf.empty
	| Mir.Match(a,ac,e_list) ->
		code_expr_match globals data a ac e_list

	(* allocation and memory expressions *)
	| Mir.LetAlloc(v,op,e) ->
		let globals,data = code_expr_letalloc globals data v op in
			code_expr globals data (dest_simple_core e)
	| Mir.SetGlobal(ac,v,a,e) -> 
		let globals,data,code = code_expr_setglobal globals data v ac a in
		let globals,data,code' = code_expr globals data (dest_simple_core e) in
			globals,data,(Listbuf.add_listbuf code code')
	
	(* safety checks *)
	| Mir.BoundsCheck(_,_,_,_,_,e)
	| Mir.LowerBoundsCheck(_,_,_,_,e)
	| Mir.UpperBoundsCheck(_,_,_,_,e)
	
	(* debug information *)
	| Mir.Debug(_,e)
	| Mir.PrintDebug(_,_,_,e)
	| Mir.CommentFIR(_, e) -> code_expr globals data (dest_simple_core e)
		
	| _ -> 
		failwith "Byte_mir.code_expr: MIR expression not supported"

(*
 * code_expr_letatom
 *
 * Purpose:
 *	code an MIR LetAtom expression
 *)
and code_expr_letatom globals data v ac = function
	  Mir.AtomUnop(op,a1) -> code_atom_unop globals data v ac a1 op
	| Mir.AtomBinop(op,a1,a2) -> code_atom_binop globals data v ac a1 a2 op
	| _ -> raise (Failure "code_expr_letatom: cannot code atom\n")
	
(*
 * code_expr_tailcall
 *
 * Purpose:
 *	code an MIR TailCall expression
 *)
and code_expr_tailcall globals data op v a_list =
	(* collect all the arguments for the tail call *)
	let globals,data,a_list = List.fold_right (fun a (globals,data,args) -> 
		let globals,data,a = code_atom globals data a in
			globals, data, a :: args) a_list (globals,data,[]) in

	let op = 
		match op with
		  Mir.DirectCall -> 
		  	Call(Label(v),a_list)
		| Mir.ExternalCall -> 
			CallEx(Label(v),a_list)
		| Mir.IndirectCall -> 
			let index = spset_spill_location data v in
			let v = if index = -1 then Var(v) else Memory(index) in
				Goto(v,a_list)
	in
		globals,data,(Listbuf.of_elt op)

(*
 * code_expr_letexternal
 *
 * Purpose:
 *	Transform a 'Mir.LetExternal' expression into its equivalent bytecode.
 *)
and code_expr_letexternal globals data v ac s a_list =
	let globals,data,a_list = code_atom_list globals data a_list in
	let data = spset_spill data v (atom_class_to_data_class ac) in
	let index = spset_spill_location data v in
	let v = if index = -1 then Var(v) else Memory(index) in
		globals,data,(Listbuf.of_elt (Extern (v,s,a_list)))
 
(*
 * code_expr_match
 *
 * Purpose:
 *	Transform a 'Mir.Match' expression into its equivalent bytecode.
 *)
and code_expr_match globals data a ac m_list = 
	(* code the match atom *)
	let globals,data,a = code_atom globals data a in

	(* code the individual cases *)
	let globals,data,cases = List.fold_right (fun (s,e) (globals,data,cases) ->
		(* convert the int set if neccessary *)
		let s = 
			match s with
			  Mir.IntSet(i_set) -> 
			  	IntSet.fold (fun raw_set lo_bnd hi_bnd ->
					let convert_bound = function
						  Infinity -> Infinity
						| Open(i) -> Open(Rawint.of_int Rawint.Int32 true i)
						| Closed(i) -> Closed(Rawint.of_int Rawint.Int32 true i)
					in 
					
					let new_set = RawIntSet.of_interval Rawint.Int32 true 
						(convert_bound lo_bnd) (convert_bound hi_bnd) 
					in 
						RawIntSet.union raw_set new_set
				) (RawIntSet.empty Rawint.Int32 true) i_set
			| Mir.RawIntSet(i_set) -> i_set
		in		
		
		(* code the expressions for the case *)
		let globals,data,code' = code_expr globals data (dest_simple_core e) in
		let code' = Listbuf.to_list code' in
		
			globals,data,(s,code') :: cases
	) m_list (globals,data,[]) in
	
	(* close the match statement *)
	let code = Listbuf.of_elt (Match(a,cases)) in
		globals,data,code

(*
 * code_expr_letalloc
 *
 * Purpose:
 *	Transform a 'Mir.LetAlloc' expression into its equivalent bytecode.
 *)
and code_expr_letalloc globals data v op =
	let pos = "code_expr_letalloc" in
	
	match op with
	  Mir.AllocMalloc a ->
		let dt = atom_to_data_type a in
		let globals = SymbolTable.add globals v dt in
		let data = spset_spill data v (data_class_of_data_type dt) in
			globals,data
	| _ -> failwith (pos ^ "allocation operation not supported")
				
(*
 * code_expr_setglobal
 *
 * Purpose:
 *	Transform a 'Mir.SetGlobal' expression into its equivalent bytecode.
 *)
and code_expr_setglobal globals data v ac a = 
	let globals,data,a = code_atom globals data a in
	let index = spset_spill_location data v in
	let v = if index = -1 then Var(v) else Memory(index) in
		globals,data,(Listbuf.of_elt (Set(v,a)))

(*
 * compile_func, code_args, code_expr
 *
 * Purpose: 
 *	code a function from MIR to bytecode
 *
 * Return:
 *	returns an instruction block (see byte_frame.ml)
 *)	
let compile_func f (dbg, args, insts) data globals =
	let code_args args data = 
		let args = List.fold_left (fun args (v,ac) -> 
			(spset_spill_location v, atom_class_to_data_class ac, v) :: args) [] args in
		List.rev args
	in
	
	(* collect the argument variables *)
	let data = List.fold_left (fun data (v,ac) -> 
		spset_spill data v (atom_class_to_data_class ac)) data args in

	(* code the argument variables *)
	let args = List.fold_right (fun (v,_) args ->
		Memory(spset_spill_location data v) :: args) args [] in

	(* code the expression tree into bytecode *)
	let globals,data,code = code_expr globals data (dest_simple_core insts) in
	let code = Listbuf.to_list code in	
	
	let block = {
		block_debug = Some(dbg);
		block_label = f;
		block_args = args;
		block_inst = code;
	} in
		data,globals,block

(*
 * translate_global_table
 *
 * Purpose:
 *	This converts the given global table into one that is readable by
 *	the bytecode. For more information see 'frame_type.ml' and 
 *	'byte_frame_type.ml'.
 *) 
let translate_global_table globals =
	SymbolTable.fold (fun table v glob -> 		
		match glob with
		  Mir.InitAtom(a) -> 
		  	SymbolTable.add table v (atom_to_data_type a)
		| Mir.InitAlloc(op) -> 
			begin match op with
			  Mir.AllocTuple(a_list)
			| Mir.AllocUnion(_,a_list) ->
			  	SymbolTable.add table v (DTTuple(data_type_list_of_atom_list a_list))
			| Mir.AllocArray(a_list) ->
				SymbolTable.add table v (DTArray(Array.of_list (data_type_list_of_atom_list a_list)))
			| Mir.AllocMArray(_,_)
			| Mir.AllocMalloc(_)
			| _ -> failwith "translate_global_table: this 'InitAlloc' operation not supported..."
			end
		| Mir.InitRawData(p,ar) ->
			begin match p with
			  Rawint.Int8 ->
				let str = String.create (Array.length ar) in
				let str,_ = Array.fold_left (fun (str,i) elt -> 
					String.set str i (Char.chr elt);
						(str, succ i)
				) (str,0) ar in
				SymbolTable.add table v (DTString(str))
			| _ -> failwith "translate_global_table: 16-bit strings not supported"
			end
	) SymbolTable.empty globals
 
(*
 * compile
 *
 * Purpose:
 *	This is where the compilation begins. This function is given a Mir.prog
 *	and transfroms it into a Byte_frame_type.prog. For more information 
 *	see 'mir.ml' and 'byte_frame_type.ml'.
 *)
let compile prog =
	let {
		Mir.prog_file          = file_info;
	    Mir.prog_import        = imports;
    	Mir.prog_export        = exports;
     	Mir.prog_globals       = globals;
     	Mir.prog_funs          = prog_funs;
     	Mir.prog_mprog         = mprog;
     	Mir.prog_global_order  = global_order;
     	Mir.prog_fun_order     = fun_order;
	} = prog in
	
	(* transform the import and export tables *)
	let imports = SymbolTable.fold (fun imports v import ->
		let import = begin match import.import_info with
		  ImportGlobal -> Variable
		| ImportFun(_,_,_) -> Function
		end in
			SymbolTable.add imports v import) SymbolTable.empty imports in
			
	let exports = SymbolTable.fold (fun exports v export ->
		let export =
			if export.export_is_fun then Function else Variable
		in
			SymbolTable.add exports v export) SymbolTable.empty exports in
			
	(* 
	 * create a listing of the functions in the file by adding the functions
	 * in 'prog_funs' to 'fun_order', but preserving the already detemined
	 * order in 'f_order'.
	 *)
	let fun_order = SymbolTable.fold (fun fun_order key' _ ->
		if List.mem key' fun_order then
			fun_order
		else
			key' :: fun_order) fun_order prog_funs in

	(*
	 * translate the global table its equivalent 'data_type' and add the
	 * symbols to the data table; add the imports to the data table with
	 * special attributes
	 *)
	let globals = translate_global_table globals in
	let data = spset_initialize globals imports in

	(* 
	 * code each of the functions into instruction blocks in the order 
	 * specified by fun_order
	 *)
	let data,globals,fun_blocks = List.fold_left (fun (data,globals,fun_blocks) v ->
		try 
			let def = SymbolTable.find prog_funs v in
			let data,globals,block = compile_func v def data globals in
				(data,globals,block :: fun_blocks)
		with Not_found -> raise (Failure("compile: cannot find function definition for " ^ (string_of_symbol v) ^ "\n"))
	) (data,globals,[]) fun_order in
	
	let fun_blocks = Trace.of_list fun_blocks in	
		
	(* save the program *)	
	let prog = {
		byte_import		= imports;
		byte_export		= exports;
		byte_global		= globals;
		byte_funs		= fun_blocks;
		byte_data		= data;
	} in
		prog
		
