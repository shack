open Symbol

open Frame_type

open Byte_frame_type
open Byte_inst_type

(*
 * atom_class_to_data_class, atom_class_to_prec
 *
 * Purpose:
 *	convert the given 'atom_class' to its equivalent 'data_class' or 'prec'
 *)
let atom_class_to_data_class = function
	  ACInt
	| ACPoly -> DCInt(Rawint.Int32,true)
	| ACRawInt(ip,is) -> DCInt(ip,is)
	| ACFloat(fp) -> DCFloat(fp)
	| ACPointer(_) -> DCPointer
	| ACPointerInfix(_) -> DCPointer
	| ACFunction(_) -> DCString

(* 
 * atom_class_to_prec
 *
 * Purpose:
 *	return the bytecode precision type of a 'Mir.atom'
 *)
and atom_class_to_prec = function
	  ACFloat(_) -> FP
	| _ -> IP

(*
 * atom_to_data_type
 *
 * Purpose:
 *	return the bytecode 'data_type' of a 'Mir.atom'
 *)
and atom_to_data_type = function
	  Mir.AtomInt(i) -> DTInt(Rawint.of_int Rawint.Int32 true i)
	| Mir.AtomRawInt(i) -> DTInt(i)
	| Mir.AtomFloat(f) -> DTFloat(f)
	| Mir.AtomFunVar(ac,v) -> DTString(string_of_symbol v)
	| _ -> failwith "Byte_util.atom_to_data_class: cannot convert atom"

(*
 * data_type_list_of_atom_list
 *
 * Purpose:
 *	converts a 'Mir.atom' list to a 'data_type' list
 *)
let data_type_list_of_atom_list a_list = 
	List.fold_right (fun a dt_list -> (atom_to_data_type a) :: dt_list) a_list []

(*
 * data_class_of_data_type
 *
 * Purpose:
 *	given the bytecode 'data_type', it returns its equivalent bytecode 'data_class'
 *)
let rec data_class_of_data_type = function
	  DTChar(c) -> DCChar
	| DTInt(i) -> DCInt(Rawint.precision i, Rawint.signed i)
	| DTFloat(f) -> DCFloat(Rawfloat.precision f)
	| DTPointer(_) -> DCPointer
	| DTString(s) -> DCString
	| DTTuple(dt_list) -> 
		DCTuple(List.fold_right (fun dt dc_list -> (data_class_of_data_type dt) :: dc_list) dt_list [])
	| DTArray(ar) -> DCArray(data_class_of_data_type(Array.get ar 0))
	| DTNone -> DCNone
