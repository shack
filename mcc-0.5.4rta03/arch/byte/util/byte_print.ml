open Symbol
open Trace

open Frame_type
open Fir_set
open Interval_set

open Byte_frame_type
open Byte_inst_type
open Byte_spset
open Byte_util

(*
 * string_of_operand
 *
 * Purpose:
 *	returns the operand in string format
 *)
let string_of_operand = function
	  Var(v)
	| Label(v) -> string_of_symbol v
	| Memory(i) -> "$" ^ (string_of_int i)

(*
 * string_of_data_class, string_of_data_type, string_of_int_class, 
 * string_of_float_class
 *
 * Purpose:
 * returns the data class, data type,int class, or float class in string format
 *)
let rec string_of_data_class = function
	  DCChar -> ".char"
	| DCInt(p,s) -> string_of_int_class(p,s)
	| DCFloat(p) -> string_of_float_class p
	| DCPointer -> ".ptr"
	| DCString -> ".string"
	| DCTuple(dc_list) -> ".tuple"
	| DCArray(dc) -> ".array " ^ (string_of_data_class dc)
	| DCNone -> ".notype"

and string_of_float_class = function
	  Rawfloat.Single -> ".float32"
	| Rawfloat.Double -> ".float64"
	| Rawfloat.LongDouble -> ".float80"
	
and string_of_int_class = function
	  Rawint.Int8,true -> ".int8"
	| Rawint.Int16,true -> ".int16"
	| Rawint.Int32,true -> ".int32"
	| Rawint.Int64,true -> ".int64"
	| Rawint.Int8,false -> ".uint8"
	| Rawint.Int16,false -> ".uint16"
	| Rawint.Int32,false -> ".uint32"
	| Rawint.Int64,false ->	 ".uint64"
	
and string_of_data_type = function
	  DTChar(c) -> "\'" ^ (String.make 1 c) ^ "\'"
	| DTInt(i) -> "\"" ^ (Rawint.to_string i) ^ "\""
	| DTFloat(f) -> "\"" ^ (Rawfloat.to_string f) ^ "\""
	| DTString(s) -> "\"" ^ s ^ "\""
	| DTTuple(dt_list) -> string_of_tuple_list dt_list			
	| _ -> ""

(*
 * string_of_tuple_list
 *
 * Purpose:
 *	converts a tuple list into its string version for printing
 *)
and string_of_tuple_list dt_list =
	let str = List.fold_left (fun str dt ->
		match dt with
		  DTTuple(l) ->
		  	str ^ (string_of_tuple_list l)
		| _ as dt ->
			let str = str ^ (string_of_data_class(data_class_of_data_type dt)) in
			let str = str ^ ":" ^ (string_of_data_type dt) ^ "," in
				str
	) "(" dt_list in
	
	let length = String.length str in
	
	if length > 1 then
		(String.sub str 0 length) ^ ")"
	else
		str ^ ")"
	
(*
 * string_of_arg_list
 *
 * Purpose:
 *	convert the arguments for a function call into a readable string
 *)
let string_of_arg_list a_list =
	let a_list = List.fold_left (fun str arg -> str ^ "," ^ (string_of_operand arg)) "" a_list in
		String.sub a_list 1 ((String.length a_list) - 1)

(*
 * string_of_fun_args
 *
 * Purpose:
 *	convert the arguments for a function definition into a readable string
 *)
and string_of_fun_args a_list =
	let a_list = List.fold_left (fun str arg -> str ^ (string_of_operand arg) ^ ",") "" a_list in
		String.sub a_list 0 ((String.length a_list) - 1)

(*
 * string_of_code
 *
 * Purpose:
 *	converts a list of bytecode instrunction into a string for printing
 *) 
let rec string_of_code code =
	List.fold_left (fun str inst' -> str ^ "\t" ^ (string_of_inst inst') ^ "\n") "" code

(*
 * string_of_inst
 *
 * Purpose:
 *	converts an instrunction into a string for printing
 *)
and string_of_inst = function
	  Add(d,s1,s2)
	| Sub(d,s1,s2)
	| Div(d,s1,s2)
	| Mul(d,s1,s2)
	| Rem(d,s1,s2)
	| Min(d,s1,s2)
	| Max(d,s1,s2)
	| FAdd(d,s1,s2)
	| FSub(d,s1,s2)
	| FDiv(d,s1,s2)
	| FMul(d,s1,s2)
	| FRem(d,s1,s2)
	| FMax(d,s1,s2)
	| FMin(d,s1,s2)
	| Eq(d,s1,s2)
	| Neq(d,s1,s2)
	| Gt(d,s1,s2)
	| Lt(d,s1,s2)
	| Gte(d,s1,s2)
	| Lte(d,s1,s2)
	| Cmp(d,s1,s2)	
	| FEq(d,s1,s2)
	| FNeq(d,s1,s2)
	| FGt(d,s1,s2)
	| FLt(d,s1,s2)
	| FGte(d,s1,s2)
	| FLte(d,s1,s2)
	| FCmp(d,s1,s2)
	| Shl(d,s1,s2)
	| AShr(d,s1,s2)
	| LShr(d,s1,s2)
	| And(d,s1,s2)
	| Or(d,s1,s2)
	| Xor(d,s1,s2) as op ->
		(string_of_opcode op) ^ "\t" ^ (string_of_operand d) ^ "," ^ (string_of_operand s1) ^ "," ^ (string_of_operand s2)
	| FToUInt8(d,s)
	| FToUInt16(d,s)
	| FToUInt32(d,s)
	| FToUInt64(d,s)
	| FToInt8(d,s)
	| FToInt16(d,s)
	| FToInt32(d,s)
	| FToInt64(d,s)
	| FToFloat32(d,s)
	| FToFloat64(d,s)
	| FToFloat80(d,s)
	| IToFloat32(d,s)
	| IToFloat64(d,s)
	| IToFloat80(d,s)
	| IToInt8(d,s)
	| IToInt16(d,s)
	| IToInt32(d,s)
	| IToInt64(d,s)
	| IToUInt8(d,s)
	| IToUInt16(d,s)
	| IToUInt32(d,s)
	| IToUInt64(d,s)
	| Not(d,s)
	| UMinus(d,s)
	| Abs(d,s)
	| FUMinus(d,s)
	| FAbs(d,s)
	| Sin(d,s)
	| Cos(d,s)
	| Set(d,s) as op -> 
		(string_of_opcode op) ^ "\t" ^ (string_of_operand d) ^ "," ^ (string_of_operand s)
	| Goto(v,a_list) 
	| Call(v,a_list)
	| CallEx(v,a_list) as op ->
		(string_of_opcode op) ^ "\t" ^ (string_of_operand v) ^ "(" ^ (string_of_arg_list a_list) ^ ")"
	| Extern(v,s,a_list) as op -> 
		(string_of_opcode op) ^ "\t" ^ (string_of_operand v) ^ "," ^ s ^ "(" ^ (string_of_arg_list a_list) ^ ")"
	| Match(v,cases) as op ->
		(string_of_opcode op) ^ "(" ^ (string_of_operand v) ^ ")\n" ^ (string_of_cases cases) ^ "\tend"
	| _ -> " "  

(*
 * string_of_opcode
 *
 * Purpose:
 *	converts the opcode into a string. The opcode is the instruction without its operands.
 *)
and string_of_opcode = function
	  Add(_,_,_) -> "add"
	| Sub(_,_,_) -> "sub"
	| Div(_,_,_) -> "div"
	| Mul(_,_,_) -> "mul"
	| Rem(_,_,_) -> "rem"
	| Min(_,_,_) -> "min"
	| Max(_,_,_) -> "max"
	| FAdd(_,_,_) -> "f_add"
	| FSub(_,_,_) -> "f_sub"
	| FDiv(_,_,_) -> "f_div"
	| FMul(_,_,_) -> "f_mul"
	| FRem(_,_,_) -> "f_rem"
	| FMax(_,_,_) -> "f_max"
	| FMin(_,_,_) -> "f_min"
	| Eq(_,_,_) -> "eq"
	| Neq(_,_,_) -> "neq"
	| Gt(_,_,_) -> "gt"
	| Lt(_,_,_) -> "lt"
	| Gte(_,_,_) -> "gte"
	| Lte(_,_,_) -> "lte"
	| Cmp(_,_,_) -> "cmp"
	| FEq(_,_,_) -> "f_eq"
	| FNeq(_,_,_) -> "f_neq"
	| FGt(_,_,_) -> "f_gt"
	| FLt(_,_,_) -> "f_lt"
	| FGte(_,_,_) -> "f_gte"
	| FLte(_,_,_) -> "f_lte"
	| FCmp(_,_,_) -> "f_cmp"
	| Shl(_,_,_) -> "shl"
	| AShr(_,_,_) -> "ashr"
	| LShr(_,_,_) -> "lshr"
	| And(_,_,_) -> "and"
	| Or(_,_,_) -> "or"
	| Xor(_,_,_) -> "xor"
	| Not(_,_) -> "not"
	| UMinus(_,_) -> "uminus"
	| Abs(_,_) -> "abs"
	| FUMinus(_,_) -> "f_uminus"
	| FAbs(_,_) -> "f_abs"
	| Sin(_,_) -> "sin"
	| Cos(_,_) -> "cos"
	| Sqrt(_,_) -> "sqrt"
	| FToUInt8(_,_) -> "f_2_uint8"
	| FToUInt16(_,_) -> "f_2_uint16"
	| FToUInt32(_,_) -> "f_2_uint32"
	| FToUInt64(_,_) -> "f_2_uint64"
	| FToInt8(_,_) -> "f_2_int8"
	| FToInt16(_,_) -> "f_2_int16"
	| FToInt32(_,_) -> "f_2_int32"
	| FToInt64(_,_) -> "f_2_int64"	
	| FToFloat32(_,_) -> "f_2_float32"
	| FToFloat64(_,_) -> "f_2_floot64"
	| FToFloat80(_,_) -> "f_2_float80"	
	| IToFloat32(_,_) -> "i_2_float32"
	| IToFloat64(_,_) -> "i_2_float64"
	| IToFloat80(_,_) -> "i_2_float80"
	| IToInt8(_,_) -> "i_2_int8"
	| IToInt16(_,_) -> "i_2_int16"
	| IToInt32(_,_) -> "i_2_int32"
	| IToInt64(_,_) -> "i_2_int64"
	| IToUInt8(_,_) -> "i_2_uint8"
	| IToUInt16(_,_) -> "i_2_uint16"
	| IToUInt32(_,_) -> "i_2_uint32"
	| IToUInt64(_,_) -> "i_2_uint64"
	| Set(_,_) -> "set"
	| Goto(_,_) -> "goto"
	| Call(_,_) -> "call"
	| CallEx(_,_) -> "callex"
	| Extern(_,_,_) -> "extern"
	| Match(_) -> "match"
	| _ -> " "

(*
 * string_of_cases
 *
 * Purpose:
 *	returns a string version of a case for printing
 *)
and string_of_cases cases =
	List.fold_left (fun str (s,code) ->
		let str = str ^ "\tcase(" ^ (string_of_rawset s) ^ ")\n" in
		let str = str ^ (string_of_code code) in
			str
	) "" cases

(*
 * string_of_rawset
 *
 * Purpose:
 *	returns a string version of the 'RawIntSet' for printing
 *)	
and string_of_rawset raw_set = 
	let str = string_of_int_class((RawIntSet.precision raw_set),(RawIntSet.signed raw_set)) in
	let str = str ^ " {" in
	let str = RawIntSet.fold (fun str lo_bnd hi_bnd -> 
		str ^ (string_of_rawset_bnd lo_bnd hi_bnd) ^ ",") str raw_set in
	
	let str = 
		if RawIntSet.is_empty raw_set then
			str
		else
			String.sub str 0 ((String.length str) - 1)
	in	
		str ^ "}"

(*
 * string_of_rawset_bnd
 *
 * Purpose:
 *	returns a string version of the bounds in the 'RawIntSet'
 *)
and string_of_rawset_bnd lo_bnd hi_bnd =
	(* get the lower bound as a string *)
	let str = 
		match lo_bnd with
		  Infinity -> "(."
		| Open(i) -> "(" ^ (Rawint.to_string i) ^ ""
		| Closed(i) -> "[" ^ (Rawint.to_string i) ^ ""
	in
	
	let str = str ^ "," in
	
	(* get the upper bound as a string *)
	let str =
		match hi_bnd with
		  Infinity -> str ^ ".)"
		| Open(i) -> str ^ (Rawint.to_string i) ^ ")"
		| Closed(i) -> str ^ (Rawint.to_string i) ^ "]"
	in
		str
(*
 * print_table, print_imports, print_exports
 *
 * Purpose:
 *	prints the import/export table of the 'Byte_frame_type.prog'
 *)
let print_table out table =
	SymbolTable.iter (fun v sym_type ->
		match sym_type with
		  Function -> Format.fprintf out "\t.fun\t%s\n" (string_of_symbol v); ()
		| Variable -> Format.fprintf out "\t.var\t%s\n" (string_of_symbol v); ()
	) table

let print_imports out imports =
	Format.pp_print_string out ".imports\n";
	print_table out imports;
	Format.pp_print_string out "\n.end\n\n"

and print_exports out exports data =
	Format.pp_print_string out ".exports\n";
	print_table out exports;
	Format.pp_print_string out ".end\n\n"	

(*
 * print_data
 *
 * Purpose:
 *	Prints the data section of 'Byte_frame_type.prog'
 *)
let print_data out data globals =
	Format.fprintf out ".datasize = %d\n\n" (spset_spill_count data);
	Format.pp_print_string out ".data\n";
	SymbolTable.iter (fun v dt -> 
		try 
			Format.fprintf out "\t%s: $%d = %s as %s\n" (string_of_data_class((data_class_of_data_type dt)))
				(spset_spill_location data v) (string_of_data_type dt) (string_of_symbol v);
			()
		with Not_found ->
			raise (Failure("print_data: cannot find data value (" ^ (string_of_symbol v) ^ ")\n"))) globals; 
 	
	Format.pp_print_string out ".end\n\n"
	
(*
 * print_blocks
 *
 * Purpose:
 *	Iterate through the block trace for the 'Byte_frame_type.prog' and print each
 *	one to the specified output.
 *)
let print_blocks out blocks = 
	let print_block out block' =
		let {
			block_debug = fun_dbg;
			block_label = fun_sym;	
			block_args = fun_args;
			block_inst = code;
		} = block' in
	
		(* print the function arguments *)
		Format.fprintf out ".function\t%s(%s)\n" (string_of_symbol fun_sym) (string_of_fun_args fun_args);
		
		(* print the code for the function *)
		Format.fprintf out "%s.end\n\n" (string_of_code code);
			()
	in
		Trace.pp_print out (fun out block' -> print_block out block') blocks

(*
 * print_prog
 *
 * Purpose:
 *	print the assembly version of the program.
 *)
let print_prog out program = 
	let {
		byte_import = imports;
		byte_export = exports;
		byte_global = globals;
		byte_funs = blocks;
		byte_data = data;
	} = program in

	print_exports out exports data;
	print_imports out imports;
 	print_data out data globals;
		print_blocks out blocks

(*
 * print_object
 *
 * Purpose:
 *	print the object file from the given source file.
 *)
let print_object name prog =
	let
	{ 
		byte_import = imports;
		byte_export = exports;
		byte_global = globals;
		byte_funs = blocks;
		byte_data = data;
	} = prog in
	
	let out = open_out name in
	let out_format = Format.formatter_of_out_channel out in
	
	print_exports out_format exports data;
	print_imports out_format imports;
 	print_data out_format data globals;
	print_blocks out_format blocks;
		close_out out
