(*
 * Type of the frame.  This determines the calling convention for
 * backend frames.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Trace

open Rawint
open Rawfloat


(*
 * Basic vars.
 *)
type var = symbol
type label = symbol


(*
 * The ``code'' type is an abstract characterization of an instruction.
 * For register allocation all we care about is the defs/uses in the
 * instruction, and whether the instruction is normal, or a move, or a
 * jump.
 *
 * The code_class indicates the type of instruction:
 *    CodeNormal:       A typical (computational) instruction.
 *    CodeMove:         An instruction which moves a value from one
 *                      location to another. There should be exactly one
 *                      def and one use.
 *    CodeJump label:   An instruction which *may* jump to the label
 *                      indicated.  This includes conditional jumps.
 *
 * The code structure is parameterized on 'inst, the type of instructions:
 *    code_src:         Sources (uses) for the instruction
 *    code_dst:         Destinations (defs) for the instruction
 *    code_class:       Classification for the instruction (see above)
 *    code_inst:        The original instruction
 *)
type code_class =
   CodeNormal
 | CodeMove
 | CodeJump of label

type 'inst code =
   { code_src   : var list;
     code_dst   : var list;
     code_class : code_class;
     code_inst  : 'inst;
   }


(*
 * At other times, we need to know what the live variables are.
 * This structure is parameterized on 'inst, the type of the
 * instructions.
 *    live_src:         Sources (uses) for the instruction
 *    live_dst:         Destinations (defs) for the instruction
 *    live_out:         The set of variables live after this inst.
 *    live_class:       Classification for the instruction (see above)
 *    live_depth:       Loop nesting depth for the instruction
 *    live_inst:        Original instruction
 *)
type 'inst live =
   { live_src   : var list;
     live_dst   : var list;
     live_out   : SymbolSet.t;
     live_class : code_class;
     live_depth : int;
     live_inst  : 'inst;
   }


(*
 * Migration data.  This structure is not used for programs compiled from
 * source, but turns up when a program has been migrated from another
 * machine.  It contains information needed to reconstruct the program's
 * state.  Since this structure is often not needed, it is usually wrapped
 * in the option type.
 *
 *    mig_key:          Key name for the label to jump to.  This key is
 *                      derived from the unique identifier for the migrate
 *                      call, and is currently ``migrate_label_<key>'',
 *                      where <key> is the 5-digit ID given in SysMigrate.
 *
 *    mig_channel:      Channel descriptor for incoming state data.  This
 *                      is a UNIX file descriptor, and is opened by the
 *                      migrate server.
 *
 *    mig_bidir:        True if mig_channel is bidirectional.  If it is
 *                      bidirectional, then the program should acknowledge
 *                      incoming data, and should attempt to verify that
 *                      the state is complete with the sender.
 *
 *    mig_initofs:      Initial offset in the channel to read from.  If
 *                      the channel is not bidirectional, then it is a
 *                      descriptor for a file on disk.  We need to skip
 *                      past the header and program code stored in the file
 *                      to find the state for the program.
 *
 *    mig_registers:    Index for the registers block (the environment
 *                      that registers are stored into before SysMigrate).
 *                      This is an unsafe index, so it is usually hashed.
 *
 *    mig_ptr_size:     Total size of the pointer table, in words (number
 *                      of entries in the pointer table).
 *
 *    mig_heap_size:    Total size of the heap, in bytes.
 *
 *    mig_debug:        Other migrate debugging flags.  See x86_runtime.h
 *                      or net/migrate/migrate.ml for details.
 *)
type migrate =
   { mig_key       : string;
     mig_channel   : int32;
     mig_bidir     : bool;
     mig_initofs   : int32;
     mig_registers : int32;
     mig_ptr_size  : int32;
     mig_heap_size : int32;
     mig_debug     : int32;
   }


(*
 * Pointer types.  These are used mainly in the atom class and pointer
 * coercion operators to determine what type of pointer we are using.
 * Note:  memory operations into the context (e.g. reading the pointer
 * table) are considered PtrAggr operations.
 *    PtrBlock:   an ML-style block (tuple, array, union...)
 *    PtrAggr:    a C-style aggregate block (rawdata block)
 *)
type ptrty =
   PtrBlock
 | PtrAggr


(*
 * Function types.  For function pointers, we need to know the arity
 * of the function, and the types of arguments required for the function.
 * This information is usually ignored except for AC checking and for
 * runtime safety checks.  The information is represented by a list of
 * atom classes, corresponding to the parametres the function accepts.
 *    FunAny:     argument types for this function are not known
 *    FunArgs a:  List of ACs for arguments to the function
 *)
and funty =
   FunAny
 | FunArgs of atom_class list


(*
 * Atom classes are mainly used for defining the width of a subscripting
 * operation.  However, these are also important in selecting arithmetic
 * operators and determining what types of registers a value can be stored
 * in.
 *
 * The following atom classes are defined:
 *    ACInt:            ML-style integer (31-bit, signed)
 *    ACPoly:           Either ML-style integer or a real pointer
 *    ACRawInt:         Raw integer with indicated precision, signedness
 *    ACFloat:          Floating-point value with indicated precision
 *    ACPointer:        Real pointer, either ML-style block or C aggr block
 *    ACPointerInfix:   An infix pointer.  This is a pair of pointers; the
 *                      first is a base pointer (normal ACPointer), and the
 *                      second points somewhere in the block.  This is more
 *                      efficient than dealing with offsets in some cases.
 *    ACFunction:       A real function pointer, with indicated argument ACs
 *)
and atom_class =
   ACInt
 | ACPoly
 | ACRawInt of int_precision * int_signed
 | ACFloat of float_precision
 | ACPointer of ptrty
 | ACPointerInfix of ptrty
 | ACFunction of funty


(*
 * Arity tags are a simplification of atom classes that fold all value
 * types into a single class.  These are used to determine which functions
 * are compatible safely, without coercion.
 *
 * Arity defines the basic types of parametres.  Note we only care about
 * safety, so all ``value'' atom classes are put into a single atom class
 * for simplicity.  Pointer types are segregated so they can not be mixed
 * in dangerous ways.
 *
 * More information is available in arch/mir/util/mir_arity.ml.
 *
 * The arity classes are listed below:
 *    ArityValue:          Includes ACInt, ACRawInt, and ACFloat values
 *    ArityPointer:        Covers values of type ACPointer
 *    ArityPointerInfix:   Covers values of type ACPointerInfix
 *    ArityFunction:       Covers values of type ACFunction
 *    ArityPoly:           Covers all polymorphic values (must be separate)
 *)
type arity =
   ArityValue
 | ArityPointer of ptrty
 | ArityPointerInfix of ptrty
 | ArityFunction of arity list
 | ArityPoly


(*
 * General debugging info.
 *    file_dir:         Directory containing the source file
 *    file_name:        Actual base name of the source file
 *    file_class:       Source language type (C, Naml, Java)
 *)
type file_info =
   { file_dir   : string;
     file_name  : string;
     file_class : Fir.file_class
   }


(*
 * Import and export info.  Both imports and exports are symbol tables
 * using the name of the MIR symbol as the key (the MIR symbol is not
 * the same as the import/export name that is linked against).
 *
 * For imported symbols, import_name is the name of the symbol that is
 * being imported, and import_info contains typing information for the
 * symbol.
 *
 *    ImportGlobal
 *       Symbol is a variable name with unknown type
 *
 *    ImportFun (stdarg, arg_list, ac_list)
 *       Symbol is a function name.  If stdarg is true, then the function
 *       uses the standard args calling convention.  The arg_list is the
 *       FIR simplified types for the arguments, and the ac_list is the
 *       MIR atom classes for the arguments.
 *
 * For exported symbols, export_name is the name of the symbol that is
 * being exported, and export_is_fun is true if the symbol should be
 * exported as a function definition.
 *)
type import_info =
   ImportGlobal
 | ImportFun of bool * Fir.import_arg list * atom_class list

type import =
   { import_name : string;
     import_info : import_info
   }

type export =
   { export_name : string;
     export_is_fun : bool
   }


(*
 * This is a backend description suitable for use by MIR generation.
 * Much of the frame contains symbols corresponding to the context;
 * please see x86_runtime.h for a complete description of the symbols.
 *)
module type BackendSig =
sig
   (* Type of an assembly block *)
   type block

   (* Pointer table descriptors *)
   val pointer_base : var
   val pointer_size : var

   (* Closure table descriptors *)
   val function_base : var
   val function_size : var

   (* Segmentation faults *)
   val other_fault : int
   val index_fault : int
   val fun_index_fault : int
   val pointer_index_fault : int
   val bounds_fault : int
   val lower_bounds_fault : int
   val upper_bounds_fault : int
   val null_pointer_fault : int
   val fun_arity_tag_fault : int
   val fault_count : int
   val seg_fault_labels : label array

   (* Debug labels *)
   val debug_alloc : int
   val debug_let_poly : int
   val debug_let_pointer : int
   val debug_let_function : int
   val debug_set_poly : int
   val debug_set_pointer : int
   val debug_set_function : int
   val debug_pre_let_poly : int
   val debug_pre_let_pointer : int
   val debug_pre_let_function : int
   val debug_pre_set_pointer : int
   val debug_pre_set_function : int
   val debug_count : int
   val debug_labels : label array

   (* Hash value for pointer entries *)
   val hash_index_label : label
   val hash_fun_index_label : label
   val index_shift32 : int32

   (* Pointer table masks *)
   val ptable_pointer_mask : int32
   val ptable_valid_bit_off : int

   (* Size constants *)
   val aggr_header_size : int
   val block_header_size : int

   (* Special tags *)
   val rttd_tag : int

   (* Migration values *)
   val migrate_debug_gdbhooks : int32

   (* Backend arity tags *)
   val backend_arity_tags : arity list list
end


(*
 * The frame defines the calling convention for the backend.
 *)
module type FrameSig =
sig
   (*
    * Types:
    *    'a block:   An assembly block (parameterized arbitrarily)
    *    spset:      Information on the spill set (used by RA)
    *    reg_class:  Type used to mark the various register classes
    *    inst:       Type of an instruction for this architecture.
    *)
   type 'a block
   type spset
   type reg_class = int
   type inst

   (*
    * Cost for load/store operations.
    *    def_cost:      Cost of def in a normal instruction.
    *    use_cost:      Cost of use in a normal instruction.
    *    mov_def_cost:  Cost of def when used in a move instruction.
    *    mov_use_cost:  Cost of use when used in a move instruction.
    *)
   val def_cost : int
   val use_cost : int
   val mov_def_cost : int
   val mov_use_cost : int

   (*
    * block_label block -- returns the label for a block.
    *
    * block_code ignore block -- converts an instruction block into a
    *    list of code instructions, suitable for liveness analysis.
    *
    * block_live block live -- converts a block of instructions into a
    *    block of instructions with liveness information included.
    *
    * print_blocks out blocks -- prints out all blocks in the trace.
    *
    * print_live_blocks out blocks -- prints out all blocks (with
    *    liveness information) in the trace.
    *)
   val block_label : 'a block -> label
   val block_code : var list -> inst block -> inst code list
   val block_live : inst block -> inst live list -> inst live block
   val print_blocks : formatter -> inst block trace -> unit
   val print_live_blocks : formatter -> inst live block trace -> unit

   (*
    * reg_class_count -- return the total number of register classes.
    *    An example set of register classes is {integer, float}.
    *
    * registers -- list of registers for each register class (the array
    *    is indexed by each register class).  These are registers that
    *    are available for register allocation.
    *
    * registers_special -- list of special registers for each register
    *    class.  These are not available for register allocation.  An
    *    example of a special register is the flags register for ints.
    *)
   val reg_class_count : int
   val registers : var list array
   val registers_special : var list array

   (*
    * Spilling.
    *)
   val spset_spill : spset -> var -> var -> reg_class -> spset
   val spset_add : spset -> var -> var -> spset

   (*
    * Apply a substitution.
    *)
   val subst_blocks : spset -> inst block trace -> inst block trace

   (*
    * Get all the vars defined in any instruction.
    *)
   val vars_blocks : reg_class SymbolTable.t -> inst block trace -> reg_class SymbolTable.t
end


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
