/*
 * Basic definitions for FC.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 */
#ifndef _FC_H
#define _FC_H

#ifdef __mcc

/* Basic functions */
static void print_char(char x) = "print_char";
static void print_int(int x) = "print_int";
static void print_hex(int x) = "print_hex";
static void print_long(long x) = "print_long";
static void print_long_hex(long x) = "print_long_hex";
static void print_float(double x) = "print_float";
static void print_string(char *s) = "print_string";
static void print_string_int(char *s, int) = "print_string_int";
static void print_string_hex(char *s, int) = "print_string_hex";
static void print_string_long_hex(char *s, long) = "print_string_long_hex";
static void flush() = "flush";
static int atoi(char *) = "atoi";
static double atof(char *) = "atof";
static void exit(int) = "exit";
static void *malloc(int) = "malloc";

/* Atomic primitives.  Note that atomic_commit and atomic_rollback both
   *optionally* take a level argument; if no level is specified, then
   level 0 (corresponds to the most recent level) is used.  */
static int atomic_entry(int c) = "atomic_entry";
static void atomic_commit() = "atomic_commit";
static void atomic_commit_level(int level) = "atomic_commit_level";
static void atomic_rollback(int c) = "atomic_rollback";
static void atomic_rollback_level(int level, int c) = "atomic_rollback_level";

/* Migration primitives */
static void migrate(char *) = "migrate";

/* Set a default definition for NULL */
#define  NULL  0

/* If using GCC's preprocessor, then __GNUC__ is set.
   Let's undefine it here, to avoid breakage in other
   header files... */
#undef   __GNUC__

#else /* not __mcc */

#include <stdio.h>
#include <stdlib.h>

static void print_string(char *s)
{
    printf("%s", s);
}

static void print_int(int i)
{
    printf("%d", i);
}

#endif /* __mcc? */

#endif /* _FC_H */
