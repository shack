(*
 * This is the generic backend modules.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)

open Symbol
open Frame_type


(*
 * WARNING:  The values in here are nonsensical.
 *           Do not use them as real backend values.
 *)


let null_symbol = new_symbol_string "null"
let fail_miserably _ = raise (Failure "Noarch_backend: cannot compile to assembly")


module Backend : BackendSig =
struct
   (* Type of an assembly block *)
   type block = unit

   (* Pointer table descriptors *)
   let pointer_base = null_symbol
   let pointer_size = null_symbol

   (* Closure table descriptors *)
   let function_base = null_symbol
   let function_size = null_symbol

   (* Segmentation faults *)
   let other_fault = 0
   let index_fault = 0
   let fun_index_fault = 0
   let fun_arity_tag_fault = 0
   let pointer_index_fault = 0
   let bounds_fault = 0
   let lower_bounds_fault = 0
   let upper_bounds_fault = 0
   let null_pointer_fault = 0
   let fault_count = 0
   let seg_fault_labels = [||]

   (* Debug labels *)
   let debug_alloc = 0
   let debug_let_poly = 0
   let debug_let_pointer = 0
   let debug_let_function = 0
   let debug_set_poly = 0
   let debug_set_pointer = 0
   let debug_set_function = 0
   let debug_pre_let_poly = 0
   let debug_pre_let_pointer = 0
   let debug_pre_let_function = 0
   let debug_pre_set_pointer = 0
   let debug_pre_set_function = 0
   let debug_count = 0
   let debug_labels = [||]

   (* Hash value for pointer entries *)
   let hash_index_label = null_symbol
   let hash_fun_index_label = null_symbol
   let index_shift32 = Int32.zero

   (* Pointer table masks *)
   let ptable_pointer_mask = Int32.zero
   let ptable_valid_bit_off = 0

   (* Size constants *)
   let aggr_header_size = 0
   let block_header_size = 0

   (* Special tags *)
   let rttd_tag = 0

   (* Migration values *)
   let migrate_debug_gdbhooks = Int32.zero

   (* Arity tags *)
   let backend_arity_tags = []
end


(*
 * The abstract frame.
 *)
module Frame =
struct
   type 'a block = unit
   type spset = unit
   type reg_class = int
   type inst = unit

   let def_cost = 0
   let use_cost = 0
   let mov_def_cost = 0
   let mov_use_cost = 0

   let block_label = fail_miserably
   let block_code = fail_miserably
   let block_live = fail_miserably
   let print_blocks = fail_miserably
   let print_live_blocks = fail_miserably

   let reg_class_count = 0
   let registers = [||]
   let registers_special = [||]

   let spset_spill = fail_miserably
   let spset_add = fail_miserably

   let subst_blocks = fail_miserably

   let vars_blocks = fail_miserably
end
