(*
   Fail miserably attempting to compile in the noarch backend
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


let fail_miserably _ = raise (Failure "Noarch_codegen: cannot compile to assembly")


(*
 * Build this as a module.
 *)
module Codegen =
struct
   type prog = unit

   let print_prog = fail_miserably
   let print_object = fail_miserably
   let link_object = fail_miserably

   let compile_asm_of_fir = fail_miserably
   let compile_asm_of_mir = fail_miserably
   let compile_mir_of_fir = fail_miserably

   let catch f x = f x
end
