(*
 * Translate to MIR from FIR.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug

open Symbol
open Location
open Interval_set

open Frame_type
open Sizeof_const
open Sizeof_type

open Fir_set
open Fir_exn
open Fir_pos
open Fir_env
open Fir_type
open Fir_print
open Fir_frame
open Fir_mprog

open Mir
open Mir_ds
open Mir_print
open Mir_of_fir

module Pos = MakePos (struct let name = "Mir_fir" end)
open Pos

(*
 * Simple function to take unions of type environments
 *)
let venv_union venv1 venv2 =
   SymbolTable.fold venv_add venv1 venv2

(*
 * Print debugging info during MIR generation.
 *)
let debug_mir = ref false

(*
 * Generate debugging code in the MIR.
 * Currently, debugging code is always generated,
 * but filtered in x86_mir.ml
 *)
let debug_code = ref false

(*
 * Used in certain expression builders to determine if the
 * expression originated from normal FIR code or from a
 * FIR assert statement.
 *)
type generator =
   GenStandard    (* Originated from a {Let,Set}Subscript or other normal FIR *)
 | GenAssertPtr   (* Originated from a FIR Assert statement, pointer check *)


module MirOfFir (Frame : BackendSig) : MirOfFirSig =
struct
   module MirMemory = Mir_memory.MirMemory (Frame)
   module MirUtil = Mir_util.MirUtil (Frame)
   module MirPoly = Mir_poly.MirPoly (Frame)
   module MirArity = Mir_arity.MirArity (Frame)
   open Frame
   open MirMemory
   open MirUtil
   open MirPoly
   open MirArity


   (************************************************************************
    * NIL BLOCKS
    ************************************************************************)


   (*
    * Name of the global variable used for Nil pointers.
    *)
   let atom_nil_block = new_symbol_string "atom_nil"


   (************************************************************************
    * TYPES
    ************************************************************************)


   (*
    * When unions are translated, we have to map the index to
    * constant/non-constant cases.
    *)
   type tag =
      TagConstant of int
    | TagNonConstant of int


   (************************************************************************
    * UNION INDEX OPERATIONS
    ************************************************************************)


   (*
    * Constant tags are translated to disambiguate them from pointers.
    *)
   let const_tag_of_index i =
      i

   let nconst_tag_of_index i =
      i

   (*
    * Get the index of a constructor.
    * Constructors are divided into two classes:
    * constant constructors and non-constant.
    * Constants are shifted and or'ed with 1.
    *)
   let rec index_of_const_fields pos tyll i_const i_nconst index =
      match tyll, index with
         [] :: _, 0 ->
            const_tag_of_index i_const
       | (_ :: _) :: _, 0 ->
            nconst_tag_of_index i_nconst
       | [] :: tyll, _ ->
            index_of_const_fields pos tyll (succ i_const) i_nconst (pred index)
       | (_ :: _) :: tyll, _ ->
            index_of_const_fields pos tyll i_const (succ i_nconst) (pred index)
       | [], _ ->
            raise (FirException (pos, StringError "index_of_const_fields: index out of bounds"))

   let index_of_const tenv pos ty_v i =
      let pos = string_pos "index_of_const" pos in
         match tenv_lookup tenv pos ty_v with
            Fir.TyDefUnion (_, tyll) ->
               index_of_const_fields pos tyll 0 0 i
          | Fir.TyDefLambda _
          | Fir.TyDefDTuple _ ->
               raise (FirException (pos, StringError ("index_of_const: not a union type")))

   (*
    * Return the types of the index'th constructor of a union, given its list of
    * constructor types.  Handles polymorphic types.
    *)
   let rec types_of_const_fields tenv pos ty ty_vars tyll index =
      let pos = string_pos "types_of_const_fields" pos in
         match tyll, index with
            [], 0 ->
               []
          | tyml :: _, 0 ->
               (* Get the types for the polymorphic types. *)
               let _, ty = dest_all_type_dir tenv pos ty in
               let _, ty_var_types, _ = dest_union_type_dir tenv pos ty in

               (* Create a SymbolTable which maps type variables to types. *)
               let ty_trans =
                  List.fold_left2 SymbolTable.add SymbolTable.empty ty_vars ty_var_types
               in

               (*
                * Get the types of the constructor (by discarding the mutable
                * field).
                *)
               let tyl = List.map fst tyml in
                  (* Replace the TyVars therein. *)
                  List.map (function
                     Fir.TyVar v ->
                        begin
                           try SymbolTable.find ty_trans v with
                              Not_found ->
                                 raise (FirException (pos, UnboundType v))
                        end
                   | ty ->
                        ty)
                  tyl

          | _ :: tyll, _ ->
               types_of_const_fields tenv pos ty ty_vars tyll (pred index)
          | [], _ ->
               raise (FirException (pos, StringError "index out of bounds"))

   (*
    * Return a list of the types for the ith constructor of the union associated
    * with type variable ty_v.
    *)
   let types_of_const tenv pos ty ty_v i =
      let pos = string_pos "types_of_const" pos in
         match tenv_lookup tenv pos ty_v with
            Fir.TyDefUnion (ty_vars, tyll) ->
               types_of_const_fields tenv pos ty ty_vars tyll i
          | Fir.TyDefLambda _
          | Fir.TyDefDTuple _ ->
               raise (FirException (pos, StringError ("not a union type")))

   (*
    * Produce index translation tables.
    *)
   let make_union_trans tyll =
      let rec collect i i_const i_nconst cases = function
         tyl :: tyll ->
            (match tyl with
                [] ->
                   let tag = const_tag_of_index i_const in
                      collect (succ i) (succ i_const) i_nconst (TagConstant tag :: cases) tyll
              | _ ->
                   let tag = nconst_tag_of_index i_nconst in
                      collect (succ i) i_const (succ i_nconst) (TagNonConstant tag :: cases) tyll)
       | [] ->
            Array.of_list (List.rev cases)
      in
         collect 0 0 0 [] tyll

   (*
    * Produce the index sets from the translation.
    *)
   let index_sets_of_trans trans =
      let len = Array.length trans in
      let rec collect i_set t_set i =
         if i = len then
            i_set, t_set
         else
            match trans.(i) with
               TagConstant _ ->
                  let i_set = IntSet.add_point i_set i in
                     collect i_set t_set (succ i)
             | TagNonConstant _ ->
                  let t_set = IntSet.add_point t_set i in
                     collect i_set t_set (succ i)
      in
      let empty = IntSet.empty in
         collect empty empty 0

   (*
    * Translate the sets.
    *)
   let translate_set trans s =
      let translate_index i =
         match trans.(i) with
            TagConstant i -> i
          | TagNonConstant i -> i
      in
      let translate_bound = function
         Infinity ->
            Infinity
       | Open i ->
            Open (translate_index i)
       | Closed i ->
            Closed (translate_index i)
      in
      let empty = IntSet.empty in
      let s =
        IntSet.fold (fun s left right ->
              let left = translate_bound left in
              let right = translate_bound right in
              let s' = IntSet.of_interval left right in
                  IntSet.union s s') empty s
      in
         IntSet s

   (*
    * For enums, we need to translate from IntSet to RawIntSet.
    *)
   let translate_raw_set pre signed s =
      let translate_bound = function
         Infinity ->
            Infinity
       | Open i ->
            Open (Rawint.of_int pre signed i)
       | Closed i ->
            Closed (Rawint.of_int pre signed i)
      in
      let empty = RawIntSet.empty pre signed in
      let s =
        IntSet.fold (fun s left right ->
              let left = translate_bound left in
              let right = translate_bound right in
              let s' = RawIntSet.of_interval pre signed left right in
                  RawIntSet.union s s') empty s
      in
         Fir.RawIntSet s

   let translate_raw_cases pre signed cases =
      List.map (fun (label, set, e) ->
            let set =
               match set with
                  Fir.RawIntSet _ -> set
                | Fir.IntSet s -> translate_raw_set pre signed s
            in
               label, set, e) cases


   (************************************************************************
    * TRANSLATION
    ************************************************************************)


   (*
    * Translate a var.
    *)
   let mir_var tenv venv rttd frames funs pos v =
      let pos = string_pos "mir_var" pos in
         if SymbolTable.mem funs v then
            let _, _, ty, _, _ = SymbolTable.find funs v in
            let ac = atom_class_of_type tenv pos ty in
               AtomFunVar (ac, v)
         else
            let ac = atom_class_of_type tenv pos (venv_lookup venv pos v) in
               AtomVar (ac, v)


   (*
    * Operators.
    *)
   let mir_raw_int_of_raw_int_atom pos dpre dsigned spre ssigned a =
      let op = RawIntOfRawIntOp (dpre, dsigned, spre, ssigned) in
         AtomUnop (op, a)

   let mir_pointer_of_rawint_atom pos spre ssigned a =
      mir_raw_int_of_raw_int_atom pos Rawint.Int64 false spre ssigned a

   let mir_rawint_of_pointer_atom pos dpre dsigned a =
      mir_raw_int_of_raw_int_atom pos dpre dsigned Rawint.Int64 false a

   let mir_rawint_of_enum_atom pos dpre dsigned a =
      mir_raw_int_of_raw_int_atom pos dpre dsigned precision_native_int true a

   let mir_rawint_of_int_atom pos pre signed a =
      AtomUnop (RawIntOfIntOp (pre, signed), a)

   let mir_float_of_float_atom pos dpre spre a =
      AtomUnop (FloatOfFloatOp (dpre, spre), a)

   let mir_eq_eq_atom tenv venv pos a1 a2 =
      let pos = string_pos "mir_eq_eq_atom" pos in
      let ty = type_of_atom venv pos a1 in
      let ac = atom_class_of_type tenv pos ty in
         AtomBinop (EqOp ac, a1, a2)

   let mir_neq_eq_atom tenv venv pos a1 a2 =
      let pos = string_pos "mir_eq_eq_atom" pos in
      let ty = type_of_atom venv pos a1 in
      let ac = atom_class_of_type tenv pos ty in
         AtomBinop (NeqOp ac, a1, a2)


   (*
    * Translate an atom.
    *)
   let rec mir_atom tenv venv rttd frames funs pos a =
      let pos = string_pos "mir_atom" pos in
         match a with
            Fir.AtomNil ty ->
               if atom_class_may_be_pointer (atom_class_of_type tenv pos ty) then
                  AtomVar (ACPointer PtrAggr, atom_nil_block)
               else
                  AtomRawInt (Rawint.of_int precision_pointer false 0)
          | Fir.AtomInt i ->
               AtomInt i
          | Fir.AtomEnum (_, i) ->
               AtomRawInt (Rawint.of_int precision_native_int true i)
          | Fir.AtomRawInt i ->
               AtomRawInt i
          | Fir.AtomFloat x ->
               AtomFloat x
          | Fir.AtomConst (_, ty_v, i) ->
               AtomUnop (PolyOfIntOp, AtomInt (index_of_const tenv pos ty_v i))
          | Fir.AtomVar v
          | Fir.AtomFun v ->
               mir_var tenv venv rttd frames funs pos v
          | Fir.AtomLabel (label, off1) ->
               let off2 = offsetof_label frames pos label in
               let off2 = Rawint.of_int precision_subscript signed_subscript off2 in
               let off = Rawint.add off1 off2 in
                  AtomRawInt off
          | Fir.AtomSizeof (vars, off) ->
               let off =
                  List.fold_left (fun off v ->
                        let size = sizeof_frame frames pos v in
                        let size = Rawint.of_int precision_subscript signed_subscript size in
                           Rawint.add off size) off vars
               in
                  AtomRawInt off

            (*
             * BUG: jyh: I'm not sure this is what we want to do here,
             * but it seems like we can ignore polymorphism,
             *)
          | Fir.AtomTyApply (a, _, _) ->
               mir_atom tenv venv rttd frames funs pos a
          | Fir.AtomTyPack (v, _, _)
          | Fir.AtomTyUnpack v ->
               mir_var tenv venv rttd frames funs pos v

            (*
             * Arithmetic.
             *)
          | Fir.AtomUnop (op, a) ->
               mir_unop_atom tenv venv rttd frames funs pos op a
          | Fir.AtomBinop (op, a1, a2) ->
               mir_binop_atom tenv venv rttd frames funs pos op a1 a2

   (*
    * Translate an operator.
    *)
   and mir_unop_atom tenv venv rttd frames funs pos op a =
      let pos = string_pos "mir_op_atomexpr" pos in
      let a = mir_atom tenv venv rttd frames funs pos a in
         match op with
            Fir.NotEnumOp n ->
               if not (is_power2 n) then
                  raise (FirException (pos, StringIntError ("enum negation is not a power of two", n)));
               let ac = ACRawInt (precision_native_int, false) in
               let mask = Rawint.of_int precision_native_int false (n - 1) in
                  AtomBinop (XorOp ac, a, AtomRawInt mask)
          | Fir.UMinusIntOp ->
               AtomUnop (UMinusOp ACInt, a)
          | Fir.NotIntOp ->
               AtomUnop (NotOp ACInt, a)
          | Fir.AbsIntOp ->
               AtomUnop (AbsOp ACInt, a)
          | Fir.UMinusRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomUnop (UMinusOp ac, a)
          | Fir.NotRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomUnop (NotOp ac, a)
          | Fir.RawBitFieldOp (pre, signed, off, len) ->
               let ac = ACRawInt (pre, signed) in
                  AtomUnop (BitFieldOp (pre, signed, Int32.of_int off, Int32.of_int len), a)
          | Fir.UMinusFloatOp pre ->
               let ac = ACFloat pre in
                  AtomUnop (UMinusOp ac, a)
          | Fir.AbsFloatOp pre ->
               let ac = ACFloat pre in
                  AtomUnop (AbsOp ac, a)
          | Fir.SinFloatOp pre ->
               let ac = ACFloat pre in
                  AtomUnop (SinOp pre, a)
          | Fir.CosFloatOp pre ->
               let ac = ACFloat pre in
                  AtomUnop (CosOp pre, a)
          | Fir.SqrtFloatOp pre ->
               let ac = ACFloat pre in
                  AtomUnop (SqrtOp pre, a)
          | Fir.IntOfFloatOp pre ->
               AtomUnop (IntOfFloatOp pre, a)
          | Fir.FloatOfIntOp pre ->
               AtomUnop (FloatOfIntOp pre, a)
          | Fir.RawIntOfEnumOp (pre, signed, n) ->
               mir_rawint_of_enum_atom pos pre signed a
          | Fir.RawIntOfIntOp (pre, signed) ->
               mir_rawint_of_int_atom pos pre signed a
          | Fir.RawIntOfFloatOp (pre1, signed, pre2) ->
               AtomUnop (RawIntOfFloatOp (pre1, signed, pre2), a)
          | Fir.FloatOfRawIntOp (pre1, pre2, signed) ->
               AtomUnop (FloatOfRawIntOp (pre1, pre2, signed), a)
          | Fir.RawIntOfRawIntOp (dpre, dsigned, spre, ssigned) ->
               mir_raw_int_of_raw_int_atom pos dpre dsigned spre ssigned a
          | Fir.FloatOfFloatOp (dpre, spre) ->
               mir_float_of_float_atom pos dpre spre a
          | Fir.PointerOfRawIntOp (spre, ssigned) ->
               mir_pointer_of_rawint_atom pos spre ssigned a
          | Fir.RawIntOfPointerOp (dpre, dsigned) ->
               mir_rawint_of_pointer_atom pos dpre dsigned a
          | Fir.PointerOfBlockOp op ->
               let ptrty = ptrty_of_sub_block op in
               let ac = ACPointerInfix ptrty in
               let zero = AtomRawInt (Rawint.of_int Rawint.Int32 false 0) in
                  AtomBinop (InfixOfBaseOffsetOp ptrty, a, zero)
          | Fir.DTupleOfDTupleOp _
          | Fir.UnionOfUnionOp _
          | Fir.RawDataOfFrameOp _ ->
               (* This is a nop *)
               a
          | Fir.TanFloatOp _
          | Fir.ASinFloatOp _
          | Fir.ACosFloatOp _
          | Fir.ATanFloatOp _
          | Fir.SinHFloatOp _
          | Fir.CosHFloatOp _
          | Fir.TanHFloatOp _
          | Fir.ExpFloatOp _
          | Fir.LogFloatOp _
          | Fir.Log10FloatOp _
          | Fir.CeilFloatOp _
          | Fir.FloorFloatOp _
          | Fir.IntOfRawIntOp _
          | Fir.LengthOfBlockOp _ ->
               raise (FirException (pos, StringError ("unary operator not implemented: " ^ Fir_print.string_of_unop op)))

   and mir_binop_atom tenv venv rttd frames funs pos op a1 a2 =
      let pos = string_pos "mir_op_atom" pos in
      let a1 = mir_atom tenv venv rttd frames funs pos a1 in
      let a2 = mir_atom tenv venv rttd frames funs pos a2 in
         match op with
            Fir.AndEnumOp n ->
               let ac = ACRawInt (precision_native_int, false) in
                  AtomBinop (AndOp ac, a1, a2)
          | Fir.OrEnumOp _ ->
               let ac = ACRawInt (precision_native_int, false) in
                  AtomBinop (OrOp ac, a1, a2)
          | Fir.XorEnumOp _ ->
               let ac = ACRawInt (precision_native_int, false) in
                  AtomBinop (XorOp ac, a1, a2)
          | Fir.PlusIntOp ->
               AtomBinop (PlusOp ACInt, a1, a2)
          | Fir.MinusIntOp ->
               AtomBinop (MinusOp ACInt, a1, a2)
          | Fir.MulIntOp ->
               AtomBinop (MulOp ACInt, a1, a2)
          | Fir.DivIntOp ->
               AtomBinop (DivOp ACInt, a1, a2)
          | Fir.RemIntOp ->
               AtomBinop (RemOp ACInt, a1, a2)
          | Fir.LslIntOp ->
               AtomBinop (SlOp ACInt, a1, a2)
          | Fir.LsrIntOp ->
               AtomBinop (LSrOp ACInt, a1, a2)
          | Fir.AsrIntOp ->
               AtomBinop (ASrOp ACInt, a1, a2)
          | Fir.AndIntOp ->
               AtomBinop (AndOp ACInt, a1, a2)
          | Fir.OrIntOp ->
               AtomBinop (OrOp ACInt, a1, a2)
          | Fir.XorIntOp ->
               AtomBinop (XorOp ACInt, a1, a2)
          | Fir.MinIntOp ->
               AtomBinop (MinOp ACInt, a1, a2)
          | Fir.MaxIntOp ->
               AtomBinop (MaxOp ACInt, a1, a2)

          | Fir.PlusRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (PlusOp ac, a1, a2)
          | Fir.MinusRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (MinusOp ac, a1, a2)
          | Fir.MulRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (MulOp ac, a1, a2)
          | Fir.DivRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (DivOp ac, a1, a2)
          | Fir.RemRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (RemOp ac, a1, a2)
          | Fir.SlRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (SlOp ac, a1, a2)
          | Fir.SrRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
               let op =
                  if signed then
                     ASrOp ac
                  else
                     LSrOp ac
               in
                  AtomBinop (op, a1, a2)
          | Fir.AndRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (AndOp ac, a1, a2)
          | Fir.OrRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (OrOp ac, a1, a2)
          | Fir.XorRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (XorOp ac, a1, a2)
          | Fir.MinRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (MinOp ac, a1, a2)
          | Fir.MaxRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (MaxOp ac, a1, a2)
          | Fir.RawSetBitFieldOp (pre, signed, off, len) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (SetBitFieldOp (pre, signed, Int32.of_int off, Int32.of_int len), a1, a2)

          | Fir.PlusFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (PlusOp ac, a1, a2)
          | Fir.MinusFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (MinusOp ac, a1, a2)
          | Fir.MulFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (MulOp ac, a1, a2)
          | Fir.DivFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (DivOp ac, a1, a2)
          | Fir.RemFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (RemOp ac, a1, a2)
          | Fir.MinFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (MinOp ac, a1, a2)
          | Fir.MaxFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (MaxOp ac, a1, a2)
          | Fir.ATan2FloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (ATan2Op pre, a1, a2)

            (* Comparisons *)
          | Fir.EqIntOp ->
               AtomBinop (EqOp ACInt, a1, a2)
          | Fir.NeqIntOp ->
               AtomBinop (NeqOp ACInt, a1, a2)
          | Fir.LtIntOp ->
               AtomBinop (LtOp ACInt, a1, a2)
          | Fir.LeIntOp ->
               AtomBinop (LeOp ACInt, a1, a2)
          | Fir.GtIntOp ->
               AtomBinop (GtOp ACInt, a1, a2)
          | Fir.GeIntOp ->
               AtomBinop (GeOp ACInt, a1, a2)
          | Fir.CmpIntOp ->
               AtomBinop (CmpOp ACInt, a1, a2)

          | Fir.EqRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (EqOp ac, a1, a2)
          | Fir.NeqRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (NeqOp ac, a1, a2)
          | Fir.LtRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (LtOp ac, a1, a2)
          | Fir.LeRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (LeOp ac, a1, a2)
          | Fir.GtRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (GtOp ac, a1, a2)
          | Fir.GeRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (GeOp ac, a1, a2)
          | Fir.CmpRawIntOp (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
                  AtomBinop (CmpOp ac, a1, a2)

          | Fir.EqFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (EqOp ac, a1, a2)
          | Fir.NeqFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (NeqOp ac, a1, a2)
          | Fir.LtFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (LtOp ac, a1, a2)
          | Fir.LeFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (LeOp ac, a1, a2)
          | Fir.GtFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (GtOp ac, a1, a2)
          | Fir.GeFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (GeOp ac, a1, a2)
          | Fir.CmpFloatOp pre ->
               let ac = ACFloat pre in
                  AtomBinop (CmpOp ac, a1, a2)

          | Fir.EqEqOp _ ->
               mir_eq_eq_atom tenv venv pos a1 a2
          | Fir.NeqEqOp _ ->
               mir_neq_eq_atom tenv venv pos a1 a2
          | Fir.PlusPointerOp (sub, pre, signed) ->
               let ptrty = ptrty_of_sub_block sub in
               let ty = type_of_atom venv pos a1 in
               let ac = atom_class_of_type tenv pos ty in
                  AtomBinop (PlusPointerOp ptrty, a1, a2)

          | Fir.PowerFloatOp _
          | Fir.LdExpFloatIntOp _ ->
               raise (FirException (pos, StringError ("binary operator not implemented: " ^ Fir_print.string_of_binop op)))


   (*
    * Atom should be a constant.
    * Add 1 to it.
    *)
   let succ_atom_index tenv venv rttd frames funs pos a =
      let pos = string_pos "succ_atom" pos in
         match a with
            AtomInt i ->
               AtomInt (succ i * sizeof_pointer)
          | AtomRawInt i ->
               AtomRawInt (Rawint.mul (Rawint.succ i) sizeof_pointer_raw)
          | a ->
               AtomBinop (PlusOp ac_native_int, a, AtomRawInt (Rawint.of_int Rawint.Int32 true 1))


   (*
    * Memory atom is translated through the pointer table.
    *)
   let mir_memory_atom tenv venv rttd frames funs pos ty a =
      let pos = string_pos "mir_memory_atom" pos in
      let pos = error_pos (StringAtomError ("atom", a)) pos in
      let a = mir_atom tenv venv rttd frames funs pos a in
      let index_ac = ACRawInt (precision_native_int, unsigned_int) in
      let ac = atom_class_of_atom a in
         if is_fun_type_dir tenv pos ty then
            let funty = funty_of_function_type tenv pos ty in
               AtomUnop (MemFunIndexOp (funty, index_ac), a)
         else if is_pointer_type_dir tenv pos ty then
            match ac with
               ACPointer ptrty ->
                  AtomUnop (MemIndexOp (ptrty, index_ac), a)
             | ACPoly ->
                  (* BUG: not necessarily a PtrBlock *)
                  AtomUnop (MemIndexOp (PtrBlock, index_ac),
                            AtomUnop (PointerOfPolyOp PtrBlock, a))
             | _ ->
                  let str = "Non-pointer atom used with pointer type" in
                  raise (FirException (pos, StringTypeError (str, ty)))
         else
            a


   (*
    * Check to see if the atom needs to be implicitly coerced between
    * a Poly type and a normal (nonpoly) type.  WARNING: these coercions
    * are not safe!
    *)
   let mir_coerce_poly pos ac a =
      let pos = string_pos "mir_coerce_poly" pos in
      let exn_fun pos msg = FirException (pos, InternalError msg) in
         coerce_poly exn_fun pos ac a


   (*
    * Translate an expression.
    *)
   let rec mir_expr tenv venv rttd frames funs e =
      let pos = string_pos "mir_expr" (exp_pos e) in
      let loc = Fir_ds.loc_of_exp e in
      let e' =
         match Fir_ds.dest_exp_core e with
            Fir.LetAtom (v, ty, a, e) ->
               mir_atom_expr tenv venv rttd frames funs pos loc v ty a e
          | Fir.LetExt (v, ty, s, false, ty2, ty_args, args, e) ->
               mir_ext_expr tenv venv rttd frames funs pos loc v ty s ty2 ty_args args e
          | Fir.LetExt (v, ty, s, true, ty2, ty_args, args, e) ->
               mir_ext_reserve_expr tenv venv rttd frames funs pos loc v ty s ty2 ty_args args e
          | Fir.TailCall (_, f, args) ->
               mir_tailcall_expr tenv venv rttd frames funs pos loc f args
          | Fir.SpecialCall (_, op) ->
               mir_specialcall_expr tenv venv rttd frames funs pos loc op
          | Fir.Match (a, cases) ->
               mir_match_expr tenv venv rttd frames funs pos loc a cases
          | Fir.MatchDTuple (a, cases) ->
               mir_match_dtuple_expr tenv venv rttd frames funs pos loc a cases
          | Fir.TypeCase (a1, a2, name, v, e1, e2) ->
               mir_typecase_expr tenv venv rttd frames funs pos loc a1 a2 name v e1 e2
          | Fir.LetAlloc (v, op, e) ->
               mir_alloc_expr tenv venv rttd frames funs pos loc v op e
          | Fir.LetSubscript (subop, v, ty, v2, a, e) ->
               mir_subscript_expr tenv venv rttd frames funs pos loc GenStandard subop v ty v2 a e
          | Fir.SetSubscript (subop, _, v, a1, ty, a2, e) ->
               mir_set_subscript_expr tenv venv rttd frames funs pos loc subop v a1 ty a2 e
          | Fir.LetGlobal (subop, v, ty, l, e) ->
               mir_global_expr tenv venv rttd frames funs pos loc subop v ty l e
          | Fir.SetGlobal (subop, _, v, ty, a, e) ->
               mir_set_global_expr tenv venv rttd frames funs pos loc subop v ty a e
          | Fir.Memcpy (subop, _, dst_base, dst_off, src_base, src_off, size, e) ->
               mir_memcpy_expr tenv venv rttd frames funs pos loc subop dst_base dst_off src_base src_off size e
          | Fir.Assert (label, pred, e) ->
               mir_assert_expr tenv venv rttd frames funs pos loc label pred e
          | Fir.Call (_, _, _, e) ->
               mir_expr tenv venv rttd frames funs e
          | Fir.Debug (info, e) ->
               mir_debug_expr tenv venv rttd frames funs pos loc info e
      in
         make_exp loc (CommentFIR (e, e'))


   and mir_atom_expr tenv venv rttd frames funs pos loc v ty a e =
      let pos = string_pos "mir_atom_expr" pos in
      let a = mir_atom tenv venv rttd frames funs pos a in
      let venv = venv_add venv v ty in
      let e = mir_expr tenv venv rttd frames funs e in
      let ac = atom_class_of_type tenv pos ty in
         make_exp loc (LetAtom (v, ac, a, e))


   and mir_ext_expr tenv venv rttd frames funs pos loc v ty s ty2 ty_args args e =
      let pos = string_pos "mir_op_expr" pos in
      let args = List.map (mir_atom tenv venv rttd frames funs pos) args in
      let venv = venv_add venv v ty in
      let e = mir_expr tenv venv rttd frames funs e in
      let ac = atom_class_of_type tenv pos ty in
         make_exp loc (LetExternal (v, ac, s, args, e))

   and mir_ext_reserve_expr tenv venv rttd frames funs pos loc v ty s ty2 ty_args args e =
      let pos = string_pos "mir_op_expr" pos in
      let args = List.map (mir_atom tenv venv rttd frames funs pos) args in
      let venv = venv_add venv v ty in
      let e = mir_expr tenv venv rttd frames funs e in
      let ac = atom_class_of_type tenv pos ty in
      let zero = AtomRawInt (Rawint.of_int Rawint.Int32 false 0) in
      let rinfo = new_symbol_string "ext_reserve", [], zero, zero in
         make_exp loc (LetExternalReserve (rinfo, v, ac, s, args, e))


   (*
    * Translate a tailcall.
    *)
   and mir_tailcall_expr tenv venv rttd frames funs pos loc a args =
      let pos = string_pos "mir_tailcall_expr" pos in
      let f = mir_atom tenv venv rttd frames funs pos a in
      let f =
         match f with
            AtomVar (_, f)
          | AtomFunVar (_, f) ->
               f
          | _ ->
               raise (FirException (pos, StringAtomError ("function is not a variable", a)))
      in
      let tyfun = venv_lookup venv pos f in
      let _, tyargs, _ = Fir_type.dest_all_fun_type_dir tenv pos tyfun in
      let iacargs = List.map (atom_class_of_type tenv pos) tyargs in
      let coerce_arg iac a =
         let a = mir_atom tenv venv rttd frames funs pos a in
         let a = mir_coerce_poly pos iac a in
            a
      in (* end coerce_arg *)
      let args = List.map2 coerce_arg iacargs args in
      let op =
         if SymbolTable.mem funs f then
            DirectCall
         else
            IndirectCall
      in
         make_exp loc (TailCall (op, f, args))


   (*
    * Translate a SpecialCall.  This is a verbatim translation.
    *)
   and mir_specialcall_expr tenv venv rttd frames funs pos loc op =
      let pos = string_pos "mir_tailcall_expr" pos in
      let coerce_arg pos ac a =
         let a = mir_atom tenv venv rttd frames funs pos a in
         let a = mir_coerce_poly pos ac a in
            a
      in (* end coerce_arg *)
      let resolve_function_name a =
         let pos = string_pos "resolve_function_name" pos in
         let f = mir_atom tenv venv rttd frames funs pos a in
            match f with
               AtomVar (_, f)
             | AtomFunVar (_, f) ->
                  f
             | _ ->
                  raise (FirException (pos, StringAtomError ("function is not a variable", a)))
      in (* end resolve_function_name *)
      let op =
         match op with
            Fir.TailSysMigrate (label, loc_base, loc_off, f, args) ->
               let pos = string_pos "TailSysMigrate" pos in
               let f = resolve_function_name f in
               let tyfun = venv_lookup venv pos f in
               let _, tyargs, _ = Fir_type.dest_all_fun_type_dir tenv pos tyfun in
               let acargs = List.map (atom_class_of_type tenv pos) tyargs in
               let args = List.map2 (coerce_arg pos) acargs args in
               let loc_base = mir_atom tenv venv rttd frames funs pos loc_base in
               let loc_off  = mir_atom tenv venv rttd frames funs pos loc_off in
                  TailSysMigrate (label, loc_base, loc_off, f, args)
          | Fir.TailAtomic (f, i, args) ->
               let pos = string_pos "TailAtomic" pos in
               let f = resolve_function_name f in
               let tyfun = venv_lookup venv pos f in
               let _, tyargs, _ = Fir_type.dest_all_fun_type_dir tenv pos tyfun in
               let acargs = List.map (atom_class_of_type tenv pos) tyargs in
               (* Strip off the first argument of f; this is the int argument *)
               let acargs = List.tl acargs in
               let args = List.map2 (coerce_arg pos) acargs args in
               let i = mir_atom tenv venv rttd frames funs pos i in
                  TailAtomic (f, i, args)
          | Fir.TailAtomicRollback (level, i) ->
               let pos = string_pos "TailAtomicRollback" pos in
               let level = mir_atom tenv venv rttd frames funs pos level in
               let i = mir_atom tenv venv rttd frames funs pos i in
                  TailAtomicRollback (level, i)
          | Fir.TailAtomicCommit (level, f, args) ->
               let pos = string_pos "TailAtomicCommit" pos in
               let f = resolve_function_name f in
               let level = mir_atom tenv venv rttd frames funs pos level in
               let tyfun = venv_lookup venv pos f in
               let _, tyargs, _ = Fir_type.dest_all_fun_type_dir tenv pos tyfun in
               let acargs = List.map (atom_class_of_type tenv pos) tyargs in
               let args = List.map2 (coerce_arg pos) acargs args in
                  TailAtomicCommit (level, f, args)
      in
         make_exp loc (SpecialCall op)


   (************************************************
    * MATCH
    ************************************************)


   (*
    * Translate a conditional.
    *)
   and mir_match_expr tenv venv rttd frames funs pos loc a cases =
      let pos = string_pos "mir_match_expr" pos in
      let a = mir_atom tenv venv rttd frames funs pos a in
      let ty = type_of_atom venv pos a in
      let iac = atom_class_of_type tenv pos ty in
         match iac with
            ACInt ->
               mir_match_normal_cases tenv venv rttd frames funs pos loc a ACInt cases
          | ACRawInt (pre, signed) ->
               let ac = ACRawInt (pre, signed) in
               let cases = translate_raw_cases pre signed cases in
                  mir_match_normal_cases tenv venv rttd frames funs pos loc a ac cases
          | ACFloat pre ->
               mir_match_normal_cases tenv venv rttd frames funs pos loc a (ACFloat pre) cases
          | ACPointer PtrBlock ->
               mir_match_tag_cases tenv venv rttd frames funs pos loc a ty cases
          | ACPointerInfix _ ->
               raise (FirException (pos, StringError "mir_match_expr: can't match an infix pointer"))
          | ACPointer PtrAggr ->
               raise (FirException (pos, StringError "mir_match_expr: can't match an aggregate"))
          | ACFunction _ ->
               raise (FirException (pos, StringError "mir_match_expr: can't match a function"))
          | ACPoly ->
               raise (FirException (pos, StringError "mir_match_expr: can't match a polymorphic type"))

   (*
    * Match a normal case.
    *)
   and mir_match_normal_cases tenv venv rttd frames funs pos loc a ac cases =
      let pos = string_pos "mir_match_normal_cases" pos in
      let cases = mir_normal_cases tenv venv rttd frames funs pos loc cases in
         mir_match_normal loc a ac cases

   (*
    * Translation a tag match.
    * Separate the case where the argument is a union.
    *)
   and mir_match_tag_cases tenv venv rttd frames funs pos loc a ty cases =
      let pos = string_pos "mir_match_tag_cases" pos in
         match tenv_expand_dir tenv pos ty with
            Fir.TyUnion (ty_v, _, _) ->
               mir_match_union tenv venv rttd frames funs pos loc a ty_v cases
          | _ ->
               let cases = mir_normal_cases tenv venv rttd frames funs pos loc cases in
                  mir_match_tag loc a cases

   (*
    * Match normal cases.
    *)
   and mir_match_normal loc a ac cases =
      make_exp loc (Match (a, ac, cases))

   (*
    * Match on the tag.
    *)
   and mir_match_tag loc a cases =
      let v = new_symbol_string "match" in
      let e = make_exp loc (Match (AtomVar (ac_tag, v), ac_tag, cases)) in
         make_exp loc (LetAtom (v, ac_tag, AtomUnop (MemTagOp (PtrBlock, ac_tag), a), e))

   (*
    * Translate a union match.
    * We have to be careful here because the values
    * may include integers.
    *)
   and mir_match_union tenv venv rttd frames funs pos loc a ty_v cases =
      let pos = string_pos "mir_match_union" pos in
      let tyll =
         match tenv_lookup tenv pos ty_v with
            Fir.TyDefUnion (_, tyll) ->
               tyll
          | Fir.TyDefLambda _
          | Fir.TyDefDTuple _ ->
               raise (FirException (pos, StringError "not a union"))
      in
      let trans = make_union_trans tyll in
      let icases, tcases = mir_mixed_cases tenv venv rttd frames funs pos loc trans cases in
         if debug debug_mir && icases = [] && tcases = [] then
            raise (FirException (pos, StringError "mir_match_union: empty match"));
         if icases = [] then
            mir_match_tag loc a tcases
         else if tcases = [] then
            mir_match_normal loc a ac_tag icases
         else
            let e = mir_match_tag loc a tcases in
            let case = RawIntSet (RawIntSet.max_set precision_int signed_int), e in
            let icases = icases @ [case] in
               mir_match_normal loc a ac_native_int icases

   (*
    * Convert normal cases.
    *)
   and mir_normal_cases tenv venv rttd frames funs pos loc cases =
      let pos = string_pos "mir_normal_cases" pos in
      let cases =
         List.fold_left (fun cases (_, s, e) ->
            let s =
               match s with
                  Fir.IntSet s ->
                     IntSet s
                | Fir.RawIntSet s ->
                     RawIntSet s
            in
            let e = mir_expr tenv venv rttd frames funs e in
            let cases = (s, e) :: cases in
               cases) [] cases
      in
         List.rev cases

   (*
    * Mixed case translation.
    *)
   and mir_mixed_cases tenv venv rttd frames funs pos loc trans cases =
      let pos = string_pos "mir_mixed_cases" pos in

      (* Set of each type *)
      let i_set, t_set = index_sets_of_trans trans in
      let _ =
         if debug debug_mir then
            Format.eprintf "@[<hv 3>mir_mixed_cases:@ @[<hv 3>int set:@ %a@]@ @[<hv 3>tag set:@ %a@]@]@." (**)
               pp_print_int_set i_set
               pp_print_int_set t_set
      in

      (* Split the cases *)
      let split_cases icases tcases s e =
         let s =
            match s with
               Fir.IntSet s ->
                  s
             | Fir.RawIntSet s ->
                  raise (FirException (pos, StringError "match a union with RawIntSet"))
         in
         let iset = IntSet.isect s i_set in
         let tset = IntSet.isect s t_set in
         let _ =
            if debug debug_mir then
               Format.eprintf "@[<hv 3>mir_mixed_cases, case:@ @[<hv 3>set:@ %a@]@ @[<hv 3>int set:@ %a@]@ @[<hv 3>tag set:@ %a@]@]@." (**)
                  pp_print_int_set s
                  pp_print_int_set iset
                  pp_print_int_set tset
         in
         let icases =
            if IntSet.is_empty iset then
               icases
            else
               (translate_set trans iset, e) :: icases
         in
         let tcases =
            if IntSet.is_empty tset then
               tcases
            else
               (translate_set trans tset, e) :: tcases
         in
            icases, tcases
      in

      (* Collect the cases of each type *)
      let rec collect icases tcases cases =
         match cases with
            (_, s, e) :: cases ->
               let e = mir_expr tenv venv rttd frames funs e in
               let icases, tcases = split_cases icases tcases s e in
                  collect icases tcases cases
          | [] ->
               List.rev icases, List.rev tcases
      in
         collect [] [] cases

   (************************************************
    * DTUPLE MATCHING
    ************************************************)

   (*
    * Fetch the block tag, and test against the
    * tags listed in the cases.
    *)
   and mir_match_dtuple_expr tenv venv rttd frames funs pos loc a cases =
      let pos = string_pos "mir_match_dtuple_expr" pos in
      let a = mir_atom tenv venv rttd frames funs pos a in
      let zero = AtomRawInt (Rawint.of_int Rawint.Int32 false 0) in
      let a_tag = AtomBinop (MemOp (ac_native_int, PtrBlock), a, zero) in
      let v = new_symbol_string "tag" in
      let a = AtomVar (ac_native_int, v) in
      let e = mir_match_dtuple_cases tenv venv rttd frames funs pos loc a cases in
      let e = make_exp loc (LetAtom (v, ac_native_int, a_tag, e)) in
         if Fir_state.mir_safety_enabled () then
            let check_name = new_symbol_string "match_dtuple_check" in
            let sizeof_int = AtomRawInt (Rawint.of_int Rawint.Int32 false sizeof_int) in
               make_exp loc (UpperBoundsCheck (check_name, PtrBlock, a, sizeof_int, e))
         else
            e


   (*
    * Build the code to match against the tags.
    *)
   and mir_match_dtuple_cases tenv venv rttd frames funs pos loc a cases =
      let pos = string_pos "mir_match_dtuple_cases" pos in
         match cases with
            (_, Some a1, e1) :: cases ->
               let e2 = mir_match_dtuple_cases tenv venv rttd frames funs pos loc a cases in
               let a1 = mir_atom tenv venv rttd frames funs pos a1 in
               let e1 = mir_expr tenv venv rttd frames funs e1 in
                  make_exp loc (IfThenElse (REqOp ac_native_int, a1, a, e1, e2))
          | (_, None, e) :: cases ->
               if cases <> [] then
                  eprintf "Warning: usless MatchDTuple cases@.";
               mir_expr tenv venv rttd frames funs e
          | [] ->
               raise (FirException (pos, StringError "incomplete MatchDTuple cases"))

   (************************************************
    * TYPECASE
    ************************************************)

   (*
    * Pass along the typecase to the back-end.
    *)
   and mir_typecase_expr tenv venv rttd frames funs pos loc obj names name v e1 e2 =
      let obj = mir_atom tenv venv rttd frames funs pos obj in
      let names = mir_atom tenv venv rttd frames funs pos names in
      let name = mir_var tenv venv rttd frames funs pos name in
      let e1 = mir_expr tenv venv rttd frames funs e1 in
      let e2 = mir_expr tenv venv rttd frames funs e2 in
         make_exp loc (IfType (obj, names, name, v, e1, e2))

   (************************************************
    * ALLOCATION
    ************************************************)

   (*
    * Translate an allocation.
    *)
   and mir_alloc_op tenv venv rttd frames funs pos loc op =
      let pos = string_pos "mir_alloc_op" pos in
      match op with
         Fir.AllocUnion (_, ty, ty_v, i, args) ->
            let index = index_of_const tenv pos ty_v i in
            let tyl = types_of_const tenv pos ty ty_v i in
            let args = List.map2 (mir_memory_atom tenv venv rttd frames funs pos) tyl args in
            let op = AllocUnion (index, args) in
               ty, op
       | Fir.AllocTuple (tclass, _, ty, args) ->
            let _, ty = dest_exists_type_dir tenv pos ty in
            let _, ty = dest_all_type_dir tenv pos ty in
            let ty = unfold_object_type_dir tenv pos ty in
            let tyl = dest_tuple_type_dir tenv pos ty in
            let tyl = List.map fst tyl in
            let args = List.map2 (mir_memory_atom tenv venv rttd frames funs pos) tyl args in
            let op =
               match tclass with
                  Fir.NormalTuple
                | Fir.BoxTuple ->
                     AllocTuple args
                | Fir.RawTuple ->
                     let v = Rttd.find rttd tenv pos tyl in
                     let args = AtomVar (ac_block_pointer, v) :: args in
                        AllocUnion (rttd_tag, args)
            in
               ty, op
       | Fir.AllocDTuple (ty, ty_var, a, args) ->
            let fields =
               match dest_dtuple_type_dir tenv pos ty with
                  _, Some tyl -> tyl
                | _, None -> raise (FirException (pos, StringError "AllocDTuple has incomplete type"))
            in
            let tyl = List.map fst fields in
            let tyl = Fir.TyTag (ty_var, fields) :: tyl in
            let args = a :: args in
            let args = List.map2 (mir_memory_atom tenv venv rttd frames funs pos) tyl args in
            let op = AllocTuple args in
               ty, op
       | Fir.AllocArray (ty, args) ->
            let ty = dest_array_type_dir tenv pos ty in
            let args = List.map (mir_memory_atom tenv venv rttd frames funs pos ty) args in
            let op = AllocArray args in
               Fir.TyArray ty, op
       | Fir.AllocVArray (ty, _, a1, a2) ->
            let ty = dest_array_type_dir tenv pos ty in
            let args = [mir_memory_atom tenv venv rttd frames funs pos ty a1] in
            let a = mir_memory_atom tenv venv rttd frames funs pos ty a2 in
            let op = AllocMArray (args, a) in
               ty, op
       | Fir.AllocMalloc (_, Fir.AtomRawInt size) ->
            let size = Rawint.to_int size in
            let size = align_size size in
            let size = Rawint.of_int precision_int signed_int size in
            let ty = Fir.TyRawData in
            let op = AllocMalloc (AtomRawInt size) in
               ty, op
       | Fir.AllocMalloc (_, Fir.AtomInt size) ->
            let size = align_size size in
            let size = Rawint.of_int precision_int signed_int size in
            let ty = Fir.TyRawData in
            let op = AllocMalloc (AtomRawInt size) in
               ty, op
       | Fir.AllocMalloc (_, a) ->
            let ty = Fir.TyRawData in
            (* TEMP:  ATOM IS *NOT* ALIGNED HERE, is this correct?  JDS *)
            let op = AllocMalloc (mir_atom tenv venv rttd frames funs pos a) in
               ty, op
       | Fir.AllocFrame (v, _) ->
            let ty = Fir.TyRawData in
            let size = sizeof_frame frames pos v in
            let size = align_size size in
            let size = AtomRawInt (Rawint.of_int precision_subscript signed_subscript size) in
            let op = AllocMalloc size in
               ty, op

   and mir_alloc_expr tenv venv rttd frames funs pos loc v op e =
      let pos = string_pos "mir_alloc_expr" pos in
      let ty, op = mir_alloc_op tenv venv rttd frames funs pos loc op in
      let venv = venv_add venv v ty in
      let e = mir_expr tenv venv rttd frames funs e in
      let label = new_symbol_string "mir_alloc_expr" in
      let acop = atom_class_of_alloc_op op in
      let e =
         make_exp loc (LetAlloc (v, op,
         make_exp loc (PrintDebug (label, Frame.debug_labels.(debug_alloc), [AtomVar (acop, v)],
         e))))
      in
         e

   (************************************************
    * SUBSCRIPT
    ************************************************)


   (***  Bounds Check Utility Functions  ***)


   (*
    * Emit code to check that a subscript is in bounds.  This
    * assumes the stop offset has already been computed.  The
    * check WILL be emitted if this is called.
    *)
   and mir_check_bounds pos loc subop ptr_base off_start off_stop e =
      let pos = string_pos "mir_check_bounds" pos in
      let check_name = new_symbol_string "check_block_bounds" in
      let build ptrty off_start off_stop =
         let e =
            match off_start, off_stop with
               Some off_start, Some off_stop ->
                  BoundsCheck (check_name, ptrty, ptr_base, off_start, off_stop, e)
             | Some off_start, None ->
                  LowerBoundsCheck (check_name, ptrty, ptr_base, off_start, e)
             | None, Some off_stop ->
                  UpperBoundsCheck (check_name, ptrty, ptr_base, off_stop, e)
             | None, None ->
                  raise (FirException (pos, InternalError "Neither lower nor upper bounds were specified"))
         in
            make_exp loc e
      in
      let { Fir.sub_block = sblock;
            Fir.sub_index = sindex;
            Fir.sub_script = sscript
          } = subop
      in
         match sblock, sindex, sscript with
            Fir.BlockSub,  Fir.ByteIndex, Fir.RawIntIndex _ ->
               build PtrBlock off_start off_stop
          | Fir.RawDataSub,Fir.ByteIndex, Fir.RawIntIndex _ ->
               build PtrAggr off_start off_stop
          | Fir.TupleSub,  Fir.ByteIndex, Fir.RawIntIndex _ ->
               build PtrBlock off_start off_stop
          | _ ->
               (* Unrecognized subop for boundschecks *)
               raise (FirException (pos, InternalError "Invalid subop given to mir_check_bounds"))


   (*
    * WARNING:  This is part of the OLD BoundsCheck core.
    * WARNING:  This function is deprecated.
    * Check that a block subscript is in bounds.
    * This also does the subscript multiplication.
    *)
   and mir_check_bounds_block pos loc subop generator ptr_base ptr_offset value_size cont =
      let pos = string_pos "mir_check_bounds_block" pos in
      let { Fir.sub_index = sindex;
            Fir.sub_script = sscript; } = subop
      in
         match sindex, sscript with
            Fir.WordIndex, Fir.IntIndex ->
               (* Compute the proper offset to give the continuation *)
               let a_toraw = AtomUnop (RawIntOfIntOp (precision_native_int, unsigned_int),
                                       ptr_offset)
               in
               let a_offset = AtomBinop (MulOp ac_native_unsigned,
                                         a_toraw, AtomRawInt sizeof_pointer_raw)
               in
                  if Fir_state.mir_safety_enabled () && generator = GenStandard then
                     (* Note: ptr_offset is a WORD offset.  The size in ptr_base
                        is in number of WORDS.  value_size is number of WORDS.
                        Note also that the offset is an ML int, and the cont
                        requires offset to be in number of bytes.  *)
                     let v_start = new_symbol_string "start" in
                     let a_start = AtomVar (ac_native_unsigned, v_start) in
                     let check_name = new_symbol_string "check_block_bounds" in
                        make_exp loc (LetAtom (v_start, ac_native_unsigned, a_toraw,
                        make_exp loc (BoundsCheck (check_name, PtrBlock, ptr_base, a_start,
                                     AtomBinop (PlusOp ac_native_unsigned, a_start, value_size),
                                     cont ptr_base a_offset))))
                  else
                     (* MIR safety-check generation has been disabled. *)
                     cont ptr_base a_offset
          | _ ->
               raise (FirException (pos, InternalError "Invalid subop given to mir_check_bounds_block"))


   (*
    * WARNING:  This is part of the OLD BoundsCheck core.
    * WARNING:  This function is deprecated.
    * Check that a tuple subscript is in bounds.
    * This also does the subscript multiplication.
    *)
   and mir_check_bounds_tuple pos loc subop generator ptr_base ptr_offset value_size cont =
      let pos = string_pos "mir_check_bounds_tuple" pos in
      let { Fir.sub_index = sindex;
            Fir.sub_script = sscript; } = subop
      in
         match sindex, sscript with
            Fir.WordIndex, Fir.RawIntIndex _ ->
               (* Compute the proper offset to give the continuation *)
               let a_offset = AtomBinop (MulOp ac_native_unsigned,
                                         ptr_offset, AtomRawInt sizeof_pointer_raw)
               in
                  if Fir_state.mir_safety_enabled () && generator = GenStandard then
                     (* Note: ptr_offset is a WORD offset.  The size in ptr_base
                        is in number of WORDS.  value_size is number of WORDS.
                        Note also that the offset is raw int32, and the cont
                        requires offset to be in number of bytes.  *)
                     let check_name = new_symbol_string "check_tuple_bounds" in
                        make_exp loc (BoundsCheck (check_name, PtrBlock, ptr_base, ptr_offset,
                                     AtomBinop (PlusOp ac_native_unsigned, ptr_offset, value_size),
                                     cont ptr_base a_offset))
                  else
                     (* MIR safety-check generation has been disabled. *)
                     cont ptr_base a_offset
          | _ ->
               raise (FirException (pos, InternalError "Invalid subop given to mir_check_bounds_tuple"))


   (*
    * WARNING:  This is part of the OLD BoundsCheck core.
    * WARNING:  This function is deprecated.
    * Check that a raw subscript is in bounds.
    *)
   and mir_check_bounds_raw pos loc subop generator ptr_base ptr_offset value_size cont =
      let pos = string_pos "mir_check_bounds_raw" pos in
      let { Fir.sub_index = sindex;
            Fir.sub_script = sscript; } = subop
      in
         match sindex, sscript with
            Fir.ByteIndex, Fir.RawIntIndex _ ->
               if Fir_state.mir_safety_enabled () && generator = GenStandard then
                  (* Note: ptr_offset is a BYTE offset.  The block size in ptr_base
                     is in number of BYTES.  value_size is number of BYTES.  There
                     is no translation required for the continuation.*)
                  let check_name = new_symbol_string "check_raw_bounds" in
                     make_exp loc (BoundsCheck (check_name, PtrAggr, ptr_base, ptr_offset,
                                  AtomBinop (PlusOp ac_native_unsigned, ptr_offset, value_size),
                                  cont ptr_base ptr_offset))
               else
                  (* MIR safety-check generation has been disabled. *)
                  cont ptr_base ptr_offset
          | _ ->
               raise (FirException (pos, InternalError "Invalid subop given to mir_check_bounds_raw"))


   (*
    * WARNING:  This is part of the OLD BoundsCheck core.
    * WARNING:  This function is deprecated.
    * This returns the size of a block subscript operation, in words.
    * This is the number of words that will be read from memory.
    *)
   and sizeof_block_subop pos loc subop =
      let pos = string_pos "sizeof_block_subop" pos in
      let roundup num den = (num + den - 1) / den in
         match subop with
            (* Block sizes are in words *)
            Fir.PolySub
          | Fir.EnumSub _
          | Fir.IntSub
          | Fir.TagSub _
          | Fir.BlockPointerSub
          | Fir.RawPointerSub
          | Fir.FunctionSub
          | Fir.RawIntSub (Rawint.Int8, _)
          | Fir.RawIntSub (Rawint.Int16, _)
          | Fir.RawIntSub (Rawint.Int32, _) ->
               roundup sizeof_int32 sizeof_pointer
          | Fir.RawIntSub (Rawint.Int64, _)
          | Fir.PointerInfixSub ->
               roundup sizeof_int64 sizeof_pointer
          | Fir.RawFloatSub Rawfloat.Single ->
               roundup sizeof_single sizeof_pointer
          | Fir.RawFloatSub Rawfloat.Double ->
               roundup sizeof_double sizeof_pointer
          | Fir.RawFloatSub Rawfloat.LongDouble ->
               roundup sizeof_long_double sizeof_pointer


   (*
    * WARNING:  This is part of the OLD BoundsCheck core.
    * WARNING:  This function is deprecated.
    * This returns the size of a raw subscript operation, in bytes.
    * This is the number of bytes that will be read from memory.
    *)
   and sizeof_raw_subop pos loc subop =
      let pos = string_pos "sizeof_raw_subop" pos in
         match subop with
            (* Raw sizes are in bytes *)
            Fir.RawIntSub (Rawint.Int8, _) ->
               sizeof_int8
          | Fir.RawIntSub (Rawint.Int16, _) ->
               sizeof_int16
          | Fir.RawIntSub (Rawint.Int32, _) ->
               sizeof_int32
          | Fir.RawIntSub (Rawint.Int64, _)
          | Fir.PointerInfixSub ->
               sizeof_int64
          | Fir.RawFloatSub Rawfloat.Single ->
               sizeof_single
          | Fir.RawFloatSub Rawfloat.Double ->
               sizeof_double
          | Fir.RawFloatSub Rawfloat.LongDouble ->
               sizeof_long_double
          | Fir.EnumSub _
          | Fir.IntSub
          | Fir.TagSub _
          | Fir.BlockPointerSub
          | Fir.RawPointerSub
          | Fir.FunctionSub
          | Fir.PolySub ->
               sizeof_pointer


   (***  LetSubscript Code  ***)


   (*
    * Load a value from a block of memory.  We have to perform
    * a bounds-check first, and also scale the subscript.  This
    * is a toplevel function; the generator parametre must be
    * one of the following:
    *    GenStandard    Converting a FIR LetSubscript to MIR.
    *                   Generates checks if mir_safety_enabled is
    *                   set, otherwise no checks.
    *    GenAssertPtr   Converting a FIR Assert for PointerOfIndex
    *                   or FunctionOfIndex cases.  Will always
    *                   drop the bounds check and always add the
    *                   pointer/function index checks.
    *)
   and mir_subscript_expr tenv venv rttd frames funs pos loc generator subop v ty ptr off e =
      let pos = string_pos "mir_subscript_expr" pos in
      let { Fir.sub_block = sblock;
            Fir.sub_value = valop
          } = subop
      in
         (* TEMP BUG: of course, we should use sub_index and sub_script as well. *)
         match sblock with
            Fir.BlockSub ->
               let pos = string_pos "mir_subscript_block_expr" pos in
               let ptr = mir_atom tenv venv rttd frames funs pos ptr in
               let off = mir_atom tenv venv rttd frames funs pos off in
               let size = sizeof_block_subop pos loc valop in
               let size = AtomRawInt (Rawint.of_int precision_native_int false size) in
                  mir_check_bounds_block pos loc subop generator ptr off size (fun ptr off ->
                     mir_subscript tenv venv rttd frames funs pos loc generator subop PtrBlock v ty ptr off e)
          | Fir.TupleSub ->
               let pos = string_pos "mir_subscript_rawtuple_expr" pos in
               let ptr = mir_atom tenv venv rttd frames funs pos ptr in
               let off = mir_atom tenv venv rttd frames funs pos off in
               let size = sizeof_block_subop pos loc valop in
               let size = AtomRawInt (Rawint.of_int precision_native_int false size) in
                  mir_check_bounds_tuple pos loc subop generator ptr off size (fun ptr off ->
                     mir_subscript tenv venv rttd frames funs pos loc generator subop PtrBlock v ty ptr off e)
          | Fir.RawTupleSub ->
               let pos = string_pos "mir_subscript_rawtuple_expr" pos in
               let ptr = mir_atom tenv venv rttd frames funs pos ptr in
               let off = mir_atom tenv venv rttd frames funs pos off in
               let off = succ_atom_index tenv venv rttd frames funs pos off in
                  (* TEMP BUG JDS:
                     This can't possibly be right.  a2 is a WORD offset, and
                     is being passed directly into a function wanting a BYTE
                     offset.  Jason, does this code actually work??  *)
                  mir_subscript tenv venv rttd frames funs pos loc generator subop PtrBlock v ty ptr off e
          | Fir.RawDataSub ->
               let pos = string_pos "mir_subscript_raw_expr" pos in
               let ptr = mir_atom tenv venv rttd frames funs pos ptr in
               let off = mir_atom tenv venv rttd frames funs pos off in
               let size = sizeof_raw_subop pos loc valop in
               let size = AtomRawInt (Rawint.of_int precision_native_int false size) in
                  mir_check_bounds_raw pos loc subop generator ptr off size (fun ptr off ->
                     mir_subscript tenv venv rttd frames funs pos loc generator subop PtrAggr v ty ptr off e)


   (*
    * Load a value from memory.  At this point, we can assume
    * the subscript is in bounds, and the offsets are in nice,
    * comfortable units of bytes.  We have to have special
    * cases for reading pointers.  For pointers, checks will
    * be emitted if the generator is GenAssertPtr, or if MIR
    * safety checks are enabled.
    *)
   and mir_subscript tenv venv rttd frames funs pos loc generator subop ptrty v ty ptr off e =
      let pos = string_pos "mir_subscript" pos in
      let venv = venv_add venv v ty in

      (* If destination is ACPoly, then apply a coercion here *)
      let coerce_result a =
         let e = mir_expr tenv venv rttd frames funs e in
         let ac = atom_class_of_type tenv pos ty in
         let a = mir_coerce_poly pos ac a in
            make_exp loc (LetAtom (v, ac, a, e))
      in (* end coerce_result *)

      let v = new_symbol v in
      let ac = atom_class_of_fir_subop tenv pos ty subop in
         match ptrty, ac with
            _, ACInt
          | _, ACRawInt _
          | _, ACFloat _ ->
               make_exp loc (LetAtom (v, ac, AtomBinop (MemOp (ac, ptrty), ptr, off),
                        coerce_result (AtomVar (ac, v))))
          | PtrAggr, ACPointer ptrty ->
               mir_subscript_aggr_pointer_expr loc generator ptrty v ptr off coerce_result
          | PtrAggr, ACPoly ->
               mir_subscript_aggr_pointer_expr loc generator PtrBlock v ptr off coerce_result
          | PtrAggr, ACPointerInfix ptrty ->
               mir_subscript_aggr_infix_pointer_expr loc generator ptrty v ptr off coerce_result
          | PtrAggr, ACFunction funty ->
               (* TEMP HACK *)
               let _, args, _ = dest_all_fun_type_dir tenv pos ty in
               let funty = FunArgs (List.map (atom_class_of_type tenv pos) args) in
                  mir_subscript_raw_function_expr loc generator funty v ptr off coerce_result
          | PtrBlock, ACPoly ->
               mir_subscript_block_poly_expr tenv venv rttd frames funs pos loc v ptr off coerce_result
          | PtrBlock, ACPointer ptrty ->
               mir_subscript_block_pointer_expr loc ptrty v ptr off coerce_result
          | PtrBlock, ACFunction funty ->
               mir_subscript_block_function_pointer_expr loc funty v ptr off coerce_result
          | _ ->
               let print_error buf =
                  fprintf buf "@[<hv 3>ptrty = %a;@ ac = %a@]" (**)
                     pp_print_ptrty ptrty
                     pp_print_atom_class ac
               in
                  raise (FirException (pos, StringFormatError ("mir_subscript: type error", print_error)))


   (*
    * Load a polymorphic value from memory.
    * In this case, we don't know if the value is
    * a pointer or not.  We punt the check.
    *)
   (* TEMP BUG JDS:  We need this to check for safety, don't we? *)
   and mir_subscript_block_poly_expr tenv venv rttd frames funs pos loc v_poly ptr off cont =
      let pos = string_pos "mir_subscript_block_poly_expr" pos in
      let ac = ACPoly in
      let e = cont (AtomVar (ac, v_poly)) in

      (* Print the pointer just loaded *)
      let e =
         let label = new_symbol_string "mir_subscript_block_poly_expr" in
            make_exp loc (PrintDebug (label, debug_labels.(Frame.debug_let_poly),
                        [ptr; off; AtomVar (ac, v_poly)],
                        e))
      in
      let label = new_symbol_string "mir_subscript_block_poly_expr" in
         (* Print info for debugging *)
         make_exp loc (PrintDebug (label, debug_labels.(debug_pre_let_poly), [ptr; off],

         (* First, get the value from memory *)
         make_exp loc (LetAtom (v_poly, ac,
                  AtomBinop (MemOp (ACPoly, PtrBlock), ptr, off),
                  e))))


   (*
    * Load a pointer from memory.
    * It is permitted to fetch a raw pointer from the block.
    *)
   and mir_subscript_block_pointer_expr loc ptrty v_pointer ptr off cont =
      let ac = ACPointer ptrty in
      let e = cont (AtomVar (ac, v_pointer)) in
      let a_value = AtomBinop (MemOp (ac_native_unsigned, PtrBlock), ptr, off) in
         make_exp loc (LetAtom (v_pointer, ac, AtomUnop (SafePointerOfIndexOp ptrty, a_value), e))


   (*
    * Load a function pointer from memory.
    *)
   and mir_subscript_block_function_pointer_expr loc funty v_pointer ptr off cont =
      let ac = ACFunction funty in
      let e = cont (AtomVar (ac, v_pointer)) in
      let a_value = AtomBinop (MemOp (ac_native_unsigned, PtrBlock), ptr, off) in
         make_exp loc (LetAtom (v_pointer, ac, AtomUnop (SafeFunctionOfIndexOp funty, a_value), e))


   (*
    * Load an aggregate pointer from memory.
    * Includes all relevant safety checks.
    *)
   and mir_subscript_aggr_pointer_expr loc generator ptrty v_pointer ptr off cont =
      let ac = ACPointer ptrty in
      let e = cont (AtomVar (ac, v_pointer)) in
      let a_value = AtomBinop (MemOp (ac_native_unsigned, PtrAggr), ptr, off) in
         if Fir_state.mir_safety_enabled () || generator = GenAssertPtr then
            let check_name = new_symbol_string "check_aggr_pointer_index" in
               make_exp loc (PointerIndexCheck (check_name, ptrty, v_pointer, ac, a_value, e))
         else
            (* MIR safety-check generation has been disabled. *)
            let a_value = AtomUnop (UnsafePointerOfIndexOp ptrty, a_value) in
               make_exp loc (LetAtom (v_pointer, ac, a_value, e))


   (*
    * Load an infix pointer from memory.
    * The infix pointer is stored in memory as a base/offset pair.
    *)
   and mir_subscript_aggr_infix_pointer_expr loc generator ptrty v_pointer ptr off cont =
      let ac_pointer = ACPointer ptrty in
      let v_base = new_symbol_string (Symbol.to_string v_pointer ^ "_base") in

      (* Add the offset to the pointer *)
      let offset_offset =
         AtomBinop (PlusOp ac_native_unsigned, off, AtomRawInt sizeof_pointer_raw)
      in
      let cont a =
         make_exp loc (LetAtom (v_pointer, ac_pointer,
                  AtomBinop (InfixOfBaseOffsetOp ptrty, a,
                             AtomBinop (MemOp (ac_native_unsigned, PtrAggr),
                                        ptr, offset_offset)),
                  cont (AtomVar (ac_pointer, v_pointer))))
      in
         (* Fetch the pointer *)
         mir_subscript_aggr_pointer_expr loc generator ptrty v_base ptr off cont


   (*
    * Load a function pointer from memory.
    *)
   and mir_subscript_raw_function_expr loc generator funty v_pointer ptr off cont =
      let ac = ACFunction funty in
      let e = cont (AtomVar (ac, v_pointer)) in
         let a_value = AtomBinop (MemOp (ac_native_unsigned, PtrAggr), ptr, off) in
         if Fir_state.mir_safety_enabled () || generator = GenAssertPtr then
            let check_name = new_symbol_string "check_function_index" in
               make_exp loc (FunctionIndexCheck (check_name, funty, v_pointer, ac, a_value, e))
         else
            (* MIR safety-check generation has been disabled. *)
            let a_value = AtomUnop (UnsafeFunctionOfIndexOp funty, a_value) in
               make_exp loc (LetAtom (v_pointer, ac, a_value, e))


   (***  SetSubscript code  ***)


   (*
    * Get a value from the globals.
    *)
   and mir_global_expr tenv venv rttd frames funs pos loc subop v ty l e =
      let pos = string_pos "mir_global_expr" pos in
      let ac = atom_class_of_fir_sub_value tenv pos ty subop in
      let a = mir_atom tenv venv rttd frames funs pos (Fir.AtomVar l) in
      let e = mir_expr tenv venv rttd frames funs e in
         make_exp loc (LetAtom (v, ac, a, e))


   (*
    * Store a value in a the globals.
    *)
   and mir_set_global_expr tenv venv rttd frames funs pos loc subop v ty a e =
      let pos = string_pos "mir_set_global_expr" pos in
      let ac = atom_class_of_fir_sub_value tenv pos ty subop in
      let a = mir_atom tenv venv rttd frames funs pos a in
      let e = mir_expr tenv venv rttd frames funs e in
         make_exp loc (SetGlobal (ac, v, a, e))


   (*
    * Store a value in a block.
    * Check the bounds first.
    *)
   and mir_set_subscript_expr tenv venv rttd frames funs pos loc subop v a1 ty a2 e =
      let pos = string_pos "mir_set_subscript_expr" pos in
      let { Fir.sub_block = sblock;
            Fir.sub_value = valop
          } = subop
      in
         match sblock with
            Fir.BlockSub ->
               let a3 = mir_atom tenv venv rttd frames funs pos a2 in
               let a2 = mir_atom tenv venv rttd frames funs pos a1 in
               let a1 = mir_atom tenv venv rttd frames funs pos v in
               let size = sizeof_block_subop pos loc valop in
               let size = AtomRawInt (Rawint.of_int precision_native_int false size) in
                  mir_check_bounds_block pos loc subop GenStandard a1 a2 size (fun a1 a2 ->
                        mir_set_subscript tenv venv rttd frames funs pos loc subop PtrBlock a1 a2 ty a3 e)
          | Fir.TupleSub ->
               let a3 = mir_atom tenv venv rttd frames funs pos a2 in
               let a2 = mir_atom tenv venv rttd frames funs pos a1 in
               let a1 = mir_atom tenv venv rttd frames funs pos v in
               let size = sizeof_block_subop pos loc valop in
               let size = AtomRawInt (Rawint.of_int precision_native_int false size) in
                  mir_check_bounds_tuple pos loc subop GenStandard a1 a2 size (fun a1 a2 ->
                        mir_set_subscript tenv venv rttd frames funs pos loc subop PtrBlock a1 a2 ty a3 e)
          | Fir.RawTupleSub ->
               let a3 = mir_atom tenv venv rttd frames funs pos a2 in
               let a2 = mir_atom tenv venv rttd frames funs pos a1 in
               let a2 = succ_atom_index tenv venv rttd frames funs pos a2 in
               let a1 = mir_atom tenv venv rttd frames funs pos v in
                  mir_set_subscript tenv venv rttd frames funs pos loc subop PtrBlock a1 a2 ty a3 e
          | Fir.RawDataSub ->
               let a3 = mir_atom tenv venv rttd frames funs pos a2 in
               let a2 = mir_atom tenv venv rttd frames funs pos a1 in
               let a1 = mir_atom tenv venv rttd frames funs pos v in
               let size = sizeof_raw_subop pos loc valop in
               let size = AtomRawInt (Rawint.of_int precision_native_int false size) in
                  mir_check_bounds_raw pos loc subop GenStandard a1 a2 size (fun a1 a2 ->
                        mir_set_subscript tenv venv rttd frames funs pos loc subop PtrAggr a1 a2 ty a3 e)


   (*
    * Store a value in memory.
    *)
   and mir_set_subscript tenv venv rttd frames funs pos loc subop ptrty a1 a2 ty a3 e =
      let pos = string_pos "mir_set_subscript_expr" pos in
      let e = mir_expr tenv venv rttd frames funs e in
      let ac = atom_class_of_fir_subop tenv pos ty subop in
      let a3 = mir_coerce_poly pos ac a3 in
         match ptrty, ac with
            _, ACInt
          | _, ACRawInt _
          | _, ACFloat _ ->
               make_exp loc (SetMem(ac, ptrty, a1, a2, a3, e))
          | PtrAggr, ACPointer ptrty ->
               mir_set_subscript_aggr_pointer_expr loc ptrty a1 a2 a3 e
          | PtrAggr, ACPoly ->
               mir_set_subscript_aggr_poly_expr pos loc a1 a2 a3 e
          | PtrAggr, ACPointerInfix ptrty ->
               mir_set_subscript_aggr_infix_pointer_expr loc ptrty a1 a2 a3 e
          | PtrAggr, ACFunction funty ->
               mir_set_subscript_raw_function_expr loc funty a1 a2 a3 e
          | PtrBlock, ACPointer PtrBlock ->
               mir_set_subscript_block_pointer_expr tenv venv rttd frames funs pos loc a1 a2 a3 e
          | PtrBlock, ACPoly ->
               mir_set_subscript_block_poly_expr tenv venv rttd frames funs pos loc a1 a2 a3 e
          | PtrBlock, ACFunction funty ->
               mir_set_subscript_block_function_expr tenv venv rttd frames funs pos loc funty a1 a2 a3 e
          | _ ->
               raise (FirException (pos, StringTypeError ("mir_set_subscript_expr: type error", ty)))


   (*
    * Store a polymorphic value into memory.
    * Again, we punt and let the arch handle it.
    *)
   and mir_set_subscript_block_poly_expr tenv venv rttd frames funs pos loc a1 a2 a3 e =
      let pos = string_pos "mir_set_subscript_block_poly_expr" pos in
      let label = new_symbol_string "mir_set_subscript_block_poly_expr" in
         make_exp loc (PrintDebug (label, Frame.debug_labels.(debug_set_poly), [a1; a2; a3],
         make_exp loc (SetMem (ACPoly, PtrBlock, a1, a2, a3, e))))


   (*
    * Store a pointer into memory.
    *)
   and mir_set_subscript_block_pointer_expr tenv venv rttd frames funs pos loc a1 a2 a3 e =
      let pos = string_pos "mir_set_subscript_block_pointer_expr" pos in
      let label = new_symbol_string "mir_set_subscript_block_pointer_expr" in
         make_exp loc (PrintDebug (label, Frame.debug_labels.(debug_set_pointer), [a1; a2; a3],
         make_exp loc (SetMem (ac_native_unsigned, PtrBlock, a1, a2,
                 AtomUnop (SafeIndexOfPointerOp PtrBlock, a3),
         e))))


   (*
    * Store a function into memory.
    * Every function to be stored has a tag.
    *)
   and mir_set_subscript_block_function_expr tenv venv rttd frames funs pos loc funty a1 a2 a3 e =
      let pos = string_pos "mir_set_subscript_block_function_expr" pos in
      let label = new_symbol_string "mir_set_subscript_block_function_expr" in
         make_exp loc (PrintDebug (label, Frame.debug_labels.(debug_set_function), [a1; a2; a3],
         make_exp loc (SetMem (ac_native_unsigned, PtrBlock, a1, a2,
                 AtomUnop (SafeIndexOfFunctionOp funty, a3),
         e))))


   (*
    * Store a pointer into memory.
    *)
   and mir_set_subscript_aggr_pointer_expr loc ptrty ptr_base ptr_off v e =
      make_exp loc (SetMem (ac_native_unsigned, PtrAggr, ptr_base, ptr_off,
              AtomUnop (UnsafeIndexOfPointerOp ptrty, v),
              e))


   (*
    * Store a pointer into memory.
    *)
   and mir_set_subscript_aggr_poly_expr pos loc ptr_base ptr_off a e =
      let pos = string_pos "mir_set_subscript_aggr_poly_expr" pos in
      let label = new_symbol_string "mir_set_subscript_aggr_poly_expr" in
         make_exp loc (PrintDebug (label, Frame.debug_labels.(debug_set_poly), [ptr_base; ptr_off; a],
         make_exp loc (SetMem (ACPoly, PtrAggr, ptr_base, ptr_off, a,
         e))))


   (*
    * Store an infix pointer.
    * Store it as a base/offset pair.
    *)
   and mir_set_subscript_aggr_infix_pointer_expr loc ptrty ptr_base ptr_off a e =
      let v_base = new_symbol_string "base" in
      let v_infix = new_symbol_string "off" in
      let ac_pointer = ACPointer ptrty in
      let ac_infix_pointer = ACPointerInfix ptrty in
      let offset_offset = AtomBinop (PlusOp ac_native_unsigned, ptr_off, AtomRawInt sizeof_pointer_raw) in
         make_exp loc (LetAtom (v_base, ac_pointer, AtomUnop (BaseOfInfixPointerOp ptrty, a),
         make_exp loc (LetAtom (v_infix, ac_native_unsigned, AtomUnop (OffsetOfInfixPointerOp ptrty, a),
         make_exp loc (SetMem (ac_native_unsigned, PtrAggr, ptr_base, ptr_off,
                 AtomUnop (UnsafeIndexOfPointerOp PtrAggr, AtomVar (ac_pointer, v_base)),
         make_exp loc (SetMem (ac_native_unsigned, PtrAggr, ptr_base, offset_offset,
                 AtomVar (ac_native_unsigned, v_infix),
         e))))))))


   (*
    * Store a function into memory.
    * Every function to be stored has a tag.
    *)
   and mir_set_subscript_raw_function_expr loc funty ptr_base ptr_off v e =
      make_exp loc (SetMem (ac_native_unsigned, PtrAggr, ptr_base, ptr_off,
              AtomUnop (UnsafeIndexOfFunctionOp funty, v),
              e))


   (***  Memory Copy  ***)


   (*
    * Copy memory.
    *)
   and mir_memcpy_expr tenv venv rttd frames funs pos loc subop v1 a1 v2 a2 a3 e =
      let pos = string_pos "mir_memcpy_expr" pos in
         match subop.Fir.sub_block with
            Fir.BlockSub
          | Fir.TupleSub
          | Fir.RawTupleSub ->
               mir_memcpy_block_expr tenv venv rttd frames funs pos loc subop v1 a1 v2 a2 a3 e
          | Fir.RawDataSub ->
               mir_memcpy_raw_expr tenv venv rttd frames funs pos loc subop v1 a1 v2 a2 a3 e

   and mir_memcpy_block_expr tenv venv rttd frames funs pos loc subop v1 a1 v2 a2 a3 e =
      let pos = string_pos "mir_memcpy_block_expr" pos in
      let v1 = mir_atom tenv venv rttd frames funs pos v1 in
      let a1 = mir_atom tenv venv rttd frames funs pos a1 in
      let v2 = mir_atom tenv venv rttd frames funs pos v2 in
      let a2 = mir_atom tenv venv rttd frames funs pos a2 in
      let a3 = mir_atom tenv venv rttd frames funs pos a3 in
      let a1_bounds = AtomBinop (PlusOp ACInt, a1, a3) in
      let a2_bounds = AtomBinop (PlusOp ACInt, a2, a3) in
         (* TEMP:  As far as I (JDS) can tell, this code is B.S..
            The units do not even remotely agree here; is a3 size
            in WORDS or BYTES here?  You use it as though it were
            words first, but memcpy expects BYTES. *)
         raise (Failure "mir_fir.ml: mir_memcpy_block_expr:  (BUG FIXME)  This code is BS.  Contact JDS.")
         (* BROKEN BUG FIXME
         mir_check_block_subscript v1 a1_bounds (fun v1 a1_bounds ->
            mir_check_block_subscript v2 a2_bounds (fun v2 a2_bounds ->
               mir_memcpy tenv venv rttd frames funs pos PtrBlock v1 a1 v2 a2 a3 e))
          *)

   and mir_memcpy_raw_expr tenv venv rttd frames funs pos loc subop dst_base dst_off src_base src_off size e =
      let pos = string_pos "mir_memcpy_block_expr" pos in
      let dst_base = mir_atom tenv venv rttd frames funs pos dst_base in
      let dst_off = mir_atom tenv venv rttd frames funs pos dst_off in
      let src_base = mir_atom tenv venv rttd frames funs pos src_base in
      let src_off = mir_atom tenv venv rttd frames funs pos src_off in
      let size = mir_atom tenv venv rttd frames funs pos size in
         (* Note: here size is in bytes, so everyone agrees on the units *)
         mir_check_bounds_raw pos loc subop GenStandard dst_base dst_off size (fun dst_base dst_off ->
            mir_check_bounds_raw pos loc subop GenStandard src_base src_off size (fun src_base src_off ->
               mir_memcpy tenv venv rttd frames funs pos loc PtrAggr dst_base dst_off src_base src_off size e))

   and mir_memcpy tenv venv rttd frames funs pos loc ptrty dst_base dst_off src_base src_off size e =
      let pos = string_pos "mir_set_subscript_expr" pos in
      let e = mir_expr tenv venv rttd frames funs e in
         make_exp loc (Memcpy (ptrty, dst_base, dst_off, src_base, src_off, size, e))


   (***  FIR Assertion Handling  ***)


   (*
    * FIR assertions
    *)
   and mir_assert_expr tenv venv rttd frames funs pos loc label pred e =
      let pos = string_pos "mir_assert_expr" pos in
         if Fir_state.fir_safety_enabled () then
            (* Translate the FIR Assert statement *)
            match pred with
               Fir.IsMutable v ->
                  raise (FirException (pos, NotImplemented "IsMutable FIR assertion not supported yet"))

             | Fir.Reserve (a1, a2) ->
                  (* TEMP BUG: These are currently dropped, and
                     regenerated by mir_memory.  Required due to
                     certain MIR assumptions I need to fix. -JDS *)
                  mir_expr tenv venv rttd frames funs e

             | Fir.ElementCheck (ty, subop, v, a) ->
                  (* Element type check *)
                  let pos = string_pos "Fir.ElementCheck" pos in
                  let v_ignored = new_symbol_string "ignored" in
                     mir_subscript_expr tenv venv rttd frames funs pos loc GenAssertPtr subop v_ignored ty v a e

             | Fir.BoundsCheck (subop, v, lower, upper) ->
                  (* Dual bounds check *)
                  let pos = string_pos "Fir.BoundsCheck" pos in
                  let ptr = mir_atom tenv venv rttd frames funs pos v in
                  let lower = mir_atom tenv venv rttd frames funs pos lower in
                  let upper = mir_atom tenv venv rttd frames funs pos upper in
                  let e = mir_expr tenv venv rttd frames funs e in
                     mir_check_bounds pos loc subop ptr (Some lower) (Some upper) e
         else
            (* Ignore FIR Assert statement *)
            mir_expr tenv venv rttd frames funs e


   (************************************************
    * OTHER EXPRESSIONS
    ************************************************)


   (*
    * Debug.
    *)
   and mir_debug_expr tenv venv rttd frames funs pos loc info e =
      let pos = string_pos "mir_debug_expr" pos in
      let e = mir_expr tenv venv rttd frames funs e in
         match info with
            Fir.DebugString _ ->
               e
          | Fir.DebugContext (line, vars) ->
               let info = line, vars in
                  make_exp loc (Debug (info, e))


   (************************************************************************
    * MAIN FUNCTIONS
    ************************************************************************)


   (*
    * Global function.
    *)
   let zero_loc = bogus_loc "<Mir_fir>"

   let compile_prog mfir =
      let { Fir.marshal_fir      = prog;
            Fir.marshal_globals  = global_order;
            Fir.marshal_funs     = fun_order;
            Fir.marshal_rttd     = rttd;
          } = mfir
      in

      (* Now convert the prog *)
      let { Fir.prog_file     = file_info;
            Fir.prog_import   = import;
            Fir.prog_export   = export;
            Fir.prog_types    = tenv;
            Fir.prog_names    = names;
            Fir.prog_globals  = globals;
            Fir.prog_funs     = funs;
          } = prog
      in
      let pos = var_exp_pos (Symbol.add "Mir_fir.compile_prog") in

      (* Get frame info *)
      let frames = frame_prog prog in

      (* Reconstruct file information *)
      let { Fir.file_dir = file_dir; Fir.file_name = file_name; Fir.file_class = fclass } = file_info in
      let file_info = { file_dir = file_dir; file_name = file_name; file_class = fclass } in

      (* Default variable environment *)
      let venv = venv_empty in
      let venv =
         SymbolTable.fold (fun venv v (ty, _) ->
               venv_add venv v ty) venv globals
      in
      let venv =
         SymbolTable.fold (fun venv v { Fir.import_type = ty } ->
               venv_add venv v ty) venv import
      in
      let venv =
         SymbolTable.fold (fun venv f (_, ty_vars, ty, _, _) ->
               venv_add venv f (Fir.TyAll (ty_vars, ty))) venv funs
      in

      (* Drop types on the import *)
      let import =
         SymbolTable.map (fun info ->
               let { Fir.import_name = s;
                     Fir.import_type = ty;
                     Fir.import_info = info
                   } = info
               in
               let pos = string_pos s pos in
               let info =
                  match info with
                     Fir.ImportGlobal ->
                        ImportGlobal
                   | Fir.ImportFun (b, orig_args) ->
                        let _, ty_vars, _ = dest_all_fun_type_dir tenv pos ty in
                        let asm_args = List.map (atom_class_of_type tenv pos) ty_vars in
                           ImportFun (b, orig_args, asm_args)
               in
                  { import_name = s;
                    import_info = info
                  }) import
      in
      let export =
         SymbolTable.map (fun info ->
               let { Fir.export_name = s;
                     Fir.export_type = ty
                   } = info
               in
               let pos = string_pos s pos in
                  { export_name = s;
                    export_is_fun = is_fun_type_dir tenv pos ty
                  }) export
      in

      (* Convert global functions *)
      let globals =
         SymbolTable.mapi (fun v (_, info) ->
               let pos = var_exp_pos v in
               let loc = zero_loc in
               let info =
                  match info with
                     Fir.InitAtom a ->
                        Mir.InitAtom (mir_atom tenv venv rttd frames funs pos a)
                   | Fir.InitAlloc op ->
                        Mir.InitAlloc (snd (mir_alloc_op tenv venv rttd frames funs pos loc op))
                   | Fir.InitRawData (pre, s) ->
                        Mir.InitRawData (pre, s)
                   | Fir.InitNames (_, names) ->
                        let index_ac = ACRawInt (precision_native_int, unsigned_int) in
                        let atoms =
                           List.fold_left (fun atoms (v, v_opt) ->
                                 let a =
                                    AtomUnop (MemIndexOp (PtrBlock, index_ac),
                                              AtomVar (ac_block_pointer, v))
                                 in
                                 let a_opt =
                                    match v_opt with
                                       Some v ->
                                          AtomUnop (MemIndexOp (PtrBlock, index_ac),
                                                    AtomVar (ac_block_pointer, v))
                                     | None ->
                                          AtomRawInt zero_pointer
                                 in
                                    a_opt :: a :: atoms) [] names
                        in
                           Mir.InitAlloc (AllocArray (List.rev atoms))
                   | Fir.InitTag _ ->
                        let sname = Symbol.to_string v in
                        let len = String.length sname in
                        let aname = Array.make len 0 in
                           for i = 0 to pred len do
                              aname.(i) <- Char.code sname.[i]
                           done;
                           Mir.InitRawData (Rawint.Int8, aname)
               in
                  info) globals
      in

      (* Add names as strings *)
      let globals =
         SymbolTable.fold (fun globals v _ ->
               let s = Symbol.to_string v in
               let len = String.length s in
               let s = Array.init len (fun i -> Char.code s.[i]) in
               let init = Mir.InitRawData (Rawint.Int8, s) in
                  SymbolTable.add globals v init) globals names
      in

      (* Add the AtomNil global block *)
      let globals = SymbolTable.add globals atom_nil_block (InitAlloc (AllocTuple [])) in

      (* Convert all the funs *)
      let funs =
         SymbolTable.fold (fun funs' f (line, _, ty, vars, e) ->
               let pos = var_exp_pos f in
               let ty_vars, _ = dest_fun_type_dir tenv pos ty in
               let venv =
                  List.fold_left2 (fun venv v ty ->
                        venv_add venv v ty) venv vars ty_vars
               in
               let e = mir_expr tenv venv rttd frames funs e in
               let vars =
                  List.map2 (fun v ty ->
                        v, atom_class_of_type tenv pos ty) vars ty_vars
               in
               let fund = line, vars, e in
                  SymbolTable.add funs' f fund) SymbolTable.empty funs
      in

      (* Flatten the names *)
      let names =
         SymbolTable.fold (fun names v _ ->
               SymbolSet.add names v) SymbolSet.empty names
      in
      let prog =
         { prog_file          = file_info;
           prog_import        = import;
           prog_export        = export;
           prog_globals       = globals;
           prog_funs          = funs;
           prog_mprog         = mfir;
           prog_global_order  = global_order;
           prog_fun_order     = fun_order;
           prog_arity_tags    = arity_empty;
         }
      in

      (* Process implicit function argument coercions *)
      let _ =
         if debug Mir_state.debug_print_mir then
            Mir_print.debug_prog "Pre-initial MIR" prog
      in
      let prog = poly_prog prog in

      (* Build the arity tag table *)
      let prog = generate_arity_tags prog in

      let prog = Mir_standardize.standardize_prog prog in
      let _ =
         if debug Mir_state.debug_print_mir then
            Mir_print.debug_prog "Initial MIR" prog
      in
         prog

   let compile_prog = Fir_state.profile "Mir_fir.compile_prog" compile_prog

end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
