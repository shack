(*
 * Normal MIR exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Fir_print

open Mir
open Mir_exn
open Mir_pos
open Mir_print

module Pos = MakePos (struct let name = "Mir_exn_print" end)
open Pos

(*
 * Semant exception printer.
 *)
let pp_print_error buf exn =
   match exn with
      UnboundVar v ->
         Format.fprintf buf "unbound variable: %a" pp_print_symbol v
    | UnboundType v ->
         Format.fprintf buf "unbound type: %a" pp_print_symbol v
    | ArityMismatch (i, j) ->
         Format.fprintf buf "arity mismatch: wanted %d, got %d" i j
    | StringError s ->
         Format.pp_print_string buf s
    | StringIntError (s, i) ->
         Format.fprintf buf "%s: %d" s i
    | StringVarError (s, v) ->
         Format.fprintf buf "%s: %a" s pp_print_symbol v
    | StringAtomError (s, a) ->
         Format.fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_atom a
    | StringTypeError (s, ty) ->
         Format.fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_type ty
    | StringIntTypeError (s, i, ty) ->
         Format.fprintf buf "@[<hv 3>%s.%d:@ %a@]" s i pp_print_type ty
    | StringAtomClassError (s, ac) ->
         Format.fprintf buf "@[<hv 3>%s:@ %a@]" s pp_print_atom_class ac
    | StringFormatError (s, f) ->
         Format.fprintf buf "@[<hv 3>%s:@ %t@]" s f
    | ImplicitCoercion a ->
         Format.fprintf buf "Implicit coercion not allowed: %a" pp_print_atom a
    | ImplicitCoercion2 (d, s) ->
         Format.fprintf buf "Implicit coercion from@ %a@ to@ %a@ not allowed" (**)
            pp_print_atom_class s
            pp_print_atom_class d
    | NotImplemented s ->
         Format.fprintf buf "Not implemented: %s" s
    | InternalError s ->
         Format.fprintf buf "Internal error: %s" s

(*
 * Exception printer.
 *)
let pp_print_exn buf e =
   match e with
      MirException (loc, exn) ->
         Format.fprintf buf "@[<v 0>%a@ @[<hv 3>*** MIR Error: %a@]@]" (**)
            pp_print_pos loc
            pp_print_error exn
    | exn ->
         Fir_exn_print.pp_print_exn buf exn

(*
 * Exception handler.
 *)
let catch f x =
   try f x with
      MirException _
    | Fir_pos.FirException _ as exn ->
         Format.eprintf "%a@." pp_print_exn exn;
         exit 2

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
