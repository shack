(*
   Construct a venv of the MIR program.  This assumes all vars
   have been standardized; therefore we can construct a single,
   global venv for the program.  If you care about catching the
   scope of variables, then these utilities are *not* for you;
   otherwise, construct a single, global venv and use it how
   ever you want!

   --
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Symbol

open Frame_type

open Mir
open Mir_ds
open Mir_exn
open Mir_pos
open Mir_env_sig

module Pos = MakePos (struct let name = "Mir_env" end)
open Pos


module MirEnv (Frame : BackendSig) : MirEnvSig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)


   (***  Utilities  ***)


   (* Position helper function *)
   let var_exp_pos  pos = string_pos "Mir_env" (var_exp_pos  pos)
   let exp_pos pos = string_pos "Mir_env" (exp_pos pos)


   (* Variable environments.  This is a unique take on venv's, in that
      we do NOT allow a variable to be bound multiple times in the venv *)
   let venv_empty = SymbolTable.empty
   let venv_mem   = SymbolTable.mem
   let venv_add venv pos v ac =
      let pos = string_pos "venv_add" pos in
         if venv_mem venv v then
            (* Variables must ONLY be bound once in each program!
               This exception is intentional:  most MIR transforms
               require a standardized program, and the construction
               of this venv in particular requires it.  Do not
               remove this exception. *)
            raise (MirException (pos, InternalError ("Variable " ^ (string_of_symbol v) ^ " bound multiple times.")))
         else
            SymbolTable.add venv v ac


   (***  Env Interface  ***)


   (* mir_venv_expr
      Adds all variables bound in this expression to a venv.  *)
   let rec mir_venv_expr venv e =
      let pos = string_pos "mir_venv_expr" (exp_pos e) in
         match dest_exp_core e with
            LetAtom (v, ac, _, e)
          | LetExternal (v, ac, _, _, e)
          | LetExternalReserve (_, v, ac, _, _, e)
          | PointerIndexCheck (_, _, v, ac, _, e)
          | FunctionIndexCheck (_, _, v, ac, _, e) ->
               let venv = mir_venv_expr venv e in
               let venv = venv_add venv pos v ac in
                  venv
          | LetAlloc (v, op, e) ->
               let venv = mir_venv_expr venv e in
               let venv = venv_add venv pos v (MirUtil.atom_class_of_alloc_op op) in
                  venv
          | IfThenElse (_, _, _, e1, e2) ->
               let venv = mir_venv_expr venv e1 in
               let venv = mir_venv_expr venv e2 in
                  venv
          | IfType (_, _, _, v, e1, e2) ->
               let venv = mir_venv_expr venv e1 in
               let venv = mir_venv_expr venv e2 in
               (* TEMP:  Is this the correct AC for classes? *)
               let venv = venv_add venv pos v (ACPointer PtrAggr) in
                  venv
          | Match (_, _, cases) ->
               let process_case venv (_, e) = mir_venv_expr venv e in
                  List.fold_left process_case venv cases
          | Reserve (_, e)
          | SetMem (_, _, _, _, _, e)
          | SetGlobal (_, _, _, e)
          | BoundsCheck (_, _, _, _, _, e)
          | LowerBoundsCheck (_, _, _, _, e)
          | UpperBoundsCheck (_, _, _, _, e)
          | Memcpy (_, _, _, _, _, _, e)
          | Debug (_, e)
          | PrintDebug (_, _, _, e)
          | CommentFIR (_, e)
          | AtomicCommit (_, e)
          | CopyOnWrite (_, _, e) ->
               mir_venv_expr venv e
          | TailCall _
          | SpecialCall _
          | SysMigrate _
          | Atomic _
          | AtomicRollback _ ->
               venv


   (* mir_venv_prog
      Adds all variables bound in a program into a venv.  *)
   let mir_venv_prog prog =
      let globals = prog.prog_globals in
      let funs = prog.prog_funs in
      let venv = venv_empty in

      (* Bind variables in the globals *)
      let mir_venv_globals venv v glob =
         let pos = string_pos "mir_venv_globals" (var_exp_pos v) in
         let ac = MirUtil.atom_class_of_init_exp glob in
            venv_add venv pos v ac
      in
      let venv = SymbolTable.fold mir_venv_globals venv globals in

      (* Bind variables in functions *)
      let mir_venv_fun venv _ (_, args, e) =
         let pos = string_pos "mir_venv_fun" (exp_pos e) in
         let venv = mir_venv_expr venv e in
         let venv = List.fold_left (fun venv (v, ac) -> venv_add venv pos v ac) venv args in
            venv
      in
      let venv = SymbolTable.fold mir_venv_fun venv funs in
         venv


end (* struct *)
