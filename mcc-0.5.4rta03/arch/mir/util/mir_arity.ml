(*
   Maintain arity and argument type information for functions.
   This information is used to determine which function coercions
   are ``safe''; e.g. we can cast a function accepting an int
   argument to a function accepting a float argument, but NOT to
   a function accepting a pointer argument.

   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Frame_type

open Mir
open Mir_ds
open Mir_exn
open Mir_pos
open Mir_arity_map
open Mir_arity_sig

module Pos = MakePos (struct let name = "Mir_arity" end)
open Pos


(***  Begin Arity module  ***)


module MirArity (Frame : BackendSig) : MirAritySig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)
   open MirUtil


   (***  Extract arity tags from atom classes  ***)


   (* arity_of_atom_class
      Convert an atom class into its arity tag.  *)
   let rec arity_of_atom_class pos ac =
      let pos = string_pos "arity_of_atom_class" pos in
         match ac with
            ACInt
          | ACRawInt _
          | ACFloat _ ->
               ArityValue
          | ACPoly ->
               ArityPoly
          | ACPointer ty ->
               ArityPointer ty
          | ACPointerInfix ty ->
               ArityPointerInfix ty
          | ACFunction ty ->
               ArityFunction (arity_of_funty pos ty)


   (* arity_of_atom_class_list
      Convert an atom class list into the arity tags.  *)
   and arity_of_atom_class_list pos acs =
      List.map (arity_of_atom_class pos) acs


   (* arity_of_formals
      Takes a list of formal parametres from a MIR function definition,
      and builds its arity list -- a list of arity tags that define what
      functions are compatible with this function.  *)
   and arity_of_formals pos formals =
      let acs = List.map snd formals in
         arity_of_atom_class_list pos acs


   (* arity_of_funty
      Takes a funty, and builds its arity list -- a list of arity tags
      that define what functions are compatible with this function.  *)
   and arity_of_funty pos funty =
      let pos = string_pos "arity_of_funty" pos in
         match funty with
            FunAny ->
               raise (MirException (pos, StringError "Mir_arity.arity_of_funty: FunAny not allowed here"))
          | FunArgs acs ->
               arity_of_atom_class_list pos acs


   (***  Arity tables  ***)


   (* arity_empty
      Create an empty arity table.  *)
   let arity_empty = ArityTable.empty


   (* arity_add
      Add a new arity tag name to the table.  *)
   let arity_add table arity =
      if ArityTable.mem table arity then
         table
      else
         ArityTable.add table arity (new_symbol_string (string_of_arity arity))


   (* lookup_arity
      Lookup arity based on the function argument types, indicated.  *)
   let arity_lookup table pos arity =
      let pos = string_pos "arity_lookup" pos in
         try
            ArityTable.find table arity
         with
            Not_found ->
               raise (MirException (pos, StringError ("arity tag " ^ (string_of_arity arity) ^ " not bound")))


   (* lookup_arity_by_funty
      Lookup arity based on funty, indicated.  *)
   let arity_lookup_by_funty table pos funty =
      let pos = string_pos "arity_lookup_by_funty" pos in
      let arity = arity_of_funty pos funty in
         arity_lookup table pos arity


   (* lookup_arity_by_formals
      Lookup arity based on function formals list, indicated.  *)
   let arity_lookup_by_formals table pos formals =
      let pos = string_pos "arity_lookup_by_formals" pos in
      let arity = arity_of_formals pos formals in
         arity_lookup table pos arity


   (***  Arity tag generation  ***)


   (* gentag_atom_class
      Generate arity tags for this atom class.  *)
   let gentag_atom_class table pos ac =
      let pos = string_pos "gentag_atom_class" pos in
         match ac with
            ACInt
          | ACPoly
          | ACRawInt _
          | ACFloat _
          | ACPointer _
          | ACPointerInfix _ ->
               table
          | ACFunction funty ->
               arity_add table (arity_of_funty pos funty)


   (* gentag_atom
      Generate arity tags for this atom.  *)
   let rec gentag_atom table pos a =
      let pos = string_pos "gentag_atom" pos in
         match a with
            AtomInt _
          | AtomRawInt _
          | AtomFloat _ ->
               table
          | AtomVar (ac, _)
          | AtomFunVar (ac, _) ->
               gentag_atom_class table pos ac
          | AtomUnop (_, a) ->
               gentag_atom table pos a
          | AtomBinop (_, a1, a2) ->
               gentag_atom (gentag_atom table pos a1) pos a2


   (* gentag_atom_list
      Generate arity tags for a list of atoms.  *)
   let gentag_atom_list table pos atoms =
      let pos = string_pos "gentag_atom_list" pos in
         List.fold_left (fun table a -> gentag_atom table pos a) table atoms


   (* gentag_reserve_info
      Generate arity tags for reserve information.  *)
   let gentag_reserve_info table pos (_, _, a1, a2) =
      let pos = string_pos "gentag_reserve_info" pos in
         gentag_atom_list table pos [a1; a2]


   (* gentag_alloc_op
      Generate arity tags for an allocation.  *)
   let gentag_alloc_op table pos op =
      let pos = string_pos "gentag_alloc_op" pos in
         match op with
            AllocTuple atoms
          | AllocArray atoms
          | AllocUnion (_, atoms) ->
               gentag_atom_list table pos atoms
          | AllocMArray (atoms, a) ->
               gentag_atom_list table pos (a :: atoms)
          | AllocMalloc a ->
               gentag_atom table pos a


   (* gentag_special_call
      Generate arity tags for a special call.  Careful here, we have to
      infer the arity for a function whose stub has not yet been built.  *)
   let gentag_special_call table pos op =
      let pos = string_pos "gentag_special_call" pos in
      let arity_aggr = ArityPointer PtrAggr in
      let arity_value = ArityValue in
         match op with
            TailSysMigrate (_, a1, a2, _, atoms) ->
               let table = gentag_atom_list table pos (a1 :: a2 :: atoms) in
                  arity_add table [arity_aggr]
          | TailAtomic (_, a, atoms) ->
               let table = gentag_atom_list table pos (a :: atoms) in
                  arity_add table [arity_value; arity_aggr]
          | TailAtomicCommit (a, _, atoms) ->
               let table = gentag_atom_list table pos (a :: atoms) in
                  arity_add table [arity_aggr]
          | TailAtomicRollback (a1, a2) ->
               gentag_atom_list table pos [a1; a2]


   (* gentag_expr
      Generate arity tags in an expression.  *)
   let rec gentag_expr table e =
      let pos = string_pos "gentag_expr" (exp_pos e) in
         match dest_exp_core e with
            LetAtom (_, ac, a, e)
          | SetGlobal (ac, _, a, e) ->
               let table = gentag_atom_class table pos ac in
               let table = gentag_atom       table pos a  in
                   gentag_expr table e
          | TailCall (_, _, args) ->
               let table = gentag_atom_list  table pos args in
               let acs = List.map atom_class_of_atom args in
                  arity_add table (arity_of_atom_class_list pos acs)
          | LetExternal (_, ac, _, args, e) ->
               let table = gentag_atom_class table pos ac in
               let table = gentag_atom_list  table pos args in
                  gentag_expr table e
          | LetExternalReserve (rinfo, _, ac, _, args, e) ->
               let table = gentag_reserve_info table pos rinfo in
               let table = gentag_atom_class table pos ac in
               let table = gentag_atom_list  table pos args in
                  gentag_expr table e
          | IfThenElse (_, a1, a2, e1, e2) ->
               let table = gentag_atom_list  table pos [a1; a2] in
               let table = gentag_expr table e1 in
               let table = gentag_expr table e2 in
                  table
          | Match (a, ac, cases) ->
               let process_case table (_, e) = gentag_expr table e in
               let table = List.fold_left process_case table cases in
               let table = gentag_atom       table pos a in
               let table = gentag_atom_class table pos ac in
                  table
          | IfType (a1, a2, a3, _, e1, e2) ->
               let table = gentag_atom_list  table pos [a1; a2; a3] in
               let table = gentag_expr table e1 in
               let table = gentag_expr table e2 in
                  table
          | Reserve (rinfo, e) ->
               let table = gentag_reserve_info table pos rinfo in
                  gentag_expr table e
          | LetAlloc (_, op, e) ->
               let table = gentag_alloc_op   table pos op in
                  gentag_expr table e
          | SetMem (ac, _, a1, a2, a3, e) ->
               let table = gentag_atom_class table pos ac in
               let table = gentag_atom_list  table pos [a1; a2; a3] in
                  gentag_expr table e
          | BoundsCheck (_, _, a1, a2, a3, e) ->
               let table = gentag_atom_list  table pos [a1; a2; a3] in
                  gentag_expr table e
          | LowerBoundsCheck (_, _, a1, a2, e)
          | UpperBoundsCheck (_, _, a1, a2, e) ->
               let table = gentag_atom_list  table pos [a1; a2] in
                  gentag_expr table e
          | PointerIndexCheck (_, _, _, ac, a, e) ->
               let table = gentag_atom_class table pos ac in
               let table = gentag_atom       table pos a in
                  gentag_expr table e
          | FunctionIndexCheck (_, funty, _, ac, a, e) ->
               let table = gentag_atom_class table pos ac in
               let table = gentag_atom       table pos a in
               let table = arity_add table (arity_of_funty pos funty) in
                  gentag_expr table e
          | Memcpy (_, a1, a2, a3, a4, a5, e) ->
               let table = gentag_atom_list  table pos [a1; a2; a3; a4; a5] in
                  gentag_expr table e
          | Debug (_, e)
          | CommentFIR (_, e) ->
               gentag_expr table e
          | PrintDebug (_, _, args, e) ->
               let table = gentag_atom_list  table pos args in
                  gentag_expr table e
          | SpecialCall op ->
               gentag_special_call table pos op
          | SysMigrate _
          | Atomic _
          | AtomicRollback _
          | AtomicCommit _
          | CopyOnWrite _ ->
               raise (MirException (pos, InternalError "This statement is not allowed here"))


   (* generate_arity_tags
      Generate arity tags for an entire program.  *)
   let generate_arity_tags prog =
      let { prog_import       = import;
            prog_export       = export;
            prog_globals      = globals;
            prog_funs         = funs;
            prog_arity_tags   = table;
          } = prog
      in

      (* Clear the old index values for the tags *)
      let table = ArityTable.map (fun (v, _) -> v) table in

      (* Add arity tags required by the backend *)
      let table = List.fold_left (fun table tag ->
         arity_add table tag) table Frame.backend_arity_tags
      in

      (* Add arity tags required by the program functions *)
      let table = SymbolTable.fold (fun table f (_, formals, e) ->
         let pos = string_pos "generate_arity_tags" (var_exp_pos f) in
         let table = arity_add table (arity_of_formals pos formals) in
         let table = gentag_expr table e in
            table) table funs
      in

      (* Assign values for each arity tag *)
      let index = Int32.of_string "0xAA000000" in
      let table, _ = ArityTable.fold (fun (table, index) tag name ->
         let table = ArityTable.add table tag (name, index) in
         let index = Int32.succ index in
            table, index) (ArityTable.empty, index) table
      in
         { prog with
               prog_arity_tags = table;
         }

   let generate_arity_tags = Fir_state.profile "Mir_arity.generate_arity_tags" generate_arity_tags

end (* MirArity module *)
