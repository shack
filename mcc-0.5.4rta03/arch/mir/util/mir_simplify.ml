(*
   Simplifies the MIR.  This file rewrites some of the higher
   level MIR primitives into simpler expressions so we don't
   have to worry about them in the backend -- some of the safety
   checks and whatnot are harder to deal with in backend.  Note
   that this code may be called by the backend itself if they
   need to run an expression...

   --
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Debug
open Symbol
open Interval_set

open Fir_set

open Frame_type
open Sizeof_const

open Mir
open Mir_ds
open Mir_pos
open Mir_exn
open Mir_simplify_sig

module Pos = MakePos (struct let name = "Mir_simplify" end)
open Pos


(***  Policies  ***)


module MirSimplify (Frame : BackendSig) : MirSimplifySig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)
   module MirArity = Mir_arity.MirArity (Frame)
   open Frame
   open MirUtil
   open MirArity

   let exp_pos pos =
      string_pos "Mir_simplify" (exp_pos pos)


   (***  Simplification of Atoms  ***)


   (*
      These functions transform the complex *atom* expressions into simpler
      expressions that we can work with in the backend.  Note that simplifying
      atoms may require the addition of new *expression* statements, PRIOR to
      the use of the atom.  Because of this, we have to play a bit of a logic
      game here.

      Each of these functions takes a function called e_cont, as well as the
      atom to be simplified.  The code computes the expression and a new,
      simplified atom expression -- the simplified atom is usually a variable
      that contains the result.  We call e_cont with *this* atom, and e_cont
      computes the rest of the program with the substituted atom.  We then
      prepend our expression to the result.  A new expression (with the
      substituted atoms) is returned.
    *)


   (* Specifies what operation to perform for *_of_index functions.
      The *_of_index functions take this operator to determine what
      type of code they are emitting. *)
   type op =
      Safe        (* Pointer is safe, no checks required *)
    | Unsafe      (* Pointer is unsafe, but no check required *)
    | UnsafeCheck (* Pointer is unsafe, please check it *)


   (* is_safe
      Returns true if the operation specified is ``safe''.  Note: this
      affects the memory representation only; if no safety checks were
      enabled, then we'll call everything safe so everything uses the
      simple representation. *)
   let is_safe = function
      Safe ->
         true
    | Unsafe
    | UnsafeCheck ->
         not (Fir_state.safety_enabled ())


   (* do_check
      Returns true if the operation specified wants to ``check''. *)
   let do_check = function
      Safe
    | Unsafe ->
         false
    | UnsafeCheck ->
         true


   (* pointer_of_index
      Express a hashed index->pointer operation, assuming the usual
      safety checks may be required in the code.  This uses the pointer
      table.  The op specifies whether this is a safe index->pointer
      conversion, or an unsafe.  The op also specifies whether checks
      should be done.  *)
   let pointer_of_index arity_tags pos loc ptrty op a_hashed_index e_cont =
      let pos = string_pos "pointer_of_index" pos in
      let v_index = new_symbol_string "index" in
      let v_index_test = new_symbol_string "index_test" in
      let v_pointer = new_symbol_string "pointer" in
      let v_bit = new_symbol_string "bit" in
      let ac_pointer = ACPointer ptrty in
      let a_cont = AtomVar (ac_pointer, v_pointer) in
      let e_cont = e_cont a_cont in

      (* Construct the segfault expressions *)
      let seg_fault_index_expr =    (* Index was invalid *)
         make_exp loc (TailCall (ExternalCall, seg_fault_labels.(index_fault),
                   [a_hashed_index; AtomVar (ac_native_unsigned, v_index)]))
      in
      let seg_fault_pointer_expr =  (* Pointer in ptbl was invalid *)
         make_exp loc (TailCall (ExternalCall, seg_fault_labels.(pointer_index_fault),
                   [a_hashed_index;
                    AtomVar (ac_native_unsigned, v_index);
                    AtomVar (ac_pointer, v_pointer)]))
      in

      (* STEP 1 - Compute index from hash. *)
      let build_step1 e =
         if is_safe op then
            (* Safe pointers do not hash their value, currently *)
            make_exp loc (LetAtom (v_index, ac_native_unsigned, a_hashed_index, e))
         else
            (* Convert an unsafe hashed index into a real index *)
            make_exp loc (LetAtom (v_index, ac_native_unsigned,
                     AtomBinop (AndOp ac_native_unsigned, AtomRawInt (Rawint.of_int Rawint.Int32 false 0xfffffffc),
                     AtomBinop (XorOp ac_native_unsigned, a_hashed_index, hash_native_unsigned)), e))
      in (* end of build_step1 *)

      (* STEP 2 - Check that the index is valid. *)
      let build_step2 e =
         if do_check op then
            (* Check that it is a valid index *)
            make_exp loc (LetAtom (v_index_test, ac_native_bool,
                     AtomBinop (LtOp ac_native_unsigned,
                                AtomVar (ac_native_unsigned, v_index),
                                AtomVar (ac_native_unsigned, pointer_size)),
            make_exp loc (Match (AtomVar (ac_native_bool, v_index_test), ac_native_bool,
                   [RawIntSet native_false_set, seg_fault_index_expr;
                    RawIntSet native_true_set, e]))))
         else
            (* No check required. *)
            e
      in (* end of build_step2 *)

      (* STEP 3 - Read from pointer table *)
      let build_step3 e =
         (* Read the pointer from the ptbl *)
         make_exp loc (LetAtom (v_pointer, ac_pointer,
                  AtomBinop (MemOp (ac_pointer, PtrAggr), AtomVar (ac_pointer_base, pointer_base),
                             AtomVar (ac_native_unsigned, v_index)), e))
      in (* end of build_step3 *)

      (* STEP 4 - Check that the pointer is valid. *)
      let build_step4 e =
         if do_check op then
            (* Check that the pointer is valid *)
            make_exp loc (LetAtom (v_bit, ac_native_unsigned,
                     AtomBinop (AndOp ac_native_unsigned,
                                AtomUnop (Int32OfPointerOp ptrty, AtomVar (ac_pointer, v_pointer)),
                                AtomRawInt one_pointer),
            make_exp loc (Match (AtomVar (ac_native_unsigned, v_bit), ac_native_unsigned,
                   [RawIntSet one_pointer_set, seg_fault_pointer_expr;
                    RawIntSet zero_pointer_set, e]))))
         else
            (* No check required. *)
            e
      in (* end of build_step2 *)

         (* Construct the index->pointer operation *)
         build_step1 (build_step2 (build_step3 (build_step4 e_cont)))


   (* function_of_index
      Express a hashed index->pointer operation, assuming the usual
      safety checks are required in the code.  This uses the function
      pointer table.  The op specifies whether this is a safe function
      conversion or unsafe, and also specifies whether checks should
      be done.  *)
   let function_of_index arity_tags pos loc funty op a_hashed_index e_cont =
      let pos = string_pos "function_of_index" pos in
      let v_index = new_symbol_string "index" in
      let v_index_test = new_symbol_string "index_test" in
      let v_pointer = new_symbol_string "pointer" in
      let ac_pointer = ACFunction funty in
      let v_arity_test = new_symbol_string "arity_test" in
      let v_arity_tag = new_symbol_string "arity_tag" in
      let ac_arity_tag = ac_native_unsigned in
      let a_cont = AtomVar (ac_pointer, v_pointer) in
      let e_cont = e_cont a_cont in

      (* Compute the required arity tag for this function pointer. *)
      let arity_tag, _ = arity_lookup_by_funty arity_tags pos funty in

      (* Construct the segfault expressions *)
      let seg_fault_index_expr =       (* Index was invalid *)
         make_exp loc (TailCall (ExternalCall, seg_fault_labels.(fun_index_fault),
                   [a_hashed_index; AtomVar (ac_native_unsigned, v_index)]))
      in
      let seg_fault_arity_tag_expr =   (* Arity tags not compatible *)
         make_exp loc (TailCall (ExternalCall, seg_fault_labels.(fun_arity_tag_fault),
                   [a_hashed_index; AtomVar (ac_native_unsigned, v_index);
                    AtomVar (ac_arity_tag, arity_tag); AtomVar (ac_arity_tag, v_arity_tag)]))
      in

      (* STEP 1 - Compute index from hash *)
      let build_step1 e =
         if is_safe op then
            (* Safe pointers do not hash their indices, currently *)
            make_exp loc (LetAtom (v_index, ac_native_unsigned, a_hashed_index, e))
         else
            (* Unhash the unsafe function index *)
            make_exp loc (LetAtom (v_index, ac_native_unsigned,
                     AtomBinop (XorOp ac_native_unsigned, a_hashed_index, hash_fun_native_unsigned),
                     e))
      in (* end of build_step1 *)

      (* STEP 2 - Check that the index is valid *)
      let build_step2 e =
         if do_check op then
            (* Check that it is a valid index *)
            make_exp loc (LetAtom (v_index_test, ac_native_bool,
                     AtomBinop (LtOp ac_native_unsigned,
                                AtomVar (ac_native_unsigned, v_index),
                                AtomVar (ac_native_unsigned, function_size)),
            make_exp loc (Match (AtomVar (ac_native_bool, v_index_test), ac_native_bool,
                   [RawIntSet native_false_set, seg_fault_index_expr;
                    RawIntSet native_true_set, e]))))
         else
            (* No check required. *)
            e
      in (* end of build_step2 *)

      (* STEP 3 - Read from function table - pointers in the function
         table are always valid pointers, since they are in code seg. *)
      let build_step3 e =
         (* Read the pointer from the ptbl *)
         make_exp loc (LetAtom (v_pointer, ac_pointer,
                  AtomBinop (MemOp (ac_pointer, PtrAggr), AtomVar (ac_pointer_base, function_base),
                             AtomVar (ac_native_unsigned, v_index)),
                  e))
      in (* end of build_step3 *)

      (* STEP 4 - Check that the arity tag is compatible. *)
      let build_step4 e =
         if Fir_state.safe_functions () then
            (* Get arity tag on the function header block. *)
            make_exp loc (LetAtom (v_arity_tag, ac_arity_tag,
                     AtomUnop (MemFunArityTagOp (funty, ac_arity_tag),
                               AtomVar (ac_pointer, v_pointer)),
            (* Check the arity tag *)
            make_exp loc (LetAtom (v_arity_test, ac_native_bool,
                     AtomBinop (EqOp ac_native_unsigned,
                                AtomVar (ac_arity_tag, v_arity_tag),
                                AtomVar (ac_arity_tag, arity_tag)),
            make_exp loc (Match (AtomVar (ac_arity_tag, v_arity_tag), ac_arity_tag,
                   [RawIntSet native_false_set, seg_fault_arity_tag_expr;
                    RawIntSet native_true_set, e]))))))
         else
            (* No arity check required *)
            e
      in (* end of build_step4 *)

         (* Construct the final expression. *)
         build_step1 (build_step2 (build_step3 (build_step4 e_cont)))


   (* index_of_pointer
      Converts a pointer into an index.  The op specifies whether the
      conversion is using safe or unsafe representation.  *)
   let index_of_pointer arity_tags pos loc ptrty op a_pointer e_cont =
      let pos = string_pos "index_of_pointer" pos in
      let ac_index = ACRawInt (precision_native_int, unsigned_int) in
      let a_index = AtomUnop (MemIndexOp (ptrty, ac_index), a_pointer) in
         if is_safe op then
            e_cont a_index
         else
            e_cont (AtomBinop (XorOp ac_native_unsigned, a_index, hash_native_unsigned))


   (* index_of_function
      Converts a function pointer into an index.  The op specifies
      whether the conversion is using safe or unsafe representation. *)
   let index_of_function arity_tags pos loc funty op a_pointer e_cont =
      let pos = string_pos "index_of_function" pos in
      let ac_index = ACRawInt (precision_native_int, unsigned_int) in
      let a_index = AtomUnop (MemFunIndexOp (funty, ac_index), a_pointer) in
         if is_safe op then
            e_cont a_index
         else
            e_cont (AtomBinop (XorOp ac_native_unsigned, a_index, hash_fun_native_unsigned))


   (* mir_simplify_atom
      Simplifies the atom given.  *)
   let rec mir_simplify_atom arity_tags pos loc a e_cont =
      let pos = string_pos "mir_simplify_atom" pos in
         match a with
            AtomInt _
          | AtomRawInt _
          | AtomFloat _
          | AtomVar _
          | AtomFunVar _ ->
               e_cont a
          | AtomUnop (op, a) ->
               mir_simplify_unop_atom arity_tags pos loc op a e_cont
          | AtomBinop (op, a1, a2) ->
               mir_simplify_binop_atom arity_tags pos loc op a1 a2 e_cont


   (* mir_simplify_unop_atom
      Simplifies the unary operator given. *)
   and mir_simplify_unop_atom arity_tags pos loc op a e_cont =
      let pos = string_pos "mir_simplify_unop_atom" pos in
         mir_simplify_atom arity_tags pos loc a (fun a ->
            match op with
               SafePointerOfIndexOp ptrty ->
                  pointer_of_index arity_tags pos loc ptrty Safe a e_cont
             | UnsafePointerOfIndexOp ptrty ->
                  pointer_of_index arity_tags pos loc ptrty Unsafe a e_cont
             | SafeFunctionOfIndexOp funty ->
                  function_of_index arity_tags pos loc funty Safe a e_cont
             | UnsafeFunctionOfIndexOp funty ->
                  function_of_index arity_tags pos loc funty Unsafe a e_cont
             | SafeIndexOfPointerOp ptrty ->
                  index_of_pointer arity_tags pos loc ptrty Safe a e_cont
             | UnsafeIndexOfPointerOp ptrty ->
                  index_of_pointer arity_tags pos loc ptrty Unsafe a e_cont
             | SafeIndexOfFunctionOp funty ->
                  index_of_function arity_tags pos loc funty Safe a e_cont
             | UnsafeIndexOfFunctionOp funty ->
                  index_of_function arity_tags pos loc funty Unsafe a e_cont
             | _ ->
                  e_cont (AtomUnop (op, a)))


   (* mir_simplify_binop_atom
      Simplifies the binary operator given. *)
   and mir_simplify_binop_atom arity_tags pos loc op a1 a2 e_cont =
      let pos = string_pos "mir_simplify_binop_atom" pos in
         mir_simplify_atom arity_tags pos loc a1 (fun a1 ->
            mir_simplify_atom arity_tags pos loc a2 (fun a2 ->
               e_cont (AtomBinop (op, a1, a2))))


   (* mir_simplify_atom_list
      Simplifies a list of atoms, in order.  *)
   let mir_simplify_atom_list arity_tags pos loc atoms e_cont =
      let pos = string_pos "mir_simplify_atom_list" pos in
      let rec mir_simplify_atom_list arity_tags res = function
         a :: atoms ->
            mir_simplify_atom arity_tags pos loc a (fun a ->
               mir_simplify_atom_list arity_tags (a :: res) atoms)
       | [] ->
            e_cont (List.rev res)
      in
         mir_simplify_atom_list arity_tags [] atoms


   (* mir_simplify_atom_option
      Simplifies an optional atom.  *)
   let mir_simplify_atom_option arity_tags pos loc atom e_cont =
      let pos = string_pos "mir_simplify_atom_option" pos in
         match atom with
            Some atom ->
               mir_simplify_atom arity_tags pos loc atom (fun atom ->
                  e_cont (Some atom))
          | None ->
               e_cont None


   (***  Simplification of Expressions  ***)


   (* mir_simplify_expr
      Simplifies the expression given. *)
   let rec mir_simplify_expr arity_tags e =
      let pos = string_pos "mir_simplify_expr" (exp_pos e) in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetAtom (v, ac, a, e) ->
               mir_simplify_atom arity_tags pos loc a (fun a ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetAtom (v, ac, a, e)))
          | TailCall (op, f, atoms) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                  make_exp loc (TailCall (op, f, atoms)))
          | IfThenElse (op, a1, a2, e1, e2) ->
               mir_simplify_atom arity_tags pos loc a1 (fun a1 ->
                  mir_simplify_atom arity_tags pos loc a2 (fun a2 ->
                     let e1 = mir_simplify_expr arity_tags e1 in
                     let e2 = mir_simplify_expr arity_tags e2 in
                        make_exp loc (IfThenElse (op, a1, a2, e1, e2))))
          | IfType (obj, names, name, v, e1, e2) ->
               mir_simplify_atom arity_tags pos loc obj (fun obj ->
                  mir_simplify_atom arity_tags pos loc names (fun names ->
                     mir_simplify_atom arity_tags pos loc name (fun name ->
                     let e1 = mir_simplify_expr arity_tags e1 in
                     let e2 = mir_simplify_expr arity_tags e2 in
                        make_exp loc (IfType (obj, names, name, v, e1, e2)))))
          | Match (a, ac, cases) ->
               mir_simplify_atom arity_tags pos loc a (fun a ->
                  let process_case (set, e) = (set, mir_simplify_expr arity_tags e) in
                  let cases = List.map process_case cases in
                     make_exp loc (Match (a, ac, cases)))
          | LetExternal (v, ac, f, atoms, e) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetExternal (v, ac, f, atoms, e)))
          | LetExternalReserve ((label, vars, mem, ptr), v, ac, f, atoms, e) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
               mir_simplify_atom arity_tags pos loc mem (fun mem ->
               mir_simplify_atom arity_tags pos loc ptr (fun ptr ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetExternalReserve ((label, vars, mem, ptr), v, ac, f, atoms, e)))))
          | Reserve ((label, vars, mem, ptr), e) ->
               mir_simplify_atom arity_tags pos loc mem (fun mem ->
                  mir_simplify_atom arity_tags pos loc ptr (fun ptr ->
                     let e = mir_simplify_expr arity_tags e in
                        make_exp loc (Reserve ((label, vars, mem, ptr), e))))
          | LetAlloc (v, AllocTuple atoms, e) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetAlloc (v, AllocTuple atoms, e)))
          | LetAlloc (v, AllocArray atoms, e) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetAlloc (v, AllocArray atoms, e)))
          | LetAlloc (v, AllocMArray (atoms, a), e) ->
               mir_simplify_atom arity_tags pos loc a (fun a ->
                  mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                     let e = mir_simplify_expr arity_tags e in
                        make_exp loc (LetAlloc (v, AllocMArray (atoms, a), e))))
          | LetAlloc (v, AllocUnion (tag, atoms), e) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetAlloc (v, AllocUnion (tag, atoms), e)))
          | LetAlloc (v, AllocMalloc a, e) ->
               mir_simplify_atom arity_tags pos loc a (fun a ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (LetAlloc (v, AllocMalloc a, e)))
          | SetMem (ac, op, a1, a2, a3, e) ->
               mir_simplify_atom arity_tags pos loc a1 (fun a1 ->
                  mir_simplify_atom arity_tags pos loc a2 (fun a2 ->
                     mir_simplify_atom arity_tags pos loc a3 (fun a3 ->
                        let e = mir_simplify_expr arity_tags e in
                           make_exp loc (SetMem (ac, op, a1, a2, a3, e)))))
          | SetGlobal (ac, v, a, e) ->
               mir_simplify_atom arity_tags pos loc a (fun a ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (SetGlobal (ac, v, a, e)))
          | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
               mir_simplify_bounds_check arity_tags pos loc label ptrty ptr start stop e
          | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
               mir_simplify_lower_bounds_check arity_tags pos loc label ptrty ptr start e
          | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
               mir_simplify_upper_bounds_check arity_tags pos loc label ptrty ptr stop e
          | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
               mir_simplify_atom arity_tags pos loc index (fun index ->
                  pointer_of_index arity_tags pos loc ptrty UnsafeCheck index (fun a ->
                     let e = mir_simplify_expr arity_tags e in
                        make_exp loc (LetAtom (v, ac, a, e))))
          | FunctionIndexCheck (label, funty, v, ac, index, e) ->
               mir_simplify_atom arity_tags pos loc index (fun index ->
                  function_of_index arity_tags pos loc funty UnsafeCheck index (fun a ->
                     let e = mir_simplify_expr arity_tags e in
                        make_exp loc (LetAtom (v, ac, a, e))))
          | Memcpy (op, a1, a2, a3, a4, a5, e) ->
               mir_simplify_atom arity_tags pos loc a1 (fun a1 ->
                  mir_simplify_atom arity_tags pos loc a2 (fun a2 ->
                     mir_simplify_atom arity_tags pos loc a3 (fun a3 ->
                        mir_simplify_atom arity_tags pos loc a4 (fun a4 ->
                           mir_simplify_atom arity_tags pos loc a5 (fun a5 ->
                              let e = mir_simplify_expr arity_tags e in
                                 make_exp loc (Memcpy (op, a1, a2, a3, a4, a5, e)))))))
          | Debug (info, e) ->
               let e = mir_simplify_expr arity_tags e in
                  make_exp loc (Debug (info, e))
          | PrintDebug (v1, v2, atoms, e) ->
               mir_simplify_atom_list arity_tags pos loc atoms (fun atoms ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (PrintDebug (v1, v2, atoms, e)))
          | CommentFIR (fir, e) ->
               let e = mir_simplify_expr arity_tags e in
                  make_exp loc (CommentFIR (fir, e))
          | SysMigrate (label, dst_ptr, dst_off, f, env) ->
               mir_simplify_atom arity_tags pos loc dst_ptr (fun dst_ptr ->
                  mir_simplify_atom arity_tags pos loc dst_off (fun dst_off ->
                     mir_simplify_atom arity_tags pos loc env (fun env ->
                        make_exp loc (SysMigrate (label, dst_ptr, dst_off, f, env)))))
          | Atomic (f, i, env) ->
               mir_simplify_atom arity_tags pos loc i (fun i ->
                  mir_simplify_atom arity_tags pos loc env (fun env ->
                     make_exp loc (Atomic (f, i, env))))
          | AtomicRollback (level, i) ->
               mir_simplify_atom arity_tags pos loc level (fun level ->
                  mir_simplify_atom arity_tags pos loc i (fun i ->
                     make_exp loc (AtomicRollback (level, i))))
          | AtomicCommit (level, e) ->
               mir_simplify_atom arity_tags pos loc level (fun level ->
                  let e = mir_simplify_expr arity_tags e in
                     make_exp loc (AtomicCommit (level, e)))
          | CopyOnWrite ((label, vars, mem, ptr), copy_ptr, e) ->
               mir_simplify_atom arity_tags pos loc mem (fun mem ->
                  mir_simplify_atom arity_tags pos loc ptr (fun ptr ->
                     mir_simplify_atom arity_tags pos loc copy_ptr (fun copy_ptr ->
                        let e = mir_simplify_expr arity_tags e in
                           make_exp loc (CopyOnWrite ((label, vars, mem, ptr), copy_ptr, e)))))
          | SpecialCall _ ->
               raise (MirException (pos, InternalError "SpecialCall not allowed here"))


   (* mir_simplify_bounds_check_core
      Rewrites a *BoundsCheck expression into two bounds checks.
      both lower and upper bounds must be checked.  The <start> and
      <stop> arguments are atom option fields; a bounds check is
      done only if the corresponding argument matches Some (atom). *)
   and mir_simplify_bounds_check_core arity_tags pos loc label ptrty ptr start stop e =
      let pos = string_pos "mir_simplify_bounds_check_core" pos in

      (* Setup a bunch of variables that we might need to use... *)
      let v_lower = new_symbol_string "lower_offset" in
      let v_upper = new_symbol_string "upper_offset" in
      let v_lower_test = new_symbol_string "lower_offset_test" in
      let v_upper_test = new_symbol_string "upper_offset_test" in
      let v_size = new_symbol_string "block_size" in

      (* Seg fault expression for when something goes wrong. *)
      let seg_fault_expr =
         let e =
            match start, stop with
               Some start, Some stop ->
                  TailCall (ExternalCall, seg_fault_labels.(bounds_fault),
                            [ptr; start; stop; AtomVar (ac_native_unsigned, v_size)])
             | Some start, None ->
                  TailCall (ExternalCall, seg_fault_labels.(lower_bounds_fault),
                            [ptr; start; AtomVar (ac_native_unsigned, v_size)])
             | None, Some stop ->
                  TailCall (ExternalCall, seg_fault_labels.(upper_bounds_fault),
                            [ptr; stop; AtomVar (ac_native_unsigned, v_size)])
             | None, None ->
                  raise (MirException (pos, InternalError "Bounds check requested with no bounds specified!"))
         in
            make_exp loc e
      in

      (* Simplify the atoms and final expression, first *)
      mir_simplify_atom arity_tags pos loc ptr (fun ptr ->
         mir_simplify_atom_option arity_tags pos loc start (fun start ->
            mir_simplify_atom_option arity_tags pos loc stop (fun stop ->
               let e = mir_simplify_expr arity_tags e in

               (* Okay, now we can start rewriting the bounds check.  Note
                  that we need to check both the lower and UPPER bounds; if
                  we only check the upper bound, then it's possible to flow
                  into the header (hint: set size = 8, offset = -4).  NOTE:
                  In the case where lower and upper bounds are both constants,
                  one of these steps will be redundant.  We do UPPER bound
                  FIRST because that check is most exclusive, therefore we
                  can more easily catch the redundant conditional if this
                  conditional appears first! *)
               (* STEP 1 - Load values into temporary variables *)
               let build_step1 e =
                  let get_lower e =
                     match start with
                        Some start ->
                           make_exp loc (LetAtom (v_lower, ac_native_unsigned, start, e))
                      | None ->
                           e
                  in
                  let get_upper e =
                     match stop with
                        Some stop ->
                           make_exp loc (LetAtom (v_upper, ac_native_unsigned, stop, e))
                      | None ->
                           e
                  in
                  let size_ac = ACRawInt (precision_native_int, unsigned_int) in
                     make_exp loc (LetAtom (v_size, ac_native_unsigned,
                              AtomUnop (MemSizeOp (ptrty, size_ac), ptr),
                              get_lower (get_upper e)))
               in (* end of step1 *)

               (* STEP 2 - check against the upper bound *)
               let build_step2 e =
                  match stop with
                     Some stop ->
                        (* NOTE: values are unsigned, therefore this value must
                           be less than OR EQUAL TO the size of the memory block.
                           If we run over the beginning of the block, then we
                           get a large (unsigned) value, so this check will work. *)
                        make_exp loc (LetAtom (v_upper_test, ac_native_bool,
                                 AtomBinop (LeOp ac_native_unsigned,
                                            AtomVar (ac_native_unsigned, v_upper),
                                            AtomVar (ac_native_unsigned, v_size)),
                        make_exp loc (Match (AtomVar (ac_native_bool, v_upper_test), ac_native_bool,
                               [RawIntSet native_false_set, seg_fault_expr;
                                RawIntSet native_true_set, e]))))
                   | None ->
                        (* No upper bound to check *)
                        e
               in (* end of step2 *)

               (* STEP 3 - check against the lower bound *)
               let build_step3 e =
                  match start with
                     Some start ->
                        (* NOTE: values are unsigned, therefore this value must be
                           strictly LESS THAN the size of the memory block. *)
                        make_exp loc (LetAtom (v_lower_test, ac_native_bool,
                                 AtomBinop (LtOp ac_native_unsigned,
                                            AtomVar (ac_native_unsigned, v_lower),
                                            AtomVar (ac_native_unsigned, v_size)),
                        make_exp loc (Match (AtomVar (ac_native_bool, v_lower_test), ac_native_bool,
                               [RawIntSet native_false_set, seg_fault_expr;
                                RawIntSet native_true_set, e]))))
                   | None ->
                        (* No lower bound to check *)
                        e
               in (* end of step3 *)

                  (* Construct the bounds check *)
                  build_step1 (build_step2 (build_step3 e)))))


   (* mir_simplify_bounds_check
      Checks both lower and upper bounds. *)
   and mir_simplify_bounds_check arity_tags pos loc label ptrty ptr start stop e =
      let pos = string_pos "mir_simplify_bounds_check" pos in
         mir_simplify_bounds_check_core arity_tags pos loc label ptrty ptr (Some start) (Some stop) e


   (* mir_simplify_lower_bounds_check
      Checks the lower bounds only. *)
   and mir_simplify_lower_bounds_check arity_tags pos loc label ptrty ptr start e =
      let pos = string_pos "mir_simplify_bounds_check" pos in
         mir_simplify_bounds_check_core arity_tags pos loc label ptrty ptr (Some start) None e


   (* mir_simplify_upper_bounds_check
      Checks the upper bounds only. *)
   and mir_simplify_upper_bounds_check arity_tags pos loc label ptrty ptr stop e =
      let pos = string_pos "mir_simplify_bounds_check" pos in
         mir_simplify_bounds_check_core arity_tags pos loc label ptrty ptr None (Some stop) e


   (***  Simplification of Programs  ***)


   (* simplify_prog
      Simplifies an entire program.  Note: this function currently only
      simplifies function bodies; it does not simplify atoms that appear
      in globals, or other locations.  *)
   let simplify_prog prog =
      let { prog_funs         = funs;
            prog_arity_tags   = arity_tags;
          } = prog
      in
      let simplify_fun (funs, arity_tags) f (line, args, e) =
         let e = mir_simplify_expr arity_tags e in
         let funs = SymbolTable.add funs f (line, args, e) in
            funs, arity_tags
      in
      let funs, arity_tags = SymbolTable.fold simplify_fun (SymbolTable.empty, arity_tags) funs in
         if debug Mir_state.debug_print_mir then
            Mir_print.debug_prog "After simplify" prog;
         { prog with
               prog_funs         = funs;
               prog_arity_tags   = arity_tags;
         }

   let simplify_prog = Fir_state.profile "Mir_simplify.simplify_prog" simplify_prog

   let simplify_expr = Fir_state.profile "Mir_simplify.simplify_expr" mir_simplify_expr


end (* struct *)
