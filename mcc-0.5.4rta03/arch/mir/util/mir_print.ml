(*
 * Print the FIR program.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Symbol
open Location

open Fir_set
open Fir_print

open Frame_type

open Mir
open Mir_ds
open Mir_arity_map

let mir_comments_fir = ref false

(*
 * Separated list of fields.
 *)
let pp_print_sep_list = Fir_print.pp_print_sep_list

(*
 * Rawint.
 *)
let pp_print_rawint buf i =
   pp_print_string buf (Rawint.to_string i)

(*
 * Print an arbitrary set.
 *)
let pp_print_set buf s =
   match s with
      IntSet set    ->
         fprintf buf "int %a" pp_print_int_set set
    | RawIntSet set ->
         fprintf buf "rawint %a" pp_print_raw_int_set set

(*
 * Atom class.
 *)
let rec string_of_ptrty ty =
   match ty with
      PtrAggr ->
         "aggr"
    | PtrBlock ->
         "block"

and pp_print_ptrty buf ty =
   pp_print_string buf (string_of_ptrty ty)

and string_of_funty ty =
   let rec append first = function
      [] ->
         ""
    | ty :: tyl ->
         let s = (string_of_atom_class ty) ^ (append false tyl) in
            if first then
               s
            else
               "," ^ s
   in
      match ty with
         FunAny ->
            "fun[...]"
       | FunArgs args ->
            "fun[" ^ (append true args) ^ "]"

and string_of_atom_class ac =
   match ac with
      ACInt ->
         "int"
    | ACPoly ->
         "poly"
    | ACRawInt (pre, signed) ->
         let prefix = if signed then "" else "u" in
         let base =
            match pre with
               Rawint.Int8 -> "int8"
             | Rawint.Int16 -> "int16"
             | Rawint.Int32 -> "int32"
             | Rawint.Int64 -> "int64"
         in
            prefix ^ base
    | ACFloat (Rawfloat.Single) ->
         "single"
    | ACFloat (Rawfloat.Double) ->
         "double"
    | ACFloat (Rawfloat.LongDouble) ->
         "long double"
    | ACPointer ptrty ->
         string_of_ptrty ptrty ^ " ptr"
    | ACPointerInfix ptrty ->
         string_of_ptrty ptrty ^ " infixptr"
    | ACFunction funty ->
         string_of_funty funty

let pp_print_atom_class buf ac =
   pp_print_string buf (string_of_atom_class ac)

let pp_print_atom_classes buf acl =
   pp_print_sep_list "," pp_print_atom_class buf acl

(*
 * Print atoms.
 *)
let rec pp_print_unop_atom buf op a =
   fprintf buf "%s%a" op pp_print_atom a

and pp_print_unop_fun buf op a =
   fprintf buf "%s(%a)" op pp_print_atom a

and pp_print_binop_atom buf ac op a1 a2 =
   fprintf buf "%a(%a %s %a)"
      pp_print_atom_class ac
      pp_print_atom a1
      op
      pp_print_atom a2

and pp_print_binop_fun buf op a1 a2 =
   fprintf buf "%s(%a, %a)"
      op
      pp_print_atom a1
      pp_print_atom a2

and pp_print_binop_brack ac op a1 a2 buf =
   fprintf buf "%a:(%a)"
      pp_print_atom_class ac
      pp_print_atom a1;
   match op with
      PtrBlock ->
         fprintf buf ".(%a)" pp_print_atom a2
    | PtrAggr ->
         fprintf buf "[%a]" pp_print_atom a2

and string_of_binop_brack op =
   match op with
      PtrBlock ->
         ".(block)"
    | PtrAggr ->
         "[aggr]"


and string_of_unop op =
   match op with
      UMinusOp _ ->
         "-"
    | NotOp _ ->
         "~"
    | BitFieldOp (_, _, ofs, len) ->
         (Printf.sprintf "field{%d,%d}" (Int32.to_int ofs) (Int32.to_int len))
    | IntOfFloatOp _
    | IntOfRawIntOp _
    | IntOfPolyOp ->
         "(" ^ (string_of_atom_class ACInt) ^ ")"
    | Int32OfPointerOp _ ->
         "(" ^ (string_of_atom_class (ACRawInt (Rawint.Int32, false))) ^ ")"
    | RawIntOfFloatOp (pre, signed, _)
    | RawIntOfIntOp (pre, signed)
    | RawIntOfRawIntOp (pre, signed, _, _) ->
         "(" ^ (string_of_atom_class (ACRawInt (pre, signed))) ^ ")"
    | FloatOfIntOp pre
    | FloatOfRawIntOp (pre, _, _)
    | FloatOfFloatOp (pre, _) ->
         "(" ^ (string_of_atom_class (ACFloat pre)) ^ ")"
    | PolyOfIntOp
    | PolyOfPointerOp _
    | PolyOfFunctionOp _ ->
         "(" ^ (string_of_atom_class ACPoly) ^ ")"
    | PointerOfPolyOp ptrty ->
         "(" ^ (string_of_atom_class (ACPointer ptrty)) ^ ")"
    | FunctionOfPolyOp funty ->
         "(" ^ (string_of_atom_class (ACFunction funty)) ^ ")"
    | AbsOp _ ->
         "abs"
    | SinOp _ ->
         "sin"
    | CosOp _ ->
         "cos"
    | SqrtOp _ ->
         "sqrt"
    | MemTagOp (ptrty, _) ->
         string_of_ptrty ptrty ^ ".tag"
    | MemSizeOp (ptrty, _) ->
         string_of_ptrty ptrty ^ ".size"
    | MemIndexOp (ptrty, _) ->
         string_of_ptrty ptrty ^ ".index"
    | MemFunIndexOp (funty, _) ->
         string_of_funty funty ^ ".index"
    | MemFunArityTagOp (funty, _) ->
         string_of_funty funty ^ ".aritytag"
    | SafePointerOfIndexOp ptrty ->
         string_of_ptrty ptrty ^ ".safeptrofindex"
    | SafeFunctionOfIndexOp funty ->
         string_of_funty funty ^ ".safefunofindex"
    | UnsafePointerOfIndexOp ptrty ->
         string_of_ptrty ptrty ^ ".unsafeptrofindex"
    | UnsafeFunctionOfIndexOp funty ->
         string_of_funty funty ^ ".unsafefunofindex"
    | SafeIndexOfPointerOp ptrty ->
         string_of_ptrty ptrty ^ ".safeindexofptr"
    | SafeIndexOfFunctionOp funty ->
         string_of_funty funty ^ ".safeindexoffun"
    | UnsafeIndexOfPointerOp ptrty ->
         string_of_ptrty ptrty ^ ".unsafeindexofptr"
    | UnsafeIndexOfFunctionOp funty ->
         string_of_funty funty ^ ".unsafeindexofptr"
    | BaseOfInfixPointerOp ptrty ->
         string_of_ptrty ptrty ^ ".baseofinfix"
    | OffsetOfInfixPointerOp ptrty ->
         string_of_ptrty ptrty ^ ".offsetofinfix"


and string_of_binop op =
   match op with
      PlusOp ac ->
         "+"
    | MinusOp ac ->
         "-"
    | MulOp ac ->
         "*"
    | DivOp ac ->
         "/"
    | RemOp ac ->
         "%"
    | MaxOp ac ->
         "max"
    | MinOp ac ->
         "min"
    | SlOp ac ->
         "<<"
    | ASrOp ac ->
         ">>a"
    | LSrOp ac ->
         ">>l"
    | AndOp ac ->
         "&"
    | OrOp ac ->
         "|"
    | ATan2Op ac ->
         "atan2"
    | XorOp ac ->
         "^"
    | SetBitFieldOp (_, _, ofs, len) ->
         (Printf.sprintf "setfield{%d,%d}" (Int32.to_int ofs) (Int32.to_int len))
    | MemOp (ac, op) ->
         Printf.sprintf "mem[%s](%s)" (string_of_atom_class ac) (string_of_binop_brack op)
    | EqOp ac ->
         "="
    | NeqOp ac ->
         "!="
    | LtOp ac ->
         "<"
    | LeOp ac ->
         "<="
    | GtOp ac ->
         ">"
    | GeOp ac ->
         ">="
    | CmpOp ac ->
         "<=>"
    | InfixOfBaseOffsetOp ptrty ->
         string_of_ptrty ptrty ^ ".infixofbaseoffset"
    | PlusPointerOp ptrty ->
         "+{" ^ string_of_ptrty ptrty ^ "}"


and pp_print_unop buf op a =
   let strop = string_of_unop op in
   let _ =
      pp_open_hvbox buf tabstop;
      match op with
         UMinusOp _
       | NotOp _
       | IntOfFloatOp _
       | RawIntOfFloatOp _
       | RawIntOfIntOp _
       | IntOfRawIntOp _
       | RawIntOfRawIntOp _
       | FloatOfIntOp _
       | FloatOfRawIntOp _
       | FloatOfFloatOp _
       | PolyOfIntOp
       | IntOfPolyOp
       | PolyOfPointerOp _
       | PointerOfPolyOp _
       | PolyOfFunctionOp _
       | FunctionOfPolyOp _
       | Int32OfPointerOp _ ->
            pp_print_unop_atom buf strop a
       | BitFieldOp _
       | AbsOp _
       | SinOp _
       | CosOp _
       | SqrtOp _
       | MemTagOp _
       | MemSizeOp _
       | MemIndexOp _
       | MemFunIndexOp _
       | MemFunArityTagOp _
       | SafePointerOfIndexOp _
       | SafeFunctionOfIndexOp _
       | UnsafePointerOfIndexOp _
       | UnsafeFunctionOfIndexOp _
       | SafeIndexOfPointerOp _
       | SafeIndexOfFunctionOp _
       | UnsafeIndexOfPointerOp _
       | UnsafeIndexOfFunctionOp _
       | BaseOfInfixPointerOp _
       | OffsetOfInfixPointerOp _ ->
            pp_print_unop_fun buf strop a
   in
      pp_close_box buf ()

and pp_print_binop buf op a1 a2 =
   let strop = string_of_binop op in
   let _ =
      pp_open_hvbox buf tabstop;
      match op with
         PlusOp ac
       | MinusOp ac
       | MulOp ac
       | DivOp ac
       | RemOp ac
       | SlOp ac
       | ASrOp ac
       | LSrOp ac
       | AndOp ac
       | OrOp ac
       | XorOp ac
       | EqOp ac
       | NeqOp ac
       | LtOp ac
       | LeOp ac
       | GtOp ac
       | GeOp ac
       | CmpOp ac ->
            pp_print_binop_atom buf ac strop a1 a2
       | MaxOp _
       | MinOp _
       | ATan2Op _
       | SetBitFieldOp _
       | PlusPointerOp _
       | InfixOfBaseOffsetOp _ ->
            pp_print_binop_fun buf strop a1 a2
       | MemOp (ac, op) ->
            pp_print_binop_brack ac op a1 a2 buf
   in
      pp_close_box buf ()

(*
 * Print the atom.
 *)
and pp_print_atom buf a =
   match a with
      AtomInt i ->
         fprintf buf "%a %d" pp_print_atom_class ACInt i
    | AtomRawInt i ->
         fprintf buf "%a %s"
            pp_print_atom_class (ACRawInt (Rawint.precision i, Rawint.signed i))
            (Rawint.to_string i)
    | AtomFloat x ->
         fprintf buf "%a %s"
            pp_print_atom_class (ACFloat (Rawfloat.precision x))
            (Rawfloat.to_string x)
    | AtomVar (ac, v)
    | AtomFunVar (ac, v) ->
         fprintf buf "%a: %a"
            pp_print_atom_class ac
            pp_print_symbol v
    | AtomUnop (op, a) ->
         pp_print_unop buf op a
    | AtomBinop (op, a1, a2) ->
         pp_print_binop buf op a1 a2


let pp_print_atoms buf atoms =
   pp_print_sep_list "," pp_print_atom buf atoms

(*
 * Allocation.
 *)
let pp_print_alloc_op buf op =
   match op with
      AllocUnion (i, args) ->
         fprintf buf "@[<hv 3>alloc(tag:%d" i;
         List.iter (fun a ->
               fprintf buf ",@ %a" pp_print_atom a) args;
         fprintf buf ")@]"
    | AllocArray args ->
         fprintf buf "@[<hv 3>alloc_array(%a)@]"
            (pp_print_sep_list "," pp_print_atom) args
    | AllocMArray (args, a) ->
         fprintf buf "alloc_marray(%a)"
            (pp_print_sep_list "," pp_print_atom) (args @ [a])
    | AllocTuple args ->
         fprintf buf "@[<hv 3>alloc_tuple(%a)@]"
            (pp_print_sep_list "," pp_print_atom) args
    | AllocMalloc a ->
         fprintf buf "malloc(%a)" pp_print_atom a


(*
 * Relative ops.
 *)
let pp_print_relop op a1 a2 buf =
   match op with
      REqOp ac ->
         pp_print_binop_atom buf ac "==" a1 a2
    | RNeqOp ac ->
         pp_print_binop_atom buf ac "!=" a1 a2
    | RLtOp ac ->
         pp_print_binop_atom buf ac "<" a1 a2
    | RLeOp ac ->
         pp_print_binop_atom buf ac "<=" a1 a2
    | RGtOp ac ->
         pp_print_binop_atom buf ac ">" a1 a2
    | RGeOp ac ->
         pp_print_binop_atom buf ac ">=" a1 a2
    | RAndOp ac ->
         pp_print_binop_atom buf ac "&" a1 a2

(*
 * Tailcall.
 *)
let pp_print_tailcall_op buf op =
   match op with
      DirectCall ->
         ()
    | IndirectCall ->
         pp_print_string buf "(indirect)"
    | ExternalCall ->
         pp_print_string buf "(external)"

(*
 * Debugging.
 *)
let pp_print_debug_line = pp_print_location

let pp_print_debug_vars buf vars =
   List.iter (fun (v1, ty, v2) ->
         fprintf buf "@ @[<hv 3># %a :@ %a = %a@]"
            pp_print_symbol v1
            pp_print_type ty
            pp_print_symbol v2) vars

let pp_print_debug buf (line, vars) =
   fprintf buf "@[<hv 0>%a@ %a@]"
      pp_print_debug_line line
      pp_print_debug_vars vars

(*
 * Reserve var list.
 *)
let pp_print_reserve_vars ty f vars buf =
   fprintf buf "/*@[<hv 0>%s: %a@ (%a)@ */@]"
      ty
      pp_print_symbol f
      (pp_print_sep_list "," pp_print_symbol) vars

let pp_print_reserve ty (f, vars, mem, ptr) cont buf =
   fprintf buf "@[<hv 0>%s(%a,@ %a)%t);@]@ %t@ "
      ty
      pp_print_atom mem
      pp_print_atom ptr
      cont
      (pp_print_reserve_vars ty f vars)

(*
 * "Let" opener.
 *)
let pp_print_let_ac_open buf v ac =
   fprintf buf "@[<hv 3>let @[<hv 0>%a :@ %a =@]@ "
      pp_print_symbol v
      pp_print_atom_class ac

let pp_print_let_open buf v =
   fprintf buf "@[<hv 3>let %a =@ " pp_print_symbol v

let rec pp_print_let_close limit buf e =
   fprintf buf " in@]@ ";
   pp_print_expr limit buf e

and pp_print_expr_now limit buf e =
   match dest_exp_core e with
      LetAtom (v, ac, a, e) ->
         pp_print_let_ac_open buf v ac;
         pp_print_atom buf a;
         pp_print_let_close limit buf e

    | LetExternal (v, ac, s, args, e) ->
         pp_print_let_ac_open buf v ac;
         fprintf buf "%s(%a)" s
            (pp_print_sep_list "," pp_print_atom) args;
         pp_print_let_close limit buf e

    | LetExternalReserve (rinfo, v, ac, s, args, e) ->
         pp_print_reserve "ExternalReserve" rinfo (fun _ -> ()) buf;
         pp_print_let_ac_open buf v ac;
         fprintf buf "%s(%a)" s
            (pp_print_sep_list "," pp_print_atom) args;
         pp_print_let_close limit buf e

    | TailCall (op, f, args) ->
         pp_print_tailcall_op buf op;
         fprintf buf "%a(%a)"
            pp_print_symbol f
            (pp_print_sep_list "," pp_print_atom) args

    | PrintDebug (label, f, args, e) ->
         fprintf buf "@[<hov 3>Debug[%a]:@ %a(%a);@]@ "
            pp_print_symbol label
            pp_print_symbol f
            (pp_print_sep_list "," pp_print_atom) args;
         pp_print_expr limit buf e

    | Match (a, ac, cases) ->
         fprintf buf "@[<hv 0>match %a : %a with"
            pp_print_atom a
            pp_print_atom_class ac;
         List.iter (fun (set, e) ->
               fprintf buf "@ @[<hv 3> | %a ->@ %a@]"
                  pp_print_set set
                  (pp_print_expr limit) e) cases;
         fprintf buf "@]"

    | IfThenElse (op, a1, a2, e1, e2) ->
         fprintf buf "@[<hv 0>@[<hv 3>if %t then@ %a@]@ @[<hv 3>else@ %a@]@]"
            (pp_print_relop op a1 a2)
            (pp_print_expr limit) e1
            (pp_print_expr limit) e2

    | IfType (obj, names, name, v, e1, e2) ->
         fprintf buf "@[<hv 3>typecase(%a, %a)@ @[<hv 3>%a, %a ->@ %a@]@ @[<hv 3> | _ ->@ %a@]@]"
            pp_print_atom obj
            pp_print_atom names
            pp_print_atom name
            pp_print_symbol v
            (pp_print_expr limit) e1
            (pp_print_expr limit) e2

    | Reserve (rinfo, e) ->
         pp_print_reserve "Reserve" rinfo (fun _ -> ()) buf;
         pp_print_expr limit buf e

    | LetAlloc (v, op, e) ->
         pp_print_let_open buf v;
         fprintf buf "@[<hv 3>%a@]" pp_print_alloc_op op;
         pp_print_let_close limit buf e

    | SetMem (ac, op, a1, a2, a3, e) ->
         fprintf buf "@[<hov 3>@[<hv 0>%t :@ %a <-@]@ %a;@]@ "
            (pp_print_binop_brack ac op a1 a2)
            pp_print_atom_class ac
            pp_print_atom a3;
         pp_print_expr limit buf e

    | SetGlobal (ac, v, a, e) ->
         fprintf buf "@[<hv 3>%a :@ %a <-g@ %a;@]@ "
            pp_print_symbol v
            pp_print_atom_class ac
            pp_print_atom a;
         pp_print_expr limit buf e

    | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
         fprintf buf "@[<hv 3>%a: %s.boundscheck(%a)@]@ "
            pp_print_symbol label
            (string_of_ptrty ptrty)
            (pp_print_sep_list "," pp_print_atom) [ptr; start; stop];
         pp_print_expr limit buf e

    | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
         fprintf buf "@[<hv 3>%a: %s.lowerboundscheck(%a)@]@ "
            pp_print_symbol label
            (string_of_ptrty ptrty)
            (pp_print_sep_list "," pp_print_atom) [ptr; start];
         pp_print_expr limit buf e

    | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
         fprintf buf "@[<hv 3>%a: %s.upperboundscheck(%a)@]@ "
            pp_print_symbol label
            (string_of_ptrty ptrty)
            (pp_print_sep_list "," pp_print_atom) [ptr; stop];
         pp_print_expr limit buf e

    | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
         pp_print_let_ac_open buf v ac;
         fprintf buf "@[<hv 3>%a: %s.indexcheck(%a)@]"
            pp_print_symbol label
            (string_of_ptrty ptrty)
            pp_print_atom index;
         pp_print_let_close limit buf e

    | FunctionIndexCheck (label, funty, v, ac, index, e) ->
         pp_print_let_ac_open buf v ac;
         fprintf buf "@[<hv 3>%a: %s.indexcheck(%a)@]"
            pp_print_symbol label
            (string_of_funty funty)
            pp_print_atom index;
         pp_print_let_close limit buf e

    | Memcpy (op, dst_base, dst_off, src_base, src_off, size, e) ->
         fprintf buf "@[<hv 3>memcpy(%t, %t, %a);@ @]"
            (pp_print_binop_brack ACInt op dst_base dst_off)
            (pp_print_binop_brack ACInt op src_base src_off)
            pp_print_atom size;
         pp_print_expr limit buf e

    | Debug (info, e) ->
         pp_print_debug buf info;
         pp_print_space buf ();
         pp_print_expr limit buf e

    | CommentFIR (e', e) ->
         if !mir_comments_fir then
            fprintf buf "@[<hv 3>/* FIR:@ %a*/@]@ "
               (pp_print_expr_size 1) e';
         pp_print_expr limit buf e

    | SpecialCall (TailSysMigrate (id, loc_ptr, loc_off, f, args)) ->
         fprintf buf "$tail sysmigrate(%d, %a, %a, %a)"
            id
            (pp_print_sep_list "," pp_print_atom) [loc_ptr; loc_off]
            pp_print_symbol f
            (pp_print_sep_list "," pp_print_atom) args

    | SpecialCall (TailAtomic (f, c, args)) ->
         fprintf buf "$tail atomic(%a, %a)"
            pp_print_symbol f
            (pp_print_sep_list "," pp_print_atom) args

    | SpecialCall (TailAtomicRollback (level, c)) ->
         fprintf buf "$tail atomicrollback(%a)"
            (pp_print_sep_list "," pp_print_atom) [level; c]

    | SpecialCall (TailAtomicCommit (level, f, args)) ->
         fprintf buf "$tail atomiccommit(%a, %a, %a)"
            pp_print_atom level
            pp_print_symbol f
            (pp_print_sep_list "," pp_print_atom) args

    | SysMigrate (label, loc_ptr, loc_off, f, env) ->
         fprintf buf "$sysmigrate(migrate_%d, %a, %a, %a)"
            label
            (pp_print_sep_list "," pp_print_atom) [loc_ptr; loc_off]
            pp_print_symbol f
            pp_print_atom env

    | Atomic (f, i, env) ->
         fprintf buf "$atomic(%a, %a)"
            pp_print_symbol f
            (pp_print_sep_list "," pp_print_atom) [i; env]

    | AtomicRollback (level, i) ->
         fprintf buf "$atomic_rollback(%a)"
            (pp_print_sep_list "," pp_print_atom) [level; i]

    | AtomicCommit (level, e) ->
         fprintf buf "$atomic_commit(%a)@ " pp_print_atom level;
         pp_print_expr limit buf e

    | CopyOnWrite (rinfo, ptr, e) ->
         pp_print_reserve "CopyOnWrite" rinfo (fun buf ->
               fprintf buf ",@ %a" pp_print_atom ptr) buf;
         pp_print_expr limit buf e

(*
 * Rename all the vars in the expr.
 *)
and pp_print_expr limit buf e =
   match limit with
      None ->
         pp_print_expr_now None buf e
    | Some i ->
         if i <= 0 then
            fprintf buf "...@ "
         else
            pp_print_expr_now (Some (pred i)) buf e

and pp_print_function f (line, vars, body) buf =
   fprintf buf "%a@ (%a)(%a) =@ "
      pp_print_debug_line line
      pp_print_symbol f
      (pp_print_sep_list "," (fun buf (v, ac) ->
             fprintf buf "%a %a"
                pp_print_atom_class ac
                pp_print_symbol v)) vars;
   pp_print_expr None buf body

let pp_print_expr_size i = pp_print_expr (Some i)
let pp_print_expr = pp_print_expr None

(*
 * Info printing.
 *)
let pp_print_import_info buf info =
   match info with
      ImportGlobal ->
         pp_print_string buf "<global>"
    | ImportFun (b, orig_args, asm_args) ->
         if b then
            pp_print_string buf "varargs: ";
         fprintf buf "%a@ : %a"
            Fir_print.pp_print_import_args orig_args
            pp_print_atom_classes asm_args

(************************************************************************
 * GLOBAL FUNCTIONS
 ************************************************************************)

let pp_print_prog buf
    { prog_import       = import;
      prog_export       = export;
      prog_globals      = globals;
      prog_funs         = funs;
      prog_global_order = global_order;
      prog_fun_order    = fun_order;
      prog_arity_tags   = arity_tags;
    } =

   pp_open_hvbox buf 0;

   fprintf buf "@[<hv 3>Program imports:";
   SymbolTable.iter (fun v info ->
         let { import_name = s;
               import_info = info
             } = info
         in
            fprintf buf "@ @[<hv 3>%a = \"%s\"@ : %a;@]"
               pp_print_symbol v
               (String.escaped s)
               pp_print_import_info info) import;

   fprintf buf "@]@ @[<hv 3>Program exports:";
   SymbolTable.iter (fun v info ->
         let { export_name = s;
               export_is_fun = funp
             } = info
         in
            fprintf buf "@ %a : %s = \"%s\""
               pp_print_symbol v
               (if funp then "fun" else "glob")
               (String.escaped s)) export;

   fprintf buf "@]@ @[<hv 3>Program globals:";
   SymbolTable.iter (fun v init ->
         fprintf buf "@ @[<hv 3>%a =@ " pp_print_symbol v;
         (match init with
             InitAtom a ->
                pp_print_atom buf a
           | InitAlloc op ->
                pp_print_alloc_op buf op
           | InitRawData (_, s) ->
                Fir_print.pp_print_raw_string buf s);
         pp_close_box buf ()) globals;

   fprintf buf "@]@ @[<hv 3>Program arity tags:";
   ArityTable.iter (fun arity (v, index) ->
         fprintf buf "@ @[<hv 3>%a =@ %s[%s]@]"
            pp_print_symbol v
            (string_of_arity arity)
            (Int32.format "0x%08x" index)) arity_tags;

   fprintf buf "@]@ @[<hv 3>Program global order:";
   List.iter (fun v ->
         fprintf buf "@ %a" pp_print_symbol v) global_order;

   fprintf buf "@]@ @[<hv 3>Program function order:";
   List.iter (fun v ->
         fprintf buf "@ %a" pp_print_symbol v) fun_order;

   fprintf buf "@]@ @[<hv 3>Program funs:";
   SymbolTable.iter (fun f fund ->
         fprintf buf "@ @[<hv 3>%t@]" (pp_print_function f fund)) funs;
   fprintf buf "@]@]"


let debug_prog debug prog =
   eprintf "@[<hv 0>*** MIR: %s@ %a@]@."
      debug
      pp_print_prog prog


(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
