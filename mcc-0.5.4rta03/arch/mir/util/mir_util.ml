(*
   Utilities for FIR->MIR conversion
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Symbol
open Sizeof_const

open Frame_type

open Fir_set
open Fir_pos
open Fir_exn
open Fir_env
open Fir_type

open Mir
open Mir_util_sig

module Pos = MakePos (struct let name = "Mir_util" end)
open Pos


module MirUtil (Frame : BackendSig) : MirUtilSig =
struct


   (***  Definitions  ***)


   (* ML ints. *)
   let signed_int = true
   let unsigned_int = false

   (* Native ints. *)
   let ac_native_int = ACRawInt (precision_native_int, true)
   let zero_native_int = Rawint.of_int precision_native_int true 0
   let one_native_int = Rawint.of_int precision_native_int true 1

   let ac_native_unsigned = ACRawInt (precision_native_int, false)
   let zero_native_unsigned = Rawint.of_int precision_native_int false 0
   let one_native_unsigned = Rawint.of_int precision_native_int false 1
   let zero_native_unsigned_set = RawIntSet.of_point zero_native_unsigned
   let one_native_unsigned_set = RawIntSet.of_point one_native_unsigned

   let ac_native_bool = ac_native_int
   let native_false_value = AtomRawInt zero_native_int
   let native_true_value = AtomRawInt one_native_int
   let native_false_set = RawIntSet.of_point zero_native_int
   let native_true_set = RawIntSet.subtract_point (RawIntSet.max_set precision_native_int true) zero_native_int
   let native_true_set_alt = RawIntSet.of_point one_native_int

   let hash_native_unsigned = AtomVar (ACRawInt (precision_native_int, false), Frame.hash_index_label)
   let hash_fun_native_unsigned = AtomVar (ACRawInt (precision_native_int, false), Frame.hash_fun_index_label)
   let ptbl_pointer_native_unsigned = Rawint.of_int32 precision_native_int false Frame.ptable_pointer_mask

   (* Pointers. *)
   let sizeof_pointer_raw = Rawint.of_int precision_native_int false sizeof_pointer
   let ac_block_pointer = ACPointer PtrBlock
   let ac_aggr_pointer = ACPointer PtrAggr
   let ac_pointer_base = ACPointer PtrAggr   (* ... for pointer_base and function_base vars *)
   let one_pointer = Rawint.of_int precision_pointer false 1
   let zero_pointer = Rawint.of_int precision_pointer false 0
   let zero_pointer_set = RawIntSet.of_point zero_pointer
   let one_pointer_set = RawIntSet.subtract_point (RawIntSet.max_set precision_pointer false) zero_pointer
   let unsigned_true_set = one_pointer_set
   let unsigned_false_set = zero_pointer_set

   (* Runtime constants. *)
   let raw_index_shift = Rawint.of_int32 precision_native_int false Frame.index_shift32

   (* Subscripts are native ints. *)
   let ac_subscript = ac_native_int

   (* Tags are ML ints. *)
   let ac_tag = ACInt


   (* Powers of 2. *)
   let is_power2 i =
      let rec search k =
         if k > i || k = (1 lsl 30) then
            false
         else
            k = i || search (k lsl 1)
      in
         search 1


   (***  Type Expansion  ***)


   (* tenv_expand
      Expand the types, blatantly ignoring quantifiers.  *)
   let rec tenv_expand tenv pos ty =
      let pos = string_pos "tenv_expand" pos in
      let ty = Fir_type.tenv_expand_dir tenv pos ty in
         match ty with
            Fir.TyExists (_, ty)
          | Fir.TyAll (_, ty) ->
               tenv_expand tenv pos ty
          | _ ->
               ty


   (***  Atom Classes  ***)


   (* ptrty_of_sub_block
      Get the block type. *)
   let ptrty_of_sub_block op =
      match op with
         Fir.BlockSub
       | Fir.TupleSub
       | Fir.RawTupleSub ->
            PtrBlock
       | Fir.RawDataSub ->
            PtrAggr


   (* atom_class_of_type
      Get the atom class of a type.  *)
   let rec atom_class_of_type tenv pos ty =
      let pos = string_pos "atom_class_of_type" pos in
         match tenv_expand tenv pos ty with
            Fir.TyInt ->
               ACInt
          | Fir.TyEnum _
          | Fir.TyTag _ ->
               ac_native_int
          | Fir.TyRawInt (pre, signed) ->
               ACRawInt (pre, signed)
          | Fir.TyFloat pre ->
               ACFloat pre
          | Fir.TyFun (args, _) ->
               ACFunction (FunArgs (List.map (atom_class_of_type tenv pos) args))
          | Fir.TyRawData
          | Fir.TyFrame _ ->
               ACPointer PtrAggr
          | Fir.TyPointer op ->
               ACPointerInfix (ptrty_of_sub_block op)
          | Fir.TyUnion _
          | Fir.TyTuple _
          | Fir.TyArray _
          | Fir.TyProject _
          | Fir.TyCase _
          | Fir.TyObject _
          | Fir.TyDTuple _ ->
               ACPointer PtrBlock
          | Fir.TyVar _ ->
               ACPoly
          | Fir.TyAll (_, ty)
          | Fir.TyExists (_, ty) ->
               atom_class_of_type tenv pos ty
          | Fir.TyApply _ ->
               raise (FirException (pos, InternalError "atom_class_of_type"))
          | Fir.TyDelayed ->
               raise (FirException (pos, StringError "atom_class_of_type: TyDelayed is not allowed"))


   (* funty_of_function_type
      Get the funty object corresponding to a FIR function type.  *)
   let funty_of_function_type tenv pos ty =
      let pos = string_pos "funty_of_function_type" pos in
      let _, args, _ = dest_all_fun_type_dir tenv pos ty in
         FunArgs (List.map (atom_class_of_type tenv pos) args)


   (* atom_class_of_unop_args
      Class of unary operation.  This returns two atom classes; the atom
      class of the result, and the expected atom class of the argument for
      this operator.  For most arithmetic operators, the two atom classes
      will agree.  *)
   let atom_class_of_unop_args = function
      UMinusOp ac
    | AbsOp    ac
    | NotOp    ac ->
         ac, ac

    | SinOp    pre
    | CosOp    pre
    | SqrtOp   pre ->
         ACFloat pre, ACFloat pre

    | BitFieldOp (dpre, dsigned, _, _) ->
         ACRawInt (dpre, dsigned), ac_native_int

    | IntOfFloatOp spre ->
         ACInt, ACFloat spre
    | FloatOfIntOp dpre ->
         ACFloat dpre, ACInt
    | RawIntOfRawIntOp (dpre, dsigned, spre, ssigned) ->
         ACRawInt (dpre, dsigned), ACRawInt (spre, ssigned)
    | FloatOfFloatOp (dpre, spre) ->
         ACFloat dpre, ACFloat spre
    | RawIntOfFloatOp (dpre, dsigned, spre) ->
         ACRawInt (dpre, dsigned), ACFloat spre
    | FloatOfRawIntOp (dpre, spre, ssigned) ->
         ACFloat dpre, ACRawInt (spre, ssigned)
    | RawIntOfIntOp (dpre, dsigned) ->
         ACRawInt (dpre, dsigned), ACInt
    | IntOfRawIntOp (spre, ssigned) ->
         ACInt, ACRawInt (spre, ssigned)

    | PolyOfIntOp ->
         ACPoly, ACInt
    | IntOfPolyOp ->
         ACInt, ACPoly
    | PolyOfPointerOp ptrty ->
         ACPoly, ACPointer ptrty
    | PointerOfPolyOp ptrty ->
         ACPointer ptrty, ACPoly
    | PolyOfFunctionOp funty ->
         ACPoly, ACFunction funty
    | FunctionOfPolyOp funty ->
         ACFunction funty, ACPoly

    | Int32OfPointerOp ptrty ->
         ac_native_unsigned, ACPointer ptrty

    | MemTagOp          (ptrty, ac) ->
         ac, ACPointer ptrty
    | MemSizeOp         (ptrty, ac)
    | MemIndexOp        (ptrty, ac) ->
         ac, ACPointer ptrty
    | MemFunIndexOp     (funty, ac) ->
         ac, ACFunction funty
    | MemFunArityTagOp  (funty, ac) ->
         ac, ACFunction funty

    | SafePointerOfIndexOp ptrty
    | UnsafePointerOfIndexOp ptrty ->
         ACPointer ptrty, ac_native_unsigned
    | SafeFunctionOfIndexOp funty
    | UnsafeFunctionOfIndexOp funty ->
         ACFunction funty, ac_native_unsigned
    | SafeIndexOfPointerOp ptrty
    | UnsafeIndexOfPointerOp ptrty ->
         ac_native_unsigned, ACPointer ptrty
    | SafeIndexOfFunctionOp funty
    | UnsafeIndexOfFunctionOp funty ->
         ac_native_unsigned, ACFunction funty

    | BaseOfInfixPointerOp ptrty ->
         ACPointer ptrty, ACPointerInfix ptrty
    | OffsetOfInfixPointerOp ptrty ->
         ac_native_unsigned, ACPointerInfix ptrty


   (* atom_class_of_unop
      Same as atom_class_of_unop_args, but this ONLY returns the
      result atom class for the atom. *)
   let atom_class_of_unop op =
      let ac, _ = atom_class_of_unop_args op in
         ac


   (* atom_class_of_binop_args
      Class of a binary operation.  This returns three atom classes; the first
      is the atom class of the result, and the last two are the expected atom
      classes for the arguments to this operator.  For arithmetic operators,
      the atom classes usually agree.  *)
   let atom_class_of_binop_args = function
      PlusOp  ac
    | MinusOp ac
    | MulOp   ac
    | DivOp   ac
    | RemOp   ac
    | MaxOp   ac
    | MinOp   ac ->
         ac, ac, ac

    | SlOp    ac
    | ASrOp   ac
    | LSrOp   ac
    | AndOp   ac
    | OrOp    ac
    | XorOp   ac ->
         ac, ac, ac

    | ATan2Op pre ->
         ACFloat pre, ACFloat pre, ACFloat pre

    | SetBitFieldOp (pre, signed, _, _) ->
         ACRawInt (pre, signed), ac_native_int, ac_native_int

    | MemOp (ac, ptrty) ->
         ac, ACPointer ptrty, ac_native_unsigned

    | EqOp     ac
    | NeqOp    ac
    | LtOp     ac
    | LeOp     ac
    | GtOp     ac
    | GeOp     ac ->
         ac_native_bool, ac, ac
    | CmpOp    ac ->
         ACInt, ac, ac

    | PlusPointerOp ptrty ->
         let ac = ACPointerInfix ptrty in
            ac, ac, ac_native_unsigned

    | InfixOfBaseOffsetOp ptrty ->
         ACPointerInfix ptrty, ACPointer ptrty, ac_native_unsigned


   (* atom_class_of_binop
      Same as atom_class_of_binop_args, but this ONLY returns the
      result atom class for the atom. *)
   let atom_class_of_binop op =
      let ac, _, _ = atom_class_of_binop_args op in
         ac


   (* atom_class_of_relop
      Returns the atom class the relop is intended to operate on.
      NOTE that the return type of relops is always ac_native_bool. *)
   let atom_class_of_relop op =
      match op with
         REqOp  ac
       | RNeqOp ac
       | RLtOp  ac
       | RLeOp  ac
       | RGtOp  ac
       | RGeOp  ac
       | RAndOp ac ->
            ac


   (* atom_class_of_atom
      Get atom class of an MIR atom.  *)
   let rec atom_class_of_atom = function
      AtomInt _ ->
         ACInt
    | AtomRawInt i ->
         ACRawInt (Rawint.precision i, Rawint.signed i)
    | AtomFloat x ->
         ACFloat (Rawfloat.precision x)
    | AtomVar (ac, _)
    | AtomFunVar (ac, _) ->
         ac
    | AtomUnop (op, _) ->
         atom_class_of_unop op
    | AtomBinop (op, _, _) ->
         atom_class_of_binop op

   and atom_class_of_pointer a =
      match atom_class_of_atom a with
         ACPointer op ->
            ACPointerInfix op
       | _ ->
            raise (Invalid_argument "atom_class_of_pointer")


   (* type_of_atom_class
      Type of an MIR atom class.  *)
   let type_of_atom_class = function
      ACInt ->
         Fir.TyInt
    | ACPoly ->
         Fir.TyVar (new_symbol_string "poly")
    | ACRawInt (pre, signed) ->
         Fir.TyRawInt (pre, signed)
    | ACFloat pre ->
         Fir.TyFloat pre
    | ACPointer _
    | ACFunction _ ->
         Fir.TyRawInt (Rawint.Int32, false)
    | ACPointerInfix _ ->
         Fir.TyRawInt (Rawint.Int64, false)


   (* type_of_atom
      Type of an MIR atom.  *)
   let type_of_atom venv pos a =
      let pos = string_pos "type_of_atom" pos in
         match a with
            AtomInt _ ->
               Fir.TyInt
          | AtomRawInt i ->
               let pre = Rawint.precision i in
               let signed = Rawint.signed i in
                  Fir.TyRawInt (pre, signed)
          | AtomFloat x ->
               Fir.TyFloat (Rawfloat.precision x)
          | AtomVar (_, v)
          | AtomFunVar (_, v) ->
               venv_lookup venv pos v
          | AtomUnop (op, _) ->
               type_of_atom_class (atom_class_of_unop op)
          | AtomBinop (op, _, _) ->
               type_of_atom_class (atom_class_of_binop op)


   (* atom_class_of_fir_subop
      Get atom class of a subscript operator.  *)
   let rec atom_class_of_fir_subop tenv pos ty subop =
      let pos = string_pos "atom_class_of_fir_subop" pos in
      let { Fir.sub_block = sub_block;
            Fir.sub_value = sub_value
          } = subop
      in
         match sub_block, sub_value with
            _, Fir.PolySub ->
               ACPoly
          | _, Fir.EnumSub _
          | _, Fir.TagSub _ ->
               ac_native_int
          | _, Fir.IntSub ->
               ACInt
          | Fir.TupleSub, Fir.BlockPointerSub
          | Fir.RawTupleSub, Fir.BlockPointerSub
          | Fir.BlockSub, Fir.BlockPointerSub ->
               ACPointer PtrBlock
          | Fir.TupleSub, Fir.FunctionSub
          | Fir.RawTupleSub, Fir.FunctionSub
          | Fir.BlockSub, Fir.FunctionSub ->
               (* In this case, we compute the function type using the generic method *)
               atom_class_of_type tenv pos ty
          | _, Fir.RawIntSub (pre, signed) ->
               ACRawInt (pre, signed)
          | _, Fir.RawFloatSub pre ->
               ACFloat pre
          | Fir.RawDataSub, Fir.RawPointerSub
          | Fir.TupleSub, Fir.RawPointerSub
          | Fir.RawTupleSub, Fir.RawPointerSub
          | Fir.BlockSub, Fir.RawPointerSub ->
               ACPointer PtrAggr
          | Fir.RawDataSub, Fir.BlockPointerSub ->
               ACPointer PtrBlock
          | Fir.RawDataSub, Fir.FunctionSub ->
               ACFunction FunAny
          | Fir.RawDataSub, Fir.PointerInfixSub ->
               ACPointerInfix PtrAggr
          | Fir.BlockSub, Fir.PointerInfixSub
          | Fir.TupleSub, Fir.PointerInfixSub
          | Fir.RawTupleSub, Fir.PointerInfixSub ->
               ACPointerInfix PtrBlock


   (* atom_class_of_fir_sub_value
      Get atom class of a subscript value.  *)
   let rec atom_class_of_fir_sub_value tenv pos ty sub_value =
      let pos = string_pos "atom_class_of_fir_sub_value" pos in
         match sub_value with
            Fir.PolySub ->
               ACPoly
          | Fir.EnumSub _
          | Fir.TagSub _ ->
               ac_native_int
          | Fir.IntSub ->
               ACInt
          | Fir.BlockPointerSub ->
               ACPointer PtrBlock
          | Fir.FunctionSub ->
               (* In this case, we compute the function type using the generic method *)
               atom_class_of_type tenv pos ty
          | Fir.RawPointerSub ->
               ACPointer PtrAggr
          | Fir.PointerInfixSub ->
               ACPointerInfix PtrBlock
          | Fir.RawIntSub (pre, signed) ->
               ACRawInt (pre, signed)
          | Fir.RawFloatSub pre ->
               ACFloat pre


   (* atom_class_of_alloc_op
      Get atom class of an alloc operator.  *)
   let atom_class_of_alloc_op = function
      AllocTuple _
    | AllocArray _
    | AllocMArray _
    | AllocUnion _ ->
         ACPointer PtrBlock
    | AllocMalloc _ ->
         ACPointer PtrAggr


   (* atom_class_of_init_exp
      Get atom class of a global variable.  *)
   let atom_class_of_init_exp = function
      InitAtom a ->
         atom_class_of_atom a
    | InitAlloc op ->
         atom_class_of_alloc_op op
    | InitRawData _ ->
         ACPointer PtrAggr


   (* atom_class_may_be_pointer
      Returns true if the AC indicated is a valid data pointer type.
      We conservatively return true for polymorphic values...  *)
   let atom_class_may_be_pointer = function
      ACPointer _
    | ACPointerInfix _
    | ACPoly ->
         true
    | ACFunction _
    | ACRawInt _
    | ACFloat _
    | ACInt ->
         false


   (* sizeof_atom_class
      Returns the size of a single atom.  *)
   let sizeof_atom_class = function
      ACInt ->
         sizeof_int
    | ACRawInt (Rawint.Int8, _)
    | ACRawInt (Rawint.Int16, _)
    | ACRawInt (Rawint.Int32, _) ->
         sizeof_int32
    | ACRawInt (Rawint.Int64, _) ->
         sizeof_int64
    | ACPointer _
    | ACFunction _
    | ACPoly ->
         sizeof_pointer
    | ACPointerInfix _ ->
         2 * sizeof_pointer
    | ACFloat Rawfloat.Single ->
         sizeof_single
    | ACFloat Rawfloat.Double ->
         sizeof_double
    | ACFloat Rawfloat.LongDouble ->
         sizeof_long_double

   let sizeof_atom_class ac = Int32.of_int (sizeof_atom_class ac)

   let sizeof_atom a = sizeof_atom_class (atom_class_of_atom a)

   let sizeof_atom_list atoms =
      List.fold_left (fun sum a -> Int32.add sum (sizeof_atom a)) Int32.zero atoms


end (* struct *)
