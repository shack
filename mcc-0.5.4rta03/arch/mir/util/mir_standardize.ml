(*
 * MIR standardizer.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Mir
open Mir_ds

(************************************************************************
 * ENVIRONMENT
 ************************************************************************)

(*
 * Variable environment for variable renamining.
 *)
let genv_empty = SymbolSet.empty

let venv_empty = SymbolTable.empty

let venv_lookup venv v =
   try SymbolTable.find venv v with
      Not_found ->
         v

(************************************************************************
 * RENAMING
 ************************************************************************)

(*
 * Bind a var.
 *)
let standardize_bind genv venv v =
   if SymbolSet.mem genv v then
      (* Variable already bound elsewhere *)
      let v' = new_symbol v in
         genv, SymbolTable.add venv v v', v'
   else
      SymbolSet.add genv v, venv, v

(*
 * Rename a var.
 *)
let standardize_var venv v =
   venv_lookup venv v

let standardize_vars venv vars =
   List.map (standardize_var venv) vars

(*
 * Rename an atom.
 *)
let rec standardize_atom venv a =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _ ->
         a
    | AtomVar (ac, v) ->
         AtomVar (ac, standardize_var venv v)
    | AtomFunVar (ac, v) ->
         AtomFunVar (ac, standardize_var venv v)
    | AtomUnop (op, a) ->
         AtomUnop (op, standardize_atom venv a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, standardize_atom venv a1, standardize_atom venv a2)

let standardize_atoms venv args =
   List.map (standardize_atom venv) args

(*
 * Rename an allocation.
 *)
let standardize_alloc_op venv op =
   match op with
      AllocTuple args ->
         AllocTuple (standardize_atoms venv args)
    | AllocArray args ->
         AllocArray (standardize_atoms venv args)
    | AllocMArray (args, a) ->
         AllocMArray (standardize_atoms venv args, standardize_atom venv a)
    | AllocUnion (i, args) ->
         AllocUnion (i, standardize_atoms venv args)
    | AllocMalloc a ->
         AllocMalloc (standardize_atom venv a)

(*
 * Rename an initializer.
 *)
let standardize_init venv init =
   match init with
      InitAtom a ->
         InitAtom (standardize_atom venv a)
    | InitAlloc op ->
         InitAlloc (standardize_alloc_op venv op)
    | InitRawData _ ->
         init

(*
 * Rename a tailcall.
 *)
let standardize_tailop venv op =
   match op with
      TailSysMigrate (i, a1, a2, f, args) ->
         TailSysMigrate (i,
                         standardize_atom venv a1,
                         standardize_atom venv a2,
                         standardize_var venv f,
                         standardize_atoms venv args)
    | TailAtomic (v, a, args) ->
         TailAtomic (standardize_var venv v,
                     standardize_atom venv a,
                     standardize_atoms venv args)
    | TailAtomicRollback (level, a) ->
         TailAtomicRollback (standardize_atom venv level, standardize_atom venv a)
    | TailAtomicCommit (level, f, args) ->
         TailAtomicCommit (standardize_atom venv level, standardize_var venv f, standardize_atoms venv args)

(*
 * Rename a reservation.
 *)
let standardize_reserve venv (v, vars, a1, a2) =
   standardize_var venv v,
   standardize_vars venv vars,
   standardize_atom venv a1,
   standardize_atom venv a2

(*
 * Rename debugging info.
 *)
let standardize_debug venv (line, vars) =
   let vars =
      List.map (fun (v1, ty, v2) ->
            v1, ty, standardize_var venv v2) vars
   in
      line, vars

(*
 * Rename an expression.
 *)
let rec standardize_exp genv venv e =
   let loc = loc_of_exp e in
   let genv, e = standardize_exp_core genv venv (dest_exp_core e) in
      genv, make_exp loc e

and standardize_exp_core genv venv e =
   match e with
      LetAtom (v, ac, a, e) ->
         standardize_atom_exp genv venv v ac a e
    | TailCall (op, f, args) ->
         genv, TailCall (op, standardize_var venv f, standardize_atoms venv args)
    | LetExternal (v, ac, s, args, e) ->
         standardize_external_exp genv venv v ac s args e
    | LetExternalReserve (rinfo, v, ac, s, args, e) ->
         standardize_external_reserve_exp genv venv rinfo v ac s args e
    | IfThenElse (op, a1, a2, e1, e2) ->
         standardize_if_exp genv venv op a1 a2 e1 e2
    | Match (a, ac, cases) ->
         standardize_match_exp genv venv a ac cases
    | IfType (a1, a2, a3, v, e1, e2) ->
         standardize_iftype_exp genv venv a1 a2 a3 v e1 e2
    | Reserve (info, e) ->
         let genv, e = standardize_exp genv venv e in
            genv, Reserve (standardize_reserve venv info, e)
    | LetAlloc (v, op, e) ->
         standardize_alloc_exp genv venv v op e
    | SetMem (ac, op, a1, a2, a3, e) ->
         standardize_set_mem_exp genv venv ac op a1 a2 a3 e
    | SetGlobal (ac, v, a, e) ->
         standardize_set_global_exp genv venv ac v a e
    | BoundsCheck (v, ty, ptr, start, stop, e) ->
         standardize_bounds_check_exp genv venv v ty ptr start stop e
    | LowerBoundsCheck (v, ty, ptr, start, e) ->
         standardize_lower_bounds_check_exp genv venv v ty ptr start e
    | UpperBoundsCheck (v, ty, ptr, stop, e) ->
         standardize_upper_bounds_check_exp genv venv v ty ptr stop e
    | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
         standardize_pointer_index_check_exp genv venv label ptrty v ac index e
    | FunctionIndexCheck (label, funty, v, ac, index, e) ->
         standardize_function_index_check_exp genv venv label funty v ac index e
    | Memcpy (op, a1, a2, a3, a4, a5, e) ->
         standardize_memcpy_exp genv venv op a1 a2 a3 a4 a5 e
    | Debug (info, e) ->
         let genv, e = standardize_exp genv venv e in
            genv, Debug (standardize_debug venv info, e)
    | PrintDebug (v1, v2, args, e) ->
         standardize_print_debug_exp genv venv v1 v2 args e
    | CommentFIR (e1, e2) ->
         let genv, e2 = standardize_exp genv venv e2 in
            genv, CommentFIR (e1, e2)
    | SpecialCall op ->
         genv, SpecialCall (standardize_tailop venv op)
    | SysMigrate (id, dptr, doff, f, env) ->
         standardize_sys_migrate_exp genv venv id dptr doff f env
    | Atomic (v, a1, a2) ->
         standardize_atomic_exp genv venv v a1 a2
    | AtomicRollback (level, a) ->
         genv, AtomicRollback (standardize_atom venv level, standardize_atom venv a)
    | AtomicCommit (level, e) ->
         let genv, e = standardize_exp genv venv e in
            genv, AtomicCommit (standardize_atom venv level, e)
    | CopyOnWrite (info, a, e) ->
         standardize_cow_exp genv venv info a e

(*
 * Atom.
 *)
and standardize_atom_exp genv venv v ac a e =
   let a = standardize_atom venv a in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, LetAtom (v', ac, a, e)

(*
 * External call.
 *)
and standardize_external_exp genv venv v ac s args e =
   let args = standardize_atoms venv args in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, LetExternal (v', ac, s, args, e)

and standardize_external_reserve_exp genv venv rinfo v ac s args e =
   let args = standardize_atoms venv args in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
   let rinfo = standardize_reserve venv rinfo in
      genv, LetExternalReserve (rinfo, v', ac, s, args, e)

(*
 * Conditional.
 *)
and standardize_if_exp genv venv op a1 a2 e1 e2 =
   let a1 = standardize_atom venv a1 in
   let a2 = standardize_atom venv a2 in
   let genv, e1 = standardize_exp genv venv e1 in
   let genv, e2 = standardize_exp genv venv e2 in
      genv, IfThenElse (op, a1, a2, e1, e2)

(*
 * Arbitrary match.
 *)
and standardize_match_exp genv venv a ac cases =
   let a = standardize_atom venv a in
   let genv, cases = List.fold_left (fun (genv, cases) (s, e) ->
      let genv, e = standardize_exp genv venv e in
      let cases = (s, e) :: cases in
         genv, cases) (genv, []) cases
   in
   let cases = List.rev cases in
      genv, Match (a, ac, cases)

(*
 * Iftype.
 *)
and standardize_iftype_exp genv venv a1 a2 a3 v e1 e2 =
   let a1 = standardize_atom venv a1 in
   let a2 = standardize_atom venv a2 in
   let a3 = standardize_atom venv a3 in
   let genv, e2 = standardize_exp genv venv e2 in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e1 = standardize_exp genv venv e1 in
      genv, IfType (a1, a2, a3, v', e1, e2)

(*
 * Allocation.
 *)
and standardize_alloc_exp genv venv v op e =
   let op = standardize_alloc_op venv op in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, LetAlloc (v', op, e)

(*
 * SetMem.
 *)
and standardize_set_mem_exp genv venv ac op a1 a2 a3 e =
   let a1 = standardize_atom venv a1 in
   let a2 = standardize_atom venv a2 in
   let a3 = standardize_atom venv a3 in
   let genv, e = standardize_exp genv venv e in
      genv, SetMem (ac, op, a1, a2, a3, e)

and standardize_set_global_exp genv venv ac v a e =
   let v = standardize_var venv v in
   let a = standardize_atom venv a in
   let genv, e = standardize_exp genv venv e in
      genv, SetGlobal (ac, v, a, e)

and standardize_memcpy_exp genv venv op a1 a2 a3 a4 a5 e =
   let a1 = standardize_atom venv a1 in
   let a2 = standardize_atom venv a2 in
   let a3 = standardize_atom venv a3 in
   let a4 = standardize_atom venv a4 in
   let a5 = standardize_atom venv a5 in
   let genv, e = standardize_exp genv venv e in
      genv, Memcpy (op, a1, a2, a3, a4, a5, e)

(*
 * Safety checks.
 *)
and standardize_bounds_check_exp genv venv v ty ptr start stop e =
   let ptr   = standardize_atom venv ptr in
   let start = standardize_atom venv start in
   let stop  = standardize_atom venv stop in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, BoundsCheck (v', ty, ptr, start, stop, e)

and standardize_lower_bounds_check_exp genv venv v ty ptr start e =
   let ptr   = standardize_atom venv ptr in
   let start = standardize_atom venv start in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, LowerBoundsCheck (v', ty, ptr, start, e)

and standardize_upper_bounds_check_exp genv venv v ty ptr stop e =
   let ptr   = standardize_atom venv ptr in
   let stop = standardize_atom venv stop in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, UpperBoundsCheck (v', ty, ptr, stop, e)

and standardize_pointer_index_check_exp genv venv label ptrty v ac index e =
   let index = standardize_atom venv index in
   let genv, venv, label' = standardize_bind genv venv label in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, PointerIndexCheck (label', ptrty, v', ac, index, e)

and standardize_function_index_check_exp genv venv label funty v ac index e =
   let index = standardize_atom venv index in
   let genv, venv, label' = standardize_bind genv venv label in
   let genv, venv, v' = standardize_bind genv venv v in
   let genv, e = standardize_exp genv venv e in
      genv, FunctionIndexCheck (label', funty, v', ac, index, e)

(*
 * Debugging.
 *)
and standardize_print_debug_exp genv venv v1 v2 args e =
   let v1 = standardize_var venv v1 in
   let v2 = standardize_var venv v2 in
   let args = standardize_atoms venv args in
   let genv, e = standardize_exp genv venv e in
      genv, PrintDebug (v1, v2, args, e)

(*
 * Migrate.
 *)
and standardize_sys_migrate_exp genv venv id dptr doff f env =
   let dptr = standardize_atom venv dptr in
   let doff = standardize_atom venv doff in
   let f    = standardize_var  venv f in
   let env  = standardize_atom venv env in
      genv, SysMigrate (id, dptr, doff, f, env)

(*
 * Atomic.
 *)
and standardize_atomic_exp genv venv f a1 a2 =
   let f  = standardize_var  venv f in
   let a1 = standardize_atom venv a1 in
   let a2 = standardize_atom venv a2 in
      genv, Atomic (f, a1, a2)

(*
 * Copy-on-write.
 *)
and standardize_cow_exp genv venv info a e =
   let info = standardize_reserve venv info in
   let a = standardize_atom venv a in
   let genv, e = standardize_exp genv venv e in
      genv, CopyOnWrite (info, a, e)

(*
 * Rename the vars in a program.
 *)
let standardize_prog prog =
   let { prog_file         = file;
         prog_import       = import;
         prog_export       = export;
         prog_globals      = globals;
         prog_funs         = funs;
         prog_mprog        = mprog;
         prog_global_order = global_order;
         prog_fun_order    = fun_order;
         prog_arity_tags   = arity_tags;
       } = prog
   in

   (* Make new names for the program parts *)
   let genv = genv_empty in
   let venv = venv_empty in
   let genv, venv =
      SymbolTable.fold (fun (genv, venv) v _ ->
         let genv, venv, _ = standardize_bind genv venv v in
            genv, venv) (genv, venv) import
   in
   let genv, venv =
      SymbolTable.fold (fun (genv, venv) v _ ->
         let genv, venv, _ = standardize_bind genv venv v in
            genv, venv) (genv, venv) globals
   in

   (* WARNING:  We _can't_ rename the funs because of the program ordering *)

   (* Rename the imports *)
   let import =
      SymbolTable.fold (fun import v info ->
         let v = venv_lookup venv v in
            SymbolTable.add import v info) SymbolTable.empty import
   in

   (* Rename the exports *)
   let export =
      SymbolTable.fold (fun export v info ->
         let v = venv_lookup venv v in
            SymbolTable.add export v info) SymbolTable.empty export
   in

   (* Rename the globals *)
   let globals =
      SymbolTable.fold (fun globals v init ->
         let v = venv_lookup venv v in
         let init = standardize_init venv init in
            SymbolTable.add globals v init) SymbolTable.empty globals
   in

   (* Rename the funs *)
   let genv, funs =
      SymbolTable.fold (fun (genv, funs) f (line, vars, e) ->
         let f = venv_lookup venv f in
         let genv, venv, vars =
            List.fold_left (fun (genv, venv, vars) (v, ac) ->
               let genv, venv, v' = standardize_bind genv venv v in
               let vars = (v', ac) :: vars in
                  genv, venv, vars) (genv, venv, []) vars
         in
         let genv, e = standardize_exp genv venv e in
            genv, SymbolTable.add funs f (line, List.rev vars, e)) (genv, SymbolTable.empty) funs
   in
      (* Rebuild the prog *)
      { prog_file          = file;
        prog_import        = import;
        prog_export        = export;
        prog_globals       = globals;
        prog_funs          = funs;
        prog_mprog         = mprog;
        prog_global_order  = global_order;
        prog_fun_order     = fun_order;
        prog_arity_tags    = arity_tags;
      }

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
