(*
 * Normal MIR exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mir

(*
 * Exception values.
 *)
type mir_error =
   UnboundVar of var
 | UnboundType of ty_var
 | ArityMismatch of int * int
 | StringError of string
 | StringIntError of string * int
 | StringVarError of string * var
 | StringAtomError of string * atom
 | StringTypeError of string * ty
 | StringIntTypeError of string * int * ty
 | StringFormatError of string * (Format.formatter -> unit)
 | StringAtomClassError of string * atom_class
 | ImplicitCoercion of atom
 | ImplicitCoercion2 of atom_class * atom_class
 | NotImplemented of string
 | InternalError of string

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
