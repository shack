(*
   Check that the atom classes make sense in a MIR program.  This
   may not be quite as strict as what you would see over in FIR.

   --
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Symbol

open Frame_type

open Fir_set

open Mir
open Mir_ds
open Mir_pos
open Mir_exn
open Mir_type_sig

module Pos = MakePos (struct let name = "Mir_type" end)
open Pos


module MirType (Frame : BackendSig) : MirTypeSig =
struct
   module MirEnv  = Mir_env.MirEnv   (Frame)
   module MirUtil = Mir_util.MirUtil (Frame)

   open MirUtil

   (* Type of failure functions *)
   type fail = pos -> atom_class -> atom_class -> bool

   (***  Variable Environments  ***)


   let env_lookup env pos v =
      try
         SymbolTable.find env v
      with
         Not_found ->
            raise (MirException (pos, UnboundVar v))


   (***  Type checking interface  ***)


   (* function_args_agree
      Check that two funty's are compatible.  If the first funty
      is FunAny, then this always returns true.  *)
   let rec function_args_agree_action ((fail : fail), dst', src') pos dst src =
      match dst, src with
         FunAny, _ ->
            true
       | _, FunAny ->
            fail pos dst' src'
       | FunArgs dst, FunArgs src ->
            if (List.length dst) = (List.length src) then
               List.for_all2 (fun dst src ->
                  match dst, src with
                     ACPoly, _
                   | _, ACPoly
                     when not (Fir_state.strict_poly ()) ->
                        true
                   | _ ->
                        atom_classes_agree_action fail pos dst src) dst src
            else
               fail pos dst' src'


   (* atom_classes_agree
      Check that two atom classes agree.  *)
   and atom_classes_agree_action fail pos dst src =
      let pos = string_pos "atom_class_agree_action" pos in
         match dst, src with
            ACInt, ACInt
          | ACPoly, ACPoly ->
               true
          | ACRawInt (pre, signed), ACRawInt (pre', signed')
            when pre = pre' ->
               (* Note: we permit coercions to different sign *)
               true
          | ACFloat pre, ACFloat pre'
            when pre = pre' ->
               true
          | ACPointer ty, ACPointer ty'
          | ACPointerInfix ty, ACPointerInfix ty'
            when ty = ty' ->
               true
          | ACFunction ty, ACFunction ty' ->
               if function_args_agree_action (fail, dst, src) pos ty ty' then
                  true
               else
                  fail pos dst src
          | _ ->
               fail pos dst src

   let atom_classes_agree pos dst src =
      atom_classes_agree_action (fun _ _ _ -> false) pos dst src


   (* atom_classes_agree_option
      Check that two atom classes agree.  The first atom class is
      an option; if it is ``None'', then the atom classes will
      always agree; the first atom is the destination type.  The
      second atom class must always be a class.  *)
   let atom_classes_agree_option_action fail pos dst src =
      match dst with
         None ->
            true
       | Some dst ->
            atom_classes_agree_action fail pos dst src

   let atom_classes_agree_option pos dst src =
      atom_classes_agree_option_action (fun _ _ _ -> false) pos dst src


   (* check_atom_classes
      Checks that two atom classes are compatible, throws an exception
      if they are not.  The first atom class is an option; if it is
      ``None'', then the atom classes will always agree; the first atom
      is the destination type.  The second atom class must always be a
      class.  *)
   let check_atom_classes pos dst src =
      let pos = string_pos "check_atom_classes" pos in
      let fail pos dst src = raise (MirException (pos, ImplicitCoercion2 (dst, src))) in
         ignore (atom_classes_agree_action fail pos dst src)


   (* var_agrees
      Check that a variable use agrees with the atom class given in
      the variable environment.  *)
   let var_agrees_action fail venv pos ac v =
      if SymbolTable.mem venv v then
         let ac' = SymbolTable.find venv v in
            atom_classes_agree_option_action fail pos ac ac'
      else
         true

   let var_agrees venv pos ac v =
      var_agrees_action (fun _ _ _ -> false) venv pos ac v


   (* check_var
      Check that a variable use agrees with the atom class given in
      the variable environment.  *)
   let check_var venv pos ac v =
      let pos = string_pos "check_var" pos in
      let fail pos dst src = raise (MirException (pos, ImplicitCoercion2 (dst, src))) in
         ignore (var_agrees_action fail venv pos ac v)


   (* atom_agrees
      Checks that an atom has the required atom class for its result,
      and that all types are internally consistent within the atom.
      Note:  if ac = None, then the result type for the atom may be
      any valid type.  *)
   let rec atom_agrees_action fail venv pos ac a =
      let pos = atom_pos a (string_pos "atom_agrees_action" pos) in
         match a with
            AtomInt i ->
               atom_classes_agree_option_action fail pos ac ACInt
          | AtomRawInt i ->
               let pre = Rawint.precision i in
               let signed = Rawint.signed i in
                  atom_classes_agree_option_action fail pos ac (ACRawInt (pre, signed))
          | AtomFloat f ->
               let pre = Rawfloat.precision f in
                  atom_classes_agree_option_action fail pos ac (ACFloat pre)
          | AtomVar (ac', v) ->
               (* Atom class must strictly agree in this case *)
               (* BUG: Justin, what to do about nil?
                  Sometimes nil is a block, and sometimes an aggr.
                  We _could_ have two different nil values, but it
                  might be confusing...

                  var_agrees venv pos (Some ac') v
               && *) atom_classes_agree_option_action fail pos ac ac'
          | AtomFunVar (ac', f) ->
               (* Atom class must strictly agree in this case *)
               atom_classes_agree_option_action fail pos ac ac'
          | AtomUnop (op, a1) ->
               (* For unops, we allow some leniency in the argument types
                  for the pointer cases, under certain circumstances. *)
               let acres, ac1 = atom_class_of_unop_args op in
                  atom_classes_agree_option_action fail pos ac acres
                  && atom_agrees_pointer_action fail venv pos ac1 a1
          | AtomBinop (op, a1, a2) ->
               (* For binops, we allow some leniency in the argument types
                  for the pointer cases, under certain circumstances. *)
               let pos = string_pos "atom_binop" pos in
               let acres, ac1, ac2 = atom_class_of_binop_args op in
                  atom_classes_agree_option_action fail pos ac acres
                  && atom_agrees_pointer_action fail venv pos ac1 a1
                  && atom_agrees_pointer_action fail venv pos ac2 a2

   and atom_agrees venv pos ac a =
      atom_agrees_action (fun _ _ _ -> false) venv pos ac a


   (* atom_agrees_pointer
      Checks that an atom has a valid pointer type; if the desired
      AC is an ACPointer, then we will accept either ACPointer ptrty,
      or ACInfixPointer ptrty.  If the desired AC is specifically an
      ACInfixPointer, then no leniency is allowed.  *)
   and atom_agrees_pointer_action fail venv pos ac a =
      let pos = string_pos "atom_agrees_pointer_action" pos in
      let pos = atom_class_pos ac pos in
      let pos = atom_pos a pos in
         match ac with
            ACPointer ptrty ->
               let pos = int_pos 1 pos in
               let ac1 = Some (ACPointer ptrty) in
               let ac2 = Some (ACPointerInfix ptrty) in
                  if atom_agrees venv pos ac1 a || atom_agrees venv pos ac2 a then
                     true
                  else
                     fail pos ac (atom_class_of_atom a)
          | _ ->
               let pos = int_pos 2 pos in
                  atom_agrees_action fail venv pos (Some ac) a

   and atom_agrees_pointer venv pos ac a =
      atom_agrees_pointer_action (fun _ _ _ -> false) venv pos ac a


   (* check_atom
      Checks that an atom has the required atom class for its result,
      and that all types are internally consistent within the atom.
      Note:  if ac = None, then the result type for the atom may be
      any valid type.  *)
   let rec check_atom venv pos ac a =
      let pos = string_pos "check_atom" pos in
      let fail pos dst src = raise (MirException (pos, ImplicitCoercion2 (dst, src))) in
         ignore (atom_agrees_action fail venv pos (Some ac) a)

   let check_atom_any venv pos a =
      let pos = string_pos "check_atom_any" pos in
      let fail pos dst src = raise (MirException (pos, ImplicitCoercion2 (dst, src))) in
         ignore (atom_agrees_action fail venv pos None a)


   (* check_atom_pointer
      Checks that an atom has a valid pointer type; it must be
      either ACPointer ptrty, or ACInfixPointer ptrty.  *)
   let check_atom_pointer venv pos ptrty a =
      let pos = string_pos "check_atom_pointer" pos in
      let fail pos dst src = raise (MirException (pos, ImplicitCoercion2 (dst, src))) in
         ignore (atom_agrees_pointer_action fail venv pos (ACPointer ptrty) a)


   (* check_args
      Check that a function's arguments agree with the function's
      prototype.  *)
   let check_args venv fenv pos f args =
      let pos = string_pos "check_args" pos in
      let acs = env_lookup fenv pos f in
      let rec check acs args =
         match acs, args with
            ac :: acs, arg :: args ->
               check_atom venv pos ac arg;
               check acs args
          | [], [] ->
               ()
          | _ ->
               raise (MirException (pos, StringError "arity mismatch calling function"))
      in
         check acs args


   (* check_alloc_op
      Check that an alloc op is used properly.  *)
   let check_alloc_op venv pos op =
      let pos = string_pos "check_alloc_op" pos in
         match op with
            AllocTuple args
          | AllocArray args
          | AllocUnion (_, args) ->
               List.iter (check_atom_any venv pos) args
          | AllocMArray (args, a) ->
               List.iter (check_atom venv pos ac_native_unsigned) args;
               check_atom_any venv pos a
          | AllocMalloc a ->
               check_atom venv pos ac_native_unsigned a


   (* check_reserve
      Check reserve information.  *)
   let check_reserve venv pos (_, _, mem, ptr) =
      let pos = string_pos "check_reserve" pos in
         check_atom venv pos ac_native_unsigned mem;
         check_atom venv pos ac_native_unsigned ptr


   (* check_expr
      Checks that all types in the MIR are consistent.  All bound variables
      should be stored in the venv.  Note that this does not check scoping
      for variables.  *)
   let rec check_expr venv fenv e =
      let pos = string_pos "check_expr" (exp_pos e) in
         match dest_exp_core e with
            LetAtom (_, ac, a, e) ->
               check_atom venv pos ac a;
               check_expr venv fenv e
          | PointerIndexCheck (_, ptrty, _, ac, a, e) ->
               check_atom venv pos ac_native_unsigned a;
               check_atom_classes pos (ACPointer ptrty) ac;
               check_expr venv fenv e
          | FunctionIndexCheck (_, funty, _, ac, a, e) ->
               check_atom venv pos ac_native_unsigned a;
               check_atom_classes pos (ACFunction funty) ac;
               check_expr venv fenv e
          | LetAlloc (_, op, e) ->
               check_alloc_op venv pos op;
               check_expr venv fenv e
          | IfThenElse (op, a1, a2, e1, e2) ->
               let ac = atom_class_of_relop op in
                  check_atom venv pos ac a1;
                  check_atom venv pos ac a2;
                  check_expr venv fenv e1;
                  check_expr venv fenv e2
          | IfType (obj, names, name, _, e1, e2) ->
               (* TEMP:  IFTYPE *)
               check_expr venv fenv e1;
               check_expr venv fenv e2
          | Match (a, ac, cases) ->
               check_match_expr venv fenv pos a ac cases
          | LetExternal (_, _, _, args, e) ->
               List.iter (fun a -> check_atom_any venv pos a) args;
               check_expr venv fenv e
          | LetExternalReserve (rinfo, _, _, _, args, e) ->
               check_reserve venv pos rinfo;
               List.iter (fun a -> check_atom_any venv pos a) args;
               check_expr venv fenv e
          | Reserve (rinfo, e) ->
               check_reserve venv pos rinfo;
               check_expr venv fenv e
          | SetMem (ac, ptrty, ptr, off, a, e) ->
               check_atom venv pos ac a;
               check_atom_pointer venv pos ptrty ptr;
               check_atom venv pos ac_native_unsigned off;
               check_expr venv fenv e
          | SetGlobal (ac, _, a, e) ->
               check_atom venv pos ac a;
               check_expr venv fenv e
          | BoundsCheck (_, ptrty, ptr, start, stop, e) ->
               check_atom venv pos (ACPointer ptrty) ptr;
               check_atom venv pos ac_native_unsigned start;
               check_atom venv pos ac_native_unsigned stop;
               check_expr venv fenv e
          | LowerBoundsCheck (_, ptrty, ptr, start, e) ->
               check_atom venv pos (ACPointer ptrty) ptr;
               check_atom venv pos ac_native_unsigned start;
               check_expr venv fenv e
          | UpperBoundsCheck (_, ptrty, ptr, stop, e) ->
               check_atom venv pos (ACPointer ptrty) ptr;
               check_atom venv pos ac_native_unsigned stop;
               check_expr venv fenv e
          | Memcpy (ptrty, dst, doff, src, soff, len, e) ->
               check_atom venv pos (ACPointer ptrty) dst;
               check_atom venv pos (ACPointer ptrty) src;
               check_atom venv pos ac_native_unsigned doff;
               check_atom venv pos ac_native_unsigned soff;
               check_atom venv pos ac_native_unsigned len;
               check_expr venv fenv e
          | PrintDebug (_, _, args, e) ->
               List.iter (check_atom_any venv pos) args;
               check_expr venv fenv e
          | Debug (_, e)
          | CommentFIR (_, e) ->
               check_expr venv fenv e
          | CopyOnWrite (rinfo, ptr, e) ->
               check_reserve venv pos rinfo;
               check_atom_any venv pos ptr;
               check_expr venv fenv e
          | TailCall (DirectCall, f, args) ->
               check_args venv fenv pos f args
          | TailCall (IndirectCall, f, args) ->
               (* We may not have type information for function pointers *)
               if SymbolTable.mem fenv f then begin
                  check_args venv fenv pos f args
               end else begin
                  let acs = List.map atom_class_of_atom args in
                     check_var venv pos (Some (ACFunction (FunArgs acs))) f;
                     List.iter (check_atom_any venv pos) args
               end
          | TailCall (ExternalCall, f, args) ->
               (* We won't have type information for the external target *)
               List.iter (check_atom_any venv pos) args
          | SpecialCall (TailSysMigrate (id, dst_base, dst_off, f, args)) ->
               check_args venv fenv pos f args;
               check_atom venv pos ac_aggr_pointer dst_base;
               check_atom venv pos ac_native_unsigned dst_off
          | SpecialCall (TailAtomic (f, c, args)) ->
               check_args venv fenv pos f (c :: args);
               check_atom venv pos ac_native_int c
          | SpecialCall (TailAtomicRollback (level, c)) ->
               check_atom venv pos ac_native_unsigned level;
               check_atom venv pos ac_native_int c
          | SpecialCall (TailAtomicCommit (level, f, args)) ->
               check_atom venv pos ac_native_unsigned level;
               check_args venv fenv pos f args
          | SysMigrate (_, dst_base, dst_off, f, env) ->
               check_atom venv pos ac_aggr_pointer dst_base;
               check_atom venv pos ac_native_unsigned dst_off;
               check_atom venv pos ac_native_unsigned env;
               check_args venv fenv pos f [env]
          | Atomic (f, i, e) ->
               check_atom venv pos ac_native_int i;
               check_atom venv pos ac_aggr_pointer e;
               check_args venv fenv pos f [i; e]
          | AtomicRollback (level, i) ->
               check_atom venv pos ac_native_unsigned level;
               check_atom venv pos ac_native_int i
          | AtomicCommit (level, e) ->
               check_atom venv pos ac_native_unsigned level;
               check_expr venv fenv e


   (* check_match_expr
      Check a Match expression for correctness.  Currently, we don't have
      any way to express the direct read of the block pointer for unions
      which have constant constructors; we have to handle this specially. *)
   and check_match_expr venv fenv pos a ac cases =
      let pos = string_pos "check_match_expr" pos in
         match ac, atom_class_of_atom a with
            ACRawInt (Rawint.Int32, true), ACPointer PtrBlock ->
               (* Union match case *)
               let rec process_cases = function
                  [RawIntSet s, e]
                  when RawIntSet.precision s = Rawint.Int32 && RawIntSet.signed s ->
                     check_expr venv fenv e
                | (IntSet _, e) :: (_ :: _ as cases) ->
                     check_expr venv fenv e;
                     process_cases cases
                | _ ->
                     raise (MirException (int_pos 1 pos, StringError "bogus set types for union match"))
               in (* end process_cases *)
                  check_atom venv (int_pos 2 pos) ac_block_pointer a;
                  process_cases cases
          | _ ->
               (* Standard match case; must be more consistent *)
               let process_case (s, e) =
                  check_expr venv fenv e;
                  match ac, s with
                     ACInt, IntSet _ ->
                        ()
                   | ac, RawIntSet s ->
                        let pre = RawIntSet.precision s in
                        let signed = RawIntSet.signed s in
                           check_atom_classes (int_pos 3 pos) ac (ACRawInt (pre, signed))
                   | _ ->
                        raise (MirException (int_pos 4 pos, StringError "bogus set types for normal match"))
               in (* end process_case *)
                  check_atom venv (int_pos 5 pos) ac a;
                  List.iter process_case cases


   (* check_prog
      Checks the entire MIR program for type consistency.  *)
   let check_prog prog =
      (* Build the environment tables *)
      let venv = MirEnv.mir_venv_prog prog in
      let fenv = SymbolTable.map (fun (_, args, _) ->
         List.map (fun (_, ac) -> ac) args) prog.prog_funs in
      let fenv = SymbolTable.fold (fun fenv f import ->
         match import.import_info with
            ImportGlobal ->
               fenv
          | ImportFun (_, _, acs) ->
               SymbolTable.add fenv f acs) fenv prog.prog_import
      in

      (* Check that the globals are reasonably sound *)
      let () = SymbolTable.iter (fun v init ->
         let pos = var_exp_pos v in
         match init with
            InitAtom a ->
               check_atom_any venv pos a
          | InitAlloc op ->
               check_alloc_op venv pos op
          | InitRawData _ ->
               ()) prog.prog_globals
      in

      (* Check that the functions are reasonably sound *)
      let () = SymbolTable.iter (fun _ (_, _, e) ->
         check_expr venv fenv e) prog.prog_funs
      in
         ()


end (* MirType module *)
