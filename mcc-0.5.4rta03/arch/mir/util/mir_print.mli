(*
 * Print MIR code.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Frame_type
open Mir

val mir_comments_fir : bool ref

val string_of_unop : unop -> string
val string_of_binop : binop -> string
val string_of_atom_class : atom_class -> string

val pp_print_expr       : formatter -> exp -> unit
val pp_print_expr_size  : int -> formatter -> exp -> unit
val pp_print_atom_class : formatter -> atom_class -> unit
val pp_print_ptrty      : formatter -> ptrty -> unit
val pp_print_atom       : formatter -> atom -> unit
val pp_print_prog       : formatter -> prog -> unit

val debug_prog : string -> prog -> unit

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
