(*
   MIR miscellaneous state
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Debug


let debug_print_mir   =
   create_debug
      { debug_name = "print_mir";
        debug_description = "print the program's machine intermediate representation";
        debug_value = false
      }

let debug_print_final_fir  =
   create_debug
      { debug_name = "print_final_fir";
        debug_description = "print the final stage of program's functional intermediate representation";
        debug_value = false
      }

let debug_print_final_mir  =
   create_debug
      { debug_name = "print_final_mir";
        debug_description = "print the final stage of program's machine intermediate representation";
        debug_value = false
      }

