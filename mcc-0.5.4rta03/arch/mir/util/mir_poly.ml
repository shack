(*
   Introduce poly<->value coercions in the case where a function
   is coerced between the types ACFunction [..., int, ...] and
   ACFunction [..., poly, ...].  This conversion requires a new
   stub function that contains the actual coercion.

   --
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Location
open Frame_type

open Mir
open Mir_ds
open Mir_pos
open Mir_exn
open Mir_poly_sig
open Mir_ac_map

module Pos = MakePos (struct let name = "Mir_poly" end)
open Pos


module MirPoly (Frame : BackendSig) : MirPolySig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)
   open MirUtil


   (***  Basic funty manipulation  ***)


   (* args_of_funty
      Extract the list of atom classes for arguments in a funty
      qualifier.  If funty is FunAny, then an exception is thrown.  *)
   let args_of_funty pos funty =
      let pos = string_pos "args_of_funty" pos in
         match funty with
            FunArgs args ->
               args
          | FunAny ->
               raise (MirException (pos, StringError "FunAny not allowed here"))


   (* args_of_atom_class
      Extract the list of atom classes for arguments in an ACFunction
      atom class.  If the atom class isn't an ACFunction, then an
      exception is thrown.  *)
   let args_of_atom_class pos ac =
      let pos = string_pos "args_of_atom_class" pos in
         match ac with
            ACFunction funty ->
               args_of_funty pos funty
          | _ ->
               raise (MirException(pos, StringError "Expected ACFunction here"))


   (***  The basic poly<->value coercion function  ***)


   (* coerce_poly
      Check to see if the atom needs to be implicitly coerced between a
      Poly type and a normal (nonpoly) type.  The ac given is the desired
      destination type.  The exn_fun is used to build an exception, which
      may be a FIR or a MIR exception.  This function is used by the FIR
      to MIR translation as well.  WARNING: these coercions are not safe!  *)
   let coerce_poly exn_fun pos ac a =
      let ac' = atom_class_of_atom a in
         match ac, ac' with
            (* ACPoly coercions, and ACPoly -> ACPoly case *)
            ACInt, ACPoly ->
               AtomUnop (IntOfPolyOp, a)
          | ACRawInt (pre, signed), ACPoly ->
               AtomUnop (RawIntOfIntOp (pre, signed), AtomUnop (IntOfPolyOp, a))
          | ACPointer ptrty, ACPoly ->
               AtomUnop (PointerOfPolyOp ptrty, a)
          | ACFunction funty, ACPoly ->
               AtomUnop (FunctionOfPolyOp funty, a)
          | ACPoly, ACInt ->
               AtomUnop (PolyOfIntOp, a)
          | ACPoly, ACRawInt (pre, signed) ->
               AtomUnop (PolyOfIntOp, AtomUnop (IntOfRawIntOp (pre, signed), a))
          | ACPoly, ACPointer ptrty ->
               AtomUnop (PolyOfPointerOp ptrty, a)
          | ACPoly, ACFunction funty ->
               AtomUnop (PolyOfFunctionOp funty, a)
          | ACPoly, ACPoly ->
               a
            (* Any other ACPoly coercion is not allowed. *)
          | ACPoly, _ ->
               let ac' = Mir_print.string_of_atom_class ac' in
                  raise (exn_fun pos ("attempted to coerce to poly from an incompatible type: " ^ ac'))
          | _, ACPoly ->
               let ac = Mir_print.string_of_atom_class ac in
                  raise (exn_fun pos ("attempted to coerce to an incompatible type from poly: " ^ ac))
            (* Normal atom manipulations. *)
          | _ ->
               a


   (***  Environments  ***)


   (* assign
      Indicates whether a function pointer was assigned directly from an
      AtomFunVar (name of a function), or if it was indirectly assigned
      from another function pointer.  If it was DirectAssign, then we can
      generate a function stub that jumps directly to the original fun;
      otherwise, we have to jump to a function pointer, which means we
      need closures.
         DirectAssign f:   Directly assigned from an AtomFunVar (_, f)
         IndirectAssign:   Assigned from another function pointer.
    *)
   type assign =
      DirectAssign of var
    | IndirectAssign


   (* env
      Environment used for poly stub manipulations.

      env_fenv = (assign * args) SymbolTable.t
         Environment of all known function pointers.  Indicates whether
         the function pointer was directly or indirectly assigned, as
         well as the atom class for the function.  The atom class is
         ACFunction (FunArgs args).

      env_funs = args SymbolTable.t
         Environment of all real functions that might escape.  The types
         of the arguments are given in args.

      env_stubs = (stub_name ACPairTable.t) SymbolTable.t
         List of all stub functions that need to be generated.  Stub
         functions have prototype stub_name (stub_args), transform the
         args as necessary and then call target_name (target_args). The
         outer symbol table is indexed on target_name, and the ACPair
         table is indexed on (stub_args, target_args).  Note that the
         stub_args == dst_args, and target_args == src_args (hint: the
         dst_args are what we are assigning TO).
    *)
   type env =
      { env_fenv : (assign * atom_class list) SymbolTable.t;
        env_funs : (atom_class list) SymbolTable.t;
        env_stubs : (var ACPairTable.t) SymbolTable.t;
      }


   (* env_empty
      Create an initial environment that is completely empty.  *)
   let env_empty =
      { env_fenv = SymbolTable.empty;
        env_funs = SymbolTable.empty;
        env_stubs = SymbolTable.empty;
      }


   (* fenv_add
      Add a new function pointer to the environment.  *)
   let fenv_add env v assign args =
      { env with env_fenv = SymbolTable.add env.env_fenv v (assign, args) }


   (* fenv_lookup
      Lookup a function pointer in the environment.  *)
   let fenv_lookup env pos v =
      let pos = string_pos "fenv_lookup" pos in
         try
            SymbolTable.find env.env_fenv v
         with
            Not_found ->
               raise (MirException (pos, UnboundVar v))


   (* funs_add
      Add a new function to the environment, with indicated arguments.  *)
   let funs_add env f acs =
      { env with env_funs = SymbolTable.add env.env_funs f acs }


   (* funs_lookup
      Lookup a function in the environment.  *)
   let funs_lookup env pos f =
      let pos = string_pos "funs_lookup" pos in
         try
            SymbolTable.find env.env_funs f
         with
            Not_found ->
               raise (MirException (pos, UnboundVar f))


   (* stubs_mem
      Check to see if a stub for indicated target, arguments has been
      defined in the stubs environment yet.  *)
   let stubs_mem env f dst_args src_args =
      try
         ACPairTable.mem (SymbolTable.find env.env_stubs f) (dst_args, src_args)
      with
         Not_found ->
            false


   (* stubs_lookup
      Return the name of the stub function for the indicated target
      function and arguments.  Throw an exception if nothing is found. *)
   let stubs_lookup env pos f dst_args src_args =
      let pos = string_pos "stubs_lookup" pos in
         try
            ACPairTable.find (SymbolTable.find env.env_stubs f) (dst_args, src_args)
         with
            Not_found ->
               raise (MirException (pos, InternalError "could not find requested stub class"))


   (* stubs_add
      Add a new stub function name to the environment for the indicated
      target function and arguments.  *)
   let stubs_add env f dst_args src_args stub_name =
      (* Lookup the ACPair table to add the stub name to. *)
      let stubs_pair =
         try
            SymbolTable.find env.env_stubs f
         with
            Not_found ->
               (* f not bound in the outer table yet... *)
               ACPairTable.empty
      in

      (* Bind the new stub name to the ACPair table, then add the revised
         ACPair table to the outer symboltable (which clobbers old entry). *)
      let stubs_pair = ACPairTable.add stubs_pair (dst_args, src_args) stub_name in
      let stubs = SymbolTable.add env.env_stubs f stubs_pair in
         { env with env_stubs = stubs }


   (***  Stub function generation  ***)


   (* make_custom_stub_function
      Setup a new custom stub function for the target function f (which
      is a real function, NOT a function pointer) and the dst_args /
      src_args indicated.  The dst_args are the args the stub function
      will take, and the src_args are the argument types for f.  If a
      stub function for this set of argument coercions is found, then
      it will be returned instead of creating a new stub.  *)
   let make_custom_stub_function env pos f dst_args src_args =
      let pos = string_pos "make_custom_stub_function" pos in
         if stubs_mem env f dst_args src_args then
            (* Found a stub function matching our criteria *)
            env, stubs_lookup env pos f dst_args src_args
         else
            (* No match found; make a new stub function *)
            let stub_name = new_symbol f in
               stubs_add env f dst_args src_args stub_name, stub_name


   (* generate_stubs
      Generate the stub functions.  The new stubs are added to the funs
      table which is passed into this function.  *)
   let generate_stubs loc funs stubs =
      (* Revise coerce_poly so it will throw a MIR exception. *)
      let coerce_poly = coerce_poly (fun pos msg -> MirException (pos, StringError msg)) in

      (* Code to copy stub arguments and apply the necessary coercions. *)
      let rec copy_stub_args pos target_vars target_acs stub_vars stub_acs e =
         match target_vars, target_acs, stub_vars, stub_acs with
            t :: target_vars, tac :: target_acs, s :: stub_vars, sac :: stub_acs ->
               let e = copy_stub_args pos target_vars target_acs stub_vars stub_acs e in
               let a = coerce_poly pos tac (AtomVar (sac, s)) in
                  make_exp loc (LetAtom (t, tac, a, e))
          | [], [], [], [] ->
               e
          | _ ->
               raise (MirException (pos, InternalError "4-way arity error building stubs"))
      in (* end of copy_stub_args *)

      (* Code to add one new stub function to the funs table. *)
      let add_stub funs stub_name stub_acs target_name target_acs =
         let pos = string_pos "generate_stubs" (var_exp_pos target_name) in
         let stub_vars   = List.map (fun _ -> new_symbol_string "stub") stub_acs in
         let target_vars = List.map (fun _ -> new_symbol_string "stub") stub_acs in
         let stub_formals = List.map2 (fun v ac -> v, ac) stub_vars stub_acs in
         let target_atoms = List.map2 (fun ac v -> AtomVar (ac, v)) target_acs target_vars in
         let e = make_exp loc (TailCall (DirectCall, target_name, target_atoms)) in
         let e = copy_stub_args pos target_vars target_acs stub_vars stub_acs e in
         let debug = bogus_loc "<Mir_poly>" in
            SymbolTable.add funs stub_name (debug, stub_formals, e)
      in (* end of add_stub *)

         (* Fold over the stub tables to generate all the new functions. *)
         SymbolTable.fold (fun funs target_name stubs_pair ->
            ACPairTable.fold (fun funs (stub_args, target_args) stub_name ->
               add_stub funs stub_name stub_args target_name target_args) funs stubs_pair) funs stubs


   (* insert_new_stub_functions
      The stubs created escape, so we need to insert them into the globals
      table.  We do this by inserting them after the target function they
      are associated with; if multiple stub functions are associated with
      a single target function, then they are inserted in an order matching
      the ACPair ordering (which must be absolute, independent of migration).

      See insert_new_functions in mir_special.ml for more insight on this
      process -- it is VERY sensitive to order, do not edit if you don't know
      what you are doing!   *)
   let insert_new_stub_functions fun_order stubs =
      (* Extract the stub names associated with a target function *)
      let get_stub_names stubs_pair =
         List.rev (ACPairTable.fold (fun ls _ f -> f :: ls) [] stubs_pair)
      in (* end of get_stub_names *)

      (* Insert all stub functions after their target function. *)
      let insert_after new_funs f =
         let new_funs = f :: new_funs in
         if SymbolTable.mem stubs f then
            (* Insert new elements after this one *)
            let stubs_pair = SymbolTable.find stubs f in
               (get_stub_names stubs_pair) @ new_funs
         else
            (* Nothing to insert here *)
            new_funs
      in (* end of insert_after *)

      (* Update the fun_order table to include all the stubs. *)
      let fun_order = List.fold_left insert_after [] fun_order in
      let fun_order = List.rev fun_order in
         fun_order


   (***  Identify binding occurrences of variables  ***)


   (* poly_coercion
      Returns true if a polymorphic coercion is detected between
      the two argument lists.  This check can be conservative... *)
   let rec poly_coercion dst_args src_args =
      dst_args <> src_args


   (* poly_src_info
      Computes information (assign type and argument list) about a source.
      If the source is an AtomFunVar or a function pointer directly assigned,
      return DirectAssign with the proper target function; otherwise return
      IndirectAssign.  *)
   let poly_src_info env pos a =
      let pos = string_pos "poly_src_info" pos in
         match a with
            AtomFunVar (_, f) ->
               DirectAssign f, funs_lookup env pos f
          | AtomVar (_, v) ->
               fenv_lookup env pos v
          | _ ->
               let ac = atom_class_of_atom a in
               let args = args_of_atom_class pos ac in
                  IndirectAssign, args


   (* poly_check_for_coercion
      Check to see if a coercion occurs in the arguments during a
      function pointer assignment (or use).  If a coercion occurs,
      then build a stub and rewrite the atom to refer to the stub.
      Otherwise, pass through unaltered.  *)
   let poly_check_for_coercion env pos a assign dst_args src_args cont =
      let pos = string_pos "poly_check_for_coercion" pos in
         if poly_coercion dst_args src_args then
            (* Coercion occurred; check the assign tag. *)
            match assign with
               DirectAssign f ->
                  (* Generate a custom stub for function f *)
                  let env, f' = make_custom_stub_function env pos f dst_args src_args in
                  let a = AtomFunVar (ACFunction (FunArgs dst_args), f') in
                     cont env a
             | IndirectAssign ->
                  (* Generate general purpose stub taking a
                     function pointer as the target. *)
                  (* Damn.  Jason's AML code hits this case.  I was hoping
                     we'd be able to put it off for a little while longer. *)
                  raise (MirException (pos, NotImplemented "cannot build stub from indirect function -- contact JDS before attempting to correct this"))
         else
            (* No coercion occurred; a is unaltered. *)
            cont env a


   (* poly_atom
      Check for coercions in an atom which is used with type ac.  If the
      atom is not a function, then it is passed through unaltered.  The
      continuation gets the transformed atom.  *)
   let poly_atom env pos ac a cont =
      let pos = string_pos "poly_atom" pos in
         match ac with
            ACFunction funty ->
               let dst_args = args_of_funty pos funty in
               let assign, src_args = poly_src_info env pos a in
                  poly_check_for_coercion env pos a assign dst_args src_args cont
          | _ ->
               (* Not a function; we don't care. *)
               cont env a


   (* poly_atom_list
      Call poly_atom on a list of atoms, each used with the corresponding
      atom class in acs.  The continuation gets the transformed list of
      atoms.  *)
   let poly_atom_list env pos acs atoms cont =
      let pos = string_pos "poly_atom_list" pos in
      let rec poly_atom_list env new_atoms acs atoms =
         match acs, atoms with
            ac :: acs, a :: atoms ->
               poly_atom env pos ac a (fun env a ->
                  poly_atom_list env (a :: new_atoms) acs atoms)
          | [], [] ->
               cont env (List.rev new_atoms)
          | _ ->
               raise (MirException (pos, InternalError "inconsistency in number of args for ACs and atoms"))
      in
         poly_atom_list env [] acs atoms


   (* poly_bind_atom
      Binding occurrence of an atom to a variable.  If the variable has
      ACFunction type, then we need to add it to fenv as a function ptr. *)
   let poly_bind_atom env pos v ac a cont =
      let pos = string_pos "poly_bind_atom" pos in
         poly_atom env pos ac a (fun env a ->
            let env =
               match ac with
                  ACFunction funty ->
                     let assign, args = poly_src_info env pos a in
                        fenv_add env v assign args
                | _ ->
                     env
            in
               cont env a)


   (* poly_bind_formals
      Binding occurrence of formal parametres to a function.
      Add the formals which are function pointers to fenv.  *)
   let poly_bind_formals env pos formals =
      let pos = string_pos "poly_bind_formals" pos in
      let poly_bind_formal env (v, ac) =
         match ac with
            ACFunction funty ->
               let args = args_of_funty pos funty in
                  fenv_add env v IndirectAssign args
          | _ ->
               env
      in
         List.fold_left poly_bind_formal env formals


   (***  Process function-argument coercions in an expression  ***)


   (* poly_expr
      Process polymorphic function coercions in an expression.  *)
   let rec poly_expr env e =
      let pos = string_pos "poly_expr" (exp_pos e) in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetAtom (v, ac, a, e) ->
               poly_bind_atom env pos v ac a (fun env a ->
                  let env, e = poly_expr env e in
                     env, make_exp loc (LetAtom (v, ac, a, e)))
          | TailCall (DirectCall, f, args) ->
               let acs = funs_lookup env pos f in
                  poly_atom_list env pos acs args (fun env args ->
                     env, make_exp loc (TailCall (DirectCall, f, args)))
          | TailCall (IndirectCall, v, args) ->
               let _, dst_args = fenv_lookup env pos v in
                  poly_atom_list env pos dst_args args (fun env args ->
                     env, make_exp loc (TailCall (IndirectCall, v, args)))
          | TailCall (ExternalCall, _, _) ->
               (* cannot make any assumptions on args here *)
               env, e
          | LetExternal (v, ac, f, args, e) ->
               (* cannot make any assumptions on args here *)
               let env, e = poly_expr env e in
                  env, make_exp loc (LetExternal (v, ac, f, args, e))
          | LetExternalReserve (rinfo, v, ac, f, args, e) ->
               (* cannot make any assumptions on args here *)
               let env, e = poly_expr env e in
                  env, make_exp loc (LetExternalReserve (rinfo, v, ac, f, args, e))
          | IfThenElse (op, a1, a2, e1, e2) ->
               let env, e1 = poly_expr env e1 in
               let env, e2 = poly_expr env e2 in
                  env, make_exp loc (IfThenElse (op, a1, a2, e1, e2))
          | Match (a, ac, cases) ->
               let rec process_case (env, cases) (s, e) =
                  let env, e = poly_expr env e in
                     env, (s, e) :: cases
               in
               let env, cases = List.fold_left process_case (env, []) cases in
                  env, make_exp loc (Match (a, ac, List.rev cases))
          | IfType (a1, a2, a3, v, e1, e2) ->
               let env, e1 = poly_expr env e1 in
               let env, e2 = poly_expr env e2 in
                  env, make_exp loc (IfType (a1, a2, a3, v, e1, e2))
          | Reserve (rinfo, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (Reserve (rinfo, e))
          | LetAlloc (v, op, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (LetAlloc (v, op, e))
          | SetMem (ac, ptrty, ptr, off, value, e) ->
               poly_atom env pos ac value (fun env value ->
                  let env, e = poly_expr env e in
                     env, make_exp loc (SetMem (ac, ptrty, ptr, off, value, e)))
          | SetGlobal (ac, v, a, e) ->
               (* globals are not functional; do not bind *)
               poly_atom env pos ac a (fun env a ->
                  let env, e = poly_expr env e in
                     env, make_exp loc (SetGlobal (ac, v, a, e)))
          | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (BoundsCheck (label, ptrty, ptr, start, stop, e))
          | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (LowerBoundsCheck (label, ptrty, ptr, start, e))
          | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (UpperBoundsCheck (label, ptrty, ptr, stop, e))
          | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (PointerIndexCheck (label, ptrty, v, ac, index, e))
          | FunctionIndexCheck (label, funty, v, ac, index, e) ->
               (* We check on funty, but assign to ac. *)
               let src_args = args_of_funty pos funty in
               let dst_args = args_of_atom_class pos ac in
               let env = fenv_add env v IndirectAssign dst_args in
               let env, e = poly_expr env e in
                  env, make_exp loc (FunctionIndexCheck (label, funty, v, ac, index, e))
          | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (Memcpy (ptrty, a1, a2, a3, a4, a5, e))
          | Debug (info, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (Debug (info, e))
          | PrintDebug (v1, v2, info, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (PrintDebug (v1, v2, info, e))
          | CommentFIR (fir, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (CommentFIR (fir, e))
          | SpecialCall (TailSysMigrate (id, dst_base, dst_off, f, args)) ->
               let acs = funs_lookup env pos f in
                  poly_atom_list env pos acs args (fun env args ->
                     env, make_exp loc (SpecialCall (TailSysMigrate (id, dst_base, dst_off, f, args))))
          | SpecialCall (TailAtomic (f, c, args)) ->
               let acs = funs_lookup env pos f in
               let ac, acs =
                  match acs with
                     ac :: acs ->
                        ac, acs
                   | [] ->
                        raise (MirException (pos, InternalError "target function takes no arguments"))
               in
                  poly_atom env pos ac c (fun env c ->
                     poly_atom_list env pos acs args (fun env args ->
                        env, make_exp loc (SpecialCall (TailAtomic (f, c, args)))))
          | SpecialCall (TailAtomicCommit (level, f, args)) ->
               let acs = funs_lookup env pos f in
                  poly_atom_list env pos acs args (fun env args ->
                     env, make_exp loc (SpecialCall (TailAtomicCommit (level, f, args))))
          | SpecialCall (TailAtomicRollback _) ->
               env, e
          | CopyOnWrite (rinfo, ptr, e) ->
               let env, e = poly_expr env e in
                  env, make_exp loc (CopyOnWrite (rinfo, ptr, e))
          | SysMigrate _
          | Atomic _
          | AtomicRollback _
          | AtomicCommit _ ->
               raise (MirException (pos, InternalError "statement illegal at this level"))


   (***  Top-level functions  ***)


   (* poly_prog
      Process polymorphic function coercions in a program.  *)

   let loc = bogus_loc "<Mir_poly:please-fix-this>"

   let poly_prog prog =
      let { prog_funs      = funs;
            prog_import    = imports;
            prog_fun_order = fun_order;
          } = prog
      in
      let env = env_empty in
      let env = SymbolTable.fold (fun env f (_, formals, _) ->
         funs_add env f (List.map snd formals)) env funs
      in
      let env = SymbolTable.fold (fun env v import ->
         match import.import_info with
            ImportGlobal ->
               env
          | ImportFun (_, _, acs) ->
               fenv_add env v IndirectAssign acs) env imports
      in
      let stubs, funs = SymbolTable.fold (fun (stubs, funs) f (debug, formals, e) ->
         let pos = string_pos "poly_prog" (var_exp_pos f) in
         let env' = { env with env_stubs = stubs } in
         let env' = poly_bind_formals env' pos formals in
         let env', e = poly_expr env' e in
         let stubs = env'.env_stubs in
         let funs = SymbolTable.add funs f (debug, formals, e) in
            stubs, funs) (env.env_stubs, SymbolTable.empty) funs
      in
      let funs = generate_stubs loc funs stubs in
      let fun_order = insert_new_stub_functions fun_order stubs in
         { prog with
               prog_funs      = funs;
               prog_fun_order = fun_order;
         }


   let poly_prog prog =
      if Fir_state.strict_poly () then
         poly_prog prog
      else
         prog


   let poly_prog = Fir_state.profile "Mir_poly.poly_prog" poly_prog

end (* MirPoly struct *)
