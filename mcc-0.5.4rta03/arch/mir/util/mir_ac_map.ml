(*
   Sets indexed on atom classes and related.

   --
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Frame_type


(*
   WARNING: AC MUST NOT INCLUDE SYMBOLS, nor any other element whose
            order is not absolutely determined.  The order of atom
            classes must be preserved after a system migration.  We
            cannot rely on Pervasives.compare to do the right thing
            here --- many things will break in migration if this
            explicit comparison code is deleted.
 *)


(* compare_pre
   Compare two Rawint precision qualifiers.  *)
let rec compare_pre pre1 pre2 =
   match pre1, pre2 with
      Rawint.Int8,  Rawint.Int8
    | Rawint.Int16, Rawint.Int16
    | Rawint.Int32, Rawint.Int32
    | Rawint.Int64, Rawint.Int64 ->
         0
    | Rawint.Int8,  Rawint.Int16
    | Rawint.Int8,  Rawint.Int32
    | Rawint.Int8,  Rawint.Int64
    | Rawint.Int16, Rawint.Int32
    | Rawint.Int16, Rawint.Int64
    | Rawint.Int32, Rawint.Int64 ->
         -1
    | Rawint.Int16, Rawint.Int8
    | Rawint.Int32, Rawint.Int8
    | Rawint.Int64, Rawint.Int8
    | Rawint.Int32, Rawint.Int16
    | Rawint.Int64, Rawint.Int16
    | Rawint.Int64, Rawint.Int32 ->
         1


(* compare_signed
   Compare two signed flags (used in Rawint precisions).  *)
and compare_signed signed1 signed2 =
   match signed1, signed2 with
      true, true
    | false, false ->
         0
    | true, false ->
         -1
    | false, true ->
         1


(* compare_fpre
   Compare two floating-point precision qualifiers.  *)
and compare_fpre fpre1 fpre2 =
   match fpre1, fpre2 with
      Rawfloat.Single, Rawfloat.Single
    | Rawfloat.Double, Rawfloat.Double
    | Rawfloat.LongDouble, Rawfloat.LongDouble ->
         0
    | Rawfloat.Single, Rawfloat.Double
    | Rawfloat.Single, Rawfloat.LongDouble
    | Rawfloat.Double, Rawfloat.LongDouble ->
         -1
    | Rawfloat.Double, Rawfloat.Single
    | Rawfloat.LongDouble, Rawfloat.Single
    | Rawfloat.LongDouble, Rawfloat.Double ->
         1


(* compare_ptrty
   Compare two pointer type qualifiers.  *)
and compare_ptrty ptrty1 ptrty2 =
   match ptrty1, ptrty2 with
      PtrBlock, PtrBlock
    | PtrAggr, PtrAggr ->
         0
    | PtrBlock, PtrAggr ->
         -1
    | PtrAggr, PtrBlock ->
         1


(* compare_funty
   Compare two function type qualifiers. If both are FunArgs, then we must
   check the argument lists to make sure they also agree; compare_ac_list
   does this quite nicely for us.  *)
and compare_funty funty1 funty2 =
   match funty1, funty2 with
      FunAny, FunAny ->
         0
    | FunArgs args1, FunArgs args2 ->
         compare_ac_list args1 args2
    | FunAny, FunArgs _ ->
         -1
    | FunArgs _, FunAny ->
         1


(* compare_ac
   Compare two atom classes.  *)
and compare_ac ac1 ac2 =
   match ac1, ac2 with
      ACInt, ACInt
    | ACPoly, ACPoly ->
         0
    | ACRawInt (pre1, signed1), ACRawInt (pre2, signed2) ->
         let i = compare_pre pre1 pre2 in
            if i = 0 then
               compare_signed signed1 signed2
            else
               i
    | ACFloat fpre1, ACFloat fpre2 ->
         compare_fpre fpre1 fpre2
    | ACPointer ptrty1, ACPointer ptrty2
    | ACPointerInfix ptrty1, ACPointerInfix ptrty2 ->
         compare_ptrty ptrty1 ptrty2
    | ACFunction funty1, ACFunction funty2 ->
         compare_funty funty1 funty2
    | ACInt,            ACPoly
    | ACInt,            ACRawInt _
    | ACInt,            ACFloat _
    | ACInt,            ACPointer _
    | ACInt,            ACPointerInfix _
    | ACInt,            ACFunction _
    | ACPoly,           ACRawInt _
    | ACPoly,           ACFloat _
    | ACPoly,           ACPointer _
    | ACPoly,           ACPointerInfix _
    | ACPoly,           ACFunction _
    | ACRawInt _,       ACFloat _
    | ACRawInt _,       ACPointer _
    | ACRawInt _,       ACPointerInfix _
    | ACRawInt _,       ACFunction _
    | ACFloat _,        ACPointer _
    | ACFloat _,        ACPointerInfix _
    | ACFloat _,        ACFunction _
    | ACPointer _,      ACPointerInfix _
    | ACPointer _,      ACFunction _
    | ACPointerInfix _, ACFunction _ ->
         -1
    | ACPoly,           ACInt
    | ACRawInt _,       ACInt
    | ACFloat _,        ACInt
    | ACPointer _,      ACInt
    | ACPointerInfix _, ACInt
    | ACFunction _ ,    ACInt
    | ACRawInt _,       ACPoly
    | ACFloat _,        ACPoly
    | ACPointer _,      ACPoly
    | ACPointerInfix _, ACPoly
    | ACFunction _,     ACPoly
    | ACFloat _,        ACRawInt _
    | ACPointer _,      ACRawInt _
    | ACPointerInfix _, ACRawInt _
    | ACFunction _,     ACRawInt _
    | ACPointer _,      ACFloat _
    | ACPointerInfix _, ACFloat _
    | ACFunction _,     ACFloat _
    | ACPointerInfix _, ACPointer _
    | ACFunction _,     ACPointer _
    | ACFunction _,     ACPointerInfix _ ->
         1


(* compare_ac_list
   Compare two lists of atom classes.  *)
and compare_ac_list acs1 acs2 =
   match acs1, acs2 with
      ac1 :: acs1, ac2 :: acs2 ->
         let i = compare_ac ac1 ac2 in
            if i = 0 then
               compare_ac_list acs1 acs2
            else
               i
    | [], [] ->
         0
    | [], _ ->
         -1
    | _, [] ->
         1


(* compare_ac_pair
   Compare two pairs of atom class lists.  *)
let compare_ac_pair (ac11, ac12) (ac21, ac22) =
   let i = compare_ac_list ac11 ac21 in
      if i = 0 then
         compare_ac_list ac12 ac22
      else
         i


(* ACPairCompare
   A comparison function using a pair of atom class lists as the base type.
   This is used for generating stub functions, where we create a function
   stub to coerce arguments of one type to arguments of another type.  *)
module ACPairCompare = struct
   type t = atom_class list * atom_class list
   let compare = compare_ac_pair
end (* module ACPairCompare *)


(* ACPairTable
   A map based on ACPair.  *)
module ACPairTable = Mc_map.McMake (ACPairCompare)
