(*
 * Normal MIR exceptions.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2001 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol
open Location

open Fir_print
open Fir_state

open Mir
open Mir_ds
open Mir_exn
open Mir_print

(*
 * Location of exception.
 *)
type item =
   Exp of exp
 | Atom of atom
 | AtomClass of atom_class
 | Symbol of var
 | String of string

type pos = item Position.pos

(*
 * FIR exception.
 *)
exception MirException of pos * mir_error

(************************************************************************
 * UTILITIES
 ************************************************************************)

(*
 * Get the source location for an exception.
 *)
let string_loc = bogus_loc "<Mir_pos>"

let rec loc_of_value x =
   match x with
      Exp e ->
         loc_of_exp e
    | Atom _
    | AtomClass _
    | Symbol _
    | String _ ->
         string_loc

(*
 * Print debugging info.
 *)
let rec pp_print_value buf x =
   match x with
      Exp e ->
         pp_print_expr_size (exn_expr_size ()) buf e

    | Atom a ->
         pp_print_atom buf a

    | AtomClass ac ->
         pp_print_atom_class buf ac

    | Symbol v ->
         pp_print_symbol buf v

    | String s ->
         pp_print_string buf s

(************************************************************************
 * CONSTRUCTION
 ************************************************************************)

module type PosSig =
sig
   val loc_pos : loc -> pos

   val exp_pos : exp -> pos
   val var_exp_pos : var -> pos
   val string_exp_pos : string -> pos
   val string_pos : string -> pos -> pos
   val pos_pos : pos -> pos -> pos
   val int_pos : int -> pos -> pos
   val var_pos : var -> pos -> pos
   val atom_pos : atom -> pos -> pos
   val atom_class_pos : atom_class -> pos -> pos

   val del_pos : (formatter -> unit) -> loc -> pos
   val del_exp_pos : (formatter -> unit) -> pos -> pos

   (* Utilities *)
   val loc_of_pos : pos -> loc
   val pp_print_pos : formatter -> pos -> unit
end

module type NameSig =
sig
   val name : string
end

module MakePos (Name : NameSig) : PosSig =
struct
   module Name' =
   struct
      type t = item

      let name = Name.name

      let loc_of_value = loc_of_value
      let pp_print_value = pp_print_value
   end

   module Pos = Position.MakePos (Name')

   include Pos

   let exp_pos e = base_pos (Exp e)
   let atom_pos a pos = cons_pos (Atom a) pos
   let atom_class_pos ac pos = cons_pos (AtomClass ac) pos
   let var_exp_pos v = base_pos (Symbol v)
   let string_exp_pos s = base_pos (String s)
   let var_pos = symbol_pos
end

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
