(*
   Transform SpecialCall statements into the real MIR primitives
   This includes the environment packaging system that are required
   for most of these primitives.
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Debug
open Symbol
open Location

open Frame_type

open Sizeof_const

open Mir
open Mir_ds
open Mir_pos
open Mir_exn
open Mir_special_sig

module Pos = MakePos (struct let name = "Mir_special" end)
open Pos


module MirSpecial (Frame : BackendSig) : MirSpecialSig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)
   module MirType = Mir_type.MirType (Frame)
   open MirUtil

   (***  Environment Maintenance  ***)


   (*
      envs are used to carry information from the SpecialCall rewrite
      functions that is needed to emit the new function stubs.  Both
      Atomic and SysMigrate need this facility to generate the stub
      functions for unpacking their environments.

      stub_type indicates the type of stub function required:
         StubAtomic     Takes an integer i and an environment env;
                        unpacks the formals list from env, and calls
                        the final function f (i :: formals).
         StubMigrate    Takes an environment env; unpacks the formals
                        list from env, and calls f (formals)

      stub_info = type * f' * formals_list
         Build a stub function of indicated type.  The stub function
         should unpack the formals in formals_list from an environment
         and call function f with those formals.  Extra arguments may
         be passed through, depending on the type of stub.  The name
         of the STUB function is stored in f'.

      envs contains the list of all required stub functions (stored
      in a symbol table using the FINAL function name as the key)
      Currently, envs contains no other information.
    *)
   type stub_type =
      StubAtomic
    | StubMigrate

   type stub_info = stub_type * var * (atom_class * var) list

   type envs = {
      env_stub    :  stub_info SymbolTable.t
   }


   (* Standard environment manipulation *)
   let env_empty = SymbolTable.empty
   let env_add = SymbolTable.add
   let env_find = SymbolTable.find
   let env_fold = SymbolTable.fold


   (***  Environment Packaging  ***)


   (*
      When most of these primitives are translated from SpecialCall
      (which lists live variables as normal function arguments) to
      the MIR primitives, the live variables must be packaged into
      a memory block -- for example, in migration we can't have any
      values in registers, and pointers have to be stored as indices
      for migration to succeed.

      This process is called ``environment packaging'' -- to package,
      we convert a list of arguments into code to allocate an aggr.
      block, and store the contents of all registers into the block.
      To unpackage in the destination function, we simply read those
      arguments from the memory block back into registers.

      For packaging we have to generate a new, stub ``destination''
      function, because someone may have been calling the original
      function with the normal arguments -- our custom destination
      function only takes an environment, however :)

      We have to be careful with types -- for packaging, we use the
      atom classes of the arguments, for unpackaging we use the fun.
      formals.  These types had better agree!!
    *)


   (* store_atom_in_env
      Stores the atom a into memory at env[off].  Appropriate
      conversions are performed here (e.g. pointer to hashed
      index values).  *)
   let store_atom_in_env loc ac env off a e =
      let env = AtomVar (ac_aggr_pointer, env) in
      let off = AtomRawInt (Rawint.of_int32 precision_native_int unsigned_int off) in
      let set_mem ac a =
         make_exp loc (SetMem (ac, PtrAggr, env, off, a, e))
      in
         match ac with
            ACInt
          | ACRawInt _
          | ACFloat _ ->
               set_mem ac a
          | ACPoly ->
               (* TEMP: is this valid treatment for ACPoly?? *)
               (* (modelling off of mir_fir code) *)
               let a = AtomUnop (PointerOfPolyOp PtrAggr, a) in
               let a = AtomUnop (UnsafeIndexOfPointerOp PtrAggr, a) in
                  set_mem ac_native_unsigned a
          | ACPointer ptrty ->
               set_mem ac_native_unsigned (AtomUnop (UnsafeIndexOfPointerOp ptrty, a))
          | ACFunction funty ->
               set_mem ac_native_unsigned (AtomUnop (UnsafeIndexOfFunctionOp funty, a))
          | ACPointerInfix _ ->
               raise (Invalid_argument "store_atom_in_env: ACPointerInfix not allowed")


   (* package_vars
      Package up all arguments (atoms) given into an environment.
      This allocates an environment, and stores each atom in args
      into env.  e_cont takes the symbol name for the environment
      and builds the remainder of the expression.  The full expr
      as well as the symbol name of the env are returned.  *)
   let package_vars loc args e_cont =
      let env = new_symbol_string "env" in
      let a_env = AtomVar (ac_aggr_pointer, env) in

      (* Compute the final part of the expression *)
      let (e : exp) = e_cont env in

      (* package_var
         Store variables into the memory block.  off is the
         current offset into env; the total size of the block
         and the expression to package are returned. *)
      let rec package_var off atoms =
         match atoms with
            [] ->
               off, e
          | a :: atoms ->
               let ac = atom_class_of_atom a in
               let ac_size = sizeof_atom_class ac in
               let next_off = Int32.add off ac_size in
               let size, e = package_var next_off atoms in
               let e = store_atom_in_env loc ac env off a e in
                  size, e
      in (* end of package_var *)

      (* Allocate the initial block *)
      let size, e = package_var Int32.zero args in
      let size = AtomRawInt (Rawint.of_int32 precision_native_int unsigned_int size) in
      let e = LetAlloc (env, AllocMalloc size, e) in
         env, make_exp loc e


   (* load_var_from_env
      Loads the value in memory at env[off] into a new variable.
      Appropriate conversions are performed here (e.g. from hashed
      index into pointer).  *)
   let load_var_from_env loc ac v env off e =
      let env = AtomVar (ac_aggr_pointer, env) in
      let off = AtomRawInt (Rawint.of_int32 precision_native_int unsigned_int off) in
      let read_mem ac = AtomBinop (MemOp (ac, PtrAggr), env, off) in
      let e =
         match ac with
            ACInt
          | ACRawInt _
          | ACFloat _ ->
               LetAtom (v, ac, read_mem ac, e)
          | ACPoly ->
               LetAtom (v, ac,
                        AtomUnop (PolyOfPointerOp PtrAggr,
                                  AtomUnop (UnsafePointerOfIndexOp PtrAggr, read_mem ac_native_unsigned)),
                        e)
          | ACPointer ptrty ->
               PointerIndexCheck (new_symbol_string "special_check", ptrty,
                                  v, ac, read_mem ac_native_unsigned, e)
          | ACFunction funty ->
               FunctionIndexCheck (new_symbol_string "special_check", funty,
                                   v, ac, read_mem ac_native_unsigned, e)
          | ACPointerInfix _ ->
               raise (Invalid_argument "load_var_from_env: ACPointerInfix not allowed")
      in
         make_exp loc e


   (* unpackage_vars
      Unpackage up all arguments (specified here as formals, or
      atom_class * var pairs) given from an environment.  This
      loads each atom from memory into the formals, and stores
      them into variables corresponding to the given formals.
      Again, e_cont takes the symbol name for the environment
      and builds the remainder of the expression.  The full expr
      as well as the symbol name of the env are returned.  This
      function is generally intended to be used at the beginning
      of a function, so a new env name is constructed; when the
      symbol name is returned, it can be incorporated into the
      formals of a stub function.  *)
   let unpackage_vars loc formals e_cont =
      let env = new_symbol_string "env" in
      let a_env = AtomVar (ac_aggr_pointer, env) in

      (* Compute the final part of the expression *)
      let e = e_cont env in

      (* Remove variables from the memory block. *)
      let rec unpackage_var off formals =
         match formals with
            [] ->
               off, e
          | (ac, v) :: formals ->
               let ac_size = sizeof_atom_class ac in
               let next_off = Int32.add off ac_size in
               let size, e = unpackage_var next_off formals in
               let e = load_var_from_env loc ac v env off e in
                  size, e
      in (* end of unpackage_var *)

      (* Initially, we do a single check to ensure ALL dereferences will
         be valid.  Only need to do an upper bounds check, lower is always
         safe. *)
      let size, e = unpackage_var Int32.zero formals in
      let size = AtomRawInt (Rawint.of_int32 precision_native_int unsigned_int size) in
      let check_label = new_symbol_string "check_migrate" in
      let e = UpperBoundsCheck (check_label, PtrAggr, a_env, size, e) in
         env, make_exp loc e


   (***  Rewrite SpecialCalls  ***)


   (* formals_of_atoms
      Generates new formals for the list of atoms given. *)
   let formals_of_atoms atoms =
      List.map (fun a ->
         let v = new_symbol_string "arg" in
         let ac = atom_class_of_atom a in
            ac, v) atoms


   (* vars_of_formals
      Extracts the variables named in the list of formals.
      This returns AtomVars of each variable name, with an
      appropriate atom class.  *)
   let vars_of_formals formals =
      List.map (fun (ac, v) -> AtomVar (ac, v)) formals


   (* compare_formals
      Check that two sets of formals are equivalent.  *)
   let rec compare_formals pos formals formals' =
      let pos = string_pos "compare_formals" pos in
         match formals, formals' with
            (ac, _) :: formals, (ac', _) :: formals' ->
               MirType.atom_classes_agree pos ac ac' && compare_formals pos formals formals'
          | [], [] ->
               true
          | _ ->
               false


   (* check_for_stub
      Checks to see if a stub function for function <f> has already been
      defined; if so, then return that stub function's information. Else,
      generate information for a new stub function and add it to the
      envs.  This returns the new envs, and the name for the STUB fun.  *)
   let check_for_stub envs pos f ty args =
      let pos = string_pos "check_for_stub" pos in
      let formals = formals_of_atoms args in
         try
            (* Try to find an entry for f in the table. *)
            let ty', f', formals' = env_find envs.env_stub f in
               (* We found an entry!  Make sure it is valid... *)
               if ty <> ty' then
                  raise (MirException (pos, InternalError "Attempted to generate incompatible stubs for same target function"));
               if not (compare_formals pos formals formals') then
                  raise (MirException (pos, InternalError "Attempted to generate incompatible stubs for same target function"));
               envs, f'
         with
            Not_found ->
               (* First time we've encountered this function *)
               let f' = new_symbol_string "mir_special_target" in
               let stub = env_add envs.env_stub f (ty, f', formals) in
                  { envs with env_stub = stub }, f'


   (* compute_index_of_pointer
      Emits MIR code to compute an unsafe index of an aggregate pointer
      stored in the indicated variable.  The continuation is called with
      an atom containing the index value.  *)
   let compute_index_of_pointer loc ptr cont =
      let index = new_symbol_string "index" in
         make_exp loc (LetAtom (index, ac_native_unsigned,
                  AtomUnop (UnsafeIndexOfPointerOp PtrAggr, AtomVar (ac_aggr_pointer, ptr)),
                  cont (AtomVar (ac_native_unsigned, index))))


   (* compute_pointer_of_index
      Emits MIR code to convert an unsafe index (given in a particular
      variable) to an aggregate pointer, stored to the indicated result
      variable.  *)
   let compute_pointer_of_index loc ptr index e =
      make_exp loc (LetAtom (ptr, ac_aggr_pointer,
               AtomUnop (UnsafePointerOfIndexOp PtrAggr, AtomVar (ac_native_unsigned, index)),
               e))


   (* rewrite_migrate_special_call
      Rewrites a TailSysMigrate call to use SysMigrate.  *)
   let rewrite_migrate_special_call envs pos loc label loc_base loc_off f args =
      let pos = string_pos "rewrite_migrate_special_call" pos in

      (* Check for a stub function to use for this migrate call. *)
      let envs, f' = check_for_stub envs pos f StubMigrate args in

      (* Emit the PACKAGE and migrate call code *)
      let env_index = new_symbol_string "env_index" in
      let _, e =
         package_vars loc args (fun env ->
            compute_index_of_pointer loc env (fun env ->
               make_exp loc (SysMigrate (label, loc_base, loc_off, f', env))))
      in
         envs, e


   (* rewrite_atomic_special_call
      Rewrites a TailAtomic call to use Atomic.  Atomic requires
      an additional stub function, so we update envs to store the
      information for that stub function here, as well. *)
   let rewrite_atomic_special_call envs pos loc f i args =
      let pos = string_pos "rewrite_atomic_special_call" pos in

      (* Check for a stub function to use for this migrate call. *)
      let envs, f' = check_for_stub envs pos f StubAtomic args in

      (* Emit the code for PACKAGE and atomic call *)
      let _, e =
         package_vars loc args (fun env ->
            make_exp loc (Atomic (f', i, AtomVar (ac_aggr_pointer, env))))
      in
         envs, e


   (* rewrite_atomic_rollback_special_call *)
   let rewrite_atomic_rollback_special_call loc envs level i =
      envs, make_exp loc (AtomicRollback (level, i))


   (* rewrite_atomic_commit_special_call *)
   let rewrite_atomic_commit_special_call loc envs level f args =
      envs, make_exp loc (AtomicCommit (level, make_exp loc (TailCall (DirectCall, f, args))))


   (* rewrite_special_calls
      Rewrites the SpecialCalls in this expression to use the
      proper MIR primitives.  *)
   let rec rewrite_special_calls envs e =
      let pos = exp_pos e in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetAtom (v, ac, a, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (LetAtom (v, ac, a, e))
          | TailCall _
          | Atomic _
          | AtomicRollback _ ->
               envs, e
          | IfThenElse (op, a1, a2, e1, e2) ->
               let envs, e1 = rewrite_special_calls envs e1 in
               let envs, e2 = rewrite_special_calls envs e2 in
                  envs, make_exp loc (IfThenElse (op, a1, a2, e1, e2))
          | IfType (obj, names, name, v, e1, e2) ->
               let envs, e1 = rewrite_special_calls envs e1 in
               let envs, e2 = rewrite_special_calls envs e2 in
                  envs, make_exp loc (IfType (obj, names, name, v, e1, e2))
          | Match (a, ac, cases) ->
               let process_case (envs, cases) (s, e) =
                  let envs, e = rewrite_special_calls envs e in
                  let cases = (s, e) :: cases in
                     envs, cases
               in
               let envs, cases = List.fold_left process_case (envs, []) cases in
               let cases = List.rev cases in
                  envs, make_exp loc (Match (a, ac, cases))
          | LetExternal (v, ac, f, atoms, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (LetExternal (v, ac, f, atoms, e))
          | LetExternalReserve (rinfo, v, ac, f, atoms, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (LetExternalReserve (rinfo, v, ac, f, atoms, e))
          | Reserve (rinfo, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (Reserve (rinfo, e))
          | LetAlloc (v, op, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (LetAlloc (v, op, e))
          | SetMem (ac, op, a1, a2, a3, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (SetMem (ac, op, a1, a2, a3, e))
          | SetGlobal (ac, v, a, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (SetGlobal (ac, v, a, e))
          | BoundsCheck (label, ty, ptr, start, stop, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (BoundsCheck (label, ty, ptr, start, stop, e))
          | LowerBoundsCheck (label, ty, ptr, start, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (LowerBoundsCheck (label, ty, ptr, start, e))
          | UpperBoundsCheck (label, ty, ptr, stop, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (UpperBoundsCheck (label, ty, ptr, stop, e))
          | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (PointerIndexCheck (label, ptrty, v, ac, index, e))
          | FunctionIndexCheck (label, funty, v, ac, index, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (FunctionIndexCheck (label, funty, v, ac, index, e))
          | Memcpy (ty, a1, a2, a3, a4, a5, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (Memcpy (ty, a1, a2, a3, a4, a5, e))
          | Debug (info, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (Debug (info, e))
          | PrintDebug (v1, v2, info, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (PrintDebug (v1, v2, info, e))
          | CommentFIR (fir, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (CommentFIR (fir, e))
          | SysMigrate (label, loc_base, loc_off, f, env) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (SysMigrate (label, loc_base, loc_off, f, env))
          | AtomicCommit (level, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (AtomicCommit (level, e))
          | CopyOnWrite (rinfo, ptr, e) ->
               let envs, e = rewrite_special_calls envs e in
                  envs, make_exp loc (CopyOnWrite (rinfo, ptr, e))
          | SpecialCall (TailSysMigrate (label, loc_base, loc_off, f, args)) ->
               rewrite_migrate_special_call envs pos loc label loc_base loc_off f args
          | SpecialCall (TailAtomic (f, i, args)) ->
               rewrite_atomic_special_call envs pos loc f i args
          | SpecialCall (TailAtomicRollback (level, i)) ->
               rewrite_atomic_rollback_special_call loc envs level i
          | SpecialCall (TailAtomicCommit (level, f, args)) ->
               rewrite_atomic_commit_special_call loc envs level f args


   (* rewrite_special_calls
      Interface for above; uses function definitions.  *)
   let rewrite_special_calls funs =
      let envs = { env_stub = env_empty } in
      let envs, funs =
         env_fold (fun (envs, funs) f (debug, formals, e) ->
            let envs, e = rewrite_special_calls envs e in
            let funs = env_add funs f (debug, formals, e) in
               envs, funs) (envs, env_empty) funs
      in
         envs, funs


   (***  Emit new Stub Functions  ***)


   (* emit_atomic_special_call_wrapper
      Emits a new function stub for an Atomic statement.  Note
      that f is the original function name, f' is the name of
      the STUB function to create.  *)
   let special_loc = bogus_loc "<Mir_special>"

   let emit_atomic_special_call_wrapper loc funs f f' formals =
      let i = new_symbol_string "int_param" in
      let atoms = vars_of_formals ((ac_native_int, i) :: formals) in
      let env, e =
         unpackage_vars loc formals (fun env ->
            make_exp loc (TailCall (DirectCall, f, atoms)))
      in
      let formals' = [i, ac_native_int; env, ac_aggr_pointer] in
      let funs = env_add funs f' (special_loc, formals', e) in
         funs


   (* emit_migrate_special_call_wrapper
      Emits a new function stub for a SysMigrate statement.  Note
      that f is the original function name, f' is the name of the
      STUB function to generate.  *)
   let emit_migrate_special_call_wrapper loc funs f f' formals =
      let atoms = vars_of_formals formals in
      let env, e =
         unpackage_vars loc formals (fun env ->
            make_exp loc (TailCall (DirectCall, f, atoms)))
      in
      let env' = new_symbol env in
      let formals' = [env', ac_native_unsigned] in
      let e = compute_pointer_of_index loc env env' e in
      let funs = env_add funs f' (special_loc, formals', e) in
         funs


   (* emit_special_call_wrappers
      Emits stub functions (aka wrappers) for SpecialCalls. *)
   let emit_special_call_wrappers loc funs envs =
      env_fold (fun funs f (ty, f', formals) ->
         match ty with
            StubAtomic ->
               emit_atomic_special_call_wrapper loc funs f f' formals
          | StubMigrate ->
               emit_migrate_special_call_wrapper loc funs f f' formals) funs envs.env_stub


   (* insert_new_functions
      Atomicity generates new functions that escape.  These functions
      MUST be added to the marshal_funs list, and must be added in a
      well-defined order.  The rule we adopt is the newly-generated
      functions will always be inserted after their sponsor function
      in the list... *)
   let insert_new_functions fun_order envs =
      let rec insert = function
         [] ->
            []
       | f :: fun_order ->
            try
               let _, f', _ = env_find envs.env_stub f in
                  f :: f' :: insert fun_order
            with
               Not_found ->
                  f :: insert fun_order
      in
      let fun_order = insert fun_order in
         fun_order


   (***  Interface Code  ***)


   (* special *)
   let special prog =
      let { prog_funs      = funs;
            prog_fun_order = fun_order;
          } = prog
      in
      let loc = bogus_loc "<Mir_special>" in
      let envs, funs = rewrite_special_calls funs in
      let funs = emit_special_call_wrappers loc funs envs in
      let fun_order = insert_new_functions fun_order envs in
      let prog = { prog with
                   prog_funs        = funs;
                   prog_fun_order   = fun_order;
                 }
      in
         if debug Mir_state.debug_print_mir then
            Mir_print.debug_prog "After special" prog;
         prog

   let special = Fir_state.profile "Mir_special.special" special


end (* struct *)
