(*
   Assemble the given FIR code to MIR code.
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Debug
open Symbol
open Flags

open Frame_type
open Mir_codegen_sig


module MirCodegen (Frame : BackendSig) : MirCodegenSig =
struct
   module MirFir = Mir_fir.MirOfFir (Frame)
   module MirType = Mir_type.MirType (Frame)
   module MirSpecial = Mir_special.MirSpecial (Frame)
   module MirOpt = Mir_optimize.MirOpt (Frame)
   module MirMemory = Mir_memory.MirMemory (Frame)
   module MirSimplify = Mir_simplify.MirSimplify (Frame)


   (***  Compilation (low-level)  ***)


   (* compile_mir_of_fir
      Compile the given FIR code by translating first to MIR.
      This will typecheck the FIR (which may come from a source
      that is not trusted).  *)
   let compile_mir_of_fir prog =
      (* Typecheck the FIR code one last time *)
      let () =
         if debug_level Fir_state.debug_print_fir 2 then
            Fir_print.debug_mprog "*** Backend: initial FIR" prog
      in
      let prog = Fir_standardize.standardize_mprog prog in
      let () =
         if debug_level Fir_state.debug_print_fir 2 then
            Fir_print.debug_mprog "*** Backend: standardized FIR" prog
      in
      let prog = Fir_check.check_mprog prog in
      let () =
         if debug_level Fir_state.debug_print_fir 1 then
            Fir_print.debug_mprog "*** Backend: checked FIR" prog
      in
      let prog =
         if std_flags_get_bool "mir.debug.double_check" then
            (* Some programs crash when check is run twice; this affects
               migration because migrated programs are checked twice --
               once for the initial program, and once each time they move
               to a new machine.  This lets me simulate the double-check
               to try to find the glitch in FIR type inference.  -JDS *)
            (* JDS, 2002.07.20:  I think this is obsolete, now that Jason
               has rewritten FIR type inference... *)
            let prog = Fir_check.check_mprog prog in
            let () =
               if debug_level Fir_state.debug_print_fir 1 then
                  Fir_print.debug_mprog "*** Backend: checked FIR (second attempt)" prog
            in
               prog
         else
            (* Only check the FIR once; this is the default *)
            prog
      in

      (* Print the final FIR code at this point (if desired) *)
      let _ =
         if !Mir_state.debug_print_final_fir then
            Fir_print.debug_mprog "Final FIR" prog;
      in

      (* Generate MIR *)
      let prog = MirFir.compile_prog prog in
      let ()   = MirType.check_prog prog in
      let prog = MirSpecial.special prog in
      let ()   = MirType.check_prog prog in
      let prog = MirOpt.optimize prog in
      let ()   = MirType.check_prog prog in
      let prog = MirMemory.memory prog in
      let ()   = MirType.check_prog prog in
      let prog = MirSimplify.simplify_prog prog in
      let ()   = MirType.check_prog prog in

      (* We alter the code a bit, here, so what the heck:
         let's optimize again.  (hint: simplify emits new
         code, we need at the least, valias to clean up
         the code again to avoid a 10% performance hit.) *)
      let prog = MirOpt.optimize prog in
      let ()   = MirType.check_prog prog in

      (* Print the MIR code at this point (if desired) *)
      let _ =
         if !Mir_state.debug_print_mir || !Mir_state.debug_print_final_mir then
            Mir_print.debug_prog "Final MIR" prog;
      in
         prog

   let compile_mir_of_fir prog =
      Fir_state.profile "Mir_codegen.compile_mir_of_fir" compile_mir_of_fir prog
end (* struct *)


let () = std_flags_help_section_text "mir.debug"
   "MIR debugging options."

let () = std_flags_register_list_help "mir.debug"
  ["mir.debug.double_check",     FlagBool false,
                                 "Run FIR type inference twice on the code before compiling to" ^
                                 " MIR.  This sometimes reveals errors in FIR type inference that" ^
                                 " can turn up in system migration, but is not useful otherwise."]
