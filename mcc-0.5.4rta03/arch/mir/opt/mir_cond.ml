(*
   Perform IfThenElse optimization on the MIR code
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Format

open Debug
open Symbol
open Flags

open Frame_type

open Mir
open Mir_ds
open Mir_pos
open Mir_exn
open Mir_print
open Mir_valias
open Mir_standardize

open Fir_set
open Fir_state

module Pos = MakePos (struct let name = "Mir_cond" end)
open Pos


module type MirCondSig =
sig
   val rewrite_matches : prog -> prog
end


module MirCond (Frame : BackendSig) : MirCondSig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)
   open MirUtil


   (*
      Rewrite match statements immediately following a relative
      operator to use our IfThenElse primitive instead.  This
      optimization is fairly sensitive to the way code is emitted
      in the FIR->MIR conversion, so check it when major changes
      are made to make sure the pattern match is still working.
    *)


   (***  Utilities  ***)


   (* Position utility *)
   let exp_pos pos = string_pos "Mir_cond" (exp_pos pos)


   (* venv utilities *)
   let venv_empty = SymbolTable.empty
   let venv_add = SymbolTable.add
   let venv_mem = SymbolTable.mem
   let venv_lookup venv pos v =
      try SymbolTable.find venv v with
         Not_found -> raise (MirException (pos, UnboundVar v))


   (* rewrite_var
      Rewrites a variable, substituting variable names as necessary.  *)
   let rewrite_var env v =
      try SymbolTable.find env v with
         Not_found -> v


   (* rewrite_vars
      Plural version of above.  *)
   let rewrite_vars env vars = List.map (rewrite_var env) vars


   (* remove_var
      Removes a variable from the rewrite environment.  *)
   let remove_var env v =
      SymbolTable.remove env v


   (* rewrite_atom
      Rewrites an atom, substituting variable names as necessary.  *)
   let rec rewrite_atom env a =
      match a with
         AtomVar (ac, v) ->
            AtomVar (ac, rewrite_var env v)
       | AtomFunVar (ac, v) ->
            AtomFunVar (ac, rewrite_var env v)
       | AtomUnop (op, a) ->
            AtomUnop (op, rewrite_atom env a)
       | AtomBinop (op, a1, a2) ->
            AtomBinop (op, rewrite_atom env a1, rewrite_atom env a2)
       | AtomInt _
       | AtomRawInt _
       | AtomFloat _ ->
            a


   (* rewrite_atoms
      Plural version of above.  *)
   let rewrite_atoms env atoms = List.map (rewrite_atom env) atoms


   (* rewrite_alloc_op
      Rewrites an allocation operator.  *)
   let rewrite_alloc_op env op =
      match op with
         AllocTuple atoms ->
            AllocTuple (rewrite_atoms env atoms)
       | AllocArray atoms ->
            AllocArray (rewrite_atoms env atoms)
       | AllocMArray (atoms, a) ->
            AllocMArray (rewrite_atoms env atoms, rewrite_atom env a)
       | AllocUnion (tag, atoms) ->
            AllocUnion (tag, rewrite_atoms env atoms)
       | AllocMalloc a ->
            AllocMalloc (rewrite_atom env a)


   (* rewrite_reserve_info
      Rewrites the reserve information (only need vars) *)
   let rewrite_reserve_info env rinfo =
      let label, vars, mem, ptr = rinfo in
      let vars = rewrite_vars env vars in
      let mem = rewrite_atom env mem in
      let ptr = rewrite_atom env ptr in
         (label, vars, mem, ptr)


   (* translate_relop
      Try to rewrite the operation specified as a relative operator
      that can be used in an if/then/else expression.  If the operator
      given is NOT a relative operator, then None is returned.  Other-
      wise, we will return Some of the corresponding if/then/else
      operator.  *)
   let translate_relop = function
      EqOp(ac) ->  Some (REqOp ac)
    | NeqOp(ac) -> Some (RNeqOp ac)
    | LtOp(ac) ->  Some (RLtOp ac)
    | LeOp(ac) ->  Some (RLeOp ac)
    | GtOp(ac) ->  Some (RGtOp ac)
    | GeOp(ac) ->  Some (RGeOp ac)
    | AndOp(ac) -> Some (RAndOp ac)
    | _           ->  None


   (***  Conditional Rewrite - Transform Match to IfThenElse  ***)


   (* rewrite_match_rawint
      Attempt to rewrite the match given.  The parametres are as given
      in the pattern that matches the LetAtom/Match form we see in the
      expression stream.  *)
   let rec rewrite_match_rawint env loc v op ac a1 a2 s1 e1 s2 e2 =
      let is_true_set s =
         RawIntSet.equal s native_true_set
         || RawIntSet.equal s native_true_set_alt
         || RawIntSet.equal s unsigned_true_set
      in
      let is_false_set s =
         RawIntSet.equal s native_false_set
         || RawIntSet.equal s unsigned_false_set
      in

      (* Rewrite the basic atoms *)
      let a1 = rewrite_atom env a1 in
      let a2 = rewrite_atom env a2 in
      let env = remove_var env v in

         (* Find out if we can rewrite this match *)
         match translate_relop op with
            Some op when is_true_set s1 && is_false_set s2 ->
               let env1 = SymbolTable.add env v (new_symbol v) in
               let env2 = SymbolTable.add env v (new_symbol v) in
               let e1 = make_exp loc (LetAtom (v, ac, native_true_value, rewrite_matches env1 e1)) in
               let e2 = make_exp loc (LetAtom (v, ac, native_false_value, rewrite_matches env2 e2)) in
                  make_exp loc (IfThenElse (op, a1, a2, e1, e2))
          | Some op when is_false_set s1 && is_true_set s2 ->
               let env1 = SymbolTable.add env v (new_symbol v) in
               let env2 = SymbolTable.add env v (new_symbol v) in
               let e1 = make_exp loc (LetAtom (v, ac, native_false_value, rewrite_matches env1 e1)) in
               let e2 = make_exp loc (LetAtom (v, ac, native_true_value, rewrite_matches env2 e2)) in
                  make_exp loc (IfThenElse (op, a1, a2, e2, e1))
          | _ ->
               let e1 = rewrite_matches env e1 in
               let e2 = rewrite_matches env e2 in
                  make_exp loc (LetAtom (v, ac, AtomBinop(op, a1, a2),
                  make_exp loc (Match (AtomVar (ac, v), ac,
                                       [RawIntSet s1, e1; RawIntSet s2, e2]))))


   (* rewrite_matches
      Rewrite all matches that can be converted to if/then/else
      within an entire expression.  *)
   and is_match_exp v e =
      match dest_exp_core e with
         CommentFIR (_, e) ->
            is_match_exp v e
       | Match (AtomVar (_, v'), ACRawInt _, [RawIntSet _, _; RawIntSet _, _]) ->
            Symbol.eq v v'
       | _ ->
            false

   and dest_match e =
      match dest_exp_core e with
         CommentFIR (_, e) ->
            dest_match e
       | Match (_, ACRawInt _, [RawIntSet s1, e1; RawIntSet s2, e2]) ->
            s1, e1, s2, e2
       | _ ->
            raise (Invalid_argument "Mir_cond.dest_match")

   and rewrite_matches env e =
      let loc = loc_of_exp e in
         match dest_exp_core e with
            LetAtom (v, ac, AtomBinop (op, a1, a2), e)
            when is_match_exp v e ->
               let s1, e1, s2, e2 = dest_match e in
                  rewrite_match_rawint env loc v op ac a1 a2 s1 e1 s2 e2

          | LetAtom (v, ac, a, e) ->
               let a = rewrite_atom env a in
               let env = remove_var env v in
               let e = rewrite_matches env e in
                  make_exp loc (LetAtom (v, ac, a, e))
          | LetAlloc (v, op, e) ->
               let op = rewrite_alloc_op env op in
               let env = remove_var env v in
               let e = rewrite_matches env e in
                  make_exp loc (LetAlloc (v, op, e))
          | LetExternal (v, ac, f, atoms, e) ->
               let atoms = rewrite_atoms env atoms in
               let env = remove_var env v in
               let e = rewrite_matches env e in
                  make_exp loc (LetExternal (v, ac, f, atoms, e))
          | LetExternalReserve (rinfo, v, ac, f, atoms, e) ->
               let rinfo = rewrite_reserve_info env rinfo in
               let atoms = rewrite_atoms env atoms in
               let env = remove_var env v in
               let e = rewrite_matches env e in
                  make_exp loc (LetExternalReserve (rinfo, v, ac, f, atoms, e))
          | TailCall (op, f, atoms) ->
               let f = rewrite_var env f in
               let atoms = rewrite_atoms env atoms in
                  make_exp loc (TailCall (op, f, atoms))
          | Match (a, ac, cases) ->
               let a = rewrite_atom env a in
               let cases = List.map (fun (set, e) -> set, rewrite_matches env e) cases in
                  make_exp loc (Match (a, ac, cases))
          | IfThenElse (op, a1, a2, e1, e2) ->
               let a1 = rewrite_atom env a1 in
               let a2 = rewrite_atom env a2 in
               let e1 = rewrite_matches env e1 in
               let e2 = rewrite_matches env e2 in
                  make_exp loc (IfThenElse (op, a1, a2, e1, e2))
          | IfType (obj, names, name, v, e1, e2) ->
               let obj   = rewrite_atom env obj in
               let names = rewrite_atom env names in
               let name  = rewrite_atom env name in
               let env'  = remove_var env v in
               let e1    = rewrite_matches env' e1 in
               let e2    = rewrite_matches env  e2 in
                  make_exp loc (IfType (obj, names, name, v, e1, e2))
          | SetMem (ac, ptrty, a1, a2, a3, e) ->
               let a1 = rewrite_atom env a1 in
               let a2 = rewrite_atom env a2 in
               let a3 = rewrite_atom env a3 in
               let e = rewrite_matches env e in
                  make_exp loc (SetMem (ac, ptrty, a1, a2, a3, e))
          | SetGlobal (ac, v, a, e) ->
               let v = rewrite_var env v in
               let a = rewrite_atom env a in
               let e = rewrite_matches env e in
                  make_exp loc (SetGlobal (ac, v, a, e))
          | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
               let ptr = rewrite_atom env ptr in
               let start = rewrite_atom env start in
               let stop = rewrite_atom env stop in
               let e = rewrite_matches env e in
                  make_exp loc (BoundsCheck (label, ptrty, ptr, start, stop, e))
          | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
               let ptr = rewrite_atom env ptr in
               let start = rewrite_atom env start in
               let e = rewrite_matches env e in
                  make_exp loc (LowerBoundsCheck (label, ptrty, ptr, start, e))
          | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
               let ptr = rewrite_atom env ptr in
               let stop = rewrite_atom env stop in
               let e = rewrite_matches env e in
                  make_exp loc (UpperBoundsCheck (label, ptrty, ptr, stop, e))
          | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
               let index = rewrite_atom env index in
               let env = remove_var env v in
                  make_exp loc (PointerIndexCheck (label, ptrty, v, ac, index, e))
          | FunctionIndexCheck (label, funty, v, ac, index, e) ->
               let index = rewrite_atom env index in
               let env = remove_var env v in
                  make_exp loc (FunctionIndexCheck (label, funty, v, ac, index, e))
          | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
               let a1 = rewrite_atom env a1 in
               let a2 = rewrite_atom env a2 in
               let a3 = rewrite_atom env a3 in
               let a4 = rewrite_atom env a4 in
               let a5 = rewrite_atom env a5 in
               let e = rewrite_matches env e in
                  make_exp loc (Memcpy (ptrty, a1, a2, a3, a4, a5, e))
          | Debug (info, e) ->
               make_exp loc (Debug (info, rewrite_matches env e))
          | PrintDebug (v1, v2, a, e) ->
               make_exp loc (PrintDebug (v1, v2, a, rewrite_matches env e))
          | CommentFIR (fir, e) ->
               make_exp loc (CommentFIR (fir, rewrite_matches env e))
          | SpecialCall (TailSysMigrate (label, loc_base, loc_off, f, args)) ->
               let loc_base = rewrite_atom env loc_base in
               let loc_off  = rewrite_atom env loc_off in
               let args     = rewrite_atoms env args in
                  make_exp loc (SpecialCall (TailSysMigrate (label, loc_base, loc_off, f, args)))
          | SpecialCall (TailAtomic (f, i, args)) ->
               let i    = rewrite_atom env i in
               let args = rewrite_atoms env args in
                  make_exp loc (SpecialCall (TailAtomic (f, i, args)))
          | SpecialCall (TailAtomicRollback (level, i)) ->
               let level = rewrite_atom env level in
               let i = rewrite_atom env i in
                  make_exp loc (SpecialCall (TailAtomicRollback (level, i)))
          | SpecialCall (TailAtomicCommit (level, f, args)) ->
               let level = rewrite_atom env level in
               let args = rewrite_atoms env args in
                  make_exp loc (SpecialCall (TailAtomicCommit (level, f, args)))
          | SysMigrate (label, dptr, doff, f, e) ->
               let f = rewrite_var env f in
               let dptr = rewrite_atom env dptr in
               let doff = rewrite_atom env doff in
               let e = rewrite_atom env e in
                  make_exp loc (SysMigrate (label, dptr, doff, f, e))
          | Atomic (f, i, e) ->
               let f = rewrite_var env f in
               let i = rewrite_atom env i in
               let e = rewrite_atom env e in
                  make_exp loc (Atomic (f, i, e))
          | AtomicRollback (level, i) ->
               let level = rewrite_atom env level in
               let i = rewrite_atom env i in
                  make_exp loc (AtomicRollback (level, i))
          | AtomicCommit (level, e) ->
               let level = rewrite_atom env level in
               let e = rewrite_matches env e in
                  make_exp loc (AtomicCommit (level, e))
          | CopyOnWrite (rinfo, ptr, e) ->
               let rinfo = rewrite_reserve_info env rinfo in
               let ptr = rewrite_atom env ptr in
               let e = rewrite_matches env e in
                  make_exp loc (CopyOnWrite (rinfo, ptr, e))
          | Reserve (rinfo, e) ->
               let rinfo = rewrite_reserve_info env rinfo in
               let e = rewrite_matches env e in
                  make_exp loc (Reserve (rinfo, e))


   (***  Remove - Remove Unconditional IfThenElse Statements  ***)


   (* remove_unconditional_cond
      This occurs once we've realized we can write an IfThenElse expr.
      In many cases, these ``conditional'' expressions turn out to be
      unconditional, usually because we compare against a lower range
      bound.  Let's see if we can simplify the expression further by
      removing this conditional.  Note: this is primarily intended to
      clean up code emitted by MIR, not to clean up source programs
      that use unconditional conds -- therefore it is not thorough!  *)
   let remove_unconditional_cond loc op a1 a2 e1 e2 =
      let default_expr = make_exp loc (IfThenElse (op, a1, a2, e1, e2)) in
         match op, a1, a2 with
            _, AtomRawInt v1, AtomRawInt v2 ->
               begin
               (* Computation is known in advance because both atoms
                  are constant integers.  This happens mostly in the
                  conditional checks after constant simplification. *)
               let build_expr op pre signed =
                  let v1 = Rawint.of_rawint pre signed v1 in
                  let v2 = Rawint.of_rawint pre signed v2 in
                     if op (Rawint.compare v1 v2) 0 then
                        e1
                     else
                        e2
               in (* end of build_expr *)
                  match op with
                     REqOp  (ACRawInt (pre, signed)) -> build_expr (=)  pre signed
                   | RNeqOp (ACRawInt (pre, signed)) -> build_expr (<>) pre signed
                   | RLeOp  (ACRawInt (pre, signed)) -> build_expr (<=) pre signed
                   | RLtOp  (ACRawInt (pre, signed)) -> build_expr (<)  pre signed
                   | RGeOp  (ACRawInt (pre, signed)) -> build_expr (>=) pre signed
                   | RGtOp  (ACRawInt (pre, signed)) -> build_expr (>)  pre signed
                   | _ ->
                        (* Shouldn't have happened, but we don't have pos info.
                           Oh well... we'll just emit default for now. *)
                        default_expr
               end (* atoms are constant int *)

          | RLeOp (ACRawInt (Rawint.Int32, false)), AtomRawInt v1, _ ->
               (* Seen once in awhile in bounds checks *)
               let v1 = Rawint.of_rawint Rawint.Int32 false v1 in
                  if Rawint.compare (Rawint.of_int Rawint.Int32 false 0) v1 == 0 then
                     e1
                  else
                     default_expr

          | RGeOp (ACRawInt (Rawint.Int32, false)), _, AtomRawInt v2 ->
               (* Seen once in awhile in bounds checks *)
               let v2 = Rawint.of_rawint Rawint.Int32 false v2 in
                  if Rawint.compare (Rawint.of_int Rawint.Int32 false 0) v2 == 0 then
                     e1
                  else
                     default_expr
          | _ ->
               default_expr


   (* remove_unconditional_dual_cond
      This function eliminates unconditional comparisons that are
      unconditional BECAUSE they were preceded by another check that
      was more ``complete''.  This happens usually because of the
      BoundsCheck function, which is fond of emitting statements like
         if 24 <= size then
            if 20 < size then
               ...
      This function cleans such expressions out.  The form for the
      arguments is
         if a11 op1 a12 then
            if a21 op2 a22 then
               e1
            else
               e2
         else
            e3
      Again, this function is not thorough -- it is only intended to
      clean up backend MIR code.  *)
   let remove_unconditional_dual_cond loc op1 a11 a12 op2 a21 a22 e1 e2 e3 =
      let default_nested_cond = remove_unconditional_cond loc op2 a21 a22 e1 e2 in
      let default_expr = remove_unconditional_cond loc op1 a11 a12 default_nested_cond e3 in
      let removed_expr = remove_unconditional_cond loc op1 a11 a12 e1 e3 in
         match op1, op2 with
            RLeOp (ACRawInt (_, false)), RLtOp (ACRawInt (_, false)) ->
               begin
               match a11, a21 with
                  AtomRawInt i11, AtomRawInt i21 when Rawint.compare i11 i21 >= 0 ->
                     (* Second conditional is redundant *)
                     removed_expr
                | _ ->
                     (* Second conditional not redundant *)
                     default_expr
               end (* <=, < unsigned *)
          | _ ->
               (* Unknown pair of operators *)
               default_expr


   (* remove_cond
      Interface for above functions. *)
   let remove_cond loc op a1 a2 e1 e2 =
      match dest_exp_core e1 with
         IfThenElse (op', a1', a2', e1', e2') ->
            remove_unconditional_dual_cond loc op a1 a2 op' a1' a2' e1' e2' e2
       | _ ->
            remove_unconditional_cond loc op a1 a2 e1 e2


   (* remove_expr
      Removes unconditional conditionals from the expression.  This should be
      called AFTER rewrite_matches because this function only considers the
      IfThenElse expressions - not Match statements. *)
   let rec remove_expr e =
      let loc = loc_of_exp e in
         match dest_exp_core e with
            IfThenElse (op, a1, a2, e1, e2) ->
               remove_cond loc op a1 a2 (remove_expr e1) (remove_expr e2)
          | LetAtom (v, ac, a, e) ->
               make_exp loc (LetAtom (v, ac, a, remove_expr e))
          | LetAlloc (v, op, e) ->
               make_exp loc (LetAlloc (v, op, remove_expr e))
          | LetExternal (v, ac, f, atoms, e) ->
               make_exp loc (LetExternal (v, ac, f, atoms, remove_expr e))
          | LetExternalReserve (rinfo, v, ac, f, atoms, e) ->
               make_exp loc (LetExternalReserve (rinfo, v, ac, f, atoms, remove_expr e))
          | Match (a, ac, cases) ->
               let process_case (s, e) = (s, remove_expr e) in
                  make_exp loc (Match (a, ac, List.map process_case cases))
          | IfType (obj, names, name, v, e1, e2) ->
               make_exp loc (IfType (obj, names, name, v, remove_expr e1, remove_expr e2))
          | SetMem (ac, ptrty, a1, a2, a3, e) ->
               make_exp loc (SetMem (ac, ptrty, a1, a2, a3, remove_expr e))
          | SetGlobal (ac, v, a, e) ->
               make_exp loc (SetGlobal (ac, v, a, remove_expr e))
          | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
               make_exp loc (BoundsCheck (label, ptrty, ptr, start, stop, remove_expr e))
          | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
               make_exp loc (LowerBoundsCheck (label, ptrty, ptr, start, remove_expr e))
          | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
               make_exp loc (UpperBoundsCheck (label, ptrty, ptr, stop, remove_expr e))
          | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
               make_exp loc (PointerIndexCheck (label, ptrty, v, ac, index, remove_expr e))
          | FunctionIndexCheck (label, funty, v, ac, index, e) ->
               make_exp loc (FunctionIndexCheck (label, funty, v, ac, index, remove_expr e))
          | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
               make_exp loc (Memcpy (ptrty, a1, a2, a3, a4, a5, remove_expr e))
          | Debug (info, e) ->
               make_exp loc (Debug (info, remove_expr e))
          | PrintDebug (v1, v2, a, e) ->
               make_exp loc (PrintDebug (v1, v2, a, remove_expr e))
          | CommentFIR (fir, e) ->
               make_exp loc (CommentFIR (fir, remove_expr e))
          | AtomicCommit (level, e) ->
               make_exp loc (AtomicCommit (level, remove_expr e))
          | CopyOnWrite (rinfo, ptr, e) ->
               make_exp loc (CopyOnWrite (rinfo, ptr, remove_expr e))
          | Reserve (rinfo, e) ->
               make_exp loc (Reserve (rinfo, remove_expr e))
          | TailCall _
          | SpecialCall _
          | SysMigrate _
          | Atomic _
          | AtomicRollback _ ->
               e


   (***  Conditional Rewrites -- Fold Relops into IfThenElse Code  ***)


   (* elim_relop
      Attempt to eliminate relops that can be merged into an existing
      IfThenElse statement.  All too often we see code like:
         let v = (a == b) in
         if v == 0 then
            ...
         else
            ...
      This is inefficient, and while the backend can USUALLY identify
      and simplify these cases, the backend doesn't always catch it.
      These relops are expensive for us, so it's in our interests to
      try to remove these cases now...

      The venv is an environment of all variables which are bound to
      a simple relop expression.  *)
   let rec elim_relop venv e =
      let pos = string_pos "elim_relop" (exp_pos e) in
      let loc = loc_of_exp e in
         match dest_exp_core e with
            IfThenElse (op, a1, a2, e1, e2) ->
               elim_relop_if_then_else venv pos loc op a1 a2 e1 e2
          | LetAtom (v, ac, a, e) ->
               elim_relop_let_atom venv pos loc v ac a e
          | LetAlloc (v, op, e) ->
               make_exp loc (LetAlloc (v, op, elim_relop venv e))
          | LetExternal (v, ac, f, atoms, e) ->
               make_exp loc (LetExternal (v, ac, f, atoms, elim_relop venv e))
          | LetExternalReserve (rinfo, v, ac, f, atoms, e) ->
               make_exp loc (LetExternalReserve (rinfo, v, ac, f, atoms, elim_relop venv e))
          | Match (a, ac, cases) ->
               let process_case (s, e) = (s, elim_relop venv e) in
                  make_exp loc (Match (a, ac, List.map process_case cases))
          | IfType (obj, names, name, v, e1, e2) ->
               (* v is bound here, but not as a relop *)
               make_exp loc (IfType (obj, names, name, v, elim_relop venv e1, elim_relop venv e2))
          | SetMem (ac, ptrty, a1, a2, a3, e) ->
               make_exp loc (SetMem (ac, ptrty, a1, a2, a3, elim_relop venv e))
          | SetGlobal (ac, v, a, e) ->
               make_exp loc (SetGlobal (ac, v, a, elim_relop venv e))
          | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
               make_exp loc (BoundsCheck (label, ptrty, ptr, start, stop, elim_relop venv e))
          | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
               make_exp loc (LowerBoundsCheck (label, ptrty, ptr, start, elim_relop venv e))
          | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
               make_exp loc (UpperBoundsCheck (label, ptrty, ptr, stop, elim_relop venv e))
          | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
               (* v is bound here, but not as a relop *)
               make_exp loc (PointerIndexCheck (label, ptrty, v, ac, index, elim_relop venv e))
          | FunctionIndexCheck (label, funty, v, ac, index, e) ->
               (* v is bound here, but not as a relop *)
               make_exp loc (FunctionIndexCheck (label, funty, v, ac, index, elim_relop venv e))
          | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
               make_exp loc (Memcpy (ptrty, a1, a2, a3, a4, a5, elim_relop venv e))
          | Debug (info, e) ->
               make_exp loc (Debug (info, elim_relop venv e))
          | PrintDebug (v1, v2, a, e) ->
               make_exp loc (PrintDebug (v1, v2, a, elim_relop venv e))
          | CommentFIR (fir, e) ->
               make_exp loc (CommentFIR (fir, elim_relop venv e))
          | AtomicCommit (level, e) ->
               make_exp loc (AtomicCommit (level, elim_relop venv e))
          | CopyOnWrite (rinfo, ptr, e) ->
               make_exp loc (CopyOnWrite (rinfo, ptr, elim_relop venv e))
          | Reserve (rinfo, e) ->
               make_exp loc (Reserve (rinfo, elim_relop venv e))
          | TailCall _
          | SpecialCall _
          | SysMigrate _
          | Atomic _
          | AtomicRollback _ ->
               e


   (* elim_relop_let_atom
      If this is a binding occurence of a variable to a simple relop
      expression, then add it to the venv.  Note that we do not attempt
      to remove the binding occurrence here; if we are successful in
      eliminating v, then mir_dead_code.ml will remove it later. *)
   and elim_relop_let_atom venv pos loc v ac a e =
      let pos = string_pos "elim_relop_let_atom" pos in
      let venv =
         match a with
            AtomBinop (EqOp  ac, a1, a2) ->
               venv_add venv v (REqOp  ac, a1, a2)
          | AtomBinop (NeqOp ac, a1, a2) ->
               venv_add venv v (RNeqOp ac, a1, a2)
          | AtomBinop (LtOp  ac, a1, a2) ->
               venv_add venv v (RLtOp  ac, a1, a2)
          | AtomBinop (LeOp  ac, a1, a2) ->
               venv_add venv v (RLeOp  ac, a1, a2)
          | AtomBinop (GtOp  ac, a1, a2) ->
               venv_add venv v (RGtOp  ac, a1, a2)
          | AtomBinop (GeOp  ac, a1, a2) ->
               venv_add venv v (RGeOp  ac, a1, a2)
          | _ ->
               venv
      in
         make_exp loc (LetAtom (v, ac, a, elim_relop venv e))


   (* elim_relop_if_then_else
      Attempt to merge a relop into this conditional. *)
   and elim_relop_if_then_else venv pos loc op a1 a2 e1 e2 =
      let pos = string_pos "elim_relop_if_then_else" pos in

      (* Process the subexpressions *)
      let e1 = elim_relop venv e1 in
      let e2 = elim_relop venv e2 in

      (* We need to be able to negate relops in a few cases... *)
      let negate_relop = function
         REqOp  ac -> RNeqOp ac
       | RNeqOp ac -> REqOp  ac
       | RLtOp  ac -> RGeOp  ac
       | RLeOp  ac -> RGtOp  ac
       | RGtOp  ac -> RLeOp  ac
       | RGeOp  ac -> RLtOp  ac
       | RAndOp _ ->
            raise (MirException (pos, InternalError "RAndOp not allowed here"))
      in
      let e =
         (* Find out what operation is performed in the IfThenElse *)
         match op, a1, a2 with
            REqOp  (ACRawInt (pre, signed)), AtomRawInt i, AtomVar (_, v)
          | REqOp  (ACRawInt (pre, signed)), AtomVar (_, v), AtomRawInt i
            when Rawint.compare i (Rawint.of_int pre signed 0) = 0 ->
               (* Any prior relop must be FALSE *)
               if venv_mem venv v then
                  (* We can fold a previous relop into this expression! *)
                  let op, a1, a2 = venv_lookup venv pos v in
                  let op = negate_relop op in
                     IfThenElse (op, a1, a2, e1, e2)
               else
                  (* Unknown case *)
                  IfThenElse (op, a1, a2, e1, e2)
          | RNeqOp (ACRawInt (pre, signed)), AtomRawInt i, AtomVar (_, v)
          | RNeqOp (ACRawInt (pre, signed)), AtomVar (_, v), AtomRawInt i
            when Rawint.compare i (Rawint.of_int pre signed 0) = 0 ->
               (* Any prior relop must be TRUE *)
               if venv_mem venv v then
                  (* We can fold a previous relop into this expression! *)
                  let op, a1, a2 = venv_lookup venv pos v in
                     IfThenElse (op, a1, a2, e1, e2)
               else
                  (* Unknown case *)
                  IfThenElse (op, a1, a2, e1, e2)
          | _ ->
               (* Default is just construct the cond the way it was... *)
               IfThenElse (op, a1, a2, e1, e2)
      in
         make_exp loc e


   (***  Interface  ***)


   let rewrite_matches e = rewrite_matches (SymbolTable.empty) e

   let rewrite_matches prog =
      (* Do standard conditional rewrites. *)
      let funs = SymbolTable.mapi (fun name (line, args, e) ->
         line, args, remove_expr (rewrite_matches e)) prog.prog_funs
      in
      let prog = { prog with prog_funs = funs } in

      (* Note: we let valiasing get a chance here, because elim_relop
         will alter the landscape of the IfThenElse expressions, and
         we want to make sure we get to exploit aliases in subexpr's
         FIRST.  If we don't valias, odds are good we'll remove real
         aliases for the relop vars we're trying to eliminate! *)
      let prog = valias_prog prog in
      let funs = SymbolTable.mapi (fun name (line, args, e) ->
         line, args, elim_relop venv_empty e) prog.prog_funs
      in
      let prog = { prog with prog_funs = funs } in
      let prog = valias_prog prog in
      let prog = standardize_prog prog in
         if debug Mir_state.debug_print_mir then
            debug_prog "Cond" prog;
         prog

   let rewrite_matches prog = Fir_state.profile "Mir_cond.rewrite_matches" rewrite_matches prog

   let rewrite_matches prog =
      if optimize_mir_level "opt.mir.cond" 2 then
         rewrite_matches prog
      else
         prog


end (* struct *)

let () = std_flags_register_list_help "opt.mir"
  ["opt.mir.cond",   FlagBool true,
                     "Enable MIR conditional optimizations.  The MIR attempts to revise" ^
                     " certain match constructs to use the simpler IfThenElse construct" ^
                     " which can be handled more efficiently by many backends.  Some branch" ^
                     " prediction may also be performed here."]
