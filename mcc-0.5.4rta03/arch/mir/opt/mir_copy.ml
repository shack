(*
 * Introduce extra variable copying.
 * We copy each and every variable that is ever used.
 * That's a lot of variables, so register allocation will
 * be a lot slower.  However, we should get better code because
 * spill decisions become more localized.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

open Frame_type

open Mir
open Mir_ds
open Mir_exn
open Mir_pos
open Mir_standardize

module Pos = MakePos (struct let name = "Mir_copy" end)
open Pos

(*
 * Variable environment.
 *)
type venv = atom_class SymbolTable.t

let venv_empty = SymbolTable.empty
let venv_add = SymbolTable.add

(*
 * Copy a variable, given an atom class.
 *)
let copy_var_ac loc v ac e =
   make_exp loc (LetAtom (v, ac, AtomVar (ac, v), e))

(*
 * Copy a variable.  The atom class should be in
 * the environment.
 *)
let copy_var venv loc v e =
   try copy_var_ac loc v (SymbolTable.find venv v) e with
      Not_found ->
         e

let copy_vars venv loc vars e =
   List.fold_left (fun e v -> copy_var venv loc v e) e vars

(*
 * Copy all the vars in an atom.
 *)
let rec copy_atom loc a e =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _
    | AtomFunVar _ ->
         e
    | AtomVar (ac, v) ->
         copy_var_ac loc v ac e
    | AtomUnop (_, a) ->
         copy_atom loc a e
    | AtomBinop (_, a1, a2) ->
         copy_atom loc a1 (copy_atom loc a2 e)

let copy_atoms loc args e =
   List.fold_left (fun e a -> copy_atom loc a e) e args

(*
 * atom_class_of_alloc_op
 * Get atom class of an alloc operator.
 *)
let atom_class_of_alloc_op = function
   AllocTuple _
 | AllocArray _
 | AllocMArray _
 | AllocUnion _ ->
      ACPointer PtrBlock
 | AllocMalloc _ ->
      ACPointer PtrAggr

(*
 * Copy all the atoms in the alloc info.
 *)
let copy_alloc_op loc op e =
   match op with
      AllocTuple args
    | AllocArray args
    | AllocUnion (_, args) ->
         copy_atoms loc args e
    | AllocMArray (args, a) ->
         copy_atoms loc args (copy_atom loc a e)
    | AllocMalloc a ->
         copy_atom loc a e

(*
 * Copy the reserve info.
 *)
let copy_reserve_info venv loc (_, regs, bytes, ptrs) e =
   copy_vars venv loc regs (copy_atom loc bytes (copy_atom loc ptrs e))

(*
 * Copy all the variables that are ever used.
 * This introduces a "let v = v in e" construction
 * that should be standardized later.
 *)
let rec copy_exp venv e =
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ac, a, e) ->
            let venv = venv_add venv v ac in
            let e = make_exp loc (LetAtom (v, ac, a, copy_exp venv e)) in
               copy_atom loc a e
       | TailCall (op, f, args) ->
            copy_atoms loc args e
       | IfThenElse (op, a1, a2, e1, e2) ->
            let e = make_exp loc (IfThenElse (op, a1, a2, copy_exp venv e1, copy_exp venv e2)) in
               copy_atom loc a1 (copy_atom loc a2 e)
       | IfType (obj, names, name, v, e1, e2) ->
            let e = make_exp loc (IfType (obj, names, name, v, copy_exp venv e1, copy_exp venv e2)) in
               copy_atom loc obj (copy_atom loc names (copy_atom loc name e))
       | Match (a, ac, cases) ->
            let cases = List.map (fun (s, e) -> s, copy_exp venv e) cases in
            let e = make_exp loc (Match (a, ac, cases)) in
               copy_atom loc a e
       | LetExternal (v, ac, f, args, e) ->
            let venv = venv_add venv v ac in
            let e = make_exp loc (LetExternal (v, ac, f, args, copy_exp venv e)) in
               copy_atoms loc args e
       | LetExternalReserve (rinfo, v, ac, f, args, e) ->
            let venv = venv_add venv v ac in
            let e = make_exp loc (LetExternalReserve (rinfo, v, ac, f, args, copy_exp venv e)) in
            let e = copy_atoms loc args e in
               copy_reserve_info venv loc rinfo e
       | Reserve (rinfo, e) ->
            let e = make_exp loc (Reserve (rinfo, copy_exp venv e)) in
               copy_reserve_info venv loc rinfo e
       | LetAlloc (v, op, e) ->
            let venv' = venv_add venv v (atom_class_of_alloc_op op) in
            let e = make_exp loc (LetAlloc (v, op, copy_exp venv' e)) in
               copy_alloc_op loc op e
       | SetMem (ac, op, ptr, off, a, e) ->
            let e = make_exp loc (SetMem (ac, op, ptr, off, a, copy_exp venv e)) in
               copy_atom loc ptr (copy_atom loc off (copy_atom loc a e))
       | SetGlobal (ac, v, a, e) ->
            let e = make_exp loc (SetGlobal (ac, v, a, copy_exp venv e)) in
               copy_atom loc a e
       | BoundsCheck (label, op, ptr, start, stop, e) ->
            let e = make_exp loc (BoundsCheck (label, op, ptr, start, stop, copy_exp venv e)) in
               copy_atom loc ptr (copy_atom loc start (copy_atom loc stop e))
       | LowerBoundsCheck (label, op, ptr, start, e) ->
            let e = make_exp loc (LowerBoundsCheck (label, op, ptr, start, copy_exp venv e)) in
               copy_atom loc ptr (copy_atom loc start e)
       | UpperBoundsCheck (label, op, ptr, stop, e) ->
            let e = make_exp loc (UpperBoundsCheck (label, op, ptr, stop, copy_exp venv e)) in
               copy_atom loc ptr (copy_atom loc stop e)
       | PointerIndexCheck (label, op, v, ac, index, e) ->
            let e = make_exp loc (PointerIndexCheck (label, op, v, ac, index, copy_exp venv e)) in
               copy_var venv loc v (copy_atom loc index e)
       | FunctionIndexCheck (label, op, v, ac, index, e) ->
            let e = make_exp loc (FunctionIndexCheck (label, op, v, ac, index, copy_exp venv e)) in
               copy_var venv loc v (copy_atom loc index e)
       | Memcpy (op, dptr, doff, sptr, soff, size, e) ->
            let e = make_exp loc (Memcpy (op, dptr, doff, sptr, soff, size, copy_exp venv e)) in
               copy_atom loc dptr (copy_atom loc doff (copy_atom loc sptr (copy_atom loc soff (copy_atom loc size e))))
       | Debug (info, e) ->
            make_exp loc (Debug (info, copy_exp venv e))
       | PrintDebug (v1, v2, args, e) ->
            let e = make_exp loc (PrintDebug (v1, v2, args, copy_exp venv e)) in
               copy_var venv loc v1 (copy_var venv loc v2 (copy_atoms loc args e))
       | CommentFIR (fir, e) ->
            make_exp loc (CommentFIR (fir, copy_exp venv e))
       | Atomic (f, i, a) ->
            copy_var venv loc f (copy_atom loc i (copy_atom loc a e))
       | AtomicRollback (level, i) ->
            copy_atom loc level (copy_atom loc i e)
       | AtomicCommit (level, e) ->
            let e = make_exp loc (AtomicCommit (level, copy_exp venv e)) in
               copy_atom loc level e
       | CopyOnWrite (rinfo, ptr, e) ->
            let e = make_exp loc (CopyOnWrite (rinfo, ptr, copy_exp venv e)) in
               copy_reserve_info venv loc rinfo (copy_atom loc ptr e)
       | SpecialCall _
       | SysMigrate _ ->
            let pos = string_pos "copy_exp" (exp_pos e) in
               raise (MirException (pos, InternalError "unsupported expression"))

(*
 * Introduce copies in the program.
 *)
let copy_prog prog =
   let { prog_funs = funs } = prog in
   let funs =
      SymbolTable.map (fun (info, args, e) ->
            let venv = List.fold_left (fun venv (v, ac) -> venv_add venv v ac) venv_empty args in
               info, args, copy_exp venv e) funs
   in
   let prog = { prog with prog_funs = funs } in
      standardize_prog prog

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
