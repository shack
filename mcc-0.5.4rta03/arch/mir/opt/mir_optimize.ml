(*
   Perform optimizations on the MIR
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Debug
open Flags

open Frame_type
open Fir_state

open Mir
open Mir_print
open Mir_cond
open Mir_const
open Mir_dead
open Mir_inline
open Mir_valias
open Mir_copy


(***  Flag registration  ***)


let () = std_flags_help_section_text "opt.mir"
   "Optimization flags specific to the MIR stage of the compiler."


module type MirOptSig =
sig
   val optimize : prog -> prog
end

module MirOpt (Frame : BackendSig) : MirOptSig =
struct
   module MirCond = MirCond (Frame)

   let const_dead prog =
      let prog = const_prog prog in
      let prog = dead_prog prog in
         prog

   let optimize0 prog =
      let prog = const_dead prog in
      let prog = inline_prog prog in
      let prog = const_dead prog in
      let prog = MirCond.rewrite_matches prog in
      let prog = const_dead prog in
      (* For unknown reasons, we must repeat rewrite_matches to
         get the dual_cond optimization to work properly. Why?? *)
      let prog = MirCond.rewrite_matches prog in
      let prog = const_dead prog in
         prog

   let optimize7 prog =
      copy_prog prog

   (* Debugging output *)
   let optimize prog =
      let level =
         if opt_get_mir () then
            opt_get_level ()
         else
            0
      in
      let prog = optimize0 prog in
      let prog =
         if level >= 7 then
            optimize7 prog
         else
            prog
      in
         if debug Mir_state.debug_print_mir then
            debug_prog "After optimizations" prog;
         prog

   let optimize prog = Fir_state.profile "Mir_optimize.optimize" optimize prog
end
