(*
   Perform variable alias analysis on the MIR code
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Debug
open Symbol
open Flags

open Fir_state

open Mir
open Mir_ds
open Mir_pos
open Mir_exn
open Mir_print
open Frame_type

module Pos = MakePos (struct let name = "Mir_valias" end)
open Pos


(***  Environment Manipulation  ***)


(* Variable environments.  Note for the lookup case, there will always be
   a few variables that aren't bound in the scope of the function.  If we
   cannot find the variable binding, then we simply return the requested
   variable in an atom. *)
let venv_empty = SymbolTable.empty
let venv_add = SymbolTable.add

let rec venv_lookup_var venv v =
   try
      match SymbolTable.find venv v with
         AtomVar (_, v') when not (Symbol.eq v v') ->
            venv_lookup_var venv v'
       | _ ->
            v
   with
      Not_found ->
         v

let rec venv_lookup venv ac v =
   try
      match SymbolTable.find venv v with
         AtomVar (_, v') when not (Symbol.eq v v') ->
            venv_lookup venv ac v'
       | a ->
            a
   with
      Not_found ->
         AtomVar (ac, v)


(***  Atom Manipulation  ***)


(* valias_atom
   valias_atoms
   Rewrite aliased variables in atom expressions. *)
let rec valias_atom venv = function
   AtomVar (ac, v)          -> venv_lookup venv ac v
 | AtomFunVar (ac, v)       -> AtomFunVar (ac, v)
 | AtomUnop (op, a)         -> AtomUnop (op, valias_atom venv a)
 | AtomBinop (op, a1, a2)   -> AtomBinop (op, valias_atom venv a1, valias_atom venv a2)
 | AtomInt _ as a    -> a
 | AtomRawInt _ as a -> a
 | AtomFloat _ as a  -> a

let valias_atoms venv = List.map (valias_atom venv)


(* valias_reserve_info
   Rewrite aliased variables that appear in a reserve info statement. *)
let valias_reserve_info venv rinfo =
   let label, vars, mem, ptr = rinfo in
   let mem = valias_atom venv mem in
   let ptr = valias_atom venv ptr in
   let vars =
      List.fold_left (fun vars v ->
         try
            match SymbolTable.find venv v with
               AtomVar (_, v) ->
                  venv_lookup_var venv v :: vars
             | AtomInt _
             | AtomRawInt _
             | AtomFloat _ ->
                  vars
             | AtomFunVar _
             | AtomUnop _
             | AtomBinop _ ->
                  raise (MirException (var_exp_pos v, StringError "valias_reserve_info: variable was amazingly transformed"))
         with
            Not_found ->
               v :: vars) [] vars
   in
      (label, vars, mem, ptr)


(***  Variable Alias Analysis  ***)


(* valias_expr
   Rewrites aliased variables appearing in an expression.  A variable is an
   alias if it is bound to a simple constant expression (constant, or to
   another variable's value).  Since vars are functional (and not shadowed!)
   these variables can be eliminated and replaced with their binding expr. *)
let rec valias_expr venv e =
   let loc = loc_of_exp e in
   let e = valias_expr_core venv (dest_exp_core e) in
      make_exp loc e

and valias_expr_core venv e =
   match e with
      LetAtom (v, ac, a, e) ->
         valias_let_atom venv v ac a e
    | TailCall (op, f, atoms) ->
         valias_tailcall venv op f atoms
    | Match (a, ac, cases) ->
         valias_match venv a ac cases
    | IfThenElse (op, a1, a2, e1, e2) ->
         valias_if_then_else venv op a1 a2 e1 e2
    | IfType (obj, names, name, v, e1, e2) ->
         valias_if_type venv obj names name v e1 e2
    | LetExternal (v, ac, f, atoms, e) ->
         valias_let_external venv v ac f atoms e
    | LetExternalReserve (rinfo, v, ac, f, atoms, e) ->
         valias_let_external_reserve venv rinfo v ac f atoms e
    | Reserve (rinfo, e) ->
         valias_reserve venv rinfo e
    | LetAlloc (v, op, e) ->
         valias_let_alloc venv v op e
    | SetMem (ac, ptrty, a1, a2, a3, e) ->
         valias_set_mem venv ac ptrty a1 a2 a3 e
    | SetGlobal (ac, v, a, e) ->
         valias_set_global venv ac v a e
    | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
         valias_bounds_check venv label ptrty ptr start stop e
    | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
         valias_lower_bounds_check venv label ptrty ptr start e
    | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
         valias_upper_bounds_check venv label ptrty ptr stop e
    | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
         valias_pointer_index_check venv label ptrty v ac index e
    | FunctionIndexCheck (label, funty, v, ac, index, e) ->
         valias_function_index_check venv label funty v ac index e
    | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
         valias_memcpy venv ptrty a1 a2 a3 a4 a5 e
    | Debug (info, e) ->
         valias_debug venv info e
    | PrintDebug (v1, v2, atoms, e) ->
         valias_print_debug venv v1 v2 atoms e
    | CommentFIR (fir, e) ->
         valias_comment_fir venv fir e
    | SpecialCall op ->
         valias_special_call venv op
    | SysMigrate (label, dptr, doff, f, env) ->
         valias_migrate venv label dptr doff f env
    | Atomic (f, i, e) ->
         valias_atomic venv f i e
    | AtomicRollback (level, i) ->
         valias_atomic_rollback venv level i
    | AtomicCommit (level, e) ->
         valias_atomic_commit venv level e
    | CopyOnWrite (rinfo, ptr, e) ->
         valias_copy_on_write venv rinfo ptr e


and valias_debug venv info e =
   let e = valias_expr venv e in
      Debug (info, e)


and valias_print_debug venv v1 v2 atoms e =
   let e = valias_expr venv e in
   let atoms = valias_atoms venv atoms in
      PrintDebug (v1, v2, atoms, e)


and valias_comment_fir venv fir e =
   let e = valias_expr venv e in
      CommentFIR (fir, e)


(* valias_let_atom
   Check to see if the variable bound here can be rewritten; it can
   if it is bound to a simple atom expression (constant or variable). *)
and valias_let_atom venv v ac a e =
   let a = valias_atom venv a in
      match a with
         AtomVar _
       | AtomInt _
       | AtomRawInt _
       | AtomFloat _ ->
            (* Simple constant expressions can be rewritten *)
            let venv = venv_add venv v a in
            let e = valias_expr venv e in
               LetAtom (v, ac, a, e)
       | AtomFunVar _
       | AtomUnop _
       | AtomBinop _ ->
            (* Computations should not be rewritten *)
            let e = valias_expr venv e in
               LetAtom (v, ac, a, e)


and valias_let_external venv v ac f atoms e =
   let atoms = valias_atoms venv atoms in
   let e = valias_expr venv e in
      LetExternal (v, ac, f, atoms, e)


and valias_let_external_reserve venv rinfo v ac f atoms e =
   let rinfo = valias_reserve_info venv rinfo in
   let atoms = valias_atoms venv atoms in
   let e = valias_expr venv e in
      LetExternalReserve (rinfo, v, ac, f, atoms, e)


and valias_tailcall venv op f atoms =
   let atoms = valias_atoms venv atoms in
      TailCall (op, f, atoms)


and valias_match venv a ac cases =
   let valias_match_case cases (set, e) = (set, valias_expr venv e) :: cases in
   let cases = List.fold_left valias_match_case [] cases in
   let cases = List.rev cases in
   let a = valias_atom venv a in
      Match (a, ac, cases)


(* valias_if_then_else
   For conditionals where we have == or != with a variable reference,
   note that the variable MUST have a particular value within one of
   the subexpressions; therefore we can rewrite it in that subexpr! *)
and valias_if_then_else venv op a1 a2 e1 e2 =
   (* Simplify the atom expressions first *)
   let a1 = valias_atom venv a1 in
   let a2 = valias_atom venv a2 in

   (* Check to see if there's an implicit alias because of the cond *)
      match op, a1, a2 with
         REqOp (ACRawInt _), (AtomRawInt _ as a), AtomVar (_, v)
       | REqOp (ACRawInt _), AtomVar (_, v), (AtomRawInt _ as a)
       | REqOp (ACFloat _), (AtomFloat _ as a), AtomVar (_, v)
       | REqOp (ACFloat _), AtomVar (_, v), (AtomFloat _ as a) ->
         (* Rewrite the alias in the ``then'' expression *)
            let venv' = venv_add venv v a in
            let e1 = valias_expr venv' e1 in
            let e2 = valias_expr venv  e2 in
               IfThenElse (op, a1, a2, e1, e2)
       | RNeqOp (ACRawInt _), (AtomRawInt _ as a), AtomVar (_, v)
       | RNeqOp (ACRawInt _), AtomVar (_, v), (AtomRawInt _ as a)
       | RNeqOp (ACFloat _), (AtomFloat _ as a), AtomVar (_, v)
       | RNeqOp (ACFloat _), AtomVar (_, v), (AtomFloat _ as a) ->
         (* Rewrite the alias in the ``else'' expression *)
            let venv' = venv_add venv v a in
            let e1 = valias_expr venv  e1 in
            let e2 = valias_expr venv' e2 in
               IfThenElse (op, a1, a2, e1, e2)
       | _ ->
         (* All other cases are boring *)
            let e1 = valias_expr venv e1 in
            let e2 = valias_expr venv e2 in
               IfThenElse (op, a1, a2, e1, e2)


(* valias_if_type
   Variable aliasing across an IfType construct *)
and valias_if_type venv obj names name v e1 e2 =
   let obj   = valias_atom venv obj in
   let names = valias_atom venv names in
   let name  = valias_atom venv name in
   let e1 = valias_expr venv e1 in
   let e2 = valias_expr venv e2 in
      IfType (obj, names, name, v, e1, e2)


and valias_reserve venv rinfo e =
   let rinfo = valias_reserve_info venv rinfo in
   let e = valias_expr venv e in
      Reserve (rinfo, e)


and valias_copy_on_write venv rinfo ptr e =
   let ptr = valias_atom venv ptr in
   let rinfo = valias_reserve_info venv rinfo in
   let e = valias_expr venv e in
      CopyOnWrite (rinfo, ptr, e)


and valias_let_alloc venv v op e =
   let op =
      match op with
         AllocMalloc a -> AllocMalloc (valias_atom venv a)
       | AllocUnion (i, atoms) -> AllocUnion (i, valias_atoms venv atoms)
       | AllocTuple atoms -> AllocTuple (valias_atoms venv atoms)
       | AllocArray atoms -> AllocArray (valias_atoms venv atoms)
       | AllocMArray (atoms, a) -> AllocMArray (valias_atoms venv atoms, valias_atom venv a)
   in
   let e = valias_expr venv e in
      LetAlloc (v, op, e)


and valias_set_mem venv ac ptrty a1 a2 a3 e =
   let a1 = valias_atom venv a1 in
   let a2 = valias_atom venv a2 in
   let a3 = valias_atom venv a3 in
   let e  = valias_expr venv e in
      SetMem (ac, ptrty, a1, a2, a3, e)


and valias_set_global venv ac v a e =
   let a = valias_atom venv a in
   let e  = valias_expr venv e in
      SetGlobal (ac, v, a, e)


and valias_bounds_check venv label ptrty ptr start stop e =
   let ptr  = valias_atom venv ptr in
   let start= valias_atom venv start in
   let stop = valias_atom venv stop in
   let e    = valias_expr venv e in
      BoundsCheck (label, ptrty, ptr, start, stop, e)


and valias_lower_bounds_check venv label ptrty ptr start e =
   let ptr  = valias_atom venv ptr in
   let start= valias_atom venv start in
   let e    = valias_expr venv e in
      LowerBoundsCheck (label, ptrty, ptr, start, e)


and valias_upper_bounds_check venv label ptrty ptr stop e =
   let ptr  = valias_atom venv ptr in
   let stop = valias_atom venv stop in
   let e    = valias_expr venv e in
      UpperBoundsCheck (label, ptrty, ptr, stop, e)


and valias_pointer_index_check venv label ptrty v ac index e =
   let index = valias_atom venv index in
   let e     = valias_expr venv e in
      PointerIndexCheck (label, ptrty, v, ac, index, e)


and valias_function_index_check venv label funty v ac index e =
   let index = valias_atom venv index in
   let e     = valias_expr venv e in
      FunctionIndexCheck (label, funty, v, ac, index, e)


and valias_memcpy venv ptrty a1 a2 a3 a4 a5 e =
   let a1 = valias_atom venv a1 in
   let a2 = valias_atom venv a2 in
   let a3 = valias_atom venv a3 in
   let a4 = valias_atom venv a4 in
   let a5 = valias_atom venv a5 in
   let e  = valias_expr venv e in
      Memcpy (ptrty, a1, a2, a3, a4, a5, e)


and valias_special_call venv op =
   let op =
      match op with
         TailSysMigrate (label, loc_base, loc_off, f, args) ->
            let loc_base = valias_atom venv loc_base in
            let loc_off  = valias_atom venv loc_off in
            let args     = valias_atoms venv args in
               TailSysMigrate (label, loc_base, loc_off, f, args)
       | TailAtomic (f, i, args) ->
            let i    = valias_atom venv i in
            let args = valias_atoms venv args in
               TailAtomic (f, i, args)
       | TailAtomicRollback (level, i) ->
            TailAtomicRollback (valias_atom venv level, valias_atom venv i)
       | TailAtomicCommit (level, f, args) ->
            TailAtomicCommit (valias_atom venv level, f, valias_atoms venv args)
   in
      SpecialCall op


and valias_migrate venv label dptr doff f env =
   let dptr = valias_atom venv dptr in
   let doff = valias_atom venv doff in
   let env  = valias_atom venv env in
      SysMigrate (label, dptr, doff, f, env)


and valias_atomic venv f i e =
   let i = valias_atom venv i in
   let e = valias_atom venv e in
      Atomic (f, i, e)


and valias_atomic_rollback venv level i =
   let level = valias_atom venv level in
   let i = valias_atom venv i in
      AtomicRollback (level, i)


and valias_atomic_commit venv level e =
   let level = valias_atom venv level in
   let e = valias_expr venv e in
      AtomicCommit (level, e)


(***  Interface Code  ***)


let valias_fun name (line, args, e) =
   let e = valias_expr venv_empty e in
      (line, args, e)

let valias_prog prog =
  let funs = SymbolTable.mapi valias_fun prog.prog_funs in
    { prog with prog_funs = funs }

let valias_prog prog = Fir_state.profile "Mir_valias.valias_prog" valias_prog prog

let valias_prog prog =
   if optimize_mir_level "opt.mir.valias" 1 then
      valias_prog prog
   else
      prog

let () = std_flags_register_list_help "opt.mir"
  ["opt.mir.valias",    FlagBool true,
                        "Enable variable alias elimination in the MIR."]
