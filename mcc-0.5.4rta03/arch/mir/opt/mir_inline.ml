(*
   Perform simple inlining on the MIR code.
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(*
   NOTE:  This optimization will HURT performance, probably because of
   the larger blocks jumps have to go over (we lose local scope for
   jumps in some spots).  This optimization exists primarily to aid in
   debugging other MIR code; it should not be enabled for production
   code.
 *)


(* Useful modules *)
open Format
open Debug
open Symbol
open Flags

open Mir
open Mir_ds
open Mir_exn
open Mir_pos
open Mir_valias
open Mir_print

open Fir_state

module Pos = MakePos (struct let name = "Mir_inline" end)
open Pos


(***  Configuration  ***)


let flag_enable = "opt.mir.inline"
let flag_debug = "opt.mir.inline.debug"
let flag_size = "opt.mir.inline.size"

let () = std_flags_register_list_help "opt.mir"
  [flag_enable,   FlagBool false,
                  "Enable MIR inlining.  This is not recommended, and should" ^
                  " only be used when debugging certain MIR stages.";
   flag_debug,    FlagBool false,
                  "Enable debugging of the MIR inline module.";
   flag_size,     FlagInt 10,
                  "Set maximum size for inlining of MIR code."]


(***  Variable environment  ***)


let venv_empty = SymbolTable.empty
let venv_add   = SymbolTable.add
let venv_mem   = SymbolTable.mem
let venv_lookup venv pos v =
   try SymbolTable.find venv v with
      Not_found -> raise (MirException (pos, UnboundVar v))


(***  Ranking expressions for inlinability  ***)


let rec rank_function e =
   match dest_exp_core e with
      Debug (_, e)
    | PrintDebug (_, _, _, e)
    | CommentFIR (_, e)
    | Reserve (_, e)
    | CopyOnWrite (_, _, e) ->
         rank_function e
    | LetAtom (_, _, _, e)
    | LetExternal (_, _, _, _, e)
    | LetExternalReserve (_, _, _, _, _, e)
    | LetAlloc (_, _, e)
    | SetMem (_, _, _, _, _, e)
    | SetGlobal (_, _, _, e)
    | BoundsCheck (_, _, _, _, _, e)
    | LowerBoundsCheck (_, _, _, _, e)
    | UpperBoundsCheck (_, _, _, _, e)
    | PointerIndexCheck (_, _, _, _, _, e)
    | FunctionIndexCheck (_, _, _, _, _, e)
    | Memcpy (_, _, _, _, _, _, e)
    | AtomicCommit (_, e) ->
         1 + (rank_function e)
    | TailCall _
    | SpecialCall _
    | SysMigrate _
    | Atomic _
    | AtomicRollback _ ->
         0
    | IfThenElse (_, _, _, e1, e2) ->
         (rank_function e1) + (rank_function e2)
    | IfType (_, _, _, _, e1, e2) ->
         (rank_function e1) + (rank_function e2)
    | Match (_, _, cases) ->
         List.fold_left (fun n (_, e) -> n + (rank_function e)) 0 cases


let candidate_for_inlining funs pos f =
   if venv_mem funs f then
      let rank, _, _ = venv_lookup funs pos f in
         rank <= std_flags_get_int flag_size
   else
      false


(***  Resolving shadowed variable references  ***)


let shadow_var_def venv v =
   let v' = new_symbol v in
   let venv = venv_add venv v v' in
      venv, v'


let shadow_var venv v =
   try SymbolTable.find venv v with
      Not_found -> v


let rec shadow_atom venv a =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _ ->
         a
    | AtomVar (ac, v) ->
         AtomVar (ac, shadow_var venv v)
    | AtomFunVar (ac, f) ->
         AtomFunVar (ac, shadow_var venv f)
    | AtomUnop (op, a) ->
         AtomUnop (op, shadow_atom venv a)
    | AtomBinop (op, a1, a2) ->
         AtomBinop (op, shadow_atom venv a1, shadow_atom venv a2)

let shadow_atoms venv atoms = List.map (shadow_atom venv) atoms


(***  Resolving shadowed variables in expressions  ***)


let shadow_reserve_info venv (label, vars, mem, ptr) =
   let label = new_symbol label in
   let vars = List.map (shadow_var venv) vars in
   let mem = shadow_atom venv mem in
   let ptr = shadow_atom venv ptr in
      (label, vars, mem, ptr)


let shadow_alloc_op venv = function
   AllocMalloc a ->
      AllocMalloc (shadow_atom venv a)
 | AllocTuple atoms ->
      AllocTuple (shadow_atoms venv atoms)
 | AllocArray atoms ->
      AllocArray (shadow_atoms venv atoms)
 | AllocMArray (atoms, a) ->
      AllocMArray (shadow_atoms venv atoms, shadow_atom venv a)
 | AllocUnion (tag, atoms) ->
      AllocUnion (tag, shadow_atoms venv atoms)


let rec shadow_expr venv e =
   let pos = string_pos "shadow_expr" (exp_pos e) in
   let loc = loc_of_exp e in
   let e =
      match dest_exp_core e with
         LetAtom (v, ac, a, e) ->
            let a = shadow_atom venv a in
            let venv, v = shadow_var_def venv v in
               LetAtom (v, ac, a, shadow_expr venv e)
       | TailCall (op, f, args) ->
            let f = shadow_var venv f in
            let args = shadow_atoms venv args in
               TailCall (op, f, args)
       | IfThenElse (op, a1, a2, e1, e2) ->
            let a1 = shadow_atom venv a1 in
            let a2 = shadow_atom venv a2 in
            let e1 = shadow_expr venv e1 in
            let e2 = shadow_expr venv e2 in
               IfThenElse (op, a1, a2, e1, e2)
       | IfType (obj, names, name, v, e1, e2) ->
            let obj = shadow_atom venv obj in
            let names = shadow_atom venv names in
            let name = shadow_atom venv name in
            let venv', v = shadow_var_def venv v in
            let e1 = shadow_expr venv' e1 in
            let e2 = shadow_expr venv  e2 in
               IfType (obj, names, name, v, e1, e2)
       | Match (a, ac, cases) ->
            let process_case (set, e) = (set, shadow_expr venv e) in
            let cases = List.map process_case cases in
            let a = shadow_atom venv a in
               Match (a, ac, cases)
       | LetExternal (v, ac, f, args, e) ->
            let args = shadow_atoms venv args in
            let venv, v = shadow_var_def venv v in
               LetExternal (v, ac, f, args, shadow_expr venv e)
       | LetExternalReserve (rinfo, v, ac, f, args, e) ->
            let rinfo = shadow_reserve_info venv rinfo in
            let args = shadow_atoms venv args in
            let venv, v = shadow_var_def venv v in
               LetExternalReserve (rinfo, v, ac, f, args, shadow_expr venv e)
       | Reserve (rinfo, e) ->
            let rinfo = shadow_reserve_info venv rinfo in
               Reserve (rinfo, shadow_expr venv e)
       | LetAlloc (v, op, e) ->
            let op = shadow_alloc_op venv op in
            let venv, v = shadow_var_def venv v in
               LetAlloc (v, op, shadow_expr venv e)
       | SetMem (ac, op, ptr, off, a, e) ->
            let ptr = shadow_atom venv ptr in
            let off = shadow_atom venv off in
            let a = shadow_atom venv a in
               SetMem (ac, op, ptr, off, a, shadow_expr venv e)
       | SetGlobal (ac, v, a, e) ->
            let a = shadow_atom venv a in
            let venv, v = shadow_var_def venv v in
               SetGlobal (ac, v, a, shadow_expr venv e)
       | BoundsCheck (label, op, ptr, start, stop, e) ->
            let ptr = shadow_atom venv ptr in
            let start = shadow_atom venv start in
            let stop = shadow_atom venv stop in
               BoundsCheck (label, op, ptr, start, stop, shadow_expr venv e)
       | LowerBoundsCheck (label, op, ptr, start, e) ->
            let ptr = shadow_atom venv ptr in
            let start = shadow_atom venv start in
               LowerBoundsCheck (label, op, ptr, start, shadow_expr venv e)
       | UpperBoundsCheck (label, op, ptr, stop, e) ->
            let ptr = shadow_atom venv ptr in
            let stop = shadow_atom venv stop in
               UpperBoundsCheck (label, op, ptr, stop, shadow_expr venv e)
       | PointerIndexCheck (label, op, v, ac, index, e) ->
            let index = shadow_atom venv index in
            let venv, v = shadow_var_def venv v in
               PointerIndexCheck (label, op, v, ac, index, shadow_expr venv e)
       | FunctionIndexCheck (label, op, v, ac, index, e) ->
            let index = shadow_atom venv index in
            let venv, v = shadow_var_def venv v in
               FunctionIndexCheck (label, op, v, ac, index, shadow_expr venv e)
       | Memcpy (op, dptr, doff, sptr, soff, size, e) ->
            let dptr = shadow_atom venv dptr in
            let doff = shadow_atom venv doff in
            let sptr = shadow_atom venv sptr in
            let soff = shadow_atom venv soff in
            let size = shadow_atom venv size in
               Memcpy (op, dptr, doff, sptr, soff, size, shadow_expr venv e)
       | Debug (info, e) ->
            Debug (info, shadow_expr venv e)
       | PrintDebug (v1, v2, args, e) ->
            let v1 = shadow_var venv v1 in
            let v2 = shadow_var venv v2 in
            let args = shadow_atoms venv args in
               PrintDebug (v1, v2, args, shadow_expr venv e)
       | CommentFIR (fir, e) ->
            CommentFIR (fir, shadow_expr venv e)
       | SpecialCall _ ->
            raise (MirException (pos, InternalError "SpecialCall not allowed here"))
       | SysMigrate _ ->
            raise (MirException (pos, InternalError "SysMigrate calls CANNOT be inlined at MIR stage"))
       | Atomic (f, i, e) ->
            let f = shadow_var venv f in
            let i = shadow_atom venv i in
            let e = shadow_atom venv e in
               Atomic (f, i, e)
       | AtomicRollback (level, i) ->
            let level = shadow_atom venv level in
            let i = shadow_atom venv i in
               AtomicRollback (level, i)
       | AtomicCommit (level, e) ->
            let level = shadow_atom venv level in
               AtomicCommit (level, shadow_expr venv e)
       | CopyOnWrite (rinfo, ptr, e) ->
            let rinfo = shadow_reserve_info venv rinfo in
            let ptr = shadow_atom venv ptr in
               CopyOnWrite (rinfo, ptr, e)
   in
      make_exp loc e


let rec bind_args venv pos loc formals args e =
   match formals, args with
      (v, ac) :: formals, a :: args ->
         let venv, v = shadow_var_def venv v in
            make_exp loc (LetAtom (v, ac, a, bind_args venv pos loc formals args e))
    | [], [] ->
         shadow_expr venv e
    | _ ->
         raise (MirException (pos, InternalError "Number of formals did not match number of args"))


let bind_args pos loc formals args e =
   let pos = string_pos "bind_args" pos in
      bind_args venv_empty pos loc formals args e


(***  Inline functions  ***)


let rec inline_expr funs e =
   let pos = string_pos "inline_expr" (exp_pos e) in
   let loc = loc_of_exp e in
      match dest_exp_core e with
         TailCall (op, f, args) ->
            inline_tailcall funs pos loc op f args
       | LetAtom (v, ac, a, e) ->
            make_exp loc (LetAtom (v, ac, a, inline_expr funs e))
       | LetAlloc (v, op, e) ->
            make_exp loc (LetAlloc (v, op, inline_expr funs e))
       | IfThenElse (op, a1, a2, e1, e2) ->
            make_exp loc (IfThenElse (op, a1, a2, inline_expr funs e1, inline_expr funs e2))
       | IfType (obj, names, name, v, e1, e2) ->
            make_exp loc (IfType (obj, names, name, v, inline_expr funs e1, inline_expr funs e2))
       | Match (a, ac, cases) ->
            let process_case (set, e) = set, inline_expr funs e in
            let cases = List.map process_case cases in
               make_exp loc (Match (a, ac, cases))
       | LetExternal (v, ac, f, args, e) ->
            make_exp loc (LetExternal (v, ac, f, args, inline_expr funs e))
       | LetExternalReserve (rinfo, v, ac, f, args, e) ->
            make_exp loc (LetExternalReserve (rinfo, v, ac, f, args, inline_expr funs e))
       | Reserve (rinfo, e) ->
            make_exp loc (Reserve (rinfo, inline_expr funs e))
       | SetMem (ac, op, ptr, off, a, e) ->
            make_exp loc (SetMem (ac, op, ptr, off, a, inline_expr funs e))
       | SetGlobal (ac, v, a, e) ->
            make_exp loc (SetGlobal (ac, v, a, inline_expr funs e))
       | BoundsCheck (label, op, ptr, start, stop, e) ->
            make_exp loc (BoundsCheck (label, op, ptr, start, stop, inline_expr funs e))
       | LowerBoundsCheck (label, op, ptr, start, e) ->
            make_exp loc (LowerBoundsCheck (label, op, ptr, start, inline_expr funs e))
       | UpperBoundsCheck (label, op, ptr, stop, e) ->
            make_exp loc (UpperBoundsCheck (label, op, ptr, stop, inline_expr funs e))
       | PointerIndexCheck (label, op, v, ac, index, e) ->
            make_exp loc (PointerIndexCheck (label, op, v, ac, index, inline_expr funs e))
       | FunctionIndexCheck (label, op, v, ac, index, e) ->
            make_exp loc (FunctionIndexCheck (label, op, v, ac, index, inline_expr funs e))
       | Memcpy (op, dptr, doff, sptr, soff, size, e) ->
            make_exp loc (Memcpy (op, dptr, doff, sptr, soff, size, inline_expr funs e))
       | Debug (info, e) ->
            make_exp loc (Debug (info, inline_expr funs e))
       | PrintDebug (v1, v2, info, e) ->
            make_exp loc (PrintDebug (v1, v2, info, inline_expr funs e))
       | CommentFIR (fir, e) ->
            make_exp loc (CommentFIR (fir, inline_expr funs e))
       | SpecialCall _ ->
            raise (MirException (pos, InternalError "SpecialCall not allowed here"))
       | SysMigrate _
       | Atomic _
       | AtomicRollback _ ->
            e
       | AtomicCommit (level, e) ->
            make_exp loc (AtomicCommit (level, inline_expr funs e))
       | CopyOnWrite (rinfo, ptr, e) ->
            make_exp loc (CopyOnWrite (rinfo, ptr, inline_expr funs e))


and inline_tailcall funs pos loc op f args =
   let pos = string_pos "inline_tailcall" pos in
      match op with
         DirectCall ->
            if candidate_for_inlining funs pos f then
               (* Inline function f *)
               let _, formals, e = venv_lookup funs pos f in
                  bind_args pos loc formals args e
            else
               make_exp loc (TailCall (op, f, args))
       | IndirectCall
       | ExternalCall ->
            make_exp loc (TailCall (op, f, args))


(***  Interface Code  ***)


(* inline_prog
   Function inlining over an entire program. *)
let rec inline_prog prog =
   let funs = prog.prog_funs in
   let funs' = SymbolTable.mapi (fun name (_, args, e) ->
      let rank = rank_function e in
         if std_flags_get_bool flag_debug then
            printf "$$$ Inline rank: %s = %d@." (string_of_symbol name) rank;
         rank, args, e) funs in

   (* Inline functions in function bodies *)
   let inline_fun name (line, args, e) = (line, args, inline_expr funs' e) in
   let funs = SymbolTable.mapi inline_fun funs in

   (* Construct the new program *)
   let prog = { prog with prog_funs = funs } in

   (* Inlining can introduce aliases; let's get rid of them *)
   let prog = valias_prog prog in
      if debug Mir_state.debug_print_mir && std_flags_get_bool flag_debug then
         debug_prog "After Mir_inline.inline_prog" prog;
      prog

let inline_prog prog = Fir_state.profile "Mir_const.inline_prog" inline_prog prog

let inline_prog prog =
   if optimize_mir_level "opt.mir.inline" 9 then
      inline_prog (inline_prog (inline_prog prog))
   else
      prog

