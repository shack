(*
 * This version of dead code eliminates dead arguments.
 * This is a two-stage process: first comput the live vars,
 * then eliminate all vars that are not live.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Symbol
open Flags

open Fir_state

open Mir
open Mir_ds
open Mir_print

(************************************************************************
 * LIVENESS
 ************************************************************************)

(*
 * In the live environment, we collect two kinds of live vars:
 * General live vars, and function vars.
 *)
type live = SymbolSet.t

let live_empty = SymbolSet.empty

(*
 * Equality test.
 *)
let live_equal live1 live2 =
   SymbolSet.equal live1 live2

(*
 * Membership test.
 *)
let live_mem = SymbolSet.mem

(*
 * Add a live var, but don't add it to the escape set.
 *)
let live_var = SymbolSet.add

(*
 * Add the liveness for the atom.
 *)
let rec live_atom live a =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _ ->
         live
    | AtomVar (_, v)
    | AtomFunVar (_, v) ->
         live_var live v
    | AtomUnop (_, a) ->
         live_atom live a
    | AtomBinop (_, a1, a2) ->
         live_atom (live_atom live a1) a2

let live_atoms live args =
   List.fold_left live_atom live args

(*
 * Collect only the arguments that are live.
 *)
let live_args live funs f args =
   try
      let _, vars, _ = SymbolTable.find funs f in
         List.fold_left2 (fun live (v, _) a ->
               if live_mem live v then
                  live_atom live a
               else
                  live) live vars args
   with
      Not_found ->
         live_atoms live args

(*
 * Expression liveness.
 *)
let rec live_exp live funs e =
   match dest_exp_core e with
      LetAtom (v, _, a, e) ->
         live_atom_exp live funs v a e
    | TailCall (_, f, args) ->
         live_tailcall_exp live funs f args
    | IfThenElse (_, a1, a2, e1, e2) ->
         live_if_exp live funs a1 a2 e1 e2
    | Match (a, _, cases) ->
         live_match_exp live funs a cases
    | IfType (a1, a2, a3, _, e1, e2) ->
         live_iftype_exp live funs a1 a2 a3 e1 e2
    | LetExternal (v, _, _, args, e) ->
         live_ext_exp live funs args e
    | LetExternalReserve (info, v, _, _, args, e) ->
         live_ext_reserve_exp live funs info args e
    | Reserve (info, e) ->
         live_reserve_exp live funs info e
    | LetAlloc (v, op, e) ->
         live_alloc_exp live funs v op e
    | SetMem (_, _, a1, a2, a3, e) ->
         live_set_mem_exp live funs a1 a2 a3 e
    | SetGlobal (_, v, a, e) ->
         live_set_global_exp live funs v a e
    | BoundsCheck (_, _, ptr, start, stop, e) ->
         live_bounds_check_exp live funs ptr start stop e
    | LowerBoundsCheck (_, _, ptr, start, e) ->
         live_lower_bounds_check_exp live funs ptr start e
    | UpperBoundsCheck (_, _, ptr, stop, e) ->
         live_upper_bounds_check_exp live funs ptr stop e
    | PointerIndexCheck (_, _, _, _, index, e) ->
         live_pointer_index_check_exp live funs index e
    | FunctionIndexCheck (_, _, _, _, index, e) ->
         live_function_index_check_exp live funs index e
    | Memcpy (_, a1, a2, a3, a4, a5, e) ->
         live_memcpy_exp live funs a1 a2 a3 a4 a5 e
    | Debug (info, e) ->
         live_debug_exp live funs info e
    | PrintDebug (_, _, args, e) ->
         live_atoms_exp live funs args e
    | CommentFIR (_, e) ->
         live_exp live funs e
    | SpecialCall op ->
         live_special_call_exp live funs op
    | SysMigrate (_, a1, a2, f, a3) ->
         live_sys_migrate_exp live funs a1 a2 f a3
    | Atomic (v, a1, a2) ->
         live_atomic_exp live funs v a1 a2
    | AtomicRollback (level, a) ->
         live_atoms live [level; a]
    | AtomicCommit (level, e) ->
         live_exp (live_atom live level) funs e
    | CopyOnWrite (info, a, e) ->
         live_cow_exp live funs info a e

(*
 * Add an atom definition.
 *)
and live_atom_exp live funs v a e =
   let live = live_exp live funs e in
      if live_mem live v then
         live_atom live a
      else
         live

and live_atoms_exp live funs args e =
   let live = live_atoms live args in
      live_exp live funs e

(*
 * Lots of tailcall types.
 *)
and live_tailcall_exp live funs f args =
   let live = live_args live funs f args in
   let live = live_var live f in
      live

and live_special_call_exp live funs op =
   match op with
      TailSysMigrate (_, a1, a2, f, args) ->
         let live = live_atom live a1 in
         let live = live_atom live a2 in
         let live = live_var live f in
         let live = live_args live funs f args in
            live
    | TailAtomic (v, a, args) ->
         let live = live_var live v in
         let live = live_atom live a in
         let live = live_atoms live args in
            live
    | TailAtomicRollback (level, a) ->
         live_atoms live [level; a]
    | TailAtomicCommit (level, f, args) ->
         let live = live_atom live level in
         let live = live_var live f in
         let live = live_args live funs f args in
            live

(*
 * External calls are never dead.
 *)
and live_ext_exp live funs args e =
   let live = live_atoms live args in
      live_exp live funs e

and live_ext_reserve_exp live funs info args e =
   let live = live_reserve live info in
   let live = live_atoms live args in
      live_exp live funs e

(*
 * Conditional.
 *)
and live_if_exp live funs a1 a2 e1 e2 =
   let live = live_atom live a1 in
   let live = live_atom live a2 in
   let live = live_exp live funs e1 in
   let live = live_exp live funs e2 in
      live

(*
 * Match.  All branches are live.
 *)
and live_match_exp live funs a cases =
   let live = live_atom live a in
      List.fold_left (fun live (_, e) ->
            live_exp live funs e) live cases

(*
 * Typecase.
 *)
and live_iftype_exp live funs a1 a2 a3 e1 e2 =
   let live = live_atom live a1 in
   let live = live_atom live a2 in
   let live = live_atom live a3 in
   let live = live_exp live funs e1 in
   let live = live_exp live funs e2 in
      live

(*
 * Reservation.
 * The variables in the reserve are _not_ included as live.
 *)
and live_reserve_exp live funs info e =
   let live = live_reserve live info in
      live_exp live funs e

and live_cow_exp live funs info a e =
   let live = live_reserve live info in
   let live = live_atom live a in
      live_exp live funs e

and live_reserve live (v, _, a1, a2) =
   let live = live_atom live a1 in
   let live = live_atom live a2 in
      live

(*
 * Allocation.
 *)
and live_alloc_exp live funs v op e =
   let live = live_exp live funs e in
      if live_mem live v then
         live_alloc_op live op
      else
         live

and live_alloc_op live op =
   match op with
      AllocTuple args
    | AllocArray args
    | AllocUnion (_, args) ->
         live_atoms live args
    | AllocMArray (args, a) ->
         live_atom (live_atoms live args) a
    | AllocMalloc a ->
         live_atom live a

(*
 * Memory assignments are never dead.
 *)
and live_set_mem_exp live funs a1 a2 a3 e =
   let live = live_atom live a1 in
   let live = live_atom live a2 in
   let live = live_atom live a3 in
      live_exp live funs e

(*
 * Global assignments are also always live.
 *)
and live_set_global_exp live funs v a e =
   let live = live_var live v in
   let live = live_atom live a in
      live_exp live funs e

(*
 * Safety checks.  These can never be considered dead.
 *)
and live_bounds_check_exp live funs ptr start stop e =
   let live = live_atom live ptr in
   let live = live_atom live start in
   let live = live_atom live stop in
      live_exp live funs e

and live_lower_bounds_check_exp live funs ptr start e =
   let live = live_atom live ptr in
   let live = live_atom live start in
      live_exp live funs e

and live_upper_bounds_check_exp live funs ptr stop e =
   let live = live_atom live ptr in
   let live = live_atom live stop in
      live_exp live funs e

and live_pointer_index_check_exp live funs index e =
   let live = live_atom live index in
      live_exp live funs e

and live_function_index_check_exp live funs index e =
   let live = live_atom live index in
      live_exp live funs e

(*
 * Memory copying.
 *)
and live_memcpy_exp live funs a1 a2 a3 a4 a5 e =
   let live = live_atom live a1 in
   let live = live_atom live a2 in
   let live = live_atom live a3 in
   let live = live_atom live a4 in
   let live = live_atom live a5 in
      live_exp live funs e

(*
 * Debugging.
 * The variables in the debugging should remain live.
 *)
and live_debug_exp live funs (_, vars) e =
   let live = live_exp live funs e in
      List.fold_left (fun live (v1, _, v2) ->
            live_var (live_var live v1) v2) live vars

(*
 * Migration.
 *)
and live_sys_migrate_exp live funs a1 a2 f a3 =
   let live = live_atom live a1 in
   let live = live_atom live a2 in
   let live = live_atom live a3 in
   let live = live_var live f in
      (* We must force the function argument to be live *)
      try
         match SymbolTable.find funs f with
            _, [arg, _], _ ->
               let live = live_var live arg in
                  live
          | _ ->
               raise (Failure
                      (sprintf "Mir_dead.live_sys_migrate_exp: malformed migration target %s" (string_of_symbol f)))
      with
         Not_found ->
            raise (Failure
                   (sprintf "Mir_dead.live_sys_migrate_exp: undefined migration target %s" (string_of_symbol f)))

(*
 * Atomic call.
 *)
and live_atomic_exp live funs v a1 a2 =
   let live = live_var live v in
   let live = live_atom live a1 in
   let live = live_atom live a2 in
      live

(*
 * Liveness for a global.
 *)
let live_init live init =
   match init with
      InitAtom a ->
         live_atom live a
    | InitAlloc op ->
         live_alloc_op live op
    | InitRawData _ ->
         live

(*
 * Program liveness requires a fixpoint.
 *)
let live_step live prog =
   let { prog_globals = globals;
         prog_funs = funs
       } = prog
   in

   (* Walk through all the functions *)
   let live =
      SymbolTable.fold (fun live f (_, _, e) ->
            if live_mem live f then
               live_exp live funs e
            else
               live) live funs
   in

   (* Collect live globals *)
   let live =
      SymbolTable.fold (fun live v init ->
            if live_mem live v then
               live_init live init
            else
               live) live globals
   in
      live

let live_prog prog =
   let { prog_export       = export;
         prog_funs         = funs;
         prog_global_order = globals;
         prog_fun_order    = esc;
       } = prog
   in

   (* Add all exported values *)
   let live =
      SymbolTable.fold (fun live v _ ->
            live_var live v) live_empty export
   in

   (* Add all marshaled globals *)
   let live = List.fold_left live_var live globals in

   (* Add all escaping functions, plus their arguments *)
   let live =
      List.fold_left (fun live v ->
            let live = live_var live v in
               try
                  let _, vars, _ = SymbolTable.find funs v in
                     List.fold_left (fun live (v, _) ->
                           live_var live v) live vars
               with
                  Not_found ->
                     live) live esc
   in

   (* Compute the fixpoint of all the function bodies *)
   let rec fixpoint live =
      let live' = live_step live prog in
         if live_equal live live' then
            live
         else
            fixpoint live'
   in
      fixpoint live

(************************************************************************
 * DEADCODE ELIMINATION
 ************************************************************************)

(*
 * Remove dead arguments.
 *)
let dead_args live funs f args =
   try
      let _, vars, _ = SymbolTable.find funs f in
      let args =
         List.fold_left2 (fun args (v, _) a ->
               if live_mem live v then
                  a :: args
               else
                  args) [] vars args
      in
         List.rev args
   with
      Not_found ->
         args

(*
 * Filter dead vars from the reserve.
 *)
let dead_reserve live (v, vars, a1, a2) =
   let vars =
      List.fold_left (fun vars v ->
            if live_mem live v then
               v :: vars
            else
               vars) [] vars
   in
      v, List.rev vars, a1, a2

(*
 * Deadcode elim on an expression.
 *)
let rec dead_exp live funs e =
   let loc = loc_of_exp e in
      match dest_exp_core e with
         LetAtom (v, ac, a, e) ->
            dead_atom_exp loc live funs v ac a e
       | TailCall (op, f, args) ->
            dead_tailcall_exp loc live funs op f args
       | IfThenElse (op, a1, a2, e1, e2) ->
            make_exp loc (IfThenElse (op, a1, a2, dead_exp live funs e1, dead_exp live funs e2))
       | Match (a, ac, cases) ->
            make_exp loc (Match (a, ac, List.map (fun (s, e) -> s, dead_exp live funs e) cases))
       | IfType (a1, a2, a3, v, e1, e2) ->
            make_exp loc (IfType (a1, a2, a2, v, dead_exp live funs e1, dead_exp live funs e2))
       | LetExternal (v, ac, s, args, e) ->
            make_exp loc (LetExternal (v, ac, s, args, dead_exp live funs e))
       | LetExternalReserve (info, v, ac, s, args, e) ->
            make_exp loc (LetExternalReserve (dead_reserve live info, v, ac, s, args, dead_exp live funs e))
       | Reserve (info, e) ->
            make_exp loc (Reserve (dead_reserve live info, dead_exp live funs e))
       | LetAlloc (v, op, e) ->
            dead_alloc_exp loc live funs v op e
       | SetMem (ac, op, a1, a2, a3, e) ->
            make_exp loc (SetMem (ac, op, a1, a2, a3, dead_exp live funs e))
       | SetGlobal (ac, v, a, e) ->
            make_exp loc (SetGlobal (ac, v, a, dead_exp live funs e))
       | BoundsCheck (label, ty, ptr, start, stop, e) ->
            make_exp loc (BoundsCheck (label, ty, ptr, start, stop, dead_exp live funs e))
       | LowerBoundsCheck (label, ty, ptr, start, e) ->
            make_exp loc (LowerBoundsCheck (label, ty, ptr, start, dead_exp live funs e))
       | UpperBoundsCheck (label, ty, ptr, stop, e) ->
            make_exp loc (UpperBoundsCheck (label, ty, ptr, stop, dead_exp live funs e))
       | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
            make_exp loc (PointerIndexCheck (label, ptrty, v, ac, index, dead_exp live funs e))
       | FunctionIndexCheck (label, funty, v, ac, index, e) ->
            make_exp loc (FunctionIndexCheck (label, funty, v, ac, index, dead_exp live funs e))
       | Memcpy (op, a1, a2, a3, a4, a5, e) ->
            make_exp loc (Memcpy (op, a1, a2, a3, a4, a5, dead_exp live funs e))
       | Debug (info, e) ->
            make_exp loc (Debug (info, dead_exp live funs e))
       | PrintDebug (v1, v2, args, e) ->
            make_exp loc (PrintDebug (v1, v2, args, dead_exp live funs e))
       | CommentFIR (fir, e) ->
            make_exp loc (CommentFIR (fir, dead_exp live funs e))
       | SpecialCall _
       | SysMigrate _
       | Atomic _
       | AtomicRollback _ ->
            e
       | AtomicCommit (level, e) ->
            make_exp loc (AtomicCommit (level, dead_exp live funs e))
       | CopyOnWrite (info, a, e) ->
            make_exp loc (CopyOnWrite (dead_reserve live info, a, dead_exp live funs e))

(*
 * Atom is dead if the var is dead.
 *)
and dead_atom_exp loc live funs v ac a e =
   let e = dead_exp live funs e in
      if live_mem live v then
         make_exp loc (LetAtom (v, ac, a, e))
      else
         e

and dead_alloc_exp loc live funs v op e =
   let e = dead_exp live funs e in
      if live_mem live v then
         make_exp loc (LetAlloc (v, op, e))
      else
         e

and dead_tailcall_exp loc live funs op f args =
   let args = dead_args live funs f args in
      make_exp loc (TailCall (op, f, args))

(*
 * Dead program.
 *)
let dead_prog prog =
   let { prog_import = import;
         prog_globals = globals;
         prog_funs = funs
       } = prog
   in

   (* Compute liveness *)
   let live = live_prog prog in

   (* Filter out dead code *)
   let import =
      SymbolTable.fold (fun import v x ->
            if live_mem live v then
               SymbolTable.add import v x
            else
               import) SymbolTable.empty import
   in
   let globals =
      SymbolTable.fold (fun globals v x ->
            if live_mem live v then
               SymbolTable.add globals v x
            else
               globals) SymbolTable.empty globals
   in
   let funs =
      SymbolTable.fold (fun funs' f (info, vars, e) ->
            if live_mem live f then
               let vars =
                  List.fold_left (fun vars v ->
                        let v', _ = v in
                           if live_mem live v' then
                              v :: vars
                           else
                              vars) [] vars
               in
               let e = dead_exp live funs e in
               let fund = info, List.rev vars, e in
                  SymbolTable.add funs' f fund
            else
               funs') SymbolTable.empty funs
   in
   let prog =
      { prog with prog_import = import;
                  prog_globals = globals;
                  prog_funs = funs
      }
   in
      if debug Mir_state.debug_print_mir then
         debug_prog "Deadcode" prog;
      prog

(*
 * Main wrapper.
 *)
let dead_prog prog = Fir_state.profile "Mir_dead_code.dead_prog" dead_prog prog

let dead_prog prog =
   if optimize_mir_level "opt.mir.dce" 1 then
      dead_prog prog
   else
      prog

let () = std_flags_register_list_help "opt.mir"
  ["opt.mir.dce",    FlagBool true,
                     "Enable MIR dead code elimination"]
