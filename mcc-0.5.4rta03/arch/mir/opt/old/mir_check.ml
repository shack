(*
   Fold BoundsCheck statements together.  This uses a conservative
   algorithm for now, and is mainly intended to fold together the
   constant-value bounds checks.
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Format
open Debug
open Symbol
open Frame_type
open Mir
open Mir_exn
open Mir_print
open Fir_state


(*
   Note:  this file was abandoned when the changes to BoundsCheck
   to use start/stop instead of start/length went through.  This
   file will probably not be resurrected, since assert optimizations
   will live in the FIR now.
 *)


(*
   The algorithm here is fairly naive, for now.  We fold together
   two types of BoundsCheck statements -- constant/constant and
   incrementor/constant CheckBounds (explained below):

   constant/constant:  both the offset and size are constants, in
   the CheckBounds statement.  This is common for structures.
   Multiple CheckBounds on the same base pointer can be folded
   together by taking the minimum start offset and the maximum
   end offset, and performing the full check at the location of
   the first CheckBounds for that variable.  This assumes memory
   blocks cannot dynamically resize.  Careful when carrying this
   optimization across conditionals.

   incrementor/constant:  the size is constant, and the offset is
   a variable.  Often, the variable is an ``incrementor''; that
   is, we have taken a base value and incremented it by constant
   values.  In this case, we exploit the fact that variables are
   FUNCTIONAL, and fold together multiple CheckBounds that are
   basically constant offsets from a single base variable.  So,
   if we have something like
      let a = base + 4 in
      checkbounds(p, a, 4) in
      let a' = a + 4 in
      checkbounds(p, a', 4) in
      ...
   We can replace this with a single checkbounds like so:
      checkbounds(p, base + 4, 8)

   This shows up a lot when we're walking through memory and
   doing simple (read: predictable) manipulations on the offset
   value.  Note that even if the variable used is different, we
   will catch it because a simple analysis determines that both
   a and a' are constant offsets from the same variable, base.

   To work, we maintain both the atom values of each variable
   that might be an incrementor (aenv), and an environment of the
   current known lower and upper bounds for a pointer dereference
   (benv).  In the const/const case, we store the smallest start
   offset and the largest end offset into benv.  For inc/const
   cases, for *each* base offset (which is a variable), we store
   the smallest start offset and largest end offset from that
   base variable, for the pointer.

   Currently, we do not attempt to consolidate const/const and
   each type of inc/const -- it hasn't shown up in the emitted
   code yet...

   This optimization requires a decent amount of constant folding
   and valiasing to work to its fullest.
   
   
   Update:  2002.01.17
   We cannot normally lift checks above conditional boundaries,
   however sometimes a check done in a conditional case is already
   computed unconditionally above.  For this reason, we have a cenv;
   an environment of bounds that have been checked at higher level.
   If a set of bounds appear in cenv but not benv, then we CANNOT
   lift a new check up; however, if the bounds in cenv cover the
   current check, then we can safely OMIT it.
   
   
   WARNING:  This assumes no shadowing occurs; i.e. it will fail
 *)


(***  Policies  ***)


(* checks_can_cross_cond
   If true, checks can cross conditional boundaries.  Note that
   this could result in false positives, so until we improve
   this algorithm significantly, you may want to leave this set
   to false. *)
let checks_can_cross_cond = false


(* debug_mir_check
   Set this to true if you want debugging output as this file
   runs.  Default is off. *)
let debug_mir_check = ref false


(***  Environments and Utilities  ***)


(* const_offset
   const/const and inc/const use the same benv, and index on the
   pointer name/offset variable.  const/const doesn't have an
   offset variable by definition, so we fake it by using a new
   symbol name for the const/const case... *)
let const_offset = new_symbol_string "const_offset"


(* Position helper *)
let atom_pos pos = string_pos "Mir_check" (atom_pos pos)


(* DualSymbol, DualSymbolTable
   SymbolTable-like module that is indexed on a pair of symbol names. *)
module DualSymbol =
struct
   type t = symbol * symbol
   let compare (a1, a2) (b1, b2) =
      let c = Symbol.compare a1 b1 in
         if c = 0 then
            Symbol.compare a2 b2
         else
            c
end

module DualSymbolTable = Mc_map.McMake (DualSymbol)


(* aenv
   Stores the atom expressions associated with variables.  If a
   variable is not in the aenv, then it is irreducible (possibly
   the result of a LetExternal, or a function argument).  If it
   is irreducible, then it must be treated as an offset variable. *)
let aenv_empty = SymbolTable.empty
let aenv_add = SymbolTable.add
let aenv_mem = SymbolTable.mem
let aenv_lookup env pos v =
   try SymbolTable.find env v with
      Not_found -> raise (MirException (pos, UnboundVar v))


(* benv
   Stores the current required lower and upper bounds that we
   must check for, for a given base pointer (variable) and a
   given offset variable (or const_offset for the const/const
   case).  The value stored is a pair of Rawints representing
   the lower and upper bound, in that order. *)
let benv_empty = DualSymbolTable.empty
let benv_add = DualSymbolTable.add
let benv_mem = DualSymbolTable.mem
let benv_lookup env pos (v1, v2) =
   try DualSymbolTable.find env (v1, v2) with
      Not_found ->
         let s1 = string_of_symbol v1 in
         let s2 = string_of_symbol v2 in
         let s = "Unbound symbol pair: " ^ s1 ^ "," ^ s2 in
            raise (MirException (pos, StringError s))


(* cenv
   Stores checks that have already been done at a higher level.
   This is very similar to the benv, except UNLIKE benv on a
   conditional boundary, cenv can ALWAYS cross conditional 
   bounds.  The interpretation is that cenv is for checks that
   CANNOT be propagated up, but can still be omitted if the
   bounds in cenv cover the interval being tested. *)
let cenv_empty = benv_empty
let cenv_add = benv_add
let cenv_mem = benv_mem
let cenv_lookup = benv_lookup


(* aenv_is_incremented_var
   For variable v that is used as an offset, this function figures
   out what the base offset variable is, and what the constant offset
   is.  If v isn't bound, or is an unrecognized expression, then we
   will consider v the base offset variable.  *)
let rec aenv_is_incremented_var aenv pos v =
   if aenv_mem aenv v then
      match aenv_lookup aenv pos v with
         AtomBinop (PlusOp (ACRawInt (Rawint.Int32, _)),
                    AtomRawInt i, AtomVar (ACRawInt (Rawint.Int32, _), v))
       | AtomBinop (PlusOp (ACRawInt (Rawint.Int32, _)),
                    AtomVar (ACRawInt (Rawint.Int32, _), v), AtomRawInt i) ->
            (* Incrementing another variable... *)
            let base, off = aenv_is_incremented_var aenv pos v in
               base, Rawint.add off i
       | _ ->
            (* Unknown operator; treat as a base pointer *)
            v, Rawint.of_int Rawint.Int32 false 0
   else
      (* Unknown variable; treat as a base pointer *)
      v, Rawint.of_int Rawint.Int32 false 0

let aenv_is_incremented_var aenv pos v =
   let pos = string_pos "aenv_is_incremented_var" pos in
      aenv_is_incremented_var aenv pos v


(* benv_interval_union
   Takes the union of intervals in two benv's... *)
let benv_interval_union pos benv1 benv2 =
   let pos = string_pos "benv_interval_union" pos in
   let merge_intervals benv tag (lower, upper) =
      let lower, upper =
         if benv_mem benv tag then
            (* Must take union of two benv's... *)
            let lower', upper' = benv_lookup benv pos tag in
               Rawint.min lower lower', Rawint.max upper upper'
         else
            (* This tag only exists in one benv *)
            lower, upper
      in
         benv_add benv tag (lower, upper)
   in
      DualSymbolTable.fold merge_intervals benv1 benv2


(* unsigned_rawint
   init_bounds
   new_bounds
   Offsets are signed, but we never allow negative offsets.  Therefore,
   we treat values as unsigned for max/min calculations.  If a negative
   offset is given, it is seen as a large unsigned value which will
   certainly exceed the blocksize; we just need to make sure this value
   is included in the upper bound to fail the test.  *)
let unsigned_rawint i =
   Rawint.of_rawint Rawint.Int32 false i

let init_bounds off size =
   let lower = unsigned_rawint off in
   let upper = Rawint.add lower (unsigned_rawint size) in
      if Rawint.compare lower upper > 0 then
         upper, lower
      else
         lower, upper

let new_bounds lower upper off size =
   let lower', upper' = init_bounds off size in
   let lower = Rawint.min lower lower' in
   let upper = Rawint.max upper upper' in
      lower, upper


(***  Check bounds in expression  ***)


(* check_expr
   Simplifies the checks in an expression.  *)
let rec check_expr aenv benv cenv e =
   let pos = string_pos "check_expr" (atom_pos e) in
      match e with
         LetAtom (v, ac, a, e) ->
            let aenv = aenv_add aenv v a in
            let benv, e = check_expr aenv benv cenv e in
               benv, LetAtom (v, ac, a, e)
       | SetGlobal (ac, v, a, e) ->
            let aenv = aenv_add aenv v a in
            let benv, e = check_expr aenv benv cenv e in
               benv, SetGlobal (ac, v, a, e)
       | TailCall _
       | SpecialCall _
       | Atomic _
       | AtomicRollback _ ->
            benv, e
       | Match (a, ac, cases) ->
            (* CAUTION: See the warning below in IfThenElse. *)
            if checks_can_cross_cond then
               let process_case (benv1, cases) (s, e) =
                  let benv2, e = check_expr aenv benv cenv e in
                  let benv1 = benv_interval_union pos benv1 benv2 in
                  let cases = (s, e) :: cases in
                     benv1, cases
               in
               let benv, cases = List.fold_left process_case (benv_empty, []) cases in
               let cases = List.rev cases in
                  benv, Match (a, ac, cases)
            else
               let process_case (s, e) = s, check_top_expr aenv cenv e in
               let cases = List.map process_case cases in
                  benv, Match (a, ac, cases)
       | IfThenElse (op, a1, a2, e1, e2) ->
            (* CAUTION: we cannot feed benv1 into the check_expr
               for benv2, because tags bound inside e1 should NOT
               apply for e2.  Therefore we have to use the union
               code above, and pass both check_expr's the original
               benv. *)
            if checks_can_cross_cond then
               let benv1, e1 = check_expr aenv benv cenv e1 in
               let benv2, e2 = check_expr aenv benv cenv e2 in
               let benv = benv_interval_union pos benv1 benv2 in
                  benv, IfThenElse (op, a1, a2, e1, e2)
            else
               let e1 = check_top_expr aenv cenv e1 in
               let e2 = check_top_expr aenv cenv e2 in
                  benv, IfThenElse (op, a1, a2, e1, e2)
       | IfType (obj, names, name, v, e1, e2) ->
            (* CAUTION: see the warning above in IfThenElse. *)
            (* Note: v is bound here, but v cannot be an incrementor
               variable; therefore we do not need to keep track of it
               in the aenv. *)
            if checks_can_cross_cond then
               let benv1, e1 = check_expr aenv benv cenv e1 in
               let benv2, e2 = check_expr aenv benv cenv e2 in
               let benv = benv_interval_union pos benv1 benv2 in
                  benv, IfType (obj, names, name, v, e1, e2)
            else
               let e1 = check_top_expr aenv cenv e1 in
               let e2 = check_top_expr aenv cenv e2 in
                  benv, IfType (obj, names, name, v, e1, e2)
       | LetExternal (v, ac, s, atoms, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, LetExternal (v, ac, s, atoms, e)
       | Reserve (rinfo, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, Reserve (rinfo, e)
       | LetAlloc (v, op, e) ->
            check_alloc_expr aenv benv cenv pos v op e
       | SetMem (ac, ptrty, a1, a2, a3, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, SetMem (ac, ptrty, a1, a2, a3, e)
       | CheckBounds (label, ptrty, ptr, ofs, size, e) ->
            check_bounds_expr aenv benv cenv pos label ptrty ptr ofs size e
       | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, Memcpy (ptrty, a1, a2, a3, a4, a5, e)
       | Debug (info, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, Debug (info, e)
       | PrintDebug (v1, v2, atoms, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, PrintDebug (v1, v2, atoms, e)
       | CommentFIR (fir, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, CommentFIR (fir, e)
       | SysMigrate (label, d, s, dptr, doff, e) ->
            let e = check_top_expr aenv cenv e in
               benv, SysMigrate (label, d, s, dptr, doff, e)
       | AtomicCommit e ->
            let benv, e = check_expr aenv benv cenv e in
               benv, AtomicCommit e
       | CopyOnWrite (rinfo, copy_ptr, e) ->
            let benv, e = check_expr aenv benv cenv e in
               benv, CopyOnWrite (rinfo, copy_ptr, e)


(* check_alloc_expr
   An optimization can be made here; if we allocate a pointer v, then
   we *know* v has the size declared in the alloc operator, so we do
   not need to bounds check it within the specified size.  We optimize
   constant bounds checks away for v by adding it to the cenv (which
   indicates it was already ``checked'', which in a sense is true :) *)
and check_alloc_expr aenv benv cenv pos v op e =
   let pos = string_pos "check_alloc_expr" pos in
      match op with
         AllocMalloc (AtomRawInt size) ->
            (* We can optimize this case! *)
            let tag = v, const_offset in
            let zero = Rawint.of_int Rawint.Int32 false 0 in
            let lower, upper = init_bounds zero size in
            let _ = 
               if !debug_mir_check then
                  printf "$$$ Malloc of %s, interval is [%s, %s]\n%t" 
                     (string_of_symbol v) (Rawint.to_string lower) (Rawint.to_string upper) flush
            in
            let cenv = cenv_add cenv tag (lower, upper) in
            let benv, e = check_expr aenv benv cenv e in
               benv, LetAlloc (v, op, e)
       | _ ->
            (* Default case is to still check... *)
            let benv, e = check_expr aenv benv cenv e in
               benv, LetAlloc (v, op, e)


(* check_bounds_expr
   See if this is a const/const or inc/const CheckBounds; if so, try
   to fold it together with other const/const or inc/const expressions.
   If ptr isn't currently bound in benv, then this is the first time
   we've checked the pointer in this scope, so the check *must* be
   preserved. *)
and check_bounds_expr aenv benv cenv pos label ptrty ptr off size e =
   let pos = string_pos "check_bounds_expr" pos in

   (* The check_bounds function actually rewrites the check expression
      once we have figured out what the appropriate tag is.  off_adder
      function takes a constant offset and constructs the real offset
      for the final check expression (by adding a base variable, if one
      is required).  Note that size is always a constant... *)
   let check_bounds tag off_adder off size =
      if benv_mem benv tag then
         (* We've seen this pointer with bounds already, and we can
            propagate this check up to the original check. *)
         let lower, upper = benv_lookup benv pos tag in
         let lower, upper = new_bounds lower upper off size in
         let _ =
            if !debug_mir_check then
               printf "$$$    DROP: Subcheck of a higher expression: bounds [%s, %s]\n%t"
                  (Rawint.to_string lower) (Rawint.to_string upper) flush
         in
         let benv = benv_add benv tag (lower, upper) in

         (* The cenv entry may have more generous bounds, so update
            it separately... *)
         let lower, upper = cenv_lookup benv pos tag in
         let lower, upper = new_bounds lower upper off size in
         let cenv = cenv_add cenv tag (lower, upper) in
            (* This check is omitted *)
            check_expr aenv benv cenv e
      else 
         (* We cannot propagate this check up; however, we may
            be able to omit this test entirely.  Check the cenv
            next... *)
         let first_time best_lower best_upper =
            (* If this is called, then it is the first time we have 
               seen this pointer with constant offset, and any checks
               existing in the cenv were insufficient for this check. *)

            (* Update the cenv based on most aggressive known limits *)
            let cenv = cenv_add cenv tag (best_lower, best_upper) in
               
            (* Update the benv based on INITIAL parameters *)
            let lower, upper = init_bounds off size in
            let benv = benv_add benv tag (lower, upper) in
            let benv, e = check_expr aenv benv cenv e in

            (* Construct the revised bounds for this expr *)
            let lower, upper = benv_lookup benv pos tag in
            let off  = off_adder lower in
            let size = AtomRawInt (Rawint.sub upper lower) in
               benv, CheckBounds (label, ptrty, ptr, off, size, e)
         in (* end first_time *)

            (* Check if we have an entry in the cenv at least... *)
            if cenv_mem cenv tag then begin
               let lower', upper' = cenv_lookup cenv pos tag in
               let lower, upper = new_bounds lower' upper' off size in
                  if !debug_mir_check then 
                     printf "$$$    Tag has been checked, required interval is [%s, %s], checked interval is [%s, %s]\n%t"
                        (Rawint.to_string lower') (Rawint.to_string upper')
                        (Rawint.to_string lower)  (Rawint.to_string upper) flush;
                  if lower' = lower && upper' = upper then begin
                     (* Current interval is within the cenv interval; 
                        we can omit this check entirely.  No updates 
                        to the environments are necessary... *)
                     if !debug_mir_check then
                        printf "$$$    DROP: Prior check was sufficient.\n%t" flush;
                     check_expr aenv benv cenv e
                  end else begin
                     (* Current interval is outside the cenv interval;
                        we must proceed with the check.  Don't forget
                        to update cenv... *)
                     if !debug_mir_check then
                        printf "$$$    CHECK: Prior check was not sufficient.\n%t" flush;
                     first_time lower upper
                  end (* Does cenv cover our check? *)
            end else begin
               (* No entry even in the cenv; this is a first! *)
               let lower, upper = init_bounds off size in
                  if !debug_mir_check then
                     printf "$$$    CHECK: Tag has never been checked, this is the first time.  Interval is [%s, %s]\n%t"
                        (Rawint.to_string lower)  (Rawint.to_string upper) flush;
                  first_time lower upper
            end (* Checked in cenv?  Ever? *)
   in (* end check_bounds *)

   let check_bounds tag off_adder off size =
      if !debug_mir_check then begin
         let ptr, base = tag in
         let label = string_of_symbol label in
         let tag = (string_of_symbol ptr) ^ "." ^ (string_of_symbol base) in
            printf "$$$ Checking bounds %s (tag %s), off %s, size %s\n%t"
               label tag (Rawint.to_string off) (Rawint.to_string size) flush
      end; (* Debugging code *)
      check_bounds tag off_adder off size
   in
      match ptr, off, size with
         AtomVar (_, v), AtomRawInt off, AtomRawInt size ->
            (* Constant bounds check *)
            let tag = v, const_offset in
            let off_adder lower = AtomRawInt lower in
               check_bounds tag off_adder off size
       | AtomVar (_, v), AtomVar (_, off), AtomRawInt size ->
            (* Offset may be incremented off a known base *)
            let off_base, off_incr = aenv_is_incremented_var aenv pos off in
            let tag = v, off_base in
            let plus_op = PlusOp (ACRawInt (Rawint.Int32, false)) in
            let off_base = AtomVar (ACRawInt (Rawint.Int32, false), off_base) in
            let off_adder lower = AtomBinop (plus_op, off_base, AtomRawInt lower) in
               check_bounds tag off_adder off_incr size
       | _ ->
            (* Unrecognized form, sorry. *)
            let benv, e = check_expr aenv benv cenv e in
               benv, CheckBounds (label, ptrty, ptr, off, size, e)


(* check_top_expr
   Checks the toplevel expression.  Assume all checks must be
   repeated from this point, so benv is cleared.  *)
and check_top_expr aenv cenv e =
   let _, e = check_expr aenv benv_empty cenv e in
      e


(***  Interface Code  ***)


(* check_prog
   Simplifies checks throughout the program.  *)
let rec check_prog prog =
   let funs = prog.prog_funs in

   (* Fold constants in function bodies *)
   let check_fun name (line, args, e) = (line, args, check_top_expr aenv_empty cenv_empty e) in
   let funs = SymbolTable.mapi check_fun funs in

   (* Construct the new program *)
   let prog = { prog with prog_funs = funs } in
      if debug Fir_state.debug_print_mir && !debug_mir_check then
         debug_prog "After Mir_check.check_prog" prog;
      prog

let check_prog prog = Fir_state.profile "Mir_check.check_prog" check_prog prog

let check_prog prog =
   if optimize_mir_level "opt.mir.check" 1 then
      check_prog prog
   else
      prog

