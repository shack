(*
   Perform simple memory alias analysis on the MIR code.
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(*
   NOTE:  This file was intended as a proof-of-concept, not a permanent
   optimization.  Memory optimizations now exist in the FIR, so this
   file is abandoned.
 *)


(* Useful modules *)
open Debug
open Symbol
open Frame_type
open Mir
open Mir_exn
open Mir_valias
open Mir_print
open Fir_state


let debug_mir_memory_opt = ref false


module type MirMemoryOptSig =
sig
   val mir_memory_prog : prog -> prog
end


module MirMemoryOpt (Frame : BackendSig) : MirMemoryOptSig =
struct
   module MirUtil = Mir_util.MirUtil (Frame)
   open MirUtil


   (***  Exceptions  ***)


   (* Position helper *)
   let atom_pos pos = string_pos "Mir_memory" (atom_pos pos)


   (***  Environments for let memory analysis  ***)


   type letmem =
      { lm_pointer : atom;
        lm_offset  : atom;
        lm_ac      : atom_class;
        lm_stored  : symbol;
      }


   let letmem_empty = []


   let letmem_new_read letmem v ac ptr off =
      let lm_ent = { lm_pointer = ptr;
                     lm_offset  = off;
                     lm_ac      = ac;
                     lm_stored  = v;
                   }
      in
         lm_ent :: letmem


   let letmem_lookup letmem ptr off ac =
      let rec find = function
         lm_ent :: letmem ->
            if lm_ent.lm_pointer = ptr && lm_ent.lm_offset = off && lm_ent.lm_ac = ac then
               Some (AtomVar (ac, lm_ent.lm_stored))
            else
               find letmem
       | [] ->
            None
      in
         find letmem


   (***  Environments for set memory analysis  ***)


   type setmem =
      { sm_current_pointer : symbol;
        sm_valid_intervals : (int32 * int32 * atom) list;
      }


   let setmem_empty =
      { sm_current_pointer = new_symbol_string "no_pointer";
        sm_valid_intervals = [];
      }


   let setmem_new_pointer ptr =
      { setmem_empty with sm_current_pointer = ptr }


   let setmem_lookup_value setmem ptr off ac =
      (* Search for an interval matching (off, size). *)
      let off = Rawint.to_int32 off in
      let size = sizeof_atom_class ac in
      let rec scan = function
         (off', size', a) :: intervals ->
            if off = off' && size = size' then
               Some a
            else
               scan intervals
       | [] ->
            None
      in
         (* Make sure we are operating on valid base pointer *)
         if Symbol.eq ptr setmem.sm_current_pointer then
            scan setmem.sm_valid_intervals
         else
            None


   let setmem_new_interval setmem off ac a =
      let off = Rawint.to_int32 off in
      let size = sizeof_atom_class ac in
      let rec invalidate = function
         ((off', size', _) as interval) :: intervals ->
            if (off' >= off && off' < Int32.add off size)
             || (off >= off' && off < Int32.add off' size') then
               (* Intervals collide; the old interval must be removed *)
               invalidate intervals
            else
               interval :: invalidate intervals
       | [] ->
            []
      in
      let intervals = invalidate setmem.sm_valid_intervals in
      let intervals = (off, size, a) :: intervals in
         { setmem with sm_valid_intervals = intervals }


   let setmem_new_assignment setmem ptr off ac a =
      match ptr, off with
         AtomVar (_, ptr), AtomRawInt off ->
            if Symbol.eq ptr setmem.sm_current_pointer then
               setmem_new_interval setmem off ac a
            else
               setmem_new_interval (setmem_new_pointer ptr) off ac a
       | _ ->
            setmem_empty


   (***  General environments  ***)


   type mem =
      { mem_letmem : letmem list;
        mem_setmem : setmem;
      }


   let mem_empty =
      { mem_letmem = letmem_empty;
        mem_setmem = setmem_empty;
      }


   (***  Optimizing memory references in atoms  ***)


   let rec mir_memory_atom mem a =
      match a with
         AtomInt _
       | AtomRawInt _
       | AtomFloat _
       | AtomVar _
       | AtomFunVar _ ->
            a
       | AtomUnop (op, a) ->
            AtomUnop (op, mir_memory_atom mem a)
       | AtomBinop (MemOp (ac, PtrAggr), (AtomVar (_, ptr) as aptr), (AtomRawInt off as aoff)) ->
            begin
            match setmem_lookup_value mem.mem_setmem ptr off ac with
               Some a ->
                  a
             | None ->
                  match letmem_lookup mem.mem_letmem aptr aoff ac with
                     Some a ->
                        a
                   | None ->
                        a
            end
       | AtomBinop (MemOp (ac, PtrAggr), aptr, aoff) ->
            begin
            match letmem_lookup mem.mem_letmem aptr aoff ac with
               Some a ->
                  a
             | None ->
                  a
            end
       | AtomBinop (op, a1, a2) ->
            AtomBinop (op, mir_memory_atom mem a1, mir_memory_atom mem a2)


   let mir_memory_atoms mem atoms = List.map (mir_memory_atom mem) atoms


   let mir_memory_alloc_op mem = function
      AllocMalloc a ->
         AllocMalloc (mir_memory_atom mem a)
    | AllocTuple atoms ->
         AllocTuple (mir_memory_atoms mem atoms)
    | AllocArray atoms ->
         AllocArray (mir_memory_atoms mem atoms)
    | AllocMArray (atoms, a) ->
         AllocMArray (mir_memory_atoms mem atoms, mir_memory_atom mem a)
    | AllocUnion (tag, atoms) ->
         AllocUnion (tag, mir_memory_atoms mem atoms)


   let mir_memory_reserve_info mem (label, vars, nptr, nmem) =
      let nptr = mir_memory_atom mem nptr in
      let nmem = mir_memory_atom mem nmem in
         (label, vars, nptr, nmem)


   (***  Optimizing memory references in expressions  ***)


   let rec mir_memory_expr mem e =
      let pos = string_pos "mir_memory_expr" (atom_pos e) in
         match e with
            LetAtom (v, ac, a, e) ->
               let a = mir_memory_atom mem a in
               let mem =
                  match a with
                     AtomBinop (MemOp (_, PtrAggr), ptr, off) ->
                        let letmem = letmem_new_read mem.mem_letmem v ac ptr off in
                           { mem with mem_letmem = letmem }
                   | _ ->
                        mem
               in
                  LetAtom (v, ac, a, mir_memory_expr mem e)
          | SetMem (ac, op, ptr, off, a, e) ->
               (* Note:  what if op not aggr?? *)
               let ptr = mir_memory_atom mem ptr in
               let off = mir_memory_atom mem off in
               let a = mir_memory_atom mem a in
               let setmem = setmem_new_assignment mem.mem_setmem ptr off ac a in
               let mem = { mem_empty with mem_setmem = setmem } in
                  SetMem (ac, op, ptr, off, a, mir_memory_expr mem e)
          | LetExternal (v, ac, f, args, e) ->
               let args = mir_memory_atoms mem args in
                  LetExternal (v, ac, f, args, mir_memory_expr mem_empty e)
          | TailCall (op, f, args) ->
               let args = mir_memory_atoms mem args in
                  TailCall (op, f, args)
          | IfThenElse (op, a1, a2, e1, e2) ->
               let a1 = mir_memory_atom mem a1 in
               let a2 = mir_memory_atom mem a2 in
               let e1 = mir_memory_expr mem e1 in
               let e2 = mir_memory_expr mem e2 in
                  IfThenElse (op, a1, a2, e1, e2)
          | IfType (obj, names, name, v, e1, e2) ->
               let obj = mir_memory_atom mem obj in
               let names = mir_memory_atom mem names in
               let name = mir_memory_atom mem name in
               let e1 = mir_memory_expr mem e1 in
               let e2 = mir_memory_expr mem e2 in
                  IfType (obj, names, name, v, e1, e2)
          | Match (a, ac, cases) ->
               let a = mir_memory_atom mem a in
               let process_case (set, e) = set, mir_memory_expr mem e in
               let cases = List.map process_case cases in
                  Match (a, ac, cases)
          | Reserve (rinfo, e) ->
               let rinfo = mir_memory_reserve_info mem rinfo in
                  Reserve (rinfo, mir_memory_expr mem e)
          | LetAlloc (v, op, e) ->
               let op = mir_memory_alloc_op mem op in
                  LetAlloc (v, op, mir_memory_expr mem e)
          | SetGlobal (ac, v, a, e) ->
               let a = mir_memory_atom mem a in
                  LetAtom (v, ac, a, mir_memory_expr mem e)
          | BoundsCheck (label, op, ptr, start, stop, e) ->
               let ptr = mir_memory_atom mem ptr in
               let start = mir_memory_atom mem start in
               let stop = mir_memory_atom mem stop in
                  BoundsCheck (label, op, ptr, start, stop, mir_memory_expr mem e)
          | LowerBoundsCheck (label, op, ptr, start, e) ->
               let ptr = mir_memory_atom mem ptr in
               let start = mir_memory_atom mem start in
                  LowerBoundsCheck (label, op, ptr, start, mir_memory_expr mem e)
          | UpperBoundsCheck (label, op, ptr, stop, e) ->
               let ptr = mir_memory_atom mem ptr in
               let stop = mir_memory_atom mem stop in
                  UpperBoundsCheck (label, op, ptr, stop, mir_memory_expr mem e)
          | PointerIndexCheck (label, op, v, ac, index, e) ->
               let index = mir_memory_atom mem index in
                  PointerIndexCheck (label, op, v, ac, index, mir_memory_expr mem e)
          | FunctionIndexCheck (label, v, ac, index, e) ->
               let index = mir_memory_atom mem index in
                  FunctionIndexCheck (label, v, ac, index, mir_memory_expr mem e)
          | Memcpy (op, dptr, doff, sptr, soff, size, e) ->
               let dptr = mir_memory_atom mem dptr in
               let doff = mir_memory_atom mem doff in
               let sptr = mir_memory_atom mem sptr in
               let soff = mir_memory_atom mem soff in
               let size = mir_memory_atom mem size in
                  Memcpy (op, dptr, doff, sptr, soff, size, mir_memory_expr mem e)
          | Debug (info, e) ->
               Debug (info, mir_memory_expr mem e)
          | PrintDebug (v1, v2, info, e) ->
               PrintDebug (v1, v2, info, mir_memory_expr mem e)
          | CommentFIR (fir, e) ->
               CommentFIR (fir, mir_memory_expr mem e)
          | SpecialCall _ ->
               raise (MirException (pos, InternalError "SpecialCall not allowed here"))
          | SysMigrate (label, d, s, dptr, doff, e) ->
               let dptr = mir_memory_atom mem dptr in
               let doff = mir_memory_atom mem doff in
                  SysMigrate (label, d, s, dptr, doff, mir_memory_expr mem_empty e)
          | Atomic (f, e, i) ->
               let e = mir_memory_atom mem e in
               let i = mir_memory_atom mem i in
                  Atomic (f, e, i)
          | AtomicRollback i ->
               let i = mir_memory_atom mem i in
                  AtomicRollback i
          | AtomicCommit e ->
               AtomicCommit (mir_memory_expr mem e)
          | CopyOnWrite (rinfo, ptr, e) ->
               let rinfo = mir_memory_reserve_info mem rinfo in
               let ptr = mir_memory_atom mem ptr in
                  CopyOnWrite (rinfo, ptr, e)


   (***  Interface Code  ***)


   (* mir_memory_prog
      Function inlining over an entire program. *)
   let rec mir_memory_prog prog =
      let funs = prog.prog_funs in

      (* Memory simplification in function bodies *)
      let mir_memory_fun name (line, args, e) = (line, args, mir_memory_expr mem_empty e) in
      let funs = SymbolTable.mapi mir_memory_fun funs in

      (* Construct the new program *)
      let prog = { prog with prog_funs = funs } in

      (* We probably introduced aliases *)
      let prog = valias_prog prog in
         if debug Mir_state.debug_print_mir && !debug_mir_memory_opt then
            debug_prog "After Mir_memory_opt.mir_memory_prog" prog;
         prog

   let mir_memory_prog prog = Fir_state.profile "Mir_const.mir_memory_prog" mir_memory_prog prog

   let mir_memory_prog prog =
      if optimize_mir_level "opt.mir.memory" 9 then
         mir_memory_prog prog
      else
         prog


end (* struct *)

let () = optimize_new_names 
   ["opt.mir.memory",   FlagBool true]
