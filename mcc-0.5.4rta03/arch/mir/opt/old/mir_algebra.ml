(*
 * Simple loop optimizations.
 * We classify each variable as loop-invariant, as
 * an induction variable, or arbitrary.  Loop invariants
 * and induction variables are passed around the loop as
 * extra arguments.  We definitely need dead-code elim
 * after this stage.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *
 * Based on the Fir_algebra by Geoffrey Irving.
 *)

open Symbol

open Frame_type

open Mir
open Mir_exn
open Mir_print

(************************************** miscellanous useful stuff *)

(*
 * Zero value.
 *)
let zero_of_ac pos ac =
   match ac with
      ACInt
    | ACPoly ->
         AtomInt 0
    | ACRawInt (p, s) ->
         AtomRawInt (Rawint.of_int p s 0)
    | _ ->
         raise (MirException (pos, StringError "illegal zero"))

let atom_false = AtomRawInt (Rawint.of_int Rawint.Int32 false 0)
let atom_true = AtomRawInt (Rawint.of_int Rawint.Int32 false 1)

let test b =
   if b then
      atom_true
   else
      atom_false

(************************************** etree tables *)

module AtomTable = Mc_map.McMake (struct
  type t = atom
  let compare = Pervasives.compare
end)

(************************************** raw computation *)

let compute_unop pos op c =
   match op, c with
      UMinusOp _,                     AtomInt i       -> AtomInt    (-i)
    | UMinusOp _,                     AtomRawInt i    -> AtomRawInt (Rawint.neg i)
    | UMinusOp _,                     AtomFloat x     -> AtomFloat  (Rawfloat.neg x)
    | AbsOp _,                        AtomInt i       -> AtomInt    (abs i)
    | AbsOp _,                        AtomRawInt i    -> AtomRawInt (Rawint.abs i)
    | AbsOp _,                        AtomFloat x     -> AtomFloat  (Rawfloat.abs x)
    | NotOp _,                        AtomInt i       -> AtomInt    (lnot i)
    | NotOp _,                        AtomRawInt i    -> AtomRawInt (Rawint.lognot i)
    | SinOp _,                        AtomFloat x     -> AtomFloat  (Rawfloat.sin x)
    | CosOp _,                        AtomFloat x     -> AtomFloat  (Rawfloat.cos x)
    | SqrtOp _,                       AtomFloat x     -> AtomFloat  (Rawfloat.sqrt x)
    | IntOfFloatOp _,                 AtomFloat x     -> AtomInt    (Rawfloat.to_int x)
    | FloatOfIntOp p,                 AtomInt i       -> AtomFloat  (Rawfloat.of_int p i)
    | FloatOfFloatOp (p, _),          AtomFloat x     -> AtomFloat  (Rawfloat.of_rawfloat p x)
    | RawIntOfIntOp (p, s),           AtomInt i       -> AtomRawInt (Rawint.of_int p s i)
    | RawIntOfFloatOp (p, s, _),      AtomFloat x     -> AtomRawInt (Rawfloat.to_rawint p s x)
    | FloatOfRawIntOp (p, _, _),      AtomRawInt i    -> AtomFloat  (Rawfloat.of_rawint p i)
    | RawIntOfRawIntOp (p, s, _, _),  AtomRawInt i    -> AtomRawInt (Rawint.of_rawint p s i)
    | BitFieldOp (p, s, off, len),    AtomRawInt i    -> AtomRawInt (Rawint.field i (Int32.to_int off) (Int32.to_int len))
    | _ ->
         let pos = string_pos "compute_unop" pos in
            raise (MirException (pos, StringError "bad unop"))


let compute_binop pos op c1 c2 =
   let pos = string_pos "compute_binop" pos in
      match c1, c2 with
         AtomInt i, AtomInt j ->
            (match op with
                PlusOp _  -> AtomInt (i + j)
              | MinusOp _ -> AtomInt (i - j)
              | MulOp _   -> AtomInt (i * j)
              | DivOp _   -> AtomInt (i / j)
              | RemOp _   -> AtomInt (i mod j)
              | SlOp _    -> AtomInt (i lsl j)
              | LSrOp _   -> AtomInt (i lsr j)
              | ASrOp _   -> AtomInt (i asr j)
              | AndOp _   -> AtomInt (i land j)
              | OrOp _    -> AtomInt (i lor j)
              | XorOp _   -> AtomInt (i lxor j)
              | MinOp _   -> AtomInt (min i j)
              | MaxOp _   -> AtomInt (max i j)
              | EqOp _    -> test (i == j)
              | NeqOp _   -> test (i != j)
              | LtOp _    -> test (i <  j)
              | LeOp _    -> test (i <= j)
              | GtOp _    -> test (i >  j)
              | GeOp _    -> test (i >= j)
              | CmpOp _   -> AtomInt (Pervasives.compare i j)
              | _         ->
                   raise (MirException (pos, StringError "bad binop")))
       | AtomRawInt i, AtomRawInt j ->
            (match op with
                PlusOp _  -> AtomRawInt (Rawint.add i j)
              | MinusOp _ -> AtomRawInt (Rawint.sub i j)
              | MulOp _   -> AtomRawInt (Rawint.mul i j)
              | DivOp _   -> AtomRawInt (Rawint.div i j)
              | RemOp _   -> AtomRawInt (Rawint.rem i j)
              | SlOp _    -> AtomRawInt (Rawint.shift_left i j)
              | LSrOp _
              | ASrOp _   -> AtomRawInt (Rawint.shift_right i j)
              | AndOp _   -> AtomRawInt (Rawint.logand i j)
              | OrOp _    -> AtomRawInt (Rawint.logor i j)
              | XorOp _   -> AtomRawInt (Rawint.logxor i j)
              | MinOp _   -> AtomRawInt (Rawint.min i j)
              | MaxOp _   -> AtomRawInt (Rawint.max i j)
              | EqOp _    -> test (Rawint.compare i j == 0)
              | NeqOp _   -> test (Rawint.compare i j != 0)
              | LtOp _    -> test (Rawint.compare i j <  0)
              | LeOp _    -> test (Rawint.compare i j <= 0)
              | GtOp _    -> test (Rawint.compare i j >  0)
              | GeOp _    -> test (Rawint.compare i j >= 0)
              | CmpOp _   -> AtomRawInt (Rawint.of_int Rawint.Int32 true (Rawint.compare i j))
              | SetBitFieldOp (_, _, off, len) -> AtomRawInt (Rawint.set_field i (Int32.to_int off) (Int32.to_int len) j)
              | _ ->
                   raise (MirException (pos, StringError "bad binop")))
       | AtomFloat x, AtomFloat y ->
            (match op with
                PlusOp  _ -> AtomFloat (Rawfloat.add x y)
              | MinusOp _ -> AtomFloat (Rawfloat.sub x y)
              | MulOp   _ -> AtomFloat (Rawfloat.mul x y)
              | DivOp   _ -> AtomFloat (Rawfloat.div x y)
              | MinOp   _ -> AtomFloat (Rawfloat.min x y)
              | MaxOp   _ -> AtomFloat (Rawfloat.max x y)
              | ATan2Op _ -> AtomFloat (Rawfloat.atan2 x y)
              | EqOp    _ -> test (Rawfloat.compare x y = 0)
              | NeqOp   _ -> test (Rawfloat.compare x y <> 0)
              | LtOp    _ -> test (Rawfloat.compare x y < 0)
              | LeOp    _ -> test (Rawfloat.compare x y <= 0)
              | GtOp    _ -> test (Rawfloat.compare x y > 0)
              | GeOp    _ -> test (Rawfloat.compare x y >= 0)
              | CmpOp   _ -> AtomInt (Rawfloat.compare x y)
              | _ ->
                   raise (MirException (pos, StringError "bad binop")))
       | _ ->
            let pos = string_pos "compute_binop" pos in
               raise (MirException (pos, StringError "bad binop"))

(************************************** simple operator simplification *)

let rec simple pos a =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _
    | AtomVar _
    | AtomFunVar _ ->
         a
    | AtomUnop (op, a) ->
         simple_unop pos op a
    | AtomBinop (op, a1, a2) ->
         simple_binop pos op a1 a2

and simple_unop pos op a =
   let pos = string_pos "simple_unop" pos in
   let a = simple pos a in
      match a with
         AtomInt _
       | AtomRawInt _
       | AtomFloat _ ->
            compute_unop pos op a
       | _ ->
            match op with
               UMinusOp ac ->
                  AtomBinop (MinusOp ac, zero_of_ac pos ac, a)
             | _ ->
                  AtomUnop (op, a)

and simple_binop pos op a1 a2 =
   let pos = string_pos "simple_binop" pos in
   let a1 = simple pos a1 in
   let a2 = simple pos a2 in
      match a1, a2 with
         AtomInt _, AtomInt _
       | AtomRawInt _, AtomRawInt _
       | AtomFloat _, AtomFloat _ ->
            (* fully constant operations *)
            compute_binop pos op a1 a2
       | AtomInt _, _
       | AtomRawInt _, _
       | AtomFloat _, _ ->
            (* left identity and annihilation *)
            (match op, a1 with
                PlusOp _, AtomInt 0
              | OrOp _,   AtomInt 0
              | XorOp _,  AtomInt 0 ->
                   a2
              | PlusOp _, AtomRawInt i
              | OrOp _, AtomRawInt i
              | XorOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   a2

              | MulOp _, AtomInt 0
              | AndOp _, AtomInt 0
              | SlOp _, AtomInt 0
              | LSrOp _, AtomInt 0
              | ASrOp _, AtomInt 0 ->
                   a1

              | MulOp _, AtomRawInt i
              | AndOp _, AtomRawInt i
              | SlOp _, AtomRawInt i
              | LSrOp _, AtomRawInt i
              | ASrOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   a1

              | MulOp _, AtomInt 1 ->
                   a2
              | MulOp _, AtomRawInt i
                when Rawint.is_one i ->
                   a2

                (* for comparisons, we put the constants on the right *)
              | EqOp _, _
              | NeqOp _, _ ->
                   simple_binop pos op a2 a1

              | LtOp ac, _ -> simple_binop pos (GtOp ac) a2 a1
              | LeOp ac, _ -> simple_binop pos (GeOp ac) a2 a1
              | GtOp ac, _ -> simple_binop pos (LtOp ac) a2 a1
              | GeOp ac, _ -> simple_binop pos (LeOp ac) a2 a1

              | _ ->
                   AtomBinop (op, a1, a2))

       | _, AtomInt _
       | _, AtomRawInt _
       | _, AtomFloat _ ->
            (* commutativity and right identity and annihilation *)
            (match op, a2 with
                PlusOp _, _
              | MulOp _, _
              | AndOp _, _
              | OrOp _, _
              | XorOp _, _
              | MaxOp _, _
              | MinOp _, _ ->
                   simple_binop pos op a2 a1

              | MinusOp ac, AtomInt i ->
                   simple_binop pos (PlusOp ac) (AtomInt (-i)) a1
              | MinusOp ac, AtomRawInt i ->
                   simple_binop pos (PlusOp ac) (AtomRawInt (Rawint.neg i)) a1

              | SlOp _, AtomInt 0
              | ASrOp _, AtomInt 0
              | LSrOp _, AtomInt 0 ->
                   a1
              | SlOp _, AtomRawInt i
              | ASrOp _, AtomRawInt i
              | LSrOp _, AtomRawInt i
                when Rawint.is_zero i ->
                   a1

              | DivOp _, AtomInt 1 ->
                   a1
              | DivOp _, AtomRawInt i
                when Rawint.is_one i ->
                   a1

              | RemOp _, AtomInt 1 ->
                   AtomInt 0
              | RemOp _, AtomRawInt i
                when Rawint.is_one i ->
                   AtomRawInt (Rawint.sub i i)

              | _ ->
                   AtomBinop (op, a1, a2))

       | _ ->
            AtomBinop (op, a1, a2)

(************************************** associativity *)

let is_const a =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _ ->
         true
    | AtomVar _
    | AtomFunVar _
    | AtomUnop _
    | AtomBinop _ ->
         false

let is_associative op op' =
   match op with
      PlusOp _
    | MulOp _
    | AndOp _
    | OrOp _
    | XorOp _
    | MinOp _
    | MaxOp _ ->
         op = op'
    | _ ->
         false

let rec associate pos a =
   let pos = string_pos "associate" pos in
      match a with
         AtomInt _
       | AtomRawInt _
       | AtomFloat _
       | AtomVar _
       | AtomFunVar _ ->
            a
       | AtomUnop (op, a') ->
            let a'' = associate pos a' in
               if a'' != a' then
                  AtomUnop (op, a'')
               else
                  a
       | AtomBinop (op, c1, AtomBinop (op', c2, t))
       | AtomBinop (op, AtomBinop (op', c1, t), c2)
         when is_const c1 && is_const c2 && is_associative op op' ->
            AtomBinop (op, compute_binop pos op c1 c2, associate pos t)
       | AtomBinop (op, t1, AtomBinop (op', t2, t3)) when is_associative op op' ->
            associate pos (AtomBinop (op, AtomBinop (op, t1, t2), t3))
       | AtomBinop (op, t1, t2) ->
            let t1' = associate pos t1 in
            let t2' = associate pos t2 in
               if t1 != t1' || t2 != t2' then
                  AtomBinop (op, t1', t2')
               else
                  a

(************************************** distributivity *)

let is_ring op1 op2 =
  match op1, op2 with
     MulOp _, PlusOp _
   | MulOp _, MinusOp _
   | AndOp _, OrOp _
   | AndOp _, XorOp _
   | MaxOp _, MinOp _ ->
        true
   | _ ->
        false

let rec distribute pos t =
   match t with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _
    | AtomVar _
    | AtomFunVar _ ->
         t
    | AtomUnop (op, t') ->
         let t'' = distribute pos t' in
            if t'' != t' then
               AtomUnop (op, t'')
            else
               t
    | AtomBinop (op1, AtomBinop (op2, t1, t2), t3) when is_ring op1 op2 ->
         let t3 = distribute pos t3 in
            AtomBinop (op2, AtomBinop (op1, distribute pos t1, t3), AtomBinop (op1, distribute pos t2, t3))
    | AtomBinop (op1, c1, AtomBinop (op2, c2, t))
      when is_const c1 && is_const c2 && is_ring op1 op2 ->
         AtomBinop (op2, compute_binop pos op1 c1 c2, distribute pos t)
    | AtomBinop (op1, t1, AtomBinop (op2, t2, t3)) when is_ring op1 op2 ->
         let t1 = distribute pos t1 in
            AtomBinop (op2, AtomBinop (op1, t1, distribute pos t2), AtomBinop (op1, t1, distribute pos t3))
    | AtomBinop (op, t1, t2) ->
         let t1' = distribute pos t1 in
         let t2' = distribute pos t2 in
            if t1 != t1' || t2 != t2' then
               AtomBinop (op, t1', t2')
            else
               t

(************************************** factoring *)

let rec factor t =
   match t with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _
    | AtomVar _
    | AtomFunVar _ ->
         t
    | AtomUnop (op, t) ->
         AtomUnop (op, factor t)
    | AtomBinop (op, t1, t2) ->
         let t1 = factor t1 in
         let t2 = factor t2 in
            match t1, t2 with
               AtomBinop (op1, t1, t2), AtomBinop (op2, t3, t4) when op1 = op2 && is_ring op1 op ->
                  if t1 = t3 then
                     AtomBinop (op1, AtomBinop (op, t2, t4), t1)
                  else if t1 = t4 then
                     AtomBinop (op1, AtomBinop (op, t2, t3), t1)
                  else if t2 = t3 then
                     AtomBinop (op1, AtomBinop (op, t1, t4), t2)
                  else if t2 = t4 then
                     AtomBinop (op1, AtomBinop (op, t1, t3), t2)
                  else
                     AtomBinop (op, AtomBinop (op1, t1, t2), AtomBinop (op2, t3, t4))
             | _ ->
                  AtomBinop (op, t1, t2)

(************************************** strength reduction *)

let rec strength t = t

(************************************** toplevel transformation functions *)

let rec canonicalize_atom pos t =
   let pos = string_pos "canonicalize_atom" pos in
   let t = simple pos t in
   let t = associate pos t in
   let t' = distribute pos t in
      if t != t' then
         canonicalize_atom pos t'
      else
         t'

let optimize_atom t =
   strength (factor t)
