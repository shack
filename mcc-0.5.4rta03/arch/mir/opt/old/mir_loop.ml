(*
 * Dominators and loop nests.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Jason Hickey, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format

open Debug
open Symbol

open Mir

(*
 * Collect all the calls in the function.
 *)
let rec collect_calls funs calls e =
   match e with
      LetAtom (_, _, _, e)
    | LetExternal (_, _, _, _, e)
    | Reserve (_, e)
    | LetAlloc (_, _, e)
    | SetMem (_, _, _, _, _, e)
    | SetGlobal (_, _, _, e)
    | CheckBounds (_, _, _, _, _, e)
    | Memcpy (_, _, _, _, _, _, e)
    | Debug (_, e)
    | PrintDebug (_, _, _, e)
    | CommentFIR (_, e)
    | SysMigrate (_, _, _, _, _, e)
    | AtomicCommit e
    | CopyOnWrite (_, _, e) ->
         collect_calls funs calls e

    | Match (_, _, cases) ->
         List.fold_left (fun calls (_, e) ->
               collect_calls funs calls e) calls cases
    | IfType (_, _, _, _, e1, e2)
    | IfThenElse (_, _, _, e1, e2) ->
         collect_calls funs (collect_calls funs calls e1) e2
    | TailCall (_, f, _)
    | SpecialCall (TailSysMigrate (_, _, _, f, _))
    | SpecialCall (TailAtomicCommit (f, _))
    | Atomic (f, _, _) ->
         if SymbolSet.mem funs f then
            SymbolSet.add calls f
         else
            calls
    | SpecialCall (TailAtomic _)
    | SpecialCall (TailAtomicRollback _)
    | AtomicRollback _ ->
         calls

(*
 * Build the call graph.
 *)
let build_loop prog =
   let { prog_funs = funs;
         prog_export = export
       } = prog
   in

   (* Table of real function names *)
   let fset =
      SymbolTable.fold (fun fset f _ ->
            SymbolSet.add fset f) SymbolSet.empty funs
   in

   (* Scan each function body *)
   let nodes, direct =
      SymbolTable.fold (fun (nodes, direct) f (_, _, e) ->
            let calls = collect_calls fset SymbolSet.empty e in
            let direct = SymbolSet.union direct calls in
            let nodes = (f, SymbolSet.to_list calls) :: nodes in
               nodes, direct) ([], SymbolSet.empty) funs
   in

   (* Build the root node *)
   let roots = SymbolSet.diff fset direct in
   let roots =
      SymbolTable.fold (fun roots f _ ->
            if SymbolSet.mem fset f then
               SymbolSet.add roots f
            else
               roots) roots export
   in
   let root = (new_symbol_string "root", SymbolSet.to_list roots) in

   (* Build the loop-nest tree *)
   let loop = Loop.create "Mir_loop" fst snd root nodes in
   let loop = Loop.loop_nest loop fst in
      Trace.map fst loop

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
