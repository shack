(* Fir constant folding, algebraic simplification, and reassociation
 * Geoffrey Irving
 * $Id: mir_algebra.mli,v 1.2 2002/04/18 03:48:14 jyh Exp $ *)

open Mir
open Mir_exn

module AtomTable : Mc_map.McMap with type key = atom

val canonicalize_atom : exn_loc -> atom -> atom
val optimize_atom : atom -> atom
