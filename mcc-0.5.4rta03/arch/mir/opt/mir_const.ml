(*
   Perform simple constant folding in MIR.  Note that this optimization
   module is primarily intended to clean up code emitted during the
   backend, NOT to cleanup constants lying around from the source code.
   Copyright (C) 2002,2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)


(* Useful modules *)
open Symbol
open Flags

open Frame_type

open Mir
open Mir_ds
open Mir_valias

open Fir_state


(***  Constant Atoms  ***)


(* const_atom
   Simplifies constants in atoms, where possible.  Note that this
   is primarily intended to cleanup generated MIR code, not source
   code -- therefore is quite incomplete! *)
let rec const_atom senv a =
   match a with
      AtomInt _
    | AtomRawInt _
    | AtomFloat _
    | AtomVar _
    | AtomFunVar _ ->
         a

      (* Unary simplifications *)
    | AtomUnop (op, a) ->
         begin
         let a = const_atom senv a in
            match op, a with
               UMinusOp (ACRawInt _), AtomRawInt a ->
                  AtomRawInt (Rawint.neg a)
             | MemSizeOp (_, ACRawInt (Rawint.Int32, false)), AtomVar (_, v) ->
                  if SymbolTable.mem senv v then
                     (* Size of a block we've allocated - we know the size! *)
                     SymbolTable.find senv v
                  else
                     AtomUnop (op, a)

               (* Coercions to self should be dropped *)
             | RawIntOfRawIntOp (dpre, dsign, spre, ssign), _
               when dpre = spre && dsign = ssign ->
                  a
             | FloatOfFloatOp (dpre, spre), _
               when dpre = spre ->
                  a

               (* Pointer/index conversions that cancel out; this happens
                  sometime when combined with simple memory alias analysis *)
             | SafePointerOfIndexOp ptrty, AtomUnop (SafeIndexOfPointerOp ptrty', a)
             | UnsafePointerOfIndexOp ptrty, AtomUnop (UnsafeIndexOfPointerOp ptrty', a)
               when ptrty = ptrty' ->
                  a
             | SafeFunctionOfIndexOp funty, AtomUnop (SafeIndexOfFunctionOp funty', a)
             | UnsafeFunctionOfIndexOp funty, AtomUnop (UnsafeIndexOfFunctionOp funty', a)
               when funty = funty' ->
                  a

               (* Poly coercions that can be easily eliminated *)
             | IntOfPolyOp, AtomUnop (PolyOfIntOp, a) ->
                  a
             | PointerOfPolyOp ptrty, AtomUnop (PolyOfPointerOp ptrty', a)
               when ptrty = ptrty' ->
                  a
             | FunctionOfPolyOp funty, AtomUnop (PolyOfFunctionOp funty', a)
               when funty = funty' ->
                  a
             | _ ->
                  AtomUnop (op, a)
         end (* Unary *)

      (* Binary simplifications *)
    | AtomBinop (op, a, b) ->
         begin
         let a = const_atom senv a in
         let b = const_atom senv b in
            match op, a, b with
               PlusOp (ACRawInt _), AtomRawInt a, AtomRawInt b ->
                  AtomRawInt (Rawint.add a b)
             | PlusOp (ACRawInt (pre, signed)), a, AtomRawInt b
             | PlusOp (ACRawInt (pre, signed)), AtomRawInt b, a
               when Rawint.compare b (Rawint.of_int pre signed 0) = 0 ->
                  a
             | MinusOp (ACRawInt _), AtomRawInt a, AtomRawInt b ->
                  AtomRawInt (Rawint.sub a b)
             | MinusOp (ACRawInt (pre, signed)), a, AtomRawInt b
               when Rawint.compare b (Rawint.of_int pre signed 0) = 0 ->
                  a
             | MulOp (ACRawInt (pre, signed)), AtomRawInt a, AtomRawInt b ->
                  let a = Rawint.of_rawint pre signed a in
                  let b = Rawint.of_rawint pre signed b in
                     AtomRawInt (Rawint.mul a b)
             | MaxOp (ACRawInt (pre, signed)), AtomRawInt a, AtomRawInt b ->
                  let a = Rawint.of_rawint pre signed a in
                  let b = Rawint.of_rawint pre signed b in
                     AtomRawInt (Rawint.max a b)
             | MaxOp (ACRawInt (pre, false)), a, AtomRawInt b
             | MaxOp (ACRawInt (pre, false)), AtomRawInt b, a
               when Rawint.compare b (Rawint.of_int pre false 0) = 0 ->
                  a
             | MinOp (ACRawInt (pre, signed)), AtomRawInt a, AtomRawInt b ->
                  let a = Rawint.of_rawint pre signed a in
                  let b = Rawint.of_rawint pre signed b in
                     AtomRawInt (Rawint.min a b)
             | OrOp  (ACRawInt (pre, signed)), a, AtomRawInt b
             | OrOp  (ACRawInt (pre, signed)), AtomRawInt b, a
             | XorOp (ACRawInt (pre, signed)), a, AtomRawInt b
             | XorOp (ACRawInt (pre, signed)), AtomRawInt b, a
               when Rawint.compare b (Rawint.of_int pre signed 0) = 0 ->
                  a
             | _ ->
                  AtomBinop (op, a, b)
         end (* Binary *)


let const_atoms senv atoms =
   List.map (const_atom senv) atoms


(***  Constant Folding  ***)


(* const_expr
   Constant folding over an expression.  The senv is the sizeof environment;
   for blocks that we've allocated, we already know their size for the rest
   of the function (no resizing allowed), so we can replace sizeof operations
   safely. *)
let rec const_expr senv e =
   let loc = loc_of_exp e in
   let e = const_expr_core senv (dest_exp_core e) in
      make_exp loc e

and const_expr_core senv e =
   match e with
      LetAtom (v, ac, a, e) ->
         LetAtom (v, ac, const_atom senv a, const_expr senv e)
    | TailCall (op, f, atoms) ->
         TailCall (op, f, const_atoms senv atoms)
    | Match (a, ac, cases) ->
         let process_case (s, e) = (s, const_expr senv e) in
            Match (const_atom senv a, ac, List.map process_case cases)
    | IfThenElse (op, a1, a2, e1, e2) ->
         IfThenElse (op, const_atom senv a1, const_atom senv a2,
                     const_expr senv e1, const_expr senv e2)
    | IfType (obj, names, name, v, e1, e2) ->
         IfType (const_atom senv obj, const_atom senv names, const_atom senv name,
                 v, const_expr senv e1, const_expr senv e2)
    | LetExternal (v, ac, s, atoms, e) ->
         LetExternal (v, ac, s, const_atoms senv atoms, const_expr senv e)
    | LetExternalReserve ((label, vars, mem, ptr), v, ac, s, atoms, e) ->
         LetExternalReserve ((label, vars, const_atom senv mem, const_atom senv ptr),
                             v, ac, s, const_atoms senv atoms, const_expr senv e)
    | Reserve ((label, vars, mem, ptr), e) ->
         Reserve ((label, vars, const_atom senv mem, const_atom senv ptr),
                  const_expr senv e)
    | LetAlloc (v, op, e) ->
         let senv, op =
            match op with
               AllocTuple atoms ->
                  senv, AllocTuple (const_atoms senv atoms)
             | AllocArray atoms ->
                  senv, AllocArray (const_atoms senv atoms)
             | AllocMArray (atoms, a) ->
                  senv, AllocMArray (const_atoms senv atoms, const_atom senv a)
             | AllocUnion (i, atoms) ->
                  senv, AllocUnion (i, const_atoms senv atoms)
             | AllocMalloc a ->
                  let senv = SymbolTable.add senv v a in
                     senv, AllocMalloc (const_atom senv a)
         in
            LetAlloc (v, op, const_expr senv e)
    | SetMem (ac, ptrty, a1, a2, a3, e) ->
         SetMem (ac, ptrty, const_atom senv a1, const_atom senv a2,
                 const_atom senv a3, const_expr senv e)
    | SetGlobal (ac, v, a, e) ->
         SetGlobal (ac, v, const_atom senv a, const_expr senv e)
    | BoundsCheck (label, ptrty, ptr, start, stop, e) ->
         BoundsCheck (label, ptrty, const_atom senv ptr, const_atom senv start,
                      const_atom senv stop, const_expr senv e)
    | LowerBoundsCheck (label, ptrty, ptr, start, e) ->
         LowerBoundsCheck (label, ptrty, const_atom senv ptr,
                           const_atom senv start, const_expr senv e)
    | UpperBoundsCheck (label, ptrty, ptr, stop, e) ->
         UpperBoundsCheck (label, ptrty, const_atom senv ptr,
                           const_atom senv stop, const_expr senv e)
    | PointerIndexCheck (label, ptrty, v, ac, index, e) ->
         PointerIndexCheck (label, ptrty, v, ac, const_atom senv index, const_expr senv e)
    | FunctionIndexCheck (label, funty, v, ac, index, e) ->
         FunctionIndexCheck (label, funty, v, ac, const_atom senv index, const_expr senv e)
    | Memcpy (ptrty, a1, a2, a3, a4, a5, e) ->
         Memcpy (ptrty, const_atom senv a1, const_atom senv a2,
                 const_atom senv a3, const_atom senv a4,
                 const_atom senv a5, const_expr senv e)
    | Debug (info, e) ->
         Debug (info, const_expr senv e)
    | PrintDebug  (v1, v2, atoms, e) ->
         PrintDebug (v1, v2, const_atoms senv atoms, const_expr senv e)
    | CommentFIR (fir, e) ->
         CommentFIR (fir, const_expr senv e)
    | SpecialCall op ->
         let op =
            match op with
               TailSysMigrate (label, loc_base, loc_off, f, args) ->
                  TailSysMigrate (label, const_atom senv loc_base, const_atom senv loc_off,
                                  f, const_atoms senv args)
             | TailAtomic (f, i, args) ->
                  TailAtomic (f, const_atom senv i, const_atoms senv args)
             | TailAtomicRollback (level, i) ->
                  TailAtomicRollback (const_atom senv level, const_atom senv i)
             | TailAtomicCommit (level, f, args) ->
                  TailAtomicCommit (const_atom senv level, f, const_atoms senv args)
         in
            SpecialCall op
    | SysMigrate (label, dptr, doff, f, env) ->
         SysMigrate (label, const_atom senv dptr, const_atom senv doff,
                     f, const_atom senv env)
    | Atomic (f, i, env) ->
         Atomic (f, const_atom senv i, const_atom senv env)
    | AtomicRollback (level, i) ->
         AtomicRollback (const_atom senv level, const_atom senv i)
    | AtomicCommit (level, e) ->
         AtomicCommit (const_atom senv level, const_expr senv e)
    | CopyOnWrite ((label, vars, mem, ptr), copy_ptr, e) ->
         CopyOnWrite ((label, vars, const_atom senv mem, const_atom senv ptr),
                      const_atom senv copy_ptr, const_expr senv e)


(***  Interface Code  ***)


(* const_prog
   Constant folding over an entire expression. *)
let rec const_prog prog =
   let funs = prog.prog_funs in

   (* Fold constants in function bodies *)
   let const_fun name (line, args, e) = (line, args, const_expr SymbolTable.empty e) in
   let funs = SymbolTable.mapi const_fun funs in

   (* Construct the new program *)
   let prog = { prog with prog_funs = funs } in

   (* We probably introduced new valiases *)
   let prog = valias_prog prog in
      prog

let const_prog prog =
   const_prog (const_prog (const_prog prog))

let const_prog prog = Fir_state.profile "Mir_const.const_prog" const_prog prog

let const_prog prog =
   if optimize_mir_level "opt.mir.const" 1 then
      const_prog prog
   else
      prog

let () = std_flags_register_list_help "opt.mir"
  ["opt.mir.const",  FlagBool true,
                     "Enable simple MIR constant folding.  This optimization is less powerful" ^
                     " than the FIR optimization and is intended only for cleaning up code" ^
                     " introduced by the MIR stage."]
