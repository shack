(*
   Maintain arity and argument type information for functions.
   This information is used to determine which function coercions
   are ``safe''; e.g. we can cast a function accepting an int
   argument to a function accepting a float argument, but NOT to
   a function accepting a pointer argument.
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Frame_type


(* ArityCompare
   ArityTable
   Simple module for comparing two lists of arity information, and
   a table typically used to map arity lists to a unique symbolic
   identifier that stores some label for the arity information.  *)
module ArityCompare = struct
   type t = arity list

   let rec compare = Pervasives.compare
end (* module ArityCompare *)

module ArityTable = Mc_map.McMake (ArityCompare)


(* string_of_arity
   Builds a simple, readable string representation of an arity list. *)
let string_of_arity arity =
   let rec append = function
      [] ->
         ""
    | a :: arity ->
         let s =
            match a with
               ArityValue                 -> "value"
             | ArityPointer PtrAggr       -> "aggr"
             | ArityPointer PtrBlock      -> "block"
             | ArityPointerInfix PtrAggr  -> "infixaggr"
             | ArityPointerInfix PtrBlock -> "infixblock"
             | ArityFunction ls           -> "fun" ^ (string_of_int (List.length ls))
             | ArityPoly                  -> "poly"
         in
            "_" ^ s ^ (append arity)
   in
      "arity" ^ (append arity)
