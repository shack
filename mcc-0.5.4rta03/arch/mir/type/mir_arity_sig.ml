(*
   Signature for MIR arity utilities
   Copyright (C) 2002 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Frame_type

open Mir
open Mir_pos
open Mir_arity_map


module type MirAritySig =
sig
   val arity_empty : 'a ArityTable.t
   val arity_lookup : 'a ArityTable.t -> pos -> arity list -> 'a
   val arity_lookup_by_formals : 'a ArityTable.t -> pos -> (var * atom_class) list -> 'a
   val arity_lookup_by_funty : 'a ArityTable.t -> pos -> funty -> 'a
   val generate_arity_tags : prog -> prog
end
