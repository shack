(*
   Utilities used by FIR->MIR conversion
   Copyright (C) 2001 Justin David Smith, Caltech

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *)
open Symbol
open Fir_set
open Frame_type
open Mir


module type MirUtilSig =
sig
   (* Number utilities *)
   val is_power2 : int -> bool

   (* ML ints. *)
   val signed_int : bool
   val unsigned_int : bool

   (* Native ints. *)
   val ac_native_int : atom_class

   val ac_native_unsigned : atom_class

   val ac_native_bool : atom_class
   val native_false_value : atom
   val native_true_value : atom
   val native_false_set : RawIntSet.t
   val native_true_set : RawIntSet.t
   val native_true_set_alt : RawIntSet.t

   val hash_native_unsigned : atom
   val hash_fun_native_unsigned : atom
   val ptbl_pointer_native_unsigned : Rawint.rawint

   (* Pointers. *)
   val sizeof_pointer_raw : Rawint.rawint
   val ac_block_pointer : atom_class
   val ac_aggr_pointer : atom_class
   val ac_pointer_base : atom_class
   val one_pointer : Rawint.rawint
   val zero_pointer : Rawint.rawint
   val one_pointer_set : RawIntSet.t
   val zero_pointer_set : RawIntSet.t
   val unsigned_true_set : RawIntSet.t
   val unsigned_false_set : RawIntSet.t

   (* Tags are ML ints. *)
   val ac_tag : atom_class

   (* Functions relating to atom classes *)
   val ptrty_of_sub_block : Fir.sub_block -> Frame_type.ptrty
   val atom_class_of_atom : Mir.atom -> Mir.atom_class
   val atom_class_of_type          : Fir_env.tenv -> Fir_pos.pos -> Fir.ty -> Mir.atom_class
   val atom_class_of_fir_subop     : Fir_env.tenv -> Fir_pos.pos -> Fir.ty -> Fir.subop -> Mir.atom_class
   val atom_class_of_fir_sub_value : Fir_env.tenv -> Fir_pos.pos -> Fir.ty -> Fir.sub_value -> Mir.atom_class
   val funty_of_function_type      : Fir_env.tenv -> Fir_pos.pos -> Fir.ty -> funty
   val type_of_atom : Fir_env.venv -> Fir_pos.pos -> atom -> Fir.ty
   val atom_class_of_unop_args : Mir.unop -> Mir.atom_class * Mir.atom_class
   val atom_class_of_unop : Mir.unop -> Mir.atom_class
   val atom_class_of_binop_args : Mir.binop -> Mir.atom_class * Mir.atom_class * Mir.atom_class
   val atom_class_of_binop : Mir.binop -> Mir.atom_class
   val atom_class_of_relop : Mir.relop -> Mir.atom_class
   val atom_class_of_alloc_op : Mir.alloc_op -> Mir.atom_class
   val atom_class_of_init_exp : Mir.init_exp -> Mir.atom_class
   val atom_class_may_be_pointer : Mir.atom_class -> bool
   val sizeof_atom_class : Mir.atom_class -> int32
   val sizeof_atom : Mir.atom -> int32
   val sizeof_atom_list : Mir.atom list -> int32
end
