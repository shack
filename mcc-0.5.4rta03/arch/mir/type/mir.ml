(*
 * Machine Intermediate Representation, type definition.  Please
 * note that some definitions required by the MIR reside in the
 * arch/type/frame_type.ml file now; in particular, atom classes
 * are needed by the frame.
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * jyh@cs.caltech.edu
 *)
open Symbol
open Location
open Attribute
open Interval_set

open Fir_set

open Frame_type

open Mir_arity_map

(*
 * This is used to annotate terms with location information.
 *)
type 'core simple_subst = (loc, 'core) simple_term


(*
 * Inherit some types.
 *)
type var          = symbol
type ty_var       = symbol
type ty           = Fir.ty
type atom_class   = Frame_type.atom_class


(************************************************************************
 * SETS
 ************************************************************************)


(*
 * Sets of values.
 *)
type int_set      = IntSet.t
type rawint_set   = RawIntSet.t

type int_precision   = Rawint.int_precision
type int_signed      = Rawint.int_signed
type float_precision = Rawfloat.float_precision

(*
 * General set.
 *)
type set =
   IntSet      of int_set
 | RawIntSet   of rawint_set


(************************************************************************
 * EXPRESSIONS                                                          *
 ************************************************************************)


(*
 * Debugging info.
 *)
type debug_line = loc

type debug_vars = (var * ty * var) list

type debug_info = debug_line * debug_vars


(*
 * Unary operators for use in atoms.
 *)
type unop =
   (* Integer+float unary operations *)
   UMinusOp       of atom_class        (* Arithmetic negation *)
 | AbsOp          of atom_class        (* Absolute value *)

   (* Integer-only unary operations *)
 | NotOp          of atom_class        (* Bitwise negation *)

   (* Float-only unary operations *)
 | SinOp          of float_precision   (* Sine *)
 | CosOp          of float_precision   (* Cosine *)
 | SqrtOp         of float_precision   (* Square root *)

   (*
    * BitFieldOp (pre, signed, offset, length)
    *    Extracts the bitfield in an integer value, which starts at bit
    *    <offset> (counting from LSB) and containing <length> bits. The
    *    bit shift and length are constant values.  To access a variable
    *    bitfield you must do a manual shift/mask; this optimization is
    *    only good for constant values.  (pre, signed) refer to the
    *    rawint precision of the result; the input should be int32.
    *)
 | BitFieldOp     of int_precision * int_signed * int32 * int32

   (*
    * Coercions:
    *    Int refers to ML-style integer
    *    Float refers to floating-point value
    *    RawInt refers to native int, any precision
    *
    * Dest precisions always appear before source precisions in operators
    * where both terms must have precision qualifiers.
    *)
 | IntOfFloatOp      of float_precision
 | FloatOfIntOp      of float_precision
 | RawIntOfRawIntOp  of int_precision * int_signed * int_precision * int_signed
 | FloatOfFloatOp    of float_precision * float_precision
 | RawIntOfFloatOp   of int_precision * int_signed * float_precision
 | FloatOfRawIntOp   of float_precision * int_precision * int_signed
 | RawIntOfIntOp     of int_precision * int_signed
 | IntOfRawIntOp     of int_precision * int_signed

   (*
    * Polymorphic coercions:
    *
    * Currently, all polymorphic coercions are identity operations.
    * E.g. PolyOfInt converts int31->int32 value, but it PRESERVES the
    * LSB of the value.  Poly operators do NOT perform safety checks.
    *)
 | PolyOfIntOp
 | IntOfPolyOp
 | PolyOfPointerOp   of ptrty
 | PointerOfPolyOp   of ptrty
 | PolyOfFunctionOp  of funty
 | FunctionOfPolyOp  of funty

   (*
    * Pointer coercions.
    *
    * These are all identity operators, but exist for the benefit of
    * the atom class checker.  They should NEVER appear before MIR
    * simplification, since the ONLY time we should be poking directly
    * at a pointer's value directly are when we are converting between
    * pointer and (hashed) index.
    *)
 | Int32OfPointerOp  of ptrty

   (*
    * Memory operations.  For memory operations, the input is always
    * a real pointer to the base of a block, or a real function pointer.
    * The atom class given is the atom class of the result, which should
    * be ACInt or ACRawInt (Rawint.Int32, false).
    *
    * MemTagOp (ptrty, ac)
    *    Returns the tag of a memory block with given base pointer.
    *    ptrty indicates the type of block to examine, result is ac.
    *
    * MemSizeOp (ptrty, ac)
    *    Returns the size of a memory block, in bytes.  ptrty
    *    indicates type of block, result has type ac.
    *
    * MemIndexOp (ptrty, ac)
    *    Returns the unhashed index of a block of memory.  ptrty is
    *    the type of block, result has type ac.
    *
    * MemFunIndexOp (funty, ac)
    *    Returns the unhashed index of a function pointer.  The
    *    result has type ac.
    *
    * MemFunArityTagOp (funty, ac)
    *    Returns the arity tag for a function pointer.  This returns
    *    the tag stored in the header, not necessarily the tag that
    *    _ought_ to be returned for this funty.  The result is ac.
    *)
 | MemTagOp          of ptrty * atom_class
 | MemSizeOp         of ptrty * atom_class
 | MemIndexOp        of ptrty * atom_class
 | MemFunIndexOp     of funty * atom_class
 | MemFunArityTagOp  of funty * atom_class

   (*
    * Convert real pointers to (hashed?) index values and vice
    * versa.  There are two types of conversion; Safe and Unsafe.
    * Safe pointers are represented in memory as unhashed index
    * values, while Unsafe pointers are represented using hashed
    * index values.  The index values are always Int32 values.
    *
    * None of these operators do safety checks when converting from
    * index to pointer; for safety checks use the PointerIndexCheck
    * and FunctionIndexCheck operators instead.
    *)
   (* These pseudo-ops are eliminated by the time of asm generation.  *)
 | UnsafePointerOfIndexOp of ptrty  (* hashed index -> pointer *)
 | UnsafeFunctionOfIndexOp of funty (* hashed index -> function ptr *)
 | SafePointerOfIndexOp of ptrty    (* index -> pointer *)
 | SafeFunctionOfIndexOp of funty   (* index -> function ptr *)
 | UnsafeIndexOfPointerOp of ptrty  (* pointer -> hashed index *)
 | UnsafeIndexOfFunctionOp of funty (* fun ptr -> hashed index *)
 | SafeIndexOfPointerOp of ptrty    (* pointer -> index *)
 | SafeIndexOfFunctionOp of funty   (* fun ptr -> index *)

   (*
    * Infix pointer operations.  Infix pointers are pairs of a base
    * pointer for a block, and a real pointer to somewhere in the
    * same block.  They are used to optimize memory operations. The
    * operators deal with conversion from infix to (base, off) form.
    *
    * BaseOfInfixPointerOp (ptrty)
    *    Takes an infix pointer, and extracts the block base pointer.
    *
    * OffsetOfInfixPointerOp (ptrty)
    *    Takes an infix pointer, and determines the offset into the block.
    *)
 | BaseOfInfixPointerOp of ptrty
 | OffsetOfInfixPointerOp of ptrty


(*
 * Binary operators for use in atoms.
 *)
and binop =
   (*
    * Integer/float operations.  Both inputs should have the same atom
    * class; the output will have the same atom class as the inputs.
    *)
   PlusOp   of atom_class
 | MinusOp  of atom_class
 | MulOp    of atom_class
 | DivOp    of atom_class
 | RemOp    of atom_class
 | MaxOp    of atom_class
 | MinOp    of atom_class

   (*
    * Integer-only operations.  Both inputs should have the same atom
    * class; the output will have the same atom class as the inputs.
    *)
 | SlOp     of atom_class        (* Shift left (arithmetic/logical) *)
 | ASrOp    of atom_class        (* Arithmetic shift right (preserve MSB) *)
 | LSrOp    of atom_class        (* Logical shift right (shift in zeros) *)
 | AndOp    of atom_class        (* Bitwise and operator *)
 | OrOp     of atom_class        (* Bitwise or operator *)
 | XorOp    of atom_class        (* Bitwise xor operator *)

   (* Float-only operations *)
 | ATan2Op  of float_precision   (* arctan(y/x); first argument is y. *)

   (*
    * SetBitFieldOp (pre, signed, offset, length)
    *    See comments for BitFieldOp.  This modifies the bitfield starting
    *    at bit <offset> and extending <length> bits.  There are two atoms:
    *       First atom is the integer containing the field (32-bit).
    *       Second atom is the value to be set in the field.
    *    The resulting integer contains the revised field, with type
    *    ACRawInt (pre, signed)
    *)
 | SetBitFieldOp of int_precision * int_signed * int32 * int32

   (*
    * Memory operations:
    *    First atom is always a real pointer
    *    Second atom is an offset, always Int32 (never an ML-style int!)
    *
    * MemOp (ac, ptrty)
    *    Reads from the pointer/offset specified.  The offset should be a
    *    BYTE offset and a raw integer.  The atom class of the value to read
    *    is given in <ac>.  The value read may need to be converted; e.g.
    *    when reading pointers from memory, you need to convert the index
    *    read to a real pointer yourself.
    *)
 | MemOp    of atom_class * ptrty

   (*
    * Integer/float comparisons.  The result is always ac_native_bool,
    * which is defined in mir_util.ml; it is a raw integer type, not ML
    * (an exception is CmpOp, which returns ACInt, *not* a boolean).
    * The atom class specifies the types of the inputs; if it is an
    * unsigned atom class, then unsigned comparison will be used.
    *)
 | EqOp     of atom_class     (* Equivalence *)
 | NeqOp    of atom_class     (* Non-equivalence *)
 | LtOp     of atom_class     (* Strictly less than *)
 | LeOp     of atom_class     (* Less than or equal *)
 | GtOp     of atom_class     (* Strictly greater than *)
 | GeOp     of atom_class     (* Greater than or equal *)
 | CmpOp    of atom_class     (* Similar to ML's ``compare'' function *)

   (*
    * Infix pointer operations.  Infix pointers are pairs of a base
    * pointer for a block, and a real pointer to somewhere in the
    * same block.  They are used to optimize memory operations. The
    * operators deal with conversion from (base, off) to infix form.
    *
    * InfixOfBaseOffsetOp (ptrty)
    *    Takes a pointer base, and an offset into the block, and
    *    builds a 64-bit infix pointer.
    *)
 | PlusPointerOp of ptrty
 | InfixOfBaseOffsetOp of ptrty


(*
 * Relative operators, used primarily in IfThenElse.  Like the binary
 * comparison operators, atom class is for the type of the inputs; if the
 * atom class is an unsigned atom class, then unsigned comparison will be
 * used.
 *)
and relop =
   REqOp    of atom_class     (* Equivalence *)
 | RNeqOp   of atom_class     (* Non-equivalence *)
 | RLtOp    of atom_class     (* Strictly less than *)
 | RLeOp    of atom_class     (* Less than or equal *)
 | RGtOp    of atom_class     (* Strictly greater than *)
 | RGeOp    of atom_class     (* Greater than or equal *)
 | RAndOp   of atom_class     (* Boolean AND operation *)


(*
 * Allocation operators:
 *
 *    AllocTuple args         Allocate a new tuple, using args to initialise
 *                            the tuple block.  The block is an ML block,
 *                            and size/tag are determined automatically.
 *
 *    AllocArray args         Allocate a new array.  Similar to AllocTuple.
 *
 *    AllocMArray (dimen, a)  Allocate a new multidimensional array (used by
 *                            Java).  The list of dimens are the dimensions
 *                            for the array, and should be Int32 values. The
 *                            solitary atom is used as the initialiser for
 *                            ALL entries in the array.
 *
 *    AllocUnion (tag, args)  Allocate a union.  Similar to AllocTuple, but
 *                            the tag is explicitly given here.
 *
 *    AllocMalloc size        Allocate an aggregate block of <size> BYTES.
 *                            The size is an Int32 value.  The block is left
 *                            in an uninitialized state.
 *)
and alloc_op =
   AllocTuple of atom list
 | AllocArray of atom list
 | AllocMArray of atom list * atom
 | AllocUnion of int * atom list
 | AllocMalloc of atom


(*
 * Kinds of tailcall:
 *    DirectCall        Jump directly to the named label (must be a function)
 *    IndirectCall      The label contains a function pointer to jump to.
 *    ExternalCall      Call an external (C) function -- used for segfaults.
 *)
and tailcall_op =
   DirectCall
 | IndirectCall
 | ExternalCall


(*
 * Normal atom values.  In the MIR, atoms are expression trees:
 *    AtomInt i            ML-style 31-bit integer
 *    AtomRawInt i         Raw integer of arbitrary precision/signedness.
 *    AtomFloat f          Raw float of arbitrary precision (always signed)
 *    AtomVar (ac, v)      Variable, used with specified atom class
 *    AtomFunVar (ac, f)   Function label (always points to escape version)
 *    AtomUnop (op, a1)    Unary operator applied to an argument.
 *    AtomBinop (op,a1,a2) Binary operator applied to two arguments.
 *)
and atom =
   AtomInt     of int
 | AtomRawInt  of Rawint.rawint
 | AtomFloat   of Rawfloat.rawfloat
 | AtomVar     of atom_class * var
 | AtomFunVar  of atom_class * var
 | AtomUnop    of unop * atom
 | AtomBinop   of binop * atom * atom


(*
 * Reserve information (listed in the order it appears in reserve_info):
 *
 * label
 *    Unique label identifying the reserve block.  This label is the
 *    one that will be used to emit the registers block of data.
 *
 * ptr_regs
 *    List of registers that contain (or may contain) pointers. This
 *    list is determined by the type of each variable; types that are
 *    pointer types should be in ptr_regs.  These will be the roots
 *    in GC.
 *
 * num_bytes
 *    Total amount of memory that *may* need to be allocated, in BYTES.
 *    Include any headers in this sum.  Do not include allocations after
 *    a guaranteed Reserve statement, and do not include the possible
 *    allocation from COW (these are handled separately by COW).  DO
 *    include memory allocated after a COW, since COW may not always
 *    run the garbage collector.
 *
 * num_ptrs
 *    Total number of new pointers that *may* need to be allocated.
 *    Do not include allocations after a guaranteed Reserve statement.
 *    Do include pointers allocated after a COW.
 *)
and reserve_info = var * var list * atom * atom


(*
 * This is the same tailop that appears in the FIR (fir/type/fir.ml);
 * since SpecialCall is eliminated early on in the MIR, it's not worth
 * redocumenting it here.  See the FIR type for more information.
 *)
and tailop =
   TailSysMigrate of int * atom * atom * var * atom list
 | TailAtomic of var * atom * atom list
 | TailAtomicRollback of atom * atom
 | TailAtomicCommit of atom * var * atom list


(*
 * MIR Expressions.
 *)
and exp = exp_core simple_subst

and exp_core =
   (* Standard primitives *)
   (*
    * LetAtom
    *    General assignment.  The variable is functional; the atom
    *    is an arbitrary expression tree -- see atoms, above.
    *)
   LetAtom of var * atom_class * atom * exp

   (* Allocation and memory operations *)
   (*
    * Reserve (rinfo, e)
    *    Reserve memory for allocation in the heap/pointer table.  See the
    *    comments on reserve_info for more information.
    *
    * LetAlloc (v, op, e)
    *    Allocate a block of memory.  See the comments on alloc_op above
    *    for more information.
    *
    * SetMem (ac, ptrty, ptr, off, a, e)
    *    Write to memory.  The value a (which should have atom class ac)
    *    is written to the location ptr[off].  The offset must be Int32
    *    and should be a byte offset; the base pointer in ptr should have
    *    corresponding type in ptrty.  The value being written may need to
    *    be converted; e.g. when writing pointers to memory you must do
    *    the conversion to pointer index yourself.
    *
    * SetGlobal (ac, v, a, e)
    *    Modify the global variable v to have value a.  The global variable
    *    should have the indicated atom class.
    *)
 | Reserve of reserve_info * exp
 | LetAlloc of var * alloc_op * exp
 | SetMem of atom_class * ptrty * atom * atom * atom * exp
 | SetGlobal of atom_class * var * atom * exp

   (* Functions *)
   (*
    * TailCall (op, f, args)
    *    Call f with indicated args.  See tailcall_op for details.
    *
    * LetExternal (v, ac, f, args, e)
    *    Call external function f with indicated args.  The return
    *    value of f has atom class given by ac, and will be stored
    *    in variable v.
    *
    * LetExternalReserve (rinfo, v, ac, f, args, e)
    *    Call external function f with indicated args.  The return
    *    value of f has atom class given by ac, and will be stored
    *    in variable v.  This version allows garbage collection
    *    during the call.
    *)
 | TailCall of tailcall_op * var * atom list
 | LetExternal of var * atom_class * string * atom list * exp
 | LetExternalReserve of reserve_info * var * atom_class * string * atom list * exp

   (* Flow control *)
   (*
    * IfThenElse (op, a1, a2, e1, e2)
    *    Evaluate e1 if a1 (op) a2 is true; otherwise, evaluate
    *    expression e2.
    *
    * Match (a, ac, cases)
    *    Evaluate the case whose set contains the value a.  In
    *    most cases, the sets used in each case must agree with
    *    the atom class used for the atom (a notable exception
    *    occurs in matches for union tag, when the union contains
    *    constant and nonconstant constructors).  The first case
    *    whose set contains a will be evaluated.  The cases must
    *    be complete; the backend will likely assume that the
    *    last case is the ``default'' case, and drop the check.
    *)
 | IfThenElse of relop * atom * atom * exp * exp
 | Match of atom * atom_class * (set * exp) list

   (* Type coercion for classes *)
   (*
    * IfType (obj, names, name, v, e1, e2)
    *    The expression translates roughly to:
    *       if name in names then
    *          let v = ... in
    *             e1
    *       else
    *          e2
    *    The assignment to v depends on whether name is an object name or an
    *    interface name.  If it is an object name, then v is assigned obj
    *    directly; if it is an interface, we assign a pair of the interface
    *    VMA and the object to v.  Therefore IfType must be considered an
    *    unconditional allocation point for a pair of words.
    *)
 | IfType of atom * atom * atom * var * exp * exp

   (* Safety checks *)
   (*
    * BoundsCheck (label, ptrty, ptr, start, stop, e)
    *    This statement verifies that all offsets within the interval
    *    [start, end) (note end is open) are valid offsets into <ptr>.
    *    The offsets must be raw integers, and in same units as the
    *    size field in the block header.
    *
    * LowerBoundsCheck (label, ptrty, ptr, start, e)
    *    This statement only verifies that the lower bound indicated
    *    is valid.  The <start> offset should be a raw integer, and in
    *    the same units as the size field in the block header.
    *
    * UpperBoundsCheck (label, ptrty, ptr, stop, e)
    *    This statement only verifies that the upper bound indicated
    *    is valid.  The <stop> offset should be a raw integer, and in
    *    the same units as the size field in the block header.
    *
    * In all cases, the label is only used to identify this Check statement;
    * it should be a unique label name but is not critical to compilation.
    *)
   (* These are pseudo-statements that are dropped by asm generation. *)
 | BoundsCheck of var * ptrty * atom * atom * atom * exp
 | LowerBoundsCheck of var * ptrty * atom * atom * exp
 | UpperBoundsCheck of var * ptrty * atom * atom * exp

   (*
    * PointerIndexCheck (label, ptrty, v, ac, index, e)
    *    Inserts safety checks to verify that <index> is a valid index
    *    to a block, then stores the real pointer for that block into
    *    variable <v>.  This is equivalent to:
    *       LetAtom (v, ac,
    *                AtomUnop (UnsafePointerOfIndexOp ptrty, index), e)
    *    Except that safety checks on <index> are done here.
    *
    * FunctionIndexCheck (label, funty, v, ac, index, e)
    *    Inserts safety checks to verify that <index> is a valid function
    *    index, then stores the real function pointer into <v>.  This is
    *    equivalent to:
    *       LetAtom (v, ac,
    *                AtomUnop (UnsafeFunctionOfIndexOp funty, index), e)
    *    Except that safety checks on <index> are done here.  Note that
    *    funty contains the expected arguments for the function; this info
    *    will be checked as well, to ensure the function pointer agrees
    *    with the expected arity and argument types.
    *
    * In both forms, label is simply a unique label identifying this check
    * expression.  It is possible that v will be dead after these statements;
    * however these checks must not be removed by dead code elimination. The
    * check versions must be separate from the operators, since DCE would try
    * to remove the operator forms.
    *)
   (* These are pseudo-statements that are dropped by asm generation. *)
 | PointerIndexCheck of var * ptrty * var * atom_class * atom * exp
 | FunctionIndexCheck of var * funty * var * atom_class * atom * exp

   (*
    * Memcpy (ptrty, dst, dst_off, src, src_off, len, e)
    *    Copies <len> BYTES from the <src> block, starting at <src_off>, to
    *    the <dst> block, starting at <dst_off>.  The ptrty indicates what
    *    type of memory operation to perform.  Safety checks must be placed
    *    before the Memcpy on both the source and destination blocks, if
    *    desired.  This form is more efficient than copying element by
    *    element.
    *
    *    WARNING:  This implements a forward-copy; if dst and src are the
    *    same block, and src_off < dst_off < src_off + len, then the copy
    *    will overwrite data that it is supposed to copy, BEFORE it has
    *    copied the data. As a result, Memcpy is not recommended when src
    *    and dst refer to the same block.
    *)
 | Memcpy of ptrty * atom * atom * atom * atom * atom * exp

   (* Extra debugging info *)
 | Debug of debug_info * exp
 | PrintDebug of var * var * atom list * exp
 | CommentFIR of Fir.exp * exp

   (* Advanced primitives *)

   (*
    * SpecialCall is the same that existed in the FIR.  It should be
    * removed by the time of assembly generation and replaced with the
    * MIR primitives below.  The reason this primitive exists here is
    * because FIR->MIR conversion is hideous already; adding the env
    * packager to it will just make things worse...
    *)
   (* This is a pseudo-statement that is dropped by asm generation. *)
 | SpecialCall of tailop

   (* Ambient primitives *)
   (*
    * SysMigrate (id, host, host_off, f, env)
    *    Migrates from current machine to the location named in the
    *    <host>,<host_off> pointer/offset pair.  Here <env> is the INDEX
    *    of a block that holds all registers; note that NO register will
    *    be live across the migrate call, so all registers must be moved
    *    into a memory block for migration.  After migration, call the
    *    function <f>; <f> takes exactly one argument, which is the INDEX
    *    of the env block after migration.
    *
    *    <id> is a unique identifier for this statement; it must be
    *    defined before we marshal the FIR and must be unique, as it is
    *    what we use to re-locate this statement after a successful
    *    migration.
    *
    *    NOTE: This primitive doesn't look a whole lot like the FIR
    *    primitive because by this point, we have already packed all the
    *    variables into a compact memory block.
    *)
 | SysMigrate of int * atom * atom * var * atom

   (* Rollback primitives *)
   (*
    * Atomic (f, i, env)
    *    Enters a new atomic block.  <f> is the name of the atomic function
    *    to call, with type f : int -> 'env -> void.  <env> is a pointer to
    *    the block that contains all registers.  <i> is an arbitrary integer
    *    constant to pass to <f>.  This is a tailcall.
    *
    * AtomicRollback (level, i)
    *    Rolls back an atomic block; <level> is the level being rolled back
    *    (from 1 to N, where there are N levels).  The entry function that is
    *    associated with <level> will be called, with a new constant <i> as
    *    its argument (but the same environment).  The atomic data is stored
    *    in the context, therefore it is not necessary to pass it into this
    *    function.
    *
    *    ALL levels more recent than <level> will be rolled back by this
    *    function.  After this operation, we will be in level <level> again;
    *    we DO re-enter the transaction.  Note that this is a tailcall.
    *
    *    If <level> is 0, then we will roll back the most recent level; so
    *    <level> = 0 is equivalent to <level> = N.  The atomic data is
    *    stored in the context, therefore it is not necessary to pass it
    *    into this function.
    *
    * AtomicCommit (level, e)
    *    Inline statement that commits an operation and releases the atomic
    *    data associated with atomic <level> (from 1 to N).  The data in
    *    <level> is merged with the previous level, overriding information
    *    in the previous level.  This does NOT commit data in later levels.
    *
    *    If <level> is 0, then we will commit the most recent level; so
    *    <level> = 0 is equivalent to <level> = N.
    *)
 | Atomic of var * atom * atom
 | AtomicRollback of atom * atom
 | AtomicCommit of atom * exp

   (*
    * CopyOnWrite (rinfo, ptr, e)
    *    Checks the *real* pointer stored in <ptr> to see if the copy-on-
    *    write bit is set; if set, then the block is copied.  The *working*
    *    copy is the newly allocated block; the existing block remains
    *    immutable and is the ``backup'' copy.  To be successful we have to
    *    rewrite any aliases in the system to point to the new block, so
    *    <rinfo> contains the list of all registers containing pointers.
    *    This expression may also need to call GC, so <rinfo> should contain
    *    all the usual information a Reserve would carry.
    *
    *    Note:  the GC currently uses this as a hook to mark data that is
    *    modified in the major heap of the current atomic generation; as a
    *    result it is important these calls are emitted before modifying
    *    memory, even if we *know* we are not in an atomic block.
    *
    *    NOTE: This expression does NOT replace a Reserve; the GC will only
    *    be called if allocation actually occurs, therefore the reserve info
    *    coded here should include all memory that still needs to be alloc'd
    *    after this point (if we call GC, then the previous reserve will be
    *    forgotten).  Reserve statements should not include potential
    *    allocations for COW, since we assume COW allocation is rare.
    *)
 | CopyOnWrite of reserve_info * atom * exp


(*
 * Function definition.
 *)
and fundef = debug_line * (var * atom_class) list * exp


(*
 * Initializers.  For InitRawData, I have been assured that the data
 * contained within is always 8- or 16-bit data.  I (JDS) think this use of
 * ints is flat-out bogus, and think a Rawint would do just fine here (we
 * could use the int_precision to coerce all rawints to same precision), but
 * I guess whatever works... but I don't like it.
 *
 * InitAtom (a)
 *    Initialise the global with an atom.  Currently, there are restrictions
 *    on what this atom can be; arithmetic operators are not allowed.
 *
 * InitAlloc (op)
 *    Allocate a space in the heap (and probably initialise it).  See the
 *    alloc_op comments for details.
 *
 * InitRawData (pre, data)
 *    An array of 8- or 16-bit data.  This is used for embedded strings; the
 *    pre should always be Int8 or Int16.
 *)
type init_exp =
   InitAtom of atom
 | InitAlloc of alloc_op
 | InitRawData of int_precision * int array


(*
 * The program itself:
 *
 * prog_file
 *    Name of the source file that produced this program.  Informative only.
 *
 * prog_import
 *    List of symbols that must be imported from outside.  Keyed to the
 *    symbol name used in the code for this import. arch/type/frame_type.ml
 *    contains the import type definition.
 *
 * prog_export
 *    List of symbols that are exported to the outside.  Keyed to the
 *    symbol name used in the code for the export. arch/type/frame_type.ml
 *    contains the export type definition.
 *
 * prog_globals
 *    Global variables for this program (which may or may not be exported).
 *    The initializer for the global is given here; it may be an array...
 *
 * prog_funs
 *    The meat of the program --- function definitions, including body.
 *
 * prog_mprog
 *    This is the marshallable FIR that is embedded in the final executable
 *    in case this program is migrated elsewhere.  This data structure must
 *    be ignored from MIR onward, except that we must respect the ordering
 *    of globals and functions in the marshallable FIR so tables will match
 *    exactly after migration.  It should contain exactly the final, refined
 *    and optimized FIR code.
 *
 *    WARNING:  DO NOT MODIFY THESE FIELDS UNDER ANY CIRCUMSTANCE (see below).
 *
 * prog_global_order
 *    Order the globals must be emitted in.  This structure may be modified
 *    in the MIR to insert derived globals after their original global. Any
 *    derived globals should appear immediately after their original global,
 *    in a well-defined order.  The original globals must not be reordered
 *    with respect to each other.
 *
 * prog_fun_order
 *    Order the functions must be emitted in for the function closure table.
 *    Any name appearing in an AtomFunVar must appear in this table.  This
 *    structure may be modified in the MIR to insert derived functions after
 *    their originating function.  Any derived functions should appear
 *    immediately after their originating function, in a well-defined order.
 *    The original functions in this table must not be reordered with respect
 *    to each other.
 *
 * prog_arity_tags
 *    Arity tags associated with this program.
 *
 * WARNING: Originally, the marshal_globals/marshal_funs fields in prog_mprog
 *          were modified directly to incorporate the derived globals/funs.
 *          This caused breakage when the program is migrated, because the
 *          derived variables would appear in the migrated tables, but they
 *          do NOT occur anywhere in the FIR code.  The tables in prog_mprog
 *          MUST be preserved, so we use the prog_global_order/prog_fun_order
 *          fields to incorporate the derived values instead.
 *)
type prog =
   { prog_file          : file_info;
     prog_import        : import SymbolTable.t;
     prog_export        : export SymbolTable.t;
     prog_globals       : init_exp SymbolTable.t;
     prog_funs          : fundef SymbolTable.t;
     prog_mprog         : Fir.mprog;
     prog_global_order  : var list;
     prog_fun_order     : var list;
     prog_arity_tags    : (symbol * int32) ArityTable.t;
   }


(*
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
