(*
 * MIPS instruction set.
 * Note, we include architecture-specific instructions (for example,
 * for the PS2 Emotion Engine).  These should not be used for a
 * generic MIPS architecture.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002,2001 Jason Hickey, Caltech
 * Copyright (C) 2002,2001 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Symbol

(*
 * Precision qualifiers.  For most instructions, this determines
 * the precision of both the source and destination operands; for
 * a few instructions (such as sign/zero extend instructions), one
 * operand is assumed to be a fixed precision.
 *)
type iprec =
   IB    (* 8-bit integer *)
 | IW    (* 16-bit integer *)
 | IL    (* 32-bit integer *)
 | ID    (* 64-bit integer *)
 | IQ    (* 128-bit integer *)

type fprec =
   FS    (* Single - 32bit *)

(*
 * Many instructions allw traps on overflow.
 *)
type overflow =
   TrapOverflow
 | IgnoreOverflow

(*
 * Operand basics.
 *)
type reg = Symbol.symbol
type var = Symbol.symbol
type label = Symbol.symbol

(*
 * Offsets.  Note, we pretend that 32-bit immediates and
 * addresses are allowed.  These are pseudo-instructions; the
 * assembler will generate two instructions for long immediates.
 *)
type offset =
   (* Constants *)
   OffNumber of int32         (* Numbers *)
 | OffLabel of label          (* Label in the code *)

   (* Unary operations *)
 | OffUMinus of offset        (* Arithmetic negation *)
 | OffUNot   of offset        (* Bitwise negation *)
 | OffUAbs   of offset        (* Absolute value *)
 | OffULog2  of offset        (* Logarithm, base 2 *)
 | OffUPow2  of offset        (* Exponentiation, 2^x *)

   (* Binary operations *)
 | OffAdd of offset * offset
 | OffSub of offset * offset
 | OffRel of offset * label   (* Offset relative to a label *)
 | OffMul of offset * offset
 | OffDiv of offset * offset
 | OffSal of offset * offset
 | OffSar of offset * offset
 | OffShr of offset * offset
 | OffAnd of offset * offset
 | OffOr  of offset * offset
 | OffXor of offset * offset


(*
 * Operand types.
 *)
type operand =
   ImmediateNumber of offset
 | ImmediateLabel of label
 | Register of reg
 | FloatRegister of reg

   (*
    * Treat spills specially, the register is just a comment
    * used to indicate what the original name of the value was.
    *)
 | SpillRegister of reg * offset

type src = operand
type dst = operand

(*
 * Condition codes for (integer) conditional operations.
 *)
type cc =
   EQ
 | NEQ

   (* Signed comparisons *)
 | LT
 | LE
 | GT
 | GE

   (* Unsigned comparisons *)
 | ULT
 | ULE
 | UGT
 | UGE

(*
 * Reservations are either base pointers
 * or infix pointers.
 *)
type reserve =
   ResBase of operand
 | ResInfix of operand * operand

(*
 * Info for debugging.
 *)
type debug_line = string * int
type debug_vars = (var * Fir.ty * operand) list
type debug_info = debug_line * debug_vars

(*
 * Assembly instructions.
 *)
type inst =
   (************************************************************************
    * MIPS I/III INSTRUCTIONS
    ************************************************************************)

   (* Memory operations *)
   LD    of iprec * reg * src
 | ST    of iprec * dst * reg
 | MOV   of iprec * reg * reg

   (*
    * Arithmetic operations.
    * Note that the only sensible precisions are IL, ID, and IQ.
    *)
 | ADD  of iprec * overflow * reg * reg * src
 | SUB  of iprec * overflow * reg * reg * src
 | AND  of iprec * reg * reg * src
 | OR   of iprec * reg * reg * src
 | XOR  of iprec * reg * reg * src
 | NOR  of iprec * reg * reg * src
 | SLL  of iprec * reg * reg * src
 | SRL  of iprec * reg * reg * src
 | SRA  of iprec * reg * reg * src

   (*
    * Normal MUL/DIV in MIPS uses the special register
    * HI/LO for the result.
    *)
 | MUL  of iprec * reg * reg
 | DIV  of iprec * reg * reg

   (* Stack operations *)
 | PUSH  of iprec * src    (* push src onto stack *)
 | POP   of iprec * dst    (* pop stack into dst *)
 | CALL  of src            (* call external: return value in $1 *)
 | RET

   (* Branching *)
 | JMP   of dst                  (* unconditional jump to label named *)
 | FJMP  of dst                  (* unconditional jump to label named (implicit) *)
 | IJMP  of src list * dst       (* indirect jump to code pointer in dst *)
 | JCC   of cc * reg * reg * dst (* conditional jump based on cc, to label named *)

   (*
    * Info for reserve/copy-on-write
    * First arg is label/unique identifier
    * Second arg is list of live pointers
    * Third arg is #bytes to allocate
    * Fourth arg is #ptrs to allocate
    *
    * COW adds a FIFTH argument, the copy pointer.
    *)
 | RES   of src * reserve list * src * src
 | COW   of src * reserve list * src * src * src

   (* Comment and nil pseudo-codes *)
 | CommentFIR of Fir.exp
 | CommentMIR of Mir.exp
 | CommentString of string
 | CommentInst of string * inst
 | NOP

   (************************************************************************
    * FLOATING-POINT COPROCESSOR
    ************************************************************************)

   (* We should put something here *)

   (************************************************************************
    * PS2 EMOTION ENGINE INSTRUCTIONS
    ************************************************************************)

   (* Three-operand MUL/DIV *)
 | EEMUL of iprec * overflow * reg * reg * reg
 | EEDIV of iprec * overflow * reg * reg * reg

   (* There are a lot more then these two *)

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
