(*
 * MCC version information.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Mcc_config


(*
 * Display program version information, and then exit.
 *)
let display_version_info () =
   let fir_rev = Parse_id.parse_id_revision Fir.fir_version in
      Printf.printf "MCC (Mojave Compiler) version %s, FIR revision %s\n" version_string fir_rev;
      Printf.printf "Compiled on %s, using the %s backend\n" host_info backend_name;
      (if metaprl_enabled then
         Printf.printf "Compiled with MetaPRL support.\n" 
      else
         Printf.printf "Compiled without MetaPRL support.\n");
      exit 1
