(*
 * MCC compiler main.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2000 Jason Hickey, Caltech
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)
open Format
open Debug
open Flags

open Fir_marshal
open Fir_state
open Fir_mprog

open Mcc_paths
open Mcc_config
open Mcc_version


(*
 * Try to find out where the hell we're installed.
 *)
let mcc_paths = get_mcc_paths ()


(*
 * External programs and libraries.
 *)
let frontend_ext_option = "extension"
let frontend_handler_option = "handler"
let frontend_target_option = "target"
let lang_config = "lang.conf"

let cpp_program = mcc_paths.path_cpp ^ " -D__mcc -D__MCC " ^ mcc_paths.path_include_opt

let link_program = mcc_paths.path_gcc
let runtime_library = mcc_paths.path_runtime


(************************************************************************
 * OPTIONS AND ARGUMENTS
 ************************************************************************)


(*
 * Program options and arguments.
 *)
let output_name = ref None          (* Name of final output file, if set *)
let rev_include = Fir_state.rev_include (* Include directories on command line *)
let rev_defines = ref []            (* Macro definitions on commandline *)
let rev_linker_options = ref []     (* Options to pass direct to the linker *)
let rev_files = ref []              (* List of input files to be compiled *)
let rev_argv = ref []               (* Arguments to pass to the evaluator *)
let output_fir = ref false          (* If set, output the binary FIR file *)
let print_to_asm_file = ref false   (* If set, print output to alt. assembly file *)
let debug_steps = ref false         (* Debug compiler steps, if set *)
let preserve_temps = ref false      (* Preserve temporary files, if set *)
let stop_at_preprocessor = ref false(* If set, preprocess inputs only *)
let stop_at_assembler = ref false   (* If set, stop at assembly output *)
let stop_at_object_file = ref false (* If set, stop at object emission *)
let rev_linkfiles = ref []          (* List of files to link together *)
let rev_tempfiles = ref []          (* List of temporary files to remove *)
let metaprl_send = ref false        (* If set, send FIR to MetaPRL for processing. *)
let metaprl_print_fir = ref false   (* If set, print FIR before and after MetaPRL processing. *)


(*
 * Collect file arguments.
 *)
let collect_file name =
   rev_files := name :: !rev_files

let collect_arg name =
   rev_argv := name :: !rev_argv


(************************************************************************
 * UTILITIES
 ************************************************************************)


(*
 * Split the LAST component of a ch-delimited string into a suffix.
 * If no suffix exists, an empty string is returned for the suffix.
 *)
let split_suffix_component s ch default =
   try
      let index_last_component = (String.rindex s ch) + 1 in
      let component_length = (String.length s) - index_last_component in
      let prefix = String.sub s 0 (index_last_component - 1) in
      let suffix = String.sub s index_last_component component_length in
         prefix, suffix
   with
      Not_found ->
         default


(*
 * Fixes up the pathname
 *)
let correct_pathname path =
   if path = "" then
      "."
   else
      path


(*
 * Extract all filename components
 *)
let extract_filename_components filename =
   let path, filename = split_suffix_component filename '/' ("", filename) in
   let filebase, fileext = split_suffix_component filename '.' (filename, "") in
   let path = correct_pathname path in
      path, filebase, fileext


(*
 * Rewrite the extension of the given filename
 *)
let replace_extension filename ext =
   let path, base, _ = extract_filename_components filename in
      path ^ "/" ^ base ^ "." ^ ext


(*
 * Create a name for a temporary file, based on the input provided.
 *)
let temporary_file filename =
   if not !preserve_temps then
      rev_tempfiles := filename :: !rev_tempfiles;
   filename


(*
 * Quote an option so it is suitable for passing to a shell interpretor
 *)
let rec quote_option option =
   let skip_char s =
      let ch = String.make 1 s.[0] in
      let s = quote_option (String.sub s 1 ((String.length s) - 1)) in
         ch ^ s
   in
   let escape_char s =
      "\\" ^ (skip_char s)
   in
      if String.length option > 0 then
         match option.[0] with
            'a'..'z'
          | 'A'..'Z'
          | '0'..'9'
          | '.'
          | '_'
          | '-' ->
               skip_char option
          | _ ->
               escape_char option
      else
         ""

(************************************************************************
 * PREPROCESSORS
 ************************************************************************)


(*
 * Preprocess a file using cpp - returns the filename of the preprocessed
 * file on success...  Note this will use the -o name for output if we
 * chose to run the preprocessor only with -E, but it will cause problems
 * if you named multiple input files...
 *)
let preprocess_cpp in_filename =
   (* Construct the output filename and remove it (if it exists). *)
   let out_filename =
      match !stop_at_preprocessor, !output_name with
         true, Some name ->
            name
       | _ ->
            temporary_file (replace_extension in_filename "i")
   in

   (* Construct the call to CPP *)
   let includes = List.fold_left (fun s inc -> s ^ " -I" ^ (quote_option inc)) "" (List.rev !rev_include) in
   let defines = List.fold_left (fun s def -> s ^ " -D" ^ (quote_option def)) "" (List.rev !rev_defines) in
   let command = cpp_program ^ includes ^ defines ^ " " ^ in_filename ^ " > " ^ out_filename in
      if !debug_steps then
         Printf.eprintf "Preprocessing %s ==> %s\n   Running %s\n%t" in_filename out_filename command flush;
      if Sys.command command <> 0 then
         raise (Failure "Mcc_main.preprocess_cpp:  Preprocessor failed")
      else
         out_filename


(************************************************************************
 * FRONT-ENDS
 ************************************************************************)


(*
 * Special ``frontend'' for FIR compilation.
 *)
let compile_fir in_filename =
   let inc = open_in in_filename in
   let prog =
      try ChannelMarshal.unmarshal_prog inc with
         exn ->
            close_in inc;
            raise exn
   in
      prog.Fir.marshal_fir


(*
 * List of all supported frontends, and their associated preprocessors.
 *)
let aml_flag = ref false

let front_ends () =
   let front_ends =
      [["unl"],       None,                  Some Unlam_compile.compile;
       ["c"],         Some preprocess_cpp,   Some Fc_compile.compile;
       ["i"],         None,                  Some Fc_compile.compile;
       ["pas"],       None,                  Some (Pasqual_compile.compile true);
       ["psq"],       None,                  Some (Pasqual_compile.compile false);
       ["mc"],        None,                  Some Mc_compile.compile;
       ["fir"],       None,                  Some compile_fir;
       ["java"],      None,                  Some Fj_fir_fir.compile;
       ["pho"],       None,                  Some (Phobos_compile.compile (List.rev !rev_include));
       ["py"],        None,                  Some Python_compile.compile;
       ["o"; "mo"],   None,                  None]
   in
      if !aml_flag then
         (["ml"; "mli"], None,                  Some Aml_compile.compile) :: front_ends
      else
         (["ml"; "mli"], None,                  Some Naml_compile.compile) :: front_ends


(*
 * Find the frontend that's right for us!
 *)
let find_assoc_or_raise key list msg =
   if List.mem_assoc key list then
      List.assoc key list
   else
      raise (Failure msg)

let find_dynamic_front ext =
   let rec search path = function
      (section, options) :: tl ->
         (match List.mem_assoc frontend_ext_option options with
            true ->
               let ext_list = List.assoc frontend_ext_option options in
               if not (List.mem ext ext_list) then
                  search path tl
               else begin
                  let handler_list = find_assoc_or_raise 
                     frontend_handler_option options (lang_config ^ ": Missing frontend handler clause") 
                  in
                  if List.length handler_list > 1 then
                     raise (Failure (lang_config ^ ": Multiple language definitions for " ^ ext));
                  let handler = List.hd handler_list in
                  let target_list = find_assoc_or_raise 
                     frontend_target_option options (lang_config ^ ": Missing target")
                  in
                  if List.length target_list > 1 then
                     raise (Failure (lang_config ^ ": Multiple targets for " ^ ext));
                  let target = List.hd target_list in
                  (match String.lowercase (target) with
                     "fc" ->
                        Fir_state.compiled_grammar_filename := path ^ "/" ^ handler;
                        Fir_state.use_fc_ast := true;
                           None, Some (Phobos_compile.compile (List.rev !rev_include))
                   | "fir" ->
                        Fir_state.compiled_grammar_filename := path ^ "/" ^ handler;
                           None, Some (Phobos_compile.compile (List.rev !rev_include))
                  | _ ->
                        raise (Failure (lang_config ^ ": invalid target = " ^ target)))
               end
          | false ->
               search path tl)
    | [] ->
         raise (Failure ("Mcc_main.find_front:  Unrecognized file extension " ^ ext))
   in
      search Mcc_frontends.path Mcc_frontends.frontends

let find_front ext =
   let rec search = function
      (exts, preprocess, compile) :: tl ->
         if List.mem ext exts then
            preprocess, compile
         else
            search tl
    | [] ->
         find_dynamic_front ext
   in
      search (front_ends ())


(************************************************************************
 * COMPILING
 ************************************************************************)


(*
 * Print FIR.
 *)
let print_fir note prog =
   Format.printf "@[<v 2>*** FIR: %s@ %a@]@." note Fir_print.pp_print_prog prog


(*
 * Output FIR.
 *)
let output_fir_prog in_filename prog =
   let out_filename = replace_extension in_filename "fir" in
   let _ =
      if !debug_steps then
         Printf.eprintf "Printing FIR program %s ==> %s\n%t" in_filename out_filename flush
   in
   let out = open_out_bin out_filename in
   let prog = mprog_of_prog prog in
      ChannelMarshal.marshal_prog out prog;
      close_out out


(*
 * Output assembly to the (prenamed) file given.
 *)
let output_assembly_to_file out_filename prog =
   let out = open_out out_filename in
   let buf = formatter_of_out_channel out in
      try
         ArchCodegen.print_prog buf prog;
         close_out out
      with
         exn ->
            close_out out;
            raise exn

let output_assembly_to_file out_filename prog =
   Fir_state.profile "Mcc_main.output_assembly_to_file" (output_assembly_to_file out_filename) prog


(*
 * Output assembly to the default filename.  Used for emitting
 * assembly for debugging purposes (i.e. this isn't the terminal
 * file being written).
 *)
let output_assembly_to_default_file in_filename prog =
   let out_filename = replace_extension in_filename "s" in
      if !debug_steps then
         Printf.eprintf "Emitting default assembly for %s ==> %s\n%t" in_filename out_filename flush;
      output_assembly_to_file out_filename prog


(*
 * Output assembly to the final output name.  This is only
 * called when compiling with -S... note things get confused
 * if multiple source files are named.  This returns the
 * filename that was used.
 *)
let output_assembly in_filename prog =
   let out_filename =
      match !output_name with
         Some name ->
            name
       | None ->
            replace_extension in_filename "s"
   in
      if !debug_steps then
         Printf.eprintf "Emitting output assembly for %s ==> %s\n%t" in_filename out_filename flush;
      output_assembly_to_file out_filename prog;
      out_filename


(*
 * Output object code.  Called if we are at LEAST going to a final
 * object file.  Since multiple object files may need to be linked
 * together, we generate a temporary name here...  This returns
 * the name of the file that was used.
 *)
let output_object in_filename prog =
   let out_filename = temporary_file (replace_extension in_filename "mo") in
      if !debug_steps then
         Printf.eprintf "Emitting output object for %s ==> %s\n%t" in_filename out_filename flush;
      ArchCodegen.print_object out_filename prog;
      out_filename

let output_object in_filename prog =
   Fir_state.profile "Mcc_main.output_object" (output_object in_filename) prog


(*
 * Copy a file.
 *)
let copy_file src dst =
   let src = open_in_bin src in
   let dst = open_out_bin dst in
   let len = 0x1000 in
   let buf = String.create len in
   let rec io_loop () =
      let count = input src buf 0 len in
         if count = 0 then
            ()
         else begin
            output dst buf 0 count;
            io_loop ()
         end
   in
      io_loop ();
      close_in src;
      close_out dst


(*
 * Link the object code.  Returns the filename that we stored the
 * result in -- if -c is given, this should be the file named in
 * -o, otherwise it is a temporary file only.  Returns the name of
 * the emitted file.
 *)
let link_objects in_filenames =
   let name = "mcc.o" in
   let out_filename =
      if !stop_at_object_file then
         match !output_name with
            Some name ->
               name
          | None ->
               name
      else
         temporary_file name
   in
      if !debug_steps then begin
         Printf.eprintf "Linking to output object %s\n   Input objects:" out_filename;
         List.iter (fun in_filename -> Printf.eprintf " %s" in_filename) in_filenames;
         Printf.eprintf "\n%t" flush
      end;
      (match in_filenames with
         [] ->
            raise (Failure "No input files to link!")
       | [in_filename] ->
            copy_file in_filename out_filename
       | _ ->
            ArchCodegen.link_object out_filename in_filenames);
      out_filename


(*
 * Emit an executable based on the named object file.  If we made it
 * to this stage, then we must assume the file named in output_name
 * is the name of the final executable file...  Returns the name of
 * the executable program.
 *)
let emit_executable in_filename =
   (* Determine the name to use for the executable file *)
   let out_filename =
      match !output_name with
         Some name ->
            name
       | None ->
            "a.out"
   in

   (* Build the final link command *)
   let link_options = List.rev !rev_linker_options in
   let link_options = List.fold_left (fun s opt -> s ^ " " ^ (quote_option opt)) "" link_options in
   let link_options = link_options ^ " -o " ^ out_filename in
   let link_options = link_options ^ " " ^ in_filename ^ " " ^ runtime_library in
   let command = link_program ^ link_options in
      if !debug_steps then
         Printf.eprintf "Emitting executable from %s -> %s\n   Running %s\n%t" in_filename out_filename command flush;
      if Sys.command command <> 0 then
         raise (Failure "Mcc_main.emit_executable:  Final link failed")
      else
         out_filename


(*
 * To compile a file:
 *    1. First, use the front-end to compile the source
 *    2. Apply FIR optimizations
 *    3. Save the FIR file if desired
 *    4. Evaluate the FIR if desired (and exit after evaluation)
 *    5. Generate MIR
 *    6. Optimize MIR
 *    7. Produce assembly
 *)
let compile_front in_filename front =
   (* Produce fir *)
   let prog = front in_filename in
      if Fir_state.fir_safety_enabled () then
         Fir_assert.assert_prog prog
      else
         prog

let compile_middle in_filename prog =
   if !Fir_state.debug_print_fir >= 1 then
      print_fir "after front end" prog;

   (* MetaPRL processing. *)
   if !metaprl_print_fir then
      print_fir "Before MetaPRL" prog;

   let prog =
      if !metaprl_send then
         Mp_mc_compile.compile_mc_fir prog
      else
         prog
   in

   if !metaprl_print_fir then
      print_fir "After MetaPRL" prog;

   (* Optimize fir *)
   let prog = Fir_optimize.optimize prog in

   if !Fir_state.debug_print_fir >= 1 then
      print_fir "optimized" prog;

   (* Save fir *)
   if !output_fir then
      output_fir_prog in_filename prog;

   prog

let compile_back in_filename prog =
   (* Produce MIR code *)
   let prog = mprog_of_prog prog in
   let prog = ArchCodegen.compile_mir_of_fir prog in

   (* Generate assembly *)
   let prog = ArchCodegen.compile_asm_of_mir prog None in

   (* Emit assembly code for reference; useful mainly
      when using the internal assembler to build. *)
   let _ =
      if !print_to_asm_file then
         output_assembly_to_default_file in_filename prog
   in
      (* Emit the final output file *)
      if !stop_at_assembler then
         output_assembly in_filename prog
      else
         output_object in_filename prog


(*
 * Have to separate exception handlers.
 *)
let compile_file filename =
   let _, _, ext = extract_filename_components filename in
   let preproc, front =
      if !grammar_filename <> "" || !compiled_grammar_filename <> "" then
         None, Some (Phobos_compile.compile (List.rev !rev_include))
      else
         find_front ext
   in

   (* Run the preprocessor *)
   let preprocessed, filename =
      match preproc with
         Some preproc ->
            true, preproc filename
       | None ->
            false, filename
   in

   (* Run the compiler proper *)
   let compiled, filename =
      match !stop_at_preprocessor, front with
         true, _
       | _, None ->
            false, filename
       | false, Some front ->
            let prog = Fir_exn_print.catch (Fir_state.profile "Mcc_main.compile_front" (compile_front filename)) front in
            let prog = Fir_exn_print.catch (Fir_state.profile "Mcc_main.compile_middle" (compile_middle filename)) prog in
            let filename = ArchCodegen.catch (Fir_state.profile "Mcc_main.compile_back" (compile_back filename)) prog in
               true, filename
   in
   let _, _, ext = extract_filename_components filename in
      match ext with
         "o"
       | "mo" ->
            rev_linkfiles := filename :: !rev_linkfiles
       | _ ->
            ()


(*
 * Link the files.
 *)
let link () =
   (* Link the object only if we were supposed to make it this far *)
   if not (!stop_at_preprocessor || !stop_at_assembler) then
      link_objects (List.rev !rev_linkfiles)
   else
      ""


(*
 * Emit executable.
 *)
let exec linkfile =
   (* Emit executable only if we made it this far... *)
   if not (!stop_at_preprocessor || !stop_at_assembler || !stop_at_object_file || linkfile = "") then
      emit_executable linkfile
   else
      ""


(*
 * Remove the temporary files
 *)
let rec remove_temp_files = function
   [] ->
      ()
 | tempfile :: tempfiles ->
      if !debug_steps then
         Printf.eprintf "Removing temporary file %s\n%t" tempfile flush;
      if Sys.file_exists tempfile then
         Sys.remove tempfile;
      remove_temp_files tempfiles


(*
 * Compile all the files.
 *)
let compile () =
   let cleanup () =
      remove_temp_files !rev_tempfiles;
      if !Profile.profile_file_opened then
         close_out !Profile.profile_out
   in
      at_exit cleanup;

      (* Compile all source files *)
      List.iter compile_file (List.rev !rev_files);
      ArchCodegen.catch (fun () -> ignore (exec (link ()))) ();
      exit 0


(************************************************************************
 * ARGUMENT PARSING
 ************************************************************************)


(*
 * Arguments.
 *)
let usage = "MCC (Mojave Compiler) version " ^ version_string ^ "\n\n" ^
            "Usage: mcc [options] [files...] [-- <program arguments>]"

let spec =
  ["Basic Options",
     ["-o",                Mc_arg.String (fun s -> output_name := Some s),
                           "output name for assembly/executable code";
      "-I",                Mc_arg.String (fun s -> rev_include := s :: !rev_include),
                           "specify an additional include directory; -I<dirname>";
      "-D",                Mc_arg.String (fun s -> rev_defines := s :: !rev_defines),
                           "specify a preprocessor define; -D<var> or -D<var>=<val>";
      "-l",                Mc_arg.String (fun s -> rev_linker_options := ("-l" ^ s) :: !rev_linker_options),
                           "link against named library; -l<libname>";
      "-L",                Mc_arg.String (fun s -> rev_linker_options := ("-L" ^ s) :: !rev_linker_options),
                           "specify an additional library path; -L<dirname>";
      "-g",                Mc_arg.Set Fir_state.debug_stab,
                           "add debugging information to object files";
      "-fstub",            Mc_arg.Unit (fun () -> std_flags_set_bool "x86.as.make_stub" true),
                           "generate external stubs (required if you want to invoke" ^
                           " functions in C libraries, including libc)";
      "-warn-error",       Mc_arg.Set debug_warning_is_error,
                           "terminate on warnings as well as errors"];

   "Information on compiler/installation",
     ["-version",          Mc_arg.Unit display_version_info,
                           "display version information";
      "-print-paths",      Mc_arg.Unit (fun () -> print_mcc_paths mcc_paths),
                           "print all configured paths"];

   "Compiler Stages",
     ["-E",                Mc_arg.Set stop_at_preprocessor,
                           "preprocess input files only";
      "-S",                Mc_arg.Set stop_at_assembler,
                           "generate an assembly file only";
      "-c",                Mc_arg.Set stop_at_object_file,
                           "generate an object file only"];

   "General optimization flags",
     ["-O", Mc_arg.Int (fun level -> opt_set_level level),
                           "set optimization level -- level 0 disables optimizations," ^
                           " [1,3] enable standard optimizations, and" ^
                           " [4,9] enable experimental optimizations";
      "-disable-fir-opt",  Mc_arg.Unit (fun _ -> opt_set_fir false),
                           "disable all FIR optimizations";
      "-disable-mir-opt",  Mc_arg.Unit (fun _ -> opt_set_mir false),
                           "disable all MIR optimizations";
      "-disable-asm-opt",  Mc_arg.Unit (fun _ -> opt_set_asm false),
                           "disable all assembly optimizations"];

   "Compiler flags environment",
     ["-enable",           Mc_arg.String (fun flag -> std_flags_set_bool flag true),
                           "enable named flag (set flag to true)";
      "-disable",          Mc_arg.String (fun flag -> std_flags_set_bool flag false),
                           "disable named flag (set flag to false)";
      "-set",              Mc_arg.String (fun expr -> std_flags_set_expr expr),
                           "set named flag to indicated value; -set&nbsp;<name>=<value>.  You can" ^
                           " set multiple arguments using: -set&nbsp;<n1>=<v1>,<n2>=<v2>,...";
      "-print-flags",      Mc_arg.Unit (fun _ -> print_std_flag_defaults (); exit 1),
                           "display all settable flags, with their default values";
      "-print-state",      Mc_arg.Unit print_std_flag_values,
                           "display all currently set flags, with their current values (and continue)"];

   "Frontend language-specific flags",
     ["-grammar",          Mc_arg.String (fun s ->
                              grammar_filename := s),
                           "Phobos:  language description to use";
      "-cgrammar",         Mc_arg.String (fun s ->
                              compiled_grammar_filename := s),
                           "Phobos:  compiled language description to use";
      "-save-grammar",     Mc_arg.Set Fir_state.save_grammar,
                           "Phobos:  save compiled language description";
      "-debug-grammar",    Mc_arg.Set Fir_state.debug_grammar,
                           "Phobos:  enable Phobos debugging";
      "-use-fc",           Mc_arg.Set Fir_state.use_fc_ast,
                           "Phobos:  convert Phobos output to FC AST";
      "-debug-no-rewrites",Mc_arg.Set Fir_state.apply_no_rewrites,
                           "Phobos:  ignore all rewrites and return the parsed term";

      "-pasqual-add-boot", Mc_arg.Set Fir_state.pasqual_add_boot,
                           "Pasqual:  include default declarations";
      "-aml",              Mc_arg.Set aml_flag,
                           "AML:  use AML instead of NAML to compile .ml,.mli files";
      "-nopervasives",     Mc_arg.Set Aml_state.no_pervasives,
                           "AML:  Don't automatically include Pervasives module";
      "-check-ir",         Mc_arg.Set Fir_state.debug_check_ir,
                           "typecheck frontend intermediate code";
      "-eval-ir",          Mc_arg.Set Fir_state.debug_eval_ir,
                           "evaluate frontend intermediate code"];

   "Print IR/FIR stages",
     ["-print-parse",      Mc_arg.Set Fir_state.debug_print_parse,
                           "print source-level code";
      "-print-ast",        Mc_arg.Set Fir_state.debug_print_ast,
                           "print source-level code";
      "-print-tast",       Mc_arg.Set Fir_state.debug_print_tast,
                           "print typed source-level code";
      "-print-air",        Mc_arg.Set Fir_state.debug_print_air,
                           "print abstract intermediate code";
      "-print-ir",         Mc_arg.Set Fir_state.debug_print_ir,
                           "print intermediate code";
      "-print-fir",        Mc_arg.Unit (fun _ -> incr Fir_state.debug_print_fir),
                           "print fir code (use twice to print more stages)";
      "-print-no-poly",    Mc_arg.Set Fir_state.debug_print_no_poly,
                           "do not print polymorphic coercions in the fir";
      "-print-mir",        Mc_arg.Set Mir_state.debug_print_mir,
                           "print MIR code";
      "-print-final-fir",  Mc_arg.Set Mir_state.debug_print_final_fir,
                           "print final pass of FIR code only";
      "-print-final-mir",  Mc_arg.Set Mir_state.debug_print_final_mir,
                           "print final pass of MIR code only";
      "-print-asm",        Mc_arg.Set Fir_state.debug_print_asm,
                           "print assembly code"];

   "Output intermediate stages to file",
     ["-F",                Mc_arg.Set output_fir,
                           "output binary fir to a file";
      "-print-asm-to-file",Mc_arg.Set print_to_asm_file,
                           "print GNU assembly to file"];

   "General debugging flags",
     ["-debug",            Mc_arg.String Debug.set_debug_flags,
                           "general debug flags: use -debug -help for help";
      "-debug-steps",      Mc_arg.Set debug_steps,
                           "debug major compiler steps (main function debugging)";
      "-debug-symbol",     Mc_arg.Set Symbol.debug_symbol,
                           "extra symbol debugging";
      "-debug-pos",        Mc_arg.Set Fir_state.debug_pos,
                           "print position information in exceptions";
      "-trace-pos",        Mc_arg.Set Fir_state.debug_trace_pos,
                           "trace function calls";
      "-debug-profile",    Mc_arg.Set Fir_state.debug_profile,
                           "enable profiling of the compiler";
      "-profile",          Mc_arg.String (fun s ->
                              Profile.profile_file := s;
                              Fir_state.debug_profile := true),
                           "save profile information to a file";
      "-preserve-temps",   Mc_arg.Set preserve_temps,
                           "preserve compiler-generated temporary files" ^
                           " (use with -debug_steps to see all temporary files generated)";
      "-debug-loop",       Mc_arg.Set Loop.debug_loop,
                           "FIR:  enable dominator debugging (verbose)";
      "-debug-dead",       Mc_arg.Set Fir_state.debug_dead,
                           "debug FIR dead code elimination";
      "-debug-infer",      Mc_arg.Set Fir_state.debug_infer,
                           "debug FIR type inference";
      "-debug-unify",      Mc_arg.Set Fir_state.debug_unify,
                           "debug FIR type unification";
      "-debug-fir-loop",   Mc_arg.Set Fir_state.debug_loop,
                           "debug FIR loop analysis";
      "-debug-alias",      Mc_arg.Set Fir_state.debug_alias,
                           "debug FIR alias analysis";
      "-debug-pre",        Mc_arg.Set Fir_state.debug_pre,
                           "debug FIR PRE optimization";
      "-debug-fir-ty-closure", Mc_arg.Set Fir_state.debug_fir_ty_closure,
                           "debug FIR type closure conversion";
      "-debug-fir-closure", Mc_arg.Set Fir_state.debug_fir_closure,
                           "debug FIR closure conversion";
      "-debug-phobos",     Mc_arg.Set Fir_state.debug_phobos,
                           "debug Phobos";
      "-error-file",       Mc_arg.Unit (fun () -> ()),
                           "ignored (used by test scripts only)";
      "-test-profile",     Mc_arg.String (fun _ -> ()),
                           "ignored (used by test scripts only)"];

   "Register allocator debugging options",
     ["-debug-ra",         Mc_arg.Unit (fun _ -> incr Ra_state.debug_regalloc),
                           "debug the register allocator";
      "-debug-live",       Mc_arg.Set Live.debug_live,
                           "debug liveness analysis"];

   "Back-end debugging options",
     ["-debug-asm-code",   Mc_arg.Set Mir_fir.debug_code,
                           "include assembly debug print code";
      "-mir-comments-fir", Mc_arg.Set Mir_print.mir_comments_fir,
                           "print FIR comments in MIR output";
      "-asm-comments-fir", Mc_arg.Unit (fun () -> std_flags_set_bool "x86.state.comments.fir" true),
                           "print FIR comments in assembly code";
      "-asm-comments-mir", Mc_arg.Unit (fun () -> std_flags_set_bool "x86.state.comments.mir" true),
                           "print MIR comments in assembly code";
      "-asm-comments-text",Mc_arg.Unit (fun () -> std_flags_set_bool "x86.state.comments.text" true),
                           "print miscellaneous text comments in assembly code";
      "-asm-comments-all", Mc_arg.Unit (fun () ->
                              std_flags_set_bool "x86.state.comments.fir"  true;
                              std_flags_set_bool "x86.state.comments.mir"  true;
                              std_flags_set_bool "x86.state.comments.text" true),
                           "print all comments in assembly code"];

   "Debugging flags to test object generation",
     ["-debug-gas",        Mc_arg.Set Bfd_state.debug_gas,
                           "try to generate object code just like gas (suboptimal)";
      "-debug-bfd",        Mc_arg.Set Bfd_state.debug_bfd,
                           "general BFD debugging";
      "-debug-bfd-reloc",  Mc_arg.Set Bfd_state.debug_bfd_reloc,
                           "debug BFD relocation";
      "-debug-bfd-choice", Mc_arg.Set Bfd_state.debug_bfd_choice,
                           "debug BFD instruction length choices";
      "-debug-as",         Mc_arg.Unit (fun () -> std_flags_set_bool "x86.as.debug" true),
                           "debug the assembler";
      "-Wstub",            Mc_arg.Unit (fun () -> std_flags_set_bool "x86.as.debug_stub" true),
                           "warn about generated external stubs";
      "-fspillmmx",        Mc_arg.Unit (fun () -> std_flags_set_bool "x86.debug.spill_mmx" true),
                           "use MMX registers for spills"];

   "Runtime safety checks",
     ["-safe-fir",         Mc_arg.Unit (fun () ->
                              std_flags_set_bool fir_safety_name true;
                              std_flags_set_bool mir_safety_name false),
                           "generate safety checks in the FIR" ^
                           (if Fir_state.fir_safety_enabled () then " (default)" else "");
      "-safe-mir",         Mc_arg.Unit (fun () ->
                              std_flags_set_bool fir_safety_name false;
                              std_flags_set_bool mir_safety_name true),
                           "generate safety checks in the MIR" ^
                           (if Fir_state.mir_safety_enabled () then " (default)" else "");
      "-unsafe",           Mc_arg.Unit (fun () ->
                              std_flags_set_bool fir_safety_name false;
                              std_flags_set_bool mir_safety_name false),
                           "disable all safety checks (not safe!)" ^
                           (if Fir_state.safety_enabled () then "" else " (default)");
      "-mir-strict-poly",  Mc_arg.Unit (fun () -> std_flags_set_bool mir_strict_poly_name true),
                           "enforce explicit polymorphic coercions on function arguments" ^
                           (if Fir_state.strict_poly () then " (default)" else "");
      "-mir-weak-poly",    Mc_arg.Unit (fun () -> std_flags_set_bool mir_strict_poly_name false),
                           "allow implicit poly coercions on function arguments (not safe!)" ^
                           (if not (Fir_state.strict_poly ()) then " (default)" else "");
      "-mir-safe-functions",  Mc_arg.Unit (fun () -> std_flags_set_bool mir_safe_functions_name true),
                           "check for arity match when calling function pointer" ^
                           (if Fir_state.safe_functions () then " (default)" else "");
      "-mir-unsafe-functions",Mc_arg.Unit (fun () -> std_flags_set_bool mir_safe_functions_name false),
                           "do not check for arity match on function pointer (not safe!)" ^
                           (if not (Fir_state.safe_functions ()) then " (default)" else "")];

   "MetaPRL processing",
     ["-metaprl-send",     Mc_arg.Unit (fun _ -> metaprl_send := true),
                           "send FIR to MetaPRL for processing";
      "-metaprl-debug",    Mc_arg.Unit (fun _ -> Mp_mc_base.debug := true),
                           "enable debugging of MetaPRL processing";
      "-metaprl-print-fir",Mc_arg.Unit (fun _ -> metaprl_print_fir := true),
                           "print FIR before and after MetaPRL processing"];

   "Evaluator arguments",
     ["--",                Mc_arg.Rest (fun s -> rev_argv := s :: !rev_argv),
                           "arguments to pass to an evaluator"]]


(*
 * Run the compiler.
 *)
let compile () =
   Sys.catch_break true;
   Mc_arg.parse spec collect_file usage;
   compile ()


(*
 * Run the compiler, and print uncaught exceptions.
 *)
let catch () =
   try compile () with
      exn ->
         let _ =
            match exn with
               Sys.Break ->
                  Printf.eprintf "MCC error (compiler interrupted)\n"
             | Failure msg ->
                  Printf.eprintf "MCC error (failure):  %s\n" msg
             | Invalid_argument msg ->
                  Printf.eprintf "MCC error (invalid argument):  %s\n" msg
             | _ ->
                  Printf.eprintf "MCC error (uncaught exception):   %s\n" (Printexc.to_string exn)
         in
            flush stderr;
            exit 1


(*
 * If the OCAMLRUNPARAM is set, then
 * don't catch generic exceptions so that the
 * OCaml runtime can print a backtrace.  Don't
 * get rid of this; we have been really careful
 * not to catch Failure exceptions exactly for
 * this reason.
 *)
let dont_catch_exceptions =
   try
      let _ = Sys.getenv "OCAMLRUNPARAM" in
         true
   with
      Not_found ->
         false


(*
 * Now, finally we can run the entire compiler.
 *)
let _ =
   if dont_catch_exceptions then
      compile ()
   else
      catch ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
