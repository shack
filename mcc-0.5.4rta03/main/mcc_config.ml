(* Configuration for MC *)
(* Automatically built by configure - DO NOT EDIT BY HAND *)


(* Version information *)
let version_string = "0.5.4rta03"
let metaprl_enabled= false
let host_info      = "i686-pc-linux-gnu"


(* Install paths *)
let mcc_prefix     = "/home/weel/opt"
let mcc_bindir     = "/home/weel/opt/bin"
let mcc_libdir     = "/home/weel/opt/lib/mcc"
let mcc_includedir = "/home/weel/opt/include/mcc"


(* Paths to compiler and preprocessor *)
let path_to_cpp    = "gcc -E"
let path_to_gcc    = "gcc"


(* Backend to compile with *)
let backend_name   = "x86"
module ArchFrame   = X86_backend.Frame
module ArchBackend = X86_backend.Backend
module ArchCodegen = X86_codegen.Codegen
