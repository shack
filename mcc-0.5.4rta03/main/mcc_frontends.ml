(*
 * Manage dynamic Phobos front-ends.
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * @email{granicz@cs.caltech.edu}
 * @end[license]
 *)

open Mcc_paths
open Mcc_config

(*
 * Name of the configuration file.
 *)
let config_file = "/lang.conf"

(*
 * Return path to the languages directory.
 *)
let path =
   let paths = get_mcc_paths () in
      paths.path_languages

(*
 * Parse configuration file.
 *)
let frontends =
   if metaprl_enabled then
      try
         let inx = open_in (path ^ config_file) in
         let lex = Lexing.from_channel inx in
            Mcc_frontends_parser.main Mcc_frontends_lexer.main lex
      with
         Not_found ->
            []
       | Parsing.Parse_error ->
            print_string "warning ";
            print_string (Mcc_frontends_lexer.string_of_pos (Mcc_frontends_lexer.current_position ()));
            print_string ": error in language configuration file, content ignored.\n";
            []
   else
      []
