(*
 * Locate the root install for the compiler
 *
 * ----------------------------------------------------------------
 *
 * @begin[license]
 * Copyright (C) 2002 Justin David Smith, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Jason Hickey
 * @email{jyh@cs.caltech.edu}
 * @end[license]
 *)


open Mcc_config


(*
 * Configuration structure.  This structure contains the path names
 * to various components of the MC system, as well as the names of a
 * few external programs for the system.
 *)
type mcc_paths = 
   { path_compiler         :  string;  (* Path to the main compiler *)
     path_languages        :  string;  (* Path to dynamic front-ends *)
     path_include_opt      :  string;  (* Paths to the include dir 1 *)
     path_runtime          :  string;  (* Path to the runtime lib *)
     path_cpp              :  string;  (* Path to the C preprocessor *)
     path_gcc              :  string;  (* Path to the GNU C compiler *)
   }


(*
 * Name of this compiler.  DO NOT REMOVE THE PRECEDING SLASHES,
 * OTHERWISE YOU *WILL* BREAK THE INSTALL TARGETS!  -JDS
 *)
let compiler_name = "/mcc"
let languages_dir = "/languages"

(*
 * Name of the standard include directory
 *)
let include_mojave = "/arch/" ^ backend_name ^ "/include/mojave"
let include_shadow = "/arch/" ^ backend_name ^ "/include/shadow"


(*
 * Name of the standard library directory
 *)
let runtime = "/arch/" ^ backend_name ^ "/runtime/" ^ backend_name ^ "runtime.a"
  
  
(*
 * Try to find out where the hell we're installed.
 *)
let get_mcc_paths () =
   (* Get a list of uninstalled paths to search in *)
   let uninstalled_search_paths = ["../../../../"; "../../../"; "../../"; "../"; "./"] in

   (* Search for our installation, using main name as a guideline *)
   let rec find_compiler = function
      path :: paths ->
         (* Checking uninstalled paths takes priority *)
         let main_path = path ^ "main/" in
            if Sys.file_exists (main_path ^ compiler_name) then
            false, main_path, path, path
         else
            find_compiler paths
    | [] ->
         (* Final guess: check the installed path *)
         let installed_compiler = Mcc_config.mcc_bindir ^ compiler_name in
            if Sys.file_exists installed_compiler then
               true, Mcc_config.mcc_bindir, Mcc_config.mcc_libdir, Mcc_config.mcc_includedir
            else
               raise (Failure ("Mcc_paths.get_mcc_paths:  Cannot find installed/uninstalled copy of compiler! " ^
                               "Cannot find " ^ installed_compiler))
   in
   
   (* Find out if we are installed/uninstalled, and get base path *)
   let installed, bindir, libdir, incdir = find_compiler uninstalled_search_paths in
   let languages_dir =
      if installed then
         libdir ^ languages_dir
      else
         bindir ^ languages_dir
   in
      { path_compiler         =  bindir ^ compiler_name;
        path_languages        =  languages_dir;
        path_include_opt      =  "-I" ^ incdir ^ include_mojave ^ " " ^
                                 "-I" ^ incdir ^ include_shadow;
        path_runtime          =  libdir ^ runtime;
        path_cpp              =  Mcc_config.path_to_cpp;
        path_gcc              =  Mcc_config.path_to_gcc;
      }


(*
 * Print out the current MC paths
 *)
let print_mcc_paths paths =
   let { path_compiler        =  p_compiler;
         path_languages       =  p_languages;
         path_include_opt     =  p_include;
         path_runtime         =  p_runtime;
         path_cpp             =  p_cpp;
         path_gcc             =  p_gcc;
       } = paths
   in
      Printf.printf "Path to compiler:         %s\n" p_compiler;
      Printf.printf "Path to dynamic language: %s\n" p_languages;
      Printf.printf "Path to include dirs:     %s\n" p_include;
      Printf.printf "Path to runtime lib:      %s\n" p_runtime;
      Printf.printf "Path to C preprocessor:   %s\n" p_cpp;
      Printf.printf "Path to GNU C compiler:   %s\n" p_gcc;
      exit 1
