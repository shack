(*
 *
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

open Prof_util

(*
 * Arguments.
 *)
let usage =
   Printf.sprintf "usage: prof -f descr_file -i filenames"

let descr_filename = ref ""
let input_files = ref []

let collect_file name =
   input_files := !input_files @ [name]

let process () =
   if !descr_filename = "" then
      begin
         Printf.printf "no description file.\n";
         exit 1;
      end;
   let expr_list = parse_prof !descr_filename in
   if List.length !input_files = 0 then
      begin
         Printf.printf "no input files.\n";
         exit 1;
      end;
   (* For now, we only look at one sample file *)
   let env = load_sample_file (List.hd !input_files) in
   let env = eval_expr_list env expr_list in
      Printf.printf "\n"

(*
 * Call the main program.
 *)
let spec =
   [
    "-f", Arg.String (fun s -> descr_filename := s), "specify what to do";
    "-i", Arg.String (fun s -> input_files := !input_files @ [s]), "input files"
   ]

let _ =
   Sys.catch_break true;
   Arg.parse spec collect_file usage;
   process ()

(*!
 * @docoff
 *
 * -*-
 * Local Variables:
 * Caml-master: "compile"
 * End:
 * -*-
 *)
