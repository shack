(*
 * Parsing state.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

open Prof_type

val tabstop : int

val set_current_position : pos -> unit
val current_position : unit -> pos
val current_file : unit -> string

val string_of_pos : pos -> string 
val print_pos : pos -> unit

val prof_current_position : unit -> pos
val prof_current_file : unit -> string
val set_prof_position : pos -> unit

val union_pos : pos -> pos -> pos
