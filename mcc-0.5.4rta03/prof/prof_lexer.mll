(*
 * Lexer for Phobos.
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

{
open Prof_parser
open Prof_parse_state
open Prof_type

(*
 * File position.
 *)
let current_line = ref 1
let current_schar = ref 0

(*
 * Advance a line.
 *)
let set_next_line lexbuf =
   incr current_line;
   current_schar := Lexing.lexeme_end lexbuf

(*
 * Get the position of the current lexeme.
 * We assume it is all on one line.
 *)
let set_lexeme_position lexbuf =
   let line = !current_line in
   let schar = Lexing.lexeme_start lexbuf - !current_schar in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let file = current_file () in
   let pos = file, line, schar, line, echar in
      set_current_position pos;
      pos

(*
 * Provide a buffer for building strings.
 *)
let stringbuf = Buffer.create 32
let string_start = ref (0, 0)

let string_add_char c =
   Buffer.add_string stringbuf 

let pop_string lexbuf =
   let s = Buffer.contents stringbuf in
   let sline, schar = !string_start in
   let eline = !current_line in
   let echar = Lexing.lexeme_end lexbuf - !current_schar in
   let pos = current_file (), sline, schar, eline, echar in
      Buffer.clear stringbuf;
      s, pos

let set_string_start lexbuf =
   string_start := !current_line, Lexing.lexeme_start lexbuf - !current_schar
}

(*
 * Regular expressions.
 *)
let name_prefix = ['_' 'A'-'Z' 'a'-'z']
let name_suffix = ['_' '.' 'A'-'Z' 'a'-'z' '0'-'9']
let name = name_prefix name_suffix*

let digit= ['0'-'9']
let decimal	= ['1'-'9'] digit*
let float = decimal '.' decimal

let char_normal = [' '-'\255']

(*
 * Main lexer.
 *)
rule main = parse 
     [' ' '\t']+        { main lexbuf }
   | '\n'               { set_next_line lexbuf; main lexbuf }
   | decimal            { let pos = set_lexeme_position lexbuf in
                           TokInt (int_of_string (Lexing.lexeme lexbuf), pos)
                        }
   | float              { let pos = set_lexeme_position lexbuf in
                           TokFloat (float_of_string (Lexing.lexeme lexbuf), pos)
                        }
   | name               { let pos = set_lexeme_position lexbuf in
                          let old_id = Lexing.lexeme lexbuf in
                           match (String.lowercase old_id) with
                                "output" ->
                                 TokOutput pos
                              | "percentages" ->
                                 TokPercentages pos
                              | "print" ->
                                 TokPrint pos
                              | _ ->
                                 TokId ((Symbol.add old_id), pos)
                        }

   | "//" [^ '\n']* '\n'{ set_next_line lexbuf; main lexbuf }
   | "(*"               { comment lexbuf; main lexbuf }
   | "\""               { set_string_start lexbuf;
                          string lexbuf;
                          let s, pos = pop_string lexbuf in
                           TokString(s, pos)
                        }
   (*
    * Special chars.
    *)
   | "("                { let pos = set_lexeme_position lexbuf in TokLeftParen pos }
   | ")"                { let pos = set_lexeme_position lexbuf in TokRightParen pos }
   | "["                { let pos = set_lexeme_position lexbuf in TokLeftBrack pos }
   | "]"                { let pos = set_lexeme_position lexbuf in TokRightBrack pos }
   | "+"                { let pos = set_lexeme_position lexbuf in TokPlus pos }
   | "-"                { let pos = set_lexeme_position lexbuf in TokMinus pos }
   | "/"                { let pos = set_lexeme_position lexbuf in TokDiv pos }
   | "*"                { let pos = set_lexeme_position lexbuf in TokMult pos }
   | ","                { let pos = set_lexeme_position lexbuf in TokComma pos }
   | ";"                { let pos = set_lexeme_position lexbuf in TokSemi pos }

   | eof                { TokEof }
   | _                  { let pos = set_lexeme_position lexbuf in
                          raise (Prof_type.ParseError (pos, Printf.sprintf "illegal char: '%s'"
                          (String.escaped (Lexing.lexeme lexbuf))))
                        }

and string = parse 
     "\"" | eof         { () }
   | "\\\""	            { Buffer.add_string stringbuf "\\\""; string lexbuf }
   | "\\n"              { Buffer.add_string stringbuf "\n"; string lexbuf }
   | '\n'               { set_next_line lexbuf; Buffer.add_char stringbuf '\n'; string lexbuf }
   | _                  { Buffer.add_string stringbuf (Lexing.lexeme lexbuf);
                          string lexbuf
                        }

and comment = parse 
     "(*"               { comment lexbuf; comment lexbuf }
   | "*)"               { () }
   | eof                { () }
   | '\n'               { set_next_line lexbuf;
                          comment lexbuf
                        }
   | _                  { comment lexbuf }

