(*
 * Prof utilities
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2002 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

open Symbol
open Prof_type
open Prof_parse_state

let env_lookup env pos v =
   try
      SymbolTable.find env v
   with
      Not_found ->
         print_pos pos;
         Printf.printf " fatal: %s not found\n" (Symbol.to_string v);
         exit 1

let env_empty = SymbolTable.empty
let env_add = SymbolTable.add

let rec print_value = function
     ValElement f ->
      Printf.printf " %f" f
   | ValList lst ->
      print_value_list lst
   | ValPercentage f ->
      Printf.printf " %.2f%%" f
   | ValString s ->
      Printf.printf "%s" s

and print_value_list = function
     head :: rest ->
      print_value head;
      print_value_list rest
   | [] ->
      ()

let calc_average pos = function
     ValList vl ->
      let total = List.fold_left (fun total vl ->
         match vl with
              ValElement f
            | ValPercentage f ->
               total +. f
            | ValString _
            | ValList _ ->
               raise (Invalid_argument "calc_average")) 0.0 vl
      in
         ValElement (total /. float_of_int (List.length vl))
   | _ ->
      raise (Invalid_argument "calc_average: not a list value")

let calc_percentages pos = function
     ValList vlist ->
      let total, lst = List.fold_left (fun (total, lst) vl ->
         match vl with
              ValElement f 
            | ValPercentage f ->
               total +. f, lst @ [f]
            | ValString _
            | ValList _ ->
               raise (Invalid_argument "calc_percentages")) (0.0, []) vlist
      in
         List.map (fun f ->
            ValPercentage (f /. total *. 100.0)) lst
   | _ ->
      raise (Invalid_argument "calc_percentages: not a list value")
   
let values_op v1 v2 op =
   match v1, v2 with
        ValElement f1, ValElement f2 ->
         ValElement (op f1 f2)
      | _ ->
         raise (Invalid_argument "values_op")

let rec eval_single_expr env e =
   match e with
        VarExpr (v, pos) ->
         env, ValElement (env_lookup env pos v)
      | NumExpr (f, pos) ->
         env, ValElement f
      | PlusExpr (e1, e2, pos) ->
         let env, v1 = eval_single_expr env e1 in
         let env, v2 = eval_single_expr env e2 in
            env, values_op v1 v2 (+.)
      | MinusExpr (e1, e2, pos) ->
         let env, v1 = eval_single_expr env e1 in
         let env, v2 = eval_single_expr env e2 in
            env, values_op v1 v2 (-.)
      | DivExpr (e1, e2, pos) ->
         let env, v1 = eval_single_expr env e1 in
         let env, v2 = eval_single_expr env e2 in
            env, values_op v1 v2 (/.)
      | MultExpr (e1, e2, pos) ->
         let env, v1 = eval_single_expr env e1 in
         let env, v2 = eval_single_expr env e2 in
            env, values_op v1 v2 ( *.)
      | Average (el, pos) ->
         let env, fl = eval_multi_expr env el in
            env, calc_average pos fl
      | PrintExpr (s, pos) ->
            env, ValString s

and eval_multi_expr env e =
   match e with
        Percentages (el, pos) ->
         let env, fl = eval_multi_expr env el in
            env, ValList (calc_percentages pos fl)
      | ListExpr (el, pos) ->
         let env, fl = eval_single_expr_list env el in
            env, ValList fl

and eval_single_expr_list env el =
   let env, val_lst = List.fold_left (fun (env, val_lst) item ->
      let env, vl = eval_single_expr env item in
         env, val_lst @ [vl]) (env, []) el
   in
      env, val_lst

let rec eval_expr env e =
   match e with
        Single e ->
         eval_single_expr env e
      | Multi e ->
         eval_multi_expr env e

and eval_expr_list env el =
   List.fold_left (fun env expr ->
      let env, vl = eval_expr env expr in
         print_value vl;
         env) env el

(*
 * Profile files.
 *)
let rec trim_left s count =
   if s.[count] = ' ' then
      trim_left s (count + 1)
   else
      count

let rec trim_right s count =
   if s.[count] = ' ' then
      trim_right s (count - 1)
   else
      count

let trim s =
   let left = trim_left s 0 in
   let right = trim_left s (String.length s - 1) in
   let s = String.sub s left (right - left) in
      s

let rec read_all_entries env inx =
   try
      let s = trim (input_line inx) in
      let p =
         try
            String.index s '#'
         with
            Not_found ->
               raise (Invalid_argument "Invalid input file")
      in
      let name = String.sub s 0 (p-1) in
      let vl = String.sub s (p+1) (String.length s - (p+1)) in
      let vl = float_of_string vl in
      let env = env_add env (Symbol.add name) vl in
         read_all_entries env inx
   with
      End_of_file ->
         env

let load_sample_file name =
   let inx = open_in name in
   let env = read_all_entries env_empty inx in
      env

(*
 * Parse .pro files.
 *)
let parse_prof name =
   let inx = open_in name in
   let pos = name, 0, 0, 0, 0 in
   let expr_list =
      set_current_position pos;
      try
         let lex = Lexing.from_channel inx in
            Prof_parser.main Prof_lexer.main lex
      with
           Prof_type.ParseError (pos, s) ->
            Printf.printf "Prof error: %s\n" s;
            exit 1
         | Invalid_argument s ->
            Printf.printf "Error: %s\n" s;
            exit 1
         | Parsing.Parse_error ->
            Printf.printf "Parse error: ";
            print_pos (current_position ());
            Printf.printf "\n";
            exit 1
         | _ ->
            Printf.printf "Uncaught exception.\n";
            exit 1
   in
      close_in inx;
      expr_list
