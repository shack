(*
 * ----------------------------------------------------------------
 *
 * Copyright (C) 2001 Adam Granicz, Caltech
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Author: Adam Granicz
 * Email: granicz@cs.caltech.edu
 *)

open Symbol

(*
 * Parse position.
 *)
type pos = string * int * int * int * int

(*
 * Parse errors.
 *)
exception ParseError of pos * string

type expr_single =
     Average of expr_multi * pos
   | NumExpr of float * pos
   | VarExpr of symbol * pos
   | PlusExpr of expr_single * expr_single * pos
   | MinusExpr of expr_single * expr_single * pos
   | DivExpr of expr_single * expr_single * pos
   | MultExpr of expr_single * expr_single * pos
   | PrintExpr of string * pos

and expr_multi =
     Percentages of expr_multi * pos
   | ListExpr of expr_single list * pos

type expr =
     Single of expr_single
   | Multi of expr_multi

type eval_value =
     ValElement of float
   | ValList of eval_value list
   | ValPercentage of float
   | ValString of string

type env = float SymbolTable.t
