Summary: The Mojave compiler.
Name: mcc
Version: 0.5.0
Release: 1
URL: http://mojave.cs.caltech.edu
Source0: %{name}-%{version}.tar.gz
License: GPL
Group: Development/Tools
BuildRoot: %{_tmppath}/%{name}-root

%description

The Mojave compiler is a multi-language compiler that currently
supports the C, ML, Java, and Pascal programming languages.  It also
supports process migration and transactions.

%prep
%setup -q

%build
./configure --prefix=/usr/bin
cons

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/lib/mcc/arch/x86/runtime
mkdir -p $RPM_BUILD_ROOT/usr/lib/mcc/arch/x86/include

install -m 755 main/mcc $RPM_BUILD_ROOT/usr/bin
install -m 644 arch/x86/runtime/x86runtime.a $RPM_BUILD_ROOT/usr/lib/mcc/arch/x86/runtime
cp -r arch/x86/include $RPM_BUILD_ROOT/usr/lib/mcc/arch/x86/include/.

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)

/usr/bin/mcc

%changelog
* Tue May  7 2002 Jason Hickey <jyh>
- Initial build.
