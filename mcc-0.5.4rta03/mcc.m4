dnl Support functions for configure script
dnl Copyright(c) 2002 Justin David Smith
dnl


dnl MCC_CHECK_LIBBFD([ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND]])
dnl   Check to see if the BFD library exists and can be linked against.
dnl   The appropriate action is run based on the result; in addition,
dnl   LIBBFD_LIB is set to the directory containing libbfd.{a,so}, and
dnl   LIBBFD_INC is set to the include path for bfd header files.  If
dnl   no luck, or default path is sufficient, both vars are left empty.
dnl   Set LIBBFD_LIB_RESULT and LIBBFD_INC_RESULT to text messages that
dnl   indicate the final status.
dnl
dnl   If successful, LIBERTY_CCFLAGS is set to the ML flags necessary to
dnl   link in -liberty, if it is required for the system; LIBERTY_CFLAGS
dnl   is set to the ld flags necessary to link in -liberty.
dnl
AC_DEFUN(MCC_CHECK_LIBBFD, [
   dnl Check for --with-libbfd=<path>
   dnl Allows user to specify exactly where libbfd is.
   dnl
   AC_MSG_CHECKING([for --with-libbfd flag])
   AC_ARG_WITH(libbfd, [  --with-libbfd=<path>    Specify path of directory containing libbfd [auto]], , with_libbfd=no)

   dnl Check the result.
   dnl
   if test "x$with_libbfd" = "xno" -o "x$with_libbfd" = "xyes" -o "x$with_libbfd" = "xauto"; then
      dnl Hope we find it in the default path...
      AC_MSG_RESULT([trying default library path])
      LIBBFD_LIB=""
      LIBBFD_INC=""
      LIBBFD_LIB_RESULT="using default"
      LIBBFD_INC_RESULT="using default"
      libbfd_try_lib_options=""
      libbfd_try_inc_options=""
   else
      dnl User gave us a path to try...
      AC_MSG_RESULT([trying $with_libbfd])
      LIBBFD_LIB="$with_libbfd/lib"
      LIBBFD_INC="$with_libbfd/include"
      LIBBFD_LIB_RESULT="$LIBBFD_LIB"
      LIBBFD_INC_RESULT="$LIBBFD_INC"
      libbfd_try_lib_options="-L$LIBBFD_LIB"
      libbfd_try_inc_options="-I$LIBBFD_INC"
   fi

   dnl Check to see if the libiberty functions are in a separate library or in libbfd
   AC_CHECK_LIB(iberty, xstrerror, 
      [LIBERTY_CCFLAGS="-cclib -liberty"; LIBERTY_CFLAGS="-liberty"],
      [AC_CHECK_LIB(bfd, libiberty_concat_ptr, 
         [LIBERTY_CCFLAGS=""; LIBERTY_CFLAGS=""], 
         [AC_MSG_ERROR(Unable to locate libiberty, please check your binutils installation.)]
      )]
   )

   dnl Check for libbfd library.  We check a number of functions
   dnl that are actually invoked by MCC, to make sure we have a
   dnl new enough version of libbfd.
   dnl
   libbfd_okay=1
   old_libs="$LIBS"
   old_ldflags="$LDFLAGS"
   LDFLAGS="$LDFLAGS $libbfd_try_lib_options"
   AC_CHECK_LIB(bfd, bfd_get_error,             , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_errmsg,                , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_set_format,            , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_set_section_size,      , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_set_section_flags,     , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_set_section_contents,  , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_set_symtab,            , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_reloc_type_lookup,     , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_set_reloc,             , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_openr,                 , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_check_format,          , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_map_over_sections,     , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   AC_CHECK_LIB(bfd, bfd_close,                 , [libbfd_okay=0], [$LIBERTY_CFLAGS])
   LIBS="$old_libs"
   LDFLAGS="$old_ldflags"

   dnl Check for bfd.h header file.
   dnl
   old_cppflags="$CPPFLAGS"
   CPPFLAGS="$CPPFLAGS $libbfd_try_inc_options"
   AC_CHECK_HEADER(bfd.h, , [libbfd_okay=0])
   CPPFLAGS="$old_cppflags"

   dnl Enable substs on the resulting path
   dnl
   AC_SUBST(LIBBFD_LIB)
   AC_SUBST(LIBBFD_INC)

   dnl Run final actions
   dnl
   if test "x$libbfd_okay" = "x0"; then
      dnl We failed.
      LIBBFD_LIB=""
      LIBBFD_INC=""
      LIBBFD_LIB_RESULT="not found"
      LIBBFD_INC_RESULT="not found"
      ifelse([$2], , :, [$2])
   else
      ifelse([$1], , :, [$1])
   fi
])


dnl MCC_CHECK_METAPRL([ACTION_IF_FOUND [, ACTION_IF_NOT_FOUND]])
dnl   Check to see if MetaPRL is requested.  If it is requested and has
dnl   been configured, then we will setup MP_PATH to point to the base
dnl   of the MetaPRL install, and will set MP_ENABLED=true.  Otherwise,
dnl   we'll set MP_ENABLED=false.  Both variables can be subst'd, and
dnl   for convenience the variable MP_PATH_RESULT will be set to a text
dnl   message indicating whether MetaPRL support is enabled.
dnl
dnl   The user can choose whether to build all of MetaPRL, or just the
dnl   parts required for MCC.  MP_FULL will be set to true/false based
dnl   on their choice.
dnl
dnl   Configure checks for OCaml must be done before MetaPRL checks.
dnl
AC_DEFUN(MCC_CHECK_METAPRL, [
   dnl Check for --with-metaprl=<path>
   dnl Allows us to build with MetaPRL support.
   dnl
   AC_MSG_CHECKING([for --with-metaprl flag])
   AC_ARG_WITH(metaprl, [  --with-metaprl=<path>   Enable MetaPRL with source path [no]], , with_metaprl=no)

   dnl Check the result.
   dnl
   if test "x$with_metaprl" = "xno"; then
      dnl MetaPRL support will be disabled.
      MP_PATH=""
      MP_PATH_RESULT="disabled"
      MP_ENABLED=false
      AC_MSG_RESULT([$MP_PATH_RESULT])
   elif test "x$with_metaprl" = "xyes" -o "x$with_metaprl" = "xauto"; then
      dnl User requested MetaPRL but did not give a path.
      dnl We'll try the default location (which is ok for most of us)
      MP_PATH="../meta-prl/"
      MP_PATH_RESULT="using $MP_PATH"
      MP_ENABLED=true
      AC_MSG_RESULT([no path specified, assuming $MP_PATH])
   else
      dnl MetaPRL support requested; make sure it is valid.
      MP_PATH="$with_metaprl"
      MP_PATH_RESULT="using $MP_PATH"
      MP_ENABLED=true
      AC_MSG_RESULT([$MP_PATH_RESULT])
   fi

   if test "x$MP_ENABLED" = "xtrue"; then
      dnl Check for a well-known source file in MetaPRL.
      AC_MSG_CHECKING([for MetaPRL files])
      if test ! -r "$MP_PATH/refiner/refiner/refine.ml"; then
         AC_MSG_RESULT([not found])
         AC_MSG_ERROR([Cannot find MetaPRL in $MP_PATH.])
      fi
      AC_MSG_RESULT([found])

      dnl Check for the mk/config file; if missing, try to create it.
      AC_MSG_CHECKING([for MetaPRL mk/config file])
      if test ! -r "$MP_PATH/mk/config"; then
         AC_MSG_RESULT([cannot find mk/config, using default])
         cp "meta-prl/config.default" "$MP_PATH/mk/config"
         if test $? != 0; then
            AC_MSG_ERROR([Unable to copy default mk/config file to MetaPRL])
         fi
      else
         AC_MSG_RESULT([found mk/config, we will use that])
      fi
   fi

   dnl Check to see if the user wants ALL of MetaPRL built (otherwise
   dnl we'll just build the small chunks which we have to have).
   dnl
   AC_MSG_CHECKING([for --enable-full-metaprl])
   AC_ARG_ENABLE(full-metaprl, [  --enable-full-metaprl   Build all parts of MetaPRL [no]], , enable_full_metaprl=no)
   if test "x$enable_full_metaprl" = "xyes"; then
      AC_MSG_RESULT([building all of MetaPRL])
      MP_FULL=true
   else
      AC_MSG_RESULT([only building MetaPRL parts required for MCC])
      MP_FULL=false
   fi

   dnl Check to see if we should skip checks for certain interface
   dnl and library files in the OCaml standard library.  This option
   dnl should not be given by end-users...
   dnl
   AC_MSG_CHECKING([for --enable-metaprl-checks])
   AC_ARG_ENABLE(metaprl-checks, [  --enable-metaprl-checks Run OCaml standard library checks [yes] (recommended)], , enable_metaprl_checks=yes)
   if test "x$enable_metaprl_checks" = "xno"; then
      AC_MSG_RESULT([checks disabled (but this isn't recommended!)])
      run_mp_checks=0
   else
      AC_MSG_RESULT([checks enabled])
      run_mp_checks=1
   fi

   dnl MetaPRL requires threading support; check for the appropriate
   dnl threads library.  Also, MetaPRL requires a few interfaces be
   dnl installed that aren't installed by default in OCaml.
   dnl
   if test "x$MP_ENABLED" = "xtrue" -a "x$run_mp_checks" = "x1"; then
      dnl Make sure OCaml has been configured properly
      if test "x$OCAML_STD_LIB" = "x" -o "x$OCAML_SUFFIX_LIB" = "x"; then
         AC_MSG_ERROR([OCaml configure variables undefined; was OCaml found?])
      fi

      dnl Find the thread and camlp4 libraries.
      required_caml_libraries="threads/threads camlp4/camlp4 camlp4/odyl"
      for caml_library in $required_caml_libraries; do
         for caml_extension in $OCAML_SUFFIX_LIB .a; do
            caml_library_file="$caml_library$caml_extension"
            AC_MSG_CHECKING([for $caml_library_file library])
            if test ! -r "$OCAML_STD_LIB/$caml_library_file"; then
               AC_MSG_RESULT([not found])
               AC_MSG_ERROR([OCaml thread library $caml_library_file not found])
            fi
            AC_MSG_RESULT([found])
         done  dnl Extensions
      done  dnl Base names

      dnl Check for required OCaml interfaces and libraries
      required_caml_interfaces="parsetree longident typecore location camlp4/odyl_main"
      for caml_interface in $required_caml_interfaces; do
         caml_interface_file="$caml_interface.cmi"
         AC_MSG_CHECKING([for $caml_interface_file interface])
         if test ! -r "$OCAML_STD_LIB/$caml_interface_file"; then
            AC_MSG_RESULT([not found])
            AC_MSG_ERROR([Failed to find $caml_interface_file in $OCAML_STD_LIB, required by MetaPRL])
         fi
         AC_MSG_RESULT([found])
      done

      dnl Check for required OCaml object files
      required_caml_objects="camlp4/q_MLast camlp4/pa_extend camlp4/pr_dump camlp4/pa_o"
      for caml_object in $required_caml_objects; do
         for caml_extension in .cmx .o; do
            caml_object_file="$caml_object$caml_extension"
            AC_MSG_CHECKING([for $caml_object_file object])
            if test ! -r "$OCAML_STD_LIB/$caml_object_file"; then
               AC_MSG_RESULT([not found])
               AC_MSG_ERROR([Failed to find $caml_object_file in $OCAML_STD_LIB, required by MetaPRL])
            fi
            AC_MSG_RESULT([found])
         done  dnl Extensions
      done  dnl Base names
   fi    dnl Check that the OCaml standard library is sound?

   dnl Print final result and add new subst's.
   dnl
   AC_SUBST(MP_PATH)
   AC_SUBST(MP_ENABLED)
   AC_SUBST(MP_FULL)

   dnl Run either the ``found'' or ``not found'' action.
   dnl
   if test "x$MP_ENABLED" = "xtrue"; then
      ifelse([$1], , :, [$1])
   else
      ifelse([$2], , :, [$2])
   fi
])


